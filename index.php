<?php
session_start();
if(!isset($_SESSION['app_lang']))
{
    $_SESSION['app_lang'] = 'sk';
}


/**
 * Shared settings
 */
define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/'));
define('PUBLIC_DIR', APPLICATION_PATH.'/');
define('APP_DIR', APPLICATION_PATH.'/app');
define('TEMP_DIR', APPLICATION_PATH . '/temp');
define('LATTE_TEMP_DIR', TEMP_DIR . '/Latte');
define('DEV_DIR', APPLICATION_PATH . '/dev');
define('LIB_DIR',APPLICATION_PATH.'/vendor');
define('GLOBAL_DIR',APP_DIR.'/global');
define('TEMPLATE_DIR',GLOBAL_DIR.'/templates');
define('NOTIFICATION_TEMPLATE_DIR',TEMPLATE_DIR.'/notification');
define('MODUL_DIR',APP_DIR.'/modules');
define('GENERATOR_DIR', MODUL_DIR . '/generator/skelet');
define('INVOICE_DIR', APPLICATION_PATH . '/invoices');
define('WEB_NAME', 'COACH RUFUS');
define('LANG', $_SESSION['app_lang']);
define('DEFAULT_MAIL_SENDER', [
    'name' => 'Coach Rufus',
    'email' => 'info@coachrufus.com'
]);

/**
 * setup enviroment
 */
require_once APP_DIR . '/config/env_config.php';
require_once APP_DIR . '/config/db_config.php';
require_once APP_DIR . '/config/mail_config.php';
require_once APP_DIR . '/config/config.php';

$register_namespaces = array(
    'PH\Base' => MODUL_DIR . '/base',
    'Base' => MODUL_DIR . '/base',
    'PH\Demo' => MODUL_DIR . '/demo',
    'User' => MODUL_DIR . '/user',
    'Social' => MODUL_DIR . '/social',
    'Notification' => MODUL_DIR . '/notification',
    'Core\Notification' => MODUL_DIR . '/notification',
    'Generator' => MODUL_DIR . '/generator',
    'Webteamer\Team' => MODUL_DIR . '/Team',
    'CR\Team' => MODUL_DIR . '/Team',
    'Webteamer\Team\Lib' => MODUL_DIR . '/Team',
    'Webteamer\Sport' => MODUL_DIR . '/Sport',
    'Webteamer\Locality' => MODUL_DIR . '/Locality',
    'Webteamer\Player' => MODUL_DIR . '/Player',
    'CR\Player' => MODUL_DIR . '/Player',
    'Webteamer\Event' => MODUL_DIR . '/Event',
    'CR\Widget' => MODUL_DIR . '/widget',
    'CR\TeamSeason' => MODUL_DIR . '/TeamSeason',
    'CR\TeamPlayground' => MODUL_DIR . '/TeamPlayground',
    'CR\Socializing' => MODUL_DIR . '/Socializing',
    'CR\Message' => MODUL_DIR . '/Message',
    'CR\Scouting' => MODUL_DIR . '/Scouting',
    'CR\Pay' => MODUL_DIR . '/Pay',
    'CR\Gamification' => MODUL_DIR . '/Gamification',
    'Coachrufus\Api' => MODUL_DIR . '/Api',
    'GoPay' => LIB_DIR . '/GoPay',
    'GetResponse' => LIB_DIR . '/GetResponse',
    'CR\Tournament' => MODUL_DIR . '/tournament',
    'CR\HpWall' => MODUL_DIR . '/HpWall',
    'Tournament' => LIB_DIR . '/Tournament',
    '' => LIB_DIR,
);


$web_app = Core\WebApp::getInstance();
$web_app->setNamespaces($register_namespaces);
$web_app->addModuleConfig(MODUL_DIR.'/base/config/config.php');
$web_app->addModuleConfig(MODUL_DIR.'/user/config/config.php');
$web_app->addModuleConfig(MODUL_DIR.'/notification/config/config.php');
$web_app->addModuleConfig(MODUL_DIR.'/social/config/config.php');
$web_app->addModuleConfig(MODUL_DIR.'/generator/config/config.php');
$web_app->addModuleConfig(MODUL_DIR.'/Sport/config/config.php');
$web_app->addModuleConfig(MODUL_DIR.'/Locality/config/config.php');
$web_app->addModuleConfig(MODUL_DIR.'/Player/config/config.php');
$web_app->addModuleConfig(MODUL_DIR.'/Team/config/config.php');
$web_app->addModuleConfig(MODUL_DIR.'/widget/config/config.php');
$web_app->addModuleConfig(MODUL_DIR.'/TeamSeason/config/config.php');
$web_app->addModuleConfig(MODUL_DIR.'/TeamPlayground/config/config.php');
$web_app->addModuleConfig(MODUL_DIR.'/Event/config/config.php');
$web_app->addModuleConfig(MODUL_DIR.'/Socializing/config/config.php');
$web_app->addModuleConfig(MODUL_DIR.'/Message/config/config.php');
$web_app->addModuleConfig(MODUL_DIR.'/Scouting/config/config.php');
$web_app->addModuleConfig(MODUL_DIR.'/Pay/config/config.php');
$web_app->addModuleConfig(MODUL_DIR.'/Gamification/config/config.php');
$web_app->addModuleConfig(MODUL_DIR.'/HpWall/config/config.php');
$web_app->addModuleConfig(MODUL_DIR.'/Api/config/config.php');
$web_app->addModuleConfig(MODUL_DIR.'/tournament/config/config.php');


try
{

    /*
    if( $_SESSION['m_access'] != 'yes')
    {
        header('location: /maintenance');
        exit;
    }
    */
    
    header('Access-Control-Allow-Origin: *');  
    
    $layout = Core\ServiceLayer::getService('layout');
    $content = $web_app->processUri();

    /*
    if( $_SESSION['m_access'] != 'yes' && 'maintenance' != \Core\Router::$current_route_name && $layout->getTemplate() != null)
    {
        header('location: /maintenance');
        exit;
    }
    */
    
    if ($layout->getTemplate() != null)
    {
        $layout->renderTemplate(array(
                'module_content' => $content,
                'layout' => $layout,
        ));
    }
    else
    {
        echo $content;
    }
}
catch(AclException $e)
{
    $request = Core\ServiceLayer::getService('request');
    $router = Core\ServiceLayer::getService('router');
    //dont redirect ajax calls with rurl
    if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH']=="XMLHttpRequest")
    {
          $request->redirect($router->link('login'));
    }
    else 
    {
         $request->redirect($router->link('login',array('rurl' => filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL))));
    }

    
    
   
}
catch(Exception $e)
{
    //action_file_not_exist
    if($e->getCode() == 001)
    {
        echo 'action_file_not_exist';
    }
    else
    {
        throw $e;
    }
}
catch(Swift_RfcComplianceException $e)
{
    Request::Redirect('/crash-page');
}