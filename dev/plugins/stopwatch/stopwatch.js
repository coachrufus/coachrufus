

  var Stopwatch = function(elem, options) {
      //var timer       = createTimer(),
      var timer       = elem,
      startButton = createButton($('#stopwatch-control .time_start'), start),
      stopButton  = createButton($('#stopwatch-control .time_pause'), stop),
      //resetButton = createButton("reset", reset,'<i class="fa fa-stop"></i>'),
      offset,
      clock,
      interval;

  // default options
  options = options || {};
  options.delay = options.delay || 1;
  // append elements     
  /*
  elem.append(timer);
  $(elem).append(startButton);
  $(elem).append(stopButton);
      */
  //$(elem).append(resetButton);
  /*
  elem.appendChild(startButton);
  elem.appendChild(stopButton);
  elem.appendChild(resetButton);
      */
  // initialize
  reset();
  
  

  // private functions
  function createTimer() {
    return document.createElement("span");
  }

  function createButton(elem, handler) {
    //var a =  $('<a/>',{'class':'btn'});
    //a.html(inner);

    elem.on('click',function(event){
        handler();
        event.preventDefault(elem);
    });
    /*
    var a = document.createElement("a");
    a.href = "#" + action;
    a.innerHTML = action;
    a.addEventListener("click", function(event) {
      handler();
      event.preventDefault();
    });
    */
    //return a;
  }

  function start() {
    if (!interval) {
      offset   = Date.now();
      
      
       var current_time = timer.val();
       current_time_parts = current_time.split(':');
       current_time_hours = parseInt(current_time_parts[0]);
       current_time_minutes = parseInt(current_time_parts[1]);
       current_time_seconds = parseInt(current_time_parts[2]);
       clock = (current_time_hours*3600)+(current_time_minutes*60)+current_time_seconds;
      clock = clock*1000;
      
      interval = setInterval(update, options.delay);
      
      $('#stopwatch-control .time_pause').show();
      $('#stopwatch-control .time_start').hide();
      
       $('#stopwatch-control .time_pause').css("display", "inline-block");
    }
  }

  function stop(elem) {
     clearInterval(interval);
      interval = null;

      $('#stopwatch-control .time_pause').hide();
      $('#stopwatch-control .time_start').show();
       $('#stopwatch-control .time_start').css("display", "inline-block");
      
      /*
      if (interval) {
      clearInterval(interval);
      interval = null;
    }
    else
    {
        interval = setInterval(update, options.delay);
    }
    */
  }

  function reset() {
    clock = 0;
    render();
  }

  function update() {
    clock += delta();
    render();
  }

  function render() {
      total_seconds = Math.floor(clock/1000);
      
      hour = Math.floor(total_seconds/3600);
      hour_rest = total_seconds-(3600*hour);

      
      minutes =  Math.floor(hour_rest/60);
      seconds = hour_rest-(minutes*60);
     
      
      timer.val( ("0" + hour).slice(-2)+':'+("0" + minutes).slice(-2)+':'+ ("0" + seconds).slice(-2)); 
  }

  function delta() {
    var now = Date.now(),
        d   = now - offset;

    offset = now;
    return d;
  }

  // public API
  this.start  = start;
  this.stop   = stop;
  this.reset  = reset;
  };


