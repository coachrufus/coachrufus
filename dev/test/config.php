<?php
define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../../'));
define('APP_DIR', APPLICATION_PATH.'/app');
define('MODUL_DIR',APP_DIR.'/modules');
define('LIB_DIR',APPLICATION_PATH.'/vendor');
define('WEB_DOMAIN', 'http://app.coachrufus.lamp');
define('RUN_ENV', 'prod');