<?php
namespace Coachrufus\Api;


class SystemNotifyTest  extends \PHPUnit_Framework_TestCase 
{
    private $apiUrl = WEB_DOMAIN;
    protected function setUp()
    {
        
    }

    protected function tearDown()
    {
    }

    public function testSystemNotifyUnreadList()
    {
        $url = '/api/system-notify/unread';

        //success
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $response = $this->call($url,'GET',$data);
        var_dump($response);
        
        /*
        //bad user id
        $data['user_id'] = '0';
        $response2 = $this->call($url,'GET',$data);
        $list['BAD_USER_ID'] = array('response' => $response2);
        
        //bad api key
        $data['apikey'] = '-';
        $data['user_id'] = '3';
        $response3 = $this->call($url,'GET',$data);
        $list['BAD_API_KEY'] = array('response' => $response3);
        
        //bad method
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $response4 = $this->call($url,'POST',$data);
        $list['BAD_METHOD'] = array('response' => $response4);

        return $this->render('Coachrufus\Api\ApiModule:test:result.php', array(
                    'url' => $this->apiUrl.$url,
                    'list' => $list
        ));
         
         
         
         
         $manager = $this->getManager();
         
         $scheduleMockBuilder = new \CR\Tournament\Model\ScheduleMockBuilder();
         $tournamentMockBuilder = new \CR\Tournament\Model\TournamentMockBuilder();
         $teamMockBuilder = new \CR\Tournament\Model\TeamMockBuilder();
         
         $scheduleMock = $scheduleMockBuilder->getOneGroupMock();
         $tournamentMock = $tournamentMockBuilder->getTournamentMock();
         
         $scheduleDefinition =json_decode($scheduleMock->getJson(),true);
         
        
         $teams = array();
         foreach($scheduleDefinition['schedule']['teams'] as $key => $teamName)
         {
             $team =  $teamMockBuilder->getTeamMock();
             $team->method('getName')->willReturn($teamName); 
             $team->method('getId')->willReturn($key+1); 
             $teams[] = $team;
         }
         
        $result = $manager->generateTournamentEvents($tournamentMock,$teams,$scheduleMock);
         * 
         */
    }
    
     private function call($apiMethod = null, $httpMethod = 'GET', $params = array())
    {
        $ch = curl_init();

        $header = array( 
            'Accept: application/json',
        );
        $urlParams = '';
        $restParams = '';
        if ($httpMethod == 'POST')
        {
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
            //$header[] = 'Content-Type: application/x-www-form-urlencoded';
        }
        elseif ('PUT' == $httpMethod)
        {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
        }
        elseif ('DELETE' == $httpMethod)
        {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
           curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
        }
        else
        {
            //$header[] = 'Content-Type: application/json';
            if (!empty($params))
            {
                $urlParams = '?' . http_build_query($params);
            }
        }


        $restUrl = $this->apiUrl . $apiMethod . $urlParams;


        curl_setopt($ch, CURLOPT_URL, $restUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        curl_close($ch);

        if (RUN_ENV == 'dev')
        {

            t_dump('API URL:' . $restUrl);
            t_dump('API PARAMS:');
            t_dump($params);
            t_dump('API RAW RESPONSE: ' . $response);
            t_dump(json_decode($response, true));
        }



        return json_decode($response, true);
    }
    
    
    
    
    
}
