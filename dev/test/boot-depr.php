<?php
define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../../'));

echo APPLICATION_PATH;

define('PUBLIC_DIR', APPLICATION_PATH.'/');
define('APP_DIR', APPLICATION_PATH.'/app');
define('DEV_DIR', APPLICATION_PATH . '/dev');
define('LIB_DIR',APPLICATION_PATH.'/vendor');
define('GLOBAL_DIR',APP_DIR.'/global');
define('MODUL_DIR',APP_DIR.'/modules');
define('GENERATOR_DIR', MODUL_DIR . '/generator/skelet');
define('RUN_ENV', 'dev');
define('WEB_DOMAIN', 'http://teamer.plus421');
define('WEB_NAME', 'WEBTEAMER');
define('VERSION', rand(1,1000));


require_once(APP_DIR.'/config/config.php');


 $register_namespaces = array(
			'PH\Base' => MODUL_DIR.'/base',
			'PH\Demo' => MODUL_DIR.'/demo',
			'User' => MODUL_DIR.'/user',
			'Social' => MODUL_DIR.'/social',
			'Notification' => MODUL_DIR.'/notification',
			'Generator' => MODUL_DIR.'/generator',
			'Webteamer\Team' => MODUL_DIR.'/Team',
			'Webteamer\Sport' => MODUL_DIR.'/Sport',
			'Webteamer\Locality' => MODUL_DIR.'/Locality',
			'Webteamer\Player' => MODUL_DIR.'/Player',
			'Webteamer\Event' => MODUL_DIR.'/Event',
			'' => LIB_DIR,
	);

$web_app = Core\WebApp::getInstance();
$web_app->setNamespaces($register_namespaces);
$web_app->addModuleConfig(MODUL_DIR.'/base/config/config.php');
$web_app->addModuleConfig(MODUL_DIR.'/user/config/config.php');
$web_app->addModuleConfig(MODUL_DIR.'/notification/config/config.php');
$web_app->addModuleConfig(MODUL_DIR.'/social/config/config.php');
$web_app->addModuleConfig(MODUL_DIR.'/generator/config/config.php');

$web_app->addModuleConfig(MODUL_DIR.'/Team/config/config.php');
$web_app->addModuleConfig(MODUL_DIR.'/Sport/config/config.php');
$web_app->addModuleConfig(MODUL_DIR.'/Locality/config/config.php');
$web_app->addModuleConfig(MODUL_DIR.'/Player/config/config.php');
$web_app->addModuleConfig(MODUL_DIR.'/Event/config/config.php');
