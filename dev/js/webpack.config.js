module.exports =
{
	publicPath: './Modules/',
	entry: "./Modules/index.js",
	output:
	{
		path: __dirname + "/Modules/bundle/",
		filename: "/index.js"
	},
	module:
	{
		loaders: [
			{ test: /\.js$/, exclude: /node_modules/, loader: "babel-loader"}
		]
	}
}