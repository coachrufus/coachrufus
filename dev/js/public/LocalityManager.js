var LocalityManager = function (element)
{
   
    this.google;
    this.searchInput = document.getElementById('pac-input');
    this.places;
    this.markers = [];
    this.map;
    this.map_center;
    this.map_zoom = 8;
    this.searchCallback = function(latLng){ this.setLocationInfoByGeocoder(latLng)};

    
    
};

LocalityManager.prototype.createSearchInput = function(element_id)
{

    var search_input = this.searchInput;
    if(element_id != null)
    {
        search_input = document.getElementById(element_id);
    }

    var manager = this;
    var searchBox = new this.google.maps.places.SearchBox(search_input);
    searchBox.addListener('places_changed', function () {
        manager.places = searchBox.getPlaces();
        manager.map_center =  manager.places[0].geometry.location;
        //manager.setLocationInfoByGeocoder(manager.map_center);
        manager.searchCallback(manager.map_center);
        manager.map_zoom = 15;
        
         manager.map = new manager.google.maps.Map(document.getElementById('map'), {
            center: manager.map_center,
            zoom: manager.map_zoom,
            mapTypeId: manager.google.maps.MapTypeId.ROADMAP
        });
        manager.placeMarker(manager.map,manager.map_center);
    });
    

};

LocalityManager.prototype.setLocationInfoByGeocoder = function(latLng)
{
    var manager = this;
    var geocoder = new manager.google.maps.Geocoder;
    geocoder.geocode({'location': latLng}, function (results, status) {
            if (status === manager.google.maps.GeocoderStatus.OK) {
                
                if (results[0]) {

                    $('.locality_json_data').val(JSON.stringify(results[0]));

                } else {
                    window.alert('No results found');
                }
            } else {
                window.alert('Geocoder failed due to: ' + status);
            }
        });
};


LocalityManager.prototype.placeMarker = function(map, location)
{

    this.markers.forEach(function (marker) {
        marker.setMap(null);
    });

    var marker = new this.google.maps.Marker({
        position: location,
        draggable:true,
        map: map
    });
    
    this.markers.push(marker);
    
    var manager = this;
     manager.google.maps.event.addListener(marker, 'dragend', function (event) {
        //manager.setNewLocation(event.latLng);
         manager.setLocationInfoByGeocoder(event.latLng);
    });

};
