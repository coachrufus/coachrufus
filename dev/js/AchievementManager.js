
var AchievementManager = function ()
{
   $('.s2s_skip_trigger').on('click',function(e){
       e.preventDefault();
       $('#'+$(this).attr('data-skip-id')).modal('hide');
       $('#skip_modal').modal('show');
   });
   $('.s2s_skip_cancel').on('click',function(e){
       e.preventDefault();
       $('#skip_modal').modal('hide');
   });
   
   $('.s2s_modal_continue').on('click',function(){
       $('.progress-modal').modal('hide');
   });
};


AchievementManager.prototype.showAlert = function(element_id)
{
    if(element_id != 'team_create')
    {
         $('#'+element_id).modal('show'); 
    }
    
   
};
