
var ScoutingManager = function (data)
{
    this.widgetContainer = $('.scouting-widget');
    this.dataUrl = data.dataUrl;
    this.displayPanelSettings = true;
    this.addModal = $('#add-scouting-modal');
    this.replyModal = $('#reply-scouting-modal');
    //this.localitySearch = function(){};

};

ScoutingManager.prototype.showLoader = function ()
{
    this.widgetContainer.html(' <div class="alert alert-info text-center"><i class="fa fa-refresh fa-spin fa-3x fa-fw"></i></div>Loading');
};

ScoutingManager.prototype.hideLoader = function ()
{
    this.widgetContainer.html('');
};


ScoutingManager.prototype.addShareScouting = function(){
    $('.share-scouting').on('click',function(e){
        e.preventDefault();
        var link = $(this).attr('href');
         FB.ui({
             method: 'share',
             display: 'popup',
             href: link,
             hashtag: '#coachrufus'
           }, function(response){});
     });
};

ScoutingManager.prototype.addLoadPostsTrigger = function ()
{
    var manager = this;
    $('.load-scouting').on('click',function(e){
       e.preventDefault();
       
       var filter_data = $('.scouting-search-form').serializeObject() ;
       filter_data['from'] = $(this).attr('data-start');
      var data = {'url':$(this).attr('href'),'filter':filter_data};

       
      // filter_data['url'] = $(this).attr('href');
       //console.log(filter_data);
       
      //var data = {'from':$(this).attr('data-start'),'url':$(this).attr('href')};
       $('.load-scouting').hide();
       manager.loadPosts(data);
       $('#team_scouting_panel').append('<div class="loader text-center"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></div>');
      
   });
   
   $(document).on('scroll',function(){
        var loader = $('.load-scouting');
        var is_scrolled = isScrolledIntoView(loader);
        if(is_scrolled && $('.load-scouting').is(":visible"))
        {
            loader.trigger('click');
        }
    });
    
};


ScoutingManager.prototype.loadPosts = function (data)
{
    $.ajax({
            method: "GET",
            url: data.url,
            data: data.filter,
            dataType: 'json'
        }).done(function (response) {
            
            $.each(response.lines, function (key, val) {
                $('#team_scouting_panel').append(val);
             });
             
             if(response['allItemsLoaded'] ==true)
             {
                 $('.load-scouting').hide();
                 //$('.load-scouting').remove();
             }
             else
             {
                  $('.load-scouting').show();
             }
             
             $('.load-scouting').attr('data-start',response.start);
             $('#team_scouting_panel').find('.loader').remove();
        });
};


ScoutingManager.prototype.loadWidget = function (google) {
    this.showLoader();
    var manager = this;

    action = this.dataUrl;
    $.ajax({
        method: "GET",
        url: action,
        data: {'rt': 'html'}
    }).done(function (response) {
        manager.widgetContainer.html(response);
        manager.replyModal = $('#reply-scouting-modal');
        manager.bindTriggers();
        manager.bindSearchFormLocationTrigger(google);
    });
};

ScoutingManager.prototype.bindAddScouting = function () {
    
    
    var  manager = this;
    
    $(document).on('click', '.add-scouting-trigger', function (e) {
        e.preventDefault();
        manager.addModal.find('.scouting-form-wrap').hide();
        
        console.log(manager.addModal);
        
        var target_form = $(this).attr('data-form');
        
         console.log(target_form);
        
        $('.' + target_form).show();
        manager.addModal.modal('show');
    });
};

ScoutingManager.prototype.bindSendForm = function () {
   var manager = this;
    $('.scouting-form').on('submit', function (e) {
        e.preventDefault();
        
        var form = $(this);
        var data = form.serialize();
        var action = form.attr('action');


        $.ajax({
            method: "POST",
            url: action,
            data: data,
            dataType: 'json'
        }).done(function (response) {
            form.find('.control-label-error').remove();
            if (response.result == 'ERROR')
            {
                $.each(response.errors, function (key, val) {
                    form.find('[data-error-bind="' + key + '"]').append('<span class="control-label-error">' + val + '</span>');
                });
            }
            else
            {
                manager.addModal.modal('hide');
                $('#team_scouting_panel').find('.scouting-row').last().remove();

                $('#team_scouting_panel').prepend(response.row);

                $('#team_scouting_panel').effect("highlight", {}, 2000);
                $('.empty-scouting').hide();

                $('html, body').animate({
                    scrollTop: $('#team_scouting_panel').offset().top
                }, 500, function () {

                });

            }

        });

    });
};



ScoutingManager.prototype.bindReply = function () {
    var  manager = this;
    $(document).on('click', '.scouting-reply', function (e) {
        e.preventDefault();
        subject = $(this).parents('.scouting-row').find('.sr-cnt h3');
        text = $(this).parents('.scouting-row').find('.sr-cnt p');
        action = $(this).attr('href');
        manager.replyModal.find('form').attr('action',action);
        manager.replyModal.find('form textarea').val('');
        manager.replyModal.find('form').show();
        manager.replyModal.find('.send-success').hide();
        
        $('#reply_subject').val('RE:'+subject.text());
        $('#reply-scouting').html(text.text());
        manager.replyModal.modal('show');
    });
    
    $('.scouting-reply-form').on('submit', function (e) {
        e.preventDefault();
        
        var form = $(this);
        var data = form.serialize();
        var action = form.attr('action');
        
        manager.replyModal.find('.modal-loader').show();
        


        $.ajax({
            method: "POST",
            url: action,
            data: data,
            dataType: 'json'
        }).done(function (response) {
            form.find('.control-label-error').remove();
            manager.replyModal.find('.modal-loader').hide();
            if (response.result == 'ERROR')
            {
                $.each(response.errors, function (key, val) {
                    form.find('[data-error-bind="' + key + '"]').append('<span class="control-label-error">' + val + '</span>');
                });
            }
            else
            {
                manager.replyModal.find('form').hide();
                manager.replyModal.find('.send-success').fadeIn();
                //manager.replyModal.modal('hide');
            }

        });

    });
    
};



ScoutingManager.prototype.bindSearchFormTriggers = function()
{
    $(document).on('click','.add_scouting_criteria_trigger',function(e){
       e.preventDefault();
       var target_elem = $('#'+$(this).attr('data-target'));
       
       var  data_type_name = $(this).text();
       var  data_type_val = $(this).attr('data-type-val');
       var  data_type = $(this).attr('data-type');
       
       var criteria = $('<span/>',{'text':data_type_name});
       var criteria_input = $('<input/>',{'type':'hidden','name':data_type+'[]','value':data_type_val});
       var criteria_close = $('<i/>',{'class':'ico ico-close', 'data-target':$(this).attr('data-target').replace('current-','')});

       criteria.prepend(criteria_input);
       criteria.prepend(criteria_close);
       
       target_elem.append(criteria);
       target_elem.show();
       $(this).parent().remove();
    });
    
    
    $(document).on('click','.selected-choices .ico-close',function(){
        
        var target_elem = $('#'+$(this).attr('data-target'));
        var data_type = $(this).parent().find('input').val();
        var data_type_name = $(this).parent().text();
        var select_choice = $('<li/>');
        var data_target = 'current-'+data_type;
        var select_choice_link = $('<a/>',{'text':data_type_name,'class':'add_scouting_criteria_trigger','data-type':data_type,'data-target':data_target, 'href': '#'});
        select_choice.append(select_choice_link);

        target_elem.append(select_choice);
        $(this).parent().remove();
    });
    
    //create location search

       
        /*
        manager.places = searchBox.getPlaces();
        manager.map_center =  manager.places[0].geometry.location;
        manager.setTeamLocationByGeocoder(manager.map_center);
        manager.map_zoom = 15;
        
         manager.map = new manager.google.maps.Map(document.getElementById('map'), {
            center: manager.map_center,
            zoom: manager.map_zoom,
            mapTypeId: manager.google.maps.MapTypeId.ROADMAP
        });
        manager.placeMarker(manager.map,manager.map_center);
        */
    
    
    
    
};

ScoutingManager.prototype.bindSearchFormLocationTrigger = function(google){
    var searchBox = new google.maps.places.SearchBox(document.getElementById('search-scouting-form'));
    searchBox.addListener('places_changed', function () {
        places = searchBox.getPlaces();
        map_center =  places[0].geometry.location;
        $('#search-scouting-form-coordinates').val(map_center.lat()+','+map_center.lng());
    });
};

 



ScoutingManager.prototype.bindTriggers = function () {

    var manager = this;
    manager.bindAddScouting();
    manager.bindSendForm();
    manager.bindReply();
    manager.addLoadPostsTrigger();
    manager.bindSearchFormTriggers();
    manager.addShareScouting();
    
    var sidebarTimeout;

    $(document).on('mouseenter click', '.sr-sidebar-trigger', function (e) {
        e.preventDefault();
        clearTimeout(sidebarTimeout);
         $('.sr-sidebar-trigger').removeClass('active');
         $('.sr-sidebar-cnt').hide();
        
        $('#'+$(this).attr('data-target')).show();
        $(this).addClass('active');
    });
    
    $(document).on('mouseleave', '.sr-sidebar-trigger', function () {
        target=  $('#'+$(this).attr('data-target'));
        sidebarTimeout = setTimeout(function(){
             target.hide();
              $('.sr-sidebar-trigger').removeClass('active');
        }, 100); 

    });
    
    $(document).on('mouseenter', '.sr-sidebar-cnt', function () {
         clearTimeout(sidebarTimeout);

    });

    $(document).on('mouseleave', '.sr-sidebar-cnt', function () {
         target = $(this);
         sidebarTimeout = setTimeout(function(){
             target.hide();
              $('.sr-sidebar-trigger').removeClass('active');
        }, 100); 

    });


    $(document).on('click', '.remove-scouting-trigger',function (e) {
        e.preventDefault();
        $('#removeScoutingModal .modal_submit').attr('data-href', $(this).attr('href'));
        $('#removeScoutingModal').modal('show');

    });

    $(document).on('click','#removeScoutingModal .modal_submit', function () {
        location.href = $(this).attr('data-href');
    });


    $('.player-scouting-form-trigger').on('click', function (e) {
        e.preventDefault();
        var form_wrap = $('.scouting-form-wrap3');
        form_wrap.slideDown();
    });


    $('#scouting-form-choice').on('change', function () {
        $('.scouting-form-wrap').hide();


        if ('' != $('#scouting-form-choice').val())
        {
            var form_wrap = $('.scouting-form-wrap' + $('#scouting-form-choice').val());
            form_wrap.slideDown();
        }
    });

    

};

