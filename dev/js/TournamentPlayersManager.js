
/*TEAM PLAYERS*/
var TournamentPlayersManager = function()
{
    this.rating_url = '/player/add-rating';
    this.rating_trigger_class = '.score_trigger';
     
    this.change_role_url = '/team/players/edit';
    this.change_role_trigger_class = '.change-role-trigger';
    this.remove_modal = $('#removePlayerModal');
    this.unactivate_modal = $('#unactivatePlayerModal');
    this.edit_modal = $('#edit-player-modal');
    this.import_modal = $('#import-players-modal');
    this.unregistred_invitation_modal = $('#unregistred-invitation-modal');
    this.remove_url = '';
    this.unactivate_url = '';
    this.remove_row;
};

TournamentPlayersManager.prototype.bindTriggers = function(){
    var manager = this;

    manager.bindModalEditForm();
    
    $('.unregistred_invitation').on('click',function(e){
        e.preventDefault();
        $('#unregistred-invitation-modal-form').attr('action',$(this).attr('href'));
        
        
        manager.unregistred_invitation_modal.modal('show');
    });
    
    $(this.change_role_trigger_class).on('click',function(e){
        e.preventDefault();
        manager.changeRole($(this));
    });
    
    $(this.rating_trigger_class).on('click',function(e){
        e.preventDefault();
        manager.sendRating($(this));
    });
    
    $('#import_players').on('click',function(e){
        e.preventDefault();
        manager.import_modal.modal('show');
    });
    
    
     this.fileuploadTrigger();
    

    $('#add_player').on('click',function(e){
        e.preventDefault();
        
        //vat current_row = $('this')
        
        var base_template_row = $('.template_row');
        
        var action_row = $('#action_row');
        var rand_hash = Math.random().toString(36).substring(7);
        var template_row = $('.template_row').clone();
        
        first_name_input = template_row.find('#player_first_name0');
        first_name_input.attr('name','player['+rand_hash+'][first_name]');
        first_name_input.attr('id','player_first_name'+rand_hash+'');
        //first_name_input.val('');
        
        
        last_name_input = template_row.find('#player_last_name0');
        last_name_input.attr('name','player['+rand_hash+'][last_name]');
        last_name_input.attr('id','player_last_name'+rand_hash+'');
        //last_name_input.val('');
        
        email_input =  template_row.find('#player_email0');
        email_input.attr('name','player['+rand_hash+'][email]');
        email_input.attr('id','player_email'+rand_hash+'');
        email_input.attr('data-row',rand_hash);
        //email_input.val('');
        
        player_id_input =  template_row.find('#player_id0');
        player_id_input.attr('name','player['+rand_hash+'][player_id]');
        player_id_input.attr('id','player_id'+rand_hash+'');
        //player_id_input.val('');

        /*
        remove_btn = $('<a/>',{'class':'rm_player_btn','href':'#'});
        remove_btn_ico = $('<i/>',{'class':'ico ico-cross'});
        remove_btn.append(remove_btn_ico);
        
        remove_btn.on('click',function(e){
            e.preventDefault();
            $(this).parents('.player_row').remove();
        });
        
        
        template_row.find('.btn_col').html('');
        template_row.find('.btn_col').append(remove_btn);
        */
        
        autocomplete = new UserSearchAutocomplete(email_input);
        autocomplete.initEmailSearch();
        autocomplete.onclick = 'fillInput';
        
       
        template_row.removeClass('template_row');
        $('#add_player_wrap').append(template_row);
        //reset template
        base_template_row.find('#player_first_name0').val('');
        base_template_row.find('#player_last_name0').val('');
        base_template_row.find('#player_email0').val('');
        base_template_row.find('#player_id0').val('');
        
        
       
        //base_template_row.before(template_row);
        
        //add remove button
       
       
        /*
        current_remove_btn = $('<a/>',{'class':'rm_player_btn','href':'#'});
        current_remove_btn_ico = $('<i/>',{'class':'ico ico-cross'});
        current_remove_btn.append(current_remove_btn_ico);
        
        current_remove_btn.on('click',function(e){
            e.preventDefault();
            $(this).parents('.player_row').remove();
        });
        
        $(this).parents('.player_row').find('.btn_col').append(current_remove_btn);
        */
       
   
    });
    
    $(document).on('click', '.rm_player_btn', function (e) {
        e.preventDefault();
        $(this).parents('.player_row').remove();
    });
    
    $('#add_player_form').on('submit',function(e){
        var errors = false;
        $('div.player_row').each(function(index, val){
           
            player_first_name =  $(val).find('.player_first_name_row').val();
            player_last_name =  $(val).find('.player_last_name_row').val();
            player_email =  $(val).find('.player_email_row').val();
            
            if(player_email != '')
            {
                if(player_first_name == '' && player_last_name == '')
                {
                    errors = true; 
                }
            }
        });
        
        if(errors)
        {
            $('#error-alerts').addClass('alert alert-danger');
            return false;
        }
        else
        {
            return true;
        }

    });
    
    
   $('.add_player_form_trigger').on('click',function(e){
        e.preventDefault();
        
        $('.player_row').each(function(index, val){
            
            $(val).parent().find('input').each(function(index, val){
                console.log($(val).val());
            });
        });
        
         //manager.sendForm();
         
        /*
        var send_form = false;
        $('input[data-validate-required=required]').each(function(key, val){
          if(manager.validateData($(val)) == false)
        {
             send_form = false;
        } 
       });
        */
        /*
        if(manager.validateData() == true)
        {
             manager.sendForm();
        } 
        */
   });
   
   
   $('.remove-team-player-trigger').on('click',function(e){
        e.preventDefault();
        manager.remove_url = $(this).attr('href');
        manager.remove_row = $('#'+$(this).attr('data-row'));
       
        manager.remove_modal.modal('show');
       
   });
   
   $('#removePlayerModal .modal_submit').on('click',function(){
       manager.removePlayer();
   });
   
   $('.unactivate-team-player-trigger').on('click',function(e){
        e.preventDefault();
        manager.unactivate_url = $(this).attr('href');
        manager.unactivate_modal.modal('show');
       
   });
   
   $('#unactivatePlayerModal .modal_submit').on('click',function(){
       location.href = manager.unactivate_url;
   });
   
   $('.players_list_filter input').on('ifChecked',function(){
        var filter_class = $(this).val();
        $('.'+filter_class).each(function(key,val){
            $(val).show();
        });
   });
   $('.players_list_filter input').on('ifUnchecked',function(){
        var filter_class = $(this).val();
        $('.'+filter_class).each(function(key,val){
            $(val).hide();
        });
   });
   
   $('.player-row-edit, .player-row-edit-sm').on('click',function(){
       /*
       var target_id = $(this).attr('data-edit');
       var inline_edit = $('#inline-edit-'+target_id);
       var static_data = $('#static-data-'+target_id);
       $('#player_row_'+target_id).addClass('edited');
       $('#player_row_'+target_id).removeClass('static');
       inline_edit.show();
       static_data.hide();
       */
      var pid = $(this).attr('data-pid');
      var player_id = $(this).attr('data-edit');
      var target_row = $('#player_row_'+ player_id);
      var first_name =target_row.find('.first_name').text();
      var last_name =target_row.find('.last_name').text();
      var email =target_row.find('.email').text();
      var team_role =$('#team_role_'+player_id).val();
      var level =$('#player_level_'+player_id).val();
      var player_number =$('#player_number'+player_id).val();
      var status =$('#player_status_'+player_id).val();
      var status_text = $('#player_status_text_'+player_id).html();

      if(status == 'unactive' && email == '')
      {
          $('#edit_modal_form_status').show();
          $('#edit_modal_form_player_status_text').html(''); 
          $('#edit_modal_form_player_status_text').hide(); 
      }
      else if(status != 'confirmed')
      {
          $('#edit_modal_form_status').hide();
          $('#edit_modal_form_player_status_text').html(status_text); 
          $('#edit_modal_form_player_status_text').show(); 
      }
      else
      {
          $('#edit_modal_form_status').show();
          $('#edit_modal_form_player_status_text').html(''); 
          $('#edit_modal_form_player_status_text').hide(); 
      }
      
      $('#edit_modal_form_player_id').val(player_id);
      $('#edit_modal_form_first_name').val(first_name);
      $('#edit_modal_form_last_name').val(last_name);
      $('#edit_modal_form_email').val(email);
      $('#edit_modal_form_team_role').val(team_role);
      $('#edit_modal_form_player_number').val(player_number);
      $('#edit_modal_form_level').val(level);
      $('#edit_modal_form_status').val(status);
      
      //registred player
      
      if(pid > 0)
      {
          //$('#edit_modal_form_email').attr('disabled','disabled');
          $('#edit_modal_email_text').remove();
          $('#edit_modal_form_email').parent().append('<div id="edit_modal_email_text">'+email+'</div>');
          $('#edit_modal_form_email').hide();
      }
      else
      {
           $('#edit_modal_email_text').remove();
           $('#edit_modal_form_email').show();
           //$('#edit_modal_form_email').removeAttr('disabled');
      }
      
      manager.edit_modal.find('.control-label-error').remove();
      manager.edit_modal.modal('show');
       
      
   });
   $('.player-row-cancel-edit').on('click',function(){
       var target_id = $(this).attr('data-edit');
       var inline_edit = $('#inline-edit-'+target_id);
       var static_data = $('#static-data-'+target_id);
       $('#player_row_'+target_id).removeClass('edited');
       $('#player_row_'+target_id).addClass('static');
       inline_edit.hide();
       static_data.show();
       
       $('#player_first_name_'+target_id).val(static_data.find('.first_name').text());
       $('#player_last_name_'+target_id).val(static_data.find('.last_name').text());
       $('#player_email_'+target_id).val(static_data.find('.email').text());
       
   });
   
};


TournamentPlayersManager.prototype.fileuploadTrigger = function()
{
    var manager = this;

    
    $('#members_import_file').fileupload({
      dropZone: $('#dropzone'),
        dataType: 'json',
       done: function (e, data) {
           $.each(data.result.files, function (index, file) {
               $('<p/>').text(file.name).appendTo(document.body);
           });
       },
       progress: function (e, data) {
           $('.upload-progress').show();
           var progress = parseInt(data.loaded / data.total * 100, 10);
           $('.upload-progress .progress-num').text((progress-1) + '%');
           $('.upload-progress .progress-bar').css('width',progress + '%');
           
           $('.upload-error').remove();
       },
       done: function (e, data) {

           if(data.result.result == 'SUCCESS')
           {
               location.reload();
           }
           
           if(data.result.result == 'ERROR')
           {
               $('.upload-wrap').before('<div class="alert alert-error upload-error">'+data.result.error_message +'</div>');
           }
           

       }
   });
};


TournamentPlayersManager.prototype.validateData = function(elem)
{
  
  
    validator = new Validator(); 
    validator.setElements(elem);
    console.log(elem); console.log(validator);
    
    validator.checkRequired([elem.attr('id')]);
    
    
    
    $('.control-label-error').remove();

    if(validator.hasErrors())
    {
         errors = validator.getErrors();
         console.log(errors);
          for (key in errors) {
            errors[key].element.after('<label class="control-label-error" for="inputError">'+ errors[key].message+'</label>');
          }
          return false;
    }
    else
    {
        return true;
    }
};

TournamentPlayersManager.prototype.changeRole = function(elem){
    var select = $('#'+elem.attr('data-rel'));
    var player = {id:select.attr('data-id'),team_role:select.val()};
    
    var data = {players:[player]};
     $.ajax({
            method: "GET",
            url: this.change_role_url,
            data: data,
            dataType: 'json'
        }).done(function (response) {

           console.log(response);
        
        });
};

TournamentPlayersManager.prototype.removePlayer = function(){
    
     manager = this;
     $.ajax({
            method: "GET",
            url: this.remove_url
        }).done(function (response) {

           manager.remove_row.fadeOut(500);
           manager.remove_modal.modal('hide');
           manager.remove_url = '';
           
        });
};

TournamentPlayersManager.prototype.sendRating = function(elem){
     var rating = elem.attr('data-score');
     var player_id = elem.attr('data-player');
     var match_id = elem.attr('data-match');
     var data = {rating:rating,player_id:player_id,match_id:match_id};
     $.ajax({
            method: "GET",
            url: this.rating_url,
            data: data,
            dataType: 'json'
        }).done(function (response) {

           console.log(response);
        
        });
};

TournamentPlayersManager.prototype.bindModalEditForm = function()
{
    var manager = this;
    $('#edit-player-modal-form').on('submit', function (e) {
        e.preventDefault();
        var form = $(this);
        var data = form.serialize();
        var action = form.attr('action');
        $.ajax({
            method: "POST",
            url: action,
            data: data,
            dataType: 'json'
        }).done(function (response) {
            form.find('.control-label-error').remove();
            if (response.result == 'ERROR')
            {
                form.effect( "shake");
                $.each(response.errors, function (key, val) {
                    form.find('[data-error-bind="' + key + '"]').append('<span class="control-label-error">' + val + '</span>');
                });
            }
            else
            {
               //alert('ok');
               //var redirect_url = 
                location.reload();
                //window.location.href = location.href+"#playersList";
                /*
                var player_data = response.player_data;
                var player_id = player_data.team_player_id;
                var target_row = $('#player_row_'+player_id);
                
                target_row.find('.first_name').text(player_data.first_name);
                target_row.find('.last_name').text(player_data.last_name);
                target_row.find('.email').text(player_data.email);
                $('#team_role_'+player_id).val(player_data.team_role);
                $('#player_level_'+player_id).val(player_data.level);
                $('#player_number'+player_id).val(player_data.player_number);
                $('#player_status_'+player_id).val(player_data.status);
                manager.edit_modal.modal('hide');
                target_row.effect("highlight", {}, 2000);
                */
            }
        });
    });
    
    
    
   
};

