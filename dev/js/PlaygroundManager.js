
var PlaygroundManager = function (options)
{
     if (typeof options === 'undefined') 
     { 
         this.mapId = 'map-edit';
     }
     else
     {
         this.mapId = options.mapId ||  'map-edit';
     }

     this.map;
     this.google;
     this.markers = [];
     this.map_center = {lat: 48.978076, lng: 19.519994};
};


PlaygroundManager.prototype.initMap = function(latLng)
{
    $('#'+this.mapId).css('height','400px');
    
    this.map = new google.maps.Map(document.getElementById(this.mapId), {
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        
     if(null != latLng)
     {
         this.map.setCenter(latLng);
          this.placeMarker(this.map, latLng);
     }
     else
     {
          this.map.setCenter(this.map_center);
          this.map.setZoom(2);
     }

     
      this.addMapClickListener(this.map);
};


PlaygroundManager.prototype.setNewLocation = function(latLng)
{
     var manager = this;
     manager.map.setZoom(15);
     var geocoder = new google.maps.Geocoder;
            geocoder.geocode({'location': latLng}, function (results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                          $.each(results[0].address_components, function (i, address_component) {


                            if (address_component.types[0] == "route") {
                                $('#playgrounds_street').val(address_component.long_name);
                            }

                            if (address_component.types[0] == "locality") {
                                $('#playgrounds_city').val(address_component.long_name);
                            }

                            if (address_component.types[0] == "street_number") {
                                $('#playgrounds_street_number').val(address_component.long_name);
                            }

                        });

                        $('#playground_locality_json_data').val(JSON.stringify(results[0]));
                        //$('#pac-input').val( $('#playgrounds_street').val()+','+ $('#playgrounds_city').val()+' '+$('#playgrounds_street_number').val());

                    } else {
                        window.alert('No results found');
                    }
                } else {
                    window.alert('Geocoder failed due to: ' + status);
                }
            });
};



PlaygroundManager.prototype.placeMarker = function(map, location)
{
    var manager = this;
    this.markers.forEach(function (marker) {
        marker.setMap(null);
    });
    var marker = new this.google.maps.Marker({
        position: location,
        draggable:true,
        map: map
    });
    this.markers.push(marker);
    
    this.google.maps.event.addListener(marker, 'dragend', function (event) {
        manager.setNewLocation(event.latLng);
    });
};

PlaygroundManager.prototype.addMapClickListener = function (map)
{
    var manager = this;
    manager.google.maps.event.addListener(map, 'click', function (event) {
        var marker = manager.placeMarker(map, event.latLng);
        manager.setNewLocation(event.latLng);
    }); 
};


PlaygroundManager.prototype.createSearchInput = function(element_id)
{
    var manager = this;
    
    if(typeof element_id === 'undefined')
    {
        element_id = 'pac-input';
    }
    
    var searchBox = new this.google.maps.places.SearchBox(document.getElementById(element_id));
    searchBox.addListener('places_changed', function () {
        manager.places = searchBox.getPlaces();
        manager.map_center =  manager.places[0].geometry.location;
        manager.initMap(manager.map_center);
        manager.setNewLocation(manager.map_center);
        //manager.map.setCenter(manager.map_center);
        manager.placeMarker(manager.map,manager.map_center);
    });
};

PlaygroundManager.prototype.initModalCreate = function()
{
   var manager = this;
    
    $('.add_playground_modal_trigger').on('click',function(e){
        e.preventDefault();
         manager.createSearchInput('create-playground-search');
        $('#add-playground-modal').modal('show');
    });
    
    $('#add-playground-modal form').on('submit',function(e){
         
        e.preventDefault();
       var form = $(this);
       
         
        var action = $(this).attr('action');
        var data = $(this).serialize();
         $.ajax({
            method: "POST",
            url: action,
            data: data,
            dataType: 'json'
          }).done(function( response ) {
                console.log(response);
                form.find('.control-label-error').remove();
                if(response.result == 'ERROR')
                {
                     //$('#error-alerts').addClass('alert alert-danger');
                    
                    $.each(response.errors,function(key,val){
                        form.find('[data-error-bind="'+key+'"]').append('<span class="control-label-error">'+val+'</span>');
                    });
                }
                else
                {
                    
                    $('#add-playground-modal').modal('hide');
                    new_option = $('<option/>',{text:response.name,value:response.value});
                    $('#playground_choice').append(new_option);
                    $('#playground_choice').val(response.value);
                    
                    $("#playground_choice").select2('destroy');
                    $("#playground_choice").select2();
                    
                    //$('#playground_choice').effect("highlight", {}, 1000);
                    
                }
               
            });
    });
    
};



