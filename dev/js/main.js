//Global plugs
$(function () {
   
    var panelTimeout;
    var sidebarTimeout;
    var progressTimeout;
    
    $('.notifications-menu').on('click',function(){
         $.ajax({
                method: "GET",
                url: '/system-notify/change-status',
                data: {status:'read', type:'last_visit','mid':'a'},
                dataType: 'json'
            });
            
            $('.notifications-menu .label-warning').remove();
 });
 
    var messageOrigin;
    var messageSource;
    document.addEventListener('message', function (e) {
      messageOrigin = e.origin;
      messageSource = e.source;

    });
    
    
     $('.mpopup').on('click',function(e){
        e.preventDefault();
        var message = {
            'href':$(this).attr('href'),
            'action': $(this).attr('data-action'),
            'screen': $(this).attr('data-screen'),
            'onclose-action': $(this).attr('data-popup-onclose-action')
        };

         window.postMessage(JSON.stringify(message) ,messageOrigin);
    });
    
    $(document).on('click','.mpopup1',function(e){
        e.preventDefault();
        var message = {
            'href':$(this).attr('data-href'),
            'action': $(this).attr('data-action'),
            'screen': $(this).attr('data-screen'),
            'onclose-action': $(this).attr('data-popup-onclose-action')
        };
         window.postMessage(JSON.stringify(message) ,messageOrigin);
    });
    

    
   
    
    $('.mobile-app-nav .navBtn').on('click',function(){
        //var previewURL = document.referrer;
        var url = $(this).attr('data-url');
        var currentUrl = window.location.href;
        
        if('history' == url)
        {
            window.history.back();
            setTimeout(function(){
                // if location was not changed in 100 ms, then there is no history back
                if(currentUrl === window.location.href){

                    var message = {
                        'action': 'close'
                    };

                    window.postMessage(JSON.stringify(message) ,messageOrigin);
                }
            });
        }
        else
        {
            window.location.href = url;
        }

       
    });
    
    $('.pay-label').on('click',function(){
        $('.pay-label').removeClass('pay-label-active');
        $(this).addClass('pay-label-active');
        $('#payment-price').val($(this).attr('data-price'));
        $('#variant_id').val($(this).attr('data-variant'));
        
    });
    
    
    $('#panel_progress li').on('mouseenter',function(){
        clearTimeout(progressTimeout);
        
        $('#panel_progress .desc').hide();
         $('#panel_progress .desc-cursor').hide();
        
        $('#'+$(this).attr('data-desc')).show();
        $(this).find('.desc-cursor').show();
    });
    
    $('#panel_progress li').on('mouseleave',function(){
        target=  $('#'+$(this).attr('data-desc'));
        progressTimeout = setTimeout(function(){
             target.hide();
             $('#panel_progress .desc-cursor').hide();
        }, 100); 
    });
    
    
    $('#panel_progress .desc ').on('mouseenter',function(){
        clearTimeout(progressTimeout);
    });
    
    $('#panel_progress .desc ').on('mouseleave',function(){
       target=  $(this);
        progressTimeout = setTimeout(function(){
             target.hide();
             $('#panel_progress .desc-cursor').hide();
        }, 100); 
    });
    
    
    
    /*
    $('.pro-icon').on('click',function(e){
        e.preventDefault();
        $('#team-credit-modal').modal('show'); 
    });
    */

    $(document).on('mouseenter', '.row-sidebar-trigger', function () {
        clearTimeout(sidebarTimeout);
         $('.row-sidebar-trigger').removeClass('active');
         $('.row-sidebar-cnt').hide();
        
        $('#'+$(this).attr('data-target')).show();
        $(this).addClass('active');
    });
    
    $(document).on('mouseleave', '.row-sidebar-trigger', function () {
        target=  $('#'+$(this).attr('data-target'));
        sidebarTimeout = setTimeout(function(){
             target.hide();
              $('.row-sidebar-trigger').removeClass('active');
        }, 100); 

    });
    
    $(document).on('mouseenter', '.row-sidebar-cnt', function () {
         clearTimeout(sidebarTimeout);

    });

    $(document).on('mouseleave', '.row-sidebar-cnt', function () {
         target = $(this);
         sidebarTimeout = setTimeout(function(){
             target.hide();
              $('.row-sidebar-trigger').removeClass('active');
        }, 100); 

    });
    

    $(document).on('click','.fake-link',function(){
        location.href= $(this).attr('data-redirect');
    });
    
    $(document).on('mouseover click','.panel-settings-trigger',function(e){
        e.preventDefault();
        clearTimeout(panelTimeout);
        target =$('#'+$(this).attr('data-target'));
        $(this).addClass('active');
        target.show();
    });
    
    $(document).on('mouseout','.panel-settings-trigger',function(){
        var  target =$('#'+$(this).attr('data-target'));
        
        panelTimeout = setTimeout(function(){
           $('.panel-settings-trigger').removeClass('active');
            target.hide();
        }, 300);
    });
    
    $(document).on('mouseover','.panel-settings',function(){
        clearTimeout(panelTimeout);
        $(this).show();
    });
    $(document).on('mouseout','.panel-settings',function(){
        var target = $(this);
         panelTimeout = setTimeout(function(){
            $('.panel-settings-trigger').removeClass('active');
             target.hide();
        }, 300);
    });
    
   
   
    
    $(".select-dropdown li a").click(function(){
        var selText = $(this).text();
        $(this).parents('.btn-group').find('.dropdown-toggle').html(selText+' <span class="caret"></span>');
      });

    $('.skip_and_free_trigger').on('click',function(e){
        //e.preventDefault();
        //$('.skip_and_free_alert').fadeIn();
        
        
    });
    
    
     $('.event-date').on('click',function(){
        
         //console.log($(this).attr('data-href'));
         
         
         if(undefined !== $(this).attr('data-href'))
         {
             location.href= $(this).attr('data-href');
         }
         
         
    });

    
    $('.sidebar-toggle').on('click',function(){
        var new_sidebar_status = '';
        if($("body").hasClass('sidebar-collapse'))
        {
            new_sidebar_status = 'close';
        }
        else
        {
             new_sidebar_status = 'open';
        }

         $.ajax({
                method: "GET",
                url: '/base/layout/settings',
                data: {setting:'sidebar', v:new_sidebar_status},
                dataType: 'json'
            });
    });
    
    
      
    
    $('.tr-trigger').on('click',function(e){
        e.preventDefault();
         $('#translate-frame').attr('src',$(this).attr('data-href'));
         $('#translate-modal').modal('show');
        
    });


    $('input[type=checkbox], input[type=radio]').not('.no-icheck').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass: 'iradio_flat-green',
        increaseArea: '20%' // optional
    });

    $('.collapse-switcher a.in').on('click',function(e){
        e.preventDefault();
        var target_id = $(this).attr('href');
        $(target_id).collapse('show');
        
        $(this).addClass('btn-success');
        $(this).parent().find('a.out').removeClass('btn-danger');
        $(this).parent().find('a.out').addClass('btn-default');
    });
      
    $('.collapse-switcher a.out').on('click',function(e){
         e.preventDefault();
        var target_id = $(this).attr('href');
        $(target_id).collapse('hide');
        
        $(this).addClass('btn-danger');
        $(this).parent().find('a.in').removeClass('btn-success');
        $(this).parent().find('a.in').addClass('btn-default');
    });
      
            
     $.each($('.inline-edit'), function (index, value) {
        $(value).attr('readonly','readonly');
        $(value).on('click',function(){
            $(this).removeAttr('readonly');
            $(this).removeClass('inline-edit-inactive');
        });
        
        $(value).on('blur',function(){
            var url = $(this).attr('data-url');
            var val = $(this).val();
            var key = $(this).attr('name');
            $.ajax({
                method: "POST",
                url: url,
                data: {k:key, v:val},
                dataType: 'json'
            });
            
            $(this).addClass('inline-edit-inactive');
            $(this).attr('readonly','readonly');
        });
    });
            
            
     $.each($('a.print-link'), function (index, value) {
         
        $(value).on('click',function(e){
           
            e.preventDefault();
            $('#print_div').remove();
            print_div = $('<div/>',{'style':'position:absolute; top:-10000px; left:-5000px;width:100%; height:100%;','id':'print_div'});
            $('body').append(print_div);
            
            print_div.html('<iframe style="width:100%; height:100%;" src="'+$(this).attr('href')+'" onload="this.contentWindow.print();" ></iframe>');

        });
         
      
    });
    
     $.each($('a.popup-link'), function (index, value) {
         
        $(value).on('click',function(e){
            e.preventDefault();
            window.open($(this).attr('href'),'_blank');
        });
         
      
    });
    
     $(".numeric-only").on('keyup',function (e) {
    	if (String.fromCharCode(e.keyCode).match(/[^0-9]/g)) 
    	{
    		 $(this).val($(this).val().replace(/[^0-9]/g, ''));
    	}
 
    });
    
   
});

function isScrolledIntoView(elem)
{
    var docViewTop = $(window).scrollTop();
    var docViewBottom = docViewTop + $(window).height();

    var elemTop = $(elem).offset().top;
    var elemBottom = elemTop + $(elem).height();

    return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
}

$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};


Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

var Validator = function(){
    this.errors = {};
    this.elements;
};

Validator.prototype.setElements = function(el)
{
   this.elements = el;
};

Validator.prototype.checkRequired = function(names)
{
    validator = this;
    $.each(names,function(index,val){
       if(validator.elements[val].val() == '')
       {
           validator.errors[val] = { message: 'Required Field',element: validator.elements[val]};
       }
    });
};

Validator.prototype.getErrors = function()
{
    return this.errors;
};

Validator.prototype.getError = function(index)
{
    return this.errors[index];
};

Validator.prototype.hasErrors = function()
{
    var size = 0, key;
    for (key in this.errors) {
        if (this.errors.hasOwnProperty(key)) size++;
    }

    if(size > 0)
    {
        return true;
    }
    
    return false;
};

var PlayerLocalityManager = function ()
{
  
    this.google;
    this.searchInput = document.getElementById('pac-input');
    this.markers = [];
    this.map;
    this.map_center;
};

PlayerLocalityManager.prototype.init = function()
{
    var manager = this;
};

PlayerLocalityManager.prototype.addMapClickListener = function (map)
{
    var manager = this;
    manager.google.maps.event.addListener(map, 'click', function (event) {
        var marker = manager.placeMarker(map, event.latLng);
        manager.setNewLocation(event.latLng);
    });
};


PlayerLocalityManager.prototype.createSearchInput = function()
{
    var manager = this;
    var searchBox = new this.google.maps.places.SearchBox(this.searchInput);
    searchBox.addListener('places_changed', function () {
        manager.places = searchBox.getPlaces();
        manager.map_center =  manager.places[0].geometry.location;
        manager.setNewLocation(manager.map_center);
        manager.showSearchMap();
    });
};

PlayerLocalityManager.prototype.showSearchMap = function()
{
    var manager = this;
    manager.map = new this.google.maps.Map(document.getElementById('map'), {
            center: manager.map_center,
            zoom: 16,
            mapTypeId: manager.google.maps.MapTypeId.ROADMAP
        });
        
        $('#map').css('position','relative');
        $('#map').css('left','0');
        
        this.placeMarker(manager.map,manager.map_center);
        this.addMapClickListener(manager.map);
};

PlayerLocalityManager.prototype.initMap = function(latLng)
{
     this.map = new google.maps.Map(document.getElementById('map'), {
            center:latLng,
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        
        this.placeMarker(this.map, latLng);
        this.addMapClickListener(this.map);
};

PlayerLocalityManager.prototype.setNewLocation = function(latLng)
{
    $('#locality_id').val('');
     var manager = this;
     var geocoder = new google.maps.Geocoder;
            geocoder.geocode({'location': latLng}, function (results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        $.each(results[0].address_components, function (i, address_component) {


                            if (address_component.types[0] == "route") {
                                $('#locality_street').val(address_component.long_name);
                            }

                            if (address_component.types[0] == "locality") {
                                $('#locality_city').val(address_component.long_name);
                            }

                            if (address_component.types[0] == "street_number") {
                                $('#locality_street_number').val(address_component.long_name);
                            }

                        });

                        $('#locality_json_data').val(JSON.stringify(results[0]));
                        $('#locality_name').val(results[0].formatted_address);
                        
                       

                    } else {
                        window.alert('No results found');
                    }
                } else {
                    window.alert('Geocoder failed due to: ' + status);
                }
            });
};


PlayerLocalityManager.prototype.placeMarker = function(map, location)
{
    this.markers.forEach(function (marker) {
        marker.setMap(null);
    });
    var marker = new this.google.maps.Marker({
        position: location,
        map: map
    });
    this.markers.push(marker);
};



var LocalityManager = function (element)
{
   this.playground_dialog= $("#playground_dialog").dialog({
        autoOpen: false,
        height: 800,
        width: 600
    });
    this.google;
    this.searchInput = document.getElementById('pac-input');
    this.places;
    this.markers = [];
    this.map;
    this.map_center;
    this.map_zoom = 8;
    
    
};

LocalityManager.prototype.init = function(callback)
{
    var manager = this;
    this.map_center = {lat: 48.985286, lng: 18.9794753};
    this.getUserGeolocationInfo(callback);


    $('.add_new_playground').on('click', function (e) {
            e.preventDefault();
            $('#add_new_playground_alert').html('click playground place on map and fill data');
            manager.openNewPlaygroundDialog();
        });
        
     $('.map_trigger').on('click',function(e){
         e.preventDefault();
         
         if($('#map_wrap').hasClass('map-active'))
         {
             $('#map_wrap').removeClass('map-active');
             $('#map_wrap').addClass('map-inactive');
         }
         else
         {
              $('#map_wrap').removeClass('map-inactive');
             $('#map_wrap').addClass('map-active');
         }
         
         manager.showSearchMap();
     });
     
     $('#custom_playground_name').on('keyup',function(){
         $('#playground_name').val($(this).val());
     });
};


LocalityManager.prototype.getUserGeolocationInfo = function(callback)
{
    var manager = this;
    navigator.geolocation.getCurrentPosition(function(position) {
      pos = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };
       manager.map_center = pos;
       
        if (typeof callback === "function") {
            callback();
        }
       

       //console.log(manager);
    }, function() {
     //console.log(manager);
    });

};



LocalityManager.prototype.createSearchInput = function(element_id)
{

    var search_input = this.searchInput;
    if(element_id != null)
    {
        search_input = document.getElementById(element_id);
    }
    
    var manager = this;
    var searchBox = new this.google.maps.places.SearchBox(search_input);
    searchBox.addListener('places_changed', function () {
        manager.places = searchBox.getPlaces();
        manager.map_center =  manager.places[0].geometry.location;
        manager.setTeamLocationByGeocoder(manager.map_center);
        manager.map_zoom = 15;
        
         manager.map = new manager.google.maps.Map(document.getElementById('map'), {
            center: manager.map_center,
            zoom: manager.map_zoom,
            mapTypeId: manager.google.maps.MapTypeId.ROADMAP
        });
        manager.placeMarker(manager.map,manager.map_center);
    });
    

};



LocalityManager.prototype.showSearchMap = function()
{
    var manager = this;
    manager.map = new this.google.maps.Map(document.getElementById('map'), {
            center: manager.map_center,
            zoom: manager.map_zoom,
            mapTypeId: manager.google.maps.MapTypeId.ROADMAP
        });
        
        $('#map').css('position','relative');
        $('#map').css('left','0');
        
        this.placeMarker(manager.map,manager.map_center);
        this.addMapClickListener(manager.map);
};


LocalityManager.prototype.openNewPlaygroundDialog = function()
{
     this.playground_dialog.dialog('open');
     var map_center =  this.places[0].geometry.location;
     var google = this.google;
     
     var map = new google.maps.Map(document.getElementById('add_form_map'), {
                center: map_center,
                zoom: 14,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });
            
    this.addMapClickListener(map);
};

LocalityManager.prototype.addMapClickListener = function (map)
{
    
    var manager = this;
    manager.google.maps.event.addListener(map, 'click', function (event) {
        var marker = manager.placeMarker(map, event.latLng);
        var infowindow = new manager.google.maps.InfoWindow;
        manager.setTeamLocationByGeocoder(event.latLng);
    });
};


LocalityManager.prototype.setTeamLocationByGeocoder = function(latLng)
{
    var manager = this;
    var geocoder = new manager.google.maps.Geocoder;
    geocoder.geocode({'location': latLng}, function (results, status) {
            if (status === manager.google.maps.GeocoderStatus.OK) {
                
                if (results[0]) {
                    $.each(results[0].address_components, function (i, address_component) {

                        if (address_component.types[0] == "route") {
                            $('#team_address_street').val(address_component.long_name);
                        }

                        if (address_component.types[0] == "locality") {
                            $('#team_address_city').val(address_component.long_name);
                        }

                        if (address_component.types[0] == "street_number") {
                            $('#playgrounds_street_number').val(address_component.long_name);
                        }
                        
                        
                    });

                    $('#playground_locality').val(JSON.stringify(results[0]));
                    $('.playground_locality').val(JSON.stringify(results[0]));
                    $('#playground_place_id').val(results[0].place_id);
                    
                    var custom_name  = $('#custom_playground_name').val();
                    if('' == custom_name)
                    {
                         $('#playground_name').val(results[0].formatted_address);
                    }
                    else
                    {
                         $('#playground_name').val(custom_name);
                    }
                    
                   
                    //$('#team_full_address').val($('#team_address_street').val()+' '+$('#playgrounds_street_number').val()+', '+$('#team_address_city').val());

                } else {
                    window.alert('No results found');
                }
            } else {
                window.alert('Geocoder failed due to: ' + status);
            }
        });
};


LocalityManager.prototype.placeMarker = function(map, location)
{

    this.markers.forEach(function (marker) {
        marker.setMap(null);
    });

    var marker = new this.google.maps.Marker({
        position: location,
        draggable:true,
        map: map
    });
    
    this.markers.push(marker);
    
    var manager = this;
     manager.google.maps.event.addListener(marker, 'dragend', function (event) {
        //manager.setNewLocation(event.latLng);
         manager.setTeamLocationByGeocoder(event.latLng);
    });

};





function TeamLocality() {
    var map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: -33.8688, lng: 151.2195},
        zoom: 13,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });


    // Create the search box and link it to the UI element.
    //var input = document.getElementById('pac-input');
    //var searchBox = new google.maps.places.SearchBox(input);
    //map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    // Bias the SearchBox results towards current map's viewport.
    map.addListener('bounds_changed', function () {
        searchBox.setBounds(map.getBounds());
    });

    $('.playground_item_remove').on('click', function (e) {
        e.preventDefault();
        $(this).parent('.playground_item').remove();
    });


    var markers = [];
    // [START region_getplaces]
    // Listen for the event fired when the user selects a prediction and retrieve
    // more details for that place.
    searchBox.addListener('places_changed', function () {
        var places = searchBox.getPlaces();


        if (places.length == 0) {
            return;
        }

        // Clear out the old markers.
        markers.forEach(function (marker) {
            marker.setMap(null);
        });
        markers = [];

        // For each place, get the icon, name and location.
        var bounds = new google.maps.LatLngBounds();

       


        if ($('#pac-input').hasClass('add_place'))
        {
            showAddForm(places[0]);
        }
        else
        {
            $.ajax({
                method: "GET",
                url: "/ajax/locality/area",
                data: {lat: places[0].geometry.location.lat(), lng: places[0].geometry.location.lng(), distance: 20},
                dataType: 'json'
            }).done(function (msg) {

                $('#search_term').html(places[0].formatted_address);
                if (msg.length > 0)
                {
                    showLocalityPlaygrounds(msg);
                }
                else
                {
                    $('#add_new_playground_alert').html('no playgrounds found, click playground place on map and fill name');
                    showAddForm(places[0]);
                }
            });
        }






        /*
         $('.playground_dialog_trigger').on('click',function(e){
         e.preventDefault();
         playground_dialog.dialog('open');
         });*/

       

        $('.playground_dialog_save_trigger').on('click', function (e) {
            e.preventDefault();
            playground_name = $('#playground_name').val();
            playground_city = $('#playground_city').val();
            playground_street = $('#playground_street').val();
            playground_street_number = $('#playground_street_number').val();
            playground_description = $('#playground_street_description').val();
            playground_id = $('#add_playground_place_id').val();
            playground_location = $('#add_playground_locality').val();
            playground = {id: playground_id, name: playground_name, city: playground_city, street: playground_street, street_number: playground_street_number, description: playground_description, location: playground_location};
            addPlayground(playground);
            playground_dialog.dialog('close');
        });

        /*
         $('#playground_add_form button').on('click', function (e) {
         e.preventDefault();
         playground_name = $('#add_playground_name').val();
         playground_id = $('#add_playground_id').val();
         playground_location = $('#add_playground_id').val();
         playground = {id: playground_id, name: playground_name, location: playground_location};
         addPlayground(playground);
         });
         */

        function createPlayground(playground)
        {
            playground_element = $('<div/>', {class: 'playground_item'});
            playground_element.append(playground.name);
            playground_element.append('<input type="hidden" name="playgrounds[' + playground.id + '][place_id]" value="' + playground.id + '" />');
            playground_element.append('<input type="hidden" name="playgrounds[' + playground.id + '][name]" value="' + playground.name + '" />');
            playground_element.append('<input type="hidden" name="playgrounds[' + playground.id + '][city]" value="' + playground.city + '" />');
            playground_element.append('<input type="hidden" name="playgrounds[' + playground.id + '][street]" value="' + playground.street + '" />');
            playground_element.append('<input type="hidden" name="playgrounds[' + playground.id + '][street_number]" value="' + playground.street_number + '" />');


            if (playground.hasOwnProperty('description'))
            {
                playground_element.append('<input type="hidden" name="playgrounds[' + playground.id + '][description]" value="' + playground.description + '" />');
            }

            if (playground.hasOwnProperty('location'))
            {
                playground_element.append('<input type="hidden" name="playgrounds[' + playground.id + '][locality_json_data]" value=\'' + playground.location + '\' />');
            }


            if (playground.hasOwnProperty('locality_id'))
            {
                playground_element.append('<input type="hidden" name="playgrounds[' + playground.id + '][locality_id]" value=\'' + playground.locality_id + '\' />');
            }


            return playground_element;
        }

        function addPlayground(playground)
        {
            playground_element = createPlayground(playground);

            playground_element_remove = $('<a/>', {class: 'playground_item_remove', href: '', text: ' Remove'});

            //check if location exist
            if ("" == playground.id)
            {
                $('#add_new_playground_alert').html('First select place on map');
            }
            else
            {
                playground_element.append(playground_element_remove);
                playgrounds_block = $('#team_playgrounds_container');
                playgrounds_block.prepend(playground_element);
                $('#playgrounds').show();
                $('#playground_add_form').hide();
            }

        }

        function addExistPlayground(playground)
        {
            playground_element = createPlayground(playground);


            playground_element_add = $('<a/>', {class: 'playground_item_add', href: '', text: ' Add'});
            playground_element_add.on('click', function (e) {
                e.preventDefault();
                addPlayground(playground);
            });

            playground_element.append(playground_element_add);

            playgrounds_block = $('#exist_playgrounds_container');
            playgrounds_block.append(playground_element);
            $('#exist_playgrounds').show();
        }

        function addPlaygroundToMap(playground)
        {
            latlng = {lat: parseFloat(playground.lat), lng: parseFloat(playground.lng)};
            var marker = new google.maps.Marker({
                position: latlng,
                map: map
            });

            markers.push(marker);
        }

        function showLocalityPlaygrounds(playgrounds) {
            playgrounds_block = $('<div/>', {id: 'playgrounds'});
            //$('#map').css('left','0');

            //clear markers
            markers.forEach(function (marker) {
                marker.setMap(null);
            });


            for (i in playgrounds)
            {
                playground = playgrounds[i];
                addExistPlayground(playground);
                addPlaygroundToMap(playground);
            }


        }


        function showAddForm(place)
        {
            //$('#playground_add_form').show();
            //$('#map').css('left','0');
            playground_dialog.dialog('open');



            var map = new google.maps.Map(document.getElementById('add_form_map'), {
                center: place.geometry.location,
                zoom: 14,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });



            addNewPlaygroundClickListener(map);
            //add click event
        }





        places.forEach(function (place) {

            var icon = {
                url: place.icon,
                size: new google.maps.Size(71, 71),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(17, 34),
                scaledSize: new google.maps.Size(25, 25)
            };

            // Create a marker for each place.
            markers.push(new google.maps.Marker({
                map: map,
                icon: icon,
                title: place.name,
                position: place.geometry.location
            }));

            if (place.geometry.viewport) {
                // Only geocodes have viewport.
                bounds.union(place.geometry.viewport);
            } else {
                bounds.extend(place.geometry.location);
            }

        });







        function placeMarker(map, location)
        {

            markers.forEach(function (marker) {
                marker.setMap(null);
            });

            var marker = new google.maps.Marker({
                position: location,
                map: map
            });

      
            //markers.push(marker);

            //return marker;
        }




        map.fitBounds(bounds);
    });
    // [END region_getplaces]
}






function ManagePlaygroundEvent() {

    var cell;
    var dialog = $("#calendar_dialog").dialog({
        autoOpen: false,
        height: 800,
        width: 600
    });



    $('#playground_event_send_trigger').on('click', function (e) {
        e.preventDefault();
        var data = $('#add_playground_event_form').serialize();




        $.ajax({
            method: "GET",
            url: "/playground/calendar/add-event",
            data: data,
            dataType: 'json'
        }).done(function (response) {


            cell.html('');

            cellHtml = '<strong>' + response.name + '</strong><br />';



            response.teams.forEach(function (teamName) {
                //cell.append(teamName);
                cellHtml += teamName;
            });

            if ('' != response.note)
            {
                cellHtml += ' <span class="label label-default">' + response.note + '</span>';
            }



            cell.append(cellHtml);
            dialog.dialog('close');
        });
    });



    $('.playground_calendar_add_event').on('click', function (e) {
        e.preventDefault();

        cell = $(this).parent();
        $('#record_termin').val($(this).attr('data-termin'));
        $('#record_playground_id').val($(this).attr('data-playground'));

        dialog.dialog('open');
    });
}

ManagePlaygroundEvent();



var UserSearchAutocomplete = function (element)
{
    this.element = element;
    this.phrase = '';
    this.onclick = 'fillInput';
    this.autocomplete_list = element.closest('.autocomplete_container').find('.autocomplete_list');
    this.selected_players = $('#selected_players');
    this.row_id = element.attr('data-row');
    //this.sendRequest = function(){};
    
};


UserSearchAutocomplete.prototype.init = function () {
    var autocomplete = this;
    this.element.on('keyup', function () {
        if ($(this).val().length > 3)
        {
            autocomplete.phrase = $(this).val();
            autocomplete.sendRequest();
        }
    });
};
UserSearchAutocomplete.prototype.initEmailSearch = function () {
    var autocomplete = this;
    this.element.on('keyup', function () {
        at_patt = new RegExp("@");
        at_res = at_patt.test($(this).val());

        if ($(this).val().length > 3 && at_res)
        {
            autocomplete.phrase = $(this).val();
            autocomplete.sendRequest();
        }
    });
};


UserSearchAutocomplete.prototype.sendRequest = function () {

    data = {phrase: this.phrase};
    var autocomplete = this;
    $.ajax({
        method: "GET",
        url: "/player/search",
        data: data,
        dataType: 'json'
    }).done(function (response) {
        autocomplete.showResult(response);
    });

};

UserSearchAutocomplete.prototype.showResult = function (result) {
    var autocomplete = this;
    this.autocomplete_list.html('');


    if (result.length > 0)
    {
        result.forEach(function (user)
        {
            autocomplete.addItem(user);
        });
        this.autocomplete_list.show();
    }



};

UserSearchAutocomplete.prototype.addItem = function (user) {

    //console.log(user);
    var item = $('<li/>');
    var autocomplete = this;
    item.html('<strong>' + user.name + ' ' + user.surname + '</strong>' + user.email);

    if (autocomplete.onclick == 'fillInput')
    {
        item.on('click', function () {
            autocomplete.fillInput(user);
            autocomplete.autocomplete_list.html('');
            autocomplete.autocomplete_list.fadeOut();
        });
    }
    else
    {

    }

   
    this.autocomplete_list.append(item);
};

UserSearchAutocomplete.prototype.fillInput = function (user) {

    $('#player_email'+this.row_id).val(user.email);
    $('#player_first_name'+this.row_id).val(user.name);
    $('#player_last_name'+this.row_id).val(user.surname);
    $('#player_id'+this.row_id).val(user.id);


}

var UserProfilManager = function ()
{

};

UserProfilManager.prototype.init = function () {

    var manager = this;
    $('#add_player_sport').on('click', function (e) {
        e.preventDefault();
        manager.addSport($('#sport_selector option:selected'));
    });


    $.each($('#player_sports div.row'), function (index, value) {

        sport_item_remove = $(this).find('.player_sport_remove');

        manager.addRemoveSportEvent(sport_item_remove, this);
    });

    /*
     $('').on('click',function(e){
     e.preventDefault();
     manager.addSport( $('#sport_selector option:selected'));
     });
     */
    //$(document)
};

UserProfilManager.prototype.addSport = function (element) {
    text = element.text();
    sport_id = element.val();
    

 
    //check exist
    if ($('#sport_item_' + sport_id).length == 0)
    {
        sports_container = $('#player_sports');
        sport_item = $('<div/>', {class:'row sport-row'});
        sport_item.attr('id', 'sport_item_' + sport_id);
        
        row1 = $('<span/>', {text: text,class:'sport-name'});
        row2 = $('<div/>', {class:'col-xs-10'});
        row3 = $('<div/>', {class:'col-xs-2'});
        
        sport_item_level = $('#sport_level_template').clone();
        //sport_item_level.select2('destroy');
        sport_item_level.attr('id', '');
        sport_item_level.attr('style', '');
        sport_item_level.attr('class', 'form-control');
        sport_item_level.attr('name', 'sports[' + sport_id + '][level]');
        sport_item_level.val($('#sport_level_template').val());
        row2.append(row1);
        row2.append(sport_item_level);

        
        sport_item_remove = $('<a/>', {class: 'player_sport_remove', href: '', html: '<i class="ico ico-trash"></i>'});
        sport_item_input = $('<input/>', {type: 'hidden', name: 'sports[' + sport_id + '][sport]', value: sport_id});
        row3.append(sport_item_remove);
        row3.append(sport_item_input);

        this.addRemoveSportEvent(sport_item_remove, sport_item);

        //sport_item.append(row1);
        sport_item.append(row2);
        sport_item.append(row3);
        sports_container.append(sport_item);
    }



};


UserProfilManager.prototype.addRemoveSportEvent = function (trigger, target) {

    trigger.on('click', function (e) {
        e.preventDefault();
        target.remove();
    });

};


SystemAlert = function ()
{

};



SystemAlert.prototype.showErrorAlert = function () {
    $('#base-alert-error-modal').modal('show');
};






