
var ScheduleManager = function (data)
{

};

ScheduleManager.prototype.createGroup = function (container,groupData)
{
    var index = Math.random().toString(36).substring(7);
    var group_cnt = $('<div/>', {'class': 'col-sm-3 group_cnt well-sm well'});
    var group_inp_name = $('<input/>', {'type': 'text', 'name': 'groups[' + index + '][name]'});
    //var group_teams_count = $('<input/>',{'type' :'number','class':'group_team_number'});
   // var group_teams_label = 'Počet tímov';
    var group_teams_cnt = $('<div/>',{'class' : 'group_teams_cnt'});
    
    
    var available_teams = $('#teams-list').clone();
    available_teams.attr('id','');
    
    
    available_teams.find('input').each(function(key,team_input){
        $(team_input).attr('name','groups['+index+'][team_names]['+key+']');
         //$(team_input).addClass('groups'+index);
        
        $(team_input).on('change',function(){
            if($(this).is(':checked'))
            {
                console.log('.'+$(this).attr('class'));
                $('.'+$(this).parent().attr('class')).find('input').attr('disabled','disabled');
                $('.'+$(this).parent().attr('class')).hide();
                $(this).parent().show();
                $(this).removeAttr('disabled');
                //$('.'+$(this).attr('class')).not('.groups'+index).hide();
            }
            else
            {
                $('.'+$(this).parent().attr('class')).find('input').removeAttr('disabled');
                $('.'+$(this).parent().attr('class')).show();
               
            }
            
        });
        
    });
    
    
    group_inp_name.val(groupData.group_name);
    group_teams_cnt.html(available_teams);
    
    
    
    
    
    /*
    group_teams_count.on('input',function(){
        group_teams_cnt.html('');
        var count = $(this).val();
        for(i=1;i<=count;i++)
        {
            var team_name = $('<input/>',{'name': 'groups['+index+'][team_names]['+i+']','class':'team_name'});
              group_teams_cnt.append(team_name);
        }
    });
    */
    group_cnt.append(group_inp_name);
    //group_cnt.append(group_teams_label);
    //group_cnt.append(group_teams_count);
    group_cnt.append(group_teams_cnt);
    container.append(group_cnt);
};

ScheduleManager.prototype.handleCreateGroup = function (container)
{
    var manager = this;
    $('#schedule_groups_count').on('input', function () {
        
        var groups_count = $('.group_cnt').size();
        var input_count = $(this).val();
        
        var diff = input_count-groups_count;
        
        if(diff > 0)
        {
            for(i=0;i<diff;i++)
            {
                manager.createGroup($('.groups_cnt'),{group_name:'Skupina '+(groups_count+1)});
            }
        }
        
        if(diff < 0)
        {
            var diff_abs = Math.abs(diff);
            for(i=0;i<diff_abs;i++)
            {
                 $('.group_cnt').last().remove();
            }
        }
    });
};

ScheduleManager.prototype.handleGroupTeamsCount = function ()
{
    $('.group_team_number').on('input',function(){
        

        var group_teams_cnt = $('.'+$(this).attr('data-groups-cnt'));
        var index = $(this).attr('data-index');
        
        var teams_count = group_teams_cnt.find('.team_name').size();
        var input_count = $(this).val();
        
        var diff = input_count-teams_count;
        
        if(diff > 0)
        {
            for(i=0;i<diff;i++)
            {
                var team_name = $('<input/>',{'name': 'groups['+index+'][team_names]['+input_count+']','class':'team_name'});
                group_teams_cnt.append(team_name);
            }
        }
        
        if(diff < 0)
        {
            var diff_abs = Math.abs(diff);
            for(i=0;i<diff_abs;i++)
            {
                 group_teams_cnt.find('.team_name').last().remove();
            }
        }
    });
};

ScheduleManager.prototype.handleGenerateTeamNames = function ()
{
    $('.team_names_generator').on('change',function(){
        var type = $(this).val();
        var group_teams_cnt = $('.'+$(this).attr('data-groups-cnt'));


       group_teams_cnt.find('.team_name').each(function(index){
           
           if('team' == type)
           {
               $(this).val('Tím '+(index+1));
           }
           
           if('letter' == type)
           {
               var letter = String.fromCharCode('A'.charCodeAt() + index);
                $(this).val(letter);
           }
           
           if('number' == type)
           {
               $(this).val(index+1);
           }
        
           
       });
        
        
    });
};

ScheduleManager.prototype.handleSingleEliminationTeamChoice = function()
{
    $('.single-elimination-team-choice').on('change',function(){
        
        var  current_value = $(this).val();
      
        if('' == current_value)
        {
            $.each($(this).find('option'), function(index,val){
               console.log($(val).css('display'));
               
               if($(val).css('display')  == 'block')
               {
                    $('.single-elimination-team-choice option[value="'+$(val).val()+'"]').show();
               }
            });
            
            
        }
        else
        {
             $('.single-elimination-team-choice option[value="'+current_value+'"]').hide();
             $(this).find('option[value="'+current_value+'"]').show();
             
             
        }
        
       
    });
};






ScheduleManager.prototype.bindTriggers = function () {

    var manager = this;
    manager.handleCreateGroup();
    manager.handleGroupTeamsCount();
    manager.handleGenerateTeamNames();
    manager.handleSingleEliminationTeamChoice();
    
};



      