var TournamentEventManager = function ()
{
    this.confirm_match_reset_modal = $('#confirm_match_reset_modal');
};

TournamentEventManager.prototype.bindTriggers = function () {
    var manager = this;

    $(document).on('click', '.attendance_trigger', function (e) {
        e.preventDefault();
        manager.changePlayerAttendance($(this));
    });
    
    
     $(document).on('click','.reset-match-trigger',function(e){
       e.preventDefault();
       manager.confirm_match_reset_modal.find('.modal_submit').attr('href', $(this).attr('href'));
       manager.confirm_match_reset_modal.modal('show'); 
   });
};


TournamentEventManager.prototype.changePlayerAttendance = function (element) {
    type = element.attr('data-rel');
    url = element.attr('data-url');
    event_id = element.attr('data-event');
    tournament_id = element.attr('data-tournament');
    team_id = element.attr('data-team-id');
    team_player_id = element.attr('data-team-player');
    team_position = element.attr('data-team-position');
    btn_in = element.parent().find('.in');
    btn_out = element.parent().find('.out');

    parent_row = element.parents('.attendant-row');


    btn_out.removeClass('deny');
    btn_in.removeClass('accept');
    var btn_html = element.html();

    element.html('<i class="fa fa-refresh fa-spin fa-fw"></i>');

    $.ajax({
        method: "GET",
        url: url,
        data: {tournament_id:tournament_id,team_id:team_id,team_position:team_position,event_id: event_id, type: type, team_player_id: team_player_id}
    }).done(function (msg) {

        element.html(btn_html);
        if ('in' == type)
        {
            btn_in.addClass('accept');
            parent_row.find('.in').addClass('accept');

            //$('#attendance-group-in-players').append(parent_row);

            var current_count = $('#attendance-group-in-players .attendant-row').size();

            $('#attendance-group-in-players').effect("highlight", {}, 2000);

            if ($('#attendance-group-in-players').length)
            {
                $('html, body').animate({scrollTop: $('#attendance-group-in-players').offset().top}, 500);
            }


        }
        if ('out' == type)
        {
            btn_out.addClass('deny');
            parent_row.find('.out').addClass('deny');
            //target_group = $('#attendance-group-out');
            //$('#attendance-group-in-players').detach().append(parent_row);
            //parent_row.detach().prepend('#attendance-group-out');

            $('#attendance-group-out h2').after(parent_row);


            var current_count = $('#attendance-group-in-players .attendant-row').size();
            var event_limit = $('#attendance-group-in-players').attr('data-event-limit');

/*
            if (current_count < event_limit)
            {
                $('.attendance-limit-divider').remove();
            }

            $('#attendance-group-out').effect("highlight", {}, 2000);
*/
            if ($('#attendance-group-out').length)
            {
                $('html, body').animate({scrollTop: $('#attendance-group-out').offset().top}, 500);
            }


        }
        if ('na' == type)
        {
            btn_na.addClass('btn-warning');
        }
        /*
        attendance_info = msg.attendanceInfo;
        $('.members-attendance-' + attendance_info.uid + ' .members-attendance-in').text(attendance_info.in);
        $('#members-attendance-' + attendance_info.uid + ' .members-attendance-in').text(attendance_info.in);
        $('.members-attendance-' + attendance_info.uid + ' .progress-bar').css('width', attendance_info.progress + '%');
        $('.members-attendance-' + attendance_info.uid + ' .members-attendance-text').css('left', attendance_info.progress + '%');

        if (attendance_info.in == 0)
        {
            //$('.members-attendance-'+attendance_info.uid+' .members-attendance-text').css('margin-left','0');
        } else
        {
            //$('.members-attendance-'+attendance_info.uid+' .members-attendance-text').css('margin-left','-50px');
        }


        if (attendance_info.in > 1)
        {
            $('.btn_continue').fadeIn();
            $('#event_attendance_confirm.ready').modal('show');
            rooster_link = '/team-match/new-lineup?event_id=' + event_id + '&event_date=' + event_date;
            $('#event_attendance_confirm.ready').find('.s2s_attendance_confirm_link').attr('href', rooster_link);

            $('#event_attendance_confirm').removeClass('ready');
        }


        var attendance_accept_players_count = $('#attendance-group-in-players .attendant-row').length;
        var attendance_accept_guest_count = $('#attendance-group-in-guest .attendant-row').length;
        var attendance_deny_count = $('#attendance-group-out .attendant-row').length;


        $('.attendance_accept_players_count').text(attendance_accept_players_count);
        $('.attendance_accept_guest_count').text(attendance_accept_guest_count);
        $('.attendance_deny_count').text(attendance_deny_count);
        //$('#attendance-group-in-players .attendant-row').length;

        fee_input_el = element.siblings(".attendance_fee").eq(0);
        fee_input_el.prop("disabled", false);


        if (msg.result != 'create_success')
        {
            system_alert = new SystemAlert();
            system_alert.showErrorAlert();
        }
        */





        //detach_row = parent_row.detach();





    }).fail(function (xhr, ajaxOptions, thrownError) {
        element.html(btn_html);
        system_alert = new SystemAlert();
        system_alert.showErrorAlert();
    });
};
