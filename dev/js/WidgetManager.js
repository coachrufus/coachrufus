var WidgetManager = function ()
{
     
};

WidgetManager.prototype.showLoader = function (widgetContainer)
{
    widgetContainer.html(' <div class="alert alert-info text-center"><i class="fa fa-refresh fa-spin fa-3x fa-fw"></i></div>Loading');
};

WidgetManager.prototype.hideLoader = function (widgetContainer)
{
    widgetContainer.html('');
};

WidgetManager.prototype.loadWidget = function (options) {
   
    var widgetContainer = options.widgetContainer;
    this.showLoader(widgetContainer);
    action = options.dataUrl;
    
    $.ajax({
        method: "GET",
        url: action
    }).done(function (response) {
        widgetContainer.html(response);
    });
};