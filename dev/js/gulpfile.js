var
	gulp = require('gulp'),
	uglify = require('gulp-uglify');
 
gulp.task('default', function() {
  return gulp.src('./Modules/bundle/index.js')
    .pipe(uglify())
    .pipe(gulp.dest('../../js/Modules/bundle'));
});