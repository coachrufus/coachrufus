var SmallCalendar = function (data)
{
    this.calendarTemplate;
    this.dataUrl = data.dataUrl;
};

SmallCalendar.prototype.bindTriggers = function()
{
    this.bindShowDayInfo();
    this.bindMoveControls();
    this.bindSwitchView();
};

SmallCalendar.prototype.bindSwitchView = function()
{
     $('.upcoming-events-trigger').on('click',function(e){
         e.preventDefault();
         $('#upcoming-events').css('max-height','500px');
         $('#small-calendar-wrap').slideUp(200,function(){
             $('#upcoming-events').slideDown();
         });

         $(this).hide();
         $('.calendar-events-trigger').show();
         
     });
     
     $('.calendar-events-trigger').on('click',function(e){
         e.preventDefault();
         
         $('#upcoming-events').slideUp(200,function(){
             $('#small-calendar-wrap').slideDown();
         });

         $(this).hide();
         $('.upcoming-events-trigger').show();

     }); 
};

SmallCalendar.prototype.showLoader = function()
{
     $('#small-calendar-cnt').html(' <div class="alert alert-info"><i class="fa fa-refresh fa-spin fa-3x fa-fw"></i></div>Loading');
};

SmallCalendar.prototype.hideLoader = function()
{
     $('#small-calendar-cnt').html('');
};


SmallCalendar.prototype.initLoad = function()
{
        this.showLoader();
    
        action = this.dataUrl;
        calendar = this;
        $.ajax({
            method: "GET",
            url: action,
            data: {'rt':'html'}
          }).done(function( response ) {
              $('#small-calendar-cnt').html(response);
              calendar.bindTriggers();
         });
};


SmallCalendar.prototype.bindShowDayInfo = function()
{
    $(document).on('click','.day_events_show_trigger',function(){
        $('.day_events_show_trigger').removeClass('active');
        $(this).addClass('active');
        var events_info = $(this).find('.day_events');
        $('#small-calendar-info-panel').html(events_info.html());
        $('#small-calendar-info-panel').fadeIn();
        
        $('.panel-calendar').css('height','auto');
        $('.panel-calendar').css('overflow','none');
    });
};


SmallCalendar.prototype.bindMoveControls = function()
{
    var calendar = this;
     $(document).on('click','.small-calendar-move-month-trigger',function(e){
        e.preventDefault();
        calendar.showLoader();
        action = $(this).attr('href');        
         $.ajax({
            method: "GET",
            url: action,
            data: {'rt':'html'}
          }).done(function( response ) {
              $('#small-calendar-cnt').html(response);
            });
            
            
        /*
         nunjucks.configure({ autoescape: true });
         test = nunjucks.renderString(calendar.calendarTemplate, { username: 'James' });

        console.log(test);
        */
      
    });
};


