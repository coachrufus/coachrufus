
var TeamEventsManager = function ()
{
     this.edit_modal = $('#confirm-change-modal');
     this.delete_single_modal = $('#confirm-single-delete-modal');
     this.delete_calendar_modal = $('#confirm-calendar-delete-modal');
     this.season_modal = $('#add-season-modal');
     this.edit_confirm = false;
     this.map;
     this.google;
     this.place_marker = null;
     this.place_marker_infowindow = null;
     this.nearest_markers = [];
     this.map_center;
};


TeamEventsManager.prototype.initDetailMap = function()
{

    
    if( $('#event_detail_map').length)
    {
        var lat = parseFloat($('#event_detail_map').attr('data-lat'));
        var lng = parseFloat($('#event_detail_map').attr('data-lng'));
        var map = new google.maps.Map(document.getElementById('event_detail_map'), {
                center: {lat: lat, lng: lng},
                 scrollwheel: false,
                zoom: 15,
                draggable:false,
                mapTypeId: this.google.maps.MapTypeId.ROADMAP
            });


            var marker = new google.maps.Marker({
                position:  {lat: lat, lng: lng},
                map: map
            });
    }
};

TeamEventsManager.prototype.buildRepeatSummary = function()
{
      //sumary
      $('#repeat_summary_period span,  #repeat_summary_end_date, #repeat_summary_end_after, #repeat_summary_repeat_days').hide();
      
        if($('#record_period_interval').val() > 0)
        {
             $('#repeat_summary_period_interval').text($('#record_period_interval').val());
        }
    
       
        
        
        $('#repeat_summary_from').text($('#record_start_date').val());
        
        $('#repeat_summary_period').show();
        $('#repeat_summary_period_interval').show();
        $('#repeat_summary_period span').hide();
        $('#repeat_summary_period .'+$('#record_period').val()).show();
        
        /*
        if($('#record_period').val() == 'weekly')
        {
            $('#repeat_summary_repeat_days').show();
        }
         
         */
       var end_period_type = $('#record_end_period_type').val();
       if(end_period_type == 'after')
       {
           $('#repeat_summary_end_after').show();
           
           if('' != $('#record_end_period').val())
           {
                $('#repeat_summary_end_period').text($('#record_end_period').val());
           }
       }
       
        if(end_period_type == 'date')
       {
           $('#repeat_summary_end_date').show();
           
           if('' != $('#record_end').val())
           {
                $('#repeat_summary_to').text($('#record_end').val());
           }
           
        
       }
         
};

TeamEventsManager.prototype.init = function () {
    var manager = this;

    $('#record_start_date, #record_season').on('change input',function(){
            manager.checkSeasonRange();
    });
    
    
    
    $('.map-trigger').on('click',function(){ 
        if($(this).hasClass('active'))
        {
            $('#event_detail_map').css('position','absolute');
            $('#event_detail_map').css('top','-2000px');
            $(this).removeClass('active');
        }
        else
        {
            manager.initDetailMap();
            $('#event_detail_map').css('position','relative');
            $('#event_detail_map').css('top','0');
            $(this).addClass('active');
        }
     });
     
     

    
      $('input.recip_select').on('ifChecked',function(){
        var filter_class = $(this).val();
        $('.recipient-'+filter_class).each(function(key,val){
            $(val).iCheck('check');
            $(val).addClass('checked');
        });
   });
   
   $('input.recip_select').on('ifUnchecked',function(){
        var filter_class = $(this).val();
        $('.recipient-'+filter_class).each(function(key,val){
            
             $(val).iCheck('uncheck');
             $(val).removeClass('checked');
            
           
        });
   });


    $('.delete-event-trigger').on('click',function(e){
        e.preventDefault();
        var event_type = $(this).attr('data-event-type');
        
        var link = $(this).attr('href');
        var type = $(this).attr('data-event-type');
        
        var link_all = link+'&t=all';
        var link_only = link+'&t=only';
        var link_next = link+'&t=next';
        
        
        manager.delete_calendar_modal.find('#delete_event_link_all').attr('href',link_all);
        manager.delete_calendar_modal.find('#delete_event_link_all_repeat').attr('href',link_all);
        manager.delete_calendar_modal.find('#delete_event_link_only').attr('href',link_only);
        manager.delete_calendar_modal.find('#delete_event_link_next').attr('href',link_next);
        

        
        if('none' == event_type)
        {
            //manager.delete_single_modal.modal('show'); 
            manager.delete_calendar_modal.find('#single-delete-event-wrap').show();
            manager.delete_calendar_modal.find('#multi-delete-event-wrap').hide();
        }
        else
        {
            manager.delete_calendar_modal.find('#single-delete-event-wrap').hide();
            manager.delete_calendar_modal.find('#multi-delete-event-wrap').show();
            //manager.delete_calendar_modal.modal('show'); 
        }
        
        manager.delete_calendar_modal.modal('show'); 

    });


/*
    $('#delete-calendar-event-trigger').on('click',function(e){
        e.preventDefault();
        var action = $(this).attr('href');
         $.ajax({
            method: "POST",
            url: action,
          }).done(function( response ) {
               manager.delete_calendar_modal.find('.modal-body').html(response);
               manager.delete_calendar_modal.modal('show'); 
            });

       
    });
    */
    
    
    
    $('#add_season_trigger').on('click',function(){
        manager.season_modal.modal('show'); 
    });
    
   
   
    $('#add_season_form').on('submit',function(e){
         e.preventDefault();
         var form = $(this);
         var season_name = $('#season_name').val();
         var start_date = $('#season_start_date').val();
         var end_date = $('#season_end_date').val();
         
         /*
         if('' == season_name || '' == start_date || '' == end_date)
         {
              $('#error-alerts').addClass('alert alert-danger');
             return false;
         }
*/
         
        var action = $(this).attr('action');
        var data = $(this).serialize();
         $.ajax({
            method: "POST",
            url: action,
            data: data,
            dataType: 'json'
          }).done(function( response ) {
                console.log(response);
                 form.find('.control-label-error').remove();
                if(response.result == 'ERROR')
                {
                     //$('#error-alerts').addClass('alert alert-danger');
                    
                    $.each(response.errors,function(key,val){
                        form.find('[data-error-bind="'+key+'"]').append('<span class="control-label-error">'+val+'</span>');
                    });
                }
                else
                {
                     manager.season_modal.modal('hide');
                    new_option = $('<option/>',{text:response.name,value:response.value});
                    $('#record_season').append(new_option);
                    $('#record_season').effect("highlight", {}, 1000);
                }
               
            });
        
    });
    
    
    
    $('#record_period').on('change', function () {
        manager.showPeriodInterval($(this).val());
        manager.showPeriodWeekDays($(this).val());
        manager.showNotificationsOptions($(this).val());
        manager.showEventCycle($(this).val());
        
        if($(this).val() == 'none')
        {
            $('#repeat_summary').hide();
        }
        else
        {
             $('#repeat_summary').show();
        }
    });
    
    $('#record_period_interval, #record_end_period').on('keyup',function(){
         manager.buildRepeatSummary();
    });
    
    $('#record_end_period_type').on('change',function(){
        manager.buildRepeatSummary();
    });
    $('#record_end').on('change keydown keypress keyup mousedown click mouseup focus',function(){
        manager.buildRepeatSummary();
    });
    
    $('#record_end_period_type').on('change',function(){
         $('.end_period_type_choice').hide();
        $('.end_period_type_'+$(this).val()).show();
    });
    
    
    $('#event-repeat-options-trigger').on('click',function(e){ 
        e.preventDefault();
        
        if($(this).hasClass('collapse-trigger-collapsed'))
        {
            $('#event-repeat-options').fadeIn();
            $(this).removeClass('collapse-trigger-collapsed');
            $(this).addClass('collapse-trigger-active');
        }
        else
        {
            $('#event-repeat-options').fadeOut();
            $(this).addClass('collapse-trigger-collapsed');
            $(this).removeClass('collapse-trigger-active');
        }
      
    });
    
    $('#event-notification-options-trigger').on('click',function(e){ 
        e.preventDefault();
        
         if($(this).hasClass('collapse-trigger-collapsed'))
        {
            $('#event-notification-options').fadeIn();
            $(this).removeClass('collapse-trigger-collapsed');
            $(this).addClass('collapse-trigger-active');
        }
        else
        {
            $('#event-notification-options').fadeOut();
            $(this).addClass('collapse-trigger-collapsed');
            $(this).removeClass('collapse-trigger-active');
        }
    });


    $('#record_notify_termin_type').on('change',function(){
        $('.notify_termin_type_options').hide();
        $('.notify_termin_type_options_'+$(this).val()).show();
    });
    
    
    //manager.showNotificationsOptions( $('#record_period').val());
    
    
    
    $('#add_playground_trigger').on('click',function(e){
        e.preventDefault();
        $('#new_event_playground_wrap').show();
        //playground_lat =  $('#new_event_playground_wrap').attr('data-lat');
       // playground_lng =  $('#new_event_playground_wrap').attr('data-lng');
        //manager.placeMarker(manager.map,{lat:parseFloat(playground_lat),lng:parseFloat(playground_lng)});
        //manager.map.setCenter({lat:parseFloat(playground_lat),lng:parseFloat(playground_lng)});
    });

/*
    $('#playground_choice').on('change', function (e) {
        e.preventDefault();
        manager.showPlaygroundDesc($(this));
        var choice_val = $(this).val();
        
        $('#record_playground_id').val(choice_val);
        playground_desc = $('#playground_' + choice_val);  
        playground_lat = playground_desc.attr('data-lat');
        playground_lng = playground_desc.attr('data-lng');
         manager.placeMarker(manager.map,{lat:parseFloat(playground_lat),lng:parseFloat(playground_lng)});
         manager.map.setCenter({lat:parseFloat(playground_lat),lng:parseFloat(playground_lng)});
        
    });
    */
    $('#end_period_type_never, #end_period_type_after, #end_period_type_date').on('ifChecked',function(){
        $('.end-period-type-wrapper input[type="text"]').val('');
        $('.end-period-type-wrapper input[type="text"]').attr('disabled','disabled');

        $(this).parents('.checkbox-input').find('input[type="text"]').attr('disabled',false);
    });

    

    $('#record_notification').on('ifChecked', function () {
        $('#notify-team-wrap').show();
    });
    
    $('#record_notification').on('ifUnchecked', function () {
        $('#notify-team-wrap').hide();
    });

    if($('#record_notification').is(':checked'))
    {
         $('#notify-team-wrap').show();
    }
    else
    {
         $('#notify-team-wrap').hide();
    }

    $('#notify_termin_type_later').on('ifChecked', function () {
        $('#notify_termin_wrap').show();
    });
    $('#notify_termin_type_later').on('ifUnchecked', function () {
        $('#notify_termin_wrap').hide();
    });


    $(document).on('click','.attendance_trigger',function(e){
        e.preventDefault();
        manager.changePlayerAttendance($(this));
    });
    
    $(document).on('change','.attendance_fee_trigger',function(e){
        e.preventDefault();
        manager.changePlayerFee($(this));
    });
    
    $(document).on('change','.costs_trigger',function(e){
        e.preventDefault();
        manager.changeCosts($(this));
    });


    $('.change_event_playground_trigger').on('click',function(e){
        e.preventDefault();
        $('#current_playground_wrap').hide();
        $('#change_playground_wrap').css('visibility','visible');
    });


     $('.event-detail-notify').on('click',function(e){
          e.preventDefault();
          $('#notification-modal').find('.send-success').html('');
          $('#notification-modal').find('.send-success').hide();
          $('#notification-modal').modal('show');
     });
     
      $('.notification-send-trigger').on('click',function(){
         manager.sendNotification($(this));
    }) ;
    
    $('#edit_event_form').on('submit',function(e){
        
        
        
        
        if(manager.edit_confirm == false)
        {
            
            e.preventDefault();
            
            
            /*
            data =   $('#edit_event_form').serialize();
             $.ajax({
                method: "GET",
                url: action,
                data: data,
                dataType: 'json'
              }).done(function( response ) {
                    manager.season_modal.modal('hide');
                    new_option = $('<option/>',{text:response.name,value:response.value});
                    $('#record_season').append(new_option);
                    $('#record_season').effect("highlight", {}, 1000);
                });
            */
            
            var repeat_event = $('#record_period').val();
            var source_period_type = $('#source_period_type').val();
            
            if(source_period_type != 'none')
            {
                manager.edit_modal.modal('show');
            }
            else
            {
                $('#change_type').val('only');
                manager.edit_confirm = true;
                $('#edit_event_form').submit();
            }
        }
    });
    
    $('.confirm-change-submit').on('click',function(e){
        e.preventDefault();
        $('#change_type').val($(this).attr('data-val'));
        manager.edit_confirm = true;
        $('#edit_event_form').submit();

    });
    
     $(document).on('click', '.select_playground_trigger', function(e){
               e.preventDefault();
               playground_id = $(this).attr('data-id');
               playground_name = $(this).attr('data-name');
               playground_address = $(this).attr('data-address');
               $('#exist_playground').val(playground_id);
               $('#pac-input').val(playground_address);
               
               if('' == $('#playground_name').val()){
                   $('#playground_name').val(playground_name);
               }
               
               
              
               
            } );
            
            

};



TeamEventsManager.prototype.loadNearestPlaygrounds = function(latLng){


    var manager = this;
    $.ajax({
        method: "GET",
        url: '/ajax/locality/area',
        data: {lat:latLng.lat(),lng:latLng.lng()},
        dataType: 'json'
      }).done(function( response ) {
            
            //reset markers
            manager.nearest_markers.forEach(function (marker) {
                marker.setMap(null);
            });
      
            response.forEach(function (playground) {
               playground_center = {lat:playground.lat,lng:playground.lng};
               manager.placeNearestMarker(manager.map,{lat:parseFloat(playground.lat),lng:parseFloat(playground.lng)},playground);
            });
        });
        
};


TeamEventsManager.prototype.initMap = function(latLng)
{
      this.map = new google.maps.Map(document.getElementById('map'), {
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        
     if(null != latLng)
     {
         this.map.setCenter(latLng);
     }
    
    
        
      this.placeMarker(this.map, latLng);
      this.addMapClickListener(this.map);
};

TeamEventsManager.prototype.showSearchMap = function()
{
     $('#map').css('position','relative');
     $('#map').css('left','0');
};

TeamEventsManager.prototype.hideSearchMap = function()
{
     $('#map').css('position','absolute');
     $('#map').css('left','-2000px');
};



TeamEventsManager.prototype.createSearchInput = function()
{
    var manager = this;
    var searchBox = new this.google.maps.places.SearchBox(document.getElementById('pac-input'));
    searchBox.addListener('places_changed', function () {
        manager.places = searchBox.getPlaces();
        manager.map_center =  manager.places[0].geometry.location;
        manager.setNewLocation(manager.map_center);
        manager.map.setCenter(manager.map_center);
        manager.placeMarker(manager.map,manager.map_center);
        manager.loadNearestPlaygrounds(manager.map_center);
       
    });
};


TeamEventsManager.prototype.setNewLocation = function(latLng)
{
     var manager = this;
     var geocoder = new google.maps.Geocoder;
            geocoder.geocode({'location': latLng}, function (results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                       

                        $('#locality_json_data').val(JSON.stringify(results[0]));
                        //$('#playground_name').val(results[0].formatted_address);
                        //results[0].formatted_address;
                        
                        if(manager.place_marker_infowindow !== null)
                        {
                            manager.place_marker_infowindow.setMap(null);
                        }
                        
                         manager.place_marker_infowindow  = new  manager.google.maps.InfoWindow({
                                    content: results[0].formatted_address+'<br /><a data-address="'+results[0].formatted_address+'" data-id="" href="#" class="select_playground_trigger">Select this place</a>'
                                  });


                        manager.google.maps.event.addListener(manager.place_marker, 'click', function() {
                            manager.place_marker_infowindow.open(manager.map,manager.place_marker);
                         });
                         
                        manager.place_marker_infowindow.open(manager.map,manager.place_marker);
                        
                        
                        
                        
                        
                    } else {
                        window.alert('No results found');
                    }
                } else {
                    window.alert('Geocoder failed due to: ' + status);
                }
            });
      
};



TeamEventsManager.prototype.placeMarker = function(map, location)
{
    var manager = this;
    if(manager.place_marker !== null)
    {
        manager.place_marker.setMap(null);
    }
    
    
    manager.place_marker = new this.google.maps.Marker({
        position: location,
        draggable:true,
        map: map
    });

    this.google.maps.event.addListener(manager.place_marker, 'dragend', function (event) {
        manager.setNewLocation(event.latLng);
    });
};


TeamEventsManager.prototype.placeNearestMarker = function(map, location,playground)
{
    var manager = this;
    var marker = new this.google.maps.Marker({
        position: location,
        map: map,
    });
    
    var infowindow = new  this.google.maps.InfoWindow({
        content: playground.city+','+playground.street+' '+playground.street_number+'<br /><strong>also known as:</strong><br />'+playground.name+'<br /><a data-address="'+playground.city+','+playground.street+' '+playground.street_number+'" data-name="'+playground.name+'" data-id="'+playground.id+'" href="#" class="select_playground_trigger">Select this playground</a>'
      });
    
    this.google.maps.event.addListener(marker, 'click', function() {
        infowindow.open(map,marker);
     });
    infowindow.open(map,marker);
    
    
    this.nearest_markers.push(marker);
    
    
};





TeamEventsManager.prototype.addMapClickListener = function (map)
{
    
    var manager = this;
    manager.google.maps.event.addListener(map, 'click', function (event) {
        var marker = manager.placeMarker(map, event.latLng);
        manager.setNewLocation(event.latLng);
    });
};






TeamEventsManager.prototype.showPeriodInterval = function (type) {

    $('.event_interval_label').hide();
    if (type == 'none')
    {
        $('#period_interval_wrapper').fadeOut();
    }
    else
    {
        $('#period_interval_wrapper').fadeIn();
        $('.interval_label_'+type).show();
    }
    this.buildRepeatSummary();

};
TeamEventsManager.prototype.showEventCycle = function (type) {

    if (type == 'none')
    {
        $('.end-period-type-wrapper').removeClass('active');
    }
    else
    {
        $('.end-period-type-wrapper').addClass('active');
    }
     this.buildRepeatSummary();
};

TeamEventsManager.prototype.showPeriodWeekDays = function (type) {

    if (type == 'weekly')
    {
        $('#period_interval_week_wrapper').fadeIn();
        
        var repeat_days = $('input[name="record[repeat_days][]"]:checked');
        if(repeat_days.length == 0)
        {
            var start_date_parts = $('#record_start_date').val().split(".");
            year  = parseInt(start_date_parts[2]);
            month  = parseInt(start_date_parts[1]);
            day  = parseInt(start_date_parts[0]);
            date_string = month+"/"+day+"/"+year;
            var start_date = new Date(date_string);
            //$('input').iCheck('check');
           $('#repeat_day_'+start_date.getDay()).iCheck('check');

        }        
        
    }
    else
    {
        $('#period_interval_week_wrapper').fadeOut();
    }
     this.buildRepeatSummary();
};

TeamEventsManager.prototype.showNotificationsOptions = function (type) {

    if (type == 'none')
    {
        $('#record_notify_termin_type option').show();
        $('.notify_termin_type_options_date').show();
    }
    else
    {
        $('#record_notify_termin_type').val('beep');
        $('.notify_termin_type_options_beep').show();
        //$('#record_notify_termin_type option[value="now"]').hide();
        $('#record_notify_termin_type option[value="date"]').hide();
        $('.notify_termin_type_options_date').hide();
    }


    /*
    if (type == 'none')
    {
        $('.notify_termin_type_single').show();
    }
    else
    {
        $('.notify_termin_type_single').hide();
        $('#notify_termin_type_beep').iCheck('check');
        $('#notify_termin_type_now').iCheck('uncheck');
    }
    */
};


TeamEventsManager.prototype.showPlaygroundDesc = function (element) {
    $('.playground-desc').hide();
    $('#playground_' + element.val()).show();
};

TeamEventsManager.prototype.sendNotification = function (element) {
    var message = $('#notification-message').val();
    var url = $('#notification-form').attr('action');
    var data  = $('#notification-form').serialize();
    
    $('#notification-modal .modal-loader').slideDown();
     $.ajax({
        method: "GET",
        url: url ,
        data: data,
        dataType: 'json'
      }).done(function( msg ) {
          //$('#notification-modal').modal('hide');
          $('#notification-modal .modal-loader').slideUp();
          $('#notification-modal .send-success').append('<p>'+msg.message+'</p>');
          $('#notification-modal .send-success').fadeIn();
        });
};


TeamEventsManager.prototype.changeCosts = function (element) {
    url = element.attr('data-url');
    event_id = element.attr('data-event');
    event_date = element.attr('data-eventdate');
    costs = element.val();
    
    label = element.siblings("label").eq(0);
    label.html('<i class="fa fa-refresh fa-spin fa-fw"></i>');
    
    $.ajax({
       method: "GET",
       url: url ,
       data: { event_id: event_id,
               event_date:event_date,
               costs: costs}
    }).done(function( msg ) {
        label.html('€');
    }).fail(function (xhr, ajaxOptions, thrownError) {
             //element.html(btn_html);
             system_alert = new SystemAlert();
             system_alert.showErrorAlert();
    });   
}

TeamEventsManager.prototype.changePlayerFee = function (element) {
    url = element.attr('data-url');
    event_id = element.attr('data-event');
    event_date = element.attr('data-eventdate');
    player_id = element.attr('data-player');
    team_player_id = element.attr('data-team-player');
    fee = element.val();
    
    label = element.siblings("label").eq(0);
    label.html('<i class="fa fa-refresh fa-spin fa-fw"></i>');
    
    $.ajax({
       method: "GET",
       url: url ,
       data: { event_id: event_id,
               event_date:event_date,
               player_id:player_id,
               team_player_id:team_player_id,
               fee: fee}
    }).done(function( msg ) {
        label.html('€');
    }).fail(function (xhr, ajaxOptions, thrownError) {
             element.html(btn_html);
             system_alert = new SystemAlert();
             system_alert.showErrorAlert();
    });   
}


TeamEventsManager.prototype.changePlayerAttendance = function (element,callback) {
    type = element.attr('data-rel');
    url = element.attr('data-url');
    event_id = element.attr('data-event');
    event_date = element.attr('data-eventdate');
    player_id = element.attr('data-player');
    team_player_id = element.attr('data-team-player');
    team_role = element.attr('data-team-role');
    team_id = element.attr('data-team-id');
    btn_in = element.parent().find('.in');
    btn_na = element.parent().find('.na');
    btn_out = element.parent().find('.out');
    
    if(element.hasClass('my_attendance_trigger'))
    {
        parent_row = $('.attendant-row-'+team_player_id);
    }
    else
    {
        parent_row = element.parents('.attendant-row');
    }
    
   
    btn_out.removeClass('deny');
    btn_in.removeClass('accept');
    btn_na.removeClass('btn-warning');
    var btn_html = element.html();
    
    element.html('<i class="fa fa-refresh fa-spin fa-fw"></i>');

    $.ajax({
        method: "GET",
        url: url ,
        data: { event_id: event_id,event_date:event_date,type:type,player_id:player_id,team_player_id:team_player_id,team_id:team_id}
      }).done(function( msg ) {

           if(element.hasClass('my_attendance_trigger'))
           {
               $('.my-attendance').hide();
           }
           element.html(btn_html);
           if ('in' == type)
            {
                btn_in.addClass('accept');
                parent_row.find('.in').addClass('accept');
                    if(team_role == 'GUEST')
                    {
                         //target_group = $('#attendance-group-in-guest');
                          $('#attendance-group-in-guest').append(parent_row);
                          $('#attendance-group-in-guest').effect("highlight", {}, 2000);
                          //$('html, body').animate({ scrollTop: $('#attendance-group-in-guest').offset().top}, 500);
                    }
                    else
                    {
                         //target_group = $('#attendance-group-in-players');
                         $('#attendance-group-in-players').append(parent_row);
                         
                         var current_count = $('#attendance-group-in-players .attendant-row').size();
                         var event_limit = $('#attendance-group-in-players').attr('data-event-limit');
                         
                         if(current_count == event_limit)
                         {
                              $('#attendance-group-in-players').append('<div class="attendance-limit-divider"></div>');
                         }
                         
                         
                         $('#attendance-group-in-players').effect("highlight", {}, 2000);
                         
                           if($('#attendance-group-in-players').length)
                            {
                                //$('html, body').animate({ scrollTop: $('#attendance-group-in-players').offset().top}, 500);
                            }
                
                         
                    }

            }
            if ('out' == type)
            {
                 btn_out.addClass('deny');
                 parent_row.find('.out').addClass('deny');
                //target_group = $('#attendance-group-out');
                 //$('#attendance-group-in-players').detach().append(parent_row);
                //parent_row.detach().prepend('#attendance-group-out');
                
                $('#attendance-group-out h2').after(parent_row);
                
                 
                var current_count = $('#attendance-group-in-players .attendant-row').size();
                var event_limit = $('#attendance-group-in-players').attr('data-event-limit');

                if(current_count < event_limit)
                {
                     $('.attendance-limit-divider').remove();
                }
                 
                $('#attendance-group-out').effect("highlight", {}, 2000);
                
                if($('#attendance-group-out').length)
                {
                    //$('html, body').animate({ scrollTop: $('#attendance-group-out').offset().top}, 500);
                }
                
                
            }
            if ('na' == type)
            {
                 btn_na.addClass('btn-warning');
            }
            
            attendance_info = msg.attendanceInfo;
            $('.members-attendance-'+attendance_info.uid+' .members-attendance-in').text(attendance_info.in);
            $('#members-attendance-'+attendance_info.uid+' .members-attendance-in').text(attendance_info.in);
            $('.members-attendance-'+attendance_info.uid+' .progress-bar').css('width',attendance_info.progress+'%');
            $('.members-attendance-'+attendance_info.uid+' .members-attendance-text').css('left',attendance_info.progress+'%');
            
            if(attendance_info.in == 0)
            {
                 //$('.members-attendance-'+attendance_info.uid+' .members-attendance-text').css('margin-left','0');
            }
            else
            {
                //$('.members-attendance-'+attendance_info.uid+' .members-attendance-text').css('margin-left','-50px');
            }
            

            if(attendance_info.in > 1)
            {
                $('.btn_continue').fadeIn();
                $('#event_attendance_confirm.ready').modal('show'); 
                rooster_link = '/team-match/new-lineup?event_id='+event_id+'&event_date='+event_date;
                 $('#event_attendance_confirm.ready').find('.s2s_attendance_confirm_link').attr('href',rooster_link);
                
                $('#event_attendance_confirm').removeClass('ready'); 
            }
            
            
             var attendance_accept_players_count =  $('#attendance-group-in-players .attendant-row').length;
             var attendance_accept_guest_count = $('#attendance-group-in-guest .attendant-row').length;
             var attendance_deny_count =  $('#attendance-group-out .attendant-row').length;
             
             
             $('.attendance_accept_players_count').text(attendance_accept_players_count);
             $('.attendance_accept_guest_count').text(attendance_accept_guest_count);
             $('.attendance_deny_count').text(attendance_deny_count);
             //$('#attendance-group-in-players .attendant-row').length;
           
            fee_input_el = element.siblings(".attendance_fee").eq(0);
            fee_input_el.prop( "disabled", false );
            
            
           if(msg.result != 'create_success')
           {
                system_alert = new SystemAlert();
                system_alert.showErrorAlert();
           }
           else
           {
               if(typeof callback === "function")
                {
                    callback();
                }
           }

           //detach_row = parent_row.detach();
           
          
           
           

        }).fail(function (xhr, ajaxOptions, thrownError) {
             element.html(btn_html);
             system_alert = new SystemAlert();
             system_alert.showErrorAlert();
        });
};

TeamEventsManager.prototype.checkSeasonRange = function(){
    
    $('.season_range_error').hide();
    var season_val = $('#record_season option:selected').text();
    var season_parts = season_val.split(' ');
    var season_range = season_parts[1];
    var season_dates = season_range.split('-');
    
    
    var start_date_text = $('#record_start_date').val().split('.');
    var start_date = new Date(start_date_text[2],start_date_text[1],start_date_text    [0]);
    if(season_dates.length == 2)
    {
        var start_text = season_dates[0].replace('(','').split('.');
        var end_text = season_dates[1].replace(')','').split('.');
        
        var season_start = new Date('20'+start_text[2],start_text[1],start_text[0]);
        var season_end = new Date('20'+end_text[2],end_text[1],end_text[0]);

        console.log(season_start);
        console.log(season_end);
        console.log(start_date.getTime());
        
        if(start_date.getTime() < season_start.getTime() || start_date.getTime() > season_end.getTime())
        {
            $('.season_range_error').show();
        }
    }
};