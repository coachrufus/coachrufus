var WebviewManager = function ()
{
    this.postMessageOrigin = null;
    this.postmessageSource = null;


};


WebviewManager.prototype.showSidebar = function(target,sidebar_trigger){
      $('.row-sidebar-trigger').removeClass('active');
        $('.row-sidebar-cnt.active').hide();
        target.slideDown();
        sidebar_trigger.addClass('active');
        target.addClass('active');
        
        $('html, body').animate({
            scrollTop: $(target).offset().top-230
        }, 500);
        
        
};

WebviewManager.prototype.handleSidebar = function () {
    var manager = this;
    
    $(document).on('click', function (e) {
        var trigger = $(e.target);
        if (trigger.hasClass('row-sidebar-trigger') || trigger.parents('.row-sidebar-trigger').length > 0)
        {
            e.preventDefault();
            if (trigger.hasClass('row-sidebar-trigger'))
            {
                var target = $('#' + trigger.attr('data-target'));
                var sidebar_trigger = trigger;
            } else
            {
                var target = $('#' + trigger.parent().attr('data-target'));
                var sidebar_trigger = trigger.parent();
            }

            if (target.hasClass('active'))
            {
                target.slideUp();
                $('.row-sidebar-trigger').removeClass('active');
                target.removeClass('active');
            } else
            {
                manager.showSidebar(target,sidebar_trigger);
            }
        } 
        else
        {
            if ($('.row-sidebar-trigger.active').length > 0)
            {
                if (!$(e.target).hasClass('tool-item'))
                {
                    $('.row-sidebar-cnt').slideUp();
                    $('.row-sidebar-trigger').removeClass('active');
                    $('.row-sidebar-cnt').removeClass('active');
                }
            }
        }
    });
};

WebviewManager.prototype.handlePostMessages = function () {
    var manager = this;

    document.addEventListener('message', function (e) {
        manager.postMessageOrigin = e.origin;
        manager.postmessageSource = e.source;
    });

    manager.handleSendPostMessages();
};

WebviewManager.prototype.handleSendPostMessages = function () {
    var manager = this;
    $(document).on('click','.mpopup', function (e) {
        e.preventDefault();
        var message = {
            'href': $(this).attr('href'),
            'action': $(this).attr('data-action'),
            'screen': $(this).attr('data-screen'),
            'onclose-action': $(this).attr('data-popup-onclose-action')
        };

        window.postMessage(JSON.stringify(message), manager.postMessageOrigin);
    });
    
    $(document).on('click','.mpopup1', function (e) {
        e.preventDefault();
        var message = {
            'href': $(this).attr('data-href'),
            'action': $(this).attr('data-action'),
            'screen': $(this).attr('data-screen'),
            'onclose-action': $(this).attr('data-popup-onclose-action')
        };

        window.postMessage(JSON.stringify(message), manager.postMessageOrigin);
    });
    
    
    
};

