
var TeamManager = function (data)
{
    data = data || {};
    this.edit_photo_modal = $('#edit-photo-modal');
    this.delete_modal = $('#confirm-team-delete-modal');
    this.leave_modal = $('#confirm-team-leave-modal');
    this.invitation_modal = $('#send-invitation-modal');
    this.other_sport_modal = $('#other-sport-modal');
    this.container = $('.team-panel');
    this.dataUrl = data.dataUrl || null;
    
    this.locationMap;
    this.google;
    this.place_marker = null;
    this.map_center = {lat: 48.978076, lng: 19.519994};
    this.graphColors = {up: {base:'#81B82B',indicator:'#5C7D3E'},neutral: {base:'#D7D9DB',indicator:'#888E93'},down:{base:'#CE3346',indicator:'#823A48'}};
};

TeamManager.prototype.showLoader = function (element)
{
    element.html(' <div class="alert alert-info text-center team-panel-loader"><i class="fa fa-refresh fa-spin fa-3x fa-fw"></i><br />Loading</div>');
};

TeamManager.prototype.drawCRSGraph = function (graph_element)
{
    var direction = graph_element.attr('data-direction');
    var colors = this.graphColors[direction];

    var data = [
        { label: "",  data:  graph_element.attr('data-val1'),  color: colors.base},
        { label: "",  data: graph_element.attr('data-val2'), color: colors.indicator}
    ];
   this.drawGraph(graph_element,data);
};

TeamManager.prototype.drawGoalGraph = function (graph_element)
{
    var direction = graph_element.attr('data-direction');
    var colors = this.graphColors[direction];

    var data = [
        { label: "",  data:  graph_element.attr('data-val1'),  color: colors.base},
        { label: "",  data: graph_element.attr('data-val2'), color: colors.indicator}
    ];
   this.drawGraph(graph_element,data);
};
TeamManager.prototype.drawAssistGraph = function (graph_element)
{
    var direction = graph_element.attr('data-direction');
    var colors = this.graphColors[direction];

    var data = [
        { label: "",  data:  graph_element.attr('data-val1'),  color: colors.base},
        { label: "",  data: graph_element.attr('data-val2'), color: colors.indicator}
    ];
   this.drawGraph(graph_element,data);
};


TeamManager.prototype.drawGraph = function(graph_element,data)
{
    $.plot(graph_element, data, {
        series: {
            pie: {
                innerRadius: 0.80,
                show: true,
                label: {
                    show:false
                },
                stroke: {
                    width: 0,
                    color: '00000'
                }
            }
        },
        legend: {
            show: false
        }
    });
};

TeamManager.prototype.loadWidget = function (element) {
    this.showLoader(element);
    var manager = this;
    
    team_id = element.attr('data-team-id');
    action = this.dataUrl;
    $.ajax({
        method: "GET",
        url: action,
        data: {'id': team_id}
    }).done(function (response) {
        element.html(response);

        graph_crs_element = element.find('.avg-crs .team-member-graph');
        graph_goal_element = element.find('.avg-goal .team-member-graph');
        graph_assist_element = element.find('.avg-assist .team-member-graph');
        
        if(graph_crs_element.length)
        {
            manager.drawCRSGraph(graph_crs_element);
            manager.drawGoalGraph(graph_goal_element);
            manager.drawAssistGraph(graph_assist_element);
        }

       
        
    });
};


TeamManager.prototype.bindSwitchMoreDetail = function()
{
    $('.team-more-detail-trigger').on('click',function(e){
        e.preventDefault();
        
        var target = $($(this).attr('href'));
        var current_text = $(this).find('span').text();
        var new_text = $(this).attr('data-switch-text');
        
        $(this).find('span').text(new_text);
        $(this).attr('data-switch-text',current_text);
        

        
        if(target.hasClass('in'))
        {

            $('#team_detail_status').val('no');
            target.removeClass('in');
            target.addClass('out');
            $(this).find('i.ico').removeClass('ico-minus-g');
            $(this).find('i.ico').addClass('ico-plus-green');
        }
        else
        {
 
            $('#team_detail_status').val('yes');
            target.removeClass('out');
            target.addClass('in');
            $(this).find('i.ico').addClass('ico-minus-g');
            $(this).find('i.ico').removeClass('ico-plus-green');
        }
    });
};

TeamManager.prototype.bindShowMapTrigger = function()
{
    var manager = this;
    $('.map_trigger').on('click',function(e){
        e.preventDefault();
        
        if($('#map').hasClass('active'))
        {
            manager.hideSearchMap();
        }
        else
        {
            manager.showSearchMap();
        }
        
        
    });
};


TeamManager.prototype.addOtherSportTrigger = function()
{
    var manager = this;
     $('#team_sport_id').on('change',function(e){
        if( $(this).find(":selected").hasClass('other-sports-choices'))
        {
            manager.other_sport_modal.modal('show');
             
        }
    });
};





TeamManager.prototype.init = function ()
{
    this.bindShowMapTrigger();
    this.bindSwitchMoreDetail();
    this.fileuploadTrigger();
    this.addEditPhotoTrigger($('.edit-team-photo'));
    this.addRemovePhotoTrigger($('.delete-team-photo'));
    this.addDeleteTrigger($('.delete-team-trigger'));
    this.addLeaveTrigger($('.leave-team-trigger'));
    this.addOtherSportTrigger();
    //this.sendInvitationTrigger($('.send-invitation'));
    //$('.edit-team-photo').
};

TeamManager.prototype.addDeleteTrigger = function (elem)
{
    manager = this;
    elem.on('click',function(e){
        e.preventDefault();
        manager.delete_modal.modal('show');
    });
};

TeamManager.prototype.addLeaveTrigger = function (elem)
{
    manager = this;
    elem.on('click',function(e){
        e.preventDefault();
        manager.leave_modal.modal('show');
    });
};

TeamManager.prototype.addEditPhotoTrigger = function (elem)
{
    manager = this;
    elem.on('click',function(e){
        e.preventDefault();
        manager.edit_photo_modal.modal('show');
    });
};

TeamManager.prototype.addRemovePhotoTrigger = function (elem)
{
    manager = this;
    link = elem.attr('href');
    elem.on('click',function(e){
        e.preventDefault();
        $.ajax({
            method: "GET",
            url: link,
            dataType: 'json'
        }).done(function (response) {
                
               //$('#exist_team_settings_photo').attr('src',response.file);
               
               $('#exist-photo-wrap').css('background-image','url("' + response.file + '")');
            
        });
    });
};

TeamManager.prototype.fileuploadTrigger = function()
{
    var manager = this;
    
     $(document).bind('drop dragover', function (e) {
        e.preventDefault();
    });
    
    $(document).bind('dragover', function (e) {
        var dropZone = $('#dropzone'),
            timeout = window.dropZoneTimeout;
        if (!timeout) {
            dropZone.addClass('in');
        } else {
            clearTimeout(timeout);
        }
        var found = false,
            node = e.target;
        do {
            if (node === dropZone[0]) {
                found = true;
                break;
            }
            node = node.parentNode;
        } while (node != null);
        if (found) {
            dropZone.addClass('hover');
        } else {
            dropZone.removeClass('hover');
        }
        window.dropZoneTimeout = setTimeout(function () {
            window.dropZoneTimeout = null;
            dropZone.removeClass('in hover');
        }, 100);
    });
    
    
    $('#team_settings_photo').fileupload({
      dropZone: $('#dropzone'),
       done: function (e, data) {
           $.each(data.result.files, function (index, file) {
               $('<p/>').text(file.name).appendTo(document.body);
           });
       },
       progress: function (e, data) {
           $('.upload-progress').show();
           var progress = parseInt(data.loaded / data.total * 100, 10);
           $('.upload-progress .progress-num').text((progress-1) + '%');
           $('.upload-progress .progress-bar').css('width',progress + '%');
           
           $('.upload-error').remove();
       },
       done: function (e, data) {

           
           if(data.result.result == 'SUCCESS')
           {
               $('#exist-photo-wrap').css('background-image','url("' + data.result.file + '")');
                $('#team_photo').val(data.result.base_name);
                var edit_img = $('<a/>',{class:'edit-team-photo',text:'<i class="fa fa-pencil-square-o"></i>'});
                var remove_img = $('<a/>',{class:'delete-team-photo',text:'<i class="fa fa-times"></i>'});
                manager.addEditPhotoTrigger(edit_img);
                $('.upload-progress .progress-num').text('100%');
                $('.upload-progress').hide();
                manager.edit_photo_modal.modal('hide');
           }
           
           if(data.result.result == 'ERROR')
           {
               $('.upload-wrap').before('<div class="alert alert-error upload-error">'+data.result.error_message +'</div>');
           }
           

       }
   });
};

TeamManager.prototype.addSendInvitationTrigger = function(elem)
{
     manager = this;
     elem.on('click',function(e){
         e.preventDefault();
         $('#team_invitation').val(elem.attr('data-hash'));
         manager.addSendInvitationFormHandler($('.send-invitation-form'));
         manager.invitation_modal.modal('show');
     });
    
};

TeamManager.prototype.addSendInvitationFormHandler = function(elem)
{
    manager = this;
    link = elem.attr('action');
    team_hash = $('#team_invitation').val();
    elem.on('submit',function(e){
        e.preventDefault();
        $.ajax({
            method: "GET",
            data:{hash:team_hash},
            url: link,
            dataType: 'json'
        }).done(function (response) {
                
              elem.html('<div class="alert alert-success">'+response.message+'</div>');
            
        });
    });
};


TeamManager.prototype.initMap = function(latLng)
{
      this.map = new google.maps.Map(document.getElementById('map'), {
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        
     if(null != latLng)
     {
         this.map.setCenter(latLng);
         this.placeMarker(this.map, latLng);
     }
     else
     {
          this.map.setCenter(this.map_center);
          this.map.setZoom(2);
     }
 
      
     this.addMapClickListener(this.map);
};

TeamManager.prototype.showSearchMap = function()
{
     $('#map').css('position','relative');
     $('#map').css('left','0');
     $('#map').addClass('active');
};

TeamManager.prototype.hideSearchMap = function()
{
     $('#map').css('position','absolute');
     $('#map').css('left','-2000px');
      $('#map').removeClass('active');
};

TeamManager.prototype.createSearchInput = function()
{
    var manager = this;
    var searchBox = new this.google.maps.places.SearchBox(document.getElementById('locality-search'));
    searchBox.addListener('places_changed', function () {
        manager.places = searchBox.getPlaces();
        manager.map_center =  manager.places[0].geometry.location;
        manager.setNewLocation(manager.map_center);
        manager.map.setZoom(14);
        manager.map.setCenter(manager.map_center);
        manager.placeMarker(manager.map,manager.map_center);
    });
};


TeamManager.prototype.setNewLocation = function(latLng)
{
     var manager = this;
     var geocoder = new google.maps.Geocoder;
            geocoder.geocode({'location': latLng}, function (results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                       

                        $('#locality_json_data').val(JSON.stringify(results[0]));
                        //$('#playground_name').val(results[0].formatted_address);
                        //results[0].formatted_address;
                        
                        /*
                        if(manager.place_marker_infowindow !== null)
                        {
                            manager.place_marker_infowindow.setMap(null);
                        }
                        
                         manager.place_marker_infowindow  = new  manager.google.maps.InfoWindow({
                                    content: results[0].formatted_address+'<br /><a data-address="'+results[0].formatted_address+'" data-id="" href="#" class="select_playground_trigger">Select this place</a>'
                                  });


                        manager.google.maps.event.addListener(manager.place_marker, 'click', function() {
                            manager.place_marker_infowindow.open(manager.map,manager.place_marker);
                         });
                         
                        manager.place_marker_infowindow.open(manager.map,manager.place_marker);
                        
                        */
                        
                        
                        
                    } else {
                        window.alert('No results found');
                    }
                } else {
                    window.alert('Geocoder failed due to: ' + status);
                }
            });
      
};



TeamManager.prototype.placeMarker = function(map, location)
{
    var manager = this;
    if(manager.place_marker !== null)
    {
        manager.place_marker.setMap(null);
    }
    
    
    manager.place_marker = new this.google.maps.Marker({
        position: location,
        draggable:true,
        map: map
    });

    this.google.maps.event.addListener(manager.place_marker, 'dragend', function (event) {
        manager.setNewLocation(event.latLng);
    });
};


TeamManager.prototype.addMapClickListener = function (map)
{
    
    var manager = this;
    manager.google.maps.event.addListener(map, 'click', function (event) {
        var marker = manager.placeMarker(map, event.latLng);
        manager.setNewLocation(event.latLng);
    });
};



/*
ScoutingManager.prototype.bindAddScouting = function () {
    $(document).on('click', '.add-scouting-trigger', function (e) {
        e.preventDefault();
        
        manager.addModal.find('.scouting-form-wrap').hide();
        
        var target_form = $(this).attr('data-form');
        $('.' + target_form).show();
        manager.addModal.modal('show');
    });
};

ScoutingManager.prototype.bindSendForm = function () {
   manager = this;
    $('.scouting-form').on('submit', function (e) {
        e.preventDefault();
        
        var form = $(this);
        var data = form.serialize();
        var action = form.attr('action');


        $.ajax({
            method: "POST",
            url: action,
            data: data,
            dataType: 'json'
        }).done(function (response) {
            form.find('.control-label-error').remove();
            if (response.result == 'ERROR')
            {
                $.each(response.errors, function (key, val) {
                    form.find('[data-error-bind="' + key + '"]').append('<span class="control-label-error">' + val + '</span>');
                });
            }
            else
            {
                console.log(response);
                manager.addModal.modal('hide');
                $('#team_scouting_panel').find('.scouting-row').last().remove();

                $('#team_scouting_panel').prepend(response.row);

                $('#team_scouting_panel').effect("highlight", {}, 2000);

                $('html, body').animate({
                    scrollTop: $('#team_scouting_panel').offset().top
                }, 500, function () {

                });

            }

        });

    });
};



ScoutingManager.prototype.bindReply = function () {
    $(document).on('click', '.scouting-reply', function (e) {
        e.preventDefault();
        subject = $(this).parents('.scouting-row').find('.sr-cnt h3');
        text = $(this).parents('.scouting-row').find('.sr-cnt p');
        action = $(this).attr('href');
        manager.replyModal.find('form').attr('action',action);
        
        $('#reply_subject').val('RE:'+subject.text());
        $('#reply-scouting').html(text.text());
        manager.replyModal.modal('show');
    });
    
    $('.scouting-reply-form').on('submit', function (e) {
        e.preventDefault();
        
        var form = $(this);
        var data = form.serialize();
        var action = form.attr('action');
        
        manager.replyModal.find('.modal-loader').show();
        


        $.ajax({
            method: "POST",
            url: action,
            data: data,
            dataType: 'json'
        }).done(function (response) {
            form.find('.control-label-error').remove();
            manager.replyModal.find('.modal-loader').hide();
            if (response.result == 'ERROR')
            {
                $.each(response.errors, function (key, val) {
                    form.find('[data-error-bind="' + key + '"]').append('<span class="control-label-error">' + val + '</span>');
                });
            }
            else
            {
                manager.replyModal.find('form').hide();
                manager.replyModal.find('.send-success').fadeIn();
                //manager.replyModal.modal('hide');
            }

        });

    });
    
};


ScoutingManager.prototype.bindTriggers = function () {

    manager = this;
    manager.bindAddScouting();
    manager.bindSendForm();
    manager.bindReply();

    $(document).on('mouseenter', '.sr-sidebar', function () {
        $(this).parent().find('.sr-sidebar-cnt').show();
    });

    $(document).on('mouseleave', '.sr-sidebar', function () {
        $(this).parent().find('.sr-sidebar-cnt').hide();
    });


    $('.remove-scouting-trigger').on('click', function (e) {
        e.preventDefault();
        $('#removeScoutingModal .modal_submit').attr('data-href', $(this).attr('href'));
        $('#removeScoutingModal').modal('show');

    });

    $('#removeScoutingModal .modal_submit').on('click', function () {
        location.href = $(this).attr('data-href');
    });


    $('.player-scouting-form-trigger').on('click', function (e) {
        e.preventDefault();
        var form_wrap = $('.scouting-form-wrap3');
        form_wrap.slideDown();
    });


    $('#scouting-form-choice').on('change', function () {
        $('.scouting-form-wrap').hide();


        if ('' != $('#scouting-form-choice').val())
        {
            var form_wrap = $('.scouting-form-wrap' + $('#scouting-form-choice').val());
            form_wrap.slideDown();
        }

    });

    

};
*/


