module.exports =
{
	publicPath: './Sources/',
	entry: "./Sources/index.js",
	output:
	{
		path: __dirname + "/bundle/",
		filename: "/bundle.js"
	},
	module:
	{
		loaders: [
			{ test: /\.js$/, exclude: /node_modules/, loader: "babel-loader"}
		]
	}
}