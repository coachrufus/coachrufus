import { combineReducers, createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { playersComparsion } from '../Reducers/PlayersComparsion.js';

export default createStore(
    playersComparsion,
    applyMiddleware(thunk)
);