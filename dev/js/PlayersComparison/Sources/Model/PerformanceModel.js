import Immutable from 'immutable'
import { lang } from '../App/Langs.js';

function getTestChartModel()
{
    let d1 = [];
    for (let x = 0; x <= 10; x++)
    {
        d1.push([x, parseInt(Math.random() * 30)]);
    }
    let d2 = [];
    for (let y = 0; y <= 10; y++)
    {
        d2.push([y, parseInt(Math.random() * 30)]);
    }
    return [
        { data: d1, label: "Average", color: "#00b3be", axisLabelPadding: 0, shadowSize: 0 },
        { data: d2, label: "Personal", color: "#ff6d22", axisLabelPadding: 0, shadowSize: 0 }
    ];
}

function toCounterData(PerformanceData)
{
    let fields = [[lang.GOALS, 'goals'], [lang.ASSIST, 'assists'], [lang.WINS, 'wins'], [lang.DRAW, 'draws']];
    
    return fields.map(([caption, id]) => {
        return {
            caption,
            direction: PerformanceData.upDowns[id],
            value: PerformanceData.last[id],
            id 
        }
    })
}

function toChartModel(PerformanceData, player1, player2, field)
{
    const
        { chartData } = PerformanceData,
        { teamPlayers, max } = chartData;
    const
        player1Data = teamPlayers[player1].map((v, k) => [k, v[field]]),
        player2Data = teamPlayers[player2].map((v, k) => [k, v[field]]);
    return [
        { data: player1Data, label: "Player1", color: "#ff6d22", axisLabelPadding: 0, shadowSize: 0 },
        { data: player2Data, label: "Player2", color: "#00b3be", axisLabelPadding: 0, shadowSize: 0 }
    ]
}

function getData(data, player1, player2)
{
    if(data.total.length == 0)
    {
         return {
            ChartModels: null,
            Counters: toCounterData(data),
            UserStats: data.players,
            player1,
            player2
        }
    }
    else
    {
        return {
            ChartModels: {
                form: toChartModel(data, player1, player2, 'efficiency'),
                CRS: toChartModel(data, player1, player2, 'CRS'),
                goals: toChartModel(data, player1, player2, 'goals'),
                assists: toChartModel(data, player1, player2, 'assists')
            },
            Counters: toCounterData(data),
            UserStats: data.players,
            player1,
            player2,
            seasonName: data.seasonName,
            teamId: data.teamId
        }
    }
    
   
}

function toPlayerList(response)
{
    var playerList = response.players;
    return playerList.map(a => ({ name: a[1], id: a[0]}));
}

const PlayerList = toPlayerList($.ajax({
    type: "GET",
    url: '/widget/api/team-players',
    async: false,
    dataType: 'json'
}).responseJSON);


export function loadData(player1, player2)
{
   
    return getData($.ajax({
        type: "POST",
        url: '/widget/api/season',
        data: { players: [ player1, player2 ] },
        async: false,
        dataType: 'JSON'
    }).responseJSON, player1, player2);
}

export function loadDataAsync(player1, player2, success)
{
    return $.ajax({
        type: "POST",
        url: '/widget/api/season',
        data: { players: [ player1, player2 ] },
        async: true,
        success: (result) => { success(getData(result, player1, player2)) },
        dataType: 'JSON'
    });
}

const player1 = PlayerList[0].id, player2 = PlayerList[1].id;

export default Object.assign({
    ChartType: "form",
    PlayerList,
    UserStatsColumns: [
        ['name', lang.MEMBER],
        ['CRS', 'CRS'],
        ['goals', lang.GOALS],
        ['assists', lang.ASSIST],
        ['wins', lang.WINS],
        ['draws', lang.DRAW]
    ],
    freeMode: window.PgKey !== "PRO",
    orderLink: `/orders/${window.activeTeam.id}`
}, loadData(player1, player2));