import { combineReducers, createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { performance } from '../Reducers/Performance.js';

export default createStore(
    performance,
    applyMiddleware(thunk)
);