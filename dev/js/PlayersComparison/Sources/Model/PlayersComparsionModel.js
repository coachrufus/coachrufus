import Immutable from 'immutable'
import { lang } from '../App/Langs.js';
function toPlayerList(response)
{
    var playerList = response.players;
    
   
    
    return { playerList: playerList.map(a => ({ name: a[1], id: a[0]})), seasonName: response.seasonName };
}

const dateResult = toPlayerList($.ajax({
    type: "GET",
    url: '/widget/api/team-players',
    async: false,
    dataType: 'json'
}).responseJSON);

const PlayerList = dateResult.playerList;
const seasonName = dateResult.seasonName;

const Fields = ['CRS', 'A', 'D', 'GP', 'W', 'G'];
const FieldLabels = ['CRS', lang.ASSIST, lang.DRAW, lang.MATCHES, lang.WINS, lang.GOALS];

export default
{
    Interval: 'season',
    Fields,
    Labels: FieldLabels.map(f => ({"label": f})),
    Page: 'Chart',
    PlayerList,
    seasonName: seasonName,
    Versus: PlayerList.slice(0, 2).reverse(),
    freeMode: window.PgKey !== "PRO",
    orderLink: `/orders/${window.activeTeam.id}`
}