import React from 'react'
import ReactDOM from 'react-dom'
import Immutable from 'immutable'
import { Provider, connect } from 'react-redux'

import PerformanceStore from '../Model/PerformanceStore'
import { loadDataAsync } from '../Model/PerformanceModel'
import { PerformanceBox } from '../Containers/PerformanceBox'

export default function(container, handleShareClick)
{
    function changeChartType(chartType)
    {
        return PerformanceStore.dispatch({
            type: "CHANGE_CHART_TYPE",
            chartType
        });
    }

    function viewAllStats()
    {
         return  '/widget/analytics/list?team_id=';
    }

    function updatePlayer(player1, player2)
    {
        loadDataAsync(player1, player2, data => PerformanceStore.dispatch({
            type: "UPDATE_PLAYER",
            data
        }));
    }

    const Mappers = 
    {
        mapStateToProps(state)
        {
            return { ...state };
        },

        mapDispatchToProps()
        {
            return {
                share: handleShareClick,
                changeChartType,
                viewAllStats,
                updatePlayer
            }
        }
    }

    const PerformanceBoxConnected = connect(
        Mappers.mapStateToProps,
        Mappers.mapDispatchToProps,
        null, { pure: false }
    )(PerformanceBox);

    ReactDOM.render(
        <Provider store={PerformanceStore}>
            <PerformanceBoxConnected />
        </Provider>,
        container
    );
}