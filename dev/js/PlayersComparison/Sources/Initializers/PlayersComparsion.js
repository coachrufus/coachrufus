import React from 'react'
import ReactDOM from 'react-dom'
import Immutable from 'immutable'
import { Provider, connect } from 'react-redux'

import PlayersComparsionStore from '../Model/PlayersComparsionStore'
import { PlayersComparsionBox } from '../Containers/PlayersComparsionBox'

export default function(container, handleShareClick)
{
    function getChartModel(state, model)
    {
        const result = [
            {
                label: "Max",
                color: "rgba(0, 0, 0, 0)",
                data: state.Fields.map((f, i) => [i, model["max"][f]]),
                spider: {show: false, fill: false}
            },
            {
                label: "Avg",
                color: "rgba(255, 109, 34, 0.65)",
                data: state.Fields.map((f, i) => [i, model["player2"][f]]),
                spider: {show: false, fill: true}
            },
            {
                label: "Player",
                color: "rgba(0, 179, 190, 0.65)",
                data: state.Fields.map((f, i) => [i, model["player1"][f]]),
                spider: {show: false, fill: true}
            }
        ];
        return result;
    }

    function loadChartData(state, interval, playerIds, success, error)
    {
        const players = Immutable.Map(playerIds.map((id, i) => [`player${i + 1}`, id])).toJS();
        $.ajax({
            type: "GET",
            url: '/widget/api/players-comparsion',
            data: {
                interval,
                ...players
            },
            success: m => { /* console.log(m); */ success(getChartModel(state, m)) },
            error,
            dataType: "JSON"
        });
    }

    function updateChartModel()
    {
        PlayersComparsionStore.dispatch((dispatch, getState) =>
        {
            const state = getState();
            loadChartData(state, state.Interval, state.Versus.map(p => p.id), chartModel =>
            {
                PlayersComparsionStore.dispatch({
                    type: "UPDATE_CHART_MODEL", chartModel
                });
            });
        });
    }

    function changeInterval(interval)
    {
        PlayersComparsionStore.dispatch({ type: "CHANGE_INTERVAL", interval });
        updateChartModel();
    }

    function changePage(page)
    {
        PlayersComparsionStore.dispatch({ type: "CHANGE_PAGE", page });
    }

    function changePlayer(index, item)
    {
        const state = PlayersComparsionStore.getState();
        const versus = Immutable.List(state.Versus).set(index, item).toJS();
        PlayersComparsionStore.dispatch({ type: "CHANGE_PLAYER", index, versus });
        if (index === 1) updateChartModel();
    }

    const Mappers = 
    {
        mapStateToProps(state)
        {
            return { ...state };
        },

        mapDispatchToProps()
        {
            return {
                changeInterval,
                changePage,
                changePlayer,
                share: handleShareClick
            }
        }
    }

    const PlayersComparsionBoxConnected = connect(
        Mappers.mapStateToProps,
        Mappers.mapDispatchToProps,
        null, { pure: false }
    )(PlayersComparsionBox);

    updateChartModel();

    ReactDOM.render(
        <Provider store={PlayersComparsionStore}>
            <PlayersComparsionBoxConnected />
        </Provider>, container
    );
}