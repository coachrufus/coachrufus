import React from 'react'
import { lang } from '../App/Langs.js';
export class BoxHeader extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {};
    }

    render()
    {

        return (
            <div className="BoxHeader">
                <img className="icon chart" src={this.props.icon} />
                <h1>{lang[this.props.title]} {this.props.seasonName}</h1>
                <a onClick={this.props.onShareClick} className="icon social" href="javascript:void(0)"><img src={this.props.shareIcon} /></a>
            </div>
        )
    }
}