import React from 'react'

export class PlayerList extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = { index: 0, selected: null };
    }

    handleClick(item, k)
    {
        if (this.props.onChange) this.props.onChange(this.state.index, item);
        const index = this.state.index === 0 ? 1 : 0;
        this.setState({ index, selected: k });
    }

    render()
    {
        const classes = `PlayerList ${this.state.index === 0 ? 'first' : 'second'}`;
        return (
            <div className={classes}>
                {this.props.items.map((i, k) =>
                    <div key={k}>
                        <a className={this.state.selected === k ? 'selected' : undefined} onClick={this.handleClick.bind(this, i, k)} href="javascript:void(0)">{i.name}</a>
                    </div>)}
            </div>
        )
    }
}