import React from 'react'
import { lang } from '../App/Langs.js';

export class UserImage extends React.Component
{
    constructor(props)
    {
        super(props);
    }

    get style()
    {
        return {
            backgroundImage: this.props.source ?
                `url('${this.props.source}')` : undefined
        };
    }

    render()
    {
        return (
            <div className="UserImage">
                <div className="Image" style={this.style} />
            </div>
        )
    }
}