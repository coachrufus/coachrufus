import React from 'react'
import { FlotChart } from './FlotChart.js'

function round(value, precision)
{
    let multiplier = Math.pow(10, precision || 0);
    return Math.round(parseFloat(value) * multiplier) / multiplier;
}

export class CurvedLineChart extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {};
        this.options =
        {
            series: {
                lines: {
                    show: true,
                    fill: 0.2,
                    lineWidth: 1
                },
                curvedLines: {
                    active: true,
                    apply: true
                }
            },
            tooltip: {
                show: true,
                content: function(label, x, y, item)
                {
                    let value = round(y, 1);
                    let { color } = item.series;
                    return `<div class="tooltip tooltip-label" style="opacity: 1; background: ${color};">
                                ${value}
                                <div class="arrow" style='border-color: ${color} transparent transparent transparent'></div>
                            </div>`;
                },
                shifts: {
                    x: -12,
                    y: -41
                },
                defaultTheme: false
            },
            xaxis:
            {
                tickColor: '#fff',
                color: "#fff",
                min: 1,
                tickDecimals: 0
            },
            yaxis:
            {
                tickColor: '#f5f5f5',
                color: "#fff",
                min: 0
            },
            grid:
            { 
                noSpacing: true,
                hoverable: true,
                clickable: true,
                editable: true,
                tickColor: "#ffffff",
                color: '#d5d5d5',
                borderWidth:
                {
                    top: 0,
                    right: 0,
                    bottom: 1,
                    left: 0
                }
            },
            legend:
            {
                show: false
            }
        }
    }

    get width() { return '100%'; }
    get height() { return 180; }

    renderFlotChart()
    {
        return (
            <div>
                <FlotChart
                    fixCanvasSize={false}
                    isEnabled={this.props.isEnabled}
                    width={this.width} height={this.height}
                    model={this.props.chartModel} options={this.options} />
            </div>
        )
    }

    render()
    {
        return (
            <div className="CurvedLineChart" style={{width: this.width, height: this.height}}>
                {this.props.chartModel ? this.renderFlotChart() : undefined}
            </div>
        )
    }
}