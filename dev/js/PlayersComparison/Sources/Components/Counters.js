import React from 'react'
import { lang } from '../App/Langs.js';
import { ProLabel } from './ProLabel.js';

class Counter extends React.Component
{
    constructor(props)
    {
        super(props);
    }

    render()
    {
        const i = this.props.item;
        return (
            <article className="Counter">
                <div className={`chart ${i.id}`} />
                <div className="value">{i.value}</div>
                <div className="caption">{i.caption}<span className={`direction ${i.direction}`} /></div>
            </article>
        )
    }
}

export class Counters extends React.Component
{
    constructor(props)
    {
        super(props);
    }

    render()
    {
        return (
            <section className="Counters clearfix">
                {this.props.items.map((i, k) =>
                    <Counter item={i} key={k} />)}
                <a className="AllStats"
                   href="javascript:void(0)"
                   onClick={this.props.onAllStatsClick}>{lang.ALL_STATS}{this.props.freeMode ? <ProLabel /> : undefined}</a>
            </section>
        )
    }
}