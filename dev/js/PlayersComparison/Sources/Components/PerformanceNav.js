import React from 'react'
import { lang } from '../App/Langs.js';

export class PerformanceNav extends React.Component
{
    constructor(props)
    {
        super(props);
        this.items = [
            { caption: lang.ACTUAL_FORM, value: 'form' },
            { caption: 'CRS', value: 'CRS' },
            { caption: lang.GOALS, value: 'goals' },
            { caption: lang.ASSIST, value: 'assists' }
        ];
    }

    handleClick(value, e)
    {
        if (this.props.onChange)
        {
            e.target.value = value;
            this.props.onChange(e);
        }
    }

    render()
    {
        return (
            <nav className="PerformanceNav">
                {this.items.map((i, k) =>
                    <a key={k} href="javascript:void(0)"
                       className={i.value === this.props.value ? 'active' : undefined}
                       onClick={this.handleClick.bind(this, i.value)}>{i.caption}</a>)}
            </nav>
        )
    }
}