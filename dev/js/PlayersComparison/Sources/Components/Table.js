import React from 'react'
import { ComparerBuilder } from '../Core/ComparerBuilder'
import { lang } from '../App/Langs.js';
import { UserImage } from './UserImage.js';
import { ProLabel } from './ProLabel.js';

export class Table extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            sortColumn: 'CRS',
            sortDirection: 'desc',
            isMinimized: true
        };
        this.comparerBuilder = new ComparerBuilder();
    }

    handleChangeSort(sortColumn, e)
    {
        if (sortColumn === this.state.sortColumn)
        {
            this.setState({
                sortDirection: this.state.sortDirection === "asc" ? "desc" : "asc"
            });
        }
        else
        {
            this.setState({
                sortDirection: sortColumn === "name" ? "asc" : "desc",
                sortColumn
            });
        }
    }
    
     showCreditModal()
    {
        $('#team-credit-modal').modal('show'); 
    }

    renderSortIcon(sortDirection)
    {
        return (
            <i className={`glyphicon glyphicon-sort-by-attributes${sortDirection === "asc" ? "" : "-alt" }`} />
        )
    }

    handleMoreClick(isMinimized)
    {
        this.setState({ isMinimized });
    }

    renderHeaderColumn(id, name)
    {
        const { sortColumn, sortDirection } = this.state;
        return (
            <th key={id} onClick={this.handleChangeSort.bind(this, id)} className={`sort ${id}`}>
                {name}{sortColumn === id ? this.renderSortIcon(sortDirection) : <i className="glyphicon glyphicon-sort" />}
            </th>
        );
    }

    renderFreeRow(freeMode, i, k, coef)
    {
        const { columns } = this.props;
        const freeColumns = columns.slice(0, 2);
        return (
            <tr key={k} className={i.isActive ? 'active' : undefined}>
                <td className="img">
                    <UserImage source={i.avatar} />
                </td>
                <td key={columns[0][0]} className={columns[0][0]}>{i[columns[0][0]]}</td>
                <td key={columns[1][0]} className={columns[1][0]}><div className="CRSBar" style={{ width: `${parseFloat(i[columns[1][0]]) * coef}%` }} /></td>
            </tr>
        )
    }

    renderRow(freeMode, i, k)
    {
        const { columns } = this.props;
        return (
            <tr key={k} className={i.isActive ? 'active' : undefined}>
                <td className="img">
                    <UserImage source={i.avatar} />
                </td>
                {columns.map(([id]) => <td key={id} className={id}>{i[id]}</td>)}
            </tr>
        )
    }

    render()
    {
        const
            { sortColumn, sortDirection } = this.state,
            { columns, items, freeMode } = this.props;
        const maxCRS = items.length === 0 ? 0 : Math.max.apply(Math, items.map(o => o.CRS));
        const coef = maxCRS === 0 ? 0 : (90 / maxCRS);
        return (
            <div>
                <div className={this.state.isMinimized && items.length > 10 ? 'minimized-table' : undefined}>
                    <table ref="table" className="Table table">
                        <thead>
                            {freeMode ? <tr>
                                <th className="img"></th>
                                {this.renderHeaderColumn(columns[0][0], columns[0][1])}
                            <th colSpan={5} className="IndividualPro"><a href="#" onClick={freeMode ? (event => { this.showCreditModal(); event.preventDefault() }) : event.preventDefault()} > {lang.SHOW_INDIVIDUAL} <ProLabel /></a></th>
                            </tr> : <tr>
                                <th className="img"></th>
                                {columns.map(([id, name]) => this.renderHeaderColumn(id, name))}
                            </tr>}
                        </thead>
                        <tbody>
                        {items.sort((a, b) => this.comparerBuilder.getComparer(sortColumn, sortDirection, a, b)).map((i, k) => this[freeMode ? 'renderFreeRow' : 'renderRow'](freeMode, i, k, coef))}
                        </tbody>
                    </table>
                </div>
                {items.length > 10 ? <a onClick={this.handleMoreClick.bind(this, !this.state.isMinimized)} className="More" href="javascript:void(0)">
                    {this.state.isMinimized ?
                        <span>{lang.MORE} <i className="fa fa-angle-double-down" aria-hidden="true" /></span> :
                        <span>{lang.LESS}<i className="fa fa-angle-double-up" aria-hidden="true" /></span>}
                </a> : undefined}
            </div>
        )
    }
}