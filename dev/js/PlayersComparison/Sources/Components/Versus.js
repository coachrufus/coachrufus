import React from 'react'
import { ProLabel } from './ProLabel.js';

export class Versus extends React.Component
{
    constructor(props)
    {
        super(props);
    }
    
     
    showCreditModal()
    {
        $('#team-credit-modal').modal('show'); 
    }

    render()
    {
        const { orderLink, freeMode } = this.props;
        return (
            <nav className="Versus">
                <a onClick={freeMode ? (() => this.showCreditModal()) : this.props.onClick} href="javascript:void(0)">{this.props.items[0].name}</a>
                <span>vs</span>
                <a onClick={freeMode ? (() =>  this.showCreditModal()) : this.props.onClick} href="javascript:void(0)">{this.props.items[1].name}</a>
                {freeMode ? <ProLabel /> : undefined}
            </nav>
        )
    }
}