import React from 'react'
import { FlotChart } from './FlotChart.js'

export class PlayersComparsionChart extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {};
        this.options =
        {
            series:
            { 
                editMode: 'v',
                editable: true,
                spider:
                {
                    active: true,
                    highlight: { mode: "area" },
                    lineWidth: 1,
                    lineStyle: "rgba(255, 255, 255, 0.5)",
                    legs:
                    { 
                        font: "12px 'Montserrat', sans-serif",
                        fillStyle: "rgba(255,255,255, 0.0)",
                        legStartAngle: 0,
                        data: props.labels,
                        legScaleMax: 1,
                        legScaleMin: 0,
                        legStartAngle: 270
                    },
                    spiderSize: 0.70,
                    lineWidth: 1,
                    pointSize: 0,
                    connection:
                    {
                        width: 1
                    }
                }
            },
            grid:
            { 
                hoverable: false,
                clickable: true,
                editable: true,
                tickColor: "#ffffff",
                mode: "radar",
                show: false
            },
            legend:
            {
                show: false
            }
        };
        this.positions = [
            {top: 3, left: 142},
            {top: 73, right: 15},
            {bottom: 73, right: 15},
            {bottom: 3, left: 132},
            {bottom: 73, left: 15},
            {top: 73, left: 25}
        ];
        this.pointPositions = [
            {top: 40, left: 148, borderColor: "#FF6D22"},
            {top: 94, right: 55, borderColor: "#00759A"},
            {bottom: 94, right: 55, borderColor: "#A78C46"},
            {bottom: 40, left: 148, borderColor: "#925EBF"},
            {bottom: 94, left: 55, borderColor: "#A7C346"},
            {top: 94, left: 55, borderColor: "#00B3BE"}
        ];
    }

    get size()
    {
        return 310;
    }

    renderLabels(labels)
    {
        return labels.map((l, k) => 
        {
            return k == 2 || k == 3 || k == 4 ?
                <div style={this.positions[k]} key={k} className="radarLabel"><strong>{l[1]}</strong><div>{l[0]}</div></div> :
                <div style={this.positions[k]} key={k} className="radarLabel"><div>{l[0]}</div><strong>{l[1]}</strong></div>
        });
    }

    renderPoints(pointPositions)
    {
        return this.pointPositions.map((style, k) => 
        {
            return (
                <div style={style} key={k} className="ChartPoint" />
            )
        });
    }

    handleRenderHovers(coords)
    {
        let hovers = 
            coords
                .map(i =>
                    i
                    .filter(c => c.x === c.x && c.y === c.y)
                    .map(c => `<a href="javascript:void(0)" class="ChartHover" style="top: ${c.y}px; left: ${c.x}px" title='${c.value}' />`)
                    .join(''))
                .join('');
        var container = $(this.refs.hovers);
        container.html(hovers);
        container.find('a.ChartHover').tooltip('destroy').tooltip();
    }

    renderFlotChart(labels)
    {
        return (
            <div>
                <FlotChart
                    onRenderHovers={this.handleRenderHovers.bind(this)}
                    fixCanvasSize={true}
                    isEnabled={this.props.isEnabled}
                    width={this.size} height={this.size}
                    model={this.props.chartModel} options={this.options} />
                {labels ? this.renderLabels(labels) : undefined}
                {this.renderPoints()}
            </div>
        )
    }

    render()
    {
        var labels = null;
        if (this.props.chartModel && this.props.chartModel[0])
        {
            const { data } = this.props.chartModel[0];
            labels = data.map(([i, value]) => [this.props.labels[i].label, value]);
        }
        return (
            <div className="RadarChart" style={{width: this.size, height: this.size}}>
                {this.props.chartModel ? this.renderFlotChart(labels) : undefined}
                <div ref="hovers" />
            </div>
        )
    }
}