import React from 'react'
import { ProLabel } from './ProLabel.js';

export class Switch extends React.Component
{
    constructor(props)
    {
        super(props);
    }
    
    showCreditModal()
    {
        $('#team-credit-modal').modal('show'); 
    }

    renderItem(i, k)
    {
        const classes = i[1] == this.props.value ? "active" : undefined;
        const { orderLink, freeMode } = this.props;
        return (
            <a className={classes} onClick={freeMode && k === 1 ? (() => this.showCreditModal()) : this.handleClick.bind(this, i[1])} href="javascript:void(0)" key={k}>{i[0]}</a>
        );
    }

    handleClick(value)
    {
        if (this.props.onChange) this.props.onChange(value);
    }

    render()
    {
        const { freeMode } = this.props;
        return (
            <nav className="Switch">
                {this.props.items.map((i, k) => this.renderItem(i, k))}
                {freeMode ? <ProLabel /> : undefined}
            </nav>
        )
    }
}