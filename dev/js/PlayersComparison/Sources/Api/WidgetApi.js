export class WidgetApi
{   
    uploadImage(data)
    {
        return new Promise(function(resolve, reject)
        {            
            $.ajax(
            {
                url: `/widget/api/upload-image`,
                method: 'POST',
                data,
                success: resolve,
                error: reject
            });
        });
    }
}