import React from 'react'
import Immutable from 'immutable'
import { BoxHeader } from '../Components/BoxHeader'
import { CurvedLineChart } from '../Components/CurvedLineChart'
import { PerformanceNav } from '../Components/PerformanceNav'
import { Counters } from '../Components/Counters'
import { Table } from '../Components/Table'
import { lang } from '../App/Langs.js';
import { ProLabel } from '../Components/ProLabel.js';

export class PerformanceBox extends React.Component
{
    constructor(props)
    {
        super(props);
        this.faded = false;
    }

    handleResize(e)
    {
        this.forceUpdate();
    }
    
    componentDidMount()
    {
        window.addEventListener('resize', this.handleResize.bind(this));
    }

    componentWillUnmount()
    {
        window.removeEventListener('resize', this.handleResize.bind(this));
    }

    fadeIn()
    {
        if (!this.faded)
        {
            $(this.refs.fadeOut).animate({
                opacity: '1'
            }, 300);
            this.faded = true;
        }
    }

    componentDidMount()
    {
        this.fadeIn();
    }

    componentDidUpdate()
    {
        this.fadeIn();
    }

    handleShareClick()
    {
        const box = this.refs.PerformanceBox;
        if (this.props.share) this.props.share(box);
    }

    handleChangeChartType(e)
    {
        this.props.changeChartType(e.target.value);
    }

    handleSelectPlayer(prop, id)
    {
        switch (prop)
        {
            case "player1":
            {
                this.props.updatePlayer(id, this.props.player2);
                return;
            }
            case "player2":
            {
                this.props.updatePlayer(this.props.player1, id);
                return;
            }
        }
    }
    
    showCreditModal()
    {
        $('#team-credit-modal').modal('show'); 
    }
    

    getTpName(tp)
    {
        return tp.id === "avg" ? lang.TEAM_AVG : tp.name;
    }

    renderPlayerDropdown(prop)
    {
        const actualTp = this.props.PlayerList.find(tp => tp.id === this.props[prop]);
        return (
            <div className={`dropdown PlayerDropdown ${prop}`}>
                <button onClick={this.props.freeMode ? (event => { this.showCreditModal(); event.preventDefault() }) : undefined} className={this.props.freeMode ? "btn btn-dark" : "btn btn-dark dropdown-toggle"} type="button" id={prop} data-toggle={this.props.freeMode ? undefined : "dropdown"} aria-haspopup="true" aria-expanded="true">
                    <span className="button-text">{this.getTpName(actualTp)}</span> <span className="caret" />
                </button>
                <ul className="dropdown-menu" aria-labelledby={`#${prop}`}>
                    {this.props.freeMode ? undefined : this.props.PlayerList.map((tp, k) => <li key={k}><a onClick={this.handleSelectPlayer.bind(this, prop, tp.id)} href="javascript:void(0)">{this.getTpName(tp)}</a></li>)}
                </ul>
            </div>
        )
    }

    getBoxProClass(prop)
    {
        if(prop.freeMode)
        {
            return 'PerformanceBox panel panel-default free-mode';
        }
        else
        {
             return 'PerformanceBox panel panel-default';
        }
    }
    
    onAllStatsClick(prop)
    {
        return function(){
             window.location = '/widget/analytics/list?team_id='+prop.teamId;
        }
    }
    
   

    render()
    {
        if(null == this.props.ChartModels)
        {
             return (
                <section ref="PerformanceBox" className="PerformanceBox panel panel-default">
         <div className="fadeOut" ref="fadeOut">
                <BoxHeader
                        icon="/img/Performance/Chart.png"
                        title="TITLE_PERFORMANCE"
                        shareIcon="/img/Performance/Social.png"
                        onShareClick={this.handleShareClick.bind(this)} />
              

                 <div className="row">
                        <div className="col-sm-12">
                            <img src="/img/main/logo-sq.svg" />
                        </div>
                        <div className="col-sm-4 col-md-offset-4 text-center">
                            {lang.NO_DATA_PART1}
                        </div>
                        
                    </div>
                <br />
                
                
                </div>
                </section>
                );
        }
        else
        {
            const chartModel = this.props.ChartModels[this.props.ChartType];
            var seasonName = this.props.seasonName;
            const onAllStatsClick = this.onAllStatsClick(this.props);
            return (
                    <section ref="PerformanceBox" className={ this.getBoxProClass(this.props)}  >

                <div className="fadeOut" ref="fadeOut">
                    <BoxHeader
                        icon="/img/Performance/Chart.png"
                        title="TITLE_PERFORMANCE"
                        shareIcon="/img/Performance/Social.png"
                        onShareClick={this.handleShareClick.bind(this)}
                        seasonName={seasonName}/>
                    <section className="Rules">
                        <ul className="Legend">
                            <li>
                                <span className="color Personal" />
                                {this.renderPlayerDropdown('player2')}
                            </li>
                            <li className="middle">
                                <span className="player2-line" />
                                <span className="circle" />
                                <span className="player1-line" />
                            </li>
                            <li className="right">
                                <span className="color Average" />
                                {this.renderPlayerDropdown('player1')}
                                {this.props.freeMode ? <ProLabel /> : undefined}
                            </li>
                        </ul>
                        <PerformanceNav value={this.props.ChartType} onChange={this.handleChangeChartType.bind(this)} />
                    </section>
                    <CurvedLineChart chartModel={chartModel} />
                    <Counters freeMode={this.props.freeMode} orderLink={this.props.orderLink} items={this.props.Counters} onAllStatsClick={this.props.freeMode ? (event => { this.showCreditModal(); event.preventDefault() }) : onAllStatsClick } />
                    <Table freeMode={this.props.freeMode} orderLink={this.props.orderLink} columns={this.props.UserStatsColumns} items={this.props.UserStats} />
                </div>
			</section>
		)
        }
        
        
        

		
    }
}