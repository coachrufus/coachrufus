import React from 'react'
import { BoxHeader } from '../Components/BoxHeader'
import { PlayersComparsionChart } from '../Components/PlayersComparsionChart'
import { PlayerList } from '../Components/PlayerList'
import { Switch } from '../Components/Switch'
import { Versus } from '../Components/Versus'
import { lang } from '../App/Langs.js';

export class PlayersComparsionBox extends React.Component
{
    constructor(props)
    {
        super(props);
        this.faded = false;
    }

    handleResize(e)
    {
        this.forceUpdate();
    }
    
    componentDidMount()
    {
        window.addEventListener('resize', this.handleResize.bind(this));
    }

    componentWillUnmount()
    {
        window.removeEventListener('resize', this.handleResize.bind(this));
    }

    renderPage(page)
    {
        switch (page)
        {
            case 'PlayerList': return (
                <PlayerList
                    items={this.props.PlayerList}
                    index={this.props.PlayerListSelectionIndex}
                    onChange={this.props.changePlayer} />
            )
            default: return <PlayersComparsionChart labels={this.props.Labels} chartModel={this.props.ChartModel} />
        }
    }

    handleVersusClick()
    {
        if (this.props.changePage) this.props.changePage('PlayerList');
    }

    fadeIn()
    {
        if (this.props.ChartModel && !this.faded)
        {
            $(this.refs.fadeOut).animate({
                opacity: '1'
            }, 300);
            this.faded = true;
        }
    }

    componentDidMount()
    {
        this.fadeIn();
    }

    componentDidUpdate()
    {
        this.fadeIn();
    }

    handleShareClick()
    {
        const box = this.refs.PlayersComparsionBox;
        if (this.props.share) this.props.share(box);
    }

    render()
    {
            var seasonName = this.props.seasonName;
            return (
			<section ref="PlayersComparsionBox" className="PlayersComparsionBox panel panel-default">
                <div className="fadeOut" ref="fadeOut">
                    <BoxHeader
                        icon="/img/PlayersComparsion/Chart.png"
                        title="TITLE_COMPARISION"
                        shareIcon="/img/PlayersComparsion/Social.png"
                        seasonName={seasonName}
                        onShareClick={this.handleShareClick.bind(this)} />
                    <Switch
                        items={[[lang.SEASON, 'season'], [lang.MONTH, 'month']]}
                        value={this.props.Interval}
                        onChange={this.props.changeInterval} freeMode={this.props.freeMode} orderLink={this.props.orderLink} />
                    <section className="Pages">
                        {this.renderPage(this.props.Page)}
                    </section>
                    <Versus items={this.props.Versus} onClick={this.handleVersusClick.bind(this)} freeMode={this.props.freeMode} orderLink={this.props.orderLink} />
                    <footer>
                        <a onClick={this.handleShareClick.bind(this)} className="share" href="javascript:void(0)">{lang.SHARE_PERSONAL_PERFORMANCE}</a>
                    </footer>
                </div>
			</section>
		)
    }
}