import resources from './App/Resources'
import createShareClickHandler from './EventHandlers/ShareOnFacebook'
const handleShareClick = createShareClickHandler(resources);

const PerformanceContainer = document.getElementById("Performance");
if (PerformanceContainer)
{
    const PerformanceInit = require('./Initializers/Performance').default;
    PerformanceInit(PerformanceContainer, handleShareClick);
}
const PlayersComparisonContainer = document.getElementById("PlayersComparison");
if (PlayersComparisonContainer)
{
    const PlayersComparisonInit = require('./Initializers/PlayersComparsion').default;
    PlayersComparisonInit(PlayersComparisonContainer, handleShareClick);
}