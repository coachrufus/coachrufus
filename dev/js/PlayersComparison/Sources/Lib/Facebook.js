function removeProtocolFromLink(link)
{
  const
    delimiter = '://',
    index = link.indexOf(delimiter);
  return index === -1 ? link : link.substring(index + delimiter.length);
}

function getFacebookShareUrl(url)
{
    return `https://www.facebook.com/sharer/sharer.php?u=${removeProtocolFromLink(url)}`;
}

export function shareOnFacebook(guid,fb_id,root,desc)
{
    //window.location = getFacebookShareUrl(url);
    FB.ui({
        app_id: fb_id,
        method: 'feed',
        display: 'popup',
        link: root+'/img/share/'+guid+'.png',
        hashtag: '#coachrufus',
        picture: root+'/img/share/'+guid+'.png',
        caption: 'https://app.coachrufus.com',
        name: desc
    }, function(response){});
    
    console.log( root+'/img/share/'+guid+'.png');
    /*
    
        FB.ui({
             method: 'share',
             display: 'popup',
             href: url,
             hashtag: '#coachrufus'
           }, function(response){
                   
           });
           */
}

export function showLoader()
{
     $('#share-loader').show();
}
export function hideLoader()
{
     $('#share-loader').hide();
}