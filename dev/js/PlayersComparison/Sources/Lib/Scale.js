import Immutable from 'immutable'
import { shuffle } from './Shuffle.js'

function* scale({ from = 0, max, count, mapper })
{
    const
        c = parseFloat(count) + from,
        step = parseFloat(max) / (c - 1.0);
    for (var i = from; i < c; i++)
    {
        yield [i - from, mapper(step * i, i - from)];
    }
}

export function scaleMap(options)
{
    return Immutable.Map(scale(options));
}

export function shuffledScaleMap(options)
{
    const shuffled = scaleMap(options).toIndexedSeq().toJS()::shuffle();
    return Immutable.List(shuffled).toMap();
}