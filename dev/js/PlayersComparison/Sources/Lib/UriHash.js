function get()
{
    const hash = window.location.hash;
    return hash.startsWith('#') ? hash.substring(1) : hash;
}

function listen(action, defaultPage = true)
{
    window.addEventListener("hashchange", function()
    {
        action(get());
    });
    if (defaultPage) action(get());
}

export default { get, listen }