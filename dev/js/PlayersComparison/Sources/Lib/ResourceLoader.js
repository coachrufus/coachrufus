import Immutable from "immutable"

export class ResourceLoader
{
    constructor()
    {
        this._css = Immutable.Set();
        this._js = Immutable.Set();
    }
    
    get css() { return this._css; }
    get js() { return this._js; }
    
    registerLoadedCallback(element, afterLoad, loaded)
    {
        function complete()
        {
            afterLoad();
            if (loaded) loaded();
        }
        if(element.addEventListener)
        {
            element.addEventListener("load", complete, false);
        } 
        else if(element.readyState)
        {
            element.onreadystatechange = function()
            {
                if (this.readyState == 'complete') complete();
            };
        }
    }
    
    addCSS(path, loaded)
    {
        const
            body = document.getElementsByTagName("body")[0],
            element = document.createElement("link");
        element.setAttribute("rel", "stylesheet");
        element.setAttribute("type", "text/css");
        element.setAttribute("href", path);
        this.registerLoadedCallback(element,
            () => this._css = this.css.add(path), loaded);
        body.appendChild(element);
    }
    
    addCSSOnce(path, loaded)
    {
        if (!this.css.contains(path))
        {
            this.addCSS(path, loaded);
        }
        else if (loaded) loaded();
    }

    addJS(path, loaded)
    {
        const
            body = document.getElementsByTagName("body")[0],
            element = document.createElement("script");
        element.setAttribute("type", "text/javascript");
        element.setAttribute("src", path);
        this.registerLoadedCallback(element,
            () => this._js = this.js.add(path), loaded);
        body.appendChild(element);
    }
    
    addJSOnce(path, loaded)
    {
        if (!this.js.contains(path))
        {
            this.addJS(path, loaded);
        }
        else if (loaded) loaded();
    }
}

