export class State
{
    constructor(state)
    {
        this._state = state;
    }
    
    update(obj)
    {
        return Object.assign(this._state, obj);
    }
    
    toObject() { return this._state; }
}