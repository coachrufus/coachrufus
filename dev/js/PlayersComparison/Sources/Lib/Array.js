const Array =
{
    concatArrayOfArrays(arrayOfArrays)
    {
        return [].concat.apply([], arrayOfArrays);
    }
}

export default Array;