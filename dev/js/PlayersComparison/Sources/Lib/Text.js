export function nl2br(text)
{
    return (text + '').replace(/(\r\n|\n\r|\r|\n)/g, "<br/>");
}

export function br2nl(text)
{
	return text.replace(/<br\/>/g, "\r");
}

export function removeDivs(text)
{
    return text.replace(/<div>/gi, "").replace(/<\/div>/gi, "");
}

export function replaceFormatTags(text)
{
    return text
        .replace(/<b>/gi, "<strong>")
        .replace(/<\/b>/gi, "</strong>")
        .replace(/<i>/gi, "<em>")
        .replace(/<\/i>/gi, "</em>");
}

export function convertToParagraphs(text)
{
    const
        lineBreaksRemoved = text.replace(/\n/g, ""),
        wrappedInParagraphs = `<p>${lineBreaksRemoved}</p>`,
        brsRemoved = wrappedInParagraphs.replace(/<br[^>]*>[\s]*<br[^>]*>/gi, "</p>\n<p>");
    return brsRemoved.replace(/<p><\/p>/g, "");
}

export default { nl2br, br2nl, removeDivs, convertToParagraphs, replaceFormatTags }