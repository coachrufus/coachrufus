import Immutable from 'immutable'
import { State } from '../Lib/Redux/State'
import { loadData } from '../Model/PerformanceModel'
import model from '../Model/PerformanceModel'

export function performance(state, action)
{
    const _state = new State(state || model);
    switch (action.type)
    {
        case "CHANGE_CHART_TYPE":
        {
            return _state.update({
                ChartType: action.chartType
            });
        }
        case "UPDATE_PLAYER":
        {
            return _state.update({ ...action.data });
        }
        default: return _state.toObject();
    }
}