import Immutable from 'immutable'
import { State } from '../Lib/Redux/State'
import model from '../Model/PlayersComparsionModel'

export function playersComparsion(state, action)
{
    const _state = new State(state || model);
    switch (action.type)
    {
        case "CHANGE_INTERVAL":
        {
            return _state.update({
                Interval: action.interval
            });
        }
        case "CHANGE_PAGE":
        {
            return _state.update({
                Page: action.page
            });
        }
        case "CHANGE_PLAYER":
        {
            const
                myState = _state.toObject(),
                { versus, index } = action;
            return _state.update(Object.assign({
                Versus: versus
            }, index === 1 ? { Page: 'Chart' } : {}));
        }
        case "UPDATE_CHART_MODEL":
        {
            return _state.update({
                ChartModel: action.chartModel
            });
        }
        default: return _state.toObject();
    }
}