import { WidgetApi } from '../Api/WidgetApi.js'
import { shareOnFacebook } from '../Lib/Facebook.js'
import { showLoader } from '../Lib/Facebook.js'
import { hideLoader } from '../Lib/Facebook.js'
import { lang } from '../App/Langs.js';

export default function createShareClickHandler(resources)
{
    return function handleShareClick(box)
    {
        resources.addJSOnce("/js/html2canvas.min.js", function()
        {
            showLoader();
            html2canvas(box,
            {
                width: 2000,
                onrendered: async function(canvas)
                {
                    const
                        image = canvas.toDataURL("image/png"),
                        widgetApi = new WidgetApi();
                    //const guid = await widgetApi.uploadImage(image);
                    
                    var data = await widgetApi.uploadImage(image);
                    console.log(data);
                    
                    const guid = data.guid;
                    const fb_id = data.fb_id;
                    const root = data.root;
                    const desc = '';
                    //shareOnFacebook(`${window.root}/img/share/${guid}.png`);
                    hideLoader();
                    shareOnFacebook(guid,fb_id,root,desc);
                }
            });
        });
    }
}