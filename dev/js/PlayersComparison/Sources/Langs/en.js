export default
{
    LOCALE: "en_EN",
    TEAM_AVG: "Team average",
    ME: "ME",
    TITLE_PERFORMANCE: "Overal Performance Statistics",
    TITLE_COMPARISION: "Performance comparison",
    ACTUAL_FORM: "Current form",
    GOALS: "Goals",
    ASSIST: "Assists",
    WINS: "Wins",
    DRAW: "Draws",
    ALL_STATS: "All statistics",
    MEMBER: "Member",
    MATCHES: "Matches",
    SHARE_PERSONAL_PERFORMANCE: "Share",
    SEASON: "Season",
    MONTH: "Month",
    MORE: "More",
    LESS: "Less",
    NO_DATA_PART1: "I tried really really hard to dig some figures but it seems you have’t played any game by now.",
    SHOW_INDIVIDUAL: 'Show individual CRS, GOAL, ASIST, WIN, DRAW '
}