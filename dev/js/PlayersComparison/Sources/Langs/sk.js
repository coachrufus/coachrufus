export default
{
    LOCALE: "sk_SK",
    TEAM_AVG: "Priemer tímu",
    ME: "JA",
    TITLE_PERFORMANCE: "Celkový prehľad výkonov na zápasy za sezónu",
    TITLE_COMPARISION: "Porovnanie výkonov",
    ACTUAL_FORM: "Aktuálna forma",
    GOALS: "Góly",
    ASSIST: "Asistencie",
    WINS: "Výhry",
    DRAW: "Remízy",
    ALL_STATS: "Všetky štatistiky",
    MEMBER: "ČLEN",
    MATCHES: "Zápasy",
    SHARE_PERSONAL_PERFORMANCE: "Zdielať svoj výkon",
    SEASON: "Sezóna",
    MONTH: "Mesiac",
    MORE: "Viac",
    LESS: "Menej",
    NO_DATA_PART1: "Skúšal som, fakt som skúšal nejaké číselka vydolovať, ale vyzerá to že nemáš ešte nič odohraté.",
    SHOW_INDIVIDUAL: 'Ukáž individálne  CRS, góĺy, asistencie, výhry, prehry '
}