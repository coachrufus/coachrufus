export class ComparerBuilder
{
    comparer(a, b, direction)
    {
        const dir = direction === "asc" ? [1, -1] : [-1, 1];
        if (a > b) return dir[0];
        else if (a < b) return dir[1];
        else return 0;
    }

    isNumeric(n)
    {
        return !isNaN(parseFloat(n)) && isFinite(n);
    }
    
    getComparerValue(value)
    {
        if (value === null) return 0;
        return this.isNumeric(value) ? parseFloat(value) : value.toString().toUpperCase();
    }
    
    getComparer(sortColumn, sortDirection, a, b)
    {
        const
            v1 = this.getComparerValue(a[sortColumn]),
            v2 = this.getComparerValue(b[sortColumn]);
        return this.comparer(v1, v2, sortDirection);
    }
}