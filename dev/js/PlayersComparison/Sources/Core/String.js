export function ucfirst(str)
{
    return str.charAt(0).toUpperCase() + str.substring(1);
}

export default { ucfirst };