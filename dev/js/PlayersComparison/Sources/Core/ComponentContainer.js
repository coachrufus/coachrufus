export class ComponentContainer
{
    constructor()
    {
        this.classes = {};
    }
    
    resolve(key)
    {
        if (!this.classes[key])
        {
            this.classes[key] = require(`../DesignerComponents/${key}.js`)[key];
        }
        return this.classes[key];
    }
}

export default new ComponentContainer()