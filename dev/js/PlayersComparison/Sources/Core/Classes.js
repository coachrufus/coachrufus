export function join()
{
    return this.filter(c => !!c).join(' ');
}

export default { join };