
var MessageManager = function ()
{
     
};

MessageManager.prototype.bindTriggers = function(){
    
     var manager =this;
    $('.mesage-recipient-find').on('keyup', function () {
        if ($(this).val().length > 3)
        {
            phrase = $(this).val();
            team_id = $(this).attr('team-id');
            autocomplete_list = $('.autocomplete_list');
            autocomplete_input = $(this);
            
            data = {phrase:phrase,team_id:team_id};
            $.ajax({
                method: "GET",
                url: "/message/search-recipient",
                data: data,
                dataType: 'json'
            }).done(function (result) {
                if (result.length > 0)
                {
                 autocomplete_list.html('');    
                result.forEach(function (user)
                    {
                        var item = $('<li/>');
                        item.html('<div class="autocomplete-avatar" style="background-image:url('+user.mid_photo+')"></div><strong>' + user.name + ' ' + user.surname + '</strong>');
                        
                        
                         item.on('click', function () {
                            manager.addRecipient(user);
                            autocomplete_list.html('');
                            autocomplete_list.fadeOut();
                        });
                        
                        autocomplete_list.append(item);
                    });
                    wrap_position = autocomplete_input.position();
                    
                    $('.autocomplete_wrap').css('left',wrap_position.left+'px');
                    $('.autocomplete_wrap').css('top',(wrap_position.top+30)+'px');
                    
                    
                    autocomplete_list.show();
                }
            });
            
        }
    });
    
    $('.label-message-recipient .fa-close').on('click',function(){
         $(this).parent().remove();
    });
};

MessageManager.prototype.addRecipient = function(recipient){
    
    var item = $('<span/>',{'class' :'label label-info label-message-recipient','html':'<input type="hidden" name="recipients[]" value="'+recipient.id+'" /> '+recipient.name+' '+recipient.surname});
    var delete_item = $('<i/>',{class:'fa fa-close'});
    delete_item.on('click',function(){
        $(this).parent().remove();
        
    });
    
    item.prepend(delete_item);
     $('.mesage-recipient-find').before(item);
    
    $('.mesage-recipient-find').val('');
     $('.mesage-recipient-find').focus();
};
