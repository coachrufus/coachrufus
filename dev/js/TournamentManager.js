

var TournamentManager = function()
{
    this.rating_url = '/team/add-rating';
    this.rating_trigger_class = '.score_trigger';
    this.edit_photo_modal = $('#edit-photo-modal');
    this.change_role_url = '/team/teams/edit';
    this.change_role_trigger_class = '.change-role-trigger';
    this.remove_modal = $('#removePlayerModal');
    this.unactivate_modal = $('#unactivatePlayerModal');
    this.edit_modal = $('#edit-team-modal');
    this.import_modal = $('#import-teams-modal');
    this.unregistred_invitation_modal = $('#unregistred-invitation-modal');
    this.remove_url = '';
    this.unactivate_url = '';
    this.remove_row;
    
    
    this.locationMap;
    this.google;
    this.place_marker = null;
    this.map_center = {lat: 48.978076, lng: 19.519994};
};

TournamentManager.prototype.bindTriggers = function(){
    var manager = this;
    
    manager.fileuploadTrigger();
    manager.addEditPhotoTrigger($('.edit-team-photo'));
    manager.addRemovePhotoTrigger($('.delete-team-photo'));
    manager.bindSwitchMoreDetail();

    $('#add_team').on('click',function(e){
        e.preventDefault();
        var base_template_row = $('.template_row');
        
        var action_row = $('#action_row');
        var rand_hash = Math.random().toString(36).substring(7);
        var template_row = $('.template_row').clone();
        
        first_name_input = template_row.find('#team_name0');
        first_name_input.attr('name','team['+rand_hash+'][name]');
        first_name_input.attr('id','team_name'+rand_hash+'');
        first_name_input.attr('data-row',rand_hash);
       

        email_input =  template_row.find('#team_email0');
        email_input.attr('name','team['+rand_hash+'][email]');
        email_input.attr('id','team_email'+rand_hash+'');
        email_input.attr('data-row',rand_hash);
       
        
        team_id_input =  template_row.find('#team_id0');
        team_id_input.attr('name','team['+rand_hash+'][team_id]');
        team_id_input.attr('id','team_id'+rand_hash+'');
      

        autocomplete = new TeamSearchAutocomplete(first_name_input);
        autocomplete.initNameSearch();
        autocomplete.onclick = 'fillInput';
        
       
        template_row.removeClass('template_row');
        $('#add_team_wrap').append(template_row);
        //reset template
        base_template_row.find('#team_name0').val('');
        base_template_row.find('#team_email0').val('');
        base_template_row.find('#team_id0').val('');

    });
    
    $(document).on('click', '.rm_team_btn', function (e) {
        e.preventDefault();
        $(this).parents('.team_row').remove();
    });
    
    $('#add_team_form').on('submit',function(e){
        var errors = false;
        $('div.team_row').each(function(index, val){
           
            team_first_name =  $(val).find('.team_first_name_row').val();
            team_last_name =  $(val).find('.team_last_name_row').val();
            team_email =  $(val).find('.team_email_row').val();
            
            if(team_email != '')
            {
                if(team_first_name == '' && team_last_name == '')
                {
                    errors = true; 
                }
            }
        });
        
        if(errors)
        {
            $('#error-alerts').addClass('alert alert-danger');
            return false;
        }
        else
        {
            return true;
        }

    });
    
    
   $('.add_team_form_trigger').on('click',function(e){
        e.preventDefault();
        
        $('.team_row').each(function(index, val){
            
            $(val).parent().find('input').each(function(index, val){
                console.log($(val).val());
            });
        });
   });
   
   
   $('.remove-team-team-trigger').on('click',function(e){
        e.preventDefault();
        manager.remove_url = $(this).attr('href');
        manager.remove_row = $('#'+$(this).attr('data-row'));
       
        manager.remove_modal.modal('show');
       
   });
   
   $('#removePlayerModal .modal_submit').on('click',function(){
       manager.removePlayer();
   });
   
   $('.unactivate-team-team-trigger').on('click',function(e){
        e.preventDefault();
        manager.unactivate_url = $(this).attr('href');
        manager.unactivate_modal.modal('show');
       
   });
   
   $('#unactivatePlayerModal .modal_submit').on('click',function(){
       location.href = manager.unactivate_url;
   });
   
   $('.teams_list_filter input').on('ifChecked',function(){
        var filter_class = $(this).val();
        $('.'+filter_class).each(function(key,val){
            $(val).show();
        });
   });
   $('.teams_list_filter input').on('ifUnchecked',function(){
        var filter_class = $(this).val();
        $('.'+filter_class).each(function(key,val){
            $(val).hide();
        });
   });
   
   $('.team-row-edit, .team-row-edit-sm').on('click',function(){
       /*
       var target_id = $(this).attr('data-edit');
       var inline_edit = $('#inline-edit-'+target_id);
       var static_data = $('#static-data-'+target_id);
       $('#team_row_'+target_id).addClass('edited');
       $('#team_row_'+target_id).removeClass('static');
       inline_edit.show();
       static_data.hide();
       */
      var pid = $(this).attr('data-pid');
      var team_id = $(this).attr('data-edit');
      var target_row = $('#team_row_'+ team_id);
      var first_name =target_row.find('.first_name').text();
      var last_name =target_row.find('.last_name').text();
      var email =target_row.find('.email').text();
      var team_role =$('#team_role_'+team_id).val();
      var level =$('#team_level_'+team_id).val();
      var team_number =$('#team_number'+team_id).val();
      var status =$('#team_status_'+team_id).val();
      var status_text = $('#team_status_text_'+team_id).html();

      if(status == 'unactive' && email == '')
      {
          $('#edit_modal_form_status').show();
          $('#edit_modal_form_team_status_text').html(''); 
          $('#edit_modal_form_team_status_text').hide(); 
      }
      else if(status != 'confirmed')
      {
          $('#edit_modal_form_status').hide();
          $('#edit_modal_form_team_status_text').html(status_text); 
          $('#edit_modal_form_team_status_text').show(); 
      }
      else
      {
          $('#edit_modal_form_status').show();
          $('#edit_modal_form_team_status_text').html(''); 
          $('#edit_modal_form_team_status_text').hide(); 
      }
      
      $('#edit_modal_form_team_id').val(team_id);
      $('#edit_modal_form_first_name').val(first_name);
      $('#edit_modal_form_last_name').val(last_name);
      $('#edit_modal_form_email').val(email);
      $('#edit_modal_form_team_role').val(team_role);
      $('#edit_modal_form_team_number').val(team_number);
      $('#edit_modal_form_level').val(level);
      $('#edit_modal_form_status').val(status);
      
      //registred team
      
      if(pid > 0)
      {
          //$('#edit_modal_form_email').attr('disabled','disabled');
          $('#edit_modal_email_text').remove();
          $('#edit_modal_form_email').parent().append('<div id="edit_modal_email_text">'+email+'</div>');
          $('#edit_modal_form_email').hide();
      }
      else
      {
           $('#edit_modal_email_text').remove();
           $('#edit_modal_form_email').show();
           //$('#edit_modal_form_email').removeAttr('disabled');
      }
      
      manager.edit_modal.find('.control-label-error').remove();
      manager.edit_modal.modal('show');
       
      
   });
   $('.team-row-cancel-edit').on('click',function(){
       var target_id = $(this).attr('data-edit');
       var inline_edit = $('#inline-edit-'+target_id);
       var static_data = $('#static-data-'+target_id);
       $('#team_row_'+target_id).removeClass('edited');
       $('#team_row_'+target_id).addClass('static');
       inline_edit.hide();
       static_data.show();
       
       $('#team_first_name_'+target_id).val(static_data.find('.first_name').text());
       $('#team_last_name_'+target_id).val(static_data.find('.last_name').text());
       $('#team_email_'+target_id).val(static_data.find('.email').text());
       
   });
   
   $('.tournament_team_invite_admin_trigger').on('click',function(e){
       e.preventDefault();
       
       var email = $('#'+$(this).attr('data-input')).val();
       var target_link = $(this).attr('href')+'&e='+email;
       
       window.location.href = target_link;

      
   });
   
};


TournamentManager.prototype.fileuploadTrigger = function()
{
    var manager = this;

    
    $('#members_import_file').fileupload({
      dropZone: $('#dropzone'),
        dataType: 'json',
       done: function (e, data) {
           $.each(data.result.files, function (index, file) {
               $('<p/>').text(file.name).appendTo(document.body);
           });
       },
       progress: function (e, data) {
           $('.upload-progress').show();
           var progress = parseInt(data.loaded / data.total * 100, 10);
           $('.upload-progress .progress-num').text((progress-1) + '%');
           $('.upload-progress .progress-bar').css('width',progress + '%');
           
           $('.upload-error').remove();
       },
       done: function (e, data) {

           if(data.result.result == 'SUCCESS')
           {
               location.reload();
           }
           
           if(data.result.result == 'ERROR')
           {
               $('.upload-wrap').before('<div class="alert alert-error upload-error">'+data.result.error_message +'</div>');
           }
           

       }
   });
};


TournamentManager.prototype.validateData = function(elem)
{
  
  
    validator = new Validator(); 
    validator.setElements(elem);
    console.log(elem); console.log(validator);
    
    validator.checkRequired([elem.attr('id')]);
    
    
    
    $('.control-label-error').remove();

    if(validator.hasErrors())
    {
         errors = validator.getErrors();
         console.log(errors);
          for (key in errors) {
            errors[key].element.after('<label class="control-label-error" for="inputError">'+ errors[key].message+'</label>');
          }
          return false;
    }
    else
    {
        return true;
    }
};

TournamentManager.prototype.changeRole = function(elem){
    var select = $('#'+elem.attr('data-rel'));
    var team = {id:select.attr('data-id'),team_role:select.val()};
    
    var data = {teams:[team]};
     $.ajax({
            method: "GET",
            url: this.change_role_url,
            data: data,
            dataType: 'json'
        }).done(function (response) {

           console.log(response);
        
        });
};

TournamentManager.prototype.removePlayer = function(){
    
     manager = this;
     $.ajax({
            method: "GET",
            url: this.remove_url
        }).done(function (response) {

           manager.remove_row.fadeOut(500);
           manager.remove_modal.modal('hide');
           manager.remove_url = '';
           
        });
};

TournamentManager.prototype.sendRating = function(elem){
     var rating = elem.attr('data-score');
     var team_id = elem.attr('data-team');
     var match_id = elem.attr('data-match');
     var data = {rating:rating,team_id:team_id,match_id:match_id};
     $.ajax({
            method: "GET",
            url: this.rating_url,
            data: data,
            dataType: 'json'
        }).done(function (response) {

           console.log(response);
        
        });
};

TournamentManager.prototype.bindModalEditForm = function()
{
    var manager = this;
    $('#edit-team-modal-form').on('submit', function (e) {
        e.preventDefault();
        var form = $(this);
        var data = form.serialize();
        var action = form.attr('action');
        $.ajax({
            method: "POST",
            url: action,
            data: data,
            dataType: 'json'
        }).done(function (response) {
            form.find('.control-label-error').remove();
            if (response.result == 'ERROR')
            {
                form.effect( "shake");
                $.each(response.errors, function (key, val) {
                    form.find('[data-error-bind="' + key + '"]').append('<span class="control-label-error">' + val + '</span>');
                });
            }
            else
            {
                //var redirect_url = 
                location.reload();
                //window.location.href = location.href+"#teamsList";
                /*
                var team_data = response.team_data;
                var team_id = team_data.team_team_id;
                var target_row = $('#team_row_'+team_id);
                
                target_row.find('.first_name').text(team_data.first_name);
                target_row.find('.last_name').text(team_data.last_name);
                target_row.find('.email').text(team_data.email);
                $('#team_role_'+team_id).val(team_data.team_role);
                $('#team_level_'+team_id).val(team_data.level);
                $('#team_number'+team_id).val(team_data.team_number);
                $('#team_status_'+team_id).val(team_data.status);
                manager.edit_modal.modal('hide');
                target_row.effect("highlight", {}, 2000);
                */
            }
        });
    });
};

TournamentManager.prototype.addEditPhotoTrigger = function (elem)
{
    manager = this;
    elem.on('click',function(e){
        e.preventDefault();
        manager.edit_photo_modal.modal('show');
    });
};

TournamentManager.prototype.addRemovePhotoTrigger = function (elem)
{
    manager = this;
    link = elem.attr('href');
    elem.on('click',function(e){
        e.preventDefault();
        $('#team_photo').val('');
        $('#exist-photo-wrap').css('backgroundImage','');
    });
    
};

TournamentManager.prototype.fileuploadTrigger = function()
{
    var manager = this;
    
     $(document).bind('drop dragover', function (e) {
        e.preventDefault();
    });
    
    $(document).bind('dragover', function (e) {
        var dropZone = $('#dropzone'),
            timeout = window.dropZoneTimeout;
        if (!timeout) {
            dropZone.addClass('in');
        } else {
            clearTimeout(timeout);
        }
        var found = false,
            node = e.target;
        do {
            if (node === dropZone[0]) {
                found = true;
                break;
            }
            node = node.parentNode;
        } while (node != null);
        if (found) {
            dropZone.addClass('hover');
        } else {
            dropZone.removeClass('hover');
        }
        window.dropZoneTimeout = setTimeout(function () {
            window.dropZoneTimeout = null;
            dropZone.removeClass('in hover');
        }, 100);
    });
    
    
    $('#team_settings_photo').fileupload({
      dropZone: $('#dropzone'),
       done: function (e, data) {
           $.each(data.result.files, function (index, file) {
               $('<p/>').text(file.name).appendTo(document.body);
           });
       },
       progress: function (e, data) {
           $('.upload-progress').show();
           var progress = parseInt(data.loaded / data.total * 100, 10);
           $('.upload-progress .progress-num').text((progress-1) + '%');
           $('.upload-progress .progress-bar').css('width',progress + '%');
           
           $('.upload-error').remove();
       },
       done: function (e, data) {

           
           if(data.result.result == 'SUCCESS')
           {
               $('#exist-photo-wrap').css('background-image','url("' + data.result.file + '")');
                $('#team_photo').val(data.result.base_name);
                $('#team_photo_data').val(data.result.binary);
                var edit_img = $('<a/>',{class:'edit-team-photo',text:'<i class="fa fa-pencil-square-o"></i>'});
                var remove_img = $('<a/>',{class:'delete-team-photo',text:'<i class="fa fa-times"></i>'});
                manager.addEditPhotoTrigger(edit_img);
                $('.upload-progress .progress-num').text('100%');
                $('.upload-progress').hide();
                manager.edit_photo_modal.modal('hide');
           }
           
           if(data.result.result == 'ERROR')
           {
               $('.upload-wrap').before('<div class="alert alert-error upload-error">'+data.result.error_message +'</div>');
           }
           

       }
   });
};


TournamentManager.prototype.bindSwitchMoreDetail = function()
{
    $('.team-more-detail-trigger').on('click',function(e){
        e.preventDefault();
        
        var target = $($(this).attr('href'));
        var current_text = $(this).find('span').text();
        var new_text = $(this).attr('data-switch-text');
        
        $(this).find('span').text(new_text);
        $(this).attr('data-switch-text',current_text);
        
        if(target.hasClass('in'))
        {
            $('#team_detail_status').val('no');
            target.removeClass('in');
            target.addClass('out');
            $(this).find('ico').removeClass('ico-plus-green');
            $(this).find('ico').addClass('ico-minus-green');
        }
        else
        {
             $('#team_detail_status').val('yes');
            target.removeClass('out');
            target.addClass('in');
            
            $(this).find('ico').addClass('ico-plus-green');
            $(this).find('ico').removeClass('ico-minus-green');
        }
    });
};


TournamentManager.prototype.addMapClickListener = function (map)
{
    
    var manager = this;
    manager.google.maps.event.addListener(map, 'click', function (event) {
        var marker = manager.placeMarker(map, event.latLng);
        manager.setNewLocation(event.latLng);
    });
};

TournamentManager.prototype.initMap = function(latLng)
{
      this.map = new google.maps.Map(document.getElementById('map'), {
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        
     if(null != latLng)
     {
         this.map.setCenter(latLng);
         this.placeMarker(this.map, latLng);
     }
     else
     {
          this.map.setCenter(this.map_center);
          this.map.setZoom(2);
     }
 
      
     this.addMapClickListener(this.map);
};


TournamentManager.prototype.createSearchInput = function()
{
    var manager = this;
    var searchBox = new this.google.maps.places.SearchBox(document.getElementById('locality-search'));
    searchBox.addListener('places_changed', function () {
        manager.places = searchBox.getPlaces();
        manager.map_center =  manager.places[0].geometry.location;
        manager.setNewLocation(manager.map_center);
        manager.map.setZoom(14);
        manager.map.setCenter(manager.map_center);
        manager.placeMarker(manager.map,manager.map_center);
    });
};



TournamentManager.prototype.setNewLocation = function(latLng)
{
     var manager = this;
     var geocoder = new google.maps.Geocoder;
            geocoder.geocode({'location': latLng}, function (results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                       

                        $('#locality_json_data').val(JSON.stringify(results[0]));
                        //$('#playground_name').val(results[0].formatted_address);
                        //results[0].formatted_address;
                        
                        /*
                        if(manager.place_marker_infowindow !== null)
                        {
                            manager.place_marker_infowindow.setMap(null);
                        }
                        
                         manager.place_marker_infowindow  = new  manager.google.maps.InfoWindow({
                                    content: results[0].formatted_address+'<br /><a data-address="'+results[0].formatted_address+'" data-id="" href="#" class="select_playground_trigger">Select this place</a>'
                                  });


                        manager.google.maps.event.addListener(manager.place_marker, 'click', function() {
                            manager.place_marker_infowindow.open(manager.map,manager.place_marker);
                         });
                         
                        manager.place_marker_infowindow.open(manager.map,manager.place_marker);
                        
                        */
                        
                        
                        
                    } else {
                        window.alert('No results found');
                    }
                } else {
                    window.alert('Geocoder failed due to: ' + status);
                }
            });
      
};



TournamentManager.prototype.placeMarker = function(map, location)
{
    var manager = this;
    if(manager.place_marker !== null)
    {
        manager.place_marker.setMap(null);
    }
    
    
    manager.place_marker = new this.google.maps.Marker({
        position: location,
        draggable:true,
        map: map
    });

    this.google.maps.event.addListener(manager.place_marker, 'dragend', function (event) {
        manager.setNewLocation(event.latLng);
    });
};