$('.add-sport-register').on('click', function (e) {
    e.preventDefault();
    var base_template_row = $('.sport-template-row');
    var rand_hash = Math.random().toString(36).substring(7);
    var template_row = $('.sport-template-row').clone();
    
    template_row.removeClass('sport-template-row');
    
      sport_input = template_row.find('#record_sport_id');
      sport_input.attr('name','record[sports]['+rand_hash+'][sport]');
      sport_input.attr('id','record_sport_id'+rand_hash+'');
      sport_input.val(base_template_row.find('#record_sport_id').val());
      
      level_input = template_row.find('#record_sport_level');
      level_input.attr('name','record[sports]['+rand_hash+'][level]');
      level_input.attr('id','record_sport_level'+rand_hash+'');
       level_input.val(base_template_row.find('#record_sport_level').val());
      
        remove_btn = $('<span/>',{'class':'rm_sport_btn'});
        remove_btn_ico = $('<i/>',{'class':'ico ico-cross'});
        remove_btn.append(remove_btn_ico);
        
        remove_btn.on('click',function(e){
            e.preventDefault();
            $(this).parents('.sport_row').remove();
        });
        
        template_row.find('.btn-col').html('');
        template_row.find('.btn-col').append(remove_btn);
        
        
        base_template_row.find('#record_sport_id').val('');
        base_template_row.find('#record_sport_level').val('');


    base_template_row.before(template_row);
});


