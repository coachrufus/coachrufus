import 'babel-polyfill'
import React from 'react'
import ReactDOM from 'react-dom'
import Immutable from 'immutable'
import { Provider, connect } from 'react-redux';
import { Chat } from './Components/Chat/Chat.js'
import chatStore from './Model/ChatStore.js';
import { sendMessage, refreshChat, selectContact, unselectContact, selectAllContacts, unselectAllContacts, markAsReaded } from './Actions/ChatActions.js';

if (window.activeTeam)
{
    function onChangeContactCheckBox({ data, isChecked })
    {
        const { id } = data;
        if (id === "all")
        {
            $('#chat-root input.contact-check-box').iCheck(isChecked ? 'check' : 'uncheck');
        }
        else
        {
            chatStore.dispatch(isChecked ? selectContact(parseInt(id)) : unselectContact(parseInt(id)));
        }
    }
    
    async function handleSend({ message, recipients })
    {
        if (recipients.count() === 0)
        {
            alert("Prosím vyberte hráčov, ktorým chcete poslať správu");
            return false;
        }
        else
        {
            try
            {
                chatStore.dispatch(await sendMessage(recipients, message));
                return true;
            }
            catch (e)
            {
                return false;
            }
        }
    }
    
    async function handleReaded()
    {
        if (chatStore.getState().unreaded != 0)
        {
            chatStore.dispatch(await markAsReaded());
        }
    }

    const ChatMappers = 
    {
        mapStateToProps(state)
        {
            const { chatStore, ...props } = state;
            return props;
        },

        mapDispatchToProps()
        {
            return {
                onChangeContactCheckBox: onChangeContactCheckBox,
                onSend: handleSend,
                onReaded: handleReaded
            }
        }
    }

    const ChatContainer = connect(
        ChatMappers.mapStateToProps,
        ChatMappers.mapDispatchToProps,
        null, { pure: false }
    )(Chat);

   

    if(null!= document.getElementById("chat-root"))
    {
          ReactDOM.render(
            <Provider store={chatStore}>
                <ChatContainer />
            </Provider>,
            document.getElementById("chat-root")
        );
    }
  
    
    const refresh = async function()
    {
        chatStore.dispatch(await refreshChat());
    }
    
    //setInterval(refresh, 3000);
    //refresh();
}

const container = document.getElementById("charts-container");

if (container != null)
{
    let { PlayerApi } = require('./Api/PlayerApi.js');
    let { WidgetApi } = require('./Api/WidgetApi.js');
    let { resources } = require('./App/Resources.js');
    let { MainContainer } = require('./Containers/MainContainer.js');
    let {
        setTeamId, fetchWidgetData, fetchTeamMatesData,
        fetchPersonalFormData, closeWidget, updateWidgets
    } = require('./Actions/WidgetActions.js');
    let createShareClickHandler = require('./EventHandlers/ShareOnFacebook.js').default;
    let chartStore = require('./Model/ChartStore.js').default;
    resources.addCSS("/dev/js/Modules/css/widgets.css");
    resources.addCSS("/dev/js/Modules/css/fixed-data-table.min.css");
    const handleShareClick = createShareClickHandler(resources);
    const teamId = parseInt(document.getElementById("team-id").value);

    function handleCloseWidget(index)
    {
        chartStore.dispatch(closeWidget(index));
    }

    function handleCloseWidget(index)
    {
        chartStore.dispatch(async (dispatch, getState) => 
        {
            chartStore.dispatch(closeWidget(index));
            const { UserWidgets } = getState();
            await updateWidgets(UserWidgets);
        });
    }

    const SeasonLoading = 
    {
        icon: $("#seasonLoading"),
        show() { this.icon.show(); },
        hide() { this.icon.hide(); }
    }
    
    function handleTeamMatesSelectedPlayer(teamPlayerId, teamPlayer, userWidget)
    {
        chartStore.dispatch(async (dispatch, getState) => 
        {
            SeasonLoading.show();
            let state = getState();
            await fetchTeamMatesData({
                userWidgets: state.UserWidgets,
                dispatch
            });
            SeasonLoading.hide();
        });
    }

    function handleChangeDate(widget)
    {    
        SeasonLoading.show();
        chartStore.dispatch(async (dispatch, getState) => 
        {
            const state = getState();
            if (widget.name === "PersonalForm")
            {
                await fetchPersonalFormData({
                    userWidgets: state.UserWidgets,
                    dispatch
                });
            }
            else if (widget.name === "TeamMates")
            {
                await fetchTeamMatesData({
                    userWidgets: state.UserWidgets,
                    dispatch
                });
            }
            else
            {
                await updateWidgets(state.UserWidgets.toJS());
                await fetchWidgetData({
                    teamId: state.teamId,
                    userWidgets: Immutable.List([widget]),
                    dispatch,
                    all: false
                });
                const { UserWidgets } = getState();
                UserWidgets.forEach(userWidget =>
                {
                    if (userWidget.GUID === widget.GUID)
                    {
                        chartStore.dispatch({ type: `UPDATE_WIDGET_MODEL`, userWidget });
                    }
                });
            }
            SeasonLoading.hide();
        });
    }

    function updateAllWidgets(store)
    {
        SeasonLoading.show();
        store.dispatch(async (dispatch, getState) => 
        {
            let state = getState();
            await fetchWidgetData({
                teamId: state.teamId,
                userWidgets: state.UserWidgets,
                dispatch
            });
            store.dispatch({ type: `UPDATE_ALL_WIDGET_MODELS` });
            SeasonLoading.hide();
        });
    }

    $(".dropdown-menu").on('click', 'li a', function()
    {
        SeasonLoading.show();
        const
            a = $(this),
            dd = a.parents(".dropdown"),
            btnValue = dd.find('.btn .value'),
            value = a.data("value");
        btnValue.text(a.text());
        if (dd.attr('id') === 'seasonDropdownMenu')
        {
            (async function()
            {
                const widgetApi = new WidgetApi();
                const isChanged = await widgetApi.changeSeason({
                    teamId: window.teamId,
                    seasonId: value
                });
                updateAllWidgets(chartStore);
                chartStore.dispatch(async (dispatch, getState) => 
                {
                    const state = getState();

                    await fetchPersonalFormData({
                        userWidgets: state.UserWidgets,
                        dispatch
                    });

                    await fetchTeamMatesData({
                        userWidgets: state.UserWidgets,
                        dispatch
                    });
                });
            })();
        }
    });

    chartStore.dispatch(setTeamId(teamId));
    updateAllWidgets(chartStore);
    
    const WidgetMappers = 
    {
        mapStateToProps(state)
        {
            const { ...props } = state;
            return props;
        },

        mapDispatchToProps()
        {
            return {
                onShareClick: handleShareClick,
                onCloseClick: handleCloseWidget,
                onChangeDate: handleChangeDate,
                onChangeTeamMatesSelectedPlayer: handleTeamMatesSelectedPlayer
            }
        }
    }
    
    const Main = connect(
        WidgetMappers.mapStateToProps,
        WidgetMappers.mapDispatchToProps,
        null, { pure: false }
    )(MainContainer);
    
    function afterRender()
    {
        /*$(".connectedSortable").sortable({
            placeholder: "sort-highlight",
            connectWith: ".connectedSortable",
            handle: "header",
            forcePlaceholderSize: true,
            tolerance: 'pointer',
            zIndex: 999999,
            scroll: false
        });*/
    }
    
    ReactDOM.render(
        <Provider store={chartStore}>
            <Main />
        </Provider>,
        container, afterRender
    );
}
