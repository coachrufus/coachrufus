import "babel-polyfill"
import { State } from '../Lib/Redux/State.js'
import model from '../Model/ChartModel.js'
import Immutable from 'immutable'

class WidgetDataFilters
{
    constructor()
    {
        this.filters = {};
    }
    
    resolve(name)
    {
        if (!this.filters[name])
        {
            this.filters[name] = require(`../Filters/${name}.js`)[`to${name}`];
        }
        return this.filters[name];
    }
}

const widgetDataFilters = new WidgetDataFilters();

function updateWidgetModel(state, userWidget, filter)
{
    const
        _state = state.toObject(),
        rawData = _state.WidgetDatas.get(userWidget.GUID),
        model = filter(rawData);
    return state.update({
        WidgetModels: _state.WidgetModels.set(userWidget.GUID, model)
    });
}

export function chart(state, action)
{
    const _state = new State(state || model);
    
    switch (action.type)
    {
        case "SET_TEAM_ID":
        {
            return _state.update({ teamId: action.teamId });
        }
        
        case "FETCH_WIDGET_DATA":
        {
            let data;
            if (action.all) data = action.data;
            else
            {
                data = _state.toObject().WidgetDatas.map((v, k) =>
                {
                    const value = action.data.get(k)
                    return value ? value : v;
                });
            }
            return _state.update({ WidgetDatas: data });
        }
        
        case "FETCH_TEAM_MATES_DATA":
        {
            return _state.update({ teamMates: action.teamMates });
        }
        
        case "FETCH_PERSONAL_FORM_DATA":
        {
            return _state.update({ personalForm: action.personalForm });
        }

        case "FETCH_SEASON_STATS_DATA":
        {
            return _state.update({ seasonStats: action.seasonStats });
        }
        
        case "FETCH_INDIVIDUAL_STATS_DATA":
        {
            return _state.update({ individualStats: action.individualStats });
        }
        
        case "UPDATE_WIDGET_MODEL":
        {
            const
                { userWidget } = action,
                filter = widgetDataFilters.resolve(userWidget.name);
            return updateWidgetModel(_state, userWidget, filter);
        }

        case "UPDATE_ALL_WIDGET_MODELS":
        {
            let
                { UserWidgets, WidgetModels, WidgetDatas } = _state.toObject();
            UserWidgets.forEach(userWidget =>
            {
                const filter = widgetDataFilters.resolve(userWidget.name);
                const rawData = WidgetDatas.get(userWidget.GUID);
                const model = filter(rawData);
                WidgetModels = WidgetModels.set(userWidget.GUID, model);
            });
            return _state.update({
                WidgetModels: Immutable.Map(WidgetModels),
            });
        }
        
        case "UPDATE_USER_WIDGETS":
        {
            const
                { userWidget } = action;
            /*_state.UserWidgets = _state.UserWidgets.map(function(uw, i)
            {
                return uw.GUID === userWidget.GUID ? userWidget : uw;
            });*/
            return _state;
        }
        
        case "CLOSE_WIDGET":
        {            
            return _state.update({ 
                UserWidgets: _state.toObject().UserWidgets.delete(action.index)
            });
        }
        
        default: return _state.toObject();
    }
}