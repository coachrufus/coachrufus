import "babel-polyfill"
import { State } from '../Lib/Redux/State.js'
import model from '../Model/ChatModel.js'
import Immutable from 'immutable'

export function chat(state, action)
{
    const _state = new State(state || model);
    
    switch (action.type)
    {
        case "REFRESH_CHAT":
        {
            return _state.update({
                messages: action.messages,
                contacts: action.contacts,
                myContact: action.myContact,
                unreaded: action.unreaded,
                scrollDown: action.scrollDown
            });
        }
        
        case "MARK_AS_READED":
        {
            return _state.update({
                unreaded: 0
            });
        }
        
        case "SELECT_CONTACT":
        {
            const
                { selectedContacts } = _state.toObject(),
                contacts = selectedContacts.add(action.userId);
            localStorage.setItem("selectedContacts", JSON.stringify(contacts.toJS()));
            return _state.update({
                selectedContacts: contacts
            });
        }
        
        case "UNSELECT_CONTACT":
        {
            const
                { selectedContacts } = _state.toObject(),
                contacts = selectedContacts.delete(action.userId);
            localStorage.setItem("selectedContacts", JSON.stringify(contacts.toJS()));
            return _state.update({
                selectedContacts: contacts
            });
        }
        
        default: return _state.toObject();
    }
}