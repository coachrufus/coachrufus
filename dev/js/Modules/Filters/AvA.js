import Data from '../Lib/Data.js'
import { graphFilter } from '../Lib/Graph.js'

export function toAvA(data)
{
    const
        avA = data.selectAsNum(['ID', 'Player', 'avA'])::graphFilter('avA'),
        names = Data.names(avA.length),
        ticks = avA.map((a, i) => [i, names.getName(a)]),
        items = avA.map((a, i) => [a.avA, i, a.Player]);
    return { ticks, items };
}