import Data from '../Lib/Data.js'
import { graphFilter } from '../Lib/Graph.js'

export function toGP(data)
{
    const
        GP = data.selectAsNum(['ID', 'Player', 'GP'])::graphFilter('GP'),
        names = Data.names(GP.length),
        ticks = GP.map((a, i) => [i, names.getName(a)]),
        items = GP.map((a, i) => [a.GP, i, a.Player, a.Player]);
    return { ticks, items };
}