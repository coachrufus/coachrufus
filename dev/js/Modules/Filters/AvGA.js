import Data from '../Lib/Data.js'
import { graphFilter } from '../Lib/Graph.js'

export function toAvGA(data)
{
    const
        avGA = data.selectAsNum(['ID', 'Player', 'avGA'])::graphFilter('avGA'),
        names = Data.names(avGA.length),
        ticks = avGA.map((a, i) => [i, names.getName(a)]),
        items = avGA.map((a, i) => [a.avGA, i, a.Player]);
    return { ticks, items };
}