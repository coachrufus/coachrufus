import Data from '../Lib/Data.js'
import { graphFilter } from '../Lib/Graph.js'

export function toAvG(data)
{
    const
        avG = data.selectAsNum(['ID', 'Player', 'avG'])::graphFilter('avG'),
        names = Data.names(avG.length),
        ticks = avG.map((a, i) => [i, names.getName(a)]),
        items = avG.map((a, i) => [a.avG, i, a.Player]);
    return { ticks, items };
}