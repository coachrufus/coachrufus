import Data from '../Lib/Data.js'
import { graphFilter } from '../Lib/Graph.js'

export function toPM(data)
{
    const
        PM = data.selectAsNum(['ID', 'Player', '+/-'])::graphFilter('+/-'),
        names = Data.names(PM.length),
        ticks = PM.map((a, i) => [i, names.getName(a)]),
        items = PM.map((a, i) => [a['+/-'], i, a.Player]);
    return { ticks, items };
}