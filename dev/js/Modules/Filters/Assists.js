import Data from '../Lib/Data.js'
import { pieGraphFilter } from '../Lib/Graph.js'
import { palette } from '../UI/Palette.js'
import Immutable from "immutable"

export function toAssists(data)
{
    const
        rows = data.selectAsNum(['ID', 'Player', 'Assists'])::pieGraphFilter('Assists'),
        names = Data.names(rows.length),
        colorPalette = palette({
            numberOfColours: rows.length
        });
    const
        nameStack = [],
        values = rows.map((d, i) =>
        {
            const
                label = d.isAnother ? names.getName(d, 9) : names.getName(d, 9),
                color = colorPalette.get(i);
            nameStack.push([color, d.Player]);
            return {
                label,
                data: d.Assists,
                color,
            };
        });
    return { nameStack: Immutable.Map(nameStack).toJS(), values };
}