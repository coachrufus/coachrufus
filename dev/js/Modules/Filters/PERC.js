import Data from '../Lib/Data.js'
import { graphFilter } from '../Lib/Graph.js'

export function toPERC(data)
{
    const
        PERC = data.selectAsNum(['ID', 'Player', '%'])::graphFilter('%'),
        names = Data.names(PERC.length),
        ticks = PERC.map((a, i) => [i, names.getName(a)]),
        items = PERC.map((a, i) => [a['%'], i, a.Player]);
    return { ticks, items };
}