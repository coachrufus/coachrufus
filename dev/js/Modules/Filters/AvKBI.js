import Data from '../Lib/Data.js'
import { graphFilter } from '../Lib/Graph.js'

export function toAvKBI(data)
{
    const
        avKBI = data.selectAsNum(['ID', 'Player', 'avKBI'])::graphFilter('avKBI'),
        names = Data.names(avKBI.length),
        ticks = avKBI.map((a, i) => [i, names.getName(a, 8)]),
        items = avKBI.map((a, i) => [a.avKBI, i, a.Player]);
    return { ticks, items };
}