import Immutable from "immutable"
import { scaleMap } from '../Lib/Scale.js'
import { turncate } from '../Lib/StringUtils.js'

export function toSeasonKBI(data)
{   
    return { ticks: data.ticks.toArray(), dataSet: data.dataSet.toArray() };
}