import Immutable from 'immutable'
import Data from '../Lib/Data.js'
import { lang } from '../App/Langs.js';
import { graphFilter } from '../Lib/Graph.js'

const CRSParts =
{
    'CRS.MOM': lang.MOM,
    'CRS.Goals': lang.GOLY,
    'CRS.Assists': lang.ASISTENCIE,
    'CRS.Wins': lang.VYHRY,
    'CRS.Draws': lang.REMIZY
}

export const CRSColumns = Immutable.Map(CRSParts).keySeq();

export function toCRS(chartData)
{
    const
        CRSColumnArray = CRSColumns.toJS(),
        columns = ['ID', 'Player'].concat(CRSColumnArray),
        records = chartData.selectAsNum(columns)::graphFilter("CRS", null, (a, b) => {
            const
                an = a['CRS.MOM'] + a['CRS.Goals'] + a['CRS.Assists'] + a['CRS.Wins'] + a['CRS.Draws'],
                bn = b['CRS.MOM'] + b['CRS.Goals'] + b['CRS.Assists'] + b['CRS.Wins'] + b['CRS.Draws'];
            return an - bn;
        }),
        names = Data.names(records.length),
    ticks = records.map((a, i) => [i, names.getName(a)]),
    dataSet = CRSColumnArray.map(c => {
        return {
            label: CRSParts[c],
            data: records.map((a, i) => [a[c], i, a.Player])
        }
    });
    return { ticks, dataSet };
}