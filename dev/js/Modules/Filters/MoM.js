import Data from '../Lib/Data.js'
import { graphFilter } from '../Lib/Graph.js'

export function toMoM(data)
{
    const
        MoM = data.select(['ID', 'Player', 'Man of match', 'M%M'])::graphFilter('Man of match'),
        names = Data.names(MoM.length),
        ticks = MoM.map((a, i) => [i, names.getName(a)]),
        items =
        [
            MoM.map((a, i) => [a['Man of match'], i, a.Player]),
            MoM.map((a, i) => [a['M%M'], i, a.Player])
        ];
    return { ticks, items };
}