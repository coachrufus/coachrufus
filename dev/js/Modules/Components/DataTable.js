import "babel-polyfill"
import Immutable from 'immutable'
import React from 'react'

class Column extends React.Component
{   
    get className()
    {
        if (this.props.header)
        {
            if (this.props.index === this.props.sortIndex)
            {
                return `sorting_${this.props.sortDirection}`;
            }
            else return "sorting";
        }
    }
    
    th(value, index) { return <th className={this.className} onClick={this.props.onColumnClick ? this.props.onColumnClick.bind(this, index) : undefined}>{value}</th> }
    td(value, index) { return <td className={this.className} onClick={this.props.onColumnClick ? this.props.onColumnClick.bind(this, index) : undefined}>{value}</td> }
    
    render()
    {
        return this[this.props.header ? "th" : "td"](this.props.value, this.props.index);
    }
}

class Row extends React.Component
{
    render()
    {
        const hidden = this.props.hiddenColumns;
        return (
            <tr>
            {this.props.columns.map((col, i) =>
                hidden.contains(i) ? undefined :
                    <Column
                        className="row"
                        sortIndex={this.props.sortIndex}
                        sortDirection={this.props.sortDirection}
                        onColumnClick={this.props.onColumnClick}
                        key={i} index={i} value={col === null ? '-' : col}
                        header={this.props.header} />)}
            </tr>
        )
    }
}

const comparer = (a, b, direction) =>
{
    const dir = direction === "asc" ? [1, -1] : [-1, 1];
    if (a > b) return dir[0];
    else if (a < b) return dir[1];
    else return 0;
}

export class DataTable extends React.Component
{   
    constructor(props)
    {
        super(props);
        this.state = {};
        this.state.sortIndex = this.props.sortIndex || 0;
        this.state.sortDirection = this.props.sortDirection || "asc"; 
    }
    
    isNumeric(n)
    {
        return !isNaN(parseFloat(n)) && isFinite(n);
    }
    
    getComparerValue(value)
    {
        if (value === null) return 0;
        return this.isNumeric(value) ? parseFloat(value) : value.toString().toUpperCase();
    }
    
    sortComparer(sortIndex, sortDirection, a, b)
    {
        const
            v1 = this.getComparerValue(a[sortIndex]),
            v2 = this.getComparerValue(b[sortIndex]);
        return comparer(v1, v2, sortDirection);
    }
    
    handleColumnClick(columnIndex)
    {
        if (this.state.sortIndex === columnIndex)
        {
            this.setState(
            {
                sortDirection: this.state.sortDirection === "asc" ? "desc" : "asc"
            });
        }
        else
        {
            this.setState(
            {
                sortIndex: columnIndex,
                sortDirection: "asc"
            });
        }
    }

    render()
    {
        const
            [header, ...rows] = this.props.list,
            hiddenColumns = Immutable.Set(this.props.hiddenColumns),
            sortIndex = this.state.sortIndex,
            sortDirection = this.state.sortDirection,
            { className, ...props } = this.props;
        return (
            <table className={`${className} dataTable`} {...props}>
                <thead>
                    <Row
                        sortIndex={sortIndex} sortDirection={sortDirection}
                        onColumnClick={this.handleColumnClick.bind(this)}
                        columns={header} header={true} hiddenColumns={hiddenColumns} />
                </thead>
                <tbody>
                    {rows.sort(this.sortComparer.bind(this, sortIndex, sortDirection)).map((row, i) => {
                        return <Row key={i} columns={row} hiddenColumns={hiddenColumns} />;
                    })}
                </tbody>
            </table>
        );
    }
}