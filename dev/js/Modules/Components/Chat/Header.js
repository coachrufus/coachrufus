import React from 'react'

class NewMessages extends React.Component
{
    render()
    {
        if (this.props.count > 0)
        {
            return (
                <span data-toggle="tooltip" title={`${this.props.count} New Messages`} className="badge bg-green">{this.props.count}</span>
            )
        }
        else return <span></span>;
    }
}

export class Header extends React.Component
{
    render()
    {
        // <button type="button" className="btn btn-box-tool" data-widget="remove"><i className="fa fa-times"></i></button>
        const classes = `fa fa-${this.props.isCollapsed ? 'plus' : 'minus'}`;
        return (
            <header className="box-header with-border">
                <h3 className="box-title">{this.props.caption}</h3>
                <div className="box-tools pull-right">
                    <NewMessages count={this.props.unreaded} />
                    <button type="button" className="btn btn-box-tool" data-widget="collapse"><i className={classes}></i></button>
                    <button type="button" className="btn btn-box-tool" data-toggle="tooltip" title="Contacts" data-widget="chat-pane-toggle"><i className="fa fa-comments"></i></button>
                </div>
            </header>
        )
    }
}