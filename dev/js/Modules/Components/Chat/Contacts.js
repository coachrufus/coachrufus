import React from 'react'
import ReactDOM from 'react-dom'
import { turncate } from '../../Lib/StringUtils.js'

export class Contact extends React.Component
{
    constructor(props)
    {
        super();
        this.state = { selectedContacts: props.selectedContacts, init: true }
    }
    
    componentDidMount()
    {
        this.initContactCheckBox();
    }
    
    /*componentDidUpdate()
    {
        this.initContactCheckBox();
    }*/
    
    handleContactChanged(event, checkBox, isChecked)
    {
        if (this.props.onChangeContactCheckBox)
        {
            this.props.onChangeContactCheckBox({
                event,
                data: this.props.data,
                checkBox,
                isChecked
            });
        }
    }
    
    initContactCheckBox()
    {
        const checkBox = $(ReactDOM.findDOMNode(this.refs.cb));
        checkBox.iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green',
            increaseArea: '20%'
        });
        const id = parseInt(this.props.data.id);
        /*if (this.state.selectedContacts.contains(id))
        {
            checkBox.iCheck('check');
            this.state.selectedContacts = this.state.selectedContacts.delete(id);
        }*/
        checkBox.on('ifChecked', (event) => this.handleContactChanged(event, checkBox, true));
        checkBox.on('ifUnchecked', (event) => this.handleContactChanged(event, checkBox, false));
        checkBox.iCheck('check');
    }
    
    render()
    {
        const { data } = this.props;
        return (
            <li>
                <a href="#">
                    <img className="contacts-list-img" src={`/img/users/thumb_90_90/${data.image}`} alt="User Image" />
                    <div className="contacts-list-info">
                        <span className="contacts-list-name">
                            {turncate(data.nickname, 20)}
                            <small className="contacts-list-date pull-right">
                                <div>
                                    <label><input className={data.id === 'all' ? '' : 'contact-check-box'} ref="cb" type="checkbox" /></label>
                                </div>
                            </small>
                        </span>
                        <span className="contacts-list-msg">{turncate(data.name + ' ' + data.surname, 28)}</span>
                    </div>
                </a>
            </li>
        )
    }
}

export class Contacts extends React.Component
{
    render()
    {
        const items = [
            /*{
                id: 'all',
                nickname: 'All team members',
                image: 'all.png',
                name: window.activeTeam.name,
                surname: '',
                created: new Date()
            },*/
            ...this.props.items
        ];
        return (
            <div className="direct-chat-contacts">
                <ul className="contacts-list">
                    {items.filter(c => !c.isMy).map((c, i) =>
                        <Contact
                            key={i} data={c} onChangeContactCheckBox={this.props.onChangeContactCheckBox}
                            selectedContacts={this.props.selectedContacts} />)}
                </ul>
            </div>
        )
    }
}