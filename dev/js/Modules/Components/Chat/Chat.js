import React from 'react'
import { Header } from './Header.js'
import { Messages } from './Messages.js'
import { Contacts } from './Contacts.js'
import { Footer } from './Footer.js'

export class Chat extends React.Component
{
    render()
    {
        const classes = `box box-success direct-chat direct-chat-success${this.props.isCollapsed ? ' collapsed-box' : ''}`;
        return (
            <section className="Chat">
                <div className="col-md-3">
                    <div className={classes}>
                        <Header unreaded={this.props.unreaded} caption='Chat' newMessageCount={0} isCollapsed={this.props.isCollapsed} />
                        <div className="box-body">
                            <Messages onReaded={this.props.onReaded} scrollDown={this.props.scrollDown} store={this.props.store} items={this.props.messages} />
                            <Contacts items={this.props.contacts} onChangeContactCheckBox={this.props.onChangeContactCheckBox} selectedContacts={this.props.selectedContacts} />
                        </div>
                        <Footer onReaded={this.props.onReaded} onSend={this.props.onSend} contacts={this.props.contacts} selectedContacts={this.props.selectedContacts} />
                    </div>
                </div>
            </section>
        )
    }
}