import React from 'react'

export class Footer extends React.Component
{
    constructor()
    {
        super();
        this.state = { message: '', loading: false };
    }
    
    handleKeyDown(e)
    {
        if (e.keyCode == 13)
        {
            this.handleSendClick();
        }
    }
    
    handleChangeMessage(e)
    {
        this.setState({ message: e.target.value });
    }
    
    async handleSendClick()
    {
        let isSent = false;
        this.setState({ loading: true });
        if (this.props.onSend && this.state.message != '')
        {
            isSent = await this.props.onSend({
                message: this.state.message,
                recipients: this.props.selectedContacts
            });
            
        }
        this.setState({ message: '' });
        this.refs.message.focus();
        this.setState({ loading: false });
        this.props.onReaded();
    }
    
    render()
    {
        const { loading } = this.state;
        const buttonClasses = `btn btn-success btn-flat ${loading ? 'disabled' : ''}`;
        return (
            <footer className="box-footer">
                <div className="input-group">
                    <input
                        autoComplete="off"
                        type="text" ref="message" name="message"
                        placeholder="Type Message ..."
                        className="form-control"
                        value={this.state.message}
                        onKeyDown={this.handleKeyDown.bind(this)}
                        onChange={this.handleChangeMessage.bind(this)}
                        onFocus={this.props.onReaded} />
                    <span className="input-group-btn">
                        <button disabled={loading} type="button" className={buttonClasses} onClick={this.handleSendClick.bind(this)}>Send</button>
                    </span>
                </div>
            </footer>
        )
    }
}