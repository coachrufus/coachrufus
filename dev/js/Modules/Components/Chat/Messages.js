import React from 'react'
import ReactDOM from 'react-dom'

class Message extends React.Component
{
    getClasses(align)
    {
        return {
            message: `direct-chat-msg ${align === "right" ? "right" : ""}`,
            name: `direct-chat-name pull-${align === "right" ? "left" : "right"}`,
            timestamp: `direct-chat-timestamp pull-${align === "right" ? "right" : "left"}`
        };
    }

    render()
    {
        const
            align = this.props.data.isMy ? "right" : "left",
            classes = this.getClasses(align),
            data = this.props.data;
        return (
            <article className={classes.message}>
                <div className="direct-chat-info clearfix">
                    <span className={classes.name}>{data.contact.nickname}</span>
                    <span className={classes.timestamp}>{data.dateText}</span>
                </div>
                <img className="direct-chat-img" src={`/img/users/thumb_90_90/${data.contact.image}`} alt="Message User Image" />
                <div className="direct-chat-text">{data.body}</div>
            </article>
        );
    }
}

export class Messages extends React.Component
{
    constructor()
    {
        super();
    }
    
    scrollDown()
    {
        let { messages } = this.refs;
        if (messages.scrollTop == 0 || this.props.scrollDown)
        {
            messages.scrollTop = messages.scrollHeight;
        }
    }
    
    componentDidMount()
    {
        this.scrollDown();
    }
    
    componentDidUpdate()
    {
        this.scrollDown();
    }
    
    render()
    {
        return (
            <div ref="messages" className="direct-chat-messages">
                {this.props.items.map((i, k) => <Message key={k} data={i} />)}
            </div>
        )
    }
}