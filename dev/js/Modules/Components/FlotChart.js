import React from 'react'
import ReactDOM from 'react-dom'

export class FlotChart extends React.Component
{
    renderChart()
    {
        const
            div = this.refs.div,
            { model, options, onHover } = this.props;
        if (this.hasLegend)
        {
            options.legend.container = $(ReactDOM.findDOMNode(this.refs.legend));
        }
        const plot = $.plot(div, model, options);
        if (this.props.fixCanvasSize === true)
        {
            this.fixCanvasSize(plot);
        }
        if (options.grid.hoverable && onHover)
        {
            $(div).unbind("plothover").bind("plothover", onHover);
        }
    }
	
    fixCanvasSize(plot)
    {
        const canvas = $(plot.getCanvas());
        canvas.attr('width', this.props.width);
        canvas.attr('height', this.props.height);
        plot.setupGrid();
        plot.draw();
    }
    
    get hasLegend()
    {
        return this.props.options.legend.show === true;
    }
    
    componentWillReceiveProps(nextProps)
    {
    }
    
    shouldComponentUpdate(nextProps, nextState)
    {
        return true;
    }
    
    componentDidMount()
    {
        this.renderChart(); 
    }
    
    componentDidUpdate()
    {
        this.renderChart();
    }
    
    render()
    {
        const style = { 
            width: this.props.width,
            height: this.props.height
        };
        const containerStyles =
            this.hasLegend ? { 'paddingRight': 140 } : undefined;
        return (
            <div className="flot-chart-container" style={containerStyles}>
                { this.hasLegend ? <div ref="legend" className="legend"></div> : undefined }
                <div className="flot-chart" ref="div" style={style} />
            </div>
        );
    }
}