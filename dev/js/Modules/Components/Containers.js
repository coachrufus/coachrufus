import React from 'react'

export class Container extends React.Component
{
    getContainerProps(defaultClasses = "")
    {
        const
            { children, className, ...props } = this.props,
            classes = defaultClasses + (className ? ` ${className}` : '');
        return { children, className: classes, props };
    }
    
    renderWithClasses(classes)
    {
        const { children, className, props } = this.getContainerProps(classes);
        return <div className={className} {...props}>{children}</div>            
    }
}

export class Row extends Container
{
    render()
    {
        return this.renderWithClasses('row');
    }
}

export class Column extends Container
{
    render()
    {
        return this.renderWithClasses(this.props.type ? `col-${this.props.type}` : 'col');
    }
}

export class Box extends Container
{
    render()
    {
        const { children, className, props } = this.getContainerProps("box");
        return (
            <div className={className} {...props}>
                <div className="box-body">{children}</div>
            </div>            
        );
    }
}