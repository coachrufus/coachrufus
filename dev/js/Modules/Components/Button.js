import React from 'react';

export class Button extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {};
    }
    
    render()
    {
        let { children, className, ...tail } = this.props; 
        let classes = "button" + (className ? ` ${className}` : '');
        return (
            <a className={classes} {...tail}>{children}</a>
        )
    }
}