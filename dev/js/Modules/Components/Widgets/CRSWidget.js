import React from 'react'
import { Widget, WidgetBox } from './Base/Widget.js'
import { FlotChart } from '../FlotChart.js'

export class CRSWidget extends Widget
{
    constructor(props)
    {
        super(props);
        this.options =
        {
            colors: ['#ff6d22','#00b3be','#00759a','#a8c346','#a78c46'],
            series:
            {
                stack: true,
                bars: { show: true }
            },
            bars:
            {
                align: "center",
                barWidth: 0.38,
                horizontal: true,
                lineWidth: 0,
                fill: 1
            },
            xaxis:
            {
                tickColor: 'rgba(255, 255, 255, 0.3)',
                font: { color: "white" }
            },
            yaxis:
            {
                tickColor: 'rgba(255, 255, 255, 0)',
                ticks: this.props.ticks,
                font: { color: "white" }
            },
            legend:
            {
                show: true,
                position: "ne",
                noColumns: 1
            },
            grid:
            {
                hoverable: false,
                borderColor: '#fff',
                borderWidth:
                {
                    top: 0,
                    right: 0,
                    bottom: 1,
                    left: 1
                },
            }
        };
    }
    
    getHeight()
    {
        const itemSize = 26;
        return this.props.ticks.length * itemSize + 16;
    }
    
    render()
    {
        this.options.yaxis.ticks = this.props.ticks;
        return (
            <WidgetBox
                isEnabled={this.props.isEnabled}
                userWidget={this.props.userWidget}
                index={this.props.index}
                theme={this.props.theme}
                title={this.props.title}
                footer={this.props.footer}
                onShareClick={this.props.onShareClick}
                onCloseClick={this.props.onCloseClick}
                onChangeDate={this.props.onChangeDate}
                isMinimized={this.props.isMinimized}
                seasons={this.props.seasons} actualSeason={this.props.actualSeason}>
                <FlotChart
                    isEnabled={this.props.isEnabled}
                    width="100%" height={this.getHeight()}
                    model={this.props.model} options={this.options} />
            </WidgetBox>
        );
    }
}