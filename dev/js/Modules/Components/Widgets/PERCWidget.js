import React from 'react'
import { HorizontalBarChartWidget } from './Base/HorizontalBarChartWidget.js'

export class PERCWidget extends HorizontalBarChartWidget
{
    constructor(props)
    {
        super(props);
    }
    
    formatValue({ value })
    {
        return `${value}%`;
    }
    
    xTickFormatter(val, axis)
    {
        return `${val}%`;
    }
}