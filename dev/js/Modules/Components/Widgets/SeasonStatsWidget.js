import React from 'react'
import ReactDOM from 'react-dom'
import { Widget, WidgetBox } from './Base/Widget.js'
import { lang } from '../../App/Langs.js'
import Immutable from 'immutable'

export class SeasonStatsWidget extends Widget
{
    constructor(props)
    {
        super(props);
        this.state = {};
    }
    
    componentDidMount()
    {
    }

    getGoalString(diff)
    {
        if (diff === 0 || diff > 4) return lang.GOLOV;
        else if (diff === 1) return lang.GOL;
        else lang.GOLY;
    }

    renderGoalDiff([diff, count])
    {
        return (
            <tr key={diff}>
                <th>{lang.ROZDIEL_O} {diff} {this.getGoalString(diff)}</th>
                <td>{count}</td>
            </tr>
        );
    }
    
    render()
    {
        const m = this.props.model;
        return (
            <WidgetBox
                isEnabled={this.props.isEnabled}
                userWidget={this.props.userWidget}
                index={this.props.index}
                theme={this.props.theme}
                title={this.props.title}
                footer={this.props.footer}
                onShareClick={this.props.onShareClick}
                onCloseClick={this.props.onCloseClick}
                onChangeDate={this.props.onChangeDate}
                isMinimized={this.props.isMinimized}
                seasons={this.props.seasons} actualSeason={this.props.actualSeason}>
                <div className="data-table-container" style={{ height: 240 }}>
                    <div className="scroll-container">
                        <table className="stats-table season-stats">
                            {m ? <tbody>
                                <tr>
                                    <th>{lang.ZACIATOK_SEZONY}</th>
                                    <td>{m.dateFrom}</td>
                                </tr>
                                <tr>
                                    <th>{lang.KONIEC_SEZONY}</th>
                                    <td>{m.dateTo}</td>
                                </tr>
                                <tr>
                                    <th>{lang.CELKOVY_POCET_ZAPASOV}</th>
                                    <td>{m.eventCount}</td>
                                </tr>
                                <tr>
                                    <th>{lang.CELKOVY_POCET_GOLOV}</th>
                                    <td>{m.goalsTotal}</td>
                                </tr>
                                <tr>
                                    <th>{lang.CELKOVY_POCET_ZUCASTNENYCH_HRACOV}</th>
                                    <td>{m.teamPlayerCount}</td>
                                </tr>
                                <tr>
                                    <th>{lang.PRIEMERNY_POCET_GOLOV_NA_ZAPAS}</th>
                                    <td>{m.goalsAvg}</td>
                                </tr>
                                <tr>
                                    <th>{lang.NAJVYSSI_POCET_GOLOV_V_JEDNOM_ZAPASE}</th>
                                    <td>{m.goalsMax}</td>
                                </tr>
                                <tr>
                                    <th>{lang.NAJNIZSI_POCET_GOLOV_V_JEDNOM_ZAPASE}</th>
                                    <td>{m.goalsMin}</td>
                                </tr>
                                <tr>
                                    <th>{lang.NAJVYSSI_ROZDIEL_V_SCORE}</th>
                                    <td>{m.maxDiff}</td>
                                </tr>
                                <tr>
                                    <th>{lang.REMIZY}</th>
                                    <td>{m.drawsTotal}</td>
                                </tr>
                                {m.goalDiffs.map(this.renderGoalDiff.bind(this))}
                            </tbody> : undefined}
                        </table>
                    </div>
                </div>
            </WidgetBox>
        );
    }
}