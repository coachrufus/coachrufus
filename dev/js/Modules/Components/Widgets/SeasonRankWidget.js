import React from 'react'
import { LineChartWidget } from './Base/LineChartWidget.js'
import { lang } from '../../App/Langs.js'

export class SeasonRankWidget extends LineChartWidget
{
    constructor(props)
    {
        super(props);
    }
}