import React from 'react'
import { LineChartWidget } from './Base/LineChartWidget.js'
import { lang } from '../../App/Langs.js'

export class SeasonKBIWidget extends LineChartWidget
{
    constructor(props)
    {
        super(props);
    }
}