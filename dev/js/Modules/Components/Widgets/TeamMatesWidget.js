import React from 'react'
import ReactDOM from 'react-dom'
import { Widget, WidgetBox } from './Base/Widget.js'
import { lang } from '../../App/Langs.js'
import Table from 'responsive-fixed-data-table';
import { Column, Cell } from 'fixed-data-table'
import { DropdownButton, MenuItem } from 'react-bootstrap'
import Immutable from 'immutable'

const PlayerTextCell = ({rowIndex, data, columnKey, ...props}) => (
    <Cell {...props}>
        {data.getObjectAt(rowIndex)[columnKey].name}
    </Cell>
);

const TextCell = ({rowIndex, data, columnKey, ...props}) => {
    const obj = data.getObjectAt(rowIndex)[columnKey];
    return (
        <Cell {...props}>
            {obj === null ? '-' : obj}
        </Cell>
    )
};

const TextCellPerc = ({rowIndex, data, columnKey, ...props}) => {
    const obj = data.getObjectAt(rowIndex)[columnKey];
    return (
        <Cell {...props}>
            {obj === null ? '-' : `${obj}%`}
        </Cell>
    )
};

export class TeamMatesWidget extends Widget
{
    constructor(props)
    {
        super(props);
        this.state = 
        {
            columnWidths:
            {
                tp: 130,
                gamePlayed: 35,
                wins: 35,
                looses: 35,
                draws: 35,
                winsPrediction: 45,
                plusPoints: 50,
                minusPoints: 60,
                plusMinusDiff: 40,
                goals: 40,
                assists: 40,
                averageGoals: 40,
                averageAssists: 40,
                GA: 35,
                averageGA: 45,
                kbi: 35,
                averageKbi: 45,
                manOfMatch: 45,
                averageManOfMatch: 45
            },
        };
        this.onColumnResizeEndCallback = this.onColumnResizeEndCallback.bind(this);
    }
    
    onColumnResizeEndCallback(newColumnWidth, columnKey)
    {
        this.setState(({ columnWidths }) => ({
            columnWidths:
            {
                ...columnWidths,
                [columnKey]: newColumnWidth,
            }
        }));
    }
    
    handleSelectPlayer(e, teamPlayerId)
    {
        const
            tpKV = [ teamPlayerId, this.teamPlayers.get(teamPlayerId) ],
            { userWidget } = this.props;
        this.setState({
            teamPlayerId,
            teamPlayer: tpKV
        });
        if (this.props.onChangeTeamMatesSelectedPlayer)
        {
            userWidget.options.teamPlayerId = teamPlayerId;
            this.props.onChangeTeamMatesSelectedPlayer(tpKV[0], tpKV[1], userWidget);
        }
    }
    
    substringMatcher(strs)
    {
        return function findMatches(q, cb) {
            let matches = [], substringRegex = new RegExp(q, 'i');
            $.each(strs, function(i, str)
            {
                if (substringRegex.test(str))
                {
                    matches.push(str);
                }
            });
            cb(matches);
        };
    };
    
    componentDidMount()
    {
        const self = this;
        $("#dropdown-basic-0").keydown(function(e)
        {
            const chr = String.fromCharCode(e.which);
            $(this).parent().find("li").each(function()
            {
                const li = $(this);
                const name = li.text();
                if(name.toLowerCase().indexOf(chr.toLowerCase()) === 0)
                {
                    const item = self.props.model.teamPlayers.find((p, i) => p[1] === name);
                    self.handleSelectPlayer(null, item[0]);
                    self.setState({ teamPlayer: item });
                    return false; 
                }
            });
        });
    }
    
    render()
    {
        const { teamPlayers, teamMates } = this.props.model;
        this.teamPlayers = Immutable.Map(teamPlayers);
        if (teamPlayers.length > 0 && !this.state.teamPlayer)
        {
            const [ teamPlayer, ...anotherPlayers ] = teamPlayers;
            if (this.props.userWidget.options.teamPlayerId)
            {
                
                const teamPlayerId = this.props.userWidget.options.teamPlayerId;
                this.state.teamPlayerId = teamPlayerId;
                this.state.teamPlayer = teamPlayers.find(tp => tp[0] === teamPlayerId);
            }
            else
            {
                this.state.teamPlayer = teamPlayer;
            }
            this.state.firstTeamPlayer = teamPlayer;
            this.state.anotherPlayers = anotherPlayers;
        }
        const { columnWidths } = this.state;
        return (
            <WidgetBox
                isEnabled={this.props.isEnabled}
                userWidget={this.props.userWidget}
                index={this.props.index}
                theme={this.props.theme}
                title={this.props.title}
                footer={this.props.footer}
                onShareClick={this.props.onShareClick}
                onCloseClick={this.props.onCloseClick}
                onChangeDate={this.props.onChangeDate}
                isMinimized={this.props.isMinimized}
                seasons={this.props.seasons} actualSeason={this.props.actualSeason}>
                <div className="data-table-container" style={{ height: 266 }}>
                    <div className="player-selection form-inline">
                        <div className="form-group">
                            <label htmlFor="player-selection-input">{lang.CHOOSE_PLAYER}: </label>
                            <DropdownButton bsStyle="primary" title={this.state.teamPlayer[1]} id={`dropdown-basic-0`} activeKey={this.state.teamPlayerId} onSelect={this.handleSelectPlayer.bind(this)}>
                                <MenuItem eventKey={this.state.firstTeamPlayer[0]} active={this.state.firstTeamPlayer[0] == this.state.teamPlayer[0]}>{this.state.firstTeamPlayer[1]}</MenuItem>
                                <MenuItem divider />
                                {this.state.anotherPlayers.map((p, i) => <MenuItem key={i} eventKey={p[0]} active={p[0] == this.state.teamPlayer[0]}>{p[1]}</MenuItem>)}
                            </DropdownButton>
                        </div>
                    </div>
                    <Table
                        rowHeight={30}
                        headerHeight={30}
                        rowsCount={teamMates.count}
                        onColumnResizeEndCallback={this.onColumnResizeEndCallback}
                        isColumnResizing={false}
                        {...this.props}>
                        <Column
                            columnKey="tp"
                            header={<Cell>Player</Cell>}
                            cell={<PlayerTextCell data={teamMates} />}
                            fixed={true}
                            width={columnWidths.tp}
                            isResizable={true}
                        />
                        <Column
                            columnKey="gamePlayed"
                            header={<Cell>GP</Cell>}
                            cell={<TextCell data={teamMates} />}
                            width={columnWidths.gamePlayed}
                            isResizable={true}
                            minWidth={0}
                            maxWidth={170}
                        />
                        <Column
                            columnKey="wins"
                            header={<Cell>W</Cell>}
                            cell={<TextCell data={teamMates} />}
                            width={columnWidths.wins}
                            isResizable={true}
                            minWidth={0}
                            maxWidth={170}
                        />
                        <Column
                            columnKey="looses"
                            header={<Cell>L</Cell>}
                            cell={<TextCell data={teamMates} />}
                            width={columnWidths.looses}
                            isResizable={true}
                            minWidth={0}
                            maxWidth={170}
                        />
                        <Column
                            columnKey="draws"
                            header={<Cell>D</Cell>}
                            cell={<TextCell data={teamMates} />}
                            width={columnWidths.draws}
                            isResizable={true}
                            minWidth={0}
                            maxWidth={170}
                        />
                        <Column
                            columnKey="winsPrediction"
                            header={<Cell>%</Cell>}
                            cell={<TextCellPerc data={teamMates} />}
                            width={columnWidths.winsPrediction}
                            isResizable={true}
                            minWidth={0}
                            maxWidth={170}
                        />
                        <Column
                            columnKey="plusPoints"
                            header={<Cell>sPlus</Cell>}
                            cell={<TextCell data={teamMates} />}
                            width={columnWidths.plusPoints}
                            isResizable={true}
                            minWidth={0}
                            maxWidth={170}
                        />
                        <Column
                            columnKey="minusPoints"
                            header={<Cell>sMinus</Cell>}
                            cell={<TextCell data={teamMates} />}
                            width={columnWidths.minusPoints}
                            isResizable={true}
                            minWidth={0}
                            maxWidth={170}
                        />
                        <Column
                            columnKey="plusMinusDiff"
                            header={<Cell>+/-</Cell>}
                            cell={<TextCell data={teamMates} />}
                            width={columnWidths.plusMinusDiff}
                            isResizable={true}
                            minWidth={0}
                            maxWidth={170}
                        />
                        <Column
                            columnKey="goals"
                            header={<Cell>G</Cell>}
                            cell={<TextCell data={teamMates} />}
                            width={columnWidths.goals}
                            isResizable={true}
                            minWidth={0}
                            maxWidth={170}
                        />
                        <Column
                            columnKey="assists"
                            header={<Cell>A</Cell>}
                            cell={<TextCell data={teamMates} />}
                            width={columnWidths.assists}
                            isResizable={true}
                            />
                        <Column
                            columnKey="averageGoals"
                            header={<Cell>avG</Cell>}
                            cell={<TextCell data={teamMates} />}
                            width={columnWidths.averageGoals}
                            isResizable={true}
                            minWidth={0}
                            maxWidth={170}
                        />
                        <Column
                            columnKey="averageAssists"
                            header={<Cell>avA</Cell>}
                            cell={<TextCell data={teamMates} />}
                            width={columnWidths.averageAssists}
                            isResizable={true}
                            minWidth={0}
                            maxWidth={170}
                        />
                        <Column
                            columnKey="GA"
                            header={<Cell>GA</Cell>}
                            cell={<TextCell data={teamMates} />}
                            width={columnWidths.GA}
                            isResizable={true}
                            minWidth={0}
                            maxWidth={170}
                        />
                        <Column
                            columnKey="averageGA"
                            header={<Cell>avGA</Cell>}
                            cell={<TextCell data={teamMates} />}
                            width={columnWidths.averageGA}
                            isResizable={true}
                            minWidth={0}
                            maxWidth={170}
                        />
                        <Column
                            columnKey="averageGA"
                            header={<Cell>avGA</Cell>}
                            cell={<TextCell data={teamMates} />}
                            width={columnWidths.averageGA}
                            isResizable={true}
                            minWidth={0}
                            maxWidth={170}
                        />
                        <Column
                            columnKey="kbi"
                            header={<Cell>KBI</Cell>}
                            cell={<TextCell data={teamMates} />}
                            width={columnWidths.kbi}
                            isResizable={true}
                            minWidth={0}
                            maxWidth={170}
                        />
                        <Column
                            columnKey="averageKbi"
                            header={<Cell>avKBI</Cell>}
                            cell={<TextCell data={teamMates} />}
                            width={columnWidths.averageKbi}
                            isResizable={true}
                            minWidth={0}
                            maxWidth={170}
                        />
                        <Column
                            columnKey="manOfMatch"
                            header={<Cell>MoM</Cell>}
                            cell={<TextCell data={teamMates} />}
                            width={columnWidths.manOfMatch}
                            isResizable={true}
                            minWidth={0}
                            maxWidth={170}
                        />
                        <Column
                            columnKey="averageManOfMatch"
                            header={<Cell>M%M</Cell>}
                            cell={<TextCell data={teamMates} />}
                            width={columnWidths.averageManOfMatch}
                            isResizable={true}
                            minWidth={0}
                            maxWidth={170}
                        /> 
                    </Table>
                </div>
            </WidgetBox>
        );
    }
}