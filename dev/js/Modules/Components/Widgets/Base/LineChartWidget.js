import React from 'react'
import { Widget, WidgetBox } from './Widget.js'
import { FlotChart } from '../../FlotChart.js'
import { lang } from '../../../App/Langs.js'

export class LineChartWidget extends Widget
{
    constructor(props)
    {
        super(props);
        this.options =
        {
            shadowSize: 0,
            xaxis:
            {
                tickColor: 'rgba(255, 255, 255, 0.3)',
                color: "#fff"
            },
            yaxis:
            {
                tickColor: 'rgba(255, 255, 255, 0)',
                color: "#fff"
            },
            grid:
            {
                borderColor: '#fff',
                borderWidth:
                {
                    top: 0,
                    right: 0,
                    bottom: 1,
                    left: 1
                },
            }
        };
    }
    
    getHeight()
    {
        return 250;
    }
    
    render()
    {
        return (
            <WidgetBox
                isEnabled={this.props.isEnabled}
                userWidget={this.props.userWidget}
                index={this.props.index}
                theme={this.props.theme}
                title={this.props.title}
                footer={this.props.footer}
                onShareClick={this.props.onShareClick}
                onCloseClick={this.props.onCloseClick}
                onChangeDate={this.props.onChangeDate}
                isMinimized={this.props.isMinimized}
                seasons={this.props.seasons} actualSeason={this.props.actualSeason}>
                <FlotChart
                    isEnabled={this.props.isEnabled}
                    width="100%" height={this.getHeight()}
                    model={this.props.model} options={this.options} />
            </WidgetBox>
        );
    }
}