import React from 'react'
import { Widget, WidgetBox } from './Widget.js'
import { FlotChart } from '../../FlotChart.js'
import { lang } from '../../../App/Langs.js'

export class RadarChartWidget extends Widget
{
    constructor(props)
    {
        super(props);
        this.options =
        {
            series:
            { 
                editMode: 'v',
                editable: true,
                spider:
                {
                    active: true,
                    highlight: { mode: "area" },
                    legs:
                    { 
                        font: "12px 'Montserrat', sans-serif",
                        fillStyle: "white",
                        legStartAngle: 0,
                        data: this.props.model.labels,
                        legScaleMax: 1,
                        legScaleMin: 0,
                        legStartAngle: 270
                    },
                    spiderSize: 0.9
                }
            },
            grid:
            { 
                hoverable: true,
                clickable: true,
                editable: true,
                tickColor: "#ffffff",
                mode: "radar"
            },
            legend:
            {
                show: false
            }
        };
        this.className = "RadarChart";
    }
    
    getHeight()
    {
        return 266;
    }
    
    render()
    {
        this.options.series.spider.legs.data = this.props.model.labels;
        return (
            <WidgetBox
                isEnabled={this.props.isEnabled}
                userWidget={this.props.userWidget}
                className={this.className}
                index={this.props.index}
                theme={this.props.theme}
                title={this.props.title}
                footer={this.props.footer}
                onShareClick={this.props.onShareClick}
                onCloseClick={this.props.onCloseClick}
                onChangeDate={this.props.onChangeDate}
                isMinimized={this.props.isMinimized}
                seasons={this.props.seasons} actualSeason={this.props.actualSeason}>
                <div className="personal-form-legend legend">
                    <table style={{fontSize: 'smaller', color: '#545454'}}>
                    <tbody>
                        <tr>
                        <td className="legendColorBox">
                            <div style={{border: '1px solid #ccc', padding: 1}}>
                            <div style={{width: 4, height: 0, border: '5px solid rgba(255, 109, 34, 0.75)', overflow: 'hidden'}} />
                            </div>
                        </td>
                        <td className="legendLabel">{lang.TEAM_AVG}</td>
                        <td className="legendColorBox">
                            <div style={{border: '1px solid #ccc', padding: 1}}>
                            <div style={{width: 4, height: 0, border: '5px solid rgba(0, 117, 154, 0.75)', overflow: 'hidden'}} />
                            </div>
                        </td>
                        <td className="legendLabel">{lang.PERSONAL_FORM}</td>
                        </tr>
                    </tbody>
                    </table>
                </div>
                <FlotChart
					fixCanvasSize={true}
                    isEnabled={this.props.isEnabled}
                    width={this.getHeight()} height={this.getHeight()}
                    model={this.props.model.getData()} options={this.options} />
            </WidgetBox>
        );
    }
}