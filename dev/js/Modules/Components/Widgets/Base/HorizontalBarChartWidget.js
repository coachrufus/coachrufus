import React from 'react'
import { Widget, WidgetBox } from './Widget.js'
import { FlotChart } from '../../FlotChart.js'

export class HorizontalBarChartWidget extends Widget
{
    static counter = 0;
    
    constructor(props)
    {
        HorizontalBarChartWidget.counter += 1;
        super(props);
        this.index = HorizontalBarChartWidget.counter;
        this.options =
        {
            canvas: false,
            series:
            {
                bars: {  show: true },
                highlightColor: 'rgba(255, 255, 255, 1)'
            },
            bars:
            {
                align: "center",
                barWidth: 0.38,
                horizontal: true,
                fillColor: 'rgba(255, 255, 255, 0.5)',
                lineWidth: 0
            },
            xaxis:
            {
                tickColor: 'rgba(255, 255, 255, 0.3)',
                font: { color: "white" }
            },
            tooltip: {
                show: true,
                content: function(label, x, y, item)
                {
                    const name = item.series.data[x, y][2];
                    return `<div class="tooltip tooltip-inner" style="opacity: 1; width: 100px; background: rgba(255, 255, 255, 0.9); color: black;">${name} (${x})</div>`;
                },
                shifts: {
                    x: -100,
                    y: 8
                },
                defaultTheme: false
            },
            yaxis:
            {
                position: "left",
                alignTicksWithAxis: 1,
                tickColor: 'rgba(255, 255, 255, 0)',
                ticks: this.props.ticks,
                font: { color: "white" },
                mode: "categories"
            },
            legend:
            {
                noColumns: 0,
                position: "ne"
            },
            grid:
            {
                hoverable: true,
                borderColor: '#fff',
                borderWidth:
                {
                    top: 0,
                    right: 0,
                    bottom: 1,
                    left: 1
                },
            },
            hooks: { drawSeries: [this::this._drawSeriesHook] }
        };
        if (this.xTickFormatter) this.options.xaxis.tickFormatter = this.xTickFormatter;
        if (this.yTickFormatter) this.options.yaxis.tickFormatter = this.yTickFormatter;
        this.changeOptions(this.options);
    }
    
    formatValue({ value }) { return value; }
    
    _drawSeriesHook(plot, canvasContext, series)
    {
        if (this.props.isEnabled)
        {
            for (let i = 0, len = series.data.length; i < len; i++)
            {
                const
                    offset = plot.offset(),
                    data = series.data[i],
                    position = plot.p2c({ x: data[0], y: data[1] }),
                    left = position.left + offset.left,
                    top = position.top + offset.top,
                    value = this.formatValue({ value: data[0], index: data[1] }),
                    id = `draw-series-hook-${this.index}-${i}`;
                $(`#${id}`).remove();
                $(`<div id='${id}'>${value}</div>`).css({
                    color: 'white',
                    'text-align': 'center',
                    'font-weight': 'bold',
                    'font-size': '11px',
                    position: 'absolute',
                    opacity: '0.5',
                    left: left + 5,
                    top: top - 7
                }).appendTo("body");
            }
        }
    }
    
    changeOptions(options)
    {
    }
    
    getHeight()
    {
        const itemSize = 25;
        return this.props.model[0].length * itemSize + 16;
    }
    
    render()
    {
        this.options.yaxis.ticks = this.props.ticks;
        return (
            <WidgetBox
                isEnabled={this.props.isEnabled}
                userWidget={this.props.userWidget}
                index={this.props.index}
                theme={this.props.theme}
                title={this.props.title}
                footer={this.props.footer}
                onShareClick={this.props.onShareClick}
                onCloseClick={this.props.onCloseClick}
                onChangeDate={this.props.onChangeDate}
                isMinimized={this.props.isMinimized}
                seasons={this.props.seasons} actualSeason={this.props.actualSeason}>
                <FlotChart
                    isEnabled={this.props.isEnabled}
                    width="100%" height={this.getHeight()}
                    model={this.props.model} options={this.options} />
            </WidgetBox>
        );
    }
}