import "babel-polyfill"
import React from 'react'
import ReactDOM from 'react-dom'
import { Container } from '../../Containers.js'
import { Button } from '../../Button.js'
import { lang } from '../../../App/Langs.js'
import { updateWidget } from '../../../Actions/WidgetActions.js'

export class Widget extends React.Component
{
    shouldComponentUpdate(nextProps, nextState)
    {
        return JSON.stringify(nextProps.model) !== JSON.stringify(this.props.model) ;
    }
}

class Controls extends React.Component
{
    render()
    {
        return (
            <div className="Controls">
                {this.props.children}
            </div>  
        );
    }
}

export class WidgetBox extends Container
{
    constructor(props)
    {
        super(props);
        this.state = {
            isMinimized: props.isMinimized || false
        };
        this.calendarLocale = null;
    }
    
    toDate(date)
    {
        const [Y, M, D] = date.split('-')
        return `${D}. ${M}. ${Y}`;
    }
    
    componentDidMount()
    {
        const { dateFrom, dateTo } = this.props.userWidget.options;
        const calendarArrow = $(ReactDOM.findDOMNode(this.refs['calendar-arrow']));
        $(ReactDOM.findDOMNode(this.refs.calendar)).daterangepicker(
        {
            locale:
            {
                format: lang.FORMAT_DATUMU,
                applyLabel: lang.ZVOLIT,
                cancelLabel: lang.ZRUSIT,
                firstDay: 1,
                monthNames: lang.MESIACE.map(m => m.substring(0, 3)),
                daysOfWeek: lang.DNI.map(m => m.substring(0, 2)),
            },
            startDate: dateFrom ? this.toDate(dateFrom) : undefined,
            endDate: dateTo ? this.toDate(dateTo) : undefined,
            maxDate: moment()
        }).on('apply.daterangepicker', (ev, picker) =>
        {
            const
                dateFrom = picker.startDate.format('YYYY-MM-DD'),
                dateTo = picker.endDate.format('YYYY-MM-DD');
            this.props.userWidget.options.dateFrom = dateFrom;
            this.props.userWidget.options.dateTo = dateTo;
            if (this.props.onChangeDate) this.props.onChangeDate(this.props.userWidget);
        }).on('show.daterangepicker', () =>
        {
            calendarArrow.css('visibility', 'visible');
        }).on('hide.daterangepicker', () =>
        {
            calendarArrow.css('visibility', 'hidden');
        });
        this.updateTitle();
    }
    
    componentWillReceiveProps(nextProps)
    {
        this.setState({ isMinimized: nextProps.isMinimized || false });
    }
    
    handleShareClick()
    {
        const box = this.refs.WidgetBox;
        if (this.props.onShareClick) this.props.onShareClick(box);
    }
    
    handleCloseClick()
    {
        if (confirm("Naozaj chcete odstrániť tento widget?"))
        {
            if (this.props.onCloseClick) this.props.onCloseClick(this.props.index);
        }
    }
    
    handleMinMaxClick()
    {
        this.setState({ isMinimized: !this.state.isMinimized });
    }
    
    handleNavClick()
    {
        
    }
    
    renderTitle(title)
    {
        if (title)
        {
            return (
                <h1 ref="h1" data-original-title={title} data-placement="top">
                    <span ref="title" className="title"><span ref="titleInner">{title}</span></span>
                    <span ref="hellip" className="hellip">…</span>
                </h1>
            );
        }
    }
    
    getTextWidth(text, font)
    {
        let canvas = this.canvas || (this.canvas = document.createElement("canvas"));
        let context = canvas.getContext("2d");
        context.font = font;
        let metrics = context.measureText(text);
        return metrics.width;
    }
    
    findTextWidth(text, availableWidth)
    {
        for (let i = text.length; i > 0; i--)
        {
            let short = text.substring(0, i);
            let width = Math.round(this.getTextWidth(short, "400 16px 'Montserrat'"));
            if (width < availableWidth) return width;
        }
        return availableWidth;
    }
    
    updateTitle()
    {
        if (this.refs.title)
        {
            const availableWidth = this.refs.header.offsetWidth - 165;
            if (this.refs.title.offsetWidth > availableWidth)
            {
                this.refs.hellip.style.display = 'block';
                const titleWidth = this.findTextWidth(this.refs.title.innerText, availableWidth);
                this.refs.title.style.width = `${titleWidth}px`;
                this.refs.h1.setAttribute("data-toggle", "tooltip");
                this.refs.titleInner.style.width = '1000px';
                this.refs.titleInner.style.display = 'block';
            }
        }
    }
    
    renderContent(children)
    {
        if (!this.state.isMinimized)
        {
            return (
                <div className="content" ref="content">
                    {children}
                    {this.renderFooter()}
                </div>
            )
        }
    }
    
    renderWidgetDisabledMessage()
    {
        if (!this.state.isMinimized)
        {
            return (
                <div className="content widget-disabled" ref="content">
                    <div>{lang.WIDGET_JE_NEDOSTUPNY}</div>
                </div>
            )
        }
    }

    renderFooterItems(footer)
    {
        return footer.map((e, i) => <span key={i}>{e}</span>);
    }
    
    renderFooter()
    {
        if (this.props.footer)
        {
            const
                footer = this.props.footer,
                isArray = Object.prototype.toString.call(footer) === '[object Array]';
            return <footer>{isArray ? this.renderFooterItems(footer) : footer}</footer>;
        }
    }
    
    render()
    {
        const
            { children, className, props } = this.getContainerProps("box WidgetBox"),
            { title, theme, seasons, actualSeason, ...tail } = props,
            boxStyle = { background: theme.background },
            buttonStyle = { background: theme.controls.background };
        return (
            <div className={className} {...tail} ref="WidgetBox" style={boxStyle}>
                <header ref="header">
                    {this.renderTitle(title)}
                    <Controls ref="controls">
                        <Button style={buttonStyle} onClick={this.handleShareClick.bind(this)}>
                            <span />
                        </Button>
                        <Button ref="calendar" style={buttonStyle}><span /><b className="calendar-arrow" ref="calendar-arrow" /></Button>
                    </Controls>
                </header>
                {this.props.isEnabled || true ? this.renderContent(children) : this.renderWidgetDisabledMessage()}
            </div>
        );
    }
}