import React from 'react'
import { Widget, WidgetBox } from './Widget.js'
import { FlotChart } from '../../FlotChart.js'
import { lang } from '../../../App/Langs.js'

export class PieChartWidget extends Widget
{
    constructor(props)
    {
        super(props);       
        this.options =
        {
            series:
            {
                pie:
                {
                    show: true,
                    label: {
                        show: false,
                        formatter: (label, series) => `${label}, ${series.data[0][1]}`
                    },
                    innerRadius: 0.6,
                    stroke: { width: 0, color: this.props.theme.background },
                }
            },
            grid: {
                hoverable: true
            },
            legend: { show: true, sorted: "reverse" }
        };
        this.className = "PieChart";
    }
    
    onHover(event, pos, obj)
    {
        let info = $(this.refs.info);
        if (obj)
        {
            const label = this.props.nameStack[obj.series.color];
            info.html(`${label || obj.series.label}: ${obj.series.data[0][1]} ${this.totalSubject}`);
        }
        else
        {
            info.html('');
        }
    }
    
    getHeight()
    {
        return 266;
    }
    
    get total()
    {
        return this.props.model.reduce((acc, e) => acc + parseInt(e.data), 0);
    }
    
    render()
    {
        return (
            <WidgetBox
                isEnabled={this.props.isEnabled}
                userWidget={this.props.userWidget}
                className={this.className}
                index={this.props.index}
                theme={this.props.theme}
                title={this.props.title}
                footer={this.props.footer}
                onShareClick={this.props.onShareClick}
                onCloseClick={this.props.onCloseClick}
                onChangeDate={this.props.onChangeDate}
                isMinimized={this.props.isMinimized}
                seasons={this.props.seasons} actualSeason={this.props.actualSeason}>
                <div ref="info" className="pie-info"></div>
                <FlotChart
                    isEnabled={this.props.isEnabled}
                    width="100%" height={this.getHeight()}
                    model={this.props.model} options={this.options} onHover={this.onHover.bind(this)} />
                <div className="total">
                    <div>
                        <small>{lang.SPOLU}</small>
                        <span>{this.total}</span>
                        <small>{this.totalSubject}</small>
                    </div>
                </div>
            </WidgetBox>
        );
    }
}