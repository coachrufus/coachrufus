import React from 'react'
import { PieChartWidget } from './Base/PieChartWidget.js'
import { lang } from '../../App/Langs.js'

export class GoalsWidget extends PieChartWidget
{
    constructor(props)
    {
        super(props);    
        this.totalSubject = lang.GOLOV;
    }
}