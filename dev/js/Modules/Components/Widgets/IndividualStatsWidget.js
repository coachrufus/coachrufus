import React from 'react'
import ReactDOM from 'react-dom'
import { Widget, WidgetBox } from './Base/Widget.js'
import { lang } from '../../App/Langs.js'
import Immutable from 'immutable'

export class IndividualStatsWidget extends Widget
{
    constructor(props)
    {
        super(props);
        this.state = {};
    }
    
    componentDidMount()
    {
    }

    getMatchString(diff)
    {
        if (diff === 0 || diff > 4) return lang.ZAPASOV;
        else if (diff === 1) return lang.ZAPAS;
        else lang.ZAPASY;
    }

    renderGoalDiff([diff, count])
    {
        return (
            <tr key={diff}>
                <th>{lang.ROZDIEL_O}</th>
                <td>{count}</td>
            </tr>
        );
    }
    
    render()
    {
        const m = this.props.model;
        return (
            <WidgetBox
                isEnabled={this.props.isEnabled}
                userWidget={this.props.userWidget}
                index={this.props.index}
                theme={this.props.theme}
                title={this.props.title}
                footer={this.props.footer}
                onShareClick={this.props.onShareClick}
                onCloseClick={this.props.onCloseClick}
                onChangeDate={this.props.onChangeDate}
                isMinimized={this.props.isMinimized}
                seasons={this.props.seasons} actualSeason={this.props.actualSeason}>
                <div className="data-table-container" style={{ height: 266 }}>
                    <div className="scroll-container">
                        <table className="stats-table individual-stats">
                            {m ? <tbody>
                                <tr>
                                    <td>
                                        <table>
                                            <tbody>
                                            {m.maxGoals.map((e, i) =><tr key={i}><th>{i == 0 ? lang.NAJVYSSI_POCET_GOLOV_V_JEDNOM_ZAPASE : ''}</th><td className="count-col">{e.goals}</td><td className="name-col">{e.player.player_name}</td><td></td></tr>)}
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>

                                <tr>
                                    
                                    <td>
                                        <table>
                                            <tbody>
                                            {m.maxAssists.map((e, i) =><tr key={i}><th>{i == 0 ? lang.NAJVYSSI_POCET_ASISTENCII_V_JEDNOM_ZAPASE : ''}</th><td className="count-col">{e.assists}</td><td className="name-col">{e.player.player_name}</td><td></td></tr>)}
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <table>
                                            <tbody>
                                            {m.maxGA.map((e, i) =><tr key={i}><th>{i == 0 ? lang.NAJVYSSI_POCET_GA_V_JEDNOM_ZAPASE : ''}</th><td className="count-col">{e.goals}+{e.assists}</td><td className="name-col">{e.player.player_name}</td><td></td></tr>)}
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>

                                {m.continual_wins ? <tr>
                                    <td>
                                        <table>
                                            <tbody>
                                            {m.continual_wins.map((e, i) =><tr key={i}><th>{i == 0 ? lang.NAJDLHSIA_SNURA_VYHIER : ''}</th><td className="count-col">{e.counter}</td><td className="name-col">{e.teamPlayer.player_name}</td><td>{e.start_date} - {e.end_date}</td></tr>)}
                                            </tbody>
                                        </table>
                                    </td>
                                </tr> : undefined}

                                {m.continual_looses ? <tr>
                                    <td>
                                        <table>
                                            <tbody>
                                            {m.continual_looses.map((e, i) =><tr key={i}><th>{i == 0 ? lang.NAJDLHSIA_SNURA_PREHIER : ''}</th><td className="count-col">{e.counter}</td><td className="name-col">{e.teamPlayer.player_name}</td><td>{e.start_date} - {e.end_date}</td></tr>)}
                                            </tbody>
                                        </table>
                                    </td>
                                </tr> : undefined}

                                {m.continual_no_wins ? <tr>
                                    <td>
                                        <table>
                                            <tbody>
                                            {m.continual_no_wins.map((e, i) =><tr key={i}><th>{i == 0 ? lang.NAJDLHSIA_SNURA_BEZ_VYHRY : ''}</th><td className="count-col">{e.counter}</td><td className="name-col">{e.teamPlayer.player_name}</td><td>{e.start_date} - {e.end_date}</td></tr>)}
                                            </tbody>
                                        </table>
                                    </td>
                                </tr> : undefined}

                                {m.continual_no_looses ? <tr>
                                    <td>
                                        <table>
                                            <tbody>
                                            {m.continual_no_looses.map((e, i) =><tr key={i}><th>{i == 0 ? lang.NAJDLHSIA_SNURA_BEZ_PREHRY : ''}</th><td className="count-col">{e.counter}</td><td className="name-col">{e.teamPlayer.player_name}</td><td>{e.start_date} - {e.end_date}</td></tr>)}
                                            </tbody>
                                        </table>
                                    </td>
                                </tr> : undefined}

                                {m.playerPairs ? <tr>
                                    <td>
                                        <table>
                                            <tbody>
                                            {m.playerPairs.map((e, i) =><tr key={i}><th>{i == 0 ? lang.NAJCASTEJSIE_SPOLU_HRAJUCE_DVOJICE : ''}</th><td className="count-col">{e.count}x</td><td className="name-col">{e.teamPlayer1.player_name} & {e.teamPlayer2.player_name}</td><td></td></tr>)}
                                            </tbody>
                                        </table>
                                    </td>
                                </tr> : undefined}

                            </tbody> : undefined}
                        </table>
                    </div>
                </div>
            </WidgetBox>
        );
    }
}