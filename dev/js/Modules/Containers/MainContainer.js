import 'babel-polyfill'
import React from 'react'
import themes from '../UI/Themes.js'
import { Row, Column, Box } from '../Components/Containers.js'
import { lang } from '../App/Langs.js';

class WidgetClasses
{
    constructor()
    {
        this.classes = {};
    }
    
    resolve(name)
    {
        if (!this.classes[name])
        {
            this.classes[name] = require(`../Components/Widgets/${name}.js`)[name];
        }
        return this.classes[name];
    }
}

let widgetClasses = new WidgetClasses();

export class MainContainer extends React.Component
{
    handleResize(e)
    {
        this.forceUpdate();
    }
    
    componentDidMount()
    {
        window.addEventListener('resize', this.handleResize.bind(this));
    }

    componentWillUnmount()
    {
        window.removeEventListener('resize', this.handleResize.bind(this));
    }
    
    get seasonName()
    {
        const seasonName = $('#seasonDropdownMenu').find('.value').text();
        return seasonName.substring(0, seasonName.indexOf('(')).trim();
    }

    renderWidget(userWidget, index)
    {
        const
            {
                onShareClick, onCloseClick, onChangeDate, Widgets,
                WidgetModels, WidgetDatas, teamPlayers, teamMates,
                personalForm, seasonStats, individualStats, seasons,
                actualSeason
            } = this.props,
            { name, width, theme, GUID, isEnabled } = userWidget,
            widget = Widgets.get(name),
            model = WidgetModels.get(GUID),
            rawData = WidgetDatas.get(GUID),
            season = window.actualSeason,
            seasonText = `${lang.SEZONA} ${this.seasonName}`;
        switch (name)
        {
            case "SeasonStats":
            {
                var widgetModel = seasonStats;
                break;
            }
            case "IndividualStats":
            {
                var widgetModel = individualStats;
                break;
            }
            case "TeamMates":
            {
                var widgetModel = { teamPlayers, teamMates };
                break;
            }
            case "PersonalForm":
            {
                var widgetModel = personalForm;
                break;
            }
            default:
            {
                var widgetModel = widget.modelCreator(model);
                break;
            }
        }
        if (model)
        {
            return (
                <Column key={index} type={`md-${width}`}>
                    {React.createElement(widgetClasses.resolve(`${name}Widget`), {
                        index,
                        title: widget.title,
                        theme,
                        isEnabled,
                        userWidget: userWidget,
                        ticks: model.ticks,
                        onChangeTeamMatesSelectedPlayer: name === "TeamMates" ? this.props.onChangeTeamMatesSelectedPlayer : undefined,
                        model: widgetModel,
                        nameStack: model.nameStack,
                        footer: [rawData.team.name, seasonText],
                        onShareClick, onCloseClick, onChangeDate,
                        seasons, actualSeason
                    })}
                </Column>
            )
        }
    }
    
    render()
    {

        const 
            { UserWidgets, WidgetModels } = this.props,
            renderWidgets = UserWidgets.count() <= WidgetModels.count();
        return (
            <div className="Main">
                <Row className="connectedSortable">
                    { renderWidgets ? UserWidgets.map(this.renderWidget.bind(this)) : undefined }
                </Row>
            </div>
        )
    }
}