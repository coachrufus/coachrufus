import { WidgetApi } from '../Api/WidgetApi.js'
import { shareOnFacebook } from '../Lib/Facebook.js'

export default function createShareClickHandler(resources)
{
    return function handleShareClick(box)
    {
        resources.addJSOnce("/js/html2canvas.min.js", function()
        {
            html2canvas(box,
            {
                width: 2000,
                onrendered: async function(canvas)
                {
                    const
                        image = canvas.toDataURL("image/png"),
                        widgetApi = new WidgetApi();
                    const guid = await widgetApi.uploadImage(image);
                    //shareOnFacebook(`${window.root}/img/share/${guid}.png`);
                    shareOnFacebook(`${window.root}/share-chart/${guid}`);
                }
            });
        });
    }
}