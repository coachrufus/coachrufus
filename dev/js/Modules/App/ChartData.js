import "babel-polyfill"
import Immutable from "immutable"
import Data from '../Lib/Data.js'

export class ChartData
{
    constructor(data)
    {
        if (data.raw)
        {
            const [columns, ...records] = data.raw;
            this._columns = columns;
            this._records = records;
            this._team = data.team;
        }
        else if (data.chartData)
        {
            ['columns', 'records', 'team'].forEach(p => 
            {
                this[`_${p}`] = data[p] === undefined ?
                    data.chartData[p] : data[p];
            });
        }
    }
    
    filter(filter)
    {
        return new ChartData({
            ...{ chartData: this },
            ...filter({
                columns: this.columns,
                records: this.records
            })
        });
    }
    
    max(column)
    {
        const i = this.columnIndex(column);
        return Math.max.apply(Math, this.records.map(e => e[i]));
    }

    columnIndex(column) { return this.columns.findIndex(c => c === column); }
    
    select(columns)
    {
        const
            columnIndex = columns.map(c => [c, this.columnIndex(c)]),
            columnIndexMap = Immutable.Map(columnIndex);
        return this.records.map((e, i) =>
            Immutable.Map(columns.map(c => [c, e[columnIndexMap.get(c)]])).toJS());
    }
    
    selectAsNum(columns, action)
    {
        const
            columnIndex = columns.map(c => [c, this.columnIndex(c)]),
            columnIndexMap = Immutable.Map(columnIndex);
        return this.records.map((e, i) =>
            Immutable.Map(columns.map(c => [c, c == "Player" ? e[columnIndexMap.get(c)] : Data.parseNum(e[columnIndexMap.get(c)]) ])).toJS());
    }
    
    get team() { return this._team; }
    get columns() { return this._columns; }
    get records() { return this._records; }
}