import Immutable from 'immutable';

export class PersonalFormDataListStore
{
    constructor(data)
    {
        this._data = data;
        this._columns = [ "CRS", "A", "G", "W", "D" ];
        this._labels = this.columns.map(c => ({ label: c }));
    }
    
    getStats(section)
    {
        const items = this._data[section];
        return this.columns.map((c, i) => [i, items[c]]);
    }
    
    getData()
    {
        return [
            { label: "Max", color: "rgba(0, 0, 0, 0)", data: this.getStats("max"), spider: { show: false, fill: false } },
            { label: "Avg", color: "rgba(255, 109, 34, 0.75)", data: this.getStats("avg"), spider: { show: true, fill: true } },
            { label: "Player", color: "rgba(0, 117, 154, 0.75)", data: this.getStats("player"), spider: { show: true, fill: true } },
        ];
    }
    
    get columns()
    {
        return this._columns;
    }
    
    get labels()
    {
        return this._labels;
    }
}