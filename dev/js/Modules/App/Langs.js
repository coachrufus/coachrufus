class LangManager
{
    constructor(lang)
    {
        this._lang = lang;
        this._translations = [];
    }
    
    get lang()
    {
        return this._lang;
    }
    
    translation(lang = null)
    {
        const language = lang || this._lang;
        if (!this._translations[language])
        {
            this._translations[language] = require(`../Langs/${language}.js`).default;
        }
        return this._translations[language];
    }
}

export const langManager = new LangManager(window.lang);
export const lang = langManager.translation();