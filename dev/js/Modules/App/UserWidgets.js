import Guid from '../Lib/Guid.js';

const UserWidgets =
{
    register(userWidget)
    {
        return userWidget.GUID ? userWidget : Object.assign(userWidget, { GUID: Guid.newGuid() });
    },
    
    registerAll(userWidgets)
    {
        return userWidgets.map(UserWidgets.register);
    }
}

export default UserWidgets;