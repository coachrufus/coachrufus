import Immutable from 'immutable';

export class TeamMatesDataListStore
{
    constructor([head, ...rows] )
    {
        this.head = head;
        this.rows = rows;
    }

    getObjectAt(index)
    {
        const row = this.rows[index];
        return Immutable.Map(this.head.map((h, i) => {
            return [h, row[i]];
        })).toJS();
    }
    
    get count() { return this.rows.length; }
}