import "babel-polyfill"
import Immutable from "immutable"

export class SeasonChartData
{
    constructor({ raw, team })
    {
        this._raw = Immutable.Map(raw).valueSeq();
        this._team = team;
    }
    
    get raw() { return this._raw }
    get ticks() { return this.raw.map((e, i) => [e.player.name, i]) }
    get dataSet() { return this.raw.map((e, i) =>
        Immutable.Map(e.items)
            .map((v, k) => [Date.parse(k), v])
            .valueSeq()
            .toArray()
            .sort((a, b)  => a[0] - b[0])) }
    get team() { return this._team; }
}