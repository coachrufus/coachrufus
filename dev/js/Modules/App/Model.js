import Immutable from 'immutable';
import UserWidgetsTools from '../App/UserWidgets.js';

const appModel =
{
    create(object)
    {
        return Object.assign(object, {
            UserWidgets: UserWidgetsTools.registerAll(Immutable.List(object.UserWidgets)),
            WidgetModels: Immutable.Map(),
            WidgetDatas: Immutable.Map()
        });
    }
}

export default appModel;