import { shuffledScaleMap, scaleMap } from '../Lib/Scale.js'
import { hslToRgb } from './HSL.js'

export function palette({ numberOfColours, shuffle = true, alpha = 1 })
{
    return (shuffle ? shuffledScaleMap : scaleMap)({ 
        from: 1,
        max: 1.0,
        count: numberOfColours,
        mapper: (c, i) => {
            const [r, g, b] = hslToRgb(c, 0.5, 0.5);
            return `rgba(${r}, ${g}, ${b}, ${alpha})`;
        }
    });
}
