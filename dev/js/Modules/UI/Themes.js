const themes =
{
    blue:
    {
        background: '#00759a',
        controls: { background: '#076482' }
    },
    green:
    {
        background: '#a8c346',
        controls: { background: '#8da23e' }
    },
    magenta:
    {
        background: '#a560a7',
        controls: { background: '#8b538c' }
    },
    cyan:
    {
        background: '#00b3be',
        controls: { background: '#07959e' }
    },
    gray:
    {
        background: '#37424a',
        controls: { background: '#333b42' }
    }
};

export default themes;