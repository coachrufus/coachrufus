import { combineReducers, createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { chat } from '../Reducers/Chat.js';

const store = createStore(
    chat,
    applyMiddleware(thunk)
);

export default store;