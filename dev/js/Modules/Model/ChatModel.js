import Immutable from "immutable"

const
    selectedContactsJSON = localStorage.getItem("selectedContacts"),
    selectedContacts = selectedContactsJSON ? 
        JSON.parse(selectedContactsJSON).map(id => isNaN(id) ? id : parseInt(id)) : null;

function isCollapsed()
{
    if (localStorage.getItem("chatCollapsed") == null)
    {
        return false;
    }
    else
    {
        return JSON.parse(localStorage.getItem("chatCollapsed"));
    }
}

export default {
    messages: [],
    contacts: [],
    myContact: null,
    selectedContacts: selectedContacts ? Immutable.Set(selectedContacts) : Immutable.Set(),
    scrollDown: false,
    isCollapsed: isCollapsed(),
    unreaded: 0
}