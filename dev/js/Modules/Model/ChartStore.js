import { combineReducers, createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { chart } from '../Reducers/Chart.js';

const store = createStore(
    chart,
    applyMiddleware(thunk)
);

export default store;