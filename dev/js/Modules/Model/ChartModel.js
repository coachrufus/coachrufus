import "babel-polyfill"
import Immutable from "immutable"
import themes from '../UI/Themes.js';
import appModel from '../App/Model.js';
import { lang } from '../App/Langs.js';
import { TeamMatesDataListStore } from '../App/TeamMatesDataListStore.js';
import { PersonalFormDataListStore } from '../App/PersonalFormDataListStore.js';
import { CRSColumns } from '../Filters/CRS.js';

let Widgets = Immutable.Map(
{
     CRS: { columns: CRSColumns.toJS(), modelCreator: (data) => data.dataSet, title: lang.CELKOVE_PORADIE_PODLA_CRS }, 
     GP: { columns: ['GP'], modelCreator: (data) => [data.items], title: lang.POCET_ODOHRANYCH_ZAPASOV },
    PERC: { columns: ['%'], modelCreator: (data) => [data.items], title: lang.PERCENTUALNA_USPESNOST_VYHIER },
    PM: { columns: ['+/-'], modelCreator: (data) => [data.items], title: lang.CELKOVE_PORADIE_PLUS_MINUS },
    AvG: { columns: ['avG'], modelCreator: (data) => [data.items], title: lang.GOLY_PRIEMER },
    AvA: { columns: ['avA'], modelCreator: (data) => [data.items], title: lang.ASISTENCIE_PRIEMER },
    AvGA: { columns: ['avGA'], modelCreator: (data) => [data.items], title: lang.KANADSKE_BODY_PRIEMER },
    Goals: { columns: ['Goals'], modelCreator: (data) => data.values, title: lang.GOLY_CELKOVO },
    Assists: { columns: ['Assists'], modelCreator: (data) => data.values, title: lang.ASISTENCIE_CELKOVO },
    AvKBI: { columns: ['avKBI'], modelCreator: (data) => [data.items], title: lang.CRS_PRIEMER },
    MoM: { columns: ['Man of match', 'M%M'], modelCreator: (data) => data.items, title: lang.CELKOVE_PORADIE_PODLA_MOM },
    SeasonRank: { columns: ['SeasonRate'], modelCreator: (data) => data.dataSet, title: lang.VYVOJ_PORADIA_POCCAS_SEZONY },
    SeasonKBI: { columns: ['SeasonKBI'], modelCreator: (data) => data.dataSet, title: lang.VYVOJ_CRS_POCAS_SEZONY },
    TeamMates: { columns: [], modelCreator: (data) => data, title: lang.TEAM_MATES },
    PersonalForm: { columns: [], modelCreator: (data) => data, title: lang.PERSONAL_FORM },
    SeasonStats: { columns: [], modelCreator: (data) => data, title: lang.SEASON_STATS },
    IndividualStats: { columns: [], modelCreator: (data) => data, title: lang.INDIVIDUAL_STATS }
});

class PackageProductsDataListStore
{
    constructor(packageProducts)
    {
        this.productMap = Immutable.Map(packageProducts);
        
        console.log( this.productMap);
    }

    isEnabled(key)
    {
        var product = this.productMap.get(key);
        return product !== undefined && parseInt(product.is_enabled) === 1;
    }
}

let packageProducts = new PackageProductsDataListStore(window.packageProducts);

let UserWidgets =
[
    //{ name: 'SeasonKBI', width: 12, theme: themes.cyan, options: {} },
    //{ name: 'SeasonRank', width: 12, theme: themes.magenta, options: {} },
     { name: 'CRS', width: 12, theme: themes.gray, options: {}, isEnabled: packageProducts.isEnabled('graf-crs-riadkovy-sumarny') },
    { name: 'PersonalForm', width: 4, theme: themes.magenta, options: {}, isEnabled: packageProducts.isEnabled('personal-form') },
    { name: 'Goals', width: 4, theme: themes.blue, options: {}, isEnabled: packageProducts.isEnabled('grafy-g,a,ga,mom') },
    { name: 'Assists', width: 4, theme: themes.magenta, options: {}, isEnabled: packageProducts.isEnabled('grafy-g,a,ga,mom') },
    { name: 'TeamMates', width: 12, theme: themes.blue, options: {}, isEnabled: packageProducts.isEnabled('teammates') },
    { name: 'GP', width: 4, theme: themes.magenta, options: {}, isEnabled: packageProducts.isEnabled('gp-perc-pm') },
    { name: 'PERC', width: 4, theme: themes.cyan, options: {}, isEnabled: packageProducts.isEnabled('gp-perc-pm') },
    { name: 'PM', width: 4, theme: themes.blue, options: {}, isEnabled: packageProducts.isEnabled('gp-perc-pm') },
    { name: 'AvG', width: 4, theme: themes.gray, options: {}, isEnabled: true },
    { name: 'AvA', width: 4, theme: themes.green, options: {}, isEnabled: true },
    { name: 'AvGA', width: 4, theme: themes.cyan, options: {}, isEnabled: true },
    { name: 'AvKBI', width: 4, theme: themes.gray, options: {}, isEnabled: true },
    { name: 'MoM', width: 4, theme: themes.blue, options: {}, isEnabled: packageProducts.isEnabled('grafy-g,a,ga,mom') },
    { name: 'SeasonStats', width: 4, theme: themes.blue, options: {}, isEnabled: true },
    { name: 'IndividualStats', width: 12, theme: themes.gray, options: {}, isEnabled: true }
];

const mainTeamPlayer = window.teamPlayers.find(tp => tp.playerId == window.userId);
const anotherTeamPlayers = window.teamPlayers.filter(tp => tp.playerId != window.userId)
    .sort((tp1, tp2) => tp1.fullName.localeCompare(tp2.fullName));
const teamPlayersVK = [mainTeamPlayer, ...anotherTeamPlayers].map(tp =>
    [tp.id, tp.fullName])

const model = appModel.create({
    Widgets,
    //UserWidgets: window.UISettings.userWidgets ? window.UISettings.userWidgets : UserWidgets,
    UserWidgets:  UserWidgets,
    WidgetModels: null,
    WidgetDatas: null,
    data: null,
    teamId: null,
    teamPlayers: teamPlayersVK,
    teamMates: new TeamMatesDataListStore(window.teamMates),
    personalForm: new PersonalFormDataListStore(window.personalForm),
    seasonStats: window.seasonStats,
    individualStats: window.individualStats,
    actualSeason: window.actualSeason,
    seasons: window.seasons
});

export default model;