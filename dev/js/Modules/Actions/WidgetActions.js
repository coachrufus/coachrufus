import Immutable from 'immutable'
import { PlayerApi } from '../Api/PlayerApi.js'
import { WidgetApi } from '../Api/WidgetApi.js'
import model from '../Model/ChartModel.js'
import Arr from '../Lib/Array.js'
import Data from '../Lib/Data.js'
import { ChartData } from '../App/ChartData.js'
import { TeamMatesDataListStore } from '../App/TeamMatesDataListStore.js'
import { PersonalFormDataListStore } from '../App/PersonalFormDataListStore.js'

const playerApi = new PlayerApi();
const widgetApi = new WidgetApi();

export function setTeamId(teamId)
{
    return { type: "SET_TEAM_ID", teamId };
}

export async function fetchWidgetData({ teamId, userWidgets, all = true, dispatch })
{
    const result = await playerApi.getWidgetData({
        teamId,
        userWidgets: userWidgets
            .map(uw => Object.assign(uw, { columns: model.Widgets.get(uw.name).columns }))
            .toJS()
    });
    
    const data = Immutable.Map(result.data).map(raw =>
    {
        if (raw.column)
        {
            return new SeasonChartData({ raw: raw.data, team: result.team });
        }
        else
        {
            return new ChartData({ raw, team: result.team })
                .filter(({ records }) => ({ records: records }));
        }
    });
    dispatch({ type: "FETCH_WIDGET_DATA", data, all });
}

export async function fetchTeamMatesData({ userWidgets, dispatch })
{
    const
        data = await playerApi.getTeamMatesData({ userWidgets }),
        teamMates = new TeamMatesDataListStore(data);
    dispatch({ type: "FETCH_TEAM_MATES_DATA", teamMates });
}

export async function fetchPersonalFormData({ userWidgets, dispatch })
{
    const
        data = await playerApi.getPersonalFormData({ userWidgets }),
        personalForm = new PersonalFormDataListStore(data);
    dispatch({ type: "FETCH_PERSONAL_FORM_DATA", personalForm });
}

export function closeWidget(index)
{
    return { type: "CLOSE_WIDGET", index };
}

export async function updateWidgets(userWidgets)
{
    const result = await widgetApi.saveSettings({ userWidgets });
    return { type: "UPDATE_USER_WIDGETS", userWidgets };
}