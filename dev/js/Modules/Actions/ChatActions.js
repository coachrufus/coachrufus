import Immutable from 'immutable'
import { ChatApi } from '../Api/ChatApi.js'
import model from '../Model/ChatModel.js'
import moment from 'moment'
import { lang } from '../App/Langs.js'

class ChatDataConverter
{
    constructor(userId)
    {
        this.userId = window.userId;
    }
    
    getContactMap(contacts)
    {
        return Immutable.Map(contacts.map(c => [parseInt(c.id), c]));
    }
    
    toDate(sqlDate)
    {
        var t = sqlDate.split(/[- :]/);
        return new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);
    }
    
    convertMessage(message, contactMap, myContact)
    {
        const
            isMy = myContact.id == message.from_user_id,
            contact = isMy ? myContact : contactMap.get(parseInt(message.from_user_id)),
            date = moment(this.toDate(message.datetime)).locale(lang.LOCALE);
        return {
            id: parseInt(message.id),
            contact,
            date,
            dateText: `${date.fromNow()} ${date.format('LTS')}`,
            body: message.body,
            isReaded: message.is_readed,
            isMy
        }
    }
    
    convert({ contacts, messages, unreaded })
    {
        const contactMap = this.getContactMap(contacts);
        const myContact = contactMap.get(this.userId);
        return {
            contacts: contacts.map(c => 
            {
                c.isMy = myContact.id == c.id;
                return c;
            }),
            messages: messages.reverse().map(m => this.convertMessage(m, contactMap, myContact)),
            myContact,
            unreaded
        }
    }
}

const chatApi = new ChatApi();
const chatDataConverter = new ChatDataConverter();


export async function sendMessage(recipients, message)
{
    const chat = await chatApi.sendMessage(recipients, message);
    return { type: "REFRESH_CHAT", ...chatDataConverter.convert(chat), scrollDown: true };
}

export async function refreshChat()
{
    const chat = await chatApi.refresh()
    return { type: "REFRESH_CHAT", ...chatDataConverter.convert(chat), scrollDown: false };
}

export async function markAsReaded()
{
    await chatApi.markAsReaded()
    return { type: "MARK_AS_READED" };
}

export function selectContact(userId)
{
    return { type: "SELECT_CONTACT", userId };
}

export function unselectContact(userId)
{
    return { type: "UNSELECT_CONTACT", userId };
}

export function selectAllContacts()
{
    return { type: "SELECT_ALL_CONTACTS" };
}

export function unselectAllContacts()
{
    return { type: "UNSELECT_ALL_CONTACTS" };
}