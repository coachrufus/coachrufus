import 'babel-polyfill'
import React from 'react'
import ReactDOM from 'react-dom'
import Immutable from 'immutable'
import { Chat } from './Components/Chat/Chat.js'

let chatRoot = document.getElementById("chat-root");

ReactDOM.render(<Chat messages={[
    { nickname: 'Alexander Pierce', timestamp: '23 Jan 2:00 pm', image: 'user1-128x128.jpg', body: 'Is this template really for free? That\'s unbelievable!' },
    { nickname: 'Sarah Bullock', timestamp: '23 Jan 2:05 pm', image: 'user3-128x128.jpg', body: 'You better believe it!' }
]} />, chatRoot);