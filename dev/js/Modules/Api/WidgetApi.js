import user from '../Lib/User.js'

export class WidgetApi
{   
    uploadImage(data)
    {
        return new Promise(function(resolve, reject)
        {            
            $.ajax(
            {
                url: `/widget/api/upload-image`,
                method: 'POST',
                data,
                success: resolve,
                error: reject
            });
        });
    }
    
    saveSettings(data)
    {
        return new Promise(function(resolve, reject)
        {            
            $.ajax(
            {
                url: `/widget/api/save-settings/${user.id}`,
                method: 'POST',
                data: JSON.stringify(data),
                success: resolve,
                error: reject
            });
        });
    }
    
    changeSeason({ teamId, seasonId })
    {
        return new Promise(function(resolve, reject)
        {            
            $.ajax(
            {
                url: `/widget/api/season/${teamId}/${seasonId}`,
                method: 'POST',
                success: resolve,
                error: reject
            });
        });
    }
}