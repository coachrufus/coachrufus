export class ChatApi
{   
    sendMessage(recipients, message)
    {
        return new Promise(function(resolve, reject)
        {
            $.ajax({
                url: `/socializing/chat/send`,
                method: 'POST',
                data: { recipients: recipients.toJS(), message },
                success: (content) => resolve(content),
                error: reject
            });
        });
    }
    
    refresh()
    {
        return new Promise(function(resolve, reject)
        {
            $.ajax({
                url: `/socializing/chat/refresh`,
                method: 'POST',
                data: {},
                success: (content) => resolve(content),
                error: reject
            });
        });
    }
    
    markAsReaded()
    {
        return new Promise(function(resolve, reject)
        {
            $.ajax({
                url: `/socializing/chat/mark-as-readed`,
                method: 'POST',
                data: {},
                success: (content) => resolve(content),
                error: reject
            });
        });
    }
}