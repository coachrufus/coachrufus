export class PlayerApi
{   
    getWidgetData({ teamId, userWidgets })
    {
        return new Promise(function(resolve, reject)
        {
            $.ajax({
                url: `/widget/api/widget-data/${teamId}`,
                method: 'POST',
                data: { userWidgets: JSON.stringify(userWidgets) },
                success: (content) => resolve(content),
                error: reject
            });
        });
    }
    
    getTeamMatesData({ userWidgets })
    {
        return new Promise(function(resolve, reject)
        {
            $.ajax({
                url: `/widget/api/team-mates`,
                method: 'POST',
                data: JSON.stringify(userWidgets),
                success: (content) => resolve(content),
                error: reject
            });
        });
    }
    
    getPersonalFormData({ userWidgets })
    {
        return new Promise(function(resolve, reject)
        {
            $.ajax({
                url: `/widget/api/personal-form`,
                method: 'POST',
                data: JSON.stringify(userWidgets),
                success: (content) => resolve(content),
                error: reject
            });
        });
    }
}