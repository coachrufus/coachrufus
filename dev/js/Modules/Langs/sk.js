export default
{
    LOCALE: "sk_SK",
    
    REMIZY: "Remízy",
    VYHRY: "Výhry",
    GOLY: "Góly",
    ASISTENCIE: "Asistencie",
    MOM: "MOM",

    // Názvy widgetov
    POCET_ODOHRANYCH_ZAPASOV: "Počet odohraných zápasov",
    PERCENTUALNA_USPESNOST_VYHIER: "Percentuálna úspešnosť výhier",
    CELKOVE_PORADIE_PLUS_MINUS: "Celkové poradie +/-",
    GOLY_PRIEMER: "Góly priemer",
    ASISTENCIE_PRIEMER: "Asistencie priemer",
    KANADSKE_BODY_PRIEMER: "Kanadské body priemer",
    GOLY_CELKOVO: "Góly celkovo",
    ASISTENCIE_CELKOVO: "Asistencie celkovo",
    CELKOVE_PORADIE_PODLA_CRS: "Celkové poradie podľa CRS",
    CRS_PRIEMER: "CRS Priemer",
    CELKOVE_PORADIE_PODLA_MOM: "Celkové poradie podľa MoM",
    VYVOJ_PORADIA_POCCAS_SEZONY: "Vývoj poradia počas sezóny",
    VYVOJ_CRS_POCAS_SEZONY: "Vývoj CRS počas sezóny",
    TEAM_MATES: "Team mates",
    PERSONAL_FORM: "Osobná forma",
    SEASON_STATS: "Celosezónne štatistiky",
    INDIVIDUAL_STATS: "Individuálne štatistiky",

    TEAM_MAX: "Maximum tímu",
    TEAM_AVG: "Priemer tímu",
    
    //Widget - footer
    NAZOV_TEAMU: "Názov tímu",

    SPOLU: "Spolu",
    GOLOV: "Gólov",
    ASISTENCII: "Asistencií",
    
    // filtrovanie podla datumu
    
    VYBERTE_INTERVAL_OD_DO: "vyberte interval od - do",
    DATUM_OD: "dátum od",
    DATUM_DO: "dátum do",
    
    ZVOLIT: "Zvoliť",
    ZRUSIT: "Zrušiť",
    
    FORMAT_DATUMU: 'DD.MM.YYYY',
    DNI: ['Nedeľa', 'Pondelok', 'Utorok', 'Streda', 'Štvrtok', 'Piatok', 'Sobota'],
    MESIACE: ['Január', 'Februáry', 'Marec', 'Apríl', 'Máj', 'Jún', 'Júl', 'August', 'September', 'Október', 'November', 'December'],
    
    OSTATNI: "Ostatní",
    SEZONA: "Sezóna",

    ZACIATOK_SEZONY: "Začiatok sezóny",
    KONIEC_SEZONY: "Koniec sezóny",
    CELKOVY_POCET_ZAPASOV: "Celkový počet zápasov",
    CELKOVY_POCET_GOLOV: "Celkový počet gólov",
    CELKOVY_POCET_ZUCASTNENYCH_HRACOV: "Celkový počet zúčastnených hráčov",
    PRIEMERNY_POCET_GOLOV_NA_ZAPAS: "Priemerný počet gólov na zápas",
    NAJVYSSI_POCET_GOLOV_V_JEDNOM_ZAPASE: "Najvyšší počet gólov v jednom zápase",
    NAJNIZSI_POCET_GOLOV_V_JEDNOM_ZAPASE: "Najnižší počet gólov v jednom zápase",
    NAJVYSSI_ROZDIEL_V_SCORE: "Najvišší rozdiel v score",
    REMIZY: "Remízy",
    ROZDIEL_O: "Rozdiel o",
    GOL: "gól",
    GOLY: "góly",
    GOLOV: "gólov",
    NAJVYSSI_POCET_GOLOV_V_JEDNOM_ZAPASE: "Najvyšší počet gólov v jednom zápase",
    NAJVYSSI_POCET_ASISTENCII_V_JEDNOM_ZAPASE: "Najvyšší počet asistencií v jednom zápase",
    NAJVYSSI_POCET_GA_V_JEDNOM_ZAPASE: "Najvyšší počet G+A v jednom zápase",
    NAJVYSSI_CRS_V_JEDNOM_ZAPASE: "Najvyšší CRS v jednom zápase",
    NAJDLHSIA_SNURA_VYHIER: "Najdlhšia šnúra výhier",
    NAJDLHSIA_SNURA_PREHIER: "Najdlhšia šnúra prehier",
    NAJDLHSIA_SNURA_BEZ_VYHRY: "Najdlhšia šnúra bez výhry",
    NAJDLHSIA_SNURA_BEZ_PREHRY: "Najdlhšia šnúra bez prehry",
    NAJNIZSI_POCET_GA_NA_MOM: "Najnižší počet G+A na MoM",
    NAJVYSSI_POCET_GA_BEZ_MOM: "Najvyšší počet G+A bez MoM",
    NAJCASTEJSIE_SPOLU_HRAJUCE_DVOJICE: "Najčastejšie spolu hrajúce dvojice",
    NAJUSPESNEJSIA_DVOJICA: "Najúspešnejšia dvojica",
    NAJNEUSPESNEJSIA_DVOJICA: "Najneúspešnejšia dvojica",
    ZAPAS: "zápas",
    ZAPASY: "zápasy",
    ZAPASOV: "zápasov",
    WIDGET_JE_NEDOSTUPNY: "Widget je nedostupný",
    CHOOSE_PLAYER: "Vyberte hráča"
}