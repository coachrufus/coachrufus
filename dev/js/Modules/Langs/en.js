export default
{
    LOCALE: "en_EN",
    
    REMIZY: "Draws",
    VYHRY: "Wins",
    GOLY: "Goals",
    ASISTENCIE: "Assistance",
    MOM: "MOM",

    // Názvy widgetov
    POCET_ODOHRANYCH_ZAPASOV: "Number of games played",
    PERCENTUALNA_USPESNOST_VYHIER: "Percentage wins",
    CELKOVE_PORADIE_PLUS_MINUS: "General +/-",
    GOLY_PRIEMER: "Goals average",
    ASISTENCIE_PRIEMER: "Assists average",
    KANADSKE_BODY_PRIEMER: "Canadian scoring average",
    GOLY_CELKOVO: "Goals total",
    ASISTENCIE_CELKOVO: "Assists total",
    CELKOVE_PORADIE_PODLA_CRS: "Overall ranking by CRS",
    CRS_PRIEMER: "CRS average",
    CELKOVE_PORADIE_PODLA_MOM: "Overall ranking by MoM",
    VYVOJ_PORADIA_POCCAS_SEZONY: "Ranking progress during the season",
    VYVOJ_CRS_POCAS_SEZONY: "CRS progress during the season",
    TEAM_MATES: "Team mates",
    PERSONAL_FORM: "Personal form",
    SEASON_STATS: "Season stats",
    INDIVIDUAL_STATS: "Individual stats",

    TEAM_MAX: "Team maxiimum",
    TEAM_AVG: "Team average",
    
    //Widget - footer
    NAZOV_TEAMU: "Team title",

    SPOLU: "Total",
    GOLOV: "Goals",
    ASISTENCII: "Assists",
    
    // filtrovanie podla datumu
    
    VYBERTE_INTERVAL_OD_DO: "select  interval from - to",
    DATUM_OD: "date from",
    DATUM_DO: "date  to",
    
    ZVOLIT: "Select",
    ZRUSIT: "Cancel",
    
    FORMAT_DATUMU: 'DD.MM.YYYY',
    DNI: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
    MESIACE: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
    
    OSTATNI: "Rest of team",
    SEZONA: "Season",

    ZACIATOK_SEZONY: "Season start",
    KONIEC_SEZONY: "End of season",
    CELKOVY_POCET_ZAPASOV: "Total match count",
    CELKOVY_POCET_GOLOV: "Total goal count",
    CELKOVY_POCET_ZUCASTNENYCH_HRACOV: "Total count of attendents",
    PRIEMERNY_POCET_GOLOV_NA_ZAPAS: "Average goals for match",
    NAJVYSSI_POCET_GOLOV_V_JEDNOM_ZAPASE: "Most goals in one match",
    NAJNIZSI_POCET_GOLOV_V_JEDNOM_ZAPASE: "Lowest number of goals in one match",
    NAJVYSSI_ROZDIEL_V_SCORE: "Biggest score difference",
    REMIZY: "Draws",
    ROZDIEL_O: "Difference of",
    GOL: "Goal",
    GOLY: "Goals",
    GOLOV: "Goals",
    NAJVYSSI_POCET_GOLOV_V_JEDNOM_ZAPASE: "Most goals in one match",
    NAJVYSSI_POCET_ASISTENCII_V_JEDNOM_ZAPASE: "Most assists in one match",
    NAJVYSSI_POCET_GA_V_JEDNOM_ZAPASE: "Most G+A in one match",
    NAJVYSSI_CRS_V_JEDNOM_ZAPASE: "Biggest CSR in one match",
    NAJDLHSIA_SNURA_VYHIER: "Most wins in a row",
    NAJDLHSIA_SNURA_PREHIER: "Most losses in a row",
    NAJDLHSIA_SNURA_BEZ_VYHRY: "Losing streak",
    NAJDLHSIA_SNURA_BEZ_PREHRY: "Winning streak",
    NAJNIZSI_POCET_GA_NA_MOM: "Lowest G+A to MoM",
    NAJVYSSI_POCET_GA_BEZ_MOM: "Highest  G+A without MoM",
    NAJCASTEJSIE_SPOLU_HRAJUCE_DVOJICE: "Most often teamates",
    NAJUSPESNEJSIA_DVOJICA: "The most successful teamates",
    NAJNEUSPESNEJSIA_DVOJICA: "The most unsuccessful teamates",
    ZAPAS: "match",
    ZAPASY: "matches",
    ZAPASOV: "matches",
    WIDGET_JE_NEDOSTUPNY: "Widget is not available",
    CHOOSE_PLAYER: "Choose player"
}