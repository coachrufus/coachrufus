import { turncate } from './StringUtils.js'

function parseNum(input)
{
    const value = parseFloat(input);
    return isNaN(value) ? 0.0 : value;
}

class Names
{
    constructor(length)
    {
        this._counter = length;
    }
    
    getName(a, max)
    {
        const index = this._counter;
        this._counter--;
        const idx = a.index === undefined ? index : a.index;
        return `${idx === '' ? '' : idx + '. '}${turncate(a.Player, max || 8)}  `;
    }
}

function names(length)
{
    return new Names(length);
}

export default { parseNum, names }