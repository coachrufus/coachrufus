import user from './User.js'
import Immutable from 'immutable'
import { lang } from '../App/Langs.js';

export function asc(column)
{
    return this.sort((a, b) => b[column] - a[column]);
}

export function desc(column)
{
    return this.sort((a, b) => a[column] - b[column]);
}

export function elimit(count)
{
    return this.slice(this.length - count);
}

export function descElimit(column, count)
{
    return this::desc(column)::elimit(count);
}

export function graphFilter(column, count, sorter)
{
    var sorted = sorter ? this.sort(sorter) : this::desc(column);
    var data = sorted::elimit(count || 10);
    const index = data.findIndex(e => e.ID == user.id);
    if (index === -1)
    {
        let playerIndex = sorted.findIndex(e => e.ID == user.id);
        if (playerIndex !== -1)
        {
            let currentPlayer = this[playerIndex];
            let index = sorted.length - playerIndex;
            data = [{ ...currentPlayer, i: true, index }, ...data.slice(0, data.length - 1)];
        }
    }
    else
    {
        let currentPlayer = data[index];
        data[index] = { ...currentPlayer, i: true };
    }
    return data;
}

export function pieGraphFilter(column, count)
{
    const data = Immutable.List(this::graphFilter(column, count));
    const ids = data.map(e => e.Player).toSet();
    const sum = this.filter(e => !ids.contains(e.Player))
        .map(e => e[column])
        .reduce((acc, e) => acc + e, 0.0);
    return data.insert(0, { Player: lang.OSTATNI, [column]: sum, isAnother: true, index: '' }).toJS();
}