export function shuffle()
{
    const a = this.slice(0);
    let j, x, i;
    for (i = a.length; i; i -= 1)
    {
        j = Math.floor(Math.random() * i);
        x = a[i - 1];
        a[i - 1] = a[j];
        a[j] = x;
    }
    return a;
}