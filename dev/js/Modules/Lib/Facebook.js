function removeProtocolFromLink(link)
{
  const
    delimiter = '://',
    index = link.indexOf(delimiter);
  return index === -1 ? link : link.substring(index + delimiter.length);
}

function getFacebookShareUrl(url)
{
    return `https://www.facebook.com/sharer/sharer.php?u=${removeProtocolFromLink(url)}`;
}

export function shareOnFacebook(url)
{
    //window.location = getFacebookShareUrl(url);
    
     FB.ui({
             method: 'share',
             display: 'popup',
             href: url,
             hashtag: '#coachrufus'
           }, function(response){
               
           });
}