export class User
{
    get id() { return window.userId; }
}

export default new User();