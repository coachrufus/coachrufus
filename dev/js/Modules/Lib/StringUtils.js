export function turncate(str, maxLength, char = '… ')
{
    return str.length > maxLength ? 
        str.substring(0, maxLength - char.length) + char : str;
}