
var TeamWallManager = function (data)
{
    this.widgetContainer = $('.wall-widget');
    
     if (typeof data === 'undefined') 
     { 
         this.dataUrl = null;
     }
     else
     {

          this.dataUrl = data.dataUrl || null;
     }

    
    this.parseUrl = true;
    this.currentParseUrl  ='';
    this.omittedUrl = [];
    this.post_image_modal = $('#wall-photo-modal');
};

TeamWallManager.prototype.showLoader = function ()
{
    this.widgetContainer.html(' <div class="alert alert-info text-center"><i class="fa fa-refresh fa-spin fa-3x fa-fw"></i></div>Loading');
};

TeamWallManager.prototype.hideLoader = function ()
{
    this.widgetContainer.html('');
};



TeamWallManager.prototype.loadWidget = function () {
    this.showLoader();
    var manager = this;

    action = this.dataUrl;
    console.log(manager);
    
    $.ajax({
        method: "GET",
        url: action,
    }).done(function (response) {
        manager.widgetContainer.html(response);
        //manager.bindTriggers();
    });
    
};

TeamWallManager.prototype.sendWallPost = function(){
        manager = this;
        manager.updatePostBody();
        var form = $('.team-wall-post-form');
        var data = form.serialize();
        var action = form.attr('action');
        $.ajax({
            method: "POST",
            url: action,
            data: data,
            dataType: 'json'
        }).done(function (response) {
            form.find('.control-label-error').remove();
            if (response.result == 'ERROR')
            {
                $.each(response.errors, function (key, val) {
                    form.find('[data-error-bind="' + key + '"]').append('<span class="control-label-error">' + val + '</span>');
                });
            }
            else
            {
                $('#post_add_content_wrap').html('');
                $('#post_add_image_wrap').html('');
            
                $('#record_body').html('');
            
                $('#wall_posts').prepend(response.item);
            }
        });
};


TeamWallManager.prototype.bindTriggers = function () {
    
    var manager = this;
    $('.team-wall-post-form').on('submit', function (e) {
         e.preventDefault();
         manager.sendWallPost();
    });
    
    $('#record_body').keydown(function(e){
        if(e.keyCode == 13)
        {
            manager.sendWallPost();
        }
    });
    
   
    
    $(document).on('click','.post_comment_trigger',function(e){
        e.preventDefault();
        var action = $(this).attr('data-action');
        var post_id = $(this).attr('data-post-id');
        var comment_body = $('#post-comment-body-'+post_id).val();
        var data =  {'comment':{'body':comment_body,'post_id':post_id}};
         $.ajax({
            method: "POST",
            url: action,
            data: data,
            dataType: 'json'
        }).done(function (response) {
            $('#post-'+post_id).find('.control-label-error').remove();
            if (response.result == 'ERROR')
            {
                $.each(response.errors, function (key, val) {
                    $('#post-'+post_id).find('[data-error-bind="' + key + '"]').append('<span class="control-label-error">' + val + '</span>');
                });
            }
            else
            {
               $('#post-'+post_id).find('#post_reaction_'+post_id).before(response.item);
           
                //$('#post-'+post_id).append(response.item);
            }
        });
    });
    
    
    $(document).on('click','.show-add-comments-trigger, .show-comments-trigger',function(e){
        $(this).parent('.post').find('.post_comment_wrap_cnt').css('display','inline-block');
        $(this).parent('.post').find('.comment_send_wrap').css('display','inline-block');

    });
    
    
     $(document).on('click','.show-comments-trigger',function(e){
        e.preventDefault();
        
        var data_target_id = $(this).attr('data-target');
        var data_target = $('#'+data_target_id);
        var action = $(this).attr('href');
        
        if(data_target.hasClass('active'))
        {
            
             /*
            data_target.hide();
            data_target.removeClass('active');
             */
        }
        else
        {
            if(!data_target.hasClass('loaded'))
            {
                 $.ajax({
                    method: "POST",
                    url: action,
                    dataType: 'json'
                }).done(function (response) {


                   $.each(response.lines, function (key, val) {
                       data_target.append(val);
                    });
                    
                    //postReaction
                    if(response.hasOwnProperty('postReaction'))
                    {
                        data_target.append(response.postReaction);
                    }
                    
                    data_target.find('.post_comment_wrap_cnt').show();
                    data_target.find('.comment_send_wrap').show();

                });
                 data_target.addClass('loaded');
            }
           
            
            data_target.show();
            data_target.addClass('active');
           
        }
        
        
    });
    
   
    //this.addUrlPostTrigger();
    //this.postFileuploadTrigger();
   // this.addPostImageTrigger($('#add-wall-post-photo'));
    //this.addPostDeleteImageTrigger();
    this.addLoadPostsTrigger();
    //this.addMentionTrigger();
    this.addLikeTrigger();
    this.removeLikeTrigger();
    this.deletePostTrigger();
};


TeamWallManager.prototype.deletePostTrigger = function()
{
    $(document).on('click','.delete-post-trigger',function(e){
         e.preventDefault();
        $('#removePostModal .modal_submit').attr('data-href',$(this).attr('href'));
        $('#removePostModal .modal_submit').attr('data-target',$(this).attr('data-target'));
        $('#removePostModal').modal('show');

    });
    
    $(document).on('click','#removePostModal .modal_submit',function(){
           //location.href=$(this).attr('data-href');
           var action = $(this).attr('data-href');
           var target_post = $(this).attr('data-target');
            $.ajax({
            method: "GET",
            url: action,
            dataType: 'json'
                }).done(function (response) {
                   
                    if (response.result == 'ERROR')
                    {
                        alert('error');
                    }
                    else
                    {
                        $('#'+target_post).fadeOut();
                        $('#removePostModal').modal('hide');
                    }
                });
           
       });

};





TeamWallManager.prototype.removeLikeTrigger = function(){
     

    $(document).on('click','.post-unlike-trigger',function(e){
        e.preventDefault();
        var trigger = $(this);
        var action = $(this).attr('href');
      
         $.ajax({
            method: "POST",
            url: action,
            dataType: 'json'
        }).done(function (response) {
            if (response.result == 'ERROR')
            {
                system_alert = new SystemAlert();
                system_alert.showErrorAlert();
            }
            else
            {
               //trigger.find('.likes_count').text('('+response.likesCount+')');
               trigger.attr('href',response.linkurl);
               trigger.removeClass('post-unlike-trigger');
               trigger.addClass('post-like-trigger');
                trigger.text(response.linktext);
            }
            
        });
    });
} ;


TeamWallManager.prototype.addLikeTrigger = function(){
     

    $(document).on('click','.post-like-trigger',function(e){
        e.preventDefault();
        var trigger = $(this);
        var action = $(this).attr('href');
      
         $.ajax({
            method: "POST",
            url: action,
            dataType: 'json'
        }).done(function (response) {
            if (response.result == 'ERROR')
            {
                system_alert = new SystemAlert();
                system_alert.showErrorAlert();
            }
            else
            {
               //trigger.find('.likes_count').text('('+response.likesCount+')');
              trigger.removeClass('post-like-trigger');
               trigger.addClass('post-unlike-trigger');
               trigger.attr('href',response.linkurl);
               trigger.text(response.linktext);
            }
            
        });
    });
} ;

TeamWallManager.prototype.updatePostBody = function(){
     $('#post_body').val($('#record_body').html());
};

TeamWallManager.prototype.addMentionTrigger = function ()
{
   
   $('#record_body').textcomplete([
  { // mention strategy
    match: /(^|\s)@(\w*)$/,
    search: function (term, callback) {
        $.getJSON('/player/mentions', { q: term})
        .done(function (resp) {
          callback(resp.mentions); // `resp` must be an Array
        })
        .fail(function () {
          callback([]); // Callback must be invoked even if something went wrong.
        });
        /*
        mentions = [
            {id:10,fullName:'Tomáš Nagy',nickname:'nagy'},
            {id:5,fullName:'marek',nickname:'hulvat'},
            {id:5, fullName:'martin konarsky',nickname:'tiko'}
         ];
        
        term_length = term.length;
        callback($.map(mentions, function (mention) {
             if(mention.nickname.substring(0,term_length) === term )
             {
                 return mention.fullName;
             }
             else
             {
                 return null;
             }
        }));
        */
    },
    replace: function (value) {
      return '$1' + value + ' ';
    },
    cache: true
  },
  { // emoji strategy
    match: /(^|\s):(\w*)$/,
    search: function (term, callback) {
      var regexp = new RegExp('^' + term);
      callback($.grep(emojies, function (emoji) {
        return regexp.test(emoji);
      }));
    },
    replace: function (value) {
      return '$1:' + value + ': ';
    }
  }
], { maxCount: 10, debounce: 100 });
    
   
 
   
};




TeamWallManager.prototype.addLoadPostsTrigger = function ()
{
     var manager = this;
    $('.load-wall_posts').on('click',function(e){
       e.preventDefault();
       data = {'from':$(this).attr('data-start'),'team':$(this).attr('data-team'),'url':$(this).attr('href')};
       manager.loadPosts(data);
       $('#wall_posts').append('<div class="loader text-center"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></div>');
      
   });
};


TeamWallManager.prototype.loadPosts = function (data)
{
    $.ajax({
            method: "GET",
            url: data.url,
            data: {'from':data.from,'team':data.team},
            dataType: 'json'
        }).done(function (response) {
            
            $.each(response.lines, function (key, val) {
                $('#wall_posts').append(val);
             });
             
             if(response['allPostLoaded'] ==true)
             {
                 $('.load-wall_posts').hide();
             }
             
             $('.load-wall_posts').attr('data-start',response.start);
             $('#wall_posts').find('.loader').remove();
        });
};

  
TeamWallManager.prototype.addPostDeleteImageTrigger = function ()
{
    $(document).on('click','.remove-post-image',function(){
        $(this).parents('.post-image-block').remove();
    });
    
  
};

TeamWallManager.prototype.addPostImageTrigger = function (elem)
{
     var manager = this;
    elem.on('click',function(e){
        e.preventDefault();
        manager.post_image_modal.modal('show');
    });
};


TeamWallManager.prototype.postFileuploadTrigger = function()
{
    var manager = this;
    
    
    
    $('#post_fileupload').fileupload({
            dataType: 'json',
            progress: function (e, data) {
                $('.upload-progress').show();
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('.upload-progress .progress-num').text((progress-1) + '%');
                $('.upload-progress .progress-bar').css('width',progress + '%');
                 $('.upload-error').remove();
            },
            done: function (e, data) {

                if(data.result.result == 'SUCCESS')
                {
                    var container = $('#post_add_image_wrap');
                    image = $('<img/>',{src:data.result.file});
                    image_input = $('<input/>',{'type':'hidden','name':'post_images[]','value':data.result.file});
                    img_remove = $('<i/>',{'class':'fa fa-close remove-post-image'});
                    
                    img_wrap = $('<div />',{'class':'post-image-block'});
                    img_wrap.append(img_remove);
                    img_wrap.append(image);
                    img_wrap.append(image_input);
                    

                    img_wrap.append(img_wrap);
                    container.append(img_wrap);
                    
                    
                    
                    
                    $('.upload-progress .progress-num').text('100%');
                    $('.upload-progress').hide();
                    $('#post_add_image_wrap').show();
                }

                if(data.result.result == 'ERROR')
                {
                    $('.upload-progress').hide();        
                    $('.upload-progress').before('<div class="alert alert-error upload-error">'+data.result.error_message +'</div>');
                }
           
            }
        });
};

TeamWallManager.prototype.addUrlPostTrigger = function()
{
    var manager = this;
    $('#record_body').on('keyup paste input',function(){
        manager.updatePostBody();
    });
    
    
    $('#record_body').on('keyup  input',function(e){
        var element = $(this);
        setTimeout(function () {
            if(element.text().length == 0)
            {
                manager.resetUrlPostTrigger();
            }

            if(manager.parseUrl == true)
            {
                var str = element.text();

                last_char = str.substr(str.length - 1); 
                if(last_char.replace(/^\s+|\s+$/gm,'').length == 0)
                {
                    manager.checkPostUrl(str);
                }
            }
          }, 100);
    });
    
     $('#record_body').on('paste',function(e){
        var element = $(this);
        setTimeout(function () {
       
        if(manager.parseUrl == true)
        {
            var str = element.text();
            manager.checkPostUrl(str);
        }
      }, 100);
    });
    
    
    
};


TeamWallManager.prototype.checkPostUrl = function(post_text){
    
    post_text = post_text.trim();
    patt = /(([a-z]+:\/\/)?(([a-z0-9\-]+\.)+([a-z]{2}|aero|arpa|biz|com|coop|edu|gov|info|int|jobs|mil|museum|name|nato|net|org|pro|travel|local|internal))(:[0-9]{1,5})?(\/[a-z0-9_\-\.~\+]+)*(\/([a-z0-9_\-\.]*)(\?[a-z0-9+_\-\.%=&amp;]*)?)?(#[a-zA-Z0-9!$&'()*+.=-_~:@/?]*)?)(\s+|$)/gi;
    res = patt.test(post_text);
    if(res)
    {
         urls = post_text.match(patt);
         parse_url = urls[urls.length - 1];
         this.currentParseUrl = parse_url.trim();
         if($.inArray( this.currentParseUrl,this.omittedUrl ) == -1)
         {
            
            this.parseUrl = false;
            this.getUrlData(parse_url);
         }
    }
};


TeamWallManager.prototype.getUrlData = function(url){

     var manager = this;
    $('#post_add_content_wrap').show();
     
     url = encodeURIComponent(url);
     
    $.ajax({
            method: "GET",
            url: '/wall/get-url-data?url='+url,
            dataType: 'json'
        }).done(function (response) {
           console.log(response);
           manager.addUrlPostAddContent(response.data);
        });
   
};


TeamWallManager.prototype.resetUrlPostTrigger = function()
{
    this.parseUrl = true;
    $('#post_add_content_wrap').css('display','none');
    $('#post_add_content_wrap .loader').css('display','inline-block');
    $('#post_add_content').val('');
    this.omittedUrl = [];
};


TeamWallManager.prototype.addUrlPostAddContent = function(data){
    
     var manager = this;
    $wrap = $('<div/>',{'id':'post_add_content_wrap_cnt'});
    $wrap_remove = $('<i/>',{'class':'fa fa-close'});
    $wrap_remove.on('click',function(){
        $('#post_add_content_wrap_cnt').remove();
         manager.resetUrlPostTrigger(); 
         if(manager.omittedUrl.indexOf(manager.currentParseUrl) == -1)
    {
        manager.omittedUrl.push(manager.currentParseUrl);
    }
    });
    
    
    img_wrap = $('<div/>',{class:'col-sm-4  text-center','html': '<img src="'+data.image+'" />'});
    desc_wrap = $('<div/>',{class:'col-sm-8 text-left','html': '<strong><a href="'+data.url+'">'+data.title+'</a></strong><p>'+data.description+'</p>'});
    $('#post_add_content_wrap').find('.loader').hide();
    
    
    
    $wrap.append($wrap_remove);
    $wrap.append(img_wrap);
    $wrap.append(desc_wrap);
    $('#post_add_content_wrap').append($wrap);
    $('#post_add_content').val(JSON.stringify(data));
};







