
var PlayerManager = function ()
{

};

PlayerManager.prototype.bindTriggers = function () {
    $('.player-wall-post-form').on('submit', function (e) {
        e.preventDefault();
        var form = $(this);
        var data = form.serialize();
        var action = form.attr('action');
        $.ajax({
            method: "POST",
            url: action,
            data: data,
            dataType: 'json'
        }).done(function (response) {
            form.find('.control-label-error').remove();
            if (response.result == 'ERROR')
            {
                $.each(response.errors, function (key, val) {
                    form.find('[data-error-bind="' + key + '"]').append('<span class="control-label-error">' + val + '</span>');
                });
            }
            else
            {
                $('#wall_posts').prepend(response.item);
            }
        });
    });
    
    $(document).on('click','.post_comment_trigger',function(e){
        e.preventDefault();
        var action = $(this).attr('data-action');
        var post_id = $(this).attr('data-post-id');
        var comment_body = $('#post-comment-body-'+post_id).val();
        var data =  {'comment':{'body':comment_body,'post_id':post_id}};
         $.ajax({
            method: "POST",
            url: action,
            data: data,
            dataType: 'json'
        }).done(function (response) {
            $('#post-'+post_id).find('.control-label-error').remove();
            if (response.result == 'ERROR')
            {
                $.each(response.errors, function (key, val) {
                    $('#post-'+post_id).find('[data-error-bind="' + key + '"]').append('<span class="control-label-error">' + val + '</span>');
                });
            }
            else
            {
                $('#post_comments_'+post_id).append(response.item);
            }
        });
    });
    
    $('.show-comments-trigger').on('click',function(e){
        e.preventDefault();
        
        var data_target_id = $(this).attr('data-target');
        var data_target = $('#'+data_target_id);
        var action = $(this).attr('href');
        
        if(data_target.hasClass('active'))
        {
            data_target.hide();
            data_target.removeClass('active');
        }
        else
        {
            if(!data_target.hasClass('loaded'))
            {
                 $.ajax({
                    method: "POST",
                    url: action,
                    dataType: 'json'
                }).done(function (response) {
                   console.log(response);

                   $.each(response.lines, function (key, val) {
                       data_target.append(val);
                    });

                });
                 data_target.addClass('loaded');
            }
           
            
            data_target.show();
            data_target.addClass('active');
           
        }
        
        
    });


};


