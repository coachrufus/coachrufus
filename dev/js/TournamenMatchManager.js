
var TournamenMatchManager = function ()
{
   this.current_hit_time;
   this.edit_item_label;
   this.stopwatch;
   this.confirm_save_modal = $('#confirm-save-modal');
   this.confirm_delete_modal = $('#confirm-stat-delete-modal');
   this.confirm_reset_modal = $('#confirm-stat-reset-modal');
};


TournamenMatchManager.prototype.initLineupTriggers = function()
{
    var manager = this;
    $.each($('.change_lineup'), function (index, value) {
        manager.changeLineupEvent($(this));
    });

    $.each($('.remove_player'), function (index, value) {
        manager.removePlayerEvent($(this));
    });



    $('.add-new-lineup-player').on('click', function (e) {
        e.preventDefault();
        manager.addNewLineupPlayer($(this));
    });


    $('.new-lineup-player-trigger').on('click', function (e) {
        e.preventDefault();
        var target_line_name = $(this).attr('data-target');
        var form = $('#' + target_line_name + '_new_player');
        form.show();
    });

    $(document).on("click", ".add-lineup-player", function (e) {
        e.preventDefault();
        manager.addLineupPlayer($(this));
    });

    this.countTotalLineEfficiency();
    this.countGoals();
};

TournamenMatchManager.prototype.initTimelineTriggers = function()
{
    var manager = this;
    $('.add_row_trigger').on('click', function (e) {
        e.preventDefault();
        var team_id = $(this).attr('data-team-id');
        var data_group_container_id = $(this).attr('data-group-container');
        var data_group_container = $('#' + data_group_container_id);
        var template_row = $('#container_template_item');
        var new_container_item = $('<div/>', {class: 'container_item', 'data-team-id': team_id});
        new_container_item.html(template_row.html());
        data_group_container.find('.container_item_wrap').append(new_container_item);
    });
    
    
    $('.add_stat_time_event').on('click',function(e){
        e.preventDefault();
        manager.showStatTimeEventForm();
    });
    
     $('.goal_player').on('change',function(e){
        e.preventDefault();
        manager.showAssistPlayers($(this));
    });
    
     $('#add_timeline_form .assist_player').on('change',function(e){
        e.preventDefault();
        manager.addSimpleStatTimeEvent($(this));
    });
     $('#edit_timeline_form .assist_player').on('change',function(e){
        e.preventDefault();
        manager.editStatTimeEvent($(this));
    });
    
    $('.timeline_stat_trigger').on('click',function(e){
        e.preventDefault();
        manager.addStatTimeEvent($(this));
    });
    
    $('#add_timeline_form .close_timeline_form').on('click',function(e){
        e.preventDefault();
        $('#add_timeline_form').hide();
        $('.add_stat_time_mom_manual').show();
    });
    $('#edit_timeline_form .close_timeline_form').on('click',function(e){
        e.preventDefault();
        $('#edit_timeline_form').hide();
        $('.timeline_event').show();
    });
    
    $('.add_stat_time_mom, .add_stat_time_mom_manual').on('click',function(e){
        if(manager.stopwatch != null)
        {
             manager.stopwatch.stop();
        }
        $('#edit_timeline_form').hide();
       
        manager.confirm_save_modal.modal('show'); 
    });
    
    $('.confirm-save-modal-submit').on('click',function(e){
          e.preventDefault();
           //manager.stopwatch.stop();
         $('.add_stat_time_mom_wrap').hide();
         $('.add_stat_time_event_wrap').hide();
         
         $('#add_timeline_mom_form').show();
         manager.confirm_save_modal.modal('hide'); 
         
         $('html, body').animate({
            scrollTop: $("#add_timeline_mom_form").offset().top-80
        }, 500);
         
         
    });
    
    $('.unconfirm-save-modal-submit').on('click',function(e){
         e.preventDefault();
        manager.confirm_save_modal.modal('hide'); 
        $('html, body').animate({
            scrollTop: $("#timeline-end").offset().top
        }, 500);

    });
    
     $('#add_timeline_mom_form .btn-danger').on('click',function(e){
        e.preventDefault();
        $('#add_timeline_mom_form').hide();
         $('.add_stat_time_mom_wrap').show();
         $('.add_stat_time_event_wrap').show();
    });
    
     
    $('.timeline_mom_trigger').on('click',function(e){
        e.preventDefault();
        manager.addStatTimeMom($(this));
        //$('#add_timeline_mom_form').hide();
    });
    

    $( document ).on( "click", ".current_hit_time .ico-edit", function() {
        var container = $(this).parents('.timeline-label'); 
        if(!container.hasClass('closed'))
        {
            container.find('.edit_hit_time_wrap').show();
            container.find('.current_hit_time').hide();
        }
        
      });
      
    $( document ).on( "click", ".confirm_hit_time", function(e) {
         e.preventDefault();
        manager.changeHitTime($(this));
      });
      
      
    $( document ).on( "click", ".edit-timeline-item-trigger", function(e) {
        if($(this).attr('data-type') == 'mom')
        {
             manager.showEditMomTimelineForm($(this));
        }
        else
        {
             manager.showEditTimelineForm($(this));
        }
       
     });
      
    
   $('.start_timeline').on('click',function(e){
       e.preventDefault();
       //var current_time = $('#start_timeline_time').val();
       
       //$("#stopwatch").css('visibility','hidden');
       manager.stopwatch = new Stopwatch($("#start_timeline_time"));
      // $('#start_timeline_time').val(current_time);
       manager.stopwatch.start();
       
       
       
       $('.add_stat_time_event').fadeIn(200);
       $('.start_timeline').hide();
       
       $('#stopwatch-control').show();
       $('.timeline-start-item').removeClass('hide');
       $('#stat_timeline').removeClass('.manual_timeline');
       
        $('html, body').animate({
            scrollTop: $(".add_stat_time_event_wrap").offset().top-220
        }, 500);
        
        $('.start_triggers').hide();
        
/*
       $('.start-wrap').animate({
        top: "-100px",
      }, 200, function() {
        $( '.start-wrap' ).hide();
          $("#stopwatch").css('visibility','visible');
          $('.add_stat_time_event_wrap').fadeIn(200);
          $('.add_stat_time_mom_wrap').fadeIn(200);
      });
       */
   });
   
   $('.add_manual_timeline').on('click',function(e){
        e.preventDefault();
        $('.add_stat_time_event').fadeIn(200);
        $('.start_timeline').hide();
        $('input.current_hit_time').hide();
        $('.start_triggers').hide();
        $('#stat_timeline').addClass('manual_timeline');
        $('.add_stat_time_event_wrap_mom').addClass('active');
   });
   
   $(document).on('click','.delete-timeline-item-trigger',function(e){
       e.preventDefault();
       manager.confirm_delete_modal.find('.modal_submit').attr('href', $(this).attr('href'));
       manager.confirm_delete_modal.modal('show'); 
   });
   
    $(document).on('click','.reset-trigger',function(e){
       e.preventDefault();
       manager.confirm_reset_modal.find('.modal_submit').attr('href', $(this).attr('href'));
       manager.confirm_reset_modal.modal('show'); 
   });
   
   
   $('.match_phases_chain').on('click',function(e){
       e.preventDefault();
       var next_elem_class = $(this).attr('data-next');
       var link = $(this).attr('href');
       $(this).removeClass('active');
       $('.'+next_elem_class).addClass('active');
       
       //clock actions
       var clock_action = $(this).attr('data-clock-action');
       if(clock_action == 'pause')
       {
           $('.time_pause').trigger('click');
       }
       if(clock_action == 'continue')
       {
           $('.time_start').trigger('click');
       }
      

       
        $.ajax({
            method: "GET",
            url: link,
            dataType: 'json'
        }).done(function (response) {
                
                console.log(response);
               
                
        });
        
   });
};


TournamenMatchManager.prototype.countTotalLineEfficiency = function()
{
    var first_line_total = 0;
    var second_line_total = 0;
    var first_line_width = 0;
    var second_line_width = 0;
    
    $('#first_line .efficiency').each(function(key,val){
        first_line_total += parseFloat($(val).attr('data-val'));
    });
    
    $('#second_line .efficiency').each(function(key,val){
        second_line_total += parseFloat($(val).attr('data-val'));
    });
    
    first_line_total = Math.round(first_line_total * 100) / 100;
    second_line_total = Math.round(second_line_total * 100) / 100;
    
    $('.first-line-col').removeClass('strong');
    $('.first-line-col').removeClass('week');
    $('.second-line-col').removeClass('strong');
    $('.second-line-col').removeClass('week');
   
    if(first_line_total > second_line_total)
    {
        coef = first_line_total/100;
        first_line_width = Math.round(first_line_total/coef);
        second_line_width = Math.round(second_line_total/coef);
        $('#first_line_eff_sum').css('width','100%');
        $('#second_line_eff_sum').css('width',second_line_width+'%');
        
         $('.first-line-col').addClass('strong');
         $('.second-line-col').addClass('week');
         
          $('.first-line-col .power-indicator').html('<i class="ico ico-power-up"></i>');
          $('.second-line-col .power-indicator').html('<i class="ico ico-power-down"></i>');
    }
    
     if(first_line_total < second_line_total)
    {
        coef = second_line_total/100;
        first_line_width = Math.round(first_line_total/coef);
        second_line_width = Math.round(second_line_total/coef);
       
      
        $('#first_line_eff_sum').css('width',first_line_width+'%');
        $('#second_line_eff_sum').css('width','100%');
        
         $('.first-line-col').addClass('week');
         $('.second-line-col').addClass('strong');
         
         $('.first-line-col .power-indicator').html('<i class="ico ico-power-down"></i>');
         $('.second-line-col .power-indicator').html('<i class="ico ico-power-up"></i>');
    }
    
     if(first_line_total == second_line_total)
    {
        $('#first_line_eff_sum').css('width','100%');
        $('#second_line_eff_sum').css('width','100%');
    }
    
    
    
    if(first_line_total == 0)
    {
        $('#first_line_eff_sum').css('width','0');
    }
    
    
    
    if(second_line_total == 0)
    {
        $('#second_line_eff_sum').css('width','0');
    }

    $('.first-line-col .power-total').text(first_line_total);
    $('.first-line-col .power-perc').text(first_line_width+'%');
    
    $('.second-line-col .power-total').text(second_line_total);
    $('.second-line-col .power-perc').text(second_line_width+'%');
    

    
    
};

TournamenMatchManager.prototype.countGoals = function(){
    var first_line_goals = 0;
    var second_line_goals = 0;

    $('.timeline_event_first_line .timeline_goal ').each(function(key,val){
         first_line_goals +=1;
    });
    
     $('.timeline_event_second_line .timeline_goal ').each(function(key,val){
        second_line_goals +=1;
    });
    
    $('#second_line_goals').text(second_line_goals);
    $('#first_line_goals').text(first_line_goals);
};


TournamenMatchManager.prototype.changeLineupEvent = function(element)
{
       var manager = this;
    element.on('click',function(){
            element = $(this);
            var lineup = element.parents('tbody').attr('id');
             var row = element.parents('tr');
             var item_id = element.attr('data-rel');
             var target_line;

             if('first_line' == lineup)
             {
                 row.find('.ico').removeClass('ico-arr-rgt-green2');
                 row.find('.ico').addClass('ico-arr-lft-green');
                 row.find('.change_lineup_wrap').detach().prependTo(row);
                 
                 
                 row.find('.remove_player').detach().appendTo(row.find('.change_lineup_wrap'));
                 
                 
                 
                 row.detach().appendTo("#second_line");
                 target_line = 'second_line';
             }
             if('second_line' == lineup)
             {
                 row.find('.ico').removeClass('ico-arr-lft-green');
                 row.find('.ico').addClass('ico-arr-rgt-green2');
                 row.find('.change_lineup_wrap').detach().appendTo(row);
                 
                 row.find('.change_lineup').detach().appendTo(row.find('.change_lineup_wrap'));
                 
                 row.detach().appendTo("#first_line");
                  target_line = 'first_line';
             }

              $.ajax({
                      method: "GET",
                      url: '/team-match/change-lineup-player',
                      data: {'item_id':item_id,lineup:target_line},
                      dataType: 'json'
                  }).done(function (response) {
                
                      manager.countTotalLineEfficiency();

                });;
        });
};
TournamenMatchManager.prototype.buildAddPlayerOption = function(player,line,eff)
{
     
     add_player_option = $('<li/>');
     add_player_option_link  = $('<a/>',{text:player.player_name+'','data-target':line,'data-playerid':player.player_id,'data-name':player.player_name,'data-lineup':player.lineup_id,'data-eff':eff,'class':'add-lineup-player','data-team-player-id':player.team_player_id,'data-icon': player.icon,'data-team-role':player.team_role,'data-team-number':player.team_number});
     add_player_option.append(add_player_option_link);
     
     return add_player_option;
};


TournamenMatchManager.prototype.removePlayerEvent = function(element)
{
      var manager = this;
    element.on('click',function(){
             element = $(this);
             var row = element.parents('tr');
             var item_id = element.attr('data-rel');
             var player_eff = element.attr('data-eff');;
             row.remove();

              $.ajax({
                      method: "GET",
                      url: '/team-match/remove-lineup-player',
                      data: {'item_id':item_id},
                      dataType: 'json'
                  }).done(function (response) {
                    row.find('.change_lineup').attr('data-rel', response.id);
                    row.find('.remove_player').attr('data-rel', response.id);

                    player = response.player;
                    first_line_add_player = manager.buildAddPlayerOption(player,'first_line',player_eff);
                    second_line_add_player = manager.buildAddPlayerOption(player,'second_line',player_eff);
                    $('.add-first-lineup-player-choice').prepend(first_line_add_player);
                    $('.add-second-lineup-player-choice').prepend(second_line_add_player);

                    manager.countTotalLineEfficiency();
             });
        });
};


TournamenMatchManager.prototype.buildLineupPlayerRow = function(data,lineup)
{
     row = $('<tr/>',{'class':'lineup_row'});
     
     
     
     
     var user_block = $('<div/>',{'class':'user-block'});
     var user_img = $('<div/>',{'class':'round-50','style':'background-image:url('+data.icon+')'});
     var user_username = $('<span/>',{'class':'username','text':data.name});
     var user_desc = $('<span/>',{'class':'description','text':data.team_role+' #'+data.team_number});
     user_block.append(user_img);
     user_block.append(user_username);
     user_block.append(user_desc);

     
     player_cell = $('<td/>');
     player_cell.append(user_block);
     button_cell = $('<td/>',{'class':'change_lineup_wrap'});
     change_button =  $('<button/>',{'class':'btn change_lineup','data-rel':''});
     this.changeLineupEvent(change_button);
     change_icon = $('<i/>',{'class':'ico'});
     change_button.append(change_icon);

     
     remove_button =  $('<button/>',{'class':'btn btn-danger remove_player','data-rel':'','data-eff':data.eff});
     remove_icon = $('<i/>',{'class':'ico ico-close-black'});
     this.removePlayerEvent(remove_button);
     remove_button.append(remove_icon);
     eff_span = $('<span/>',{html:'('+data.eff+')','class':'efficiency','data-val':data.eff,'data-toggle':'tooltip','title':'Actual form'});
     player_cell.append(eff_span);
     
     // <span class="efficiency" data-val="<?php echo $attendance['efficiency']  ?>"><?php echo $translator->translate('Efficiency') ?>: <?php echo $attendance['efficiency']  ?></span>
     
     if('first_line' == lineup)
     {
        change_icon.addClass('ico-arr-rgt-green2');
        button_cell.append(remove_button);
        button_cell.append(change_button);
        row.append(player_cell);
        row.append(button_cell);
     }
     
     if('second_line' == lineup)
     {
         change_icon.addClass('ico-arr-lft-green');
         button_cell.append(change_button);
         button_cell.append(remove_button);
         
         row.append(button_cell);
         row.append(player_cell);
     }

     return row;
};

TournamenMatchManager.prototype.saveLineupPlayerRow = function(row,ajax_data)
{
    var manager = this;
    $.ajax({
                 method: "GET",
                 url: '/team-match/add-lineup-player',
                 data: ajax_data,
                 dataType: 'json'
             }).done(function (response) {
                row.find('.change_lineup').attr('data-rel', response.id);
                row.find('.remove_player').attr('data-rel', response.id);
                 manager.countTotalLineEfficiency();
             });
};

TournamenMatchManager.prototype.addLineupPlayer = function(element){
    var target_line_name = element.attr('data-target');
    var target_line = $('#'+target_line_name);
    var name = element.attr('data-name');
    var player_id = element.attr('data-playerid');
    var team_player_id = element.attr('data-team-player-id');
    var lineup_id = element.attr('data-lineup');
    var eff  = element.attr('data-eff');
    var icon  = element.attr('data-icon');
    var team_role  = element.attr('data-team-role');
    var team_number  = element.attr('data-team-number');

    
     row = this.buildLineupPlayerRow({'name':name,'eff':eff,'icon':icon,'team_role':team_role,'team_number':team_number},target_line_name);
   
     target_line.prepend(row);
     row.effect("highlight", {}, 1000);
     ajax_data = {'player_id':player_id,'target_line':target_line_name,'lineup_id':lineup_id,'player_name':name,'team_player_id':team_player_id };
    
    this.saveLineupPlayerRow(row,ajax_data);
    
    $('.add-lineup-player[data-team-player-id="'+team_player_id+'"]').parent().remove();
   

    
};

TournamenMatchManager.prototype.addNewLineupPlayer = function(element){
    
    
    var target_line_name = element.attr('data-target');
    var form = $('#'+target_line_name+'_new_player');
    var target_line = $('#'+target_line_name);
    var lineup_id = element.attr('data-lineup');
    var name = form.find('.new-player-name').val();
    var eff  = element.attr('data-eff');


     row = this.buildLineupPlayerRow({'name':name,'eff':0,'icon':'','team_role':'','team_number':''},target_line_name);
     row.attr('style','display:none');
     target_line.prepend(row);
     form.hide();
     row.show();
     row.effect("highlight", {}, 1000);
     ajax_data = {'target_line':target_line_name,'lineup_id':lineup_id,'player_name':name};

  this.saveLineupPlayerRow(row,ajax_data);
  
};

TournamenMatchManager.prototype.showStatTimeEventForm = function()
{
    $('#add_timeline_form').show();
    this.current_hit_time = $('#start_timeline_time').val();
    $('#add_timeline_form .current_hit_time').val(this.current_hit_time);
    
    $('.add_stat_time_mom_manual').hide();
    
};


TournamenMatchManager.prototype.showAssistPlayers = function(trigger)
{
    var form = trigger.parents('form');
    var assist_player_select = form.find('.assist_player');
    var assist_player_select_options = assist_player_select.find('option[value!="0"]');
    assist_player_select_options.hide();
    var goal_player = form.find('.goal_player option:selected');
    var goal_lid_team = goal_player.attr('data-lid-team');
   var goal_player_id =goal_player.val();

    assist_player_select_options.each(function(index,val){
        
      
        
        if($(val).attr('data-lid-team') === goal_lid_team && goal_player_id != $(val).val())
        {
            $(val).show();
        }
    });
    
     form.find('.assist_player_wrap').show();
};


TournamenMatchManager.prototype.changeHitTime = function(elem)
{
    var container = elem.parents('.timeline-label'); 
    var edit_wrapper = container.find('.edit_hit_time_wrap');
    var edit_hit_time =  container.find('.form-control').val();
    var current_hit_time = container.find('.current_hit_time');
    var link = elem.attr('href');
    current_hit_time.html(edit_hit_time);
    edit_wrapper.hide();
    current_hit_time.show();
    
    //console.log(link);
    //console.log(edit_hit_time);
    
    $.ajax({
            method: "GET",
            url: link,
            data: {time:edit_hit_time},
            dataType: 'json'
        });
    
  
};

TournamenMatchManager.prototype.showEditMomTimelineForm = function(trigger){
     var item =  trigger.parents('.timeline_event');
     item.hide();
      $('#add_timeline_mom_form').show();
      
      /*
     item.after($('#add_timeline_mom_form'));
     
     var trigger_position = item.position();
     
     this.edit_item_label = item;
     var goal_hid = item.find('.player_row_goal').attr('data-hid');
     var assist_hid = item.find('.player_row_assist').attr('data-hid');
     var time = item.find('.timeline-body').attr('data-time');
    $('#edit_timeline_form').attr('data-goal-hid',goal_hid);
    $('#edit_timeline_form').attr('data-assist-hid',assist_hid);
    $('#edit_timeline_form').attr('data-time',time);
    $('#edit_timeline_form').show();
    */
};

TournamenMatchManager.prototype.showEditTimelineForm = function(trigger){
     var item =  trigger.parents('.timeline_event');
     item.hide();
     
     item.after($('#edit_timeline_form'));
     var trigger_position = item.position();
     this.edit_item_label = item;
     var goal_hid = item.find('.player_row_goal').attr('data-hid');
     var assist_hid = item.find('.player_row_assist').attr('data-hid');
     var time = item.find('.timeline-body').attr('data-time');
/*
     $('#edit_timeline_form').css('left',trigger_position.left+'px');
     $('#edit_timeline_form').css('margin-left',item.css('margin-left')+'px');
     $('#edit_timeline_form').css('top',trigger_position.top+'px');*/
    $('#edit_timeline_form').attr('data-goal-hid',goal_hid);
    $('#edit_timeline_form').attr('data-assist-hid',assist_hid);
    $('#edit_timeline_form').attr('data-time',time);
    $('#edit_timeline_form').show();
    
};



TournamenMatchManager.prototype.addStatTimeEvent = function(trigger)
{
    var link = trigger.attr('href');
    var lineup_player_id = trigger.parent().find('.player_choice').val();
    var player_id = trigger.parent().find('.player_choice  option:selected').attr('data-pid');
    var lid_team = trigger.parent().find('.player_choice  option:selected').attr('data-lid-team');
    var time = $('#stopwatch span').html();
    var manager = this;
    //console.log(player_id);
    
     $.ajax({
            method: "GET",
            url: link,
            data: {'player_id':player_id,lineup_player_id:lineup_player_id,time:time,lid_team:lid_team},
            dataType: 'json'
        }).done(function (response) {
                
               
               
  
                timeline_item_label = $('<li/>', {class: 'time-label'});
                timeline_item_label_span =  $('<span/>', {class: 'bg-red'});
                timeline_item_label_edit_wrap = $('<div/>', {class: 'input-group edit_hit_time_wrap'});
                timeline_item_label_edit_input = $('<input/>', {class: 'form-control',name: 'hit_time',type:'text'});
                timeline_item_label_edit_confirm = $('<a/>', {href: link+'&ct='+time,class: 'input-group-addon bg-green confirm_hit_time',html:'<i  class="fa fa-check"></i>'});
                timeline_item_label_edit_wrap.append(timeline_item_label_edit_input);
                timeline_item_label_edit_wrap.append(timeline_item_label_edit_confirm);
                timeline_item_label_span.append(timeline_item_label_edit_wrap);
                timeline_item_label.append(timeline_item_label_span);
               
                //timeline_item_label = '<li class="time-label"><span class="bg-red">'+time+'</span></li>';
                timeline_item = '<li class="timeline_event timeline_'+response.type+' timeline_'+lid_team+'"><div class="timeline-item"><div class="timeline-body" data-time="'+time+'">'+response.typeName+','+response.player+'</div></div></li>';
                
                $('#add_timeline_form').before(timeline_item_label);
                $('#add_timeline_form').before(timeline_item);
                manager.countGoals();

            
        });
};


TournamenMatchManager.prototype.addSimpleStatTimeEvent= function()
{
    var form = $('#add_timeline_form form');
    var link = form.attr('action');
    var goal_player = form.find('.goal_player option:selected');
    var assit_player = form.find('.assist_player option:selected');
    
    var goal_lineup_player_id = goal_player.val();
    var goal_player_id =goal_player.attr('data-pid');
    var goal_lid_team = goal_player.attr('data-lid-team');
    var goal_team_id = goal_player.attr('data-tid');
    
    var assit_lineup_player_id = assit_player.val();
    var assit_player_id =assit_player.attr('data-pid');
    var assit_lid_team = assit_player.attr('data-lid-team');
    var assit_team_id = assit_player.attr('data-tid');
    var time = this.current_hit_time;

    var manager = this;
    
    var edit_time_link = link.replace('add-timeline-stat','edit-timeline-time');

    var request_data = {
                'goal': {'player_id':goal_player_id,lineup_player_id:goal_lineup_player_id,time:time,lid_team:goal_lid_team,tid:goal_team_id},
                'assist': {'player_id':assit_player_id,lineup_player_id:assit_lineup_player_id,time:time,lid_team:assit_lid_team,tid:assit_team_id},
                'time': time
            };

     $('#add_timeline_form_loader').show();
     

     $.ajax({
            method: "GET",
            url: link,
            data: request_data,
            dataType: 'json'
        }).done(function (response) {
                
                console.log(response);
               
                $('#add_timeline_form').before(response);
               
                manager.countGoals();
                
               $('#add_timeline_form').hide();
                $('#add_timeline_form_loader').hide();
               //reset 
                $('#add_timeline_form .goal_player').val('');
                $('#add_timeline_form .assist_player').val('');
                $('#add_timeline_form .assist_player_wrap').hide();
                $('.add_stat_time_mom_manual').show();
                
        });
};

TournamenMatchManager.prototype.editStatTimeEvent= function()
{
    var form = $('#edit_timeline_form form');
    var goal_hid = $('#edit_timeline_form').attr('data-goal-hid');
    var assist_hid = $('#edit_timeline_form').attr('data-assist-hid');
    var link = form.attr('action');
    var goal_player = form.find('.goal_player option:selected');
    var assit_player = form.find('.assist_player option:selected');
    
    var goal_lineup_player_id = goal_player.val();
    var goal_player_id =goal_player.attr('data-pid');
    var goal_lid_team = goal_player.attr('data-lid-team');
    var goal_team_id = goal_player.attr('data-tid');
      
    var assit_lineup_player_id = assit_player.val();
    var assit_player_id =assit_player.attr('data-pid');
    var assit_lid_team = assit_player.attr('data-lid-team');
    var assit_team_id = assit_player.attr('data-tid');
    var manager = this;
   var time = $('#edit_timeline_form').attr('data-time');

    var request_data = {
                'goal': {'player_id':goal_player_id,lineup_player_id:goal_lineup_player_id,lid_team:goal_lid_team,hid:goal_hid,tid:goal_team_id},
                'assist': {'player_id':assit_player_id,lineup_player_id:assit_lineup_player_id,lid_team:assit_lid_team,hid:assist_hid,tid:assit_team_id}
            };
 $('#edit_timeline_form_loader').show();
     $.ajax({
            method: "GET",
            url: link,
            data: request_data,
            dataType: 'json'
        }).done(function (response) {

                /*
                timeline_item = $('<li/>',{'class':'timeline_event timeline_goal timeline_'+goal_lid_team+'  edit-timeline-item'});
                timeline_item.html('<div class="timeline-item"><div class="timeline-body" data-time="'+time+'">'+response+'</div></div>');
                timeline_item_edit = $('<i/>',{'class':'fa fa-edit edit-timeline-item-trigger'});
                timeline_item.prepend(timeline_item_edit);
                
                //manager.edit_item_label.before(timeline_item_label);
                manager.edit_item_label.before(timeline_item);
                manager.edit_item_label.remove();
                */
               manager.edit_item_label.before(response);
                $('#edit_timeline_form_loader').hide();
                $('#edit_timeline_form').hide();

            
        });
    
};


TournamenMatchManager.prototype.addStatTimeMom = function(trigger)
{
    var link = trigger.attr('href');
    var lineup_player_id = trigger.parent().find('.player_choice').val();
    var player_id = trigger.parent().find('.player_choice  option:selected').attr('data-pid');
    var lid_team = trigger.parent().find('.player_choice  option:selected').attr('data-lid-team');
    var overtime = 'no';
    if($('#overtime').is(':checked'))
    {
        overtime = 'yes';
    }
        
        
    var time = $('#stopwatch span').html();
    $('#stopwatch-control').hide();
    var manager = this;
     $('#mom_timeline_form_loader').show();
    //console.log(player_id);
    
     $.ajax({
            method: "GET",
            url: link,
            data: {'player_id':player_id,lineup_player_id:lineup_player_id,time:time,lid_team:lid_team,overtime:overtime},
            dataType: 'json'
        }).done(function (response) {
                
               location.reload();
        
            /*
                timeline_item_label = '<li class="time-label"><span class="bg-red">'+time+'</span></li>';
                timeline_item = '<li class="timeline_event timeline_'+response.type+' timeline_'+lid_team+'"><div class="timeline-item"><div class="timeline-body">'+response.typeName+','+response.player+'</div></div></li>';
                */
               
              // manager.edit_item_label.before(response);
               
                //$('#add_timeline_form').before(timeline_item_label);
                //$('#add_timeline_form').before(timeline_item);
                manager.countGoals();
                $('#mom_timeline_form_loader').hide();
                $('#add_timeline_mom_form').hide();
                $('#add_timeline_mom_form').after(response);
                $('.btn_progress_continue').fadeIn();
            
        });
};

TournamenMatchManager.prototype.addMatchPhaseChain = function()
{
    
}

