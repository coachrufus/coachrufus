var TeamSearchAutocomplete = function (element)
{
    this.element = element;
    this.phrase = '';
    this.onclick = 'fillInput';
    this.autocomplete_list = element.closest('.autocomplete_container').find('.autocomplete_list');
    //this.selected_players = $('#selected_players');
    this.row_id = element.attr('data-row');
    //this.sendRequest = function(){};
    
};


TeamSearchAutocomplete.prototype.init = function () {
    var autocomplete = this;
    this.element.on('keyup', function () {
        if ($(this).val().length > 3)
        {
            autocomplete.phrase = $(this).val();
            autocomplete.sendRequest();
        }
    });
};
TeamSearchAutocomplete.prototype.initNameSearch = function () {
    var autocomplete = this;
    this.element.on('keyup', function () {
        //at_patt = new RegExp("@");
        //at_res = at_patt.test($(this).val());

        if ($(this).val().length > 3)
        {
            autocomplete.phrase = $(this).val();
            autocomplete.sendRequest();
        }
    });
};


TeamSearchAutocomplete.prototype.sendRequest = function () {

    data = {phrase: this.phrase};
    var autocomplete = this;
    $.ajax({
        method: "GET",
        url: "/team/autocomplete/search",
        data: data,
        dataType: 'json'
    }).done(function (response) {
        autocomplete.showResult(response);
    });

};

TeamSearchAutocomplete.prototype.showResult = function (result) {
    var autocomplete = this;
    this.autocomplete_list.html('');


    if (result.length > 0)
    {
        result.forEach(function (item)
        {
            autocomplete.addItem(item);
        });
        this.autocomplete_list.show();
    }



};

TeamSearchAutocomplete.prototype.addItem = function (item) {

   
    var list_item = $('<li/>');
    var autocomplete = this;
    list_item.html('<strong>#'+ item.id + ' ' + item.name +  '</strong>');

    if (autocomplete.onclick == 'fillInput')
    {
        list_item.on('click', function () {
            autocomplete.fillInput(item);
            autocomplete.autocomplete_list.html('');
            autocomplete.autocomplete_list.fadeOut();
        });
    }
    else
    {

    }

   
    this.autocomplete_list.append(list_item);
};

TeamSearchAutocomplete.prototype.fillInput = function (item) {

    $('#team_name'+this.row_id).val('#'+item.id+' '+item.name);
    
    $('#team_id'+this.row_id).val(item.id);


}
