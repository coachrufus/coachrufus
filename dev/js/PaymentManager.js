var PaymentManager = function ()
{

};

PaymentManager.prototype.bindPaymentGateEvent = function () {
    $(document).ready(function () {
        $('.pay-button').on('click', function (e) {
            e.preventDefault();
            var data = $('#credit_modal_form').serialize();
            var url = $('#credit_modal_form').attr('action');
            $('#team-credit-modal-loader').show();
            $('#credit_modal_form').hide();
            
           
            
            
            $.ajax({
                method: "GET",
                url: url,
                data: data,
                dataType: 'json'
            }).done(function (response) {

                setTimeout(function () {

                    $('#team-credit-modal-loader').hide();
                    $('#credit_modal_form').show();
                    $('#team-credit-modal').modal('hide');
                }, 1500);

                _gopay.checkout({gatewayUrl: response.redirectUrl, inline: true}, function (checkoutResult) {

                    $.ajax({
                        method: "GET",
                        url: $('#credit_modal_form_url').val() + '?id=' + checkoutResult.id,
                        dataType: 'json'
                    }).done(function (result) {


                        if (result.status == 'PAID')
                        {

                            location.href = result.redirectUrl;
                        } else
                        {

                        }
                    });
                });


            });
        });
    });
};

PaymentManager.prototype.bindTriggers = function () {

    this.bindPaymentGateEvent();
    
    $('.credit_modal_vop_link').on('click',function(e){
        e.preventDefault();
        $('#team-credit-modal-vop-frame').attr('src',$(this).attr('href'));
        $('#team-credit-modal-vop').modal('show');
    });

    $('.team_credit_modal_trigger, .PerformanceBox  .pro').on('click', function (e) {
        e.preventDefault();
        
         dataLayer.push({
                'event':'VirtualPageview_pro_price',
                'pro_price':'/package/payment/pro'
                });
        
        
        $('.progress-modal').modal('hide');
        $('#team-credit-modal').modal('show');
    });

    $('#promo-code-form').on('submit', function (e) {

        e.preventDefault();
        var data = $('#promo-code-form').serialize();
        var url = $('#promo-code-form').attr('action');
        var variant_id = $('#variant_id').val();

        $.ajax({
            method: "GET",
            url: url,
            data: data + '&variant=' + variant_id,
            dataType: 'json'
        }).done(function (response) {
            var basePrice = response.basePrice;
            var price = response.price;
            var discount = response.discount;
            var discountSum = response.discount;
            $('#discountCode').val(response.discountCode);
            var content = basePrice + ' - ' + discountSum + ' (' + discount + '%)  = ' + price;
            $('#promo-result-row').html(content);


            $('#promo-form-row').hide();
            $('#promo-result-row').show();
        });
    });

    $('.subscription-cancel-trigger').on('click', function (e) {
        e.preventDefault();


        var packageInfo = $(this).attr('data-title') + '<br />' + $(this).attr('data-valid-to');
        $('#subscription-modal-body-text').html(packageInfo);


        $('#subscription-cancel-link').attr('href', $(this).attr('href'));
        $('#subscription-cancel-modal').modal('show');
    });

    $('.subscription-disabled-trigger').on('click', function (e) {
        e.preventDefault();
        $('#subscription-disabled-modal').modal('show');
    });


    $('.subscription-price-trigger').on('click', function (e) {
        e.preventDefault();

        var variant_id = $(this).attr('data-variant');
        $('.subscription-modal .subscription-variant').val(variant_id);

        var subscription_row = $(this).parents('.subscription-change-row');
        subscription_row.find('.subscription-change-btn-row button').removeClass('active');
        $(this).addClass('active');
    });


    $('.subscription-change-trigger').on('click', function (e) {
        e.preventDefault();
        var tid = $(this).attr('data-tid');
        $('#subscription-change-tid').val(tid);

        var oid = $(this).attr('data-oid');
        $('#subscription-change-oid').val(oid);



        var subscription_row = $(this).parents('.subscription-change-row');

        subscription_row.find('.subscription-change-btn-row button').removeClass('active');
        $(this).addClass('active');

        var subscription_row_desc = subscription_row.find('.subscription-change-desc').html();
        $('.subscription-change-modal-desc').html(subscription_row_desc);
        $('#subscription-change-modal').modal('show');

        $('#subscription-change-modal-variant').val($(this).attr('data-variant'));

    });

    $(document).ready(function () {
        $('.subscription-change-submit').on('click', function (e) {
            e.preventDefault();
            var data = $('#subscription-change-modal-form').serialize();
            var url = $('#subscription-change-modal-form').attr('action');

            $.ajax({
                method: "POST",
                url: url,
                data: data,
                dataType: 'json'
            }).done(function (response) {


                _gopay.checkout({gatewayUrl: response.redirectUrl, inline: true}, function (checkoutResult) {

                    $.ajax({
                        method: "GET",
                        url: $('#subscription-change-rurl').val() + '?id=' + checkoutResult.id,
                        dataType: 'json'
                    }).done(function (result) {


                        if (result.status == 'PAID')
                        {

                            location.href = result.redirectUrl;
                        } else
                        {
                            //alert('problem');
                            location.href = result.redirectUrl;
                        }
                    });
                });
            });
        });

    });
    
    //fail payemnt
     $(document).ready(function () {
        $('.subscription-fail-payment-submit').on('click', function (e) {
            e.preventDefault();
            var data = $('#subscription-fail-modal-form').serialize();
            var url = $('#subscription-fail-modal-form').attr('action');

            $.ajax({
                method: "POST",
                url: url,
                data: data,
                dataType: 'json'
            }).done(function (response) {


                _gopay.checkout({gatewayUrl: response.redirectUrl, inline: true}, function (checkoutResult) {

                    $.ajax({
                        method: "GET",
                        url: $('#subscription-fail-modal-rurl').val() + '?id=' + checkoutResult.id,
                        dataType: 'json'
                    }).done(function (result) {


                        if (result.status == 'PAID')
                        {
                            location.href = result.redirectUrl;
                        } else
                        {
                            //alert('problem');
                            location.href = result.redirectUrl;
                        }
                    });
                });
            });
        });

    });
};
