Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

var Validator = function(){
    this.errors = {};
    this.elements;
};

Validator.prototype.setElements = function(el)
{
   this.elements = el;
};

Validator.prototype.checkRequired = function(names)
{
    validator = this;
    $.each(names,function(index,val){
       if(validator.elements[val].val() == '')
       {
           validator.errors[val] = { message: 'Required Field',element: validator.elements[val]};
       }
    });
};

Validator.prototype.getErrors = function()
{
    return this.errors;
};

Validator.prototype.getError = function(index)
{
    return this.errors[index];
};

Validator.prototype.hasErrors = function()
{
    var size = 0, key;
    for (key in this.errors) {
        if (this.errors.hasOwnProperty(key)) size++;
    }

    if(size > 0)
    {
        return true;
    }
    
    return false;
};

/*TEAM PLAYERS*/
var TeamPlayersManager = function()
{
    this.rating_url = '/player/add-rating';
    this.rating_trigger_class = '.score_trigger';
     
    this.change_role_url = '/team/players/edit';
    this.change_role_trigger_class = '.change-role-trigger';
    this.modal;
   
};

TeamPlayersManager.prototype.bindTriggers = function(){
    var teamPlayers = this;
    
    $(this.change_role_trigger_class).on('click',function(e){
        e.preventDefault();
        teamPlayers.changeRole($(this));
    });
    
    $(this.rating_trigger_class).on('click',function(e){
        e.preventDefault();
        teamPlayers.sendRating($(this));
    });
    

    $('#add_player').on('click',function(e){
        e.preventDefault();
        if(teamPlayers.validateData() == true)
        {
            teamPlayers.modal = $('#addPlayerModal').modal();
        }
        
        
        
    });
    
   $('.add_player_form_trigger').on('click',function(e){
        e.preventDefault();
        teamPlayers.sendForm();
   });
};



TeamPlayersManager.prototype.validateData = function()
{
    elements = [];
    elements['first_name'] = $('#player_first_name');
    elements['last_name'] = $('#player_last_name');
   

    validator = new Validator(); 
    validator.setElements(elements);
    validator.checkRequired(['first_name','last_name']);
    
    $('.control-label-error').remove();

    if(validator.hasErrors())
    {
         errors = validator.getErrors();
          for (key in errors) {
            errors[key].element.after('<label class="control-label-error" for="inputError">'+ errors[key].message+'</label>');
          }
          return false;
    }
    else
    {
        return true;
    }
}

TeamPlayersManager.prototype.changeRole = function(elem){
    var select = $('#'+elem.attr('data-rel'));
    var player = {id:select.attr('data-id'),team_role:select.val()};
    
    var data = {players:[player]};
     $.ajax({
            method: "GET",
            url: this.change_role_url,
            data: data,
            dataType: 'json'
        }).done(function (response) {

           console.log(response);
        
        });
};

TeamPlayersManager.prototype.sendRating = function(elem){
     var rating = elem.attr('data-score');
     var player_id = elem.attr('data-player');
     var match_id = elem.attr('data-match');
     var data = {rating:rating,player_id:player_id,match_id:match_id};
     $.ajax({
            method: "GET",
            url: this.rating_url,
            data: data,
            dataType: 'json'
        }).done(function (response) {

           console.log(response);
        
        });
};

TeamPlayersManager.prototype.sendForm = function()
{
    form_data = $('#add_player_form').serialize();
     $.ajax({
            method: "GET",
            url: "/team/create-player",
            data: form_data
        }).done(function (response) {
           location.reload();
        });
    
    
};


