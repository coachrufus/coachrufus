import React, { Component } from 'react';
import axios from 'axios';
const Config = require('./../Config.jsx');
import Translations from  './../Translations.js';
var Trans;
class PlayerItem extends Component {

    constructor(props) {
        super(props);
         Trans = Translations.getTranslations(this.props.lang);
        var player = this.props.player;
        var player_goals = 0;
        var player_assist = 0;
        var timelineItems = this.props.timeline.timeline_items;
        var timelineItemsKeys = Object.keys(timelineItems);
        var i = 0;
        
        timelineItemsKeys.forEach(function (k) {
            if("goal" in timelineItems[k])
            {
               if(timelineItems[k].goal.linup_player_id == player.id )
               {
                   player_goals++;
               }
            }
            if("assist" in timelineItems[k])
            {
               if(timelineItems[k].assist.linup_player_id == player.id )
               {
                   player_assist++;
               }
            }
            if("mom" in timelineItems[k])
            {
               //momValue = 'mom_'+timelineItems[k].mom.linup_player_id;
            }
        });
        

         this.state = {
            player:player,
            goalValue:player_goals > 0 ? player_goals : '-',
            assistValue:player_assist > 0 ? player_assist : '-',
        };
        
        this.choalChoices = [];
        this.assistChoices = [];
        var i = 0;
        for(i=0;i<21;i++)
        {
            this.choalChoices[i] = i;
            this.assistChoices[i] = i;
        }
        
       
        
         this.handleChangeGoalValue = this.handleChangeGoalValue.bind(this);
         this.handleChangeAssistValue = this.handleChangeAssistValue.bind(this);
         this.handleChangeMomValue = this.handleChangeMomValue.bind(this);
    }
    
    handleChangeMomValue(event){
        //this.setState({momValue: event.target.value});
        this.props.onMomUpdate(event,'mom_'+event.target.value);

        const addScoreUrl = encodeURI( Config.BASE_URL + '/team/match/add-simple-timeline-stat');
        var params = new URLSearchParams();
        var player = this.state.player;


        params.append("event_id", player.event_id);
        params.append("event_date", player.event_date);
        params.append("lid", player.lineup_id);
        params.append("time",'00:00:00'); //00:00:00
        params.append("mom[player_id]",player.player_id);
        params.append("mom[lineup_player_id]",player.id);
        params.append("mom[time]",'00:00:00');
        params.append("mom[lid_team]",player.lineup_position); 


        var request = {
          params: params
        };
        axios.get(addScoreUrl, request).then(res => {

        });;
        
        
    }
    
  handleChangeGoalValue(event) {
   
    var oldValue = this.state.goalValue;
    var newValue = event.target.value;
    if(this.state.goalValue == '-')
    {
        oldValue = 0;
    }
    if(newValue == '-')
    {
        newValue = 0;
    }
        
    var diff =  newValue - oldValue;
   
    this.setState({goalValue: event.target.value});
    const addScoreUrl = encodeURI( Config.BASE_URL + '/team/match/add-simple-timeline-stat');
    var params = new URLSearchParams();
    var player = this.state.player;
    
    //var timelineEventTime = time.hour+':'+time.minute+':'+time.second;
    
    params.append("event_id", player.event_id);
    params.append("event_date", player.event_date);
    params.append("lid", player.lineup_id);
    params.append("time",'00:00:00'); //00:00:00
    params.append("goal[count]",event.target.value);
    params.append("goal[player_id]",player.player_id);
    params.append("goal[lineup_player_id]",player.id);
    params.append("goal[time]",'00:00:00');
    params.append("goal[lid_team]",player.lineup_position); 
  
    this.props.onUpdate(event,diff,player.lineup_position); 
    var request = {
      params: params
    };
    axios.get(addScoreUrl, request).then(res => {

            
    });;

  }
  
  
  handleChangeAssistValue(event) {
    this.setState({assistValue: event.target.value});
    const addScoreUrl = encodeURI( Config.BASE_URL + '/team/match/add-simple-timeline-stat');
    var params = new URLSearchParams();
    var player = this.state.player;
    
    //var timelineEventTime = time.hour+':'+time.minute+':'+time.second;
    
    params.append("event_id", player.event_id);
    params.append("event_date", player.event_date);
    params.append("lid", player.lineup_id);
    params.append("time",'00:00:00'); //00:00:00
    params.append("assist[count]",event.target.value);
    params.append("assist[player_id]",player.player_id);
    params.append("assist[lineup_player_id]",player.id);
    params.append("assist[time]",'00:00:00');
    params.append("assist[lid_team]",player.lineup_position); 
  
   
    var request = {
      params: params
    };
    axios.get(addScoreUrl, request).then(res => {
         
    });;

  }
  
  

   

    render() {
        
       var player = this.state.player;
        
        return (
                <div className="row  playerSelector-item">

                           
                               <div className="col-xs-6">
                                  <div className="playerNumber">{player.playerNumber}</div> 
                                  <div className="playerName">{player.player_name}  { player.efficiency === 'none' ?  "" : "("+player.efficiency+")" }<span className="player-position">{player.player_position}</span></div>
                              </div>
                              <div className="col-xs-2 hiddenSelect">
                                <select value={this.state.goalValue} className={'selected'+this.state.goalValue} onChange={this.handleChangeGoalValue}>
                                        <option value="-">&nbsp;&nbsp;+ </option>
                                         {
                                            this.choalChoices.map((choice, key) => {
                                                        return (
                                                                <option key={'g'+key} value={choice}>&nbsp;&nbsp;{choice}</option>
                                                       )}
                                                   )
                                              }
                                  </select>
                              </div>
                              <div className="col-xs-2 hiddenSelect">
                                <select value={this.state.assistValue} className={'selected'+this.state.assistValue} onChange={this.handleChangeAssistValue}>
                                <option value="-">&nbsp;&nbsp;+ </option>
                                         {
                                            this.choalChoices.map((choice, key) => {
                                                        return (
                                                                <option key={'g'+key} value={choice}>&nbsp;&nbsp;{choice}</option>
                                                       )}
                                                   )
                                              }
                                  </select>
                              </div>
                              <div className="col-xs-2">
                                <p>
                                <input type="radio" name="mom" id={'mom_'+player.id } value={player.id } checked={this.props.mom == 'mom_'+player.id }   onChange={this.handleChangeMomValue}/>
                                <label htmlFor={'mom_'+player.id }>&nbsp;</label>
                              </p>
                            </div>
                          
                         </div>
                   
                 );
    }
}
;


export default PlayerItem;
