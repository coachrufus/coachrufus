import React, { Component } from 'react';
import axios from 'axios';

const Config = require('./../Config.jsx');
import Translations from  './../Translations.js';
var Trans;
class NavBar extends Component {


    constructor(props) {
        super(props);
        Trans = Translations.getTranslations(this.props.lang);
        this.messageOrigin;
        this.messageSource;

        this.componentDidMount = this.componentDidMount.bind(this); 
        this.initHandler = this.initHandler.bind(this); 
        this.sendMessage = this.sendMessage.bind(this);
    };

    initHandler(e)
    {
        this.messageOrigin = e.origin;
        this.messageSource = e.source;
    }

    componentDidMount() {
        window.addEventListener('message', this.initHandler);
    }

    sendMessage() {
        var message = {
            'action': this.props.action
        };

        window.postMessage(JSON.stringify(message) ,'*');
    };
    
    closeMatch() {
        
        const addScoreUrl = encodeURI( Config.BASE_URL + '/team/match/add-simple-timeline-stat');
        var params = new URLSearchParams();
        params.append("close", 'true');
        params.append("lid", this.props.lineup.id);
        
        params.append("event_id", this.props.event.id);
        params.append("event_date", this.props.event.current_date);
       

        var request = {
          params: params
        };
        axios.get(addScoreUrl, request).then(res => {

        });;
        
         setTimeout(() => {
                       var message = {
                               'action': 'close',
                                'screen': 'results'
                            };
                        window.postMessage(JSON.stringify(message), '*');
                      }, 300);
        
        
    };

    render() {
        return (
            <div className="nav-topbar">
            <div className="row">
                 <div className="col-xs-4" onClick={(e) => {this.sendMessage()}}>{Trans.cancel}</div>
                 <div className="col-xs-4">{Trans.title}</div>
                 <div className="col-xs-4" onClick={(e) => {this.closeMatch()}} >
                     <span  className="save-btn">{Trans.save}</span>
                             </div>
            </div>
               
              
            </div>

        );
    };
};


export default NavBar;
