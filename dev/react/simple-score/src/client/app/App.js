import React, {
  Component
} from "react";
import axios from 'axios';
import humanDate from 'human-date';
import NavBar from  './components/NavBar.js';
import PlayerItem from  './components/PlayerItem.js';
import Translations from  './Translations.js';
var Trans;
const Config = require('./Config.jsx');


class App extends Component {
  constructor(props) {
    super(props);
 Trans = Translations.getTranslations(this.props.lang);
    this.state = {
      lineup: {}, 
      event: {},
      timeline:{},
      lineupLoaded:false,
      eventLoaded:false,
      timelineLoaded:false,
      eventId:this.props.eventId,
      lineupId:this.props.lineupId,
      eventCurrentDate:this.props.eventCurrentDate,
      uid:this.props.uid,
      firstLineGoals:0,
      secondLineGoals:0,
      mom:'',
      alertVisible:false
    };

    this.handleAddScoreEvent = this.handleAddScoreEvent.bind(this);
    this.handleAddMatchEnd = this.handleAddMatchEnd.bind(this); 
    this.handleUpdateScore = this.handleUpdateScore.bind(this); 
    this.handleMomUpdate = this.handleMomUpdate.bind(this); 
  }

  componentDidMount() {
     this.loadLineup(); 
     this.loadEventDetail();
     this.loadTimeline();
    
  }

  loadEventDetail()
  {
     
    const eventDataUrl = Config.API_URL+'/event/detail?apikey='+Config.API_KEY+'&uid='+this.state.uid+'&id='+this.state.eventId+'&date='+this.state.eventCurrentDate;
    axios.get(eventDataUrl)
      .then(res => {
        var eventData = res.data;        
        var today = new Date();
        var tomorrow = new Date();
        tomorrow.setDate(today.getDate()+1);
        tomorrow.setHours(23);
        tomorrow.setMinutes(59);
        tomorrow.setSeconds(59);
        
        
        var yesterday = new Date();
        yesterday.setDate(today.getDate()-1);
       
        if(eventData.termin <  (tomorrow.getTime() / 1000) && eventData.termin >  (yesterday.getTime() / 1000))
        {
            eventData.date = humanDate.relativeTime(eventData.termin - (new Date().getTime() / 1000));
        }
        else
        {
           var eventDate = new Date(eventData.termin*1000);
           eventData.date =  eventDate.getDate()+'.'+(eventDate.getMonth()+1)+'.'+eventDate.getFullYear()+', '+eventDate.getHours()+':'+eventDate.getMinutes();
        }      
        this.setState({
            event: eventData,
            eventLoaded: true
          });
      });
        
  }
  
  loadLineup()
  {

     const lineupDataUrl =  Config.API_URL+'/content/game/lineup?apikey='+Config.API_KEY+'&user_id='+this.state.uid+'&event_id='+this.state.eventId+'&event_current_date='+this.state.eventCurrentDate+'&lid='+this.state.lineupId;
    var lineupId = null;
    var lineup = null;
    axios.get(lineupDataUrl)
      .then(res => {
        if (res.data.id == null)
        {
          return false;
        }
        else {
          var lineupStatus =  res.data.status;
          this.setState({
            lineup: res.data,
            lineupLoaded:true,
            lineupStatus: lineupStatus
          });
        }
      });
  }
  
  loadTimeline()
  {
        const timelineDataUrl = Config.API_URL + '/content/game/timeline?apikey=' + Config.API_KEY + '&user_id=' + this.props.uid + '&event_id=' + this.props.eventId + '&event_current_date=' + this.props.eventCurrentDate + '&lid=' + this.state.lineupId;
        axios.get(timelineDataUrl)
                .then(res => {
                    
                     var momValue = '';
                    var timelineItems = res.data.timeline_items;
                     var timelineItemsKeys = Object.keys(timelineItems);
                     timelineItemsKeys.forEach(function (k) {
                        if("mom" in timelineItems[k])
                        {
                           momValue = 'mom_'+timelineItems[k].mom.linup_player_id;
                        }
                    });
                    
                    
                  
                    this.setState({
                        firstLineGoals: parseInt(res.data.match_result.first_line) ,
                        secondLineGoals: parseInt(res.data.match_result.second_line),
                        timeline: res.data,
                        timelineLoaded:true,
                        mom: momValue
                    });
                    
                });
    }


 
  handleAddScoreEvent(e,goalPlayer,assistPlayer,time){
    this.setState({ 
             timelineLoaded: false
           });
              
    var lid_team = '';
    if('first_line' == this.state.currentLine)
    {
      lid_team = 'first_line';
    }
     if('second_line' == this.state.currentLine)
    {
      lid_team = 'second_line';
    }

    const addScoreUrl = encodeURI( Config.BASE_URL + '/team/match/add-timeline-stat');
    var params = new URLSearchParams();
    

    
    var timelineEventTime = time.hour+':'+time.minute+':'+time.second;
    
    params.append("event_id", this.state.eventId);
    params.append("event_date", this.state.eventCurrentDate);
    params.append("lid", this.state.lineupId);
    params.append("time",timelineEventTime); //00:00:00
    params.append("goal[player_id]",goalPlayer.player_id);
    params.append("goal[lineup_player_id]",goalPlayer.id);
    params.append("goal[time]",timelineEventTime);
    params.append("goal[lid_team]",lid_team); 
    params.append("assist[player_id]",assistPlayer.player_id);
    params.append("assist[lineup_player_id]",assistPlayer.id); 
    params.append("assist[time]",timelineEventTime);
    params.append("assist[lid_team]",lid_team);
   
    var request = {
      params: params
    };
    axios.get(addScoreUrl, request).then(res => {
          this.setState({ 
              timelineLoaded: true,
              PlayerSelectorVisible: false
           });
           
           /*
           setTimeout(() => {
              this.setState({ 
                alertVisible:false
             });
                
              }, 3000);
            */
      
    });;
  };
  
  


  handleAddMatchEnd(e,time){
    //this.handleShowPlayerChoices(e,'mom');
  };
  
  handleMomUpdate(e,val){
    
        this.setState({
            mom:val
        });  
  };

 handleUpdateScore(e,goals,lineup){
    //e.preventDefault();
    
    if('first_line' == lineup)
    {
        var newGoals = parseInt(this.state.firstLineGoals)+ parseInt(goals);
        this.setState({
            firstLineGoals:newGoals,
            alertVisible:true
        });
    
    }
    
    if('second_line' == lineup)
    {
        var newGoals = parseInt(this.state.secondLineGoals)+parseInt(goals);

        this.setState({
            secondLineGoals:newGoals,
             alertVisible:true
        });
    
    }
    
    setTimeout(() => {
              this.setState({ 
                alertVisible:false
             });
                
              }, 1000);
    
  }
 

    render() {
        var firstLineup = [];
        var secondLineup = [];
        var lineup = this.state.lineup;
       
        if(this.state.lineupLoaded && this.state.timelineLoaded)
        {
            var firstlineupKeys = Object.keys(lineup.firstLine);
            var secondlineupKeys = Object.keys(lineup.secondLine);
             var i = 0;
            firstlineupKeys.forEach(function (k) {
                i++;
                firstLineup[i] = lineup.firstLine[k].player;
                firstLineup[i].playerNumber = lineup.firstLine[k].teamPlayer.player_number;
                firstLineup[i].efficiency = lineup.firstLine[k].efficiency;
            });
            i = 0;
             secondlineupKeys.forEach(function (k) {
                i++;
                secondLineup[i] = lineup.secondLine[k].player;
                secondLineup[i].playerNumber = lineup.secondLine[k].teamPlayer.player_number;
                secondLineup[i].efficiency = lineup.secondLine[k].efficiency;
            });
        }
       
        
        
        
        
        return (
          <div className="App">
          {this.state.eventLoaded ?  
         
           <div className="container-fluid">
           
           <div className={`alert  ${this.state.alertVisible ? 'alert-shown' : 'alert-hidden'}`}>
            Skóre bolo zapísané
          </div>

           
           <div className="header">
                <NavBar   lang={this.props.lang} title="Fast score" action="close" lineup={this.state.lineup} event={this.state.event} />

                 <div className="row event-info">
                     <div className="col-xs-12 event-date">{  this.state.event.date }</div>
                     <div className="col-xs-12 event-name">
                       {this.state.event.name} 
                      </div>
                 </div>

                 <div className="row vs-row">
                     <div className="col-xs-5 vs-first">{this.state.lineup.firstLineName}</div>
                     <div className="col-xs-2 match-points"><span id="firstLinePoints">{this.state.firstLineGoals}</span>:<span id="secondLinePoints">{this.state.secondLineGoals}</span></div>
                     <div className="col-xs-5 vs-second">{this.state.lineup.secondLineName}</div>
                 </div>
            </div>
            <div className="row">
                <div className="col-sm-6 firstline-players">
                     <div className="row lineup-head-row">
                          <div className="col-xs-6">{this.state.lineup.firstLineName}</div>
                          <div className="col-xs-2">G</div>
                          <div className="col-xs-2">A</div>
                          <div className="col-xs-2">MoM</div>
                       </div>
                  {
                    firstLineup.map((player, key) => {
                      return (
                              <PlayerItem  mom={this.state.mom} onMomUpdate={this.handleMomUpdate} onUpdate={this.handleUpdateScore}  timeline={this.state.timeline} player={player} key={'f'+key} />
                     )}
                 )
                    
            }
                </div>
                <div className="col-sm-6 secondline-players">
                     <div className="row lineup-head-row">
                          <div className="col-xs-6">{this.state.lineup.secondLineName}</div>
                          <div className="col-xs-2">G</div>
                          <div className="col-xs-2">A</div>
                          <div className="col-xs-2">MoM</div>
                       </div>
                  {
                    secondLineup.map((player, key) => {
                      return (
                              <PlayerItem   lang={this.props.lang} mom={this.state.mom} onMomUpdate={this.handleMomUpdate}  onUpdate={this.handleUpdateScore} timeline={this.state.timeline} player={player} key={'s'+key} />
                     )}
                 )
                    
            }
                </div>

            </div>
                 
            </div>
           

        : null }
        </div>
       
        );
    }
}

export default App;