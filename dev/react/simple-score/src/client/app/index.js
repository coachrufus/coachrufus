import React from "react";
import ReactDOM from "react-dom";
import App from "./App.js";

var targetElement = document.getElementById('simple-score');
var eventId = targetElement.getAttribute("data-event-id");
var eventCurrentDate = targetElement.getAttribute("data-event-current-date");
var uid = targetElement.getAttribute("data-uid");
var apiProvider = targetElement.getAttribute("data-api-provider");
var lineupId = targetElement.getAttribute("data-lineup-id");
var lang = targetElement.getAttribute("data-lang");
ReactDOM.render(
  <App uid={uid} eventId={eventId} eventCurrentDate={eventCurrentDate} apiProvider={apiProvider} lineupId={lineupId}  lang={lang} />,
  document.getElementById("simple-score")
);
