import React, { Component } from 'react';

class Translations
{
    constructor()
    {
        this.lang = 'sk';
        this.translations = {
            title: {
                sk: 'Rýchly zápis',
                en: 'Fast Input'
            },
            cancel: {
                sk: 'Zrušiť',
                en: 'Cancel'
            },
            save: {
                sk: 'Uložiť',
                en: 'Save'
            },
            seconds: {
                sk: 'sekúnd od teraz',
                en: 'seconds from now'
            },
            minutes: {
                sk: 'minúty',
                en: 'minutes'
            },
            hour: {
                sk: 'hodina',
                en: 'hour'
            },
            hours: {
                sk: 'hodín',
                en: 'hours'
            },
            from_now: {
                sk: 'od teraz',
                en: 'from now'
            },
            ago: {
                sk: 'dozadu',
                en: 'ago'
            },
            fastScore: {
                sk: 'Rýchly zápis',
                en: 'Fast Input'
            },

        }
    }

    getTranslations(lang) {
        var keys = Object.keys(this.translations);
        var allTrans = this.translations;
        var translations = {};
        //var lang = lang;
        keys.forEach(function (k) {
            translations[k] = allTrans[k][lang];
        });

        return translations;
    }
}

export  default Translations = new Translations();

