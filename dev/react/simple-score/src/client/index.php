<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="apple-touch-icon" href="http://app.coachrufus.com/apple_icon.png">
        <title>Coach Rufus</title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,700|Roboto:400,700&amp;subset=cyrillic" rel="stylesheet">
        <link media="all" rel="stylesheet" href="/dev/theme/bootstrap/css/bootstrap.grid.css" />
    
     <link media="all" rel="stylesheet" href="public/styles/index.css?ver=<?php echo time() ?>" />
  </head>
  <body>
      <div id="simple-score" data-lineup-id="6894" data-event-id="6707" data-event-current-date="2019-11-18" data-api-provider="local" data-uid="3" data-lang="en"></div>
      
      
      
    <script src="public/bundle.js?ver=<?php echo rand(1,100) ?>" type="text/javascript"></script>
  </body>
</html>
