import React, { Component } from 'react';
import TimelineItem from  './../components/TimelineItem.js';
import axios from 'axios';

const Config = require('./../Config.js');

class Timeline extends Component {

    constructor(props) {
        super(props);
        this.state = {
            timelineItems: [],
        };
    }

    createTimelineItems(timeline) {
        var timelineItems = [];
        var timelineProp = timeline;
        var i = 0;
        var timelineKeys = Object.keys(timelineProp);
        timelineKeys.forEach(function (k) {
            timelineProp[k].key = k;
            timelineItems[i++] = timelineProp[k];
        });
        return timelineItems;
    }

    loadTimeline()
    {
        

         var timelineItems = this.createTimelineItems(this.props.timelineItems);
                    this.setState({
                        timelineItems: timelineItems,
                        timelineLoaded: true
                    });

    }

    componentDidMount() {
        this.loadTimeline();
    }

    renderMomLineItem(group, key)
    {
        if (group.lineup_position == 'first_line')
        {
            return (
                    <div className="timeline-item timeline-item-mom" key={key} >
                        <div className="first-line">
                            <div className="goal-row">
                                <span className="player-name">{group.mom.player}</span>
                                <div className="avatar"><img src={this.props.baseUrl + group.mom.player_avatar} /></div>
                            </div>
                            <div className="icon"><i className="ico ico-cup"></i></div>
                        </div>
                    
                    
                        <div className="second-line">
                            <div className="time">
                                MOM
                            </div>
                        </div>
                    </div>
                    );
        }

        if (group.lineup_position == 'second_line')
        {
            return (
                    <div className="timeline-item timeline-item-mom second-line-item" key={key} >
                        <div className="first-line">
                            <div className="time">
                                MOM
                            </div>
                        </div>
                    
                    
                        <div className="second-line">
                            <div className="goal-row">
                    
                                <div className="avatar"><img src={this.props.baseUrl + group.mom.player_avatar} /></div>
                                <span className="player-name">{group.mom.player}</span>
                            </div>
                            <div className="icon"><i className="ico ico-cup"></i></div>
                        </div>
                    </div>
                    );
        }
    }


    renderItems()
    {
        var timelineItems = this.state.timelineItems;
        
       
            return (
                
                    timelineItems.map((group, key) => {
                        return (
                                <TimelineItem  lang={this.props.lang}  key={key} group={group} groupId={key} />
                                )
                    })
            )
        
        
        
    }

    render() {

        var wrapperClassName = "timeline-wrap";
        //console.log('render timeline');
        //console.log(this.state.timelineItems);



        if (this.state.PlayerSelectorVisible)
        {
            wrapperClassName = "timeline-wrap timeline-wrap-hidden";
        }

        if (this.state.timelineItems.length < 1)
        {
            wrapperClassName = "timeline-wrap timeline-wrap-hidden";
        }


        return (
                <div className={wrapperClassName}>
                    <div className="row timeline" >
                        {this.renderItems()}
                        <div className="timeline-item timeline-item-start">
                            <div className="icon"><i className="ico ico-clock"></i></div>
                        </div>
                    </div>
                </div>

                );
    }
}
;



export default Timeline;
