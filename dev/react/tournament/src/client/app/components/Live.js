import React, { Component } from 'react';
import axios from 'axios';
const Config = require('./../Config.js');
import Translations from  './../Translations.js';
import LiveDetail from  './../components/LiveDetail.js';
var Trans;
class Live extends Component {

    constructor(props) {
        super(props);
        Trans = Translations.getTranslations(this.props.lang);
        
        var currentGame = null;
        var currentGameId = null;
        var currentView = this.props.currentScreen;
        if(currentView == 'detail')
        {
            currentView = 'loader';
        }


        this.state = {
            allGames:this.props.games,
            liveGames: [] ,
            currentView: currentView,
            currentGameId: currentGame,
            currentGame: currentGameId
        };
        
        this.handleGameDetail = this.handleGameDetail.bind(this);
    }

    componentDidMount() {
        this.loadEvents();
    }
    
    
    loadEvents()
    {
        const dataUrl = Config.API_URL + '/tournament/live/events?apikey=' + Config.API_KEY + '&user_id=' + this.props.uid + '&tid=' + this.props.tournamentId;
        axios.get(dataUrl)
                .then(res => {
                    
                    
                    var currentGame = null;
                    var currentGameId = null;
                    if(this.props.currentScreen == 'detail')
                    {
                       currentGameId = this.props.gameDetailId;
                       
                       //first check live games
                        res.data.data.map((game, key) => {
                            if(game.id == this.props.gameDetailId)
                            {
                                currentGame = game;
                            }
                        });
                        //check all games
                        if(currentGame == null)
                        {
                              
                            Object.keys(this.state.allGames).map((key) => {
                                this.state.allGames[key].map((game, gkey) => {
                                    if(game.id == this.props.gameDetailId)
                                    {
                                        currentGame = game;
                                    }
                                });
                              });
                              
                    
                           
                        }
                        
                       this.props.updateTopNav(Trans.topNavLiveDetail); 
                       this.props.updateTopNavBacklink('live'); 
                       
                        this.setState({
                         currentView:'detail',
                         liveGames: res.data.data,
                         currentGameId: currentGameId,
                         currentGame: currentGame
                     });
                     
                      
                    }
                    else
                    {
                          this.setState({
                            liveGames: res.data.data,
                        });
                    }
                  

                });
    }
    


 renderGames()
    {
        var games = this.state.liveGames;
        return (
                <div className="games">{
                
                    games.map((game, key) => {
                        return (
                                <div onClick={(e) => {this.handleGameDetail(game.id)}}   className="gameRow gameLiveRow" key={game.id}>
                                    <div className="cell game_notify"><img src="/theme/img/icon/bell.svg" /></div>
                                    <div className="cell game_teams">
                                        <div className="game_team_row">
                                            <img src={game.team.icon} />
                                            {game.team.name}   
                                             <div className="game_team_row_points">{game.lineup.match_result.first_line}</div>
                                        </div>
                                        <div className="game_team_row">
                                            <img src={game.opponent_team.icon} />
                                            {game.opponent_team.name}   
                                            <div className="game_team_row_points">{game.lineup.match_result.second_line}</div>       
                                        </div>
                                    </div>
                                    <div className="time">
                                        <i className="ico ico-clock"></i>
                                        <span>{game.lineup.match_time.minutes}'</span>   
                                    </div>
                                   
                                </div>
                                );
                    })
                        
                }</div>       
                        
        );
    }
    
    renderGameDetail()
    {

        return (
                <LiveDetail game={this.state.currentGame}  eventId={this.state.currentGameId} />
                    
        );

    }
    
  
    handleGameDetail(id) {
         
         var currentGame;
         this.state.liveGames.map((game, key) => {
             if(game.id == id)
             {
                 currentGame = game;
             }
         });
        this.props.updateTopNav(Trans.topNavLiveDetail); 
        this.props.updateTopNavBacklink('live'); 

        this.setState({
            currentView: 'detail',
            currentGameId:id,
            currentGame:currentGame
        });
    }

    render() {

        if(this.state.currentView == 'list')
        {
            return this.renderGames();
        }
        
         if(this.state.currentView == 'detail')
        {
            return this.renderGameDetail();
        }
        
          if(this.state.currentView == 'loader')
        {
            return (<div className="loader"><img src="/img/load.gif" /><br />Loading</div>)
        }
        
        
          
    }
}
;


export default Live;
