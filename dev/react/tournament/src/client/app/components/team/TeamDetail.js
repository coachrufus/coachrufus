import React, { Component } from 'react';
import axios from 'axios';
const Config = require('./../../Config.js');
import Translations from  './../../Translations.js';
import Players from  './Players.js';
import GamesList from  './../../components/game/GamesList.js';
var Trans;
class TeamDetail extends Component {

    constructor(props) {
        super(props);
        Trans = Translations.getTranslations(this.props.lang);
        var teamGames = [];
        
        
        Object.keys(this.props.games).map((key) => {
            this.props.games[key].map((game, key) => {
                if (game.team_id == this.props.teamId || game.opponent_team_id == this.props.teamId)
                {
                    var safarDate =game.start.replace('-','/').replace('-','/');
                    var eventTerminDate = new Date(safarDate);
                    var eventTerminMinutes = (eventTerminDate.getMinutes()<10?'0':'') + eventTerminDate.getMinutes();
                    var eventTerminHours = (eventTerminDate.getHours()<10?'0':'') + eventTerminDate.getHours();
                    var eventTerminIndex = eventTerminDate.getDate()+'.'+(eventTerminDate.getMonth()+1)+'.'+eventTerminDate.getFullYear()+' '+eventTerminHours+':'+eventTerminMinutes+'-'+game.id;
                    var keyExist = eventTerminIndex in teamGames;

                    if(!keyExist)
                    {
                        teamGames[eventTerminIndex] = [];
                    }
                    teamGames[eventTerminIndex].push(game);
                }
            });
        });

        
        this.handleUpdateTopNav = this.handleUpdateTopNav.bind(this);
        this.updateTopNavBacklink = this.updateTopNavBacklink.bind(this);    
        
       
        
        var games = <GamesList key={Math.random()}  lang={this.props.lang} games={teamGames} currentScreen="list" updateTopNav={this.handleUpdateTopNav} updateTopNavBacklink={this.updateTopNavBacklink}  />;

        this.state = {
            players:null,
                games:<GamesList key={Math.random()}  games={teamGames} currentScreen="list" updateTopNav={this.handleUpdateTopNav} updateTopNavBacklink={this.updateTopNavBacklink}  />,
            currentScreen:'games'
        };
        
        this.handleTabClick = this.handleTabClick.bind(this);
    }
    
    componentDidMount() {
        this.loadPlayers();
    }
    
    
    loadPlayers()
    {
        const dataUrl = Config.API_URL + '/tournament/teams/players?apikey=' + Config.API_KEY + '&user_id=' + this.props.uid + '&tid=' + this.props.teamId;
        axios.get(dataUrl)
                .then(res => {
                    this.setState({
                        players: <Players players={res.data.data} />
                    });

                });
    }
   
    handleTabClick(e) {
        e.preventDefault();
        
         this.setState({
            currentScreen: e.target.id
        });
    }
    
      handleUpdateTopNav(title)
    {
       this.props.updateTopNav(Trans.gameDetailSubnavTitle);
    }
    
     updateTopNavBacklink(screen)
    {
       this.props.updateTopNavBacklink('teams');
    }


    render() {
        var currentScreen;
        var gamesTabClass = 'subnavItem';
        var playersTabClass = 'subnavItem';
        if(this.state.currentScreen == 'games')
        {
            currentScreen = this.state.games;
            gamesTabClass = 'subnavItem active';
        }
        
        if(this.state.currentScreen == 'players')
        {
            currentScreen = this.state.players;
            playersTabClass = 'subnavItem active';
        }
        
        
        return (
                <div id="teamDetail">
                    <div className="subnav">
                        <div onClick={this.handleTabClick} id="games" className={gamesTabClass}>
                            {Trans.teamDetailSubnavEvents}
                        </div>
                        <div onClick={this.handleTabClick} id="players" className={playersTabClass}>
                            {Trans.teamDetailSubnavPlayers}
                        </div>
                    </div>
                    
                    {currentScreen}

                </div>

                );
    }
}
;


export default TeamDetail;
