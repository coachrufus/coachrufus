import React, { Component } from 'react';
import axios from 'axios';
const Config = require('./../../Config.js');
import Translations from  './../../Translations.js';
import TeamDetail from  './TeamDetail.js';
var Trans;
class Teams extends Component {

    constructor(props) {
        super(props);
        Trans = Translations.getTranslations(this.props.lang);

        this.state = {
            teams:this.props.teams,
            currentView: this.props.currentScreen,
            currentTeamId: null,
        };
        
        
        this.handleTeamDetail = this.handleTeamDetail.bind(this);
        this.handleUpdateTopNav = this.handleUpdateTopNav.bind(this);
        this.updateTopNavBacklink = this.updateTopNavBacklink.bind(this);    
    }
    
     
    handleUpdateTopNav(title)
    {
       
        this.props.updateTopNav(title);
    }
    
     updateTopNavBacklink(screen)
    {
        this.props.updateTopNavBacklink(screen);
    }

  
    renderTeams()
    {
        var teams = this.state.teams;
        return (
                <div className="teams">{
                
                    teams.map((team, key) => {
                        return (
                                <div onClick={this.handleTeamDetail} id={team.id}   className="teamRow" key={Math.random()}>
                                    <div onClick={this.handleTeamDetail} id={team.id} className="cell avatar"><img src={team.icon} /></div>
                                    <div onClick={this.handleTeamDetail} id={team.id} className="cell title">{team.name}</div>
                                   
                                </div>
                                );
                    })
                        
                }</div>       
                        
        );
    }
    
    renderTeamDetail()
    {
        return (
                <TeamDetail key={this.state.currentTeamId} games={this.props.games} teamId={this.state.currentTeamId} lang={this.props.lang} updateTopNav={this.handleUpdateTopNav} updateTopNavBacklink={this.updateTopNavBacklink} />
                    
        );
    }

    handleTeamDetail(e) {
        var teamName = '';
        this.state.teams.map((team, key) => {
            if(team.id == e.target.id)
            {
                teamName = team.name;
            }
        });
        
            
        this.props.updateTopNav(teamName); 
        this.props.updateTopNavBacklink('teams'); 
        
        this.setState({
            currentView: 'detail',
            currentTeamId:e.target.id
        });
    }

    render() {

        if(this.state.currentView == 'list')
        {
            return this.renderTeams();
        }
        
         if(this.state.currentView == 'detail')
        {
            return this.renderTeamDetail();
        }
        
        
          
    }
}
;

 //onClick={ (e) => {this.handleTeamDetail(team.id)} } 
export default Teams;
