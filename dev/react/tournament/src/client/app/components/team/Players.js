import React, { Component } from 'react';
import axios from 'axios';
const Config = require('./../../Config.js');
import Translations from  './../../Translations.js';
var Trans;
class Players extends Component {

    constructor(props) {
        super(props);
        Trans = Translations.getTranslations(this.props.lang);
        this.state = {
            players:this.props.players,
        };
    }

    render() {
        return (
                    <div className="teamPlayers">{
                        this.state.players.map((player, key) => {
                                var playerNumber = '';
                                if(player.player_number != null)
                                {
                                    playerNumber = '#'+player.player_number;
                                }
            
                                    return (
                                    <div  className="teamPlayerRow" key={player.id}>
                                        <div  id={player.id} className="cell avatar"><img src={player.icon} /></div>
                                        <div  id={player.id} className="cell title">  {player.first_name}   {player.last_name} <span className="player_number">{playerNumber}</span> </div>

                                    </div>
                                    );
                        })

                    }</div>


                );
    }
}
;


export default Players;
