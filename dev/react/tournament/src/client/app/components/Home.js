import React, { Component } from 'react';
import axios from 'axios';
const Config = require('./../Config.js');
import Translations from  './../Translations.js';
var Trans;
class Home extends Component {

    constructor(props) {
        super(props);
        Trans = Translations.getTranslations(this.props.lang);

        this.state = {
            tournament: this.props.tournament,
        };
    }

 
    render() {


        var tournament = this.state.tournament;
        
        
        var safarStartDate = tournament.start_date.replace('-','/').replace('-','/');
        var safariEndDate = tournament.end_date.replace('-','/').replace('-','/');
        
        var startDate = new Date(safarStartDate);
        var endDate = new Date(safariEndDate);
        var startDateTermin = startDate.getDate()+'.'+(startDate.getMonth()+1)+'.'+startDate.getFullYear();
        var endDateTermin = endDate.getDate()+'.'+(endDate.getMonth()+1)+'.'+endDate.getFullYear();
        //console.log();
        return (
                <div className="home screen-cnt">
                    <div className="t-panel">
                        <div className="t-header">
                            <div className="team_img_round">

                            </div>
                            <h1>{tournament.name}</h1>
                        </div>
                         {tournament.register_status == 'allowed' ?  <a className="btn btn-join-tournament" href={tournament.share_link}>{Trans.joinTournament}</a> : null}
                         <div className="row">
                        <div className="col-sm-4">
                        <table className="table t-table">
                            <tbody>
                                <tr>
                                    <td className="t-label"><strong>{Trans.detail_start}</strong></td>
                                    <td>{startDateTermin}</td>
                                </tr>
                                <tr>
                                    <td><strong>{Trans.detail_end}</strong></td>
                                    <td>{endDateTermin}</td>
                                </tr>
                                <tr>
                                    <td><strong>{Trans.detail_gender}</strong></td>
                                    <td>{tournament.gender_name}</td>
                                </tr>
                                <tr>
                                    <td><strong>{Trans.detail_sport}</strong></td>
                                    <td>{tournament.sport_name}</td>
                                </tr>
                                <tr>
                                    <td><strong>{Trans.detail_level}</strong></td>
                                    <td>{tournament.level}</td>
                                </tr>
                                <tr>
                                    <td><strong>{Trans.detail_age}</strong></td>
                                    <td>{tournament.age_from} - {tournament.age_to}</td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                    <div className="col-sm-4">
                        <table className="table t-table">
                            <tbody>

                                <tr>
                                    <td  className="t-label"><strong>{Trans.detail_email}</strong></td>
                                    <td><a href="mailto:{tournament.email}">{tournament.email}</a></td>
                                </tr>

                                <tr>
                                    <td><strong>{Trans.detail_phone}</strong></td>
                                    <td><a href="tel:{tournament.phone}"></a></td>
                                </tr>

                                <tr>
                                    <td><strong>{Trans.detail_address}</strong></td>
                                    <td>{tournament.address}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    </div>
                    </div>
        
        
                     
                   
        
                   
        
        
                </div>

                );
    }
}
;


export default Home;
