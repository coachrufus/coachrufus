import React, { Component } from 'react';
import axios from 'axios';
const Config = require('./../../Config.js');
import Translations from  './../../Translations.js';
var Trans;
class Ladder extends Component {

    constructor(props) {
        super(props);
        Trans = Translations.getTranslations(this.props.lang);
     

        this.state = {
            ladder:this.props.ladder
        };
    }

    render() {
        return (
                <div className="ladder">
        
                {
                    Object.keys(this.state.ladder).map((key) => {
                        return (
                                <div className="ladderGroupWrap">
                                <h2>{Trans.ladderGroup} {key}</h2>
                                <table className="table table-stripped stats-table">
                                    <thead>
                                        <tr>
                                            <th>{Trans.ladderTableTeamTitle}</th>
                                             <th>{Trans.ladderTableGames}</th>
                                            <th>{Trans.ladderTableWins}</th>
                                            <th>{Trans.ladderTableWinsOvertime}</th>
                                            <th>{Trans.ladderTableDraws}</th>
                                            <th>{Trans.ladderTableLosses}</th>
                                            <th>{Trans.ladderTableLossesOvertime}</th>
                                            <th>{Trans.ladderTablePoints}</th>
                                            <th>{Trans.ladderTableScore}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                            {
                                             this.state.ladder[key].map((team,key) => {
                                                 return (
                                                         <tr>
                                                            <td>{team.team.name}</td>
                                                            <td>{team.games}</td>
                                                            <td>{team.wins}</td>
                                                            <td>{team.winsOvertime}</td>
                                                            <td>{team.draws}</td>
                                                            <td>{team.losses}</td>
                                                            <td>{team.lossesOvertime}</td>
                                                            <td>{team.points}</td>
                                                            <td>{team.goals}:{team.goalMinus}</td>
                                                        </tr>      
                                                )
                                            })
                                            }
                                    </tbody>
                                </table>
                                </div>
                                )
                    })
                }
        
                
                </div>

                );
    }
}
;


export default Ladder;
