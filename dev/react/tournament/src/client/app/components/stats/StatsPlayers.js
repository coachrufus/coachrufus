import React, { Component } from 'react';
import axios from 'axios';
const Config = require('./../../Config.js');
import Translations from  './../../Translations.js';
var Trans;
class StatsPlayers extends Component {

    constructor(props) {
        super(props);
        Trans = Translations.getTranslations(this.props.lang);

        this.state = {
          ladder: this.props.ladder
        };
    }

/*
    loadLadder()
    {
        const dataUrl = Config.API_URL + '/tournament/players-stats?apikey=' + Config.API_KEY + '&user_id=' + this.props.uid + '&tid=' + this.props.tournamentId;
        axios.get(dataUrl)
                .then(res => {
                    this.setState({
                        ladder: res.data.data,
                    });

                });
    }
    
     
    componentDidMount() {
         this.loadLadder();
    }

    */
 

    render() {


        return (
                <div className="text-center screen-cnt">
                 <div className="ladder">
        

                                <table className="table table-stripped stats-table">
                                    <thead>
                                        <tr>
                                            <th>{Trans.ladderPlayerName}</th>
                                            <th>{Trans.ladderPlayerGoals}</th>
                                            <th>{Trans.ladderPlayerAssist}</th>
                                            <th>{Trans.ladderPlayerMom}</th>
                                            <th>{Trans.ladderPlayerPoints}</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                            {
                                             this.state.ladder.map((player,key) => {
                                                 return (
                                                         <tr>
                                                            <td>{player.name}</td>
                                                            <td>{player.goal}</td>
                                                            <td>{player.assist}</td>
                                                            <td>{player.mom}</td>
                                                            <td>{player.points}</td>
                                                        </tr>      
                                                )
                                            })
                                            }
                                    </tbody>
                                </table>
                                </div>

                </div>

                );
    }
}
;


export default StatsPlayers;
