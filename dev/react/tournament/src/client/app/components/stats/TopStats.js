import React, { Component } from 'react';
import axios from 'axios';
const Config = require('./../../Config.js');
import Translations from  './../../Translations.js';
var Trans;
class TopStats extends Component {

    constructor(props) {
        super(props);
        Trans = Translations.getTranslations(this.props.lang);
        
        
        var topWins = {'wins':0,'name' :'?'};
        var topGoalAverage = {'avg':0,'name' :'?'};
        
        var topGoals = {'goals':0,'name' :'?'};
        var topAssists = {'assist':0,'name' :'?'};
        
        //team
        Object.keys(this.props.ladder).map((key) => {
                    
            this.props.ladder[key].map((team,key) => {
                
                if(team.wins > topWins['wins'])
                {
                    topWins['wins'] = team.wins;
                    topWins['name'] = team.team.name;
                }
                
                var averageGoals = team.goals/team.games; 
                if(averageGoals > topGoalAverage.avg)
                {
                    topGoalAverage['avg'] = Math.round(averageGoals * 100) / 100;
                    topGoalAverage['name'] = team.team.name;
                }
                
           });                    
        });
        
        
        //players
         this.props.playersLadder.map((player,key) => {
             
              var playerGoal = parseInt(player.goal);
              var currentTopGoal = parseInt(topGoals['goals']);
              
              var playerAssist = parseInt(player.assist);
              var currentTopAssists = parseInt(topAssists['assist']);
              
            
            if(playerGoal > currentTopGoal)
                {
                    topGoals['goals'] = player.goal;
                    topGoals['name'] = player.name;
                }
              if(playerAssist > currentTopAssists)
                {
                    topAssists['assist'] = player.assist;
                    topAssists['name'] = player.name;
                }
        });
        

        this.state = {
          topWins:topWins,
          topGoalAverage:topGoalAverage,
          topGoals:topGoals,
          topAssists:topAssists,
        };
    }


    
 

    render() {


        return (
                <div className="text-center screen-cnt">
                <div className="row top-stats">
                    <div className="col-xs-6">
                        <h2>{Trans.statsTopWins}</h2>
                       
                        <span>{this.state.topWins.wins}</span>
                         <strong>{this.state.topWins.name}</strong>
                    </div>
                    <div className="col-xs-6">
                        <h2>{Trans.statsTopGoals}</h2>
                        
                        <span>{this.state.topGoals.goals}</span>
                        <strong>{this.state.topGoals.name}</strong>
                    </div>
                    <div className="col-xs-6">
                        <h2>{Trans.statsTopGoalAverage}</h2>
                       
                        <span>{this.state.topGoalAverage.avg}</span>
                         <strong>{this.state.topGoalAverage.name}</strong>
                    </div>
                    <div className="col-xs-6">
                        <h2>{Trans.statsTopAssists}</h2>
                        
                        <span>{this.state.topAssists.assist}</span>
                        <strong>{this.state.topAssists.name}</strong>
                    </div>
                </div>
        
                </div>

                );
    }
}
;


export default TopStats;
