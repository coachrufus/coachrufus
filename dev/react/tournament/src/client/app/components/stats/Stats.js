import React, { Component } from 'react';
import axios from 'axios';
const Config = require('./../../Config.js');
import Translations from  './../../Translations.js';
import Ladder from  './Ladder.js';
import StatsPlayers from  './StatsPlayers.js';
import TopStats from  './TopStats.js';
var Trans;
class Stats extends Component {

    constructor(props) {
        super(props);
        Trans = Translations.getTranslations(this.props.lang);

        this.state = {
            ladder:<Ladder ladder={this.props.ladder}   lang={this.props.lang} updateTopNav={this.handleUpdateTopNav} key={Math.random()}  tournamentId={this.props.tournamentId} uid={this.props.uid} />,
            players:<StatsPlayers lang={this.props.lang}  tournamentId={this.props.tournamentId} uid={this.props.uid}  key={Math.random()}  />,
            topstats:<TopStats ladder={this.props.ladder} lang={this.props.lang}tournamentId={this.props.tournamentId} uid={this.props.uid}  key={Math.random()}  />,
            currentScreen: this.props.currentScreen,
            playersLadder:[]
        };
        
        
        this.handleTabClick = this.handleTabClick.bind(this);
        this.handleUpdateTopNav = this.handleUpdateTopNav.bind(this);
        this.updateTopNavBacklink = this.updateTopNavBacklink.bind(this);    
    }
    
     loadPlayersLadder()
    {
        const dataUrl = Config.API_URL + '/tournament/players-stats?apikey=' + Config.API_KEY + '&user_id=' + this.props.uid + '&tid=' + this.props.tournamentId;
        axios.get(dataUrl)
                .then(res => {
                    this.setState({
                        playersLadder: res.data.data,
                        players:<StatsPlayers lang={this.props.lang} ladder={res.data.data}  tournamentId={this.props.tournamentId} uid={this.props.uid}  key={Math.random()}  />,
                        topstats:<TopStats ladder={this.props.ladder} playersLadder={res.data.data} lang={this.props.lang} tournamentId={this.props.tournamentId} uid={this.props.uid}  key={Math.random()}  />,
                    });

                });
    }
    
     componentDidMount() {
         this.loadPlayersLadder();
    }

    
    
     
    handleUpdateTopNav(title)
    {
       this.props.updateTopNav(title);
    }
    
     updateTopNavBacklink(screen)
    {
       this.props.updateTopNavBacklink(screen);
    }

  
    renderLadder()
    {

        return (
                <Ladder ladder={this.props.ladder}   lang={this.props.lang} updateTopNav={this.handleUpdateTopNav} key={Math.random()}  tournamentId={this.props.tournamentId} uid={this.props.uid} />
                    
        );
    }
    
   
     handleTabClick(e) {
        e.preventDefault();
        
         this.setState({
            currentScreen: e.target.id
        });
    }

   
    render() {
        var currentScreen;
        var ladderTabClass = 'subnavItem';
        var playersTabClass = 'subnavItem';
        var topstatsTabClass = 'subnavItem';
        if(this.state.currentScreen == 'ladder')
        {
            currentScreen = this.state.ladder;
            ladderTabClass = 'subnavItem active';
        }
        
        if(this.state.currentScreen == 'players')
        {
            currentScreen = this.state.players;
            playersTabClass = 'subnavItem active';
        }
        if(this.state.currentScreen == 'topstats')
        {
            currentScreen = this.state.topstats;
            topstatsTabClass = 'subnavItem active';
        }
        
        
        return (
                <div id="teamDetail">
                    <div className="subnav subnav3">
                        <div onClick={this.handleTabClick} id="ladder" className={ladderTabClass}>
                            {Trans.statsSubnavTeamLadder}
                        </div>
                        <div onClick={this.handleTabClick} id="players" className={playersTabClass}>
                            {Trans.statsSubnavPlayers}
                        </div>
                        <div onClick={this.handleTabClick} id="topstats" className={topstatsTabClass}>
                            {Trans.statsSubnavTopstats}
                        </div>
                    </div>
                    
                    {currentScreen}

                </div>

                );
    }
}
;

 //onClick={ (e) => {this.handleTeamDetail(team.id)} } 
export default Stats;
