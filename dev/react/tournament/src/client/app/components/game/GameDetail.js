import React, { Component } from 'react';
import axios from 'axios';
const Config = require('./../../Config.js');
import Translations from  './../../Translations.js';
var Trans;
class GameDetail extends Component {

    constructor(props) {
        super(props);
        Trans = Translations.getTranslations(this.props.lang);
        

        this.state = {
            firstLine:[],
            secondLine:[],
            game:this.props.game
        };
        
    };;
    
    

    componentDidMount() {
        this.loadDetail();
    }
    
     loadDetail()
    {
         
        
        
        const dataUrl = Config.API_URL + '/tournament/events/'+this.props.eventId+'?apikey=' + Config.API_KEY + '&user_id=' + this.props.uid;
        axios.get(dataUrl)
                .then(res => {
                    
                    //console.log( res.data.data);
                    
                    
                    this.setState({
                        firstLine: res.data.data.first_line,
                        secondLine: res.data.data.second_line,
                    });
                    

                });
                
    }
   


    render() {
        
        var safariDate =this.state.game.start.replace('-','/').replace('-','/');
        
        var eventTerminDate = new Date(safariDate);
        
        
        
        var eventTerminMinutes = (eventTerminDate.getMinutes()<10?'0':'') + eventTerminDate.getMinutes();
        var eventTerminHours = (eventTerminDate.getHours()<10?'0':'') + eventTerminDate.getHours();

        var eventTerminTitle = eventTerminDate.getDate()+'.'+eventTerminDate.getMonth()+'.'+eventTerminDate.getFullYear()+' '+eventTerminHours+':'+eventTerminMinutes;
        
        return (
                
                <div id="game-detail">
                <h2>{eventTerminTitle}</h2>
                <div className="live-detail">
                            <div className="topWrap">
                                <div className="homeTeam">
                                    
                                    <div className="teamTitle">{this.state.game.team.name}   </div>
                                    <img src={this.state.game.team.icon} />
                                </div>
                                <div className="result">
                                   vs.
                                </div>
                                <div className="opponentTeam">
                                   
                                    <div className="teamTitle">{this.state.game.opponent_team.name}    </div>
                                     <img src={this.state.game.opponent_team.icon} />
                                </div>
                            </div>
                </div>
                
                <div className="linuep-players">
                <div className="row">
                    <div className="col-xs-6">
                   
                    {
                        this.state.firstLine.map((player, key) => {
                            return (
                                   
                                    
                                    <div  className="teamPlayerRow" key={player.id}>
                                        <div  className="cell avatar"><img src={player.icon} /></div>
                                        <div  className="cell title">  {player.name}</div>
                                    </div>
                     
                                    
                                    );
                        })

                    }
                    </div>
                    <div className="col-xs-6">
                  
                    {
                        this.state.secondLine.map((player, key) => {
                            return (
                                     <div  className="teamPlayerRow" key={player.id}>
                                        <div  className="cell avatar"><img src={player.icon} /></div>
                                        <div  className="cell title">  {player.name}</div>
                                    </div>
                                    );
                        })

                    }</div>
                </div>
                </div>
                </div>
                );
    }
}
;


export default GameDetail;
