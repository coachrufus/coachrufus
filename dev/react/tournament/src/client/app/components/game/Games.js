import React, { Component } from 'react';
import axios from 'axios';
const Config = require('./../../Config.js');
import Translations from  './../../Translations.js';
import GamesList from  './GamesList.js';
import Playoff from  './Playoff.js';
var Trans;
class Games extends Component {

    constructor(props) {
        super(props);
        Trans = Translations.getTranslations(this.props.lang);
        
        var finishedGames = [];
        var waitingGames = [];
        var playoffGames = [];

        Object.keys(this.props.games).map((key) => {
            this.props.games[key].map((game,gkey) => {
              
                if(game.tournament_type == 'playoff' || game.tournament_type == 'playoff-bronze' || game.tournament_type == 'playoff-final'  )
                {
                        
                        playoffGames[key] = [];
                        playoffGames[key][gkey] = game;
                }
                
                if(game.lineup.status == 'closed')
                {
                     finishedGames[key] = [];
                     finishedGames[key][gkey] = game;
                }
                else
                {
                     waitingGames[key] = [];
                     waitingGames[key][gkey] = game;
                }
           });
        });
        
       
        
        this.state = {
            finishedGames:finishedGames,
            waitingGames: waitingGames,
            playoffGames: playoffGames,
            currentScreen: 'waiting'
        };

         this.handleUpdateTopNav = this.handleUpdateTopNav.bind(this);
        this.updateTopNavBacklink = this.updateTopNavBacklink.bind(this);
        this.handleTabClick = this.handleTabClick.bind(this);
    }
    
    handleUpdateTopNav(title)
    {
       this.props.updateTopNav(title);
    }
    
     updateTopNavBacklink(screen)
    {
       this.props.updateTopNavBacklink(screen);
    }

    handleTabClick(e) {
        e.preventDefault();
        
        if(e.target.id == 'finished')
        {
            this.setState({
               currentScreen: 'finished'
           });
        }
        
        if(e.target.id == 'waiting')
        {
            this.setState({
               currentScreen: 'waiting'
           });
        }
        if(e.target.id == 'playoff')
        {
            this.setState({
               currentScreen: 'playoff'
           });
        }
        
    }

    render() {
        var currentScreen;
        var waitingTabClass = 'subnavItem';
        var finishedTabClass = 'subnavItem';
        var playoffTabClass = 'subnavItem';
        if(this.state.currentScreen == 'finished')
        {
            finishedTabClass = 'subnavItem active';
         currentScreen = <GamesList  uid={this.props.uid}key={Math.random()} lang={this.props.lang} games={this.state.finishedGames} currentScreen="list" updateTopNav={this.handleUpdateTopNav} updateTopNavBacklink={this.updateTopNavBacklink} />;
        }
        
        if(this.state.currentScreen == 'waiting')
        {
            waitingTabClass = 'subnavItem active';
            currentScreen = <GamesList tournament={this.props.tournament} uid={this.props.uid}  key={Math.random()} lang={this.props.lang} games={this.state.waitingGames} currentScreen="list" updateTopNav={this.handleUpdateTopNav} updateTopNavBacklink={this.updateTopNavBacklink} />;
            
        }
        
        if(this.state.currentScreen == 'playoff')
        {
            playoffTabClass = 'subnavItem active';
            currentScreen = <Playoff games={this.state.playoffGames} />;
        }
        
        var subnavClass = 'subnav';
      
       if(Object.keys(this.state.playoffGames).length > 1)
       {
           subnavClass = subnavClass+' playoffActive';
       }

        return (
                <div>
                 <div className={subnavClass}>
                        <div onClick={this.handleTabClick} id="waiting" className={waitingTabClass}>
                            {Trans.gameSubnavWaiting}
                        </div>
                        <div onClick={this.handleTabClick} id="finished" className={finishedTabClass}>
                            {Trans.gameSubnavFinished}
                        </div>
                        <div onClick={this.handleTabClick} id="playoff" className={playoffTabClass}>
                            {Trans.gameSubnavPlayoff}
                        </div>
                    </div>
                {currentScreen}
                </div>
                )
    }
}
;

//onClick={ (e) => {this.handleTeamDetail(team.id)} } 
export default Games;
