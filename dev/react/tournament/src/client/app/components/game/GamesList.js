import React, { Component } from 'react';
import axios from 'axios';
const Config = require('./../../Config.js');
import GameDetail from  './GameDetail.js';
import LiveDetail from  './../../components/LiveDetail.js';
import Translations from  './../../Translations.js';
var Trans;
class GamesList extends Component {

    constructor(props) {
        super(props);
        Trans = Translations.getTranslations(this.props.lang);
        this.state = {
            games:this.props.games,
            currentView: this.props.currentScreen,
            currentGameId: null,
            currentGame: null,
            subscribeAlertVisible:false,
            subscribeAlertText:'',
            downloadAppAlertVisible: false
        };
        
         this.handleGameDetail = this.handleGameDetail.bind(this);
         this.handleGameNotifySubscribe = this.handleGameNotifySubscribe.bind(this);
         this.renderGames = this.renderGames.bind(this);
         this.renderGameDetail = this.renderGameDetail.bind(this);
         this.handleCloseDownloadAppAlert = this.handleCloseDownloadAppAlert.bind(this);
       
    }
    
    
    
    handleGameNotifySubscribe(lineupId,type)
    {
        var params = new URLSearchParams();
        params.append('user_id', this.props.uid);
        params.append('lineup_id', lineupId);
        params.append('action', type);

        if(this.props.uid == '')
        {
              
            this.setState({
                downloadAppAlertVisible:true
            });
        }
        else
        {
            
        }

        const dataUrl = Config.API_URL + '/tournament/events-subscribe?apikey=' + Config.API_KEY + '&user_id=' + this.props.uid;
        axios.post(dataUrl,params)
                .then(res => {
                        var updatedGames = this.state.games;
                        var subscribeAlertText = '';
                          Object.keys(updatedGames).map((key) => {
                              updatedGames[key].map((game,key) => {
                                  if(game.lineup.id == lineupId)
                                  {
                                    if(type == 'subscribe')
                                    {
                                         game.user_subscribed = 'yes';
                                         subscribeAlertText = Trans.subscribeAlertText;
                                    }
                                    if(type == 'unsubscribe')
                                    {
                                         game.user_subscribed = 'no';
                                         subscribeAlertText = Trans.unsubscribeAlertText;
                                    }
                                  }
                              });
                             
                          });
                          
                         this.setState({
                            games: updatedGames,
                            subscribeAlertVisible:true,
                            subscribeAlertText:subscribeAlertText
                            
                        });

                     setTimeout(() => {
                        this.setState({ subscribeAlertVisible: false });
                      }, 1000);
                      
                });
                
                
    }
    
    handleCloseDownloadAppAlert(){
        this.setState({
            downloadAppAlertVisible:false
        });
    }
    
     renderGames()
    {
        
        return (
                <div className="games">
                    {this.state.downloadAppAlertVisible ?
                                <div className="downloadAppAlert">
                                  <div className="downloadAppAlertCnt">
                                
                                <span className="closeDownloadAppAlert"  onClick={this.handleCloseDownloadAppAlert}>x</span>
                                <h2> {Trans.downloadAppTitle}</h2>
                                      {Trans.downloadAppText}
                                    <div className="row">
                                        <div className="col-xs-6 text-center">
                                            <a  href="https://play.google.com/store/apps/details?id=com.altamira.rufus">
                                                <img className="mobile_app_promo_lg" src="/theme/img/rufus_badge_sk_googleplay_sk.svg" alt="" title="" />

                                            </a>

                                        </div>
                                        <div className="col-xs-6 text-center">
                                            <a  href="https://itunes.apple.com/us/app/rufus-your-favourite-teammate/id1435067809">
                                                <img className="mobile_app_promo_lg" src="/theme/img/rufus_badge_sk_ios_sk.svg" alt="" title="" />

                                            </a>
                                        </div>

                                    </div>
                                </div>
                                </div>
                                 : ''
                                }
                        
        
        {
       
                    Object.keys(this.state.games).map((key) => {
                        
                        var keyParts = key.split('-');
                        var eventDate =keyParts[0];
                        
                        return (
                                <div key={key} className="dateEvents">
                        
                                {this.state.subscribeAlertVisible ?
                                 <div className="subscribeAlert">{this.state.subscribeAlertText}</div>
                                 : ''
                                }
                               
                               
                        
                        
                                <div className="date">{eventDate}</div>
                                {
                                 this.state.games[key].map((game,key) => {
                                         var className = 'gameRow';
                                         var imgNotify = '/theme/img/icon/bell.svg';
                                         var subscribeType = 'subscribe';
                                            if(game.user_subscribed == 'yes')
                                            {
                                                imgNotify = '/theme/img/icon/bell-green.svg';
                                                subscribeType = 'unsubscribe';
                                            }
                                            var matchTimeMinutes = '';
                                            if(game.lineup.status == 'running' || game.lineup.status == 'paused')
                                            {
                                                className = 'gameRow gameLiveRow';
                                                matchTimeMinutes = game.lineup.match_time.minutes;
                                            };

                                            if(game.team == null)
                                            {
                                                game.team = [];
                                                game.team.icon = '/img/team/users.svg';
                                                game.team.name = game.lineup.first_line_name;
                                            }
                                            
                                            if(game.opponent_team == null)
                                            {
                                                game.opponent_team = [];
                                                game.opponent_team.icon = '/img/team/users.svg';
                                                game.opponent_team.name = game.lineup.second_line_name;
  
                                            }
                                            
                                        return (
                                                <div    className={className} key={game.id}>
                                                    <div onClick={(e) => {this.handleGameNotifySubscribe(game.lineup.id,subscribeType)}} className="cell game_notify"><img src={imgNotify} /></div>
                                                    <div onClick={(e) => {this.handleGameDetail(game.id)}} className="cell game_teams">
                                                        <div className="game_team_row">
                                                            <img src={game.team.icon} />
                                                            {game.team.name}   
                                                             <div className="game_team_row_points">{game.lineup.match_result.first_line}</div>
                                                        </div>
                                                        <div className="game_team_row">
                                                            <img src={game.opponent_team.icon} />
                                                            {game.opponent_team.name}   
                                                            <div className="game_team_row_points">{game.lineup.match_result.second_line}</div>       
                                                        </div>
                                                    </div>
                                                     <div className="time">
                                                        <i className="ico ico-clock"></i>
                                                        <span>{matchTimeMinutes}'</span>   
                                                    </div>
                                                    
                                                   

                                                </div>
                            
                            
                                        )
                                        
                                    
                                        
                                    })
                                }
                                </div>
                            
                    
                           
                            
                        )
                               
                    })
                        
                }</div>       
                        
        );
    }
    
    renderGameDetail()
    {
        if(this.state.currentGame.lineup.status == 'closed')
        {
            return (
                    
                    <LiveDetail game={this.state.currentGame}  eventId={this.state.currentGameId} />
            );
        }
        else
        {
             return (
                    <GameDetail game={this.state.currentGame}  eventId={this.state.currentGameId} />
            );
        }
                        
                        
                       
    }
    
     handleGameDetail(id) {
         
         var currentGame;
         
          Object.keys(this.state.games).map((key) => {
            this.state.games[key].map((game, key) => {
                 if(game.id == id)
                {
                    currentGame = game;
                }
            });
        });


        this.props.updateTopNav(Trans.gameDetailSubnavTitle); 
        this.props.updateTopNavBacklink('games'); 
        
   
        this.setState({
            currentView: 'detail',
            currentGameId:id,
            currentGame:currentGame
        });
    }
    
   

    render() {
        var screen;

        if (this.state.currentView == 'list')
        {
            screen = this.renderGames();

        }
        if (this.state.currentView == 'detail')
        {
            screen = this.renderGameDetail();

        }
        return (screen);

    }
}
;


export default GamesList;
