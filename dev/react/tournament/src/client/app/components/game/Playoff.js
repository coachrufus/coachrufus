import React, { Component } from 'react';
import axios from 'axios';
const Config = require('./../../Config.js');
import Translations from  './../../Translations.js';
var Trans;
class Playoff extends Component {

    constructor(props) {
        super(props);
        Trans = Translations.getTranslations(this.props.lang);
        
        var roundedGames = [];
        
         //console.log(this.props.games);
        
         Object.keys(this.props.games).map((key)  => {

              var game = this.props.games[key][0];
              var index = game.tournament_round-1;
              roundedGames[index] = {'name':game.tournament_group,'events':[]};
         });
         
         //console.log(roundedGames);

         Object.keys(this.props.games).map((key)  => {
             
            var game = this.props.games[key][0];
             var index = game.tournament_round-1;
             roundedGames[index].events.push(game);
             
         });
         


        this.state = {
            games:roundedGames,
            activeRound: 1,
            activeMatch: 0,
            activeMatch2: 0,
        };

        this.handleUpdateTopNav = this.handleUpdateTopNav.bind(this);
        this.updateTopNavBacklink = this.updateTopNavBacklink.bind(this);
        this.handleMove = this.handleMove.bind(this);
    }
    
    handleUpdateTopNav(title)
    {
       this.props.updateTopNav(title);
    }
    
     updateTopNavBacklink(screen)
    {
       this.props.updateTopNavBacklink(screen);
    }
    
    
    handleMove(round,target,target2)
    {
         this.setState({
              activeRound: round,
              activeMatch:target,
              activeMatch2:target2
         });
      
    }

  

    render() {
        var roundNavClassName = 'roundNav roundNav'+Object.keys(this.state.games).length;
        return (
                <div className="playoff">
                <div className={roundNavClassName}>
                 {  Object.keys(this.state.games).map((key)  => {
                     var round = parseInt(key)+1;
                     var nextRound = round;
                     var className = 'roundNavItem';
                     if(round == this.state.activeRound)
                    {
                         className = className+' roundNavItemActive';
                    }
                     
                    return (
                             <div key={'roundNavItem'+key} onClick={(e) => {this.handleMove(nextRound,null,null)}} className={className}>{this.state.games[key]['name']}</div>         
                    )
                     
                })}
                </div>
        
        
        
                {  Object.keys(this.state.games).map((key)  => {
                    //var game = this.state.games[key];
                    var round = parseInt(key)+1;
                    var nextRound = round+1;
                    var previewRound = round-1;
                    var mainClassName = 'pl-round pl-round-'+round;
                    if(this.state.activeRound == round)
                    {
                        mainClassName = mainClassName+' active';
                    }
                    
                    if(round == Object.keys(this.state.games).length)
                    {
                         mainClassName = mainClassName+' pl-round-last';
                    }
                   
                    
                    
                    
                    var events = this.state.games[key]['events'];
                    var divider = 0;
                    
                            
                    return (
                    <div  key={key} className={mainClassName} >
                        {
                            
            
                            events.map((game,key) => {
                                divider++;
                                var z = divider % 2;
                                var dividerDiv = '';
                                var cellClass = 'pl-cell';
                                if(z == 0)
                                {
                                    cellClass = cellClass+' pl-cell-4';
                                }
                                if(this.state.activeMatch == game.id)
                                {
                                    cellClass = cellClass+' active';
                                } 
                                
                                 if(this.state.activeMatch2 == game.id)
                                {
                                    cellClass = cellClass+' active';
                                } 
                                
                                
                                var gameTitle = '';
                                if('playoff-bronze' == game.tournament_type)
                                {
                                    gameTitle = <div className="pl-cell-title">o 3 miesto</div>;
                                    cellClass = cellClass+' pl-cell-bronze';
                                }
                                
                                if('playoff-final' == game.tournament_type)
                                {
                                    gameTitle = <div className="pl-cell-title">Finále</div>;
                                    cellClass = cellClass+' pl-cell-final';
                                }
                              
                                 return (

                                        <div key={'game_'+key} className={cellClass}>
                                            {gameTitle}
                                            <div className="pl-cell-cnt">
                                                <div className="pl-conector-left"></div>
                                                <div className="pl-cell-cnt1">
                                                    {game.lineup.first_line_name}
                                                </div>
                                                <div className="pl-spacer"></div>
                                                <div className="pl-cell-cnt2">
                                                     {game.lineup.second_line_name}
                                                </div>
                                                <div className="pl-conector-right"></div>
                                            </div>
                                            <div className="pl-cell-results">
                                                <div className="pl-cell-cnt1">
                                                    {game.lineup.match_result.first_line}
                                                </div>
                                                <div className="pl-cell-cnt2">
                                                     {game.lineup.match_result.second_line}
                                                </div>
                                            </div>
                                            <div className="moveTrigger" onClick={(e) => {this.handleMove(nextRound,game.tournament_target_event,null)}}></div>
                                            <div className="moveBackTrigger" onClick={(e) => {this.handleMove(previewRound,game.tournament_playoff_parent_event1,game.tournament_playoff_parent_event2)}}></div>
                                            <div className="clear"></div>
                                         </div>
                                )
                                
                                
                                
                                
                                
                               
                            })
                        }
                        
                    </div>
                    )
                }) }
               
                </div>
                )
    }
}
;

//onClick={ (e) => {this.handleTeamDetail(team.id)} } 
export default Playoff;
 