import React, { Component } from 'react';
import axios from 'axios';
const Config = require('./../Config.js');
import Translations from  './../Translations.js';
import Timeline from  './../components/Timeline.js';
var Trans;
class LiveDetail extends Component {

    constructor(props) {
        super(props);
        Trans = Translations.getTranslations(this.props.lang);
        

        this.state = {
            match_result:{},
            game:this.props.game,
            timelineLoaded:false,
            timeline:null
        };
        
        this.loadDetail = this.loadDetail.bind(this);
        
        this.timer = setInterval(this.loadDetail, 5000);
    };;
    
    

    componentDidMount() {
        this.loadDetail();
         //setInterval(this.loadDetail, 5000);
    }
    
    componentWillUnmount() {
       clearInterval(this.timer);
    }
    
     loadDetail()
    {
        
    
        var eventDate = this.props.game.lineup.event_date.replace(' 00:00:00','');
        const dataUrl = Config.API_URL + '/content/game/timeline?apikey=' + Config.API_KEY + '&user_id=' + this.props.uid+'&event_id='+this.props.eventId+'&event_current_date='+eventDate+'&lid='+this.props.game.lineup.id;
        axios.get(dataUrl)
                .then(res => {
                    
                    this.setState({
                        match_result: res.data.match_result,
                        timelineLoaded:true,
                        timeline: <Timeline key={Math.random()} lang={this.props.lang} eventId={this.props.eventId} eventCurrentDate={eventDate} timelineItems={res.data.timeline_items}
                        />
                    });
                    

                });
                
    }
   


    render() {
        var game = this.state.game;
        var eventDate = this.props.game.lineup.event_date.replace(' 00:00:00','');
        
        return (
                <div className="live-detail">
                    <div className="topWrap">
                        <div className="homeTeam">
                            <img src={game.team.icon} />
                            {game.team.name}   
                        </div>
                        <div className="result">
                            {this.state.match_result.first_line}:{this.state.match_result.second_line}
                        </div>
                        <div className="opponentTeam">
                            <img src={game.opponent_team.icon} />
                            {game.opponent_team.name}   
                        </div>
                    </div>
                    
                    {this.state.timeline}
                    
                  
                </div>

                );
    }
}
;


export default LiveDetail;
