import React, { Component } from 'react';

class Translations
{
    constructor()
    {
        this.lang = 'sk';
        this.translations = {
             mainNavHome: {
                sk: 'Domov',
                en: 'Home'
            },
            
             mainNavTeams: {
                sk: 'Tímy',
                en: 'Teams'
            },
             mainNavGames: {
                sk: 'Zápasy',
                en: 'Games'
            },
             mainNavStats: {
                sk: 'Štatistiky',
                en: 'Stats'
            },
            
             mainNavLive: {
                sk: 'Live',
                en: 'Live'
            },
            
            tournamentDetail: {
                sk: 'Detail turnaja',
                en: 'Tournament info'
            },
            topNavTitleTeams: {
                sk: 'Tímy',
                en: 'Teams'
            },
            topNavTitleGames: {
                sk: 'Zápasy',
                en: 'Games'
            },
            topNavTitlePlayoff: {
                sk: 'Pavúk',
                en: 'Playoff'
            },
            topNavTitleLive: {
                sk: 'Priamy prenos',
                en: 'Live'
            },
            
            topNavLiveDetail: {
                sk: 'Priamy prenos',
                en: 'Live'
            },
            teamDetailSubnavPlayers: {
                sk: 'Hráči',
                en: 'Players'
            },
            teamDetailSubnavEvents: {
                sk: 'Zápasy',
                en: 'Games'
            },
            gameDetailSubnavTitle: {
                sk: 'Detail zápasu',
                en: 'Game detail'
            },
            ladderTableTeamTitle: {
                sk: 'Tím',
                en: 'Team'
            },
            ladderTablePoints: {
                sk: 'B',
                en: 'P'
            },
            ladderTableGoals: {
                sk: 'Góly',
                en: 'Goals'
            },
            
            ladderGroup: {
                sk: 'Skupina',
                en: 'Group'
            },
            
            ladderTableWins: {
                sk: 'V',
                en: 'W'
            },
            ladderTableWinsOvertime: {
                sk: 'Vpp',
                en: 'Woo'
            },
            ladderTableLosses: {
                sk: 'P',
                en: 'L'
            },
            ladderTableLossesOvertime: {
                sk: 'Ppp',
                en: 'Loo'
            },
            
            ladderTableDraws: {
                sk: 'R',
                en: 'D'
            },
            ladderTableGames: {
                sk: 'Z',
                en: 'G'
            },
            ladderTableScore: {
                sk: 'S',
                en: 'S'
            },
            
            gameSubnavWaiting: {
                sk: 'Neodohraté',
                en: 'Future'
            },
            gameSubnavFinished: {
                sk: 'Odohraté',
                en: 'Finished'
            },
            gameSubnavPlayoff: {
                sk: 'Playoff',
                en: 'Playoff'
            },
            subscribeAlertText: {
                sk: 'Boli ste prihlásený na odber notikácii k zápasu',
                en: 'You have been subscribed to receive game notifications'
            },
            unsubscribeAlertText: {
                sk: 'Boli ste odhláseny na odber notikácii k zápasu',
                en: 'You have been unsubscribed to receive game notifications'
            },
            statsSubnavTeamLadder: {
                sk: 'Tabuľka',
                en: 'Table'
            },
            statsSubnavPlayers: {
                sk: 'Hráči',
                en: 'Players'
            },
            statsSubnavTopstats: {
                sk: 'Top',
                en: 'Top'
            },
            topNavTitleStats: {
                sk: 'Štatistiky',
                en: 'Stats'
            },
            downloadAppTitle: {
                sk: 'Stiahni si apku',
                en: 'Download app'
            },
            downloadAppText: {
                sk: 'Stiahni si apku a dostávaj info o vyvoji zápasu live',
                en: 'Download app and get live info'
            },
            
            joinTournament: {
                sk: 'Pripoj sa k turnaju',
                en: 'Join tournament'
            },
            detail_start: {
                sk: 'Začiatok turnaja',
                en: 'Tournament start'
            },
            detail_end: {
                sk: 'Koniec turnaja',
                en: 'Tournament end'
            },
            detail_gender: {
                sk: 'Pohlavie',
                en: 'Gender'
            },
            detail_sport: {
                sk: 'Šport',
                en: 'Sport'
            },
            detail_level: {
                sk: 'Úroveň',
                en: 'Skill'
            },
            detail_age: {
                sk: 'Vek',
                en: 'Age'
            },
            detail_email: {
                sk: 'Email',
                en: 'Email'
            },
            detail_phone: {
                sk: 'Telefón',
                en: 'Phone'
            },
            detail_address: {
                sk: 'Adresa',
                en: 'Address'
            },
            
            
            ladderPlayerName: {
                sk: 'Hráč',
                en: 'Player'
            },
            ladderPlayerGoals: {
                sk: 'Góly',
                en: 'Goals'
            },
            ladderPlayerAssist: {
                sk: 'Asistencie',
                en: 'Assist'
            },
            ladderPlayerMom: {
                sk: 'MoM',
                en: 'MoM'
            },
            ladderPlayerPoints: {
                sk: 'Bodov',
                en: 'Points'
            },
            statsTopWins: {
                sk: 'Najviac vyhratých zápasov',
                en: 'Most wins'
            },
            statsTopGoals: {
                sk: 'Najlepší strelec',
                en: 'Best shooter'
            },
            statsTopGoalAverage: {
                sk: 'Najviac gólov na zápas',
                en: 'Best goal averate'
            },
            statsTopAssists: {
                sk: 'Asistencie',
                en: 'Assists'
            },
           
            

        }
    }
    
   getTranslations(lang) {
        var keys = Object.keys(this.translations);
        var allTrans = this.translations;
        var translations = {};
        //var lang = lang;
        keys.forEach(function (k) {
            translations[k] = allTrans[k][lang];
        });

        return translations;
}
}

export  default Translations = new Translations();

