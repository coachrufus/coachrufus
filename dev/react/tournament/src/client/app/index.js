import React from "react";
import ReactDOM from "react-dom";
import App from "./App.js";

var targetElement = document.getElementById('tournament');
var tournamentId = targetElement.getAttribute("data-id");
var lang = targetElement.getAttribute("data-lang");
var uid = targetElement.getAttribute("data-uid");
var screen = targetElement.getAttribute("data-screen");
var matchId = targetElement.getAttribute("data-matchId");
ReactDOM.render(
  <App tournamentId={tournamentId} lang={lang} uid={uid} screen={screen} matchId={matchId}  />,
  document.getElementById("tournament")
);
