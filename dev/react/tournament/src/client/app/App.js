import React, { Component} from "react";
import axios from 'axios';
import Home from  './components/Home.js';
import Teams from  './components/team/Teams.js';
import Games from  './components/game/Games.js';
import Stats from  './components/stats/Stats.js';
import Live from  './components/Live.js';
const Config = require('./Config.js');
import Translations from  './Translations.js';
var Trans;

class App extends Component {

    constructor(props) {
        super(props);
        Trans = Translations.getTranslations(this.props.lang);
        this.state = {
            games:{},
            teams:{},
            ladder: {},
            tournament: {},
            currentScreen: this.props.screen,
            topNavTitle: Trans.tournamentDetail,
            topNavBacklinkScreen: 'close',
            cnt: <div className="loader"><img src="/img/load.gif" /><br />Loading</div>
        };

        this.handleMenuClick = this.handleMenuClick.bind(this);
        this.handleBackButton = this.handleBackButton.bind(this);
        this.handleUpdateTopNav = this.handleUpdateTopNav.bind(this);
        this.updateTopNavBacklink = this.updateTopNavBacklink.bind(this);
        this.showScreen = this.showScreen.bind(this);
    }
    
     loadDetail()
    {
        const dataUrl = Config.API_URL + '/tournament/detail?apikey=' + Config.API_KEY + '&user_id=' + this.props.uid+'&tid='+this.props.tournamentId;
        axios.get(dataUrl)
                .then(res => {
                    this.setState({
                        tournament: res.data.data,
                    });
                     this.showScreen(this.state.currentScreen);
                });
               
    }
    
     loadTeams()
    {
        const dataUrl = Config.API_URL + '/tournament/teams?apikey=' + Config.API_KEY + '&user_id=' + this.props.uid + '&tid=' + this.props.tournamentId;
        axios.get(dataUrl)
                .then(res => {
                    this.setState({
                        teams: res.data.data,
                    });

                });
    }
    
     loadLadder()
    {
        const dataUrl = Config.API_URL + '/tournament/stats?apikey=' + Config.API_KEY + '&user_id=' + this.props.uid + '&tid=' + this.props.tournamentId;
        axios.get(dataUrl)
                .then(res => {
                    this.setState({
                        ladder: res.data.data,
                    });

                });
    }
    
    loadEvents()
    {
        const dataUrl = Config.API_URL + '/tournament/events?apikey=' + Config.API_KEY + '&user_id=' + this.props.uid + '&tid=' + this.props.tournamentId;
        axios.get(dataUrl)
                .then(res => {
                    
                     var dayGames = [];
                        res.data.data.map((game, key) => {

                        var safarDate =game.start.replace('-','/').replace('-','/');
                        
                        var eventTerminDate = new Date(safarDate);
                        var eventTerminMinutes = (eventTerminDate.getMinutes()<10?'0':'') + eventTerminDate.getMinutes();
                        var eventTerminHours = (eventTerminDate.getHours()<10?'0':'') + eventTerminDate.getHours();

                        var eventTerminIndex = eventTerminDate.getDate()+'.'+(eventTerminDate.getMonth()+1)+'.'+eventTerminDate.getFullYear()+' '+eventTerminHours+':'+eventTerminMinutes+'-'+game.id;
                        var keyExist = eventTerminIndex in dayGames;

                        if(!keyExist)
                        {
                            dayGames[eventTerminIndex] = [];
                        }
                        dayGames[eventTerminIndex].push(game);
                    });
                    
                    
                    
                    this.setState({
                        games: dayGames,
                    });

                });
    }
    
    componentDidMount() {
         this.loadEvents();
         this.loadTeams();
         this.loadLadder();
         this.loadDetail();
         //this.showScreen(this.state.currentScreen);
    }

    
    showScreen(screen)
    {
        var cnt;
        var title;
        var currentScreen;
        var topNavBacklinkScreen = 'close';
        if (screen == 'home')
        {
            cnt = <Home key={Math.random()}  tournament={this.state.tournament} lang={this.props.lang} tournamentId={this.props.tournamentId} uid={this.props.uid} />;
            title = Trans.tournamentDetail;
            currentScreen = 'home';
        }
        if (screen == 'teams')
        {
            cnt = <Teams teams={this.state.teams} games={this.state.games}   lang={this.props.lang} updateTopNav={this.handleUpdateTopNav} updateTopNavBacklink={this.updateTopNavBacklink} key={Math.random()} currentScreen="list" tournamentId={this.props.tournamentId} uid={this.props.uid} />;
            title = Trans.topNavTitleTeams;
            currentScreen = 'teams';
            topNavBacklinkScreen = 'home';
        }

        if (screen == 'games')
        {
            cnt = <Games  tournament={this.state.tournament} games={this.state.games}  lang={this.props.lang} updateTopNav={this.handleUpdateTopNav} updateTopNavBacklink={this.updateTopNavBacklink} key={Math.random()} currentScreen="list" tournamentId={this.props.tournamentId} uid={this.props.uid} />;
            title = Trans.topNavTitleGames;
            currentScreen = 'games';
            topNavBacklinkScreen = 'home';
        }
        if (screen == 'ladder')
        {
            cnt = <Stats ladder={this.state.ladder}   lang={this.props.lang} updateTopNav={this.handleUpdateTopNav} key={Math.random()} currentScreen="ladder" tournamentId={this.props.tournamentId} uid={this.props.uid} />;
            title = Trans.topNavTitleStats;
            currentScreen = 'ladder';
            topNavBacklinkScreen = 'home';
        }
        if (screen == 'live')
        {
            cnt = <Live  lang={this.props.lang} updateTopNav={this.handleUpdateTopNav} updateTopNavBacklink={this.updateTopNavBacklink} key={Math.random()} currentScreen="list"  tournamentId={this.props.tournamentId} uid={this.props.uid}  games={this.state.games}  />;
            title = Trans.topNavTitleLive;
            currentScreen = 'live';
            topNavBacklinkScreen = 'home';
        }
        
         if (screen == 'live-detail')
        {
            cnt = <Live  lang={this.props.lang} updateTopNav={this.handleUpdateTopNav} updateTopNavBacklink={this.updateTopNavBacklink} key={Math.random()} currentScreen="detail" gameDetailId={this.props.matchId} tournamentId={this.props.tournamentId} uid={this.props.uid}  games={this.state.games} />;
            title = Trans.topNavTitleLive;
            currentScreen = 'live';
            topNavBacklinkScreen = 'home';
        }
        
        


        this.setState({
            cnt: cnt,
            topNavTitle: title,
            currentScreen: currentScreen,   
            topNavBacklinkScreen: topNavBacklinkScreen   
        });
    }

    handleMenuClick(e) {
        e.preventDefault();
        
        this.showScreen(e.target.id);
    }

    updateTopNavBacklink(screen)
    {
        this.setState({ 
            topNavBacklinkScreen: screen   
        });
    }

    handleBackButton(e) {
        if(this.state.topNavBacklinkScreen == 'close' || typeof this.state.topNavBacklinkScreen === "undefined")
        {
            var message = {
                    'action': 'close',
                     'screen': 'home'
                 };
             window.postMessage(JSON.stringify(message), '*');

        }
        else
        {
             this.showScreen(this.state.topNavBacklinkScreen);
        }
        
       
    }

    handleUpdateTopNav(title)
    {
        this.setState({
            topNavTitle: title
        });

    }

    render() {
        /*var classNameHome = 'bottomMenuItem ';
        var classNameTeams = 'bottomMenuItem ';
        var classNameGames = 'bottomMenuItem ';
        var classNameLadder = 'bottomMenuItem ';
        var classNameLive = 'bottomMenuItem ';*/
        //if(this.state.currentScreen)
        var classNameBottomMenu = 'bottomMenu '+ this.state.currentScreen+'-active';
        return(
                <div className="container-fluid">
                    <div className="nav-topbar">
                        <div onClick={this.handleBackButton} className="navBtn"></div>
                        {this.state.topNavTitle}
                    </div>
                
                    <div className="mainCnt">
                        {this.state.cnt}
                    </div>
                
                    <div className={classNameBottomMenu}>
                        <div className="bottomMenuItem" onClick={this.handleMenuClick} id="home">
                            {Trans.mainNavHome}
                        </div>
                        <div  className="bottomMenuItem" onClick={this.handleMenuClick} id="teams">
                            {Trans.mainNavTeams}
                        </div>
                        <div   className="bottomMenuItem" onClick={this.handleMenuClick} id="games">
                           {Trans.mainNavGames}
                        </div>
                        <div   className="bottomMenuItem" onClick={this.handleMenuClick} id="ladder">
                            {Trans.mainNavStats}
                        </div>
                        <div   className="bottomMenuItem" onClick={this.handleMenuClick} id="live">
                            {Trans.mainNavLive}
                        </div>
                    </div>
                
                </div>
                );
    }
}
;

export default App;