<html>
  <head>
    <meta charset="utf-8">
    <title>React.js using NPM, Babel6 and Webpack</title>
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,700|Roboto:400,700&amp;subset=cyrillic" rel="stylesheet">
        <link media="all" rel="stylesheet" href="/dev/theme/bootstrap/css/bootstrap.min.css" />
         <link media="all" rel="stylesheet" href="public/styles/index.css?ver=<?php echo time() ?>" />
  </head>
  <body class="platform_iphone local">
      <div id="tournament" data-lang="sk"  data-id="1441" data-uid="3" data-screen="home" data-matchId=""></div>
    <script src="public/bundle.js?ver=<?php echo rand(1,100) ?>" type="text/javascript"></script>
  </body>
</html>