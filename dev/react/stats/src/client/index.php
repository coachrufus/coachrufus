<html>
  <head>
    <meta charset="utf-8">
    <title>React.js using NPM, Babel6 and Webpack</title>
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
     <link media="all" rel="stylesheet" href="public/styles/index.css?ver=<?php  echo time()?>" />
  </head>
  <body>
      <div id="stat_widget" data-t="3" data-s="" data-h="" data-api-provider="http://app.coachrufus.lamp"></div>
    <script src="public/bundle.js?ver=<?php echo rand(1,100) ?>" type="text/javascript"></script>
  </body>
</html>