import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { Provider } from 'react-redux';
import store from './store/index.js';

var targetElement = document.getElementById('stat_widget');
var t = targetElement.getAttribute("data-t");
var s = targetElement.getAttribute("data-s");
var h = targetElement.getAttribute("data-h");
var provider = targetElement.getAttribute("data-api-provider");

ReactDOM.render(<Provider store={store}><App t={t} s={s} h={h} provider={provider} /></Provider>, document.getElementById('stat_widget'));