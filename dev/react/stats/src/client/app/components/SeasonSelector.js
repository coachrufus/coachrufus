import React, { Component } from 'react';

class Selector extends Component {


    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
        this.handleOutsideClick = this.handleOutsideClick.bind(this);
        this.handlePointChange = this.handlePointChange.bind(this);

        this.state = {
            showSelections: false,
            currentSelectionText: this.props.options[0].name,
            options: this.props.options
        };



    }

    handleClick(e) {
        if (!this.state.showSelections) {
            // attach/remove event handler
            document.addEventListener('click', this.handleOutsideClick, false);
        } else {
            document.removeEventListener('click', this.handleOutsideClick, false);
        }

        e.preventDefault();
        if (this.state.showSelections == true) {
            this.setState({
                showSelections: false
            });
        }
        else {
            this.setState({
                showSelections: true
            });
        }
    }

    handleOutsideClick(e) {
        // ignore clicks on the component itself
        if (this.node.contains(e.target)) {
            return;
        }

        this.setState({
            showSelections: false
        });
    }

    showSelections = (e) => {
        e.preventDefault();
        if (this.state.showSelections == true) {
            this.setState({
                showSelections: false
            });
        }
        else {
            this.setState({
                showSelections: true
            });
        }

    }
    collapseSelections = (e) => {
        e.preventDefault();
        this.setState({
            showSelections: false
        });

    }

    handlePointChange = (e, type, selectionText) => {
        e.preventDefault();
        this.props.onItemSelect(e, type);

        this.setState({
            showSelections: false,
            currentSelectionText: selectionText
        });


    };



    render() {
        return (
            <div className="filter-cell filter-point-type selector" ref={(node) => { this.node = node; }}>
                <span onClick={(e) => { this.handleClick(e) }} className="current_selection">{this.state.currentSelectionText}</span>
                <div className={"selections " + (this.state.showSelections == true ? 'active' : '')}>

                    {this.state.options.map((option, key) => {
                        return (
                            <a key={key} href="#" onClick={(e) => this.handlePointChange(e, option.value, option.name)} className="point-trigger">{option.name}</a>


                        )}
                    )}



                </div>
            </div>
        );
    }
}
;


export default Selector;
