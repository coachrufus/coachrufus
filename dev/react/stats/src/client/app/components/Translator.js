class Translator
{
    constructor(lang)
    {
        this.lang = lang;
        this.translations = {
            'sk': {
                GOAL: 'Gól',
                ASSIST: 'Asistencie',
                GAME_PLAYED: 'GP',
                WINS: 'Výhry',
                DRAWS: 'Remízy',
                LOOSES: 'Prehry',
                WIN_PREDICTION: '%',
                S_PLUS: 'sPlus',
                S_MINUS: 'sMinus',
                PLUS_MINUS_DIFF: '+/-',
                AVERAGE_GOALS: 'Priemerne gólov',
                AVERAGE_ASSISTS: 'Priemerne asistencií',
                GA: 'GA',
                AVERAGE_GA: 'Priemerne GA',
                CRS: 'CRS',
                AVERAGE_CRS: 'Priemerne CRS',
                GAA: 'GAA',
                SHOTOUTS: 'Shotouts',
                MOM: 'MOM',
                AVERAGE_MOM: 'priemerne MoM',
            },
            'en': {
                GOAL: 'Goal',
                ASSIST: '',
                GAME_PLAYED: '',
                WINS: '',
                DRAWS: '',
                LOOSES: '',
                WIN_PREDICTION: '',
                S_PLUS: '',
                S_MINUS: '',
                PLUS_MINUS_DIFF: '',
                AVERAGE_GOALS: '',
                AVERAGE_ASSISTS: '',
                GA: '',
                AVERAGE_GA: '',
                CRS: '',
                AVERAGE_CRS: '',
                GAA: '',
                SHOTOUTS: '',
                MOM: '',
                AVERAGE_MOM: '',
            }
           
        };
    }

    translate(val)
    {
       return this.translations[this.lang][val];
    }
}

export  default Translator = new Translator('sk');