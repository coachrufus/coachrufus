import React, { Component } from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import Selector from  './components/Selector.js';
import Translator from './components/Translator.js';
class App extends Component {

   
    constructor(props) {
        super(props);

        this.state = {
          visiblePoints: 'crs',
          lang: 'sk',
          seasonLoaded: false
        };


        this.pointSelectOptions = [
            {'name':Translator.translate('CRS'),'value':'crs'},
            {'name':Translator.translate('GOAL'),'value':'goals'},
            {'name':Translator.translate('ASSIST'),'value':'assists'},
            {'name':Translator.translate('GAME_PLAYED'),'value':'game_played'},
            {'name':Translator.translate('WINS'),'value':'wins'},
            {'name':Translator.translate('DRAWS'),'value':'draws'},
            {'name':Translator.translate('LOOSES'),'value':'looses'},
            {'name':Translator.translate('WIN_PREDICTION'),'value':'win_prediction'},
            {'name':Translator.translate('S_PLUS'),'value':'s_plus'},
            {'name':Translator.translate('S_MINUS'),'value':'s_minus'},
            {'name':Translator.translate('PLUS_MINUS_DIFF'),'value':'plus_minus_diff'},
            {'name':Translator.translate('AVERAGE_GOALS'),'value':'average_goals'},
            {'name':Translator.translate('AVERAGE_ASSISTS'),'value':'average_assists'},
            {'name':Translator.translate('GA'),'value':'ga'},
            {'name':Translator.translate('AVERAGE_GA'),'value':'average_ga'},
            {'name':Translator.translate('AVERAGE_CRS'),'value':'average_crs'},
            {'name':Translator.translate('GAA'),'value':'gaa'},
            {'name':Translator.translate('SHOTOUTS'),'value':'shotouts'},
            {'name':Translator.translate('MOM'),'value':'mom'},
            {'name':Translator.translate('AVERAGE_MOM'),'value':'average_mom'},
           
        ];
        
         this.handleSeasonChange = this.handleSeasonChange.bind(this);
         this.handlePointChange = this.handlePointChange.bind(this);
      }


    componentDidMount() {

        this.props.dispatch({ type: 'INIT'});
        

        const dataUrl = this.props.provider+'/api/content/games/stats-data?h='+this.props.h+'&team_id='+this.props.t+'&season_id='+this.props.s+'&apikey=app@40101499408386491';
        axios.get(dataUrl)
            .then(res => {
                const players = [];

                res.data.players.forEach((item) => {
                    players.push(item);
                });
                this.props.dispatch({ type: 'AJAX_LOAD_PLAYERS', players:players });
            });


            //load seasons
            const seasonDataUrl = this.props.provider+'/api/team/season?h='+this.props.h+'&team_id='+this.props.t+'&apikey=app@40101499408386491';
            axios.get(seasonDataUrl)
            .then(res => {
                const seasons = [];

                res.data.data.forEach((item) => {
                    seasons.push({'name':item.name.substring(0,15),'value':item.id});
                });
                this.props.dispatch({ type: 'AJAX_LOAD_SEASONS', seasons:seasons });
                this.setState({seasonLoaded:true});
            });

    }

    handlePointChange(e,type){
        e.preventDefault();

        this.props.dispatch({ type: 'SORT', sortIndex: type });
        
        this.setState({
            visiblePoints: type
        });
        
    };

    handleSeasonChange(e,type){
        e.preventDefault();
               
        
    
        const dataUrl = this.props.provider+'/api/content/games/stats-data?h='+this.props.h+'&team_id='+this.props.t+'&season_id='+type+'&apikey=app@40101499408386491';
        axios.get(dataUrl)
            .then(res => {
                const players = [];

                res.data.players.forEach((item) => {
                    players.push(item);
                });
                this.props.dispatch({ type: 'AJAX_LOAD_PLAYERS', players:players });
            });

    };

  
    

    render() {
        
        var provider = this.props.provider;
        //console.log(provider);
        return (
            <div>
                <div className="filter-row">
                   <Selector name="point" onItemSelect={this.handlePointChange} options={this.pointSelectOptions} />
                    {this.state.seasonLoaded ?  <Selector name="season" onItemSelect={this.handleSeasonChange} options={this.props.seasons} /> : null}
                </div>
                {this.props.players.map((player, key) => {
                     return (
                     <div key={key} className={"row row"+(key+1)}>
                        <div className="cell order-number">{key+1}</div>
                        <div className="cell avatar"><img src={provider+player.avatar} /></div>
                        <div className="cell title">{player.name}<br />{player.surname}</div>
                        <div className={"cell points  goals " + (this.state.visiblePoints == 'goals' ? 'active' : '')}>{player.goals}</div>
                        <div className={"cell points  assists " + (this.state.visiblePoints == 'assists' ? 'active' : '')}>{player.assists}</div>
                        <div className={"cell points  game_played " + (this.state.visiblePoints == 'game_played' ? 'active' : '')}>{player.game_played}</div>
                        <div className={"cell points  wins " + (this.state.visiblePoints == 'wins' ? 'active' : '')}>{player.wins}</div>
                        <div className={"cell points  draws " + (this.state.visiblePoints == 'draws' ? 'active' : '')}>{player.draws}</div>
                        <div className={"cell points  looses " + (this.state.visiblePoints == 'looses' ? 'active' : '')}>{player.looses}</div>
                        <div className={"cell points  win_prediction " + (this.state.visiblePoints == 'win_prediction' ? 'active' : '')}>{player.win_prediction}</div>
                        <div className={"cell points  s_plus " + (this.state.visiblePoints == 's_plus' ? 'active' : '')}>{player.s_plus}</div>
                        <div className={"cell points  s_minus " + (this.state.visiblePoints == 's_minus' ? 'active' : '')}>{player.s_minus}</div>
                        <div className={"cell points  plus_minus_diff " + (this.state.visiblePoints == 'plus_minus_diff' ? 'active' : '')}>{player.plus_minus_diff}</div>
                        <div className={"cell points  average_goals " + (this.state.visiblePoints == 'average_goals' ? 'active' : '')}>{player.average_goals}</div>
                        <div className={"cell points  average_assists " + (this.state.visiblePoints == 'average_assists' ? 'active' : '')}>{player.average_assists}</div>
                        <div className={"cell points  ga " + (this.state.visiblePoints == 'ga' ? 'active' : '')}>{player.ga}</div>
                        <div className={"cell points  average_ga " + (this.state.visiblePoints == 'average_ga' ? 'active' : '')}>{player.average_ga}</div>
                        <div className={"cell points  crs " + (this.state.visiblePoints == 'crs' ? 'active' : '')}>{player.crs}</div>
                        <div className={"cell points  average_crs " + (this.state.visiblePoints == 'average_crs' ? 'active' : '')}>{player.average_crs}</div>
                        <div className={"cell points  gaa " + (this.state.visiblePoints == 'gaa' ? 'active' : '')}>{player.gaa}</div>
                        <div className={"cell points  shotouts " + (this.state.visiblePoints == 'shotouts' ? 'active' : '')}>{player.shotouts}</div>
                        <div className={"cell points  mom " + (this.state.visiblePoints == 'mom' ? 'active' : '')}>{player.mom}</div>
                        <div className={"cell points  average_mom " + (this.state.visiblePoints == 'average_mom' ? 'active' : '')}>{player.average_mom}</div>
                     </div>)}
                )}
                
                
            </div>
        );
    }
}
;

function mapStateToProps(state) {
    
    return {
        players: state.players,
        seasons: state.seasons
    };
  }

//export default App;
export default connect(mapStateToProps)(App);