import { createStore } from "redux";
import axios from 'axios';
//import rootReducer from "../reducers/index";

const initState = {
    players:[],
    seasons:[]
};

function compareValues(key, order='desc') {
    return function(a, b) {
      if(!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
        // property doesn't exist on either object
        console.log('nio'+key);
        comparison = -1;
      }
     
      const varA = parseFloat(a[key]);
      const varB = parseFloat(b[key]);
 
       let comparison = 0;
      
      if(a[key] == '-' && b[key] != '-')
      {
        comparison = -1;
      }
      else if(a[key] != '-' && b[key] == '-')
      {
        comparison = 1;
      }
      else if(a[key] == '-' && b[key] == '-')
      {
        comparison = -1;
      }
      else if (varA > varB) {
        comparison = 1;
      } else if (varA < varB) {
        comparison = -1;
      }




      /*
      const varA = (typeof a[key] === 'string') ? 
        a[key].toUpperCase() : a[key];
      const varB = (typeof b[key] === 'string') ? 
        b[key].toUpperCase() : b[key];
      */

     
     


      return (
        (order == 'desc') ? (comparison * -1) : comparison
      );
    };
  }


function reducer(state = initState, action) {
   
    /*
    if(action.type === "INIT") {
        return initState
      }
*/
    //console.log('reducer');
    if(action.type === "AJAX_LOAD_PLAYERS") {

        const players = action.players;
        //console.log(players);

        players.sort(compareValues('crs'));

        return {
            players: players,
            seasons: state.seasons
        };
      }
    

      if(action.type === "AJAX_LOAD_SEASONS") {

        const seasons = action.seasons;
        //console.log(seasons);

        return {
            players:state.players,
            seasons: seasons

        };
      }

    if(action.type === "SORT") {

       // console.log(action.sortIndex);
        const players = state.players;

       players.sort(compareValues(action.sortIndex));
      // console.log(players);



       return {
           players: players
       };

        /*
        return {
            players: [
                {name: 'fake_assist','goals':2,'assists':50},
                {name: 'fake_goal','goals':10,'assists':5},
            ]
        };
        */
      }
   
    return state;
  }

const store = createStore(reducer);
export default store;