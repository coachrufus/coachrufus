import React, { Component } from 'react';
import axios from 'axios';
import Translations from  './../Translations.js';
var Trans;
const Config = require('./../Config.js');
class PlayerSelector extends Component {


    constructor(props) {
        super(props);
         Trans = Translations.getTranslations(this.props.lang);
        var currentLineup;
        if('first_line' == this.props.currentLine)
        {
            currentLineup = this.props.lineup.firstLine;
        }

        if('second_line' == this.props.currentLine)
        {
            currentLineup = this.props.lineup.secondLine;
        }

        if('mom' == this.props.currentLine)
        {
            currentLineup=  Object.assign(this.props.lineup.firstLine, this.props.lineup.secondLine);
        }



        var goalChoices = [];  
        var assistChoices = []; 
        var momChoices = [];
        var i  = 0;
        var lineupKeys = Object.keys(currentLineup);
        lineupKeys.forEach(function (k) {
            i++;
            currentLineup[k].player.playerNumber = currentLineup[k].teamPlayer.player_number;
            currentLineup[k].player.efficiency = currentLineup[k].efficiency;
            goalChoices[i] = currentLineup[k].player;
            assistChoices[i] = currentLineup[k].player;
            momChoices[i] = currentLineup[k].player;
        });
        
        //console.log(this.props.lineup);
        
        //add nobody choice
        var lid_team = ('firstLine' == this.props.currentLine) ? 'first_line' : 'second_line';
        assistChoices.push({'avatar':'/img/team/users.svg','player_id':null,'id':0,'lid_team':lid_team,'player_name':Trans.no_assist,'efficiency':'none'});
        momChoices.push({'avatar':'/img/team/users.svg','player_id':null,'id':0,'lid_team':lid_team,'player_name':'Nobody','lineup_id':this.props.lineup.id,'lineup_position':'none','efficiency':'none'});
        
        var time = this.props.time;
        var time = {
            'hour': ('0' + this.props.time.h).slice(-2) ,
            'minute': ('0' + this.props.time.m).slice(-2) ,
            'second': ('0' + this.props.time.s).slice(-2) 
        };

        /*
        time.hour = Math.floor(this.props.seconds / 3600);
        time.minute = Math.floor((this.props.seconds - (time.hour * 3600)) / 60);
        time.second = Math.floor(this.props.seconds - (time.hour * 3600) - (time.minute * 60));
        */

        this.state = {
            lineup:this.props.lineup,
            currentLine:this.props.currentLine,
            goalChoices: goalChoices, 
            assistChoices: assistChoices, 
            currentAssistChoices: assistChoices, 
            momChoices: momChoices,
            goalPlayer:null,
            assistPlayer:null,
            showGoalChoice:true,
            showAssistChoice: false, 
            showMomChoice: false,
            finishLoader:false,
            time:time
        };

        if('mom' == this.props.currentLine)
        {
            this.state.showGoalChoice = false; 
            this.state.showMomChoice = true;
        }
        
        this.messageOrigin;
        this.messageSource;
        this.initHandler = this.initHandler.bind(this); 
        this.sendMessage = this.sendMessage.bind(this);

        this.handleGoalPlayerSelect = this.handleGoalPlayerSelect.bind(this);
        this.handleAssistPlayerSelect = this.handleAssistPlayerSelect.bind(this);
        this.handleMomPlayerSelect = this.handleMomPlayerSelect.bind(this);
        this.renderPlayerSelectorItem = this.renderPlayerSelectorItem.bind(this); 
        this.renderTopbar = this.renderTopbar.bind(this);
        this.handleAssistBack = this.handleAssistBack.bind(this);
        this.handleTimeMinuteChange = this.handleTimeMinuteChange.bind(this);
        this.handleTimeSecondChange = this.handleTimeSecondChange.bind(this);
        
        this.lineupUrl = Config.BASE_URL+ this.props.lineup.lineupUrl;
        
        
    }
    
     sendMessage(e) {
        e.preventDefault();
      
        var message = {
            'action': 'popup-full',
            'onclose-action':'refresh',
            'href':Config.BASE_URL+this.props.lineup.lineupUrl
        };

        window.postMessage(JSON.stringify(message) ,'*');
    };
    
    initHandler(e)
    {
        this.messageOrigin = e.origin;
        this.messageSource = e.source;
    }
    
    componentDidMount()
    {
        window.addEventListener('message', this.initHandler);
        this.setState({
             finishLoader:false
        });
    }

    handleGoalPlayerSelect(e,playerKey){
        e.preventDefault();
        var goalPlayer = this.state.goalChoices[playerKey];
        var allAssistChoices =  this.state.assistChoices;
        var currentAssistChoices = [];
        var i = 0;
        allAssistChoices.forEach(function (player, key) {
           if(player.id != goalPlayer.id)
            {
                currentAssistChoices[i] = player;
                i++;
            }
        });

        
        this.setState({
            goalPlayer: goalPlayer,
            showAssistChoice:true,
            showGoalChoice: false,
            currentAssistChoices: currentAssistChoices
        });
        
    };

    handleAssistPlayerSelect(e,playerKey){
        e.preventDefault();
        
        this.setState({
            finishLoader:true
        });
        
        
        var assistPlayer = this.state.currentAssistChoices[playerKey];
        this.props.onItemSelect(e,this.state.goalPlayer,assistPlayer,this.state.time);
        

    }; 

    handleAssistBack(){
         this.setState({
            showAssistChoice:false,
            showGoalChoice: true
        });
    }; 

    handleMomPlayerSelect(e,playerKey){
        e.preventDefault();
        var momPlayer = this.state.momChoices[playerKey];
        this.props.onMomSelect(e,momPlayer);
    };

    renderPlayerSelectorItem(player,key, selectCallback)
    {
        return(
             <div className="col-xs-12 playerSelector-item" key={'line'+key}  onClick={(e) => {selectCallback(e,key)}}  >
                <div className="playerNumber">{player.playerNumber}</div> 
                <div className="avatar"><img src={Config.BASE_URL+player.avatar} /></div>
                <div className="playerName">{player.player_name}  { player.efficiency === 'none' ?  "" : "("+player.efficiency+")" }<span className="player-position">{player.player_position}</span></div>
             </div>
        )
    }


    renderTopbar()
    {
        var title = '';
        var backCallback;
        if(this.state.showMomChoice)
        {
            title = 'MOM';
            backCallback = this.props.onClose; 

        }
        if(this.state.showGoalChoice)
        {
            title = Trans.navbar_Goal;
            backCallback =  this.props.onClose; 
        } 
        if(this.state.showAssistChoice)
        {
            title = Trans.navbar_Assist;
            backCallback = this.handleAssistBack; 
        }

        return (
            <div className="playerSelector-topbar">
                <div className="navBtn" onClick={(e) => {backCallback()}}></div>
                {title}
            </div>
        )
    }
    
    renderLoader()
    {
        return (
                <div className="loader">{Trans.loading_text}</div>
       );
    };
    
    handleTimeMinuteChange(event) {
        var time = this.state.time;
        time.minute = event.target.value;
        this.setState({time: time});
    }
    
    handleTimeSecondChange(event) {
        var time = this.state.time;
        time.second = event.target.value;
        this.setState({time: time});
    }

    render() {
        var minuteSelectOptions = [];
        var i =0;
        for(i=0;i<60;i++)
        {
            minuteSelectOptions[i] = ('0' + i).slice(-2) ;
        }
        
        var secondSelectOptions = [];
        var s =0;
        for(s=0;s<60;s++)
        {
            secondSelectOptions[s] = ('0' + s).slice(-2) ; // '04';
        }
        
        console.log(minuteSelectOptions);
       console.log(this.state.time.minute);


        return (

            <div className="row playerSelector">
            {this.renderTopbar()}
          
             <select className="timeSelect" name="minute" value={this.state.time.minute}   onChange={this.handleTimeMinuteChange}>
              {
                minuteSelectOptions.map(function(minute) {
                  return <option key={minute}
                    value={minute}>{minute}</option>;
                })
              }
             </select>
             :
              <select className="timeSelect" name="second" value={this.state.time.second}   onChange={this.handleTimeSecondChange}>
              {
                secondSelectOptions.map(function(second) {
                  return <option key={second}
                    value={second}>{second}</option>;
                })
              }
             </select>

            {this.state.finishLoader ? this.renderLoader() : null}
            
            

           {this.state.showGoalChoice && this.state.goalChoices.length > 0 
                ?  
                 this.state.goalChoices.map((player, key) => {
                      return (
                         this.renderPlayerSelectorItem(player, key,this.handleGoalPlayerSelect)
                     )}
                 )
                : 
                ''   
            }
            
            {this.state.showGoalChoice && this.state.goalChoices.length == 0 ?

                <div className='no-lineup-alert'>
                    No available players
                    <a onClick={(e) => {this.sendMessage(e)}} className='btn btn-lineup' href={this.lineupUrl}>Check lineup</a>
                </div>
                
             : ''   
            }
            
            

             {this.state.showAssistChoice ?  
                this.state.currentAssistChoices.map((player, key) => {
                     return (
                        this.renderPlayerSelectorItem(player, key,this.handleAssistPlayerSelect)
                     )}
                )
                : 
                ''
            }

             {this.state.showMomChoice ?  
                this.state.momChoices.map((player, key) => {
                     return (
                        this.renderPlayerSelectorItem(player, key,this.handleMomPlayerSelect)
                     )}
                )
                : 
                ''
            }

            
            </div>

        );
    }
}
;


export default PlayerSelector;
