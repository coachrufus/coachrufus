import React, { Component } from 'react';
import Translations from  './../Translations.js';
var Trans;
class NavBar extends Component {


    constructor(props) {
        super(props);
                 Trans = Translations.getTranslations(this.props.lang);
        this.messageOrigin;
        this.messageSource;

        this.componentDidMount = this.componentDidMount.bind(this); 
        this.initHandler = this.initHandler.bind(this); 
        this.sendMessage = this.sendMessage.bind(this);
    };

    initHandler(e)
    {
        this.messageOrigin = e.origin;
        this.messageSource = e.source;
    }

    componentDidMount() {
        window.addEventListener('message', this.initHandler);
    }

    sendMessage() {
        var message = {
            'action': this.props.action
        };
        window.postMessage(JSON.stringify(message) ,'*');
        
        window.history.back();
    };

    render() {
        return (
            <div className="nav-topbar">
                <div className="navBtn" onClick={(e) => {this.sendMessage()}}></div>
               {this.props.title}
                <a  className="fastScore" href={this.props.fastScoreUrl}>{Trans.fastScore}</a>
               
                
            </div>

        );
    };
};


export default NavBar;
