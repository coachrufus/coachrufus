import React, { Component } from 'react';
import axios from 'axios';
const Config = require('./../Config.js');
import Translations from  './../Translations.js';
var Trans;
class Timer extends Component {


    constructor(props) {
        super(props);
         Trans = Translations.getTranslations(this.props.lang);
        this.apiUrl = this.props.apiUrl;
        this.timer = 0;
        this.startTimer = this.startTimer.bind(this);
        this.pauseTimer = this.pauseTimer.bind(this);
        this.updateCounter = this.updateCounter.bind(this);

        var time = {
            'h': 0,
            'm': 0,
            's': 0
        };

        if ('running' == this.props.status || 'paused' == this.props.status) {
            time.h = Math.floor(this.props.seconds / 3600);
            time.m = Math.floor((this.props.seconds - (time.h * 3600)) / 60);
            time.s = Math.floor(this.props.seconds - (time.h * 3600) - (time.m * 60));
        }

        if ('running' == this.props.status) {
            this.timer = setInterval(this.updateCounter, 1000);
        }

        this.props.onUpdateTime(time);

        this.state = {
            time: time,
            seconds: this.props.seconds,
            status: this.props.status,
            overtime: false

        };

        this.handleEndMatch = this.handleEndMatch.bind(this);
        this.handleOvertime = this.handleOvertime.bind(this);
    }

    startTimer() {
        this.timer = setInterval(this.updateCounter, 1000);
        const startMatchUrl = encodeURI(Config.API_URL + '/content/game/start-match');
        axios.put(startMatchUrl, 'apikey=' + Config.API_KEY + '&user_id='+this.props.uid+'&lid='+this.props.lineup.id);

        this.setState({
            status: 'running',
        });

    }

    pauseTimer() {
        clearInterval(this.timer);

        //update seconds_past
        const startMatchUrl = encodeURI(Config.API_URL + '/content/game/pause-match');
        axios.put(startMatchUrl, 'apikey=' + Config.API_KEY + '&user_id='+this.props.uid+'&lid='+this.props.lineup.id+'&seconds=' + this.state.seconds);

        this.setState({
            status: 'paused',
        });

    }

    updateCounter() {
        let seconds = this.state.seconds + 1;

        this.props.onUpdateTime( this.secondsToTime(seconds));

        this.setState({
            time: this.secondsToTime(seconds),
            seconds: seconds,
        });

        if (seconds == 0) {
            clearInterval(this.timer);
        }
    }

    secondsToTime(secs) {
        let hours = Math.floor(secs / (60 * 60));
        let divisor_for_minutes = secs % (60 * 60);
        let minutes = Math.floor(divisor_for_minutes / 60);
        let divisor_for_seconds = divisor_for_minutes % 60;
        let seconds = Math.ceil(divisor_for_seconds);
        let obj = {
            "h": hours,
            "m": minutes,
            "s": seconds
        };
        return obj;
    }
    
    handleOvertime(e)
    {
        if(this.state.overtime == false)
        {
            this.setState({
                overtime:true
            });
        }
        else
        {
             this.setState({
                overtime:false
            });
        }
    }


    handleEndMatch(e) {
        e.preventDefault();
        this.props.onMatchEnd(e,this.state.overtime);
    };
    render() {
        
        
        var overtimeClass = 'overtimeBtn'
        if(this.state.overtime)
        {
            overtimeClass += ' active';
        }
        return (
            <div>
                <div className="counter">{('0'+this.state.time.m).slice(-2)}:{('0'+this.state.time.s).slice(-2)}</div>
                

                <div className="row timer-toolbar">
                <div className="col-xs-4"></div> 
                <div className="col-xs-4">
                      {this.state.status == 'running' ? 
                            <button className="pauseBtn" onClick={this.pauseTimer} href=""></button>
                            : 
                            <button className="startBtn" onClick={this.startTimer} href=""></button>
                        }

                </div> 
                <div className="col-xs-4">
                   
                    <button className="finalBtn" onClick={(e) => {this.handleEndMatch(e)}}>{Trans.finishGame}</button><br />
                        
                    <div className="overtimeWrap">
                        {Trans.overtime}
                        <span className={overtimeClass}  onClick={(e) => {this.handleOvertime(e)}}><span className="marker"></span></span>
                    </div>
                </div>
                </div>
              
               
            </div>

        );
    }
};


export default Timer;
