import React, { Component } from 'react';

import Translations from  './../Translations.js';
var Trans;
const Config = require('./../Config.js');

class TimelineItem extends Component {

    constructor(props) {
        super(props);
         Trans = Translations.getTranslations(this.props.lang);
        var itemClassName = this.props.group.lineup_position == 'first_line' ? 'first-line-item' : 'second-line-item';
        if(this.props.group.type == 'mom')
        {
            itemClassName = itemClassName + ' mom-line-item';
        }
        if(this.props.group.type == 'end')
        {
            itemClassName = itemClassName + ' end-line-item';
        }
        //console.log('construct timeline item');
        
        if("goal" in this.props.group)
        {
            this.props.group.time = this.props.group.goal.time;
        }
        
        if("assist" in this.props.group)
        {
            this.props.group.time = this.props.group.assist.time;
        }
        
         this.state = {
            toolbarVisible:false,
            visible:true,
            position:this.props.group.lineup_position,
            type:this.props.group.type,
            groupId:this.props.groupId,
            group:this.props.group,
            className:itemClassName
        };
        

        
        this.renderFirstLineContent = this.renderFirstLineContent.bind(this);
        this.handleToolbarClose = this.handleToolbarClose.bind(this);
        this.handleItemDelete = this.handleItemDelete.bind(this);
    }
    
    renderFirstLineGoalRow(group)
    {
         if("goal" in group)
        {
            return (
                     <div className="goal-row">
                       <span className="player-name">{group.goal.player} </span>
                       <div className="avatar"><img src={Config.BASE_URL + group.goal.player_avatar} /></div>
                       <span className="hit-accr">G</span>
                   </div>
            );
        }
    }
    renderFirstLineAssistRow(group)
    {
         if("assist" in group)
        {
            return (
                      <div className="assist-row">
                       <span className="player-name">{group.assist.player}</span>
                       <div className="avatar"><img src={Config.BASE_URL + group.assist.player_avatar} /></div>
                       <span className="hit-accr">A</span>
                   </div>
            );
        }
    }
    
    
    renderSecondLineGoalRow(group)
    {
         if("goal" in group)
        {
            return (
                      <div className="goal-row">
                            <span className="hit-accr">G</span>
                            <div className="avatar"><img src={Config.BASE_URL+ group.goal.player_avatar} /></div>
                            <span className="player-name">{group.goal.player}</span>
                        </div>
            );
        }
    }
    renderSecondLineAssistRow(group)
    {
         if("assist" in group)
        {
            return (
                       
                        <div className="assist-row">
                            <span className="hit-accr">A</span>
                            <div className="avatar"><img src={Config.BASE_URL+ group.assist.player_avatar} /></div>
                            <span className="player-name">{group.assist.player}</span>
                        </div>
            );
        }
    }
    
    
    renderFirstLineContent(){
        var group = this.state.group;
        
        if('end' === this.state.type)
        {
            return (
                    <div className="first-line">    
                        <div className="time-row">
                            <div className="time">
                            {group.end.time}
                            </div>
                        </div>
                    </div>
            );
        };

        
        if(this.state.position === 'second_line' && 'hit' === this.state.type)
        {
            return (
                     <div className="first-line">    
                        <div className="time-row">
                            <div className="time">
                            {group.time}
                            </div>
                        </div>
                    </div>
            );
        };
        
        if(this.state.position === 'second_line' && 'mom' === this.state.type)
        {
            return (
                     <div className="first-line">    
                        <div className="time-row">
                            <div className="time">
                            MOM
                            </div>
                        </div>
                    </div>
            );
        };
        
        if(this.state.position === 'first_line'  && 'hit' === this.state.type)
        {
            return (
                    <div className="first-line">
                        {this.renderFirstLineGoalRow(group)}
                        {this.renderFirstLineAssistRow(group)}
                        <div className="icon"><i className="ico ico-ball"></i></div>
                   </div>
            );
        };
        
         if(this.state.position === 'first_line' && 'mom' === this.state.type)
        {
            return (
                     <div className="first-line">
                        <div className="goal-row">
                           <span className="player-name">{group.mom.player} </span>
                           <div className="avatar"><img src={Config.BASE_URL+ group.mom.player_avatar} /></div>
                       </div>

                       <div className="icon"><i className="ico ico-cup"></i></div>
                       </div>
            );
        };
        
        
    }
    
     renderSecondLineContent(){
        var group = this.state.group;
         
         if('end' === this.state.type)
        {
            return (
                      <div className="second-line">
                        <div className="goal-row">
                            <span className="player-name">{Trans.game_over}</span>
                        </div>
                     
                        <div className="icon"><i className="ico ico-clock"></i></div>
                    </div>
            );
        };
        
        
        
        if(this.state.position === 'second_line' && 'hit' === this.state.type)
        {
            return (
                   <div className="second-line">
                        {this.renderSecondLineGoalRow(group)}
                        {this.renderSecondLineAssistRow(group)}
                                
                        <div className="icon"><i className="ico ico-ball"></i></div>
                    </div>
            );
        };
        
        
        if(this.state.position === 'second_line' && 'mom' === this.state.type)
        {
            return (
                        
                         
                         <div className="second-line">
                        <div className="goal-row">
                            <div className="avatar"><img src={Config.BASE_URL+ group.mom.player_avatar} /></div>
                            <span className="player-name">{group.mom.player}</span>
                        </div>
                      
                        <div className="icon"><i className="ico ico-cup"></i></div>
                    </div>
            );
        };
        
        
        if(this.state.position === 'first_line'  && 'hit' === this.state.type)
        {
            return (
                    <div className="second-line">
             <div className="time-row">
                        <div className="time">
                            {group.time}
                        </div>
                         </div>
                         </div>
            );
        };
        
         if(this.state.position === 'first_line'  && 'mom' === this.state.type)
        {
            return (
                    <div className="second-line">
             <div className="time-row">
                        <div className="time">
                            MOM
                        </div>
                         </div>
                         </div>
            );
        };
    }
    
    
    handleClick(){
        this.setState({
            toolbarVisible:true
        }); 
   }    
   
   handleToolbarClose(){
        this.setState({
            toolbarVisible:false
        }); 
   }
   
   handleItemDelete(){
       this.props.onDelete(this.state.group.key);
       //console.log('delte');
       //console.log(this.state.group.key);
       this.setState({
           toolbarVisible: false,
           className: 'timeline-item-deleted'
       });
   }
   
   handleItemEdit(){
       this.props.onEdit(this.state.group,this.state.groupId);
        this.setState({
           toolbarVisible: false
       });
   }
   
   

    render() {
        
        
        //console.log('render timeline item');
        //console.log(this.state);
        
        return (
                <div className={"timeline-item "+this.state.className} >
                     
                    {this.state.toolbarVisible === true ?
            
                    <div className="timeline-item-tools">
                        <div className="editBtn"  onClick={(e) => {this.handleItemEdit()}}>Edit</div>
                        <div className="deleteBtn" onClick={(e) => {this.handleItemDelete()}}></div>
                        <div className="closeBtn" onClick={(e) => {this.handleToolbarClose()}}></div>
                    </div>
                    
                    : null }
                            
                    <div className="toolbar_trigger_wrap" onClick={(e) => {this.handleClick()}}>
                    {this.renderFirstLineContent()}
                    {this.renderSecondLineContent()}
                    </div>
                    
                  
                       
                </div>
                   
                 );
    }
}
;


export default TimelineItem;
