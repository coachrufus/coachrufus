import React, { Component } from 'react';
import TimelineItem from  './TimelineItem.js';
import PlayerSelector from  './PlayerSelector.js';
import axios from 'axios';

const Config = require('./../Config.js');

class Timeline extends Component {

    constructor(props) {
        super(props);
        //console.log('timeline component');
        //var timelineItems = this.createTimelineItems(this.props.timeline);
        this.state = {
            timelineItems: [],
            timelineLoaded:false,
            PlayerSelectorVisible: false,
            PlayerSelectorSeconds:0,
            currentLine: '',
            currentEditedItem: null,
            currentEditedItemIndex: null,
            currentEditedItemTime:null
        };


        this.handleItemDelete = this.handleItemDelete.bind(this);
        this.handleItemEdit = this.handleItemEdit.bind(this);
        this.handleClosePlayerChoice = this.handleClosePlayerChoice.bind(this);
        this.handleEditScoreEvent = this.handleEditScoreEvent.bind(this);
        this.loadTimeline = this.loadTimeline.bind(this);
    }

    createTimelineItems(timeline) {
        var timelineItems = [];
        var timelineProp = timeline;
        var i = 0;
        var timelineKeys = Object.keys(timelineProp);
        timelineKeys.forEach(function (k) {
            timelineProp[k].key = k;
            timelineItems[i++] = timelineProp[k];
        });
        return timelineItems;
    }

    loadTimeline()
    {
        const timelineDataUrl = Config.API_URL + '/content/game/timeline?apikey=' + Config.API_KEY + '&user_id=' + this.props.uid + '&event_id=' + this.props.eventId + '&event_current_date=' + this.props.eventCurrentDate + '&lid=' + this.props.lineup.id;
        axios.get(timelineDataUrl)
                .then(res => {
                    var timelineItems = this.createTimelineItems(res.data.timeline_items);
                    this.setState({
                        timelineItems: timelineItems,
                        timelineLoaded: true
                    });
                    this.props.onUpdateTimeline(res.data);
                });
    }

    componentDidMount() {
        this.loadTimeline();
    }

    renderMomLineItem(group, key)
    {
        if (group.lineup_position == 'first_line')
        {
            return (
                    <div className="timeline-item timeline-item-mom" key={key} >
                        <div className="first-line">
                            <div className="goal-row">
                                <span className="player-name">{group.mom.player}</span>
                                <div className="avatar"><img src={this.props.baseUrl + group.mom.player_avatar} /></div>
                            </div>
                            <div className="icon"><i className="ico ico-cup"></i></div>
                        </div>
                    
                    
                        <div className="second-line">
                            <div className="time">
                                MOM
                            </div>
                        </div>
                    </div>
                    );
        }

        if (group.lineup_position == 'second_line')
        {
            return (
                    <div className="timeline-item timeline-item-mom second-line-item" key={key} >
                        <div className="first-line">
                            <div className="time">
                                MOM
                            </div>
                        </div>
                    
                    
                        <div className="second-line">
                            <div className="goal-row">
                    
                                <div className="avatar"><img src={this.props.baseUrl + group.mom.player_avatar} /></div>
                                <span className="player-name">{group.mom.player}</span>
                            </div>
                            <div className="icon"><i className="ico ico-cup"></i></div>
                        </div>
                    </div>
                    );
        }
    }

    handleItemDelete(id) {
        
       
        
        const removeUrl = encodeURI(Config.BASE_URL + '/team/match/remove-timeline-stat');
        var params = new URLSearchParams();
        params.append("hg", id);
        var request = {
            params: params
        };
        //axios.get(removeUrl, request);
        axios.get(removeUrl, request).then(res => {
            //this.props.onItemDelete();
           // this.loadTimeline();
        });
        ;


    }

    handleItemEdit(item, itemIndex) {
        
        var timeParts = item.goal.time.split(':');
        var PlayerSelectorSeconds = (parseInt(timeParts[0])*60*60)+(parseInt(timeParts[1])*60)+parseInt(timeParts[2]);
        
        var time = {
            'h': parseInt(timeParts[0]),
            'm': parseInt(timeParts[1]),
            's': parseInt(timeParts[2])
        };
        
        this.setState({
            PlayerSelectorVisible: true,
            PlayerSelectorSeconds: PlayerSelectorSeconds,
            currentLine: item.lineup_position,
            currentEditedItem: item,
            currentEditedItemIndex: itemIndex,
            currentEditedItemTime:time
        });
        
    }

    handleClosePlayerChoice() {
        //e.preventDefault();
        this.setState({
            PlayerSelectorVisible: false
        });
    }

    handleEditScoreEvent(e, goalPlayer, assistPlayer,time) {



        const editScoreUrl = encodeURI(Config.BASE_URL + '/team/match/edit-timeline-stat');
        var params = new URLSearchParams();
        var timelineEventTime = time.hour+':'+time.minute+':'+time.second;
        params.append("event_id", goalPlayer.event_id);
        params.append("event_date", goalPlayer.event_date);
        params.append("lid", goalPlayer.lineup_id);
        params.append("time",timelineEventTime); //00:00:00
        params.append("goal[player_id]", goalPlayer.player_id);
        params.append("goal[lineup_player_id]", goalPlayer.id);
        params.append("goal[lid_team]", goalPlayer.lineup_position);
        params.append("goal[hid]", this.state.currentEditedItem.goal.id);
        params.append("goal[time]",timelineEventTime);
        params.append("goal[lid_team]", assistPlayer.lineup_position);
        params.append("assist[player_id]", assistPlayer.player_id);
        params.append("assist[lineup_player_id]", assistPlayer.id);
        params.append("assist[time]",timelineEventTime);
        params.append("assist[lid_team]", assistPlayer.lid_team);
        params.append("assist[hid]", this.state.currentEditedItem.assist.id);


        var request = {
            params: params
        };
        axios.get(editScoreUrl, request);

        //rebuild timeline
        var newTimelineItems = this.state.timelineItems;
        newTimelineItems[this.state.currentEditedItemIndex].goal.player = goalPlayer.player_name;
        newTimelineItems[this.state.currentEditedItemIndex].goal.player_avatar = goalPlayer.avatar;
        newTimelineItems[this.state.currentEditedItemIndex].goal.time = timelineEventTime;
        newTimelineItems[this.state.currentEditedItemIndex].assist.player = assistPlayer.player_name;
        newTimelineItems[this.state.currentEditedItemIndex].assist.player_avatar = assistPlayer.avatar;
        newTimelineItems[this.state.currentEditedItemIndex].assist.time = timelineEventTime;


        //this.loadTimeline();
        this.setState({
            PlayerSelectorVisible: false,
            timelineItems: newTimelineItems,
        });
    }

    renderItems()
    {
        var timelineItems = this.state.timelineItems;
        
       
            return (
                
                    timelineItems.map((group, key) => {
                        return (
                                <TimelineItem  lang={this.props.lang} onDelete={this.handleItemDelete} onEdit={this.handleItemEdit}  key={key} group={group} groupId={key} />
                                )
                    })
            )
        
        
        
    }

    render() {

        var wrapperClassName = "timeline-wrap";
        //console.log('render timeline');
        //console.log(this.state.timelineItems);



        if (this.state.PlayerSelectorVisible)
        {
            wrapperClassName = "timeline-wrap timeline-wrap-hidden";
        }

        if (this.state.timelineItems.length < 1)
        {
            wrapperClassName = "timeline-wrap timeline-wrap-hidden";
        }


        return (
                <div className={wrapperClassName}>
                
                    {this.state.PlayerSelectorVisible ?
                                    <div className="full-window">
                                        <PlayerSelector 
                                            onClose={this.handleClosePlayerChoice} 
                                            onItemSelect={this.handleEditScoreEvent} 
                                            onMomSelect={this.handleEditMom} 
                                            lineup={this.props.lineup} 
                                            currentLine={this.state.currentLine}
                                            seconds={this.state.PlayerSelectorSeconds}
                                            time={this.state.currentEditedItemTime}
                                             lang={this.props.lang}
                                            />
                                    </div>
                            : null}
                
                
                    <div className="row timeline" >
                        {this.renderItems()}
                    </div>
                </div>

                );
    }
}
;



export default Timeline;
