import React, { Component } from 'react';


var _Environments = {
    production:  {},
    staging:     {BASE_URL: '', API_KEY: ''},
    development: {
        BASE_URL: 'https://dev.coachrufus.com', 
        API_URL: 'https://dev.coachrufus.com/api', 
        API_KEY: 'app@40101499408386491', 
        LANG:'sk'
    },
    local: {
        BASE_URL: 'http://app.coachrufus.lamp', 
        API_URL: 'http://app.coachrufus.lamp/api', 
        API_KEY: 'app@40101499408386491', 
        LANG:'sk'
    },
     prod: {
        BASE_URL: 'https://app.coachrufus.com', 
        API_URL: 'https://app.coachrufus.com/api', 
        API_KEY: 'app@40101499408386491', 
        LANG:'sk'
    }
};

function getEnvironment() {
    const hostname = window && window.location && window.location.hostname;
    if('app.coachrufus.lamp' == hostname)
    {
        return _Environments['local'];
    }
    
    if('dev.coachrufus.com' == hostname)
    {
        return _Environments['development'];
    }
    
    if('app.coachrufus.com' == hostname)
    {
        return _Environments['prod'];
    }
    
    
}

var Config = getEnvironment();
module.exports = Config;