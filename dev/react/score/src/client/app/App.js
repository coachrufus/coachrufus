import React, {
  Component
} from "react";
import axios from 'axios';
import humanDate from 'human-date';
import PlayerSelector from  './components/PlayerSelector.js';
import Timeline from  './components/Timeline.js';
import Timer from  './components/Timer.js';
import NavBar from  './components/NavBar.js';
const Config = require('./Config.js');
import Translations from  './Translations.js';
var Trans;

class App extends Component {
  constructor(props) {
    super(props);
 Trans = Translations.getTranslations(this.props.lang);
    this.state = {
      PlayerSelectorVisible: false,
      PlayerSelectorLoaded:false,
      timeline: {},
      lineup: {}, 
      event: {},
      timelineEventTime:null,
      seconds: null,
      lineupStatus:null,
      currentLine: null,
      lineupLoaded:false,
      timelineLoaded:true, 
      eventLoaded:false,
      eventId:this.props.eventId,
      lineupId:this.props.lineupId,
      eventCurrentDate:this.props.eventCurrentDate,
      apiKey:'app@40101499408386491',
      uid:this.props.uid,
      firstLineGoals:null,
      secondLineGoals:null ,
      fastScoreUrl:'',
      overtime:'no',
      closeMatchAlertVisible:false,
      loaderVisible: false
    };

    this.time = null;

   
    this.handleAddScoreEvent = this.handleAddScoreEvent.bind(this);
    this.handleAddMatchEnd = this.handleAddMatchEnd.bind(this); 
    this.handleClosePlayerChoice = this.handleClosePlayerChoice.bind(this); 
    this.handleAddMatchEndEvent = this.handleAddMatchEndEvent.bind(this);
    this.handleTimerUpdate = this.handleTimerUpdate.bind(this);
    this.handleTimelineItemDelete = this.handleTimelineItemDelete.bind(this);
    this.handleTimelineUpdate = this.handleTimelineUpdate.bind(this);
  }
        

 

  componentDidMount() {
     this.loadLineup(); 
     //this.loadTimeline(); 
     this.loadEventDetail();
  }

  loadEventDetail()
  {
     
    const eventDataUrl = Config.API_URL+'/event/detail?apikey='+Config.API_KEY+'&uid='+this.state.uid+'&id='+this.state.eventId+'&date='+this.state.eventCurrentDate;
    axios.get(eventDataUrl)
      .then(res => {
        //console.log(res.data);
        
        var eventData = res.data;        
        var today = new Date();

        var tomorrow = new Date();
        tomorrow.setDate(today.getDate()+1);
        tomorrow.setHours(23);
        tomorrow.setMinutes(59);
        tomorrow.setSeconds(59);
        
        
        var yesterday = new Date();
        yesterday.setDate(today.getDate()-1);
       
        if(eventData.termin <  (tomorrow.getTime() / 1000) && eventData.termin >  (yesterday.getTime() / 1000))
        {
            eventData.date = humanDate.relativeTime(eventData.termin - (new Date().getTime() / 1000));
        }
        else
        {
           var eventDate = new Date(eventData.termin*1000);
           eventData.date =  eventDate.getDate()+'.'+(eventDate.getMonth()+1)+'.'+eventDate.getFullYear()+', '+eventDate.getHours()+':'+eventDate.getMinutes();
        }
         //humanDate.relativeTime(this.state.event.termin - (new Date().getTime() / 1000))
                    
                    
        this.setState({
            event: eventData,
            eventLoaded: true
          });
      });
        
  }
  
  loadLineup()
  {

     const lineupDataUrl =  Config.API_URL+'/content/game/lineup?apikey='+Config.API_KEY+'&user_id='+this.state.uid+'&event_id='+this.state.eventId+'&event_current_date='+this.state.eventCurrentDate+'&lid='+this.state.lineupId;
    var lineupId = null;
    var lineup = null;
    axios.get(lineupDataUrl)
      .then(res => {
        if (res.data.id == null)
        {
          return false;
        }
        else {
          var lineupStatus =  res.data.status;
          var seconds = 0;
          
          if('running' == lineupStatus )
          {
            
            var startTime = new Date(res.data.started_at.date);
            var currentTime = new Date();
            var lastSeconds =  Math.floor((currentTime.getTime() - startTime.getTime())/1000);
            seconds =  parseInt(lastSeconds) + parseInt(res.data.seconds_past);
          }

          if('paused' == lineupStatus )
          {
            seconds =  parseInt(res.data.seconds_past);
          }

          this.setState({
            seconds: seconds,
            lineup: res.data,
            lineupLoaded:true,
            lineupStatus: lineupStatus,
            fastScoreUrl:res.data.fastScoreUrl
          });
        }
      });
  }


  handleShowPlayerChoices(e,targetLine){
    e.preventDefault();
    if('first_line' == targetLine)
    {
       this.setState({ 
        currentLine:'first_line',
        timelineEventTime:this.time,
        PlayerSelectorVisible: true,
      });
    }

     if('second_line' == targetLine)
    {
       this.setState({ 
       currentLine:'second_line',
       timelineEventTime:this.time,
        PlayerSelectorVisible: true,
        //timeline: new_timeline,
      });
    } 

    if('mom' == targetLine)
    {
       this.setState({ 
        currentLine:'mom',
        timelineEventTime:this.time,
        PlayerSelectorVisible: true,
        //timeline: new_timeline,
      });
    }
  }
  
  handleClosePlayerChoice(){
    //e.preventDefault();
    this.setState({ 
      PlayerSelectorVisible: false
    });
  }

  

  handleAddScoreEvent(e,goalPlayer,assistPlayer,time){
    this.setState({ 
             timelineLoaded: false
           });
              
    var lid_team = '';
    if('first_line' == this.state.currentLine)
    {
      lid_team = 'first_line';
    }
     if('second_line' == this.state.currentLine)
    {
      lid_team = 'second_line';
    }

    const addScoreUrl = encodeURI( Config.BASE_URL + '/team/match/add-timeline-stat');
    var params = new URLSearchParams();
    

    
    var timelineEventTime = time.hour+':'+time.minute+':'+time.second;
    
    params.append("event_id", this.state.eventId);
    params.append("event_date", this.state.eventCurrentDate);
    params.append("lid", this.state.lineupId);
    params.append("time",timelineEventTime); //00:00:00
    params.append("goal[player_id]",goalPlayer.player_id);
    params.append("goal[lineup_player_id]",goalPlayer.id);
    params.append("goal[time]",timelineEventTime);
    params.append("goal[lid_team]",lid_team); 
    params.append("assist[player_id]",assistPlayer.player_id);
    params.append("assist[lineup_player_id]",assistPlayer.id); 
    params.append("assist[time]",timelineEventTime);
    params.append("assist[lid_team]",lid_team);
   
    var request = {
      params: params
    };
    axios.get(addScoreUrl, request).then(res => {
          this.setState({ 
              timelineLoaded: true,
              PlayerSelectorVisible: false,
           });
      
    });;
  };
  
  handleTimelineItemDelete(){
       /*
        this.setState({ 
             timelineLoaded: false
           });
        */
  }


  handleAddMatchEnd(e,overtime){
    //show players window
    console.log(overtime);
    if(overtime == true)
    { 
        this.setState({
            overtime:'yes'
        });
    }
    
    //console.log(this.state.overtime);

    this.handleShowPlayerChoices(e,'mom');
  };

  handleAddMatchEndEvent(e,momPlayer){
    const addMomUrl = encodeURI( Config.BASE_URL + '/team/match/add-timeline-mom');
    var params = new URLSearchParams();
     var timelineEventTime =this.state.timelineEventTime.h+':'+this.state.timelineEventTime.m+':'+this.state.timelineEventTime.s;
    params.append("event_id", this.state.eventId); 
    params.append("type", 'hit');
    params.append("event_date", this.state.eventCurrentDate);
    params.append("lid", momPlayer.lineup_id);
    params.append("player_id", momPlayer.player_id);
    params.append("lineup_player_id", momPlayer.id);
    params.append("lid_team", momPlayer.lineup_position);
    params.append("time",timelineEventTime);
    params.append("overtime",this.state.overtime);
   
    var request = {
      params: params
    };
     this.setState({ 
             timelineLoaded: false,
             loaderVisible:true
           });
    
    
    axios.get(addMomUrl, request).then(res => {
        this.setState({ 
                loaderVisible:false,
                 closeMatchAlertVisible: true
          });
           setTimeout(() => {
                        this.setState({ closeMatchAlertVisible: false });
                      }, 2000);
   });

    //this.loadTimeline();

    this.setState({ 
         timelineLoaded: true,
        PlayerSelectorVisible: false
    });
    
    
                      

                    
    

    //reload timeline
  };

  handleTimerUpdate(time)
  {
    this.time = time;
  }
  
  handleTimelineUpdate(timeline)
  {
      this.setState({
          firstLineGoals:timeline.match_result.first_line,
          secondLineGoals:timeline.match_result.second_line,
      });
  }

    render() {
        
        
        
        var homeTeamStyle = {
           backgroundImage: "url(" + this.state.lineup.teamImage + ")"
          };
        var opponentTeamStyle = {
           backgroundImage: "url(" + this.state.lineup.teamOpponentImage + ")"
          };
        
        var homeTeamImg = '';
        var opponentTeamImg = '';
        
        
        if(this.state.lineup.teamImage != '' && '/img/team/users.svg' != this.state.lineup.teamImage)
        {
            homeTeamImg = <div className="teamLogo"  style={homeTeamStyle} ></div>
        }
        
        var opponentTeamImg = '';
        if(this.state.lineup.teamOpponentImage != '' && '/img/team/users.svg' != this.state.lineup.teamOpponentImage)
        {
            opponentTeamImg =  <div className="teamLogo"  style={opponentTeamStyle} ></div>
        }
        
        //console.log(this.state.event.playground);
        return (
          <div className="App">
             {this.state.closeMatchAlertVisible ?
                <div className="closeMatchAlert">{Trans.closeMatchAlert}</div>
                : ''
               }
                   
             {this.state.loaderVisible ?
             <div className="loader"><img src="/img/loader.png" /></div>
                : ''
               }
  
           
  
  
            {this.state.lineupLoaded && this.state.PlayerSelectorVisible ?  
            <div className="full-window">
              <PlayerSelector 
                baseUrl={this.baseUrl} 
                onClose={this.handleClosePlayerChoice} 
                onItemSelect={this.handleAddScoreEvent} 
                onMomSelect={this.handleAddMatchEndEvent} 
                lineup={this.state.lineup} 
                currentLine={this.state.currentLine} 
                seconds={this.state.seconds}
                time={this.time}
                 lang={this.props.lang}
                />
            </div>    : null}


           {this.state.eventLoaded && this.state.lineupLoaded  ?  
           <div className={"container-fluid" + (this.state.PlayerSelectorVisible ? ' visiblePlayerSelector' : '')}>
           <NavBar lang={this.props.lang} fastScoreUrl={this.state.fastScoreUrl} title="Live score" action="close" />

            <div className="row event-info">
                <div className="col-xs-12 event-date">{  this.state.event.date }</div>
                <div className="col-xs-12 event-name">{this.state.event.name}</div>
                <div className="col-xs-12 team-name">
                  {this.state.event.team_name} 
                  {this.state.event.playground != null && this.state.event.playground.name != null ? '('+this.state.event.playground.name+')' : ''}
                 </div>
             
             
            </div>

            <div className="row">
              <div className="col-xs-12 match-points"><span id="firstLinePoints">{this.state.firstLineGoals}</span>:<span id="secondLinePoints">{this.state.secondLineGoals}</span></div>
            </div>

          <div className="row">
              <div className="col-xs-6 addPoints-wrap">
                <a href="#" className="addPoints addPointsFl" onClick={(e) => this.handleShowPlayerChoices(e,'first_line')}>{homeTeamImg}</a>
                {this.state.lineup.firstLineName}
              </div>

               <div className="col-xs-6 addPoints-wrap">
                <a href="#" className="addPoints addPointsSl" onClick={(e) => this.handleShowPlayerChoices(e,'second_line')}>{opponentTeamImg}</a>
                 {this.state.lineup.secondLineName}
              </div>
            </div>
             {this.state.lineupLoaded  &&  this.state.lineupStatus != 'closed' ? 
             <Timer  lang={this.props.lang}  apiUrl={this.apiUrl} lineup={this.state.lineup} uid={this.state.uid} seconds={this.state.seconds} status={this.state.lineupStatus} onUpdateTime={this.handleTimerUpdate} onMatchEnd={this.handleAddMatchEnd} /> 
             : null }
             
        
        { this.state.timelineLoaded && this.state.lineupLoaded  ?
            <Timeline 
             lang={this.props.lang}
                onItemDelete={this.handleTimelineItemDelete}  
                lineup={this.state.lineup} 
                onUpdateTimeline={this.handleTimelineUpdate} 
                uid={this.state.uid}
                eventId={this.state.eventId}
                eventCurrentDate={this.state.eventCurrentDate}
                />
                : null}
          </div>
           : null}
          </div>
        );
    }
}

export default App;