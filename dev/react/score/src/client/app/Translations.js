import React, { Component } from 'react';

class Translations
{
    constructor()
    {
        this.lang = 'sk';
        this.translations = {
            navbar_Goal: {
                sk: 'Gól',
                en: 'Goal'
            },
            navbar_Assist: {
                sk: 'Asistencia',
                en: 'Assist'
            },
            no_assist: {
                sk: 'Bez asistencie',
                en: 'No assist'
            },
            loading_text: {
                sk: 'Odosielam ...',
                en: 'Sending ...'
            },
            game_over: {
                sk: 'Koniec zápasu',
                en: 'Game Over'
            },
            fastScore: {
                sk: 'Rýchly zápis',
                en: 'Quick Score'
            },
            finishGame: {
                sk: 'Ukončiť zápas',
                en: 'Finish game'
            },
            overtime: {
                sk: 'v predlžení',
                en: 'in overtime'
            },
            closeMatchAlert: {
                sk: 'Zápas bol ukončený',
                en: 'The match has been closed'
            },

        }
    }

    getTranslations(lang) {
        var keys = Object.keys(this.translations);
        var allTrans = this.translations;
        var translations = {};
        //var lang = lang;
        keys.forEach(function (k) {
            translations[k] = allTrans[k][lang];
        });

        return translations;
    }
}

export  default Translations = new Translations();
