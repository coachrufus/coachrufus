<html>
  <head>
    <meta charset="utf-8">
    <title>Lineup</title>
     <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,700|Roboto:400,700&amp;subset=cyrillic" rel="stylesheet">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link media="all" rel="stylesheet" href="/dev/theme/bootstrap/css/bootstrap.grid.css" />
     <link media="all" rel="stylesheet" href="public/styles/index.css?ver=<?php echo time() ?>" />
     <link media="all" rel="stylesheet" href="public/styles/playerSelector.css?ver=<?php echo time() ?>" />
     
  </head>
  <body>
    
     

      <div data-token="a0d391a307c8cdadb55f580594ab71e7d023ee7b#ee55ff59d370d94eeb597574b8bde8e2eca1a87c" id="lineup" data-event-id="194" data-event-current-date="2018-05-08" data-api-provider="local" data-uid="3" data-lang="sk"></div>


    <script src="public/bundle.js?ver=<?php echo rand(1,100) ?>" type="text/javascript"></script>
  </body>
</html>