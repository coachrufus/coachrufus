import React, { Component } from 'react';

class Translations
{
    constructor()
    {
        this.lang = 'sk';
        this.translations = {
    title: {
        sk: 'Súpiska',
        en: 'Lineup'
    },
    tab_random: {
        sk: 'Náhodne',
        en: 'Random'
    },
    tab_power: {
        sk: 'Podľa formy',
        en: 'Power'
    },
    tab_manual: {
        sk: 'Manuálne',
        en: 'Manual'
    },
       
    white_side: {
        sk: 'Svetlí',
        en: 'White side'
    },
    black_side: {
        sk: 'Tmaví',
        en: 'Black side'
    },
    power: {
        sk: 'Výkon',
        en: 'Power'
    },
    seconds: {
        sk: 'sekúnd od teraz',
        en: 'seconds from now'
    },
    minutes: {
        sk: 'minúty',
        en: 'minutes'
    },
    hour: {
        sk: 'hodina',
        en: 'hour'
    },
    hours: {
        sk: 'hodín',
        en: 'hours'
    },
    from_now: {
        sk: 'od teraz',
        en: 'from now'
    },
    ago: {
        sk: 'dozadu',
        en: 'ago'
    },
    no_players: {
        sk: 'Pridali ste všetkých hráčov alebo nemáte prihlásených žiadných hráčov na zápas',
        en: 'No available players, you have added all players or there are no players in event attendance'
    },
    check_attendance: {
        sk: 'Skontrolovať účasť',
        en: 'Check attendance'
    },
    add_score: {
        sk: 'Zadaj skóre',
        en: 'Add Score'
    },
       
}
    }
    
   getTranslations(lang) {
        var keys = Object.keys(this.translations);
        var allTrans = this.translations;
        var translations = {};
        //var lang = lang;
        keys.forEach(function (k) {
            translations[k] = allTrans[k][lang];
        });

        return translations;
}
}

export  default Translations = new Translations();

