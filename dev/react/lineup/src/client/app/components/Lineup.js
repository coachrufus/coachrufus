import React, { Component } from 'react';
import axios from 'axios';
const Config = require('./../Config.jsx');

class Lineup extends Component {

    constructor(props) {
        super(props);

        this.state = {
            line: [],
            lineupPosition: this.props.lineupPosition,
            lineupId: null,
            missingPlayers: []
        };
        this.handleMoveRow = this.handleMoveRow.bind(this);
        this.showRowToolbar = this.showRowToolbar.bind(this);
        this.hideRowToolbar = this.hideRowToolbar.bind(this);
        this.handleDeleteRow = this.handleDeleteRow.bind(this);
        this.baseUrl = this.props.baseUrl;
    }
    
    buildLine(lineup)
    {
        var lineupPosition = this.state.lineupPosition;
        var line = [];
        if ('firstLine' == lineupPosition) {
            var currentLine = lineup.firstLine;
        }

        if ('secondLine' == lineupPosition) {
            var currentLine = lineup.secondLine;
        }

        if (null != currentLine) {
            var eventsKeys = Object.keys(currentLine);
            var i = 0;
            eventsKeys.forEach(function (k) {

                //currentLine[k].player.player_name = currentLine[k].player.player_name.replace(" ","\n");
                //console.log(currentLine[k].player);
                var playerNameParts = currentLine[k].player.player_name.split(' ');
                currentLine[k].player.first_name = playerNameParts[0];
                currentLine[k].player.last_name = playerNameParts[1];
                line[i++] = {
                    'player': currentLine[k].player,
                    'teamPlayer': currentLine[k].teamPlayer,
                    'actionRowStatus': 'hidden',
                    'efficiency': currentLine[k].efficiency,
                    'key': k
                };
            });
        }
        var  missingPlayers = [];
        var a = line.length+1;
        var missingPlayerKey = 0;
        for(a; a < this.props.max; a++)
        {
             missingPlayers[missingPlayerKey++] = {
                    'player': {'first_name':'Missing','last_name':' player'},
                    'teamPlayer': {},
                    'actionRowStatus': 'hidden',
                    'efficiency': null,
                    'key': a
                };
        }

        this.setState({
            line: line,
            missingPlayers: missingPlayers
        });
    }

    componentDidMount(){
        this.buildLine(this.props.lineup);
    }

    componentWillReceiveProps({ lineup }) {
        this.buildLine(lineup);
    }



    handleMoveRow(e, key) {
        e.preventDefault();
        lineupPosition: this.props.lineupPosition,
        this.props.onMovePlayer(e, this.state.line[key], this.state.lineupPosition);
    }

    handleDeleteRow(e, key) {
        e.preventDefault();
        lineupPosition: this.props.lineupPosition,
        this.props.onDeletePlayer(e, this.state.line[key], this.state.lineupPosition);
    }

    showRowToolbar(e, key) {
        var line = this.state.line;
        line[key].actionRowStatus = 'visible';

        this.setState({
            line: line,
        });
    }

    hideRowToolbar(e, key) {
        var line = this.state.line;
        line[key].actionRowStatus = 'hidden';
        this.setState({
            line: line,
        });
    }

    renderPlayerRowInfo(row,key){
        if('firstLine' == this.state.lineupPosition)
        {
            return (
                <div className="playerRowInfo" onClick={(e) => this.showRowToolbar(e, key)}>
                                    <div className="playerAvatar"><img src={Config.BASE_URL+row.player.avatar} /></div>
                                    <div className="playerName">
                                    {row.player.first_name}<br />{row.player.last_name}({row.efficiency})
                                                <div className="playerPosition">{row.player.player_position}</div>
                                    </div>
                                    
                                    </div>
            );
        }

        if('secondLine' == this.state.lineupPosition)
        {
            return (
                <div className="playerRowInfo" onClick={(e) => this.showRowToolbar(e, key)}>
                                    
                                    <div className="playerName">
                                    {row.player.first_name}<br />{row.player.last_name}({row.efficiency})
                                            <div className="playerPosition">{row.player.player_position}</div>
                                            </div>
                                    <div className="playerAvatar"><img src={Config.BASE_URL+row.player.avatar} /></div>
                                    </div>
            );
        }
    }

    missingPlayerRowInfo(row,key){
        if('firstLine' == this.state.lineupPosition)
        {
            return (
                <div className="playerRowInfo">
                    <div className="playerAvatar"></div>
                    <div className="playerName">{row.player.first_name}<br />{row.player.last_name}</div>
                </div>
            );
        }

        if('secondLine' == this.state.lineupPosition)
        {
            return (
                <div className="playerRowInfo">            
                    <div className="playerName">{row.player.first_name}<br />{row.player.last_name}</div>
                    <div className="playerAvatar"></div>
                </div>
            );
        }
    }
    
   
    render() {
        return (
            <div className="player-list">
                {this.state.line.map((row, key) => {
                    return (
                        <div className="playerRow" key={'fl' + key}     >

                            {row.actionRowStatus == 'visible'
                                ?
                                <div className="playerRowActions">
                                <a className="closeActions" href="#" onClick={(e) => this.hideRowToolbar(e, key)} ></a>
                                    <a className="deleteRow" href="#" onClick={(e) => this.handleDeleteRow(e, key)}></a>
                                    <a className="moveRow" href="#" onClick={(e) => this.handleMoveRow(e, key)}></a>
                                </div>
                                :
                                this.renderPlayerRowInfo(row,key) 
                            }

                        </div>);
                }
                )}

                 {this.state.missingPlayers.map((row, key) => {
                    return (
                        <div className="playerRow missingPlayerRow" key={'fl' + key}     >

                            {this.missingPlayerRowInfo(row,key) }

                        </div>);
                }
                )}
            </div>
        );
    }
};




export default Lineup;
