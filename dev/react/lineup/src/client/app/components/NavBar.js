import React, { Component } from 'react';

class NavBar extends Component {


    constructor(props) {
        super(props);

        this.messageOrigin;
        this.messageSource;

        this.componentDidMount = this.componentDidMount.bind(this); 
        this.initHandler = this.initHandler.bind(this); 
        this.sendMessage = this.sendMessage.bind(this);
    };

    initHandler(e)
    {
        this.messageOrigin = e.origin;
        this.messageSource = e.source;
    }

    componentDidMount() {
        window.addEventListener('message', this.initHandler);
    }

    sendMessage() {
        var message = {
            'action': this.props.action
        };

        window.postMessage(JSON.stringify(message) ,'*');
    };

    render() {
        return (
            <div className="nav-topbar">
                <div className="navBtn" onClick={(e) => {this.sendMessage()}}></div>
               {this.props.title}
            </div>

        );
    };
};


export default NavBar;
