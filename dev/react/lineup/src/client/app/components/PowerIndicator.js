import React, { Component } from 'react';
const Config = require('./../Config.jsx');
import Translations from  './../Translations.js';
var Trans;
class PowerIndicator extends Component {

    constructor(props) {
        super(props);
Trans = Translations.getTranslations(this.props.lang);
        this.state = {
            firstLinePower: 0,
            secondLinePower: 0,
            firstLineRelativePower:0,
            secondLineRelativePower:0
        };
        
        this.handleShowPlayerChoices = this.handleShowPlayerChoices.bind(this);
    }

    calculateLinuepPower(currentLine) {
        var lineupPower = 0;
        if(null != currentLine)
        {
            var lineupKeys = Object.keys(currentLine);
            lineupKeys.forEach(function (k) {
                lineupPower += currentLine[k].efficiency;
            });
        }
        

        return Math.round(lineupPower * 100) / 100 ;
        
    }

    componentWillReceiveProps({ lineup }) {
        var firstLinePower = this.calculateLinuepPower(lineup.firstLine);
        var secondLinePower = this.calculateLinuepPower(lineup.secondLine);
        var firstLineRelativePower = 100;
        var secondLineRelativePower = 100;
        if(firstLinePower > secondLinePower)
        {
            secondLineRelativePower  = Math.round(secondLinePower/(firstLinePower/100) * 100) / 100 ;
        }

        if(firstLinePower < secondLinePower)
        {
            firstLineRelativePower  = Math.round(firstLinePower/(secondLinePower/100) * 100) / 100
        }

        this.setState({
            firstLinePower: firstLinePower,
            secondLinePower: secondLinePower,
            firstLineRelativePower:firstLineRelativePower,
            secondLineRelativePower:secondLineRelativePower
        });
    }
    
     handleShowPlayerChoices(e,targetLine){
        e.preventDefault();
        //console.log( this.props.onAddPlayer);
        
        
        this.props.onAddPlayer(e,targetLine);
    };

    
    //onAddPlayer

    render() {
        let firstLineClass = 'strong';
        let secondLineClass = 'strong';
        if(this.state.firstLineRelativePower > this.state.secondLineRelativePower)
        {
            secondLineClass = 'weak';
        }
        if(this.state.firstLineRelativePower < this.state.secondLineRelativePower)
        {
            firstLineClass = 'weak';
        }

        return (
            <div className="row lineup-head">
                <a href="#" className={'col-xs-6 first-line '+firstLineClass} onClick={(e) => {this.handleShowPlayerChoices(e,'firstLine');}}>
                
                     <button  className="player-choice-trigger">+</button>
                
                    <strong>{Trans.white_side}</strong>
                    <span className="power">{Trans.power} {this.state.firstLinePower} <span className="power-perc">({this.state.firstLineRelativePower})%</span></span>
                </a>
                <a href="#" className={'col-xs-6 second-line '+secondLineClass} onClick={(e) => {this.handleShowPlayerChoices(e,'secondLine');}}>
                <button   className="player-choice-trigger">+</button>
                    <strong>{Trans.black_side}</strong>
                    <span className="power">{Trans.power} {this.state.secondLinePower} <span className="power-perc">({this.state.secondLineRelativePower})%</span></span>
                </a>
            </div>
        );
    }
};


export default PowerIndicator;
