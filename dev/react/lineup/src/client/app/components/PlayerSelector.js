import React, { Component } from 'react';
import axios from 'axios';
const Config = require('./../Config.jsx');
import Translations from  './../Translations.js';
var Trans;
class PlayerSelector extends Component {


    constructor(props) {
        super(props);
         Trans = Translations.getTranslations(this.props.lang);
/*
         var choicesKeys = Object.keys(this.props.choices);
         choicesKeys.forEach(function (k) {
            console.log(choicesKeys[k].);
        });
    */
        //remove already added players
         //var lineupKeys = Object.keys(this.props.currentLineup.firstLine);
         //console.log(lineupKeys);
        var existTeamPlayerIds = [];
        var currentLineup = this.props.currentLineup;

        var lineupKeys = Object.keys(currentLineup.firstLine);
        lineupKeys.forEach(function (k) {
            existTeamPlayerIds.push(currentLineup.firstLine[k].teamPlayer.id);
        });

        var lineupKeys = Object.keys(currentLineup.secondLine);
        lineupKeys.forEach(function (k) {
            existTeamPlayerIds.push(currentLineup.secondLine[k].teamPlayer.id);
        });

         var choices = this.props.choices;
            console.log(existTeamPlayerIds);
            console.log(choices);
            
            var selectorChoices = [];
            
         choices.forEach(function (choice,k) {
            var res = existTeamPlayerIds.indexOf(choice.id);

            if(res < 0)
            {
                //delete choices[k];
                //choices.splice(k, 1);
                 //console.log(choices);
                 selectorChoices.push(choices[k]);
            }
   
        });

        this.state = {
            players: selectorChoices,
            playersLoaded:true
        };
        this.handlePlayerSelect = this.handlePlayerSelect.bind(this); 
        this.handleClose = this.handleClose.bind(this);
        
        this.attendanceUrl = Config.BASE_URL+'/event/'+this.props.eventId+'/'+this.props.eventCurrentDate+'?lt=mal'


    }


    handlePlayerSelect(e,playerKey){
        e.preventDefault();
        this.props.onItemSelect(e, this.state.players[playerKey]);
    };

     handleClose(e){
        e.preventDefault();
        this.props.onClose(e);
    };

   

    render() {
         console.log(this.state.players.length);
        return (
               
            <div className="row playerSelector">
             <div className="playerSelector-topbar">
                <div className="navBtn"  onClick={(e) => {this.handleClose(e)}} ></div>
                {Trans.title}
            </div>
            
            
    
            {this.state.playersLoaded && this.state.players.length > 0 ?  
                this.state.players.map((player, key) => {
                     return (
                      <div className="col-xs-12  playerSelector-item" key={'line'+key}  onClick={(e) => {this.handlePlayerSelect(e,key)}}  >
                        <div className="playerNumber">{player.player_number}</div> 
                      <div className="avatar"><img src={Config.BASE_URL+player.avatar} /></div>
                       <div className="playerName">{player.first_name} {player.last_name} ({player.efficiency})<span className="player-position">{player.player_position}</span></div>
                     
                    </div> )}
                )
                : 
                <div className="col-xs-12">
                <div className="no-players-alert">
                {Trans.no_players}
                    
                    <br />
                    <a href={this.attendanceUrl}><strong>{Trans.check_attendance}</strong></a>
                </div>
                </div>
            }
            </div>

        );
    }
}
;


export default PlayerSelector;
