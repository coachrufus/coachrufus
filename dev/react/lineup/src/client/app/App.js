import React, { Component} from "react";
import axios from 'axios';
import humanDate from 'human-date';
import PlayerSelector from  './components/PlayerSelector.js';
import Lineup from  './components/Lineup.js';
import PowerIndicator from "./components/PowerIndicator.js";
import NavBar from  './components/NavBar.js';

import Translations from  './Translations.js';
var Trans;
const Config = require('./Config.jsx');
class App extends Component{

  constructor(props) {
    super(props);
 Trans = Translations.getTranslations(this.props.lang);
    this.state = {
      lineup: {firstLine:{},secondLine:{}},
      lineupLoaded: false,
      PlayerSelectorVisible: false,
      PlayerSelectorLoaded:false,
      event: {},
      eventLoaded:false,
      selectedLine:null,
      playerChoices: [],
      currentLineupType: 'random',
      lineupId:null,
      eventId:this.props.eventId,
      eventCurrentDate:this.props.eventCurrentDate,
      apiKey:'app@40101499408386491',
      uid:this.props.uid,
      scoreUrl:''
    };
    this.messageOrigin;
    this.messageSource;
   this.componentDidMount = this.componentDidMount.bind(this); 
    this.handleAddPlayer = this.handleAddPlayer.bind(this);
    this.createLineup = this.createLineup.bind(this);
    this.handleMoveRow = this.handleMoveRow.bind(this);
    this.handleDeleteRow = this.handleDeleteRow.bind(this);
    this.handleCreateLineup = this.handleCreateLineup.bind(this);
    this.handleClosePlayerChoice = this.handleClosePlayerChoice.bind(this);
    this.handleShowPlayerChoices = this.handleShowPlayerChoices.bind(this);
    this.handleScoreRedirect = this.handleScoreRedirect.bind(this);
    this.handlePrint = this.handlePrint.bind(this);
    
    
  }
  
    initHandler(e)
    {
        this.messageOrigin = e.origin;
        this.messageSource = e.source;
    }

 createLineup(type){
    const createLineupUrl = Config.API_URL+'/content/game/create-lineup';
    axios.post(createLineupUrl,'apikey='+Config.API_KEY+'&user_id='+this.state.uid+'&event_id='+this.state.eventId+'&event_current_date='+this.state.eventCurrentDate+'&type='+type)
            .then(res => {
              this.setState({
                  currentLineupType:type
              });
            
              this.loadLineup(); 
            });
        ;
  }

  handleCreateLineup(type){
        this.createLineup(type);
  }
 
  loadEventDetail()
  {
    //http://app.coachrufus.lamp/api/event/detail?apikey=app@40101499408386491&uid=3&id=1046&date=2018-05-08
     const eventDataUrl = Config.API_URL+'/event/detail?apikey='+Config.API_KEY+'&uid='+this.state.uid+'&id='+this.state.eventId+'&date='+this.state.eventCurrentDate;
    axios.get(eventDataUrl)
      .then(res => {
        this.setState({
            event: res.data,
            eventLoaded: true
          });
      });
  }
 
 
  loadLineup()
  {
    const lineupDataUrl = Config.API_URL+'/content/game/lineup?apikey='+Config.API_KEY+'&user_id='+this.state.uid+'&event_id='+this.state.eventId+'&event_current_date='+this.state.eventCurrentDate;
    var lineupId = null;
    var lineup = null;
    axios.get(lineupDataUrl)
      .then(res => {
        if (res.data.id === null)
        {
          this.createLineup('random');
        }
        else {
          lineupId: res.data.id;
          lineup: res.data;
          
          this.setState({
            lineupId:  res.data.id,
            lineup: res.data,
            lineupLoaded:true,
            scoreUrl:res.data.scoreUrl
          });
          
        }
      });
  }

  componentDidMount() {
     window.addEventListener('message', this.initHandler);
        this.loadLineup();
    //load event
    this.loadEventDetail();

    //load players
    var playerChoices = [];
    const dataUrl = Config.API_URL+'/content/event-attendance?apikey='+Config.API_KEY+'&user_id='+this.state.uid+'&event_id='+this.state.eventId+'&event_current_date='+this.state.eventCurrentDate;
      axios.get(dataUrl)
          .then(res => {
            const players = [];
            res.data.forEach((item) => {
                playerChoices.push(item);
            });

             this.setState({
                playerChoices: playerChoices,
                PlayerSelectorLoaded: true
             });
          });
  }
  
  handleShowPlayerChoices(e,targetLine){
   
    e.preventDefault();
    this.setState({ 
      PlayerSelectorVisible: true,
      selectedLine:targetLine
    });
     
  }
  
  handleClosePlayerChoice(e){
    
    e.preventDefault();
    this.setState({ 
      PlayerSelectorVisible: false
    });
    
  }
  
    handlePrint(e) {
        e.preventDefault();
        setTimeout(() => {

            var message = {
                'action': 'popup-full',
                'screen': 'events',
                'href': Config.BASE_URL + '/team-match/print-lineup?apikey=' + Config.API_KEY + '&user_id=' + this.props.uid + '&id=' + this.state.lineup.id,
            };
            window.postMessage(JSON.stringify(message), '*');
        }, 200);
    }

  handleAddPlayer(e,player){

        
    var lineup_id =  this.state.lineupId;
    if('firstLine' == this.state.selectedLine)
    {
        var target_line_code  = 'first_line';
        var targetLine = this.state.lineup.firstLine; 
    }
    if('secondLine' == this.state.selectedLine)
    {
        var target_line_code  = 'second_line';
         var targetLine= this.state.lineup.secondLine;
    }
    const addPlayerUrl = encodeURI(Config.BASE_URL+'/team-match/add-lineup-player?player_id='+player.player_id+'&target_line='+target_line_code+'&lineup_id='+lineup_id+'&player_name='+player.first_name+' '+player.last_name+'&team_player_id='+player.id);
   
    axios.get(addPlayerUrl)
        .then(res => {
            targetLine[player.id] = {
              'teamPlayer':player,
              'player':{'id':res.data.id,'player_name':player.first_name+' '+player.last_name,'avatar':player.avatar},
              'efficiency':player.efficiency
            };
            var lineup = this.state.lineup;
           
            if('firstLine' == this.state.selectedLine)
            {
                lineup.firstLine = targetLine;
            }
            if('secondLine' == this.state.selectedLine)
            {
                lineup.secondLine = targetLine;
            }

            //rebuild player choices
            var playerChoices = this.state.playerChoices;
            playerChoices.forEach(function (playerChoice,k) {
               if(player.id == playerChoice.id)
               {
                delete playerChoices[k];
               }
            });

             this.setState({ 
              lineup: lineup, 
              playerChoices:playerChoices,
              PlayerSelectorVisible: false
              
            });
        });
  }

  handleMoveRow(e,rowData,currentPosition) {
    if('firstLine' == currentPosition)
    {
      var sourceLineup = this.state.lineup.firstLine;
      var targetLineup = this.state.lineup.secondLine;
      var targetLineupCode = 'second_line';
    }

    if('secondLine' == currentPosition)
    {
      var sourceLineup = this.state.lineup.secondLine;
      var targetLineup = this.state.lineup.firstLine;
      var targetLineupCode = 'first_line';
    }
    
    var lineKeys = Object.keys(sourceLineup);
    lineKeys.forEach(function (k) {
       if(k == rowData.key)
       {
        targetLineup[k] = {'teamPlayer':rowData.teamPlayer,'player':rowData.player,'efficiency':rowData.efficiency};
        delete sourceLineup[k];
       }
    });
    var newLineup = this.state.lineup;
    if('firstLine' == currentPosition)
    {
      newLineup.firstLine = sourceLineup ;
      newLineup.secondLine = targetLineup ;
    }

    if('secondLine' == currentPosition)
    {
      newLineup.secondLine = sourceLineup ;
      newLineup.firstLine = targetLineup ;
    }

    const changePlayerUrl = encodeURI(Config.BASE_URL+'/team-match/change-lineup-player?item_id='+rowData.player.id+'&lineup='+targetLineupCode);
    axios.get(changePlayerUrl);
    this.setState({
      lineup:newLineup
    });
 }

 handleDeleteRow(e,rowData,currentPosition) {
    if('firstLine' == currentPosition)
    {
      var sourceLineup = this.state.lineup.firstLine;
    }

    if('secondLine' == currentPosition)
    {
      var sourceLineup = this.state.lineup.secondLine;
    }
    var lineKeys = Object.keys(sourceLineup);
    
    //console.log(lineKeys);
    //console.log(rowData);
    //var itemId;
    var deletedItem;
    lineKeys.forEach(function (k) {
       if(k == rowData.key)
       {
        deletedItem = sourceLineup[k].player;
        //itemId = sourceLineup[k].player.id;
        delete sourceLineup[k];
        //sourceLineup.splice(k, 1);
       }
    });

    var newLineup = this.state.lineup;

    if('firstLine' == currentPosition)
    {
      newLineup.firstLine = sourceLineup ;
    }

    if('secondLine' == currentPosition)
    {
      newLineup.secondLine = sourceLineup ;
    }

    const removePlayerUrl = encodeURI(Config.BASE_URL + '/team-match/remove-lineup-player?item_id=' + deletedItem.id);
    axios.get(removePlayerUrl);

    //rebuild player choices
    var playerChoices = this.state.playerChoices;
    //deletedItem['id'] = rowData.key;
    //deletedItem['player_id'] = rowData.teamPlayer.
    //var newPlayerChoice = rowData.teamPlayer;
    //rowData.teamPlayer.
    //playerChoices.push(rowData.teamPlayer); 

    this.setState({
      lineup:newLineup,
      //playerChoices: playerChoices
    });
}

    handleScoreRedirect(e){
        e.preventDefault();
        
        location.href=this.state.scoreUrl;
        
        
        /*
        var scoreWindowOpened = true;
        if(scoreWindowOpened)
        {
            //close current and refresh score
             var message = {
                'action': 'close'
            };

            window.postMessage(JSON.stringify(message) ,'*');
            
        }
        else
        {
            location.href=this.state.scoreUrl;
        }
        */
    }

  render(){
    var randomButtonClassName = this.state.currentLineupType == 'random' ? 'active' : '';
    var efficiencyButtonClassName = this.state.currentLineupType == 'efficiency' ? 'active' : '';
    var manualButtonClassName = this.state.currentLineupType == 'manual' ? 'active' : '';

    var playersCount = Object.keys(this.state.lineup.firstLine).length + Object.keys(this.state.lineup.secondLine).length;

    if(this.state.eventLoaded)
    {
      var firstLineMax = Math.ceil(this.state.event.capacity/2);
      var secondLineMax = this.state.event.capacity - firstLineMax;
    }
   
    var dateDiff = this.state.event.termin - (new Date().getTime() / 1000);
    var dateFormated =  humanDate.relativeTime(dateDiff);
    dateFormated = dateFormated.replace('seconds',Trans.seconds);
    dateFormated = dateFormated.replace('minutes',Trans.minutes);
    dateFormated = dateFormated.replace('hours',Trans.hours);
    dateFormated = dateFormated.replace('hour',Trans.hour);
    dateFormated = dateFormated.replace('from now',Trans.from_now);
    dateFormated = dateFormated.replace('ago',Trans.ago);
    
    
    
    if(Math.abs(dateDiff) > (24*60*60))
    {
        var eventDate = new Date();
        eventDate.setTime(this.state.event.termin * 1000);

                
        var month = ('0'+(eventDate.getMonth()+1)).slice(-2);
        var day = ('0'+eventDate.getDate()).slice(-2);
        var minutes = ('0'+eventDate.getMinutes()).slice(-2);
        
        var dateFormated =  day+'.'+month+'.'+eventDate.getFullYear()+' o '+eventDate.getHours()+':'+minutes;
    }
    

   

    return(
      <div className="App">
          
         {this.state.PlayerSelectorVisible && this.state.PlayerSelectorLoaded && this.state.lineupLoaded  ?  
        <div className="full-window">
          <PlayerSelector lang={this.props.lang} baseUrl={Config.BASE_URL} onItemSelect={this.handleAddPlayer} onClose={this.handleClosePlayerChoice} choices={this.state.playerChoices} currentLineup={this.state.lineup} eventId={this.props.eventId} eventCurrentDate={this.props.eventCurrentDate} />
        </div>   
        : null}
          
         {this.state.eventLoaded  && this.state.lineupLoaded  ?    
            <div className="container-fluid">
            <NavBar title={Trans.title} action="close" />
            
            
            <div className="header">
              <div className="row">
                <div className="col-xs-9 event-summary">
                  <div className="event-date">{ dateFormated }</div>
                  <div className="event-name">{this.state.event.name}</div>
                   <div className="attend-summary">Players {playersCount}/{this.state.event.capacity}</div>
                </div>
                <div className="col-xs-3 print-cell">
              
                </div>
              </div>
            
            </div>
            <div className="button-bar">
                <a className={randomButtonClassName} href="#" onClick={(e) => this.handleCreateLineup('random')}>{Trans.tab_random}</a>
                <a className={efficiencyButtonClassName} href="#" onClick={(e) => this.handleCreateLineup('efficiency')}>{Trans.tab_power}</a>
                <a className={manualButtonClassName} href="#" onClick={(e) => this.handleCreateLineup('manual')}>{Trans.tab_manual}</a>
              </div>
             <PowerIndicator lang={this.props.lang} lineup={this.state.lineup} onAddPlayer={this.handleShowPlayerChoices} />
             
             

              <div className="row lineup-players">
                <div className="col-xs-6 first-line">
                      <Lineup max={firstLineMax} baseUrl={this.baseUrl} lineupPosition="firstLine" lineup={this.state.lineup} onMovePlayer={this.handleMoveRow}  onDeletePlayer={this.handleDeleteRow}  />
                </div>
                <div className="col-xs-6 second-line">
                      <Lineup max={secondLineMax} baseUrl={this.baseUrl}  lineupPosition="secondLine" lineup={this.state.lineup} onMovePlayer={this.handleMoveRow} onDeletePlayer={this.handleDeleteRow} />
                </div>
              </div>

            {  Object.keys(this.state.lineup.firstLine).length > 0 && Object.keys(this.state.lineup.secondLine).length > 0 && this.state.lineup.scoreUrl != '' ?
              <a onClick={(e) => {this.handleScoreRedirect(e)}} className="score_btn" href={this.state.scoreUrl}>{Trans.add_score}</a> 
      : '' }
            </div>
       : null}
      </div>
    );
  }
};



export default App;