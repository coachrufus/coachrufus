import React from "react";
import ReactDOM from "react-dom";
import App from "./App.js";
var targetElement = document.getElementById('homewall');
var uid = targetElement.getAttribute("data-uid");
var action = targetElement.getAttribute("data-action");
var postId = targetElement.getAttribute("data-post-id");
var tab = targetElement.getAttribute("data-tab");
var lang = targetElement.getAttribute("data-lang");

ReactDOM.render(
  <App uid={uid} action={action} postId={postId} tab={tab}  lang={lang} />,
  document.getElementById("homewall")
);