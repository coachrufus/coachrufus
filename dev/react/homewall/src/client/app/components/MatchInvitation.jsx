import React, { Component } from 'react';
import Translations from  './../Translations.js';
var Trans;
import axios from 'axios';
const Config = require('./../Config.js');
class MatchInvitation extends Component {

    constructor(props) {
        super(props);
         Trans = Translations.getTranslations(this.props.lang);
        
        this.date = new Date(this.props.event.termin*1000);
        var month = ('0'+(this.date .getMonth()+1)).slice(-2);
        var day = ('0'+this.date.getDate()).slice(-2);
        var minutes = ('0'+this.date.getMinutes()).slice(-2);
        
        var dateFormated =  day+'.'+month+'.'+this.date .getFullYear()+' o '+this.date.getHours()+':'+minutes;
        this.text =  dateFormated+ ' ('+this.props.event.name+')';
        
        
        this.state = {
            loading: false,
            visible:true
        };

        this.handleClose = this.handleClose.bind(this);

    }
    
    handleAttendance(type)
    {
        
        this.setState({
            loading:true
        });
        
        var date = new Date(this.props.event.termin*1000);
        var month = ('0'+(date.getMonth()+1)).slice(-2);
        var day = ('0'+date.getDate()).slice(-2);
        var eventDateFormated = date.getFullYear() + '-' + month+'-'+day;
        const dataUrl = Config.BASE_URL + '/team/change-players-attendance?apikey=' + Config.API_KEY + '&event_id=' + this.props.event.id + '&event_date='+eventDateFormated+'&type='+type+'&team_id='+this.props.event.teamId;
        axios.get(dataUrl) .then(res => {
             this.props.onUpdate(this.props.index);
             
        });
        
    }
    
    handleClose()
    {
        this.setState({
            visible: false
        });
         this.props.onUpdate(this.props.index);
    }

    render() {
        
        if(this.props.visible == false || this.state.visible == false)
        {
            return null;
        }
        
        if(this.state.loading )
        {
             return (
                <div className="hwItem row hwItemMatchInvitation" >
                    
                    <div className="loader">
                    <strong>{Trans.sendingData}</strong>
                    <img src={Config.BASE_URL+'/img/load.gif'}/>
                            </div>
                </div>
            );
        }
        else
        {
             return (
                <div className="hwItem row hwItemMatchInvitation" >
        <div className="close_btn" onClick={this.handleClose}></div>
                    <div className="hwItemTop">
                        <div className="avatar"><img src={this.props.user.icon} /></div>
                        <div className="cnt">
                            <h2>{this.props.user.name} {Trans.inv_Text}</h2>
                            <span className="date">{this.text}, {this.props.event.teamName}</span>
                        </div>
                    </div>
                
                    <div className="hwItemBottom invItemBottom row">
                        <div className="col-xs-6 btn btn-lft confirm_btn" onClick={() => {this.handleAttendance('in')}}>
                            {Trans.inv_Yes}
                        </div>
                        <div className="col-xs-6 btn decline_btn" onClick={() => {this.handleAttendance('out')}}>
                              {Trans.inv_No}
                        </div>
                        
                    </div>
                </div>

                );
        }

       
    }
}
;


export default MatchInvitation;
