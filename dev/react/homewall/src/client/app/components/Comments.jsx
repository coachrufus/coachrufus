import React, { Component } from 'react';
import humanDate from 'human-date';
import ContentEditable from 'react-contenteditable'
import Translations from  './../Translations.js';
var Trans;
import axios from 'axios';
const Config = require('./../Config.js');
class Comments extends Component {

    constructor(props) {
        super(props);
                Trans = Translations.getTranslations(this.props.lang);


        this.state = {
            visible: false,
            commentValue: '',
            comments:[],
            commentsLoaded:false,
            commentsSendLoader:false,
            post:null,
            image: null
        };
        
        this.messageOrigin;
        this.messageSource;

        this.handleCommentChange = this.handleCommentChange.bind(this);
        this.handleSendComment = this.handleSendComment.bind(this);
        this.handleFiles = this.handleFiles.bind(this);
        this.closeWindow = this.closeWindow.bind(this);
        
        this.componentDidMount = this.componentDidMount.bind(this);
        this.initHandler = this.initHandler.bind(this);
    }
    
     initHandler(e)
    {
        this.messageOrigin = e.origin;
        this.messageSource = e.source;
    }
    
    componentDidMount(){
      window.addEventListener('message', this.initHandler); 
      this.loadPost();
      this.loadPostComments();
    }
    
    loadPost()
    {
        const dataUrl = Config.API_URL + '/hpwall/post/'+this.props.postId+'?apikey=' + Config.API_KEY + '&user_id=' + this.props.user.id;
        axios.get(dataUrl)
                .then(res => {
                   var post =  res.data.result[0];
           
                    var safariDate = post.published_at.replace('-','/').replace('-','/');
                    var date = new Date(safariDate);

                   var today = new Date();
                   var timeDiff = Math.abs(today.getTime() - date.getTime());
                   var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 

                   if(diffDays < 8)
                   {
                       post.date = humanDate.relativeTime(safariDate) ;
                   }
                   else
                   {
                        post.date = date.toLocaleDateString("sk-SK");
                   }

                   if(post.type == 'personal')
                   {
                        post.avatar = post.user_photo;
                        post.authorName = post.user_name+' '+post.user_surname;
                   }

                   if(post.type == 'team' || post.type == 'team_after_match')
                   {
                        post.avatar = post.team_photo;
                        post.authorName = post.team_name;
                   }
                    
                    this.setState({
                            post: post,
                            visible:true
                        });
                });
    }
    

    
     loadPostComments()
    {
       
        const dataUrl = Config.API_URL + '/hpwall/postComment/'+this.props.postId+'?apikey=' + Config.API_KEY + '&user_id=' + this.props.user.id+'&status=visible';
        axios.get(dataUrl)
                .then(res => {
                    if (res.data.result.length == 0)
                    {
                         this.setState({
                            commentsLoaded:true,
                             commentsSendLoader: false,
                            commentValue: ''
                        });    
                    } 
                    else
                    {
                        this.setState({
                            comments: res.data.result,
                            commentsLoaded:true,
                             commentsSendLoader: false,
                            commentValue: ''
                        });
                    }
                });
    }
    

    handleCommentChange(event) {
        this.setState({commentValue: event.target.value});
    }
    
    closeWindow() {
         setTimeout(() => {
                       var message = {
                               'action': 'close',
                                'screen': 'home'
                            };
                        window.postMessage(JSON.stringify(message), '*');

                      }, 200);
    }
    
    handleFiles(event){
        const file = event.target.files[0];
        let formData = new FormData();
        formData.append('file', file);
        formData.append('pid', this.props.postId);
        const dataUrl = Config.API_URL + '/hpwall/upload';
        //var c = this;
        axios({
            method: 'post',
            url: dataUrl,
            data: formData,
            config: { headers: {'Content-Type': 'multipart/form-data' }}
            })
            .then(res =>  {
                this.setState({
                    image:Config.BASE_URL+res.data.file
                });
            })
            .catch(function (response) {
                console.log(response);
            });
      }

    handleSendComment(event) {
        event.preventDefault();

        var createdAt = new Date();
        
        var commentBody = this.state.commentValue;
        
        if(this.state.image != null)
        {
            commentBody = '<div class="postCommentTextWrap">'+commentBody+'</div><div class="postCommentImgWrap"><img src="'+this.state.image+'" /></div>';
        }
        
      
        var params = new URLSearchParams();
        params.append('body', commentBody);
        params.append('author_id', this.props.user.id);
        params.append('post_id', this.props.postId);
        params.append('status', 'visible');
        

        this.setState({commentsSendLoader: true});

        
        const dataUrl = Config.API_URL + '/hpwall/postComment?apikey=' + Config.API_KEY + '&user_id=' + this.props.user.id;
        axios.post(dataUrl,params)
                .then(res => {

                    this.loadPostComments();
                    //alert('test');
                    
                    //window.location.reload();
                    //reload
                    /*
                    setTimeout(() => {

                     var message = {
                            'action': 'redirect',
                            'href': Config.BASE_URL+'/api/content/home?apikey='+Config.API_KEY+'&user_id='+this.props.user.id+'#item'+this.props.postId,
                            'screen': 'home'
                        };

                    window.postMessage(JSON.stringify(message), '*');
                  }, 100);
                    this.closeWindow();
                    */
                });
                

    }

    render() {
        

        
        
        if (this.state.visible)
        {
            return (
                    
                    <div  className="commentsContainer">
                      <div className="nav-topbar">
                        <div className="navBtn" onClick={(e) => {this.closeWindow()}}></div>
                      {Trans.comments}
                    </div>
                    
                        <div className="hwItem row" >
                            <div className="hwItemTop">
                                <div className="avatar"><img src={this.state.post.avatar} /></div>
                                <div className="cnt">
                                    <h2>{this.state.post.authorName}</h2>
                                    <span className="date">{this.state.post.date}</span>
                                </div>
                            </div>

                            <div className="hwItemBody">
                                 <div dangerouslySetInnerHTML={{__html: this.state.post.body}} />
                            </div>
                        </div>

                        <div className="comments">
                         {this.state.commentsLoaded ?

                                   this.state.comments.map((item, key) => {
                                      return (
                                         <div className="comment_row" key={key}> 
                                            <div className="avatar-wrap">
                                                <i className="avatar"><img src={item.user_photo} /></i>
                                            </div>
                                            <div className="cnt">
                                              <strong>{item.user_name} {item.user_surname}</strong><span dangerouslySetInnerHTML={{__html: item.body}} />
                                                <span className="created-date">  {item.created_at}</span>
                                            </div>
                                            
                                        </div>
                                     );
                                  })

                                   : 
                                   <div> {Trans.loadingComments} </div>
                                 }
                                 
                            {this.state.commentsSendLoader ?
                            <div className="commentsSendLoader"><img src={Config.BASE_URL+'/img/load.gif'} /></div>
                            : ''}
                                 

                            <form className="commentForm" onSubmit={this.handleSendComment}>
                           
                                <img className="avIcon"  src={this.props.user.icon} />
                                <ContentEditable
                                    html={this.state.commentValue}
                                    disabled={false}      
                                    onChange={this.handleCommentChange} 
                                    className="add-comment-input"
                                  />

                                <div className="add-comment-input-tools">
                                    <button className="add-comment-submit" type="submit" value="Submit">&nbsp;</button>
                                </div>
                                
                                <div className="uploadResult"><img src={this.state.image} /></div>
                            </form>
                        </div>
                    </div>

                    );
        } else
        {
            return null;
        }



    }
}
;


export default Comments;
