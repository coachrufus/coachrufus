import React, { Component } from 'react';
import Translations from  './../Translations.js';
var Trans;
const Config = require('./../Config.js');
class NoTeam extends Component {

    constructor(props) {
        super(props);
        Trans = Translations.getTranslations(this.props.lang);

        this.state = {
        };

    }
    
    handleCreateTeamBtn()
    {
        var message = {
            'action': 'popup-full',
            'screen': 'home',
            'href': Config.BASE_URL+'/team/create?lt=mal'
        };

        window.postMessage(JSON.stringify(message), '*');
      
    }


    render() {
        return (
                <div className="rufus-alert-bg">
                <div className="rufus-alert rufus-alert-missing-team">
                <img className="rufus-alert-buble" src="/img/error/back.png" />
                    <div className="rufus-alert-text">
                        <h1> {Trans.noTeamTitle}</h1>
                        <strong> {Trans.noTeamText}</strong>
                                
                        <a  onClick={this.handleCreateTeamBtn} className="cta-btn-missing-team mpopup" >{Trans.noTeamCta}</a>
                                
                    </div>
                  <img className="rufus-alert-body" src="/img/rufus/rufus_ukazuje@2x.png" />
                </div>
                </div>
                );

    }
}
;


export default NoTeam;
