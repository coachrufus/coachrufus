import React, { Component } from 'react';
import Translations from  './../Translations.js';
var Trans;
import axios from 'axios';
const Config = require('./../Config.js');
class UnconfirmedPlayer extends Component {

    constructor(props) {
        super(props);
         Trans = Translations.getTranslations(this.props.lang);
        

        
        
        this.state = {
            loading: false,
            visible:true
        };

        this.handleClose = this.handleClose.bind(this);

    }
    
    handleInvitation(type)
    {
        
        this.setState({
            loading:true
        });
        
        var dataUrl;
        
        if('yes' == type)
        {
            dataUrl = Config.BASE_URL +  this.props.invitation.acceptLink;
        }
        
        if('no' == type)
        {
            dataUrl = Config.BASE_URL +  this.props.invitation.refuseLink;
        }
        
        var putData = {'status':'confirmed'};

        axios.put(dataUrl,putData)
                .then(res => {
                    this.props.onUpdate(this.props.index,'reload');
                });

    }
    
    
    
    handleClose()
    {
        this.setState({
            visible: false
        });
         //this.props.onUpdate(this.props.index,'update');
    }

    render() {

               
        var divClass = 'hwItem row hwItemMatchInvitation hwItemTeamInvitation off0';
        if(this.props.matchInvitations.length >0 || this.props.teamInvitations.length >0)
        {
            divClass = 'hwItem row hwItemMatchInvitation hwItemTeamInvitation off1';
        }
        
        if(this.props.matchInvitations.length >0 && this.props.teamInvitations.length >0)
        {
            divClass = 'hwItem row hwItemMatchInvitation hwItemTeamInvitation off2';
        }
        
        
        if(this.props.visible == false || this.state.visible == false)
        {
            return null;
        }
        
        if(this.state.loading )
        {
             return (
                <div className={divClass} >
                    
                    <div className="loader">
                    <strong>{Trans.sendingData}</strong>
                    <img src={Config.BASE_URL+'/img/load.gif'}/>
                            </div>
                </div>
            );
        }
        else
        {
             return (
                <div className={divClass} >
        <div className="close_btn" onClick={this.handleClose}></div>
                    <div className="hwItemTop">
                        <div className="avatar"><img src={this.props.invitation.icon} /></div>
                        <div className="cnt">
                            <h2>{this.props.invitation.name} {Trans.unconfirmedPlayer_Text} {this.props.invitation.teamName}</h2>
                           
                        </div>
                    </div>
                
                    <div className="hwItemBottom invItemBottom row">
                        <div className="col-xs-6 btn btn-lft confirm_btn" onClick={() => {this.handleInvitation('yes')}}>
                            {Trans.invTeam_Yes}
                        </div>
                        <div className="col-xs-6 btn decline_btn" onClick={() => {this.handleInvitation('no')}}>
                              {Trans.invTeam_No}
                        </div>
                        
                    </div>
                </div>

                );
        }

       
    }
}
;


export default UnconfirmedPlayer;
