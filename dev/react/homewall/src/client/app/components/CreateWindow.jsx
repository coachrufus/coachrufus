import React, { Component } from 'react';
import axios from 'axios';
const Config = require('./../Config.js');
import Translations from  './../Translations.js';
var Trans;
class CreateWindow extends Component {

    constructor(props) {
        super(props);
 Trans = Translations.getTranslations(this.props.lang);
        this.state = {
            post: {body:'',author_id:this.props.user.id,type:'personal'},
            selectedTeam:'all',
            visible: true
        };

        this.handleTeamChange = this.handleTeamChange.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleSaveAndPublish = this.handleSaveAndPublish.bind(this);
        this.handleSave = this.handleSave.bind(this);

        this.messageOrigin;
        this.messageSource;

        this.componentDidMount = this.componentDidMount.bind(this);
        this.initHandler = this.initHandler.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
    }

    initHandler(e)
    {
        this.messageOrigin = e.origin;
        this.messageSource = e.source;
    }

    componentDidMount() {
        window.addEventListener('message', this.initHandler);
        
        /*
        const dataUrl = Config.API_URL + '/hpwall/post/'+this.props.postId+'?apikey=' + Config.API_KEY + '&user_id=' + this.props.user.id;
        axios.get(dataUrl)
                .then(res => {
                    console.log(res.data);
                    this.setState({
                            post: res.data.result[0],
                        });
                });
                */
    }

    handleChange(event) {
        var post = this.state.post;
        post.body = event.target.value;
        this.setState({post: post});
    }

    handleTeamChange(event) {

        this.setState({selectedTeam: event.target.value});
    }

    handleSaveAndPublish(event) {
        event.preventDefault();
        

        var params = this.buildSaveParams();
        params.append('status', 'published');


        const dataUrl = Config.API_URL + '/hpwall/post?apikey=' + Config.API_KEY + '&user_id=' + this.props.user.id;
        axios.post(dataUrl,params)
                .then(res => {
                   //this.props.onSaveAndPublish();
           
                    setTimeout(() => {

                     var message = {
                            'action': 'redirect',
                            'href': Config.BASE_URL+'/api/content/home?apikey='+Config.API_KEY+'&user_id='+this.props.user.id,
                            'screen': 'home'
                        };
console.log(Config.BASE_URL+'/api/content/home?apikey='+Config.API_KEY+'&user_id='+this.props.user.id);
                    window.postMessage(JSON.stringify(message), '*');
                  }, 200);

                   setTimeout(() => {

                    var message = {
                            'action': 'close',
                             'screen': 'home'
                         };
console.log('close');

                     window.postMessage(JSON.stringify(message), '*');
                   }, 300);
              
                });

        
    }
    
    buildSaveParams()
    {
        var postData = this.state.post;
        const params = new URLSearchParams();
        params.append('body', postData.body);
        params.append('author_id', postData.author_id);
        params.append('type', 'personal');
         if('me' == this.state.selectedTeam )
        {
             params.append('team_id[]', 0);
        }
        
        if('all' == this.state.selectedTeam )
        {
              this.props.user.teams.map(function(teamObj) {
                params.append('team_id[]', teamObj.id );
              });
        }
        
        if('all' != this.state.selectedTeam && 'me' != this.state.selectedTeam)
        {
            params.append('team_id[]',this.state.selectedTeam);
        }
        
        return params;
    }
    
    handleSave(event) {
        event.preventDefault();
        
        var postData = this.state.post;
        
        var createdAt = new Date();
        
        var params = this.buildSaveParams();
        params.append('status', 'proposal');
       
        const dataUrl = Config.API_URL + '/hpwall/post?apikey=' + Config.API_KEY + '&user_id=' + this.props.user.id;
        axios.post(dataUrl,params)
                .then(res => {

                      setTimeout(() => {

                     var message = {
                            'action': 'redirect',
                            'href': Config.BASE_URL+'/api/content/home?apikey='+Config.API_KEY+'&user_id='+this.props.user.id+'&tab=personal',
                            'screen': 'home'
                        };

                    window.postMessage(JSON.stringify(message), '*');
                  }, 200);

                   setTimeout(() => {

                    var message = {
                            'action': 'close',
                             'screen': 'home'
                         };


                     window.postMessage(JSON.stringify(message), '*');
                   }, 300);
                    
                });
    }

    handleCancel()
    {
        //lert('asdfas');
        //this.redirectToHome();
        //this.props.onClose();
         setTimeout(() => {

                     var message = {
                            'action': 'redirect',
                            'href': Config.BASE_URL+'/api/content/home?apikey='+Config.API_KEY+'&user_id='+this.props.user.id,
                            'screen': 'home'
                        };

                    window.postMessage(JSON.stringify(message), '*');
                  }, 200);

                   setTimeout(() => {

                    var message = {
                            'action': 'close',
                             'screen': 'home'
                         };


                     window.postMessage(JSON.stringify(message), '*');
                   }, 300);
    }
    
    redirectToHome()
    {
        //refresh 
         this.props.onClose();
                    
        /*
        var message = {
            'action': 'redirect',
            'href': Config.BASE_URL+'/api/content/home?apikey='+Config.API_KEY+'&user_id='+this.props.user.id+'&pm=close',
            'screen':'home'
        };
*/
        var message = {
            'action': 'close',
            'screen': 'home',
        };
        
        window.postMessage(JSON.stringify(message), '*');
    }

    render() {
        if(this.state.visible)
        {
            return (
                <div className="editPostWindow">
                    <div className="row tool-panel">
                        <div className="col-xs-4 cancel-col" onClick={this.handleCancel}>{Trans.cancel}</div>
                        <div className="col-xs-4">{Trans.newPost}</div>
                        <div className="col-xs-4 save-col" onClick={this.handleSave}>{Trans.save}</div>
                    </div>
                    
                    <div className="row visibilityRow">
                    <div className="col-xs-5 visibilityRowLabel">{Trans.choiceTeam}</div>
                    <div className="col-xs-7">
                    
            <div className="custom_native_select">
                        <select  name="team" className="team_select_native" value={this.state.selectedTeam}   onChange={this.handleTeamChange}>
                        <option value="all">{Trans.choiceTeamAll}</option>
                        <option value="me">{Trans.choiceTeamMe}</option>
                        {
                           this.props.user.teams.map(function(teamObj) {
                             return <option key={teamObj.id}
                               value={teamObj.id}>{teamObj.name}</option>;
                           })
                         }
                        </select>
                        </div>
                    
                      
                    </div>
                    </div>
                    
                    
                    <div className="row">
                        <form onSubmit={this.handleSaveAndPublish}>
                            <textarea  value={this.state.post.body} onChange={this.handleChange} ></textarea>
                            <input className="save-and-publish" type="submit" value={Trans.saveAndPublish} />
                        </form>
                    </div>
                </div>

                );
        }
        else
        {
            return null;
        }
    

        
    }
};


export default CreateWindow;
