import React, { Component } from 'react';
import Translations from  './../Translations.js';
var Trans;
import axios from 'axios';
const Config = require('./../Config.js');
class EditWindow extends Component {

    constructor(props) {
        super(props);
 Trans = Translations.getTranslations(this.props.lang);
        this.state = {
            post: {body:''}
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSaveAndPublish = this.handleSaveAndPublish.bind(this);
        this.handleSave = this.handleSave.bind(this);

        this.messageOrigin;
        this.messageSource;

        this.componentDidMount = this.componentDidMount.bind(this);
        this.initHandler = this.initHandler.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
    }

    initHandler(e)
    {
        this.messageOrigin = e.origin;
        this.messageSource = e.source;
    }

    componentDidMount() {
        window.addEventListener('message', this.initHandler);
        
        const dataUrl = Config.API_URL + '/hpwall/post/'+this.props.postId+'?apikey=' + Config.API_KEY + '&user_id=' + this.props.user.id;
        axios.get(dataUrl)
                .then(res => {
                    console.log(res.data);
                    this.setState({
                            post: res.data.result[0],
                        });
                });
    }

    handleChange(event) {
        var post = this.state.post;
        post.body = event.target.value;
        this.setState({post: post});
    }

    handleSaveAndPublish(event) {
        event.preventDefault();
        
        var putData = this.state.post;
        putData.status = 'published';

        const dataUrl = Config.API_URL + '/hpwall/post?apikey=' + Config.API_KEY + '&user_id=' + this.props.user.id;
        axios.put(dataUrl,putData)
                .then(res => {
                    //this.props.onUpdate(); 
             this.redirectToHome();
                });

        
    }
    
    handleSave(event) {
        event.preventDefault();
        
        var putData = this.state.post;
        const dataUrl = Config.API_URL + '/hpwall/post?apikey=' + Config.API_KEY + '&user_id=' + this.props.user.id;
        axios.put(dataUrl,putData)
                .then(res => {
                    //this.props.onUpdate(); 
             this.redirectToHome();
                });
    }

    handleCancel()
    {
        //lert('asdfas');
        this.redirectToHome();
       
    }
    
    redirectToHome()
    {
         var message = {
            'action': 'close',
            'screen': 'home',
        };
        window.postMessage(JSON.stringify(message), '*');
    }

    render() {

        return (
                <div className="editPostWindow">
                    <div className="row tool-panel">
                        <div className="col-xs-4 cancel-col" onClick={this.handleCancel}>Cancel</div>
                        <div className="col-xs-4">Post edit</div>
                        <div className="col-xs-4 save-col" onClick={this.handleSave}>Save</div>
                    </div>
                    <div className="row">
                        <form onSubmit={this.handleSaveAndPublish}>
                         <textarea className="editCnt"  onChange={this.handleChange}  value={this.state.post.body} />
                            <input className="save-and-publish" type="submit" value={Trans.saveAndPublish} />
                        </form>
                         { this.state.post.img != null ? 
                            <img src={this.state.post.img } />
                         : ''}
                    </div>
                </div>

                );
    }
}
;


export default EditWindow;
