import React, { Component } from 'react';
import Translations from  './../Translations.js';
import axios from 'axios';
var Trans;
const Config = require('./../Config.js');
class MarketingAgree extends Component {

    constructor(props) {
        super(props);
        Trans = Translations.getTranslations(this.props.lang);

        this.state = {
        };

    }

    handleAgree(value)
    {
       this.setState({
            loading:true
        });
        
         var params = new URLSearchParams();

        params.append('user_id', this.props.uid);
        params.append('marketing_agreement', value);

        const dataUrl = Config.BASE_URL + '/api/user/privacy?apikey=' + Config.API_KEY;
        axios.post(dataUrl,params) .then(res => {
             //this.props.onUpdate(this.props.index);
             this.setState({
                loading:false
            });
             window.location.reload();
        });
        
    }

    render() {


        var agree_url = "/player/dashboard/" + this.props.uid;
        
        if(this.state.loading )
        {
             return (
                <div className="hwItem row hwItemMatchInvitation" >
                    
                    <div className="loader">
                    <strong>{Trans.sendingData}</strong>
                    <img src={Config.BASE_URL+'/img/load.gif'}/>
                            </div>
                </div>
            );
        }
        else
        {
            return (
                    <div className="marketing-agree">
                        <div className="row">
                            <div className="col-sm-12">

                                <img src="/theme/img/icon/big-envelope.svg" />

                                <div>
                                    <div dangerouslySetInnerHTML={{__html: Trans.marketingAgreeText }} />

                                </div>

                            </div>
                            <div className="button-form">
                                <input type="hidden" name="f_act" value="gdpr_marketing_agreement" />
                                <button onClick={() => {this.handleAgree('2')}} name="gdpr_marketing_agreement" value="2" className="btn btn-primary agree-clear" type="submit"> {Trans.marketingAgreeBtnNo}</button>
                                <button onClick={() => {this.handleAgree('1')}} name="gdpr_marketing_agreement" value="1" className="btn btn-primary agree-clear agree-primary" type="submit"> {Trans.marketingAgreeBtnYes}</button>

                            </div>
                        </div>
                    </div>
            );
        }
        
        
        

    }
}
;

export default MarketingAgree;
