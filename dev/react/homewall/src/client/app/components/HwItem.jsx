import React, { Component } from 'react';
import Comments from  './../components/Comments.jsx';
import axios from 'axios';
import humanDate from 'human-date';
const Config = require('./../Config.js');
import Translations from  './../Translations.js';
var Trans;
class HwItem extends Component {

    constructor(props) {
        super(props);
        Trans = Translations.getTranslations(this.props.lang);
        this.avatar;
        this.authorName;
        this.hwItemDate;
        this.hwItemTeamAfterMatchInfo  = '';
        var safariDate = this.props.item.published_at.replace('-','/').replace('-','/');
        var date = new Date(safariDate);
        
        
         if(this.props.item.type == 'personal')
        {
             this.avatar = this.props.item.user_photo;
             this.authorName =  this.props.item.user_name+' '+ this.props.item.user_surname;
        }

        if(this.props.item.type == 'team' || this.props.item.type == 'team_after_match' )
        {
             this.avatar = this.props.item.team_photo;
             this.authorName = this.props.item.team_name;
        }
        
        if(this.props.item.type == 'team_after_match')
        {
            //hwItemTeamAfterMatchInfo
            var postBodyData = JSON.parse(this.props.item.body_data);
            var eventSafariDate = postBodyData.event_date.date.replace('-','/').replace('-','/').replace(' 00:00:00.000000','');
            var eventDate = new Date(eventSafariDate);
            if(null != postBodyData.event_name)
            {
                this.hwItemTeamAfterMatchInfo  = '('+postBodyData.event_name+' '+eventDate.getDate()+"."+(eventDate.getMonth()+1)+"."+eventDate.getFullYear()+' '+Trans.atTime+' '+postBodyData.event_time+')';
            }
        }
        
        if(this.props.item.type == 'team_ladder')
        {
             
            this.avatar = this.props.item.user_photo;
             var postBodyData = JSON.parse(this.props.item.body_data);
             
             this.authorName =  this.props.item.user_name+' '+ this.props.item.user_surname+'('+postBodyData.team_name+')';
             
             
            var eventSafariDate = postBodyData.event_date.date.replace('-','/').replace('-','/').replace(' 00:00:00.000000','');
            var eventDate = new Date(eventSafariDate);
            if(null != postBodyData.event_name)
            {
                this.hwItemTeamAfterMatchInfo  = ''+postBodyData.event_name+' '+eventDate.getDate()+"."+(eventDate.getMonth()+1)+"."+eventDate.getFullYear()+' '+Trans.atTime+' '+postBodyData.event_time+'';
            }
            
        }
       

        //var date = new Date.parse(this.props.item.published_at, "yyyy-MM-dd");
        
        var today = new Date();
        var timeDiff = Math.abs(today.getTime() - date.getTime());
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        
        this.hwItemDate =  date.getDate()+"."+(date.getMonth()+1)+"."+date.getFullYear();
       // this.date  = 'dummy';

       
        if (diffDays < 8)
        {
            this.hwItemDate = humanDate.relativeTime(safariDate);
        } else
        {
            this.hwItemDate = date.toLocaleDateString("sk-SK");
        }

        if (this.props.item.type == 'personal')
        {
            this.avatar = this.props.item.user_photo;
            this.authorName = this.props.item.user_name + ' ' + this.props.item.user_surname;
        }

        //check likes
        var hasVoted = false;
        var userLikeId;
        this.props.item.likes.map((like, key) => {
            if(like.author_id == this.props.user.id)
            {
               hasVoted = true;
               userLikeId = like.id;
            }
        });
        
        //check comments
        var commentsInfo = {'faces':[],'names':[]};

         this.props.item.comments.map((comment, key) => {
                 if(key < 6)
                 {
                    commentsInfo.faces[key] = comment.user_photo;
                     commentsInfo.names[key] = comment.user_name;
                 }
                
         });
         
         var uniquecommentsInfoFaces = commentsInfo.faces.filter(function(item, pos) {
            return commentsInfo.faces.indexOf(item) == pos;
        });
        var uniquecommentsInfoNames = commentsInfo.names.filter(function(item, pos) {
            return commentsInfo.names.indexOf(item) == pos;
        });
        commentsInfo.faces = uniquecommentsInfoFaces;
        commentsInfo.names = uniquecommentsInfoNames.slice(0, 3);
        commentsInfo.text = '';
        
        if(commentsInfo.faces.length == 1)
        {
           commentsInfo.text = commentsInfo.names.join(',')+' '+Trans.hasCommented1;
        }
        
        if(commentsInfo.faces.length == 2)
        {
           commentsInfo.text = commentsInfo.names.join(',')+' '+Trans.hasCommented2;
        }
        
        if(commentsInfo.faces.length == 3)
        {
           commentsInfo.text = commentsInfo.names.join(',')+' '+Trans.hasCommented2;
        }
        
         if(commentsInfo.faces.length > 3)
        {
           commentsInfo.text = commentsInfo.names.join(',')+' '+Trans.hasCommented;
        }

       
        this.state = {
            likeCount: this.props.item.like_count,
            commentsCount: this.props.item.comments_count == null ? 0 : this.props.item.comments_count,
            commentsWindowVisible: false,
            hasVoted:hasVoted,
            userLikeId:userLikeId,
            commentsInfo:commentsInfo,
            menuVisible:false,
            removeAlertVisible:false,
            visible:true
        };
        
       


        this.messageOrigin;
        this.messageSource;
        this.componentDidMount = this.componentDidMount.bind(this);
        this.initHandler = this.initHandler.bind(this);
        this.handleCntClick = this.handleCntClick.bind(this);
        this.handleMenuClick = this.handleMenuClick.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
         this.handleRemoveAlert = this.handleRemoveAlert.bind(this);
        this.handleUnpublish = this.handleUnpublish.bind(this);

    }
    
    initHandler(e)
    {
        this.messageOrigin = e.origin;
        this.messageSource = e.source;
    }

    componentDidMount() {
        window.addEventListener('message', this.initHandler);
    }
    
    componentDidUpdate() {
        /*
        if(this.props.bulkItemAction == 'closeMenu')
        {
             this.setState({
                        menuVisible:false
                    });
        }
        */
    }
    
    
     handleLike()
    {
        if(this.state.hasVoted == false)
        {
            var type = 'l';
        }
        else
        {
            var type = 'u';
        }
            
            var likeId = '';
            const dataUrl = Config.BASE_URL + '/api/hpwall/post-like?apikey=' + Config.API_KEY + '&pid=' + this.props.item.id +'&t='+type+'&user_id='+this.props.user.id+'&lid='+this.state.userLikeId;
            axios.get(dataUrl) .then(res => {

                 if(res.data.status == 'success')
                 {
                    this.setState({
                            likeCount:res.data.likes,
                            userLikeId: res.data.likeId
                        });
                 }
            });
            
        if(this.state.hasVoted == false)
        {
            this.setState({
                hasVoted:true
            });
        }
        else
        {
             this.setState({
                hasVoted:false
            });
        }

    }
    
    showComments()
    {
        var message = {
            'action': 'popup-full',
            'screen': 'home',
            'href': Config.BASE_URL+'/api/content/home?apikey='+Config.API_KEY+'&user_id='+this.props.user.id+'&a=comments&post-edit='+this.props.item.id,
        };
        //console.log(Config.BASE_URL+'/api/content/home?apikey='+Config.API_KEY+'&user_id='+this.props.user.id+'&a=comments&post-edit='+this.props.item.id);
        window.postMessage(JSON.stringify(message), '*');
        /* 
        this.setState({
                        commentsWindowVisible:true
                    });
                    */
    }
    
    thisHandleSaveComment(){
         var  commentsCount = parseInt(this.state.commentsCount)+1;
        this.setState({
                        commentsWindowVisible:false,
                        commentsCount:commentsCount
                    });
    }
    
    handleCntClick(){
        if(this.props.item.type == 'team_after_match' )
        {
             var url = this.props.item.link ;
             
             var message = {
                'action': 'popup-full',
                'screen': 'home',
                'href': Config.BASE_URL+url
            };
            window.postMessage(JSON.stringify(message), '*');
        }
    }
    
    handleMenuClick(){
       if(this.state.menuVisible == true)
       {
            this.setState({
                        menuVisible:false,
                    });
       }
       else
       {
            this.setState({
                        menuVisible:true,
                    });
       }
    }
    
      handleRemoveAlert(e,action){
        e.preventDefault();
        if('show' == action)
         {
             this.setState({
                removeAlertVisible:true
            });
         }
        if('hide' == action)
         {
             this.setState({
                removeAlertVisible:false
            });
         }
        
    }
    
      
    handleDelete()
    {
        const dataUrl = Config.API_URL + '/hpwall/post/'+this.props.item.id+'?apikey=' + Config.API_KEY + '&user_id=' + this.props.user.id;
        axios.delete(dataUrl)
                .then(res => {
                    //this.props.onUpdate();
                    this.setState({
                        visible:false,
                         removeAlertVisible:false
                    });
                });
    }
    
    handleUnpublish()
    {
        var putData = this.props.item;
        putData.status = 'proposal';
        const dataUrl = Config.API_URL + '/hpwall/post?apikey=' + Config.API_KEY + '&user_id=' + this.props.user.id;
        axios.put(dataUrl,putData)
                .then(res => {
                    this.props.onUpdate();
                    this.setState({
                        visible:false
                    });
                });
                
    }
    
    handleBodyImgClick(item)
    {

        if('team_after_match' == item.type)
        {
             var url = item.link ;
             
             var message = {
                'action': 'popup-full',
                'screen': 'home',
                'href': Config.BASE_URL+url
            };
            window.postMessage(JSON.stringify(message), '*');
        }
    }

    render() {
        if(this.state.visible == false)
        {
            return null;
        }
        
        var commentsClassname = 'commentsHidden';
        if(this.state.commentsWindowVisible)
        {
            commentsClassname = 'commentsVisible';
        }
        
        var menuClassname = 'hwItemMenu';
         if(this.state.menuVisible)
        {
            menuClassname = 'hwItemMenu active';
        }
        
        var likeClassname = 'ico ico-thumbsUp';
        if(this.state.hasVoted == true)
        {
            likeClassname = 'ico ico-thumbsUpGreen';
        }
        
        
        if(this.props.item.type == 'promo')
        {
             return (
                <div className="hwItem row" >
                    <div className="hwItemBody">
                       <img src={this.props.item.img} />              
                        <div dangerouslySetInnerHTML={{__html: this.props.item.body}} />
                    </div>
                </div>

                );
        }
        else
        {
           
            return (
            <div  className={commentsClassname}>
          
           <div className="hwItem row" >
              {this.state.removeAlertVisible ? 
                <div className="removeAlert">
                    <div className="row text-center">
                        <p>{Trans.removeAlert}</p>
                        <div className="col-xs-6"><span onClick={this.handleDelete} className="btn btn-danger">{Trans.yes}</span></div>
                        <div className="col-xs-6"><span onClick={(e) => {this.handleRemoveAlert(e,'hide')}} className="btn">{Trans.no}</span></div>
                    </div>
                </div>
                : ''}
           
           
           
               <a  name={'item'+this.props.item.id} href="#" />
               <div className="hwItemTop">
                   <div className="avatar"><img src={this.avatar} /></div>
                   <div className="cnt">
                       <h2>{this.authorName}</h2>
                       <span className="hwItemDate">  { this.hwItemDate } { this.hwItemTeamAfterMatchInfo }</span>
                   </div>
               </div>
               
               { this.props.item.author_id ==  this.props.user.id ? 
               <div className={menuClassname}>
                    <div className="hwItemMenuTrigger" onClick={() => {this.handleMenuClick();}} ></div>
                    <div className="hwItemMenuCnt">
                        <a href="#" onClick={(e) => {this.handleRemoveAlert(e,'show')}} >Delete</a>
                        <a href="#" onClick={this.handleUnpublish}>Unpublish</a>
                    </div>
               </div>
                : ''}
                
               <div className="hwItemBody">
                 <div dangerouslySetInnerHTML={{__html: this.props.item.body}} />
                 { this.props.item.img != null ? 
                    <img onClick={() => {this.handleBodyImgClick(this.props.item);}} src={this.props.item.img} />
                 : ''}
                </div>
                
                  <div className="hwItemBottomInfo row" onClick={() => {this.showComments()}} >

                    {
                        this.state.commentsInfo.faces.map((face, key) => {
                                   return (
                                      <img className="face" key={'face'+key} src={face} />
                                  );
                               })
                    }
                    {this.state.commentsInfo.faces.length > 0 ?  <span className="commentsText">{this.state.commentsInfo.text}</span>: '' }
                </div>
                

               <div className="hwItemBottom row">

                   {this.state.likeCount > 0 ? 
                       <div onClick={() => {this.handleLike()}} className="col-xs-6 btn"><i className={likeClassname} /><span className="likesCountNumber">{this.state.likeCount}</span></div>
                       : 
                       <div onClick={() => {this.handleLike()}} className="col-xs-6 btn"><i className={likeClassname} /></div>
                   }

                   <div className="col-xs-6 btn border-item" onClick={() => {this.showComments()}} >
                        <i className="ico ico-buble" /> {this.state.commentsCount > 0 ? <span className="commentsCountNumber">{this.state.commentsCount} </span>: '' }
                   </div>
                  
               </div>
           </div>
           </div>
           );

                
                
                
                
               
        }

        
    }
}
;


export default HwItem;
