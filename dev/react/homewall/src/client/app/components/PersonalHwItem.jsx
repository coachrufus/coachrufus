import React, { Component } from 'react';
import Translations from  './../Translations.js';
import humanDate from 'human-date';
var Trans;
const Config = require('./../Config.js');
import axios from 'axios';

class PersonalHwItem extends Component {

    constructor(props) {
        super(props); 
        Trans = Translations.getTranslations(this.props.lang);
        
        this.avatar;
        this.authorName;
        this.hwItemDate;
        this.body = this.props.item.body;

        var date = new Date();
        this.hwItemDate = date.toLocaleDateString("sk-SK");
         
        //console.log(this.props.item);
        var createdSafariDate = this.props.item.created_at.replace('-','/').replace('-','/');
        var createdDate = new Date(createdSafariDate);
        this.createdDate = createdDate.toLocaleDateString("sk-SK");
        
         if(this.props.item.type == 'personal')
        {
             this.avatar = this.props.item.user_photo;
             this.authorName =  this.props.item.user_name+' '+ this.props.item.user_surname;
        }

        if(this.props.item.type == 'team' || this.props.item.type == 'team_after_match' || this.props.item.type == 'position_change' )
        {
             this.avatar = this.props.item.team_photo;
             this.authorName = this.props.item.team_name;
        }
        
        if(this.props.item.type == 'team_ladder' || this.props.item.type == 'personal_performance')
        {
             this.avatar = this.props.item.team_photo;
             this.authorName = this.props.item.team_name;
             
             var postBodyData = JSON.parse(this.props.item.body_data);

             
             //this.authorName =  this.props.item.user_name+' '+ this.props.item.user_surname+'('+postBodyData.team_name+')';
             
            var eventSafariDate = postBodyData.event_date.date.replace('-','/').replace('-','/').replace(' 00:00:00.000000','');
            var eventDate = new Date(eventSafariDate);
            if(null != postBodyData.event_name)
            {
                 this.body = ''+postBodyData.event_name+' '+eventDate.getDate()+"."+(eventDate.getMonth()+1)+"."+eventDate.getFullYear()+' '+Trans.atTime+' '+postBodyData.event_time+'<br />';
                //this.hwItemDate  = ;
            }
        }
        
        this.state = {
            visible:true,
            removeAlertVisible:false
        };
        
        this.messageOrigin;
        this.messageSource;
        
        this.componentDidMount = this.componentDidMount.bind(this);
        this.initHandler = this.initHandler.bind(this);
        
        this.handlePublish = this.handlePublish.bind(this);
        this.handleEdit = this.handleEdit.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
        this.handleRemoveAlert = this.handleRemoveAlert.bind(this);
    }
    
     initHandler(e)
    {
        this.messageOrigin = e.origin;
        this.messageSource = e.source;
    }

    componentDidMount() {
        window.addEventListener('message', this.initHandler);
    }
    
    handlePublish()
    {
        var putData = this.props.item;
        putData.status = 'published';
        putData.authorId = this.props.user.id;
        const dataUrl = Config.API_URL + '/hpwall/post?apikey=' + Config.API_KEY + '&user_id=' + this.props.user.id;
        axios.put(dataUrl,putData)
                .then(res => {
                    this.props.onUpdate();
                    this.setState({
                        visible:false
                    });
                });
    }
    
    handleDelete()
    {
        //var putData = this.props.item;
        //putData.status = 'published';
        const dataUrl = Config.API_URL + '/hpwall/post/'+this.props.item.id+'?apikey=' + Config.API_KEY + '&user_id=' + this.props.user.id;
        axios.delete(dataUrl)
                .then(res => {
                    //this.props.onUpdate();
                    this.setState({
                        visible:false,
                         removeAlertVisible:false
                    });
                });
    }
    
    handleEdit(){
        var message = {
            'action': 'popup-full',
            'href':  Config.BASE_URL+'/api/content/home?apikey='+Config.API_KEY+'&user_id='+this.props.user.id+'&post-edit='+this.props.item.id+'&a=edit',
            'screen': 'home',
            'onclose-action':'redirect'
        };
        window.postMessage(JSON.stringify(message), '*');
    }
    
    handleRemoveAlert(action){
         if('show' == action)
         {
             this.setState({
                removeAlertVisible:true
            });
         }
        if('hide' == action)
         {
             this.setState({
                removeAlertVisible:false
            });
         }
        
    }
    

    render() {
        
         if(this.state.visible == false)
        {
            return null;
        }


        return (
                <div className="hwItem row" >
        
                {this.state.removeAlertVisible ? 
                <div className="removeAlert">
                    <div className="row text-center">
                        <p>{Trans.removeAlert}</p>
                        <div className="col-xs-6"><span onClick={this.handleDelete} className="btn btn-danger">{Trans.yes}</span></div>
                        <div className="col-xs-6"><span onClick={(e) => {this.handleRemoveAlert('hide')}} className="btn">{Trans.no}</span></div>
                    </div>
                </div>
                : ''}
        
                   
                    <a  name={'item'+this.props.item.id} href="#" />
                    <div className="hwItemTop">
                       <a  name={'item'+this.props.item.id} href="#" />
                        <div className="hwItemTop">
                            <div className="avatar"><img src={this.avatar} /></div>
                            <div className="cnt">
                                <h2>{this.authorName}</h2>
                                <span className="hwItemDate">{ this.createdDate }</span>
                            </div>
                        </div>
                        
                         <div className="removeItemBtn" onClick={(e) => {this.handleRemoveAlert('show')}}></div>
                    </div>
                    
                     <div className="cnt">
                            <div dangerouslySetInnerHTML={{__html: this.body }} />
                                 { this.props.item.img != null ? 
                    <img src={this.props.item.img} />
                 : ''}
                        </div>
                
                    <div className="hwItemBottom row">
                        <div className="col-xs-6 btn" onClick={this.handleEdit} >
                            {Trans.edit}
                        </div>
                        <div className="col-xs-6 btn center-item" onClick={this.handlePublish}>
                             {Trans.publish}
                        </div>
                    </div>
                </div>

                );
    }
}
;


export default PersonalHwItem;
