import React, { Component } from 'react';

class Translations
{
    constructor()
    {
        this.lang = 'sk';
        this.translations = {
            nav_Team: {
                sk: 'Tímove',
                en: 'Team\'s'
            },
            nav_Personal: {
                sk: 'Osobné',
                en: 'Personal'
            },
            inv_Yes: {
                sk: 'Prídem',
                en: 'Yes'
            },
            inv_No: {
                sk: 'Neprídem',
                en: 'No'
            },
            inv_Text: {
                sk: 'prídeš na zápas?',
                en: 'will you attend?'
            },
            unconfirmedPlayer_Text: {
                sk: 'by sa rád pripojil k tvojmu tímu',
                en: 'would like to join your team'
            },
            invTeam_Text: {
                sk: 'Pozvánka do tímu',
                en: 'Team invitation'
            },
            invTeam_Yes: {
                sk: 'Prijať',
                en: 'Accept'
            },
            invTeam_No: {
                sk: 'Odmietnuť',
                en: 'Refuse'
            },
            edit: {
                sk: 'Upraviť',
                en: 'Edit'
            },
            publish: {
                sk: 'Uverejniť',
                en: 'Publish'
            },
            newPost: {
                sk: 'Nový príspevok',
                en: 'New'
            },
            choiceTeam: {
                sk: 'Viditeľné pre',
                en: 'Visible for'
            },
            choiceTeamAll: {
                sk: 'Všetci',
                en: 'All'
            },
            choiceTeamMe: {
                sk: 'Iba ja',
                en: 'Onle me'
            },
            saveAndPublish: {
                sk: 'Uložiť a publikovať',
                en: 'Save and Publish'
            },
            cancel: {
                sk: 'Zrušiť',
                en: 'Cancel'
            },
            save: {
                sk: 'Uložiť',
                en: 'Save'
            },
            removeAlert: {
                sk: 'Naozaj zmazať?',
                en: 'Are you sure?'
            },
            yes: {
                sk: 'Áno',
                en: 'Yes'
            },
            no: {
                sk: 'Nie',
                en: 'No'
            },
            loadingComments: {
                sk: 'Náhrávam komentáre',
                en: 'Loading comments'
            },

            writeComment: {
                sk: 'Napíš komentár',
                en: 'Write comment'
            },
            comments: {
                sk: 'Komentáre',
                en: 'Comments'
            },
            hasCommented: {
                sk: 'a daľší komentovali',
                en: 'and others has commented'
            },
            hasCommented1: {
                sk: 'komentoval',
                en: 'has commented'
            },
            hasCommented2: {
                sk: 'komentovali',
                en: 'has commented'
            },
            sendingData: {
                sk: 'Odosielanie',
                en: 'Sending data'
            },
            atTime: {
                sk: 'o',
                en: 'at'
            },
            noTeamTitle: {
                sk: 'Zatiaľ nemáš žiadny tím!',
                en: 'You don\'t have a Team yet!'
            },
            noTeamText: {
                sk: 'Ak chceš začať, založ nový tím.',
                en: 'Create Team to continue'
            },
            noTeamCta: {
                sk: 'Založiť tím',
                en: 'Create Team'
            },
            marketingAgreeText: {
                sk: '<h4>Zostaneme<br /> v kontakte?</h4>Raz za čas by sme Vám radi zaslali krátke novinky o nových funkciách, zvizualizované personalizované údaje alebo ponuky od Rufusa a jeho partnerov, ktoré veríme, že oceníte. Môžeš si to kedykoľvek rozmyslieť.',
                en: '<h4>Would you like to stay in touch? </h4>Time to time we would like to inform you about new features and special functios.  We believe you would appreciate special offers from us or our partners. We do not share your data and you can change this decision at any time.'
            },
             marketingAgreeBtnYes: {
                sk: 'Áno prosím',
                en: 'Yes'
            },
             marketingAgreeBtnNo: {
                sk: 'Nie',
                en: 'No'
            },
             empty_news_alert: {
                sk: 'Žiadne nové príspevky',
                en: 'No news'
            },

        };
    }

    getTranslations(lang)
    {
        var keys = Object.keys(this.translations);
        var allTrans = this.translations;
        var translations = {};
        //var lang = lang;
        keys.forEach(function (k) {
            translations[k] = allTrans[k][lang];
        });

        return translations;
    }

}

export  default Translations = new Translations();

