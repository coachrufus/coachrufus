import React, { Component} from "react";
import ReactDOM from "react-dom";
import axios from 'axios';
import SwipeableViews from 'react-swipeable-views';
import HwItem from  './components/HwItem.jsx';
import PersonalHwItem from  './components/PersonalHwItem.jsx';
import MatchInvitation from  './components/MatchInvitation.jsx';
import NoTeam from  './components/NoTeam.jsx';
import MarketingAgree from  './components/MarketingAgree.jsx';
import TeamInvitation from  './components/TeamInvitation.jsx';
import UnconfirmedPlayer from  './components/UnconfirmedPlayer.jsx';
import EditWindow from  './components/EditWindow.jsx';
import CreateWindow from  './components/CreateWindow.jsx';
import Comments from  './components/Comments.jsx';
import styles from './index.css';
const Config = require('./Config.js');
import Translations from  './Translations.js';
var Trans;


class App extends Component {

    constructor(props) {
        super(props);
      
        Trans = Translations.getTranslations(this.props.lang);
        
        var activeTabIndex = 0;
        if('personal' == this.props.tab)
        {
            activeTabIndex = 1;
        }
       

        this.state = {
            activeTab: this.props.tab,
            matchInvitations:[],
            teamInvitations:[],
            unconfirmedPlayers:[],
            personalItems:[],
            teamItems:[],
            userData:null,
            editWindowVisible: false,
            createWindowVisible: false,
            addCommentWindowVisible: false,
            activeTabIndex:activeTabIndex,
            itemBulkAction:''
        };
        
        if(this.props.action == 'editPost')
        {
            this.state.editWindowVisible =true;
        }
        
        if(this.props.action == 'createPost')
        {
            this.state.createWindowVisible =true;
        }
        
         if(this.props.action == 'addComment')
        {
            this.state.addCommentWindowVisible =true;
        }


        this.handleMatchInviteUpdate = this.handleMatchInviteUpdate.bind(this);
        this.handleTeamInviteUpdate = this.handleTeamInviteUpdate.bind(this);
        this.handlePersonalItemUpdate = this.handlePersonalItemUpdate.bind(this);
        this.handleItemUpdate = this.handleItemUpdate.bind(this);
        this.handleCreateWindowClose = this.handleCreateWindowClose.bind(this);
        this.handleCreateWindowSave = this.handleCreateWindowSave.bind(this);
        this.handleCreateWindowSaveAndPublish = this.handleCreateWindowSaveAndPublish.bind(this);
       
         this.initHandler = this.initHandler.bind(this);
         
         this.handleChangeIndex = this.handleChangeIndex.bind(this);
         this.handleTabClick = this.handleTabClick.bind(this);
         this.handleSectionClick = this.handleSectionClick.bind(this);
         
         
         this.messageOrigin;
        this.messageSource;
    }
    
    
     initHandler(e)
    {
        this.messageOrigin = e.origin;
        this.messageSource = e.source;
    }
    
    componentDidMount(){
        window.addEventListener('message', this.initHandler); 
        this.loadNearestEvents();
         this.loadUserInfo();
         this.loadPersonalItems();
         
         if('createPostPopup' == this.props.action)
         {
              setTimeout(() => {
               
                var message = {
                        'action': 'popup-full',
                        'screen': 'home',
                        'href': Config.BASE_URL+'/api/content/home?apikey='+Config.API_KEY+'&user_id='+this.props.uid+'&a=create',
                    };
             
                window.postMessage(JSON.stringify(message), '*');
              }, 200);
         }

         


         if('closePopups' == this.props.action)
         {
            setTimeout(() => {

               var message = {
                       'action': 'close',
                        'screen': 'home'
                    };


                window.postMessage(JSON.stringify(message), '*');
              }, 200);
         }
    }
    
    componentDidUpdate() {
        
        var hash = window.location.hash.replace('#', '');
       
        if (hash) {
            var node = ReactDOM.findDOMNode(this.refs[hash]);
            if (node) {
                node.scrollIntoView();
            }
        }
        
        
    }

    handleTabClick(target){
       if(0 == target)
      {
          this.setState({
              activeTab: 'team',
              activeTabIndex:target,
          });
      }
      
      if(1 == target)
      {
          this.setState({
              activeTab: 'personal',
               activeTabIndex:target,
          });
      }
        
        
    }
    
    /*
    createRandomPersonalItem()
    {
         const dataUrl = Config.API_URL + '/hpwall/post?apikey=' + Config.API_KEY + '&user_id=' + this.props.uid;
    }
    */
    loadTeamItems(teams)
    {
        var teamIds = [];
        this.setState({
            teamItems: [],
        });
        if (teams != null) 
        {
            if (teams.length > 0)
            {
                teams.forEach(function (val, key) {
                    teamIds[key] = val.id;
                });

                var teamParam = teamIds.join();

                const dataUrl = Config.API_URL + '/hpwall/post?apikey=' + Config.API_KEY + '&user_id=' + this.props.uid + '&status=published&teams=' + teamParam;
                axios.get(dataUrl)
                        .then(res => {
                            this.setState({
                                teamItems: res.data.result,
                            });
                        });
            }
        }
    }
   
    loadPersonalItems()
    {
         this.setState({
                            personalItems: [],
                        });
        
        
        const dataUrl = Config.API_URL + '/hpwall/post?apikey=' + Config.API_KEY + '&user_id=' + this.props.uid+'&status=proposal&author_id='+this.props.uid;
        axios.get(dataUrl)
                .then(res => {
                    if (res.data.result.length == 0)
                    {
                            
                    } 
                    else
                    {
                        this.setState({
                            personalItems: res.data.result,
                        });
                    }
                });
    }
    
   
    
    loadUserInfo()
    {
        const dataUrl = Config.API_URL + '/user/info?apikey=' + Config.API_KEY + '&uid=' + this.props.uid;
        axios.get(dataUrl)
                .then(res => {

                    //create invitations
                    var teamInvitations = [];
                    var teamInvitationsKeys = Object.keys(res.data.user_data.teamInvitations);
                    teamInvitationsKeys.forEach(function (k) {
                        teamInvitations.push(teamInvitationsKeys[k]);
                    });
                    
                    //create unconfirmed players
                    var unconfirmedPlayers = [];
                    var unconfirmedPlayersKeys = Object.keys(res.data.user_data.unconfirmedPlayers);
                    unconfirmedPlayersKeys.forEach(function (k) {
                        unconfirmedPlayers.push(unconfirmedPlayersKeys[k]);
                    });
                    
                    
                    //load team posts 
                    this.loadTeamItems(res.data.user_data.teams);
                    this.setState({
                        userData: res.data.user_data,
                        teamInvitations: res.data.user_data.teamInvitations,
                        unconfirmedPlayers: res.data.user_data.unconfirmedPlayers
                    });
                });
    }
    
    loadNearestEvents()
    {
        var today = new Date();
        var periodMonth = ('0'+(today.getMonth()+1)).slice(-2);
        var periodDay = ('0'+today.getDate()).slice(-2);
        var todayFormated = today.getFullYear() + '-' + periodMonth+'-'+periodDay;
        var period = today.getFullYear() + '-' + periodMonth;
        const eventsDataUrl = Config.API_URL + '/event/month-grid?apikey=' + Config.API_KEY + '&user_id=' + this.props.uid + '&rt=json&month=' + period;


        axios.get(eventsDataUrl)
            .then(res => {
                var eventMonthOverview = res.data.month_overview;
                
                var todayEvents = null;
                var nearestEvents = null;
        
        
                //first check today events
                eventMonthOverview.forEach(function(week){
                   week.forEach(function(day){
                      
                       //console.log(day);
                       var dayDate = new Date(day.date);
                       
                       //today form now
                       if(day.date == todayFormated && dayDate.getTime() > today.getTime() )
                       {
                           if(day.events != null)
                           {
                               todayEvents = day.events;
                           }
                       }

                       //nearest events
                       if(dayDate.getTime() > today.getTime() && day.events != null && nearestEvents == null)
                       {
                           nearestEvents = day.events;
                       }
                   });
                });
                
                var matchInvitationEvents = [];
                
                
                if(null != todayEvents)
                {
                    matchInvitationEvents = todayEvents;
                }
                else
                {
                    if(null != nearestEvents)
                    {
                        matchInvitationEvents = nearestEvents;
                    }
                }
                
                 var matchInvitations = [];

                 
                 var eventsKeys = Object.keys(matchInvitationEvents);
                 var visibleLimit = 1;
                  eventsKeys.forEach(function (k) {
                    if(matchInvitationEvents[k].attendance.userStatus == 'unknown')
                    {
                         if(visibleLimit++ <=1)
                         {
                              matchInvitationEvents[k].visible = true;
                         }
                        else
                        {
                            matchInvitationEvents[k].visible = false;
                        }
                          matchInvitationEvents[k].id = k;
                          matchInvitations.push(matchInvitationEvents[k]);
                    }
                      
                 });

                 this.setState({
                     matchInvitations:matchInvitations
                 });
        
            });
    }
    
    handleMatchInviteUpdate(key)
    {

        var matchInvitations = this.state.matchInvitations;
        matchInvitations[key].visible = false;
        var nextIndex = key+1;
        if(nextIndex < matchInvitations.length )
        {
             matchInvitations[nextIndex].visible = true;
             this.setState({
                matchInvitations:matchInvitations
            });
        }
        else
        {
            this.setState({
                matchInvitations:[]
            });
        }        
    }
    
    handleTeamInviteUpdate(key,type)
    {
        if(type == 'reload')
        {
             window.location.reload();
        }
        
         if(type == 'update')
        {
              var teamInvitations = this.state.teamInvitations;
            teamInvitations[key].visible = false;
            var nextIndex = key+1;
            if(nextIndex < teamInvitations.length )
            {
                 teamInvitations[nextIndex].visible = true;
                 this.setState({
                    teamInvitations:teamInvitations
                });
            }
            else
            {
                this.setState({
                    teamInvitations:[]
                });
            }
        }

    }
    
    
    handlePersonalItemUpdate()
    {
         this.loadTeamItems(this.state.userData.teams);
         this.setState({
              activeTab: 'team',
              activeTabIndex:0
         });
    }
    
    handleItemUpdate()
    {
        this.loadPersonalItems();
        this.handleTabClick(1);
    }
    
    handleCreateWindowClose()
    {
        //refresh items
         this.loadTeamItems(this.state.userData.teams);
         this.setState({
              activeTab: 'team',
              activeTabIndex:0,
              createWindowVisible:false
         });
    }
    
    handleCreateWindowSave()
    {
         this.setState({
              activeTab: 'personal',
              activeTabIndex:1,
              createWindowVisible:false
         });
    }
    handleCreateWindowSaveAndPublish()
    {
         this.setState({
              activeTab: 'team',
              activeTabIndex:0,
              createWindowVisible:false
         });
    }
    
 
  
  handleChangeIndex(index, indexLatest, meta){
      if(0 == index)
      {
          this.setState({
              activeTab: 'team'
          });
      }
      
      if(1 == index)
      {
          this.setState({
              activeTab: 'personal'
          });
      }
  };
  
  handleSectionClick(e){
        if('hwItemMenuTrigger' != e.target.className)
        {
            this.setState({
              itemBulkAction: 'closeMenu'
         });
        }
       
  }
  
    

    render() {
       
       var eventTabWrapClass = 'tabWrap activeCnt'+this.state.activeTab;
       if(this.state.matchInvitations.length > 0 || this.state.teamInvitations.length > 0)
       {
           eventTabWrapClass = eventTabWrapClass+' hwSectionInvitations ';
       }
       
       if(this.state.teamInvitations.length > 0 && this.state.matchInvitations.length > 0 )
       {
           eventTabWrapClass = eventTabWrapClass+' hwSectionInvitations2 ';
       }
       
       
        return(
                <div className="Homewall">
                    {this.state.editWindowVisible && this.state.userData != null ?
                        <EditWindow lang={this.props.lang} postId={this.props.postId} user={this.state.userData}  />
                    : ''}
                    
                    {this.state.createWindowVisible && this.state.userData != null ?
                        <CreateWindow lang={this.props.lang} user={this.state.userData} onClose={this.handleCreateWindowClose} onSave={this.handleCreateWindowSave} onSaveAndPublish={this.handleCreateWindowSaveAndPublish}  />
                    : ''}
                    
                     {this.state.addCommentWindowVisible && this.state.userData != null ?
                         <Comments  lang={this.props.lang} postId={this.props.postId} user={this.state.userData} />
                    : ''}
        
        
             {this.state.createWindowVisible == false && this.state.editWindowVisible == false && this.state.addCommentWindowVisible == false ?
        
                    <div className="hwNaw row">
                        <div className={"col-xs-6 tab " + (this.state.activeTab == 'team' ? ' active' : '')}  onClick={(e) => {
                        this.handleTabClick(0)
                }}>{Trans.nav_Team}</div>
                        <div className={"col-xs-6 tab " + (this.state.activeTab == 'personal' ? ' active' : '')} onClick={(e) => {
                        this.handleTabClick(1)
                }}>{Trans.nav_Personal}</div>
                    </div>
                    
                 : ''}

                 {this.state.createWindowVisible == false && this.state.editWindowVisible == false  && this.state.addCommentWindowVisible == false ?
                 
                 <div className={eventTabWrapClass}>
                    {this.state.userData != null && this.state.matchInvitations.length > 0 ?

                            this.state.matchInvitations.map((event, key) => {
                               return (
                                   <MatchInvitation  lang={this.props.lang} index={key} onUpdate={this.handleMatchInviteUpdate}  visible={event.visible} user={this.state.userData} event={event} key={key} />
                              );
                           })

                          : 
                          '' 
                        }
                        
                         {this.state.userData != null && this.state.teamInvitations.length > 0 ?

                            this.state.teamInvitations.map((invitation, key) => {
                               return (
                                   <TeamInvitation  lang={this.props.lang} index={key} matchInvitations={this.state.matchInvitations} onUpdate={this.handleTeamInviteUpdate}  user={this.state.userData} invitation={invitation}  key={key} />
                              );
                           })

                          : 
                          '' 
                        }
                        
                         {this.state.userData != null && this.state.unconfirmedPlayers.length > 0 ?

                            this.state.unconfirmedPlayers.map((invitation, key) => {
                               return (
                                   <UnconfirmedPlayer  lang={this.props.lang} index={key} matchInvitations={this.state.matchInvitations} teamInvitations={this.state.teamInvitations} onUpdate={this.handleUnconfirmedPlayersUpdate}  user={this.state.userData} invitation={invitation}  key={key} />
                              );
                           })

                          : 
                          '' 
                        }
                        
                        {this.state.userData != null  && this.state.userData.privacy.marketingAgreement  == 'unknown'  ?


                               <MarketingAgree lang={this.props.lang} uid={this.props.uid} />
                             

                          : 
                          <SwipeableViews index={this.state.activeTabIndex} onChangeIndex={this.handleChangeIndex}>
                      
                          <div className="hwSectionCnt hwSectionTeam"  onClick={(e) => {this.handleSectionClick(e)}}>
                            {this.state.userData != null && this.state.teamItems.length > 0 ?

                               this.state.teamItems.map((item, key) => {
                                  return (
                                     <HwItem bulkItemAction={this.state.itemBulkAction}  lang={this.props.lang}  onUpdate={this.handleItemUpdate}  ref={'item'+item.id} item={item} key={key} user={this.state.userData}  />
                                 );
                              })

                               : 
                               <div className="emptyNews">{Trans.empty_news_alert}</div> 
                             }
                          </div>
                      

                          <div className="hwSectionCnt hwSectionPersonal">
                           {this.state.userData != null && this.state.personalItems.length > 0 ?
                            this.state.personalItems.map((item, key) => {
                               return (
                                  <PersonalHwItem lang={this.props.lang} onUpdate={this.handlePersonalItemUpdate}  user={this.state.userData} item={item} key={key}  />
                              );
                           })
                            : 
                            <div className="emptyNews">{Trans.empty_news_alert}</div> 
                          }    
                         </div>

                    </SwipeableViews>
                        }
                        
                        
                        
                        
                    
              </div>
                  : ''}
                
                </div>
                
                    );
    }
};

export default App;