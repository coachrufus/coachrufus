<html>
  <head>
    <meta charset="utf-8">
    <title>React.js using NPM, Babel6 and Webpack</title>
      <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,700|Roboto:400,700&amp;subset=cyrillic" rel="stylesheet">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link media="all" rel="stylesheet" href="/dev/theme/bootstrap/css/bootstrap.grid.css" />
     <link media="all" rel="stylesheet" href="public/styles/index.css?ver=<?php echo time() ?>" />
  </head>
  <body>

    
    <div id="homewall" data-uid="3" data-action="addComment" data-tab="team" data-post-id="138" data-lang="sk"></div>
    <script src="public/bundle.js?ver=<?php echo rand(1,100) ?>" type="text/javascript"></script>
  </body>
</html>