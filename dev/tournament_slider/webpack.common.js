const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const Dotenv = require('dotenv-webpack');

/*
module.exports = {
  entry: {'page':'./src/page/homepage.js'},
  plugins: [
    new HtmlWebpackPlugin({
      title: 'Output Management',
      template: './src/page/templates/homepage.twig',
      inject: 'body',
      hash: true
    }),
  ],
  output: {
    filename: 'js/[name].js',
    path: path.resolve(__dirname, 'dist'),
    clean: true,
  }
};
*/

/*
module.exports = {
  entry: ['./src/page/strava.js'],
  plugins: [
    new HtmlWebpackPlugin({
      title: 'Output Management',
      template: './src/page/templates/strava.twig',
      inject: 'body',
      hash: true
    }),
  ],
  output: {
    filename: 'js/[name]bundle.js',
    path: path.resolve(__dirname, 'dist'),
    clean: true,
  }
};
*/



module.exports = {
  entry: {slider:'./src/index.js'},
  plugins: [
    new HtmlWebpackPlugin({
      title: 'Slider',
      template: './src/slider.html',
      filename:'index.html',
      inject: 'body',
      hash: true
    }),
  ],
  output: {
    filename: 'js/[name].js',
    path: path.resolve(__dirname, 'dist'),
    clean: true,
  }
};


