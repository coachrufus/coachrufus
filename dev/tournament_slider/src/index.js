import './css/main.scss';
import { API_URL } from './config.js';
var $ = require( "jquery" );

//$('#slider').css('color','red');
const tournamentId = $('#slider').attr('data-tid');
const eventId1 = $('#slider').attr('data-event1');
const eventId2 = $('#slider').attr('data-event2');
const eventCurrentDate = $('#slider').attr('data-event-current-date');
const lid1 = $('#slider').attr('data-lid1');
const lid2 = $('#slider').attr('data-lid2');

let activeIndex = 1;
let nextIndex;
let itemsCount = $('.slide').length;
let lastEventKey = '';
let lastEventTime =  Date.parse(eventCurrentDate+' 00:00:00');

//check timeline
//http://app.coachrufus.lamp/api/content/game/timeline?apikey=app@40101499408386491&user_id=undefined&event_id=7475&event_current_date=2022-05-04&lid=7311


function checkTimeline(eventId,lid){
    fetch(API_URL+"/api/content/game/timeline?apikey=app@40101499408386491&event_id="+eventId+"&event_current_date="+eventCurrentDate+"&lid="+lid)
.then(response => response.json())
.then(data => {

    $('#game_status_'+eventId).find('.first_line_goals').html(data.match_result.first_line);
    $('#game_status_'+eventId).find('.first_line_team').html(data.match_result.first_line_name);
    $('#game_status_'+eventId).find('.second_line_goals').html(data.match_result.second_line);
    $('#game_status_'+eventId).find('.second_line_team').html(data.match_result.second_line_name);
    
    const timelineKeys = Object.keys(data.timeline_items);
        timelineKeys.forEach((key, index) => {
            const item = data.timeline_items[key];
            if('hit' == item.type)
            {
                //console.log(eventCurrentDate+' '+item.goal.time); 
    
                const hitDateTime =    Date.parse(eventCurrentDate+' '+item.goal.time);
                if(hitDateTime > lastEventTime || true)
                {
                    lastEventTime = hitDateTime;

                    $('#hit_match_'+eventId).find('.goal_author_number').html(item.goal.player_number);
                    $('#hit_match_'+eventId).find('.goal_author_name').html(item.goal.player);
                    $('#hit_match_'+eventId).find('.goal_school').html(item.lineup_name);

                   console.log('adfas');
                    $('#hit_match_'+eventId).css('top','0');
                    
                    window.setTimeout(function(){
                        $('#hit_match_'+eventId).css('top','-10000px');
                    }, 5000);
                    
                }
            }
         });
});
}

//checkTimeline();






//load events
/*
fetch(API_URL+"/api/tournament/events?apikey=app@40101499408386491&user_id=3&tid="+tournamentId)
.then(response => response.json())
.then(data => {
    console.log(data );
    data.data.forEach(event => {
        if(event.id === eventId)
        {
            $('#final_result').html(event.lineup.match_result.first_line+':'+event.lineup.match_result.second_line);
        }

        $('#schedule').append('<li>'+event.name+'T:'+event.start+'</li>');
    });
});
*/


//load stats
fetch(API_URL+"/api/tournament/players-stats?apikey=app@40101499408386491&user_id=3&tid="+tournamentId)
.then(response => response.json())
.then(data => {

var topPlayers = findTopPlayers(data);
var topPlayerPosition = 0;
topPlayers.forEach(player => {
    topPlayerPosition++;
    $('#top_players').append('<tr><td>'+topPlayerPosition+'.'+player.name+'</td><td>'+player.points+'</td></tr>');
 });

 var topGoalPlayers = findTopGoalPlayers(data);
 var topGoalPlayerPosition = 0;
 topGoalPlayers.forEach(player => {
    topGoalPlayerPosition++;
    $('#top_goal_players').append('<tr><td>'+topGoalPlayerPosition+'.'+player.name+'</td><td>'+player.goal+'</td></tr>');
 });

 var topAssistPlayers = findTopAssistPlayers(data);
 var topAssistPlayerPosition = 0;
 topAssistPlayers.forEach(player => {
    topAssistPlayerPosition++;
    $('#top_assist_players').append('<tr><td>'+topAssistPlayerPosition+'.'+player.name+'</td><td>'+player.assist+'</td></tr>');
 });

});

function findTopPlayers(data)
{
    data.data.sort(function (a, b) {
        return (b.points) - (a.points) ;
      });
      var topPlayers =  data.data.slice(0,5);
      return topPlayers;
}

function findTopGoalPlayers(data)
{
    data.data.sort(function (a, b) {
        return (b.goal) - (a.goal) ;
      });
      var topPlayers =  data.data.slice(0,5);
      return topPlayers;
}

function findTopAssistPlayers(data)
{
    data.data.sort(function (a, b) {
        return (b.assist) - (a.assist) ;
      });
      var topPlayers =  data.data.slice(0,5);
      return topPlayers;
}
    


function slide() {
    activeIndex++;

    if(activeIndex > itemsCount)
    {
        activeIndex = 1;
    }
    nextIndex = activeIndex;

    $('.active').fadeOut();
    $('.active').removeClass('active');
    $('.slide:nth-child('+nextIndex+')').addClass('active');
    $('.active').fadeIn();
    //$('.slide:nth-child('+nextIndex+')').addClass('active');

    /*
    $('.active').addClass('fadout');
    $('.slide:nth-child('+nextIndex+')').addClass('active');
    window.setTimeout(function(){
        $('.fadout').removeClass('active');
        $('.fadout').removeClass('fadout');
    }, 1000);
    */

    
}



checkTimeline(eventId1,lid1);
checkTimeline(eventId2,lid2);


setInterval(slide, 3000);

setInterval( function() { checkTimeline(eventId1,lid1); }, 3000 );
setInterval( function() { checkTimeline(eventId2,lid2); }, 3000 );



/*
$('.slide').on('click',function(){
    $(this).removeClass('active');
})
*/