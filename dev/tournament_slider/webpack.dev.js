const path = require('path');
const { merge } = require('webpack-merge');
const common = require('./webpack.common.js');
const CopyPlugin = require("copy-webpack-plugin");
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const TerserPlugin = require("terser-webpack-plugin");
const Dotenv = require('dotenv-webpack');
module.exports =merge(common,{
  mode: 'development',
  devtool: 'inline-source-map',
  devServer: {
    static: './dist',
    devMiddleware: {
      writeToDisk: true,
    },
  },

  plugins: [
    
    new MiniCssExtractPlugin({
      filename: "[name].css", // relative to output.path
    }),
    new Dotenv({
      path: './.env.dev', // Path to .env file (this is the default)
    }),
    
  ],
  module: {
    rules: [
      {
        test: /\.twig$/,
        use: [
          'raw-loader',
          {
            loader: 'twig-html-loader',
            options: {
              namespaces: {
                layouts: path.resolve(__dirname, './src/templates'),
              }
            }
          }
        ]
      },
      {
        test: /\.(sass|css|scss)$/,
        use: [
          'style-loader', //for dev env, MiniCssExtractPlugin is only for prod
          'css-loader',
          {
            loader: "postcss-loader",
            options: {
              postcssOptions: {
                plugins: [
                  [
                    "postcss-preset-env",
                    {
                      // Options
                    },
                  ],
                ],
              },
            },
          },
          'sass-loader',
        ]
      },
      
      {
        test: /\.(png|svg|jpg|jpeg|gif)$/i,
        type: 'asset/resource',
        generator: {
          filename: 'img/[name][ext]'
        }
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/i,
        type: 'asset/resource',
        generator: {

          filename: 'font/[hash][ext][query]'
 
        }
      }
     
    ],
  },
});

