const path = require('path');
const { merge } = require('webpack-merge');
const common = require('./webpack.common.js');
const CopyPlugin = require("copy-webpack-plugin");
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const PurgeCSSPlugin = require('purgecss-webpack-plugin');
const TerserPlugin = require("terser-webpack-plugin");
const Dotenv = require('dotenv-webpack');
const glob = require('glob');
const PATHS = {
  src: path.join(__dirname, 'src')
}


module.exports =merge(common,{
  mode: 'production',
  optimization: {
    minimize: true,
    minimizer: [new TerserPlugin()],
    usedExports: true, // <- remove unused function
  },
  plugins: [
    
    new MiniCssExtractPlugin({
      filename: "[name].css", // relative to output.path
    }),
    
    new PurgeCSSPlugin({
      paths: glob.sync(`${PATHS.src}/**/*`,  { nodir: true }),
    }),

    new Dotenv({
      path: './.env.prod', // Path to .env file (this is the default)
    }),
    
   
    
  ],
  module: {
    rules: [
      {
        test: /\.twig$/,
        use: [
          'raw-loader',
          {
            loader: 'twig-html-loader',
            options: {
              namespaces: {
                layouts: path.resolve(__dirname, './src/templates'),
              }
            }
          }
        ]
      },
      {
        test: /\.(sass|css|scss)$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          {
            loader: "postcss-loader",
            options: {
              postcssOptions: {
                plugins: [
                  [
                    "postcss-preset-env",
                    {
                      // Options
                    },
                  ],
                ],
              },
            },
          },
          'sass-loader',
        ],
        generator: {
          filename: 'css/[hash][ext][query]'
        }
      },
      
    
      {
        test: /\.(png|svg|jpg|jpeg|gif)$/i,
        type: 'asset/resource',
        generator: {
          filename: 'img/[name][ext]'
        }
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/i,
        type: 'asset/resource',
        generator: {

          filename: 'font/[hash][ext][query]'
 
        }
      }
     
    ],
  },
});

