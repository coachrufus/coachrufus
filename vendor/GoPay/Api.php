<?php

namespace GoPay;

class Api {

    
    //PROD
    
    private $clientId = '1074080139';
    private $clientSecret = 'XN4T4WMv';
    private $url = 'https://gate.gopay.cz/api';
    private $goid = '8054685606';
    
    
    /*
    private $clientId = '1555151026';
    private $clientSecret = '6Gmu5B2E';
    private $url = 'https://gw.sandbox.gopay.com/api';
    private $goid = '8439859384';
    */
    public function getClientId()
    {
        return $this->clientId;
    }

    public function getClientSecret()
    {
        return $this->clientSecret;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function getGoid()
    {
        return $this->goid;
    }

    public function setClientId($clientId)
    {
        $this->clientId = $clientId;
    }

    public function setClientSecret($clientSecret)
    {
        $this->clientSecret = $clientSecret;
    }

    public function setUrl($url)
    {
        $this->url = $url;
    }

    public function setGoid($goid)
    {
        $this->goid = $goid;
    }

    public function getAuthCode()
    {

        $client_id = $this->getClientId();
        $client_secret = $this->getClientSecret();
        $authCode = base64_encode($client_id . ':' . $client_secret);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->getUrl()."/oauth2/token");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/x-www-form-urlencoded',
            'Accept: application/json',
            'Authorization: Basic ' . $authCode,
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, 'grant_type=client_credentials&scope=payment-all');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);


        $response = curl_exec($ch);
        curl_close($ch);
        $token = json_decode($response);

        return $token->access_token;
    }

    public function createPayment($paymentData)
    {
        $authCode = $this->getAuthCode();
        
        


        $returnUrl = WEB_DOMAIN . '/pay/gopay/success-payment';
        if (array_key_exists('return_url', $paymentData))
        {
            $returnUrl = WEB_DOMAIN . $paymentData['return_url'];
        }

        $gopayConfig = [
            'target' => [
                'type' => 'ACCOUNT',
                'goid' => $this->getGoid(),
            ],
            'payer' => [
                'default_payment_instrument' => 'PAYMENT_CARD',
                'allowed_payment_instruments' => ['PAYMENT_CARD', 'GOPAY', 'PAYPAL'],
                'contact' => [
                    'first_name' => $paymentData['contact']['first_name'],
                    'last_name' => $paymentData['contact']['last_name'],
                    'email' => $paymentData['contact']['email'],
                    'phone_number' => $paymentData['contact']['phone_number'],
                    'city' => $paymentData['contact']['city'],
                    'street' => $paymentData['contact']['street'],
                    'postal_code' => $paymentData['contact']['postal_code'],
                    'country_code' => $paymentData['contact']['country_code'],
                    ],
            ],
            'amount' => $paymentData['price'],
            'currency' => 'EUR',
            'order_number' => $paymentData['order_number'],
            'order_description' => 'PRO',
            'items' => [
                    ['name' => 'PRO', 'amount' => $paymentData['price']],
            ],
            'callback' => [
                'return_url' => $returnUrl,
                'notification_url' => WEB_DOMAIN . '/pay/gopay/notify-payment'
            ],
            'lang' => $paymentData['lang']
        ];




        if (1 == $paymentData['renew_payment'])
        {
            $gopayConfig['recurrence'] = [
                'recurrence_cycle' => $paymentData['renew_cycle'],
                'recurrence_period' => $paymentData['renew_period'],
                'recurrence_date_to' => '2099-12-31'
            ];
        }

        if (1 == $paymentData['on_demand_renew_payment'])
        {
            $gopayConfig['recurrence'] = [
                'recurrence_cycle' => 'ON_DEMAND',
                'recurrence_date_to' => '2099-12-31'
            ];
        }

        if (array_key_exists('additional_params', $paymentData))
        {
            $gopayConfig['additional_params'] = $paymentData['additional_params'];
        }
        $data = $this->encodeData('application/json', $gopayConfig);


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->getUrl()."/payments/payment");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Accept: application/json',
            'Authorization: Bearer  ' . $authCode,
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);


        $response = curl_exec($ch);
        curl_close($ch);

        $result = json_decode($response);
        return $result->gw_url;
    }

    public function cancelRecurrence($id)
    {
        $authCode = $this->getAuthCode();
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->getUrl()."/payments/payment/" . $id . "/void-recurrence");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/x-www-form-urlencoded',
            'Accept: application/json',
            'Authorization: Bearer  ' . $authCode,
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        //curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);


        $response = curl_exec($ch);
        curl_close($ch);

        $result = json_decode($response);
        return $result;
    }

    public function createRecurrencePayment($paymentData)
    {
        $authCode = $this->getAuthCode();
        $gopayConfig = [
            'amount' => $paymentData['price'],
            'currency' => 'EUR',
            'order_number' => $paymentData['order_number'],
            'order_description' => 'PRO',
            'items' => [
                    ['name' => 'PRO', 'amount' => $paymentData['price']],
            ]
        ];



        $data = $this->encodeData('application/json', $gopayConfig);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->getUrl()."/payments/payment/" . $paymentData['id'] . "/create-recurrence");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Accept: application/json',
            'Authorization: Bearer  ' . $authCode,
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);


        $response = curl_exec($ch);
        curl_close($ch);

        $result = json_decode($response);
        return $result;
    }

    public function getPaymentInfo($id)
    {
        $authCode = $this->getAuthCode();

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->getUrl()."/payments/payment/" . $id);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/x-www-form-urlencoded',
            'Accept: application/json',
            'Authorization: Bearer  ' . $authCode,
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        //curl_setopt($ch, CURLOPT_POST, TRUE);
        //curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);


        $response = curl_exec($ch);
        curl_close($ch);

        $result = json_decode($response);
        return $result;
    }

    private function encodeData($contentType, $data)
    {
        if ($data)
        {
            $encoder = $contentType == 'form' ? 'http_build_query' : 'json_encode';
            return $encoder($data);
        }
        return '';
    }

}
