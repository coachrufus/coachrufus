<?php namespace Core\Templates;

use Core\Types\StringUtils;

class Regions
{
    private static function parseRegion($line, $pos)
    {
        return [
            trim(substr($line, 1, $pos - 1)),
            trim(substr($line, $pos + 1))
        ];
    }
    
    static function parse($string)
    {
        $regions = []; $defaultRegion = [];
        foreach (StringUtils::toLines($string) as $line)
        {
            $trimmedLine = trim($line);
            $pos = strpos($trimmedLine, ':');
            if (strpos($trimmedLine, '@') === 0 && $pos !== false && strpos($trimmedLine, '@media') === false)
            {
                list($name, $value) = self::parseRegion($trimmedLine, $pos);
                $regions[$name] = $value;
            }
            else $defaultRegion[] = $line;
        }
        return $regions + ['default' => implode("\n", $defaultRegion)];
    }
}