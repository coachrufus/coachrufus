<?php
namespace Core;

/**
 * Localizatiaon service, used for customization related to specific market/country 
 */

class Localizer {
    
    private $dateFormat = 'd.m.Y';
    private $timeFormat = 'H:i';
    
    public  function getDateFormat() {
return $this->dateFormat;
}

public  function getTimeFormat() {
return $this->timeFormat;
}

public  function setDateFormat($dateFormat) {
$this->dateFormat = $dateFormat;
}

public  function setTimeFormat($timeFormat) {
$this->timeFormat = $timeFormat;
}


    
    public function formatDate(\DateTime $date)
    {
        return  $date->format($this->dateFormat);
    }
    
     public function formatTime(\DateTime $date)
    {
        return  $date->format($this->timeFormat);
    }
}
