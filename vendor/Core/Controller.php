<?php namespace Core;

use Core\Types\StringUtils;

class Controller
{
    private function getTemplatePath($viewLocation)
    {
        list($moduleName, $viewFolder, $viewFile) = explode(':', $viewLocation);
        $module = new $moduleName();
        return "{$module->getBaseDir()}/view/{$viewFolder}/{$viewFile}";
    }
    
    private function renderPhp($path, $params)
    {
        $layout = \Core\ServiceLayer::getService('layout');
        $request =  ServiceLayer::getService('request');
        if(null != $request->get('platform') )
        {

                $iphone = strpos($request->get('platform'), 'iPhone');
                $iphone10 = strpos($request->get('platform'), 'iPhone10');
                if($iphone10 !== false)
                {
                    $_SESSION['api']['platform'] = 'iphone';
                }
                elseif($iphone !== false)
                {
                    $_SESSION['api']['platform'] = 'iphone';
                }
                else
                {
                    $_SESSION['api']['platform'] = 'android';
                }
            
            
           
        }
        

        
        
        if('mal' == $request->get('lt'))
        {
            $_SESSION['api']['layout'] = 'base';
            $layout->setTemplate( GLOBAL_DIR.'/templates/WebviewContentLayout.php');
        }
        elseif(array_key_exists('api', $_SESSION) && array_key_exists('layout', $_SESSION['api']))
        {
            $layout->setTemplate( GLOBAL_DIR.'/templates/WebviewContentLayout.php');
        }
        
        ob_start();
        $layout->includePart($path, $params + [
            'translator' => \Core\ServiceLayer::getService('translator'),
            'router' => \Core\ServiceLayer::getService('router'),
            'layout' => $layout,
            'root' => ServiceLayer::getService('request')->getRoot(),
        ]);
        return ob_get_clean();
    }
    
    private function renderLatte($path, $params)
    {
        $latte = new \Latte\Engine;
        $latte->setTempDirectory(LATTE_TEMP_DIR);
        $translator = ServiceLayer::getService('translator');
        $latte->addFilter('translate', function ($value) use($translator)
        {
            return $translator->translate($value);
        });
        return $latte->renderToString($path, [
            'layout' => \Core\ServiceLayer::getService('layout'),
            'router' => \Core\ServiceLayer::getService('router'),
            'root' => ServiceLayer::getService('request')->getRoot()
        ] + $params);
    }
    
    public function renderTemplate($path, $params)
    {
        $this->preExecute();
        if (StringUtils::endsWith($path, '.php'))
        {
            $content = $this->renderPhp($path, $params);
        }
        elseif (StringUtils::endsWith($path, '.latte'))
        {
            $content = $this->renderLatte($path, $params);
        }
        $response = new ControllerResponse();
        $response->setStatus('success');
        $response->setContent($content);
        $response->setType('text/html');
        return $response;
    }
    
    private function renderDefaultTemplate($params)
    {
        $reflector = new \ReflectionClass($this);
        $dir = dirname($reflector->getFileName()) . '/../view';
        return function($controllerName, $actionName) use($dir, $params)
        {
            foreach (['.php', '.latte'] as $ext)
            {
                $path = "{$dir}/{$controllerName}/{$actionName}{$ext}";
                if (is_file($path)) return $this->renderTemplate($path, $params);
            }
        };
    }
    
    private function getViewArgs($args)
    {
        $viewLocation = null;
        $params = [];
        $options = null;
        foreach ($args as $arg)
        {
            if (is_string($arg)) $viewLocation = $arg;
            elseif ($arg instanceOf RenderOptions) $options = $arg;
            else $params = $arg;
        }
        return [$viewLocation, $params, $options];
    }
    
    private function applyOptions(RenderOptions $options)
    {
        if (isset($options->layout) && $options->layout === false)
        {
            ServiceLayer::getService('layout')->setTemplate(null);
        }
    }
    
    public function render(...$args)
    {
        list($viewLocation, $params, $options) = $this->getViewArgs($args);
        if (!is_null($options)) $this->applyOptions($options);
        return is_null($viewLocation) ?
            $this->renderDefaultTemplate($params) :
            $this->renderTemplate($this->getTemplatePath($viewLocation), $params);
    }
    
    public function getRequest()
    {
        return ServiceLayer::getService('request');
    }
    
    public function getTranslator()
    {
        return ServiceLayer::getService('translator');
    }
    public function getLocalizer()
    {
        return ServiceLayer::getService('localizer');
    }
    
    public function getRouter()
    {
        return ServiceLayer::getService('router');
    }
    
    protected function preExecute()
    {
    }
}

class RenderOptions
{
    private $options;
    
    function __construct($options)
    {
        $this->options = $options;
    }
    
    function __isset($name) { return isset($this->options[$name]); }
    function __get($name) { return $this->options[$name]; }
    
    function toArray() { return $this->options; }
}