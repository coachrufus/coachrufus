<?php namespace CR\Pay\Model;

use Core\StringUtils;
use Core\Types\DateTimeEx;

class PackageManager
{
    private $packageRepository;
    
    function __construct(PackageRepository $packageRepository)
    {
        $this->packageRepository = $packageRepository;
    }
    
    public function getPackageProductList()
    {
        $result = [];
        foreach ($this->packageRepository->getPackageProductList() as $row)
        {
            $packageId = (int)$row['package_id'];
            $productId = (int)$row['product_id'];
            $result[$packageId][$productId] = iterator_to_array($row);
        }
        return $result;
    }
    
    private function addPackageProducts(&$package, $packageProducts)
    {
        $packageId = (int)$package['id'];
        $package['products'] = $packageProducts[$packageId];
    }
    
    function getPackageList()
    {
        $packageProducts = $this->getPackageProductList();
        $result = [];
        foreach ($this->packageRepository->getPackageList() as $row)
        {
            $package = iterator_to_array($row);
            $this->addPackageProducts($package, $packageProducts);
            $result[] = $package;
        }
        return $result;
    }
    
    function getPackage(int $id)
    {
        return $this->packageRepository->getPackage(function($package) use($id)
        {
            return $package->where('id', $id);
        });
    }
    
    function addPackageProductsKeyName(&$package, $products)
    {
        foreach ($package['products'] as &$packageProduct)
        {
            $product = $products[$packageProduct['product_id']];
            $packageProduct['key'] = $product['key'];
            $packageProduct['name'] = $product['name'];
        }
    }

    function getPackageWithProducts(int $id, $products = null)
    {
        $packageProducts = $this->getPackageProductList();
        $package = iterator_to_array($this->getPackage($id));
        $this->addPackageProducts($package, $packageProducts);
        if (!is_null($products))
        {
            $this->addPackageProductsKeyName($package, $products);
        }
        return $package;
    }

    function getProducts()
    {
        return iterator_to_array((function ()
        {
            foreach ($this->packageRepository->getProducts() as $product)
            {
                $product['name'] = StringUtils::mb_ucfirst($product['name']);
                yield $product['id'] => $product;
            }
        })());
    }
}