<?php namespace CR\Pay\Model;

use Core\Types\DateTimeEx;
use Core\ActiveTeamManager;
use Core\ServiceLayer;

class ActualPackageManager
{
    const actualPackageKey = 'actualPackage';
    private $activeTeamManager;
    private $actualPackageRepository;
    
    function __construct
    (
        ActiveTeamManager $activeTeamManager,
        ActualPackageRepository $actualPackageRepository
    )
    {
        $this->activeTeamManager = $activeTeamManager;
        $this->actualPackageRepository = $actualPackageRepository;
    }

    function getActualPackage()
    {
        
         return new ActualPackage($this->actualPackageRepository->getActualPackageInfo(0));
        //return isset($_SESSION[self::actualPackageKey]) ? ActualPackage::fromString($_SESSION[self::actualPackageKey]) : null;
    }

    function checkPermissions()
    {
       /*
        $activeTeam = $this->activeTeamManager->getActiveTeam();
        if ($this->hasActualPackage() && !is_null($activeTeam))
        {
            $actualPackage = $this->getActualPackage();
            $activeTeamId = $activeTeam['id'];
            if (!is_null($actualPackage->player_limit) && $this->getTeamPlayerCount($activeTeamId) > $actualPackage->player_limit)
            {
                ServiceLayer::getService('request')->redirect("/team/manage-players/{$activeTeamId}");
            }
        }
        else
        {
            $user = ServiceLayer::getService('security')->getIdentity()->getUser();
            ServiceLayer::getService('request')->redirect("/player/dashboard/{$user->getId()}");
        }
        * 
        */
    }

    public function loadActualPackage($teamId)
    {
        //return new ActualPackage($this->actualPackageRepository->getActualPackageInfo($teamId));
         return new ActualPackage($this->actualPackageRepository->getActualPackageInfo(0));
    }

    function saveToSession()
    {
        $activeTeam = $this->activeTeamManager->getActiveTeam();
        
        $actualPackage = $this->loadActualPackage($activeTeam['id']);
        $_SESSION[self::actualPackageKey] = (string)$actualPackage;
    }

    function hasActualPackage(): bool
    {
        return isset($_SESSION[self::actualPackageKey]);
    }

    function removeFromSession()
    {
        unset($_SESSION[self::actualPackageKey]);
    }

    function getTeamPlayerCount(int $teamId): int
    {
        $result = $this->actualPackageRepository->getTeamPlayerCount($teamId);
        return $result === false ? 0 : (is_null($result['teamPlayerCount']) ? 0 : $result['teamPlayerCount']);
    }
}