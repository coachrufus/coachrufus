<?php namespace CR\Pay\Model;

use Core\Db;
use Core\Types\DateTimeEx;

class PackageRepository
{
    use Db;
    
    private $db;
    
    function __construct()
    {
        $this->db = $this->getDb();
    }
    
    function addPackage($package)
    {
        $this->db->packages()->insert($package);
        return $package;
    }
    
    function updatePackage($finder, $package)
    {
        $finder($this->db->packages())->update($package);
        return $package;
    }
    
    function getPackages($finder)
    {
        return $finder($this->db->packages());
    }
    
    function getPackage($finder)
    {
        return $finder($this->db->packages())->fetch();
    }
    
    function getPackageList()
    {
        return $this->db->packages()->where('is_active = 1');
    }
    
    function getPackageProductList()
    {
        return $this->db->package_products()
            ->order('id ASC');
    }
    
    function getProducts()
    {
        $result = [];
        foreach ($this->db->products()->where('is_active = 1')->order('id ASC') as $product)
        {
            $result[(int)$product['id']] = iterator_to_array($product);
        }
        return $result;
    }
}