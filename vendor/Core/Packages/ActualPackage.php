<?php namespace CR\Pay\Model;

use Core\Types\DateTimeEx;
use Core\ServiceLayer;

class PackageProducts implements \ArrayAccess, \Iterator
{
    private $items;
    function __construct(array $products)
    {
        $this->items = [];
        foreach ($products as $product)
        {
            if(array_key_exists('key', $product))
            {
                $this->items[$product['key']] = $product;
            }
            
        }
    }
    public function offsetSet($offset, $value) { throw new Exception('Invalid operation'); }
    protected function toArray() { return $this->items; }
    function offsetExists($offset) { return isset($this->items[$offset]); }
    function offsetUnset($offset) { throw new Exception('Invalid operation'); }
    function offsetGet($offset) { return $this->items[$offset]; }
    function rewind() { return reset($this->items); }
    function current() { return current($this->items); }
    function key() { return key($this->items); }
    function next() { return next($this->items); }
    function valid() { return key($this->items) !== null; }
    function hasProduct(string $key): bool
    {
        return isset($this[$key]) && (int)$this[$key]['is_enabled'] === 1;
    }
    function __toString()
    {
        $items = [];
        foreach ($this->items as $key => $value) $items[] = [$key, $value];
        return json_encode($items);
    }
}

class ActualPackage
{
    private $package;
    private $products;
    function __construct(array $package)
    {
        $products = $package['products'];
        $this->package = $package;
        $this->products = new PackageProducts($products);
        $this->checkExpiration();
    }

    function checkExpiration()
    {
        return;
        if (!$this->isFree())
        {
            $startDate = $this->getStartDate();
            $endDate = $this->getEndDate();
            if (!is_null($startDate) && !is_null($startDate))
            {
                $now = DateTimeEx::now()->toTimeStamp();
                if (!($now > $startDate->toTimeStamp() && $now < $endDate->toTimeStamp()))
                {
                    $actualPackageManager = ServiceLayer::getService('ActualPackageManager');
                    $actualPackageManager->saveToSession($this);
                }
            }
        }
    }

    function __isset($key) { return $key === "products" || isset($this->package[$key]); }

    function __get($key)
    {
        if ($key === "products")
        {
            return $this->products;
        }
        elseif (array_key_exists($key, $this->package))
        {
            return $this->package[$key];
        }
        else throw new \Exception("Invalid property name: {$key}");
    }

    function isFree() { return !isset($this->package['package_id']); }
    
    public function isPro() 
    { 

        if(array_key_exists('package_id', $this->package) && $this->package['package_id'] == 5)
        {
            return true;
        }
        return false;

        
    }

    function toDays(int $secs): int
    {
        return round($secs / (60 * 60 * 24));
    }

    private function getDateColumnValue($column)
    {
        $date = $this->package[$column];
        return !$date ? null : DateTimeEx::parse($date);
    }

    function getStartDate() { return $this->getDateColumnValue('start_date'); }
    function getEndDate() { return $this->getDateColumnValue('end_date'); }

    function getDuration()
    {
        $startDate = $this->getStartDate();
        if (!$startDate) return null;
        $secs = DateTimeEx::now()->toTimeStamp() - $startDate->toTimeStamp();
        return $this->toDays($secs);
    }

    function getRemains()
    {
        $endDate = $this->getEndDate();
        if (!$endDate) return null;
        $secs = $endDate->toTimeStamp() - DateTimeEx::now()->toTimeStamp();
        return $this->toDays($secs);
    }

    static function fromString($string)
    {
        return new self(json_decode($string, true));
    }

    function toArray() { return $this->package; }

    function __toString()
    {
        return json_encode($this->package);
    }
}