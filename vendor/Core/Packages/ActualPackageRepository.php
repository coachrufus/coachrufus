<?php namespace CR\Pay\Model;

use Core\Db;
use Core\Types\DateTimeEx;

class ActualPackageRepository
{
    use Db;
    
    private $db;
    private $packageManager;

    function __construct(PackageManager $packageManager)
    {
        $this->db = $this->getDb();
        $this->packageManager = $packageManager;
    }

    function getActualPackageInfo($teamId)
    {
        $now = DateTimeEx::now();
        $actualUserPackage = $this->db->user_packages()
            ->where('is_canceled', 0)
            ->where('team_id', $teamId)
            ->where('start_date < ?', $now->toDateTime())
            ->where('end_date > ?', $now->toDateTime())
            ->order('id DESC')
            ->limit(1)
            ->fetch();
        if ($actualUserPackage === false)
        {
            $packageId = $this->db->packages()
                ->select('id')
                ->order('level ASC')
                ->limit(1)
                ->fetch()['id'];
            return $this->packageManager->getPackageWithProducts($packageId, $this->packageManager->getProducts());
        }
        else
        {
            $actualUserPackage = iterator_to_array($actualUserPackage);
            $actualUserPackage['products'] = json_decode($actualUserPackage['products'], true);
            return $actualUserPackage;
        }
    }

    function getTeamPlayerCount($teamId)
    {
        return $this->db->team_players('team_id', $teamId)
            ->select('COUNT(*) as teamPlayerCount')
            ->where('status', 'confirmed')
            ->fetch();
    }
}