<?php

namespace Core;

/**
 * Repository pattern, get data from storage and build entity with factory or convert entity to data for storage
 * @author Marek Hubacek
 *
 */
class Repository {

    protected $storage;
    protected $factory;

    public function __construct($storage, $factory)
    {
        $this->storage = $storage;
        $this->factory = $factory;
    }

    public function getStorage()
    {
        return $this->storage;
    }

    public function setStorage($storage)
    {
        $this->storage = $storage;
    }

    public function getFactory()
    {
        return $this->factory;
    }

    public function setFactory($factory)
    {
        $this->factory = $factory;
    }

    public function getFormChoices($id, $name)
    {
        $choices = array();
        $list = $this->findAll();

        $id_method = 'get' . ucfirst($id);
        $name_method = 'get' . ucfirst($name);

        foreach ($list as $l)
        {
            $choices[$l->$id_method()] = $l->$name_method();
        }



        return $choices;
    }

    public function convertToArray($object)
    {
        $data_mapper = $object->getDataMapperRules();
        $values = array();
        
        foreach ($data_mapper as $key => $props)
        {
            $method_name_parts = explode('_', $key);
            $method_name_parts = array_map('ucfirst', $method_name_parts);
            $method_name = 'get' . implode($method_name_parts);


            if (method_exists($object, $method_name))
            {
                if ('datetime' == $props['type'])
                {
                    $date_object = call_user_func(array($object, $method_name));
                    if (null == $date_object)
                    {
                        $values[$key] = null;
                    }
                    else
                    {
                        $values[$key] = $date_object->format('Y-m-d H:i:s');
                    }
                }
                elseif ('boolean' == $props['type'])
                {
                    $bool_data = call_user_func(array($object, $method_name));
                    if (null !== $bool_data)
                    {
                        $values[$key] = ($bool_data === true) ? 1 : 0;
                    }
                    else
                    {
                        $values[$key] = null;
                    }
                }
               
                elseif ('non-persist' == $props['type'])
                {
                    
                }
                elseif ('non-persist' == $props['type'])
                {
                    
                }
                else
                {
                    $values[$key] = call_user_func(array($object, $method_name));
                }
            }
        }
        
        return $values;
    }

    public function convertToStorageData($object)
    {
        return $this->convertToArray($object);
    }

    public function updateEntityFromArray($entity, $data)
    {
        $this->getFactory()->updateEntityFromArray($entity, $data);
    }

    public function save($object)
    {
        $data = $this->convertToStorageData($object);

        if ($object->getId() == null)
        {
            return $this->storage->create($data);
        }
        else
        {
            return $this->storage->update($object->getId(), $data);
        }
    }

    public function findAll($options = array())
    {
        $data = $this->storage->findAll($options);
        $list = array();
        foreach ($data as $d)
        {
            $object = $this->factory->createEntityFromArray($d);
            //$object->setRepository($this);
            $list[] = $object;
        }

        return $list;
    }

    public function find($id)
    {
        $data = $this->storage->find($id);
        $object = $this->factory->createEntityFromArray($data);
        //$object->setRepository($this);
        return $object;
    }

    public function findBy($criteria, $sort_criteria = array())
    {
        $data = $this->storage->findBy($criteria, $sort_criteria);
        $list = array();
        foreach ($data as $d)
        {
            $object = $this->factory->createEntityFromArray($d);
            $list[] = $object;
        }
        return $list;
    }

    public function findOneBy($criteria, $sort_criteria = array())
    {
        $data = $this->storage->findOneBy($criteria, $sort_criteria);
        $object = $this->factory->createEntityFromArray($data);
        return $object;
    }
    
    public function createObjectFromArray($data)
    {
         $object = $this->factory->createEntityFromArray($data);
         return $object;
    }
    
    public function validateEntityData($data)
    {
         $result = $this->factory->validateEntityData($data);
         return $result;
    }

    public function createObjectList($data)
    {
        $list = array();
        foreach ($data as $d)
        {
            $object = $this->factory->createEntityFromArray($d);
            $list[] = $object;
        }
        return $list;
    }
    
    public function createArrayList($objectList)
    {
        $list = array();
        foreach ($objectList as $object)
        {
            $array = $this->convertToArray($object);
            $list[] = $array;
        }
        return $list;
    }

    public function softDelete($id)
    {
        $this->storage->softDelete($id);
    }

    public function delete($id)
    {
        $this->storage->delete($id);
    }
    
    public function deleteBy($params)
    {
        $this->storage->deleteByParams($params);
    }

}
