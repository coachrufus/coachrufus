<?php
namespace Core;

class RestController extends Controller
{
    function sendContent($data, $type, $status = 'success')
    {
        ServiceLayer::getService('layout')->setTemplate(null);
        $response = new ControllerResponse();
        $response->setStatus($status);
        $response->setContent($data);
        $response->setType($type);
        return $response;
    }
    
    protected function asJson($data, $status = 'success')
    {
        return $this->sendContent(json_encode($data), 'application/json', $status);
    }
    
    function asText($data, $status = 'success')
    {
        return $this->sendContent($data, 'text/plain', $status);
    }
    
    function asHtml($data, $status = 'success')
    {
        return $this->sendContent($data, 'text/html', $status);
    }
	
    function asXml($data, $status = 'success')
    {
        return $this->sendContent($data, 'application/xml', $status);
    }
    
    function asJavascript($data, $status = 'success')
    {
        return $this->sendContent($data, 'application/javascript', $status);
    }
    
    function isAjaxRequest()
    {
        return $_SERVER['HTTP_X_REQUESTED_WITH'] == "XMLHttpRequest";
    }
    
    function httpMethod()
    {
        return $_SERVER['REQUEST_METHOD'];
    }
}