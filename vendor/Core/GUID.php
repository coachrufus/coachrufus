<?php
namespace Core;

class GUID
{
    static function create()
    {
        mt_srand((double)microtime() * 10000);
        $charid = strtoupper(md5(uniqid(rand(), true)));
        return
            substr($charid, 0, 8) . '-' . substr($charid, 8, 4) . '-' .
            substr($charid, 12, 4) . '-' . substr($charid, 16, 4) . '-' .
            substr($charid, 20, 12);
    }
}