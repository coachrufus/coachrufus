<?php
namespace Core;

class ImageTransform 
{
    public function resizeImage($options)
    {
        
        $fileInfo = pathinfo($options['sourceImg']);
        $thumb = \PhpThumbFactory::create($options['sourceImg']);  
        $thumb->adaptiveResize($options['width'], $options['height']);
        $thumb_dir  = $options['targetDir'];
        if(!file_exists($thumb_dir))
        {
                mkdir($thumb_dir,0775,true);
        }
        
        if(array_key_exists('filename', $options))
        {
             $filename = $options['filename'];
        }
        else
        {
             $filename = $fileInfo['filename'].'.'.$fileInfo['extension'];
        }
        
        
       
        $thumb->save($thumb_dir.'/'.$filename);
        
        return array('filename' => $filename, 'full_path' => $thumb_dir.'/'.$filename);
    }
    
    public function crop($options)
    {
        $fileInfo = pathinfo($options['sourceImg']);
        $thumb = \PhpThumbFactory::create($options['sourceImg']);  
        $thumb->adaptiveResize($options['width'], $options['height']);
        $thumb_dir  = $options['targetDir'];
        if(!file_exists($thumb_dir))
        {
                mkdir($thumb_dir,0775,true);
        }
        
        if(array_key_exists('filename', $options))
        {
             $filename = $options['filename'];
        }
        else
        {
             $filename = $fileInfo['filename'].'.'.$fileInfo['extension'];
        }
        
        
       
        $thumb->save($thumb_dir.'/'.$filename);
        
        return array('filename' => $filename, 'full_path' => $thumb_dir.'/'.$filename);
    }
}
