<?php

namespace Core;

class Seo
{
	public function generateSeoString($string) 
    {
        $string = self::createSlug($string);
        
        setlocale(LC_CTYPE, "sk_SK.utf-8");
        $url = $string;
	    $url = preg_replace('~[^\\pL0-9_]+~u', '-', $url);
	    $url = trim($url, "-");
	    $url = iconv("utf-8", "us-ascii//TRANSLIT", $url);
	    $url = strtolower($url);
	    $url = preg_replace('~[^-a-z0-9_]+~', '', $url);
	    return $url;
    }
	
	public  function createSlug($string)
    {
        $string = trim($string);
        
        $string = str_replace(
            array('á','ä','č','ď','é','ě','ë','í','ľ','ĺ','ň','ô','ó','ö','ŕ','ř','š','ś','ť','ú','ů','ü','ý','ž','ź','Á','Ä','Č','Ď','É','Ě','Ë','Í','Ľ','Ĺ','Ň','Ó','Ö','Ô','Ř','Ŕ','Š','Ś','Ť','Ú','Ů','Ü','Ý','Ž','Ź',' ','?','!',','),
            array('a','a','c','d','e','e','e','i','l','l','n','o','o','o','r','r','s','s','t','u','u','u','y','z','z','A','A','C','D','E','E','E','I','L','L','N','O','O','O','R','R','S','S','T','U','U','U','Y','Z','Z','-','-','-','-'),
            $string
        );
        
        $string = strtolower($string);

        $string =  strtr($string, array(
        'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'jo', 'ж' => 'zh', 'з' => 'z', 'и' => 'i', 'й' => 'jj', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'kh', 'ц' => 'c', 'ч' => 'ch', 'ш' => 'sh', 'щ' => 'shh', 'ъ' => '', 'ы' => 'y', 'ь' => '', 'э' => 'eh', 'ю' => 'ju', 'я' => 'ja',
        'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G', 'Д' => 'D', 'Е' => 'E', 'Ё' => 'JO', 'Ж' => 'ZH', 'З' => 'Z', 'И' => 'I', 'Й' => 'JJ', 'К' => 'K', 'Л' => 'L', 'М' => 'M', 'Н' => 'N', 'О' => 'O', 'П' => 'P', 'Р' => 'R', 'С' => 'S', 'Т' => 'T', 'У' => 'U', 'Ф' => 'F', 'Х' => 'KH', 'Ц' => 'C', 'Ч' => 'CH', 'Ш' => 'SH', 'Щ' => 'SHH', 'Ъ' => '', 'Ы' => 'Y', 'Ь' => '', 'Э' => 'EH', 'Ю' => 'JU', 'Я' => 'JA',
    ));
    
        return $string;
    }

    public function removeDiacritic($string)
    {
        $string = trim($string);
        
        $string = str_replace(
            array('á','ä','č','ď','é','ě','ë','í','ľ','ĺ','ň','ô','ó','ö','ŕ','ř','š','ś','ť','ú','ů','ü','ý','ž','ź','Á','Ä','Č','Ď','É','Ě','Ë','Í','Ľ','Ĺ','Ň','Ó','Ö','Ô','Ř','Ŕ','Š','Ś','Ť','Ú','Ů','Ü','Ý','Ž','Ź'),
            array('a','a','c','d','e','e','e','i','l','l','n','o','o','o','r','r','s','s','t','u','u','u','y','z','z','A','A','C','D','E','E','E','I','L','L','N','O','O','O','R','R','S','S','T','U','U','U','Y','Z','Z'),
            $string
        );
        
        return $string;
        
    }

    public function diakritikaISO($text){
        return strtr($text, 'áäčďéěëíµĺňôóöŕřą»úůüýľÁÄČĎÉĚËÍĄĹŇÓÖÔŘŔ©«ÚŮÜÝ®',
                            'aacdeeeillnooorrstuuuyzAACDEEEILLNOOORRSTUUUYZ');

    }
}

