<?php namespace Core;

class Pipe
{
    private $value;
    private $context;
    
    protected function __construct($value, $context)
    {
        $this->value = $value;
        $this->context = $context;
    }
    
    static function for($value, $context = null)
    {
        return new self($value, $context);
    }
    
    function use($context = '')
    {
        return new self($this->value, $context);
    }
    
    function call()
    {
        $args = func_get_args();
        $func = array_shift($args);
        array_unshift($args, $this->value);
        return new self(call_user_func_array("{$this->context}{$func}", $args), $this->context);
    }

    function __call($name, $args)
    { 
        array_unshift($args, $name);
        return call_user_func_array(array($this, 'call'), $args);
    }

    function fetch() { return $this->value; }
}