<?php
/**
 * @package Core
 */

namespace Core;

class Form {
	
	/**
	 * mapping poli
	 * @var array
	 */
	protected $fields;
	
	/**
	 * Nazov formularu
	 * @var string
	 */
	protected $name = 'record';
	
	/**
	 * Mapovana entita
	 * @var object
	 */
	protected $entity;
	
	/**
	 * Priznak ci moze formular spracovat aj polia, ktore nie su nadefinvovane v fields, sluzi hlavne ako bezpecnostna kontrola voci paraziticky formularom
	 * @var unknown
	 */
	protected $allowExtraFields = false;
        
	
	//private $csfr;
	
	function __construct() 
	{
		//$this->csfr = md5(time().rand(1,10));
	}

	public function getFields()
	{
		return $this->fields;
	}

	public function setFields($fields)
	{
	    $this->fields = $fields;
	}
	
	public function setField($index,$val)
	{
		$this->fields[$index] = $val;
	}
	
	public function getField($index)
	{
		return $this->fields[$index];
	}
	
	public function setFieldValue($index,$value)
	{
	    $this->fields[$index]['value'] = $value;
	}
        
        public function setFieldChoices($index,$choices)
        {
            $this->fields[$index]['choices'] = $choices;
        }
        
        public function getFieldChoices($index)
        {
            return $this->fields[$index]['choices'];
        }
        
        public function getFieldRequired($index)
        {
            if(array_key_exists('required', $this->fields[$index]))
            {
                return $this->fields[$index]['required'];
            }
            
        }
        
         public function setFieldOption($index,$option_index,$value)
        {
            $this->fields[$index][$option_index] = $value;
        }
	
	public function getFieldValue($index)
	{
		$value = null;
	    if(array_key_exists('value', $this->fields[$index]))
	    {
	    	if(is_a($this->fields[$index]['value'], 'Datetime'))
	    	{
	    		if( array_key_exists('format', $this->fields[$index]))
                        {
                             $value = $this->fields[$index]['value']->format( $this->fields[$index]['format']);
                        }
                        else 
                        {
                             $value = $this->fields[$index]['value']->format('d.m.Y');
                        }
	    	}
	    	else
	    	{
	    		$value = $this->fields[$index]['value'];
	    	}
	    	
	    	
	    }
	    
	    return $value;
	}

	public function getName()
	{
		return $this->name;
	}
	
	public function setName($name)
	{
		$this->name = $name;
	}
	
	public function getEntity()
	{
		return $this->entity;
	}
	
	public function setEntity($entity)
	{
		$this->entity = $entity;
		if(null == $this->fields)
		{
			$this->fields = $entity->getDataMapperRules();
		}
		$this->updateValuesFromEntity($entity);
	}

	/**
	 * updatne entitu a hodnoty formularu datami
	 * @param array $data
	 */
	public function bindData($data)
	{
		//ak sa jedna o boolean, tak tam treba nastavit 0 v pripade ze sa nechadza v poli
		if(null != $this->fields)
		{
			foreach($this->fields as $key => $field)
			{
				if(!array_key_exists('type', $field))
				{
					//throwException($exception)
					throw new \Exception('Undefined form type for field:'.$key);
				}
				
				
				
				if($field['type'] == 'boolean')
				{
					if(!array_key_exists($key, $data))
					{
						$data[$key] = 0;
					}
				}
			}
		}
		
		foreach ($data as $key => $val)
		{
			if(!array_key_exists($key,$this->fields) && $this->getAllowExtraFields() == false)
			{
				unset($data[$key]);
			}
		}

		$this->updateValues($data);
		$this->updateEntity($data);
	}
	
	/**
	 * Update hodnoty formulara
	 * @param array $data
	 */
	public function updateValues($data)
	{
		foreach ($data as $key => $val)
		{
			if(array_key_exists($key,$this->fields) or $this->getAllowExtraFields() == true)
			{
				$this->setFieldValue($key,$val);
			}
		}
	}
	
	/**
	 * Updatne data formulara z entity
	 * @param mixed $entity
	 */
	public function updateValuesFromEntity($entity)
	{
		$mapper = $entity->getDataMapperRules();
		$mapped_fields = array_change_key_case($this->fields,CASE_LOWER);
		
		//var_dump($mapped_fields);
		
		if(null != $mapper)
		{
			foreach($mapper as $key => $props)
			{
				//echo strtolower($key);
				
				if(array_key_exists(strtolower($key),$mapped_fields))
				{
					$method_name_parts = explode('_',$key);
					$method_name_parts = array_map('ucfirst', $method_name_parts);
					$method_name = 'get'.implode($method_name_parts);
						
					if(method_exists($entity, $method_name))
					{
						$value = call_user_func(array($entity, $method_name));
						$this->setFieldValue($key, $value);
					}
				}
				
				
			}
		}
                
		
	}
        
        public function updateEntityFromArray($entity,$data)
	{
		$rules = $entity->getDataMapperRules();		
		foreach ($data as $key => $val)
		{
			$method_name_parts = explode('_',$key);
			$method_name_parts = array_map('ucfirst', $method_name_parts);
			$method_name = 'set'.implode($method_name_parts);
				
			if(method_exists($entity, $method_name))
			{
				if($rules[$key]['type'] == 'datetime')
				{
					if(null != $val)
					{
						$val = new \DateTime($val);
					}
					else
					{
						//pretypovanie na null kvoli ===
						//$val = null;
					}
				}
				
				
				if($rules[$key]['type'] == 'boolean')
				{
					$val = (1 == $val) ? true : false;
				}
				
				call_user_func_array(array($entity, $method_name), array($val));
			}
		}
	}
	
	/**
	 * Updatne entitu
	 * @param array $data
	 */
	public function updateEntity($data)
	{
		
		foreach ($data as $key => $val)
		{
			if(!array_key_exists($key,$this->fields) && $this->getAllowExtraFields() == false)
			{
				unset($data[$key]);
			}
		}

		$entity = $this->getEntity();
		if(null != $entity)
		{
			$this->updateEntityFromArray($entity,$data);
                        /*
                        $repository = $entity->getRepository();
			if(null != $repository)
			{
				$repository->updateEntityFromArray($entity,$data);
			}
                         * 
                         */
			
		}
		
	}
	
	public function getFieldName($index)
	{
		
                if(array_key_exists('options', $this->fields[$index]) && array_key_exists('name', $this->fields[$index]['options']))
                {
                    $field_name = $this->fields[$index]['options']['name'];
                }
                elseif(array_key_exists('multiple',$this->fields[$index]) && $this->fields[$index]['multiple'] == true)
                {
                     $field_name = $this->getName().'['.$index.'][]';
                }
                else 
                {
                     $field_name = $this->getName().'['.$index.']';
                }
            
               
		return $field_name;
	}
        
        public function getRequiredLabel($index)
        {
            $label = '';
            if($this->getFieldRequired($index) == true)
            {
                $label = '<span class="control-label-required">*</span>';
            }
                
            return $label;
        }

	public function getFieldLabel($index)
	{
		$label = '';
                if(array_key_exists('label', $this->fields[$index]))
		{
			$label =   $this->fields[$index]['label'];
		}
		else
		{
			$label =  ucfirst(str_replace('_', ' ', $index));
		}
                return $label;
	}
        
        public function getFieldHelp($index)
	{
		if(array_key_exists('help', $this->fields[$index]))
		{
			return  $this->fields[$index]['help'];
		}
		else
		{
			return null;
		}
	}
	
	public function getDefaultId($index)
	{
		$name = $this->getName();
		$default_id = $name.'_'.$index;
		return $default_id;
	}
	
	public function renderLabel($index,$options = array())
	{
		if(!array_key_exists('for', $options))
		{
			$options['for'] = $this->getDefaultId($index);
		}
		if(!array_key_exists('id', $options))
		{
			$options['id'] = 'label_'.$index;
		}
		$options_string = $this->getFieldOptionString($index,$options);
		
		$name = $this->getName();
		$label = $this->getFieldLabel($index);
		
		
		
		return '<label'.$options_string.'>'.$label.'</label>';
	}
	
	public function renderInputTag($index,$options = array())
	{
		$options_string = $this->getFieldOptionString($index,$options);

		$value = $this->getFieldValue($index);
                if(array_key_exists('value',$options))
                {
                    $value = $options['value'];
                }
                
                //$fieldOptions = $this->getFieldChoices($index)
                $required = '';

                if($this->getFieldRequired($index) == true)
                {
                    $required = ' required="required" ';
                }
                
                
                $field_name = $this->getFieldName($index)  ;
                if(array_key_exists('name',$options))
                {
                    $field_name = $options['name'];
                }

		return '<input type="text" name="'.$field_name .'" value="'.$value.'"'.$options_string.$required.'/>';
	}
        
        public function renderEmailTag($index,$options = array())
	{
		$options_string = $this->getFieldOptionString($index,$options);

		$value = $this->getFieldValue($index);
                if(array_key_exists('value',$options))
                {
                    $value = $options['value'];
                }
                
                //$fieldOptions = $this->getFieldChoices($index)
                $required = '';

                if($this->getFieldRequired($index) == true)
                {
                    $required = ' required="required" ';
                }
                
                
                $field_name = $this->getFieldName($index)  ;
                if(array_key_exists('name',$options))
                {
                    $field_name = $options['name'];
                }

		return '<input type="email" name="'.$field_name .'" value="'.$value.'"'.$options_string.$required.'/>';
	}
        
         public function renderNumberTag($index,$options = array())
	{
		$options_string = $this->getFieldOptionString($index,$options);

		$value = $this->getFieldValue($index);
                if(array_key_exists('value',$options))
                {
                    $value = $options['value'];
                }
                
                //$fieldOptions = $this->getFieldChoices($index)
                $required = '';

                if($this->getFieldRequired($index) == true)
                {
                    $required = ' required="required" ';
                }
                
                
                $field_name = $this->getFieldName($index)  ;
                if(array_key_exists('name',$options))
                {
                    $field_name = $options['name'];
                }

		return '<input type="number" name="'.$field_name .'" value="'.$value.'"'.$options_string.$required.'/>';
	}
	
	public function renderInputHiddenTag($index,$options = array())
	{
		$options_string = $this->getFieldOptionString($index,$options);
	
		$value = $this->getFieldValue($index);
	
		return '<input type="hidden" name="'. $this->getFieldName($index).'" value="'.$value.'"'.$options_string.'/>';
	}
	
	public function renderRadioTag($index,$default_value,$options = array())
	{
		$options_string = $this->getFieldOptionString($index,$options);

		$value = $this->getFieldValue($index);
		$checked = '';
		if($value === $default_value)
		{
			$checked = ' checked="checked"';
		}

		return '<input type="radio" name="'. $this->getFieldName($index).'" value="'.$default_value.'"'.$checked.$options_string.'/>';
	}
	
	public function renderCheckboxTag($index,$options = array())
	{
		$options_string = $this->getFieldOptionString($index,$options);

		$value = $this->getFieldValue($index);
		$checked = '';
		
		$default_value = '1';
		if(array_key_exists('value', $options))
		{
			$default_value = $options['value'];
		}
		
		
		
		if($value == $default_value)
		{
			$checked = ' checked="checked"';
		}

		return '<input type="checkbox" name="'. $this->getFieldName($index).'" value="'.$default_value.'"'.$checked.$options_string.'/>';
	}
       
	public function renderPasswordTag($index,$options = array())
	{
		$options_string = $this->getFieldOptionString($index,$options);

		$value = $this->getFieldValue($index);

		return '<input type="password" name="'. $this->getFieldName($index).'" value="'.$value.'"'.$options_string.'/>';
	}
	
	public function getFieldOptionString($index,$options)
	{
		$options_string = '';
		
		if(!array_key_exists('id', $options))
		{
			$options['id'] = $this->getDefaultId($index);
		}
		
		ksort($options);
		
		foreach($options as $option_name => $option_value)
		{
			$options_string .= ' '.$option_name.'="'.$option_value.'"';
		}
		return $options_string;
	}
	
	
	public function renderSelectTag($index,$options = array())
	{
		$options_string = '';
		$html_options_string = $this->getFieldOptionString($index,$options);
		$value = $this->getFieldValue($index);
                $field_name =  $this->getFieldName($index);
                
                if(array_key_exists('value',$options))
                {
                    $value = $options['value'];
                }
                
                if(array_key_exists('name',$options))
                {
                    $field_name = $options['name'];
                }
                
                
                
                
		if(array_key_exists('choices', $this->fields[$index]))
		{
			foreach($this->fields[$index]['choices'] as $key => $name)
			{
				if(is_array($name))
                                {
                                        $options_string .= '<optgroup label="'.$key.'">';

                                        foreach($name as $key_val => $val)
                                        {
                                                if($value == $key_val)
                                                {
                                                        $options_string .= '<option selected="selected" value="'.$key_val.'">'.$val.'</option>'."\n";
                                                }
                                                else
                                                {
                                                        $options_string .= '<option value="'.$key_val.'">'.$val.'</option>'."\n";
                                                }
                                        }
                                        $options_string .= '</optgroup>';

                                }
                                else
                                {
                                       if($value == $key)
                                        {
                                                $options_string .= '<option selected="selected" value="'.$key.'">'.$name.'</option>'."\n";
                                        }
                                        else
                                        {
                                                $options_string .= '<option value="'.$key.'">'.$name.'</option>'."\n";
                                        }
                                }
                                
			}
		}
		else
		{
			//TODO throw exception
		}
		
		return '<select name="'. $field_name.'"'.$html_options_string.'>'."\n".$options_string.'</select>';
	}
	
	public function renderTextareaTag($index,$options = array())
	{
		$html_options_string = $this->getFieldOptionString($index,$options);
		$value = $this->getFieldValue($index);
		return '<textarea name="'. $this->getFieldName($index).'" rows="5" cols="5"'.$html_options_string.'>'.$value.'</textarea>';
	}
	
	public function generateForm($folder,$entity_name,$entity)
	{
		$rules = $entity->getDataMapperRules();
		ob_start();
			var_export($rules);
			$form_rules = ob_get_contents();
		ob_end_clean();
		
		
		$form_string = '<?php
namespace  {{NAMESPACE}}\Form;
use \Core\Form as Form;
use  {{NAMESPACE}}\Model\{{ENTITY}} as {{ENTITY}};

class {{ENTITY}}Form extends Form 
{
	protected $fields = '.$form_rules.';
				
	public function __construct() 
	{

	}
}';
		$form_string = str_replace('{{ENTITY}}', $entity_name, $form_string);
		
		
		file_put_contents($folder.'/'.$entity_name.'Form.php',$form_string);
	}

	public function getAllowExtraFields()
	{
	    return $this->allowExtraFields;
	}

	public function setAllowExtraFields($allowExtraFields)
	{
	    $this->allowExtraFields = $allowExtraFields;
	}
}