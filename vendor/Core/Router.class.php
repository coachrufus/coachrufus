<?php
namespace Core;


class Router 
{
	//premmena ktora mi urcuje ci uz som nasiel odpovedajucu cestu
	//ak je nastavena na true tak sa nepokracuje dalej v routingu
	public static $matched = false;
	public static $variables = array();
	public static $_urlDelimiter = '/';
	public static $_urlVariable = ':';
	public static $current_route_name;
	
	/**
	 * Zoznam nadefinovanych routes
	 * @var unknown
	 */
	private $routes = array();
	
	/**
	 * Prida routu do zoznamu
	 * @param string $name
	 * @param string $pattern
	 * @param string $options
	 */
	public function addRoute($name,$pattern,$options)
	{
		$this->routes[$name] = array('pattern' => $pattern, 'options' => $options);
	}
	
	public function getRoutes()
	{
		return $this->routes;
	}
        
        public  function getCurrentRouteName()
        {
            return self::$current_route_name;
        }
    
    /**
     * Vrati uri osekanu od parameterov
     */
    public static function getUrlPath()
    {
        $uri_array = explode('?',$_SERVER['REQUEST_URI']);
        return $uri_array[0];
    }
	
    public static function resetRouteVariables()
    {
        //t_dump('reset');
        //t_dump('Variables: '.implode(',',self::$variables));
        //t_dump('get: '.implode(',',$_GET));
        
        foreach( self::$variables as $key => $val)
        {
            if(array_key_exists($key, $_GET) && $_GET[$key] == $val)
            {
               //t_dump('unset'.$_GET[$key]);
                unset(self::$variables[$key]) ;
                unset($_GET[$key]);
            }
        }
                                
            
    }
    
	
	public static function match($route)
	{
		if(!self::$matched)
		{
			$_urlDelimiter = self::$_urlDelimiter;
			
			
			/*
			 * najprv musim rozparsovat pattern na jednodlive casti
			 */
			 
			//najprv odstranim prve lomitko
			$route = trim($route, $_urlDelimiter);
			//a teraz to dam cele do pola, ktore porovnavam s url
			$route_parts = explode($_urlDelimiter,$route);

			
			/*
			 * a teraz si rozoberem  url
			 */
			 
			//najprv odstranim prve lomitko
			//$url = trim($_SERVER['REQUEST_URI'],$_urlDelimiter);
                        
                        if(ENCODE_URL)
                        {
                             $encodedUrl = substr($_SERVER['REQUEST_URI'],1);
                            $decodedUrl = self::base64UrlDecode($encodedUrl);
                            $url = trim($decodedUrl,$_urlDelimiter);
                        }
                        else
                        {
                            $url = trim(self::getPathInfo(),$_urlDelimiter);
                        }
                       
                        
			//$url = trim(self::getPathInfo(),$_urlDelimiter);
			
			//teraz odstranim  cast adresy za otaznikom
			//$url = str_replace('?'.$_SERVER['QUERY_STRING'],'',$url);
                        $url_to_uri = explode('?',$url);			
			$url = $url_to_uri[0];			

			//a teraz to dam cele do pola, ktore porovnavam s route
			$url_parts = explode($_urlDelimiter,$url);
			
			
			
			//var_dump($url_parts);
			//var_dump($route_parts);
			//echo "<br />";
			
			

			foreach($route_parts as $key => $part)
			{
                //ak sa jedna o hviezdicku - wildcard, a zaroven za nou uz nic nenasleduje, tak potom uz vyhovuje vsetko a zastavim parsovanie
                if($part == '*')
                {
                	self::setMatched();
                	return true;
                }

                //ak je tato cast definovanca ako premmenna tak routa vyhovuje
                //tu nahadazem premenne z routy do pola
                if(substr($part, 0, 1) == self::$_urlVariable)
                {
                	//prepisem nazov premennej z ":nieco" na "nieco""
                	//$var_name = str_replace(self::$_urlVariable,'',$part);
                	//teraz ziskam hodnotu premmenej
                	//ak sa jedna o specialnu routu ktora ma za nazvom premmenej hviezdicku este pred lomitkom
                	//tak potom priradim do premennej vsetko za adresou
                	//:*category_path

                	if(substr($part, 1, 1) == '*')
                	{
                		$var_val = $url;
                		$var_name = str_replace(self::$_urlVariable.'*','',$part);
                		
                		$var_val = str_replace($url_parts[$key-1].'/','',$url);

                		self::$variables[$var_name] = $var_val;
                                if(!array_key_exists($var_name, $_GET))
                                {
                                    $_GET[$var_name] = $var_val;
                                }
                		
                		
                		self::setMatched();
                		return true;
                	}
                	
                	
                	 //ak nema pozadovany tvar tak nevyhovuje
	                if(!array_key_exists($key, $url_parts))
	                {
	                	//echo "<br />";
                             self::resetRouteVariables();
	                	return false;
	                }
                	else
                	{
                		$var_val = $url_parts[$key];
                		$var_name = str_replace(self::$_urlVariable,'',$part);
                	}
                	
                	
                	self::$variables[$var_name] = $var_val;
                         if(!array_key_exists($var_name, $_GET))
                                {
                                    $_GET[$var_name] = $var_val;
                                }
                	//$_GET[$var_name] = $var_val;
                	//self::setMatched();
                	
                	
                	if(count($route_parts) == $key+1)
                	{
                		/*
	                	 * premenne ktore su vytiaihnute z route zapisun napr /test/:nazov-premnnej
	                	 * priradim k premennym Requestu, aby som zjednotil dotazovanie sa na premenne
	                	 * iba cez metodu Request::getParameter()
	                	 */
	                	//Request::addParameters(self::$variables);
                		//return true;
                	}
                	
                }
                //ak nazov routy nesedi s nazvom v url, tak koniec
                elseif(isset($url_parts[$key]) && $part != $url_parts[$key])
                {
                	//echo "false1";
                	//echo "<br />";
                     self::resetRouteVariables();
                	return false;
                }
                //ak nema pozadovany tvar tak nevyhovuje
                elseif(!array_key_exists($key, $url_parts))
                {
                	//echo "false2";
                	//echo "<br />";
                     self::resetRouteVariables();
                	return false;
                }
                //ak je url dlhsia ak routa tak nevyhovuje a zaroven neobsahuje hviezdicku
                /*
                elseif(count($url_parts) > count($route_parts))
                {
                	echo "false3";
                	 echo "<br />";
                	return false;
                }
                */
                //ak vyhovuje, tak ok
                elseif($part == $url_parts[$key])
                {
                	//nerob nic a pokracuj v cykle
                }
               
                
                
				
				//ak sa nerovnaju casti url s castami routy tak potom nevyhovuje
				/*
				if($part != $url_parts[$key])
				{
					return false;
				}
				*/
			}
			
			//ak je url dlhsia ak routa tak nevyhovuje, a taksito naopaka
			if(count($url_parts) > count($route_parts))
            {
            	//echo "false3";
            	//echo "<br />";
                            self::resetRouteVariables();
            	return false;
            }

           
                        self::setMatched();
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public static function setMatched()
	{
		self::$matched = true;
	}
	
	public static function getUrlInfo()
	{
		$_urlDelimiter = self::$_urlDelimiter;
		//$url_parts = explode($_urlDelimiter,trim($_SERVER['REQUEST_URI'], $_urlDelimiter));
		$url_parts = explode($_urlDelimiter,trim(self::getPathInfo(), $_urlDelimiter));
		return $url_parts;
	}
	
	public static function getPathInfo()
	{
            return $_SERVER['REQUEST_URI'];
	}
	
	public static function getParam($name)
	{
		if(array_key_exists($name,self::$variables))
		{
			return self::$variables[$name];
		}
		else
		{
			return null;
		}
	}
	
	public static function getParams()
	{
		return self::$variables;
	}
    
	/**
	 * Vrati ci aktualne matchnuta route vyhoivuje zadanemu menu
	 * @param string $route_name
	 */
    public static function isMatched($route_name)
    {
    	if($route_name == self::$current_route_name)
    	{
    		return true;
    	}
    	return false;
    }
    
    public function link($route_name,$params = array())
    {
    	//rozdeli podla ?
    	$link_parts = explode('?',$route_name);
    	$route_name = $link_parts[0];
    	if(count($link_parts) > 1)
    	{
    		$uri_parts = explode('&', $link_parts[1]);
    		foreach($uri_parts as $key => $uri_pair)
    		{
    			$pair_parts = explode('=',$uri_pair);
    			$params[$pair_parts[0]] = $pair_parts[1];
    		}

    	}

    	
    	
    	if(!array_key_exists($route_name,$this->routes))
    	{
    		throw new \Exception('Undefined route'.$route_name);
    	}
    	$route = $this->routes[$route_name];
    	
    	
    	//dynamicke parametere priradi to url
    	if(!empty($params))
    	{
    		foreach($params as $key => $val)
    		{
    			//$encodedVal = $this->base64UrlEncode($val);
                        $encodedVal= $val;
                        //t_dump('Rioute:'.$val.'='.$encodedVal);
                        if(false !== strpos($route['pattern'], ':'.$key))
    			{
    				$route['pattern'] = str_replace(':'.$key, $encodedVal, $route['pattern']);
    				unset($params[$key]);
    			}
    		}
    	}
        


        if(false !== strpos($route['pattern'], ':'))
        {
           if(array_key_exists('default', $route['options']))
           {
               foreach($route['options']['default'] as $key => $val)
                {
                        $encodedVal= $val;
                        if(false !== strpos($route['pattern'], ':'.$key))
                        {
                                $route['pattern'] = str_replace(':'.$key, $encodedVal, $route['pattern']);
                        }
                }
           }
            
        }

    	
    	$params_string = '';
    	if(!empty($params))
    	{
    		$params_string ='?' . http_build_query($params);
    	}

        if(ENCODE_URL)
        {
            return '/'.$this->base64UrlEncode($route['pattern'].$params_string);
        }
    	else
        {
            return $route['pattern'].$params_string;
        }
    	
    }
    
	public function base64UrlEncode($inputStr)
    {
        return strtr(base64_encode($inputStr), '+/=', '-_,');
    }

    public function base64UrlDecode($inputStr)
    {
        return base64_decode(strtr($inputStr, '-_,', '+/='));
    }
    
    public function generateApiWidgetUrlData($data)
    {
        $data['token']  = substr(base64_encode(sha1(time().rand(1,1000))),0,12) ;
        $requestData['id'] = $data['token'].base64_encode(json_encode($data)) ;
        return $requestData;
    }
    
    public function generateApiWidgetUrl($route_name,$data)
    {
      
        //$data['token']  = sha1(implode('###',$data)).'#'.sha1(time());
        //$data['token']  = substr(base64_encode(sha1(time().rand(1,1000))),0,12) ;
        //$requestData['id'] = $data['token'].base64_encode(json_encode($data)) ;
        $requestData = $this->generateApiWidgetUrlData($data);
        return $this->link($route_name,$requestData);
    }
    
}
?>