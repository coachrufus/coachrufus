<?php namespace Core;

class PlayerPrivacy
{
    use \Core\Db;
    
    private $db;
    
    function __construct()
    {
        $this->db = $this->getDb();
    }
    
    function getPrivacy($playerId, $section)
    {
        return $this->db->player_privacy()
            ->where('player_id', $playerId)
            ->where('section', $section)
            ->fetch();
    }
    
    function setPrivacy($playerId, $section, $value, $userGroup = 'all')
    {
        $privacy = [
            'section' => $section,
            'value' => $value,
            'user_group' => $userGroup,
            'player_id' => $playerId
        ];
        $result = $this->getPrivacy($playerId, $section);
        if ($result)
        {
            if (is_null($userGroup))
            {
                $privacy['user_group'] = $result['user_group'];
            }
            $result->update($privacy);
        }
        else $this->db->player_privacy()->insert($privacy);
    }
    
    function isAllowed($playerId, $section)
    {
        $privacy = $this->getPrivacy($playerId, $section);
        return $privacy === false || intVal($privacy["value"]) == 1;
    }
}