<?php
/**
 * @package Core
 */

namespace Core;

class Logger {
	
	protected $storage;
	protected $logData;
	
	
	
	public function __construct($storage)
	{
		$this->storage = $storage;
	}

    public function getStorage()
    {
        return $this->storage;
    }

    public function setStorage($storage)
    {
        $this->storage = $storage;
    }

    public function update($subject)
    {
    	$user = $subject->getIdentity()->getUser();
    	if($user->getRole() == 'ROLE_PARTNER')
    	{
    		$data['partner_ID'] = $subject->getIdentity()->getUser()->getId();
    	}
    	else
    	{
    		$data['user_ID'] = $subject->getIdentity()->getUser()->getId();
    	}
    	
    	
    	$data['activity'] = $subject->getState();
    	
    	$this->AddRecord($data);
    }

    public function addRecord($data)
    {
    	if(isset($data['user_ID']) && null != $data['user_ID'])
    	{
    		$insert_data['user_ID'] = $data['user_ID'];
    	}
    	
    	if(isset($data['partner_ID']) && null != $data['partner_ID'])
    	{
    		$insert_data['partner_ID'] = $data['partner_ID'];
    	}
    	
    	
    	$insert_data['ip'] = $_SERVER['REMOTE_ADDR'];
    	$insert_data['date'] = date('Y-m-d H:i:s');
    	$insert_data['activity'] = $data['activity'];
    	$insert_data['browser'] = $_SERVER['HTTP_USER_AGENT'];
    	
    	if(isset($data['source_table']))
    	{
    		$insert_data['source_table'] = $data['source_table'];
    	}
    	
    	if(isset($data['table_object_ID']))
    	{
    		$insert_data['table_object_ID'] = $data['table_object_ID'];
    	}

    
    	$storage = $this->getStorage();
    	$storage->create($insert_data);
    }
    
    public function addSystemRecord($data)
    {
    	$insert_data['ip'] = $_SERVER['REMOTE_ADDR'];
    	$insert_data['date'] = date('Y-m-d H:i:s');
    	$insert_data['detail'] = $data['detail'];
    	$insert_data['scope'] = $data['scope'];
    	$insert_data['activity'] = $data['activity'];
    	$insert_data['browser'] = $_SERVER['HTTP_USER_AGENT'];
    	$storage = $this->getStorage();
    	$storage->create($insert_data);
    }

    /*
	public function getLogData()
	{
	    return $this->logData;
	}

	public function setLogData($logData)
	{
	    $this->logData = $logData;
	}
	
	public function addLogData($index,$val)
	{
		
	}
	*/
}