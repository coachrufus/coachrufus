<?php
namespace Core;
class Request 
{
    private static $request_params = array();

    public static function clean_tags(&$val) {
        $val = strip_tags($val);
        return $val;    
     }
    
    public  function clean($param)
    {
        if(is_array($param))
        {
            array_walk_recursive($param, 'self::clean_tags');
        }
        else
        {
        	$param = self::clean_tags($param);
        }
        
        return $param;
        
    }
    
    function getRoot() { 
        
        if(!array_key_exists('REQUEST_SCHEME', $_SERVER) or null == $_SERVER['REQUEST_SCHEME'])
        {
            $_SERVER['REQUEST_SCHEME'] = 'http';
        }
        
        return "{$_SERVER['REQUEST_SCHEME']}://{$_SERVER['HTTP_HOST']}"; 
        
    }
    
    public  function getParameter($name,$escape=true)
    {
    	$params = self::getParameters();
    	
    	if(isset($params[$name]) && "" != $params[$name])
        {
            if($escape)
            {
            	return self::clean($params[$name]);
            }
            else
            {
            	return $params[$name];
            }
            
        }
        else
        {
            return null;
        }
    }
    
    /*
     * toto je len zastupna funkcia pre getParameter(), lebo sa mi to nechce take dlhe vypisovat :-)
     */
    public  function get($name,$escape = true)
    {
    	return $this->getParameter($name,$escape);
    }
    
    
    public function getParameters()
	{
		/*
		$get_params = self::decodeQueryParams($_GET);
		$post_params = self::decodeQueryParams($_POST);
		self::$request_params = array_merge($get_params,$post_params);
		*/
		self::$request_params = array_merge($_GET,$_POST);
		return self::$request_params;
	}
	
	
    
    public  function getFiles($name)
    {
    	if(isset($_FILES[$name]) && !empty($_FILES[$name]['name']))
        {
        	return $_FILES[$name];
        }
        else
        {
        	return null;
        }
    }
    
    public  function getFileSize($name)
    {
        if(isset($_FILES[$name]))
        {
            return $_FILES[$name]['size'];
        }
        else
        {
            return null;
        }
    }
    
    public  function uploadFile($image_name,$upload_dir,$file_name = null)
    {
    	if(null == $file_name)
    	{
    		$file_name = md5(time()).'_'.$_FILES[$image_name]['name'];
    	}
    	
    	move_uploaded_file($_FILES[$image_name]['tmp_name'],  $upload_dir.'/'.$file_name);
        return $file_name;
    }
    
    public  function getMethod()
    {
    	if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH']=="XMLHttpRequest")
        {
        	return 'AJAX_'.$_SERVER['REQUEST_METHOD'];
        }
        else
        {
        	return $_SERVER['REQUEST_METHOD'];
        }  
    }
    
    public  function getPost()
    {
    	return $_POST;
    }
    
    public  function getGet()
    {
    	return $_GET;
    }
    
    /*
     * Prida text do session, pri prvom pouziti sa odstrani
     */
    public  function addFlashMessage($name,$message)
    {
    	$_SESSION['flash_message'][$name] = $message;
    }
    
    
    public  function getFlashMessage($name)
    {
    	if(isset($_SESSION['flash_message'][$name]))
    	{
    		$tmp_flash = $_SESSION['flash_message'][$name];
    		unset($_SESSION['flash_message'][$name]);
    		return $tmp_flash;
    	}
		return false;
    }
    
    public  function hasFlashMessage($name)
    {
    	if(isset($_SESSION['flash_message'][$name]))
    	{
    		return true;
    	}
		return false;
    }
    
    public function currentUrl()
    {
    	//var_dump($_SERVER);
    	return $_SERVER['REQUEST_URI'];
    }
    
    public function Redirect($address = null,$type = 'header')
    {
    	if(null == $address)
    	{
            $address = $_SERVER['HTTP_REFERER'];
        }
        
        
        if($type == 'header')
    	{
            header('location:'.$address);
            exit;
        }
        elseif($type == 'meta')
        {
            echo '<meta http-equiv="refresh" content="0;URL='.$address.'">';
        }
        else 
        {
        }
    	//exit;
    }
    
    
    public  function getValue($val)
    {
    	if(isset($val))
    	{
    		return $val;
    	}
    	else
    	{
    		return '';
    	}
    }
    
    public function base64UrlEncode($inputStr)
    {
        return strtr(base64_encode($inputStr), '+/=', '-_,');
    }

    public function base64UrlDecode($inputStr)
    {
        return base64_decode(strtr($inputStr, '-_,', '+/='));
    }
    
    public function checkPostCSRF()
    {
        //check origing
        
        
        if(array_key_exists('HTTP_ORIGIN', $_SERVER))
        {
            if(false === strpos($_SERVER['HTTP_ORIGIN'],$_SERVER['HTTP_HOST']))
            {
                //throw new \AclException();
                die('?');
            }
        }
        
        if('POST' == $this->getMethod() or 'AJAX_POST' == $this->getMethod())
        {
            if($_SERVER['HTTP_REFERER'] == null or false === strpos($_SERVER['HTTP_REFERER'], $_SERVER['HTTP_HOST']))
            {
                 //throw new \AclException();
                die('??');
            }
        }
    }
    
    public function getDataFromEncodedApiUrl()
    {
        $getData = $this->getGet();
        $data = json_decode( base64_decode(substr($getData['id'],12)),true);
        return $data;
    }
}
?>