<?php

namespace Core;
class EntityMapper 
{
	protected $className;
         protected $assocEntitys;
	
	public function __construct($className)
	{
		$this->className = $className;
	}
        
         public  function getAssocEntitys() {
            return $this->assocEntitys;
        }

        public  function setAssocEntitys($val) {
            $this->assocEntitys = $val;
        }
        
         public function addAssocEntity($val)
        {
            $this->assocEntitys[] = $val;
        }


        public function validateEntityData($data)
        {
            $object = new $this->className();
            $rules = $object->getDataMapperRules();
            $data = array_change_key_case($data,CASE_LOWER);
            
            $errors = array();

            foreach($rules as $key => $rule)
            {
                if($rule['required'] == true && ( !array_key_exists($key, $data)  or null == $data[$key] or empty($data[$key])  ))
                {
                     $errors[$key][] = 'required';
                }
            }
            
            if(empty($errors))
            {
                return true;
            }
            else
            {
                return $errors;
            }
        }

	/**
	 * Create entity from array data
	 * @param array $data
	 * @return object
	 */
	public function createEntityFromArray($data)
	{
            if(null == $this->getAssocEntitys())
            {
                 return $this->createBaseEntity($data);
            }
            else
            {
                $entityData = array();
                $assocData = array();
                $assocPrefix = null;
                
                if(null != $data)
                {
                    foreach($data as $key => $val)
                    {

                        if($key == 'assoc')
                        {
                            $assocPrefix = $val; 
                        }


                        if($assocPrefix === null)
                        {
                            $entityData[$key] = $val;
                        }
                        else
                        {
                            $assocData[str_replace($assocPrefix, '', $key)] = $val;
                        }
                    }


                    //var_dump($entityData);


                    $object =  $this->createBaseEntity($entityData);



                    foreach($this->getAssocEntitys() as $assocDef)
                    {
                        $assocMapper = new $assocDef['mapper']($assocDef['class']);
                        $assocObject = $assocMapper->createBaseEntity($assocData);


                        $assocMethod = $assocDef['method'];
                        $object->$assocMethod($assocObject);
                    }


                    return $object;
                }
                    
                return null;
                
            }
            
           
	}
        
        public function createBaseEntity($data)
        {
            if(empty($data))
		{
			return null;
		}
		
		$object = new $this->className();
		$rules = $object->getDataMapperRules();
		
		$data = array_change_key_case($data,CASE_LOWER);
		
		foreach ($data as $key => $val)
		{
			$method_name_parts = explode('_',$key);
			$method_name_parts = array_map('ucfirst', $method_name_parts);
			$method_name = 'set'.implode($method_name_parts);
				
			if(method_exists($object, $method_name))
			{
				
				if(array_key_exists($key, $rules) && $rules[$key]['type'] == 'datetime')
				{
					if($val != null)
					{
						$val = new \DateTime($val);
					}
				}
				
				if(array_key_exists($key, $rules) && $rules[$key]['type'] == 'boolean')
				{
					$val = (1 == $val) ? true : false;
				}
				
				call_user_func_array(array($object, $method_name), array($val));
				
				
				
			}
		}
		return $object;
        }
	
	public function updateEntityFromArray($entity,$data)
	{
		$rules = $entity->getDataMapperRules();
		
		foreach ($data as $key => $val)
		{
			$method_name_parts = explode('_',$key);
			$method_name_parts = array_map('ucfirst', $method_name_parts);
			$method_name = 'set'.implode($method_name_parts);
				
			if(method_exists($entity, $method_name))
			{
				if(array_key_exists($key, $rules))
                                {
                                     if($rules[$key]['type'] == 'datetime')
                                    {
                                            if(null != $val)
                                            {
                                                    $val = new \DateTime($val);
                                            }
                                            else
                                            {
                                                    //pretypovanie na null kvoli ===
                                                    //$val = null;
                                            }
                                    }


                                    if($rules[$key]['type'] == 'boolean')
                                    {
                                            $val = (1 == $val) ? true : false;
                                    }
                                }
                            
                              
				
				call_user_func_array(array($entity, $method_name), array($val));
			}
		}
	}
}