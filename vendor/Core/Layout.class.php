<?php namespace Core;

class Layout
{
    private $template;
    private $template_parameters;
    private $slots;
    private $javascript = [];
    private $stylesheet = [];
    
    private static $instance;
    
    public function __construct() {}
   
    public static function getInstance()
    {
    	if (!self::$instance) self::$instance = new Layout();
    	return self::$instance;
    }
    
    public function setTemplate($template)
    {
        $this->template = $template;
    }
    
    public function getTemplate()
    {
        return $this->template;
    }
    
    public function setTemplateParameters($val)
    {
        $this->template_parameters = $val;
    }
    
    public function getTemplateParameters()
    {
        return $this->template_parameters;
    }
    
    public function renderTemplate($params = array())
    {
        $default_params = $this->getTemplateParameters();
        if(!empty($params) && null != $default_params)
        {
        	$params = $default_params+$params;
        }
        else if(null != $default_params)
        {
        	$params = $default_params;
        }
    	self::includePart($this->template,$params);
    }
    
    /**
     * iba obycajne zapuzdrenie nad include z toho dovodu aby sa obmedzila platnost premennych
     * 
     * @param string $file cesta k suboru ktory sa ma includnut
     * @params array $params pole premennych, ktore budu dostupne v includnutom subore
     */  
    public function includePart($file, $params = [])
    {
        if(null != $params) extract($params);
        $translator = \Core\ServiceLayer::getService('translator');
        $router =\Core\ServiceLayer::getService('router');
		$request = \Core\ServiceLayer::getService('request');
        $root = $request->getRoot();
        $identity = ServiceLayer::getService('security')->getIdentity();
        $actualPackage = ServiceLayer::getService('ActualPackageManager')->getActualPackage();
        $activeTeam = ServiceLayer::getService('ActiveTeamManager')->getActiveTeam();
        $layout = $this;
        if(file_exists($file)) include($file);
        else throw new \Exception('Subor ' . $file . ' neexistuje', '002');
    }
    
    public function slotExist($slot_name)
    {
        if(null != $this->slots && array_key_exists($slot_name,  $this->slots))
        {
            return true;
        }
        return false;
    }

    public function startSlot($slot_name)
    {
    	ob_start();
    }
    
    public function endSlot($slot_name)
    {
    	$slot_content = ob_get_contents();
    	$this->slots[$slot_name] = $slot_content;
    	ob_end_clean();
    }
    
    /**
     * Vlozi slot do sablony 
     * @param unknown_type $slot_name
     */
    public function insertSlot($slot_name)
    {
    	if(null != $this->slots && array_key_exists($slot_name,  $this->slots))
        {
            return $this->slots[$slot_name];
        }
    }
    
    /**
     * Prida odkaz na js subor
     * @param unknown_type $file
     */
    public function addJavascript($file)
    {
    	$this->javascript[] = $file;
    }
    
  	/**
  	 * Vrati zoznam vsetkych js suborov
  	 * @return unknown_type
  	 */
    public function getJavascript()
    {
    	return $this->javascript;
    }
    
    /**
     * Vlozi link na javascript subory, ak nie je zadane meno, vlozi vsteky
     * @param unknown_type $name
     */
    public function insertJavascript($name = null)
    {
    	if(null == $name)
    	{
    		foreach($this->getJavascript() as $file)
    		{
    			echo "<script type='text/javascript' src='/dev/". $file."?ver=".VERSION."'></script>\n";
    		}
    	}
    }
    
    /**
     * Prida odkaz na css subor
     * @param unknown_type $file
     */
    public function addStylesheet($file)
    {
        $this->stylesheet[] = $file;
    }
    
    /**
     * Vrati zoznam vsetkych css suborov
     * @return unknown_type
     */
    public function getStylesheet()
    {
        return $this->stylesheet;
    }
    
    /**
     * Vlozi link na css subory, ak nie je zadane meno, vlozi vsteky
     * @param unknown_type $name
     */
    public function insertStylesheetLink($name = null)
    {
        if(null == $name)
        {
            foreach($this->getStylesheet() as $file)
            {
                echo "<link href='/dev/".$file."?ver=".VERSION."' rel='stylesheet' type='text/css' media='screen' /> \n";
                
            }
        }
    }
    
     public function renderControllerAction($controller_path, $params = [])
    {
        $controller_info = explode(':', $controller_path);
        $module_name = $controller_info[0];
        $controller = $controller_info[1].'Controller';
        $action = $controller_info[2].'Action';
        $module = new $module_name();
        $namespace_parts = explode("\\", $module_name);
        array_pop($namespace_parts);
        $namespace = implode('\\',$namespace_parts);;
        require_once($module->getBaseDir().'/controller/'.$controller.'.php');
        $controller_class = $namespace.'\\Controller\\'.$controller;
        $controller_object = new $controller_class();
        $content = call_user_func(array($controller_object, $action),$params);
        return $content;
    }
    
    
    public function buildMenuClass()
    {
        $menuClass = array(
            'dashboard' => '',
            'scouting' => '',
            'teams_overview' => '',
            'team_overview' => '',
            'team_settings' => '',
            'team_event' => '',
            'team_season' => '',
            'team_members' => '',
            'team_attendance' => '',
            'team_statistic' => '',
            'team_playground' => '',
            'team_graphs' => '',
            'tournament' => '',
        );
        
        
        
        $webApp = \Core\WebApp::getInstance();

        $currenRouteName = \Core\Router::$current_route_name;
        $currentModuleName = $webApp->getCurrentModule()->getName();
        
        //t_dump($currentModuleName);
        
         if('player_dashboard' == $currenRouteName)
        {
            $menuClass['dashboard'] = 'active';
        }

        if('Scouting' == $currentModuleName)
        {
            $menuClass['scouting'] = 'active';
        }
        
        if( 'teams_overview' == $currenRouteName)
        {
            $menuClass['teams_overview'] = 'active';
        }
        if( 'team_overview' == $currenRouteName)
        {
            $menuClass['team_overview'] = 'active';
        }
        if( 'team_settings' == $currenRouteName)
        {
            $menuClass['team_settings'] = 'active';
        }
        
        $eventRoutes = array('team_event_list','create_team_event','team_event_detail','team_match_new_lineup','team_match_create_live_stat','team_event_rating','team_match_view_lineup','team_match_view_live_stat','team_match_edit_lineup','edit_team_event','');
        
        if(in_array($currenRouteName, $eventRoutes))
        {
            $menuClass['team_event'] = 'active';
        }
        
        $seasonRoutes = array('team_season_list','team_season_edit','team_create_season');
        if(in_array($currenRouteName, $seasonRoutes))
        {
            $menuClass['team_season'] = 'active';
        }
        
        $memberRoutes = array('team_public_players_list','team_players_list');
        if(in_array($currenRouteName, $memberRoutes))
        {
            $menuClass['team_members'] = 'active';
        }
        
        if( 'team_players_attendance' == $currenRouteName)
        {
            $menuClass['team_attendance'] = 'active';
        }
        
        if( 'team_player_stat' == $currenRouteName)
        {
            $menuClass['team_statistic'] = 'active';
        }
        if( 'widget_list' == $currenRouteName)
        {
            $menuClass['team_graphs'] = 'active';
        }
        
        $playgroundRoutes = array('team_playground_list','team_playground_edit','team_playground_create');
        if(in_array($currenRouteName, $playgroundRoutes))
        {
            $menuClass['team_playground'] = 'active';
        }
        
        if( 'tournament' == $currenRouteName)
        {
            $menuClass['tournament'] = 'active';
        }
        
        return $menuClass;
    }
    
    public function buildTournamentMenuClass()
    {
         $menuClass = array(
            'setting' => '',
            'detail' => '',
            'teams' => '',
            'schedule' => '',
            'stats' => '',
            
        );
         
          $currenRouteName = \Core\Router::$current_route_name;
          
        if( 'tournament_settings' == $currenRouteName)
        {
            $menuClass['settings'] = 'active';
        }
        if( 'tournament_detail' == $currenRouteName or 'tournament_team_players' == $currenRouteName)
        {
            $menuClass['detail'] = 'active';
        }
        
        if( 'tournament_teams' == $currenRouteName)
        {
            $menuClass['teams'] = 'active';
        }
        
        if( 'tournament_schedule' == $currenRouteName or 'tournament_event_detail' == $currenRouteName or 'tournament_event_score' == $currenRouteName)
        {
            $menuClass['schedule'] = 'active';
        }
        
         if( 'tournament_stats' == $currenRouteName)
        {
            $menuClass['stats'] = 'active';
        }
         if( 'tournament_players_stats' == $currenRouteName)
        {
            $menuClass['players_stats'] = 'active';
        }
        
         return $menuClass;
    }
    
    public function getMobileAppTitle($routeName)
    {
        $titleMapper = array(
            'edit_team_event' => 'mobileApp.navTitle.event',
            'team_event_detail' => 'mobileApp.navTitle.event',
            'team_match_edit_lineup' => 'mobileApp.navTitle.event.lineup',
            'team_match_create_live_stat' => 'mobileApp.navTitle.event.livestat',
            'team_event_rating' => 'mobileApp.navTitle.event.rating',
            'team_event_list' => 'mobileApp.navTitle.event.list',
            'create_team_event' => 'mobileApp.navTitle.event.create',
            'team_create' => 'mobileApp.navTitle.team.create',
            'team_public_players_list' => 'mobileApp.navTitle.team.players',
            'team_players_list' => 'mobileApp.navTitle.team.playersCreate',
        );
        
        if(array_key_exists($routeName, $titleMapper))
        {
             return $titleMapper[$routeName];
        }
        else
        {
            return 'mobileApp.navTitle.'.$routeName;
        }
       
    }
    
}
