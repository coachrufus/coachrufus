<?php
namespace Core;
/**
 * Data Gateway pattern, retrieve and persist data from storage
 * @author Marek Hubacek
 *
 */
class DbStorage {
	
	protected $tableName;
        protected $assocTables;

	public function __construct($table)
	{
		$this->tableName = $table;
	}
        
public  function getAssocTables() {
return $this->assocTables;
}

public  function setAssocTables($assocTables) {
$this->assocTables = $assocTables;
}

public function addAssocTable($index,$table)
{
    $this->assocTables[$index] = $table;
}


	
	protected $createList;
	
	public function create($data)
	{
		return \Core\DbQuery::insertFromArray($data, $this->getTableName());
	}
	
	public function update($id,$data)
	{
                $result =  \Core\DbQuery::updateFromArray($data,$id,$this->getTableName());
                return $result;
	}
	
	public function delete($id)
	{
		\Core\DbQuery::prepare('DELETE from  '.$this->getTableName().' WHERE id =:id ')
		->bindParam('id', $id)
		->execute();
	}
        
        public function deleteByParams($params)
	{

            
            $where_criteria_items  = array();
		foreach ($params as $column => $column_val)
		{
			if(is_string($column_val))
			{
				$where_criteria_items[] = ' '.$column.'=:'.$column.' ';
			}
			else
			{
				$where_criteria_items[] = ' '.$column.'=:'.$column.' ';
			}
			
		}
		$where_criteria = ' WHERE '. implode(' AND ',$where_criteria_items);
            
            
                $query = \Core\DbQuery::prepare('DELETE  FROM '.$this->getTableName(). $where_criteria);
		foreach($params as $column => $value)
		{
			if("" != $value)
			{
				$query->bindParam(':'.$column, $value);
			}
		}
		$query->execute();

	}
        
        
	
	public function softDelete($id)
	{
		\Core\DbQuery::prepare('UPDATE  '.$this->getTableName().' SET is_deleted = 1 WHERE id =:id ')
		->bindParam('id', $id)
		->execute();
	}
        
            
         public function getAssocColumnsQuery() {
                $assoc_table = $this->getAssocTables();
                if(null == $assoc_table)
                {
                    return '';
                }
                
                
                 $columns = DbQuery::prepare('SHOW COLUMNS FROM '.$assoc_table['a_'])->execute()
		->fetchAll(\PDO::FETCH_ASSOC);

                 
                 $assocColumns[] = ' "a_" as assoc ';
                 
                 foreach($columns as $column)
                 {
                     $assocColumns[] .= 'a.'.$column['Field'].' as a_'.$column['Field'];
                 }
                
                return  implode(',',$assocColumns);
        }
        
        public function prefixColumns($prefix,$columns)
        {
            if(!is_array($columns))
            {
                $columns = explode(',',$columns);
            }
            $prefixed  = array();
            
            foreach($columns as $column)
            {
                $prefixed[] .= $prefix.$column;
            }

            return $prefixed;
        }
	
	
	
	
	public function find($index)
	{
		/*
                $data = \Core\DbQuery::prepare('SELECT * FROM '.$this->getTableName().' WHERE id =:id ')
		->bindParam('id', $index)
		->execute()
		->fetchOne(\PDO::FETCH_ASSOC);
		return $data;
                 * 
                 */
            
            $list = $this->findBy(array('id' => $index));
            if(isset($list[0]))
            {
                return $list[0];
            }
            return null;
	}
	
	public function findBy($query_params = array(),$options = array())
	{

		$where_criteria_items  = array();
		foreach ($query_params as $column => $column_val)
		{
                        if(is_string($column_val))
			{
				$where_criteria_items[] = ' '.$column.'=:'.$column.' ';
			}
			else
			{
				$where_criteria_items[] = ' '.$column.'=:'.$column.' ';
			}
			
		}
		$where_criteria = ' WHERE '. implode(' AND ',$where_criteria_items);
        
                $sort_criteria = '';
                if(array_key_exists('order',$options))
                {
                    $sort_criteria = ' order by '.$options['order'];
                }
		
                 if(array_key_exists('custom_query',$options))
                {
                    $where_criteria .= $options['custom_query'] ;
                }
                
                
		$query = \Core\DbQuery::prepare('SELECT * FROM '.$this->getTableName(). $where_criteria.$sort_criteria);
		foreach($query_params as $column => $value)
		{
			if("" != $value)
			{
				$query->bindParam(':'.$column, $value);
			}
                        else 
                        {
                             $query->bindParam(':'.$column, null);
                        }
                       
		}
		$data = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
		return $data;
	}
	
	public function findOneBy($query_params = array())
	{
                $list = $this->findBy($query_params);
                
                if(!empty($list))
                {
                     return $list[0];
                }
               
                return null;
            
                /*
		$where_criteria_items  = array();
		foreach ($query_params as $column => $column_val)
		{
			if(is_string($column_val))
			{
				$where_criteria_items[] = ' '.$column.'="'.$column_val.'" ';
			}
			else
			{
				$where_criteria_items[] = ' '.$column.'='.$column_val.' ';
			}
				
		}
		$where_criteria = ' WHERE '. implode(' AND ',$where_criteria_items);

	
		$query = \Core\DbQuery::prepare('SELECT * FROM '.$this->getTableName(). $where_criteria);
		foreach($query_params as $column => $value)
		{
			if("" != $value)
			{
				$query->bindParam(':'.$column, $value);
			}
		}
		$data = $query->execute()->fetchOne(\PDO::FETCH_ASSOC);
		return $data;
                 * 
                 */
	}
	
	public function findAll($options = array())
	{
		$sort_criteria = '';
		if(array_key_exists('order',$options))
		{
			$sort_criteria = ' order by '.$options['order'];
		}
		
		$data = \Core\DbQuery::prepare('SELECT * FROM '.$this->getTableName().$sort_criteria)->execute()->fetchAll();
		return $data;
	}
	
	

	public function getTableName()
	{
	    return $this->tableName;
	}

	public function setTableName($tableName)
	{
	    $this->tableName = $tableName;
	}
	
	
	
	public function generateAllEntity($folder)
	{
		$tables = \Core\DbQuery::prepare('SHOW TABLES')
		->execute()
		->fetchAll();

		foreach($tables as $table)
		{
			$table_name = current($table);
			$table_name_parts = explode('_',$table_name);
			$table_name_parts = array_map('ucfirst', $table_name_parts);
			$entity_name = implode($table_name_parts);
			$this->setTableName($table_name);
			$this->generateEntity($entity_name,$folder);
		}
	}
        
        public function generateEntityMapper($entity_name,$folder)
        {
            $db_rows = \Core\DbQuery::prepare('SHOW COLUMNS FROM '.$this->getTableName())
		->execute()
		->fetchAll(\PDO::FETCH_ASSOC);
		
		//var_dump($db_rows);exit;

		
		$property_string = '';
		$property_method_string = '';
		$property_type_string = '';
		$property_mapper_string = '';
		foreach($db_rows as $db_row)
		{

			
			
			$coll_mapped_data = $this->mapColumn($db_row);
			
			$property_mapper_string .= "'".strtolower($db_row['Field'])."' => array(
					'type' => '".$coll_mapped_data['type']."',
					'max_length' => '".$coll_mapped_data['max_length']."',
					'required' => ".$coll_mapped_data['required']."
							
					),\n";
                }
                
                $class_string = '<?php\n	
namespace {{NAMESPACE}}\Model;
		
class '.$entity_name.'Mapper {	\n
	
	
		private $mapper_rules  = array(
	'.$property_mapper_string.'
);
	
public function getDataMapperRules()
{
	return $this->mapper_rules;
}
}\n
		';
		
		file_put_contents($folder.'/'.$entity_name.'Mapper.php',$class_string);
        }
	
	/**
	 * Generate entity based on table_cols
	 * @param string $entity_name Entity name
	 * @param string $folder
	 * @return string
	 */
	public function generateEntity($entity_name,$folder)
	{
		
		$db_rows = \Core\DbQuery::prepare('SHOW COLUMNS FROM '.$this->getTableName())
		->execute()
		->fetchAll(\PDO::FETCH_ASSOC);
		
		//var_dump($db_rows);exit;

		
		$property_string = '';
		$property_method_string = '';
		$property_type_string = '';
		$property_mapper_string = '';
		foreach($db_rows as $db_row)
		{
			$table_rows[strtolower($db_row['Field'])] = $db_row;
			
			$method_name_parts = explode('_',$db_row['Field']);
			$method_name_parts = array_map('strtolower', $method_name_parts);
			$method_name_parts = array_map('ucfirst', $method_name_parts);
			$base_property_name = lcfirst(implode($method_name_parts));
			
			$property_name = '
protected $'.$base_property_name.';'."\n";
			
			$property_string .= $property_name;
			
			$set_method_name = '	
public function set'.implode($method_name_parts).'($val){$this->'.$base_property_name.' = $val;}'."\n";
			$get_method_name = '
public function get'.implode($method_name_parts).'(){return $this->'.$base_property_name.';}'."\n";	
			
			$property_method_string .= $set_method_name;
			$property_method_string .= $get_method_name;
			
			
			$coll_mapped_data = $this->mapColumn($db_row);
			
			$property_mapper_string .= "'".strtolower($db_row['Field'])."' => array(
					'type' => '".$coll_mapped_data['type']."',
					'max_length' => '".$coll_mapped_data['max_length']."',
					'required' => ".$coll_mapped_data['required']."
							
					),\n";
			
			
			
		}
		
		$final_property_type = '
private $repository;
private $mapper_rules  = array(
	'.$property_mapper_string.'
);
	
public function getDataMapperRules()
{
	return $this->mapper_rules;
}

public function getRepository()
{
	if(null == $this->repository)
	{
		$this->repository = ServiceLayer::getService("{{ENTITY}}Repository"); 
	}
	return $this->repository;
}

public function setRepository($repository)
{
	$this->repository = $repository;
}
			
';
		
		
$class_string = "<?php\n	
namespace {{NAMESPACE}}\Model;
use Core\EntityMapper as EntityMapper;
use Core\ServiceLayer as ServiceLayer;
		
class ".$entity_name." {	\n
	
	
		".$final_property_type."
		
		
		".$property_string."
			".$property_method_string."
}\n
		";
		
		file_put_contents($folder.'/'.$entity_name.'.php',$class_string);
	}
	
	public function mapColumn($col)
	{
		$mapper = array();
		
		$is_varchar = preg_match("/varchar\((.*)\)/", $col['Type'],$varchar_matches);
		if($is_varchar)
		{
			$mapper['type'] = 'string';
			$mapper['max_length'] = $varchar_matches[1];
		}
		
		$is_text = preg_match("/text/", $col['Type']);
		if($is_text)
		{
			$mapper['type'] = 'string';
			$mapper['max_length'] = null;
		}
		
		$is_int = preg_match("/int\((.*)\)/", $col['Type'],$int_matches);
		if($is_int)
		{
			$mapper['type'] = 'int';
			$mapper['max_length'] = $int_matches[1];
		}
		
		$is_datetime = preg_match("/datetime/", $col['Type']);
		if($is_datetime)
		{
			$mapper['type'] = 'datetime';
			$mapper['max_length'] = null;
		}
		
		$is_bool = preg_match("/tinyint\((.*)\)/", $col['Type'],$bool_matches);
		if($is_bool && $bool_matches[1] == 1)
		{
			$mapper['type'] = 'boolean';
			$mapper['max_length'] = null;
		}
		
		if($col['Null'] == 'YES')
		{
			$mapper['required'] = "false";
		}
		else
		{
			$mapper['required'] = "true";
		}
		
		return  $mapper;
	}
	
}