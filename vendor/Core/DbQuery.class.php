<?php
namespace Core;
/**
 * Wrapper nad PDO, v podstate zapuzdruje iba metody pdo + par uzitocnych dalsich
 */

class DbQuery
{

    public static $db_handler;
    private $statement;
    private $params;
    private static $query;
    private $sql;
    private static $log;
    private static $counter = 0;
    public static $forceDump = true;

    public static function createConnection($host = 'localhost', $dbname, $user, $password)
    {
        try
        {
            self::$db_handler = new \PDO("mysql:socket=" . $host . ";dbname=" . $dbname, $user, $password);
            if (DEBUG)
            {
                self::$db_handler->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            }
        }
        catch(\PDOException $e)
        {
            if (RUN_ENV == 'dev')
            {
                echo $e->getMessage();
            }
            else
            {
                //sem pride kod co sa vykona ak zlyha spojenie v produkcnom
                // nasadeni, napr poslat mail adminovi, zapisat do logov a pod
                
            }
        }
    }
    

    public static  function closeConnection()
    {
        self::$db_handler = null;
    }
    
    public static function switchDb($name)
    {
    	
    
    	if($name == 'portal')
    	{
    		//mysql_close();
    		self::closeConnection();
    		self::createConnection(DB_HOST,DB_NAME,DB_USER,DB_PASS);
    		self::query("SET NAMES utf8;");
    	}
    
    	if($name == 'test')
    	{
    		//mysql_close();
    		self::closeConnection();
    		self::createConnection(DB_TEST_HOST,DB_TEST_NAME,DB_TEST_USER,DB_TEST_PASS);
    		self::query("SET NAMES utf8;");
    	}
    
    
    }

    public static function getDbHandler()
    {
        return self::$db_handler;
    }

    public static function setDbHandler($val)
    {
        self::$db_handler = $val;
    }

    public function setStatement($val)
    {
        $this->statement = $val;
    }

    public function getStatement()
    {
        return $this->statement;
    }

    
    public static function prepare($query)
    {
        $db_query = new DbQuery();
        $dbh = self::getDbHandler();
        $stmt = $dbh->prepare($query);
        $db_query->setStatement($stmt);
        self::$query = $query;
        return $db_query;
    }

    public function bindParam($key, $val)
    {
        $this->getStatement()->bindParam($key, $val);
        $this->params[$key] = $val;
        return $this;
    }
    
    public function bindValue($key, $val)
    {
        $this->getStatement()->bindValue($key, $val);
        $this->params[$key] = $val;
        return $this;
    }

    public function execute($data = array())
    {
    	$start = microtime(true);

        
    	if(!empty($data))
        {
            $exec_result = $this->getStatement()->execute($data);
        }
        else 
        {
            $exec_result = $this->getStatement()->execute($this->params);
        }
        if (false == $exec_result)
        {
            if (RUN_ENV == 'dev')
            {
            	echo 'QUERY:<br />';
            	echo self::$query;
            	echo '<br />';
            	echo '<br />';
            	echo 'DATA:<br />';
            	t_dump($data);
            	echo 'PARAMS:<br />';
            	t_dump($this->params);
            	echo '<br />';
            	echo '<br />';
            	echo 'BIND QUERY:<br />';
            	echo $this->getSql(self::$query, $this->params);
            	echo '<br />';
            	echo '<br />';
            	echo 'ERROR INFO:<br />';
            	$error_info = $this->getStatement()->errorInfo();
                t_dump($error_info);
                $this->getStatement()->debugDumpParams();
                throw new \Exception('DB ERROR');
            }
            else
            {
                //sem pride kod co sa vykona ak zlyha spojenie v produkcnom
                // nasadeni, napr poslat mail adminovi, zapisat do logov a pod
                $log = $this->getSql(self::$query, $this->params);
                ob_start();
                var_dump($this->getStatement()->errorInfo());
                $log .= ob_get_contents();
                ob_end_clean();
                 file_put_contents(APPLICATION_PATH . '/log/db.log', $log, FILE_APPEND);
            }
        }
        $time = microtime(true) - $start;
        $log_index = date('Y-m-d-H-i-s',time());
	self::$log[$log_index]['query']  = self::$query;
	self::$log[$log_index]['data']  = $data;
	self::$log[$log_index]['params']  = $this->params;
        self::$log[$log_index]['bind_query']  =$this->getSql(self::$query, $this->params);
	self::$log[$log_index]['time']  = $time;
	self::$log[$log_index]['result']  = $exec_result;
        
        self::$counter++;
        //\Tracy\Debugger::barDump(\Core\DbQuery::getLog());
        
        
        
         if (RUN_ENV == 'dev')
        {
            //\Tracy\Debugger::barDump( $this->getSql(self::$query, $this->params),'QUERY '.self::$counter.' EXECUTE TIME:'.$time);
        }
        return $this;
        //return true;
    }
    
    
    
   
    
    /**
     * Priame spracovanie dotazu
     */
    public static function query($sql)
    {
    	$dbh = self::getDbHandler();
        $dbh->exec($sql);
       
    }
    
    

    public function fetchAll($fetch_style = \PDO::FETCH_ASSOC)
    {
        $results = $this->getStatement()->fetchAll($fetch_style);
        return $results;
    }
    
    public function fetchOne($fetch_style = \PDO::FETCH_ASSOC)
    {
        $results = $this->getStatement()->fetch($fetch_style);
        return $results;
    }
    
   
    
    public static function insertFromArray($db_data,$table_name)
    {
        $bind = ':'.implode(',:', array_keys($db_data));
        $sql  = 'INSERT INTO '.$table_name.'('.implode(',', array_keys($db_data)).') '.
                  'VALUES ('.$bind.')';

        $insert_id = DbQuery::prepare($sql)
         ->execute(array_combine(explode(',',$bind), array_values($db_data)))
         ->getDbHandler()
         ->lastInsertId();
        
       
        
        return $insert_id;
    }
    
    public static function updateFromArray($db_data,$id,$table_name)
    {


        unset($db_data['id']);
        $sql = 'UPDATE '.$table_name. ' SET ';
            foreach($db_data as $key => $val)
            {
                $sql .= $key.' = :'.$key.', ';
            }
            
           
            
            
            $sql = substr($sql,0,-2);
            $sql .= ' WHERE id = :id';
            $bind = ':'.implode(',:', array_keys($db_data));
            $bind_data = array_combine(explode(',',$bind), array_values($db_data));
            $bind_data[':id'] = $id;
            $query = DbQuery::prepare($sql);


            
            foreach($bind_data as $key => $value)
            {
                    /*
                    if("" !== $value)
                    {
                            $query->bindParam($key, $value);
                    }
                     * 
                     */
                     $query->bindParam($key, $value);
            }
                //var_dump($query);exit;
            
            
            $id = $query->execute();
            return $id;
    }

    /**
     * Vrati info o aktualnom dotaze
     */
    public function dump()
    {
	echo $this->getSql(self::$query, $this->params);
        
        
        
    	//$this->getStatement()->debugDumpParams();
       // $results = $this->getStatement()->fetchAll(PDO::FETCH_BOTH);
        //var_dump($results);
    }
    
    public function logQuery($query)
    {
    	if(RUN_ENV == 'dev')
    	{
    		ob_start();
    		$query->dump();
    		$log = ob_get_contents();
    		ob_end_clean();
    		NDebugger::fireLog($log);
    	}
    }
    
    public function getSql($query, $params) {
    	$keys = array();
    	# build a regular expression for each parameter
    	if(null != $params)
    	{
    		foreach ($params as $key => $value) {
    			if (is_string($key)) {
    				$keys[] = '/:'.str_replace(':','',$key).'/';
    			} else {
    				$keys[] = '/[?]/';
    			}
    		}
    	}
    	$query = preg_replace($keys, $params, $query, 1, $count);
    
    	#trigger_error('replaced '.$count.' keys');
    
    	return $query;
    }
    
    public static function dumpLog()
    {
    	print_r(self::$log) ;
    }
    
    public static function getLog()
    {
    	return self::$log;
    }
    
   
}