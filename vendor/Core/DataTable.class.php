<?php
namespace Core;
/**
 * 
 */
class DatagridTable implements \Iterator 
{

	protected $position = 0;
	
	
	/**
     * Záznamy
     */
    protected $records;
	
	/*
     * Nazov tabulky v databaze
     */
    protected $table_name;
    
    /**
     * Stlpce v databaze
     */
    protected $table_rows;
    
    /**
     * Query
     */
    protected $query;
    
    /**
     * Count Query - ovplyvneuje strankovanoe v tabulke
     */
    protected $count_query;
    
    /**
     * Strankovac
     */
    protected $pager;
    
    /**
     * Pocet zaznamov na stranku
     */
    protected $items_per_page = 500;
    
    /**
     * Aktaualna stranka
     */
    protected $page = 1;
    
    /**
     * Stlpce ktore sa maju zobrazit
     */
    protected $visible_columns;
    
     /**
     * Nazvy stlpcov v hlavicke
     */
    protected $column_labels;
    
     /**
     * Typy filtrovania
     */
    protected $filter_types;
    
    /**
     * Permanentny filter, sluzi na to ak je nejake kriterium, podla ktoreho filtorvat treba stale
     */
    protected $permanent_filter;
    
    /**
     * Defaultne sortovanie
     */
    protected $default_sort_criteria;
    
    /**
     * parametre do filtrovania
     */
    private $query_params = array();
    
    
    public function __construct() {
    	$this->position = 0;
    }
    
    function rewind() {
    	//var_dump(__METHOD__);
    	$this->position = 0;
    }
    
    function current() {
    	//var_dump(__METHOD__);
    	return $this->records[$this->position];
    }
    
    function key() {
    	//var_dump(__METHOD__);
    	return $this->position;
    }
    
    function next() {
    	//var_dump(__METHOD__);
    	++$this->position;
    }
    
    function valid() {
    	//var_dump(__METHOD__);
    	return isset($this->records[$this->position]);
    }
    
	public function renderField($index)
	{
		$record = $this->current();
		
		$method = str_replace(' ','','get'.ucwords(str_replace('_',' ',$index)));
		if(method_exists ( $this , $method ))
		{
			//return call_user_func_array($callback, $param_arr)
			
			return call_user_func_array(array($this, $method), array($record));
		}
		else
		{
			return $record[$index];
		}
		
	}
	
    
    /*
     * Nastavi nazov tabulky, do ktorej sa maju ukladat udaje
     */
    public function setTableName($val)
    {
        $this->table_name = $val;
        return $this;
    }
    
    public function getTableName()
    {
        return $this->table_name;
    }

    public function setTableRows($val)
    {
        $this->table_rows = $val;
    }
    
    public function getTableRows()
    {
        if(null == $this->table_rows)
        {
            if(null != $this->getRecords())
            {
                //$collection = $this->getRecords()->getCollection();
                $collection = $this->getRecords();
                //var_dump($collection);
                //$collection_rows = array_keys($collection[0]->getData());
                $collection_rows = array_keys($collection[0]);
                
                $result_rows = array_combine($collection_rows, $collection_rows);
                if(null != $this->getVisibleColumns())
                {
                    $visible_columns = array_combine($this->getVisibleColumns(), $this->getVisibleColumns());
                    $this->table_rows = array_intersect_key($visible_columns,$result_rows);
                }
                else 
                {
                    $this->table_rows = $result_rows;
                }
            }
            else 
            {
                $visible_columns = array_combine($this->getVisibleColumns(), $this->getVisibleColumns());
                $this->table_rows = $visible_columns;
            }
            
        }
        return $this->table_rows;
    }
    
    public function setVisibleColumns($val)
    {
        $this->visible_columns = $val;
    }
    
    public function getVisibleColumns()
    {
        return $this->visible_columns;
    }
    
    public function setColumnLabels($val)
    {
        $this->column_labels = $val;  
    }
    
    public function getColumnLabels()
    {
        return $this->column_labels;
    }
    
    public function setPermanentFilter($val)
    {
        $this->permanent_filter = $val;
    }
    
    public function getPermanentFilter()
    {
        return $this->permanent_filter;
    }
    
    
    public function setFilterTypes($val)
    {
        $this->filter_types = $val;  
    }

    
    public function getColumnLabel($index)
    {
        if(isset($this->column_labels[$index]))  
        {
            return $this->column_labels[$index];
        }
        return $index;
    }
    
    
    public function setItemsPerPage($val)
    {
        $this->items_per_page = $val;
        $_SESSION['datagrid_items_per_page_'.$this->getTableName()] = $this->items_per_page;
    }
    
    public function getItemsPerPage()
    {
        if(isset($_SESSION['datagrid_items_per_page_'.$this->getTableName()]))
        {
            $this->items_per_page =  $_SESSION['datagrid_items_per_page_'.$this->getTableName()];
        }
        return $this->items_per_page;
    }
    
    
    public function setPage($val)
    {
        $this->page = $val;   
    }
    
    public function getPage()
    {
        return $this->page;
    }
    
    public function getDbTableRows()
    {
        $db_rows = DbQuery::executeWithResult('SHOW COLUMNS FROM '.$this->getTableName());
        foreach($db_rows as $db_row)
        {
            $table_rows[strtolower($db_row['field'])] = $db_row;
        }
        return $table_rows;
    }
    
    public function setQuery($val)
    {
        $this->query =  $val;
    }
    
    public function getQuery()
    {
        if(null == $this->query)
        {
            $this->query =  'SELECT a.* FROM '.$this->getTableName().' a ';
        }
        return $this->query;
    }
    
    public function getCountQuery()
    {
        if(null == $this->count_query)
        {
            $this->count_query =   'SELECT COUNT(*) AS COUNT FROM '.$this->getTableName().' a ';
        }
        return $this->count_query.$this->getFilterCriteriaSql();
    }
    
    public function setCountQuery($val)
    {
        $this->count_query =  $val;

    }
    
    public function addSortCriteria($criteria)
    {
        $_SESSION['datagrid_sort_criteria_'.$this->getTableName()] = null;
        $direction = '';
        if('up' == $criteria['direction'] or 'asc' == $criteria['direction'])
        {
            $direction = 'asc';
        }
        
        if('down' == $criteria['direction']  or 'desc' == $criteria['direction'])
        {
            $direction = 'desc';
        }
        
        $_SESSION['datagrid_sort_criteria_'.$this->getTableName()][$criteria['column']] = $criteria['column'].' '.$direction;
    }
    
    public function setSortCriteria($criteria)
    {
        $this->clearSortCriteria();
        $this->addSortCriteria($criteria);
    }
    
    public function getSortCriteria()
    {
        //var_dump($_SESSION['datagrid_sort_criteria']) ;
        if(isset($_SESSION['datagrid_sort_criteria_'.$this->getTableName()]))
        {
             return $_SESSION['datagrid_sort_criteria_'.$this->getTableName()];
        }
        return array();
    }
    
    public function getSortCriteriaSql()
    {
        $sort_criteria_sql = '';   
        //var_dump($_SESSION['datagrid_sort_criteria']);
        if(isset($_SESSION['datagrid_sort_criteria_'.$this->getTableName()]))
        {
            $sort_criteria_sql = trim(implode(', ',$_SESSION['datagrid_sort_criteria_'.$this->getTableName()]));
            $sort_criteria_sql .= ' ';
            $sort_criteria_sql = trim($sort_criteria_sql);

            if(!empty($sort_criteria_sql))
            {
                $sort_criteria_sql = ' ORDER BY '.$sort_criteria_sql;      
            }
        }
        return $sort_criteria_sql;
    }
    
    public function clearSortCriteria()
    {
        $_SESSION['datagrid_sort_criteria_'.$this->getTableName()] = null;
    }
    
    public function setFilterCriteria($criteria)
    {
        if(array_key_exists('clear', $criteria))
        {
            $this->clearFilterCriteria();
        }
        else
        {
            $_SESSION['datagrid_filter_criteria_'.$this->getTableName()] = $criteria;
        }
    }
    
    public function clearFilterCriteria()
    {
        $_SESSION['datagrid_filter_criteria_'.$this->getTableName()] = null;
    }
    
    public function getFilterCriteriaValue($index)
    {
        $value = null; 
        if(isset($_SESSION['datagrid_filter_criteria_'.$this->getTableName()]) && isset($_SESSION['datagrid_filter_criteria_'.$this->getTableName()][$index]))
        {
            return $_SESSION['datagrid_filter_criteria_'.$this->getTableName()][$index];
        }
    }
    
public function getFilterCriteriaSql()
    {
        //var_dump($_SESSION['datagrid_filter_criteria']);
        $filter_criteria_sql = ' WHERE 1 = 1 ';   
        if(isset($_SESSION['datagrid_filter_criteria_'.$this->getTableName()]))
        {
            //$filter_criteria_sql = ' WHERE 1 = 1 ';      
            foreach($_SESSION['datagrid_filter_criteria_'.$this->getTableName()] as $column => $value)
            {
                if(null != $value && $value != 'all')   
                {
                    $filter_alias = $column;
                    if(isset($this->filter_types[$column]['filter_alias']))
                    {
                        $filter_alias = $this->filter_types[$column]['filter_alias'];
                    }

                    //echo $column. '->'.$this->filter_types[$column]['type'];
                    if(isset($this->filter_types[$column]['sql_criteria']))
                    {
                        if('empty' == $value)
                        {
                            $filter_criteria_sql .= DbQuery::prepare($this->filter_types[$column]['empty_sql_criteria']);
                        }
                        else 
                        {
                            $filter_criteria_sql .= DbQuery::prepare($this->filter_types[$column]['sql_criteria'],array($value));
                        }
                    }
                    else
                    {
                        if($this->filter_types[$column]['type'] == 'date')
                        {
                            if('empty' == $value)
                            {
                                $filter_criteria_sql .= DbQuery::prepare(' AND ('.$filter_alias.' is null or '.$filter_alias.' = "" ) ');
                            }
                            else 
                            {
                                $filter_criteria_sql .= DbQuery::prepare(' AND (DATE_FORMAT('.$filter_alias.', "%d.%m.%Y")  = ":1")',array($value));
                            }
                            
                        }
                        elseif($this->filter_types[$column]['type'] == 'range')
                        {
                            if(null != $value['from'])
                            {
                                $filter_criteria_sql .= DbQuery::prepare(' AND ('.str_replace('_from','',$filter_alias).' > :1)',array($value['from']));
                            }
    
                            if(null != $value['to'])
                            {
                                $filter_criteria_sql .= DbQuery::prepare(' AND ('.str_replace('_to','',$filter_alias).' < :1)',array($value['to']));
                            }
                            
                        }
                        elseif($this->filter_types[$column]['type'] == 'date-range')
                        {
                            if('empty' == $value)
                            {
                                $filter_criteria_sql .= DbQuery::prepare(' AND ('.$filter_alias.' is null or '.$filter_alias.' = "" ) ');
                            }
                            else 
                            {
                                
                                
                                if(null != $value['from'])
                                {
                                    //$filter_criteria_sql .= DbQuery::prepare(' AND DATE_FORMAT('.$filter_alias.', "%d.%m.%Y")  = ":1"',array($value));
                                    
                                    $date_value_from_parts = explode('.',$value['from']);
                                    $date_value_from = $date_value_from_parts[2].'-'.$date_value_from_parts[1].'-'.$date_value_from_parts[0];
                                    
                                    $filter_criteria_sql .= DbQuery::prepare(' AND '.str_replace('_from','',$filter_alias).' >= ":1"',array($date_value_from));
                                }
        
                                if(null != $value['to'])
                                {
                                    //$filter_criteria_sql .= DbQuery::prepare(' AND DATE_FORMAT('.str_replace('_to','',$filter_alias).', "%d.%m.%Y") < ":1"',array($value['to']));  
                                    $date_value_to_parts = explode('.',$value['to']);
                                    $date_value_to = $date_value_to_parts[2].'-'.$date_value_to_parts[1].'-'.$date_value_to_parts[0];
                                    
                                    $filter_criteria_sql .= DbQuery::prepare(' AND '.str_replace('_to','',$filter_alias).' <= ":1"',array($date_value_to));
                                    
                                }
                            }
                            
                        }
                        elseif($this->filter_types[$column]['type'] == 'choice')
                        {
                            //$filter_criteria_sql .= DbQuery::prepare(' AND '.$filter_alias.' = ":1"',array($value));
                            $filter_criteria_sql .= ' AND '.$filter_alias.' = :'.$column;
                            $this->query_params[$column] =  $value;
                            
                        }
                        elseif($this->filter_types[$column]['type'] == 'custom')
                        {
                            $filter_criteria_sql .= $this->filter_types[$column]['sql_criteria'];
                        }
                        
                        
                        
                        if($this->filter_types[$column]['type'] == 'multi')
                        {
                            $filter_string = 'AND (';
                            foreach($filter_alias as $or_filter_alias)
                            {
                                $filter_array[] = $or_filter_alias.' like ":1%"';
                                //$filter_criteria_sql .= DbQuery::prepare(' AND ('.$filter_alias.' like ":1%")',array($value));
                                
                            }
                            $filter_string = 'AND ( '.implode(' OR ',$filter_array).' )';
                            $filter_criteria_sql .= DbQuery::prepare($filter_string,array($value));
                        }
                        else 
                        {
                            if(isset($this->filter_types[$column]['full_like']) && $this->filter_types[$column]['full_like'] ==true)   
                            {
                                $filter_criteria_sql .= DbQuery::prepare(' AND '.$filter_alias.' like "%:1%"',array(trim($value)));
                            }
                            elseif(
                                'empty' != $value && $this->filter_types[$column]['type'] != 'date-range' 
                                && $this->filter_types[$column]['type'] != 'range'
                                && $this->filter_types[$column]['type'] != 'choice'
                                && $this->filter_types[$column]['type'] != 'custom'
                               ) 
                            {
                                $value = (!is_array($value))  ? trim($value) : implode('',$value);
                                
                                if('' != $value)
                                {
                                    //$filter_criteria_sql .= DbQuery::prepare(' AND '.$filter_alias.' like ":val%"');
                                    //$filter_criteria_sql .= ' AND '.$filter_alias.' like ":'.$column.'%"';
                                    $filter_criteria_sql .= ' AND '.$filter_alias.' like :'.$column;
                                    $this->query_params[$column] =  $value.'%';
                                }
                                
                                
                            }
                        }
                    }
                    
                }
            }
            $filter_criteria_sql .= ' ';
        }
        
        if(null != $this->getPermanentFilter())
        {
            $filter_criteria_sql .= $this->getPermanentFilter();
        }

        return $filter_criteria_sql;
    }

    public function renderFilterField($index)
    {
        $filter_cell = '<input  class="datagrid_filter_'.$index.'" type="text" name="datagridfilter['.$index.']" value="'.$this->getFilterCriteriaValue($index).'" />';   
		$filter_form = new Form();
		$filter_form->setName('datagridfilter');
        
        if(is_array($this->filter_types) && array_key_exists($index, $this->filter_types))
        {
            if($this->filter_types[$index]['type'] == 'choice')
            {
                $selected_value = $this->getFilterCriteriaValue($index);
                //var_dump($selected_value);
                $choices = $this->filter_types[$index]['choices'];
                if($this->filter_types[$index]['add_blank_choice'])
                {
                     //$choices = array_merge(array('' => '...'),$choices);
                     $choices['all'] = '...';
                     //array_unshift($var)
                }
                
                if(null == $selected_value)
                {
                    $selected_value = 'all';
                }
                  
                $field_def = array ( 'type' => 'choice','choices' => $choices);
                $filter_form->setField($index, $field_def);
                
                $filter_cell = $filter_form->renderSelectTag($index,array('class' => 'span12 datagrid_filter_'.$index));
               
                //$filter_cell =  FormHelper::selectTag('datagridfilter['.$index.']',$choices,$selected_value,array('class' => 'span12 datagrid_filter_'.$index));
            }
            
            if($this->filter_types[$index]['type'] == 'date')
            {
                $filter_cell =  FormHelper::renderDateCalendarTag('datagridfilter['.$index.']',null,array('id' => md5(rand(1,100).time()),'class' => 'span12 datagrid_filter_'.$index));
            }
            
            if($this->filter_types[$index]['type'] == 'range')
            {
                $value =   $this->getFilterCriteriaValue($index);
                $filter_cell =  '<span class="range_row">'.Translator::Translate('Od:').FormHelper::inputTag('datagridfilter['.$index.'][from]',$value['from'],array('class' => 'span12 datagridfilter_range')).'</span>';
                $filter_cell .=  '<span class="range_row">'.Translator::Translate('Do:').FormHelper::inputTag('datagridfilter['.$index.'][to]',$value['to'],array('class' => 'span12 datagridfilter_range')).'</span>';
            }
            if($this->filter_types[$index]['type'] == 'date-range')
            {
                $value =   $this->getFilterCriteriaValue($index);
                $filter_cell =  FormHelper::renderDateCalendarTag('datagridfilter['.$index.'][from]',$value['from'],array('class' => 'span12 datagridfilter_range','id' => md5(rand(1,100).time())));
                $filter_cell .=  FormHelper::renderDateCalendarTag('datagridfilter['.$index.'][to]',$value['to'],array('class' => 'span12 datagridfilter_range','id' => md5(rand(1,100).time())));
            }
            if($this->filter_types[$index]['type'] == 'hidden')
            {
                $filter_cell =  '';
            }
        }
        
        return $filter_cell;
    }
    
    public function getRecordsCount()
    {
    	$query = DbQuery::prepare($this->getCountQuery());
    	
    	foreach($this->query_params as $column => $value)
    	{
    		if("" != $value)
    		{
    			$query->bindParam(':'.$column, $value);
    		}
    	}
    	
    	$count  = $query->execute()->fetchOne(\PDO::FETCH_ASSOC);
    	return $count['COUNT'];
    	
    }
    
    public function getRecords()
    {
        if(null == $this->records)
        {
            //echo $this->getRecordsQuery();
            //$this->records =  DbQuery::executeWithResult($this->getRecordsQuery());
            
            
            $query = DbQuery::prepare($this->getRecordsQuery());
            foreach($this->query_params as $column => $value)
            {
                if("" != $value)   
                {
                    $query->bindParam(':'.$column, $value);
                }
            }
            $this->records = $query->execute()
            ->fetchAll(\PDO::FETCH_ASSOC);
           
        }
        return $this->records;
    }
    
    public function getRecordsAs($objectClassName)
    {
    	$raw_records = $this->getRecords();
		$object_class_name = ucfirst($objectClassName);
		$records = array();
		foreach ($raw_records as $raw_record)
		{
			$object = new $object_class_name($raw_record);
			$records[] = $object;
		}

    	return $records;
    }
    
    public function getRecordsQuery()
    {
        $start = ($this->getPage()*$this->getItemsPerPage()) - $this->getItemsPerPage();
        $page_query = $this->getQuery();
        $page_query .= $this->getFilterCriteriaSql();
		
		if('' == $this->getSortCriteriaSql())
		{
			 $page_query .= $this->getDefaultSortCriteria();
		}
		else
		{
			$page_query .= $this->getSortCriteriaSql();
		}
       
        $page_query .= ' LIMIT '.$start.', '.$this->getItemsPerPage();
        return $page_query;
    }
    
    public function getPager()
    {
        
        if(null == $this->pager)
        {
            $pager = new Pager();
            $pager->setItemsPerPage($this->getItemsPerPage());
            $pager->setCount(DbQuery::getCount($this->getCountQuery()));
            $pager->setPage($this->getPage());
            $pager->setQuery($this->getQuery());
            
            $this->pager = $pager;
        }
        
        return $this->pager;
    }

    public function setDefaultSortCriteria($val)
    {
        $this->default_sort_criteria = $val;
    }
    
    public function getDefaultSortCriteria()
    {
        return $this->default_sort_criteria;
    }
    
    public function addFilterListener($request_method, $action, $filter)
    {
        if('POST' == $request_method)
        {
            if(null != $action && 'filter' == $action)
            {
                 $this->setFilterCriteria($filter);
                 Request::Redirect();
            }
        }
    }
    
    public function exportData($options = array())
    {
        header("Cache-Control: maxage=1"); //In seconds
        header("Pragma: public");
        header("Expires: 0"); 
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment; filename=export.xls;");
        header("Content-Transfer-Encoding: binary");
        header( "Content-type: application/vnd.ms-excel; charset=UTF-8;" );  
        $page_query = $this->getQuery();
        $page_query .= $this->getFilterCriteriaSql();
        $page_query .= $this->getSortCriteriaSql();
        $records =  DbQuery::executeWithResult($page_query);
        include($options['template']);
        exit;
    }
    
    public function renderSortRow($options = array())
    {
        ob_start();
        $table_rows = $this->getTableRows();
        include(LIB_DIR.'/Core/Datagrid/sort_row.php');
        $row = ob_get_contents();
        ob_end_clean();
        return $row;
    }
    
    public function renderFilterRow($options = array())
    {
        ob_start();
        $table_rows = $this->getTableRows();
        include(LIB_DIR.'/Core/Datagrid/filter_row.php');
        $row = ob_get_contents();
        ob_end_clean();
        return $row;
    }
    
    public function getRepository()
    {
    	return $this->repository;
    }
    
    public function setRepository($repository)
    {
    	$this->repository = $repository;
    }
    
   
	
}

?>