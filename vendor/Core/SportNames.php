<?php namespace Core;

use Core\Db;

class SportNames
{
    use Db;
    
    private $db;
    private $sportNames;
    
    function __construct()
    {
        $this->db = $this->getDb();
        $this->sportNames = $this->db->sport()->fetchPairs('id', 'name');
    }
    
    function setLang($lang) {}
    
    function toSportName(int $id) { return $this->sportNames[$id]; }
    
    function toArray() { return $this->sportNames; }
}