<?php
namespace Core;

class AdjacencyTree 
{
	protected $router;
	protected $translator;
    
        /*
	 * nazov stlpca ktory urcuje hierarchiu
	 */
	protected $parent_column_name;
	
	/*
	 * nazov databazovej tabulky
	 */
	protected $table_name;
	
	/*
	 * stlpce, ktore ma vracat
	 */
	protected $columns;
	
	/*
	 * kriteia pomocou ktoreho sa ma vyberat;
	 */
	protected $criteria;
	
	/*
	 * strukturovane viacrozmerne pole ktore ma presne taku strukturu ako zaznamy v databaze
	 */
	protected $tree;

	/*
	 * Nazov stlpca, ktoreho hodnotou maju byt zaindexoivane prvky v strome
	 */
	 protected $index = 'id';

	/*
	 *jednorozmerne pole ktore ma presne taku strukturu ako zaznamy v databaze
	 */
	protected $table;
	
	/**
	 * Pole, v ktorom su ulozeny predkovia, idealne na breadcrumpy
	 */
	 protected $path;

    public function AdjacencyTree($parent = 'parent_id') 
    {
    	$this->parent_column_name = $parent;
		return $this;
    }
    
    function getRouter() {
return $this->router;
}

 function setRouter($router) {
$this->router = $router;
}

function getTranslator() {
return $this->translator;
}

 function setTranslator($translator) {
$this->translator = $translator;
}




    
     /*
    * Nastavi nazov stlpca, na zaklade ktoreho je previazanost medzi potomkami a rodicmi
    * 
    * @param string $val nazov stlpca, defaultne "parent"
    * @return AdjacencyTree  
    */
    public function setParentColumnName($val)
    {
     	$this->parent_column_name = $val;
     	return $this;
    }
    
    
    /*
    * Vracia  nazov stlpca, na zaklade ktoreho je previazanost medzi potomkami a rodicmi
    * 
    * @return string  nazov stlpca, na zaklade ktoreho je previazanost medzi potomkami a rodicmi
    */
    public function getParentColumnName()
    {
     	return $this->parent_column_name;
    }


    /*
    * Nastavi kriteria, na zaklade ktorych sa maju vyberat zaznamy
    * 
    * @param string $val query
    * @return AdjacencyTree  
    */
    public function setCriteria($val)
    {
     	$this->criteria = $val;
     	return $this;
    }
    
    
    /*
    * Vracia aktualne nastavene kriteria
    * 
    * @return string Aktuálne nastavené query
    */
    public function getCriteria()
    {
     	return $this->criteria;
    }
    
    /*
    * Nastavi nazov tabulky
    * 
    * @param string $val nazov tabulky
    * @return AdjacencyTree  
    */
    public function setTableName($val)
    {
     	$this->table_name = $val;
     	return $this;
    }
    
    
    /*
    * Vracia nazov tabulky
    * 
    * @return string nazov tabulky
    */
    public function getTableName()
    {
     	return $this->table_name;
    }
    
     /*
    * Nastavi stlpce, ktore ma vybrat pri selektem, je to tu kvoli vykonu, defaultne je nastavena *
    * 
    * @param array $columns pole s nazvami databazaovych stlpcov
    * @return AdjacencyTree  
    */
    public function setColumns(array $columns)
    {
     	$this->columns = $columns;
     	return $this;
    }
    
    
    /*
    * Vracia aktualne nastavene stlpce, ak nie su nastavene, vracia *
    * 
    * @return string Aktuálne nastavené stlpce
    */
    public function getColumns($options = array())
    {

     	
     	if(isset($options['prefix']))
     	{
     		$prefix = $options['prefix'];
     		if(empty($this->columns))
	     	{
	     		return ' '.$prefix.'.* ';
	     	}
	     	else
	     	{
	     		return $prefix.'.'.implode(' , '.$prefix.'.',$this->columns);
	     	}
     	}
     	else
     	{
     		if(empty($this->columns))
	     	{
	     		return ' * ';
	     	}
	     	else
	     	{
	     		return implode(' , ',$this->columns);
	     	}
     	}
     	
    }
    
    /*
     * Nastavi index, pod akym maju byt zaindexovane prvky v strome
     */
    public function setIndex($val)
    {
    	$this->index = $val;
    	return $this;
    }
    
    /*
     * Vrati index
     */
    public function getIndex()
    {
    	return $this->index;
    }
    
    
    /*
     * Overuje ci ma zaznam este nejakych potomkov
     * 
     * @param mixed $id identifikátor záznamu
     */
	public function hasChildren($id)
	{
		
		if(!is_array($id))
		{
			$parent_criteria = 	$this->getParentColumnName().' = '.$id;
		}
		else
		{
			$parent_criteria = 	$this->getParentColumnName().' = '. implode(' OR '.$this->getParentColumnName().' = ',$id);
			
		}
		
		//$count = DbQuery::executeWithSingleResult('SELECT count(*) as count from '.$this->getTableName().'  WHERE ('.$parent_criteria.') '.$this->getCriteria());
        
         $count = DbQuery::prepare('SELECT count(*) as count from '.$this->getTableName().'  WHERE ('.$parent_criteria.') '.$this->getCriteria())
            ->execute()
            ->fetchOne(\PDO::FETCH_ASSOC);
            
		if('0' != $count['count'])
		{
			return true;
		}
		else
		{
			return false;
		}
	}
    
    /*
     * vracia pole vsetkych bezprostrednych potomkov aktualneho zaznamu
     * 
     * @param mixed $id identifikátor záznamu
     * @return array pole potomkov
     */
	public function getChildren($id)
	{
		$children = array();
		
		if(!is_array($id))
		{
			$parent_criteria = 	$this->getParentColumnName().' = '.$id;
		}
		else
		{
			$parent_criteria = 	$this->getParentColumnName().' = '. implode(' OR '.$this->getParentColumnName().' = ',$id);
			
		}
		
		//$db_childs = DbQuery::executeWithResult('SELECT '.$this->getColumns().' from '.$this->getTableName().' WHERE '.$parent_criteria.' '.$this->getCriteria());
        
         $db_childs = DbQuery::prepare('SELECT '.$this->getColumns().' from '.$this->getTableName().' WHERE '.$parent_criteria.' '.$this->getCriteria())
            ->execute()
            ->fetchAll(\PDO::FETCH_ASSOC);
        
		foreach($db_childs as $row)
		{
			$children[] = $row;
		}
		
		return $children;
	}


    /*
     * vytvori strukturovane viacrozmerne pole ktore ma presne taku strukturu ako zaznamy v databaze
     * je to vlastne presny obraz struktury celej aplikacie
     * 
     * @param mixed $id identifikátor záznamu
     * @return array strukturovane viacrozmerne pole ktore ma presne taku hierarchicku strukturu ako zaznamy v databaze
     */
	public function buildTree($id,$parent_record = null)
	{
	    $tmp = array();
      	if($this->hasChildren($id))
	    {
	    	$childs = $this->getChildren($id);
            foreach($childs as $child)
	        {
                if($this->hasChildren($child['id']))
                {
                	$child['children'] = $this->buildTree($child['id'],$child);
                	
                }
               
               	$tmp[$child[$this->getIndex()]] = $child; 
                
            }
	    }
	    $this->tree = $tmp;
        return $tmp;
	}
	
	public function getTree()
	{
		return $this->tree;
	}
	
	public function getArray()
	{
		return $this->tree;
	}
	
	
    /*
     * vytvori  pole ktore ma zoradene prvky podla toho aky je medzi nimi vztah, ale pole je na rozdiel od funkcie buildTree($id) iba jednorozmerne
     * 
     * @param mixed $id identifikátor záznamu od ktoreho sa ma zacat tvorit strom, je to vlastne identifikator vetvy, pri zadani 0 vyberie cely strom
     * @return array   pole ktore ma zoradene prvky podla toho aky je medzi nimi vztah
     */
	public function buildTable($id,$parent_record = null)
	{
      	if($this->hasChildren($id))
	    {
	    	$childs = $this->getChildren($id);
            foreach($childs as $child)
	        {
				$this->table[] = $child;
                if($this->hasChildren($child['id']))
                {
                	$this->buildTable($child[$this->getIndex()]);
                }
            }
	    }
	    return $this->table;
	}
	
	public function getTable()
	{
		return $this->table;
	}
	
	
	
	/*
	 * pretvori strom na tabulku, vyhodne vtedy ked potrebujem prechadazat cele pole, a nepotrebujem pritom stromovu strkukturu
	 */
	public function treeToTable($tree = null, $children = false)
	{
		if(null == $tree)
		{
			$tree = $this->tree;
		}
		
		foreach($tree as $branch)
        {
            if(isset($branch['children']))
            {
            	$branch_children = $branch['children'];
            }
            else
            {
            	$branch_children = false;;
            }
            
            if(!$children)
			{
				unset($branch['children']);
			}

           	$this->table[$branch[$this->getIndex()]] = $branch;


            if(null != $branch_children)
            {
                $this->treeToTable($branch_children);
            }
            
            
        }
        return $this->table;
	}
	
	
	
	public function renderList($tree)
   {
		$tree_string = '';
        if(!empty( $tree))
        {
            $tree_string .= "<ul>";
            foreach($tree as $branch)
            {
                
                $tree_string .= "<li class='open' id='".$branch['seo_id']."-".$branch['LVL']."'><span class='text'><a class='edit-category' href='' rel='".$branch['ID']."'>".$branch['NAME']."</a></span>";
                
                if(isset($branch['children']))
                {
                    $tree_string .= $this->renderList($branch['children']);
                }
                $tree_string .= "</li>";
            }
            $tree_string .= "</ul>";
        }
        
        return  $tree_string;
       
   }
   
 /**
  * 
  */
   public function getNodesFromTable($path,$table = null)
   {
	   	if(null == $table)
	   	{
	   		$table = $this->treeToTable($this->tree);
	   	}
	   	
	   	foreach($table as $branch)
        {
			if(in_array($branch['seo_id'],$path))
			{
	           	$extracted_nodes[$branch[$this->getIndex()]] = $branch;
			}
        }
        
        return $extracted_nodes;  
   }
   
    /**
	 * vrati nadradene zaznamy
	 */
    public function getPath($child_id,$end)
    {
        //$row = DbQuery::executeWithSingleResult("SELECT ".$this->getColumns()." from ".$this->getTableName()."  WHERE id like '".$child_id."'");

        $row = DbQuery::prepare("SELECT ".$this->getColumns()." from ".$this->getTableName()."  WHERE id = '".$child_id."'")
            ->execute()
            ->fetchOne(\PDO::FETCH_ASSOC);

        if($end != $row[$this->getParentColumnName()] && null != $row )
        {
        	
        	$this->path[$row[$this->getIndex()]] = $row;
            return $this->getPath($row[$this->getParentColumnName()],$end);
        }
        else
        {
        	$this->path[$row[$this->getIndex()]] = $row;
            return $this->path;
        } 
    }
    
    /**
     * pripoji celu vetvu k nejakemu node
     * @param mixed $parent_id id uzlu ku ktoremu sa to ma pripojit
     */
    public function createBranch($parent_id,$branch)
    {
	    foreach($branch as $node)
		{
			unset($node['id']);
			$node['parent'] = $parent_id;
			$node->setTableName($this->getTableName());
			//DbQuery::insertFromArray($node,$this->getTableName());
			
			$node->save();
			
			$node_id = mysql_insert_id();
			if(isset($node['children']) && !empty($node['children']))
			{
				//echo $node['nazov']. "chidlren";
				$this->createBranch($node_id,$node['children']);
			}
		}
    }
    
    /**
     * Pripoji vetvu, neuklada do databazy ale iba pripoji k existujucemu stromu
     * @param $parent_id
     * @param $branch
     * @return unknown_type
     */
	public function appendBranch($parent_id,$branch)
    {
		//var_dump($this->tree);
    	$node = $this->getNode($parent_id);
		//var_dump($node);
		
		$children = $node['children'];
		
		foreach($branch as $branch_item)
		{
			$children[$branch_item['id']] = $branch_item;
		}
		
	    //$children[$branch['id']] = $branch;
	    $node['children'] = $children;
	    
	    $this->setNode($parent_id,$node);
	    
    }
    
    /*
     * Vrati uzol podla id
     */
    public function getNode($node_id,$tree_array = null)
    {
    	if(null == $tree_array)
    	{
    		$tree_array = $this->tree;
    	}
    	
    	foreach($tree_array as $key => $node)
		{
			if($key == $node_id)
			{
				return $node;
			}
			else
			{
				if(isset($node['children']))
				{
					return $this->getNode($node_id,$node['children']);
				}
				
			}
		}
    }
    
 	public function setNode($node_id,$node_val,$tree_array = null)
    {
    	if(null == $tree_array)
    	{
    		$tree_array = $this->tree;
    	}
    	
    	foreach($tree_array as $key => $node)
		{
			if($key == $node_id)
			{
				$node = $node_val;
				return $node;
			}
			else
			{
				if(isset($node['children']))
				{
					return $this->setNode($node_id,$node_val,$node['children']);
				}
				
				
			}
		}
    }
    
    /*
     * Vrati "kmen" stromu t.j. najvrchnejsiu urovn a rozbalenu vetvu po zvoleny uzol
     * 
     * @param $node_id id uzla po ktory sa ma rozbalit vetva stromu
     * @param $root_id identifikátor najvyššej úrovne ktora sa ma zobrazit
     * $return array Strukturovane viacrozmerne pole, s prvkami usporiadanymi v stromovej strukture
     */
    public function getSinglePathTree($node_id, $root_id = 'obsah')
    {
    	/*
		 * Ok a teraz potrebujem aktualnu vetvu, t.j. vsetky stranky od aktualnej nahor, 
		 * takze si prejdem po jednom provky path co su vlastne priamy predkovia aktualnej stranky
		 * a pridam k nim ich surodencov
		 */
		$branch = array();
		//najrpv si to musim otocit aby som zacal od vrchu
		$path = $this->getPath($node_id,'obsah');
		$path = array_reverse($path);
		//var_dump($path);

		//ak som na hlavnej stranke tak toto nemusim robit, pretoze nemam zaidnu vetvu
		$branch_items = array();
		if(null == $path or  null == current($path) or '0' != $path[0])
		{
			foreach($path as $path_item)
			{
				$siblings = DbQuery::executeWithResult('SELECT '.$this->getColumns().' from '.$this->getTableName().' WHERE parent= "'.$path_item['id'].'"   '.$this->getCriteria());
	
				$path_item_siblings	= array();
				foreach($siblings as $sibling)
				{
					$sibling['PATH'] = $path_item['PATH'].'/'.$sibling['SEO_ID'];
					$sibling['LVL'] = $path_item['LVL']+1;
					$path_item_siblings[$sibling['id']] = $sibling;
				}
					
				$branch_items[$path_item['id']]['children'] = $path_item_siblings;
				
			}

			
			foreach($branch_items as $item_key => $item)
			{
				foreach($item['children'] as $key => $child)
				{
					if(array_key_exists($key,$branch_items))
					{
						$item['children'][$key]['children'] = $branch_items[$key]['children'];
					}
				}
			}
		}
		//var_dump($branch_items);

		
		//Teraz generujem najvrchnejsiu uroven
		$main_items = DbQuery::executeWithResult('SELECT '.$this->getColumns().' from '.$this->getTableName().' WHERE parent = "'.$root_id.'" '.$this->getCriteria());
		
		$main_tree = array();
		foreach($main_items as $main_item)
		{
			$main_tree[$main_item['id']] = $main_item;
			if(array_key_exists($main_item['id'],$branch_items))
			{
				$main_tree[$main_item['id']]['children'] = $branch_items[$main_item['id']]['children'];
			}
		}
		
		
		//var_dump($main_tree);
		return $main_tree;
		//$this->tree->setTree($main_tree);
    }   
	
	public function moveNode($node_id,$parent_id, $position )
   {
		//najprv vyberiem parenta
		$parent = DbQuery::getById($parent_id,$this->getTableName());
		
		
		
		//teraz poposuvam vsetky prvky tejto urovne o jedno dole
		DbQuery::execute('Update '.$this->getTableName().' set sort_order = sort_order+1 WHERE PARENT = '.$parent_id);
		
		//teraz vlozim prvok
		DbQuery::execute('Update '.$this->getTableName().' set sort_order = '.$position.', parent = '.$parent_id.', lvl = '.($parent['lvl']+1).' WHERE ID = '.$node_id);
		
		
		//teraz rekurzivne updatujem level vsetkych podriadenych
		$this->updateLevel($node_id,$parent['lvl']+2);

   }
   
   public function updateLevel($parent_id,$lvl)
   {
   		//najprv vyberiem parenta
		$parent = DbQuery::getById($parent_id,$this->getTableName());
		
		//updtnem level
		DbQuery::execute('Update '.$this->getTableName().' set lvl =  '.$lvl.' WHERE PARENT = '.$parent_id);
		
		//a rekurzivne updatnem deti
		$items = DbQuery::executeWithResult('Select * from '.$this->getTableName().'  WHERE PARENT = '.$parent_id);   
		
		if(!empty($items))	
		{
			foreach($items as $item)
			{
				$this->updateLevel($item['id'],$item['lvl']+1);
			}
		}
   }
   
   /*
    * skopiruje celu vetvu
    */
   public static function copyBranch($branch, $parent_id,$options = array('table_name' => 'obsah_obsah'))
   {
   		foreach($branch as $node)
   		{
   			//var_dump($options);
   			//echo $node['id'];
   			
   			$node->setTableName($options['table_name']);
   			$node['id']  = null;
   			$node['parent'] = $parent_id;
   			if(isset($options['lang']))
   			{
   				$node['lang'] = $options['lang'];
   			}
   			$node->save();
   			if(isset($node['children']) && !empty($node['children']))
			{
				//echo $node['nazov']. "chidlren";
				//$this->createBranch($node_id,$node['children']);
				self::copyBranch($node['children'], $node['id'],$options);
			}
			
			
   		}
   }
   
   

   
  
    

}
?>
