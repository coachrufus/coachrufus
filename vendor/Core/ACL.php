<?php
namespace Core;
/**
 *  @package Core
 * Acces control list class
 * @author Marek Hubáček
 *
 */
class ACL 
{
	/**
	 * acces control list
	 * @var array
	 */
	private $role_list;
	
	
	/**
	 * 
	 * @param string $role
	 * @param string $permission
	 */
	public function allowRole($role,$permission)
	{
		if(!is_array($role))
		{
			$this->role_list[$role][$permission] = 'allow';
		}
		else
		{
			foreach($role as $r)
			{
				$this->role_list[$r][$permission] = 'allow';
			}
		}
		
	}
	
	/**
	 * 
	 * @param string $role
	 * @param stoing $permission
	 */
	public function denyRole($role,$permission)
	{
		$this->role_list[$role][$permission] = 'deny';
	}
	
	
	/**
	 * 
	 * @param string $role
	 * @param string $permission
	 * @return boolean
	 */
	public function isAllowedRole($role,$permission)
	{
		if(!array_key_exists($role, $this->role_list))
		{
			return false;
		}
		
		if(!array_key_exists($permission, $this->role_list[$role]))
		{
			return false;
		}
		
		if($this->role_list[$role][$permission] == 'allow')
		{
			return true;
		}
		return false;
	}
	
	
	/**
	 * 
	 * @param string $role
	 * @param string $permission
	 * @return boolean
	 */
	public function isDenyRole($role,$permission)
	{
		if(!array_key_exists($role, $this->role_list))
		{
			return true;
		}
		
		if(!array_key_exists($permission, $this->role_list[$role]))
		{
			return true;
		}
		
		if($this->role_list[$role][$permission] == 'deny')
		{
			return true;
		}
		return false;
	}

	public function getRoleList()
	{
	    return $this->role_list;
	}

	public function setRoleList($role_list)
	{
	    $this->role_list = $role_list;
	}
}