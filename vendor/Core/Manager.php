<?php
namespace Core;
class Manager {

    protected $repository;

    public function __construct($repository = null)
    {
        if(null != $repository)
        {
            $this->setRepository($repository);
        }
    }

    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * 
     * @param newVal
     */
    public function setRepository($newVal)
    {
        $this->repository = $newVal;
    }

    public function convertEntityToArray($entity)
    {
        return $this->getRepository()->convertToArray($entity);
    }
    
  

}
