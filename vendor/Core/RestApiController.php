<?php
namespace Core;

class RestApiController extends RestController
{
    
    
    protected function checkApiKey($requestKey = null)
    {
        $availableKyes = ['logamic@40106144409386391','dev','app@40101499408386491','touch4it@40101499408386491','altamira@40101499408386491'];
        if(null == $requestKey)
        {
            $requestKey = $this->getRequest()->get('apikey');
            if(null == $requestKey)
            {
                 $data = $this->getRequest()->getDataFromEncodedApiUrl();
                 $requestKey = $data['apikey'];
            }
        }
        
        if(!in_array($requestKey, $availableKyes))
        {
            return false;
        }     
        
        return true;
    }
    
    public function createUserFromRequest($userId = null)
    {
        if(null == $userId)
        {
            $userId = $this->getRequest()->get('user_id');
            
            if(null == $userId)
            {
                 $data = $this->getRequest()->getDataFromEncodedApiUrl();
                 $userId = $data['user_id'];
            }
        }
        
        $security = ServiceLayer::getService('security');
        $identityProvider = $security->getIdentityProvider();
        $user = $identityProvider->findUserById($userId);
        $security->authenticateUserEntity($user);    
        
        if(null == $user->getDefaultLang())
        {
            $user->setDefaultLang('sk');
        }
        
        $_SESSION['app_lang'] = $user->getDefaultLang();
        $this->getTranslator()->setLang($user->getDefaultLang());

         
        return $user;
    }
    
    public function unvalidApiKeyResult()
    {
        $result['params']['apikey'] = $this->getRequest()->get('apikey');
        $result['status'] = 'ERROR';
        $result['error_message'] = 'UNVALID API KEY';
        return $this->asJson($result);
    }
    
    
    public function getRequestParams()
    {
        $params  = $_GET;
        unset($params['lang']);
        return $params;
        
    }  

  
    public function asJson($data, $status = 'success') {
            header("Content-Type: application/json; charset=utf-8");
            return $this->sendContent(json_encode($data,JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES ), 'application/json', $status);
    }
    
    
    
}