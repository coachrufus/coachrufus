<?php namespace Core;

class StringUtils
{
    static function mb_ucfirst($string)
    {
        return mb_strtoupper(mb_substr($string, 0, 1)) . mb_strtolower(mb_substr($string, 1));
    }
}

class NameCleaner
{    
    static function clean($name)
    {
        $names = explode(' ', $name);
        $result = [];
        foreach ($names as $name)
        {
            $name = trim($name);
            if (!empty($name)) $result[] = StringUtils::mb_ucfirst($name);
        }
        return trim(implode(' ', $result));
    }
}