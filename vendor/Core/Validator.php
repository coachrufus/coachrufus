<?php
namespace Core;
/**
 *
 * @author Marek Hubáček
 *
 */
class Validator 
{
    protected $errors = array();
    protected $rules = array();
    protected $data = array();
    protected $message_template ='<label for="{{key}}" class="control-label control-label-error"><i class="fa fa-exclamation-circle"></i> {{message}}</label>';
    
   
            
    public function __construct($rules = null) {
            $this->setRules($rules);
    }
    
    public function setMessageTemplate($template)
    {
    	$this->message_template = $template;
    }
    
    public function getMessageTemplate()
    {
    	return $this->message_template;
    }

	public function getErrors()
	{
	    return $this->errors;
	}

	public function setErrors($errors)
	{
	    $this->errors = $errors;
	}
	
	public function hasErrors()
	{
	    if(empty($this->errors))
	    {
	    	return false;
	    }
	    return true;
	}
	
	public function addErrorMessage($index,$message)
	{
		$this->errors[$index] = $message;
	}
	
	public function getErrorMessage($index)
	{
		if($this->hasError($index))
		{
			return $this->errors[$index];
		}
		return null;
	}
	
	public function hasError($index)
	{
		if(array_key_exists($index, $this->errors))
		{
			return true;
		}
		return false;
	}

	public function getRules()
	{
	    return $this->rules;
	}

	public function setRules($rules)
	{
	    $this->rules = $rules;
	}

	public function getData()
	{
	    return $this->data;
	}

	public function setData($data)
	{
	    $this->data = $data;
	}
	
	public function validateData()
	{
		$rules = $this->getRules();
		$data = $this->getData();
		//$translator = ServiceLayer::getService('translator');
		
		foreach($rules as $key => $rule)
		{
			if(array_key_exists($key, $data))
                        {
                            if($rule['required'] == true && (!array_key_exists($key, $data) or null == $data[$key]))
                            {
                                    //$mesage = $translator->translate('Povinné pole');
                                    $mesage = 'Required field';
                                    if(array_key_exists('message',$rule))
                                    {
                                            $mesage = $rule['message'];
                                    }
                                    $this->addErrorMessage($key,$mesage);				
                            }

                            if($rule['type'] == 'int' && false == $this->validateInteger($data[$key]))
                            {
                                //$mesage = $translator->translate('Povolené je iba číslo');
                                $mesage = 'Only numbers allowed';
                                if(array_key_exists('message',$rule))
                                {
                                        $mesage = $rule['message'];
                                }

                                $this->addErrorMessage($key,$mesage);		
                            }

                            if(array_key_exists('max_length', $rule) && $rule['type'] == 'string'  && strlen($data[$key]) > $rule['max_length'])
                            {
                                //$mesage = $translator->translate('maximálny povolený počet znakov je '.$rule['max_length']);
                                $mesage = 'maximálny povolený počet znakov je '.$rule['max_length'];
                                if(array_key_exists('message',$rule))
                                {
                                        $mesage = $rule['message'];
                                }

                                $this->addErrorMessage($key,$mesage);		
                            }
                        }
                    
                        
		}
	}
	
	public function showError($key,$options = array())
	{
		if(!is_array($options))
                {
                    $options = array('message' => $options);
                }
            
            
                if($this->hasError($key))
		{
			if(!array_key_exists('template', $options))
			{
				$template = $this->getMessageTemplate();
			}
			else
			{
				$template = $options['template'];
			}
			
			if(!array_key_exists('message', $options))
			{
				$message = $this->getErrorMessage($key);
			}
			else
			{
				$message = $options['message'];
			}

			$result = str_replace('{{message}}', $message, $template);
			$result = str_replace('{{key}}', $key, $result);
			return $result;
		}
	}
	
	public function showAllErrors()
	{
		if($this->hasErrors())
		{
			//$this->setMessageTemplate('<label for="{{key}}" class="error">{{key}}: {{message}}</label>');

                    $result = '<div class="alert alert-danger" style="display: block;">';
                    foreach ($this->getErrors() as $key =>  $error)
                    {
                        $result .= $this->showError($key,array('template' => '<label for="{{key}}" >{{key}}: {{message}}</label><br />'));
                    }
                    $result .= '</div>';
			/*
			$rules = $this->getRules();
			
			$result = '<div class="alert alert-danger" style="display: block;">';
			foreach($rules as $key => $rule)
			{
				$result .= $this->showError($key,array('template' => '<label for="{{key}}" >{{key}}: {{message}}</label><br />'));
			}
			
			$result .= '</div>';
			*/
			return $result;
		}
	}
	
	public function validateMobilNumber($phoneNumber,$lang = 'sk')
	{
		$predvolby = array('0905','0906','0907','0908','0915','0916','0917','0918','0919','0901','0902','0903','0904','0910','0911','0912','0914','0940','0944','0948','0949','0940','0919','959');
		$predvolbyStr = implode('|', $predvolby);
		//$pattern = '~^\+421( ?)('.$predvolbyStr.')\1\d{3}\1\d{3}$~';
		$pattern = '~^(\+421)? ?('.$predvolbyStr.') ?[0-9]{3} ?[0-9]{3}~';
		
		if (preg_match($pattern, $phoneNumber))
		{
			return true;
		}
		return false;
	}
	
	public function validatePassword($password)
	{
		//Minimálny počet 8 znakov
		//Použitie minimálne 1 veľkého a 1 malého písmena.
		//Použiť aspoň 1 číslo
		
		if (strlen($password) < 8) {
			return false;
		}
		
		if (!preg_match("#[0-9]+#", $password)) {
			return false;
		}
		
		if (!preg_match("#[a-z]+#", $password)) {
			return false;
		}
		if (!preg_match("#[A-Z]+#", $password)) {
			return false;
		}
		
		return true;
	}
	
	/**
	 * skontroluje ci ma email spravny format
	 */
	public function validateEmail($email)
	{
		 
		//toto je uplne basic validacia
		if (preg_match("/^[a-z0-9_-]+[a-z0-9_.-]*@[a-z0-9_-]+[a-z0-9_.-]*\.[a-z]{2,5}$/i", $email))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
        
        public function validateInteger($number)
     {
     		if (preg_match("/^[0-9]*$/", $number)) 
                {
                   return true;
                }
                else
                {


                        return false;
                }
     }
}
?>