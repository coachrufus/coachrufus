<?php

namespace Core;

/**
 * Trieda na dynamicky preklad výrazov
 */
class Translator {

    public $lang;
    private $translated_phrases = array();
    private $category = 'default';
    private $translations = array();

    public function __construct($lang = null, $category = null)
    {
        if (null != $lang)
        {
            $this->lang = $lang;
        }
        else
        {
            $this->lang = $_SESSION['app_lang'];
        }

        if (null != $category)
        {
            $this->category = $category;
        }
    }

    function getLang()
    {
        return $this->lang;
    }

    function setLang($lang)
    {
        if (null != $lang)
        {
            $this->lang = $lang;
        }
    }

    public function loadTranslations()
    {
         $data = \Core\DbQuery::prepare('SELECT id,keycode, sk_value, en_value, sk_tooltip, en_tooltip FROM translations')
                    ->execute()
                    ->fetchAll(\PDO::FETCH_ASSOC);

            foreach ($data as $d)
            {
                $translations[$d['keycode']]['sk'] = array('value' => $d['sk_value'],'id' => $d['id'], 'tooltip' => $d['sk_tooltip']);
                $translations[$d['keycode']]['en'] = array('value' => $d['en_value'],'id' => $d['id'], 'tooltip' => $d['en_tooltip']);
            }
            $this->setTranslations($translations);
    }
    
    function getTranslations()
    {

        if (empty($this->translations))
        {
           $this-> loadTranslations();
        }
        return $this->translations;
    }

    function setTranslations($translations)
    {
        $this->translations = $translations;
    }

    public function Translate($phrase, $lang = null)
    {
        return $this->translatePhrase($phrase, $lang);
    }

    public function getTranslationsFor($phrase)
    {
        $phrase_hash = md5($phrase);
        //najprv overenie ci uz nahodou neexistuje
        if (array_key_exists($phrase_hash, $this->translated_phrases))
        {
            if (in_array($this->getLang(), $this->translated_phrases[$phrase_hash]))
            {
                return $this->translated_phrases[$phrase_hash][$phrase['lang']];
            }
        }


        //neexsituje tak ho treba vytiahnut
        //najprv ziskat id_hash pre danu frazu
        $query = DbQuery::prepare('SELECT id, lang, value, id_hash FROM translations where value = :phrase');
        $query->bindParam(':phrase', $phrase);
        $db_phrase = $query->execute()->fetchOne(PDO::FETCH_ASSOC);

        //ak neexituje vracia null
        if (false == $db_phrase)
        {
            return null;
        }


        //teraz ziskat preklad do konretneho jazyka
        $query = DbQuery::prepare('SELECT id, lang, value, id_hash FROM translations where id_hash = :id_hash AND lang = :lang');
        $query->bindParam(':id_hash', $db_phrase['id_hash']);
        $query->bindParam(':lang', $this->getLang());
        $translated_phrase = $query->execute()->fetchOne(PDO::FETCH_ASSOC);

        $this->translated_phrases[$phrase_hash][$phrase['lang']] = $translated_phrase['value'];

        return $this->translated_phrases[$phrase_hash][$phrase['lang']];
    }

    /**
     * Prelozi dany retazec, ak neexistuje preklad, tak vrati defaultny retazec
     * @param phrase string kod retazca ku ktorému sa hľadá v databáze odpovedajúci preklad podla jazyka
     * @param $lang string 
     * 
     */
    public function translatePhrase($phrase, $lang = null)
    {
        if (null == $lang)
        {
            $lang = $this->getLang();
        }

    

        $translations = $this->getTranslations();


        if (array_key_exists($phrase, $translations))
        {
            $translated =  $translations[$phrase][$lang];
        }
        else
        {
            $db_data['keycode'] = $phrase;
            \Core\DbQuery::insertFromArray($db_data, 'translations');
            $this->loadTranslations();
        }

        $translated =  $translations[$phrase][$lang];
        
        
        //tooltip
        $tooltip = '';
        if(null != $translated['tooltip'])
        {
            $tooltip = ' <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="'.$translated['tooltip'].'"></i>';
        }
        
        
        if (null == $translated['value'])
        {

            //return 'UT:'.$phrase;
            if (array_key_exists('translate_mode', $_SESSION) && $_SESSION['translate_mode'] == true)
            {
                return '<span class="untranslated">' . $phrase . '<span data-href="/translator/edit?id=' . $translated['id'] . '" data-code="' . $phrase . '" class="tr-trigger">T</span></span>';
            }
            else
            {
                return $phrase;
            }
        }
        elseif ($translated['value'] != '')
        {
             if (array_key_exists('translate_mode', $_SESSION) && $_SESSION['translate_mode'] == true)
            {
                return '<span class="untranslated">' . $translated['value'] .$tooltip. '<span data-href="/translator/edit?id=' . $translated['id'] . '" data-code="' . $phrase . '" class="tr-trigger">T</span></span>';
            }
            else
            {
                 return $translated['value'].$tooltip;
            }
            
           
        }
    }

    public function getRegionalDate($format, $date)
    {
        return strftime($format, $date->getTimestamp());
    }

}

?>