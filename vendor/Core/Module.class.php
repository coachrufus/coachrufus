<?php
namespace Core;
class Module
{

    private $base_dir; 
    private $name;

    public function setBaseDir($val)
    {
        $this->base_dir = $val;
    }
    
    public function getBaseDir()
    {
        return $this->base_dir;
    }
    
    
    public function setName($val)
    {
        $this->name = $val;
    }
    
    public function getName()
    {
        return $this->name;
    }
}
