<?php

namespace Core;


class FormHandler {
	
	/**
	 * Surove data z formularu
	 * @var unknown
	 */
	private $postData;
	
	
	/**
	 * Uprave a ocistene data
	 * @var unknown
	 */
	private $sanitizedData;
	
	/**
	 * 
	 * @var unknown
	 */
	private $validators;
	

    /**
     * postData
     * @return unkown
     */
    public function getPostData()
    {
        return $this->postData;
    }

    /**
     * postData
     * @param unkown $postData
     * @return RegisterFormHandler
     */
    public function setPostData($postData)
    {
        $this->postData = $postData;
    }
    
    /**
     * sanitizedData
     * @return unkown
     */
    public function getSanitizedData()
    {
    	return $this->sanitizedData;
    }
    
    /**
     * sanitizedData
     * @param unkown $sanitizedData
     * @return RegisterFormHandler
     */
    public function setSanitizedData($sanitizedData)
    {
    	$this->sanitizedData = $sanitizedData;
    }
    
    /**
     * validators
     * @return unkown
     */
    public function getValidators()
    {
    	return $this->validators;
    }
    
    /**
     * validators
     * @param unkown $validators
     * @return FormHandler
     */
    public function setValidators($validators)
    {
    	$this->validators = $validators;
    	return $this;
    }
    
    public function setValidator($index,$validator)
    {
    	$this->validators[$index] = $validator;
    }
    
    public function getValidator($index)
    {
    	return $this->validators[$index];
    }
    
    public function sanitizeData()
    {
    	$this->setSanitizedData($this->postData);
    }
    
    public function validateData()
    {
    	$data = $this->sanitizeData();
    	return true;
    }
    
    
    
    public function persistData()
    {
    	
    }


   

  


   

}