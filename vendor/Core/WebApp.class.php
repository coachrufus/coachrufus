<?php
namespace Core;
use Core\ServiceLayer as ServiceLayer;


class WebApp
{
	private static $instance;
	private $debug_info = array();
	//private $app_dir;
	//private $uri;
	//private $modules;
	//private $module;
	//private $pathinfo;
	//private $lang = 'sk';
    
    private $modules;
    private $module_info;
    private $module_config;
    private $current_module;
    private $namespaces;
    

	public static function getInstance()
	{
		if (!self::$instance) 
	    { 
	        self::$instance = new WebApp(); 
	    } 
	
	    return self::$instance;
	}
    
    public function addModuleConfig($path)
    {
        $this->module_config[] = $path;
    }
	
    public function getModuleConfig()
    {
        return $this->module_config;
    }
    
    public function addModule($module)
    {
        $this->modules[$module->getName()] = $module;
    }
	
    public function getModule($name)
    {
        return $this->modules[$name];
    }
    
    public function setCurrentModule($module)
    {
        $this->current_module = $module;
    }
	
    public function getCurrentModule()
    {
        return $this->current_module;
    }
    
    /**
     * Vráti obsah na základe zadanej url, prejde vsetky pridane routingy a ak matchne url, tak vrati obsah
     */
	public function processUri()
        {    
            foreach($this->getModuleConfig() as $config_file)
            {
                    require($config_file);
            }

            $router = ServiceLayer::getService('router');
            $acl = ServiceLayer::getService('acl');
            $security = ServiceLayer::getService('security');
            $roleList = $acl->getRoleList();
            $security->setRoleList($roleList);
            

            
            foreach ($router->getRoutes() as $route_name => $route)
            {

                    $route_match = Router::match($route['pattern']);
                    if(true == $route_match)
                    {

                            if(!$security->isAllowed($security->getIdentity(),$route_name))
                            {
                                    throw new \AclException('Access denied','001');
                            }
                             if(DEBUG or 'dev' == RUN_ENV or 'test' == RUN_ENV)
                            {
                                \Tracy\Debugger::barDump($route_name, 'Route');
                                \Tracy\Debugger::barDump($route);
                            }
                            
                            /*
                            if(!array_key_exists($route_name, $roleList['guest']) )
                            {
                                if('gdpr_settings' != $route_name && !$security->hasAgreements($security->getIdentity()->getUser()))
                                {
                                    $requestUri = $_SERVER['REQUEST_URI'];
                                    $request = \Core\ServiceLayer::getService('request');
                                    $request->redirect($router->link('gdpr_settings',array('rurl' => $requestUri)));
                                }
                                
                            }
                            */
                            
                            Router::$current_route_name = $route_name;
                            $controller_info = explode(':',$route['options']['controller']);
                            $module_name = trim($controller_info[0]);
                            $controllerName = trim($controller_info[1]);
                            $controller = "{$controllerName}Controller";
                            $actionName = trim($controller_info[2]);
                            $action = "{$actionName}Action";
                            $module = new $module_name();
                            $this->setCurrentModule($module);
                            $namespace_parts = explode("\\", $module_name);
                                    array_pop($namespace_parts);
                                    $namespace = implode('\\',$namespace_parts);;
                            require_once($module->getBaseDir().'/controller/'.$controller.'.php');
                            $controller_class = $namespace.'\\Controller\\'.$controller;
                            $controller_object = new $controller_class();
                            $result = call_user_func(array($controller_object, $action));
                            $controller_response = is_callable($result) ? $result($controllerName, $actionName) : $result;
                            if(is_a($controller_response, 'Core\ControllerResponse'))
                            {
                                if($controller_response->getStatus() == 'success')
                                {
                                    $content = $controller_response->getContent();
                                }
                                $mimeType = $controller_response->getType();
                                $encoding = $controller_response->getEncoding();
                                $this->sendContentTypeHeader($mimeType, $encoding);
                            }
                            
                            $this->debug_info['controller'] = $controller_class;
                            $this->debug_info['action'] = $action;
                            
                    }
            }

            return $content;
        }
            
        protected function sendContentTypeHeader($mimeType, $charset)
        {
            $contentType = $this->getContentType($mimeType, $charset);
            header("Content-Type: {$contentType}");
        }

        protected function getContentType($mimeType, $charset)
        {
            if (empty($charset)) return $mimeType;
            if (is_null($charset))
            {
                return $this->isTextContent($mimeType) ?
                    "{$mimeType}; charset=utf-8" : $mimeType;
            }
            return "{$mimeType}; charset={$charset}";
        }

        protected function isTextContent($mimeType)
        {
            switch ($mimeType)
            {
                case "text/plain":
                case "text/html":
                case "text/javascript":
                case "application/javascript":
                case "application/json":
                case "application/xml":
                case "text/xml": return true;
                default: return false;
            }
        }

        public function setModuleInfo($info)
        {
            $this->module_info = $info;
        }

        public function getModuleInfo($index = null)
        {
            if(null != $index)
            {
                return $this->module_info[$index];
            }
            return $this->module_info;
        }
            
        public function renderController($controller_path)
        {
            $controller_info = explode(':',$controller_path);
            $module_name = $controller_info[0];
            $controller = $controller_info[1].'Controller';
            $action = $controller_info[2].'Action';
            $module = new $module_name();

            $namespace_parts = explode("\\", $module_name);
            array_pop($namespace_parts);
            $namespace = implode('\\',$namespace_parts);;
            require_once($module->getBaseDir().'/controller/'.$controller.'.php');
            $controller_class = $namespace.'\\Controller\\'.$controller;
            $controller_object = new $controller_class();
            $controller_response = $content = call_user_func(array($controller_object, $action));
            if(is_a($controller_response, 'Core\ControllerResponse'))
            {
                if($controller_response->getStatus() == 'success')
                {
                    $content = $controller_response->getContent();
                }
            }
            return $content;
        }
        
        public function getDebugInfo()
        {
            return $this->debug_info;
        }
        
        public function getNamespaces()
        {
            return $this->namespaces;
        }

        public function setNamespaces($namespaces)
        {
            $this->namespaces = $namespaces;
        }
}

?>