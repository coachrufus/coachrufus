<?php namespace Core\Activity;

class LoggerRepository
{
    private $store;
    
    function __construct(ILoggerStore $store)
    {
        $this->store = $store;
    }
    
    function addLog(Log $log)
    {
        $this->store->add($log->toArray());
    }
    
    private function createLog($iterator): Log
    {
        return Log::fromArray(iterator_to_array($iterator));
    }
    
    function getLogBy(callable $finder, $subject = 'Log')
    {
        $result = $this->store->findOneBy($finder);
        if ($result === false) throw new Exception("{$subject} not exists");
        return $this->createLog($result);
    }
    
    function getLogById(int $id)
    {
        return $this->getLogBy(function($logs) use($id)
        {
            return $logs->where('id', $id);
        });
    }
}