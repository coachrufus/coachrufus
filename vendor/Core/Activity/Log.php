<?php namespace Core\Activity;

use Core\Types\Enum;

interface ILoggerStore
{
    function add(array $data);
}

class ActivityTypes extends Enum
{
    const UNDEFINED = 0;
    const LOGIN = 1;
    const LOGOUT = 2;
    const REGISTRATION = 3;
    const ACTIVATION = 4;
    const RATE = 5;
}

class Log
{
    private $data;
    private $fields;

    private function getFields()
    {
        return [
            'co_user_id' => null,
            'type' => function() { return ActivityTypes::UNDEFINED; },
            'note' => function() { return null; },
            'date' => function() { return new \DateTime(); },
            'ip' => function() { return $_SERVER['REMOTE_ADDR']; },
            'host'=> function() { return getHostByAddr($_SERVER['REMOTE_ADDR']); }
        ];
    }

    private function parseFromArray($fields, array $source)
    {
        foreach ($fields as $field => $defaultValue)
        {
            if (isset($source[$field]) && !is_null($source[$field]))
            {
                $value = $source[$field];
                if ($field === "date" && is_string($value) && !empty($value))
                {
                    $date = \DateTime::createFromFormat('Y-m-d H:i:s', $value);
                    if ($date === false) throw new Exception("Invalid DateTime string: \"{$value}\"");
                    else yield $field => $date;
                }
                else
                {
                    yield $field => $value;
                }
            }
            else
            {
                if (is_null($defaultValue)) throw new Exception("Field \"{$field}\" is required");
                else yield $field => $defaultValue();
            }
        }
    }

    private function __construct(array $source)
    {
        $this->fields = $this->getFields();
        $this->data = iterator_to_array($this->parseFromArray($this->fields, $source));
    }

    static function fromArray(array $array) { return new self($array); }
    static function create($userId, $type = null, $note = null, $date = null, $ip = null, $host = null)
    {
        return new self([
            'co_user_id' => $userId,
            'type' => $type,
            'note' => $note,
            'date' => $date,
            'ip' => $ip,
            'host' => $host
        ]); 
    }

    function getUserId() { return $this->data['co_user_id']; }
    function getType() { return $this->data['type']; }
    function getDate() { return $this->data['date']; }
    function getNote() { return $this->data['note']; }
    function getIP() { return $this->data['ip']; }
    function getHost() { return $this->data['host']; }

    function toArray() { return $this->data; }
}