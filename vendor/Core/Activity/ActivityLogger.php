<?php namespace Core\Activity;

class ActivityLogger
{
    private $repository;
    
    function __construct(LoggerRepository $repository)
    {
        $this->repository = $repository;
    }
    
    function log(int $userId, $type = null, $note = null, $date = null, $ip = null, $host = null)
    {
        $this->repository->addLog(Log::create($userId, $type, $note, $date, $ip, $host));
    }
    
    function logMessage(int $userId, $message)
    {
        $this->repository->addLog(Log::create($userId, ActivityTypes::UNDEFINED, $message));
    }
}