<?php namespace Core\Activity;

class Activity
{
    private $repository;
    
    function __construct(LoggerRepository $repository)
    {
        $this->repository = $repository;
    }

    function lastLogForUser(int $userId, int $activityType = null)
    {
        return $this->repository->getLogBy(function($logs) use($userId, $activityType)
        {
            $logs = $logs->where('co_user_id', $userId);
            if (!is_null($activityType)) $logs = $logs->where('type', $activityType);
            return $logs->order('date DESC');
        }, 'User');
    }
    
    function lastForUser(int $userId, int $activityType = null)
    {
        return $this->lastLogForUser($userId, $activityType)->getDate();
    }
}