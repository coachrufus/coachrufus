<?php namespace Core\Activity;

class DbLoggerStore implements ILoggerStore
{
    use \Core\Db;
    
    private $db;
    
    function __construct()
    {
        $this->db = $this->getDb();
    }
    
    function add(array $data)
    {
        $this->db->log()->insert($data);
    }
    
    function findAllBy(callable $finder)
    {
        return $finder($this->db->log());
    }
    
    function findOneBy(callable $finder)
    {
        return $this->findAllBy($finder)->fetch();
    }
}