<?php


namespace Core;


/**
 * @author hubacek
 * @version 1.0
 * @created 25-10-2015 20:53:48
 */
class ControllerResponse
{

	private $content;
	private $status;
	private $type;
    private $encoding = null;

	public function getContent()
	{
		return $this->content;
	}

	public function getStatus()
	{
		return $this->status;
	}

	public function getType()
	{
		return $this->type;
	}
    
	public function getEncoding()
	{
		return $this->encoding;
	}

	/**
	 * 
	 * @param newVal
	 */
	public function setContent($newVal)
	{
		$this->content = $newVal;
	}

	/**
	 * 
	 * @param newVal
	 */
	public function setStatus($newVal)
	{
		$this->status = $newVal;
	}

	/**
	 * 
	 * @param newVal
	 */
	public function setType($newVal)
	{
		$this->type = $newVal;
	}

	public function setEncoding($newVal)
	{
		$this->encoding = $newVal;
	}

}