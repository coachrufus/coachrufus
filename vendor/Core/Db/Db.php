<?php namespace Core;

use Core\ServiceLayer;

trait Db
{
    function getDb() { return ServiceLayer::getService("Db"); }
}