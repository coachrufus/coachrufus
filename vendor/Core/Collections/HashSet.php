<?php namespace Core\Collections;

class HashSet
{
    private function __construct($values)
    {
        $this->set = $this->toSet($values);
    }
    
    static function of(array ...$args)
    {
        return new self(call_user_func_array('array_merge', $args));
    }
    
    protected function toSet($values)
    {
        return iterator_to_array((function($values)
        {
            foreach ($values as $value) yield $value => true;        
        })($values));
    }
    
    function contains($value)
    {
        return isset($this->set[$value]);
    }
        
    function toArray()
    {
        return array_keys($this->set);
    }
}
