<?php namespace Core\Collections;

class Queue implements \ArrayAccess, \Iterator
{
    private $items;
    function __construct($items = [])
    {
        $this->items = $items;
    }
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) $this->items[] = $value;
        else $this->items[$offset] = $value;
    }
    protected function getItems() { return $this->items; }
    protected function setItems($val) { $this->items = $val; }
    function offsetExists($offset) { return isset($this->items[$offset]); }
    function offsetUnset($offset) { unset($this->items[$offset]); }
    function offsetGet($offset) { return $this->items[$offset]; }
    function rewind() { return reset($this->items); }
    function current() { return current($this->items); }
    function key() { return key($this->items); }
    function next() { return next($this->items); }
    function valid() { return key($this->items) !== null; }
}