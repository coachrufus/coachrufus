<?php namespace Core\Collections;

class Iter
{
    private $iter;
    
    function __construct($iter)
    {
        $this->iter = $iter;
    }
    
    static function of($iter)
    {
        return new self($iter);
    }
    
    function first()
    {
        foreach ($this->iter as $item)
        {
            $result = $item;
            break;
        }
        return isset($result) ? $result : null;
    }
    
    function last()
    {
        foreach ($this->iter as $item)
        {
            $result = $item;
        }
        return isset($result) ? $result : null;
    }
}