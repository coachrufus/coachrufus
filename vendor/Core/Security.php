<?php namespace Core;
/**
 * 
 * @author Marek Hubáček
 *
 */
use Core\Activity\ActivityTypes;
    
class Security
{
    protected $role_list = [];
    protected $identity_provider;
    protected $identity;
    protected $state;
    protected $observers = [];
    private $activityLogger;

    public function __construct($identity_provider_class)
    {
        $this->identity_provider = new $identity_provider_class();
        $this->attach('user_logger',\Core\ServiceLayer::getService('user_logger'));
        $this->attach('partner_logger',\Core\ServiceLayer::getService('partner_logger'));
        $this->activityLogger = ServiceLayer::getService('ActivityLogger');
    }
    
    public function attach($index,$observer)
    {
        $this->observers[$index] = $observer;
    }
 
    public function detach($observer)
    {
            $index = array_search($observer, $this->observers);

        if (false !== $index) {
            unset($this->observers[$index]);
        }
    }
 
     public function notify($index = null)
    {
        if(null == $index)
        {
            foreach ($this->observers as $observer) {
                $observer->update($this);
            }
        }
        else
        {
            $observer = $this->observers[$index];
            $observer->update($this);
        }
        
    }
    
    public function getRoleList()
    {
        return $this->role_list;
    }
    
    public function setRoleList($role_list)
    {
        $this->role_list = $role_list;
    }
    
    /**
     * Vrati identitu uzivatela
     */
    public function getIdentity()
    {
        if(isset($_SESSION['user_identity']) && null != $_SESSION['user_identity'])
        {
            return  unserialize($_SESSION['user_identity']);
        }
        
        return $this->getIdentityProvider()->getDefaultIdentity();
    }
        
        public function getIdentityUser()
        {
                $identity = $this->getIdentityProvider()->getDefaultIdentity();
            if(isset($_SESSION['user_identity']) && null != $_SESSION['user_identity'])
        {
            $identity =  unserialize($_SESSION['user_identity']);
                        
        }
        
        return $identity->getUser();
        }
    
    /*
    public function setIdentity($val)
    {
        $this->identity = $val;
    }
    */
    public function getIdentityProvider()
    {
        return $this->identity_provider;
    }
    
    public function setIdentityProvider($identity_provider)
    {
        $this->identity_provider = $identity_provider;
    }
    
    /**
     * Poziadavky podla 3.12.3
     * Minimálny počet 8 znakov
     * Použitie minimálne 1 veľkého a 1 malého písmena.
     * Použiť aspoň 1 číslo
     * 
     */
    public function generatePassword($length = 8)
    {
        $sets = array();
        $sets[] = 'abcdefghjkmnpqrstuvwxyz';
        $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
        $sets[] = '23456789';

        $all = '';
        $password = '';
        foreach($sets as $set)
        {
            $password .= $set[array_rand(str_split($set))];
            $all .= $set;
        }
     
        $all = str_split($all);
        for($i = 0; $i < $length - count($sets); $i++)
            $password .= $all[array_rand($all)];
     
        $password = str_shuffle($password);
        return $password;
    }
    
    /**
     * Zakoduje heslo
     * @param string $password
     * @param string $salt
     * @return string
     */
    public function encodePassword($password, $salt)
    {
        $identity_provider = $this->getIdentityProvider();
        $encoded = $identity_provider->encodePassword($password,$salt);
        return $encoded;
    }
    
    public function remeberUser($user)
    {
        $hash = sha1($user->getSalt().$user->getEmail()) ;
        $this->getIdentityProvider()->saveUserPersistHash($user,$hash);
        setcookie('persist_hash', $hash.'-'.$user->getSalt().'-'.sha1(time()),time()+(60*60*24*30*12),'/');
    }
        
    public function authenticateUserEntity($user)
    {
        $identity_provider = $this->getIdentityProvider();
        $identity_provider->updateLastLogin($user);
        $identity = $this->getIdentity();
        $identity->setStatus('logged');
        $identity->setUser($user);
        $identity->addRole($user->getRole());
        
        //add team roles
        $teamRoles = $identity_provider->getTeamRoles($user);
        $identity->setTeamRoles($teamRoles);
        $_SESSION['user_identity'] = serialize($identity);
        
        if(null == $_SESSION['active_team']['id'])
        {
            $teams = $identity->getUserTeams();
            $currentTeam = current($teams);
            if(null != $currentTeam)
            {
                $_SESSION['active_team']['id'] = current($teams)->getId();
            }
        }

        $this->activityLogger->log($user->getId(), ActivityTypes::LOGIN);

        return [
            'RESULT' => 1,
            'MESSAGE' => 'NOTE_LOGIN_SUCCESS',
            'ID' => $user->getId(),
            'TYPE' =>  $user->getRole()
        ];
    }
               
        
    
    public function authenticateUser($username,$password)
    {
        if(null == $username or null == $password)
        {
            return $result =  array(
                    'RESULT' => 0,
                    'MESSAGE' => 'NOTE_LOGIN_FAILED',
                    'ID' => null,
                    'TYPE' => null
            );
        }
        
        
        $identity_provider = $this->getIdentityProvider();
        $user = $identity_provider->findUserByUsername($username);
                
                
                
              
        $result = array();
        if(null == $user or $this->encodePassword($password,$user->getSalt()) != $user->getPassword())
        {
            return $result =  array(
                'RESULT' => 0,
                'MESSAGE' => 'NOTE_LOGIN_FAILED',
                'ID' => null,
                'TYPE' => null
            );
        }
        else
        {
            return $this->authenticateUserEntity($user);         
        }
    }
        
        public function authenticateGoogleUser($user)
        {
            $identity = $this->getIdentity();
            $identity->setStatus('logged');
            $identity->setUser($user);
            $identity->addRole($user->getRole());
            $_SESSION['user_identity'] = serialize($identity);

            $this->activityLogger->log($user->getId(), ActivityTypes::LOGIN, "GOOGLE");
            
            $identity_provider = $this->getIdentityProvider();
            $identity_provider->updateLastLogin($user);
            $this->remeberUser($user);
            
            return [
                'RESULT' => 1,
                'MESSAGE' => 'NOTE_LOGIN_SUCCESS',
                'ID' => $user->getId(),
                'TYPE' =>  $user->getRole()
            ];
        }
        
        public function authenticateFacebookUser($user)
        {
            $identity = $this->getIdentity();
            $identity->setStatus('logged');
            $identity->setUser($user);
            $identity->addRole($user->getRole());
            $_SESSION['user_identity'] = serialize($identity);

            $this->activityLogger->log($user->getId(), ActivityTypes::LOGIN, "FB");
            
              
            $identity_provider = $this->getIdentityProvider();
            $identity_provider->updateLastLogin($user);
            
            $this->remeberUser($user);
            
            return [
                'RESULT' => 1,
                'MESSAGE' => 'NOTE_LOGIN_SUCCESS',
                'ID' => $user->getId(),
                'TYPE' =>  $user->getRole()
            ];
        }
        
        public function authenticatePersistUser($hash)
        {
            $hashParts = explode('-',$hash);
            $baseHash =  $hashParts[0];
            $salt =  $hashParts[1];
            $encodedHash = $this->encodePassword($baseHash, $salt);
            $identity_provider = $this->getIdentityProvider();
            $user = $identity_provider->findUserByPersistHash($encodedHash);
            
            if(null != $user)
            {
                return $this->authenticateUserEntity($user);        
            }
        }

    /**
    * Odhlasi uzivatela a zaznamena do logov
    */
    public function unauthenticateUser()
    {
        $user = $this->getIdentity()->getUser();
        if(!is_null($user))
        {
            unset($_SESSION['user_identity']);
            unset($_SESSION['fb_access_token']);
            unset($_SESSION['FBRLH_state']);
            unset($_SESSION['active_team']);
            unset($_SESSION['actualPackage']);
            unset($_SESSION['user_team_packages']);
            setcookie("persist_hash", "", time()-3600);
            $this->activityLogger->log($user->getId(), ActivityTypes::LOGOUT);
        }
    }
    
    /**
     * Check if identity has access
     * @param Identity $user
     * @param string $permission
     * @return boolean
     */
    public function isAllowed($user,$permission)
    {
        $user_roles = $user->getRoles();
    
        foreach($user_roles as $user_role)
        {
            if(array_key_exists($user_role, $this->role_list))
            {
                if(array_key_exists($permission, $this->role_list[$user_role]) && $this->role_list[$user_role][$permission] == 'allow')
                {
                    return true;
                }
            }
        }
    
        return false;
    }
    
    public function userHasRole($user,$role)
    {
        $user_roles = $user->getRoles();
        
        if(in_array($role, $user_roles))
        {
            return true;
        }

        return false;
    }
    
    /**
     * Check if identity has no access
     * @param Identity $user
     * @param string $permission
     * @return boolean
     */
    public function isDeny($user,$permission)
    {
        $user_roles = $user->getRoles();
    
        foreach($user_roles as $user_role)
        {
            if(array_key_exists($user_role, $this->role_list))
            {
                if(array_key_exists($permission, $this->role_list[$user_role]) && $this->role_list[$user_role][$permission] == 'allow')
                {
                    return false;
                }
            }
        }
    
        return true;
    }

    public function getState()
    {
        return $this->state;
    }

    public function setState($state)
    {
        $this->state = $state;
    }
    
    /**
     * Reload user data  
     */
    public function reloadUser($user)
    {
        $identity_provider = $this->getIdentityProvider();
        $user = $identity_provider->findUserById($user->getId());
        $identity = $this->getIdentity();
        $identity->setUser($user);
        $_SESSION['user_identity'] = serialize($identity);
    }
    
    public function userCanChangePassword($user)
    {
        
        if($user->getUserType() == 'social_user')
        {
            return false;
        }
        return true;
    }
    
    public function hasAgreements($user)
    {
         //add info about agreements
        $identityProvider = $this->getIdentityProvider();
        $privacySettings = $identityProvider->getPrivacySettings($user);
        
        $hasAgreement = false;
        foreach($privacySettings as $setting)
        {
            if($setting->getSection() == 'vop_agreement' && $setting->getValue() == '1')
            {
                 $hasAgreement = true;
            }
        }
        return  $hasAgreement;
    }
    
    public function hasMarketingAgreements($user)
    {
         //add info about agreements
        $identityProvider = $this->getIdentityProvider();
        $privacySettings = $identityProvider->getPrivacySettings($user);
        
        $hasAgreement = 'unknown';
        foreach($privacySettings as $setting)
        {
            if($setting->getSection() == 'gdpr_marketing_agreement' && $setting->getValue() == '1')
            {
                 $hasAgreement = true;
            }
            if($setting->getSection() == 'gdpr_marketing_agreement' && $setting->getValue() == '2')
            {
                 $hasAgreement = false;
            }
        }
        return  $hasAgreement;
    }
    
    public function hasTeamAdminAgreements($user,$team)
    {
         //add info about agreements
        $identityProvider = $this->getIdentityProvider();
        $privacySettings = $identityProvider->getPrivacySettings($user);
        
        $hasAgreement = false;
        foreach($privacySettings as $setting)
        {
            $data  = json_decode($setting->getData(),true);
            
            if($setting->getSection() == 'team_admin_agreement' && $data['team_id'] == $team->getId())
            {
                 $hasAgreement = true;
            }
        }
        return  $hasAgreement;
    }
    
    public function hasTournamentAdminAgreements($user,$tournament)
    {
         //add info about agreements
        $identityProvider = $this->getIdentityProvider();
        $privacySettings = $identityProvider->getPrivacySettings($user);
        
        $hasAgreement = false;
        foreach($privacySettings as $setting)
        {
            $data  = json_decode($setting->getData(),true);
            if($setting->getSection() == 'tournament_admin_marketing_agreement' && $data['tournament_id'] == $tournament->getId())
            {
                 $hasAgreement = true;
            }
        }
        return  $hasAgreement;
    }
    public function hasTournamentTeamAdminAgreements($user,$tournament)
    {
         //add info about agreements
        $identityProvider = $this->getIdentityProvider();
        $privacySettings = $identityProvider->getPrivacySettings($user);
        
        $hasAgreement = false;
        foreach($privacySettings as $setting)
        {
            $data  = json_decode($setting->getData(),true);
            if($setting->getSection() == 'tournament_team_admin_agreement' && $data['tournament_id'] == $tournament->getId())
            {
                 $hasAgreement = true;
            }
        }
        return  $hasAgreement;
    }
}