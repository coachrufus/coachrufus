<?php namespace Core\Types;

class StringUtils
{
    static function startsWith($haystack, $needle)
    {
        return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== false;
    }

    static function endsWith($haystack, $needle)
    {
        return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== false);
    }
    
    static function toLines($string)
    {
        return explode("\n", str_replace(["\r\n"], "\n", $string));
    }
    
    public function formatTime($timestamp,$format)
{
	$translator = \Core\ServiceLayer::getService('translator');

        // Get time difference and setup arrays
	$difference = time() - $timestamp;
	$periods = array("humandate.seconds", "humandate.minutes", "humandate.hour", "humandate.day", "humandate.week", "humandate.month", "humandate.year");
        $periodsPlural = array("humandate.seconds.plural", "humandate.minutes.plural", "humandate.hour.plural", "humandate.day.plural", "humandate.week.plural", "humandate.month.plural", "humandate.year.humandate");
	$lengths = array("60","60","24","7","4.35","12");
 
	// Past or present
	if ($difference >= 0) 
	{
		$ending = "humandate.ago";
	}
	else
	{
		$difference = -$difference;
		$ending = "to go";
	}
 
	// Figure out difference by looping while less than array length
	// and difference is larger than lengths.
	$arr_len = count($lengths);
	for($j = 0; $j < $arr_len && $difference >= $lengths[$j]; $j++)
	{
		$difference /= $lengths[$j];
	}
 
	// Round up		
	$difference = round($difference);
        $periodCode = $periods[$j];
        
     
	// Make plural if needed
        
	if($difference != 1) 
	{
                $periodCode = $periodsPlural[$j];
	}
        
	// Default format
	$text = $difference.' '.$translator->translate($periodCode).' '.$translator->translate($ending);
 
	// over 24 hours
	if($j > 2)
	{
		// future date over a day formate with year
                /*
		if($ending == "to go")
		{
			if($j == 3 && $difference == 1)
			{
				$text = "Tomorrow at ". date("g:i a", $timestamp);
			}
			else
			{
				$text = date("F j, Y \a\\t g:i a", $timestamp);
			}
			return $text;
		}
 
		if($j == 3 && $difference == 1) // Yesterday
		{
			$text = "Yesterday at ". date("g:i a", $timestamp);
		}
		else if($j == 3) // Less than a week display -- Monday at 5:28pm
		{
			$text = date("l \a\\t g:i a", $timestamp);
		}
		else if($j < 6 && !($j == 5 && $difference == 12)) // Less than a year display -- June 25 at 5:23am
		{
			$text = date("F j \a\\t g:i a", $timestamp);
		}
		else // if over a year or the same month one year ago -- June 30, 2010 at 5:34pm
		{
			$text = date("F j, Y \a\\t g:i a", $timestamp);
		}
                 * 
                 */
                $text = date($format, $timestamp);
	}
 
	return $text;
}
}