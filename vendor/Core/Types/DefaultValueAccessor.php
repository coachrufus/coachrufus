<?php namespace Core\Types;

class DefaultValueAccessor implements \ArrayAccess
{
    private $values;
    private $defaultGetter;
    protected function getDefaultValue($offset, $defaultGetter)
    {
        return is_null($defaultGetter) ? '' : $defaultGetter($offset);
    }
    function __construct($values = null, $defaultGetter = null)
    {
        $this->values = $values;
        $this->defaultGetter = $defaultGetter;
    }
    function offsetExists($offset) { return true; }
    function offsetGet($offset)
    {
        return !is_null($this->values) && isset($this->values[$offset]) ? $this->values[$offset] :
            $this->getDefaultValue($offset, $this->defaultGetter);
    }
    function offsetSet($offset, $value) { $this->values[$offset] = $value; }
    function offsetUnset($offset) { unset($this->values[$offset]); }
    
    public  function getValues() {
return $this->values;
}

public  function setValues($values) {
$this->values = $values;
}


}