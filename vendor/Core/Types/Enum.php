<?php namespace Core\Types;

class Enum
{
    private static $__CONSTS__;
    private static $__REVERSE__CONSTS__;

    static function getConsts()
    {
        $reflectionClass = new \ReflectionClass(get_called_class());
        self::$__CONSTS__ = $reflectionClass->getConstants();
        if (isset(self::$__CONSTS__['__DEFAULT__']))
        {
            unset(self::$__CONSTS__['__DEFAULT__']);
        }
        return self::$__CONSTS__;
    }

    static function toArray()
    {
        if (!isset(self::$__CONSTS__))
        {
            self::$__CONSTS__ = self::getConsts();
        }
        return self::$__CONSTS__;
    }

    static function toReverseArray()
    {
        if (!isset(self::$__REVERSE__CONSTS__))
        {
            self::$__REVERSE__CONSTS__ = array_flip(self::toArray());
        }
        return self::$__REVERSE__CONSTS__;
    }

    static function hasKey($key) { return isset(self::toArray()[$key]); }
    static function value($key) { return self::toArray()[$key]; }
    static function key($value) { return self::toReverseArray()[$value]; }

    private $value;

    function __construct($value = null)
    {
        if (is_null($value))
        {
            $this->value = self::__DEFAULT__;
        }
        else if (isset(self::toReverseArray()[$value]))
        {
            $this->value = $value;
        }
        else throw new \Exception("Invalid value: \"{$value}\"");
    }

    function getValue() { return $this->value; }
    function hasValue($value) { return $this->value === $value; }
    function getKey() { return (string)$this; }
    function __toString() { return self::toReverseArray()[$this->value]; }
}