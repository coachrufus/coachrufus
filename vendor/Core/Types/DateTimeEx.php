<?php namespace Core\Types;

class DateTimeEx
{
    private $dateTime;
    
    function __construct($dateTime = null)
    {
        $this->dateTime = is_null($dateTime) ? new \DateTime() : $dateTime;
    }
    
    static function of(\DateTime $dateTime) { return new self($dateTime); }
    static function now() { return new self(); }
    static function today() { return (new self())->getDate(); }
    
    static function parse($dateTimeString, $format = 'Y-m-d H:i:s')
    {
        return new self(\DateTime::createFromFormat($format, $dateTimeString));
    }

    function toDateTime() { return $this->dateTime; }
    
    function getYear(): int  { return $this->dateTime->format('Y'); }
    function getMonth(): int  { return $this->dateTime->format('m'); }
    function getDay(): int { return $this->dateTime->format('d'); }
    function getDayOfWeek(): string  { return $this->dateTime->format('l'); }
    
    function getHour(): int { return $this->dateTime->format('H'); }
    function getMinute(): int  { return $this->dateTime->format('i'); }
    function getSecond(): int  { return $this->dateTime->format('s'); }
    
    function changeTime($hour, $minute, $second): DateTimeEx
    {
        $dateTime = clone $this->dateTime;
        $dateTime->setTime($hour, $minute, $second);
        return new DateTimeEx($dateTime);
    }
    
    function changeDate($year, $month, $day): DateTimeEx
    {
        $dateTime = clone $this->dateTime;
        $dateTime->setDate($year, $month, $day);
        return new DateTimeEx($dateTime);
    }
    
    function removeTime(): DateTimeEx { return $this->changeTime(0, 0, 0); }
    
    function setYear(int $year): DateTimeEx  { return $this->changeDate($year, $this->getMonth(), $this->getDay()); }
    function addYear(int $year): DateTimeEx  { return $this->changeDate($this->getYear() + $year, $this->getMonth(), $this->getDay()); }
    function setMonth(int $month): DateTimeEx  { return $this->changeDate($this->getYear(), $month, $this->getDay()); }
    function addMonth(int $month): DateTimeEx  { return $this->changeDate($this->getYear(), $this->getMonth() + $month, $this->getDay()); }
    function setDay(int $day): DateTimeEx { return $this->changeDate($this->getYear(), $this->getMonth(), $day); }
    function addDay(int $day): DateTimeEx { return $this->changeDate($this->getYear(), $this->getMonth(), $this->getDay() + $day); }
    
    function setHour(int $hour): DateTimeEx { return $this->changeTime($hour, $this->getMinute(), $this->getSecond()); }
    function addHour(int $hour): DateTimeEx { return $this->changeTime($this->getHour() + $hour, $this->getMinute(), $this->getSecond()); }
    function setMinute(int $minute): DateTimeEx  { return $this->changeTime($this->getHour(), $minute, $this->getSecond()); }
    function addMinute(int $minute): DateTimeEx  { return $this->changeTime($this->getHour(), $this->getMinute() + $minute, $this->getSecond()); }
    function setSecond(int $second): DateTimeEx  { return $this->changeTime($this->getHour(), $this->getMinute(), $second); }
    function addSecond(int $second): DateTimeEx  { return $this->changeTime($this->getHour(), $this->getMinute(), $this->getSecond() + $second); }
    
    function getDate(): DateTimeEx { return $this->changeTime(0, 0, 0); }

    function toTimeStamp(): int { return $this->dateTime->getTimeStamp(); }

    function dateFrom(DateTimeEx $source): DateTimeEx
    {
        return $this->changeDate($source->getYear(), $source->getMonth(), $source->getDay());
    }
    
    function timeFrom(DateTimeEx $source): DateTimeEx
    {
        return $this->changeTime($source->getHour(), $source->getMinute(), $source->getSecond());
    }
    
    function toString(string $format): string { return $this->dateTime->format($format); }
    
    function __toString(): string { return $this->dateTime->format('Y-m-d H:i:s'); }
}