<?php namespace Core;

class DateNames
{
    private $locale;
    private $weekDays;
    private $months;
    private $months2;
    
    function __construct()
    {
        $this->locale = "en";
        $this->weekDays = [
            "en" => [
                "Monday",
                "Tuesday",
                "Wednesday",
                "Thursday",
                "Friday",
                "Saturday",
                "Sunday"
            ],
            "sk" => [
                "Pondelok",
                "Utorok",
                "Streda",
                "Štvrtok",
                "Piatok",
                "Sobota",
                "Nedeľa"
            ]
        ];
        $this->months = [
            "en" => [
                "January",
                "February",
                "March",
                "April",
                "May",
                "June",
                "July",
                "August",
                "September",
                "October",
                "November",
                "December"
            ],
            "sk" => [
                "Január",
                "Február",
                "Marec",
                "Apríl",
                "Máj",
                "Jún",
                "Júl",
                "August",
                "September",
                "Október",
                "November",
                "December"
            ]
        ];
    }
    
    function setLang(string $locale) { $this->locale = strtolower($locale); }
    function getLang() { return $this->locale; }
    function toWeekDayName(int $weekDay) { return $this->weekDays[$this->locale][$weekDay - 1]; }
    function toMonthName(int $month) { return $this->months[$this->locale][$month - 1]; }
}