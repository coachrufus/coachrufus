<?php
namespace Core\Images;

class ImageMarginFix
{	
    private function getImageXCoords($image, $width, $height)
    {
        $y = round($height / 2);
        $from = null; $to = $width;
        for ($x = 0; $x < $width; $x++)
        {
            $rgba = $image->getRGBAt($x, $y);
            if ($from === null && $rgba['alpha'] === 0) $from = $x;
            if ($from !== null && $rgba['alpha'] !== 0)
            {
                $to = $x;
                break;
            }
        }
        return [$from, $to];
    }
    
    function apply($image)
    {
        $width = $image->getWidth();
        $height = $image->getHeight();
        list($from, $to) = $this->getImageXCoords($image, $width, $height);	
        if ($from === 0 && $to === $width) return $image;
        else return $image->crop($from, 0, $to - $from, $height);
    }
}