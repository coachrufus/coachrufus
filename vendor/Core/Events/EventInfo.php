<?php namespace Core\Events;

use Core\ServiceLayer;
use Core\Types\DateTimeEx;
use Core\Collections\HashSet as Set;
use \__;

class EventInfo
{
    private $teamManager;
    private $teamEventManager;
    
    private $event;
    private $team;
    private $attendance;
    private $lineup;
    private $teamPlayers;
    private $subjects;
    private $lineupSubjects;
    private $admins;
    private $subjectsWithoutAdmins;
    
    function __construct($event)
    {
        $this->teamManager = ServiceLayer::getService('TeamManager');
        $this->teamEventManager = ServiceLayer::getService('TeamEventManager');
        $this->event = $event;
    }
    
    static function create(int $eventId, DateTimeEx $date)
    {
        $event = ServiceLayer::getService('TeamEventManager')->findEventById($eventId);
        if(null == $event)
        {
            $event =   new \Webteamer\Event\Model\Event();
        }
        $startTime = new DateTimeEx($event->getStart());
        $event->setCurrentDate($date->timeFrom($startTime)->toDateTime());
        return new self($event);
    }
    
    function getEvent() { return $this->event; }
    
    private function lazyLoad($prop, $loader)
    {
        if (!isset($this->$prop))
        {
            $this->$prop = $loader();
        }
        return $this->$prop;
    }
    
    function getTeam()
    {
        return $this->lazyLoad('team', function()
        {
            return $this->event->getTeam();
        });
    }
    
    function getAttendance()
    {
        return $this->lazyLoad('attendance', function()
        {
            return iterator_to_array((function()
            {
                $attendance = $this->teamEventManager->getEventAttendance($this->event);               
                foreach ($attendance as $item)
                {
                    yield $item->getTeamPlayerId() => $item;
                }
            })());
        });
    }
    
    function getLineup()
    {
        return $this->lazyLoad('lineup', function()
        {
            return $this->teamEventManager->getEventLineup($this->event);
        });
    }
    
    function getTeamPlayers()
    {
        return $this->lazyLoad('teamPlayers', function()
        {
            return iterator_to_array((function()
            {
                $teamPlayers = $this->teamManager->getTeamPlayers($this->getTeam()->getId());
                foreach ($teamPlayers as $teamPlayer)
                {
                    yield $teamPlayer->getId() => $teamPlayer;
                }
            })());
        });
    }
    
    private function toSubjects($attendance, $onlyRegisteredMembers = true)
    {
        $teamPlayers = $this->getTeamPlayers();
        foreach ($attendance as $item)
        {
            $player = $item->getPlayer();
            $playerId = $player->getId();
            if (!empty($playerId) || !$onlyRegisteredMembers)
            {
                $teamPlayerId = $item->getTeamPlayerId();
                yield $teamPlayerId => new EventSubject($item, $teamPlayers[$teamPlayerId]);
            }
        }
    }
    
    function getSubjects($onlyRegisteredMembers = true)
    {
        return $this->lazyLoad('subjects', function() use($onlyRegisteredMembers)
        {
            return iterator_to_array($this->toSubjects($this->getAttendance(), $onlyRegisteredMembers));
        });
    }
    
    function getAdminSubjects()
    {
        return $this->lazyLoad('adminSubjects', function()
        {
            return iterator_to_array((function()
            {
                $attendance = $this->getAttendance();
                foreach ($this->teamManager->getTeamAdminList($this->getTeam()) as $teamPlayer)
                {
                    $teamPlayerId = $teamPlayer->getId();
                    yield $teamPlayerId => new EventSubject($teamPlayer, 
                        isset($attendance[$teamPlayerId]) ? $attendance[$teamPlayerId] : null);
                }
            })());
        });
    }
    
    function getSubjectsWithoutAdmins()
    {
        return $this->lazyLoad('subjectsWithoutAdmins', function()
        {
            return iterator_to_array((function($admins)
            {
                foreach ($this->getSubjects() as $teamPlayerId => $subject)
                {
                    if (!isset($admins[$teamPlayerId])) yield $teamPlayerId => $subject;
                }
            })($this->getAdminSubjects()));
        });
    }
    
    private function fetchLineupSubjects($onlyRegisteredMembers = true)
    {
        $subjects = $this->getSubjects($onlyRegisteredMembers);
        $result = [];
        foreach ($this->teamEventManager->getEventLineupPlayers($this->getLineup()) as $lineupPlayer)
        {
            $teamPlayerId = $lineupPlayer->getTeamPlayerId();
            if (isset($subjects[$teamPlayerId]))
            {
                $subject = $subjects[$teamPlayerId];
                if (!is_null($subject))
                {
                    $result[$lineupPlayer->getLineupPosition()][$teamPlayerId] =
                        $subject->addLineupPlayer($lineupPlayer);
                }
            }
        }
        return $result;
    }
    
    function getLineupSubjects($onlyRegisteredMembers = true)
    {
        return $this->lazyLoad('lineupSubjects', function() use($onlyRegisteredMembers)
        {
            return $this->fetchLineupSubjects($onlyRegisteredMembers);
        });
    }
}