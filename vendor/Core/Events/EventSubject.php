<?php namespace Core\Events;

use Webteamer\Team\Model\TeamPlayer;
use Webteamer\Event\Model\EventAttendance;
use Webteamer\Team\Model\TeamMatchLineupPlayer as LineupPlayer;

class EventSubject
{
    private $teamPlayer;
    private $attendance;
    private $lineupPlayer;
    
    function __construct(...$args)
    {
        foreach ($args as $arg)
        {
            if ($arg instanceOf TeamPlayer)
            {
                $this->teamPlayer = $arg;
            }
            if ($arg instanceOf EventAttendance)
            {
                $this->attendance = $arg;
            }
            if ($arg instanceOf LineupPlayer)
            {
                $this->lineupPlayer = $arg;
            }
        }
        if (!isset($this->teamPlayer)) throw new Exception('Invalid EventSubject');
    }
    
    function getTeamPlayer() { return $this->teamPlayer; }
    function getAttendance() { return $this->attendance; }
    function getLineupPlayer() { return $this->lineupPlayer; }
    
    function hasAttendance() { return isset($this->attendance); }
    function hasLineupPlayer() { return isset($this->lineupPlayer); }
    
    function __get($name)
    {
        switch ($name)
        {
            case "teamPlayer": return $this->getTeamPlayer();
            case "attendance": return $this->getAttendance();
            case "lineupPlayer": return $this->getLineupPlayer();
            default: throw new Exception('Invalid property name');
        }
    }
    
    function addLineupPlayer(LineupPlayer $lineupPlayer)
    {
        return new EventSubject($this->teamPlayer, $this->attendance, $lineupPlayer);
    }
}