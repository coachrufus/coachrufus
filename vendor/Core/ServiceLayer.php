<?php
namespace Core;


class ServiceLayer
{
    public static $services = array();
    private static $service_instances = array();
    public static function getService($service_name)
    {
        if (!array_key_exists($service_name, self::$service_instances))
        {
            $service_path_parts = explode('/',self::$services[$service_name]['class']);
            $service_class_name_parts = explode('.',end($service_path_parts));
            
            $service_class_name = current($service_class_name_parts);
            
            if(array_key_exists('params', self::$services[$service_name]))
            {
                $service_params = self::$services[$service_name]['params'];
                
                if(array_key_exists('singleton',self::$services[$service_name]))
                {
                    $service_object = $service_class_name::getInstance();
                }
                else
                {
                    $reflection_class = new \ReflectionClass($service_class_name);
                    $service_object = $reflection_class->newInstanceArgs($service_params);
                }
            }
            else
            {
                if('' == $service_class_name)
                {
                    //echo $service_name.' NOT added';exit;
                    throw new \Exception($service_name.' NOT added',004);
                }
                
                if(array_key_exists('singleton',self::$services[$service_name]))
                {
                    $service_object = $service_class_name::getInstance();
                }
                else
                {
                    $service_object = new $service_class_name();
                }
                
            }
            
            
            self::$service_instances[$service_name] = $service_object;
        }
        
        return self::$service_instances[$service_name];
    }
    
    public static function addService($key,$service)
    {
        
        if(array_key_exists($key, self::$services))
        {
            throw new Exception('Service exist', 003);
        }
        else
        {
            //require_once($service['class']);
            //use $service['class'];
            
            self::$services[$key] = $service;
        }
    }
	
    public static function addServiceInstance($key, $serviceInstance)
    {
        if (isset(self::$services[$key])) throw new Exception('Service exist', 003);
        else self::$service_instances[$key] = $serviceInstance;
    }
}