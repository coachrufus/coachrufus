<?php namespace Core\Permissions;

use Core\Db;

class PermissionRepository
{
    use Db;
    
    private $db;
    
    function __construct()
    {
        $this->db = $this->getDb();
    }
    
    function add(array $permision)
    {
        return $this->db->permissions()->insert($permision);
    }
    
    function find(callable $finder)
    {
        return $finder($this->db->permissions())->fetch();
    }
    
    function findByGuid(string $guid)
    {
        return $this->db->permissions()
            ->where('guid', $guid)
            ->fetch();
    }
    
    function delete(string $guid)
    {
        return $this->db->permissions()
            ->where('guid', $guid)
            ->delete();
    }
}