<?php namespace Core\Permissions;

use Core\GUID;

class PermissionManager
{
    private $repository;
    
    function __construct(PermissionRepository $repository)
    {
        $this->repository = $repository;
    }
    
    function newPermission($subject, $data = null)
    {
        $permission = [
            'subject' => $subject,
            'guid' => GUID::create(),
            'date' => new \DateTime(),
            'data' => is_null($data) ? null : json_encode($data)
        ];
        $this->repository->add($permission);
        return (object)$permission;
    }
    
    function newUserPermission($subject, $userId, array $data = [])
    {
        return $this->newPermission($subject, array_merge($data, [
            'userId' => $userId
        ]));
    }
    
    function newPlayerPermission($subject, $playerId, array $data = [])
    {
        return $this->newPermission($subject, array_merge($data, [
            'playerId' => $playerId
        ]));
    }
    
    function getPermission(string $guid, $delete = true)
    {
        $permission = $this->repository->findByGuid($guid);
        if ($permission === false) return false;
        else
        {
            $result = (object)iterator_to_array($permission);
            if (!is_null($result->data) && is_string($result->data))
            {
                $result->data = json_decode($result->data);
            }
            if ($delete) $permission->delete();
            return $result;
        }
    }
    
    function delete(string $guid)
    {
        $this->repository->delete($guid);
    }
}