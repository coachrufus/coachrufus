<?php namespace Core;

class ActiveTeamManager
{
    const key = 'active_team';
    private $getterMapper;
    
    function __construct($getterMapper)
    {
        $this->getterMapper = $getterMapper;
    }
    
    function setActiveTeam($team)
    {
        $_SESSION[self::key] = $this->getterMapper->mapToArray($team, [
            'dataMapperRules'
        ]);
    }
    
    function getActiveTeam()
    {
        return $this->isActiveTeam() ? $_SESSION[self::key] : null;
    }
    
    function isActiveTeam()
    {
        return isset($_SESSION[self::key]) && !empty($_SESSION[self::key]);
    }
}