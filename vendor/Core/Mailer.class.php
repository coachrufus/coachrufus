<?php namespace Core;

class Mailer 
{
    private $transport = '' ;
    private $mailer;
	public function __construct() 
    {
    	require_once (LIB_DIR."/swiftMailer/swift_required.php");
    	$transport = $this->getTransport();
    	$this->mailer = \Swift_Mailer::newInstance($transport);
    	
    	return $this->mailer;
    }
    
    public function getTransport()
    {

         return  \Swift_SmtpTransport::newInstance('smtp.websupport.sk', 465,'ssl')
            ->setUsername('no-reply@coachrufus.com')
            ->setPassword('iSnGqhcDu7');

        /*
        return \Swift_SmtpTransport::newInstance('smtp.plus421.com', 587)
            ->setUsername('mailservis@plus421.com')
            ->setPassword('m232#2dqwdlsFEW4wservis');
         * 
         */
    }
    
    public function createMessage()
    {
    	$message = \Swift_Message::newInstance();
    	return $message;
    }
    
   
    /**
     * Odosle spravu, v pripade zlyhania odoslania vrati chybu
     * @param unknown $message
     * @return array
     */
    public function sendMessage($message)
    {
    	try
    	{
            if (MAIL_SEND)
            {
                $this->mailer->send($message);
            }
            if (MAIL_LOG)
            {
                \MailLogger::log([
                    "to" => $message->getTo(),
                    "subject" => $message->getSubject()
                ]);
            }
    		return array('RESULT' => 1);
    	}
    	catch(\Exception $e)
    	{
             $recipient = $message->getTo();
             if(is_array($recipient))
             {
                 $recipient = implode(',',array_keys($recipient));
             }
            
            $queueRepo  = ServiceLayer::getService('QueueRepository');
            $queueRepo->addRecord(array(
                'action_key' => 'email_error',
                'value' =>   $recipient.'###0###error:'.$e->getMessage().'', 
                'action_type' => 'email_resend',
                'data' => json_encode(array('subject' => $message->getSubject(),'body' =>$message->getBody(),'to' => $message->getTo() ),JSON_UNESCAPED_UNICODE)
                ));
                
            
            
            if (MAIL_LOG)
            {
                \MailLogger::log($message->getTo(), 'error', $e->getMessage());
            }
            return array('ERROR' => $e,'RESULT' => 0);
    	}
    }
    
    public function rawSend($message)
    {
        try
    	{
           $result = $this->mailer->send($message);
    	}
    	catch(\Exception $e)
    	{
            return $e->getMessage();
    	}
        
        return $result;
    }
}