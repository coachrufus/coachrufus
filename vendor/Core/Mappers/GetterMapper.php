<?php namespace Core\Mappers;

use Core\Collections\HashSet as Set;

class GetterMapper
{
    private function toFieldName($name)
    { 
        return lcfirst(substr($name, 3));
    }
    
    function mapToArray($object, $ignored = [])
    {
        $ignoreSet = Set::of($ignored);
        $rc = new \ReflectionClass($object);
        return iterator_to_array((function($object, $methods, $ignoreSet)
        {
            foreach ($methods as $method)
            {
                $name = $method->getName();
                if (
                    $method->isPublic() &&
                    $method->getNumberOfParameters() === 0 &&
                    strpos($name, 'get') === 0
                )
                {
                    $fieldName = $this->toFieldName($name);
                    if (!$ignoreSet->contains($fieldName))
                    {
                        yield $fieldName => $object->{$name}();
                    }
                }
            }
        })($object, $rc->getMethods(), $ignoreSet));
    }
}