<?php namespace Core\Mappers;

class ObjectListMapper
{
    private $getters = null;
    private $objectMapper = null;
    private $propertyMapper = null;
    private $itemFactory = null;
    
    function __construct(array $getters)
    {
        $this->getters = $getters;
    }
    
    function getGetters() { return $getters; }
    
    function setGetters($getters) { $this->getters = $getters; }
    
    function registerObjectMapper($objectMapper)
    {
        $this->objectMapper = $objectMapper;
    }
    
    function registerPropertyMapper($propertyMapper)
    {
        $this->propertyMapper = $propertyMapper;
    }
    
    function registerItemFactory($itemFactory)
    {
        $this->itemFactory = $itemFactory;
    }
    
    private function mapObjectToIterator($obj, $getters, $propertyMapper)
    {
        foreach ($getters as $getter)
        {
            yield $propertyMapper($obj, $getter);
        }
    }
    
    private function getValue($obj, $getter)
    {
        if (strpos($getter, '|') === false)
        {
            return $obj->{$getter}();
        }
        else
        {
            list($getter, $key) = explode('|', $getter);
            return $obj->{$getter}()[$key];
        }
    }
        
    private function mapObjectListToIterator($objList)
    {
        $getters = array_values($this->getters);
        $objectMapper = $this->objectMapper;
        $propertyMapper = is_null($this->propertyMapper) ? function($obj, $getter)
        {
            return is_null($obj) || is_null($getter) ? null : $this->getValue($obj, $getter);
        } : $this->propertyMapper;
        $itemFactory = is_null($this->itemFactory) ? function($iterator, $obj)
        {
            return iterator_to_array($iterator);
        } : $this->itemFactory;
        foreach ($objList as $obj)
        {
            $iterator = $this->mapObjectToIterator(is_null($objectMapper) ? $obj : $objectMapper($obj),
                $getters, $propertyMapper);
            yield $itemFactory($iterator, $obj);
        }
    }
    
    function createTable($objList)
    {
        $list = iterator_to_array($this->mapObjectListToIterator($objList));
        array_unshift($list, array_keys($this->getters));
        return $list;
    }
}