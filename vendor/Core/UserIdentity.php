<?php
namespace Core;
class UserIdentity 
{
	protected $user;
	protected $status;
	protected $roles =  array('guest');
	
	public function getRoles()
	{
		return $this->roles;
	}
	
	public function setRoles($val)
	{
		$this->roles = $val;
	}
	
	public function addRole($val)
	{
		$this->roles[] = $val;
	}
	
	public function setUser($val)
	{
		$this->user = $val;
	}
	
	public function getUser()
	{
		return $this->user;
	}
	
	public function getStatus()
	{
	    return $this->status;
	}

	public function setStatus($status)
	{
	    $this->status = $status;
	}
}