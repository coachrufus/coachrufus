<?php
namespace Core;

interface IdentityProvider
{
	
	  function findUserByUsername ($username);
	  function encodePassword ($password,$salt);
	  function getDefaultIdentity ();
}

class UserIdentityProvider  implements IdentityProvider
{
	/**
	 * Nájde užívateľa podľa jeho prihlasovacieho mena
	 * @see \Core\IdentityProvider::findUserByUsername()
	 * @param $username užívateľské meno (email)
	 * @return Object|false
	 */
	public function findUserByUsername($username)
	{
		return null;
	}

	/**
	 * Zakoduje heslo
	 * @see \Core\IdentityProvider::encodePassword()
	 * ©return string
	 */
	public function encodePassword($password,$salt)
	{
		$password = sha1($password).sha1($salt);
		return  $password;
	}


	public function getDefaultIdentity()
	{
		return new \Core\UserIdentity() ;
	}


}