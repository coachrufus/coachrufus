<?php

namespace Tournament;

class Api {

    private $key = '36dc495f888af9012b0b5f4edffdac9c';
    private $url = TOURNAMENT_API_URL;

    public function getList($userId)
    {
        return $this->call('/list','GET',array('authorId' => $userId));
    }

    public function getUserPermission($data)
    {
        return $this->call('/tournament/user-permission/get','GET',$data);
    }
    
    public function getUserTeams($data)
    {
        return $this->call('/tournament/user-teams/get','GET',$data);
    }
    
    public function getTournament($tournamentId)
    {
        return $this->call('/tournament','GET',array('id' => $tournamentId));
    }

    public function createTournament($data)
    {
        return $this->call('/tournament/create','POST',$data);
    }
    
    public function updateTournament($tournament)
    {
        return $this->call('/tournament/update','PUT',$tournament);
    }
    
    public function getTournamentTeams($tournamentId,$filter)
    {
        return $this->call('/tournament/teams','GET',array('tournamentId' => $tournamentId,'filter' => $filter));
    }
    
    public function getTournamentTeam($teamId)
    {
        return $this->call('/tournament/team','GET',array('teamId' => $teamId));
    }
    
     public function updateTournamentTeam($data)
    {
        return $this->call('/tournament/team/update','PUT',$data);
    }
    
    public function createTeamPermission($data)
    {
        return $this->call('/tournament/team-permission/create','POST',$data);
    }
    
    public function updateTeamPermission($data)
    {
        return $this->call('/tournament/team-permission/update','PUT',$data);
    }
    
    public function getTeamPermissionByHash($data)
    {
        return $this->call('/tournament/team-permission/get','GET',$data);
    }
    
    
    
    public function createTournamentTeamPlayers($data)
    {
        return $this->call('/tournament/team-players/create','POST',$data);
    }
    
    public function updateTournamentTeamPlayer($data)
    {
        return $this->call('/tournament/team-players/update','PUT',$data);
    }
    
    public function deleteTournamentTeamPlayer($data)
    {
        return $this->call('/tournament/team-players/delete','DELETE',$data);
    }
    
    
    public function getTeamPlayers($teamId)
    {
        return $this->call('/tournament/team-players','GET',array('teamId' => $teamId));
    }
    
    public function createTournamentSchedule($tournamentId,$scheduleData)
    {
        return $this->call('/tournament/schedule/create','POST',array('tournamentId' => $tournamentId,'schedule' => $scheduleData));
    }
    
    public function deleteTournamentSchedule($data)
    {
        return $this->call('/tournament/schedule/delete','DELETE',$data);
    }
    
    
    public function createTournamentMatch($eventId)
    {
        return $this->call('/tournament/match/create','POST',array('eventId' => $eventId));
    }
    
    
    public function getTournamentSchedules($tournamentId,$filter=array())
    {
        return $this->call('/tournament/schedules','GET',array('tournamentId' => $tournamentId,'filter' => $filter));
    }
    
    public function getTournamentEvents($tournamentId,$filter = array())
    {
        $response = $this->call('/tournament/events','GET',array('tournamentId' => $tournamentId,'filter' => $filter));
        return $response['response'];
    }
    
    public function getTournamentEvent($id)
    {
        return $this->call('/tournament/event','GET',array('id' => $id));
    }
    public function updateTournamentEvent($data)
    {
        return $this->call('/tournament/event/update','PUT',$data);
    }
    
    public function changeEventNomination($data)
    {
        return $this->call('/tournament/event/change/nomination','POST',$data);
    }
    
    
    public function createMatchHit($data)
    {
        return $this->call('/tournament/event/hit/create','POST',$data);
    }
    
    public function editMatchHit($data)
    {
        return $this->call('/tournament/event/hit/edit','PUT',$data);
    }
    
    public function getEventMatchTimeline($id)
    {
        return $this->call('/tournament/event/hit/timeline','GET',array('id' => $id));
    }
    
    public function resetEvent($data)
    {
        return $this->call('/tournament/event/reset','PUT',$data);
    }
    
    public function openEvent($data)
    {
        return $this->call('/tournament/event/open','PUT',$data);
    }
    
    
    public function clearEventStats($data)
    {
        return $this->call('/tournament/event/clear-stats','DELETE',$data);
    }
    
    public function updateEventResult($data)
    {
        return $this->call('/tournament/event/result','POST',$data);
    }
    
    
    public function getTournamentStats($id,$data)
    {
        return $this->call('/tournament/stats','GET',array('id' => $id,'filter' => $data));
    }
    
    public function getTournamentPlayersStats($data)
    {
        return $this->call('/tournament/stats/players','GET',$data);
    }
    
    /*
    public function getTournamentEventLiveData($eventId)
    {
        return $this->call('/tournament/event/live','GET',array('id' => $eventId));
    }
    */
    

    private function call($apiMethod = null, $httpMethod = 'GET', $params = array())
    {
        $ch = curl_init();
        
        $header =  array(
            'Accept: application/json',
            'X-Auth-Token: api-key ' . $this->key,
        );
        $urlParams = '';
        $restParams = '';
        if($httpMethod == 'POST')
        {
            curl_setopt($ch, CURLOPT_POST, TRUE);
            $restParams = json_encode($params);
            curl_setopt($ch, CURLOPT_POSTFIELDS, array('data' => $restParams));
             //$header[] = 'Content-Type: application/x-www-form-urlencoded';
        }
        elseif('PUT' == $httpMethod)
        {
            $restParams = json_encode($params);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
        }
        elseif('DELETE' == $httpMethod)
        {
            //$restParams = json_encode($params);
             curl_setopt($ch, CURLOPT_POSTFIELDS, array('data' => $restParams));
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
        }
        else 
        {
            //$header[] = 'Content-Type: application/json';
            if(!empty($params))
            {
                $urlParams = '?'.http_build_query($params);
            }
            
        }

        
        $restUrl = $this->url.$apiMethod.$urlParams;
        
        if(RUN_ENV == 'dev')
        {
           $log = $apiMethod."\n";
           ob_start();
           var_dump($params);
           $log .= ob_get_contents();
           ob_end_clean();
           file_put_contents(APPLICATION_PATH . '/log/tournament.log', date('Y-m-d H:i:s').': '.$log, FILE_APPEND);
        }

       
       
        curl_setopt($ch, CURLOPT_URL, $restUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        
        


        $response = curl_exec($ch);
        curl_close($ch);
        
        if(RUN_ENV == 'dev')
        {
            
           t_dump('API URL:'.$restUrl);
           t_dump('API PARAMS:'.$restParams); 
           t_dump('API RAW RESPONSE: '.$response);
           t_dump(json_decode($response,true));
           
            
        }
        
        
        
        return json_decode($response,true);
    }

}
