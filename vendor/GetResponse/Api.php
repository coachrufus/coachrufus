<?php

namespace GetResponse;

class Api {

    //private $key = '1Info@CoachRufus.Com';
    //private $key = '5a47f57e89823027d05c82e7cc459f8e';
    private $key = '36dc495f888af9012b0b5f4edffdac9c';
    private $url = 'https://api.getresponse.com/v3';

    public function getContacts()
    {
        return $this->call('/contacts');
    }
    
    public function getContact($id)
    {
        return $this->call('/contacts/'.$id);
    }
    
     public function getContactByEmail($email)
    {
        $records = $this->call('/contacts/?query[email]='.$email);
        return $records[0];
    }
    
    public function updateContact($id,$data)
    {
        return $this->call('/contacts/'.$id,'POST',$data);
    }
    
    public function addContact($data)
    {
        return $this->call('/contacts','POST',$data);
    }
    
    public function getWorkflows()
    {
        return $this->call('/workflow');
    }
    
    public function getTags()
    {
        return $this->call('/tags');
    }
    
     public function createTag($name)
    {
        return $this->call('/tags','POST',array('name' =>$name ));
    }
    
    public function getCustomFields()
    {
        return $this->call('/custom-fields');
    }
    
    
    
    public function getCampaings()
    {
        return $this->call('/campaigns');
    }
    

    private function call($apiMethod = null, $httpMethod = 'GET', $params = array())
    {

        return;
        /*
        if(RUN_ENV == 'dev')
        {
           $log = $apiMethod;
           ob_start();
           var_dump($params);
           $log .= ob_get_contents();
           ob_end_clean();
          
           file_put_contents(APPLICATION_PATH . '/log/getresponse.log', date('Y-m-d H:i:s').': '.$log, FILE_APPEND);
        }

        $params = json_encode($params);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url . $apiMethod);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Accept: application/json',
            'X-Auth-Token: api-key ' . $this->key,
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        

        if($httpMethod == 'POST')
        {
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        }

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        
        


        $response = curl_exec($ch);
        curl_close($ch);
        
        return json_decode($response);
         * 
         */
    }

}
