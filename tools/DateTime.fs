﻿namespace Lib

open System
open System.Text
open System.Text.RegularExpressions
open System.Runtime.CompilerServices
open System.Globalization

[<Extension>]
type DateTimeExtensions() =
    
    static let isoDateTimeFormat = CultureInfo.InvariantCulture.DateTimeFormat;

    [<Extension>]
    static member ToISOString(dateTime : DateTime) =
        dateTime.ToString(isoDateTimeFormat.UniversalSortableDateTimePattern)

    [<Extension>]
    static member ToSKDateString(dateTime : DateTime) =
        dateTime.ToString("dd.MM.yyyy")
    
    static member ParseISO(isoDateTime : string) =
        DateTime.Parse(isoDateTime, null, DateTimeStyles.RoundtripKind)

    static member ParseSK(skDateTime : string) =
        DateTime.Parse(skDateTime, CultureInfo.CreateSpecificCulture("sk-SK"))

    [<Extension>]
    static member ShortYear (dateTime : DateTime) =
        dateTime.ToString("yy")
        |> Int32.Parse

    [<Extension>]
    static member ToDate (skDate : string) = 
        let i = skDate.Replace(" ", "").Split '.'
        new DateTime (
            year = Int32.Parse (i.[2]),
            month = Int32.Parse (i.[1]),
            day = Int32.Parse (i.[0])
        )

    [<Extension>]
    static member GetTime (dateTime : DateTime) = 
        let time = dateTime.ToUniversalTime() - new DateTime(1970, 1, 1)
        int64 (time.TotalMilliseconds + 0.5)

    [<Extension>]
    static member ToRFC822Format (dateTime : DateTime) =
        sprintf "%s UT" (dateTime.ToUniversalTime().ToString("ddd, dd MMM yyyy HH:mm:ss"))

module WeekDayName =

    let toWeekDay (dayName : string) = 

        match dayName.ToLower() with
        | "monday" -> 0
        | "tuesday" -> 1
        | "wednesday" -> 2
        | "thursday" -> 3
        | "friday" -> 4
        | "saturday" -> 5
        | "sunday" -> 6
        | _ -> failwith "invalid day name"