﻿namespace Lib

open System
open System.Collections.Generic
open System.Dynamic
open System.Reflection

type PropertyObject(myObj : obj) =
    interface IDictionary<string, obj> with

    override d.TryGetMember(binder : GetMemberBinder, result : byref<obj>) =
        result <- myObj.GetType().GetProperty(binder.Name).GetValue(myObj)
        true

    override d.TrySetMember(binder : SetMemberBinder, value : obj) =
        myObj.GetType().GetProperty(binder.Name).SetValue(myObj, value)
        true

type Test() =
    member t.Name = "Fero"

let propObj = PropertyObject(new Test()) :> IDictionary<string, obj>
propObj