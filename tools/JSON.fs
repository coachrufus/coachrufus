﻿module Lib.JSON

open System.IO
open System.Runtime.Serialization.Json
open System.Xml
open System.Text

let stringify<'t> (obj : 't) =   
        use stream = new MemoryStream() 
        (new DataContractJsonSerializer(typeof<'t>)).WriteObject(stream, obj) 
        Encoding.UTF8.GetString(stream.ToArray()) 

let parse<'t> (jsonString : string) : 't =  
        use stream = new MemoryStream(Encoding.UTF8.GetBytes(jsonString)) 
        let obj = (new DataContractJsonSerializer(typeof<'t>)).ReadObject(stream) 
        obj :?> 't