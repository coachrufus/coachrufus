﻿module Lib.QueryExpressions

open System
open System.Linq
open Microsoft.FSharp.Quotations

type PartialQueryBuilder() =
    inherit Linq.QueryBuilder()
    member this.Run(e : Expr<Linq.QuerySource<'T, IQueryable>>) = e

let pquery = PartialQueryBuilder()

type Linq.QueryBuilder with
    [<ReflectedDefinition>]
    member this.Source(qs : Linq.QuerySource<'T, _>) = qs