﻿namespace Lib.Net

open System
open System.Web
open Lib.Text

module Name =

    let rec toUnique (exists : string -> bool) (index : int) (baseName : string) =
        let uri =
            match index with
            | 0 -> baseName
            | i -> sprintf "%s-%d" baseName i
        if exists uri then toUnique exists (index + 1) baseName
        else uri

    let toUri (exists : string -> bool) (name : string) =
        name.Webalize() |> toUnique exists 0

module RelativeLocation =

    let toAbsolute (location : string) =
        let request = HttpContext.Current.Request
        String.Format("{0}://{1}{2}", request.Url.Scheme, request.Url.Authority,
            if location.StartsWith("~") then location.[1..] else location)