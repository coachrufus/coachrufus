﻿open System
open System.IO
open System.Text
open System.Text.RegularExpressions

let toLines text = Regex.Split(text, "\r\n|\r|\n")

let identifierChooser (line : string) =
    match line.Trim() with
    | line when line.StartsWith("`") -> 
        let line = line.[1..]
        line.[..line.IndexOf('`') - 1] |> Some
    | _ -> None

let extractIdentifiers = Seq.choose identifierChooser

let toArray (identifiers : seq<string>) =
    identifiers
    |> Seq.filter((<>) "id")
    |> Seq.map (sprintf "    '%s' => ''")
    |> fun items -> String.Join(",\n", items)
    |> sprintf "[\n%s\n]"

let create = @"
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `package_id` int(10) unsigned NOT NULL,
  `team_id` int(11) NOT NULL,
  `team_name` varchar(75) COLLATE utf8_slovak_ci NOT NULL,
  `date` datetime NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `name` varchar(75) COLLATE utf8_slovak_ci NOT NULL,
  `note` text COLLATE utf8_slovak_ci NOT NULL,
  `price_month` decimal(10,2) DEFAULT NULL,
  `price_year` decimal(10,2) DEFAULT NULL,
  `products` text COLLATE utf8_slovak_ci NOT NULL,
  `player_limit` int(10) unsigned NOT NULL,
"

create |> toLines |> extractIdentifiers |> toArray