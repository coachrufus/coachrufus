﻿namespace Lib.Text

open System
open System.Text
open System.Text.RegularExpressions
open System.Runtime.CompilerServices
open System.Web

[<Extension>]
type StringUtils () =
    
    [<Extension>]
    static member inline Turncate(str : string, maxLength : int, suffix : string) =
        if String.IsNullOrEmpty(str) || str.Length <= maxLength then str
        else str.[0..maxLength - 1] + suffix

    [<Extension>]
    static member inline Turncate(str : string, maxLength : int) =
        StringUtils.Turncate(str, maxLength, "")

    [<Extension>]
    static member inline StripHtml input =
        Regex.Replace(input, "<.*?>", "")

    [<Extension>]
    static member inline RemoveDiacritics (input : string) =
        input
        |> (Encoding.GetEncoding 1251).GetBytes
        |> Encoding.ASCII.GetString

    [<Extension>]
    static member inline Webalize(input : string) =
        let rec link acc = function
        | ' ' :: t | '-' :: t -> t |> link ('-' :: acc)
        | h :: t when Char.IsLetter h || Char.IsDigit h ->
            t |> link (Char.ToLower h :: acc)
        | _ :: t -> t |> link acc
        | [] -> acc
        input
        |> StringUtils.RemoveDiacritics
        |> List.ofSeq |> link [] |> List.rev |> String.Concat

    static member inline OfCharSeq (chars : char seq) =
        let builder = new StringBuilder()
        for c in chars do builder.Append(c)
        builder.ToString()

    [<Extension>]
    static member inline Capitalize(input : string) =
        if String.IsNullOrEmpty input then input
        else input.[0..0].ToUpper() + input.[1..]

    [<Extension>]
    static member inline ToLines(input : string) =
        Regex.Split(input, "\r\n|\r|\n")

    [<Extension>]
    static member inline IsNumber(input : string) =
        input |> Decimal.TryParse |> fst

    [<Extension>]
    static member inline IsIntNumber(input : string) =
        input |> Int64.TryParse |> fst

module Base64Token =

    let ofText (input : string) =
        input
        |> Encoding.UTF8.GetBytes
        |> HttpServerUtility.UrlTokenEncode

    let toText (base64Token : string) =
        base64Token
        |> HttpServerUtility.UrlTokenDecode
        |> Encoding.UTF8.GetString