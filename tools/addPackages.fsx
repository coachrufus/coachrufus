﻿#r @"c:\Projekty\421_coachrufus\tools\bin\Release\Lib.dll"

open System
open System.IO
open System.Text
open System.Text.RegularExpressions
open Lib.Text

let removeBrackets (line : string) =
    match line.IndexOf("(") with
    | -1 -> line
    | x -> line.[..x - 1]

let createIdentifier (line : string) =
    (line |> removeBrackets)
        .Trim()
        .ToLower()
        .RemoveDiacritics()
        .Replace(":", "")
        .Replace("+", "")
        .Replace("-", "")
        .Replace("/", "")
        .Replace("\\", "")
        .Replace(" ", "-")
        .Replace("----", "-")
        .Replace("---", "-")
        .Replace("--", "-")

let products =
    @"c:\Projekty\421_coachrufus\tools\prods.tsv"
    |> File.ReadAllLines
    |> Seq.mapi
        (fun i l ->
            let is_enabled = if l.Split([|'\t'|]).[0].Trim() = "A" then 1 else 0
            sprintf "('%s', '%s', %d)" 1 name is_enabled)
    |> fun values ->
        sprintf @"INSERT INTO package_products (package_id, product_id, `is_enabled`)
            VALUES %s" (String.Join(",", values))
