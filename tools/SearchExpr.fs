﻿module Lib.SearchExpr

open System
open System.Linq
open Lib.Text
open System.Text

let filterAlphaNum (input : string) =
    input
    |> Seq.map(fun c -> if Char.IsLetterOrDigit(c) then c else ' ')
    |> fun chars -> new String(chars.ToArray())

let toContainsExpr (input : string) =
    (filterAlphaNum input).Split ' '
    |> Seq.choose
        (function
        | "" -> None
        | term -> Some(sprintf "\"%s*\"" term))
    |> fun words -> String.Join(" AND ", words)

let private createLikeTerm (keyword : string) (supplementBegin : bool) (supplementEnd : bool) =
    let builder = new StringBuilder()
    builder.Append("'") |> ignore
    if supplementBegin then builder.Append("%") |> ignore
    builder.Append(keyword.RemoveDiacritics().ToLower()) |> ignore
    if supplementEnd then builder.Append("%") |> ignore
    builder.Append("'") |> ignore
    builder.ToString()

let toLikeExpr (columnName : string) (input : string) =
    (filterAlphaNum input).Split ' '
    |> Seq.filter(fun term -> term <> "")
    |> Seq.mapi
        (fun i term ->
            let term = createLikeTerm term (i <> 0) true
            sprintf "LOWER(cast(%s as varchar(max)) collate SQL_Latin1_General_Cp1251_CS_AS) LIKE %s" columnName term)
    |> fun words -> sprintf "(%s)" (String.Join(" AND ", words))