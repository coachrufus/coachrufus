﻿namespace Lib

module Null =

    let toValue (defaultValue : 'a) (value : 'a) =
        match box value with
        | null -> defaultValue
        | value -> value :?> 'a 

module Nullable =

    open System

    let toValue (defaultValue : 'a) (value : Nullable<'a>) =
        if value.HasValue then value.Value else defaultValue

    let toOption (n : System.Nullable<_>) = 
       if n.HasValue 
       then Some n.Value 
       else None

    let isNullableType(t : Type) =
        t.IsGenericType && t.GetGenericTypeDefinition() = typedefof<Nullable<_>> 
    
    let nullableTypedefof(t : Type) =
        if t |> isNullableType then t.GetGenericArguments().[0]
        else null