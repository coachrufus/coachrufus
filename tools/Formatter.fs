﻿module Lib.Formatter.String

open System
open System.Text
open System.Text.RegularExpressions

let toLines input = Regex.Split(input, "\r\n|\r|\n")

let toParagraphs =
    let regex = new Regex(@"(?<p>.*?)(?:(\r\n){2,}|\r{2,}|\n{2,}|$)", RegexOptions.Singleline)
    regex.Matches
    >> Seq.cast<Match>
    >> Seq.choose
        (fun m ->
            match m.Groups.["p"].Value with
            | "" -> None | p -> Some p)

let private (|IsTitle|_|) input =
    let m = Regex.Match(input, "^/(?<level>[1-6]{1}).(?<content>.*)", RegexOptions.Singleline)
    if m.Success then Some(m.Groups.["level"].Value |> Int32.Parse, m.Groups.["content"].Value)
    else None

let private (|IsList|_|) input =
    let m = Regex.Match(input, "^/-(?<name>\w*)(\r\n|\r|\n)(?<content>.*)", RegexOptions.Singleline)
    if m.Success then
        let name =
            let name = m.Groups.["name"].Value
            if name = "" then "ul" else name
        Some(name, m.Groups.["content"].Value)
    else None

type private Token =
| Paragraph of string seq
| List of string * string seq
| Title of int * string

let private tokenize =
    toParagraphs
    >> Seq.map
        (function
        | IsTitle title -> Title(title)
        | IsList (name, content) -> List(name, content |> toLines)
        | paragraph -> Paragraph(paragraph |> toLines))

let private formatLine input =

    let replaceTags id tag input =
        let regex = new Regex(@"\(" + Regex.Escape(id) + "(?<tagName>[a-zA-Z]{1}\w*)\)(?<inner>.*?)\(\/\1\)", RegexOptions.Singleline)
        let rec replaceTags input =
            if regex.IsMatch(input) then
                regex.Replace(input,
                    fun (m : Match) ->
                        sprintf "<%s class='%s'>%s</%s>" tag (m.Groups.["tagName"].Value) (m.Groups.["inner"].Value |> replaceTags) tag)
            else input
        input |> replaceTags

    let input = input |> replaceTags ":" "span" |> replaceTags "#" "div"
    ["(*", "<strong>"; "*)", "</strong>"; "(/", "<em>"; "/)", "</em>"; "(-", "<strike>"; "-)", "</strike>"]
    |> List.fold(fun acc (oldVal, newVal) -> (acc : string).Replace(oldVal, newVal)) input

let toHtml input =
    input
    |> tokenize
    |> Seq.map
        (function
        | Paragraph lines -> sprintf "<p>%s</p>" (String.Join("<br>", lines |> Seq.map formatLine))
        | List (name, lines) -> sprintf "<%s>%s</%s>" name (lines |> Seq.map(fun line -> sprintf "<li>%s</li>" (line |> formatLine)) |> String.Concat) name
        | Title (level, content) -> sprintf "<h%d>%s</h%d>" level content level)
    |> fun lines -> String.Join("\n\n", lines)