﻿module Lib.WordIndex

open System
open Lib.Text

type NamedItem<'t> = { Name : string; Value : 't }

let private split (input : string) =
    input.Split() |> Seq.filter ((<>) "")

let private join (words : string seq) =
    String.Join(" ", words)

let private toKey (input : string) =
    input
        .RemoveDiacritics()
        .ToLower()
        .Trim()
    |> sprintf " %s"

let create (names : NamedItem<'t> seq) =
    names
    |> Seq.map 
        (fun item ->
            let words = item.Name |> split |> join
            words |> toKey, words, item.Value)

let findTerm (term : string) (index : (string * string * 't) seq) =
    index
    |> Seq.choose
        (fun (i, n, v) ->
            if i.Contains(term |> split |> join |> toKey) then
                Some (n, v)
            else None)

let equalsTerm (term : string) (index : (string * string * 't) seq) =
    index
    |> Seq.exists
        (fun (i, n, v) -> i = (term |> split |> join |> toKey))