﻿module Lib.Num

open System

let round (n : decimal) = Math.Round(n, MidpointRounding.AwayFromZero)

let roundToInt (n : decimal) = n |> round |> int

