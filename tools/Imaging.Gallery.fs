﻿namespace Lib.Imaging

open System
open System.Runtime.CompilerServices
open System.IO
open System.Windows
open System.Windows.Media
open System.Windows.Media.Imaging

open Lib.Imaging
open Lib.Imaging.Sizing

type GalleryManager(thumb: Size, maxSize: Size, ext : string, saver : string -> Image -> unit) =     

    let checkDirectory (path : string) =
        if path |> Directory.Exists |> not then
            Directory.CreateDirectory(path)
            |> ignore

    member g.FromFiles(files : string seq, outputPath : string) =
        let thumbPath = Path.Combine(outputPath, "thumb")
        checkDirectory outputPath
        checkDirectory thumbPath
        files
        |> Seq.iteri
            (fun i path ->
                let fn = sprintf "%d.%s" (i + 1) ext
                let img = Image.From(path)
                img
                    .Resize(maxSize.Width, maxSize.Height, Fit.Inside)
                    |> saver (Path.Combine(outputPath, fn))
                img
                    .Resize(thumb.Width, thumb.Height, Fit.Outside)
                    .Crop(thumb.Width, thumb.Height)
                    |> saver (Path.Combine(thumbPath, fn)))   