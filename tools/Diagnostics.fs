﻿module Lib.Diagnostics

open System
open System.Web
open System.Reflection
open System.Diagnostics
open System.Linq

let private nullableToString (value : obj) =
    match value with
    | null -> "null"
    | v -> v.ToString()

let private memberToString (instance : obj) (m : MemberInfo) =
    let t = instance.GetType()
    let name = Enum.GetName(typedefof<MemberTypes>, m.MemberType)
    let x = 
        match m.MemberType with
        | MemberTypes.Constructor
        | MemberTypes.Method -> sprintf "() : %s" name
        | MemberTypes.Field ->
            let value =
                match t.GetField(m.Name) with
                | null -> "null"
                | f -> f.GetValue() |>  nullableToString
            sprintf " : %s = %s" (name.ToString()) value
        | MemberTypes.Property ->
            let prop = t.GetProperty(m.Name)
            let value =
                match prop with
                | null -> "null"
                | p -> 
                    if p.CanRead || p.GetIndexParameters().Length = 0 then
                        p.GetValue(instance) |> nullableToString
                    else sprintf " : %s" name
            sprintf " : %s = %s" (name.ToString()) value
        | _ -> sprintf " : %s" name
    sprintf "%s%s" m.Name x

let private toTypeName (t : Type) =
    match t.BaseType with
    | null -> sprintf "%s" t.Name
    | b -> sprintf "%s : %s" t.Name b.Name

let dump (o : #obj) =
    let t = o.GetType()
    let members = 
        t
         .GetMembers()
         .OrderByDescending
            (fun m ->
                m.MemberType = MemberTypes.Field ||
                m.MemberType = MemberTypes.Property)
        |> Seq.map
            (fun m ->
                sprintf "    %s" (m |> memberToString o))

    sprintf
        "%s = {\n%s\n}"
        (t |> toTypeName)
        (String.Join(Environment.NewLine, members))