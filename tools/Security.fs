﻿namespace Lib.Security

open System
open System.Text
open System.Security.Cryptography

module Hash =

    type Options = { Algorithm : HashAlgorithm; Salt : string }

    let compute (options : Options) (input : string) =
        input + options.Salt
        |> Encoding.UTF8.GetBytes
        |> options.Algorithm.ComputeHash
        |> Convert.ToBase64String

module AgreementNumber =

    let generate() =
        let ticks = DateTime.Now.Ticks.ToString()
        ticks.[..8] + "-" + ticks.[9..]

module Password =

    module Chars =

        let alphanum = 
            [[48..57]; [65..90]; [97..122]]
            |> List.concat
            |> List.map char

        let all = [33..126] |> List.map char

    let generate length (chars : char list) =
        let max = chars.Length - 1
        let rnd = new Random(DateTime.Now.Millisecond)
        seq { for i = 0 to length do
                yield chars.[rnd.Next(0, max)] }
        |> String.Concat