﻿namespace Lib.IO

open System
open System.IO

type PositionWrapperStream(wrapped : Stream) =
    inherit Stream()
    let mutable position = 0L
    override s.CanSeek with get() = false
    override s.CanWrite with get() = true
    override s.CanRead with get() = raise (new NotSupportedException())
    override s.Length with get() = raise (new NotSupportedException())
    override s.Position
        with get() = position
        and set(value) = raise (new NotSupportedException())
    override s.Write(buffer, offset, count) =
        position <- position + (int64 count)
        wrapped.Write(buffer, offset, count)
    override s.Read(buffer, offset, count) = raise (new NotSupportedException())
    override s.Seek(offset, origin) = raise (new NotSupportedException())
    override s.SetLength(value) = raise (new NotSupportedException())
    override s.Flush() = wrapped.Flush()
    override s.Dispose(disposing : bool) =
        wrapped.Dispose()
        base.Dispose(disposing)