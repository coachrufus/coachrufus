﻿module Lib.Mail

open System
open System.Net.Mail

module Address =

    let createOrDefault (input : string) =
        try new MailAddress(input) with :? Exception -> null

    let isValid (input : string) =
        match input |> createOrDefault with
        | null -> false
        | _ -> true