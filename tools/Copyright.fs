﻿module Lib.Copyright

open System

let year (initialYear : int) =
    let actualYear = DateTime.Now.Year
    if initialYear = actualYear then initialYear.ToString()
    else sprintf "%d - %d" initialYear actualYear