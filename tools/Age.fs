﻿module Lib.Age

open System

let fromDateOfBirth (dateOfBirth : DateTime) =
    let birthDay = dateOfBirth
    let today = DateTime.Today
    let age = today.Year - birthDay.Year
    if birthDay > today.AddYears(-age) then age - 1
    else age