﻿namespace Lib

open System
open System.Linq
open System.Runtime.CompilerServices
open System.Collections.Generic
open System.Dynamic
open System.ComponentModel
open System.Linq.Expressions

module Seq =

    let cons h mseq =
        seq { yield h; yield! mseq }

    let rec splitEach size mseq =
        if Seq.length mseq < size then seq [mseq]
        else
            mseq
            |> Seq.skip size
            |> splitEach size
            |> cons (mseq |> Seq.take size)

[<Extension>]
type AnonymousUtils () =

    [<Extension>]
    static member inline ToExpando(anonymousObject : obj) =
        let expando = new ExpandoObject()
        for propertyDescriptor in TypeDescriptor.GetProperties(anonymousObject) do
            let obj = propertyDescriptor.GetValue(anonymousObject)
            (expando :> IDictionary<string, Object>).Add(propertyDescriptor.Name, obj)
        expando

    [<Extension>]
    static member inline ToExpando(anonymousObjects : seq<obj>) =
        anonymousObjects |> Seq.map(AnonymousUtils.ToExpando)

module CurrencyString =

    let ofDecimal (input : decimal) =
        match input.ToString("N") with
        | str when str.EndsWith(",00") ->
            str.[0..str.Length - 4]
        | str -> str

module Prop =

    let get<'t> (name : string) (myObj : obj) =
        myObj.GetType().GetProperty name

    let tryGet<'t> (name : string) (myObj : obj) =
        match myObj.GetType().GetProperty name with
        | null -> None
        | prop -> Some prop

    let tryGetValue<'t> (name : string) (myObj : obj) =
        match myObj |> get name with
        | null -> None
        | prop -> myObj |> prop.GetValue |> Some 

type PropertyAccessor(myObj : obj) =
    member this.Item
          with get name =
            match myObj |> Prop.get name with
            | null -> raise (KeyNotFoundException())
            | prop -> prop.GetValue(myObj)

          and set name value =
            match myObj |> Prop.get name with
            | null -> raise (KeyNotFoundException())
            | prop -> prop.SetValue(myObj, value)

module Map =

    let keys map =
        map
        |> Map.toSeq
        |> Seq.map fst

[<Extension>]
type MapUtils () =

    [<Extension>]
    static member inline Keys(map) = map |> Map.keys

type Expr = 
    static member Quote<'t>(e : Expression<'t>) = e