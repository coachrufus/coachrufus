<?php
mb_internal_encoding("UTF-8");
date_default_timezone_set('Europe/Prague');
//setlocale(LC_ALL, 'slovak');

require_once (LIB_DIR . "/swiftMailer/swift_required.php");
require_once LIB_DIR . '/Facebook/autoload.php';
//require_once LIB_DIR.'/googleApi/autoload.php';
require_once LIB_DIR.'/autoload.php';
require_once LIB_DIR.'/GoPay/autoload.php';


class MailLogger
{
    static function log($address, $type = 'success', $data = null)
    {
        $address = json_encode($address);
        $log = date('Y-m-d H:i:s')." {$address} : {$type} \n";
        if (!is_null($data)) $log .= json_encode($data) . '\n';
        $log .= "\n";
        file_put_contents(APPLICATION_PATH . '/log/mail.log', $log, FILE_APPEND);
    }
}

function core_autoload ($class) {

        $web_app = Core\WebApp::getInstance();
	$register_namespaces = $web_app->getNamespaces();

	$parts = explode('\\', $class);
	$class = array_pop($parts);
	array_pop($parts);
	$namespace = implode('\\',$parts);

	if ($namespace ==="Latte" && !isset($register_namespaces["Latte"])) return;
	
	$require_file = $register_namespaces[$namespace].'/model/'.$class.'.php';

	//skusim kniznice
	if(!file_exists($require_file))
	{
		$require_file = $register_namespaces[$namespace].'/lib/'.$class.'.php';
	}
        
        //skusim controller
	if(!file_exists($require_file))
	{
		$require_file = $register_namespaces[$namespace].'/controller/'.$class.'.php';
	}
	
	//skusim entity
	if(!file_exists($require_file))
	{
		$require_file = $register_namespaces[$namespace].'/model/Entity/'.$class.'.php';
	}
        
	//skusim mapery
        /*
	if(!file_exists($require_file))
	{
		
		$require_file = $register_namespaces[$namespace].'/model/Entity/'.$class.'Mapper.php';
	}
	*/
	//skusim formy
	if(!file_exists($require_file))
	{
		$require_file = $register_namespaces[$namespace].'/form/'.$class.'.php';
	}
	
	//skusi handlery
	if(!file_exists($require_file))
	{
		$require_file = $register_namespaces[$namespace].'/handler/'.$class.'.php';
	}
        
        //skusi managerov
	if(!file_exists($require_file))
	{
		$require_file = $register_namespaces[$namespace].'/manager/'.$class.'.php';
	}
	
	//swift autoload
        
        /*
	$path = SWIFT_CLASS_DIRECTORY . '/' . str_replace('_', '/', $class) . '.php';
	if (file_exists($path))
	{
		require_once $path;
	}
        */
        
       /*
        
        // base directory for the namespace prefix
        $fbDir = LIB_DIR.'/Facebook';
        
        $fbPrefix = 'Facebook\\';
        
        // does the class use the namespace prefix?
        $len = strlen($fbPrefix);
        if (strncmp($fbPrefix, $class, $len) !== 0) {
            // no, move to the next registered autoloader
           //return;
        }
        else 
        {
             //echo 'load'. $relativeClass = substr($class, $len);
             //echo '<br />';
        }
        
        // get the relative class name
        $relativeClass = substr($class, $len);

        // replace the namespace prefix with the base directory, replace namespace
        // separators with directory separators in the relative class name, append
        // with .php
        echo 'load fb file '. $fb_file = rtrim($fbDir, '/') . '/' . str_replace('\\', '/', $relativeClass) . '.php';
        echo '<br />';
        // if the file exists, require it
        if (file_exists($fb_file)) {
            require_once $fb_file;
        }
        */

	if(!file_exists($require_file))
	{
		//throw new Exception("Unable to load $class.");
		//echo 'Failed autoload';
	}
	else
	{
		require_once($require_file);
	}
}

spl_autoload_register('fb_autoload');
//spl_autoload_register('google_autoload');
spl_autoload_register('gopay_autoload');
spl_autoload_register('core_autoload');

require_once (LIB_DIR . "/Underscore/underscore.php");
require_once (LIB_DIR . "/Core/Types/StringUtils.php");
require_once (LIB_DIR . "/Core/Pipe.php");
require_once (LIB_DIR . "/Core/WebApp.class.php");
require_once (LIB_DIR . "/Core/DbQuery.class.php");
require_once (LIB_DIR . "/Core/ServiceLayer.php");
require_once (LIB_DIR . "/Core/DbStorage.php");
require_once (LIB_DIR . "/Core/Repository.php");
require_once (LIB_DIR . "/Core/Layout.class.php");
require_once (LIB_DIR . "/Core/Router.class.php");
require_once (LIB_DIR . "/Core/AclException.php");
require_once (LIB_DIR . "/Core/ACL.php");
require_once (LIB_DIR . "/Core/Security.php");
require_once (LIB_DIR . "/Core/IdentityProvider.php");
require_once (LIB_DIR . "/Core/UserIdentity.php");
require_once (LIB_DIR . "/Core/Logger.php");
require_once (LIB_DIR . "/Core/Translator.class.php");
require_once (LIB_DIR . "/Core/Localizer.php");
require_once (LIB_DIR . "/Core/Request.class.php");
require_once (LIB_DIR . "/Core/Module.class.php");
require_once (LIB_DIR . "/Core/Controller.php");
require_once (LIB_DIR . "/Core/RestController.php");
require_once (LIB_DIR . "/Core/RestApiController.php");
require_once (LIB_DIR . "/Core/ControllerResponse.php");
require_once (LIB_DIR . "/Core/Form.php");
require_once (LIB_DIR . "/Core/FormHandler.php");
require_once (LIB_DIR . "/Core/Validator.php");
require_once (LIB_DIR . "/Core/Mailer.class.php");
require_once (LIB_DIR . "/Core/EntityMapper.php");
require_once (LIB_DIR . "/Core/Seo.php");
require_once (LIB_DIR . "/Core/MailChimp.php");
require_once (LIB_DIR . "/Core/DataTable.class.php");
require_once (LIB_DIR . "/Core/AdjacencyTree.php");
require_once (LIB_DIR . "/Core/Manager.php");
require_once (LIB_DIR . "/Core/ActiveTeamManager.php");
require_once (LIB_DIR . "/Core/ImageTransform.php");
require_once (LIB_DIR . "/Core/String.php");
require_once (LIB_DIR . "/phpThumb/ThumbLib.inc.php");
require_once (LIB_DIR . "/Core/GUID.php");
require_once (LIB_DIR . "/Core/Types/Enum.php");
require_once (LIB_DIR . "/Core/Types/DateTimeEx.php");
require_once (LIB_DIR . "/Core/Types/DefaultValueAccessor.php");
require_once (LIB_DIR . "/Core/Collections/HashSet.php");
require_once (LIB_DIR . "/Core/Collections/Queue.php");
require_once (LIB_DIR . "/Core/Collections/Iter.php");
require_once (LIB_DIR . "/Core/Mappers/GetterMapper.php");
require_once (LIB_DIR . "/Core/Mappers/ObjectListMapper.php");
require_once (LIB_DIR . "/WideImage/WideImage.php");
require_once (LIB_DIR . "/Latte/latte.php");
require_once (LIB_DIR . "/Core/Images/ImageMarginFix.php");
require_once (LIB_DIR . "/Core/Templates/Regions.php");

require_once LIB_DIR . "/Core/Events/EventSubject.php";
require_once LIB_DIR . "/Core/Events/EventInfo.php";

require_once (LIB_DIR . "/NotORM/NotORM.php");
require_once (LIB_DIR . "/Core/Db/Db.php");

require_once (LIB_DIR . "/Core/Types/DateNames.php");
require_once (LIB_DIR . "/Core/SportNames.php");

require_once (LIB_DIR . "/Core/Activity/Log.php");
require_once (LIB_DIR . "/Core/Activity/DbLoggerStore.php");
require_once (LIB_DIR . "/Core/Activity/LoggerRepository.php");
require_once (LIB_DIR . "/Core/Activity/ActivityLogger.php");
require_once (LIB_DIR . "/Core/Activity/Activity.php");

require_once (MODUL_DIR . "/notification/lib/NotificationTemplate.php");

require_once LIB_DIR . "/Core/Privacy/Privacy.php";

require_once (LIB_DIR . "/Core/Permissions/PermissionRepository.php");
require_once (LIB_DIR . "/Core/Permissions/PermissionManager.php");

require_once (LIB_DIR . "/Core/Packages/PackageRepository.php");
require_once (LIB_DIR . "/Core/Packages/PackageManager.php");
require_once (LIB_DIR . "/Core/Packages/ActualPackageRepository.php");
require_once (LIB_DIR . "/Core/Packages/ActualPackage.php");
require_once (LIB_DIR . "/Core/Packages/ActualPackageManager.php");

require_once (LIB_DIR . "/GetResponse/Api.php");
require_once (LIB_DIR . "/Tournament/Api.php");

\Core\DbQuery::createConnection(DB_HOST,DB_NAME,DB_USER,DB_PASS);
\Core\DbQuery::query("SET NAMES utf8;");

//default services

Core\ServiceLayer::addService('Db', [
    'class' => '\NotORM',
    'params' => [ Core\DbQuery::getDbHandler() ]
]);

Core\ServiceLayer::addService('LoggerRepository', [
    'class' => 'Core\Activity\LoggerRepository',
    'params' => [ new Core\Activity\DbLoggerStore() ]
]);

Core\ServiceLayer::addService('ActivityLogger', [
    'class' => 'Core\Activity\ActivityLogger',
    'params' => [ Core\ServiceLayer::getService('LoggerRepository') ]
]);

Core\ServiceLayer::addService('Activity', [
    'class' => 'Core\Activity\Activity',
    'params' => [ Core\ServiceLayer::getService('LoggerRepository') ]
]);

Core\ServiceLayer::addService('request',array(
    'class' => "Core\Request",
));

Core\ServiceLayer::addService('acl',array(
    'class' => "Core\ACL",
));

Core\ServiceLayer::addService('security',array(
    'class' => "Core\Security",
    'params' => ["\User\Model\UserIdentityProvider"]
));

Core\ServiceLayer::addService('translator',array(
    'class' => "Core\Translator"
));
Core\ServiceLayer::addService('localizer',array(
    'class' => "Core\Localizer"
));


Core\ServiceLayer::addService('router',array(
    'class' => "Core\Router",
));

Core\ServiceLayer::addService('layout',array(
    'class' => "Core\Layout",
    'singleton' => true
));

Core\ServiceLayer::addService('user_logger',array(
    'class' => "Core\Logger",
    'params' => array(
        new \Core\DbStorage('ph_user_logs'),
    )
));

Core\ServiceLayer::addService('partner_logger',array(
    'class' => "Core\Logger",
    'params' => array(
        new \Core\DbStorage('ph_partner_logs'),
    )
));

Core\ServiceLayer::addService('system_logger',array(
    'class' => "Core\Logger",
    'params' => array(new \Core\DbStorage('system_log'))
));

Core\ServiceLayer::addService('mailer',array(
    'class' => "Core\Mailer",
));

Core\ServiceLayer::addService('imageTransform',array(
    'class' => "Core\ImageTransform",
));

Core\ServiceLayer::addService('seo',array(
    'class' => "Core\Seo",
));

Core\ServiceLayer::addService('DateNames', [
    'class' => 'Core\DateNames',
    'params' => []
]);

Core\ServiceLayer::addService('SportNames', [
    'class' => 'Core\SportNames',
    'params' => []
]);

Core\ServiceLayer::addService('GetterMapper', [
    'class' => 'Core\Mappers\GetterMapper',
    'params' => []
]);

Core\ServiceLayer::addService('ActiveTeamManager', [
    'class' => 'Core\ActiveTeamManager',
    'params' => [ Core\ServiceLayer::getService('GetterMapper') ]
]);

Core\ServiceLayer::addService('NotificationBlacklist', [
    'class' => 'Core\Notifications\NotificationBlacklist',
    'params' => []
]);

Core\ServiceLayer::addService('PermissionManager', [
    'class' => 'Core\Permissions\PermissionManager',
    'params' => [ new Core\Permissions\PermissionRepository() ]
]);

Core\ServiceLayer::addService('PlayerPrivacy', [
    'class' => 'Core\PlayerPrivacy',
    'params' => []
]);

Core\ServiceLayer::addService('PackageRepository', [
    'class' => 'CR\Pay\Model\PackageRepository',
    'params' => []
]);

Core\ServiceLayer::addService('PackageManager', [
    'class' => 'CR\Pay\Model\PackageManager',
    'params' => [ Core\ServiceLayer::getService('PackageRepository') ]
]);

Core\ServiceLayer::addService('ActualPackageRepository', [
    'class' => 'CR\Pay\Model\ActualPackageRepository',
    'params' => [ Core\ServiceLayer::getService('PackageManager') ]
]);

Core\ServiceLayer::addService('ActualPackageManager', [
    'class' => 'CR\Pay\Model\ActualPackageManager',
    'params' => [
        Core\ServiceLayer::getService('ActiveTeamManager'),
        Core\ServiceLayer::getService('ActualPackageRepository')
    ]
]);

Core\ServiceLayer::addService('MailchimpManager',array(
'class' => "Core\MailChimp",
    'params' => array('a41b5c7a6e0f9d962bcad107ca1e2941-us13')
	
));


//translator switcher
if('yes' ==  Core\ServiceLayer::getService('request')->get('live_translate'))
{
    $_SESSION['translate_mode'] = true; 
}

if('no' ==  Core\ServiceLayer::getService('request')->get('live_translate'))
{
    $_SESSION['translate_mode'] = false; 
}

//analutics switch

if('yes' ==  Core\ServiceLayer::getService('request')->get('dev'))
{
    setcookie('dev',true,time()+(60*60*24*365),'/');
}


if('04114015279256545061' ==  Core\ServiceLayer::getService('request')->get('sam'))
{
   $_SESSION['super_admin_mode'] = true;
}


$layout = Core\ServiceLayer::getService('layout');
$layout->setTemplate(GLOBAL_DIR.'/templates/BaseLayout.php');