<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="apple-touch-icon" href="http://app.coachrufus.com/apple_icon.png">
        <title>Coach Rufus</title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,700|Roboto:400,700&amp;subset=cyrillic" rel="stylesheet">

        <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="/css/bootstrap/css/font-awesome.min.css">

        <?php echo $layout->addStylesheet('css/webview.css') ?>
      
        <?php echo $layout->insertStylesheetLink() ?>
         <script src="/dev/plugins/jQuery/jQuery-2.1.4.min.js"></script>
          <script src="/dev/js/jquery-ui.min.js"></script>
          <script src="/dev/theme/bootstrap/js/bootstrap.min.js"></script>

    </head>
     <?php //$layout->includePart(GLOBAL_DIR.'/templates/parts/_analytics.php') ?>

    <!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
    <body class="platform_<?php echo $_SESSION['api']['platform'] ?> <?php echo $body_class ?> ">
         <div class="platform_check" style="background:<?php echo $platformColor ?>">&nbsp;</div>
        <?php echo $module_content ?>
         <?php echo $layout->insertJavascript() ?>
         <?php echo $layout->insertSlot('javascript') ?>
    </body>
</html>
