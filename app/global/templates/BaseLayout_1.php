<?php 

$security = Core\ServiceLayer::getService('security');
$router = Core\ServiceLayer::getService('router');
$user = $security->getIdentity()->getUser();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/favicon.ico">

    <title>Home</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="/css/dist/css/skins/_all-skins.min.css">
    

    <link href="/dev/css/jquery-ui.css" rel="stylesheet">
    <link href="/dev/css/main.css" rel="stylesheet">
   
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body  class="hold-transition skin-blue layout-top-nav">
<div class="container">
       <header class="header">
         
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-default" role="navigation">
                 <div class="container-fluid">
               <ul class="nav navbar-nav">
                 
                    <?php if(null != $user): ?>
                     <li><a href="<?php echo $router->link('base_home') ?>">Home</a></li>
                     <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $translator->translate('Playgrounds manager') ?> <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a  href="<?php echo  $router->link('playgrounds_overview_all') ?>"><?php echo $translator->translate('My Playgrounds') ?></a> </li>
                            <li><a  href="<?php echo  $router->link('locality_create') ?>"><?php echo $translator->translate('Create playground') ?></a> </li>
                        </ul>
                      </li>
                      
                      
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $translator->translate('Team manager') ?> <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                           <li><a  href="<?php echo  $router->link('teams_overview') ?>"><?php echo $translator->translate('My Teams') ?></a> </li>
                          <li><a  href="<?php echo  $router->link('team_create') ?>"><?php echo $translator->translate('Create team') ?></a> </li>
                        </ul>
                      </li>
                      
                   
                    <?php endif; ?>
                   
                  <!--
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Generator <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                          <li><a href="<?php echo $router->link('generate_modul') ?>">Module</a></li>
                          <li><a href="<?php echo $router->link('generate_entity') ?>">Entity</a></li>
                          <li><a href="<?php echo $router->link('generate_form') ?>">Form</a></li>
                          <li><a href="<?php echo $router->link('generate_view') ?>">Views</a></li>
                        </ul>
                      </li>
                  -->
            </ul>
                     
                      
                <div class="navbar-right">
                	
                    <ul class="nav navbar-nav">
                        

                       <?php if(null != $user): ?>
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>
                                <span> <?php echo $user->getName() ?>  <?php echo $user->getSurname() ?> <?php echo $user->getEmail() ?> <i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                               
                               
                                <!-- Menu Footer-->
                              
                                <?php if($security->userCanChangePassword($user)): ?>
                                     <li>
                                        <a href="<?php echo $router->link('password_change')?>" class="btn btn-default btn-flat"><?php echo  $translator->translate('Zmena hesla') ?></a>
                                   </li>
                                <?php endif; ?>
                               
                                <li>
                                     <a href="<?php echo $router->link('profil')?>" class="btn btn-default btn-flat"><?php echo  $translator->translate('Profil') ?></a>
                                </li>
                                <li>
                                      <a href="<?php echo $router->link('logout')?>" class="btn btn-default btn-flat"><?php echo  $translator->translate('Odhlásiť') ?></a>
                                </li>
                            </ul>
                        </li>
                         <?php else: ?>
                         <li><a href="<?php echo $router->link('login') ?>">Login</a></li>
                        <li><a href="<?php echo $router->link('register',array('lang' => 'sk')) ?>">Register</a></li>
                        
                         <?php endif; ?>
                    </ul>
                </div>
                    
                 </div>
                
                
            </nav>
              <?php if(null != $user): ?>
        <ul class="breadcrumb">
            <li><a href="/">Home</a></li>
             <?php echo $layout->insertSlot('crumb') ?>
        </ul>
          <?php endif; ?>
        </header>
 
 </div><!-- /.container -->
 
    <div class="container">
         
       
       
        
        <?php echo $module_content ?>
        
       
        
    </div><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="/js/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <script src="/dev/js/jquery-ui.min.js"></script>
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <script src="/dev/theme/bootstrap/js/bootstrap.min.js"></script>
    
    <script src="/dev/js/main.js?ver=<?php echo VERSION ?>"></script>
    
    
     <?php echo $layout->insertSlot('javascript') ?>
  </body>
</html>



