<?php
$templateParams = $layout->getTemplateParameters();
?>
<!DOCTYPE html>
<html>
     <head>
   <?php $layout->includePart(GLOBAL_DIR.'/templates/parts/_head.php') ?>
         <?php echo $layout->insertSlot('stylesheet') ?>
          </head>
          
  <!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
  <body class="skin-blue layout-top-nav <?php echo (is_array($templateParams) && array_key_exists('body-class', $templateParams)) ? $templateParams['body-class'] : '' ?>">
       <?php $layout->includePart(GLOBAL_DIR.'/templates/parts/_analytics.php') ?>
    <div class="wrapper">
        
      <div class="content-wrapper">
          <div class="row">
              <div class="col-md-12">
                <?php echo $module_content ?>
              </div>
             
          </div>

      </div><!-- /.content-wrapper -->
    </div><!-- ./wrapper -->

    <script src="/js/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <script src="/dev/js/jquery-ui.min.js"></script>
    <script src="/dev/theme/bootstrap/js/bootstrap.min.js"></script>
    <script src="/js/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <script src="/js/plugins/fastclick/fastclick.min.js"></script>
     <script src="/dev/js/app.min.js"></script>
  
    <script src="/dev/plugins/iCheck/icheck.min.js"></script>
    <script src="/dev/js/main.js?ver=<?php echo VERSION ?>"></script>
    
    
   
    
    <?php echo $layout->insertSlot('javascript') ?>
    
  </body>
</html>
