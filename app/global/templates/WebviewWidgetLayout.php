<?php 
$platformColor = '#ffffff';

if($_SESSION['api']['platform']  == 'iphone')
{
    $platformColor = 'green';
}

if($_SESSION['api']['platform']  == 'android')
{
    $platformColor = 'red';
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="apple-touch-icon" href="http://app.coachrufus.com/apple_icon.png">
        <title>Coach Rufus</title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,700|Roboto:400,700&amp;subset=cyrillic" rel="stylesheet">
        <link media="all" rel="stylesheet" href="/dev/theme/bootstrap/css/bootstrap.grid.css" />
      
        <?php echo $layout->insertStylesheetLink() ?>
        
    </head>
<?php //$layout->includePart(GLOBAL_DIR.'/templates/parts/_analytics.php') ?>
    <!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
    <body class="platform_<?php echo $_SESSION['api']['platform'] ?>">
        <div class="platform_check" style="display:none;background:<?php echo $platformColor ?>">&nbsp;</div>
        <?php echo $module_content ?>
         <?php echo $layout->insertJavascript() ?>
         <?php echo $layout->insertSlot('javascript') ?>
    </body>
</html>
