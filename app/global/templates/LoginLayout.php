<!DOCTYPE html>
<html>
    <head>
  <?php $layout->includePart(GLOBAL_DIR.'/templates/parts/_head.php') ?>
        <link rel="stylesheet" href="/dev/plugins/iCheck/square/grey_12.css">
    </head>
  <body class="hold-transition login-page">
       <?php $layout->includePart(GLOBAL_DIR.'/templates/parts/_analytics.php') ?>
       <?php echo $module_content ?>
      
   

    <script src="/js/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <script src="/dev/js/jquery-ui.min.js"></script>
    <script src="/dev/theme/bootstrap/js/bootstrap.min.js"></script>
    <script src="/dev/plugins/iCheck/icheck.min.js"></script>
    
    <script>
        
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-grey',
          radioClass: 'iradio_square-grey'
         
        });
      });
      
    </script>
     <?php echo $layout->insertJavascript() ?>
      <?php echo $layout->insertSlot('javascript') ?>
    
      
  </body>
</html>
