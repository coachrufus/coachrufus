<?php
$routeName = $router->getCurrentRouteName();
$mobileAppTitle = $translator->translate($layout->getMobileAppTitle($router->getCurrentRouteName()));

if('team_share_link' == $routeName)
{
    $body_class = 'login-page';
}  

$platformColor = '#ffffff';

if($_SESSION['api']['platform']  == 'iphone')
{
    $platformColor = 'green';
}

if($_SESSION['api']['platform']  == 'android')
{
    $platformColor = 'red';
}

$backUrl = '';
switch ($routeName) {
    case 'tournament_dashboard':
        $backUrl = $router->link('tournament');
        break;
    case 'tournament_detail':
        $backUrl = $router->link('tournament_dashboard');
        break;
    case 'tournament_settings':
        $backUrl = $router->link('tournament_dashboard');
        break;
    case 'tournament_public_detail':
        $backUrl = $router->link('tournament_dashboard');
        break;
    case 'tournament_share_link':
        $backUrl = $router->link('tournament_dashboard');
        break;

    default:
        $backUrl = 'history';
        break;
}


?>
<!DOCTYPE html>
<html>
     <head>
        <?php $layout->includePart(GLOBAL_DIR.'/templates/parts/_head.php') ?>
         <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
          <?php echo $layout->insertStylesheetLink() ?>
         <?php echo $layout->insertSlot('stylesheet') ?>
          
    </head>
          
  <!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
  <body class="skin-blue layout-top-nav webview-content-layout platform_<?php echo $_SESSION['api']['platform'] ?> <?php echo ($request->get('iframe') == 'yes') ? 'iframe-webview-content-layout' : ''  ?> <?php echo $body_class ?>">
      
      <div class="platform_check" style="background:<?php echo $platformColor ?>">&nbsp;</div>
      
    
       <?php $layout->includePart(GLOBAL_DIR.'/templates/parts/_analytics.php') ?>
    <div class="wrapper">
        


                  <?php if($routeName != 'api_content_attendance' && $routeName != 'team_season_billing' && 'team_players_attendance' != $routeName &&  $routeName != 'team_player_stats' && 'widget_list' != $routeName && 'api_content_stats_graphs' != $routeName  && 'api_content_stats_personal' != $routeName ): ?>
                   <div class="mobile-app-nav" >
                      <div class="navBtn" data-url="<?php echo $backUrl ?>"></div>
                      <?php echo $mobileAppTitle ?>
                  </div>
                  <?php endif; ?>
               
                  
                  
                <?php echo $module_content ?>
              

     
    </div><!-- ./wrapper -->

    <script src="/js/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <script src="/dev/js/jquery-ui.min.js"></script>
    <script src="/dev/theme/bootstrap/js/bootstrap.min.js"></script>
    <script src="/js/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <script src="/js/plugins/fastclick/fastclick.min.js"></script>
     <script src="/dev/js/app.min.js"></script>
  
    <script src="/dev/plugins/iCheck/icheck.min.js"></script>
    <script src="/dev/js/main.js?ver=<?php echo VERSION ?>"></script>
    
     <script>
        <?php if(is_null($identity->getUser())): ?>
        window.userId = null;
        <?php else: ?>
        window.userId = <?php echo $identity->getUser()->getId() ?>;
        <?php endif; ?>
        window.root = '<?php echo $root ?>';
        window.lang = '<?php echo $translator->getLang(); ?>';
        window.activeTeam = <?php echo json_encode($activeTeam); ?>;
        window.PgKey = 'PRO';
    </script>
   
    
    <?php echo $layout->insertJavascript() ?>
     <?php echo $layout->insertSlot('javascript') ?>
    
    <iframe id="HiddenIframe" style="width:0; height:0; border:0; border:none;"></iframe>
	<script>
            
            
      
            window.fbAsyncInit = function () {
            FB.init({
                appId: '<?php echo FB_APP_ID ?>',
                xfbml: true,
                version: 'v2.4'
            });
            };

            (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {
                return;
            }
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
    
    
    
    
            window.shareCallback = function(src)
            {
                $('#share-loader').show();
                $("#HiddenIframe").attr("src", src);
            };
            
            
            $(document).keyup(function(e) {
                if (e.keyCode == 27) { 
                   $('#share-loader').hide();
               }
           });
            
	</script>

    <?php if(true || $_SERVER['SERVER_ADDR'] != '127.0.0.1'): ?>
    <script src="/dev/js/Modules/bundle/index.js?v=<?php echo VERSION ?>"></script>
    <?php endif; ?>
  </body>
</html>
