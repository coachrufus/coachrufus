<?php
$templateParams = $layout->getTemplateParameters();
?>
<!DOCTYPE html>
<html>
     <head>
   <?php $layout->includePart(GLOBAL_DIR.'/templates/parts/_head.php') ?>
         
   </head>
   
  <!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
  <body class="hold-transition skin-black-light sidebar-mini <?php echo (array_key_exists('sidebar', $_COOKIE) && $_COOKIE['sidebar'] == 'close') ? 'sidebar-collapse' : '' ?> <?php echo (is_array($templateParams) && array_key_exists('body-class', $templateParams)) ? $templateParams['body-class'] : '' ?>">
    <div id="share-loader">
        <i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><br />
        <?php echo $translator->translate('Generating share content...') ?>
    </div>
      
       <?php $layout->includePart(GLOBAL_DIR.'/templates/parts/_analytics.php') ?>
    <div class="wrapper">
        
   <?php $layout->includePart(GLOBAL_DIR.'/templates/parts/_header.php') ?>
        
      <div class="content-wrapper">
          <div class="row">
              <div class="col-md-12">
                <?php echo $module_content ?>
              </div>
             
          </div>

      </div><!-- /.content-wrapper -->
      <?php $layout->includePart(GLOBAL_DIR.'/templates/parts/_footer.php') ?>
      <?php $layout->includePart(GLOBAL_DIR.'/templates/parts/_right_sidebar.php') ?>
    </div><!-- ./wrapper -->
    <?php $layout->includePart(MODUL_DIR . '/base/view/Translator/_modal.php') ?>
    <?php $layout->includePart(MODUL_DIR . '/base/view/Base/_alert_error_modal.php') ?>
    <?php $layout->includePart(MODUL_DIR . '/Pay/view/Pay/_credit_modal.php') ?>
    <?php $layout->includePart(MODUL_DIR . '/Gamification/view/alert/_skip_modal.php') ?>

    
    <script src="/dev/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <script src="/dev/js/jquery-ui.min.js"></script>
    <script src="/dev/theme/bootstrap/js/bootstrap.min.js"></script>
    <script src="/js/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <script src="/js/plugins/fastclick/fastclick.min.js"></script>
    <script type="text/javascript" src="<?php echo GOPAY_URL ?>"></script>
    <script>
        <?php if(is_null($identity->getUser())): ?>
        window.userId = null;
        <?php else: ?>
        window.userId = <?php echo $identity->getUser()->getId() ?>;
        <?php endif; ?>
        window.root = '<?php echo $root ?>';
        window.lang = '<?php echo $translator->getLang(); ?>';
        window.activeTeam = <?php echo json_encode($activeTeam); ?>;
        window.PgKey = 'PRO';
    </script>
    </script>
    <?php echo $layout->addJavascript('js/app.js') ?>
    <script src="/dev/plugins/iCheck/icheck.min.js"></script>
    <?php echo $layout->addJavascript('js/main.js') ?>
    <?php echo $layout->addJavascript('js/PaymentManager.js') ?>
    
    <?php if (!is_null($activeTeam)): ?>
    <link rel="stylesheet" type="text/css" href="/dev/js/Modules/css/socializing.css"></link>
    <?php endif; ?>
    <?php echo $layout->insertJavascript() ?>
    <?php echo $layout->insertSlot('javascript') ?>

	<iframe id="HiddenIframe" style="width:0; height:0; border:0; border:none;"></iframe>
	<script>
            
            
            var paymentManager = new PaymentManager();
            paymentManager.bindTriggers();
            
            window.fbAsyncInit = function () {
            FB.init({
                appId: '<?php echo FB_APP_ID ?>',
                xfbml: true,
                version: 'v2.4'
            });
            };

            (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {
                return;
            }
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
    
    
    
    
            window.shareCallback = function(src)
            {
                $('#share-loader').show();
                $("#HiddenIframe").attr("src", src);
            };
            
            
            $(document).keyup(function(e) {
                if (e.keyCode == 27) { 
                   $('#share-loader').hide();
               }
           });
            
	</script>

    <?php if(true || $_SERVER['SERVER_ADDR'] != '127.0.0.1'): ?>
    <script src="/dev/js/Modules/bundle/index.js?v=<?php echo VERSION ?>"></script>
    <?php endif; ?>
  
  </body>
</html>
