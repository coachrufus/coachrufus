<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="apple-touch-icon" href="http://app.coachrufus.com/apple_icon.png">
    <title>Rufus <?php echo $layout->insertSlot('title') ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    
     <?php echo $layout->insertSlot('meta') ?>
    
    <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css">
   
    <!-- Font Awesome -->
    <link rel="stylesheet" href="/css/bootstrap/css/font-awesome.min.css">
    <link rel="stylesheet" href="/dev/plugins/select2/select2.css">

    <?php $layout->addStylesheet('theme/adminlte/AdminLTE.css') ?>
    <?php //$layout->addStylesheet('theme/adminlte/skins/skin-black-light.css') ?>
    
    <?php $layout->addStylesheet('css/main.css') ?>
	<?php $layout->addStylesheet('css/share/share.css') ?>
    
    <?php $layout->addStylesheet('theme/coachrufus/layout.css') ?>
    <?php $layout->addStylesheet('theme/coachrufus/color.css') ?>
    
    <link rel="stylesheet" href="/dev/css/jquery-ui.css">
    <link rel="stylesheet" href="/dev/plugins/iCheck/flat/green.css">
     <?php echo $layout->insertSlot('stylesheet') ?>
     <?php echo $layout->insertStylesheetLink() ?>
    
    
  

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
