<ol class="breadcrumb">
    <li><a href="<?php echo $router->link('player_dashboard') ?>"> <?php echo $translator->translate('Dashboard') ?></a></li>
    <?php foreach($crumbs as $name => $route): ?>
    <li><a href="<?php echo $route ?>"><?php echo $translator->translate($name) ?></a></li>
    <?php endforeach; ?>
</ol>

<?php  $layout->startSlot('title') ?>
<?php $crumbs = array_reverse ($crumbs) ?>
 <?php foreach($crumbs as $name => $route): ?>
   <?php echo $translator->translate($name) ?> - 
    <?php endforeach; ?>

<?php  $layout->endSlot('title') ?>
