<?php 
$security = Core\ServiceLayer::getService('security');
$activeTeam = Core\ServiceLayer::getService('ActiveTeamManager')->getActiveTeam();
$userIdentity = $security->getIdentity();
$user = $userIdentity->getUser();

$customAnalytics = array();
if(null != $user)
{
    //$customAnalytics['dimension1'] = $user->getEmail() ;
    $customAnalytics['dimension6'] = $user->getGender() ;
}

if(null != $activeTeam)
{
    $teams = $userIdentity->getUserTeams();
    $customAnalytics['dimension3'] = $activeTeam['id'];
    $currentTeam = $teams[$activeTeam['id']];
    if(null != $currentTeam)
    {
        $customAnalytics['dimension5'] =  $currentTeam->getName();
    }
    
    $teamRoles = $userIdentity->getTeamRoles();
    $customAnalytics['dimension2'] = $teamRoles[$activeTeam['id']]['role'];
    
}

$customAnalyticsJson =  json_encode($customAnalytics);


?>

<?php if('prod' == RUN_ENV): ?>


<script>
 <?php if(!empty($customAnalyticsJson)): ?>
     dataLayer = [<?php echo $customAnalyticsJson ?>];
<?php endif; ?>
</script>

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-MSFTF5"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-MSFTF5');</script>
<!-- End Google Tag Manager -->




<?php endif; ?>