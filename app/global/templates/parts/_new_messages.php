<?php
$security = Core\ServiceLayer::getService('security');
$user = $security->getIdentity()->getUser();
$messageManager = Core\ServiceLayer::getService('MessageManager');
$messagesInfo = $messageManager->getRecipientMessagesInfo($user);
$messages = $messageManager->getUnreadMessages($user,4);
?>

<li class="dropdown  message-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="ico ico-envelope"></i>
                  <?php if($messagesInfo['unread']> 0): ?><span class="label label-success"><?php echo $messagesInfo['unread'] ?></span><?php  endif; ?>
                </a>
                <ul class="dropdown-menu">
                  <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="message-dropdown">
                        <?php foreach($messages as $message): ?>
                        <li><!-- start message -->
                        <a href="<?php echo $router->link('message_detail',array('mid' => $message->getId())) ?>">
                        
                          <h4>
                          <?php echo $message->getSenderName(); ?>
                            <small><?php echo $message->getCreatedAt()->format('d.m.Y H:i:s')  ?></small>
                          </h4>
                          <p><?php echo $message->getSubject()  ?></p>
                        </a>
                      </li><!-- end message -->
                        <?php endforeach; ?>
                    </ul>
                  </li>
                  <li class="footer"><a href="<?php echo $router->link('message_user_list') ?>"><?php echo $translator->translate('See all messages') ?></a></li>
                </ul>
              </li>
              <!-- Notifications: style can be found in dropdown.less -->
              