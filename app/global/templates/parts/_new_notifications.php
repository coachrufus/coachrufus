<?php
$security = Core\ServiceLayer::getService('security');
$user = $security->getIdentity()->getUser();
$notifyManager = Core\ServiceLayer::getService('SystemNotificationManager');
$notifyInfo = $notifyManager->getNotificationsInfo($user);
$notifications = $notifyManager->getReadSystemNotifications($user, 4);
?>

<li class="dropdown notifications-menu">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <i class="ico ico-bell"></i>

        <?php if ($notifyInfo['unread'] > 0): ?> <span class="label label-warning"><?php echo $notifyInfo['unread'] ?></span><?php endif; ?>
    </a>
    <ul class="dropdown-menu">
        <?php foreach ($notifications as $message): ?>
            <li><!-- start message -->
                 <a href="<?php echo $router->link('system_notify_redirect',array('mid' => $message->getId() )) ?>">
                    <p><?php echo $message->getSubject() ?></p>
                    <p><?php echo $message->getTruncatedBody() ?></p>
                     <small><?php echo $message->getCreatedAt()->format('d.m.Y H:i:s') ?></small>
                </a>
            </li><!-- end message -->
        <?php endforeach; ?>
        <li class="footer"><a href="<?php echo $router->link('system_notify_list') ?>"><?php echo $translator->translate('View all') ?></a></li>
    </ul>
</li>
<!-- Tasks: style can be found in dropdown.less -->