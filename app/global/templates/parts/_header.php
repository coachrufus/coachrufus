<?php
$security = Core\ServiceLayer::getService('security');
$router = Core\ServiceLayer::getService('router');
$user = $security->getIdentity()->getUser();
$teamManager = Core\ServiceLayer::getService('TeamManager');
$teams = $teamManager->getUserTeams($user);
$sportManager = Core\ServiceLayer::getService('SportManager');
$sportEnum = $sportManager->getSportFormChoices();

$menuClass = $layout->buildMenuClass();

?>
<header class="main-header">

    <a href="/" class="logo">
        <span class="logo-mini"> <img src="/theme/img/logo.png" title="<?php echo $translator->translate("It's game time") ?>" alt="Coach Rufus"></span>
        <span class="logo-lg">
            <img style="max-height:45px;" src="/theme/img/logo_long.svg?ver=1" onerror="this.onerror=null; this.src=/theme/img/logo_long.png'" title="<?php echo $translator->translate("It's game time") ?>" alt="Coach Rufus">
        </span>
    </a>



    <nav class="navbar navbar-static-top">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>

        </a>
        <a href="/"><img class="logo_760" src="/img/main/logo_sm.png" title="<?php echo $translator->translate("It's game time") ?>" alt="Coach Rufus" /></a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?php echo $user->getMidPhoto() ?>" class="user-image" alt="User Image">
                        <span class="hidden-xs"> <?php echo $user->getName() ?>  <?php echo $user->getSurname() ?> </span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li><a href="<?php echo $router->link('profil') ?>"><i class="ico ico-user-profile"></i><?php echo $translator->translate('Profile') ?></a></li>
                        <li><a href="<?php echo $router->link('logout') ?>"><i class="ico ico-logout"></i><?php echo $translator->translate('Sign out') ?></a></li>
                    </ul>
                </li>
                <?php $layout->includePart(GLOBAL_DIR . '/templates/parts/_new_messages.php') ?>
                <?php $layout->includePart(GLOBAL_DIR . '/templates/parts/_new_notifications.php') ?>
            </ul>
        </div>
        
        <?php if($user->getMobileAppDevice() != 1): ?>
        <div class="mobile_app_promo">
            <a onclick="window.open(this,'_blank');return false;" href="https://play.google.com/store/apps/details?id=com.altamira.rufus">
                <img class="mobile_app_promo_lg" src="/theme/img/rufus_badge_sk_googleplay_<?php echo $user->getDefaultLang() ?>.svg" alt="" title="" />
                <img class="mobile_app_promo_sm" src="/theme/img/bagde_mob_googleplay.svg" alt="" title="" />
            </a>
            <a  onclick="window.open(this,'_blank');return false;" href="https://itunes.apple.com/us/app/rufus-your-favourite-teammate/id1435067809">
                <img class="mobile_app_promo_lg" src="/theme/img/rufus_badge_sk_ios_<?php echo $user->getDefaultLang() ?>.svg" alt="" title="" />
                <img class="mobile_app_promo_sm" src="/theme/img/bagde_mob_ios.svg" alt="" title="" />
            </a>
        </div>
        <?php endif; ?>
    </nav>
</header>

<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <ul class="sidebar-menu">
            <li class="menu-main"> <a class="<?php echo $menuClass['dashboard'] ?>" href="<?php  echo $router->link('player_dashboard', array('userid' => $user->getId())) ?>"><i class="ico ico-dashboard"></i><span><?php echo $translator->translate('Dashboard') ?></span></a></li>
            
            <li class="menu-main"> <a class="<?php echo $menuClass['scouting'] ?>" href="<?php echo $router->link('scouting') ?>"><i class="ico ico-scouting"></i><span><?php echo $translator->translate('Scouting') ?></span></a></li>

            <li class="treeview menu-main">
                <a class="link <?php echo $menuClass['teams_overview'] ?>" href="<?php echo $router->link('teams_overview') ?>">
                    <i class="ico ico-teams"></i>
                    <span class="treeview-menu-link"><?php echo $translator->translate('Teams') ?> (<?php echo count($teams)  ?>)</span>

                    <i class="ico ico-menu-plus pull-right treeview-menu-trigger"></i>
                    <i class="ico ico-menu-minus pull-right treeview-menu-trigger"></i>
                </a>
                <ul class="treeview-menu treeview-menu-team">

                    <?php $i=1; foreach ($teams as $team): ?>
                        <?php 
                        $teamMenuTitle =   $team->getName().' ('.$translator->translate($sportEnum[$team->getSportId()]);
                        
                        if(strlen($teamMenuTitle) > 20)
                        {
                             $teamMenuTitle = substr($teamMenuTitle,0,20).'...)';
                        }
                        else 
                        {
                            $teamMenuTitle = $teamMenuTitle.')';
                        }
                       
                        
                        ?>
                        <li class="<?php echo (count($teams) == $i) ? 'last' : '' ?><?php echo (1 == $i) ? 'first' : '' ?><?php echo (1 != $i && count($teams) != $i) ? 'middle' : '' ?>">
                            <a  href="<?php echo $router->link('team_overview', array('id' => $team->getId())) ?>" ><?php echo $teamMenuTitle ?></a>
                            
                        </li>
                    <?php $i++; endforeach; ?>

                    <li class="add_team">
                        <a href="<?php echo $router->link('team_create') ?>">
                            <i class="ico ico-menu-plus"></i> 
                            <span><?php echo $translator->translate('Create team') ?></span>
                        </a>
                    </li>
                    
              

                </ul>
            </li>
            <?php echo $layout->insertSlot('team_menu',array('sportEnum' => $sportEnum)) ?>
            <li class="header">&nbsp;</li>
            <li class="menu-main"> <a class="<?php echo $menuClass['tournament'] ?>" href="<?php echo $router->link('tournament') ?>"><i class="ico ico-team-playgrounds"></i><span><?php echo $translator->translate('Tournament') ?></span></a></li>
              
              <?php echo $layout->insertSlot('tournament_menu') ?>

           


        </ul>
    </section>
    <!-- /.sidebar -->
</aside>

<!-- =============================================== -->

