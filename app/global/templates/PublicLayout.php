<?php
$templateParams = $layout->getTemplateParameters();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title><?php echo $templateParams['title'] ?></title>
        
        <?php foreach($templateParams['meta'] as $metakey => $metaval): ?>
            <meta name="<?php echo $metakey ?>" content="<?php echo $metaval ?>" />
        <?php endforeach; ?>

        <!-- Bootstrap -->
        <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

        <?php $layout->addStylesheet('theme/bootstrap/css/font-awesome.min.css') ?>
        <?php $layout->addStylesheet('theme/bootstrap/css/bootstrap.min.css') ?>
        <?php $layout->addStylesheet('theme/adminlte/AdminLTE.css') ?>
        <?php $layout->addStylesheet('theme/adminlte/skins/skin-black-light.css') ?>
        <?php $layout->addStylesheet('/css/main.css') ?>
        
        <?php echo $layout->insertStylesheetLink() ?>
        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
     <body class="skin-black-light  layout-top-nav <?php echo (is_array($templateParams) && array_key_exists('body-class', $templateParams)) ? $templateParams['body-class'] : '' ?>">
        <div class="wrapper">


            <div class="content-wrapper">
            <div class="container">
                <section class="content">
            
             <?php echo $module_content ?>
         
          </section>
               
                </div>

            </div><!-- ./wrapper -->
      
             <script src="/dev/plugins/jQuery/jQuery-2.1.4.min.js"></script>
            <?php  $layout->addJavascript('plugins/jQueryUI/jquery-ui.min.js') ?>
            <?php  $layout->addJavascript('theme/bootstrap/js/bootstrap.min.js') ?>
            <?php echo $layout->insertJavascript() ?>
            <?php echo $layout->insertSlot('javascript') ?>
    </body>
</html>