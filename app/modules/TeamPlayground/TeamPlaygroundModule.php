<?php
namespace CR\TeamPlayground;
use Core\Module as Module;
class TeamPlaygroundModule extends Module
{
    public function __construct() 
    {
        $this->setBaseDir(MODUL_DIR.'/TeamPlayground');
        $this->setName('TeamPlayground');
    }
}

