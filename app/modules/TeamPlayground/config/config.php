<?php
namespace CR\TeamPlayground;
require_once(MODUL_DIR.'/TeamPlayground/TeamPlaygroundModule.php');


use Core\ServiceLayer as ServiceLayer;


$acl = ServiceLayer::getService('acl');


//routing
$router = ServiceLayer::getService('router');

$acl->allowRole('ROLE_USER','team_playground_list');
$router->addRoute('team_playground_list','/team-playground/list/:team_id',array(
		'controller' => 'CR\TeamPlayground\TeamPlaygroundModule:Crud:list'
));

$acl->allowRole('ROLE_USER','team_playground_create');
$router->addRoute('team_playground_create','/team-playground/create/:team_id',array(
		'controller' => 'CR\TeamPlayground\TeamPlaygroundModule:Crud:create'
));

$acl->allowRole('ROLE_USER','team_playground_edit');
$router->addRoute('team_playground_edit','/team-playground/edit/:team_id',array(
		'controller' => 'CR\TeamPlayground\TeamPlaygroundModule:Crud:edit'
));

$acl->allowRole('ROLE_USER','team_playground_delete');
$router->addRoute('team_playground_delete','/team-playground/delete/:team_id',array(
		'controller' => 'CR\TeamPlayground\TeamPlaygroundModule:Crud:delete'
));


$acl->allowRole('ROLE_USER','team_playground_rest_create');
$router->addRoute('team_playground_rest_create','/api/team-playground/create',array(
		'controller' => 'CR\TeamPlayground\TeamPlaygroundModule:Rest:create'
));




