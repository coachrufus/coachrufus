<?php

namespace CR\TeamPlayground\Controller;

use AclException;
use Core\Controller as Controller;
use Core\ServiceLayer;

class CrudController extends Controller {

     private function checkSettingTeamPermission($team)
        {
            if( !ServiceLayer::getService('security')->getIdentity()->hasTeamPermission('managePlaygrounds',$team))
            {
                 throw  new AclException();
            }
        }
    
    public function listAction()
    {
        $request = ServiceLayer::getService('request');
        $security = ServiceLayer::getService('security');
        $teamManager = ServiceLayer::getService('TeamManager');
        $team = $teamManager->findTeamById($request->get('team_id'));

       $this->checkSettingTeamPermission($team);
        ServiceLayer::getService('security')->getIdentity()->checkTeamAccess($team);

        $manager = ServiceLayer::getService('PlaygroundManager');
        $user = $security->getIdentity()->getUser();
        $playgrounds = $manager->getUserPlaygrounds($user);

        return $this->render('CR\TeamPlayground\TeamPlaygroundModule:crud:list.php', array(
                    'playgrounds' => $playgrounds,
                    'team' => $team
        ));
    }
    
    private function getForm()
    {
        $translator = ServiceLayer::getService('translator');
        $playgroundManager = ServiceLayer::getService('PlaygroundManager');
        $areaTypeEnum = $playgroundManager->getAreaTypeEnum();
        
        $areaTypeChoices = array('' => '');
        foreach($areaTypeEnum as $key=> $val)
        {
            $areaTypeChoices[$key] = $translator->translate($val);
        }
        
        $form = new \Webteamer\Locality\Form\PlaygroundForm();
        $form->setFieldChoices('area_type', $areaTypeChoices);
        return $form;
    }

    public function editAction()
    {
        $request = ServiceLayer::getService('request');
        $translator = ServiceLayer::getService('translator');
        $security = ServiceLayer::getService('security');
        $manager = ServiceLayer::getService('PlaygroundManager');
        $localityManager = ServiceLayer::getService('LocalityManager');
        $teamManager = ServiceLayer::getService('TeamManager');
        $team = $teamManager->findTeamById($request->get('team_id'));
        
        $this->checkSettingTeamPermission($team);
        
        $playground = $manager->findPlaygroundById($request->get('id'));
        
        if($playground->getAuthorId() != $security->getIdentity()->getUser()->getId())
        {
            throw  new AclException();
        }
        
        
        $locality = $playground->getLocality();
        $form = $this->getForm();
        $validator = new \Core\Validator();
        $validator->setRules($form->getFields());
        $form->setEntity($playground);
        
     
        
        if ('POST' == $request->getMethod())
        {
            $post_data = $request->get($form->getName());
            $form->bindData($post_data);
            $validator->setData($post_data);
            $validator->validateData();

            if (!$validator->hasErrors())
            {
                $entity = $form->getEntity();
                
                if(null != $post_data['locality_json_data'])
                {
                     //create new locality, and attach to playground alias
                    $newLocality = $localityManager->createObjectFromJsonData($post_data['locality_json_data']);
                    $localityId = $localityManager->saveLocality($newLocality);
                    $entity->setLocalityId($localityId);
                }
                
                $manager->savePlayground($entity);
                
                //change alias
                $aliasRepo =  \Core\ServiceLayer::getService('PlaygroundAliasRepository');
                $playgroundAlias =$aliasRepo->findOneBy(array('playground_id' => $entity->getId(),'team_id' => $team->getId()));
                $playgroundAlias->setName($entity->getName());
                $aliasRepo->save($playgroundAlias);
                
               
                $request->addFlashMessage('playground_edit_success', $translator->translate('EDIT_PLAYGROUND_SUCCESS'));
                $request->redirect($this->getRouter()->link('team_playground_edit',array('team_id' =>$team->getId(),'id' => $playground->getId() )));
            }
        }

       
        return $this->render('CR\TeamPlayground\TeamPlaygroundModule:crud:edit.php', array(
                    'team' => $team,
                    'playground' => $playground,
                    'locality' => $locality,
                    'form' => $form,
                    'validator' => $validator
        ));
    }
    
   
    
    public function createAction()
    {
        $request = ServiceLayer::getService('request');
        $translator = ServiceLayer::getService('translator');
        $security = ServiceLayer::getService('security');
        $manager = ServiceLayer::getService('PlaygroundManager');
        $localityManager = ServiceLayer::getService('LocalityManager');
        $teamManager = ServiceLayer::getService('TeamManager');
        $team = $teamManager->findTeamById($request->get('team_id'));
        
        $this->checkSettingTeamPermission($team);
        
        $playground = new \Webteamer\Locality\Model\Playground();
        $form = $this->getForm();
        $validator = new \Core\Validator();
        $validator->setRules($form->getFields());
        $form->setEntity($playground);
        
        
        $playerManager = ServiceLayer::getService('PlayerManager');
        $player = $playerManager->findPlayerById( $security->getIdentity()->getUser()->getId());
        $locality  = $player->getLocality();

        if ('POST' == $request->getMethod())
        {
            $post_data = $request->get($form->getName());
            $playgroundData = $request->get('playgrounds');
            $form->bindData($post_data);
            $validator->setData($post_data);
            $validator->validateData();
            
            if(null == $playgroundData['locality_json_data'])
            {
                $validator->addErrorMessage('playground_locality',$translator->translate('select city name, street or place mark'));
            }

            if (!$validator->hasErrors())
            {
                
                $playgroundData['author_id'] = $security->getIdentity()->getUser()->getId();
                $playgroundData['team_id'] = $team->getId();
                $saveAliasResult = $manager->savePlaygroundAlias($playgroundData);
                $playgroundAlias = $saveAliasResult['entity'];

               
                $request->addFlashMessage('playground_create_success', $translator->translate('CREATE_PLAYGROUND_SUCCESS'));
                $request->redirect($this->getRouter()->link('team_playground_edit',array('team_id' =>$team->getId(),'id' => $playgroundAlias->getPlaygroundId() )));
            }
        }

        return $this->render('CR\TeamPlayground\TeamPlaygroundModule:crud:create.php', array(
                   'team' => $team,
                    'playground' => $playground,
                    'locality' => $locality,
                    'form' => $form,
                    'validator' => $validator
        ));
    }
    
     public function deleteAction()
    {
        $request = ServiceLayer::getService('request');
        $teamManager = ServiceLayer::getService('TeamManager');
        $team = $teamManager->findTeamById($request->get('team_id'));
        $this->checkSettingTeamPermission($team);
         
        $manager = ServiceLayer::getService('PlaygroundManager');
        $playground = $manager->findPlaygroundById($request->get('id'));
        $manager->deletePlayground($playground);
        $request->redirect($this->getRouter()->link('team_playground_list',array('team_id' =>$team->getId())));
       
        
    }

}
