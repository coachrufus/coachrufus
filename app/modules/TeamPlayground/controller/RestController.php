<?php

namespace CR\TeamPlayground\Controller;

use Core\RestApiController;
use Core\ServiceLayer;
use Core\Validator;
use Webteamer\Locality\Model\Playground;

class RestController extends RestApiController {

    
    
    
    public function createAction()
    {
        $request = ServiceLayer::getService('request');
        $translator = ServiceLayer::getService('translator');
        $security = ServiceLayer::getService('security');
        $manager = ServiceLayer::getService('PlaygroundManager');

        $playground = new Playground();
        $form = new \Webteamer\Locality\Form\PlaygroundForm();
        $validator = new Validator();
        $validator->setRules($form->getFields());
        $form->setEntity($playground);

        $post_data = $request->get($form->getName());
        $playgroundData = $request->get('playgrounds');
        $form->bindData($post_data);
        $validator->setData($post_data);
        $validator->validateData();

        if (null == $playgroundData['locality_json_data'])
        {
            $validator->addErrorMessage('playground_locality', $translator->translate('select city name, street or place mark'));
        }

        if (!$validator->hasErrors())
        {

            $playgroundData['author_id'] = $security->getIdentity()->getUser()->getId();
            $saveAliasResult = $manager->savePlaygroundAlias($playgroundData);
            $playgroundAlias = $saveAliasResult['entity'];

            $result = array('result' => 'SUCCESS','value' => $playgroundAlias->getPlaygroundId(),'name' => $playgroundAlias->getName() );
        }
        else
        {
             $result = array('result' => 'ERROR','errors' => $validator->getErrors());
        }
        
       return $this->asJson($result);
    }

}

