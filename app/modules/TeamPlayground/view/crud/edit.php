<?php if ($request->hasFlashMessage('playground_edit_success')): ?>
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <?php echo $request->getFlashMessage('playground_edit_success') ?>
    </div>
<?php endif; ?>
<?php if ($request->hasFlashMessage('playground_create_success')): ?>
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <?php echo $request->getFlashMessage('playground_create_success') ?>
    </div>
<?php endif; ?>

<?php Core\Layout::getInstance()->startSlot('team_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/Team/view/team/_team_menu.php', array('team' => $team)) ?>
<?php Core\Layout::getInstance()->endSlot('team_menu') ?>



<section class="content-header">
    <h1>
        <?php echo $playground->getName() ?>
    </h1>
    <?php $layout->includePart(GLOBAL_DIR.'/templates/parts/_breadcrumb.php',array(
        'crumbs' => array(
            'Teams' => $router->link('teams_overview'),
            $team->getName() => $router->link('team_overview',array('id' =>$team->getId() )),
            'Playgrounds' => $router->link('team_playground_list', array('team_id' => $team->getId())),
            'Edit' => ''
            ))) ?>
</section>
<section class="content">

    <div class="box box-primary">
        <div class="box-header with-border">

        </div>
        <div class="box-body">
            <form action="" class="form-bordered" method="post">
                 <input type="hidden" name="playgrounds[locality_json_data]" value="" id="playground_locality_json_data" />
                <?php echo $form->renderInputHiddenTag("city") ?>
                <?php echo $form->renderInputHiddenTag("street") ?>
                <?php echo $form->renderInputHiddenTag("street_number") ?>
                 
                   <div class="row">
                       <div class="col-sm-6">
                           <div class="row">
                               <div class="form-group col-sm-12">
                                    <label class="control-label"><?php echo $translator->translate('Playground name') ?></label>
                                     <?php echo $form->renderInputTag("name", array('class' => 'form-control','required' => 'required')) ?>
                                     <?php echo $validator->showError("name") ?>
                                </div>
                               
                               
                                <div class="form-group col-sm-12">
                                    <label class="control-label"><?php echo $translator->translate('Address') ?></label>
                                     <input id="pac-input" value="<?php echo $locality->getName() ?>" class="form-control" type="text" placeholder="<?php echo $translator->translate('City name, street, etc.') ?>">
                                     <?php echo $validator->showError("city") ?>
                                </div>
                               
                                <div class="form-group col-sm-12">
                                    <label class="control-label"><?php echo $translator->translate('Area type') ?></label>
                                     <?php echo $form->renderSelectTag("area_type", array('class' => 'form-control')) ?>
                                     <?php echo $validator->showError("area_type") ?>
                                </div>
                               
                           </div>
                       </div>
                       
                        <div class="col-sm-6">
                            <div id="map-edit" style="width:100%; height:400px;"> </div>
                       </div>
                       
                         <div class="col-sm-12">

                             <button type="submit" class="btn btn-primary"><?php echo $translator->translate('Save') ?></button>

                         </div>
                   </div>
                
            </form>
        </div>

    </div>




</section>


<?php $layout->addJavascript('js/PlaygroundManager.js') ?>

<?php $layout->startSlot('javascript') ?>

<script type="text/javascript">
playground_manger = new PlaygroundManager();
  
    function showEventLocation()
    {
        playground_manger.google = google;
        playground_manger.initMap({lat: <?php echo $locality->getLat();?>, lng: <?php echo $locality->getLng() ?>});
        playground_manger.createSearchInput();
    }




</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXU1duK54f1gIsRy_xc7z4uQURovsZdE8&libraries=places&callback=showEventLocation" async defer></script>
<?php $layout->endSlot('javascript') ?>