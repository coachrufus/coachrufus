<div class="modal fade" id="add-playground-modal" tabindex="-1" role="dialog" aria-labelledby="<?php echo $translator->translate('Add playground') ?>">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?php echo $translator->translate('Add playground') ?></h4>
            </div>

            <div class="modal-body">
                <form action="<?php echo $router->link('team_playground_rest_create') ?>" class="form-bordered" method="post">

                    <input type="hidden" name="playgrounds[locality_json_data]" value="" id="playground_locality_json_data" />
                    <input type="hidden" name="playgrounds[city]" value="" id="playgrounds_city">
                    <input type="hidden" name="playgrounds[street]" value="" id="playgrounds_street">
                    <input type="hidden" name="playgrounds[street_number]" value="" id="playgrounds_street_number">

                    <div class="row">

                        <div class="form-group col-sm-12" data-error-bind="name">
                            <label class="control-label"><?php echo $translator->translate('Playground name') ?></label>
                            <input type="text" name="playgrounds[name]" value="" class="form-control" id="playgrounds_name" required="required">
                        </div>


                        <div class="form-group col-sm-12" data-error-bind="area_type">
                            <label class="control-label"><?php echo $translator->translate('Area type') ?></label>
                            <select name="playgrounds[area_type]" class="form-control" id="playgrounds_area_type">
                                <option selected="selected" value=""></option>
                                <option value="indoor"><?php echo $translator->translate('Indoor') ?></option>
                                <option value="outdoor"><?php echo $translator->translate('Outdoor') ?></option>
                            </select>                                                                     </div>

                        <div class="form-group col-sm-12" data-error-bind="playground_locality">
                            <label class="control-label"><?php echo $translator->translate('Address') ?></label>
                            <input  id="create-playground-search" value="" class="form-control" type="text" placeholder="<?php echo $translator->translate('City name, street, etc.') ?>">
                        </div>

                        <div class="col-sm-12">
                            <div id="map-edit" style="width:100%; height:0;"> </div>
                        </div>

                        <div class="col-sm-12">

                            <button type="submit" class="btn btn-primary"><?php echo $translator->translate('Save') ?></button>

                        </div>
                    </div>

                </form>


            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $translator->translate('Cancel') ?></button>
            </div>
        </div>
    </div>
</div>