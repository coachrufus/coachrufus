<?php Core\Layout::getInstance()->startSlot('team_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/Team/view/team/_team_menu.php', array('team' => $team)) ?>
<?php Core\Layout::getInstance()->endSlot('team_menu') ?>



<section class="content-header">
    <h1>
        <?php echo $team->getName() ?> - <?php echo $translator->translate('Playgrounds') ?>
    </h1>
    
     <?php $layout->includePart(GLOBAL_DIR.'/templates/parts/_breadcrumb.php',array(
        'crumbs' => array(
            'Teams' => $router->link('teams_overview'),
            $team->getName() => $router->link('team_overview',array('id' =>$team->getId() )),
            'Playgrounds' => ''
            ))) ?>
</section>
<section class="content">
    
     <div class="box box-primary">
         <div class="box-header with-border">
             
         </div>
         <div class="box-body">
            <table class="table">
            <thead>
                <tr>
                    <td><?php echo $translator->translate('Playground name') ?></td>
                    <td></td>
                </tr>
            </thead>
            <tbody>
                <?php foreach($playgrounds as $playground): ?>
                <tr>
                    <td><a  href="<?php echo  $router->link('team_playground_edit',array('team_id' => $team->getId(),'id' => $playground->getId())) ?>" ><?php echo $playground->getName() ?></a></td>      
                    <td><a class="remove-playground-trigger"  href="<?php echo  $router->link('team_playground_delete',array('team_id' => $team->getId(),'id' => $playground->getId())) ?>" ><i class="fa fa-trash"></i></a></td>      </td>  
                </tr>
                <?php endforeach; ?>
            </tbody>
            
        </table>
        
        
       
             
             
         </div>
         <div class="box-footer">
              <a class="btn btn-primary" href="<?php echo  $router->link('team_playground_create',array('team_id' => $team->getId())) ?>"><?php echo $translator->translate('Create playground') ?></a>
         </div>
      </div>
        



</section>

 <!-- Modal -->
<div class="modal fade" id="removePlaygroundModal" tabindex="-1" role="dialog" aria-labelledby="<?php echo $translator->translate('Remove playground') ?>">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?php echo $translator->translate('Remove playground') ?></h4>
            </div>
            <div class="modal-body">
                <?php echo $translator->translate('Are you sure?') ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $translator->translate('Cancel') ?></button>
                <button type="button" class="btn btn-danger modal_submit"><?php echo $translator->translate('Confirm') ?></button>
            </div>
        </div>
    </div>
</div>
 
<?php $layout->startSlot('javascript') ?>
<script type="text/javascript">
    //var delete_url;
    $('.remove-playground-trigger').on('click',function(e){
        e.preventDefault();
        $('#removePlaygroundModal .modal_submit').attr('data-href',$(this).attr('href'));
        $('#removePlaygroundModal').modal('show');
       
   });
   
   $('#removePlaygroundModal .modal_submit').on('click',function(){
       location.href=$(this).attr('data-href');
   });
</script>
<?php $layout->endSlot('javascript') ?>