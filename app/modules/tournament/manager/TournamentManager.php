<?php

namespace CR\Tournament\Manager;

class TournamentManager extends \Core\Manager {

    public function subscribeUserEvent($userId,$lineupId)
    {
        $storage = new \Core\DbStorage('team_match_notify_users');
        $data['user_id'] = $userId;
        $data['lineup_id'] = $lineupId;
        $data['type'] = 'tournament_match_push_notify';
        $storage->create($data);
    }

    public function unsubscribeUserEvent($userId,$lineupId)
    {
        $storage = new \Core\DbStorage('team_match_notify_users');
        $data['user_id'] = $userId;
        $data['lineup_id'] = $lineupId;
        $data['type'] = 'tournament_match_push_notify';
        $storage->deleteByParams($data);
    }
    
    
    public function createTournament(\CR\Tournament\Model\Tournament $tournament)
    {
        $result = $this->getRepository()->save($tournament);
        return array('id' => $result);
    }

    public function findUserTournament($user, $id)
    {
        $object = $this->getRepository()->find($id);
        return $object;
    }

    public function registerTeamToTournament($tournament, $team, $user)
    {
        $storage = new \Core\DbStorage('tournament_team');
        $data['tournament_id'] = $tournament->getId();
        $data['team_id'] = $team->getId();
        $data['created_at'] = date('Y-m-d H:i:s');
        $data['created_by'] = $user->getId();
        $data['status'] = 'waiting-to-confirm';

        $storage->create($data);
    }
    
    public function getTournamentLadder($tournamentId)
    {
        $repository = $this->getRepository();
        $teams = $repository->findTournamentTeams($tournamentId, array('status' => 'confirmed'));
        $points = $repository->getTournamentTeamsPoints($tournamentId);
        
        
        //sort to groups
        $eventRepository = \Core\ServiceLayer::getService('EventRepository');
        $events = $eventRepository->findBy(array('tournament_id' => $tournamentId,'tournament_type' => 'ladder'));
        $groups  = array();
        foreach($events as $event)
        {
            $groups[$event->getTeamId()] = $event->getTournamentGroup();
            $groups[$event->getOpponentTeamId()] = $event->getTournamentGroup();
        }

        
        $ladder = array();
        foreach($teams as $team)
        {
            $ladder[$team->getId()] = array(
                'team' => array('id' => $team->getId(),'name' => $team->getName(),'group' => $groups[$team->getId()]),
                'wins' => 0,
                'losses' => 0,
                'draws' => 0,
                'points' => 0,
                'goals' => 0,
                'score' => 0,
                'games' => 0,
                'goalMinus' => 0,
                'winsOvertime' => 0,
                'loosesOvertime' => 0
            );
        }
        

        
        $gamePoints = array();
        foreach($points as $point)
        {
           
            $gamePoints[$point['lineup_id']]['home_team']['id'] = $point['home_team_id'];
            $gamePoints[$point['lineup_id']]['opponent_team']['id'] = $point['opponent_team_id'];
            $gamePoints[$point['lineup_id']]['overtime'] = $point['overtime'];
            
            if(!array_key_exists('goal', $gamePoints[$point['lineup_id']]['home_team']))
            {
                $gamePoints[$point['lineup_id']]['home_team']['goal'] = 0;
            }
            if(!array_key_exists('goal', $gamePoints[$point['lineup_id']]['opponent_team']))
            {
                $gamePoints[$point['lineup_id']]['opponent_team']['goal'] = 0;
            }
            
            if(!array_key_exists('assist', $gamePoints[$point['lineup_id']]['home_team']))
            {
                $gamePoints[$point['lineup_id']]['home_team']['assist'] = 0;
            }
            if(!array_key_exists('assist', $gamePoints[$point['lineup_id']]['opponent_team']))
            {
                $gamePoints[$point['lineup_id']]['opponent_team']['assist'] = 0;
            }
            
            
            
            if('goal' == $point['hit_type'])
            {
                //$gamePoints[$point['lineup_id']][$point['team_id']]['goal']++;
                if($gamePoints[$point['lineup_id']]['home_team']['id'] == $point['team_id'])
                {
                    $gamePoints[$point['lineup_id']]['home_team']['goal']++;
                }
                if($gamePoints[$point['lineup_id']]['opponent_team']['id'] == $point['team_id'])
                {
                    $gamePoints[$point['lineup_id']]['opponent_team']['goal']++;
                }
                
                
            }
            
            if('assist' == $point['hit_type'])
            {
                 if($gamePoints[$point['lineup_id']]['home_team']['id'] == $point['team_id'])
                {
                    $gamePoints[$point['lineup_id']]['home_team']['assist']++;
                }
                if($gamePoints[$point['lineup_id']]['opponent_team']['id'] == $point['team_id'])
                {
                    $gamePoints[$point['lineup_id']]['opponent_team']['assist']++;
                }
            }
        }
        

        
        foreach($gamePoints as $game)
        {
            $ladder[$game['home_team']['id']]['goals'] += $game['home_team']['goal'];
            $ladder[$game['home_team']['id']]['score'] += $game['home_team']['goal'];
            $ladder[$game['home_team']['id']]['score'] -= $game['opponent_team']['goal'];
            $ladder[$game['home_team']['id']]['goalMinus'] += $game['opponent_team']['goal'];

            
            $ladder[$game['opponent_team']['id']]['goals'] += $game['opponent_team']['goal'];
            $ladder[$game['opponent_team']['id']]['score'] += $game['opponent_team']['goal'];
            $ladder[$game['opponent_team']['id']]['score'] -= $game['home_team']['goal'];
            
            $ladder[$game['opponent_team']['id']]['goalMinus'] += $game['home_team']['goal'];
            
            
            $winPoints = 3;
            $losePoints = 0;
            if($game['overtime'] == 1)
            {
                 $winPoints = 2;
                 $losePoints = 1;
            }
            
            
            if($game['home_team']['goal'] > $game['opponent_team']['goal'] )
            {
                //$winnerId = $game['home_team']['id'];
                $ladder[$game['home_team']['id']]['points'] += $winPoints; 
                $ladder[$game['opponent_team']['id']]['points'] += $losePoints; 

                if($game['overtime'] == 1)
                {
                     $ladder[$game['home_team']['id']]['winsOvertime']++;
                     $ladder[$game['opponent_team']['id']]['lossesOvertime']++;
                }
                else
                {
                    $ladder[$game['home_team']['id']]['wins']++;
                    $ladder[$game['opponent_team']['id']]['losses']++;
                }
                
            }
            
            if($game['home_team']['goal'] < $game['opponent_team']['goal'] )
            {
               
                $ladder[$game['opponent_team']['id']]['points'] += $winPoints; 
                
                $ladder[$game['home_team']['id']]['points'] += $losePoints; 
               
                
                if($game['overtime'] == 1)
                {
                     $ladder[$game['opponent_team']['id']]['winsOvertime']++;
                     $ladder[$game['home_team']['id']]['lossesOvertime']++;
                }
                else
                {
                     $ladder[$game['opponent_team']['id']]['wins']++;
                     $ladder[$game['home_team']['id']]['losses']++;
                }
            }
            
            if($game['home_team']['goal'] == $game['opponent_team']['goal'] )
            {
                $ladder[$game['home_team']['id']]['draws']++;
                $ladder[$game['opponent_team']['id']]['draws']++;
                
                $ladder[$game['home_team']['id']]['points'] += 1; 
                $ladder[$game['opponent_team']['id']]['points'] += 1; 
            }
            
            $ladder[$game['opponent_team']['id']]['games']++; 
            $ladder[$game['home_team']['id']]['games']++;
        }

       // var_dump($ladder);exit;
       //sort ladder
        $sortedLadder = array();
        foreach($ladder as $ladderTeam)
        {
            $ranking = $ladderTeam['points']*100;
            if(array_key_exists($ranking, $sortedLadder))
            {
                $concurentLadderTeam = $sortedLadder[$ranking];
                $newRanking = $ranking+$this->getRankingDiff($concurentLadderTeam,$ladderTeam,$gamePoints);
                if(array_key_exists($newRanking, $sortedLadder))
                {
                    $newRanking = $this->rankTeam($sortedLadder,$newRanking);
                    $sortedLadder[$newRanking] = $ladderTeam;
                }
                else
                {
                    $sortedLadder[$newRanking] = $ladderTeam;
                }
            }
            else 
            {
                $sortedLadder[$ranking] = $ladderTeam;
            }
        }
       
        krsort($sortedLadder);
        $sortedLadder = array_values($sortedLadder);
        
        $groupedLadder = array();
        foreach($sortedLadder as $key => $sortedLadderItem)
        {
            if($sortedLadderItem['team']['group'] != '')
			{
				$groupedLadder[$sortedLadderItem['team']['group']][] = $sortedLadderItem;
			}
			
        }

        return $groupedLadder;
    }
    
    public function getTournamentPlayersLadder($tournamentId)
    {
        $repository = $this->getRepository();
        $teamManager = \Core\ServiceLayer::getService('TeamManager');
        $teams = $repository->findTournamentTeams($tournamentId, array('status' => 'confirmed'));
        $teamPlayers = array();
        foreach($teams as $team)
        {
            $tp = $teamManager->getActiveTeamPlayers($team->getId());
            foreach($tp as $t)
            {
                $teamPlayers[$t->getId()] = array('name' => $t->getFullName(),'assist' => 0,'goal' => 0,'mom' => 0, 'points' => 0,'number' => $t->getPlayerNumber(),'teamName' => $team->getName());
            }
        }
        $points = $repository->getTournamentPlayersPoints($tournamentId);
        
        $ladder = array();
        foreach($points as $point)
        {
            $teamPlayers[$point['team_player_id']][$point['hit_type']]  = $point['count'];
        }
        
        foreach($teamPlayers as $key => $teamPlayer)
        {
            $teamPlayers[$key]['points']  = ($teamPlayer['goal']*1)+ ($teamPlayer['assist']*1)+($teamPlayer['mom']*1);
        }
        

        
        /*
        $sortedLadder = array();
        foreach($teamPlayers as $ladderPlayer)
        {
            $ranking = $ladderPlayer['points']*100;
            if(array_key_exists($ranking, $sortedLadder))
            {
                $newRanking = $this->rankTeam($sortedLadder,$newRanking);
                 $sortedLadder[$newRanking] = $ladderPlayer;
            }
            else 
            {
                $sortedLadder[$ranking] = $ladderPlayer;
            }
        }
         * 
         */
         
        usort($teamPlayers,function($first,$second){
            return $first['points'] < $second['points'] ;
        });
        $sortedLadder = $teamPlayers;
        /*
         krsort($sortedLadder);
         var_dump($sortedLadder);
         $sortedLadder = array_values($sortedLadder);
         * 
         */
         //var_dump($sortedLadder);
         return $sortedLadder;
    }
    
    
    
    private function rankTeam($sortedLadder,$ranking)
    {
        if(array_key_exists($ranking, $sortedLadder))
        {
            $newRanking = $ranking+1;
            return $this->rankTeam($sortedLadder, $newRanking);
        }
        else 
        {
            return $ranking;
        }
    }
    
    
    private function getRankingDiff($concurentLadderTeam,$ladderTeam,$gamePoints)
    {
        $rankingDiff = 0;
        
        //vzajomny zapas
        $currentTeamId = $ladderTeam['team']['id'];
        $concurentTeamId = $concurentLadderTeam['team']['id'];
        foreach ($gamePoints as $gameId => $teams)
        {
            if (($teams['home_team']['id'] == $currentTeamId && $teams['opponent_team']['id'] == $concurentTeamId))
            {
                //vyhraty vzajomny zapas
                if ($teams['home_team']['goal'] > $teams['opponent_team']['goal'])
                {
                    $rankingDiff = 1;
                }
                //prehra vo vzajomnom zapase
                if ($teams['home_team']['goal'] < $teams['opponent_team']['goal'])
                {
                    $rankingDiff = -1;
                }
            }
            if (($teams['opponent_team']['id'] == $currentTeamId && $teams['home_team']['id'] == $concurentTeamId))
            {
                //prehraty vzajomny zapas
                if ($teams['home_team']['goal'] > $teams['opponent_team']['goal'])
                {
                    $rankingDiff = -1;
                }
                //vyhra vo vzajomnom zapase
                if ($teams['home_team']['goal'] < $teams['opponent_team']['goal'])
                {
                    $rankingDiff = 1;
                }
            }
        }

        //rozdiel skore
        if ($rankingDiff == 0)
        {
            //echo 'rozdiel skore';
            //var_dump($ladderTeam['score']);
            //var_dump($concurentLadderTeam['score']);
            if ($ladderTeam['score'] > $concurentLadderTeam['score'])
            {
                $rankingDiff = 1;
            }
            if ($ladderTeam['score'] < $concurentLadderTeam['score'])
            {
                $rankingDiff = -1;
            }
        }


        //pocet golov
        if ($rankingDiff == 0)
        {
            if ($ladderTeam['goals'] > $concurentLadderTeam['goals'])
            {
                $rankingDiff = 1;
            }
            if ($ladderTeam['goals'] < $concurentLadderTeam['goals'])
            {
                $rankingDiff = -1;
            }
        }

        return $rankingDiff;
    }
    
    public function getTournamentHits($tournamentId)
    {
        $data =  $this->getRepository()->getTournamentHits($tournamentId);
        $index = array();
        foreach($data as $d)
        {
            $timeIndexParts = explode('#',$d['hit_group']);
            
            
            //var_dump($timeIndexParts);
            
            $time = \DateTime::createFromFormat ('Y-m-d-H-i-s',$timeIndexParts[0]);
            $index[$time->getTimestamp()] = $d; 
        }
        
        $currentTimeLimit = time()-5;
        $current = array('status' => 'empty');
        foreach($index as $key => $d)
        {
            if($key > $currentTimeLimit)
            {
                $d['status'] = 'SUCCESS';
                return $d;
            }
        }
        
        return $current;
        
    }

  

}
