<?php
namespace CR\Tournament\Manager;
class TournamentPermissionManager
{
     public function hasTournamentPermission($user,$team)
    {
        if($this->isTournamentAdmin($user,$team))
        {
            return true;
        }
        
        return false;
    }
    
    public function hasTeamPermission($user,$team)
    {
        if($this->isTeamAdmin($user,$team))
        {
            return true;
        }
        
        return false;
    }
    
    public function hasEventPermission($user,$event,$permission)
    {

        if($permission == 'EVENT_NOMINATION_VIEW' or $permission == 'EVENT_LIVESCORE' or $permission == 'EVENT_EDIT' or $permission == 'EVENT_NOMINATION')
        {
            return true;
        }

        if($this->isEventAdmin($user,$event))
        {
            return true;
        }
        
        return false;

    }
    
    
    public function isTeamAdmin($user, $team)
    {
        if(in_array($user->getId(), $team->getAdminList()))
        {
            return true;
        }
        return false;
    }
    
    public function isTournamentAdmin($user, $tournament)
    {
        //t_dump($tournament->getAdminList());
        
        if(in_array($user->getId(), $tournament->getAdminList()))
        {
            return true;
        }
        return false;
        
    }
    
     public function isEventAdmin($user, $event)
    {
        if(in_array($user->getId(), $event->getAdminList()))
        {
            return true;
        }
        return false;
    }
    
    
}
