<?php

namespace CR\Tournament\Manager;
class TournamentEnumManager
{
    private function getTranslator()
    {
        return \Core\ServiceLayer::getService('translator');
    }
    
    
    public function getPlayerLevelEnum()
    {
        $translator =  $this->getTranslator();
        return array(
             '' => $translator->translate(''),
            'beginner' => $translator->translate('tournament.players.form.level.beginner') ,
            'advanced' => $translator->translate('tournament.players.form.level.advanced') ,
            'expert' => $translator->translate('tournament.players.form.level.expert') , 
            'league' => $translator->translate('tournament.players.form.level.league') , 
       );
    }

    public function getPlayerRoleEnum()
    {
       $translator =  $this->getTranslator();
         return array(
            '' => $translator->translate(''),
            'PLAYER' => $translator->translate('tournament.players.form.role.player'),
            'GOAL_KEEPER' => $translator->translate('tournament.players.form.role.goalKeeper'),
        );
    }
}
