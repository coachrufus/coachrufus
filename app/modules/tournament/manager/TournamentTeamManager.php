<?php
namespace CR\Tournament\Manager;
class TournamentTeamManager  extends \Core\Manager
{
    public function getTournamentTeams($tournament)
    {
        return $this->getRepository()->getTeamsByTournamentId($tournament->getId());
    }
 
}

