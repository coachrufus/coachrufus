<?php
$sportManager = \Core\ServiceLayer::getService('SportManager');
$sportChoices = $sportManager->getSportFormChoices();
?>

<section class="content-header">
    <h1>
        <?php echo $translator->translate('tournament.dashboard.title') ?>
    </h1>

    <?php $layout->includePart(GLOBAL_DIR . '/templates/parts/_breadcrumb.php', array('crumbs' => array('tournament.bradcrumb.title' => $router->link('tournament')))) ?>
</section>
<section class="content tournament-dashboard">

    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" <?php echo ('me' == $activeTab) ? 'class="active"' : '' ?> >
            <a href="#my" role="tab" data-toggle="tab"><?php echo $translator->translate('tournament.dashboard.tab.my') ?></a>
        </li>
        <li role="presentation" <?php echo ('public' == $activeTab) ? 'class="active"' : '' ?> >
            <a href="#public"  role="tab" data-toggle="tab"><?php echo $translator->translate('tournament.dashboard.tab.public') ?></a>
        </li>
        <li role="presentation" <?php echo ('admin' == $activeTab) ? 'class="active"' : '' ?> >
            <a href="#admin"  role="tab" data-toggle="tab"><?php echo $translator->translate('tournament.dashboard.tab.admin') ?></a>
        </li>
    </ul>

    <div class="tab-content">
        <div role="tabpanel" class="tab-pane <?php echo ('me' == $activeTab) ? 'active' : '' ?>" id="my">
            <div class="panel">
                <div class="panel-body">
                    <div class="row widget-grid">   
                        <?php foreach ($userTournaments as $tournament): ?>
                            <?php
                            $layout->includePart(MODUL_DIR . '/tournament/view/tournament/_tournament_panel.php', array(
                                'tournament' => $tournament,
                                'showJoinButton' => false,
                                'sportChoices' => $sportChoices,
                                'userPermission' => '',
                                'showTeamInfo' => true
                            ))
                            ?> 
                        <?php endforeach; ?>
                        
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane <?php echo ('public' == $activeTab) ? 'active' : '' ?>" id="public">
            <div class="panel">
                <div class="panel-body">
                    <div class="row widget-grid">   
                        <?php foreach ($publicTournaments as $tournament): ?>
                            <?php
                            $layout->includePart(MODUL_DIR . '/tournament/view/tournament/_tournament_panel.php', array(
                                'tournament' => $tournament,
                                'showJoinButton' => ($tournament->getRegisterStatus() == 'allowed') ? true : false,
                                'sportChoices' => $sportChoices,
                                'userPermission' => $tournament->getUserPermissions($user->getId()),
                                'showTeamInfo' => false
                            ))
                            ?> 
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane <?php echo ('admin' == $activeTab) ? 'active' : '' ?>" id="admin">
            <div class="panel">
                <div class="panel-body">
                    <div class="row widget-grid">   
                        <?php foreach ($adminTournaments as $tournament): ?>
                            <?php
                            $layout->includePart(MODUL_DIR . '/tournament/view/tournament/_tournament_panel.php', array(
                                'tournament' => $tournament,
                                'showJoinButton' => false,
                                'sportChoices' => $sportChoices,
                                'userPermission' => $tournament->getUserPermissions($user->getId()),
                                'showTeamInfo' => false                            ))
                            ?> 
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>


</section>



