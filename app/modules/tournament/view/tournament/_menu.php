<?php 
$sportManager = Core\ServiceLayer::getService('SportManager');
$permissionManager = Core\ServiceLayer::getService('TournamentPermissionManager');
$sportEnum = $sportManager->getSportFormChoices();
$menuClass = $layout->buildTournamentMenuClass();

 $security = Core\ServiceLayer::getService('security');
 $user = $security->getIdentity()->getUser();
 
?>

    <li class="menu-main team-header">
        <a class="<?php echo $menuClass['detail'] ?>"  href="<?php echo $router->link('tournament_detail',array('id' => $tournament->getId())) ?>"> 
           <i class="ico ico-team" style="background-image: url('<?php echo $tournament->getMidPhoto() ?>');"></i>
           <span><?php echo $tournament->getName() ?> (<?php echo $translator->translate($sportEnum[$tournament->getSportId()]) ?>)</span>
        </a>
    </li>
    <!--
     <li  class="menu-main">
        <a class="<?php echo $menuClass['schedule'] ?>" href="<?php echo $router->link('tournament_schedule', array('id' => $tournament->getId())) ?>"> 
             <i class="ico ico-team-events"></i>
        <span><?php echo $translator->translate('tournament.menu.schedule') ?> </span>
        </a>
    </li>
    
     <li  class="menu-main">
        <a  href="<?php echo $router->link('tournament_teams_overview',array('id' => $tournament->getId())) ?>"> 
             <i class="ico ico-teams"></i>
        <span><?php echo $translator->translate('tournament.menu.teams') ?> </span>
        </a>
    </li>
    
     <li  class="menu-main">
        <a class="<?php echo $menuClass['stats'] ?>" href="<?php echo $router->link('tournament_stats', array('id' => $tournament->getId())) ?>"> 
             <i class="ico ico-team-stat"></i>
        <span><?php echo $translator->translate('tournament.menu.tournament_stats') ?> </span>
        </a>
    </li>
-->
    
  
    <?php if($permissionManager->hasTournamentPermission($user,$tournament,'ADMIN')): ?>
    <li  class="menu-main">
        <a class="<?php echo $menuClass['settings'] ?>" href="<?php echo $router->link('tournament_settings', array('id' => $tournament->getId())) ?>"> 
             <i class="ico ico-team-home"></i>
        <span><?php echo $translator->translate('tournament.menu.settings') ?> </span>
        </a>
    </li>
<!--
    <li  class="menu-main">
        <a class="<?php echo $menuClass['teams'] ?>" href="<?php echo $router->link('tournament_teams', array('id' => $tournament->getId())) ?>"> 
             <i class="ico ico-team-members"></i>
        <span><?php echo $translator->translate('tournament.menu.adminTeams') ?> </span>
        </a>
    </li>
-->
     <?php endif ?>
    
   
   
   
   