<?php 
$detailLink =  $router->link('tournament_public_detail2',array('seoid' => $tournament->getSeoId()));
if($userPermission == 'TOURNAMENT_ADMIN')
{
    //$detailLink =  $router->link('tournament_public_detail2',array('seoid' => $tournament->getSeoId()));
}


?>

<div class="col-md-4 col-sm-6 widget">               
    <div class="panel panel-default panel-tournament">
        <div class="panel-heading">
            <h3><i class="ico ico-team"></i> <?php //echo $tournament->getName() ?></h3>
           
        </div>
        
        <div class="panel-body  panel-full-body team-panel fake-link" data-redirect="<?php echo $detailLink ?>">
            <div class="panel-team-header">
                <div class="img_round_wrap team_img_round">
                    <a style="background-image: url('/img/skliga-logo.png')" href="<?php echo $detailLink?>"></a>
                </div>
                <div class="team-info">
                    <h3 class=""><?php echo $tournament->getName() ?></h3>
                    <h4 class=""><?php echo $sportChoices[$tournament->getSportId()] ?></h4>
                    <small></small>
                </div>
                <a class="team-more-link" href="<?php echo $detailLink ?>"><i class="ico ico-team-more"></i></a>
            </div>

            <div class="team-members-info">
                <div>
                    <strong><?php echo $tournament->getTeamsCount() ?></strong>
                        <?php echo $translator->translate('tournament.panel.teamsCount') ?>
                    
                 </div>
                <div class="middle-border">
                    <strong><?php echo $tournament->getPlayersCount() ?></strong>
                   <?php echo $translator->translate('tournament.panel.playersCount') ?>                  </div>
                <div>
                    <strong><?php echo $tournament->getTotalMatchCount() ?> / <?php echo $tournament->getClosedMatchCount() ?></strong>
                    <?php echo $translator->translate('tournament.panel.matchTotal') ?> / <?php echo $translator->translate('torunament.panel.matchClosed') ?>                       
                </div>
            </div>


          
                <div class="row panel-quick-menu tournament-panel-quick-menu">
                    
                      <?php if($userPermission == 'TOURNAMENT_ADMIN'): ?>
                            <div class="col-sm-4 text-center">
                                 <a href="<?php echo $router->link('tournament_admin_events', array('id' => $tournament->getId())) ?>" class="btn btn-clear-green">
                                      <?php echo $translator->translate('tournament.panel.events') ?>
                                  </a>        
                            </div>
                    
                            <div class="col-sm-4 text-center">
                                 <a href="<?php echo $router->link('tournament_detail', array('id' => $tournament->getId())) ?>" class="btn btn-clear-green">
                                      <?php echo $translator->translate('tournament.panel.teams') ?>
                                  </a>        
                            </div>
                            <div class="col-sm-4 text-center">
                                <a href="<?php echo $router->link('tournament_settings', array('id' => $tournament->getId())) ?>" class="btn btn-clear-green">
                                      <?php echo $translator->translate('tournament.panel.settings') ?>
                                  </a>      
                            </div>
                      <?php endif; ?>
                    
                   
                        
                   

                    
                    <?php if($showJoinButton):  ?>
                    
                    <div class="col-sm-12  text-center">
                        <a href="<?php echo $router->link('tournament_share_link', array('hash' => $tournament->getShareLinkHash())) ?>" class="btn btn-primary">
                            <?php echo $translator->translate('tournament.panel.joinTeam') ?>
                        </a>                   
                    </div>
                    <?php endif; ?>
                    
                    
                    <?php if($showTeamInfo):  ?>
                    <div class="col-sm-12  text-center">
                            <?php if ($tournament->getUserTeamsInfo('waiting-to-confirm') > 0): ?>
                                <?php echo $translator->translate('tournament.panel.waiting') ?><br /><br />
                            <?php elseif($tournament->getUserTeamsInfo('confirmed') > 0): ?>
                                <?php echo $translator->translate('tournament.panel.confirmed') ?><br /><br />
                            <?php endif; ?>
                    </div>
                    <?php endif; ?>

                   
                </div>
               
            


        </div>
       
    </div>

</div>