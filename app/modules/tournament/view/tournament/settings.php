<?php Core\Layout::getInstance()->startSlot('tournament_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/tournament/view/tournament/_menu.php', array('tournament' => $tournament)) ?>
<?php Core\Layout::getInstance()->endSlot('tournament_menu') ?>

<section class="content-header">
    <a class="all-event-link content-header-back" href="<?php echo $router->link('tournament') ?>"><i class="ico ico-event-back"></i><?php echo $translator->translate('tournament.backlink') ?></a>
</section>

<?php Core\Layout::getInstance()->startSlot('crumb') ?>
<li><a href="<?php echo $router->link('teams_overview') ?>"><?php echo $translator->translate('Teams') ?></a></li>
<li class="active"><?php echo $translator->translate('Create team') ?></li>
<?php Core\Layout::getInstance()->endSlot('crumb') ?>

<?php if ($request->hasFlashMessage('_add_success')): ?>
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <?php echo $request->getFlashMessage('_add_success') ?>
    </div>
<?php endif; ?>

<section class="content">


   <?php //$layout->includePart(MODUL_DIR . '/Team/view/team/_team_create_progress.php', array('step' => 1)) ?>
  



    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form action="#" class="form-bordered" method="post" id="team_form"  enctype="multipart/form-data">
                        <input type="hidden" name="type"  value="<?php echo $request->get('tt') ?>" />
                         <div class="row tournament_share_link_wrap">
                            <div class="col-sm-8 form-group">
                                 <label class="control-label"><?php echo $translator->translate('Share team invite link') ?></label>
                        <div class="input-group">
                            <input id="team_share_link" type="text" value="<?php echo WEB_DOMAIN . $router->link('tournament_share_link', array('hash' => $tournament->getShareLinkHash())) ?>" class="form-control" />   
                            <div class="input-group-addon">
                               
                                 <a href="mailto:?subject=<?php echo $translator->translate('tournament.invitation.subject') ?> <?php echo $tournament->getName() ?>&body=<?php echo $translator->translate('tournament.invitation.body') ?>%0D%0A<?php echo WEB_DOMAIN. $router->link('tournament_share_link',array('hash' =>  $tournament->getShareLinkHash())) ?>" class=""><?php echo $translator->translate('tournament.invitation.send') ?></a>
                            </div>
                        </div>
                                 
                                
                                 
                                 
                                 
                                 
                            </div>
                            <div class="col-sm-4 form-group">
                                <label class="control-label">
                                    <?php echo $translator->translate($form->getFieldLabel("register_status")) ?>
                                        <?php echo $form->getRequiredLabel('register_status') ?>
                                </label>
                                <?php echo $form->renderSelectTag("register_status", array('class' => 'form-control')) ?>
                                <?php echo $validator->showError("register_status") ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-4">
                                <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("name")) ?><?php echo $form->getRequiredLabel('name') ?></label>
                                <?php echo $form->renderInputTag("name", array('class' => 'form-control')) ?>
                                <?php echo $validator->showError("name") ?>
                                <?php echo $validator->showError("name_unique",array('message' => $translator->translate('UNIQUE_TEAM_NAME'))) ?>
                            </div>
                            
                             <div class="form-group col-sm-2">
                                <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("start_date")) ?><?php echo $form->getRequiredLabel('start_date') ?></label>
                                <?php echo $form->renderInputTag("start_date", array('class' => 'form-control datetime-picker')) ?>
                                <?php echo $validator->showError("start_date") ?>
                            </div>
                             <div class="form-group col-sm-2">
                                <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("end_date")) ?><?php echo $form->getRequiredLabel('end_date') ?></label>
                                <?php echo $form->renderInputTag("end_date", array('class' => 'form-control datetime-picker')) ?>
                                <?php echo $validator->showError("end_date") ?>
                            </div>
                      
                            <div class="form-group col-sm-4">
                                <label class="control-label">
                                    <?php echo $translator->translate($form->getFieldLabel("gender")) ?><?php echo $form->getRequiredLabel('gender') ?>
                                </label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="ico ico-people"></i>
                                    </div>
                                    <?php echo $form->renderSelectTag("gender", array('class' => 'form-control')) ?>
                                </div>
                                <?php echo $validator->showError("gender") ?>
                            </div>
                           
                        </div>  
                        
                       
                        
                        <?php if($request->get('tt') == 'flch'): ?>
                        <div class="row">
                             <div class="form-group col-sm-6">
                                <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("school_name")) ?><?php echo $form->getRequiredLabel('school_name') ?></label>
                                <?php echo $form->renderInputTag("school_name", array('class' => 'form-control')) ?>
                                <?php echo $validator->showError("school_name") ?>
                            </div>

                            <div class="form-group col-sm-6">
                                <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("flch_city_name")) ?><?php echo $form->getRequiredLabel('flch_city_name') ?></label>
                               
                                <?php echo $form->renderInputTag("flch_city_name_selector", array('class' => 'form-control')) ?>
                                <?php echo $validator->showError("flch_city_name") ?>

                                <?php echo $form->renderInputHiddenTag("flch_city_name") ?>
                            </div>
                        </div>
                        <?php endif; ?>

                        <div class="row"> 

                            <div class="form-group col-sm-4">
                                <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("sport_id")) ?><?php echo $form->getRequiredLabel('sport_id') ?></label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="ico ico-ball"></i>
                                    </div>
                                    <?php if($request->get('tt') == 'flch'): ?>
                                         <?php echo $form->renderSportSelect("sport_id", array('class' => 'form-control','disabled' => 'disabled')) ?>
                                    <?php else: ?>
                                        <?php echo $form->renderSportSelect("sport_id", array('class' => 'form-control')) ?>
                                    <?php endif; ?>
                                   
                                </div>
                                <?php echo $validator->showError("sport_id",array('message' => $translator->translate('Required Field!'))) ?>
                            </div>


                            <div class="form-group col-sm-4">
                                <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("level")) ?></label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="ico ico-level"></i>
                                    </div>
                                    <?php echo $form->renderSelectTag("level", array('class' => 'form-control')) ?>
                                </div>
                                <?php echo $validator->showError("level") ?>
                            </div>


                            <div class="form-group col-sm-2">
                                <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("age_from")) ?></label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="ico ico-age"></i>
                                    </div>
                                    <?php echo $form->renderInputTag("age_from", array('class' => 'form-control')) ?>
                                </div>
                                <?php echo $validator->showError("age_from") ?>
                            </div>
                            <div class="form-group col-sm-2">
                                <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("age_to")) ?></label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="ico ico-age"></i>
                                    </div>
                                    <?php echo $form->renderInputTag("age_to", array('class' => 'form-control')) ?>
                                </div>
                                <?php echo $validator->showError("age_to") ?>
                            </div>




                        </div>

                        <div class="row"> 
                            

                            <div class="form-group col-sm-12">
                                <label class="control-label"><?php echo $translator->translate('Playground address') ?></label>


                                <div class="input-group playground-group">
                                    <span class="input-group-addon map_trigger">
                                        <a  href="#">
                                            <i class="ico ico-map-marker"></i>
                                        </a>
                                    </span>
                                    <input name="locality_name" id="locality-search" class="form-control" type="text" value="<?php echo (null != $locality) ? $locality->getName() : ''  ?>" placeholder="<?php echo $translator->translate('Adress, City name, street, etc.') ?>">
                                </div>
                                <input type="hidden" name="locality_json_data" value="" id="locality_json_data" />
                               

                              

                                <?php echo $validator->showError("playground_empty") ?>
                            </div>

                            <div class="col-sm-12">
                                <div id="map_wrap">
                                    <div id="map"></div>
                                </div>
                            </div>
                        </div>



                        <a data-switch-text="<?php echo $translator->translate('Hide detail') ?>" class="color-link team-more-detail-trigger" role="group" href="#team-more-detail">
                            <i class="ico ico-plus-green"></i> <span><?php echo $translator->translate('More description') ?></span>
                        </a>
                        <input id="team_detail_status" type="hidden" name="show_detail" value="<?php echo $request->get('show_detail') == 'yes' ? 'yes' : 'no' ?>" />

                        <div id="team-more-detail" class="panel-collapse collapse <?php echo $request->get('show_detail') == 'yes' ? 'in' : 'out' ?>">
                            <div class="row">
                                <div class="form-group col-sm-4">
                                    <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("status")) ?></label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="ico ico-space"></i>
                                        </div>
                                        <?php echo $form->renderSelectTag("status", array('class' => 'form-control')) ?>
                                    </div>
                                    <?php echo $validator->showError("status") ?>
                                </div>


                                <div class="form-group col-sm-4">
                                    <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("email")) ?></label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="ico ico-envelope"></i>
                                        </div>
                                        <?php echo $form->renderInputTag("email", array('class' => 'form-control')) ?>
                                    </div>
                                    <?php echo $validator->showError("email") ?>
                                </div>

                                
                                <div class="form-group col-sm-4">
                                   <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("phone")) ?></label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="ico ico-mobil"></i>
                                        </div>
                                        <?php echo $form->renderInputTag("phone", array('class' => 'form-control numeric-only')) ?>
                                    </div>
                                   <?php echo $validator->showError("phone") ?>
                                </div>
                                
                                <div class="form-group col-sm-6">
                                    <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("full_address")) ?></label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="ico ico-map-marker"></i>
                                        </div>
                                       <?php echo $form->renderInputTag("full_address", array('class' => 'form-control')) ?>
                                    </div>
                                   <?php echo $validator->showError("full_address") ?>
                                </div> 
                                
                                 <div class="form-group col-sm-6">
                                      <a class="add_playground_modal_trigger color-link pull-right"  href="#" type="button"><i class="ico ico-plus-green"></i> <?php echo $translator->translate('Add Playground') ?></a>
                                    <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("Playground")) ?></label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="ico ico-playground"></i>
                                        </div>
                                        <?php echo $form->renderSelectTag("exist_playgrounds", array('class' => 'form-control', 'id' => 'playground_choice')) ?>
                                    </div>
                                    <?php echo $validator->showError("exist_playgrounds") ?>
                                </div>
                                
                                
                                
                            </div>
                            <div class="row">
                                 <div class="form-group col-sm-4">
                                     <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("photo")) ?></label>
                                     <div class="row">
                                         <div class="col-sm-12">
                                          <div id="exist-photo-wrap"  style="background-image: url('<?php echo $form->getEntity()->getDefaultPhoto() ?>');">
                                                    <?php echo $form->renderInputHiddenTag('photo') ?>
                                                   
                                                    
                                                </div>
                                         
                                      
                                              <div id="photo-action">
                                                <a class="edit-team-photo color-link" href=""><?php echo $translator->translate('Change image') ?></a>
                                                

                                                
                                                <?php if ($form->getEntity()->getPhoto()  != null && $form->getEntity()->getPhoto() != $form->getEntity()->getDefaultPhoto()): ?>
                                                    <a class="delete-team-photo" href="<?php echo $router->link('team_settings_remove_photo', array('team_id' => $form->getEntity()->getId())) ?>"><i class="fa fa-times"></i></a>
                                                <?php endif; ?>
                                            </div>
                                         </div>
                                         
                                     </div>
                                </div>


                                <div class="form-group col-sm-8">
                                    <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("description")) ?><span class="help"><?php echo $translator->translate('Max. 1000 characters') ?></span></label>

                                    <?php echo $form->renderTextareaTag("description", array('class' => 'form-control')) ?>
                                    <?php echo $validator->showError("description") ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-12 text-right">
                                <button class="btn btn-primary btn-wizard-next" type="submit"> <span><?php echo $translator->translate('Save') ?></span>   </button>&nbsp;
                            </div>
                        </div>

                    </form>
                      <?php $layout->includePart(MODUL_DIR . '/Team/view/team/_edit_photo_modal.php', array('form' => $form)) ?>
                      <?php $layout->includePart(MODUL_DIR . '/Team/view/team/_other_sport_modal.php') ?>
                      <?php $layout->includePart(MODUL_DIR . '/TeamPlayground/view/crud/add_modal.php') ?>
                    
                    
                </div>
            </div>
        </div>
    </div>
</section>

<?php $layout->addStylesheet('plugins/autocomplete/styles.css') ?>
<?php $layout->addJavascript('plugins/autocomplete/jquery.autocomplete.js') ?>


<?php $layout->startSlot('javascript') ?>


<?php $layout->addStylesheet('plugins/datetimepicker/css/bootstrap-datetimepicker.min.css') ?>
<?php $layout->addJavascript('plugins/datetimepicker/js/bootstrap-datetimepicker.min.js') ?>
<?php $layout->addJavascript('js/TeamManager.js') ?>
<?php $layout->addJavascript('js/TournamentManager.js') ?>
<?php $layout->addJavascript('js/PlaygroundManager.js') ?>
<script src="/dev/plugins/select2/select2.full.min.js"></script>
<script src="/dev/plugins/fileupload/jquery.iframe-transport.js"></script>
<script src="/dev/plugins/fileupload/jquery.fileupload.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXU1duK54f1gIsRy_xc7z4uQURovsZdE8&libraries=places&callback=initLocalityManager" async defer></script>
<script type="text/javascript">

    var tournament_manager = new TournamentManager();
    tournament_manager.bindTriggers();
    
    $(".datetime-picker").datetimepicker({format: 'dd.mm.yyyy hh:ii',autoclose:true});
    
  
    
    $('#team_sport_id').on('change',function(e){

        
        if( $(this).find(":selected").hasClass('other-sports-choices'))
        {
           
             
        }
        
       
    });
    
    
    var playground_manager = new PlaygroundManager();
    //team_manager.init();
    
/*
    $(".select2").select2();
    */
    function initLocalityManager()
    {
        tournament_manager.google = google;
        tournament_manager.initMap();
        tournament_manager.createSearchInput();
        /*
        playground_manager.google = google;
        playground_manager.initModalCreate();
        */
    }
    
    
 

</script>

<?php $layout->endSlot('javascript') ?>