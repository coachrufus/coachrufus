

<section class="content-header">
    <h1>
        <?php echo $translator->translate('tournament.dashboard.title') ?>
    </h1>

    <?php $layout->includePart(GLOBAL_DIR . '/templates/parts/_breadcrumb.php', array('crumbs' => array('tournament.bradcrumb.title' => $router->link('tournament')))) ?>
</section>
<section class="content">
    Zápasy v kvalifikačných skupinách o postup na krajský turnaj sú ukončené.
</section>



