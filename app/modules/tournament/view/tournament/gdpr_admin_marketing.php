<div id="full-window">
    <div class="full-window-cnt">
    <div class="row gdpr-marketing-window">
        <div class="col-sm-12">
            
            <p><?php echo $translator->translate('gdpr.tournament.admin.marketing.agreeText') ?></p>
            <form action="" method="post">
                <input type="hidden" name="form_act" value="tournament_admin_marketing" />
               
                <button name="tournament_admin_marketing" value="2" class="btn  agree-clear" type="submit"><?php echo $translator->translate('gdpr.admin.marketing.skip') ?></button>
                 <button name="tournament_admin_marketing" value="1" class="btn  agree-clear agree-primary" type="submit"><?php echo $translator->translate('gdpr.admin.marketing.agree') ?></button>
            </form>

        </div>
    </div>
    </div>
</div>