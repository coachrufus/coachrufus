
<?php if ($request->hasFlashMessage('tournament_invitation_accept_error')): ?>
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <?php echo $request->getFlashMessage('tournament_invitation_accept_error') ?>
    </div>
<?php endif; ?>




<section class="content-header">
    <h1>
        <?php echo $tournament->getName() ?>
    </h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body text-center">
                    
                    <?php if(count($teams) == 0): ?>
                        <div class="alert">
                            <?php echo $translator->translate('tournament.invitation.noTeamAlert') ?>
                        </div>
                     <a class="btn btn-primary" href="<?php echo $router->link('team_create') ?>"> <?php echo $translator->translate('tournament.invitation.createTeam') ?></a>
                    <?php else: ?>
                        <?php foreach ($teams as $team): ?>
                           <div class="box box-widget collapsed-box">
                               <div class="box-header with-border">
                                   <span class="username">
                                       <a href="<?php echo $router->link('team_public_players_list',array('team_id' => $team->getId())) ?>"> <?php echo $team->getName() ?> </a>
                                       (<?php echo $translator->translate($team->getGender())  ?>)
                                   </span>
                                   
                                   <?php if($team->getLocalityId() == null): ?>
                                   <br />
                                       <?php echo $translator->translate('tournament.invitation.noAddress') ?><br />
                                       <a href="<?php echo $router->link('team_settings',array('team_id' => $team->getId())) ?>"><?php echo $translator->translate('tournament.invitation.addAddress') ?></a>
                                  
                                   <?php elseif($team->getGender() != $tournament->getGender()): ?><br />
                                       <?php echo $translator->translate('tournament.invitation.badGender') ?>
                                    <?php else: ?>
                                       
                                       <?php if(array_key_exists($team->getId(), $tournamentTeamsIndexed)): ?>
                                        <br />
                                         <?php if($tournamentTeamsIndexed[$team->getId()] == 'confirmed'): ?>
                                            <?php echo $translator->translate('tournament.invitation.alreadySigned') ?>
                                         <?php endif; ?>
                                       
                                         <?php if($tournamentTeamsIndexed[$team->getId()] == 'waiting-to-confirm'): ?>
                                             <?php echo $translator->translate('tournament.invitation.waiting') ?>
                                         <?php endif; ?>
                                       
                                         <?php if($tournamentTeamsIndexed[$team->getId()] == 'decline'): ?>
                                             <?php echo $translator->translate('tournament.invitation.decline') ?>
                                         <?php endif; ?>
                                       
                                       
                                      
                                        <?php else: ?>
                                        <a class="btn btn-invitation-join-btn" href="<?php echo $router->link('tournament_accept_invitation',array('hash' => $tournament->getShareLinkHash(), 'th' => $team->getShareLinkHash()  )) ?>">
                                           <?php echo $translator->translate('tournament.invitation.signup') ?>
                                       </a>
                                        <?php endif; ?>
                                       
                                       
                                       
                                       
                                    <?php endif; ?>
                                   
                                   
                                  
                               </div><!-- /.user-block -->
                           </div><!-- /.box-header -->
                       <?php endforeach; ?>
                           <a class="btn btn-primary" href="<?php echo $router->link('team_create') ?>"> <?php echo $translator->translate('tournament.invitation.createTeam') ?></a>
                    <?php endif ?>
                    
                   
                </div>

               
            </div>
        </div>
    </div> 
    
    <div class="col-md-4">

    </div>
</div>


</section>




