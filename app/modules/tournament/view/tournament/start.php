<section class="content-header">
    <h1>
        <?php echo $translator->translate('tournament.dashboard.title') ?>
    </h1>

    <?php $layout->includePart(GLOBAL_DIR . '/templates/parts/_breadcrumb.php', array('crumbs' => array('tournament.bradcrumb.title' => $router->link('tournament')))) ?>
</section>
<section class="content tournament-start text-center">
    

    <a class="btn btn-primary" href="<?php echo $router->link('tournament_dashboard',array('t' => 'me')) ?>"><?php echo $translator->translate('tournament.start.meTournaments') ?></a>
    <a class="btn btn-primary" href="<?php echo $router->link('tournament_dashboard',array('t' => 'public')) ?>"><?php echo $translator->translate('tournament.start.publicTournaments') ?></a>
    
   

</section>



