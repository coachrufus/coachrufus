<section class="content-header">
    <h1>
        <?php echo $translator->translate('tournament.invitation.success.title') ?>
    </h1>

    <?php $layout->includePart(GLOBAL_DIR . '/templates/parts/_breadcrumb.php', array('crumbs' => array('tournament.bradcrumb.title' => $router->link('tournament')))) ?>
</section>
<section class="content">
<div class="invitation-success">

            <div class="alert alert-success">
                <?php echo $translator->translate('tournament.invitation.success.text') ?>
            </div>

</div>
</section>



