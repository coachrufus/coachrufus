 <div class="panel-body  panel-full-body">
                  <h1><?php echo $event->getName() ?> </h1>
                  <span class="mute"> <?php echo $event->getStart()->format('d.m.Y H:i')  ?> - <?php echo $event->getEnd()->format('H:i')  ?></span> 
                  <div class="row">
                     <div class="col-sm-3 col-bord col-xs-6"> <i class="ico ico-event-cal"></i> <strong><?php echo $event->getStart()->format('d') ?></strong> <?php echo substr($translator->translate($event->getStart()->format('F')),0,3) ?> <?php echo $event->getStart()->format('Y') ?>                    </div>
                     <div class="col-sm-3 col-bord col-xs-6"> <i class="ico ico-event-clock"></i> <strong><?php echo $event->getStart()->format('H:i'); ?> </strong> <?php echo $translator->translate($event->getStart()->format('l')) ?> </div>
                     <div class="col-sm-3 col-bord col-xs-6">
                        <i class="ico ico-event-att"></i> 
                        <div class="members-attendance  members-attendance-76164bfe749ce2885c6381873258c7ff"> 
                            <strong class="members-attendance-text" style="left: 150%;"><?php echo $event->getNominationCount() ?></strong> </div>
                        <?php echo $translator->translate('tournament.detail.top.nominate') ?>
                     </div>
                     <div class="col-sm-3 col-xs-6"> <i class="ico ico-event-score"></i> 
                         <strong>
                             <?php if($event->getMatch()->getStatus() == 'closed'): ?>
                             <?php echo $matchStats['first_line']['goals'] ?>:<?php echo $matchStats['second_line']['goals'] ?>
<?php else: ?>
                             0:0
<?php endif; ?>
                             
                             
                         </strong> 
                             <?php echo $translator->translate('Score') ?> </div>
                  </div>
                  <h4 id="event-playground-name"><?php echo $event->getPlaygroundName() ?></h4>

                  
                
               </div>
