<div class="modal fade" id="confirm_match_reset_modal" tabindex="-1" role="dialog" aria-labelledby="<?php echo $translator->translate('tournament.match.reset.title') ?>">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $translator->translate('tournament.match.reset.title') ?></h4>
      </div>
      <div class="modal-body">
          

            <div class="alert alert-warning"><?php echo $translator->translate('tournament.match.reset.body') ?></div>
            <a href="" class="btn btn-primary modal_submit"><?php echo $translator->translate('tournament.match.reset.button_yes') ?></a>

      </div>
        
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $translator->translate('Cancel') ?></button>
      </div>
    </div>
  </div>
</div>

