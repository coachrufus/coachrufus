 <?php Core\Layout::getInstance()->startSlot('tournament_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/tournament/view/tournament/_menu.php', array('tournament' => $tournament)) ?>
<?php Core\Layout::getInstance()->endSlot('tournament_menu') ?>

<?php Core\Layout::getInstance()->startSlot('crumb') ?> <?php $layout->includePart(GLOBAL_DIR.'/templates/parts/_breadcrumb.php',array( 'crumbs' => array( 'tournament.bradcrumb.title' => $router->link('tournament'), 'tournament.bradcrumb.teams' =>  $router->link('tournament_teams', array('team_id' => $tournament->getId())), 'Edit' => '' ))) ?> <?php Core\Layout::getInstance()->endSlot('crumb') ?>   
<section class="content-header"> <a class="all-event-link" href="<?php echo $router->link('tournament_detail',array('id' =>$tournament->getId() )) ?>"><i class="ico ico-event-back"></i><?php echo $translator->translate('tournament.backlink.schedule') ?></a>   </section>
<section class="content">
   <div class="row">
      <div class="col-md-12">
         <div class="event-detail">
            <div class="panel panel-default">
               <div class="panel-heading">    </div>
              
               
                <?php $layout->includePart(MODUL_DIR . '/tournament/view/event/_detail.php', 
                                array(
                                    'event' => $event, 
                                     'menuActive' => 'attendance',
                                     'matchStats' => $matchStats
                                )) ?>
               
              
               <div class="row">
                   <div class="col-sm-12">
                         <div class="print-lineup-wrap">
                    <p><?php echo $translator->translate('Na zapisovanie skóre si môžete vytlačiť predlohu a nahodiť ho do aplikácie neskôr') ?></p>
                    <a data-toggle="tooltip" title="<?php echo $translator->translate('Print lineup') ?>" class="print-link print-lineup" href="<?php echo $router->link('tournament_match_print_lineup', array('id' => $event->getId())) ?>"><i class="ico ico-print"></i> <?php echo $translator->translate('Print lineup') ?></a>
                   
                    </div>
                   </div>
               </div>
               
               
               
               <div class="event-attendance" id="detail_event_attendance">
                 
                  <div class="row">
                     <div class="col-sm-6 attendance-group" id="attendance-group-in-players" data-event-limit="2" style="">
                        <h2>
                            
                            <?php echo $team1->getName() ?>
                            
                            <?php if($permissionManager->hasEventPermission($user,$event,'EVENT_NOMINATION') ): ?>
                            <a class="btn btn-green" href="<?php echo $router->link('tournament_team_players',array('id' => $team1->getTournamentId(),'team_id' => $team1->getId())) ?>"> <?php echo $translator->translate('tournament.event.detail.teamEdit') ?></i></a>
                            <?php endif; ?>
                        
                        
                        </h2>
                         
                          
                         <?php if($permissionManager->hasEventPermission($user,$event,'EVENT_NOMINATION') ): ?>
                         <div class="attendant-row">
                           <form action="" method="post" >
                               <input type="hidden" name="tid" value="<?php echo $team1->getId()  ?>" />
                           <div class="pull-left text-left">
                               <?php echo $translator->translate('tournament.nomination.all') ?>
                           </div>
                           <!-- /.user-block -->   
                           
                           <div class="pull-right">
                               <button type="submit" class="in  attendance_trigger_btn accept" name="type" value="in"><i class="ico ico-check"></i></button>
                               <button type="submit" class="out  attendance_trigger_btn deny" name="type" value="out"><i class="ico ico-close"></i></button>
                            </div>
                            </form>
                        </div>
                          <?php endif; ?>
                         
                        
                        <?php foreach($team1->getPlayers() as $player): ?>
                         <div class="attendant-row attendant-row-<?php echo $player->getId()  ?>">
                           <span class="ord-num"> </span> 
                           <div class="user-block pull-left">
                              <div class="round-50" style="background-image:url(<?php echo $player->getMidPhoto() ?>)"></div>
                              <span class="username"><?php echo $player->getFullName()  ?></span> <span class="description">#<?php echo $player->getPlayerNumber() ?> </span> 
                           </div>
                           <!-- /.user-block -->   
                           
                           <div class="pull-right">
                           <?php if($permissionManager->hasEventPermission($user,$event,'EVENT_NOMINATION') ): ?>
                           
                                    <a href="#"
                                       data-url="<?php echo $router->link('tournament_event_change_nomination',array('id' => $event->getId())) ?>" 
                                       data-tournament="<?php echo $tournament->getId()  ?>" 
                                       data-team-id="<?php echo $team1->getId()  ?>" 
                                       data-rel="in" 
                                       data-event="<?php echo $event->getId() ?>" 
                                       data-team-player="<?php echo $player->getId() ?>" 
                                       data-team-position="first_line" 
                                       class="in attendance_trigger attendance_trigger_btn <?php echo (array_key_exists($player->getId(), $team1->getEventNominationIn() )) ? 'accept' : '' ?>">
                                       <i class="ico ico-check"></i></a>

                                    <a href="#"  
                                       data-url="<?php echo $router->link('tournament_event_change_nomination',array('id' => $event->getId())) ?>" 
                                       data-tournament="<?php echo $tournament->getId()  ?>" 
                                       data-team-id="<?php echo $team1->getId()  ?>" 
                                       data-rel="out" 
                                       data-event="<?php echo $event->getId() ?>" 
                                       data-team-player="<?php echo $player->getId() ?>" 
                                       data-team-position="first_line" 
                                       class="out attendance_trigger attendance_trigger_btn   <?php echo (array_key_exists($player->getId(), $team1->getEventNominationOut() )) ? 'deny' : '' ?> ">
                                        <i class="ico ico-close"></i>
                                    </a>
                           
                            <?php else: ?>
                               
                                <?php if(array_key_exists($player->getId(), $team1->getEventNominationIn() )): ?>
                                <span class="in  attendance_trigger_btn accept">
                                       <i class="ico ico-check"></i></span>
                                <?php endif; ?>
                               
                               <?php if(array_key_exists($player->getId(), $team1->getEventNominationOut() )): ?>
                                <span class="out  attendance_trigger_btn deny">
                                       <i class="ico ico-close"></i></span>
                                <?php endif; ?>
                               
                            
                            <?php endif; ?>
                            </div>
                            
                        </div>
                        <?php endforeach; ?>
                        
                         
                        
                     </div>
                      
                       <div class="col-sm-6 attendance-group" id="attendance-group-in-players" data-event-limit="2" style="">
                        <h2><?php echo $team2->getName() ?>
                        
                         <?php if($permissionManager->hasEventPermission($user,$event,'EVENT_NOMINATION') ): ?>
                            <a class="btn btn-green" href="<?php echo $router->link('tournament_team_players',array('id' => $team2->getTournamentId(),'team_id' => $team2->getId())) ?>"> <?php echo $translator->translate('tournament.event.detail.teamEdit') ?></a>

                        
                            <?php endif; ?>
                        
                        
                        </h2>
                           
                            <?php if($permissionManager->hasEventPermission($user,$event,'EVENT_NOMINATION')  ): ?>
                        <div class="attendant-row">
                           <form action="" method="post" >
                               <input type="hidden" name="tid" value="<?php echo $team2->getId()  ?>" />
                           <div class="pull-left text-left">
                               <?php echo $translator->translate('tournament.nomination.all') ?>
                           </div>
                           <!-- /.user-block -->   
                           
                           <div class="pull-right">
                               <button type="submit" class="in  attendance_trigger_btn accept" name="type" value="in"><i class="ico ico-check"></i></button>
                               <button type="submit" class="out  attendance_trigger_btn deny" name="type" value="out"><i class="ico ico-close"></i></button>
                            </div>
                            </form>
                        </div>
                            <?php endif; ?>
                        
                        <?php foreach($team2->getPlayers() as $player): ?>
                         <div class="attendant-row attendant-row-<?php echo $player->getId()  ?>">
                           <span class="ord-num"> </span> 
                           <div class="user-block pull-left">
                              <div class="round-50" style="background-image:url(<?php echo $player->getMidPhoto() ?>)"></div>
                              <span class="username"><?php echo $player->getFullName()  ?></span> <span class="description">#<?php echo $player->getPlayerNumber() ?> </span> 
                           </div>
                           
                           
                            
                           <div class="pull-right">
                               <?php if($permissionManager->hasEventPermission($user,$event,'EVENT_NOMINATION') ): ?>
                                    <a href="#"
                                       data-url="<?php echo $router->link('tournament_event_change_nomination',array('id' => $event->getId())) ?>" 
                                       data-tournament="<?php echo $tournament->getId()  ?>" 
                                       data-team-id="<?php echo $team2->getId()  ?>" 
                                       data-rel="in" 
                                       data-event="<?php echo $event->getId() ?>" 
                                       data-team-player="<?php echo $player->getId() ?>" 
                                       data-team-position="second_line" 
                                       class="in attendance_trigger attendance_trigger_btn <?php echo (array_key_exists($player->getId(), $team2->getEventNominationIn() )) ? 'accept' : '' ?>">
                                       <i class="ico ico-check"></i></a>

                                    <a href="#"  
                                       data-url="<?php echo $router->link('tournament_event_change_nomination',array('id' => $event->getId())) ?>" 
                                       data-tournament="<?php echo $tournament->getId()  ?>" 
                                       data-team-id="<?php echo $team2->getId()  ?>" 
                                       data-rel="out" 
                                       data-event="<?php echo $event->getId() ?>" 
                                       data-team-player="<?php echo $player->getId() ?>" 
                                       data-team-position="second_line" 
                                       class="out attendance_trigger attendance_trigger_btn   <?php echo (array_key_exists($player->getId(), $team2->getEventNominationOut() )) ? 'deny' : '' ?> ">
                                        <i class="ico ico-close"></i>
                                    </a>
                            
                            <?php else: ?>
                                <?php if(array_key_exists($player->getId(), $team2->getEventNominationIn() )): ?>
                                <span class="in  attendance_trigger_btn accept">
                                       <i class="ico ico-check"></i></span>
                                <?php endif; ?>
                               
                               <?php if(array_key_exists($player->getId(), $team2->getEventNominationOut() )): ?>
                                <span class="out  attendance_trigger_btn deny">
                                       <i class="ico ico-close"></i></span>
                                <?php endif; ?>
                           
                           
                            <?php endif; ?>
                           </div>
                           <!-- /.user-block -->   
                           
                        </div>
                        <?php endforeach; ?>
                           
                           
                           
                     </div>
                      <div class="col-sm-12 text-center">
                          <br />
                        <a  class="btn btn-green" href="<?php echo $router->link('tournament_event_simple_score', array('id' => $event->getId())) ?>"><?php echo $translator->translate('tournament.schedule.table.simplescore') ?></a>
                      </div>
                  </div>
               </div>
               
               
               
               
            </div>
         </div>
       
      </div>
     
   </div> 
</section>

<?php $layout->addJavascript('js/TournamentEventManager.js') ?>

<?php  $layout->startSlot('javascript') ?>
<script type="text/javascript">

    var event_manager = new TournamentEventManager();
    event_manager.bindTriggers();

</script>
<?php  $layout->endSlot('javascript') ?>