 <?php Core\Layout::getInstance()->startSlot('tournament_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/tournament/view/tournament/_menu.php', array('tournament' => $tournament)) ?>
<?php Core\Layout::getInstance()->endSlot('tournament_menu') ?>

<?php Core\Layout::getInstance()->startSlot('crumb') ?> <?php $layout->includePart(GLOBAL_DIR.'/templates/parts/_breadcrumb.php',array( 'crumbs' => array( 'tournament.bradcrumb.title' => $router->link('tournament'), 'tournament.bradcrumb.teams' =>  $router->link('tournament_teams', array('team_id' => $tournament->getId())), 'Edit' => '' ))) ?> <?php Core\Layout::getInstance()->endSlot('crumb') ?>   
<section class="content-header"> <a class="all-event-link" href="<?php echo $router->link('tournament_detail',array('id' =>$tournament->getId() )) ?>"><i class="ico ico-event-back"></i><?php echo $translator->translate('Back to team') ?></a>   </section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
              
                <div class="panel-body">
                    <form action="" class="form-bordered" method="post" id="edit_event_form">
                        <div class="row">
                            
                            
                           
                            <div class="form-group col-sm-3">
                                <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("team_id")) ?><?php echo $form->getRequiredLabel('team_id') ?></label><br />
                                <h3><?php echo $team1->getName() ?></h3>
                               <?php echo $form->renderInputHiddenTag('team_id') ?>
                                <?php echo $validator->showError("team_id") ?>
                            </div>
                            <div class="form-group col-sm-1">
                                <br />vs
                            </div>
                            <div class="form-group col-sm-3">
                                <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("opponent_team_id")) ?><?php echo $form->getRequiredLabel('opponent_team_id') ?></label><br />
                                <h3><?php echo $team2->getName() ?></h3>
                                <?php echo $form->renderInputHiddenTag('opponent_team_id') ?>
                                <?php echo $validator->showError("opponent_team_id") ?>
                            </div>
                            
                             <div class="form-group col-sm-2">
                                <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("start_datetime")) ?><?php echo $form->getRequiredLabel('start_datetime') ?></label>
                                <?php echo $form->renderInputTag('start_datetime', array('class' => 'form-control  datetime-picker')) ?>
                                <?php echo $validator->showError("start_datetime") ?>
                            </div>
                            
                           <div class="form-group col-sm-3">
                               <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("playground_name")) ?><?php echo $form->getRequiredLabel('playground_name') ?></label><br />
                                <?php echo $form->renderInputTag('playground_name', array('class' => 'form-control')) ?>
                           </div>
                        </div>
<button class="btn btn-primary"><?php echo $translator->translate('Submit') ?></button>
                    
                   
                   

                 

                    </form>
                </div>
            </div>
        </div>
        
    </div>
</section>




<?php $layout->addStylesheet('plugins/datetimepicker/css/bootstrap-datetimepicker.min.css') ?>
<?php $layout->addJavascript('plugins/datetimepicker/js/bootstrap-datetimepicker.min.js') ?>



<?php  $layout->startSlot('javascript') ?>
<script type="text/javascript">

$(".datetime-picker").datetimepicker({format: 'dd.mm.yyyy hh:ii',autoclose:true});
    
</script>
<?php  $layout->endSlot('javascript') ?>




