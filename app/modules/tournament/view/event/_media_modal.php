<div class="modal fade" id="add_tournament_event_media_modal" tabindex="-1" role="dialog" aria-labelledby="<?php echo $translator->translate('tournament.event.modal.media') ?>">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?php echo $translator->translate('tournament.event.modal.media') ?></h4>
            </div>
            <div class="modal-body">
                <form role="form" id="add_tournament_event_media_form" action="<?php echo $router->link('tournament_event_add_media') ?>">
                    <input type="hidden" id="tournament_event_id" name="event_id" value="" />


                    <div class="form-group col-sm-12">
                        <label for="media_code"><?php echo $translator->translate('tournament.event.modal.media.code') ?></label>
                        <textarea name="code" id="media_code" class="form-control" placeholder="Code"></textarea>
                    </div>
                     
                    <div class="form-group col-sm-12">
                        <label for="media_link"><?php echo $translator->translate('tournament.event.modal.media.link') ?></label>
                        <input name="link" type="text" class="form-control" id="media_link" placeholder="Link">
                    </div>

                <div class="form-group col-sm-12">
                    <button type="submit" class="btn btn-primary"><?php echo $translator->translate('Save') ?></button>
                </div>



                </form>
            </div>
            <div class="modal-footer">
               
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $translator->translate('Cancel') ?></button>
            </div>
        </div>
    </div>
</div>

