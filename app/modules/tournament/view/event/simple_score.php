 <?php Core\Layout::getInstance()->startSlot('tournament_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/tournament/view/tournament/_menu.php', array('tournament' => $tournament)) ?>
<?php Core\Layout::getInstance()->endSlot('tournament_menu') ?>

<?php Core\Layout::getInstance()->startSlot('crumb') ?> <?php $layout->includePart(GLOBAL_DIR.'/templates/parts/_breadcrumb.php',array( 'crumbs' => array( 'tournament.bradcrumb.title' => $router->link('tournament'), 'tournament.bradcrumb.teams' =>  $router->link('tournament_teams', array('team_id' => $tournament->getId())), 'Edit' => '' ))) ?> <?php Core\Layout::getInstance()->endSlot('crumb') ?>   
<section class="content-header"> <a class="all-event-link" href="<?php echo $router->link('tournament_detail',array('id' =>$tournament->getId() )) ?>"><i class="ico ico-event-back"></i><?php echo $translator->translate('Back to team') ?></a>   </section>
<section class="content">
   <div class="row">
      <div class="col-md-12">
         <div class="event-detail">
            <div class="panel panel-default">
               <div class="panel-heading">    </div>
              
               
                <?php $layout->includePart(MODUL_DIR . '/tournament/view/event/_detail.php', 
                                array(
                                    'event' => $event, 
                                     'menuActive' => 'score',
                                     'matchStats' => $matchStats
                                )) ?>
               
          
                  <div class="row">
                      <form action=""  method="post">

                        <div class="col-sm-5" >
                           <h2><?php echo $team1->getName() ?></h2>
                           <table class="table table-condensed">
                               <thead>
                                   <tr>
                                       <th><?php echo $translator->translate('tournament.score.simple.playerNumber') ?></th>
                                       <th><?php echo $translator->translate('tournament.score.simple.playerName') ?></th>
                                       <th><?php echo $translator->translate('tournament.score.simple.goals') ?></th>
                                       <th><?php echo $translator->translate('tournament.score.simple.assists') ?></th>
                                       <th><?php echo $translator->translate('tournament.score.simple.mom') ?></th>
                                   </tr>
                               </thead>
                               <tbody>
                                   <?php foreach ($team1->getEventNominationIn() as $player): ?>
                                   <tr>
                                       <td class="nomination_player_number"> 
                                           <?php echo $player->getPlayerNumber() ?>
                                       </td>
                                       <td> 
                                           <?php echo $player->getPlayerName() ?>
                                       </td>
                                       <td class="nomination_goals">
                                           <input type="number" name="goals[<?php echo $team1->getId() ?>][<?php echo $player->getId() ?>][points]" value="<?php echo $points['goals'][$team1->getId()][$player->getId()] ?>" />
                                           <input type="hidden" name="goals[<?php echo $team1->getId() ?>][<?php echo $player->getId() ?>][lineup]" value="<?php echo $player->getLineupPosition() ?>" />
                                       </td>
                                       <td class="nomination_asssits">
                                           <input type="number" name="assists[<?php echo $team1->getId() ?>][<?php echo $player->getId() ?>][points]" value="<?php echo $points['assists'][$team1->getId()][$player->getId()] ?>" />
                                           <input type="hidden" name="assists[<?php echo $team1->getId() ?>][<?php echo $player->getId() ?>][lineup]" value="<?php echo $player->getLineupPosition() ?>" />
                                       </td>
                                       <td>
                                           <input type="radio" <?php echo ($points['mom'][$team1->getId()][$player->getId()] == 1) ? 'checked="checked"' : ''  ?> name="mom" value="<?php echo $team1->getId() ?>-<?php echo $player->getId() ?>-<?php echo $player->getLineupPosition() ?>" />
                                       </td>
                                   </tr>
                                <?php endforeach; ?>
                               </tbody>
                           </table>
                           
                            <?php if(count($team1->getEventNominationIn()) == 0): ?>
                            <div class="col-sm-12 text-center alert alert-danger">
                                <?php echo $translator->translate('tournament.score.simple.empty') ?>
                            </div>
                            <?php endif; ?>
                           
                        </div>
                          <div class="col-sm-2">
                              <h2><?php echo $matchOverview->getFirstLineGoals() ?>:<?php echo $matchOverview->getSecondLineGoals() ?></h2>
                          </div>
                          
                           <div class="col-sm-5" >
                           <h2><?php echo $team2->getName() ?></h2>
                           <table class="table table-condensed">
                               <thead>
                                   <tr>
                                       <th><?php echo $translator->translate('tournament.score.simple.playerNumber') ?></th>
                                       <th><?php echo $translator->translate('tournament.score.simple.playerName') ?></th>
                                       <th><?php echo $translator->translate('tournament.score.simple.goals') ?></th>
                                       <th><?php echo $translator->translate('tournament.score.simple.assists') ?></th>
                                       <th><?php echo $translator->translate('tournament.score.simple.mom') ?></th>
                                   </tr>
                               </thead>
                               <tbody>
                                   <?php foreach ($team2->getEventNominationIn() as $player): ?>
                                   <tr>
                                       <td class="nomination_player_number"> 
                                           <?php echo $player->getPlayerNumber() ?>
                                       </td>
                                       <td> 
                                           <?php echo $player->getPlayerName() ?>
                                       </td>
                                       <td class="nomination_goals">
                                           <input type="number" name="goals[<?php echo $team2->getId() ?>][<?php echo $player->getId() ?>][points]" value="<?php echo $points['goals'][$team2->getId()][$player->getId()] ?>" />
                                           <input type="hidden" name="goals[<?php echo $team2->getId() ?>][<?php echo $player->getId() ?>][lineup]" value="<?php echo $player->getLineupPosition() ?>" />
                                       </td>
                                       <td class="nomination_asssits">
                                           <input type="number" name="assists[<?php echo $team2->getId() ?>][<?php echo $player->getId() ?>][points]" value="<?php echo $points['assists'][$team2->getId()][$player->getId()] ?>" />
                                           <input type="hidden" name="assists[<?php echo $team2->getId() ?>][<?php echo $player->getId() ?>][lineup]" value="<?php echo $player->getLineupPosition() ?>" />
                                       </td>
                                       <td>
                                           <input type="radio" <?php echo ($points['mom'][$team2->getId()][$player->getId()] == 1) ? 'checked="checked"' : ''  ?> name="mom" value="<?php echo $team2->getId() ?>-<?php echo $player->getId() ?>-<?php echo $player->getLineupPosition() ?>" />
                                       </td>
                                   </tr>
                                <?php endforeach; ?>
                               </tbody>
                           </table>
                           
                            <?php if(count($team2->getEventNominationIn()) == 0): ?>
                            <div class="col-sm-12 text-center alert alert-danger">
                                <?php echo $translator->translate('tournament.score.simple.empty') ?>
                            </div>
                            <?php endif; ?>
                        </div>
                          
                         
                          
                          
                          
                          <div class="col-sm-12 text-center">
                               <p class="overtime-wrap">
                                   <input <?php echo ($inOvertime) ? 'checked="checked"' : '' ?> id="overtime" type="checkbox" name="overtime" value="1" /> <?php echo $translator->translate('tournament.score.overtime') ?>
                                    </p>
                         <button class="btn btn-primary" type="submit"><?php echo $translator->translate('tournament.score.simple.sendButton') ?></button>
                          </div>
                      </form>
                  </div>
               </div>
            </div>
         </div>
       
      </div>
     
   </div> 
</section>

<?php $layout->addJavascript('js/TournamentEventManager.js') ?>

<?php  $layout->startSlot('javascript') ?>
<script type="text/javascript">

    var event_manager = new TournamentEventManager();
    event_manager.bindTriggers();

</script>
<?php  $layout->endSlot('javascript') ?>