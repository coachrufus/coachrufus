 <?php Core\Layout::getInstance()->startSlot('tournament_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/tournament/view/tournament/_menu.php', array('tournament' => $tournament)) ?>
<?php Core\Layout::getInstance()->endSlot('tournament_menu') ?>
<?php Core\Layout::getInstance()->startSlot('crumb') ?> 
<?php $layout->includePart(GLOBAL_DIR . '/templates/parts/_breadcrumb.php', array('crumbs' => array('tournament.bradcrumb.title' => $router->link('tournament'), 'tournament.bradcrumb.teams' => $router->link('tournament_teams', array('team_id' => $tournament->getId())), 'Edit' => ''))) ?> 
<?php Core\Layout::getInstance()->endSlot('crumb') ?>   

<section class="content-header"> <a class="all-event-link" href="<?php echo $router->link('tournament_detail', array('id' => $tournament->getId())) ?>"><i class="ico ico-event-back"></i><?php echo $translator->translate('Back to team') ?></a>   </section>



<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="event-detail">
                <div class="panel panel-default rating-panel">
                    <?php
                        $layout->includePart(MODUL_DIR . '/tournament/view/event/_detail.php', array(
                            'event' => $event,
                            'menuActive' => 'score',
                             'matchStats' => $matchStats
                        ))
                    ?>

                    <div class="row panel-timeline winner-<?php echo $matchResult['winner'] ?>">
                        <div class="team_row team_row_first_line">
                            <strong><?php echo $lineup->getFirstLineName() ?></strong>
                            <span id="first_line_goals"><?php echo $matchOverview->getFirstLineGoals() ?></span>
                        </div>
                        <div class="team_row_colon">:</div>
                        <div class="team_row team_row_second_line">
                            <span  id="second_line_goals"><?php echo $matchOverview->getSecondLineGoals() ?></span>
                            <strong><?php echo $lineup->getSecondLineName() ?></strong>
                        </div>
                    </div>

                    <div class="row">
                        <table class="table table-striped table-rating table-match-stat">
                            <thead>
                                <tr>
                                    <th class="player"></th>
                                    <th class="goal">G</th>
                                    <th  class="assist">A</th>
                                    <th class="mom">MoM (<?php echo $translator->translate('Most valuable player') ?>)</th>
                                </tr>
                            </thead>   

                            <tbody>

                                <?php foreach ($matchOverview->getPlayersStats() as $lineupPlayerId => $playerStat): ?>
                                   
                                    <tr>
                                        <td>
                                            <div class="user-block">
                                                <span class="username"><?php echo $playerStat['name'] ?></span>
                                            </div><!-- /.user-block -->
                                        </td>
                                        <td><?php echo $playerStat['goals'] ?></td>
                                        <td><?php echo $playerStat['assist'] ?></td>
                                        <td><?php echo (1 == $playerStat['mom']) ? 'M' : '' ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>


                        <ul class="timeline" id="stat_timeline">
                            <li class="start-label"><?php echo $translator->translate('Timeline') ?></li>
                            <?php foreach ($eventTimeline as $groupId => $group): ?>
                                <?php
                                $layout->includePart(MODUL_DIR . '/tournament/view/event/_timeline_event_row.php', array(
                                    'group' => $group,
                                    'lineup' => $lineup,
                                    'event' => $event,
                                    'players' => $players,
                                    'groupId' => $groupId
                                ))
                                ?>
                            <?php endforeach; ?>
                        </ul>
                         <?php if ($permissionManager->hasEventPermission($user, $event, 'EVENT_LIVESCORE')): ?>
                        <a class="btn btn-clear-green" href="<?php echo $router->link('tournament_event_simple_score',array('id' => $event->getId())) ?>"><?php echo $translator->translate('tournament.score.edit') ?></a>
                        
                        
                        
                        <a class="btn btn-danger reset-match-trigger" href="<?php echo $router->link('tournament_event_reset_match',array('id' => $event->getId(),'tid' =>$event->getTournamentId() )) ?>"><?php echo $translator->translate('tournament.score.resetMatch') ?></a>
                        
                        <?php endif ?>
                    </div>

                </div>
            </div>
        </div>
     
    </div>
</section>

<?php $layout->includePart(MODUL_DIR . '/tournament/view/event/_confirm_match_reset_modal.php') ?>

<?php $layout->addJavascript('js/TournamentEventManager.js') ?>

<?php  $layout->startSlot('javascript') ?>
<script type="text/javascript">

    var event_manager = new TournamentEventManager();
    event_manager.bindTriggers();

</script>
<?php  $layout->endSlot('javascript') ?>

