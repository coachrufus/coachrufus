 <?php Core\Layout::getInstance()->startSlot('tournament_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/tournament/view/tournament/_menu.php', array('tournament' => $tournament)) ?>
<?php Core\Layout::getInstance()->endSlot('tournament_menu') ?>
<?php Core\Layout::getInstance()->startSlot('crumb') ?> 
<?php $layout->includePart(GLOBAL_DIR . '/templates/parts/_breadcrumb.php', array('crumbs' => array('tournament.bradcrumb.title' => $router->link('tournament'), 'tournament.bradcrumb.teams' => $router->link('tournament_teams', array('team_id' => $tournament->getId())), 'Edit' => ''))) ?> 
<?php Core\Layout::getInstance()->endSlot('crumb') ?>   

<section class="content-header"> <a class="all-event-link" href="<?php echo $router->link('tournament_detail', array('id' => $tournament->getId())) ?>"><i class="ico ico-event-back"></i><?php echo $translator->translate('Back to team') ?></a>   </section>


<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="event-detail">
                <div class="panel panel-default ">
                    <?php
                    $layout->includePart(MODUL_DIR . '/tournament/view/event/_detail.php', array(
                        'event' => $event,
                        'menuActive' => 'score',
                        'matchStats' => $matchStats
                    ))
                    ?>


                    <!-- timeline time label -->

                    <div class="row panel-timeline winner-<?php echo $matchResult['winner'] ?>">
                        <div class="team_row team_row_first_line">
                            <strong><?php echo $team1->getName() ?></strong>
                            <span id="first_line_goals"><?php echo $matchOverview->getFirstLineGoals() ?></span>
                        </div>
                        <div class="team_row_colon">:</div>
                        <div class="team_row team_row_second_line">
                            <span  id="second_line_goals"><?php echo $matchOverview->getSecondLineGoals() ?></span>
                            <strong><?php echo $team2->getName() ?></strong>
                        </div>

                        <div class="start_triggers">
                            <?php if ('closed' != $lineup->getStatus()): ?> 
                                <a href="#" class="start_timeline"><i class="ico ico-event-clock-white"></i> <?php echo $translator->translate('tournament.score.live') ?></a>
                                <p><?php echo $translator->translate('Or') ?></p>
                            <?php endif; ?>
                            <a class="btn btn-clear btn-clear-green add_manual_timeline add_manual_timeline_<?php echo $lineup->getStatus() ?>" href=""><?php echo $translator->translate('tournament.score.manual') ?></a>
                        </div>

                            <?php if ('closed' != $lineup->getStatus()): ?> 
                            <div id="stopwatch-control">
                                <input id="start_timeline_time"  name="" type="text" value="00:00:00" class="form-control" data-inputmask='"mask": "99:99:99"' data-mask>
                                <a  class="time_pause" href="#"><i class="ico ico-pause"></i></a>
                                <a  class="time_start" href="#"><i class="ico ico-arr-rgt-b"></i></a>
                               
                                <div class="periods-button">
                                 <a href="<?php echo $router->link('tournament_event_add_timeline_mmatch_phase', array('event_id' => $event->getId(), 'type' => 'match_phase','phase' => '3-1-end', 'event_date' => $event->getStart()->format('Y-m-d'))) ?>" 
                                    class="add_stat_time_3_1_end match_phases_chain active" 
                                    data-clock-action="pause" data-next="add_stat_time_3_2_start">
                                        <?php echo $translator->translate('First Third End') ?>
                                 </a>
                                 
                                 <a href="<?php echo $router->link('tournament_event_add_timeline_mmatch_phase', array('event_id' => $event->getId(), 'type' => 'match_phase','phase' => '3-2-start', 'event_date' => $event->getStart()->format('Y-m-d'))) ?>" 
                                    class="add_stat_time_3_2_start match_phases_chain" 
                                    data-clock-action="continue" 
                                    data-next="add_stat_time_3_2_end">
                                        <?php echo $translator->translate('Second Third start') ?>
                                 </a>
                                
                                 <a href="<?php echo $router->link('tournament_event_add_timeline_mmatch_phase', array('event_id' => $event->getId(), 'type' => 'match_phase','phase' => '3-2-end', 'event_date' => $event->getStart()->format('Y-m-d'))) ?>" 
                                    class="add_stat_time_3_2_end match_phases_chain" 
                                     data-clock-action="pause" 
                                    data-next="add_stat_time_3_3_start">
                                        <?php echo $translator->translate('Second Third end') ?>
                                 </a>
                                
                                 <a href="<?php echo $router->link('tournament_event_add_timeline_mmatch_phase', array('event_id' => $event->getId(), 'type' => 'match_phase','phase' => '3-3-start', 'event_date' => $event->getStart()->format('Y-m-d'))) ?>" 
                                    class="add_stat_time_3_3_start match_phases_chain" 
                                    data-clock-action="continue" 
                                    data-next="add_stat_time_mom">
                                        <?php echo $translator->translate('Third Third start') ?>
                                 </a>
                                
                            </div>
                                 <a href="#" class="add_stat_time_mom match_phases_chain"><?php echo $translator->translate('Game over') ?></a>
                            </div>
                            <?php endif ?>

                        <ul class="timeline" id="stat_timeline">
                            <div class="timeline-start-item <?php echo (empty($eventTimeline)) ? 'hide' : '' ?>">
                                <?php echo $translator->translate('Start Game') ?>
                                <i class="ico ico-clock2"></i>
                            </div>
                            <?php foreach ($eventTimeline as $groupId => $group): ?>
                                <?php
                                $layout->includePart(MODUL_DIR . '/tournament/view/event/_timeline_event_row.php', array(
                                    'group' => $group,
                                    'lineup' => $lineup,
                                    'event' => $event,
                                    'players' => $players,
                                    'groupId' => $groupId
                                ))
                                ?>
                                <?php endforeach; ?>




                            <li id="edit_timeline_form">
                                <div id="edit_timeline_form_loader" class="alert alert-info text-center"><i class="fa fa-refresh fa-spin fa-3x fa-fw"></i> <br /><?php echo $translator->translate('Saving data...') ?></div>
                                <div class="text-right">
                                    <a class="close_timeline_form"><i class="fa fa-close"></i></a>
                                </div>

                                <form  action="<?php echo $router->link('tournament_event_edit_timeline_stat', array('event_id' => $event->getId(), 'event_date' => $event->getStart()->format('Y-m-d'), 'lid' => $lineup->getId())) ?>">
                                    <div class="input-group input-group-contrast">
                                        <span class="input-group-addon">G</span>
                                        <select class="form-control form-control-contrast player_choice goal_player" name="">
                                            <option  value="0"></option>
                                            <optgroup label="<?php echo $team1->getName() ?>">
                                                 
                                                <?php foreach ($team1->getEventNominationIn() as $player): ?>
                                                    <option 
                                                        data-lid-team="<?php echo $player->getLineupPosition() ?>"  
                                                        data-tid="<?php echo $team1->getId() ?>"
                                                        data-pid="<?php echo $player->getPlayerId() ?>" 
                                                        value="<?php echo $player->getId() ?>">
                                                    <?php echo $player->getPlayerName() ?>
                                                    </option>
                                             <?php endforeach; ?>
                                            </optgroup>
                                            <optgroup label="<?php echo $team2->getName() ?>">
                                                 <option data-lid-team="second_line" data-tid="<?php echo $team2->getId() ?>"    data-pid="" value="0"><?php echo $translator->translate('tournament.score.unknown') ?></option>
<?php foreach ($team2->getEventNominationIn() as $player): ?>
                                                    <option 
                                                        data-lid-team="<?php echo $player->getLineupPosition() ?>"  
                                                        data-tid="<?php echo $team2->getId() ?>"
                                                        data-pid="<?php echo $player->getPlayerId() ?>" 
                                                        value="<?php echo $player->getId() ?>">
                                                    <?php echo $player->getPlayerName() ?>
                                                    </option>
<?php endforeach; ?>
                                            </optgroup>
                                        </select>
                                    </div>

                                    <div class="assist_player_wrap">
                                        <div class="input-group input-group-contrast">
                                            <span class="input-group-addon">A</span>
                                            <select class="form-control form-control-contrast player_choice assist_player" name="">
                                                <option value=""></option>
                                                 <optgroup label="<?php echo $team1->getName() ?>">
                                                     <option data-lid-team="first_line" data-tid="<?php echo $team1->getId() ?>"   data-pid="" value="0"><?php echo $translator->translate('tournament.score.unknown') ?></option>
<?php foreach ($team1->getEventNominationIn() as $player): ?>
                                                        <option 
                                                            data-lid-team="<?php echo $player->getLineupPosition() ?>"  
                                                            data-tid="<?php echo $team1->getId() ?>"
                                                            data-pid="<?php echo $player->getPlayerId() ?>" 
                                                            value="<?php echo $player->getId() ?>">
                                                        <?php echo $player->getPlayerName() ?>
                                                        </option>
<?php endforeach; ?>
                                                </optgroup>
                                                <optgroup label="<?php echo $team2->getName() ?>">
                                                     <option data-lid-team="second_line" data-tid="<?php echo $team2->getId() ?>"  data-pid="" value="0"><?php echo $translator->translate('tournament.score.unknown') ?></option>
<?php foreach ($team2->getEventNominationIn() as $player): ?>
                                                        <option 
                                                            
                                                            data-lid-team="<?php echo $player->getLineupPosition() ?>"  
                                                            data-tid="<?php echo $team2->getId() ?>"
                                                            data-pid="<?php echo $player->getPlayerId() ?>" 
                                                            value="<?php echo $player->getId() ?>">
                                                        <?php echo $player->getPlayerName() ?>
                                                        </option>
<?php endforeach; ?>
                                                </optgroup>
                                            </select>
                                        </div>
                                    </div>
                                </form>
                            </li>



                            <li id="add_timeline_form">
                                <div class="text-right">
                                    <a class="close_timeline_form"><i class="fa fa-close"></i></a>
                                </div>
                                <div id="add_timeline_form_loader" class="alert alert-info text-center"><i class="fa fa-refresh fa-spin fa-3x fa-fw"></i> <br /><?php echo $translator->translate('Saving data...') ?></div>
                                <form  action="<?php echo $router->link('tournament_event_add_timeline_stat', array('event_id' => $event->getId(), 'event_date' => $event->getStart()->format('Y-m-d'), 'lid' => $lineup->getId())) ?>"> 
                                    <input type="text" class="current_hit_time" value="00:00:00" >


                                    <div class="input-group input-group-contrast">
                                        <span class="input-group-addon">G</span>
                                        <select class="form-control form-control-contrast player_choice goal_player" name="">
                                            <option value=""></option>
                                            <optgroup label="<?php echo $team1->getName() ?>">
                                                 
<?php foreach ($team1->getEventNominationIn() as $player): ?>
                                                    <option 
                                                        data-lid-team="<?php echo $player->getLineupPosition() ?>"  
                                                        data-tid="<?php echo $team1->getId() ?>"
                                                        data-pid="<?php echo $player->getPlayerId() ?>" 
                                                        value="<?php echo $player->getId() ?>">
                                                    <?php echo $player->getPlayerName() ?>
                                                    </option>
<?php endforeach; ?>
                                            </optgroup>
                                            <optgroup label="<?php echo $team2->getName() ?>">
                                                 <option data-lid-team="second_line" data-tid="<?php echo $team2->getId() ?>"  data-pid="" value="0"><?php echo $translator->translate('tournament.score.unknown') ?></option>
<?php foreach ($team2->getEventNominationIn() as $player): ?>
                                                    <option 
                                                        data-lid-team="<?php echo $player->getLineupPosition() ?>"  
                                                        data-tid="<?php echo $team2->getId() ?>"
                                                        data-pid="<?php echo $player->getPlayerId() ?>" 
                                                        value="<?php echo $player->getId() ?>">
                                                    <?php echo $player->getPlayerName() ?>
                                                    </option>
<?php endforeach; ?>
                                            </optgroup>

                                        </select>
                                    </div>

                                    <div class="assist_player_wrap">
                                        <div class="input-group input-group-contrast">
                                            <span class="input-group-addon">A</span>
                                            <select class="form-control form-control-contrast player_choice assist_player" name="">
                                                <option  value="0"></option>
                                               
                                                <optgroup label="<?php echo $team1->getName() ?>">
                                                     <option data-lid-team="first_line" data-tid="<?php echo $team1->getId() ?>"  data-pid="" value="0"><?php echo $translator->translate('tournament.score.unknown') ?></option>
<?php foreach ($team1->getEventNominationIn() as $player): ?>
                                                        <option 
                                                            data-lid-team="<?php echo $player->getLineupPosition() ?>"  
                                                            data-tid="<?php echo $team1->getId() ?>"
                                                            data-pid="<?php echo $player->getPlayerId() ?>" 
                                                            value="<?php echo $player->getId() ?>">
                                                        <?php echo $player->getPlayerName() ?>
                                                        </option>
<?php endforeach; ?>
                                                </optgroup>
                                                <optgroup label="<?php echo $team2->getName() ?>">
                                                     <option data-lid-team="second_line" data-tid="<?php echo $team2->getId() ?>"  data-pid="" value="0"><?php echo $translator->translate('tournament.score.unknown') ?></option>
<?php foreach ($team2->getEventNominationIn() as $player): ?>
                                                        <option 
                                                            data-lid-team="<?php echo $player->getLineupPosition() ?>"  
                                                            data-tid="<?php echo $team2->getId() ?>"
                                                            data-pid="<?php echo $player->getPlayerId() ?>" 
                                                            value="<?php echo $player->getId() ?>">
                                                        <?php echo $player->getPlayerName() ?>
                                                        </option>
<?php endforeach; ?>
                                                </optgroup>
                                            </select>
                                        </div>
                                    </div>
                                </form>

                            </li>


                            <li id="add_timeline_mom_form">
                                <div id="mom_timeline_form_loader" class="alert alert-info text-center"><i class="fa fa-refresh fa-spin fa-3x fa-fw"></i> <br />
                                    <?php echo $translator->translate('Saving data...') ?>
                                </div>
                                <?php echo $translator->translate('Man of match') ?>
                                <select class="form-control form-control-contrast player_choice" name="">
                                    <option data-lid-team="none"  data-pid="" value="0"><?php echo $translator->translate('tournament.score.mom.nobody') ?></option>
                                    
                                    
                                    <optgroup label="<?php echo $team1->getName() ?>">
                                    <?php foreach ($team1->getEventNominationIn() as $player): ?>
                                                        <option 
                                                            data-lid-team="<?php echo $player->getLineupPosition() ?>"  
                                                            data-pid="<?php echo $player->getPlayerId() ?>" 
                                                            value="<?php echo $player->getId() ?>">
                                                        <?php echo $player->getPlayerName() ?>
                                                        </option>
                                    <?php endforeach; ?>
                                    </optgroup>
                                    <optgroup label="<?php echo $team2->getName() ?>">
                                    <?php foreach ($team2->getEventNominationIn() as $player): ?>
                                                        <option 
                                                            data-lid-team="<?php echo $player->getLineupPosition() ?>"  
                                                            data-pid="<?php echo $player->getPlayerId() ?>" 
                                                            value="<?php echo $player->getId() ?>">
                                                        <?php echo $player->getPlayerName() ?>
                                                        </option>
                                    <?php endforeach; ?>
                                    </optgroup>
                                    
                                  
                                </select>
                                <a href="#" class="btn btn-danger"><?php echo $translator->translate('Cancel') ?></a>
                                <a href="<?php echo $router->link('tournament_event_add_timeline_mom', array('event_id' => $event->getId(), 'type' => 'hit', 'event_date' => $event->getStart()->format('Y-m-d'))) ?>" class="btn btn-success timeline_mom_trigger">
                                    <?php echo $translator->translate('Save') ?>
                                </a>
                            </li>



                            <!-- /.timeline-label -->






                            <li id="timeline-end"></li>
                            <li class="time-label add_stat_time_event_wrap">
                                <a href="#" class="add_stat_time_event" <?php echo ('closed' != $lineup->getStatus() && !empty($eventTimeline)) ? 'style="display:inline;"' : '' ?>><i class="ico ico-plus-w"></i></a>
                            </li>
<?php if ('closed' != $lineup->getStatus()): ?> 
                                <li class="time-label add_stat_time_event_wrap add_stat_time_event_wrap_mom <?php echo (count($eventTimeline) > 0) ? 'active' : '' ?>">
                                    <a href="#" class="add_stat_time_mom_manual"><?php echo $translator->translate('Game over') ?></a>
                                    <p class="overtime-wrap">
                                   <input id="overtime" type="checkbox" name="overtime" value="1" /> <?php echo $translator->translate('tournament.score.overtime') ?>
                                    </p>
                                </li>
                            <?php endif; ?>

                            <?php if ('closed' == $lineup->getStatus()): ?> 
                                <a  data-toggle="tooltip" data-placement="top" title="<?php echo $translator->translate('Delete all match stats and reopen it') ?>" class="btn btn-danger reset-trigger" href="<?php echo $router->link('team_match_reset_timeline', array('lid' => $lineup->getId())) ?>"><?php echo $translator->translate('Reset Match') ?></a>
<?php endif; ?> 




                        </ul>

                           <?php if(count($team1->getEventNominationIn()) == 0 or count($team2->getEventNominationIn()) == 0): ?>
                            <div class="col-sm-12 text-center alert alert-danger">
                                <?php echo $translator->translate('tournament.score.simple.empty2') ?>
                            </div>
                            <?php endif; ?>

                    </div>

                </div>
            </div>
        </div>
    </div>

</section>


<?php //$layout->includePart(MODUL_DIR . '/Team/view/events/_confirm_single_delete_modal.php', array('event' => $event)) ?>
<?php //$layout->includePart(MODUL_DIR . '/Team/view/events/_confirm_delete_modal.php', array('event' => $event))  ?>



<?php $layout->includePart(MODUL_DIR . '/tournament/view/event/_confirm_save_modal.php') ?> 
<?php //$layout->includePart(MODUL_DIR . '/Team/view/teamMatch/_confirm_delete_modal.php')  ?>



<?php //$layout->includePart(MODUL_DIR . '/Team/view/teamMatch/_confirm_reset_modal.php') ?>
<?php $layout->addJavascript('plugins/stopwatch/stopwatch.js') ?>
<?php $layout->addJavascript('js/TournamenMatchManager.js') ?>
<?php $layout->addJavascript('js/SmallCalendar.js') ?>

<?php $layout->startSlot('javascript') ?>


<script src="/dev/plugins/input-mask/jquery.inputmask.js"></script>
<script type="text/javascript">
    $("[data-mask]").inputmask();

    match_manger = new TournamenMatchManager();
    match_manger.initTimelineTriggers();







</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXU1duK54f1gIsRy_xc7z4uQURovsZdE8&libraries=places&callback=showEventLocation" async defer></script>

<?php $layout->endSlot('javascript') ?>