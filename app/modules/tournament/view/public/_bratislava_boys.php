 <h2>Krajské kolo Bratislavský kraj</h2>
 <table class="table table-condesed">
     <thead>
         <tr>
             <th></th>
             <th></th>
             <th>postupujúci</th>
         </tr>
     </thead>
     <tbody>
         <tr>
             <td>ZŠ Beňovského : ZŠ M. Alexie</td>
             <td>1 : 5</td>
             <td>ZŠ M. Alexie</td>
         </tr>
         <tr>
             <td>ZŠ Nejedlého : ZŠ kpt. Nálepku</td>
             <td>2 : 6</td>
             <td>ZŠ kpt. Nálepku</td>
         </tr>
     </tbody>
     
 </table>
 
 <table class="table table-condesed">
     <thead>
         <tr>
             <th>chlapci 27.3.2019</th>
             <th>Vazovova</th>
             <th>ZŠ ALEXIE</th>
             <th>ZŠ kpt. J Nálepku</th>
             <th>body</th>
             <th>skóre</th>
             <th>umiestnenie</th>
         </tr>
     </thead>
     <tbody>
         <tr>
             <td>Vazovova</td>
             <td>x</td>
             <td>3 ₋ 3 (3 ₋ 4PN)</td>
             <td>3 ₋ 3 (3 ₋ 4PN)</td>
             <td>0</td>
             <td>6 ₋ 6</td>
             <td>3</td>
         </tr>
         <tr>
             <td>ZŠ M. ALEXIE</td>
             <td>3 ₋ 3 (4 ₋ 3PN)</td>
             <td>x</td>
             <td>7 ₋ 4</td>
             <td>6</td>
             <td>10 ₋ 7</td>
             <td>1</td>
         </tr>
         <tr>
             <td>ZŠ kpt. J Nálepku</td>
             <td>3 ₋ 3 (4 ₋ 3PN)</td>
             <td>4 ₋ 7</td>
             <td>x</td>
             <td>3</td>
             <td>7 ₋ 10</td>
             <td>2</td>
         </tr>
       
     </tbody>
     
 </table>
