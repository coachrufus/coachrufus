
<div class="tournament-public-detail">
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" <?php echo ('detail' == $activeTab) ? 'class="active"' : '' ?> >
            <a href="#detail" role="tab" data-toggle="tab"><?php echo $translator->translate('tournament.public.tab.detail') ?></a>
        </li>
        <li role="presentation" <?php echo ('teams' == $activeTab) ? 'class="active"' : '' ?> >
            <a href="#teams"  role="tab" data-toggle="tab"><?php echo $translator->translate('tournament.public.tab.teams') ?></a>
        </li>
    </ul>


  <div class="tab-content">
        <div role="tabpanel" class="tab-pane <?php echo ('detail' == $activeTab) ? 'active' : '' ?>" id="detail">
            <div class="panel">
                <div class="panel-body">
                    <div class="row">
    <div class="col-sm-4 text-center">
        <h1><?php echo $tournament->getName() ?></h1>
        <?php echo $tournament->getDescription() ?>
        
        <?php if($tournament->getRegisterStatus() == 'allowed'): ?>
        
        <?php if($user != null): ?>
         <a href="<?php echo $router->link('tournament_share_link', array('hash' => $tournament->getShareLinkHash())) ?>" class="btn btn-join-tournament">
            <?php echo $translator->translate('tournament.public.detail.join') ?>
        </a>        
        <?php else: ?>
            <a class="btn btn-join-tournament" href="https://app.coachrufus.com/sk/login?skliga2019=<?php echo $tournament->getShareLinkHash() ?>"><?php echo $translator->translate('tournament.public.detail.join') ?></a>
        <?php endif; ?>
            
        <?php endif; ?>
        
       
    </div>
    
    <div class="col-sm-4">
        <table class="table table-striped">
            <tbody>
                <tr>
                    <td><strong><?php echo $translator->translate('tournament.public.detail.start') ?></strong></td>
                    <td><?php echo $tournament->getFormatedStartDate('d.m.Y H:i') ?></td>
                </tr>
                <tr>
                    <td><strong><?php echo $translator->translate('tournament.public.detail.end') ?></strong></td>
                    <td><?php echo $tournament->getFormatedEndDate('d.m.Y H:i') ?></td>
                </tr>
                <tr>
                    <td><strong><?php echo $translator->translate('tournament.public.detail.gender') ?></strong></td>
                    <td><?php echo $translator->translate('tournament.public.detail.gender.'.$tournament->getGender()) ?></td>
                </tr>
                <tr>
                    <td><strong><?php echo $translator->translate('tournament.public.detail.sport') ?></strong></td>
                    <td><?php echo $tournament->getSportName() ?></td>
                </tr>
                <tr>
                    <td><strong><?php echo $translator->translate('tournament.public.detail.level') ?></strong></td>
                    <td><?php echo $tournament->getLevel() ?></td>
                </tr>
                <tr>
                    <td><strong><?php echo $translator->translate('tournament.public.detail.age') ?></strong></td>
                    <td><?php echo $tournament->getAgeFrom() ?> - <?php echo $tournament->getAgeTo() ?> <?php echo $translator->translate('tournament.public.detail.age.years') ?></td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="col-sm-4">
        <table class="table table-striped">
            <tbody>
                <tr>
                    <td><strong><?php echo $translator->translate('tournament.public.detail.email') ?></strong></td>
                    <td><?php echo $tournament->getEmail() ?></td>
                </tr>
                <tr>
                    <td><strong><?php echo $translator->translate('tournament.public.detail.phone') ?></strong></td>
                    <td><?php echo $tournament->getPhone() ?></td>
                </tr>
                <tr>
                    <td><strong><?php echo $translator->translate('tournament.public.detail.address') ?></strong></td>
                    <td><?php echo $tournament->getAddressCity() ?>, <?php echo $tournament->getAddressStreet() ?> <?php echo $tournament->getAddressStreetNum() ?>, <?php echo $tournament->getAddressZip() ?></td>
                </tr>

            </tbody>
        </table>
    </div>
</div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane <?php echo ('teams' == $activeTab) ? 'active' : '' ?>" id="teams">
            <div class="panel">
                <div class="panel-body">
                    <table class="table table-condensed">
                        <thead>
                            <tr>
                                <th><?php echo $translator->translate('tournament.public.detail.teams.name') ?></th>
                                <th><?php echo $translator->translate('tournament.public.detail.teams.players') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach($teams as $team): ?>
                            <tr>
                                <td><?php echo $team->getName()  ?></td>
                                <td><?php echo $teamsInfo[$team->getId()]['totalMembers']  ?></td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


</div>
