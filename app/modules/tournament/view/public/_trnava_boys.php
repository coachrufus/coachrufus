 <h2>Krajské kolo Trnavský kraj</h2>
<div class="playoff" data-current-round="1" data-current-round-panel="pl-round-1">
    <div class="m-round-nav">
                 <a class="schedule-round-trigger" data-round="1" href="#schedule-470-round-1">1 Kolo</a>
                <a class="schedule-round-trigger" data-round="2" href="#schedule-470-round-2">2 Kolo</a>
                <a class="schedule-round-trigger" data-round="3" href="#schedule-470-round-3">3 Kolo</a>
            </div>
    
            <div class="pl-round pl-round-1 active" id="schedule-470-round-1">
            
                <div class="pl-cell pl-cell- pl-cell-odd">
                    <div class="pl-top-bar">
                        <div class="pl-top-bar-row">
                            <i class="ico ico-event-clock-white"></i> 28. Marec 2019
                        </div>
                        
                        

                         <div class="pl-top-bar-row">
                            <i class="ico ico-map-marker-white"></i>                  </div>
                                            </div>

                    <div class="pl-bottom-wrap">

                        <div class="pl-cell-cnt">
                            <div class="pl-conector-left"></div>
                            <div class="pl-cell-cnt1">
                                ZŠ Školská                          </div>
                            <div class="pl-spacer"></div>
                            <div class="pl-cell-cnt2">
                                ZŠ Pata                    </div>
                            <div class="pl-conector-right"></div>
                        </div>
                        <div class="pl-cell-results">
                            <div class="pl-cell-cnt1">12
                                                            </div>
                            <div class="pl-cell-cnt2">2
                                                             </div>
                        </div>
                    </div>
                </div>
                <div class="pl-bottom-spacer"></div>
            
                <div class="pl-cell pl-cell- pl-cell-even">
                    <div class="pl-top-bar">
                        <div class="pl-top-bar-row">
                            <i class="ico ico-event-clock-white"></i> 28. Marec 2019  
                        </div>
                        
                        

                         <div class="pl-top-bar-row">
                            <i class="ico ico-map-marker-white"></i>                       </div>
                                            </div>

                    <div class="pl-bottom-wrap">

                        <div class="pl-cell-cnt">
                            <div class="pl-conector-left"></div>
                            <div class="pl-cell-cnt1">
                                ZŠ Z. Kodálya s VJM                            </div>
                            <div class="pl-spacer"></div>
                            <div class="pl-cell-cnt2">
                               Gym. M.R.Štefánika                   </div>
                            <div class="pl-conector-right"></div>
                        </div>
                        <div class="pl-cell-results">
                            <div class="pl-cell-cnt1">0
                                                            </div>
                            <div class="pl-cell-cnt2">9
                                                             </div>
                        </div>
                    </div>
                </div>
                <div class="pl-bottom-spacer"></div>
            
                <div class="pl-cell pl-cell- pl-cell-odd">
                    <div class="pl-top-bar">
                        <div class="pl-top-bar-row">
                            <i class="ico ico-event-clock-white"></i>28. Marec 2019
                        </div>
                        
                        

                         <div class="pl-top-bar-row">
                            <i class="ico ico-map-marker-white"></i>                       </div>
                                            </div>

                    <div class="pl-bottom-wrap">

                        <div class="pl-cell-cnt">
                            <div class="pl-conector-left"></div>
                            <div class="pl-cell-cnt1">
                               ZŠ Mallého                      </div>
                            <div class="pl-spacer"></div>
                            <div class="pl-cell-cnt2">
                                ZŠ Bernolákova                    </div>
                            <div class="pl-conector-right"></div>
                        </div>
                        <div class="pl-cell-results">
                            <div class="pl-cell-cnt1">6
                                                            </div>
                            <div class="pl-cell-cnt2">1
                                                             </div>
                        </div>
                    </div>
                </div>
                <div class="pl-bottom-spacer"></div>
            
                <div class="pl-cell pl-cell- pl-cell-even">
                    <div class="pl-top-bar">
                        <div class="pl-top-bar-row">
                            <i class="ico ico-event-clock-white"></i> 28. Marec 2019
                        </div>
                        
                        

                         <div class="pl-top-bar-row">
                            <i class="ico ico-map-marker-white"></i>                       </div>
                                            </div>

                    <div class="pl-bottom-wrap">

                        <div class="pl-cell-cnt">
                            <div class="pl-conector-left"></div>
                            <div class="pl-cell-cnt1">
                                ZŠ J.A. Komenského            </div>
                            <div class="pl-spacer"></div>
                            <div class="pl-cell-cnt2">
                               ZŠ Vajanského                </div>
                            <div class="pl-conector-right"></div>
                        </div>
                        <div class="pl-cell-results">
                            <div class="pl-cell-cnt1">
                                                            </div>
                            <div class="pl-cell-cnt2">
                                                             </div>
                        </div>
                    </div>
                </div>
                <div class="pl-bottom-spacer"></div>
                    </div>
            <div class="pl-round pl-round-2" id="schedule-470-round-2">
            
                <div class="pl-cell pl-cell- pl-cell-odd">
                    <div class="pl-top-bar">
                        <div class="pl-top-bar-row">
                            <i class="ico ico-event-clock-white"></i> 28. Marec 2019
                        </div>
                        
                        

                         <div class="pl-top-bar-row">
                            <i class="ico ico-map-marker-white"></i>                       </div>
                                            </div>

                    <div class="pl-bottom-wrap">

                        <div class="pl-cell-cnt">
                            <div class="pl-conector-left"></div>
                            <div class="pl-cell-cnt1">
                                ZŠ Vajanského                   </div>
                            <div class="pl-spacer"></div>
                            <div class="pl-cell-cnt2">
                               ZŠ Vajanského                  </div>
                            <div class="pl-conector-right"></div>
                        </div>
                        <div class="pl-cell-results">
                            <div class="pl-cell-cnt1">4
                                                            </div>
                            <div class="pl-cell-cnt2">1
                                                             </div>
                        </div>
                    </div>
                </div>
                <div class="pl-bottom-spacer"></div>
            
                <div class="pl-cell pl-cell- pl-cell-even">
                    <div class="pl-top-bar">
                        <div class="pl-top-bar-row">
                            <i class="ico ico-event-clock-white"></i> 28. Marec 2019
                        </div>
                        
                        

                         <div class="pl-top-bar-row">
                            <i class="ico ico-map-marker-white"></i>                       </div>
                                            </div>

                    <div class="pl-bottom-wrap">

                        <div class="pl-cell-cnt">
                            <div class="pl-conector-left"></div>
                            <div class="pl-cell-cnt1">
                                ZŠ Mallého              </div>
                            <div class="pl-spacer"></div>
                            <div class="pl-cell-cnt2">
                                 ZŠ Vajanského                   </div>
                            <div class="pl-conector-right"></div>
                        </div>
                        <div class="pl-cell-results">
                            <div class="pl-cell-cnt1">9
                                                            </div>
                            <div class="pl-cell-cnt2">2
                                                             </div>
                        </div>
                    </div>
                </div>
                <div class="pl-bottom-spacer"></div>
                    </div>
            <div class="pl-round pl-round-3" id="schedule-470-round-3">
            
                <div class="pl-cell pl-cell-playoff_final pl-cell-odd">
                    <div class="pl-top-bar">
                        <div class="pl-top-bar-row">
                            <i class="ico ico-event-clock-white"></i> 28. Marec 2019
                        </div>
                        
                        

                         <div class="pl-top-bar-row">
                            <i class="ico ico-map-marker-white"></i>                       </div>
                                            </div>

                    <div class="pl-bottom-wrap">

                        <div class="pl-cell-cnt">
                            <div class="pl-conector-left"></div>
                            <div class="pl-cell-cnt1">
                                ZŠ Školská            </div>
                            <div class="pl-spacer"></div>
                            <div class="pl-cell-cnt2">
                                ZŠ Mallého                 </div>
                            <div class="pl-conector-right"></div>
                        </div>
                        <div class="pl-cell-results">
                            <div class="pl-cell-cnt1">1
                                                            </div>
                            <div class="pl-cell-cnt2">3
                                                             </div>
                        </div>
                    </div>
                </div>
                <div class="pl-bottom-spacer"></div>
            
                <div class="pl-cell pl-cell-playoff_bronze pl-cell-even">
                    <div class="pl-top-bar">
                        <div class="pl-top-bar-row">
                            <i class="ico ico-event-clock-white"></i> 28. Marec 2019
                        </div>
                        
                        

                         <div class="pl-top-bar-row">
                            <i class="ico ico-map-marker-white"></i>                       </div>
                                            </div>

                    <div class="pl-bottom-wrap">

                        <div class="pl-cell-cnt">
                            <div class="pl-conector-left"></div>
                            <div class="pl-cell-cnt1">
                                Gym. M.R.Štefánika  </div>
                            <div class="pl-spacer"></div>
                            <div class="pl-cell-cnt2">
                                 ZŠ Vajanského              </div>
                            <div class="pl-conector-right"></div>
                        </div>
                        <div class="pl-cell-results">
                            <div class="pl-cell-cnt1">3
                                                            </div>
                            <div class="pl-cell-cnt2">4
                                                             </div>
                        </div>
                    </div>
                </div>
                <div class="pl-bottom-spacer"></div>
                    </div>
    </div>