    

    
<div class="panel m-public-t-panel">
           <div class="panel-body">
  <div class="tab-content">
      <div role="tabpanel" class="tab-pane active" id="open">
          
          <?php if($tournament->getId() == 1433): ?>
            <?php $layout->includePart(MODUL_DIR . '/tournament/view/public/_bratislava_boys.php') ?>
            <?php $layout->includePart(MODUL_DIR . '/tournament/view/public/_trnava_boys.php') ?>
          <?php endif; ?>
          
          <?php if($tournament->getId() == 1434): ?>
            <?php $layout->includePart(MODUL_DIR . '/tournament/view/public/_bratislava_girls.php') ?>
            <?php $layout->includePart(MODUL_DIR . '/tournament/view/public/_trnava_girls.php') ?>
          <?php endif; ?>
          
          
          
          
          <?php foreach ($scheduleList as $schedule): ?>
          <h2><?php echo $schedule->getName()  ?></h2>
           
  <?php $layout->includePart(MODUL_DIR . '/tournament/view/schedule/_multiple_groups_playoff.php', 
                            array(
                                'user' => $user,
                                'schedule' => $schedule,
                                'tournament' =>$tournament,
                                'status' => 'closed'
                            )) ?>
            <?php endforeach; ?>
           </div></div>
               
    <?php $layout->startSlot('javascript') ?>
 <script>
    
    
    $(function () {
        $('.schedule-round-trigger').on('click',function(e){
           e.preventDefault();
           
           var container = $(this).parents('.playoff');
           var current_round = container.attr('data-current-round');
           var current_round_panel = container.find('.'+container.attr('data-current-round-panel')) ;
           
           var round = parseInt($(this).attr('data-round'));
           var targetId = $(this).attr('href');
           var round_panel = $(targetId);

           container.find('.schedule-round-trigger').removeClass('active');
           $(this).addClass('active');

           if(round > current_round)
           {
               
               
               current_round_panel.find('.pl-cell-odd').animate({
                opacity: 0
              }, 500, function() {});
               
               
               current_round_panel.find('.pl-cell-even').animate({
                opacity: 0,
                top: "-=270",
                marginBottom:"-=270"
              }, 500, function() {
                    
                    
                    round_panel.css('opacity','1');
                    round_panel.css('left','20px');
                    
                    container.attr('data-current-round',round);
                    container.attr('data-current-round-panel','pl-round-'+round);
                    current_round_panel.removeClass('active');
                    $(targetId).addClass('active');
                    $(targetId).fadeIn();
                    
              });
              
         
           }
           
           if(round < current_round)
           {
               current_round_panel.animate({
                opacity: 0
              }, 500, function() {});
             
            round_panel.css('left','20px');
            
            
             round_panel.find('.pl-cell-odd').animate({
                opacity: 1
              }, 500, function() {});
               
               
               round_panel.find('.pl-cell-even').animate({
                opacity: 1,
                top: "+=270",
                marginBottom:"+=270"
              }, 500, function() {
                    
                    //round_panel.css('left','20px');
                    container.attr('data-current-round',round);
                    container.attr('data-current-round-panel','pl-round-'+round);
                    //current_round_panel = round_panel;
                   // current_round = round;
                    current_round_panel.removeClass('active');
                    $(targetId).addClass('active');
                    $(targetId).fadeIn();
              });
              
           }
           
          
           
        });
      });
</script>
<?php $layout->endSlot('javascript') ?>
           
               
    
    
  


