<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Coachrufus</title>
    <link href="/css/slider.css?v=4" rel="stylesheet">
</head>

<body>
    <div id="slider" data-tid="<?php echo $tid ?>" data-event1="<?php echo $event_id_1 ?>" data-event2="<?php echo $event_id_2 ?>" data-event-current-date="<?php echo $event_date ?>"
        data-lid1="<?php echo $lid1 ?>" data-lid2="<?php echo $lid2 ?>">
        <div class="slide slide_result">
            <div class="col col-red">
                <div class="hit" id="game_hit_<?php echo $event_id_1 ?>">
                    <span class="goal-title">GÓÓÓÓL</span>
            
                    <div id="hit_match_1" class="hit-match">
                        <span class="goal_author">
                            <span class="goal_author_number"></span>
                            <span class="goal_author_name"></span>
                        </span>
                    </div>
                    <span class="goal_school"></span>
                </div>
                <div id="game_status_<?php echo $event_id_1 ?>" class="game-status">
                        <div class="first_line_team"></div>
                        <div class="first_line_goals">0</div>
                        <div class="playground_name">
                            <div class="line-divider"></div>
                            <div class="logo-wrap">
                                <div class="pl_logo"  id="pl_joj"></div>
                            </div>
                        </div>
                        <div class="second_line_goals">0</div>
                        <div class="second_line_team"></div>
                </div>
            </div>
            <div class="col col-green">
                <div class="hit" id="game_hit_<?php echo $event_id_2 ?>">
                    <span class="goal-title"></span>
                    <div id="hit_match_2" class="hit-match">
                        <span class="goal_author">
                            <span class="goal_author_number"></span>
                            <span class="goal_author_name"></span>
                        </span>
                    </div>
                    <span class="goal_school"></span>
                </div>
                <div id="game_status_<?php echo $event_id_2 ?>"  class="game-status">
                    <div class="first_line_team"></div>
                        <div class="first_line_goals">0</div>
                        <div class="playground_name">
                            <div class="line-divider"></div>
                            <div class="logo-wrap logo-wrap-rufus">
                                <div class="pl_logo"  id="pl_rufus"></div>
                            </div>
                        </div>
                        <div class="second_line_goals">0</div>
                        <div class="second_line_team"></div>
                </div>
            </div>
        </div>


        <div class="slide slide_top_players">
            <div class="col col-black">
                <table id="top_assist_players" class="table">
                    <thead>
                        <tr>
                            <td class="table-title" colspan="2">Najlepší nahrávači</td>
                        </tr>
                        <tr class="table-label">
                            <td></td>
                            <td><div class="th-fake">ASSIST</div></td>
                        </tr>
                    </thead>
                   <tbody>
                    
                </tbody>
                </table>
            </div>

            <div class="col col-black">
                <table id="top_goal_players" class="table">
                    <thead>
                        <tr>
                            <td class="table-title" colspan="2">Najlepší strelci</td>
                        </tr>
                        <tr class="table-label">
                            <td></td>
                            <td><div class="th-fake">ASSIST</div></td>
                        </tr>
                    </thead>
                   <tbody>
                    
                    </tbody>
                </table>
            </div>
        </div>

        <div class="slide slide_org">
            <div style="margin-top:10%"><img src="/img/slider/logo-nadacia-joj.png" /></div>
            <div>
                <img src="/img/slider/flcup.svg" style="max-height:70%"/> 
                <img src="/img/slider/logo-tatralandia.png" />
            </div>
            <div style="margin-top:10%"><img src="/img/slider/logo-rufus-1.png" /></div>

        </div>

    </div>
    <script defer src="/js/slider.js?v2"></script>
</body>

</html>