<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Finále Florbal Sk Liga 2018 - 2019, 2. stupeň základných škôl dievčatá</title>

        <meta name="keywords" content=",Ženy,Slovensko" />

        <!-- Bootstrap -->
        <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>


        <link href='/dev/theme/bootstrap/css/font-awesome.min.css?ver=<?php echo VERSION ?>' rel='stylesheet' type='text/css' media='screen' /> 
        <link href='/dev/theme/bootstrap/css/bootstrap.min.css?ver=<?php echo VERSION ?>' rel='stylesheet' type='text/css' media='screen' /> 
        <link href='/dev/theme/adminlte/AdminLTE.css?ver=<?php echo VERSION ?>' rel='stylesheet' type='text/css' media='screen' /> 
        <link href='/dev/theme/adminlte/skins/skin-black-light.css?ver=<?php echo VERSION ?>' rel='stylesheet' type='text/css' media='screen' /> 
        <link href='/dev//css/main.css?ver=<?php echo VERSION ?>' rel='stylesheet' type='text/css' media='screen' /> 

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-black-light  layout-top-nav full-width tournament-top">
        
        
        <div id="live-goal-wrap">
         <div class="item active live-goal">
                    <div class="tournament-top-head">
                        <div class="topLogos">
                        <img class="logo-skliga" src="/theme/img/tournament/sklia_logo_white.svg" />
                        <img class="logo-rufus" src="/theme/img/tournament/rufus_logo_white.svg" />
                        </div>
                        <h1><?php echo $translator->translate('tournament.slider.goal') ?></h1>
                        <h2> FLOORBALL SK LIGA 2018 - 2019</h2>
                    </div>
                        <h3 id="goal-player-name"></h3>
                        <strong id="goal-player-team"></strong>
        </div>
        </div>
        
        
        
        <div id="tournament-top-slider" class="carousel slide">
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    
                     <div class="tournament-top-head">
                        <div class="topLogos">
                        <img class="logo-skliga" src="/theme/img/tournament/sklia_logo_white.svg" />
                        <img class="logo-rufus" src="/theme/img/tournament/rufus_logo_white.svg" />
                        </div>
                        <h1><?php echo $translator->translate('tournament.slider.topGoals') ?></h1>
                        <h2> FLOORBALL SK LIGA 2018 - 2019</h2>
                    </div>
                    
                 
                    <div class="ladder">
                        <span class="ladderLabel"><?php echo $translator->translate('tournament.slider.goals') ?></span>
                    <table>
                        <?php foreach($topGoals as $ladderPlayer): ?>
                        <?php if($ladderPlayer['goal'] > 0): ?>
                        <tr>
                            <td class="number"> <?php echo $ladderPlayer['number'] ?></td>
                            <td  class="player">
                                <?php echo $ladderPlayer['name'] ?>
                                <span><?php echo $ladderPlayer['teamName'] ?></span>
                            </td>
                            <td  class="points"><?php echo $ladderPlayer['goal'] ?></td>
                        </tr>
                        <?php endif; ?>
                        <?php endforeach; ?>
                    </table>
                    </div>
                </div>
                <div class="item">
                    
                      <div class="tournament-top-head">
                        <div class="topLogos">
                        <img class="logo-skliga" src="/theme/img/tournament/sklia_logo_white.svg" />
                        <img class="logo-rufus" src="/theme/img/tournament/rufus_logo_white.svg" />
                        </div>
                        <h1><?php echo $translator->translate('tournament.slider.topAssist') ?></h1>
                        <h2> FLOORBALL SK LIGA 2018 - 2019</h2>
                    </div>
                    
                    
                    
                    <div class="ladder">
                        <span class="ladderLabel"><?php echo $translator->translate('tournament.slider.assist') ?></span>
                    <table>
                        <?php foreach($topAssist as $ladderPlayer): ?>
                         <?php if($ladderPlayer['assist'] > 0): ?>
                        <tr>
                            <td class="number"> <?php echo $ladderPlayer['number'] ?></td>
                            <td  class="player">
                                <?php echo $ladderPlayer['name'] ?>
                                <span><?php echo $ladderPlayer['teamName'] ?></span>
                            </td>
                            <td  class="points"><?php echo $ladderPlayer['assist'] ?></td>
                        </tr>
                         <?php endif; ?>
                        <?php endforeach; ?>
                    </table>
                    </div>
                </div>
                 <div class="item">
                    
                      <div class="tournament-top-head">
                        <div class="topLogos">
                        <img class="logo-skliga" src="/theme/img/tournament/sklia_logo_white.svg" />
                        <img class="logo-rufus" src="/theme/img/tournament/rufus_logo_white.svg" />
                        </div>
                        <h1><?php echo $translator->translate('tournament.slider.topPoints') ?></h1>
                        <h2> FLOORBALL SK LIGA 2018 - 2019</h2>
                    </div>
                    
                    
                    
                    <div class="ladder">
                        <span class="ladderLabel"><?php echo $translator->translate('tournament.slider.points') ?></span>
                    <table>
                        <?php foreach($topPoints as $ladderPlayer): ?>
                         <?php if($ladderPlayer['points'] > 0): ?>
                        <tr>
                            <td class="number"> <?php echo $ladderPlayer['number'] ?></td>
                            <td  class="player">
                                <?php echo $ladderPlayer['name'] ?>
                                <span><?php echo $ladderPlayer['teamName'] ?></span>
                            </td>
                            <td  class="points"><?php echo $ladderPlayer['points'] ?></td>
                        </tr>
                         <?php endif; ?>
                        <?php endforeach; ?>
                    </table>
                    </div>
                </div>
            </div>


        </div>
        
        <script src="/dev/plugins/jQuery/jQuery-2.1.4.min.js"></script>
        <script type='text/javascript' src='/dev/plugins/jQueryUI/jquery-ui.min.js?ver=202'></script>
        <script type='text/javascript' src='/dev/theme/bootstrap/js/bootstrap.min.js?ver=202'></script>
        <script>

            $('.carousel').carousel({
                interval: 5000,
                pause:null
              });

              
              function loadLastHit (){
                   $.ajax({
                        method: "GET",
                        url: '/api/tournament/live/currentTimelineEvent?&tid=<?php echo $tournament->getId() ?>',
                        dataType: 'json'
                    }).done(function (response) {

                      if(response.status == 'SUCCESS' && response.data.status == 'SUCCESS' )
                      {
                         $('#goal-player-name').html(response.data.player_name);
                          $('#goal-player-team').html(response.data.team_name);
        
                        $('#live-goal-wrap').fadeIn(500);
                        
                        setTimeout(function(){ 
                            //$('#live-goal-wrap').fadeOut(1000);     
                            location.reload();
                            }, 10000);
                      }

                    });
              }
              
               var hitTimer = setInterval(this.loadLastHit, 4000);

        </script>


    </body>
</html
