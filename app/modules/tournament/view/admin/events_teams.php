<?php Core\Layout::getInstance()->startSlot('tournament_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/tournament/view/tournament/_menu.php', array('tournament' => $tournament)) ?>
<?php Core\Layout::getInstance()->endSlot('tournament_menu') ?>

<section class="content-header">
    <a class="all-event-link content-header-back" href="<?php echo $router->link('tournament') ?>"><i class="ico ico-event-back"></i><?php echo $translator->translate('tournament.backlink') ?></a>
</section>

<section class="content">
    <div class="panel panel-default">
        <div class="panel-body  panel-full-body">
            <form action="" method="post">
            <div class="row text-center">
                <div class="col-sm-5">
                    <h2><?php echo $lineup->getFirstLineName() ?></h2>

                    <select name="team1">
                        <option value=""><?php echo $translator->translate('tournament.admin.events.team.choice') ?></option>
                        <?php foreach ($teams as $team): ?>
                            <option <?php echo ($team->getId() == $event->getTeamId()) ? 'selected="selected"' : '' ?> value="<?php echo $team->getId() ?>"><?php echo $team->getName() ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col-sm-2">
                    <h2>vs.</h2>
                </div>
                <div class="col-sm-5">
                    <h2><?php echo $lineup->getSecondLineName() ?></h2>

                    <select name="team2">
                        <option value=""><?php echo $translator->translate('tournament.admin.events.team.choice') ?></option>
                        <?php foreach ($teams as $team): ?>
                            <option <?php echo ($team->getId() == $event->getOpponentTeamId()) ? 'selected="selected"' : '' ?> value="<?php echo $team->getId() ?>"><?php echo $team->getName() ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col-xs-12">
                    <button class="btn btn-primary"><?php echo $translator->translate('tournament.admin.events.team.save') ?></button> <br /><br /><br />
                </div>
            </div>
            </form>
        </div>
    </div>
</section>