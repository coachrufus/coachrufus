<?php Core\Layout::getInstance()->startSlot('tournament_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/tournament/view/tournament/_menu.php', array('tournament' => $tournament)) ?>
<?php Core\Layout::getInstance()->endSlot('tournament_menu') ?>

<section class="content-header">
    <a class="all-event-link content-header-back" href="<?php echo $router->link('tournament') ?>"><i class="ico ico-event-back"></i><?php echo $translator->translate('tournament.backlink') ?></a>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-8">    
            <?php foreach($groupedEvents as $group => $events): ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3><?php echo $translator->translate('tournament.admin.events.group') ?> <?php echo $group ?></h3>
                </div>
                <div class="panel-body  panel-full-body">
                    <?php foreach ($events as $event): ?>

                        <div class="event_row event_list_row tournament_event_list_row ">
                            <div class="event-date" data-href="">

                                <span class="day"><?php echo $translator->getRegionalDate('%d', $event->getCurrentDate()) ?></span>
                                <span class="month-accr"><?php echo mb_substr($translator->translate($event->getCurrentDate()->format('F')),0,3) ?>  </span>
                                <span class="dayname"><?php echo  mb_substr($translator->translate($event->getCurrentDate()->format('l')),0,3) ?></span>

                            </div>
                            <div class="event-info">
                                <div class="main-info">
                                    <h3><?php echo $event->getName() ?></h3>
                                   

                                    <span class="time"><?php echo $event->getTimeInterval(\Core\ServiceLayer::getService('localizer')->getTimeFormat()) ?> </span>
                                    <span class="playground"> <?php if(null !=$event->getPlaygroundData()):  ?> <?php echo $event->getPlaygroundName() ?> (<?php echo $event->getPlaygroundAddress() ?>)<?php endif;  ?></span>  
                                </div>
                                <strong>Live link: <a  onclick="window.open(this,'_blank');return false;" href="<?php echo $router->link('tournament_public_live_detail',array('seoid' => $tournament->getSeoId(),'lineupId' => $event->getId())) ?>"> <?php echo WEB_DOMAIN_NAME.$router->link('tournament_public_live_detail',array('seoid' => $tournament->getSeoId(),'lineupId' => $event->getId())) ?></a></strong>
                               
                            </div>
                            <div class="action-cell">
                                <?php if(null != $event->getMatchResult()): ?>
                                <span class="match-result"><?php echo $event->getMatchResult()  ?></span>
                                <?php endif; ?>
                                
                                
                                <?php if($event->getTeamId() == null): ?>
                                <a href="<?php echo $router->link('tournament_admin_events_teams',array('id' => $event->getId())) ?>" class="btn btn-primary"><?php echo $translator->translate('tournament.admin.event.teams') ?></a>
                                <?php else: ?>
                                 
                                    <a class="btn btn-primary mpopup" data-action="popup-full" data-popup-onclose-action="refresh" href="<?php echo WEB_DOMAIN.$router->generateApiWidgetUrl('api_content_games_score_widget',array('apikey' => CR_API_KEY,'user_id' => $user->getId(),'event_id' => $event->getId(),'lineup_id' => $event->getLineup()->getId() ,'event_date' => $event->getCurrentDate()->format('Y-m-d'))) ?>"><?php echo $translator->translate('widget.event.sidebar.addScore') ?></a>  
                                <?php endif; ?>
                                
                                
                                
                            </div>
                            
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
             <?php endforeach; ?>
        </div>
    </div>
</section>