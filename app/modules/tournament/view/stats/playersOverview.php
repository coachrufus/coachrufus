<?php Core\Layout::getInstance()->startSlot('tournament_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/tournament/view/tournament/_menu.php', array('tournament' => $tournament)) ?>
<?php Core\Layout::getInstance()->endSlot('tournament_menu') ?>


<?php Core\Layout::getInstance()->startSlot('stylesheet') ?>  
<link rel="stylesheet" href="/dev/plugins/datatables/dataTables.bootstrap.css">
<?php Core\Layout::getInstance()->endSlot('stylesheet') ?>

<?php Core\Layout::getInstance()->startSlot('crumb') ?>
<?php
$layout->includePart(GLOBAL_DIR . '/templates/parts/_breadcrumb.php', array(
    'crumbs' => array(
        'tournament.bradcrumb.title' => $router->link('tournament'),
        'tournament.bradcrumb.schedule' => $router->link('tournament_schedule', array('team_id' => $tournament->getId())),
        'Edit' => ''
)))
?>
<?php Core\Layout::getInstance()->endSlot('crumb') ?>

<section class="content-header">
    <a class="all-event-link content-header-back" href="<?php echo $router->link('tournament') ?>"><i class="ico ico-event-back"></i><?php echo $translator->translate('tournament.backlink') ?></a>
</section>


<section class="content">
     <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" ><a href="<?php echo $router->link('tournament_stats', array('id' => $tournament->getId())) ?>"><?php echo $translator->translate('tournament.stats.tab.title') ?></a></li>
                <li role="presentation" class="active"><a href="<?php echo $router->link('tournament_players_stats',array('id' => $tournament->getId())) ?>"><?php echo $translator->translate('tournament.playerStats.tab.title') ?></a></li>

              </ul>
    <div class="panel">
        <div class="panel-body">
             <div class="row">
                <form id="stats_filter_form" action="" method="get">
                    <input type="hidden" name="id" value="<?php echo $tournament->getId() ?>" />
                <div class="col-lg-2 col-sm-6">
                    
                    <?php echo $filterForm->renderSelectTag('region',array('class' => 'form-control' )) ?>
                  
                </div>
                <div class="col-lg-2 col-sm-6">
                     <?php echo $filterForm->renderSelectTag('group',array('class' => 'form-control' )) ?>
                </div>
                </form>
            </div>
            <br />
            <table class="table table-bordered " id="stat_table">
                <thead>
                    <tr>
                        <th><?php echo $translator->translate('tournament.stats.playerName') ?></th>
                        <th><?php echo $translator->translate('tournament.stats.playerTeamName') ?></th>
                        <th><?php echo $translator->translate('tournament.stats.playerTeamGroup') ?></th>
                        <th><?php echo $translator->translate('tournament.stats.gamePlayes') ?></th>
                        <th><?php echo $translator->translate('tournament.stats.goals') ?></th>
                        <th><?php echo $translator->translate('tournament.stats.assist') ?></th>
                        <th><?php echo $translator->translate('tournament.stats.goalAssistTotal') ?></th>
                        <th><?php echo $translator->translate('tournament.stats.mom') ?></th>
                        
                        <th><?php echo $translator->translate('tournament.stats.wins') ?></th>
                        <th><?php echo $translator->translate('tournament.stats.draws') ?></th>
                        <th><?php echo $translator->translate('tournament.stats.looses') ?></th>
                        <th><?php echo $translator->translate('tournament.stats.crs') ?></th>
                       
                       
                        
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($stats as $playerId => $stat): ?>
                    <?php if($playerId != null): ?>
                     <tr>
                        <td><?php echo $stat['player_name'] ?></td>
                        <td><?php echo $stat['player_team'] ?></td>
                        <td><?php echo $stat['player_team_group'] ?> (<?php echo strtoupper(str_replace('sk', '', $stat['player_team_locality']) ) ?>)</td>
                        <td><?php echo $stat['gp'] ?></td>
                         <td><?php echo $stat['goals'] ?></td>
                        <td><?php echo $stat['assist'] ?></td>
                        <td><?php echo $stat['assist']+$stat['goals'] ?></td>
                         <td><?php echo $stat['mom'] ?></td>
                        
                        <td><?php echo $stat['wins'] ?></td>
                        <td><?php echo $stat['draws'] ?></td>
                        <td><?php echo $stat['looses'] ?></td>
                        <td><?php echo $stat['crs'] ?></td>
                        
                       
                    </tr>
                    <?php endif; ?>
                    <?php endforeach; ?>
                </tbody>
            </table>


        </div>
    </div>
</section>


<?php $layout->startSlot('javascript') ?>
<?php  $layout->addJavascript('plugins/datatables/jquery.dataTables.min.js')  ?>
<?php  $layout->addJavascript('plugins/datatables/dataTables.bootstrap.min.js')  ?>
<?php  $layout->addJavascript('plugins/datatables/'.LANG.'.js')  ?>
 <script>
      $(function () {
        
        //var datatables_trans = new datatablesTrans();
        var table =  $("#stat_table").DataTable({
             "scrollX": true,
             "scrollY":false,
             "iDisplayLength": 10,
             "aLengthMenu": [[5, 10, 25, 50,100, -1], [5, 10, 25, 50,100, "<?php echo $translator->translate('All') ?>"]],
             "language": datatablesTrans,
             "order": [[ 4, "desc" ],[ 2, "desc" ],[ 5, "desc" ],[ 6, "desc" ],[ 7, "desc" ]],
             "dom": '<"top"iflp<"clear">>rt<"bottom"iflp<"clear">>',
        });

 $('#stats_filter_form select').on('change',function(){
            $('#stats_filter_form').submit();
        });
        
        

       
      });
      
     
    </script>
  
<?php $layout->endSlot('javascript') ?>