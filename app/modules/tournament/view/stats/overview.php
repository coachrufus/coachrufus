<?php Core\Layout::getInstance()->startSlot('tournament_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/tournament/view/tournament/_menu.php', array('tournament' => $tournament)) ?>
<?php Core\Layout::getInstance()->endSlot('tournament_menu') ?>


<?php Core\Layout::getInstance()->startSlot('stylesheet') ?>  
<link rel="stylesheet" href="/dev/plugins/datatables/dataTables.bootstrap.css">
<?php Core\Layout::getInstance()->endSlot('stylesheet') ?>

<?php Core\Layout::getInstance()->startSlot('crumb') ?>
<?php
$layout->includePart(GLOBAL_DIR . '/templates/parts/_breadcrumb.php', array(
    'crumbs' => array(
        'tournament.bradcrumb.title' => $router->link('tournament'),
        'tournament.bradcrumb.schedule' => $router->link('tournament_schedule', array('team_id' => $tournament->getId())),
        'Edit' => ''
)))
?>
<?php Core\Layout::getInstance()->endSlot('crumb') ?>

<section class="content-header">
    <a class="all-event-link content-header-back" href="<?php echo $router->link('tournament') ?>"><i class="ico ico-event-back"></i><?php echo $translator->translate('tournament.backlink') ?></a>
</section>


<section class="content">
     <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="<?php echo $router->link('tournament_stats', array('id' => $tournament->getId())) ?>"><?php echo $translator->translate('tournament.stats.tab.title') ?></a></li>
                <li role="presentation"><a href="<?php echo $router->link('tournament_players_stats',array('id' => $tournament->getId())) ?>"><?php echo $translator->translate('tournament.playerStats.tab.title') ?></a></li>

              </ul>
    <div class="panel">
     
        
        <div class="panel-body">
            <div class="row">
                <form id="stats_filter_form" action="" method="get">
                    <input type="hidden" name="id" value="<?php echo $tournament->getId() ?>" />
                <div class="col-lg-2 col-sm-6">
                    
                    <?php echo $filterForm->renderSelectTag('region',array('class' => 'form-control' )) ?>
                  
                </div>
                <div class="col-lg-2 col-sm-6">
                     <?php echo $filterForm->renderSelectTag('group',array('class' => 'form-control' )) ?>
                </div>
                </form>
            </div>
            
         
            <table class="table table-bordered " id="stat_table">
                <thead>
                    <tr>
                        <th class="t-stat-table-team"><?php echo $translator->translate('Team') ?></th>
                        <th><?php echo $translator->translate('tournament.stats.gamePlayes') ?></th>
                         <th><?php echo $translator->translate('tournament.stats.locality') ?></th>
                         <th><?php echo $translator->translate('tournament.stats.group') ?></th>
                        <th><?php echo $translator->translate('tournament.stats.wins') ?></th>
                        <th><?php echo $translator->translate('tournament.stats.winsOvertime') ?></th>
                        <th><?php echo $translator->translate('tournament.stats.draws') ?></th>
                        <th><?php echo $translator->translate('tournament.stats.looses') ?></th>
                        <th><?php echo $translator->translate('tournament.stats.losesOvertime') ?></th>
                        <th><?php echo $translator->translate('tournament.stats.points') ?></th>
                        <th><?php echo $translator->translate('tournament.stats.goalsPlus') ?></th>
                        <th><?php echo $translator->translate('tournament.stats.goalsMinus') ?></th>
                        <th><?php echo $translator->translate('tournament.stats.goalsPlusMinus') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($stats as $stat): ?>
                     <tr>
                         <td>
                              <?php if($permissionManager->hasTeamPermission($user,$stat['team'],'MANAGE_PLAYERS')): ?>
                              <a class="btn btn-green" href="<?php echo $router->link('tournament_team_players',array('id' => $stat['team']->getTournamentId(),'team_id' => $stat['team']->getId())) ?>"> <?php echo $stat['team']->getName() ?></a>
                              <?php else: ?>
                             <a href="<?php echo $router->link('tournament_team_detail',array('id' =>$stat['team']->getTournamentId(), 'team_id' =>  $stat['team']->getId() )) ?>">
                                 <?php echo $stat['team']->getName() ?>
                             </a>
                             <?php endif; ?>
                             
                         </td>
                        <td><?php echo $stat['count'] ?></td>
                        <td><?php echo $stat['team']->getLocality() ?></td>
                        <td><?php echo $stat['team']->getDivision() ?></td>
                        <td><?php echo $stat['win'] ?></td>
                        <td><?php echo $stat['win_overtime'] ?></td>
                        <td><?php echo $stat['draw'] ?></td>
                        <td><?php echo $stat['lose'] ?></td>
                        <td><?php echo $stat['lose_overtime'] ?></td>
                        <td><?php echo $stat['crs'] ?></td>
                        <td><?php echo $stat['goals_plus'] ?></td>
                        <td><?php echo $stat['goals_minus'] ?></td>
                        <td><?php echo $stat['goals_plus']-$stat['goals_minus'] ?></td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>


        </div>
    </div>
</section>


<?php $layout->startSlot('javascript') ?>
<?php  $layout->addJavascript('plugins/datatables/jquery.dataTables.min.js')  ?>
<?php  $layout->addJavascript('plugins/datatables/dataTables.bootstrap.min.js')  ?>
<?php  $layout->addJavascript('plugins/datatables/'.LANG.'.js')  ?>
 <script>
      $(function () {
        
        //var datatables_trans = new datatablesTrans();
        var table =  $("#stat_table").DataTable({
             "scrollX": true,
             "scrollY":false,
             "iDisplayLength": 1000,
             "aLengthMenu": [[5, 10, 25, 50,100, -1], [5, 10, 25, 50,100, "<?php echo $translator->translate('All') ?>"]],
             "language": datatablesTrans,
             "order": [[ 5, "desc" ],[8,"desc"],[6,"desc"]],
             "dom": '<"top"f<"clear">>rt<"bottom"<"clear">>',
        });

        $('#stats_filter_form select').on('change',function(){
            $('#stats_filter_form').submit();
        });
        

       
      });
      
     
    </script>
  
<?php $layout->endSlot('javascript') ?>