<?php Core\Layout::getInstance()->startSlot('tournament_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/tournament/view/tournament/_menu.php', array('tournament' => $tournament)) ?>
<?php Core\Layout::getInstance()->endSlot('tournament_menu') ?>


<?php Core\Layout::getInstance()->startSlot('crumb') ?>
<?php
$layout->includePart(GLOBAL_DIR . '/templates/parts/_breadcrumb.php', array(
    'crumbs' => array(
        'tournament.bradcrumb.title' => $router->link('tournament'),
        'tournament.bradcrumb.schedule' => $router->link('tournament_schedule', array('team_id' => $tournament->getId())),
        'Edit' => ''
)))
?>
<?php Core\Layout::getInstance()->endSlot('crumb') ?>

<section class="content-header">
    <a class="all-event-link content-header-back" href="<?php echo $router->link('tournament') ?>"><i class="ico ico-event-back"></i><?php echo $translator->translate('tournament.backlink') ?></a>
</section>


<section class="content">
    

      <div class="panel">
           <div class="panel-body">
            <div class="row">
                <form id="stats_filter_form" action="" method="get">
                <div class="col-lg-2 col-sm-6">
                    
                    <?php echo $filterForm->renderSelectTag('region',array('class' => 'form-control' )) ?>
                  
                </div>
                <div class="col-lg-2 col-sm-6">
                     <?php echo $filterForm->renderSelectTag('group',array('class' => 'form-control' )) ?>
                </div>
                </form>
            </div>
           </div>
      </div>

 
<div class="panel">
           <div class="panel-body">
  <div class="tab-content">
      <div role="tabpanel" class="tab-pane active" id="open">
          <?php foreach ($scheduleList as $schedule): ?>
          <h2><?php echo $schedule->getName()  ?></h2>
              <?php $layout->includePart(MODUL_DIR . '/tournament/view/schedule/_'.$schedule->getScheduleType().'.php', 
                            array(
                                'user' => $user,
                                'schedule' => $schedule,
                                'permissionManager' => $permissionManager,
                                'tournament' =>$tournament,
                                'status' => 'waiting'
                            )) ?>
        <?php endforeach; ?>
      </div>
   
     </div>
           </div></div>
    
    
  
</section>

<?php $layout->startSlot('javascript') ?>
 <script>
      $(function () {
        $('#stats_filter_form select').on('change',function(){
            $('#stats_filter_form').submit();
        });
      });
</script>
<?php $layout->endSlot('javascript') ?>
