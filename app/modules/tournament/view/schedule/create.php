 <?php Core\Layout::getInstance()->startSlot('tournament_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/tournament/view/tournament/_menu.php', array('tournament' => $tournament)) ?>
<?php Core\Layout::getInstance()->endSlot('tournament_menu') ?>

<section class="content">
    <div class="panel">
    <div class="row">
        <div class="col-xs-12 col-lg-3">
            <h3>Každý s každým (sezóna)</h3>
            <p>Rozpis, kde se každý tým minimálně jednou utká se svým soupeřem.</p>
            <a href="<?php echo $router->link('tournament_schedule_create_one_group',array('id' => $tournament->getId() )) ?>"><?php echo $translator->translate('Create') ?></a>
        </div>
        <div class="col-xs-12 col-lg-3">
            <h3>Skupinový turnaj</h3>
            <p>V rámci skupiny se týmy utkávají systémem každý s každým. Každý tým patří právě do jedné skupiny a tým neodehraje žádný zápas s týmem, který nepatří do jeho skupiny.</p>
            <a href="<?php echo $router->link('tournament_schedule_create_multi_group',array('id' => $tournament->getId() )) ?>"><?php echo $translator->translate('Create') ?></a>
        </div>
        <div class="col-xs-12 col-lg-3">
            <h3>Konferenční turnaj</h3>
            <p>Konferenční turnaj tvoří 2 skupiny. Počet týmů v konferencích je stejný, příp. v jedné konferenci může být o jeden tým více. Tým se dvakrát utká s týmy ze stejné konference a jednou s týmem, který patří do druhé konference.</p>
            <a href="<?php echo $router->link('tournament_schedule_create_conference',array('id' => $tournament->getId())) ?>"><?php echo $translator->translate('Create') ?></a>
        </div>
        <div class="col-xs-12 col-lg-3">
            <h3>Playoff</h3>
            <p>Vyřazovací systém, ve kterém se odehraje méně zápasů, než kdyby hrál každý s každým. Podle typu playoff se určuje, zda tým v případě porážky vypadne z turnaje nebo bude hrát zápas i v dalším kole.</p>
            <a href="<?php echo $router->link('tournament_schedule_create_playoff',array('id' => $tournament->getId())) ?>"><?php echo $translator->translate('Create') ?></a>
        </div>
    </div>
    </div>
    

</section>
    
    
      



