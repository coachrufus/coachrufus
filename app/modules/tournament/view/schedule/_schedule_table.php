
<?php for($i=1;$i<5;$i++): ?>

<?php $roundEvents = $schedule->getRoundEvents($i) ?>

<?php if(count($roundEvents) > 0): ?>


<div class="event_row event_list_row" >
<h3><?php echo $i ?>. Kolo</h3> 
</div>
<?php foreach ($roundEvents as $event): ?>


    <?php if($status == $event->getMatch()->getStatus()): ?>


    <div class="event_row tournament-event-row event_list_row" >
        <div class="event-date">

            <span class="day"><?php echo $translator->getRegionalDate('%d', $event->getStart()) ?></span>
            <span class="month-accr"><?php echo mb_substr($translator->translate($event->getStart()->format('F')), 0, 3) ?> </span>
            <span class="dayname"><?php echo mb_substr($translator->translate($event->getStart()->format('l')), 0, 3) ?></span>

        </div>
        <div class="event-info">
            <div class="main-info">
                <h3>
                    <?php echo $event->getListName() ?>    
                </h3>
                <span class="time"><?php echo $event->getStart()->format('H:i') ?> </span>
                <span class="playground"><?php echo ($event->getPlaygroundName() != '') ? $event->getPlaygroundName() : '&nbsp;' ?></span>  
            </div>
            
            

        </div>
        
        <div class="event-actions1">
              <?php if ($permissionManager->hasEventPermission($user, $event, 'EVENT_EDIT')): ?>
                   
                        <?php if($event->getStart() == null or $event->getPlaygroundName() == null): ?>
                            <a class="btn btn-green" href="<?php echo $router->link('tournament_event_edit', array('id' => $event->getId())) ?>"><?php echo $translator->translate('tournament.schedule.event.addInfoData') ?></a>
                        <?php else: ?>
                            <a class="btn btn-green" href="<?php echo $router->link('tournament_event_edit', array('id' => $event->getId())) ?>"><?php echo $translator->translate('tournament.schedule.event.editInfoData') ?></a>
                        <?php endif; ?>
                    
                       
                        
                         <a class="btn btn-green media-trigger" data-id="<?php echo $event->getId()  ?>" href="<?php echo $router->link('tournament_event_edit', array('id' => $event->getId())) ?>"><?php echo $translator->translate('tournament.schedule.event.video') ?></a>
                        
                    <?php endif; ?>
            </div>

        <?php if ($event->getMatch()->getStatus() == 'closed'): ?>
            <div class="score-cell tscore-cell">
                <?php echo $event->getMatch()->getFirstLineGoals() ?>: <?php echo $event->getMatch()->getSecondLineGoals() ?> 
                
                <?php if($event->getMatch()->hasOvertime()): ?>
                    <span class="overtime-flag">P</span>
                <?php endif; ?>
            </div>
      
       
        
        <div class="score-cell-match-overview">
            
              <a class="btn btn-primary" href="<?php echo $router->link('tournament_event_detail', array('id' => $event->getId())) ?>"><?php echo $translator->translate('tournament.schedule.table.nomination') ?></a>
            
         <?php if ($event->getMatch()->getStatus() == 'closed'): ?>
            <a class="btn btn-primary" href="<?php echo $router->link('tournament_event_score', array('id' => $event->getId())) ?>"><?php echo $translator->translate('tournament.schedule.event.matchOverview') ?></a>
        <?php endif; ?>
        </div>
          




        <?php else: ?>
            <div class="attendance-cell">
                <?php if ($permissionManager->hasEventPermission($user, $event, 'EVENT_NOMINATION_VIEW')): ?>
                    <a class="btn btn-primary" href="<?php echo $router->link('tournament_event_detail', array('id' => $event->getId())) ?>"><?php echo $translator->translate('tournament.schedule.table.nomination') ?></a>
                <?php endif; ?>
                    
                <?php if ($permissionManager->hasEventPermission($user, $event, 'EVENT_LIVESCORE')): ?>
                    <div class="write-score">
                    <a  class="btn btn-primary" href="<?php echo $router->link('tournament_event_simple_score', array('id' => $event->getId())) ?>"><?php echo $translator->translate('tournament.schedule.table.simplescore') ?></a>
                    </div>
                <?php endif; ?>
            </div>
        <?php endif; ?>


        <div class="event-actions1 event-actions1-m">
                 <?php if ($permissionManager->hasEventPermission($user, $event, 'EVENT_EDIT')): ?>
                        <a class="btn btn-green" href="<?php echo $router->link('tournament_event_edit', array('id' => $event->getId())) ?>"><?php echo $translator->translate('tournament.schedule.event.edit') ?></a>
                        
                         <a class="btn btn-green media-trigger" data-id="<?php echo $event->getId()  ?>" href="<?php echo $router->link('tournament_event_edit', array('id' => $event->getId())) ?>"><?php echo $translator->translate('tournament.schedule.event.video') ?></a>
                        
                    <?php endif; ?>      
            </div>

    </div>
<?php endif; ?>
<?php endforeach; ?>
<?php endif; ?>
<?php endfor; ?>


<?php $layout->includePart(MODUL_DIR . '/tournament/view/event/_media_modal.php') ?>
<?php $layout->startSlot('javascript') ?>
<script type="text/javascript">
    
     
    $('.media-trigger').on('click',function(e){
        e.preventDefault();
        $('#tournament_event_id').attr('value',$(this).attr('data-id'));
        
        
        $('#add_tournament_event_media_modal').modal('show');
    });

  </script>
<?php $layout->endSlot('javascript') ?>  

