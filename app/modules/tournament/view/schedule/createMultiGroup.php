 <?php Core\Layout::getInstance()->startSlot('tournament_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/tournament/view/tournament/_menu.php', array('tournament' => $tournament)) ?>
<?php Core\Layout::getInstance()->endSlot('tournament_menu') ?>

<section class="content">

<div class="panel">
 <form action="" method="post">
    <input type="hidden" name="tournament_type" value="multiple_groups" />

    <h2><?php echo $translator->translate('tournament.form.title.schedule') ?></h2>

    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <?php echo $translator->translate($form->getFieldLabel("groups_count")) ?>
                <?php echo $form->renderNumberTag('groups_count') ?>
            </div>
            
             <div class="row groups_cnt">

        </div>

            <div class="form-group" style="display:none;">
                <ul id="teams-list">
               <?php foreach($teams as $team): ?>
                    <li class="teams-list-item-<?php echo $team->getId() ?>">
                        <input class="" type="checkbox" name="schedule[teams][]" value="<?php echo $team->getId() ?>" />     
                        <?php echo $team->getName() ?>
                    </li>
                <?php endforeach; ?>
                </ul>
            </div>
          
           

            <div class="form-group">
                <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("fields_count")) ?><?php echo $form->getRequiredLabel('fields_count') ?></label>
                <?php echo $form->renderNumberTag('fields_count', array('class' => 'form-control')) ?>
            </div>

            <div class="form-group">
                <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("fields_paralel")) ?><?php echo $form->getRequiredLabel('fields_paralel') ?></label>
                <?php echo $form->renderCheckboxTag('fields_paralel') ?>
            </div>
            
             <div class="form-group">
                <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("periods_count")) ?><?php echo $form->getRequiredLabel('periods_count') ?></label>
                <?php echo $form->renderNumberTag('periods_count', array('class' => 'form-control')) ?>
            </div>


        </div>

        <div class="col-sm-6">
           
            <div class="form-group">
                <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("date_start")) ?><?php echo $form->getRequiredLabel('date_start') ?></label>
                <?php echo $form->renderInputTag('date_start', array('class' => 'form-control  datetime-picker')) ?>
            </div>

          

            <div class="form-group">
                <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("match_duration")) ?><?php echo $form->getRequiredLabel('match_duration') ?></label>
                <?php echo $form->renderInputTag('match_duration', array('class' => 'form-control')) ?>
            </div>

            <div class="form-group">
                <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("round_duration")) ?><?php echo $form->getRequiredLabel('round_duration') ?></label>
                <?php echo $form->renderInputTag('round_duration', array('class' => 'form-control')) ?>
            </div>

            <div class="form-group">
                <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("spare_rounds_count")) ?><?php echo $form->getRequiredLabel('spare_rounds_count') ?></label>
                <?php echo $form->renderInputTag('spare_rounds_count', array('class' => 'form-control')) ?>
            </div>

            <div class="form-group">
                <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("referee_type")) ?><?php echo $form->getRequiredLabel('referee_type') ?></label>
                <?php echo $form->renderSelectTag('referee_type', array('class' => 'form-control')) ?>
            </div>
            
            <div class="form-group">
                 <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("table_type")) ?><?php echo $form->getRequiredLabel('table_type') ?></label>
                <?php echo $form->renderSelectTag('table_type', array('class' => 'form-control')) ?>
            </div>

        </div>

    </div>



    <button class="btn btn-primary" type="submit">odoslat</button>

</form>   
    
</div>
</section>
    
    
      






<?php $layout->addStylesheet('plugins/datetimepicker/css/bootstrap-datetimepicker.min.css') ?>
<?php $layout->addJavascript('plugins/datetimepicker/js/bootstrap-datetimepicker.min.js') ?>

<?php $layout->addJavascript('js/TournamentScheduleManager.js') ?>

<?php  $layout->startSlot('javascript') ?>
<script type="text/javascript">

    var schedule_manager = new ScheduleManager();
    schedule_manager.bindTriggers();


$(".datetime-picker").datetimepicker({format: 'dd.mm.yyyy hh:ii',autoclose:true});
    
</script>
<?php  $layout->endSlot('javascript') ?>