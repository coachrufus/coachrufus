<?php Core\Layout::getInstance()->startSlot('tournament_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/tournament/view/tournament/_menu.php', array('tournament' => $tournament)) ?>
<?php Core\Layout::getInstance()->endSlot('tournament_menu') ?>


<?php Core\Layout::getInstance()->startSlot('crumb') ?>
<?php
$layout->includePart(GLOBAL_DIR . '/templates/parts/_breadcrumb.php', array(
    'crumbs' => array(
        'tournament.bradcrumb.title' => $router->link('tournament'),
        'tournament.bradcrumb.schedule' => $router->link('tournament_schedule', array('team_id' => $tournament->getId())),
        'Edit' => ''
)))
?>
<?php Core\Layout::getInstance()->endSlot('crumb') ?>

<section class="content-header">
    <a class="all-event-link content-header-back" href="<?php echo $router->link('tournament') ?>"><i class="ico ico-event-back"></i><?php echo $translator->translate('tournament.backlink') ?></a>
</section>


<section class="content">
    


    
      <div class="panel">
           <div class="panel-body">
            <div class="row">
                <form id="stats_filter_form" action="" method="get">
                <?php if(in_array($tournament->getId(), array(1433,1434,1435,1436,1437)) ): ?>
                     <div class="col-lg-2 col-sm-6">
                        <?php echo $filterForm->renderSelectTag('schedule_id',array('class' => 'form-control' )) ?>
                    </div>
                <?php else: ?>
                     <div class="col-lg-2 col-sm-6">
                        <?php echo $filterForm->renderSelectTag('region',array('class' => 'form-control' )) ?>
                    </div>
                    <div class="col-lg-2 col-sm-6">
                         <?php echo $filterForm->renderSelectTag('group',array('class' => 'form-control' )) ?>
                    </div>
                <?php endif; ?>    
                    
                    
              
                </form>
            </div>
           </div>
      </div>

   <ul class="nav nav-tabs" role="tablist">
   
    <li role="presentation" class="active"><a href="#open" aria-controls="open" role="tab" data-toggle="tab"><?php echo $translator->translate('tournament.schedule.tab.open') ?></a></li>
    <li role="presentation" ><a href="#closed" aria-controls="closed" role="tab" data-toggle="tab"><?php echo $translator->translate('tournament.schedule.tab.closed') ?></a></li>
     
    <?php if(1433 == $tournament->getId() or 1434 == $tournament->getId() or 1435 == $tournament->getId() or 1436 == $tournament->getId() or 1437 == $tournament->getId()): ?>
    <li role="presentation" ><a href="#playoff" aria-controls="playoff" role="tab" data-toggle="tab"><?php echo $translator->translate('tournament.schedule.tab.playoffBracket') ?></a></li>
    <?php endif; ?>
     
     
  </ul>


  <div class="tab-content">
      <div role="tabpanel" class="tab-pane active" id="open">
          <?php foreach ($scheduleList as $schedule): ?>
            <div class="panel">
                <div class="panel-heading">
                    <h3><?php echo $schedule->getName() ?> </h3>

                    <?php if($permissionManager->hasTournamentPermission($user,$tournament)): ?>
                    <div class="panel-toolbar">
                        <a class="tool-item delete-schedule-trigger" id="delete-schedule-trigger" href="<?php echo $router->link('tournament_schedule_delete',array('id' => $tournament->getId(),'sid' => $schedule->getId() )) ?>">
                            <i class="ico ico-trash"></i><?php echo $translator->translate('tournament.schedule.delete') ?>
                        </a>
                    </div>
                    <?php endif; ?>
                </div>

                <div class="panel-body panel-full-body">

                    <?php $layout->includePart(MODUL_DIR . '/tournament/view/schedule/_one_group.php', 
                            array(
                                'user' => $user,
                                'schedule' => $schedule,
                                'permissionManager' => $permissionManager,
                                'tournament' =>$tournament,
                                'status' => 'waiting'
                            )) ?>
                </div>
            </div>
        <?php endforeach; ?>
      </div>
   
   
      <div role="tabpanel" class="tab-pane" id="closed">
           <?php foreach ($scheduleList as $schedule): ?>
            <div class="panel">
                <div class="panel-heading">
                    <h3><?php echo $schedule->getName() ?> </h3>

                    <?php if($permissionManager->hasTournamentPermission($user,$tournament)): ?>
                    <div class="panel-toolbar">
                        <a class="tool-item delete-schedule-trigger" id="delete-schedule-trigger" href="<?php echo $router->link('tournament_schedule_delete',array('id' => $tournament->getId(),'sid' => $schedule->getId() )) ?>">
                            <i class="ico ico-trash"></i><?php echo $translator->translate('tournament.schedule.delete') ?>
                        </a>
                    </div>
                    <?php endif; ?>
                </div>

                <div class="panel-body panel-full-body">

                    <?php $layout->includePart(MODUL_DIR . '/tournament/view/schedule/_one_group.php', 
                            array(
                                'user' => $user,
                                'schedule' => $schedule,
                                'permissionManager' => $permissionManager,
                                'tournament' =>$tournament,
                                'status' => 'closed'
                            )) ?>
                </div>
            </div>
        <?php endforeach; ?>
      </div>
      
      
      <div role="tabpanel" class="tab-pane" id="playoff">
           <?php foreach ($scheduleList as $schedule): ?>
            <div class="panel">
                <div class="panel-heading">
                    <h3><?php echo $schedule->getName() ?> </h3>

                    <?php if($permissionManager->hasTournamentPermission($user,$tournament)): ?>
                    <div class="panel-toolbar">
                        <a class="tool-item delete-schedule-trigger" id="delete-schedule-trigger" href="<?php echo $router->link('tournament_schedule_delete',array('id' => $tournament->getId(),'sid' => $schedule->getId() )) ?>">
                            <i class="ico ico-trash"></i><?php echo $translator->translate('tournament.schedule.delete') ?>
                        </a>
                    </div>
                    <?php endif; ?>
                </div>

                <div class="panel-body panel-full-body">

                    <?php $layout->includePart(MODUL_DIR . '/tournament/view/schedule/_multiple_groups_playoff.php', 
                            array(
                                'user' => $user,
                                'schedule' => $schedule,
                                'permissionManager' => $permissionManager,
                                'tournament' =>$tournament,
                                'status' => 'closed'
                            )) ?>
                </div>
            </div>
        <?php endforeach; ?>
      </div>
      
  </div>

    
  
</section>

<?php $layout->startSlot('javascript') ?>
 <script>
      $(function () {
        $('#stats_filter_form select').on('change',function(){
            $('#stats_filter_form').submit();
        });
      });
</script>
<?php $layout->endSlot('javascript') ?>
