
<?php
$bracket = $schedule->getPlayoffBracketSchema();
?>

<div class="playoff" data-current-round="1" data-current-round-panel="pl-round-1">
    <div class="m-round-nav">
         <?php foreach ($bracket as $roundIndex => $round): ?>
        <a class="schedule-round-trigger" data-round="<?php echo $roundIndex ?>" href="#schedule-<?php echo $schedule->getId()  ?>-round-<?php echo $roundIndex  ?>"><?php echo $roundIndex  ?> Kolo</a>
        <?php endforeach; ?>
    </div>
    
    <?php foreach ($bracket as $roundIndex => $round): ?>
        <div  class="pl-round pl-round-<?php echo $roundIndex ?><?php echo ($roundIndex == 1) ? ' active' : '' ?>" id="schedule-<?php echo $schedule->getId()  ?>-round-<?php echo $roundIndex  ?>">
            <?php $oddi = 1; foreach ($round as $roundEvent): ?>

                <div class="pl-cell pl-cell-<?php echo $roundEvent->getMatchType() ?> pl-cell-<?php echo  ($oddi++ % 2 == 0) ? 'even' : 'odd' ?>">
                    <div class="pl-top-bar">
                        <div class="pl-top-bar-row">
                            <i class="ico ico-event-clock-white"></i> <?php echo $roundEvent->getStart()->format('d') ?>. <?php echo $translator->translate($roundEvent->getStart()->format('F')) ?> <?php echo $roundEvent->getStart()->format('Y, H:i') ?>  
                        </div>
                        
                        <?php if( $roundEvent->getPlaygroundName() != ''): ?>


                         <div class="pl-top-bar-row">
                            <i class="ico ico-map-marker-white"></i><?php echo $roundEvent->getPlaygroundName() ?>
                          </div>
                        <?php endif; ?>
                    </div>

                    <div class="pl-bottom-wrap">

                        <div class="pl-cell-cnt">
                            <div class="pl-conector-left"></div>
                            <div class="pl-cell-cnt1">
                                <?php echo $roundEvent->getFirstTeamName() ?>
                            </div>
                            <div class="pl-spacer"></div>
                            <div class="pl-cell-cnt2">
                                <?php echo $roundEvent->getSecondTeamName() ?>
                            </div>
                            <div class="pl-conector-right"></div>
                        </div>
                        <div class="pl-cell-results">
                            <div class="pl-cell-cnt1">
                                <?php if ($roundEvent->getMatch()->getStatus() == 'closed'): ?>
                                    <?php echo $roundEvent->getMatch()->getFirstLineGoals() ?>
                                <?php endif ?>
                            </div>
                            <div class="pl-cell-cnt2">
                                 <?php if ($roundEvent->getMatch()->getStatus() == 'closed'): ?>
                                    <?php echo $roundEvent->getMatch()->getSecondLineGoals() ?> 
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pl-bottom-spacer"></div>
            <?php endforeach; ?>
        </div>
    <?php endforeach; ?>
</div>


