<table class="table">
    <thead>
        <tr>
            <th><?php echo $translator->translate('tournament.schedule.table.round') ?></th>
            <th><?php echo $translator->translate('tournament.schedule.table.name') ?></th>
            <th><?php echo $translator->translate('tournament.schedule.table.time') ?></th>
            <th></th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($schedule->getEvents() as $event): ?>
            <tr>
                <td><?php echo $event->getRound() + 1 ?></td>
                <td><?php echo $event->getName() ?></td>
                <td><?php echo $event->getStart()->format('d.m.Y H:i') ?></td>
                <td>
                    <a href="<?php echo $router->link('tournament_event_detail', array('id' => $event->getId())) ?>"><?php echo $translator->translate('tournament.schedule.table.nomination') ?></a><br />
                    <a href="<?php echo $router->link('tournament_event_score', array('id' => $event->getId())) ?>"><?php echo $translator->translate('tournament.schedule.table.livescore') ?></a><br />


                </td>
                <td>
                    <div class="row">
                        <div class="col-sm-6">
                            <?php //echo $event->getTeam1()->getName()  ?>
                        </div>
                    </div>
                    <form action="<?php echo $router->link('tournament_event_score_result', array('id' => $event->getId())) ?>" method="post">
                        <input type="text" name="result[<?php echo $event->getTeamId() ?>]" value="<?php echo $event->getMatch()->getFirstLineGoals() ?>" />
                        <input type="text" name="result[<?php echo $event->getOpponentTeamId() ?>]" value="<?php echo $event->getMatch()->getSecondLineGoals() ?>" />



                        <select name="status">
                            <option value="open" <?php echo ( $event->getMatch()->getStatus() != 'closed') ? 'selected="selected"' : '' ?>>Open</option>
                            <option value="closed" <?php echo ( $event->getMatch()->getStatus() == 'closed') ? 'selected="selected"' : '' ?>>Closed</option>
                        </select>
                        <input type="submit" />
                    </form>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>