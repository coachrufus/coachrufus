<div class="modal fade" id="edit-photo-modal" tabindex="-1" role="dialog" aria-labelledby="<?php echo $translator->translate('Edit photo') ?>">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?php echo $translator->translate('Edit photo') ?></h4>
            </div>

            <div class="modal-body">

                <div class="upload-wrap">

                    <div class="upload-drop-zone " id="dropzone">
                        <?php echo $translator->translate('Just drag and drop files here') ?>
                    </div>
                    <p><?php echo $translator->translate('OR ') ?></p>

                    <span class="btn btn-success fileinput-button">
                        <i class="glyphicon glyphicon-plus"></i>
                        <span><?php echo $translator->translate('Select files from your computer') ?></span>
                        <input   id="team_settings_photo" type="file" name="team_photo" data-url="<?php echo $router->link('tournament_team_photo', array('team_id' => $teamId)) ?>" />
                    </span>
                </div>


                <div class="upload-progress">
                    <span class="progress-num"></span>
                    <div class="progress progress-sm active">
                        <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                        </div>
                    </div>
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>