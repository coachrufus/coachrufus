<div class="login-box">
    <?php $layout->includePart(MODUL_DIR . '/user/view/_logo.php') ?>
    <div class="login-box-body">
        <p class="login-box-msg"><?php echo $translator->translate('Register') ?></p>
        <form action="#" method="post">

            <div class="form-group has-feedback">
                <?php echo $validator->showError('name', $translator->translate('Required Field!')) ?>
                <div class="input-group input-group-full">
                    <?php echo $form->renderInputTag('name', array('class' => 'form-control', 'placeholder' => $translator->translate('tournament.register.name'))) ?>
                </div>
            </div>
            <div class="form-group has-feedback">
                <?php echo $validator->showError('surname', $translator->translate('Required Field!')) ?>
                <div class="input-group input-group-full">
                    <?php echo $form->renderInputTag('surname', array('class' => 'form-control', 'placeholder' => $translator->translate('tournament.register.surname'))) ?>
                </div>
            </div>
         


            <div class="form-group">
                <?php echo $validator->showError('exist_user', $translator->translate('User Exist')) ?>
                <?php echo $validator->showError('email', $translator->translate('Required Field!')) ?>


                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="ico ico-user"></i>
                    </div>
                    <?php echo $form->renderInputTag('email', array('class' => 'form-control', 'placeholder' => $translator->translate('E-mail'))) ?>
                </div>

            </div>
            <div class="form-group has-feedback">

                <?php echo $validator->showError('password', $translator->translate('Required Field!')) ?>
                <?php echo $validator->showError('password_confirm', $translator->translate('Password not confirm')) ?>


                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="ico ico-lock"></i>
                    </div>
                    <?php echo $form->renderPasswordTag('password', array('class' => 'form-control', 'placeholder' => $translator->translate('Password'))) ?>
                </div>
            </div>
            <div class="form-group has-feedback">
                <?php echo $validator->showError('password_retype', $translator->translate('Required Field!')) ?>

                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="ico ico-lock"></i>
                    </div>
                    <?php echo $form->renderPasswordTag('password_retype', array('class' => 'form-control', 'placeholder' => $translator->translate('Retype password'))) ?>
                </div>
            </div>









            <div class="form-group">
                <?php echo $form->renderInputHiddenTag('default_lang', array('class' => 'form-control')) ?>
            </div>



             <div class="row">
                <div class="col-xs-12">
                    <div class="tcblock">
                        
                       
                         
                        <?php echo $form->renderCheckboxTag('vop_agreement', array('class' => 'form-control')) ?>
                        
                        <?php echo $translator->translate('register.vop.checkbox') ?><br />
                         <br />
                    </div>
                    
                  
                    
                    <button type="submit" class="btn btn-primary btn-block btn-flat bg_orange"><?php echo $translator->translate('Register') ?>    </button>
                </div><!-- /.col -->
            </div>
        </form>




        <div class="row login-footer">

        </div>


    </div><!-- /.login-box-body -->
</div><!-- /.login-box -->


