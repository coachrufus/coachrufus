<tr id="player_row_<?php echo $player->getId() ?>" class="edit-player-row <?php echo $rowClass ?> static">
    <td>

        <input type="hidden" name="player[<?php echo $player->getId() ?>][id]" value="<?php echo $player->getId() ?>" />
        <?php echo $editForm->renderInputTag("player_number", array('id' => 'player_number' . $player->getId(), 'name' => 'player[' . $player->getId() . '][player_number]', 'class' => 'form-control', 'value' => $player->getPlayerNumber())) ?>

        <?php echo $validator->showError("player_number_unique_".$player->getId()) ?>
       
    </td>
    <td>
         <div id="inline-edit-<?php echo $player->getId() ?>" class="inline-edit-wrap row">
            
             <div class="col-sm-12 col-lg-3">
                  <?php echo $editForm->renderInputTag("first_name", array('id' => 'player_first_name_' . $player->getId(), 'name' => 'player[' . $player->getId() . '][first_name]', 'class' => 'form-control', 'value' => $player->getFirstName())) ?>
             </div>
             <div class="col-sm-12 col-lg-4">
                <?php echo $editForm->renderInputTag("last_name", array('id' => 'player_last_name_' . $player->getId(), 'name' => 'player[' . $player->getId() . '][last_name]', 'class' => 'form-control', 'value' => $player->getLastName())) ?>
             </div>
             <div class="col-sm-12 col-lg-4">
                 <?php echo $editForm->renderInputTag("email", array('id' => 'player_email_' . $player->getId(), 'name' => 'player[' . $player->getId() . '][email]', 'class' => 'form-control', 'value' => $player->getEmail())) ?>
             </div>
             <div class="col-sm-12 text-center col-lg-1">
                  <i data-toggle="tooltip" data-placement="top" title="<?php echo $translator->translate('Cancel changes') ?>" data-edit="<?php echo $player->getId() ?>" class="ico ico-close player-row-cancel-edit  round_btn bg_red"></i>
             </div>
             
             
         </div>
        <i data-pid="<?php echo $player->getPlayerId() ?>" data-edit="<?php echo $player->getId() ?>" class="ico ico-edit player-row-edit pull-right"></i>
       
       
        
        <div id="static-data-<?php echo $player->getId() ?>">
            <span class="first_name"><?php echo $player->getFirstName() ?></span> <span class="last_name"><?php echo $player->getLastName() ?></span><br />
        <a class="color-link email" href="mailto:<?php echo $player->getEmail() ?>"><?php echo $player->getEmail() ?></a>
        </div>
    </td>


    <td>
        <?php echo $editForm->renderSelectTag("team_role", array('id' => 'team_role_' . $player->getId(), 'name' => 'player[' . $player->getId() . '][team_role]', 'class' => 'form-control', 'value' => $player->getTeamRole())) ?>
         <?php echo $validator->showError("team_role_".$player->getId()) ?>
    </td>
    <td>
        <?php echo $editForm->renderSelectTag("level", array('id' => 'player_level_' . $player->getId(), 'name' => 'player[' . $player->getId() . '][level]', 'class' => 'form-control', 'value' => $player->getLevel())) ?>
    </td>

    <td>
          <i data-edit="<?php echo $player->getId() ?>" class="ico ico-edit player-row-edit-sm"></i>

        <a data-row="player_row_<?php echo $player->getId() ?>" title="<?php echo $translator->translate('Remove player from team') ?>" href="<?php echo $router->link('tournament_team_delete_player', array('id' => $player->getId(),'tid' => $tournament->getId())) ?>" class="remove-team-player-trigger"><i class="ico ico-trash"></i></a>

    </td>
</tr>