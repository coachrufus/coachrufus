<div id="full-window">
    <div class="full-window-cnt">
    <div class="row">
        <div class="col-sm-12">
            
            <p><?php echo $translator->translate('gdpr.tournament.teamadmin.agreeText') ?></p>
            <form action="" method="post">
                <input type="hidden" name="form_act" value="tournament_team_admin_agreement" />
                
                <button name="tournament_team_admin_agreement" value="1" class="btn btn-primary agree-clear" type="submit"><?php echo $translator->translate('gdpr.tournament.teamadmin.agree') ?></button>
                <a href="<?php echo $router->link('tournament_detail',array('id' => $tournament->getId())) ?>" class="btn btn-primary agree-clear"><?php echo $translator->translate('gdpr.tournament.teamadmin.disagree') ?></a>
            </form>

        </div>
    </div>
    </div>
</div>