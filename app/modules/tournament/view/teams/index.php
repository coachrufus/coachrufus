<?php Core\Layout::getInstance()->startSlot('tournament_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/tournament/view/tournament/_menu.php', array('tournament' => $tournament)) ?>
<?php Core\Layout::getInstance()->endSlot('tournament_menu') ?>



<?php Core\Layout::getInstance()->startSlot('crumb') ?>
<?php
$layout->includePart(GLOBAL_DIR . '/templates/parts/_breadcrumb.php', array(
    'crumbs' => array(
        'tournament.bradcrumb.title' => $router->link('tournament'),
        'tournament.bradcrumb.teams' => $router->link('tournament_teams', array('team_id' => $tournament->getId())),
        'Edit' => ''
)))
?>
<?php Core\Layout::getInstance()->endSlot('crumb') ?>


<section class="content-header">
    <a class="all-event-link" href="<?php echo $router->link('tournament_detail', array('id' => $tournament->getId())) ?>"><i class="ico ico-event-back"></i><?php echo $translator->translate('tournament.backlink') ?></a>


</section>
<section class="content">

    <div class="panel">

        <div id="template_row_container" style="display:none;">
<?php $i = 0; ?>
            <div class="row team_row template_row">
                <input type="hidden" name="team[<?php echo $i ?>][team_id]" value="" id="team_id<?php echo $i ?>" />

                <div class="form-group col-sm-3 autocomplete_container">
                    <label class="control-label"><?php echo $translator->translate($addForm->getFieldLabel("name")) ?> *</label>
<?php echo $addForm->renderInputTag("name", array('name' => 'team[' . $i . '][name]', 'class' => 'form-control team-find-autocomplete team_first_name_row', 'data-validate-required' => 'required', 'data-index' => 'name' . $i, 'data-row' => $i, 'id' => 'team_name' . $i)) ?>
                    <div class="autocomplete_wrap">
                        <ul class="autocomplete_list">

                        </ul>
                    </div>
                </div>

                <div class="form-group col-sm-5 autocomplete_container">
                    <label class="control-label"><?php echo $translator->translate($addForm->getFieldLabel("email")) ?></label>
<?php echo $addForm->renderInputTag("email", array('name' => 'team[' . $i . '][email]', 'class' => 'form-control  team_email_row', 'autocomplete' => 'off', 'data-index' => 'email' . $i, 'data-row' => $i, 'id' => 'team_email' . $i)) ?>
                    <div class="autocomplete_wrap">
                        <ul class="autocomplete_list">

                        </ul>
                    </div>
                </div>
                <div class=" col-sm-1 btn_col">
                    <a  class="rm_team_btn" href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo $translator->translate('tournament.team.remove') ?>"><i class="ico ico-cross"></i></a>
                </div>
            </div>
        </div>




        <form action="<?php echo $router->link('tournament_team_create', array('id' => $tournament->getId())) ?>" id="add_team_form" method="post">
            <div class="panel-body">
                <input type="hidden" name="team_id" id="team_team_id" value="<?php echo $tournament->getId() ?>" />
                <input type="hidden" name="type" value="<?php echo $type ?>"/>
                <div class="row">
                    <div class="col-sm-6 form-group">

                        <label class="control-label"><?php echo $translator->translate('Share team invite link') ?></label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <a class="popup-link" href="<?php echo WEB_DOMAIN . $router->link('tournament_share_link', array('hash' => $tournament->getShareLinkHash())) ?>"><i class="ico ico-share-link"></i></a>
                            </div>
                            <input id="team_share_link" type="text" value="<?php echo WEB_DOMAIN . $router->link('tournament_share_link', array('hash' => $tournament->getShareLinkHash())) ?>" class="form-control" />   
                            <div class="input-group-addon">
                                <span class="copy-share-link"><?php echo $translator->translate('Copy') ?></span>
                            </div>
                        </div>

                    </div>

                </div>


                <div id="add_team_wrap">
<?php for ($i = 1; $i < 2; $i++): ?>
                        <div class="row team_row">
                            <input type="hidden" name="team[<?php echo $i ?>][team_id]" value="" id="team_id<?php echo $i ?>" />

                            <div class="form-group col-sm-3 autocomplete_container">
                                <label class="control-label"><?php echo $translator->translate($addForm->getFieldLabel("name")) ?> *</label>
    <?php echo $addForm->renderInputTag("name", array('name' => 'team[' . $i . '][name]', 'class' => 'form-control team_name_row team-find-autocomplete', 'autocomplete' => 'off', 'data-validate-required' => 'required', 'data-index' => 'name' . $i, 'data-row' => $i, 'id' => 'team_name' . $i)) ?>
                                <div class="autocomplete_wrap">
                                    <ul class="autocomplete_list">

                                    </ul>
                                </div>
                            </div>

                            <di v class="form-group col-sm-5 autocomplete_container">
                                <label class="control-label"><?php echo $translator->translate($addForm->getFieldLabel("email")) ?></label>
    <?php echo $addForm->renderInputTag("email", array('name' => 'team[' . $i . '][email]', 'class' => 'form-control team_email_row', 'autocomplete' => 'off', 'data-index' => 'email' . $i, 'data-row' => $i, 'id' => 'team_email' . $i)) ?>
                                <div class="autocomplete_wrap">
                                    <ul class="autocomplete_list">

                                    </ul>
                                </div>
                        </div>
                        <div class=" col-sm-1 btn_col"></div>
                    </div>
<?php endfor; ?>


            </div>
            <div class="row">
                <div class=" form-group col-sm-12">
                    <a class="btn btn-clear btn-clear-green" id="add_team" href=""><?php echo $translator->translate('tournament.team.add_team') ?></a><br />
                </div>
            </div>

            <div class="row" id="action_row">

                <div class="form-group col-sm-12 text-center">
                    <button type="submit" class="btn btn-primary" href=""><?php echo $translator->translate('tournament.team.save_teams') ?> <i class="fa fa-chevron-right    "></i></button>
                </div>
            </div>
</form>
    </div>





    
<div class="row teams_list-panel-head">
    <div class="col-sm-2">
        <h3><?php echo $translator->translate('tournament.team.list.title') ?></h3>
    </div>
</div>
     <?php if($request->hasFlashMessage('invite_admin_success')): ?>
        <div class="alert alert-success text-center"> <?php echo $request->getFlashMessage('invite_admin_success') ?></div>
        <?php endif; ?>
<div class="panel">
    <div class="panel-body">
        
        
       
        
        
        <form action="" method="post">

                <?php if ($request->hasFlashMessage('team_team_invitation_send')): ?>
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $request->getFlashMessage('team_team_invitation_send') ?>
                </div>
<?php endif; ?>
                <?php if ($request->hasFlashMessage('unactivate_team_success')): ?>
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $request->getFlashMessage('unactivate_team_success') ?>
                </div>
<?php endif; ?>

            <table class="table resp-table team-list-table">
                <thead>
                    <tr>
                        <th>
                            <?php echo $translator->translate('tournament.team.list.name') ?>
                        </th>
                        <th>
                            <?php echo $translator->translate('tournament.team.list.email') ?>
                        </th>
                        <th>
                            <?php echo $translator->translate('tournament.team.list.invitation') ?>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($teams as $team): ?>
                        <?php $layout->includePart(MODUL_DIR . '/tournament/view/teams/_edit_team_row.php', array('team' => $team, 'editForm' => $editForm)) ?> 
                    <?php endforeach; ?>
                </tbody>
            </table>

            <div class="col-sm-12 text-center">
                <button class="btn btn-primary" type="submit"><?php echo $translator->translate('Save changes') ?></button>
            </div>

            <?php if ($finishedProgressStep): ?>
                <a class="btn btn-primary pull-right continue_btn continue_btn_second" href="<?php echo $router->link('create_team_event_progress', array('team_id' => $tournament->getId(), 'type' => 'progress')) ?>"><?php echo $translator->translate('Continue') ?></a>
<?php else: ?>
<?php endif; ?>
        </form>
    </div>
</div>
</section>


<?php $layout->startSlot('javascript') ?>
<script src="/dev/plugins/fileupload/jquery.iframe-transport.js"></script>
<script src="/dev/plugins/fileupload/jquery.fileupload.js"></script>
<?php $layout->addJavascript('js/TournamentManager.js') ?>
<?php $layout->addJavascript('js/TeamSearchAutocomplete.js') ?>

<script type="text/javascript">

    TournamentManager = new TournamentManager();
    TournamentManager.bindTriggers();

    $('.team-find-autocomplete').each(function (key, val) {
        autocomplete = new TeamSearchAutocomplete($(val));
        autocomplete.initNameSearch();
    });


    $('.copy-share-link').on('click', function () {
        var copyTextarea = document.querySelector('#team_share_link');
        copyTextarea.select();
        document.execCommand('copy');
    });

</script>
<?php $layout->endSlot('javascript') ?>
