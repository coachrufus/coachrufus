<?php Core\Layout::getInstance()->startSlot('tournament_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/tournament/view/tournament/_menu.php', array('tournament' => $tournament)) ?>
<?php Core\Layout::getInstance()->endSlot('tournament_menu') ?>

<?php Core\Layout::getInstance()->startSlot('crumb') ?> 
<?php $layout->includePart(GLOBAL_DIR . '/templates/parts/_breadcrumb.php', array('crumbs' => array('tournament.bradcrumb.title' => $router->link('tournament'), 'tournament.bradcrumb.teams' => $router->link('tournament_teams', array('team_id' => $tournament->getId())), 'Edit' => ''))) ?> <?php Core\Layout::getInstance()->endSlot('crumb') ?>   

<section class="content-header">
    <h1><?php echo $team->getName() ?></h1>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <strong><?php echo $translator->translate('tournament.team.infoTitle') ?></strong>
                </div>

                <div class="panel-body">
                    <form action="" method="post">
                        <input type="hidden" name="form_act" value="team-info" />
                        <input type="hidden" name="photo_data" value="" id="team_photo_data" />
                        <div class="row">
                            <div class="form-group col-sm-4">
                                <label class="control-label">Logo</label>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div id="exist-photo-wrap" style="background-image: url('https://t.coachrufus.com/img/team/<?php echo $team->getPhoto() ?>');">
                                            <input type="hidden" name="team[photo]" value="<?php echo $team->getPhoto() ?>" id="team_photo">                                                     </div>

                                        <div id="photo-action">
                                            <a class="edit-team-photo color-link" href="">Zmeniť obrázok</a>
                                            <a class="delete-team-photo" href=""><i class="fa fa-times"></i></a>
                                        </div>
                                    </div>

                                </div>
                            </div>


                            <div class="form-group col-sm-8">
                                <label class="control-label">Niečo o tíme<span class="help">Max. 1000 characters</span></label>
                                <textarea name="team[description]" rows="5" cols="5" class="form-control" id="team_description"><?php echo $team->getDescription() ?></textarea>                                                                 </div>
                        </div>
                        <button type="submit"><?php echo $translator->translate('tournament.team.infoSave') ?></button>
                    </form>
                </div>
            </div>


            <div class="panel panel-default">

                <div id="template_row_container" style="display:none;">
                    <?php $i = 0; ?>
                    <div class="row player_row template_row">
                        <input type="hidden" name="player[<?php echo $i ?>][player_id]" value="" id="player_id<?php echo $i ?>" />

                        <div class="form-group col-sm-3">
                            <label class="control-label"><?php echo $translator->translate($addForm->getFieldLabel("first_name")) ?> *</label>
                            <?php echo $addForm->renderInputTag("first_name", array('name' => 'player[' . $i . '][first_name]', 'class' => 'form-control player_first_name_row', 'data-validate-required' => 'required', 'data-index' => 'first_name' . $i, 'id' => 'player_first_name' . $i)) ?>
                        </div>
                        <div class="form-group col-sm-3">
                            <label class="control-label"><?php echo $translator->translate($addForm->getFieldLabel("last_name")) ?></label>
                            <?php echo $addForm->renderInputTag("last_name", array('name' => 'player[' . $i . '][last_name]', 'class' => 'form-control player_last_name_row', 'data-index' => 'last_name' . $i, 'id' => 'player_last_name' . $i)) ?>
                        </div>

                        <div class="form-group col-sm-2">
                            <label class="control-label"><?php echo $translator->translate($addForm->getFieldLabel("player_number")) ?></label>
                            <?php echo $addForm->renderInputTag("player_number", array('name' => 'player[' . $i . '][player_number]', 'class' => 'form-control', 'data-index' => 'player_number' . $i, 'id' => 'player_number' . $i)) ?>
                        </div>


                        <div class="form-group col-sm-3 autocomplete_container">
                            <label class="control-label"><?php echo $translator->translate($addForm->getFieldLabel("email")) ?></label>
                            <?php echo $addForm->renderInputTag("email", array('name' => 'player[' . $i . '][email]', 'class' => 'form-control user-find-autocomplete player_email_row', 'autocomplete' => 'off', 'data-index' => 'email' . $i, 'data-row' => $i, 'id' => 'player_email' . $i)) ?>
                            <div class="autocomplete_wrap">
                                <ul class="autocomplete_list">

                                </ul>
                            </div>
                        </div>
                        <div class=" col-sm-1 btn_col">
                            <a  class="rm_player_btn" href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo $translator->translate('tournament.players.remove') ?>"><i class="ico ico-cross"></i></a>
                        </div>
                    </div>
                </div>



                <form action="<?php echo $router->link('tournament_team_add_player', array('id' => $tournament->getId())) ?>" id="add_player_form" method="post">
                    <div class="panel-heading">
                        <strong><?php echo $translator->translate('tournament.players.addPlayerTitle') ?></strong>
                    </div>

                    <div class="panel-body">
                        <input type="hidden" name="team_id" id="player_team_id" value="<?php echo $teamId ?>" />
                        <input type="hidden" name="type" value="<?php echo $type ?>"/>



                        <div id="add_player_wrap">
                            <?php for ($i = 0; $i < 1; $i++): ?>
                                <div class="row player_row">
                                    <input type="hidden" name="player[<?php echo $i ?>][player_id]" value="" id="player_id<?php echo $i ?>" />

                                    <div class="form-group col-sm-3">
                                        <label class="control-label"><?php echo $translator->translate($addForm->getFieldLabel("first_name")) ?> *</label>
                                        <?php echo $addForm->renderInputTag("first_name", array('name' => 'player[' . $i . '][first_name]', 'class' => 'form-control player_first_name_row', 'data-validate-required' => 'required', 'data-index' => 'first_name' . $i, 'id' => 'player_first_name' . $i)) ?>
                                    </div>
                                    <div class="form-group col-sm-3">
                                        <label class="control-label"><?php echo $translator->translate($addForm->getFieldLabel("last_name")) ?></label>
                                        <?php echo $addForm->renderInputTag("last_name", array('name' => 'player[' . $i . '][last_name]', 'class' => 'form-control player_last_name_row', 'data-index' => 'last_name' . $i, 'id' => 'player_last_name' . $i)) ?>
                                    </div>

                                    <div class="form-group col-sm-2">
                                        <label class="control-label"><?php echo $translator->translate($addForm->getFieldLabel("player_number")) ?></label>
                                        <?php echo $addForm->renderInputTag("player_number", array('name' => 'player[' . $i . '][player_number]', 'class' => 'form-control', 'data-index' => 'player_number' . $i, 'id' => 'player_number' . $i)) ?>
                                    </div>

                                    <div class="form-group col-sm-3 autocomplete_container">
                                        <label class="control-label"><?php echo $translator->translate($addForm->getFieldLabel("email")) ?></label>
                                        <?php echo $addForm->renderInputTag("email", array('name' => 'player[' . $i . '][email]', 'class' => 'form-control user-find-autocomplete player_email_row', 'autocomplete' => 'off', 'data-index' => 'email' . $i, 'data-row' => $i, 'id' => 'player_email' . $i)) ?>
                                        <div class="autocomplete_wrap">
                                            <ul class="autocomplete_list">

                                            </ul>
                                        </div>
                                    </div>
                                    <div class=" col-sm-1 btn_col"></div>
                                </div>
                            <?php endfor; ?>


                        </div>
                        <div class="row">
                            <div class=" form-group col-sm-12">
                                <a class="btn btn-clear btn-clear-green" id="add_player" href=""><?php echo $translator->translate('tournament.players.additem') ?></a><br />
                            </div>
                        </div>

                        <div class="row" id="action_row">
                            <div class="form-group col-sm-12">
                                <div id="error-alerts">* <?php echo $translator->translate('First name is required') ?></div>
                            </div>
                            <div class="form-group col-sm-12 text-center">
                                <div class="gdpr-teamPlayers">
                                    <?php echo $translator->translate('gdpr.teamPlayers.text') ?>
                                </div>
                                <button type="submit" class="btn btn-primary" href=""><?php echo $translator->translate('tournament.players.addcontact') ?> <i class="fa fa-chevron-right    "></i></button>
                            </div>
                        </div>

                    </div>

                </form>
            </div>


            <div class="panel panel-default">
                <div class="event-attendance" id="detail_event_attendance">

                    <div class="row">
                        <div class="col-sm-12 attendance-group" id="attendance-group-in-players" data-event-limit="2" style="">
                            <table class="table resp-table player-list-table">
                                <thead>
                                    <tr>
                                        <th class="number-cell">
                                            #
                                        </th>
                                        <th>

                                            <?php echo $translator->translate('tournament.players.table.firstName') ?>,  <?php echo $translator->translate('tournament.players.tablelastName') ?>,  <?php echo $translator->translate('tournament.players.table.email') ?>
                                        </th>
                                        <th>
                                            <?php echo $translator->translate('tournament.players.table.teamRole') ?>
                                        </th>
                                        <th>
                                            <?php echo $translator->translate('tournament.players.table.level') ?>
                                        </th>

                                        <th> </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($teamPlayers as $player): ?>
                                        <?php
                                        $layout->includePart(MODUL_DIR . '/tournament/view/teams/_edit_player_row.php', array(
                                            'player' => $player,
                                            'editForm' => $editForm,
                                            'team' => $team,
                                            'validator' => $validator,
                                            'rowClass' => $player->getStatus(),
                                            'tournament' => $tournament
                                        ))
                                        ?> 
<?php endforeach; ?>
                                </tbody>
                            </table>




                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div> 
</section>

<?php $layout->includePart(MODUL_DIR . '/tournament/view/teams/_remove_player_modal.php') ?>
<?php $layout->includePart(MODUL_DIR . '/tournament/view/teams/_edit_player_modal.php', array('form' => $modalForm, 'tournament' => $tournament)) ?>
 <?php $layout->includePart(MODUL_DIR .'/tournament/view/teams/_edit_photo_modal.php', array('teamId' => $team->getId())) ?>


<?php $layout->addJavascript('plugins/fileupload/jquery.iframe-transport.js') ?>
<?php $layout->addJavascript('plugins/fileupload/jquery.fileupload.js') ?>
<?php $layout->addJavascript('js/TournamentPlayersManager.js') ?>
<?php $layout->addJavascript('js/TournamentManager.js') ?>



<?php $layout->startSlot('javascript') ?>
<script type="text/javascript">


    TournamentPlayersManager = new TournamentPlayersManager();
    TournamentPlayersManager.bindTriggers();

    TournamentManager = new TournamentManager();
    TournamentManager.addEditPhotoTrigger($('.edit-team-photo'));
    TournamentManager.addRemovePhotoTrigger($('.delete-team-photo'));
    TournamentManager.fileuploadTrigger();


    //var event_manager = new TournamentEventManager();
    //event_manager.bindTriggers();

</script>
<?php $layout->endSlot('javascript') ?>