<tr id="team_row_<?php echo $team->getId() ?>" class="edit-team-row static">
    <td>

            <label class="control-label"><?php echo $translator->translate($editForm->getFieldLabel("name")) ?> *</label>
            <?php echo $editForm->renderInputTag("name", array('id' => 'team_name_' . $team->getId(), 'name' => 'team[' . $team->getId() . '][name]', 'class' => 'form-control', 'value' => $team->getName())) ?>

    </td>
    <td>

            <label class="control-label"><?php echo $translator->translate($editForm->getFieldLabel("email")) ?></label>
           
             <input type="hidden" name="team[<?php echo $team->getId() ?>][name]" value="<?php echo $team->getName()  ?>" />
            <?php echo $editForm->renderInputTag("email", array(
                'name' => 'team[' . $team->getId() . '][email]', 
                'class' => 'form-control user-find-autocomplete team_email_row', 
                'autocomplete' => 'off', 
                'data-index' => 'email' . $team->getId(), 
                'data-row' => $team->getId(), 
                'id' => 'team_email' . $team->getId(),
                'value' => $team->getEmail()
            )) ?>
            
          
    </td>
    <td>
          <?php if($team->getEmail() != ''): ?>
            <a class="tournament_team_invite_admin_trigger btn btn-primary" data-email="<?php echo $team->getEmail() ?>" data-input="<?php echo 'team_email' . $team->getId() ?>" href="<?php echo $router->link('tournament_team_invite_admin',array('tid' => $team->getId())) ?>" ><?php echo $translator->translate('tournament.team.list.sendInvitation') ?></a>
            <?php else: ?>
            Nie je vyplnený email
             <?php endif; ?>

    </td>
</tr>