<?php Core\Layout::getInstance()->startSlot('tournament_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/tournament/view/tournament/_menu.php', array('tournament' => $tournament)) ?>
<?php Core\Layout::getInstance()->endSlot('tournament_menu') ?>

<section class="content-header">
    <h1>
        <?php echo $translator->translate('tournament.team.list.title') ?>
    </h1>

    <?php $layout->includePart(GLOBAL_DIR . '/templates/parts/_breadcrumb.php', array('crumbs' => array('tournament.bradcrumb.title' => $router->link('tournament')))) ?>
</section>

<section class="content">
    
     <div id="teams-map"  style="width:100%; height:600px;"></div>
    
     <a class="btn btn-primary" href="<?php echo $router->link('tournament_detail',array('id' => $tournament->getId(),'format' => 'xls')) ?>">EXPORT</a>
    <div class="panel">
        <div class="panel-body"> 
<form action="" method="post">
    <input type="hidden" name="form_act" value="team_edit" />
            <table class="table resp-table player-list-table">
                <thead>
                <th><?php echo $translator->translate('tournament.team.table.name') ?></th>
                <th><?php echo $translator->translate('tournament.team.table.status') ?></th>
                <th><?php echo $translator->translate('tournament.team.table.email') ?></th>
                <th><?php echo $translator->translate('tournament.team.table.locality') ?></th>
                <th><?php echo $translator->translate('tournament.team.table.gender') ?></th>
                <th><?php echo $translator->translate('tournament.team.table.id') ?></th>

                </thead>
                
                <tbody>
                    <?php foreach($teams as $team): ?>
                    <tr>
                        <td><a href="<?php echo $router->link('team_public_page',array('id' => $team->getId())) ?>"><?php echo $team->getName()  ?></a></td>
                        <td>
                            
                            <div class="input-group player_status_group" >
                                  <span class="input-group-addon" >
                                    <?php if('confirmed' == $team->getStatus()): ?>
                                       <span data-toggle="tooltip" data-placement="top" title="<?php echo $translator->translate('Confirmed') ?>" class="round_btn bg_green"><i class="ico ico-check"></i></span>
                                     <?php endif; ?>

                                    

                                    <?php if('unknown' == $team->getStatus() or 'waiting-to-confirm' == $team->getStatus() ): ?>
                                       <span data-toggle="tooltip" data-placement="top" title="<?php echo $translator->translate('Unknown') ?>" class="round_btn bg_gray"><i class="ico ico-dots-h"></i></span>
                                     <?php endif; ?>

                                     <?php if('decline' == $team->getStatus() ): ?>
                                       <span data-toggle="tooltip" data-placement="top" title="<?php echo $translator->translate('Decline') ?>" class="round_btn bg_red"><i class="ico ico-cross"></i></span>
                                     <?php endif; ?>

                                   </span>
                                 <?php echo $teamForm->renderSelectTag("status", array('id' => 'team_status_' . $team->getId(), 'name' => 'team[' . $team->getId() . '][status]', 'class' => 'form-control', 'value' => $team->getStatus())) ?>
                            </div>

                        
                        </td>
                        <td><a href="mailto:<?php echo $team->getCreatorEmail() ?>"><?php echo $team->getCreatorEmail() ?></a></td>
                        <td>
                            <?php if($team->getLocality() != null): ?>
 <?php echo  $team->getLocality()->getName() ?>, <a onclick="window.open(this,'_blank');return false;" href="https://maps.google.com/maps/?q=<?php echo $team->getLocality()->getLat() ?>,<?php echo $team->getLocality()->getLng() ?>"></a>
<?php endif; ?>
                           
                        </td>
                        <td>
                            <?php echo $team->getGender() ?>
                        </td>
                        <td>
                            <?php echo $team->getId() ?>
                        </td>
                      
                    </tr>
                    
                    <?php endforeach; ?>
                </tbody>
            </table>
      <div class="col-sm-12 text-center save-changes">
        <button class="btn btn-primary" type="submit"><?php echo $translator->translate('Save changes') ?></button>
        </div>
</form>
        </div>
    </div>
</section>

<?php $layout->startSlot('javascript') ?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXU1duK54f1gIsRy_xc7z4uQURovsZdE8&libraries=places&callback=initMap" async defer></script>
<script>
    
    function initMap() {
         var map = new google.maps.Map(document.getElementById('teams-map'), {
            zoom: 8,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        
        map.setCenter({lat:48.799864, lng:19.1955283});
        
       
        
        <?php foreach($teams as $team): ?>
        <?php if($team->getLocality() != null): ?>
            place_marker<?php echo $team->getId() ?> = new google.maps.Marker({
               position: {lat:<?php echo $team->getLocality()->getLat()  ?>, lng:<?php echo $team->getLocality()->getLng()  ?>},
                label: {
                text: '<?php echo $team->getName()  ?>',
                color: 'black',
                fontSize: "10px"
              },
               map: map
           });
        <?php endif; ?>
        <?php endforeach; ?>
        
        
    };
    
   


    
    
    
    /*
    var map = new ol.Map({
    target: 'teams-map',
    layers: [
      new ol.layer.Tile({
        source: new ol.source.OSM()
      })
    ],
    view: new ol.View({
      center: ol.proj.fromLonLat([19.1955283,48.799864]),
      zoom: 8
    })
  });
  */
</script>
  
<?php $layout->endSlot('javascript') ?>