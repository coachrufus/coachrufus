<?php Core\Layout::getInstance()->startSlot('tournament_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/tournament/view/tournament/_menu.php', array('tournament' => $tournament)) ?>
<?php Core\Layout::getInstance()->endSlot('tournament_menu') ?>

<?php Core\Layout::getInstance()->startSlot('crumb') ?> 
<?php $layout->includePart(GLOBAL_DIR . '/templates/parts/_breadcrumb.php', array('crumbs' => array('tournament.bradcrumb.title' => $router->link('tournament'), 'tournament.bradcrumb.teams' => $router->link('tournament_teams', array('team_id' => $tournament->getId())), 'Edit' => ''))) ?> <?php Core\Layout::getInstance()->endSlot('crumb') ?>   

<section class="content">
    <div class="row">
        
        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3><?php echo $team->getName() ?></h3>
                </div>

                
                 <div class="panel-body">
                <div class="event-attendance" id="detail_event_attendance">

                    <div class="row">
                        <div class="col-sm-12 attendance-group" id="attendance-group-in-players" data-event-limit="2" style="">
                            <?php foreach ($teamPlayers as $player): ?>
                                <div class="attendant-row attendant-row-<?php echo $player->getId() ?>">
                                    <span class="ord-num"> </span> 
                                    
                                    <div class="user-block pull-left">
                                        <div class="round-50" style="background-image:url(<?php echo $player->getMidPhoto() ?>)"></div>
                                        <span class="username"><?php echo $player->getFullName() ?></span> <span class="description"> #<?php echo $player->getPlayerNumber() ?></span> 
                                        
                                      
                                        
                                       
                                    </div>
                                   
                                    <!-- /.user-block -->   
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
       
    </div>
  
</div> 
</section>
