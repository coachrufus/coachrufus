<?php Core\Layout::getInstance()->startSlot('tournament_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/tournament/view/tournament/_menu.php', array('tournament' => $tournament)) ?>
<?php Core\Layout::getInstance()->endSlot('tournament_menu') ?>

<?php Core\Layout::getInstance()->startSlot('crumb') ?> 
<?php $layout->includePart(GLOBAL_DIR . '/templates/parts/_breadcrumb.php', array('crumbs' => array('tournament.bradcrumb.title' => $router->link('tournament'), 'tournament.bradcrumb.teams' => $router->link('tournament_teams', array('team_id' => $tournament->getId())), 'Edit' => ''))) ?> <?php Core\Layout::getInstance()->endSlot('crumb') ?>   

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">

                <div id="template_row_container" style="display:none;">
                    <?php $i = 0; ?>
                    <div class="row player_row template_row">
                        <input type="hidden" name="player[<?php echo $i ?>][player_id]" value="" id="player_id<?php echo $i ?>" />

                        <div class="form-group col-sm-3">
                            <label class="control-label"><?php echo $translator->translate($addForm->getFieldLabel("first_name")) ?> *</label>
                            <?php echo $addForm->renderInputTag("first_name", array('name' => 'player[' . $i . '][first_name]', 'class' => 'form-control player_first_name_row', 'data-validate-required' => 'required', 'data-index' => 'first_name' . $i, 'id' => 'player_first_name' . $i)) ?>
                        </div>
                        <div class="form-group col-sm-3">
                            <label class="control-label"><?php echo $translator->translate($addForm->getFieldLabel("last_name")) ?></label>
                            <?php echo $addForm->renderInputTag("last_name", array('name' => 'player[' . $i . '][last_name]', 'class' => 'form-control player_last_name_row', 'data-index' => 'last_name' . $i, 'id' => 'player_last_name' . $i)) ?>
                        </div>
                        
                        <div class="form-group col-sm-2">
                            <label class="control-label"><?php echo $translator->translate($addForm->getFieldLabel("player_number")) ?></label>
                            <?php echo $addForm->renderInputTag("player_number", array('name' => 'player[' . $i . '][player_number]', 'class' => 'form-control', 'data-index' => 'player_number' . $i, 'id' => 'player_number' . $i)) ?>
                        </div>
                        
                        
                        <div class="form-group col-sm-3 autocomplete_container">
                            <label class="control-label"><?php echo $translator->translate($addForm->getFieldLabel("email")) ?></label>
                            <?php echo $addForm->renderInputTag("email", array('name' => 'player[' . $i . '][email]', 'class' => 'form-control user-find-autocomplete player_email_row', 'autocomplete' => 'off', 'data-index' => 'email' . $i, 'data-row' => $i, 'id' => 'player_email' . $i)) ?>
                            <div class="autocomplete_wrap">
                                <ul class="autocomplete_list">

                                </ul>
                            </div>
                        </div>
                        <div class=" col-sm-1 btn_col">
                            <a  class="rm_player_btn" href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo $translator->translate('tournament.players.remove') ?>"><i class="ico ico-cross"></i></a>
                        </div>
                    </div>
                </div>



                <form action="<?php echo $router->link('tournament_team_add_player', array('id' => $tournament->getId())) ?>" id="add_player_form" method="post">
                     <div class="panel-heading">
                        <strong><?php echo $translator->translate('tournament.players.addPlayerTitle') ?></strong>
                    </div>
                    
                    <div class="panel-body">
                        <input type="hidden" name="team_id" id="player_team_id" value="<?php echo $teamId ?>" />
                        <input type="hidden" name="type" value="<?php echo $type ?>"/>



                        <div id="add_player_wrap">
                            <?php for ($i = 0; $i < 1; $i++): ?>
                                <div class="row player_row">
                                    <input type="hidden" name="player[<?php echo $i ?>][player_id]" value="" id="player_id<?php echo $i ?>" />

                                    <div class="form-group col-sm-3">
                                        <label class="control-label"><?php echo $translator->translate($addForm->getFieldLabel("first_name")) ?> *</label>
                                        <?php echo $addForm->renderInputTag("first_name", array('name' => 'player[' . $i . '][first_name]', 'class' => 'form-control player_first_name_row', 'data-validate-required' => 'required', 'data-index' => 'first_name' . $i, 'id' => 'player_first_name' . $i)) ?>
                                    </div>
                                    <div class="form-group col-sm-3">
                                        <label class="control-label"><?php echo $translator->translate($addForm->getFieldLabel("last_name")) ?></label>
                                        <?php echo $addForm->renderInputTag("last_name", array('name' => 'player[' . $i . '][last_name]', 'class' => 'form-control player_last_name_row', 'data-index' => 'last_name' . $i, 'id' => 'player_last_name' . $i)) ?>
                                    </div>
                                    
                                     <div class="form-group col-sm-2">
                                        <label class="control-label"><?php echo $translator->translate($addForm->getFieldLabel("player_number")) ?></label>
                                        <?php echo $addForm->renderInputTag("player_number", array('name' => 'player[' . $i . '][player_number]', 'class' => 'form-control', 'data-index' => 'player_number' . $i, 'id' => 'player_number' . $i)) ?>
                                    </div>
                                    
                                    <div class="form-group col-sm-3 autocomplete_container">
                                        <label class="control-label"><?php echo $translator->translate($addForm->getFieldLabel("email")) ?></label>
                                        <?php echo $addForm->renderInputTag("email", array('name' => 'player[' . $i . '][email]', 'class' => 'form-control user-find-autocomplete player_email_row', 'autocomplete' => 'off', 'data-index' => 'email' . $i, 'data-row' => $i, 'id' => 'player_email' . $i)) ?>
                                        <div class="autocomplete_wrap">
                                            <ul class="autocomplete_list">

                                            </ul>
                                        </div>
                                    </div>
                                    <div class=" col-sm-1 btn_col"></div>
                                </div>
                            <?php endfor; ?>


                        </div>
                        <div class="row">
                            <div class=" form-group col-sm-12">
                                <a class="btn btn-clear btn-clear-green" id="add_player" href=""><?php echo $translator->translate('tournament.players.additem') ?></a><br />
                            </div>
                        </div>

                        <div class="row" id="action_row">
                            <div class="form-group col-sm-12">
                                <div id="error-alerts">* <?php echo $translator->translate('First name is required') ?></div>
                            </div>
                            <div class="form-group col-sm-12 text-center">
                                <button type="submit" class="btn btn-primary" href=""><?php echo $translator->translate('tournament.players.addcontact') ?> <i class="fa fa-chevron-right    "></i></button>
                            </div>
                        </div>

                    </div>

                </form>
            </div>


            <div class="panel panel-default">
                <div class="event-attendance" id="detail_event_attendance">

                    <div class="row">
                        <div class="col-sm-12 attendance-group" id="attendance-group-in-players" data-event-limit="2" style="">
                            <?php foreach ($teamPlayers as $player): ?>
                                <div class="attendant-row attendant-row-<?php echo $player->getId() ?>">
                                    <span class="ord-num"> </span> 
                                    
                                    <div class="user-block pull-left">
                                        <div class="round-50" style="background-image:url(<?php echo $player->getMidPhoto() ?>)"></div>
                                        <span class="username"><?php echo $player->getFullName() ?></span> <span class="description"> #<?php echo $player->getPlayerNumber() ?></span> 
                                        
                                       <div id="inline-edit-<?php echo $player->getId() ?>" class="inline-edit-wrap"  >
                                           <form action="<?php echo $router->link('tournament_team_edit_player', array('id' => $tournament->getId())) ?>">
                                               <input type="hidden" name="player[id]" value="<?php echo $player->getId()  ?>" />
                                               
                                        <div class="col-sm-12 col-lg-3">
                                             <?php echo $addForm->renderInputTag("first_name", array('id' => 'player_first_name_' . $player->getId(), 'name' => 'player[first_name]', 'class' => 'form-control', 'value' => $player->getFirstName())) ?>
                                        </div>
                                        <div class="col-sm-12 col-lg-3">
                                           <?php echo $addForm->renderInputTag("last_name", array('id' => 'player_last_name_' . $player->getId(), 'name' => 'player[last_name]', 'class' => 'form-control', 'value' => $player->getLastName())) ?>
                                        </div>
                                        <div class="col-sm-12 col-lg-2">
                                           <?php echo $addForm->renderInputTag("player_number", array('id' => 'player_player_number_' . $player->getId(), 'name' => 'player[player_number]', 'class' => 'form-control', 'value' => $player->getPlayerNumber())) ?>
                                        </div>
                                        <div class="col-sm-12 col-lg-3">
                                            <?php echo $addForm->renderInputTag("email", array('id' => 'player_email_' . $player->getId(), 'name' => 'player[email]', 'class' => 'form-control', 'value' => $player->getEmail())) ?>
                                        </div>
                                        <div class="col-sm-12 text-center col-lg-1">
                                             <i data-toggle="tooltip" data-placement="top" title="<?php echo $translator->translate('Cancel changes') ?>" data-edit="<?php echo $player->getId() ?>" class="ico ico-close player-row-cancel-edit  round_btn bg_red"></i>
                                        </div>
                                               <button class="submit-inline-form" type="submit">save</button>
                                           </form>

                                    </div>
                                        
                                       
                                    </div>
                                     <span class="pull-right">
                                            <i class="ico ico-edit ico-edit-player pull-right"></i>
                                            <a href="<?php echo $router->link('tournament_team_delete_player', array('tid' => $tournament->getId(),'id' => $player->getId() )) ?>"><i class="ico ico-trash  pull-right"></i></a>
                                        </span>
                                    <!-- /.user-block -->   
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
       
    </div>
  
</div> 
</section>
<?php $layout->addJavascript('plugins/fileupload/jquery.fileupload.js') ?>
<?php $layout->addJavascript('js/TournamentEventManager.js') ?>
<?php $layout->addJavascript('js/rufus/team/TeamPlayersManager.js?ver=' . VERSION) ?>

<?php $layout->startSlot('javascript') ?>
<script type="text/javascript">


    TeamPlayersManager = new TeamPlayersManager();
    TeamPlayersManager.bindTriggers();




    var event_manager = new TournamentEventManager();
    event_manager.bindTriggers();

</script>
<?php $layout->endSlot('javascript') ?>