<?php Core\Layout::getInstance()->startSlot('tournament_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/tournament/view/tournament/_menu.php', array('tournament' => $tournament)) ?>
<?php Core\Layout::getInstance()->endSlot('tournament_menu') ?>


<section class="content-header">
    <a class="all-event-link content-header-back" href="<?php echo $router->link('tournament') ?>"><i class="ico ico-event-back"></i><?php echo $translator->translate('tournament.backlink') ?></a>
</section>




<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                
                <div class="panel-heading">
                    <strong><?php echo $translator->translate('tournament.teams.title') ?></strong>
                </div>
                <div class="panel-body">
                    <div class="row">
                <form id="stats_filter_form" action="" method="get">
                    <input type="hidden" name="id" value="<?php echo $tournament->getId() ?>" />
                <div class="col-lg-2 col-sm-6">
                    
                    <?php echo $filterForm->renderSelectTag('region',array('class' => 'form-control' )) ?>
                  
                </div>
                <div class="col-lg-2 col-sm-6">
                     <?php echo $filterForm->renderSelectTag('group',array('class' => 'form-control' )) ?>
                </div>
                </form>
            </div>
                    <?php foreach ($teams as $team): ?>
                        <div class="box box-widget collapsed-box">
                            <div class="box-header with-border">
                                <span class="username">
                                   <?php if($permissionManager->hasTeamPermission($user,$team,'MANAGE_PLAYERS')): ?>
                                        <a class="btn btn-green" href="<?php echo $router->link('tournament_team_players',array('id' => $tournament->getId(),'team_id' => $team->getId())) ?>"><?php echo $team->getName() ?></a>
                                       
                                   <?php else: ?>
                                        <a href="<?php echo $router->link('tournament_team_detail',array('id' => $tournament->getId(),'team_id' => $team->getId())) ?>"><?php echo $team->getName() ?></a>
                                   <?php endif; ?>
                                    
                                </span>
                            </div><!-- /.user-block -->
                        </div><!-- /.box-header -->
                    <?php endforeach; ?>
                </div>

               
            </div>
        </div>
    </div> 
</section>



<?php $layout->startSlot('javascript') ?>
 <script>
      $(function () {
        $('#stats_filter_form select').on('change',function(){
            $('#stats_filter_form').submit();
        });
        

       
      });
      
     
    </script>
  
<?php $layout->endSlot('javascript') ?>

