
            <table class="table resp-table player-list-table">
                <thead>
                <th><?php echo $translator->translate('tournament.team.table.name') ?></th>
                <th><?php echo $translator->translate('tournament.team.table.status') ?></th>
                <th><?php echo $translator->translate('tournament.team.table.email') ?></th>
                <th>Adresa</th>
                <th>Kraj</th>
                <th>Okres</th>
                <th>Mesto</th>
                <th>Pohlavie</th>
                <th>Id timu</th>

                </thead>
                
                <tbody>
                    <?php foreach($teams as $team): ?>
                    
                    
                    <tr>
                        <td><?php echo $team->getName()  ?></td>
                        <td>
                            
                            <div class="input-group player_status_group" >
                                  <span class="input-group-addon" >
                                    <?php if('confirmed' == $team->getStatus()): ?>
                                      <?php echo $translator->translate('Confirmed') ?>
                                     <?php endif; ?>

                                    

                                    <?php if('unknown' == $team->getStatus() or 'waiting-to-confirm' == $team->getStatus() ): ?>
                                      <?php echo $translator->translate('Unknown') ?>
                                     <?php endif; ?>

                                     <?php if('decline' == $team->getStatus() ): ?>
                                     <?php echo $translator->translate('Decline') ?>
                                     <?php endif; ?>

                                   </span>
                              
                            </div>

                        
                        </td>
                        <td><a href="mailto:<?php echo $team->getCreatorEmail() ?>"><?php echo $team->getCreatorEmail() ?></a></td>
                        <td>
                            <?php if($team->getLocality() != null): ?>
                                <?php echo  $team->getLocality()->getFullAddress() ?>
                            <?php endif; ?>
                        </td>
                        <td>
                            <?php if($team->getLocality() != null): ?>
                                <?php echo  $team->getLocality()->getRegion() ?>
                            <?php endif; ?>
                        </td>
                        <td>
                            <?php if($team->getLocality() != null): ?>
                                <?php echo  $team->getLocality()->getDistrict() ?>
                            <?php endif; ?>
                        </td>
                        <td>
                            <?php if($team->getLocality() != null): ?>
                                <?php echo  $team->getLocality()->getCity2() ?>
                            <?php endif; ?>
                        </td>
                        <td>
       
                                <?php echo  $team->getGender() ?>

                        </td>
                      
                        <td>
       
                                <?php echo  $team->getId() ?>

                        </td>
                      
                    </tr>
                    
                    <?php endforeach; ?>
                </tbody>
            </table>
     