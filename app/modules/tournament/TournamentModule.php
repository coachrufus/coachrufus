<?php
namespace CR\Tournament;
use Core\Module as Module;

class TournamentModule extends Module
{
    public function __construct() 
    {
        $this->setBaseDir(MODUL_DIR.'/tournament');
        $this->setName('Tournament');
    }
}

