<?php

namespace CR\Tournament\Form;

use \Core\Form as Form;

class EventForm extends Form {

    private $playgroundsDesc;
    protected $fields = array(
       
        'start' =>
        array(
            'type' => 'datetime',
            'max_length' => '',
            'required' => false,
            'label' => 'Date',
            'format' => 'd.m.Y H:i:s'
        ),
        'start_datetime' =>
        array(
            'type' => 'datetime',
            'max_length' => '',
            'required' => true,
            'label' => 'Start from',
            'format' => 'd.m.Y H:i'
        ),
       
        'end_time_time' =>
        array(
            'type' => 'time',
            'max_length' => '',
            'required' => false,
            'label' => 'End time',
        ),
        'end' =>
        array(
            'type' => 'datetime',
            'max_length' => '',
            'required' => false,
        ),
      
        'team_id' =>
        array(
            'type' => 'choice',
            'max_length' => '11',
            'required' => false,
            'label' => 'tournament.event.team.title'
        ),
        'opponent_team_id' =>
        array(
            'type' => 'choice',
            'max_length' => '11',
            'required' => false,
            'label' => 'tournament.event.opponent_team.title'
        ),
       
        'playground_name' =>
        array(
            'type' => 'string',
            'max_length' => '255',
            'required' => false,
            'label' => 'tournament.event.playground_name.title'
        ),
       
       
      
      
    );

    public function __construct()
    {
        
    }

   

}
