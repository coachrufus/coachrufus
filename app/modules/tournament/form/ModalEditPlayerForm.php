<?php
namespace CR\Tournament\Form;

use Core\Form as Form;
use Core\ServiceLayer;


class ModalEditPlayerForm extends TeamPlayerForm 
{
	
				
    public function __construct()
    {
        parent::__construct();
        $this->setName('edit_modal_form');
    }
}