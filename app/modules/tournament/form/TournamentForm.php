<?php

namespace CR\Tournament\Form;

use Core\Form as Form;
use Core\ServiceLayer;

class TournamentForm extends Form {

    protected $fields = array(
        'age_from' =>
        array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false,
        ),
        'age_to' =>
        array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false,
        ),
        'status' =>
        array(
            'type' => 'choice',
            'max_length' => '50',
            'required' => false,
            'choices' => array(),
            'label' => 'tournament.form.status.label'
        ),
        'description' =>
        array(
            'type' => 'string',
            'max_length' => '1000',
            'required' => false,
            'label' => 'tournament.form.description.label'
        ),
        'phone' =>
        array(
            'type' => 'string',
            'max_length' => '50',
            'required' => false,
            'label' => 'tournament.form.phone.label'
        ),
        'email' =>
        array(
            'type' => 'string',
            'max_length' => '50',
            'required' => false,
            'label' => 'tournament.form.email.label'
        ),
    );

    public function __construct()
    {
        $this->setName('tournament');
        $translator = ServiceLayer::getService('translator');

        $this->setField('name', array(
            'type' => 'string',
            'max_length' => '255',
            'required' => true,
            'label' => 'tournament.form.name.label'
        ));
        $this->setField('start_date', array(
            'type' => 'datetime',
            'required' => true,
             'format' => 'd.m.Y H:i',
            'label' => 'tournament.form.start_date.label'
        ));
        $this->setField('end_date', array(
            'type' => 'datetime',
             'format' => 'd.m.Y H:i',
            'required' => true,
            'label' => 'tournament.form.end_date.label'
        ));


        $this->setField('sport_id', array(
            'type' => 'choice',
            'choices' => array(),
            'required' => true,
            'label' => 'tournament.form.sport.label'
        ));

        $this->setField('exist_playgrounds', array(
            'type' => 'choices',
            'choices' => array(),
            'required' => false,
            'label' => 'tournament.form.end_date.playground'
        ));
        
        

        $this->setField('gender', array(
            'type' => 'choices',
            'choices' => array(),
            'required' => false,
            'label' => 'tournament.form.gender.label'
        ));

        $this->setField('age_group', array(
            'type' => 'choices',
            'choices' => array(),
            'required' => false,
            'label' => 'tournament.form.age_group.label'
        ));


        $this->setField('level', array(
            'type' => 'choices',
            'choices' => array(),
            'required' => false,
            'label' => 'tournament.form.level.label'
        ));



        $this->setField('photo', array(
            'type' => 'string',
            'max_length' => '255',
            'label' => 'tournament.form.photo.label'
        ));
        $this->setField('address_city', array(
            'type' => 'string',
            'max_length' => '255',
            'required' => false,
            'label' => 'tournament.form.address_city.label'
        ));
        $this->setField('address_zip', array(
            'type' => 'string',
            'max_length' => '255',
            'required' => false,
            'label' => 'tournament.form.address_zip.label'
        ));
        $this->setField('address_street', array(
            'type' => 'string',
            'max_length' => '255',
            'required' => false,
            'label' => 'tournament.form.address_street.label'
        ));
        $this->setField('address_street_num', array(
            'type' => 'string',
            'max_length' => '255',
            'required' => false,
            'label' => 'tournament.form.address_street_num.label'
        ));
        $this->setField('full_address', array(
            'type' => 'string',
            'max_length' => '255',
            'required' => false,
            'label' => 'tournament.form.full_address.label'
        ));
        
        $this->setField('register_status', array(
            'type' => 'choice',
            'choices' => array(
                 '' => $translator->translate('tournament.form.register_status.choice.default'),
                'allowed' => $translator->translate('tournament.form.register_status.choice.yes'),
                'disabled' => $translator->translate('tournament.form.register_status.choice.no'),
            ),
            'required' => true,
            'label' => 'tournament.form.register_status.label'
        ));
        
        
    }

    public function hasPlaygrounds()
    {
        $playgrounds = $this->getFieldValue('playgrounds');
        if (!empty($playgrounds))
        {
            return true;
        }

        return false;
    }
    
    public function renderSportSelect($index,$options)
    {
      $translator = ServiceLayer::getService('translator');
        $options_string = '';
		$html_options_string = $this->getFieldOptionString($index,$options);
		$value = $this->getFieldValue($index);
                $field_name =  $this->getFieldName($index);
                
                if(array_key_exists('value',$options))
                {
                    $value = $options['value'];
                }
                
                if(array_key_exists('name',$options))
                {
                    $field_name = $options['name'];
                }
                
                
              
		if(array_key_exists('choices', $this->fields[$index]))
		{
			foreach($this->fields[$index]['choices'] as $key => $name)
			{
				$options_string .= '<optgroup label="'.$translator->translate($key).'">';

                                foreach($name as $key_val => $val)
                                {
                                        if($value == $key_val)
                                        {
                                                $options_string .= '<option class="'.$key.'" selected="selected" value="'.$key_val.'">'.$translator->translate($val).'</option>'."\n";
                                        }
                                        else
                                        {
                                                $options_string .= '<option class="'.$key.'" value="'.$key_val.'">'.$translator->translate($val).'</option>'."\n";
                                        }
                                }
                                $options_string .= '</optgroup>';
                                
			}
		}
		else
		{
			//TODO throw exception
		}
		
		return '<select name="'. $field_name.'"'.$html_options_string.'>'."\n".$options_string.'</select>';

        
        
       
    }

}
