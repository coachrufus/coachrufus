<?php

namespace CR\Tournament\Form;

use Core\Validator;

class TournamentValidator extends Validator {

    public function validateData()
    {
        parent::validateData();

       
                $data = $this->getData();

        if(null != $data['description'])
        {
            if(mb_strlen($data['description']) > 1000)
            {
                $this->addErrorMessage('description', 'Max. 1000 characters');
            }
        }
    }

    public function validatePlayground($playgroundData)
    {
       return true;
        if(null == $playgroundData or empty($playgroundData))
       {
            $mesage = 'No playground selected';
            $this->addErrorMessage('playground_empty', $mesage);
       }
       elseif(empty( $playgroundData[0]['locality_json_data']))
       {
            $mesage = 'No playground selected';
            $this->addErrorMessage('playground_empty', $mesage);
       }
      
    }

}
