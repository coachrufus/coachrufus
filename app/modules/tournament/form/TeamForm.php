<?php

namespace CR\Tournament\Form;

use \Core\Form as Form;

class TeamForm extends Form {

   

    public function __construct()
    {
          $this->setField('status', array(
            'type' => 'choice',
            'choices' =>  $this->getAvailableStatusChoices(),
            'required' => false,
        ));
    }
    
      public function getAvailableStatusChoices()
    {
         $translator = \Core\ServiceLayer::getService('translator');
          $choices = array(
            'waiting-to-confirm' => $translator->translate('tournament.teamform.status.waiting-to-confirm'),
            'confirmed' => $translator->translate('tournament.teamform.status.confirmed'),
            'decline' => $translator->translate('tournament.teamform.status.decline'),
        );
          
          return $choices;
    }

}
