<?php

namespace CR\Tournament\Form;

use Core\Form as Form;
use Core\ServiceLayer;

class StatsFilterForm extends Form {

   
    public function __construct()
    {
        $translator = ServiceLayer::getService('translator');
        
        $this->setName('filter');

        $this->setField('region', array(
            'type' => 'choices',
            'choices' => array(
                '' => $translator->translate('tournament.stats.filter.region.all'),
                'ba' => $translator->translate('tournament.stats.filter.region.ba'),
                'bb' => $translator->translate('tournament.stats.filter.region.bb'),
                'tt' => $translator->translate('tournament.stats.filter.region.tt'),
                'tn' => $translator->translate('tournament.stats.filter.region.tn'),
                'nr' => $translator->translate('tournament.stats.filter.region.nr'),
                'za' => $translator->translate('tournament.stats.filter.region.za'),
                'po' => $translator->translate('tournament.stats.filter.region.po'),
                'ke' => $translator->translate('tournament.stats.filter.region.ke'),
            )
        ));
        $this->setField('group', array(
            'type' => 'choices',
            'choices' => array(
            )
        ));
        $this->setField('schedule_id', array(
            'type' => 'choices',
            'choices' => array(
            )
        ));
        
        //$this->buildDivisionChoices();
    }
    
    
    
    
    
    public function buildDivisionChoices($filterData = array(),$teams)
    {
        
        $translator = ServiceLayer::getService('translator');
        

        
        $divisonChoices = array('' => $translator->translate('tournament.stats.filter.group.all'));
        if (array_key_exists('region', $filterData) && null != $filterData['region'])
        {

            foreach ($teams as $team)
            {
                if ($team->getLocality() == $filterData['region'] . 'sk')
                {
                    $divisonChoices[$team->getDivision()] = $team->getDivision();
                }
            }
        }
        else
        {
            foreach ($teams as $team)
            {

                $divisonChoices[$team->getDivision()] = $team->getDivision();
            }
        }

        $this->setFieldChoices('group', $divisonChoices);
    }


}


