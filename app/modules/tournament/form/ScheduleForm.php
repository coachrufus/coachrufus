<?php

namespace CR\Tournament\Form;

use \Core\Form as Form;

class ScheduleForm extends Form {

    protected $fields = array(
        'phase_name' =>
            array(
                'type' => 'text',
                'max_length' => '20',
                'required' => false,
                
                'label' => 'tournament.schedule.form.phase_name',
            ),
        'type' =>
            array(
                'type' => 'choice',
                'max_length' => '20',
                'required' => false,
                'choices' => array(
                    '' => '',
                    'roundRobin/Kirkman' => 'Obyčajný rozpis',
                    'roundRobin/BalancedFields' => 'Týmy hrají stejný počet zápasů na každém hřišti (max 4 hřiště)',
                    'roundRobin/BalancedFreeTime' => 'Týmy čekají stejnou dobu mezi zápasy (jen pro lichý počet týmů)'
                ),
                'label' => 'tournament.schedule.form.type',
            ),
        'playoff_type' =>
            array(
                'type' => 'choice',
                'max_length' => '20',
                'required' => false,
                'choices' => array(
                    '' => '',
                    'playoff/SingleElimination' => 'Když tým prohraje, tak v turnaji končí',
                    'playoff/DoubleElimination' => 'I když tým jednou prohraje, tak má stále šanci vyhrát turnaj',
                    'playoff/Consolation' => 'Žádný tým nevypadává, ale hraje až do konce o konečné pořadí'
                ),
                'label' => 'tournament.schedule.form.playoff_type',
            ),
        'team_count' =>
            array(
                'type' => 'number',
                'max_length' => '20',
                'required' => false,
                'label' => 'tournament.schedule.form.team_count',
            ),
         'team_names_generate' =>
            array(
                'type' => 'choice',
                'max_length' => '20',
                'required' => false,
                'choices' => array(
                    '' => '',
                    'team' => 'Tím 1, Tím 2, Tím 3',
                    'number' => '1, 2, 3',
                    'letter' => 'A, B, C'
                ),
                
            ),
        'fields_count' =>
            array(
                'type' => 'number',
                'max_length' => '20',
                'required' => false,
                'label' => 'tournament.schedule.form.fields_count',
            ),
        'fields_paralel' =>
            array(
                'type' => 'number',
                'max_length' => '20',
                'required' => false,
                'label' => 'tournament.schedule.form.fields_paralel',
            ),
        'periods_count' =>
            array(
                'type' => 'number',
                'max_length' => '20',
                'required' => false,
                'label' => 'tournament.schedule.form.periods_count',
            ),
        'date_start' =>
            array(
                'type' => 'datetime',
                'max_length' => '20',
                'required' => false,
                'label' => 'tournament.schedule.form.date_start',
            ),
        'time_start' =>
            array(
                'type' => 'datetime',
                'max_length' => '20',
                'required' => false,
            ),
        'match_duration' =>
            array(
                'type' => 'number',
                'max_length' => '20',
                'required' => false,
                'label' => 'tournament.schedule.form.match_duration',
            ),
        'round_duration' =>
            array(
                'type' => 'number',
                'max_length' => '20',
                'required' => false,
                'label' => 'tournament.schedule.form.round_duration',
            ),
        'spare_rounds_count' =>
            array(
                'type' => 'number',
                'max_length' => '20',
                'required' => false,
                'label' => 'tournament.schedule.form.spare_rounds_count',
            ),
        'referee_type' =>
            array(
                'type' => 'choice',
                'max_length' => '20',
                'required' => false,
                'choices' => array(
                    '' => '',
                    'none' => ' Bez rozhodčích',
                    'referee/FreeTeams' => ' Zápasy píská člen z nehrajícího týmu ',
                    'referee/Professionals' => ' Profesionální rozhodčí, kteří pouze pískají zápasy '
                ),
                'label' => 'tournament.schedule.form.referee_type',
        ),
        'referees_count' =>
            array(
                'type' => 'number',
                'max_length' => '20',
                'required' => false,
                'label' => 'tournament.schedule.form.referees_count',
            ),
       
        'table_type' =>
            array(
                'type' => 'choice',
                'max_length' => '20',
                'required' => false,
                'choices' => array(
                    '' => '',
                    'table/Cross' => 'Křížová tabulka',
                    'table/Classic' => 'Klasická tabulka',
                   
                ),
                 'label' => 'tournament.schedule.form.table_type',
        ),
        'groups_count' =>
              array(
                'type' => 'number',
                'max_length' => '20',
                'required' => false,
                   'label' => 'tournament.schedule.form.groups_count',
        ),
        'matches_in_serie' =>
              array(
                'type' => 'number',
                'max_length' => '20',
                'required' => false,
                  'label' => 'tournament.schedule.form.matches_in_serie',
        ),
        'is_bronze_medal_match_played' =>
              array(
                'type' => 'choice',
                'max_length' => '20',
                'choices' => array(
                    'yes' => 'Yes',
                    'no' => 'No',
                   
                ),
                  'label' => 'tournament.schedule.form.is_bronze_medal_match_played',
        ),
        
    );

    public function __construct()
    {
        $this->name = 'schedule';
    }

}
