<?php
namespace CR\Tournament\Form;

use Core\Form as Form;
use Core\ServiceLayer;


class TeamPlayerForm extends Form 
{			
    public function __construct()
    {
        $this->setName('player');
        

        $this->setField('team_id', array(
            'type' => 'int',
            'required' => true,
        ));
        

        $this->setField('team_role', array(
            'type' => 'choice',
            'choices' =>  $this->getRoleChoices(),
            'required' => false,
        ));
        $this->setField('level', array(
            'type' => 'choice',
            'choices' =>  $this->getLevelChoices(),
            'required' => false,
        ));
        $this->setField('status', array(
            'type' => 'choice',
            'choices' =>  $this->getAvailableStatusChoices(),
            'required' => false,
        ));
        
        $this->setField('first_name', array(
            'type' => 'string',
            'required' => true,
        ));
        
        $this->setField('last_name', array(
            'type' => 'string',
            'required' => false,
        ));
        
        $this->setField('email', array(
            'type' => 'string',
            'required' => false,
        ));
        
          $this->setField('player_number', array(
            'type' => 'int',
              'required' => false,
        ));
        
    }
    
    public function getLevelChoices()
    {
        return \Core\ServiceLayer::getService('TournamentEnumManager')->getPlayerLevelEnum();
    }
    
    public function getRoleChoices()
    {
        return \Core\ServiceLayer::getService('TournamentEnumManager')->getPlayerRoleEnum();
    }
    
    public function getAvailableStatusChoices()
    {
        $choices = $this->statusChoices;
         unset($choices['waiting-to-confirm']);
        unset($choices['decline']);
        unset($choices['unknown']);
        unset($choices['unconfirmed']);
        return  $choices;
    }
    
    public  function getStatusChoices() {
return $this->statusChoices;
}

public  function setStatusChoices($statusChoices) {
$this->statusChoices = $statusChoices;
}


}