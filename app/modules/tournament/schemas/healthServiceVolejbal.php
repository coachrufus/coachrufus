<?php

$teams = array();

$teams[1] = array(
    'name' => 'Volejbal A1',
    'sport_id' => 6,
    'author_id' => 3,
    'players' => array(
       array(
           'first_name' => 'Martin',
           'last_name' => 'Jalovecký',
           'status' => 'confirmed',
           'team_role' => 'PLAYER'
       ),
       array(
           'first_name' => 'Kristína',
           'last_name' => 'Krajčiová',
           'status' => 'confirmed',
           'team_role' => 'PLAYER'
       ),
       array(
           'first_name' => 'Ján',
           'last_name' => 'Palaj',
           'status' => 'confirmed',
           'team_role' => 'PLAYER'
       ),
       array(
           'first_name' => 'Ivan',
           'last_name' => 'Kopča',
           'status' => 'confirmed',
           'team_role' => 'PLAYER'
       ),
    )
);
$teams[2] = array(
    'name' => 'Volejbal A2',
    'sport_id' => 6,
    'author_id' => 3,
    'players' => array(
        array(
           'first_name' => 'Peter',
           'last_name' => 'Pavelek',
           'status' => 'confirmed',
           'team_role' => 'PLAYER'
       ),
        array(
           'first_name' => 'Peter',
           'last_name' => 'Ďurana',
           'status' => 'confirmed',
           'team_role' => 'PLAYER'
       ),
        array(
           'first_name' => 'Adrian',
           'last_name' => 'Korenic',
           'status' => 'confirmed',
           'team_role' => 'PLAYER'
       ),
        array(
           'first_name' => 'Michal',
           'last_name' => 'Adamec',
           'status' => 'confirmed',
           'team_role' => 'PLAYER'
       ),
       
    )
);
$teams[3] = array(
    'name' => 'Volejbal A3',
    'sport_id' => 6,
    'author_id' => 3,
    'players' => array(
        array(
           'first_name' => 'Ondrej',
           'last_name' => 'Chytil',
           'status' => 'confirmed',
           'team_role' => 'PLAYER'
       ),
        array(
           'first_name' => 'Petr',
           'last_name' => 'Sucharda',
           'status' => 'confirmed',
           'team_role' => 'PLAYER'
       ),
        array(
           'first_name' => 'Martin',
           'last_name' => 'Šeliga',
           'status' => 'confirmed',
           'team_role' => 'PLAYER'
       ),
        array(
           'first_name' => 'Matúš',
           'last_name' => 'Mrlina',
           'status' => 'confirmed',
           'team_role' => 'PLAYER'
       ),
    )
);

$teams[4] = array(
    'name' => 'Volejbal A4',
    'sport_id' => 6,
    'author_id' => 3,
    'players' => array(
        array(
           'first_name' => 'Jozef',
           'last_name' => 'Štalmašek',
           'status' => 'confirmed',
           'team_role' => 'PLAYER'
       ),
        array(
           'first_name' => 'Ágnes',
           'last_name' => 'Horváthová',
           'status' => 'confirmed',
           'team_role' => 'PLAYER'
       ),
        array(
           'first_name' => 'Erik',
           'last_name' => 'Hrica',
           'status' => 'confirmed',
           'team_role' => 'PLAYER'
       ),
        array(
           'first_name' => 'Peter',
           'last_name' => 'Solivajs',
           'status' => 'confirmed',
           'team_role' => 'PLAYER'
       ),
    )
);
$teams[5] = array(
    'name' => 'Volejbal B1',
    'sport_id' => 6,
    'author_id' => 3,
    'players' => array(
          array(
           'first_name' => 'Lenka',
           'last_name' => 'Kosova',
           'status' => 'confirmed',
           'team_role' => 'PLAYER'
       ),
          array(
           'first_name' => 'Boris',
           'last_name' => 'Grega',
           'status' => 'confirmed',
           'team_role' => 'PLAYER'
       ),
       array(
           'first_name' => 'Peter',
           'last_name' => 'Tomek',
           'status' => 'confirmed',
           'team_role' => 'PLAYER'
       ),
       array(
           'first_name' => 'Jakub',
           'last_name' => 'Dujnič',
           'status' => 'confirmed',
           'team_role' => 'PLAYER'
       ),
    )
);
$teams[6] = array(
    'name' => 'Volejbal B2',
    'sport_id' => 6,
    'author_id' => 3,
    'players' => array(
        array(
           'first_name' => 'Michal',
           'last_name' => 'Krajčír',
           'status' => 'confirmed',
           'team_role' => 'PLAYER'
       ),
        array(
           'first_name' => 'Peter',
           'last_name' => 'Papaj',
           'status' => 'confirmed',
           'team_role' => 'PLAYER'
       ),
        array(
           'first_name' => 'Peter',
           'last_name' => 'Prištic',
           'status' => 'confirmed',
           'team_role' => 'PLAYER'
       ),
        array(
           'first_name' => 'Mária',
           'last_name' => 'Mičkalíková',
           'status' => 'confirmed',
           'team_role' => 'PLAYER'
       ),
    )
);
$teams[7] = array(
    'name' => 'Volejbal B3',
    'sport_id' => 6,
    'author_id' => 3,
    'players' => array(
       array(
           'first_name' => 'Jiří',
           'last_name' => 'Janda',
           'status' => 'confirmed',
           'team_role' => 'PLAYER'
       ),
       array(
           'first_name' => 'Daniel',
           'last_name' => 'Dubský',
           'status' => 'confirmed',
           'team_role' => 'PLAYER'
       ),
       array(
           'first_name' => 'Vladimír',
           'last_name' => 'Šíma',
           'status' => 'confirmed',
           'team_role' => 'PLAYER'
       ),
       array(
           'first_name' => 'Ivan',
           'last_name' => 'Čvančara',
           'status' => 'confirmed',
           'team_role' => 'PLAYER'
       ),
    )
);
$teams[8] = array(
    'name' => 'Volejbal B4',
    'sport_id' => 6,
    'author_id' => 3,
    'players' => array(
        array(
           'first_name' => 'Jan',
           'last_name' => 'Klouček',
           'status' => 'confirmed',
           'team_role' => 'PLAYER'
       ),
        array(
           'first_name' => 'Marián',
           'last_name' => 'Kotlár',
           'status' => 'confirmed',
           'team_role' => 'PLAYER'
       ),
         array(
           'first_name' => 'Martin',
           'last_name' => 'Medlen',
           'status' => 'confirmed',
           'team_role' => 'PLAYER'
       ),
         array(
           'first_name' => 'Michal',
           'last_name' => 'Gonda',
           'status' => 'confirmed',
           'team_role' => 'PLAYER'
       ),
    )
);


$schema = array(
    'tournament_id' => 1443,
    'name' => 'Health Services - volejbal',
    'sport_id' => 6,
    'author_id' => 3,
    'termin' => '2019-06-11 10:00:00',
    'round' => 1,
    'type' => 'ladder-playoff');
$schema['teamsIds'] = array();

$schema['events'][] = array(
    'start' => '2019-06-11 10:00:00',
    'end_time' => '2019-06-11 10:15:00',
    'team_schema_id' => 1,
    'opponent_team_schema_id' => 4,
    'author_id' => 3,
    'tournament_type' => 'ladder',
    'tournament_group' => 'A',
    'tournament_id' => 1443,
);

$schema['events'][] = array(
    'start' => '2019-06-11 10:20:00',
    'end_time' => '2019-06-11 10:35:00',
    'team_schema_id' => 2,
    'opponent_team_schema_id' => 3,
    'author_id' => 3,
    'tournament_type' => 'ladder',
    'tournament_group' => 'A',
    'tournament_id' => 1443,
);

$schema['events'][] = array(
    'start' => '2019-06-11 10:40:00',
    'end_time' => '2019-06-11 10:55:00',
    'team_schema_id' => 5,
    'opponent_team_schema_id' => 8,
    'author_id' => 3,
    'tournament_type' => 'ladder',
    'tournament_group' => 'B',
    'tournament_id' => 1443,
);
//B2	B3
$schema['events'][] = array(
    'start' => '2019-06-11 11:00:00',
    'end_time' => '2019-06-11 11:15:00',
    'team_schema_id' => 6,
    'opponent_team_schema_id' => 7,
    'author_id' => 3,
    'tournament_type' => 'ladder',
    'tournament_group' => 'B',
    'tournament_id' => 1443,
);
//A1	A3
$schema['events'][] = array(
    'start' => '2019-06-11 11:20:00',
    'end_time' => '2019-06-11 11:35:00',
    'team_schema_id' => 1,
    'opponent_team_schema_id' => 3,
    'author_id' => 3,
    'tournament_type' => 'ladder',
    'tournament_group' => 'A',
    'tournament_id' => 1443,
);
//A2	A4
$schema['events'][] = array(
    'start' => '2019-06-11 11:40:00',
    'end_time' => '2019-06-11 11:55:00',
    'team_schema_id' => 2,
    'opponent_team_schema_id' => 4,
    'author_id' => 3,
    'tournament_type' => 'ladder',
    'tournament_group' => 'A',
    'tournament_id' => 1443,
);
//B1	B3
$schema['events'][] = array(
    'start' => '2019-06-11 12:00:00',
    'end_time' => '2019-06-11 12:15:00',
    'team_schema_id' => 5,
    'opponent_team_schema_id' => 7,
    'author_id' => 3,
    'tournament_type' => 'ladder',
    'tournament_group' => 'B',
    'tournament_id' => 1443,
);
//B2	B4
$schema['events'][] = array(
    'start' => '2019-06-11 12:20:00',
    'end_time' => '2019-06-11 12:35:00',
    'team_schema_id' => 6,
    'opponent_team_schema_id' => 8,
    'author_id' => 3,
    'tournament_type' => 'ladder',
    'tournament_group' => 'B',
    'tournament_id' => 1443,
);

//A1	A2
$schema['events'][] = array(
    'start' => '2019-06-11 12:40:00',
    'end_time' => '2019-06-11 12:55:00',
    'team_schema_id' => 1,
    'opponent_team_schema_id' => 2,
    'author_id' => 3,
    'tournament_type' => 'ladder',
    'tournament_group' => 'A',
    'tournament_id' => 1443,
);

//A3	A4
$schema['events'][] = array(
    'start' => '2019-06-11 13:00:00',
    'end_time' => '2019-06-11 13:15:00',
    'team_schema_id' => 3,
    'opponent_team_schema_id' => 4,
    'author_id' => 3,
    'tournament_type' => 'ladder',
    'tournament_group' => 'A',
    'tournament_id' => 1443,
);

//B1	B2
$schema['events'][] = array(
    'start' => '2019-06-11 13:20:00',
    'end_time' => '2019-06-11 13:35:00',
    'team_schema_id' => 5,
    'opponent_team_schema_id' => 6,
    'author_id' => 3,
    'tournament_type' => 'ladder',
    'tournament_group' => 'B',
    'tournament_id' => 1443,
);

//B3	B4
$schema['events'][] = array(
    'start' => '2019-06-11 13:40:00',
    'end_time' => '2019-06-11 13:55:00',
    'team_schema_id' => 7,
    'opponent_team_schema_id' => 8,
    'author_id' => 3,
    'tournament_type' => 'ladder',
    'tournament_group' => 'B',
    'tournament_id' => 1443,
);

$playoffSchema = array('type' => 'playoff');
//A1 B4
$playoffSchema['events']['round1-match1'] = array(
    'start' => '2019-06-11 14:00:00',
    'end_time' => '2019-06-11 14:15:00',
    'name' => 'A1:B4',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => 'Štvrťfinále',
    'tournament_round' => 1,
    'tournament_id' => 1443,
    'playoff_id' => 'round1-match1'
);
//A3 - B2
$playoffSchema['events']['round1-match2'] = array(
    'start' => '2019-06-11 14:20:00',
    'end_time' => '2019-06-11 14:35:00',
    'name' => 'A3:B2',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => 'Štvrťfinále',
    'tournament_round' => 1,
    'tournament_id' => 1443,
    'playoff_id' => 'round1-match2'
);

//A2 - B3
$playoffSchema['events']['round1-match3'] = array(
    'start' => '2019-06-11 15:00:00',
    'end_time' => '2019-06-11 15:15:00',
    'name' => 'A2:B3',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => 'Štvrťfinále',
    'tournament_round' => 1,
    'tournament_id' => 1443,
    'playoff_id' => 'round1-match3'
);
//A4-B1
$playoffSchema['events']['round1-match4'] = array(
    'start' => '2019-06-11 15:20:00',
    'end_time' => '2019-06-11 15:35:00',
    'name' => 'A4:B1',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => 'Štvrťfinále',
    'tournament_round' => 1,
    'tournament_id' => 1443,
    'playoff_id' => 'round1-match4'
);
//semifinal
$playoffSchema['events']['round2-match1'] = array(
    'start' => '2019-06-11 15:40:00',
    'end_time' => '2019-06-11 15:55:00',
    'name' => 'A1/B4:A3/B2',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => 'Semifinále',
    'tournament_round' => 2,
    'tournament_id' => 1443,
    'playoff_id' => 'round2-match1',
    'playoff_preview_team_id' => 'round1-match1',
    'playoff_parent_opponent_team_id' => 'round1-match2',
);

$playoffSchema['events']['round2-match2'] = array(
    'start' => '2019-06-11 16:00:00',
    'end_time' => '2019-06-11 16:15:00',
    'name' => 'A2/B3:A4/B1',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => 'Semifinále',
    'tournament_round' => 2,
    'tournament_id' => 1443,
    'playoff_id' => 'round2-match2',
    'playoff_preview_team_id' => 'round1-match3',
    'playoff_parent_opponent_team_id' => 'round1-match4',
);


$playoffSchema['events']['round3-match1'] = array(
    'start' => '2019-06-11 16:20:00',
    'end_time' => '2019-06-11 16:35:00',
    'name' => 'Víťaz semifinal 1:Víťaz semifinal 2',
    'author_id' => 3,
    'tournament_type' => 'playoff-final',
    'tournament_group' => 'Finále',
    'tournament_round' => 3,
    'tournament_id' => 1443,
    'playoff_id' => 'round3-match1',
    'playoff_preview_team_id' => 'round2-match1',
    'playoff_parent_opponent_team_id' => 'round2-match2',
);

$playoffSchema['events']['round3-match2'] = array(
    'start' => '2019-06-11 16:40:00',
    'end_time' => '2019-06-11 16:55:00',
    'name' => 'Porazený semifinal 1:Porazený semifinal 2',
    'author_id' => 3,
    'tournament_type' => 'playoff-bronze',
    'tournament_group' => 'Finále',
    'tournament_round' => 3,
    'tournament_id' => 1443,
    'playoff_id' => 'round3-match2',
    'playoff_preview_team_id' => 'round2-match1',
    'playoff_parent_opponent_team_id' => 'round2-match2',
);

/*
$playoffSchema['events'][] = array(
    'start' => '2019-06-11 14:10:00',
    'end_time' => '2019-06-11 14:25:00',
    'name' => 'Víťaz A1',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => 'A',
    'tournament_id' => 1443,
    'playoff_id' => 'round2-match1',
    'playoff_parent_team_id' => 'round1-match1',
);
$playoffSchema['events'][] = array(
    'start' => '2019-06-11 14:30:00',
    'end_time' => '2019-06-11 14:45:00',
    'name' => 'Víťaz A2',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => 'A',
    'tournament_id' => 1443,
    'playoff_id' => 'round2-match2',
    'playoff_parent_id' => 'round1-match2',
);
*/
