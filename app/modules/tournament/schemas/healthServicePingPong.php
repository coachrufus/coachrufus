<?php
$playersNames = array('Chalama Roman','Mária Miklošovičová','Jalovecký Martin','Pavelek Peter','Peter Ďurana','Ľubo Herda','Pavol Fajdek','Michal Adamec','Jozef Štalmašek','Kelemen Ladislav','Eduard Capek','Bíro Marek','Gonda Michal','Horváthová Ágnes','Hrica Erik','Kopček Juraj','Kosova Lenka','Miklovičová Dominika','Sedliačik Miroslav','Valent Tomáš','Hrčka Roman','Šimončič Martin','Bernát Ján','Dávidová Zuzana','Bajúsová Veronika','Meszárošová Eva','Uhláriková Alexandra','Simona Bucseková','Gaššay Anton','Kollárová Martina','Prištic Peter','Gutleber Zoltan','Kosťov Peter','Praženica Juraj','Mihálik Karol','Daniel Dubský','Klouček Jan','Juraj Harkabuzik','Vojtěch Němec','Medlen Martin');
$teams = array(0 => array(
        'name' => 'FAKE',
        'sport_id' => 109,
        'author_id' => 3,
        'players' => array(
           array(
               'first_name' => 'FAKE',
               'status' => 'confirmed',
               'team_role' => 'PLAYER'
           ),
        )
    ));
$index = 1;
foreach($playersNames as $playerName)
{
    $teams[$index++] = array(
        'name' => $playerName,
        'sport_id' => 109,
        'author_id' => 3,
        'players' => array(
           array(
               'first_name' => $playerName,
               'status' => 'confirmed',
               'team_role' => 'PLAYER'
           ),
        )
    );
}



$playoffSchema = array(
    'tournament_id' => 1446,
    'name' => 'Health Services - ping pong',
    'sport_id' => 109,
    'author_id' => 3,
    'termin' => '2019-06-11 08:00:00',
    'round' => 1,
    'type' => 'playoff');
$playoffSchema['teamsIds'] = array();

//predkolo
$playoffSchema['events']['round1-match1'] = array(
    'start' => '2019-06-11 08:00:00',
    'end_time' => '2019-06-11 08:10:00',
    'name' => '-:-',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => '1. predkolo',
    'tournament_round' => 1,
    'tournament_id' => 1446,
    'playoff_id' => 'round1-match1',
    'team_schema_id' => 0,
    'opponent_team_schema_id' => 0,
    'target_event_schema_id' => 'round2-match1'
);
$playoffSchema['events']['round1-match2'] = array(
    'start' => '2019-06-11 08:00:00',
    'end_time' => '2019-06-11 08:10:00',
    'name' => 'Chalama Roman:Kelemen Ladislav',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => '1. predkolo',
    'tournament_round' => 1,
    'tournament_id' => 1446,
    'playoff_id' => 'round1-match2',
    'team_schema_id' => 1,
    'opponent_team_schema_id' => 10,
    'target_event_schema_id' => 'round2-match1'
);

$playoffSchema['events']['round1-match3'] = array(
    'start' => '2019-06-11 08:00:00',
    'end_time' => '2019-06-11 08:00:00',
    'name' => '-:-',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => '1. predkolo',
    'tournament_round' => 1,
    'tournament_id' => 1446,
    'playoff_id' => 'round1-match3',
    'team_schema_id' => 0,
    'opponent_team_schema_id' => 0,
    'target_event_schema_id' => 'round2-match2'
);
$playoffSchema['events']['round1-match4'] = array(
    'start' => '2019-06-11 08:00:00',
    'end_time' => '2019-06-11 08:00:00',
    'name' => '-:-',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => '1. predkolo',
    'tournament_round' => 1,
    'tournament_id' => 1446,
    'playoff_id' => 'round1-match4',
    'team_schema_id' => 0,
    'opponent_team_schema_id' => 0,
    'target_event_schema_id' => 'round2-match2'
);
$playoffSchema['events']['round1-match5'] = array(
    'start' => '2019-06-11 08:00:00',
    'end_time' => '2019-06-11 08:00:00',
    'name' => '-:-',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => '1. predkolo',
    'tournament_round' => 1,
    'tournament_id' => 1446,
    'playoff_id' => 'round1-match5',
    'team_schema_id' => 0,
    'opponent_team_schema_id' => 0,
    'target_event_schema_id' => 'round2-match3'
);
$playoffSchema['events']['round1-match6'] = array(
    'start' => '2019-06-11 08:00:00',
    'end_time' => '2019-06-11 08:00:00',
    'name' => '-:-',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => '1. predkolo',
    'tournament_round' => 1,
    'tournament_id' => 1446,
    'playoff_id' => 'round1-match6',
    'team_schema_id' => 0,
    'opponent_team_schema_id' => 0,
    'target_event_schema_id' => 'round2-match3'
);
$playoffSchema['events']['round1-match7'] = array(
    'start' => '2019-06-11 08:12:00',
    'end_time' => '2019-06-11 08:22:00',
    'name' => 'Valent Tomáš:Bernát Ján',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => '1. predkolo',
    'tournament_round' => 1,
    'tournament_id' => 1446,
    'playoff_id' => 'round1-match7',
    'team_schema_id' => 20,
    'opponent_team_schema_id' => 23,
    'target_event_schema_id' => 'round2-match4'
);
$playoffSchema['events']['round1-match8'] = array(
    'start' => '2019-06-11 08:00:00',
    'end_time' => '2019-06-11 08:00:00',
    'name' => '-:-',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => '1. predkolo',
    'tournament_round' => 1,
    'tournament_id' => 1446,
    'playoff_id' => 'round1-match8',
    'team_schema_id' => 0,
    'opponent_team_schema_id' => 0,
    'target_event_schema_id' => 'round2-match4'
);
$playoffSchema['events']['round1-match9'] = array(
    'start' => '2019-06-11 08:00:00',
    'end_time' => '2019-06-11 08:00:00',
    'name' => '-:-',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => '1. predkolo',
    'tournament_round' => 1,
    'tournament_id' => 1446,
    'playoff_id' => 'round1-match9',
    'team_schema_id' => 0,
    'opponent_team_schema_id' => 0,
    'target_event_schema_id' => 'round2-match5'
);
$playoffSchema['events']['round1-match10'] = array(
    'start' => '2019-06-11 08:24:00',
    'end_time' => '2019-06-11 08:34:00',
    'name' => 'Gutleber Zoltan:Gaššay Anton',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => '1. predkolo',
    'tournament_round' => 1,
    'tournament_id' => 1446,
    'playoff_id' => 'round1-match10',
    'team_schema_id' => 32,
    'opponent_team_schema_id' => 29,
    'target_event_schema_id' => 'round2-match5'
);
$playoffSchema['events']['round1-match11'] = array(
    'start' => '2019-06-11 08:00:00',
    'end_time' => '2019-06-11 08:00:00',
    'name' => '-:-',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => '1. predkolo',
    'tournament_round' => 1,
    'tournament_id' => 1446,
    'playoff_id' => 'round1-match11',
    'team_schema_id' => 0,
    'opponent_team_schema_id' => 0,
    'target_event_schema_id' => 'round2-match6'
);
$playoffSchema['events']['round1-match12'] = array(
    'start' => '2019-06-11 08:00:00',
    'end_time' => '2019-06-11 08:00:00',
    'name' => '-:-',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => '1. predkolo',
    'tournament_round' => 1,
    'tournament_id' => 1446,
    'playoff_id' => 'round1-match12',
    'team_schema_id' => 0,
    'opponent_team_schema_id' => 0,
    'target_event_schema_id' => 'round2-match6'
);
$playoffSchema['events']['round1-match13'] = array(
    'start' => '2019-06-11 08:00:00',
    'end_time' => '2019-06-11 08:00:00',
    'name' => '-:-',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => '1. predkolo',
    'tournament_round' => 1,
    'tournament_id' => 1446,
    'playoff_id' => 'round1-match13',
    'team_schema_id' => 0,
    'opponent_team_schema_id' => 0,
    'target_event_schema_id' => 'round2-match7'
);
$playoffSchema['events']['round1-match14'] = array(
    'start' => '2019-06-11 08:00:00',
    'end_time' => '2019-06-11 08:00:00',
    'name' => '-:-',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => '1. predkolo',
    'tournament_round' => 1,
    'tournament_id' => 1446,
    'playoff_id' => 'round1-match14',
    'team_schema_id' => 0,
    'opponent_team_schema_id' => 0,
    'target_event_schema_id' => 'round2-match7'
);
$playoffSchema['events']['round1-match15'] = array(
    'start' => '2019-06-11 08:36:00',
    'end_time' => '2019-06-11 08:46:00',
    'name' => 'Medlen Martin:Mihálik Karol',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => '1. predkolo',
    'tournament_round' => 1,
    'tournament_id' => 1446,
    'playoff_id' => 'round1-match15',
    'team_schema_id' => 40,
    'opponent_team_schema_id' => 35,
    'target_event_schema_id' => 'round2-match8'
);
$playoffSchema['events']['round1-match16'] = array(
    'start' => '2019-06-11 08:00:00',
    'end_time' => '2019-06-11 08:00:00',
    'name' => '-:-',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => '1. predkolo',
    'tournament_round' => 1,
    'tournament_id' => 1446,
    'playoff_id' => 'round1-match16',
    'team_schema_id' => 0,
    'opponent_team_schema_id' => 0,
    'target_event_schema_id' => 'round2-match8'
);
$playoffSchema['events']['round1-match17'] = array(
    'start' => '2019-06-11 08:00:00',
    'end_time' => '2019-06-11 08:00:00',
    'name' => '-:-',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => '1. predkolo',
    'tournament_round' => 1,
    'tournament_id' => 1446,
    'playoff_id' => 'round1-match17',
    'team_schema_id' => 0,
    'opponent_team_schema_id' => 0,
    'target_event_schema_id' => 'round2-match9'
);
$playoffSchema['events']['round1-match18'] = array(
    'start' => '2019-06-11 08:48:00',
    'end_time' => '2019-06-11 08:58:00',
    'name' => 'Michal Adamec:Peter Ďurana',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => '1. predkolo',
    'tournament_round' => 1,
    'tournament_id' => 1446,
    'playoff_id' => 'round1-match18',
    'team_schema_id' => 8,
    'opponent_team_schema_id' => 5,
    'target_event_schema_id' => 'round2-match9'
);
$playoffSchema['events']['round1-match19'] = array(
    'start' => '2019-06-11 08:00:00',
    'end_time' => '2019-06-11 08:00:00',
    'name' => '-:-',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => '1. predkolo',
    'tournament_round' => 1,
    'tournament_id' => 1446,
    'playoff_id' => 'round1-match19',
    'team_schema_id' => 0,
    'opponent_team_schema_id' => 0,
    'target_event_schema_id' => 'round2-match10'
);
$playoffSchema['events']['round1-match20'] = array(
    'start' => '2019-06-11 08:00:00',
    'end_time' => '2019-06-11 08:00:00',
    'name' => '-:-',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => '1. predkolo',
    'tournament_round' => 1,
    'tournament_id' => 1446,
    'playoff_id' => 'round1-match20',
    'team_schema_id' => 0,
    'opponent_team_schema_id' => 0,
     'target_event_schema_id' => 'round2-match10'
);
$playoffSchema['events']['round1-match21'] = array(
    'start' => '2019-06-11 08:00:00',
    'end_time' => '2019-06-11 08:00:00',
    'name' => '-:-',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => '1. predkolo',
    'tournament_round' => 1,
    'tournament_id' => 1446,
    'playoff_id' => 'round1-match21',
    'team_schema_id' => 0,
    'opponent_team_schema_id' => 0,
    'target_event_schema_id' => 'round2-match11'
);
$playoffSchema['events']['round1-match22'] = array(
    'start' => '2019-06-11 08:00:00',
    'end_time' => '2019-06-11 08:00:00',
    'name' => '-:-',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => '1. predkolo',
    'tournament_round' => 1,
    'tournament_id' => 1446,
    'playoff_id' => 'round1-match22',
    'team_schema_id' => 0,
    'opponent_team_schema_id' => 0,
    'target_event_schema_id' => 'round2-match11'
);
$playoffSchema['events']['round1-match23'] = array(
    'start' => '2019-06-11 09:00:00',
    'end_time' => '2019-06-11 09:10:00',
    'name' => 'Gonda Michal:Hrica Erik ',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => '1. predkolo',
    'tournament_round' => 1,
    'tournament_id' => 1446,
    'playoff_id' => 'round1-match23',
    'team_schema_id' => 13,
    'opponent_team_schema_id' => 15,
    'target_event_schema_id' => 'round2-match12'
);
$playoffSchema['events']['round1-match24'] = array(
    'start' => '2019-06-11 08:00:00',
    'end_time' => '2019-06-11 08:00:00',
    'name' => '-:-',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => '1. predkolo',
    'tournament_round' => 1,
    'tournament_id' => 1446,
    'playoff_id' => 'round1-match24',
    'team_schema_id' => 0,
    'opponent_team_schema_id' => 0,
    'target_event_schema_id' => 'round2-match12'
);
$playoffSchema['events']['round1-match25'] = array(
    'start' => '2019-06-11 08:00:00',
    'end_time' => '2019-06-11 08:00:00',
    'name' => '-:-',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => '1. predkolo',
    'tournament_round' => 1,
    'tournament_id' => 1446,
    'playoff_id' => 'round1-match25',
    'team_schema_id' => 0,
    'opponent_team_schema_id' => 0,
    'target_event_schema_id' => 'round2-match13'
);
$playoffSchema['events']['round1-match26'] = array(
    'start' => '2019-06-11 09:12:00',
    'end_time' => '2019-06-11 09:22:00',
    'name' => 'Klouček Jan:Šimončič Martin',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => '1. predkolo',
    'tournament_round' => 1,
    'tournament_id' => 1446,
    'playoff_id' => 'round1-match26',
    'team_schema_id' => 37,
    'opponent_team_schema_id' => 22,
    'target_event_schema_id' => 'round2-match13'
);
$playoffSchema['events']['round1-match27'] = array(
    'start' => '2019-06-11 08:00:00',
    'end_time' => '2019-06-11 08:00:00',
    'name' => '-:-',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => '1. predkolo',
    'tournament_round' => 1,
    'tournament_id' => 1446,
    'playoff_id' => 'round1-match27',
    'team_schema_id' => 0,
    'opponent_team_schema_id' => 0,
    'target_event_schema_id' => 'round2-match14'
);
$playoffSchema['events']['round1-match28'] = array(
    'start' => '2019-06-11 08:00:00',
    'end_time' => '2019-06-11 08:00:00',
    'name' => '-:-',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => '1. predkolo',
    'tournament_round' => 1,
    'tournament_id' => 1446,
    'playoff_id' => 'round1-match28',
    'team_schema_id' => 0,
    'opponent_team_schema_id' => 0,
    'target_event_schema_id' => 'round2-match14'
);
$playoffSchema['events']['round1-match29'] = array(
    'start' => '2019-06-11 08:00:00',
    'end_time' => '2019-06-11 08:00:00',
    'name' => '-:-',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => '1. predkolo',
    'tournament_round' => 1,
    'tournament_id' => 1446,
    'playoff_id' => 'round1-match29',
    'team_schema_id' => 0,
    'opponent_team_schema_id' => 0,
    'target_event_schema_id' => 'round2-match15'
);
$playoffSchema['events']['round1-match30'] = array(
    'start' => '2019-06-11 08:00:00',
    'end_time' => '2019-06-11 08:00:00',
    'name' => '-:-',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => '1. predkolo',
    'tournament_round' => 1,
    'tournament_id' => 1446,
    'playoff_id' => 'round1-match30',
    'team_schema_id' => 0,
    'opponent_team_schema_id' => 0,
    'target_event_schema_id' => 'round2-match15'
);
$playoffSchema['events']['round1-match31'] = array(
    'start' => '2019-06-11 09:24:00',
    'end_time' => '2019-06-11 09:34:00',
    'name' => 'Sedliačik Miroslav:Jozef Štalmašek',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => '1. predkolo',
    'tournament_round' => 1,
    'tournament_id' => 1446,
    'playoff_id' => 'round1-match31',
    'team_schema_id' => 19,
    'opponent_team_schema_id' => 9,
    'target_event_schema_id' => 'round2-match16'
);
$playoffSchema['events']['round1-match32'] = array(
    'start' => '2019-06-11 08:00:00',
    'end_time' => '2019-06-11 08:00:00',
    'name' => '-:-',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => '1. predkolo',
    'tournament_round' => 1,
    'tournament_id' => 1446,
    'playoff_id' => 'round1-match32',
    'team_schema_id' => 0,
    'opponent_team_schema_id' => 0,
    'target_event_schema_id' => 'round2-match16'
);

//1 kolo
$playoffSchema['events']['round2-match1'] = array(
    'start' => '2019-06-11 09:36:00',
    'end_time' => '2019-06-11 09:46:00',
    'name' => 'Ľubo Herda:-',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => '1. kolo',
    'tournament_round' => 2,
    'tournament_id' => 1446,
    'playoff_id' => 'round2-match1',
    'team_schema_id' => 6,
    'playoff_parent_opponent_team_id' => 'round1-match2',
    'target_event_schema_id' => 'round3-match1'
);
$playoffSchema['events']['round2-match2'] = array(
    'start' => '2019-06-11 09:48:00',
    'end_time' => '2019-06-11 09:58:00',
    'name' => 'Jalovecký Martin:Kosťov Peter',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => '1. kolo',
    'tournament_round' => 2,
    'tournament_id' => 1446,
    'playoff_id' => 'round2-match2',
    'team_schema_id' => 3,
    'opponent_team_schema_id' => 33,
    'target_event_schema_id' => 'round3-match1'
);

$playoffSchema['events']['round2-match3'] = array(
    'start' => '2019-06-11 10:00:00',
    'end_time' => '2019-06-11 10:10:00',
    'name' => 'Mária Miklošovičová:Pavelek Peter',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => '1. kolo',
    'tournament_round' => 2,
    'tournament_id' => 1446,
    'playoff_id' => 'round2-match3',
    'team_schema_id' => 2,
    'opponent_team_schema_id' => 4,
    'target_event_schema_id' => 'round3-match2'
);

$playoffSchema['events']['round2-match4'] = array(
    'start' => '2019-06-11 10:12:00',
    'end_time' => '2019-06-11 10:22:00',
    'name' => '-:Daniel Dubský',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => '1. kolo',
    'tournament_round' => 2,
    'tournament_id' => 1446,
    'playoff_id' => 'round2-match4',
    'playoff_preview_team_id' => 'round1-match7',
    'opponent_team_schema_id' => 36,
    'target_event_schema_id' => 'round3-match2'
);

$playoffSchema['events']['round2-match5'] = array(
    'start' => '2019-06-11 10:24:00',
    'end_time' => '2019-06-11 10:34:00',
    'name' => 'Miklovičová Dominika:-',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => '1. kolo',
    'tournament_round' => 2,
    'tournament_id' => 1446,
    'playoff_id' => 'round2-match5',
    'team_schema_id' => 18,
    'playoff_parent_opponent_team_id' => 'round1-match10',
    'target_event_schema_id' => 'round3-match3'
);


$playoffSchema['events']['round2-match6'] = array(
    'start' => '2019-06-11 10:36:00',
    'end_time' => '2019-06-11 10:46:00',
    'name' => 'Vojtěch Němec:Simona Bucseková',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => '1. kolo',
    'tournament_round' => 2,
    'tournament_id' => 1446,
    'playoff_id' => 'round2-match6',
    'team_schema_id' => 39,
    'opponent_team_schema_id' => 28,
    'target_event_schema_id' => 'round3-match3'
);

$playoffSchema['events']['round2-match7'] = array(
    'start' => '2019-06-11 10:48:00',
    'end_time' => '2019-06-11 10:58:00',
    'name' => 'Juraj Harkabuzik:Bajúsová Veronika',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => '1. kolo',
    'tournament_round' => 2,
    'tournament_id' => 1446,
    'playoff_id' => 'round2-match7',
    'team_schema_id' => 38,
    'opponent_team_schema_id' => 25,
    'target_event_schema_id' => 'round3-match4'
);

$playoffSchema['events']['round2-match8'] = array(
    'start' => '2019-06-11 11:00:00',
    'end_time' => '2019-06-11 11:10:00',
    'name' => '-:Meszárošová Eva',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => '1. kolo',
    'tournament_round' => 2,
    'tournament_id' => 1446,
    'playoff_id' => 'round2-match8',
    'playoff_preview_team_id' => 'round1-match15',
    'opponent_team_schema_id' => 26,
    'target_event_schema_id' => 'round3-match4'
);

$playoffSchema['events']['round2-match9'] = array(
    'start' => '2019-06-11 11:12:00',
    'end_time' => '2019-06-11 11:22:00',
    'name' => 'Horváthová Ágnes:-',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => '1. kolo',
    'tournament_round' => 2,
    'tournament_id' => 1446,
    'playoff_id' => 'round2-match9',
    'team_schema_id' => 14,
    'playoff_parent_opponent_team_id' => 'round1-match18',
    'target_event_schema_id' => 'round3-match5'
);

$playoffSchema['events']['round2-match10'] = array(
    'start' => '2019-06-11 11:24:00',
    'end_time' => '2019-06-11 11:34:00',
    'name' => 'Kopček Juraj:Pavol Fajdek',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => '1. kolo',
    'tournament_round' => 2,
    'tournament_id' => 1446,
    'playoff_id' => 'round2-match10',
    'team_schema_id' => 16,
    'opponent_team_schema_id' => 7,
    'target_event_schema_id' => 'round3-match5'
);

$playoffSchema['events']['round2-match11'] = array(
    'start' => '2019-06-11 11:36:00',
    'end_time' => '2019-06-11 11:46:00',
    'name' => 'Kosova Lenka:Bíro Marek',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => '1. kolo',
    'tournament_round' => 2,
    'tournament_id' => 1446,
    'playoff_id' => 'round2-match11',
    'team_schema_id' => 17,
    'opponent_team_schema_id' => 12,
    'target_event_schema_id' => 'round3-match6'
);

$playoffSchema['events']['round2-match12'] = array(
    'start' => '2019-06-11 11:48:00',
    'end_time' => '2019-06-11 11:58:00',
    'name' => '-:Prištic Peter',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => '1. kolo',
    'tournament_round' => 2,
    'tournament_id' => 1446,
    'playoff_id' => 'round2-match12',
    'playoff_preview_team_id' => 'round1-match23',
    'opponent_team_schema_id' => 31,
    'target_event_schema_id' => 'round3-match6'
);

$playoffSchema['events']['round2-match13'] = array(
    'start' => '2019-06-11 10:00:00',
    'end_time' => '2019-06-11 12:10:00',
    'name' => 'Dávidová Zuzana:-',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => '1. kolo',
    'tournament_round' => 2,
    'tournament_id' => 1446,
    'playoff_id' => 'round2-match13',
    'team_schema_id' => 24,
    'playoff_parent_opponent_team_id' => 'round1-match26',
    'target_event_schema_id' => 'round3-match7'
);

$playoffSchema['events']['round2-match14'] = array(
    'start' => '2019-06-11 12:12:00',
    'end_time' => '2019-06-11 12:22:00',
    'name' => 'Praženica Juraj:Uhláriková Alexandra',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => '1. kolo',
    'tournament_round' => 2,
    'tournament_id' => 1446,
    'playoff_id' => 'round2-match14',
    'team_schema_id' => 34,
    'opponent_team_schema_id' => 27,
    'target_event_schema_id' => 'round3-match7'
);

$playoffSchema['events']['round2-match15'] = array(
    'start' => '2019-06-11 12:24:00',
    'end_time' => '2019-06-11 12:34:00',
    'name' => 'Kollárová Martina:Hrčka Roman',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => '1. kolo',
    'tournament_round' => 2,
    'tournament_id' => 1446,
    'playoff_id' => 'round2-match15',
    'team_schema_id' => 30,
    'opponent_team_schema_id' => 21,
    'target_event_schema_id' => 'round3-match8'
);

$playoffSchema['events']['round2-match16'] = array(
    'start' => '2019-06-11 12:36:00',
    'end_time' => '2019-06-11 12:46:00',
    'name' => '-:Eduard Capek',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => '1. kolo',
    'tournament_round' => 2,
    'tournament_id' => 1446,
    'playoff_id' => 'round2-match16',
    'playoff_preview_team_id' => 'round1-match31',
    'opponent_team_schema_id' => 31,
    'target_event_schema_id' => 'round3-match8'
);

//SEMIFINALE
$playoffSchema['events']['round3-match1'] = array(
    'start' => '2019-06-11 12:48:00',
    'end_time' => '2019-06-11 12:58:00',
    'name' => '-:-',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => 'Osemfinále',
    'tournament_round' => 3,
    'tournament_id' => 1446,
    'playoff_id' => 'round3-match1',
    'playoff_preview_team_id' => 'round2-match1',
    'playoff_parent_opponent_team_id' => 'round2-match2',
    'target_event_schema_id' => 'round4-match1'
);

$playoffSchema['events']['round3-match2'] = array(
    'start' => '2019-06-11 13:00:00',
    'end_time' => '2019-06-11 13:10:00',
    'name' => '-:-',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => 'Osemfinále',
    'tournament_round' => 3,
    'tournament_id' => 1446,
    'playoff_id' => 'round3-match2',
    'playoff_preview_team_id' => 'round2-match3',
    'playoff_parent_opponent_team_id' => 'round2-match4',
    'target_event_schema_id' => 'round4-match1'
);
$playoffSchema['events']['round3-match3'] = array(
    'start' => '2019-06-11 13:12:00',
    'end_time' => '2019-06-11 13:22:00',
    'name' => '-:-',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => 'Osemfinále',
    'tournament_round' => 3,
    'tournament_id' => 1446,
    'playoff_id' => 'round3-match3',
    'playoff_preview_team_id' => 'round2-match5',
    'playoff_parent_opponent_team_id' => 'round2-match6',
    'target_event_schema_id' => 'round4-match2'
);
$playoffSchema['events']['round3-match4'] = array(
    'start' => '2019-06-11 13:24:00',
    'end_time' => '2019-06-11 13:34:00',
    'name' => '-:-',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => 'Osemfinále',
    'tournament_round' => 3,
    'tournament_id' => 1446,
    'playoff_id' => 'round3-match4',
    'playoff_preview_team_id' => 'round2-match7',
    'playoff_parent_opponent_team_id' => 'round2-match8',
    'target_event_schema_id' => 'round4-match2'
);
$playoffSchema['events']['round3-match5'] = array(
    'start' => '2019-06-11 13:36:00',
    'end_time' => '2019-06-11 13:46:00',
    'name' => '-:-',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => 'Osemfinále',
    'tournament_round' => 3,
    'tournament_id' => 1446,
    'playoff_id' => 'round3-match5',
    'playoff_preview_team_id' => 'round2-match9',
    'playoff_parent_opponent_team_id' => 'round2-match10',
    'target_event_schema_id' => 'round4-match3'
);
$playoffSchema['events']['round3-match6'] = array(
    'start' => '2019-06-11 13:48:00',
    'end_time' => '2019-06-11 13:58:00',
    'name' => '-:-',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => 'Osemfinále',
    'tournament_round' => 3,
    'tournament_id' => 1446,
    'playoff_id' => 'round3-match6',
    'playoff_preview_team_id' => 'round2-match11',
    'playoff_parent_opponent_team_id' => 'round2-match12',
    'target_event_schema_id' => 'round4-match3'
);
$playoffSchema['events']['round3-match7'] = array(
    'start' => '2019-06-11 14:00:00',
    'end_time' => '2019-06-11 14:10:00',
    'name' => '-:-',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => 'Osemfinále',
    'tournament_round' => 3,
    'tournament_id' => 1446,
    'playoff_id' => 'round3-match7',
    'playoff_preview_team_id' => 'round2-match13',
    'playoff_parent_opponent_team_id' => 'round2-match14',
    'target_event_schema_id' => 'round4-match4'
);
$playoffSchema['events']['round3-match8'] = array(
    'start' => '2019-06-11 14:12:00',
    'end_time' => '2019-06-11 14:22:00',
    'name' => '-:-',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => 'Osemfinále',
    'tournament_round' => 3,
    'tournament_id' => 1446,
    'playoff_id' => 'round3-match8',
    'playoff_preview_team_id' => 'round2-match15',
    'playoff_parent_opponent_team_id' => 'round2-match16',
    'target_event_schema_id' => 'round4-match4'
);
//stvrtfinale
$playoffSchema['events']['round4-match1'] = array(
    'start' => '2019-06-11 14:24:00',
    'end_time' => '2019-06-11 14:34:00',
    'name' => '-:-',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => 'Štvrťfinále',
    'tournament_round' => 4,
    'tournament_id' => 1446,
    'playoff_id' => 'round4-match1',
    'playoff_preview_team_id' => 'round3-match1',
    'playoff_parent_opponent_team_id' => 'round3-match2',
    'target_event_schema_id' => 'round5-match1'
);
$playoffSchema['events']['round4-match2'] = array(
    'start' => '2019-06-11 14:36:00',
    'end_time' => '2019-06-11 14:46:00',
    'name' => '-:-',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => 'Štvrťfinále',
    'tournament_round' => 4,
    'tournament_id' => 1446,
    'playoff_id' => 'round4-match2',
    'playoff_preview_team_id' => 'round3-match3',
    'playoff_parent_opponent_team_id' => 'round3-match4',
     'target_event_schema_id' => 'round5-match1'
);
$playoffSchema['events']['round4-match3'] = array(
    'start' => '2019-06-11 14:48:00',
    'end_time' => '2019-06-11 14:58:00',
    'name' => '-:-',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => 'Štvrťfinále',
    'tournament_round' => 4,
    'tournament_id' => 1446,
    'playoff_id' => 'round4-match3',
    'playoff_preview_team_id' => 'round3-match5',
    'playoff_parent_opponent_team_id' => 'round3-match6',
     'target_event_schema_id' => 'round5-match2'
);
$playoffSchema['events']['round4-match4'] = array(
    'start' => '2019-06-11 15:00:00',
    'end_time' => '2019-06-11 15:10:00',
    'name' => '-:-',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => 'Štvrťfinále',
    'tournament_round' => 4,
    'tournament_id' => 1446,
    'playoff_id' => 'round4-match4',
    'playoff_preview_team_id' => 'round3-match7',
    'playoff_parent_opponent_team_id' => 'round3-match8',
     'target_event_schema_id' => 'round5-match2'
);
//semifinale
$playoffSchema['events']['round5-match1'] = array(
    'start' => '2019-06-11 15:12:00',
    'end_time' => '2019-06-11 15:22:00',
    'name' => '-:-',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => 'Semifinále',
    'tournament_round' => 5,
    'tournament_id' => 1446,
    'playoff_id' => 'round5-match1',
    'playoff_preview_team_id' => 'round4-match1',
    'playoff_parent_opponent_team_id' => 'round4-match2',
    'target_event_schema_id' => 'round6-match2'
);

$playoffSchema['events']['round5-match2'] = array(
    'start' => '2019-06-11 15:24:00',
    'end_time' => '2019-06-11 15:34:00',
    'name' => '-:-',
    'author_id' => 3,
    'tournament_type' => 'playoff',
    'tournament_group' => 'Semifinále',
    'tournament_round' => 5,
    'tournament_id' => 1446,
    'playoff_id' => 'round5-match2',
    'playoff_preview_team_id' => 'round4-match3',
    'playoff_parent_opponent_team_id' => 'round4-match4',
     'target_event_schema_id' => 'round6-match2'
);
//finale


$playoffSchema['events']['round6-match2'] = array(
    'start' => '2019-06-11 15:48:00',
    'end_time' => '2019-06-11 15:58:00',
    'name' => '-:-',
    'author_id' => 3,
    'tournament_type' => 'playoff-final',
    'tournament_group' => 'Finále',
    'tournament_round' => 6,
    'tournament_id' => 1446,
    'playoff_id' => 'round6-match1',
    'playoff_preview_team_id' => 'round5-match1',
    'playoff_parent_opponent_team_id' => 'round5-match2',
);


$playoffSchema['events']['round6-match1'] = array(
    'start' => '2019-06-11 15:36:00',
    'end_time' => '2019-06-11 15:46:00',
    'name' => '-:-',
    'author_id' => 3,
    'tournament_type' => 'playoff-bronze',
    'tournament_group' => 'Finále',
    'tournament_round' => 6,
    'tournament_id' => 1446,
    'playoff_id' => 'round6-match1',
    'playoff_preview_team_id' => 'round5-match1',
    'playoff_parent_opponent_team_id' => 'round5-match2',
);