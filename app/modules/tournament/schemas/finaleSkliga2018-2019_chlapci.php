<?php
 $schema = array( 
            'tournament_id' => 1442,
            'name' => 'Finále Dievčatá 2018-2019',
            'sport_id' => 42,
            'author_id' => 3,
            'termin' => '2019-06-11 09:00:00',
            'round' => 1,
            'type' => 'playoff');
        
        $schema['teamIds'] = array(2054,2055,2056,2057);
        
        $schema['events'] = array();
        $schema['events'][0]['start'] = '2019-06-11 09:00:00';
        $schema['events'][0]['end_time'] = '2019-06-11 09:30:00';
        $schema['events'][0]['name'] = 'ZŠ sv.D.Savia Zvolen : ZŠ Kopčany';
        $schema['events'][0]['author_id'] = 3;
        $schema['events'][0]['tournament_group'] = 'A';
        $schema['events'][0]['tournament_type'] = 'playoff';
        $schema['events'][0]['tournament_id'] = 1442;
        $schema['events'][0]['team_id'] = 2054;
        $schema['events'][0]['opponent_team_id'] = 2055;
        
        
        $schema['events'][] = array(
            'start' =>    '2019-06-11 10:10:00',
            'end_time' => '2019-06-11 10:40:00',
            'name' => 'ZŠ Komenského Snina : ZŠ Malinovského Partizánske',
            'team_id' => 2056,
            'opponent_team_id' => 2057,
            'author_id' => 3,
            'tournament_type' => 'ladder',
            'tournament_group' => 'A',
            'tournament_id' => 1442, 
        );
        $schema['events'][] = array(
            'start' =>    '2019-06-11 12:05:00',
            'end_time' => '2019-06-11 12:35:00',
            'name' => 'ZŠ Kopčany : ZŠ Malinovského Partizánske',
            'team_id' => 2055,
            'opponent_team_id' => 2057,
            'author_id' => 3,
            'tournament_type' => 'ladder',
            'tournament_group' => 'A',
            'tournament_id' => 1442, 
        );
        $schema['events'][] = array(
            'start' =>    '2019-06-11 13:15:00',
            'end_time' => '2019-06-11 13:45:00',
            'name' => 'ZŠ sv.D.Savia Zvolen : ZŠ Komenského Snina',
            'team_id' => 2054,
            'opponent_team_id' => 2056,
            'author_id' => 3,
            'tournament_type' => 'ladder',
            'tournament_group' => 'A',
            'tournament_id' => 1442, 
        );
        $schema['events'][] = array(
            'start' =>    '2019-06-11 14:25:00',
            'end_time' => '2019-06-11 14:55:00',
            'name' => 'ZŠ Komenského Snina : ZŠ Kopčany',
            'team_id' => 2056,
            'opponent_team_id' => 2055,
            'author_id' => 3,
            'tournament_type' => 'ladder',
            'tournament_group' => 'A',
            'tournament_id' => 1442, 
        );
        $schema['events'][] = array(
            'start' =>    '2019-06-11 15:35:00',
            'end_time' => '2019-06-11 16:05:00',
            'name' => 'ZŠ Malinovského Partizánske : ZŠ sv.D.Savia Zvolen',
            'team_id' => 2057,
            'opponent_team_id' => 2054,
            'author_id' => 3,
            'tournament_type' => 'ladder',
            'tournament_group' => 'A',
            'tournament_id' => 1442, 
        );
