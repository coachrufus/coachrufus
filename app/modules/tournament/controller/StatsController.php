<?php

namespace CR\Tournament\Controller;

use Core\Controller;
use Core\ServiceLayer;
use Core\Validator;
use DateTime;
use Webteamer\Locality\Form\PlaygroundForm;

class StatsController extends Controller {

    public function teamsOverviewAction()
    {
        $security = ServiceLayer::getService('security');
        $user = $security->getIdentity()->getUser();
        $repository = ServiceLayer::getService('TournamentRepository');
        $request = $this->getRequest();
        
        $tournament = $repository->findUserTournament($user,$this->getRequest()->get('id'));
        $permissionManager = ServiceLayer::getService('TournamentPermissionManager');
        $tournamentStats = $repository->getTournamentStats($this->getRequest()->get('id'),$request->get('filter'));

        
        $teams = $repository->findTournamentTeams($tournament->getId());

     
        //$filter
        $filterForm = new \CR\Tournament\Form\StatsFilterForm();
        $filterForm->bindData($request->get('filter'));
        $filterForm->buildDivisionChoices($request->get('filter'),$teams);

        
        return $this->render('CR\Tournament\TournamentModule:stats:overview.php', array(
           'tournament' => $tournament,
           'stats' => $tournamentStats,
           'permissionManager' => $permissionManager,
           'user' => $user,
           'filterForm' => $filterForm
        ));
    }
    
    public function playersOverviewAction()
    {
        $security = ServiceLayer::getService('security');
        $request = $this->getRequest();
        $user = $security->getIdentity()->getUser();
        $repository = ServiceLayer::getService('TournamentRepository');
        $tournament = $repository->findUserTournament($user,$this->getRequest()->get('id'));
        $tournamentStats = $repository->getTournamentPlayersStats(array('id' => $this->getRequest()->get('id'),'filter' => $request->get('filter')));
        
        $teams = $repository->findTournamentTeams($tournament->getId());
        $filterForm = new \CR\Tournament\Form\StatsFilterForm();
        $filterForm->bindData($request->get('filter'));
        $filterForm->buildDivisionChoices($request->get('filter'),$teams);
        

        return $this->render('CR\Tournament\TournamentModule:stats:playersOverview.php', array(
            'tournament' => $tournament,
            'stats' => $tournamentStats,
            'filterForm' => $filterForm
        ));
    }
    
   

}

?>