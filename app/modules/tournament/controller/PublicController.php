<?php

namespace CR\Tournament\Controller;

use Core\Controller;
use Core\ServiceLayer;
use Core\Validator;

class PublicController extends Controller {

    
    
    public function registerAction()
    {

        return $this->render('CR\Tournament\TournamentModule:public:register.php', array(
        ));
    }
    
    public function topAction()
    {
          $repository = ServiceLayer::getService('TournamentRepository');
        $translator = ServiceLayer::getService('translator');
        $tournament = $repository->findOneBy(array('seo_id' => $this->getRequest()->get('seoid')));

        
         $manager = ServiceLayer::getService('TournamentManager');
         $goalLadder = $manager->getTournamentPlayersLadder($tournament->getId());
         $assistLadder = $goalLadder;
         $pointsLadder = $goalLadder;
         

         
        usort($goalLadder,function($first,$second){
            return $first['goal'] < $second['goal'] ;
        });

        $topGoals = array_slice($goalLadder,0,3);
                
        usort($assistLadder,function($first,$second){
            return $first['assist'] < $second['assist'] ;
        });
        $topAssist = array_slice($assistLadder,0,3);
        
         usort($pointsLadder,function($first,$second){
            return $first['points'] < $second['points'] ;
        });
        $topPoints = array_slice($pointsLadder,0,3);
        
        
         

        
        //ServiceLayer::getService('layout')->setTemplate(GLOBAL_DIR . '/templates/PublicLayout.php');
         ServiceLayer::getService('layout')->setTemplate(null);
        ServiceLayer::getService('layout')->setTemplateParameters(array(
            'body-class' => 'full-width tournament-top',
            'title' => $tournament->getName(),
            'meta' => array(
                'keywords' => $tournament->getSportName().','. $translator->translate('tournament.public.detail.gender.'.$tournament->getGender()).','.$tournament->getAddressCity()
            )
            ));
        
        
        return $this->render('CR\Tournament\TournamentModule:public:top.php', array(
            'topGoals' => $topGoals,
            'topAssist' => $topAssist,
            'topPoints' => $topPoints,
            'tournament' => $tournament
        ));
    }
    
    public function detailReactAction()
    {  
        $repository = ServiceLayer::getService('TournamentRepository');
        $translator = ServiceLayer::getService('translator');
        $tournament = $repository->findOneBy(array('seo_id' => $this->getRequest()->get('seoid')));
        $teams = $repository->findTournamentTeams($tournament->getId(),array('status' => 'confirmed'));
        $teamManager = ServiceLayer::getService('TeamManager');
        
        $sportManager = ServiceLayer::getService('SportManager');
        $sportEnum = $sportManager->getSportFormChoices();
        $tournament->setSportName($sportEnum[$tournament->getSportId()]);
        
        $teamsInfo = $teamManager->getTeamsInfo($teams);
         $security = ServiceLayer::getService('security');
         $user = $security->getIdentity()->getUser();
         
        ServiceLayer::getService('layout')->setTemplate(GLOBAL_DIR . '/templates/WebviewWidgetLayout.php');
        
        
        if(null != $this->getRequest()->get('lineupId'))
        {
            $currentScreen = 'live-detail' ;
            $currentmatchId = $this->getRequest()->get('lineupId');
        }
        else
        {
             $currentScreen = 'home' ;
        }
       

        
        $activeTab = (null != $this->getRequest()->get('t')) ? $this->getRequest()->get('t') : 'detail';
         
        return $this->render('CR\Tournament\TournamentModule:public:detail_react.php', array(
            'tournament' => $tournament,
            'activeTab' => $activeTab,
            'teams' => $teams,
            'teamsInfo' => $teamsInfo,
            'user' => $user,
            'currentmatchId' => $currentmatchId,
            'currentScreen' => $currentScreen
        ));
    }

    public function detailAction()
    {  
        $repository = ServiceLayer::getService('TournamentRepository');
        $translator = ServiceLayer::getService('translator');
        $tournament = $repository->findOneBy(array('seo_id' => $this->getRequest()->get('seoid')));
        $teams = $repository->findTournamentTeams($tournament->getId(),array('status' => 'confirmed'));
        $teamManager = ServiceLayer::getService('TeamManager');
        
        $sportManager = ServiceLayer::getService('SportManager');
        $sportEnum = $sportManager->getSportFormChoices();
        $tournament->setSportName($sportEnum[$tournament->getSportId()]);
        
        $teamsInfo = $teamManager->getTeamsInfo($teams);
         $security = ServiceLayer::getService('security');
         $user = $security->getIdentity()->getUser();
         
        ServiceLayer::getService('layout')->setTemplate(GLOBAL_DIR . '/templates/PublicLayout.php');
        ServiceLayer::getService('layout')->setTemplateParameters(array(
            'body-class' => 'full-width',
            'title' => $tournament->getName(),
            'meta' => array(
                'keywords' => $tournament->getSportName().','. $translator->translate('tournament.public.detail.gender.'.$tournament->getGender()).','.$tournament->getAddressCity()
            )
            ));
        
        $activeTab = (null != $this->getRequest()->get('t')) ? $this->getRequest()->get('t') : 'detail';
         
        return $this->render('CR\Tournament\TournamentModule:public:detail.php', array(
            'tournament' => $tournament,
             'activeTab' => $activeTab,
            'teams' => $teams,
            'teamsInfo' => $teamsInfo,
            'user' => $user
        ));
    }

    public function scheduleAction()
    {
        $repository = ServiceLayer::getService('TournamentApiRepository'); 
        $tournament = $repository->findTournament($this->getRequest()->get('id'));
        $events = $repository->getTournamentEvents($tournament->getId());
        $schedules = $repository->getTournamentSchedules($tournament->getId());
        $teams = $repository->findTournamentTeams($tournament->getId());
        foreach ($events as $event)
        {
            $schedules[$event->getScheduleId()]->addEvent($event);
        }

        $list = array();
        $scheduleList = $schedules;
        foreach ($scheduleList as $schedule)
        {
            if (null != $schedule->getEvents())
            {
                $list[$schedule->getId()] = $schedule;
            }
        }


        krsort($list, true);

        ServiceLayer::getService('layout')->setTemplate(GLOBAL_DIR . '/templates/PublicLayout.php');
        ServiceLayer::getService('layout')->setTemplateParameters(array('body-class' => 'full-width'));


        return $this->render('CR\Tournament\TournamentModule:public:schedule.php', array(
                    'tournament' => $tournament,
                    'scheduleList' => $list,
        ));
    }

    public function sliderAction()
    {
        $request = $this->getRequest();
        ServiceLayer::getService('layout')->setTemplate(null);
       // $eventIdPlayground1 =  $this->getRequest()->get('eid');
        //$eventIdPlayground2 =  $this->getRequest()->get('eid2');

        $teamEventManager = ServiceLayer::getService('TeamEventManager');
        $event = $teamEventManager->findEventById($request->get('eid'));
        $event->setCurrentDate($event->getStart());
        $existLineup = $teamEventManager->getEventLineup($event);


        $event2 = $teamEventManager->findEventById($request->get('eid2'));
        $event2->setCurrentDate($event->getStart());
        $existLineup2 = $teamEventManager->getEventLineup($event2);
        $existLineup2 = $teamEventManager->getEventLineup($event2);

        return $this->render('CR\Tournament\TournamentModule:public:slider.php', array(
          'tid' => $this->getRequest()->get('tournament_id'),
          'event_id_1' =>$event->getId(),
          'event_id_2' =>$event2->getId(),
          'event_date' => $event->getStart()->format('Y-n-j'),
          'lid1' => $existLineup->getId(),
          'lid2' => $existLineup2->getId(),
        ));

        /*
        $response = new ControllerResponse();
        $response->setStatus('success');
        $response->setContent($data);
        $response->setType($type);
        return $response;
        */
        /*
        $html = $this->render('CR\Tournament\TournamentModule:public:slider.php', array(
            
        ))->getContent();
        
        return $html;
        */
    }

}

?>