<?php

namespace CR\Tournament\Controller;

use Core\Controller;
use Core\ServiceLayer;
use Core\Validator;


class ScheduleController extends Controller {

    public function indexAction()
    {
        $translator = $this->getTranslator();
        $security = ServiceLayer::getService('security');
        $request = $this->getRequest();
        $user = $security->getIdentity()->getUser();
        $repository = ServiceLayer::getService('TournamentRepository');
        $tournament = $repository->findUserTournament($user,$this->getRequest()->get('id'));

        $events = $repository->getTournamentEvents($tournament->getId(),$request->get('filter'));
        $schedules = $repository->getTournamentSchedules($tournament->getId());
        
        $teams = $repository->findTournamentTeams($tournament->getId());
        foreach($events as $event)
        {
          $schedules[$event->getScheduleId()]->addEvent($event); 
        }
       

        $permissionManager = ServiceLayer::getService('TournamentPermissionManager');
        if($permissionManager->hasTournamentPermission($user,$tournament))
        {
           $scheduleList = $schedules;
        }
        else
        {
            
            $userTeams = array();
            foreach($teams as $team)
            {
                if( in_array($user->getId(), $team->getAdminList()) )
                {
                    $userTeams[$team->getId()] = $team;
                }
            }
            $userSchedules = array();
            foreach($schedules as $schedule)
            {
                foreach($schedule->getEvents() as $event)
                {
                    if( array_key_exists($event->getTeamId(), $userTeams) or array_key_exists($event->getOpponentTeamId(), $userTeams) or $schedule->getScheduleType() == 'multiple_groups_playoff')
                    {
                        $userSchedules[$schedule->getId()] = $schedule;
                    }
                  
                }
            }
            $scheduleList = $userSchedules;
        }
        
        //krsort($scheduleList,true);
        

        $list = array();
        $filterFormScheduleChoices  = array('' => $translator->translate('tournament.stats.filter.group.all'));
        foreach($scheduleList as $schedule)
        {
            if(null != $schedule->getEvents())
            {
                $list[$schedule->getId()] = $schedule;
            }
            $filterFormScheduleChoices[$schedule->getId()] = $schedule->getName();
        }
        
        
        krsort($list,true);
        
        
        $filterForm = new \CR\Tournament\Form\StatsFilterForm();
        $filterForm->bindData($request->get('filter'));
        $filterForm->buildDivisionChoices($request->get('filter'),$teams);
        $filterForm->setFieldChoices('schedule_id', $filterFormScheduleChoices);

        
        $teamplate = 'index.php';
        /*
        if(1433 == $tournament->getId())
        {
             $teamplate ='index_playoff.php';
        }
        */

        return $this->render('CR\Tournament\TournamentModule:schedule:'.$teamplate, array(
            'tournament' => $tournament,
            'scheduleList' => $list,
            'permissionManager' => $permissionManager,
            'user' => $user,
            'filterForm' => $filterForm
        ));
    }
    

     public function createAction()
    {
        $security = ServiceLayer::getService('security');
        $user = $security->getIdentity()->getUser();
        $repository = ServiceLayer::getService('TournamentRepository');
        $tournament = $repository->findUserTournament($user,$this->getRequest()->get('id'));
        return $this->render('CR\Tournament\TournamentModule:schedule:create.php', array(
            'tournament' => $tournament,
        ));
    }
    
    public function createOneGroupAction()
    {
        $scheduleForm = new \CR\Tournament\Form\ScheduleForm();
        
        $security = ServiceLayer::getService('security');
        $user = $security->getIdentity()->getUser();
        $repository = ServiceLayer::getService('TournamentRepository');
        $tournament = $repository->findUserTournament($user,$this->getRequest()->get('id'));
        $teams = $repository->findTournamentTeams($tournament->getId());
       
        $request = $this->getRequest();
        if('POST' == $request->getMethod())
        {
            $postData = $request->get('schedule');
            $postData['tournament_type'] = 'one_group';

            $repository->createTournamentSchedule($tournament,$postData);
            
            $this->getRequest()->redirect($this->getRouter()->link('tournament_schedule',array('id' => $tournament->getId())));
        }

        return $this->render('CR\Tournament\TournamentModule:schedule:createOneGroup.php', array(
             'form' => $scheduleForm,
            'teams' => $teams,
             'tournament' => $tournament,
        ));
    }
    
    public function createMultiGroupAction()
    {
        $scheduleForm = new \CR\Tournament\Form\ScheduleForm();
        
        $security = ServiceLayer::getService('security');
        $user = $security->getIdentity()->getUser();
        $repository = ServiceLayer::getService('TournamentRepository');
        $tournament = $repository->findUserTournament($user,$this->getRequest()->get('id'));
        $teams = $repository->findTournamentTeams($tournament->getId());
       
        $request = $this->getRequest();
        if('POST' == $request->getMethod())
        {
            $postData = $request->get('schedule');
            $postData['groups'] = $request->get('groups');
            $postData['tournament_type'] = 'multiple_groups';
            $repository->createTournamentSchedule($tournament,$postData);
            $this->getRequest()->redirect($this->getRouter()->link('tournament_schedule',array('id' => $tournament->getId())));
        }

        return $this->render('CR\Tournament\TournamentModule:schedule:createMultiGroup.php', array(
             'form' => $scheduleForm,
            'teams' => $teams,
             'tournament' => $tournament,
        ));
    }
    
    public function createConferenceAction()
    {
        $scheduleForm = new \CR\Tournament\Form\ScheduleForm();
        
        $security = ServiceLayer::getService('security');
        $user = $security->getIdentity()->getUser();
        $repository = ServiceLayer::getService('TournamentRepository');
        $tournament = $repository->findUserTournament($user,$this->getRequest()->get('id'));
        $teams = $repository->findTournamentTeams($tournament->getId());
       
        $request = $this->getRequest();
        if('POST' == $request->getMethod())
        {
            $postData = $request->get('schedule');
            $postData['groups'] = $request->get('groups');
            $postData['tournament_type'] = 'conference';
            $repository->createTournamentSchedule($tournament,$postData);
             $this->getRequest()->redirect($this->getRouter()->link('tournament_schedule',array('id' => $tournament->getId())));
        }

        return $this->render('CR\Tournament\TournamentModule:schedule:createConference.php', array(
             'form' => $scheduleForm,
            'teams' => $teams,
             'tournament' => $tournament,
        ));
    }
    
     public function createPlayoffAction()
    {
        $scheduleForm = new \CR\Tournament\Form\ScheduleForm();
        
        $security = ServiceLayer::getService('security');
        $user = $security->getIdentity()->getUser();
        $repository = ServiceLayer::getService('TournamentRepository');
        $tournament = $repository->findUserTournament($user,$this->getRequest()->get('id'));
        $teams = $repository->findTournamentTeams($tournament->getId());
        
        //$boxIterations = floor(count($teams)/2);
       
        $request = $this->getRequest();
        if('POST' == $request->getMethod())
        {
           $postData = $request->get('schedule');
            $postData['groups'] = $request->get('groups');
            
            foreach($postData['groups'] as $key => $group)
            {
                $postData['groups'][$key]['name'] = 'Group '.($key+1);
            }
            $postData['tournament_type'] = 'multiple_groups_playoff';
            $repository->createTournamentSchedule($tournament,$postData);
            $this->getRequest()->redirect($this->getRouter()->link('tournament_schedule',array('id' => $tournament->getId())));
        }

        return $this->render('CR\Tournament\TournamentModule:schedule:createPlayoff.php', array(
             'form' => $scheduleForm,
            'teams' => $teams,
             'tournament' => $tournament,
        ));
    }
    
    public function deleteAction()
    {
         $request = $this->getRequest();
        $security = ServiceLayer::getService('security');
        $repository = ServiceLayer::getService('TournamentRepository');

        $user = $security->getIdentity()->getUser(array('user_id'));
        $data['schedule_id'] = $request->get('sid');
        $data['user_id'] = $user->getId();
        $data['tournament_id'] = $request->get('id');
        $repository->deleteTournamentSchedule($data);
        $this->getRequest()->redirect($this->getRouter()->link('tournament_schedule',array('id' => $request->get('id'))));
        
       
    }
    
   

}

?>