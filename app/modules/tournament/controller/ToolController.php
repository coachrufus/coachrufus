<?php

namespace CR\Tournament\Controller;

use Core\Controller;
use Core\ServiceLayer;
use Core\Validator;
use DateTime;
use Webteamer\Locality\Form\PlaygroundForm;
use CR\Tournament\Model\Team;

class ToolController extends Controller {

    private function createTeams($teams)
    {
        $teamRepo = ServiceLayer::getService('TeamRepository');
        $teamPlayerRepo = ServiceLayer::getService('TeamPlayerRepository');

        foreach ($teams as $key => $teamData)
        {
            $team = new \Webteamer\Team\Model\Team();
            $team->setName($teamData['name']);
            $team->setSportId($teamData['sport_id']);
            $team->setAuthorId($teamData['author_id']);
            $teamId = $teamRepo->save($team);

            $teams[$key]['id'] = $teamId;

            //create players
            foreach ($teamData['players'] as $teamPlayerData)
            {
                $teamPlayer = new \Webteamer\Team\Model\TeamPlayer();
                $teamPlayer->setFirstName($teamPlayerData['first_name']);
                $teamPlayer->setLastName($teamPlayerData['last_name']);
                $teamPlayer->setStatus($teamPlayerData['status']);
                $teamPlayer->setTeamRole($teamPlayerData['team_role']);
                $teamPlayer->setPlayerId(0);
                $teamPlayer->setTeamId($teamId);
                $teamPlayerRepo->save($teamPlayer);
            }
        }

        return $teams;
    }

    private function createEvents($schema)
    {
        $tournamentRepo = ServiceLayer::getService('TournamentRepository');

        $security = ServiceLayer::getService('security');
        $user = $security->getIdentity()->getUser();

        //teams
        $teamRepo = ServiceLayer::getService('TeamRepository');
        $teamManager = ServiceLayer::getService('TeamManager');
        $teams = array();


        //season
        $seasonRepo = ServiceLayer::getService('TeamSeasonRepository');


        //event
        $eventRepo = ServiceLayer::getService('EventRepository');
        foreach ($schema['events'] as $eventKey => $eventData)
        {
            $eventStart = new \DateTime($eventData['start']);
            $seasonEnd = clone $eventStart;
            $seasonEnd->modify('+1 year');
            $season = new \CR\TeamSeason\Model\TeamSeason();
            $season->setName($schema['name']);
            $season->setTeamId($eventData['team_id']);
            $season->setStartDate($eventStart);
            $season->setEndDate($seasonEnd);
            $seasonId = $seasonRepo->save($season);


            $event = new \Webteamer\Event\Model\Event();

            if (array_key_exists('name', $eventData))
            {
                $event->setName($eventData['name']);
            }
            else
            {
                $event->setName($schema['teams'][$eventData['team_schema_id']]['name'] . ':' . $schema['teams'][$eventData['opponent_team_schema_id']]['name']);
            }


            $event->setSeason($seasonId);
            $event->setEventType('tournament_game');
            $event->setPeriod('none');
            $event->setStart($eventStart);

            if (array_key_exists('team_id', $eventData))
            {
                $event->setTeamId($eventData['team_id']);
            }

            if (array_key_exists('opponent_team_id', $eventData))
            {
                $event->setOpponentTeamId($eventData['opponent_team_id']);
            }

            if (array_key_exists('team_schema_id', $eventData))
            {
                $event->setTeamId($schema['teams'][$eventData['team_schema_id']]['id']);
            }
            if (array_key_exists('opponent_team_schema_id', $eventData))
            {
                $event->setOpponentTeamId($schema['teams'][$eventData['opponent_team_schema_id']]['id']);
            }



            $event->setAuthorId($eventData['author_id']);
            $event->setTournamentType($eventData['tournament_type']);
            $event->setTournamentRound($eventData['tournament_round']);
            $event->setTournamentGroup($eventData['tournament_group']);
            $event->setTournamentId($eventData['tournament_id']);
            $event->setPlaygroundId($eventData['playground_id']);


            if ('playoff' == $eventData['tournament_type'] or 'playoff-bronze' == $eventData['tournament_type'])
            {
                if (array_key_exists('playoff_preview_team_id', $eventData))
                {
                    $event->setTournamentPlayoffParentEvent1($schema['events'][$eventData['playoff_preview_team_id']]['id']);
                }
                if (array_key_exists('playoff_parent_opponent_team_id', $eventData))
                {
                    $event->setTournamentPlayoffParentEvent2($schema['events'][$eventData['playoff_parent_opponent_team_id']]['id']);
                }
            }


            $eventId = $eventRepo->save($event);
            $schema['events'][$eventKey]['id'] = $eventId;

            //create lineup

            $lineupRepo = ServiceLayer::getService('TeamMatchLineupRepository');
            $lineupNames = explode(':', $event->getName());


            $lineupEventDate = new \DateTime($eventStart->format('Y-m-d 00:00:00'));
            $lineup = new \Webteamer\Team\Model\TeamMatchLineup();
            $lineup->setEventId($eventId);
            $lineup->setEventDate($lineupEventDate);
            $lineup->setSportId($schema['sportId']);
            $lineup->setTeamId($event->getTeamId());
            $lineup->setOpponentTeamId($event->getOpponentTeamId());
            $lineup->setCreatedAt(new \DateTime());
            $lineup->setFirstLineName($lineupNames[0]);
            $lineup->setSecondLineName($lineupNames[1]);
            $lineupId = $lineupRepo->save($lineup);

            /*
              $teamPlayers = $teamManager->getActiveTeamPlayers($event->getTeamId());
              foreach ($teamPlayers as $teamPlayer)
              {
              $lineupPlayer = new \Webteamer\Team\Model\TeamMatchLineupPlayer();
              $lineupPlayer->setEventDate($lineupEventDate);
              $lineupPlayer->setLineupId($lineupId);
              $lineupPlayer->setCreatedAt(new \DateTime());
              $lineupPlayer->setPlayerId($teamPlayer->getPlayerId());
              $lineupPlayer->setPlayerName($teamPlayer->getFullName());
              $lineupPlayer->setEventId($eventId);
              $lineupPlayer->setLineupPosition('first_line');
              $lineupPlayer->setLineupName($lineupNames[0]);
              $lineupPlayer->setTeamPlayerId($teamPlayer->getId());
              $lineupPlayer->setPlayerPosition('PLAYER');
              $lineupRepo->savePlayer($lineupPlayer);
              }

              $opponentTeamPlayers = $teamManager->getActiveTeamPlayers($event->getOpponentTeamId());
              foreach ($opponentTeamPlayers as $teamPlayer)
              {
              $lineupPlayer = new \Webteamer\Team\Model\TeamMatchLineupPlayer();
              $lineupPlayer->setEventDate($lineupEventDate);
              $lineupPlayer->setLineupId($lineupId);
              $lineupPlayer->setCreatedAt(new \DateTime());
              $lineupPlayer->setPlayerId($teamPlayer->getPlayerId());
              $lineupPlayer->setPlayerName($teamPlayer->getFullName());
              $lineupPlayer->setEventId($eventId);
              $lineupPlayer->setLineupPosition('second_line');
              $lineupPlayer->setLineupName($lineupNames[1]);
              $lineupPlayer->setTeamPlayerId($teamPlayer->getId());
              $lineupPlayer->setPlayerPosition('PLAYER');
              $lineupRepo->savePlayer($lineupPlayer);
              }
             * 
             */
        }

        foreach ($schema['events'] as $eventKey => $eventData)
        {
            //tareget event
            if (array_key_exists('target_event_schema_id', $eventData))
            {
                $event = $eventRepo->find($eventData['id']);
                $event->setTournamentTargetEvent($schema['events'][$eventData['target_event_schema_id']]['id']);
                $eventRepo->save($event);
            }
        }
    }

    public function generateOneGroupTournamentSchedule($data)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "https://www.rozpisyzapasu.cz/api/schedules");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);

        curl_setopt($ch, CURLOPT_POST, TRUE);

        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        $apiData = array();
        //$startDate = \DateTime::createFromFormat('d.m.Y H:i', $tournamentData['start_date']);

        $apiData['scheduleType'] = $data['type'];


        $apiData['teams'] = array_values($data['groups'][0]['team_names']);

        $apiData['periodsCount'] = (int) $data['periods_count'];
        $apiData['fields']['count'] = (int) $data['fields_count'];
        $apiData['fields']['areFieldsParallel'] = ($data['fields_paralel'] == 1) ? true : false;
        $apiData['duration']['dateStart'] = $data['date_start'];
        $apiData['duration']['match']['durationInMinutes'] = (int) $data['match_duration'];
        $apiData['duration']['round']['durationInMinutes'] = (int) $data['round_duration'];
        $apiData['referees']['refereeType'] = '';
        $apiData['table']['scheduleType'] = $data['table_type'];



        $encodedData = json_encode($apiData, JSON_UNESCAPED_UNICODE);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $encodedData);


        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type: application/json"
        ));

        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }

    public function import2019()
    {


        $data = file(APPLICATION_PATH . '/skliga2020/skupiny.csv');

        //SELECT * FROM `team_match_lineup` where event_id in (select id from team_event  where tournament_id in (1440,1441))
        //SELECT * FROM `team_event` where tournament_id in (1440,1441)/
        //delete FROM `tournament_team`  where tournament_id in (1440,1441)

        $teamRepo = ServiceLayer::getService('TeamRepository');
        $userRepo = ServiceLayer::getService('user_repository');
        $tournamentRepo = ServiceLayer::getService('TournamentRepository');
        $schedules = array();
        $teams = array();
        foreach ($data as $d)
        {
            $row = explode(';', $d);
            //var_dump($row);

            $boyTeamId = trim($row[8]);
            $girlTeamId = trim($row[18]);
            $group = trim($row[9]);

            $schedule = array();

            //ak neexistuje tim, vytorim ho podla dievcenskeho
            /*
              if(trim($boyTeamId) == 'nemáme')
              {
              $oppositeTeam  = $teamRepo->find($girlTeamId);


              //najprv skusim najst chlapcensky tim podla autora
              $existTeam = $teamRepo->findOneBy(array('author_id' => $oppositeTeam->getAuthorId(),'gender' => 'man'));
              if(null == $existTeam)
              {
              $team = $teamRepo->find($girlTeamId);
              $team->setName($team->getName().'-COPY');
              $team->setId(null);
              $team->setGender('man');
              $boyTeamId = $teamRepo->save($team);
              }
              else
              {
              $boyTeamId = $existTeam->getId();
              }
              }

              //ak neexistuje tim, vytorim ho podla chlapcenskeho
              if(trim($girlTeamId) == 'nemame')
              {
              $oppositeTeam  = $teamRepo->find($boyTeamId);

              //najprv skusim najst dievcenky tim podla autora
              $existTeam = $teamRepo->findOneBy(array('author_id' => $oppositeTeam->getAuthorId(),'gender' => 'woman'));
              if(null == $existTeam)
              {
              $team = $teamRepo->find($boyTeamId);
              $team->setId(null);
              $team->setName($team->getName().'-COPY');
              $team->setGender('woman');
              $girlTeamId = $teamRepo->save($team);
              }
              else
              {
              $girlTeamId = $existTeam->getId();
              }

              }
             */

            $boyTeam = $teamRepo->find($boyTeamId);
            $girlTeam = $teamRepo->find($girlTeamId);
            $boyUser = $userRepo->find($boyTeam->getAuthorId());
            $girlUser = $userRepo->find($girlTeam->getAuthorId());
            $tournamentRepo->createTournamentTeam(1441, $boyTeamId, $boyUser);
            $tournamentRepo->createTournamentTeam(1440, $girlTeamId, $girlUser);

            $teams['boys'][] = $boyTeamId;
            $teams['girls'][] = $girlTeamId;

            $schedules['boys'][$group]['teams'][] = $boyTeamId;
            $schedules['girls'][$group]['teams'][] = $girlTeamId;
        }

        //var_dump($schedules);
        $this->createTournamentEvents(1441, $schedules['boys']);
        $this->createTournamentEvents(1440, $schedules['girls']);
    }

    public function createTournamentEvents($tournamentId, $schedules)
    {
        $teamRepo = ServiceLayer::getService('TeamRepository');
        foreach ($schedules as $name => $schedule)
        {
            $scheduleEvents = array();
            $teams = $schedule['teams'];
            $iterations = count($teams);
            $currentTeam = array_shift($teams);
            for ($i = 0; $i < $iterations; $i++)
            {
                //var_dump($teams);
                foreach ($teams as $teamId)
                {
                    $scheduleEvents[] = array($currentTeam, $teamId);
                }
                $currentTeam = array_shift($teams);
            }
            $schedules[$name]['events'] = $scheduleEvents;
        }

        //season
        $seasonRepo = ServiceLayer::getService('TeamSeasonRepository');
        $eventRepo = ServiceLayer::getService('EventRepository');

        foreach ($schedules as $name => $schedule)
        {
            foreach ($schedule['events'] as $eventData)
            {
                $homeTeam = $teamRepo->find($eventData[0]);
                $oppositeTeam = $teamRepo->find($eventData[1]);


                $eventStart = new \DateTime('2019-11-06 09:00:00');
                $seasonEnd = clone $eventStart;
                $seasonEnd->modify('+1 year');
                $season = new \CR\TeamSeason\Model\TeamSeason();
                $season->setName('SK LIGA 19/20');
                $season->setTeamId($homeTeam->getId());
                $season->setStartDate($eventStart);
                $season->setEndDate($seasonEnd);
                $seasonId = $seasonRepo->save($season);

                //1 round
                for ($i = 1; $i < 3; $i++)
                {
                    $event = new \Webteamer\Event\Model\Event();
                    $event->setName($homeTeam->getName() . ':' . $oppositeTeam->getName());
                    $event->setTournamentRound($i);
                    $event->setSeason($seasonId);
                    $event->setEventType('tournament_game');
                    $event->setPeriod('none');

                    if ($i == 1)
                    {
                        $event->setStart(new DateTime('2019-11-06 09:00:00'));
                    }
                    else
                    {
                        $event->setStart(new DateTime('2019-11-13 09:00:00'));
                    }

                    $event->setTeamId($homeTeam->getId());
                    $event->setOpponentTeamId($oppositeTeam->getId());
                    $event->setAuthorId($homeTeam->getAuthorId());
                    $event->setTournamentType('ladder');
                    $event->setTournamentGroup($name);
                    $event->setTournamentId($tournamentId);
                    $eventId = $eventRepo->save($event);

                    //create lineup
                    $lineupRepo = ServiceLayer::getService('TeamMatchLineupRepository');

                    $lineupEventDate = new \DateTime($event->getStart()->format('Y-m-d 00:00:00'));



                    $lineup = new \Webteamer\Team\Model\TeamMatchLineup();
                    $lineup->setEventId($eventId);
                    $lineup->setEventDate($lineupEventDate);
                    $lineup->setSportId(42);
                    $lineup->setTeamId($homeTeam->getId());
                    $lineup->setOpponentTeamId($oppositeTeam->getId());
                    $lineup->setCreatedAt(new \DateTime());
                    $lineup->setFirstLineName($homeTeam->getName());
                    $lineup->setSecondLineName($oppositeTeam->getName());
                    $lineupId = $lineupRepo->save($lineup);
                }
            }
        }
    }

    private function generateLineups($tournamentId)
    {
        //1440 1441
        $lineupRepo = ServiceLayer::getService('TeamMatchLineupRepository');
        $eventRepo = ServiceLayer::getService('EventRepository');
        $teamManager = ServiceLayer::getService('TeamManager');
        $events = $eventRepo->findBy(array('tournament_id' => $tournamentId)); //38137
        foreach ($events as $event)
        {
            $lineup = $lineupRepo->findOneBy(array('event_id' => $event->getId()));

            $teamPlayers = $teamManager->getActiveTeamPlayers($event->getTeamId());
            foreach ($teamPlayers as $teamPlayer)
            {
                if ($teamPlayer->getFirstName() == 'RUFUS 2019' or true)
                {
                    $lineupPlayer = new \Webteamer\Team\Model\TeamMatchLineupPlayer();
                    $lineupPlayer->setEventDate($lineup->getEventDate());
                    $lineupPlayer->setLineupId($lineup->getId());
                    $lineupPlayer->setCreatedAt(new \DateTime());
                    $lineupPlayer->setPlayerId(0);
                    $lineupPlayer->setPlayerName($teamPlayer->getFirstName());
                    $lineupPlayer->setEventId($event->getId());
                    $lineupPlayer->setLineupPosition('first_line');
                    $lineupPlayer->setLineupName($lineup->getFirstLineName());
                    $lineupPlayer->setTeamPlayerId($teamPlayer->getId());
                    $lineupPlayer->setPlayerPosition('PLAYER');
                    $lineupRepo->savePlayer($lineupPlayer);
                }
            }

            $opponentTeamPlayers = $teamManager->getActiveTeamPlayers($event->getOpponentTeamId());
            foreach ($opponentTeamPlayers as $teamPlayer)
            {
                if ($teamPlayer->getFirstName() == 'RUFUS 2019' or true)
                {
                    $lineupPlayer = new \Webteamer\Team\Model\TeamMatchLineupPlayer();
                    $lineupPlayer->setEventDate($lineup->getEventDate());
                    $lineupPlayer->setLineupId($lineup->getId());
                    $lineupPlayer->setCreatedAt(new \DateTime());
                    $lineupPlayer->setPlayerId(0);
                    $lineupPlayer->setPlayerName($teamPlayer->getFirstName());
                    $lineupPlayer->setEventId($event->getId());
                    $lineupPlayer->setLineupPosition('second_line');
                    $lineupPlayer->setLineupName($lineup->getSecondLineName());
                    $lineupPlayer->setTeamPlayerId($teamPlayer->getId());
                    $lineupPlayer->setPlayerPosition('PLAYER');
                    $lineupRepo->savePlayer($lineupPlayer);
                }
            }

            /*
              $eventStart = new \DateTime($eventData['start']);
              $lineupEventDate = new \DateTime($eventStart->format('Y-m-d 00:00:00'));

              $lineupPlayer = new \Webteamer\Team\Model\TeamMatchLineupPlayer();
              $lineupPlayer->setEventDate($lineup->getEventDate());
              $lineupPlayer->setLineupId($lineup->getId());
              $lineupPlayer->setCreatedAt(new \DateTime());
              $lineupPlayer->setPlayerId(0);
              $lineupPlayer->setPlayerName();
              $lineupPlayer->setEventId($eventId);
              $lineupPlayer->setLineupPosition('first_line');
              $lineupPlayer->setLineupName($lineupNames[0]);
              $lineupPlayer->setTeamPlayerId($teamPlayer->getId());
              $lineupPlayer->setPlayerPosition('PLAYER');
              $lineupRepo->savePlayer($lineupPlayer);


              $opponentTeamPlayers = $teamManager->getActiveTeamPlayers($eventData['opponent_team_id']);
              foreach ($opponentTeamPlayers as $teamPlayer)
              {
              $lineupPlayer = new \Webteamer\Team\Model\TeamMatchLineupPlayer();
              $lineupPlayer->setEventDate($lineupEventDate);
              $lineupPlayer->setLineupId($lineupId);
              $lineupPlayer->setCreatedAt(new \DateTime());
              $lineupPlayer->setPlayerId($teamPlayer->getPlayerId());
              $lineupPlayer->setPlayerName($teamPlayer->getFullName());
              $lineupPlayer->setEventId($eventId);
              $lineupPlayer->setLineupPosition('second_line');
              $lineupPlayer->setLineupName($lineupNames[1]);
              $lineupPlayer->setTeamPlayerId($teamPlayer->getId());
              $lineupPlayer->setPlayerPosition('PLAYER');
              $lineupRepo->savePlayer($lineupPlayer);
              }
             * 
             */
        }
    }

    private function results2019()
    {
        $lineupRepo = ServiceLayer::getService('TeamMatchLineupRepository');
        $lineupPlayerRepo = $lineupRepo->getPlayerRepository();
        $teamManager = ServiceLayer::getService('TeamManager');
        $eventRepo = ServiceLayer::getService('EventRepository');


        //$eventRepo = ServiceLayer::getService('EventRepository');
        $data = file(APPLICATION_PATH . '/skliga2020/SK_LIGA_dievcata.csv');
        foreach ($data as $d)
        {
            $row = explode(';', $d);
            //var_dump($row);

            $eventId = $row[0];
            $homePoints = trim($row[4]);
            $opponentPoints = trim($row[5]);
            $overtime = trim($row[6]);

            //find lineup
            if ($eventId > 0)
            {
                $lineup = $lineupRepo->findOneBy(array('event_id' => $eventId));
                $event = $eventRepo->find($eventId);
                $lineupPlayer = $lineupPlayerRepo->findOneBy(array('lineup_id' => $lineup->getId(), 'player_name' => 'RUFUS 2019'));
                //delete timeline events
                if ($homePoints > 0 or $opponentPoints > 0)
                {
                     \Core\DbQuery::query('DELETE from player_timeline_event where lineup_id = '.$lineup->getId());
                }

                if (null != $lineupPlayer)
                {
                    for ($i = 0; $i < $homePoints; $i++)
                    {
                        $scoreData = [];
                        $scoreData['hit_type'] = 'goal';
                        $scoreData['hit_time'] = '00:00:00';
                        $scoreData['lineup_id'] = $lineup->getId();
                        $scoreData['event_id'] = $eventId;
                        $scoreData['event_date'] = $event->getStart()->format('Y-m-d') . ' 00:00:00';
                        $scoreData['lineup_position'] = 'first_line';
                        $scoreData['lineup_player_id'] = $lineupPlayer->getId();
                        $scoreData['team_id'] = $lineup->getTeamId();
                        $scoreData['hit_group'] = 'CR2019' . $event->getStart()->format('Y-m-d') . '#' . md5($lineupPlayer->getId() . rand(1, 1000) . time());

                        \Core\DbQuery::insertFromArray($scoreData, 'player_timeline_event');
                    }

                    for ($i = 0; $i < $opponentPoints; $i++)
                    {
                        $scoreData = [];
                        $scoreData['hit_type'] = 'goal';
                        $scoreData['hit_time'] = '00:00:00';
                        $scoreData['lineup_id'] = $lineup->getId();
                        $scoreData['event_id'] = $eventId;
                        $scoreData['event_date'] = $event->getStart()->format('Y-m-d') . ' 00:00:00';
                        $scoreData['lineup_position'] = 'second_line';
                        $scoreData['lineup_player_id'] = $lineupPlayer->getId();
                        $scoreData['team_id'] = $lineup->getOpponentTeamId();
                        $scoreData['hit_group'] = 'CR2019' . $event->getStart()->format('Y-m-d') . '#' . md5($lineupPlayer->getId() . rand(1, 1000) . time());

                        \Core\DbQuery::insertFromArray($scoreData, 'player_timeline_event');
                    }

                    $lineup->setStatus('closed');
                    if($overtime == '')
                    {
                        $lineup->setOvertime(1);
                    }
                    $lineupRepo->save($lineup);
                }
            }


            // \Core\DbQuery::execute('DELETE ' );
            /*
              if(null != $lineupPlayer)
              {
              for($i = 0;$i<$homePoints;$i++)
              {
              $scoreData = [];
              $scoreData['hit_type'] = 'goal';
              $scoreData['hit_time'] = '00:00:00';
              $scoreData['lineup_id'] = $lineup->getId();
              $scoreData['event_id'] = $eventId;
              $scoreData['event_date'] = $event->getStart()->format('Y-m-d').' 00:00:00';
              $scoreData['lineup_position'] = 'first_line';
              $scoreData['lineup_player_id'] = $lineupPlayer->getId();
              $scoreData['team_id'] = $lineup->getTeamId();
              $scoreData['hit_group'] = 'CR2019'.$event->getStart()->format('Y-m-d').'#'.md5($lineupPlayer->getId().rand(1,1000).time());

              \Core\DbQuery::insertFromArray($scoreData, 'player_timeline_event' );
              }

              for($i = 0;$i<$opponentPoints;$i++)
              {
              $scoreData = [];
              $scoreData['hit_type'] = 'goal';
              $scoreData['hit_time'] = '00:00:00';
              $scoreData['lineup_id'] = $lineup->getId();
              $scoreData['event_id'] = $eventId;
              $scoreData['event_date'] = $event->getStart()->format('Y-m-d').' 00:00:00';
              $scoreData['lineup_position'] = 'second_line';
              $scoreData['lineup_player_id'] = $lineupPlayer->getId();
              $scoreData['team_id'] = $lineup->getOpponentTeamId();
              $scoreData['hit_group'] = 'CR2019'.$event->getStart()->format('Y-m-d').'#'.md5($lineupPlayer->getId().rand(1,1000).time());

              \Core\DbQuery::insertFromArray($scoreData, 'player_timeline_event' );
              }

              $lineup->setStatus('closed');
              $lineupRepo->save($lineup);
              }
              else
              {

              }
             */
            /*
              $opponentTeamPlayers = $teamManager->getActiveTeamPlayers($event->getOpponentTeamId());
              foreach($opponentTeamPlayers as $teamPlayer)
              {
              if($teamPlayer->getFirstName() == 'RUFUS 2019')
              {
              $lineupPlayer = new \Webteamer\Team\Model\TeamMatchLineupPlayer();
              $lineupPlayer->setEventDate($lineup->getEventDate());
              $lineupPlayer->setLineupId($lineup->getId());
              $lineupPlayer->setCreatedAt(new \DateTime());
              $lineupPlayer->setPlayerId(0);
              $lineupPlayer->setPlayerName($teamPlayer->getFirstName());
              $lineupPlayer->setEventId($event->getId());
              $lineupPlayer->setLineupPosition('second_line');
              $lineupPlayer->setLineupName($lineup->getSecondLineName());
              $lineupPlayer->setTeamPlayerId($teamPlayer->getId());
              $lineupPlayer->setPlayerPosition('PLAYER');
              $lineupRepo->savePlayer($lineupPlayer);
              }
              }
             */
        }
    }
    
    public function registerTournamentTeams($tournament_id,$teamIds)
    {
        $tournamentRepo = ServiceLayer::getService('TournamentRepository');
        $security = ServiceLayer::getService('security');
        $user = $security->getIdentity()->getUser();
        foreach ($teamIds as $teamId)
        {
            $tournamentRepo->createTournamentTeam($tournament_id, $teamId, $user);
        }
    }
    
    public function createTeamAdmin()
    {
        $teams =  array(2776,2777,2778,2779,2780,2781,2782,2783,2784,2785,2786,2787);
        $users = array(3,34);
        
        $repo = ServiceLayer::getService('TeamPlayerRepository');
        
        foreach($teams as $teamId)
        {
            foreach($users as $user)
            {
                $teamPlayer = new \Webteamer\Team\Model\TeamPlayer();
                $teamPlayer->setTeamId($teamId);
                $teamPlayer->setPlayerId($user);
                $teamPlayer->setCreatedAt(new \DateTime());
                $teamPlayer->setStatus('confirmed');
                $teamPlayer->setTeamRole('ADMIN');
                $teamPlayer->setLevel('beginner');
                $teamPlayer->setFirstName('RUFUS ADMIN');
                $repo->save($teamPlayer);
            }
            
        }
    }
    
    public function resetTeamPlayers($teamIds)
    {
        $teamManager = ServiceLayer::getService('TeamManager');
        $teamPlayerRepo = ServiceLayer::getService('TeamPlayerRepository');
        
        foreach ($teamIds as $teamId)
        {
            $teamPlayers = $teamManager->getActiveTeamPlayers($teamId);
            foreach($teamPlayers as $teamPlayer)
            {
                $teamPlayer->setStatus('unactive');
                $teamPlayerRepo->save($teamPlayer);
            }
        }
    }

    private function createSchemaFromCsv($path)
    {
        $data = file($path);
        $schema = array( 
            'tournament_id' => 1473,
            'name' => 'Finále 2023- chlapci',
            'sport_id' => 42,
            'author_id' => 3,
            'termin' => '2023-05-24 08:00:00',
            'round' => 1,
            'type' => 'ladder');
        $matchId = 1;
        foreach($data as $rowData)
        {
            
            $row = explode(';',$rowData);
            //var_dump( $row);
            $schema['teamIds'][] = $row[6];
            $schema['teamIds'][] = $row[8];
            $eventIndex = 'round'.$row[5].'-match'.$matchId;
            $start = DateTime::createFromFormat('j.n.Y  H:i:s',$row[0].' '.$row[1]);
            $end = DateTime::createFromFormat('j.n.Y H:i:s',$row[0].' '.$row[2]);
            $schema['events'][$eventIndex] = array(
                'start' =>    $start->format('Y-m-d H:i:s'),
                'end_time' => $end->format('Y-m-d H:i:s'),
                'name' => trim($row[7]).':'.trim($row[9]),
                'team_id' => ($row[6] == '') ? null : $row[6],
                'opponent_team_id' => ($row[8] == '') ? null : $row[8],
                'author_id' => 3,
                'tournament_type' => 'ladder',
                'tournament_group' => $row[3],
                'tournament_id' =>  $row[4], 
                'tournament_round' => $row[5],
                'playground_id' =>trim($row[10]),
            );
            $matchId++;
        }
        $schema['teamIds'] = array_unique($schema['teamIds']);
        $schema['teamIds'] = array_filter($schema['teamIds']);
        return $schema;
    }

    public function importSchemaAction()
    {
        $schema = $this->createSchemaFromCsv(MODUL_DIR . '/tournament/schemas/finale_2023_chlapci.csv');
        //var_dump($schema );exit;
        //include(MODUL_DIR . '/tournament/schemas/krajKeDievcata.php');
        //$this->createTeamAdmin();
        
        $tournamentRepo = ServiceLayer::getService('TournamentRepository');
        $security = ServiceLayer::getService('security');
        $user = $security->getIdentity()->getUser();

        $this->registerTournamentTeams($schema['tournament_id'],$schema['teamIds']);
        $this->createEvents($schema);
        $this->resetTeamPlayers($schema['teamIds']);
        
        //$this->generateLineups($schema['tournament_id']);
        
        
        //$this->createTeamAdmin();

       
    }

}

?>