<?php

namespace CR\Tournament\Controller;

use Core\Controller;
use Core\ControllerResponse;
use Core\ServiceLayer;
use Core\Validator;
use const MODUL_DIR;


class EventController extends Controller {

    public function editAction()
    {
        $request = $this->getRequest();
        $security = ServiceLayer::getService('security');
        $repository = ServiceLayer::getService('TournamentRepository');
        $event = $repository->getTournamentEvent($this->getRequest()->get('id'));

        $user = $security->getIdentity()->getUser();
        $tournament = $repository->findUserTournament($user,$event->getTournamentId());
        $teams = $repository->findTournamentTeams($tournament->getId());
        $teamChoices = array();
        foreach($teams as $team)
        {
            $teamChoices[$team->getId()] = $team->getName();
        }
        
       
        $form = new \CR\Tournament\Form\EventForm();
        $form->setFieldChoices('team_id', $teamChoices);
        $form->setFieldChoices('opponent_team_id', $teamChoices);
        $form->setFieldValue('start_datetime', $event->getStart());
        $form->setEntity($event);
        
        $validator = new Validator();
        
         if ('POST' == $request->getMethod())
        {
            $postData = $request->get($form->getName());
            $postData['id'] = $this->getRequest()->get('id');
            $form->bindData($postData);
            $validator->setData($postData);
            $validator->validateData();

            if (!$validator->hasErrors())
            {
                $data['user_id'] = $user->getId();
                $data['tournament_id'] = $tournament->getId();
                $postData['name'] = $teamChoices[$postData['team_id']].':'.$teamChoices[$postData['opponent_team_id']];
                
                if(null !=  $postData['start_datetime'])
                {
                     $start = \DateTime::createFromFormat('d.m.Y H:i', $postData['start_datetime']); 
                    $postData['start'] =$start->format('Y-m-d H:i:s');
                }
                
               
                $data['event'] =  $postData;
                $repository->updateTournamentEvent($data);
                $request->redirect($this->getRouter()->link('tournament_schedule',array('id' => $tournament->getId())));
            }
        }

        
        
        
        
        return $this->render('CR\Tournament\TournamentModule:event:edit.php', array(
            'event' => $event,
            'team1' => $event->getTeam1(),
            'team2' => $event->getTeam2(),
            'tournament' => $tournament,
            'form' => $form,
            'validator' => $validator
        ));
    }
    
    
    public function detailAction()
    {
        $security = ServiceLayer::getService('security');
        $repository = ServiceLayer::getService('TournamentRepository');
        $event = $repository->getTournamentEvent($this->getRequest()->get('id'));
        $request = $this->getRequest();
     

        $user = $security->getIdentity()->getUser();
        $tournament = $repository->findUserTournament($user,$event->getTournamentId());
        
        //$repository->createTournamentMatch($event);
        $permissionManager = ServiceLayer::getService('TournamentPermissionManager');
        if(!$permissionManager->hasEventPermission($user,$event,'EVENT_NOMINATION_VIEW'))
        {
            throw new \AclException();
        }
        
        $matchOverview = $repository->getEventMatchOverview($event->getId());
        $matchStats = $matchOverview->getStats();
        
        if('POST' == $request->getMethod())
        {
            $teamId = $request->get('tid');
            $teamPlayers = array();
            $teamPosition = '';

            if($event->getTeam1()->getId() == $teamId)
            {
                $teamPlayers = $event->getTeam1()->getPlayers();
                $teamPosition = 'first_line';
            }
            
            if($event->getTeam2()->getId() == $teamId)
            {
                $teamPlayers = $event->getTeam2()->getPlayers();
                $teamPosition = 'second_line';
            }

            foreach($teamPlayers as $player)
            {
                $data['userId'] = $user->getId();
                $data['eventId'] = $event->getId();
                $data['type'] = $request->get('type');
                $data['teamPlayerId'] = $player->getId();
                $data['tournamentId'] = $tournament->getId();
                $data['teamId'] =$teamId;
                $data['teamPosition'] = $teamPosition;
                $result = $repository->changeEventNomination($data);
            }
            
            $request->redirect();
            
        }
        
        return $this->render('CR\Tournament\TournamentModule:event:detail.php', array(
            'event' => $event,
            'team1' => $event->getTeam1(),
            'team2' => $event->getTeam2(),
            'tournament' => $tournament,
            'permissionManager' => $permissionManager,
            'user' => $user,
             'matchStats' => $matchStats
        ));
    }
    
    public function nominationPrintAction()
    {
        $security = ServiceLayer::getService('security');
        $repository = ServiceLayer::getService('TournamentRepository');
        $event = $repository->getTournamentEvent($this->getRequest()->get('id'));
        $user = $security->getIdentity()->getUser();
        $tournament = $repository->findUserTournament($user,$event->getTournamentId());
        
        
        $layout = ServiceLayer::getService('layout');
        $layout->setTemplate(GLOBAL_DIR . '/templates/PrintLayout.php');

        return $this->render('CR\Tournament\TournamentModule:event:nomination_print.php', array(
            'event' => $event,
            'team1' => $event->getTeam1(),
            'team2' => $event->getTeam2(),
            'tournament' => $tournament
        ));

       
    }
    
    public function nominationAction()
    {
        $security = ServiceLayer::getService('security');
        $repository = ServiceLayer::getService('TournamentRepository');
        $event = $repository->getTournamentEvent($this->getRequest()->get('id'));
        $user = $security->getIdentity()->getUser();
        $tournament = $repository->findUserTournament($user,$event->getTournamentId());
        return $this->render('CR\Tournament\TournamentModule:event:nomination.php', array(
            'event' => $event,
            'team1' => $event->getTeam1(),
            'team2' => $event->getTeam2(),
            'tournament' => $tournament
        ));
    }
    
    public function changeNominationAction()
    {
        $request= $this->getRequest();
        $repository = ServiceLayer::getService('TournamentRepository');
        $security = ServiceLayer::getService('security');
        $user = $security->getIdentity()->getUser();
        
        $data['userId'] = $user->getId();
        $data['eventId'] = $request->get('id');
        $data['type'] = $request->get('type');
        $data['teamPlayerId'] = $request->get('team_player_id');
        $data['tournamentId'] = $request->get('tournament_id');
        $data['teamId'] = $request->get('team_id');
        $data['teamPosition'] = $request->get('team_position');
        $result = $repository->changeEventNomination($data);
        
        $layout = ServiceLayer::getService('layout');
        $layout->setTemplate(null);
        $controllerResponse = new ControllerResponse();
        $controllerResponse->setStatus('success');
        $controllerResponse->setContent(json_encode($result));
        $controllerResponse->setType('json');
        return $controllerResponse;
    }
    
    public function scoreAction()
    {
        $security = ServiceLayer::getService('security');
        $repository = ServiceLayer::getService('TournamentRepository');
        $event = $repository->getTournamentEvent($this->getRequest()->get('id'));
        $user = $security->getIdentity()->getUser();
        $tournament = $repository->findUserTournament($user,$event->getTournamentId()); 
        $matchOverview = $repository->getEventMatchOverview($event->getId());
        $eventTimeline = $matchOverview->getTimeline();
        $matchStats = $matchOverview->getStats();
        
        $permissionManager = ServiceLayer::getService('TournamentPermissionManager');
        if(!$permissionManager->hasEventPermission($user,$event,'EVENT_LIVESCORE'))
        {
            throw new \AclException();
        }
        
        $eventMatch = $event->getMatch();
        $template = 'score';
        if($eventMatch->getStatus() == 'closed')
        {
            $template = 'closed_match';
        }
        $event->setMatch($eventMatch);
        


        return $this->render('CR\Tournament\TournamentModule:event:'.$template.'.php', array(
            'event' => $event,
            'team1' => $event->getTeam1(),
            'team2' => $event->getTeam2(),
            'tournament' => $tournament,
            'lineup' => $eventMatch,
            'matchOverview' => $matchOverview,
            'eventTimeline' => $eventTimeline,
            'matchStats' => $matchStats,
            'permissionManager' => $permissionManager,
            'user' => $user
        ));
    }
    
    public function simpleScoreAction()
    {
        
        $security = ServiceLayer::getService('security');
        $repository = ServiceLayer::getService('TournamentRepository');
        $event = $repository->getTournamentEvent($this->getRequest()->get('id'));
        $user = $security->getIdentity()->getUser();
        $tournament = $repository->findUserTournament($user,$event->getTournamentId()); 
        $matchOverview = $repository->getEventMatchOverview($event->getId());
        $eventTimeline = $matchOverview->getTimeline();
        $matchStats = $matchOverview->getStats();
        $request = $this->getRequest();

        $permissionManager = ServiceLayer::getService('TournamentPermissionManager');
        if(!$permissionManager->hasEventPermission($user,$event,'EVENT_LIVESCORE'))
        {
            throw new \AclException();
        }
        
        $eventMatch = $event->getMatch();
        $event->setMatch($eventMatch);
        
        $points = array();

        foreach( $event->getTeam1()->getEventNominationIn() as $nomination)
        {
           
            $points['goals'][$event->getTeam1()->getId()][$nomination->getId()] = 0;
            $points['assists'][$event->getTeam1()->getId()][$nomination->getId()] = 0;
            $points['mom'][$event->getTeam1()->getId()][$nomination->getId()] = 0;
        }
        foreach( $event->getTeam2()->getEventNominationIn() as $nomination)
        {
            $points['goals'][$event->getTeam2()->getId()][$nomination->getId()] = 0;
            $points['assists'][$event->getTeam2()->getId()][$nomination->getId()] = 0;
            $points['mom'][$event->getTeam2()->getId()][$nomination->getId()] = 0;
        }
        
        foreach($eventTimeline as $group)
        {
            foreach($group  as $hit)
            {
                if('goal' == $hit->getHitType())
                {
                    $points['goals'][$hit->getTeamId()][$hit->getLineupPlayerId()]++;
                }
                if('assist' == $hit->getHitType())
                {
                    $points['assists'][$hit->getTeamId()][$hit->getLineupPlayerId()]++;
                }
                if('mom' == $hit->getHitType())
                {
                    $points['mom'][$hit->getTeamId()][$hit->getLineupPlayerId()] = 1;
                }
            }
        }

        
        if('POST'  == $request->getMethod())
        {
            $goals = $request->get('goals');
            $assists = $request->get('assists');

            //delete old
            $repository->clearEventStats(array(
                'event_id' => $event->getId(),
                'tournament_id' => $tournament->getId(),
                'user_id' => $user->getId()
            ));
             
           $matchResult = array($event->getTeam1()->getId() => 0,$event->getTeam2()->getId() => 0);
            //goals
            foreach($goals as $teamId => $teamGoals)
            {
                foreach($teamGoals as $playerId => $playerGoal)
                {
                     $goal = $playerGoal['points'];
                     
                     if(0 < $goal)
                     {
                        for($i=0;$i<$goal;$i++)
                        {
                            $data = array();
                            $data['eventId'] = $event->getId();
                            $data['eventDate'] = $event->getStart()->format('Y-m-d');
                            $data['lid'] = $eventMatch->getId();
                            $data['goal']['player_id'] = '';
                            $data['goal']['lineup_player_id'] = $playerId;
                            $data['goal']['time'] = '00:00:00';
                            $data['goal']['lid_team'] = $playerGoal['lineup'];
                            $data['goal']['tid'] = $teamId;
                            $data['hitType'] = 'points';
                            $data['time'] = '00:00:00';
                            $response = $repository->createMatchHit($data);
                            
                            $matchResult[$teamId]++;
                        }
                     }
                }
            }
            
            //assist
             foreach($assists as $teamId => $teamAssists)
            {
                foreach($teamAssists as $playerId => $playerAssist)
                {
                     $assist = $playerAssist['points'];
                     
                     if(0 < $assist)
                     {
                        for($i=0;$i<$assist;$i++)
                        {
                            $data = array();
                            $data['eventId'] = $event->getId();
                            $data['eventDate'] = $event->getStart()->format('Y-m-d');
                            $data['lid'] = $eventMatch->getId();
                            $data['assist']['player_id'] = '';
                            $data['assist']['lineup_player_id'] = $playerId;
                            $data['assist']['time'] = '00:00:00';
                            $data['assist']['lid_team'] = $playerAssist['lineup'];
                            $data['assist']['tid'] = $teamId;
                            $data['hitType'] = 'points';
                            $data['time'] = '00:00:00';
                            $response = $repository->createMatchHit($data);
                        }
                     }
                }
            }
           
            //mom
            if(null != $request->get('mom'))
            {
                $momParts = explode('-',$request->get('mom'));
                
                $data = array();
                $data['eventId'] = $event->getId();
                $data['eventDate'] = $event->getStart()->format('Y-m-d');
                $data['time'] =  '00:00:00';
                $data['hitType'] = 'mom';
                $data['lidTeam'] = $momParts[2];
                $data['lineupPlayerId'] = $momParts[1];

                $response = $repository->createMatchHit($data);
                
            }
            
        $data = array();
         $data['event_id'] = $event->getId();
         $data['result'] = $matchResult;
         $data['status'] = 'closed';
         $data['overtime'] = $request->get('overtime');
         
         
         $repository->updateEventResult($data);
            /*
            $repository->closeEvent(array(
            'id' => $event->getId(),
            'overtime' => $request->get('overtime')
                ));
            */
           $request->redirect();
        }
        
        $inOvertime = false;
        $matchResult = $eventMatch->getMatchResult();
        if($matchResult['first_line']['overtime'] == 1 or $matchResult['second_line']['overtime'] == 1 ) 
        {
            $inOvertime = true;
        }
        
        return $this->render('CR\Tournament\TournamentModule:event:simple_score.php', array(
            'event' => $event,
            'team1' => $event->getTeam1(),
            'team2' => $event->getTeam2(),
            'tournament' => $tournament,
            'lineup' => $eventMatch,
            'matchOverview' => $matchOverview,
            'eventTimeline' => $eventTimeline,
            'matchStats' => $matchStats,
            'points' => $points,
            'inOvertime' => $inOvertime
        ));
    }
    
    public function addHitAction()
    {
        $request= $this->getRequest();
        $repository = ServiceLayer::getService('TournamentRepository');
        $data['eventId'] = $request->get('event_id');
        $data['eventDate'] = $request->get('event_date');
        $data['lid'] = $request->get('lid');
        $data['goal'] = $request->get('goal');
        
        $assistData = $request->get('assist');
        if($assistData['lineup_player_id'] != 0)
        {
            $data['assist'] = $request->get('assist');
        }
        
        $data['time'] = $request->get('time');
        $data['hitType'] = 'points';
        
        
        $response = $repository->createMatchHit($data);


         ob_start();
        ServiceLayer::getService('layout')->includePart(MODUL_DIR . '/tournament/view/event/_timeline_event_row.php', array(
            'group' => $response,
            'groupId' => $response[0]->getHitGroup(),
            'players' => array(),
            'groupTime' => $data['time']
        ));
        $row = ob_get_contents();
        ob_end_clean();
        $result = $row;

        $layout = ServiceLayer::getService('layout');
        $layout->setTemplate(null);
        $controllerResponse = new ControllerResponse();
        $controllerResponse->setStatus('success');
        $controllerResponse->setContent(json_encode($result));
        $controllerResponse->setType('json');
        return $controllerResponse;

    }
    
    public function editHitAction()
    {
        $request= $this->getRequest();
        $repository = ServiceLayer::getService('TournamentRepository');
        $data['eventId'] = $request->get('event_id');
        $data['eventDate'] = $request->get('event_date');
        $data['lid'] = $request->get('lid');
        $data['goal'] = $request->get('goal');
        $data['assist'] = $request->get('assist');
        $data['time'] = $request->get('time');
        $response = $repository->editMatchHit($data);


         ob_start();
        ServiceLayer::getService('layout')->includePart(MODUL_DIR . '/tournament/view/event/_timeline_event_row.php', array(
            'group' => $response,
            'groupId' => $response[0]->getHitGroup(),
            'players' => array(),
            'groupTime' => $data['time']
        ));
        $row = ob_get_contents();
        ob_end_clean();
        $result = $row;

        $layout = ServiceLayer::getService('layout');
        $layout->setTemplate(null);
        $controllerResponse = new ControllerResponse();
        $controllerResponse->setStatus('success');
        $controllerResponse->setContent(json_encode($result));
        $controllerResponse->setType('json');
        return $controllerResponse;

    }
    
    public function addMomAction()
    {
        $request= $this->getRequest();
        $repository = ServiceLayer::getService('TournamentRepository');
        $data['eventId'] = $request->get('event_id');
        $data['eventDate'] = $request->get('event_date');
        $data['time'] = $request->get('time');
        $data['hitType'] = 'mom';
        $data['lidTeam'] = $request->get('lid_team');
        $data['lineupPlayerId'] = $request->get('lineup_player_id');
        
        if($data['lineupPlayerId'] != 0)
        {
             $response = $repository->createMatchHit($data);
        
             
             
        }
        
        
       
        $repository->closeEvent(array(
            'id' => $request->get('event_id'),
            'overtime' => $request->get('overtime')
                ));
       

        
        /*
        $layout = ServiceLayer::getService('layout');
        $layout->setTemplate(null);
        $controllerResponse = new ControllerResponse();
        $controllerResponse->setStatus('success');
        $controllerResponse->setContent(json_encode($result));
        $controllerResponse->setType('json');
        return $controllerResponse;
        */
    }
    
    public function addMatchPhaseAction()
    {
        $request= $this->getRequest();
        $repository = ServiceLayer::getService('TournamentRepository');
        $data['eventId'] = $request->get('event_id');
        $data['eventDate'] = $request->get('event_date');
        $data['time'] = $request->get('time');
        $data['hitType'] = $request->get('type');
        $data['phase'] = $request->get('phase');


        $response = $repository->createMatchHit($data);
    }
    
    public function scoreResultAction()
    {
         $request= $this->getRequest();
         $repository = ServiceLayer::getService('TournamentRepository');

         
         $data['event_id'] = $request->get('id');
         $data['result'] = $request->get('result');
         $data['status'] = $request->get('status');
         $data['overtime'] = $request->get('overtime');
         
         $repository->updateEventResult($data);
         
         $request->redirect();
    }
    
    public function addMediaAction()
    {
         $request= $this->getRequest();
         $repository = ServiceLayer::getService('TournamentRepository');

         
         $data['id'] = $request->get('event_id');
         $media = array();
         $media['code'] = $request->get('code',false);
         $media['link'] = $request->get('link');
         $data['media'] = json_encode($media);
         
         

         
         $repository->updateTournamentEvent(array('event' => $data));
         
         $request->redirect();
    }
    
    public function resetMatchAction()
    {
        $request= $this->getRequest();
        $repository = ServiceLayer::getService('TournamentRepository');
        $security = ServiceLayer::getService('security');
        $data['eventId'] = $request->get('id');
        
        $event = $repository->getTournamentEvent($request->get('id'));
        $user = $security->getIdentity()->getUser();
        
        $permissionManager = ServiceLayer::getService('TournamentPermissionManager');
        if(!$permissionManager->hasEventPermission($user,$event,'EVENT_LIVESCORE'))
        {
            throw new \AclException();
        }
        
     
        
        //set status as open
        $repository->resetEvent(array('id' => $event->getId()));
             

       
        $request->redirect($this->getRouter()->link('tournament_schedule',array('id' => $request->get('tid'))));
    }
    
    
}

?>