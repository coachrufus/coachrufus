<?php

namespace CR\Tournament\Controller;

use AclException;
use Core\Controller;
use Core\ControllerResponse;
use Core\GUID;
use Core\Notifications\TournamentTeamAdminInvitationNotificationSender;
use Core\Notifications\TournamentTeamAdminInvitationStore;
use Core\ServiceLayer;
use Core\Validator;
use CR\Tournament\Form\ModalEditPlayerForm;
use CR\Tournament\Form\RegisterForm;
use CR\Tournament\Form\TeamForm;
use CR\Tournament\Form\TeamPlayerForm;
use CR\Tournament\Model\Team;
use stdClass;
use User\Handler\StepRegisterHandler;
use const GLOBAL_DIR;
use const MODUL_DIR;
use const WEB_DOMAIN;


class TeamsController extends Controller {

    public function overviewAction()
    {
         $request = $this->getRequest();
        $security = ServiceLayer::getService('security');
        $user = $security->getIdentity()->getUser();
        $repository = ServiceLayer::getService('TournamentRepository');
        $permissionManager = ServiceLayer::getService('TournamentPermissionManager');
        
        $tournament = $repository->findUserTournament($user,$this->getRequest()->get('id'));
        $events = $repository->getTournamentEvents($tournament->getId());
        
        $filterData = $request->get('filter');
        $filterCriteria = array();
           
        if(null != $filterData['region'])
        {

            $filerCriteria['locality'] = $filterData['region'].'sk';
        }

        if(null != $filterData['group'])
        {
            $filerCriteria['division'] = $filterData['group'];
        }
          

        $teams = $repository->findTournamentTeams($tournament->getId(),$filerCriteria);
        
          //$filter
        $filterForm = new \CR\Tournament\Form\StatsFilterForm();
        $filterForm->bindData($request->get('filter'));
        $filterForm->buildDivisionChoices($request->get('filter'),$teams);
        
        
        /*
        $teams = $allTeams;
        $filterData = $request->get('filter');
        if(null !=$filterData)
        {
            $teams = array();
            foreach($teams as $team)
            {
                if(array_key_exists('region', $filterData))
                {
                    
                }
            }
        }
        */
        
        

        return $this->render('CR\Tournament\TournamentModule:teams:overview.php', array(
           'tournament' => $tournament,
           'events' => $events,
           'teams' => $teams,
           'user' => $user,
           'permissionManager' => $permissionManager,
           'filterForm' => $filterForm
        ));
    }
    
    public function detailAction()
    {
       
       
        $request = $this->getRequest();
        $security = ServiceLayer::getService('security');
        $repository = ServiceLayer::getService('TournamentRepository');
        $user = $security->getIdentity()->getUser();
        $teamId = $request->get('team_id');
        $team = $repository->findTournamentTeam($teamId);
       
        $tournament = $repository->findUserTournament($user,$this->getRequest()->get('id'));
        $teamPlayers = $repository->findTeamPlayers($teamId);
        $addPlayerForm  = new TeamPlayerForm();
        
        return $this->render('CR\Tournament\TournamentModule:teams:detail.php', array(
           'tournament' => $tournament,
            'teamPlayers' => $teamPlayers,
            'addForm' =>$addPlayerForm,
            'teamId' => $request->get('team_id'),
            'team' => $team
        ));
    }
    
    public function indexAction()
    {
        $security = ServiceLayer::getService('security');
        $user = $security->getIdentity()->getUser();
        $repository = ServiceLayer::getService('TournamentRepository');
        $tournament = $repository->findUserTournament($user,$this->getRequest()->get('id'));
        
        //$tournamentManager = ServiceLayer::getService('TournamentManager');
        $tournamentTeamManager = ServiceLayer::getService('TournamentTeamManager');
        
        $teams = $tournamentTeamManager->getTournamentTeams($tournament);
        
        /*
        $permissionManager = ServiceLayer::getService('TournamentPermissionManager');
        if(!$permissionManager->isTournamentAdmin($user,$tournament))
        {
            throw new AclException();
        }
        */
        $teamForm = new TeamForm();
        $editForm = new TeamForm();
        $request = $this->getRequest();
        if('POST' == $request->getMethod())
        {
            $postData = $request->get('team');
            
            $tournamentTeams = array();
            foreach($postData as $teamId => $teamData)
            {
                $team = new Team();
                $team->setId($teamId);
                $team->setName($teamData['name']);
                $team->setEmail($teamData['email']);
                $tournamentTeams[] = $repository->convertToArray($team);
            }
            $tournamentData = $repository->convertToArray($tournament);

            $tournamentData['teams'] = $tournamentTeams;
            
            
            $repository->updateTournament($tournamentData);

            $request->redirect();
            
        }

        return $this->render('CR\Tournament\TournamentModule:teams:index.php', array(
            'addForm' => $teamForm,
            'editForm' => $editForm,
            'tournament' => $tournament,
            'teams' => $teams
        ));
    }
    
    public function createAction()
    {
        $request = $this->getRequest();
        $security = ServiceLayer::getService('security');
        $user = $security->getIdentity()->getUser();
        $repository = ServiceLayer::getService('TournamentRepository');
        $teamPlayerRepository = ServiceLayer::getService('TeamPlayerRepository');
        $teamManager = ServiceLayer::getService('TeamManager');
        $tournament = $repository->findUserTournament($user,$this->getRequest()->get('id'));
        
        $teams = $request->get('team');

        $tournamentTeams = array();
        foreach($teams as $teamFormData)
        {
            $team = new Team();
            $team->setName($teamFormData['name']);
            $team->setEmail($teamFormData['email']);
            $teamData  = $repository->convertToArray($team);
            
            if(null != $teamFormData['team_id'])
            {
                //$team->setExternalId($teamFormData['team_id']);
                
                $teamData['external_id'] = $teamFormData['team_id'];
                $tournamentTeamPlayers  = array();
                //get team players
                $teamPlayers = $teamManager->getActiveTeamPlayers($teamFormData['team_id']);
                foreach($teamPlayers as $teamPlayer)
                {
                    $teamPlayer->setId(null);
                    $tournamentTeamPlayers[] = $teamPlayerRepository->convertToArray($teamPlayer);
                }
                
                $teamData['players'] = $tournamentTeamPlayers;
                
            }
            $tournamentTeams[] = $teamData;
           
        }
        $tournamentData = $repository->convertToArray($tournament);
        $tournamentData['teams'] = $tournamentTeams;
        $repository->updateTournament($tournamentData);
       
        $request->redirect();
        /*
        return $this->render('CR\Tournament\TournamentModule:teams:index.php', array(
            'addForm' => $teamForm,
            'tournament' => $tournament
        ));
         * 
         */
        
    }
    
    public function changePhotoAction()
    {
        $request = ServiceLayer::getService('request');
        $response = new ControllerResponse();
        //validate file size
        $fileSize = $request->getFileSize('team_photo');
        $allowedSize = 2 * 1024 * 1024;
        if ($allowedSize > $fileSize)
        {
            $tmp_name = md5(time().rand(1,100)) . '_' . $_FILES['team_photo']['name'];
            $request->uploadFile('team_photo', PUBLIC_DIR . '/img/tournament', $tmp_name);
           

            $imageTransform = ServiceLayer::getService('imageTransform');
            $imageTransform->resizeImage(array(
                'sourceImg' => PUBLIC_DIR . 'img/tournament/' . $tmp_name,
                'width' => 320,
                'height' => 320,
                'targetDir' => PUBLIC_DIR . '/img/tournament'));

          

            $result['result'] = 'SUCCESS';
            $result['file'] = '/img/tournament/' . $tmp_name;
            $result['base_name'] = $tmp_name;
            
            $type = pathinfo(PUBLIC_DIR . '/img/tournament/'.$tmp_name, PATHINFO_EXTENSION);
            $data = file_get_contents(PUBLIC_DIR . '/img/tournament/'.$tmp_name);
            $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
            
            
            
            $result['binary'] = $base64;
            $response->setStatus('success');
        }
        else
        {
            $response->setStatus('success');
            $result['result'] = 'ERROR';
            $result['error_message'] = ServiceLayer::getService('translator')->translate('Allowed max size is 2MB');
        }





        $layout = ServiceLayer::getService('layout');
        $layout->setTemplate(null);



        $response->setContent(json_encode($result));
        $response->setType('json');
        return $response;
    }

    public function removePhotoAction()
    {
        
    }
    
    public function playersAction()
    {
        $request = $this->getRequest();
        $security = ServiceLayer::getService('security');
        $repository = ServiceLayer::getService('TournamentRepository');
        $privacyRepo = ServiceLayer::getService('PlayerPrivacyRepository');
        $user = $security->getIdentity()->getUser();
        $teamId = $request->get('team_id');
        $team = $repository->findTournamentTeam($teamId);
        $tournament = $repository->findUserTournament($user,$this->getRequest()->get('id'));
        
        
        if('POST' == $request->getMethod() && 'team-info' == $request->get('form_act'))
        {
           $data = $request->get('team');
           $data['id'] = $teamId;
           $data['photo_data'] =  $request->get('photo_data');
           
           unlink(PUBLIC_DIR . '/img/tournament/'.$data['photo']);
           
            $repository->updateTournamentTeam($data);
            $request->redirect();
        }
        
        /*
        if(!$security->hasTournamentTeamAdminAgreements($security->getIdentity()->getUser(),$tournament))
        {
            $layout = \Core\ServiceLayer::getService('layout');
            $layout->setTemplateParameters(array('body-class' => 'full-overlay-window'));
             return $this->render('CR\Tournament\TournamentModule:teams:gdpr_marketing.php', array(
           'tournament' => $tournament
            ));
        }
        */
        
       
        $permissionManager = ServiceLayer::getService('TournamentPermissionManager');
        /*
        if(!$permissionManager->hasTeamPermission($user,$team,'MANAGE_PLAYERS'))
        {
            throw new \AclException();
        }
         */
         
       
        $teamPlayers = $repository->findTeamPlayers($teamId);
        $addPlayerForm  = new TeamPlayerForm();
        
        $editForm = new TeamPlayerForm();
        $modalForm = new ModalEditPlayerForm();
        $validator = new Validator();
       
        
        return $this->render('CR\Tournament\TournamentModule:teams:players.php', array(
           'tournament' => $tournament,
            'teamPlayers' => $teamPlayers,
            'addForm' =>$addPlayerForm,
            'editForm' =>$editForm,
            'teamId' => $request->get('team_id'),
            'validator' => $validator,
            'modalForm' => $modalForm,
            'team' => $team
        ));
    }
    
    public function createPlayerAction()
    {
        $request = $this->getRequest();
        $security = ServiceLayer::getService('security');
        $user = $security->getIdentity()->getUser();
        $repository = ServiceLayer::getService('TournamentRepository');
        $teamPlayerRepository = ServiceLayer::getService('TeamPlayerRepository');
        $teamManager = ServiceLayer::getService('TeamManager');
        $tournament = $repository->findUserTournament($user,$this->getRequest()->get('id'));
        
        $players = $request->get('player');
        /*
        $newPlayers = array();
        foreach($players as $player)
        {
            
            $teamPlayer  = new \Webteamer\Team\Model\TeamPlayer();
            $teamPlayer->setFirstName($player['first_name']);
            $teamPlayer->setLastName($player['last_name']);
            $teamPlayer->setEmail($player['email']);
            
            $newPlayers[] = $player;
        }
         * 
         */
        $data['team_id'] = $request->get('team_id');
        $data['user_id'] = $user->getId();
        $data['tournament_id'] = $tournament->getId();
        $data['players'] =  $players;

        
        $repository->createTournamentTeamPlayers($data);
        $request->redirect();
    }
    
    public function editPlayerAction()
    {
        $request = $this->getRequest();
        $security = ServiceLayer::getService('security');
        $translator = ServiceLayer::getService('translator');
        $user = $security->getIdentity()->getUser();
        $repository = ServiceLayer::getService('TournamentRepository');
        $teamPlayerRepository = ServiceLayer::getService('TeamPlayerRepository');
        $teamManager = ServiceLayer::getService('TeamManager');
        $tournament = $repository->findUserTournament($user,$this->getRequest()->get('id'));
        
        $playerData = $request->get('edit_modal_form');
        $playerData['id'] =  $request->get('player_id');
        
       
        $data['team_id'] = $request->get('team_id');
        $data['user_id'] = $user->getId();
        $data['tournament_id'] = $tournament->getId();
        $data['player'] =  $playerData;
        
        $validator = new Validator();
        //validate email
        if(null != $playerData['email'])
        {
            if(false ==  $validator->validateEmail($playerData['email']))
            {
               $validator->addErrorMessage('email', 'Unvalid email');
            }
           
        }
        if (!$validator->hasErrors())
        {
            $repository->updateTournamentTeamPlayer($data);
            $result = array('result' => 'SUCCESS');
        }
        else
        {
             $result = array('result' => 'ERROR','errors' => $validator->getErrors());
        }
        

        $layout = ServiceLayer::getService('layout');
        $layout->setTemplate(null);
        $controllerResponse = new ControllerResponse();
        $controllerResponse->setStatus('success');
        $controllerResponse->setContent(json_encode($result));
        $controllerResponse->setType('json');
        return $controllerResponse;
        
        
       // $request->redirect();
    }
    
    public function deletePlayerAction()
    {
        $request = $this->getRequest();
        $security = ServiceLayer::getService('security');
        $user = $security->getIdentity()->getUser();
        $repository = ServiceLayer::getService('TournamentRepository');
       
        $tournament = $repository->findUserTournament($user,$this->getRequest()->get('tid'));
        

        $data['user_id'] = $user->getId();
        $data['tournament_id'] = $tournament->getId();
        $data['id'] =  $this->getRequest()->get('id');

        
        $repository->deleteTournamentTeamPlayer($data);
        $request->redirect();
    }
    
    public function inviteAdminAction()
    {
        $request = $this->getRequest();
        $teamId = $this->getRequest()->get('tid');
        $repository = ServiceLayer::getService('TournamentRepository');
        $teamRepository = ServiceLayer::getService('TournamentTeamRepository');
        
        $security = ServiceLayer::getService('security');
        $user = $security->getIdentity()->getUser();
        $targetEmail = $this->getRequest()->get('e');
        
        $team = $repository->findTournamentTeam($teamId);
        $tournament = $tournament = $repository->findUserTournament($user,$team->getTournamentId());
        
        //check if user exist
        $userRepo = ServiceLayer::getService('user_repository');
        $targetUser = $userRepo->findOneBy(array('email' => $targetEmail));
        
        
    
        //create waiting admin
        $permissionData = array();
        if(null != $targetUser)
        {
            $permissionData['userId'] = $targetUser->getId();
           
        }
        $permissionData['userEmail'] = $targetEmail;
        $permissionData['teamId'] = $teamId;
        $permissionData['tournamentId'] = $team->getTournamentId();
        $permissionData['permissionType'] = 'TEAM_ADMIN';
        $permissionData['status'] = 'waiting';
        $permissionData['hash'] = sha1(time().rand(1,1000));

        $teamRepository->createTeamPermission($permissionData);
        
        //sendInvitation
        $hash = $permissionData['hash'];
        $notifyManger = ServiceLayer::getService('notification_manager');
        $notifyManger->sendTournamentAdminInvitation(ServiceLayer::getService('security')->getIdentity()->getUser(),$targetEmail,$team,$tournament,$hash);    
        
        $request->addFlashMessage('invite_admin_success',$this->getTranslator()->translate('tournament.team.list.sendInvitation.success'));
        $request->redirect();
    }
    
    public function inviteAdminResponseAction()
    {
       
        $teamRepository = ServiceLayer::getService('TournamentTeamRepository');
        $permissionHash = $this->getRequest()->get('t');
        $action = $this->getRequest()->get('r');
        $request = $this->getRequest();
        $security = ServiceLayer::getService('security');
        
        $layout = ServiceLayer::getService('layout');
        $layout->setTemplate(GLOBAL_DIR.'/templates/LoginLayout.php');

        
        $data['hash'] = $permissionHash;
        $permission = $teamRepository->getTeamPermissionByHash($data['hash']);
        $team = $teamRepository->findTournamentTeam($permission['team_id']);

        $form =  new RegisterForm();
        $form->setFieldValue('email', $permission['user_email']);
        
        if('POST' == $request->getMethod())
        {
            $userData = $request->get('record');
            $form->bindData($userData);
            
            $registerHandler = new StepRegisterHandler();
            
            //$userData['team_admin_agreement'] = ('1' == $userData['agree3']) ? '1' : '0';
            //$userData['vop_agreement'] = ('1' == $userData['agree2']) ? '1' : '0';

            
            $registerHandler->setPostData($userData);
            $registerHandler->sanitizeData();
            $validate_result = $registerHandler->validateStart();
            
           if(null == $userData['name'])
           {
               $registerHandler->getValidator('start')->addErrorMessage('meno','Required Field!');     
           }
           if(null == $userData['surname'])
           {
               $registerHandler->getValidator('start')->addErrorMessage('surname','Required Field!');     
           }
            
           /*
           if(null ==   $userData['agree1'])
           {
               $registerHandler->getValidator('start')->addErrorMessage('agree1','Required Field!');     
           }
           if(null ==  $userData['agree2'])
           {
               $registerHandler->getValidator('start')->addErrorMessage('agree2','Required Field!');     
           }
           
           if(null ==  $userData['agree3'])
           {
               $registerHandler->getValidator('start')->addErrorMessage('agree3','Required Field!');     
           }
           */
           
         
            
            if ($validate_result && !$registerHandler->getValidator('start')->hasErrors())
            {
                $activationGuid = GUID::create();
                $registerData['activation_guid'] = $activationGuid;
                $registerData['status'] = 1;
                $registerData['password'] = $pass;
                $registerData['email'] = $permission['user_email'];
                $registerData['team_admin_agreement'] = $userData['team_admin_agreement'];
                $registerData['vop_agreement'] = $userData['vop_agreement'];
              
                $registerHandler->persistStartData($registerData);

                $newUser = $security->getIdentityProvider()->findUserByUsername($permission['user_email']);
                $newUser->setDefaultLang('sk');
                $newUser->setName($userData['name']);
                $newUser->setSurname($userData['surname']);

               
                

                $userRepo = ServiceLayer::getService('user_repository');
                $userRepo->save($newUser);

                $data['status'] = 'confirmed';
                $data['user_id'] = $newUser->getId();
                $teamRepository->updateTeamPermission($data);
                
                 //export to mailchimp
                $repository = ServiceLayer::getService('TournamentRepository');
                $repository->mailchimpExport($newUser,$team);
                
                

                $security->authenticateUserEntity($newUser);
                $request->redirect($this->getRouter()->link('tournament'));
            }
            
            
             $template = 'invitation_accept';

            return $this->render('CR\Tournament\TournamentModule:teams:'.$template.'.php', array(
               'form' => $form,
                'validator' => $registerHandler->getValidator('start'),
            ));
               
           
        }
        else
        {
            if('accept' == $action)
            {
                //check user
                $existUser = $security->getIdentityProvider()->findUserByUsername($permission['user_email']);
                if(null != $existUser)
                {
                    $data['status'] = 'confirmed';
                    $data['user_id'] = $existUser->getId();
                    $teamRepository->updateTeamPermission($data);
                    
                    $repository = ServiceLayer::getService('TournamentRepository');
                    $repository->mailchimpExport($existUser,$team);
                    
                    
                    $security->authenticateUserEntity($existUser);
                    $request->redirect($this->getRouter()->link('tournament'));
                }
                
                
                
                $template = 'invitation_accept';
                $form =  new RegisterForm();
                $form->setFieldValue('email', $permission['user_email']);

                $registerHandler = new StepRegisterHandler();
                
                 $conditionsLink = 'http://www.coachrufus.com/terms-and-conditions/';
                if('en' == $_SESSION['app_lang'])
                {
                     $conditionsLink = 'http://www.coachrufus.com/en/terms-and-conditions/';
                }

                return $this->render('CR\Tournament\TournamentModule:teams:'.$template.'.php', array(
                   'form' => $form,
                    'validator' => $registerHandler->getValidator('start'),
                    'conditionsLink' => $conditionsLink
                ));
                //$data['status'] = 'confirmed';
            }
            if('reject' == $action)
            {
                $data['status'] = 'rejected';
                $teamRepository->updateTeamPermission($data);
                $template = 'invitation_rejected';
                 return $this->render('CR\Tournament\TournamentModule:teams:'.$template.'.php', array(

                ));
            }
        
        }
        
        
        
        
        
       
        /*
        $permission = $teamRepository->getTeamPermissionByHash($data['hash']);
        if(null != $permission['user_id'])
        {
            $teamRepository->updateTeamPermission($data);
            $request->redirect($this->getRouter()->link('tournament'));
        }
        else //create new user and login
        {
            $registerHandler = new \User\Handler\StepRegisterHandler();
            $pass = substr(md5(time().rand(1,100)),0,8);
            
            $activationGuid = \Core\GUID::create();
            $registerData['activation_guid'] = $activationGuid;
            $registerData['status'] = 1;
            $registerData['password'] = $pass;
            $registerData['email'] = $permission['user_email'];
            $registerHandler->persistStartData($registerData);

            $newUser = $security->getIdentityProvider()->findUserByUsername($permission['user_email']);
            
            $data['user_id'] = $newUser->getId();
            $teamRepository->updateTeamPermission($data);
            
            
            $security->authenticateUserEntity($newUser);
            $request->redirect($this->getRouter()->link('tournament'));
        }
        */
        
        
        
        
    }
    
    public function sendBulkInvitationAction()
    {
        /*
       $sender = new TournamentTeamAdminInvitationNotificationSender(new TournamentTeamAdminInvitationStore());
        $notifyData = new stdClass();
        $notifyData->email = $targetEmail;
        $notifyData->teamName = $team->getName();
        $notifyData->senderName = $user->getFullName();
        $notifyData->tournamentName = $tournament->getName();
        $notifyData->default_lang =  $notifyLang;
        $notifyData->acceptLink =   WEB_DOMAIN.$this->getRouter()->link('tournament_team_invite_response',array('t' => $hash,'r' => 'accept'));
        $notifyData->declineLink =  WEB_DOMAIN.$this->getRouter()->link('tournament_team_invite_response',array('t' => $hash,'r' => 'reject'));
        $sender->send($notifyData);
        */
        $data = file_get_contents(MODUL_DIR.'/tournament/model/invite.csv');
        $data = explode('####',$data);
        $double = array();
        
        
        $emails = array();
        $i = 1;
        //$data = array_slice($data,0,10);
        foreach($data as $d)
        {
            $teamData = explode(';',$d);
            //t_dump($teamData);exit;
            $email = str_replace('emial: ', '', $teamData[7]);
            
            $email = explode(',',$email);
            
            /*
SELECT t.*,  tur.name, tt.name, '####' FROM `tournament_permission` t
LEFT JOIN team tt ON t.team_id = tt.id
LEFT JOIN tournament tur ON t.tournament_id = tur.id
where (t.tournament_id = 1428 or t.tournament_id = 1424  or t.tournament_id = 1425)
and t.status = 'waiting'
             *              */
            
            
            
            $email = $email[0];
            $teamName = $teamData[9];
            $senderName = '';
            $tournamentName = $teamData[8];
            $default_Lang = 'sk';
            $hash = $teamData[1];
            
           
            
            
            $sender = new TournamentTeamAdminInvitationNotificationSender(new TournamentTeamAdminInvitationStore());
            $notifyData = new stdClass();
            $notifyData->email = trim($email);
            //$notifyData->email = 'marek.hubacek@plus421.com';
            $notifyData->teamName = trim($teamName);
            $notifyData->senderName = 'Martin Konarsky';
            $notifyData->tournamentName = trim($tournamentName);
            $notifyData->default_lang =  'sk';
            $notifyData->acceptLink =   WEB_DOMAIN.$this->getRouter()->link('tournament_team_invite_response',array('t' => $hash,'r' => 'accept'));
            $notifyData->declineLink =  WEB_DOMAIN.$this->getRouter()->link('tournament_team_invite_response',array('t' => $hash,'r' => 'reject'));
            /*
            $sender->send($notifyData);
            */
            echo $i++;
             echo '<br />';
            echo 'AHOJ '. $notifyData->email;
            echo '<br />';
            echo 'Administrátor turnaja '. $notifyData->tournamentName.' ťa práve pozval administrovať tím  '. $notifyData->teamName .' .<br />';
            echo $notifyData->acceptLink;
            echo '<br />';
            echo $notifyData->declineLink;
            echo '<br />';
            echo '<br />';
            echo '<br />';
            echo '<br />';

            if('' == $notifyData->teamName or '' == $notifyData->email or '' == $notifyData->tournamentName)
            {
                $fails[] = $d;
            }
            $emails[$notifyData->email] = $notifyData->email;
            
        }
        
        /*
        foreach($double as $d)
        {
            $emails = explode(',',$d[7]);
            
            $newEmails = array_slice($emails, 1);
            
            
            foreach($newEmails as $email)
            {
                $hash = sha1(rand(1,100).$email);
                $email  = str_replace(' ', '', $email);
                echo "INSERT INTO tournament_permission (hash, team_id, tournament_id, permission_type, status, user_email) "
                . "VALUES ('".$hash."', '".$d[3]."', '".$d[4]."', 'TEAM_ADMIN', 'waiting', '".trim($email)."');<br />";
            }
        }
         * 
         */
       //var_dump($emails);
        
        //var_dump($double);
        exit;
     
    }

}

?>