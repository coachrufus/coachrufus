<?php

namespace CR\Tournament\Controller;

use Core\Controller;
use Core\ServiceLayer;
use Core\Validator;
use DateTime;
use Webteamer\Locality\Form\PlaygroundForm;
use CR\Tournament\Model\Team;

class TournamentController extends Controller {

    public function startAction()
    {

        return $this->render('CR\Tournament\TournamentModule:tournament:start.php', array(
                        //'lastTournaments' => $lastTournaments,
        ));
    }

    public function invitationAction()
    {
        $security = ServiceLayer::getService('security');
        $user = $security->getIdentity()->getUser();
        $repository = ServiceLayer::getService('TournamentRepository');
        $hash = $this->getRequest()->get('hash');
        $id = substr($hash, 3);
        $id = substr($id, 0, -8);

        $tournament = $repository->findUserTournament($user, $id);
        $teamManager = ServiceLayer::getService('TeamManager');
        $userTeams = $teamManager->getUserTeams($user);
        $tournamentTeams = $repository->findTournamentTeams($tournament->getId());
        $tournamentTeamsIndexed = array();
        foreach ($tournamentTeams as $tournamentTeam)
        {
            $tournamentTeamsIndexed[$tournamentTeam->getId()] = $tournamentTeam->getStatus();
        }

        /*
        $availableTeams = array();
        foreach($userTeams as $userTeam)
        {
            if($userTeam->getGender() == $tournament->getGender())
            {
                $availableTeams[] = $userTeam;
            }
        }
        */
        $availableTeams = $userTeams;
        
        $template  = 'invitation.php';
        if('disallowed' == $tournament->getRegisterStatus())
        {
             $template  = 'invitation_disabled.php';
        }
        
        return $this->render('CR\Tournament\TournamentModule:tournament:'.$template, array(
                    'tournament' => $tournament,
                    'teams' => $availableTeams,
                    'tournamentTeamsIndexed' => $tournamentTeamsIndexed
        ));
    }

    public function invitationAcceptAction()
    {
        $security = ServiceLayer::getService('security');
        $user = $security->getIdentity()->getUser();

        $repository = ServiceLayer::getService('TournamentRepository');
        $translator = ServiceLayer::getService('translator');

        $tournamentManager = ServiceLayer::getService('TournamentManager');

        //$teamPlayerRepository = ServiceLayer::getService('TeamPlayerRepository');
        $hash = $this->getRequest()->get('hash');
        $id = substr($hash, 3);
        $id = substr($id, 0, -8);

        $tournament = $tournamentManager->findUserTournament($user, $id);
        $teamManager = ServiceLayer::getService('TeamManager');
        $team = $teamManager->findTeamByHash($this->getRequest()->get('th'));

        
        
        //check if team exist
         $tournamentTeams = $repository->findTournamentTeams($tournament->getId());
         foreach($tournamentTeams as $tournamentTeam)
         {
             if($tournamentTeam->getId() == $team->getId())
             {
                 $message  = '';
                 if($tournamentTeam->getStatus() == 'confirmed')
                 {
                     $message = $translator->translate('tournament.invitation.alreadySigned');
                 }
                 if($tournamentTeam->getStatus() == 'waiting-to-confirm')
                 {
                     $message = $translator->translate('tournament.invitation.waiting');
                 }
                 if($tournamentTeam->getStatus() == 'decline')
                 {
                     $message = $translator->translate('tournament.invitation.decline');
                 }

                 $this->getRequest()->addFlashMessage('tournament_invitation_accept_error', $message);
                 $this->getRequest()->redirect($this->getRouter()->link('tournament_share_link',array('hash' => $hash )));
             }
         }
        


        $tournamentManager->registerTeamToTournament($tournament, $team, $user);

        //send notification
        $notifyManager = ServiceLayer::getService('notification_manager');
        $notifyManager->sendTournamentJoinAdminNotification($tournament, $team);

        //send push notify
        $pushNotificationManager = ServiceLayer::getService('SystemNotificationManager');
        $pushNotificationManager->sendTournamentJoinAdminNotify($tournament, $team);



        $this->getRequest()->redirect($this->getRouter()->link('tournament_accept_invitation_success', array('id' => $id)));


        /*
          $team = new Team();
          $team->setName($crTeam->getName());
          $team->setEmail($crTeam->getEmail());
          $teamData  = $repository->convertToArray($team);
          $teamData['external_id'] = $crTeam->getId();

          $tournamentTeamPlayers  = array();
          $teamPlayers = $teamManager->getActiveTeamPlayers($crTeam->getId());
          foreach($teamPlayers as $teamPlayer)
          {
          $teamPlayer->setId(null);
          $tournamentTeamPlayers[] = $teamPlayerRepository->convertToArray($teamPlayer);
          }

          $teamData['players'] = $tournamentTeamPlayers;
          $tournamentTeams[] = $teamData;
          $tournamentData = $repository->convertToArray($tournament);
          $tournamentData['teams'] = $tournamentTeams;
          $repository->updateTournament($tournamentData);
         * 
         */
    }

    public function invitationAcceptSuccessAction()
    {
        return $this->render('CR\Tournament\TournamentModule:tournament:tournament_invitation_accept.php', array(
        ));
    }

    public function dashboardAction()
    {

        $security = ServiceLayer::getService('security');
        $user = $security->getIdentity()->getUser();
        $repository = ServiceLayer::getService('TournamentRepository');

        $userTournaments = $repository->findUserTournaments($user);
        $adminTournaments = $repository->findAdminTournaments($user);



        $publicTournaments = $repository->findPublicTournaments();
        $tournamentManager = ServiceLayer::getService('TournamentManager');

        $activeTab = (null != $this->getRequest()->get('t')) ? $this->getRequest()->get('t') : 'me';

        return $this->render('CR\Tournament\TournamentModule:tournament:dashboard.php', array(
                    'userTournaments' => $userTournaments,
                    'adminTournaments' => $adminTournaments,
                    'publicTournaments' => $publicTournaments,
                    'tournamentManager' => $tournamentManager,
                    'user' => $user,
                    'activeTab' => $activeTab
        ));


        /*
          if($user->getId() == 5 or $user->getId() == 4 or $user->getId() == 3)
          {
          $list = $repository->findUserTournaments($user);
          return $this->render('CR\Tournament\TournamentModule:tournament:dashboard.php', array(
          'list' => $list
          ));
          }
          else
          {
          return $this->render('CR\Tournament\TournamentModule:tournament:stop.php', array(

          ));
          }
         */
    }

    public function detailAction()
    {
        $security = ServiceLayer::getService('security');
        $router = $user = $security->getIdentity()->getUser();
        $repository = ServiceLayer::getService('TournamentRepository');
        $localityRepo = \Core\ServiceLayer::getService('LocalityRepository');
        $privacyRepo = \Core\ServiceLayer::getService('PlayerPrivacyRepository');
        $request = $this->getRequest();

        $tournament = $repository->findUserTournament($user, $this->getRequest()->get('id'));

        if ($tournament->getUserPermissions() != 'TOURNAMENT_ADMIN')
        {
            $request->redirect($this->getRouter()->link('tournament_public_detail', array('seoid' => $tournament->getSeoId(), 't' => 'teams')));
        }



        //$permissionManager = ServiceLayer::getService('TournamentPermissionManager');
        //$tournamentStats = $repository->getTournamentStats($this->getRequest()->get('id'),$request->get('filter'));

        $teams = $repository->findTournamentTeams($tournament->getId());

        foreach ($teams as $team)
        {
            $locality = $localityRepo->find($team->getLocalityId());
            $team->setLocality($locality);
        }




        if ('POST' == $request->getMethod() && 'tournament_admin_marketing' == $request->get('form_act'))
        {
            $userPrivacy = new \Webteamer\Player\Model\PlayerPrivacy();
            $userPrivacy->setSection('tournament_admin_marketing_agreement');
            $userPrivacy->setUserGroup('coachrufus');
            $userPrivacy->setPlayerId($user->getId());
            $userPrivacy->setValue($request->get('tournament_admin_marketing'));
            $userPrivacy->setData(json_encode(array('tournament_id' => $tournament->getId())));
            $privacyRepo->save($userPrivacy);
            $request->redirect();
        }

        if ('POST' == $request->getMethod() && 'team_edit' == $request->get('form_act'))
        {
            $indexedTeams = array();
            foreach ($teams as $team)
            {
                $indexedTeams[$team->getId()] = $team;
            }


            $data = $request->get('team');
            $notifyManager = ServiceLayer::getService('notification_manager');
            $pushNotificationManager = ServiceLayer::getService('SystemNotificationManager');
            foreach ($data as $teamId => $d)
            {
                //$notifyManager->sendTournamentTeamConfirmed($tournament,$indexedTeams[$teamId]);

                $repository->updateTeamStatus($teamId, $tournament->getId(), $d['status']);


                if ($indexedTeams[$teamId]->getStatus() != $d['status'])
                {
                    //send notification
                    if ($d['status'] == 'confirmed')
                    {
                        $notifyManager->sendTournamentTeamConfirmed($tournament, $indexedTeams[$teamId]);
                        $pushNotificationManager->sendTournamentTeamConfirmedNotify($tournament, $team);
                    }
                    if ($d['status'] == 'decline')
                    {
                        $notifyManager->sendTournamentTeamRejected($tournament, $indexedTeams[$teamId]);
                        $pushNotificationManager->sendTournamentTeamRejectedNotify($tournament, $team);
                    }
                }
            }
            $request->redirect();
        }



        if (!$security->hasTournamentAdminAgreements($security->getIdentity()->getUser(), $tournament))
        {
            $layout = \Core\ServiceLayer::getService('layout');
            $layout->setTemplateParameters(array('body-class' => 'full-overlay-window'));
            return $this->render('CR\Tournament\TournamentModule:tournament:gdpr_admin_marketing.php', array(
            ));
        }


        //$filter
        $filterForm = new \CR\Tournament\Form\StatsFilterForm();
        //$filterForm->setFieldChoices('group', $divisonChoices);

        $filterForm->bindData($request->get('filter'));
        $filterForm->buildDivisionChoices($request->get('filter'), $teams);

        $teamForm = new \CR\Tournament\Form\TeamForm();
        
        
        if('xls' == $request->get('format'))
        {
             $layout = ServiceLayer::getService('layout');
            $layout->setTemplate(null);
            $response =$this->render('CR\Tournament\TournamentModule:teams:list_xls.php', array(
                    'tournament' => $tournament,
                    'teams' => $teams,
                    'user' => $user,
                    'filterForm' => $filterForm,
                    'teamForm' => $teamForm
        ));


            header('Content-Disposition: attachment; filename=export.xls');
             $response->setType('application/octet-stream');
            header('Content-Description: File Transfer');

            header('Content-Transfer-Encoding: binary');
            header('Connection: Keep-Alive');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            return $response;

        }
        

        return $this->render('CR\Tournament\TournamentModule:teams:list.php', array(
                    'tournament' => $tournament,
                    'teams' => $teams,
                    //'stats' => $tournamentStats,
                    //'permissionManager' => $permissionManager,
                    'user' => $user,
                    'filterForm' => $filterForm,
                    'teamForm' => $teamForm
        ));
    }

    private function getForm()
    {
        $security = ServiceLayer::getService('security');
        $translator = ServiceLayer::getService('translator');
        $manager = ServiceLayer::getService('TeamManager');
        $playgroundManager = ServiceLayer::getService('PlaygroundManager');
        $user = $security->getIdentity()->getUser();
        $manager->setTranslator($translator);



        $ageGroupChoices = array('' => '') + $manager->getAgeGroupEnum();

        $form = new \CR\Tournament\Form\TournamentForm();

        $statusChoices = array('' => '') + $manager->getStatusEnum();
        $form->setFieldChoices('status', $statusChoices);

        $sportManager = ServiceLayer::getService('SportManager');

        $sportChoices = $sportManager->getTeamSportFormChoices();


        $sportChoices = array('' => array('' => '')) + $sportChoices;
        $form->setFieldChoices('sport_id', $sportChoices);
        $form->setFieldOption('sport_id', 'label', $translator->translate('Sport'));

        $genderChoices = array('' => '') + $manager->getGenderEnum();
        $form->setFieldChoices('gender', $genderChoices);


        $form->setFieldChoices('age_group', $ageGroupChoices);

        $levelChoices = array('' => '') + $manager->getLevelEnum();
        $form->setFieldChoices('level', $levelChoices);

        $playgrounds = $playgroundManager->getUserPlaygrounds($user);
        $playgroundsChoices = array('' => $translator->translate('Select playground'));
        foreach ($playgrounds as $playground)
        {
            $playgroundsChoices[$playground->getId()] = $playground->getName();
        }

        //city choices
        $form->setFieldChoices('exist_playgrounds', $playgroundsChoices);
        return $form;
    }

    public function createTournamentAction()
    {
        $security = ServiceLayer::getService('security');
        $request = ServiceLayer::getService('request');
        $router = ServiceLayer::getService('router');
        $translator = ServiceLayer::getService('translator');

        $tournamentManager = ServiceLayer::getService('TournamentManager');
        //$repository = ServiceLayer::getService('TournamentRepository');
        //$localityManager = ServiceLayer::getService('LocalityManager');



        $user = $security->getIdentity()->getUser();
        $form = $this->getForm();
        $entity = new \CR\Tournament\Model\Tournament();
        $entity->setAuthorId($security->getIdentity()->getUser()->getId());
        $entity->setCreatedAt(new DateTime());
        $entity->setStatus('public');
        $form->setEntity($entity);


        $playgroundForm = new PlaygroundForm();
        $validator = new \CR\Tournament\Form\TournamentValidator();
        $validator->setRules($form->getFields());
        $playerLocality = ServiceLayer::getService('LocalityRepository')->find($user->getLocalityId());
        $playgroundValidator = new Validator();
        $playgroundData = array();

        if ('POST' == $request->getMethod())
        {
            $postData = $request->get($form->getName());
            $form->bindData($postData);
            $validator->setData($postData);
            $validator->validateData();

            if (!$validator->hasErrors())
            {
                $postData['author_id'] = $security->getIdentity()->getUser()->getId();
                $form->bindData($postData);
                $validator->setData($postData);
                $validator->validateData();
                if (!$validator->hasErrors())
                {
                    $entity = $form->getEntity();

                    $response = $tournamentManager->createTournament($entity);

                    /*
                      $data = $repository->convertToArray($entity);
                      $api = new \Tournament\Api();
                      $response = $api->createTournament(array('tournament' => $data));
                     */

                    $request->redirect($router->link('tournament_teams', array('id' => $response['id'])));
                }
            }
        }

        return $this->render('CR\Tournament\TournamentModule:tournament:create.php', array(
                    'form' => $form,
                    'playgroundForm' => $playgroundForm,
                    'request' => $request,
                    'validator' => $validator,
                    'playgroundValidator' => $playgroundValidator,
                    'playerLocality' => $playerLocality,
        ));
    }

    public function settingsAction()
    {
        $security = ServiceLayer::getService('security');
        $request = ServiceLayer::getService('request');
        $router = ServiceLayer::getService('router');
        $translator = ServiceLayer::getService('translator');

        //$teamManger = ServiceLayer::getService('TeamManager');
        $repository = ServiceLayer::getService('TournamentRepository');
        $localityManager = ServiceLayer::getService('LocalityManager');


        $user = $security->getIdentity()->getUser();
        $tournament = $repository->findUserTournament($user, $this->getRequest()->get('id'));

        $locality = null;
        if (null != $tournament->getLocalityId())
        {
            $locality = $localityManager->getLocalityById($tournament->getLocalityId());
        }


        $form = $this->getForm();
        $form->setEntity($tournament);
        $playgroundForm = new PlaygroundForm();
        $validator = new \CR\Tournament\Form\TournamentValidator();
        $validator->setRules($form->getFields());
        $playerLocality = ServiceLayer::getService('LocalityRepository')->find($user->getLocalityId());
        $playgroundValidator = new Validator();
        $playgroundData = array();

        if ('POST' == $request->getMethod())
        {
            $postData = $request->get($form->getName());
            $form->bindData($postData);
            $validator->setData($postData);
            $validator->validateData();

            if (!$validator->hasErrors())
            {
                $postData['author_id'] = $security->getIdentity()->getUser()->getId();
                $form->bindData($postData);
                $validator->setData($postData);
                $validator->validateData();
                if (!$validator->hasErrors())
                {
                    $entity = $form->getEntity();

                    //save locality
                    if (null != $request->get('locality_json_data'))
                    {
                        $locality = $localityManager->createObjectFromJsonData($request->get('locality_json_data'));
                        if (null != $request->get('locality_name'))
                        {
                            $locality->setName($request->get('locality_name'));
                        }
                        $locality->setAuthorId($entity->getAuthorId());
                        $localityId = $localityManager->saveLocality($locality);
                        $entity->setLocalityId($localityId);
                    }
                    if (null == $request->get('locality_name'))
                    {
                        $entity->setLocalityId(null);
                    }

                    $repository->save($entity);
                    /*
                      $data = $repository->convertToArray($entity);
                      $api = new \Tournament\Api();
                      $response = $api->updateTournament($data);
                     * 
                     */
                    $request->redirect();

                    $request->addFlashMessage('tournament_update_success', $translator->translate('CUPDATE_ENTITY_SUCCESS'));
                }
            }
        }

        return $this->render('CR\Tournament\TournamentModule:tournament:settings.php', array(
                    'form' => $form,
                    'playgroundForm' => $playgroundForm,
                    'request' => $request,
                    'validator' => $validator,
                    'playgroundValidator' => $playgroundValidator,
                    'playerLocality' => $playerLocality,
                    'tournament' => $tournament,
                    'locality' => $locality,
        ));
    }

    public function adminEventsAction()
    {
        $security = ServiceLayer::getService('security');
        $user = $security->getIdentity()->getUser();
        $tournamentRepository = ServiceLayer::getService('TournamentRepository');
        $statManager = ServiceLayer::getService('PlayerStatManager');
        $repository = ServiceLayer::getService('EventRepository');
        $teamEventManager = ServiceLayer::getService('TeamEventManager');
        $events = $repository->findBy(array('tournament_id' => $this->getRequest()->get('id')));


        $tournament = $tournamentRepository->findUserTournament($user, $this->getRequest()->get('id'));

        if ($tournament->getUserPermissions($user->getId()) != 'TOURNAMENT_ADMIN')
        {
            throw new \AclException();
        }


        //lineups
        $groupedEvents = array();
        foreach ($events as $event)
        {
            $event->setCurrentDate($event->getStart());

            $lineup = $teamEventManager->getEventLineup($event);
            $event->setLineup($lineup);


            $matchOverview = $statManager->getMatchOverview($lineup);

            $matchResult = array('first_line' => '-', 'second_line' => '-');
            if ('closed' == $lineup->getStatus() and null != $matchOverview)
            {
                $matchResult['first_line'] = ($matchOverview->getFirstLineGoals() == null) ? '0' : $matchOverview->getFirstLineGoals();
                $matchResult['second_line'] = ($matchOverview->getSecondLineGoals() == null) ? '0' : $matchOverview->getSecondLineGoals();
            }

            $event->setMatchResult($matchResult['first_line'] . ':' . $matchResult['second_line']);

            $groupedEvents[$event->getTournamentGroup()][] = $event;
        }


        //lineups
        /*
          $eventsLineups = array();
          foreach($events as $event)
          {
          $event->setCurrentDate($event->getStart());
          $lineup = $teamEventManager->getEventLineup($event);
          if($lineup->getStatus() == 'paused' or $lineup->getStatus() == 'running')
          {
          $lineupData = $repository->convertToArray($lineup);
          $lineupData['match_result'] = $this->getMatchResuls($lineup);
          $lineupData['match_time'] = $this->getMatchTime($lineup);
          $eventsLineups[$event->getId()] = $lineupData;
          $list[] = $repository->convertToArray($event);
          }
          }

          //teams
          $teams = $tournamentRepository->findTournamentTeams($this->getRequest()->get('tid'), array('status' => 'confirmed'));
          $teamIcons = array();
          foreach($teams as $team)
          {
          $teamIcons[$team->getId()]  = $team->getMidPhoto();
          }
          $teamList = $repository->createArrayList($teams);
          foreach($teamList as $key => $l)
          {
          $teamList[$l['id']] = $l;
          $teamList[$l['id']]['icon'] = $teamIcons[$l['id']];
          }

          foreach ($list as $key => $event)
          {
          $list[$key]['lineup'] = $eventsLineups[$event['id']];
          $list[$key]['team'] = $teamList[$event['team_id']];
          $list[$key]['opponent_team'] = $teamList[$event['opponent_team_id']];
          }
         * 
         */

        return $this->render('CR\Tournament\TournamentModule:admin:events.php', array(
                    'groupedEvents' => $groupedEvents,
                    'user' => $user,
                    'tournament' => $tournament
        ));
    }

    public function adminEventsTeamsAction()
    {
        $request = $this->getRequest();
        $security = ServiceLayer::getService('security');
        $user = $security->getIdentity()->getUser();
        $tournamentRepository = ServiceLayer::getService('TournamentRepository');
        $teamMatchLineupRepository = ServiceLayer::getService('TeamMatchLineupRepository');
        $repository = ServiceLayer::getService('EventRepository');
        $teamEventManager = ServiceLayer::getService('TeamEventManager');
        $teamManager = ServiceLayer::getService('TeamManager');
        $event = $repository->find($this->getRequest()->get('id'));
        $event->setCurrentDate($event->getStart());
        $lineup = $teamEventManager->getEventLineup($event);
        $event->setLineup($lineup);


        $tournament = $tournamentRepository->findUserTournament($user, $event->getTournamentId());
        $teams = $tournamentRepository->findTournamentTeams($tournament->getId(), array('status' => 'confirmed'));
        
        $indexedTeams = array();
        foreach($teams as $team)
        {
            $indexedTeams[$team->getId()] = $team;
        }


        if ($tournament->getUserPermissions($user->getId()) != 'TOURNAMENT_ADMIN')
        {
            throw new \AclException();
        }
        
        
        if('POST' == $request->getMethod())
        {
            $team1  = $request->get('team1');
            $team2  = $request->get('team2');
            
            $event->setTeamId($team1);
            $event->setOpponentTeamId($team2);
            $event->setName($indexedTeams[$team1]->getName().':'.$indexedTeams[$team2]->getName());
            $repository->save($event);
            
            $lineup->setTeamId($team1);
            $lineup->setFirstLineName($indexedTeams[$team1]->getName());
            $lineup->setSecondLineName($indexedTeams[$team2]->getName());
            $lineup->setOpponentTeamId($team2);
            $teamEventManager->saveLineup($lineup);
            
            //remove old players
            $teamMatchLineupRepository->getPlayerRepository()->deleteBy(array('lineup_id' => $lineup->getId()));
            
            //add new players
            $eventDate = DateTime::createFromFormat('Y-m-d H:i:s', $event->getStart()->format('Y-m-d').' 00:00:00');
            $team1Players = $teamManager->getActiveTeamPlayers($team1);
            foreach($team1Players as $team1Player)
            {
                $lineupPlayer = new \Webteamer\Team\Model\TeamMatchLineupPlayer();
                $lineupPlayer->setLineupId($lineup->getId());
                $lineupPlayer->setEventId($event->getId());
                $lineupPlayer->setCreatedAt(new DateTime());
                $lineupPlayer->setPlayerId($team1Player->getPlayerId());
                $lineupPlayer->setPlayerName($team1Player->getFullName());
                $lineupPlayer->setEventDate($eventDate);
                $lineupPlayer->setLineupPosition('first_line');
                $lineupPlayer->setLineupName($lineup->getFirstLineName());
                $lineupPlayer->setTeamPlayerId($team1Player->getId());
                $lineupPlayer->setPlayerPosition('PLAYER');
                
                $teamMatchLineupRepository->getPlayerRepository()->save($lineupPlayer);
            }
            
            $team2Players = $teamManager->getActiveTeamPlayers($team2);
            foreach($team2Players as $team2Player)
            {
                $lineupPlayer = new \Webteamer\Team\Model\TeamMatchLineupPlayer();
                $lineupPlayer->setLineupId($lineup->getId());
                $lineupPlayer->setEventId($event->getId());
                $lineupPlayer->setCreatedAt(new DateTime());
                $lineupPlayer->setPlayerId($team2Player->getPlayerId());
                $lineupPlayer->setPlayerName($team2Player->getFullName());
                $lineupPlayer->setEventDate($eventDate);
                $lineupPlayer->setLineupPosition('second_line');
                $lineupPlayer->setLineupName($lineup->getSecondLineName());
                $lineupPlayer->setTeamPlayerId($team2Player->getId());
                $lineupPlayer->setPlayerPosition('PLAYER');
                $teamMatchLineupRepository->getPlayerRepository()->save($lineupPlayer);
            }
            
            
            $request->redirect($this->getRouter()->link('tournament_admin_events',array('id' => $event->getTournamentId())));
        }
        

        return $this->render('CR\Tournament\TournamentModule:admin:events_teams.php', array(
                    'tournament' => $tournament,
                    'teams' => $teams,
                    'event' => $event,
                    'lineup' => $lineup
        ));
    }

}

?>