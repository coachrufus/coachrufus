<?php

$acl->allowRole(array('ROLE_USER'),'tournament_event_detail');
$router->addRoute('tournament_event_detail','/tournament/event/detail/:id',array(
		'controller' => 'CR\Tournament\TournamentModule:Event:detail'
));

$acl->allowRole(array('ROLE_USER'),'tournament_event_edit');
$router->addRoute('tournament_event_edit','/tournament/event/edit/:id',array(
		'controller' => 'CR\Tournament\TournamentModule:Event:edit'
));

$acl->allowRole(array('ROLE_USER'),'tournament_event_nomination');
$router->addRoute('tournament_event_nomination','/tournament/event/nomination/:id',array(
		'controller' => 'CR\Tournament\TournamentModule:Event:nomination'
));

$acl->allowRole(array('ROLE_USER'),'tournament_event_change_nomination');
$router->addRoute('tournament_event_change_nomination','/tournament/event/nomination/change/:id',array(
		'controller' => 'CR\Tournament\TournamentModule:Event:changeNomination'
));

$acl->allowRole(array('ROLE_USER'),'tournament_event_score');
$router->addRoute('tournament_event_score','/tournament/event/score/:id',array(
		'controller' => 'CR\Tournament\TournamentModule:Event:score'
));

$acl->allowRole(array('ROLE_USER'),'tournament_event_simple_score');
$router->addRoute('tournament_event_simple_score','/tournament/event/simple-score/:id',array(
		'controller' => 'CR\Tournament\TournamentModule:Event:simpleScore'
));

$acl->allowRole(array('ROLE_USER'),'tournament_event_reset_match');
$router->addRoute('tournament_event_reset_match','/tournament/event/reset-match/:id',array(
		'controller' => 'CR\Tournament\TournamentModule:Event:resetMatch'
));

$acl->allowRole(array('ROLE_USER'),'tournament_event_score_result');
$router->addRoute('tournament_event_score_result','/tournament/event/score-result/:id',array(
		'controller' => 'CR\Tournament\TournamentModule:Event:scoreResult'
));

$acl->allowRole(array('ROLE_USER'),'tournament_event_add_timeline_stat');
$router->addRoute('tournament_event_add_timeline_stat','/tournament/event/add-hit',array(
		'controller' => 'CR\Tournament\TournamentModule:Event:addHit'
));
$acl->allowRole(array('ROLE_USER'),'tournament_event_edit_timeline_stat');
$router->addRoute('tournament_event_edit_timeline_stat','/tournament/event/edit-hit',array(
		'controller' => 'CR\Tournament\TournamentModule:Event:editHit'
));

$acl->allowRole(array('ROLE_USER'),'tournament_event_add_timeline_mmatch_phase');
$router->addRoute('tournament_event_add_timeline_mmatch_phase','/tournament/event/add-match-phase',array(
		'controller' => 'CR\Tournament\TournamentModule:Event:addMatchPhase'
));

$acl->allowRole(array('ROLE_USER'),'tournament_event_add_timeline_mom');
$router->addRoute('tournament_event_add_timeline_mom','/tournament/event/add-mom',array(
		'controller' => 'CR\Tournament\TournamentModule:Event:addMom'
));

$acl->allowRole(array('ROLE_USER'),'tournament_event_close');
$router->addRoute('tournament_event_close','/tournament/event/close',array(
		'controller' => 'CR\Tournament\TournamentModule:Event:close'
));

$acl->allowRole(array('ROLE_USER'),'tournament_match_print_lineup');
$router->addRoute('tournament_match_print_lineup','/tournament/event/print-lineup',array(
		'controller' => 'CR\Tournament\TournamentModule:Event:nominationPrint'
));

$acl->allowRole(array('ROLE_USER'),'tournament_event_add_media');
$router->addRoute('tournament_event_add_media','/tournament/event/addMedia',array(
		'controller' => 'CR\Tournament\TournamentModule:Event:addMedia'
));
