<?php
namespace CR\Tournament;

$acl->allowRole(array('ROLE_USER'),'tournament_start');
$router->addRoute('tournament_start','/tournament/start',array(
		'controller' => 'CR\Tournament\TournamentModule:Tournament:start'
));

$acl->allowRole(array('ROLE_USER'),'tournament');
$router->addRoute('tournament','/tournament',array(
		'controller' => 'CR\Tournament\TournamentModule:Tournament:start'
));

$acl->allowRole(array('ROLE_USER'),'tournament_dashboard');
$router->addRoute('tournament_dashboard','/tournament/dashboard',array(
		'controller' => 'CR\Tournament\TournamentModule:Tournament:dashboard'
));

$acl->allowRole(array('ROLE_USER'),'tournament_create');
$router->addRoute('tournament_create','/tournament/create',array(
		'controller' => 'CR\Tournament\TournamentModule:Tournament:createTournament'
));

$acl->allowRole(array('ROLE_USER'),'tournament_settings');
$router->addRoute('tournament_settings','/tournament/settings/:id',array(
		'controller' => 'CR\Tournament\TournamentModule:Tournament:settings'
));

$acl->allowRole(array('ROLE_USER'),'tournament_admin_events');
$router->addRoute('tournament_admin_events','/tournament/admin/events/:id',array(
		'controller' => 'CR\Tournament\TournamentModule:Tournament:adminEvents'
));

$acl->allowRole(array('ROLE_USER'),'tournament_admin_events_teams');
$router->addRoute('tournament_admin_events_teams','/tournament/admin/events/:id/teams',array(
		'controller' => 'CR\Tournament\TournamentModule:Tournament:adminEventsTeams'
));

$acl->allowRole(array('ROLE_USER'),'tournament_share_link');
$router->addRoute('tournament_share_link','/tournament/invitation/:hash',array(
		'controller' => 'CR\Tournament\TournamentModule:Tournament:invitation'
));

$acl->allowRole(array('ROLE_USER'),'tournament_accept_invitation');
$router->addRoute('tournament_accept_invitation','/tournament/invitation/:hash/accept',array(
		'controller' => 'CR\Tournament\TournamentModule:Tournament:invitationAccept'
));

$acl->allowRole(array('ROLE_USER'),'tournament_accept_invitation_success');
$router->addRoute('tournament_accept_invitation_success','/tournament/invitation-success',array(
		'controller' => 'CR\Tournament\TournamentModule:Tournament:invitationAcceptSuccess'
));



$acl->allowRole(array('ROLE_USER'),'tournament_schedule');
$router->addRoute('tournament_schedule','/tournament/:id/schedule',array(
		'controller' => 'CR\Tournament\TournamentModule:Schedule:index'
));

$acl->allowRole(array('ROLE_USER'),'tournament_schedule_create');
$router->addRoute('tournament_schedule_create','/tournament/:id/schedule/create',array(
		'controller' => 'CR\Tournament\TournamentModule:Schedule:create'
));

$acl->allowRole(array('ROLE_USER'),'tournament_schedule_delete');
$router->addRoute('tournament_schedule_delete','/tournament/:id/schedule/delete',array(
		'controller' => 'CR\Tournament\TournamentModule:Schedule:delete'
));



$acl->allowRole(array('ROLE_USER'),'tournament_schedule_create_one_group');
$router->addRoute('tournament_schedule_create_one_group','/tournament/:id/schedule/create-one-group',array(
		'controller' => 'CR\Tournament\TournamentModule:Schedule:createOneGroup'
));

$acl->allowRole(array('ROLE_USER'),'tournament_schedule_create_multi_group');
$router->addRoute('tournament_schedule_create_multi_group','/tournament/:id/schedule/create-multi-group',array(
		'controller' => 'CR\Tournament\TournamentModule:Schedule:createMultiGroup'
));

$acl->allowRole(array('ROLE_USER'),'tournament_schedule_create_conference');
$router->addRoute('tournament_schedule_create_conference','/tournament/:id/schedule/create-conference',array(
		'controller' => 'CR\Tournament\TournamentModule:Schedule:createConference'
));

$acl->allowRole(array('ROLE_USER'),'tournament_schedule_create_playoff');
$router->addRoute('tournament_schedule_create_playoff','/tournament/:id/schedule/create-playoff',array(
		'controller' => 'CR\Tournament\TournamentModule:Schedule:createPlayoff'
));


$acl->allowRole(array('ROLE_USER'),'tournament_schema_import');
$router->addRoute('tournament_schema_import','/tournament/schema-import',array(
		'controller' => 'CR\Tournament\TournamentModule:Tool:importSchema'
));

include_once('_event_routing.php');
include_once('_team_routing.php');
include_once('_public_routing.php');


$acl->allowRole(array('ROLE_USER'),'tournament_stats');
$router->addRoute('tournament_stats','/tournament/stats',array(
		'controller' => 'CR\Tournament\TournamentModule:Stats:teamsOverview'
));

$acl->allowRole(array('ROLE_USER'),'tournament_players_stats');
$router->addRoute('tournament_players_stats','/tournament/player-stats',array(
		'controller' => 'CR\Tournament\TournamentModule:Stats:playersOverview'
));

$acl->allowRole(array('ROLE_USER'),'tournament_detail');
$router->addRoute('tournament_detail','/tournament/detail/:id',array(
		'controller' => 'CR\Tournament\TournamentModule:Tournament:detail'
));

/*
$acl->allowRole(array('ROLE_USER'),'tournament_join_team');
$router->addRoute('tournament_join_team','/tournament/join-team/:id',array(
		'controller' => 'CR\Tournament\TournamentModule:Tournament:joinTeam'
));
*/


/*
$acl->allowRole(array('ROLE_USER'),'tournament_event_nomination_create_manual');
$router->addRoute('tournament_event_nomination_create_manual','/tournament/event/nomination/create/:id',array(
		'controller' => 'CR\Tournament\TournamentModule:Event:nomination'
));
*/

/*
$acl->allowRole(array('ROLE_USER'),'tournament_match');
$router->addRoute('tournament_match','/tournament/:id/match',array(
		'controller' => 'CR\Tournament\TournamentModule:Schedule:index'
));
 * *
 */