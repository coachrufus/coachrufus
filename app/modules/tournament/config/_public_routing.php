<?php

$acl->allowRole(array('guest'),'tournament_public_schedule');
$router->addRoute('tournament_public_schedule','/tournament/public/schedule/:id',array(
		'controller' => 'CR\Tournament\TournamentModule:Public:schedule'
));


$acl->allowRole(array('guest'),'tournament_public_detail');
$router->addRoute('tournament_public_detail','/tournament/:seoid',array(
		'controller' => 'CR\Tournament\TournamentModule:Public:detailReact'
));

$acl->allowRole(array('guest'),'tournament_public_live_detail');
$router->addRoute('tournament_public_live_detail','/tournament/:seoid/live/:lineupId',array(
		'controller' => 'CR\Tournament\TournamentModule:Public:detailReact'
));

$acl->allowRole(array('guest'),'tournament_public_top');
$router->addRoute('tournament_public_top','/tournament/:seoid/top',array(
		'controller' => 'CR\Tournament\TournamentModule:Public:top'
));


$acl->allowRole(array('guest'),'tournament_public_detail2');
$router->addRoute('tournament_public_detail2','/tournament/:seoid',array(
		'controller' => 'CR\Tournament\TournamentModule:Public:detailReact'
));


