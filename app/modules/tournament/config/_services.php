<?php
namespace CR\Tournament;
use Core\ServiceLayer;


ServiceLayer::addService('TournamentRepository',array(
'class' => "\CR\Tournament\Model\TournamentRepository",
    'params' => array(
		new \CR\Tournament\Model\TournamentStorage('tournament'),
		new \Core\EntityMapper('CR\Tournament\Model\Tournament')
	)
	
));

ServiceLayer::addService('TournamentApiRepository',array(
'class' => "\CR\Tournament\Model\TournamentApiRepository",
    'params' => array(
		new \Tournament\Api(),
                new \Core\EntityMapper('CR\Tournament\Model\Tournament')
	)
	
));


ServiceLayer::addService('TournamentTeamRepository',array(
'class' => "CR\Tournament\Model\TeamRepository",
    'params' => array(
		//new \CR\Tournament\Model\TournamentTeamStorage('tournament_teams'),
		//new \Core\EntityMapper('CR\Tournament\Model\Team')
	)
));



ServiceLayer::addService('TournamentPermissionManager',array(
'class' => "\CR\Tournament\Manager\TournamentPermissionManager",
));

ServiceLayer::addService('TournamentEnumManager',array(
'class' => "\CR\Tournament\Manager\TournamentEnumManager",
));


ServiceLayer::addService('TournamentTeamManager',array(
'class' => "\CR\Tournament\Manager\TournamentTeamManager",
    'params' => array(
		ServiceLayer::getService('TournamentTeamRepository'),
	)
));


ServiceLayer::addService('TournamentManager',array(
'class' => "\CR\Tournament\Manager\TournamentManager",
    'params' => array(
		ServiceLayer::getService('TournamentRepository'),
		//ServiceLayer::getService('TournamentTeamManager'),
	)
));

;