<?php

$acl->allowRole(array('ROLE_USER'),'tournament_teams');
$router->addRoute('tournament_teams','/tournament/:id/teams',array(
		'controller' => 'CR\Tournament\TournamentModule:Teams:index'
));

$acl->allowRole(array('ROLE_USER'),'tournament_teams_overview');
$router->addRoute('tournament_teams_overview','/tournament/:id/teams-overview',array(
		'controller' => 'CR\Tournament\TournamentModule:Teams:overview'
));

$acl->allowRole(array('ROLE_USER'),'tournament_team_create');
$router->addRoute('tournament_team_create','/tournament/:id/team/create',array(
		'controller' => 'CR\Tournament\TournamentModule:Teams:create'
));

$acl->allowRole(array('ROLE_USER'),'tournament_team_players');
$router->addRoute('tournament_team_players','/tournament/:id/team-players/:team_id',array(
		'controller' => 'CR\Tournament\TournamentModule:Teams:players'
));


$acl->allowRole(array('ROLE_USER'),'tournament_team_detail');
$router->addRoute('tournament_team_detail','/tournament/:id/team-detail/:team_id',array(
		'controller' => 'CR\Tournament\TournamentModule:Teams:detail'
));



$acl->allowRole(array('ROLE_USER'),'tournament_team_photo');
$router->addRoute('tournament_team_photo','/tournament/team/change-photo',array(
		'controller' => 'CR\Tournament\TournamentModule:Teams:changePhoto'
));

$acl->allowRole(array('ROLE_USER'),'tournament_team_remove_photo');
$router->addRoute('tournament_team_remove_photo','/tournament/team/remove-photo/:team_id',array(
		'controller' => 'CR\Tournament\TournamentModule:Teams:removePhoto'
));




$acl->allowRole(array('ROLE_USER'),'tournament_team_add_player');
$router->addRoute('tournament_team_add_player','/tournament/:id/team-player-add',array(
		'controller' => 'CR\Tournament\TournamentModule:Teams:createPlayer'
));
$acl->allowRole(array('ROLE_USER'),'tournament_team_edit_player');
$router->addRoute('tournament_team_edit_player','/tournament/:id/team-player-edit',array(
		'controller' => 'CR\Tournament\TournamentModule:Teams:editPlayer'
));

$acl->allowRole(array('ROLE_USER'),'tournament_team_delete_player');
$router->addRoute('tournament_team_delete_player','/tournament/:id/team-player-delete',array(
		'controller' => 'CR\Tournament\TournamentModule:Teams:deletePlayer'
));

$acl->allowRole(array('ROLE_USER'),'tournament_team_invite_admin');
$router->addRoute('tournament_team_invite_admin','/tournament/team-invite-admin',array(
		'controller' => 'CR\Tournament\TournamentModule:Teams:inviteAdmin'
));

$acl->allowRole(array('guest'),'tournament_team_invite_response');
$router->addRoute('tournament_team_invite_response','/tournament/team-invite-admin-response',array(
		'controller' => 'CR\Tournament\TournamentModule:Teams:inviteAdminResponse'
));

$acl->allowRole(array('ROLE_USER'),'tournament_team_bulk_invite_admin');
$router->addRoute('tournament_team_bulk_invite_admin','/tournament/team-bulk-invite-admin',array(
		'controller' => 'CR\Tournament\TournamentModule:Teams:sendBulkInvitation'
));