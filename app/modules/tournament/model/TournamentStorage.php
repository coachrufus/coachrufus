<?php

namespace CR\Tournament\Model;

use Core\DbStorage;

class TournamentStorage extends DbStorage {

    public function findUserTournaments($userId)
    {
        $data = \Core\DbQuery::prepare('
                 SELECT tt.`status` as team_status,  t.* FROM tournament_team tt
                    LEFT JOIN tournament t ON tt.tournament_id = t.id
                    LEFT JOIN team_players tp ON tt.team_id = tp.team_id
                    WHERE tp.player_id = :user_id order by t.priority desc, t.id desc')
                ->bindParam('user_id', $userId)
                ->execute()
                ->fetchAll(\PDO::FETCH_ASSOC);
        return $data;
    }
    
    public function getTournamentTeamsPoints($tournamentId)
    {
        $data = \Core\DbQuery::prepare('
                 SELECT pe.hit_type, pe.team_id, pe.lineup_id,tml.team_id as "home_team_id", tml.opponent_team_id as "opponent_team_id", te.tournament_group, tml.overtime    from team_event te 
                LEFT JOIN team_match_lineup tml ON te.id = tml.event_id
                LEFT JOIN player_timeline_event pe ON tml.id =  pe.lineup_id
                where te.tournament_id = :tid
                and tml.status = "closed"')
                ->bindParam('tid', $tournamentId)
                ->execute()
                ->fetchAll(\PDO::FETCH_ASSOC);
        return $data;
    }
    public function getTournamentPlayersPoints($tournamentId)
    {
        $data = \Core\DbQuery::prepare('
                 SELECT lp.player_name, hit_type, count(hit_type) AS count, lp.team_player_id, player_timeline_event.event_date
                   FROM player_timeline_event   
                   LEFT JOIN team_match_lineup_player lp ON player_timeline_event.lineup_player_id = lp.id
                   LEFT JOIN team_match_lineup lip ON lp.lineup_id = lip.id
                   LEFT JOIN team_event e ON lp.event_id = e.id
                   where ( lip.team_id in (select team_id from tournament_team where tournament_id = :tid) or lip.opponent_team_id in (select team_id from tournament_team where tournament_id = :tid) )
                   and e.tournament_id = :tid
                   GROUP BY hit_type, lp.team_player_id')
                ->bindParam('tid', $tournamentId)
                ->execute()
                ->fetchAll(\PDO::FETCH_ASSOC);
        return $data;
    }
    
    public function findAdminTournaments($userId)
    {
        $data = \Core\DbQuery::prepare('
                 SELECT tt.`status` as team_status, tpp.permission_type,  t.* FROM  tournament t
                    LEFT JOIN tournament_team tt  ON  t.id = tt.tournament_id
                    LEFT JOIN tournament_permission tpp ON t.id = tpp.tournament_id and tpp.permission_type = "TOURNAMENT_ADMIN"
                    WHERE tpp.user_id = :user_id group by t.id order by t.priority desc, t.id desc')
                ->bindParam('user_id', $userId)
                ->execute()
                ->fetchAll(\PDO::FETCH_ASSOC);
        return $data;
    }
    
    public function getTournamentTeams($tournamentId,$filterData = null)
    {
        $filterCriteria = '';
        $bindParams = array();
        if(null != $filterData)
        {
            if(array_key_exists('status',$filterData))
            {
                $filterCriteria = ' AND tt.status = :status';
                $bindParams['status'] = $filterData['status'];
            }
        }
        
        
        $query = \Core\DbQuery::prepare('
                 SELECT t.*, tt.status, u.email as creator_email from tournament_team tt
                 LEFT JOIN team t ON tt.team_id = t.id
                 LEFT JOIN co_users u ON tt.created_by = u.id
                 WHERE tt.tournament_id = :tournament_id '.$filterCriteria.' group by t.id order by tt.id desc')
                ->bindParam('tournament_id', $tournamentId);
        foreach($bindParams as $bindIndex => $bindValue)
        {
            $query->bindParam($bindIndex, $bindValue);
        }
        
        
       $data = $query->execute()
                ->fetchAll(\PDO::FETCH_ASSOC);
        return $data;
    }
    
    public function findPublicTournaments()
    {
         $data = \Core\DbQuery::prepare('
                 SELECT * FROM tournament t WHERE t.status = "public" order by priority desc, id desc')
                ->execute()
                ->fetchAll(\PDO::FETCH_ASSOC);
        return $data;
    }
    
    public function findTournamentPermissions($tournamentId)
    {
          $data = \Core\DbQuery::prepare('
                 SELECT user_id, team_id, tournament_id, permission_type, status from tournament_permission where tournament_id = :tournament_id')
                ->bindParam('tournament_id', $tournamentId)
                ->execute()
                ->fetchAll(\PDO::FETCH_ASSOC);
        return $data;
    }
    
    public function findTournamentMetadata($tournamentId)
    {
          $data = \Core\DbQuery::prepare('
                 SELECT COUNT(tt.team_id) as players from tournament_team tt 
                    LEFT JOIN team_players tp ON tt.team_id = tp.team_id
                    WHERE tt.tournament_id = :tournament_id
                    GROUP BY tt.team_id')
                ->bindParam('tournament_id', $tournamentId)
                ->execute()
                ->fetchAll(\PDO::FETCH_ASSOC);
        return $data;
    }
    
    public function findTournamentMatchMetadata($tournamentId)
    {
          $data = \Core\DbQuery::prepare('
                 select l.id, l.status from team_event te LEFT JOIN team_match_lineup l ON te.id = l.event_id where te.tournament_id = :tournament_id')
                ->bindParam('tournament_id', $tournamentId)
                ->execute()
                ->fetchAll(\PDO::FETCH_ASSOC);
        return $data;
    }
    
    public function updateTeamStatus($teamId,$tournamentId,$status)
    {
        \Core\DbQuery::prepare('UPDATE tournament_team  set status=:status where team_id = :team_id and tournament_id = :tournament_id  ')
		->bindParam('status', $status)
		->bindParam('team_id', $teamId)
		->bindParam('tournament_id', $tournamentId)
		->execute(); 
    }
    
    public function getTournamentHits($tournamentId)
    {
        $data = \Core\DbQuery::prepare('select tp.player_name, pte.hit_group, t.name as team_name from player_timeline_event pte
                            LEFT JOIN team_event te ON pte.event_id = te.id 
                            LEFT JOIN team_match_lineup_player tp ON pte.lineup_player_id = tp.id
                            LEFT JOIN team t on pte.team_id = t.id
                            WHERE te.tournament_id = :tournament_id and pte.hit_type = "goal"')
		->bindParam('tournament_id', $tournamentId)
		->execute()
                 ->fetchAll(\PDO::FETCH_ASSOC);
        return $data;
    }
        

}
