<?php

namespace CR\Tournament\Model;

use Core\Repository as Repository;
use Core\ServiceLayer;

class TournamentApiRepository extends Repository {

    public function getTeamMapper()
    {
        return new \Core\EntityMapper('CR\Tournament\Model\Team');
    }
    
    public function getTeamPlayerMapper()
    {
        return new \Core\EntityMapper('CR\Tournament\Model\TeamPlayer');
    }

    public function getScheduleMapper()
    {
        return new \Core\EntityMapper('CR\Tournament\Model\TournamentSchedule');
    }
    
   
    public function getEventMapper()
    {
        return new \Core\EntityMapper('CR\Tournament\Model\TournamentEvent');
    }
    
      public function getEventMatchMapper()
    {
        return new \Core\EntityMapper('CR\Tournament\Model\EventMatch');
    }
    
    public function getNominationMapper()
    {
        return new \Core\EntityMapper('CR\Tournament\Model\EventNomination');
    }
    
    public function getEventMatchHitMapper()
    {
        return new \Core\EntityMapper('CR\Tournament\Model\EventMatchHit');
    }

   public function getUserPermission($user)
   {
       return $this->getStorage()->getUserPermission(array('user_id' => $user->getId()));
   }
    
    
    public function findUserTournaments($user)
    {
        $data = $this->getStorage()->getList($user->getId());
        $list = array();
        foreach($data as $tournament)
        {
            $object = $this->factory->createEntityFromArray($tournament);
             $list[] = $object;
        }
        return $list;
    }
    
     public function findUserTournament($user, $tournamentId)
    {
        $data = $this->getStorage()->getTournament($tournamentId);
        $object = $this->createObjectFromArray($data);
        return $object;
    }

    public function findTournament($tournamentId)
    {
        $data = $this->getStorage()->getTournament($tournamentId);
        $object = $this->createObjectFromArray($data);
        return $object;
    }

    public function findTournamentTeams($tournamentId,$filterData = null)
    {
        $data = $this->getStorage()->getTournamentTeams($tournamentId,$filterData);
        $mapper = $this->getTeamMapper();
        $list = array();
        foreach ($data as $d)
        {
            $object = $mapper->createEntityFromArray($d);
            $list[] = $object;
        }
        return $list;
    }
    
    public function findTournamentTeam($teamId)
    {
        $data = $this->getStorage()->getTournamentTeam($teamId);
        $mapper = $this->getTeamMapper();
        $object = $mapper->createEntityFromArray($data);
        return $object;
    }
    
      public function findUserTeams($user)
    {
        $data = $this->getStorage()->getUserTeams(array('user_id' => $user->getId()));
        $list = array();
        foreach($data as $tournament)
        {
            $object = $this->factory->createEntityFromArray($tournament);
             $list[] = $object;
        }
        return $list;
    }
    
    public function findTeamPlayers($teamId)
    {
        $data = $this->getStorage()->getTeamPlayers($teamId);
        $mapper = $this->getTeamPlayerMapper();
        $list = array();
        foreach ($data as $d)
        {
            $object = $mapper->createEntityFromArray($d);
            $list[] = $object;
        }
        return $list;
    }
    
    public function createTournamentTeamPlayers($data)
    {
         $this->getStorage()->createTournamentTeamPlayers($data);
    }
    
    public function updateTournamentTeamPlayer($data)
    {
         $this->getStorage()->updateTournamentTeamPlayer($data);
    }
    
    public function deleteTournamentTeamPlayer($data)
    {
         $this->getStorage()->deleteTournamentTeamPlayer($data);
    }

    public function updateTournament($tournament)
    {
        $this->getStorage()->updateTournament($tournament);
    }
    
     public function updateTournamentTeam($team)
    {
        $this->getStorage()->updateTournamentTeam($team);
    }

    public function createTournamentSchedule($tournament, $data)
    {
        $this->getStorage()->createTournamentSchedule($tournament->getId(), $data);
    }
    
    public function deleteTournamentSchedule($data)
    {
        $this->getStorage()->deleteTournamentSchedule($data);
    }
    
    public function createTournamentMatch($event)
    {
        $this->getStorage()->createTournamentMatch($event->getId());
    }
    
    public function changeEventNomination($data)
    {
        return $this->getStorage()->changeEventNomination($data);
    }

    public function getTournamentSchedules($tournamentId,$filter = array())
    {
        $data = $this->getStorage()->getTournamentSchedules($tournamentId,$filter);
          $mapper = $this->getScheduleMapper();
        $list = array();
        foreach ($data as $d)
        {
            $object = $mapper->createEntityFromArray($d);
            $list[$object->getId()] = $object;
        }

        return $list;
    }
    
    public function updateTournamentEvent($data)
    {
         $this->getStorage()->updateTournamentEvent($data);
    }

    
    public function getTournamentEvents($tournamentId,$filter = array())
    {
        $data = $this->getStorage()->getTournamentEvents($tournamentId,$filter);
        $mapper = $this->getEventMapper();
        $matchMapper = $this->getEventMatchMapper();
        $list = array();
        foreach ($data as $d)
        {
            $object = $mapper->createEntityFromArray($d);
            $match = $matchMapper->createEntityFromArray($d['match']);
            $match->setMatchResult($d['match']['result']);
            $object->setMatch($match);
            $object->setScheduleName($d['schedule_name']);
            $list[] = $object;
        }
        return $list;
    }
    
    public function getTournamentEvent($id)
    {
        $data = $this->getStorage()->getTournamentEvent($id);
        $mapper = $this->getEventMapper();
        $object = $mapper->createEntityFromArray($data);
        
        //teams
        $teamMapper =  $this->getTeamMapper();
        $teamPlayerMapper =  $this->getTeamPlayerMapper();
        $nominationMapper =  $this->getNominationMapper();
        $teams = $data['teams'];
        $team1 =  $teamMapper->createEntityFromArray($teams['team1']);
        $team2 =  $teamMapper->createEntityFromArray($teams['team2']);
        
        //players
        $team1Players = array();
        foreach($data['teams']['team1']['players'] as $player)
        {
            $team1Players[] = $teamPlayerMapper->createEntityFromArray($player);
        }
        $team1->setPlayers($team1Players);
        
        $team2Players = array();
        foreach($data['teams']['team2']['players'] as $player)
        {
            $team2Players[] = $teamPlayerMapper->createEntityFromArray($player);
        }
        $team2->setPlayers($team2Players);
        
        //nominations
        $team1Nominations = array();
        foreach($data['teams']['team1']['nomination']['in'] as $key => $team1Player)
        {
            $nominationPlayer =  $nominationMapper->createEntityFromArray($team1Player);
            $nominationPlayer->setPlayerNumber($data['teams']['team1']['players'][$key]['player_number']);
            $team1Nominations['in'][$key] =  $nominationPlayer;
        }
        foreach($data['teams']['team1']['nomination']['out'] as $key => $team1Player)
        {
            $nominationPlayer =  $nominationMapper->createEntityFromArray($team1Player);
            $nominationPlayer->setPlayerNumber($data['teams']['team1']['players'][$key]['player_number']);
            $team1Nominations['out'][$key] =  $nominationPlayer;
        }
        
        $team2Nominations = array();
        foreach($data['teams']['team2']['nomination']['in'] as $key => $team2Player)
        {
            $nominationPlayer =  $nominationMapper->createEntityFromArray($team2Player);
            $nominationPlayer->setPlayerNumber($data['teams']['team2']['players'][$key]['player_number']);
            
            $team2Nominations['in'][$key] = $nominationPlayer;
        }
        foreach($data['teams']['team2']['nomination']['out'] as $key => $team2Player)
        {
            $nominationPlayer =  $nominationMapper->createEntityFromArray($team2Player);
            $nominationPlayer->setPlayerNumber($data['teams']['team2']['players'][$key]['player_number']);
            $team2Nominations['out'][$key] =  $nominationPlayer;
        }
        
        $team1->setEventNomination($team1Nominations);
        $team2->setEventNomination($team2Nominations);
        //$team1->setEventNomination($data['teams']['team1']['nomination']);
        //$team2->setEventNomination($data['teams']['team2']['nomination']);
        
        
        $object->setTeam1($team1);
        $object->setTeam2($team2);
        
        //match
        $eventMatchMapper = $this->getEventMatchMapper();
        $eventMatch = $eventMatchMapper->createEntityFromArray($data['lineup']);
        $eventMatch->setMatchResult($data['lineup']['result']);
         
        $object->setMatch($eventMatch);
        
        
        
        return $object;
    }
    
    public function createMatchHit($hitData)
    {
         $data =  $this->getStorage()->createMatchHit($hitData);
         $mapper = $this->getEventMatchHitMapper();
         if(array_key_exists('goal', $data['data']))
         {
              $goalData = $data['data']['goal'];
              $groups[] = $mapper->createEntityFromArray($goalData);
         }
         
         if(array_key_exists('assist', $data['data']))
         {
              $assistData = $data['data']['assist'];
              $groups[] = $mapper->createEntityFromArray($assistData);
         }
         
         if(array_key_exists('mom', $data['data']))
         {
              $momData = $data['data']['mom'];
              $groups[] = $mapper->createEntityFromArray($momData);
         }

         return  $groups;
    }
    
    
    public function editMatchHit($hitData)
    {
         $data =  $this->getStorage()->editMatchHit($hitData);
         $mapper = $this->getEventMatchHitMapper();
         if(array_key_exists('goal', $data['data']))
         {
              $goalData = $data['data']['goal'];
              $groups[] = $mapper->createEntityFromArray($goalData);
         }
         
         if(array_key_exists('assist', $data['data']))
         {
              $assistData = $data['data']['assist'];
              $groups[] = $mapper->createEntityFromArray($assistData);
         }
         
         if(array_key_exists('mom', $data['data']))
         {
              $momData = $data['data']['mom'];
              $groups[] = $mapper->createEntityFromArray($momData);
         }

         return  $groups;
    }
    
    public function clearEventStats($data)
    {
         $this->getStorage()->clearEventStats($data);
    }
    
    public function resetEvent($eventId)
    {
         $this->getStorage()->resetEvent($eventId);
        
    }
    public function openEvent($eventId)
    {
         $this->getStorage()->openEvent($eventId);
        
    }
    
  
    
    public function getEventMatchOverview($id)
    {
        $data = $this->getStorage()->getEventMatchTimeline($id);
        $mapper = $this->getEventMatchHitMapper();
        
        $timeline = array();
        foreach($data as $d)
        {
            $object = $mapper->createEntityFromArray($d);
            $timeline[$object->getHitGroup()][] = $object;
        }
        
        
        //$firstLineData  = array();
        //$secondLineData  = array();
        $stats = array(
            'first_line' => array(
                'goals' => 0,
                'players' =>array()
            ),
            'second_line' => array(
                'goals' => 0,
                'players' =>array()
            )
        );
        
        
         foreach($data as $d)
         {
              $stats[$d['lineup_position']]['players'][$d['lineup_player_id']]['goals']  = 0;
              $stats[$d['lineup_position']]['players'][$d['lineup_player_id']]['assist']  = 0;
              $stats[$d['lineup_position']]['players'][$d['lineup_player_id']]['mom']  = 0;
              $stats[$d['lineup_position']]['players'][$d['lineup_player_id']]['name']  = $d['player_name'];
         }
      

        foreach($data as $d)
        {
            if($d['hit_type'] == 'goal')
            {
                $stats[$d['lineup_position']]['goals'] ++;
                $stats[$d['lineup_position']]['team_name']  = $d['match_team_name'];
                $stats[$d['lineup_position']]['players'][$d['lineup_player_id']]['goals']  += 1;
                $stats[$d['lineup_position']]['players'][$d['lineup_player_id']]['name']  = $d['player_name'];
            }
            if($d['hit_type'] == 'assist')
            {
                $stats[$d['lineup_position']]['players'][$d['lineup_player_id']]['assist']  += 1;
                $stats[$d['lineup_position']]['players'][$d['lineup_player_id']]['name']  = $d['player_name'];
            }
             if($d['hit_type'] == 'mom')
            {
                $stats[$d['lineup_position']]['players'][$d['lineup_player_id']]['mom']  = 1;
                $stats[$d['lineup_position']]['players'][$d['lineup_player_id']]['name']  = $d['player_name'];
            }
        }
        
      
        
        $matchOverview = new EventMatchOverview();
        $matchOverview->setTimeline($timeline);
        $matchOverview->setStats($stats);
        return $matchOverview;
        
    }

   public function updateEventResult($data)
   {
       return $this->getStorage()->updateEventResult($data);
   }
   
   public function getTournamentPlayersStats($data)
   {
        $statData =   $this->getStorage()->getTournamentPlayersStats($data);
        return $statData;
   }
   
   public function getTournamentStats($id,$filterData = null)
   {
       $data =   $this->getStorage()->getTournamentStats($id,$filterData);
       $mapper = $this->getTeamMapper();
        
        $list = array();
        foreach($data as $d)
        {
            $object = $mapper->createEntityFromArray($d['team']);
            $d['team'] = $object;
            $list[] = $d;
        }
        
        return $list;
   }
   
   public function getTournamentEventLiveData($eventId)
   {
        $timelineData =  $this->getStorage()->getEventMatchTimeline($eventId);
        
        //$firstLineData  = array();
        //$secondLineData  = array();
        $data = array(
            'first_line' => array(
                'points' => 0
            ),
            'second_line' => array(
                'points' => 0
            )
        );
        
        foreach($timelineData as $d)
        {
            if($d['hit_type'] == 'goal')
            {
                $data[$d['lineup_position']]['points'] ++;
                $data[$d['lineup_position']]['team_name']  = $d['match_team_name'];
            }
        }
        
        t_dump($data);
   }
   
   public function mailchimpExport($user,$team)
   {
        $mailchimpListId = 'c74a69f735';

        
         $MailChimp = ServiceLayer::getService('MailchimpManager');
          $logger = ServiceLayer::getService('system_logger');
          
          $data = array(
                    'email_address'=> $user->getEmail(),
                    'language' => $user->getDefaultLang(), 
                    'status' => 'subscribed', 
                    'merge_fields' => array(
                        'FNAME' => $user->getName(),
                        'LNAME' => $user->getSurname(),
                        'SKOLA' => $team->getName(),
                        'KRAJ' => strtoupper(str_replace('sk', '', $team->getLocality())),
                        
                        )
                    );
          

                $response = $MailChimp->post('lists/'.$mailchimpListId.'/members',$data);

                $log_data = array();
                if($response['status'] == 400)
                {
                    $log_data['activity'] = 'mailchimp_export_error';
                    $log_data['detail'] = 'Error export email '.$user->getEmail().',DETAIL:'.$response['detail '];
                }
                else
                {
                    $log_data['activity'] = 'mailchimp_export_succes';
                    $log_data['detail'] = 'export email '.$user->getEmail().',ID:'.$response['id'].',status'.$response['status'];
                }
                $logger->addSystemRecord($log_data);
   }
}
