<?php

namespace CR\Tournament\Model;

use Core\EntityMapper as EntityMapper;
use Core\ServiceLayer as ServiceLayer;

class TeamPlayer {

    protected $mapper_rules = array(
        'id' => array(
            'type' => 'int',
            'required' => false,
        ),
        'name' => array(
            'type' => 'string',
            'required' => false,
            'type' => 'non-persist'
        ),
        'player_id' => array(
            'type' => 'int',
            'required' => false
        ),
        'team_id' => array(
            'type' => 'int',
            'required' => false
        ),
        'status' => array(
            'type' => 'string',
            'required' => false
        ),
        'team_role' => array(
            'type' => 'string',
            'required' => false
        ),
        'level' => array(
            'type' => 'string',
            'required' => false
        ),
        'first_name' => array(
            'type' => 'string',
            'required' => false
        ),
        'last_name' => array(
            'type' => 'string',
            'required' => false
        ),
        'email' => array(
            'type' => 'string',
            'required' => false
        ),
        'created_at' => array(
            'type' => 'datetime',
            'required' => false
        ),
        'player_number' => array(
            'type' => 'int',
            'required' => false
        ),
       
    );

    public function getDataMapperRules()
    {
        return $this->mapper_rules;
    }

    protected $id;
    protected $playerId;
    protected $teamId;
    protected $name;
    protected $teamRole;
    protected $status;
    protected $level;
    protected $firstName;
    protected $lastName;
    protected $email;
    protected $createdAt;
    protected $photo;
    protected $rating;
    protected $player;
    protected $stats;
    protected $eventAttendance;
    protected $playerNumber;
    protected $lineupPosition;
    protected $playerName;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getPlayerId()
    {
        return $this->playerId;
    }

    public function getTeamId()
    {
        return $this->teamId;
    }

    public function setPlayerId($playerId)
    {
        $this->playerId = $playerId;
    }

    public function setTeamId($teamId)
    {
        $this->teamId = $teamId;
    }

    public function getTeamRole()
    {
        return $this->teamRole;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function getLevel()
    {
        return $this->level;
    }

    public function setTeamRole($teamRole)
    {
        $this->teamRole = $teamRole;
    }

    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function setLevel($level)
    {
        $this->level = $level;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setCreatedAt($createAt)
    {
        $this->createdAt = $createAt;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getFirstName()
    {
        return $this->firstName;
    }

    public function getLastName()
    {
        return $this->lastName;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getFullName()
    {
        return $this->getFirstName() . ' ' . $this->getLastName();
    }

    public function getPhoto()
    {
        return $this->photo;
    }
    
    function getPlayerNumber()
    {
        return $this->playerNumber;
    }

    function setPlayerNumber($playerNumber)
    {
        $this->playerNumber = $playerNumber;
    }

    public function getMidPhoto()
    {
        $playerManager = ServiceLayer::getService('PlayerManager');
        if (null == $this->photo)
        {
            if($this->getPlayerId() > 0)
            {
                $player = $playerManager->findPlayerById($this->getPlayerId());
                if(null != $player)
                {
                    $avatar = $playerManager->createAvatar($player->getFullname());
                    $avatarName = $playerManager->savePlayerAvatar($player,$avatar);
                    $player->setPhoto($avatarName);
                    $playerManager->savePlayer($player);
                    $midPhoto = '/img/users/'.$avatarName;
                } 
                else 
                {
                    if(!file_exists(PUBLIC_DIR.'/img/tournament-team-players/'.$this->getId().'.png'))
                    {
                      $avatar = $playerManager->createAvatar($this->getFullname());
                      $avatarName = $playerManager->saveTournamentTeamPlayerAvatar($this,$avatar);
                      $midPhoto =  '/img/tournament-team-players/'.$avatarName;
                    }
                    else
                    {
                        $midPhoto =  '/img/tournament-team-players/'.$this->getId().'.png';
                    }
                }
            }
            else 
            {
                  
                if(!file_exists(PUBLIC_DIR.'/img/tournament-team-players/'.$this->getId().'.png'))
                {
                  $avatar = $playerManager->createAvatar($this->getFullname());
                  $avatarName = $playerManager->saveTournamentTeamPlayerAvatar($this,$avatar);
                  $midPhoto =  '/img/tournament-team-players/'.$avatarName;
                }
                else
                {
                    $midPhoto =  '/img/tournament-team-players/'.$this->getId().'.png';
                }
                    
            }

            //$midPhoto = '/img/users/default.svg';
            //$midPhoto = '/img/users/default.svg';
        }
        else
        {
            
            $midPhoto = '/img/users/thumb_320_320/' . $this->getPhoto();
            if(!file_exists(PUBLIC_DIR.$midPhoto))
            {
                  $avatar = $playerManager->createAvatar($this->getFullname());
                  $avatarName = $playerManager->saveTournamentTeamPlayerAvatar($this,$avatar);
                  $midPhoto =  '/img/tournament-team-players/'.$avatarName;
            } 
             
             
        }


        return $midPhoto;
    }

    public function setPhoto($photo)
    {
        $this->photo = $photo;
    }

    public function getTeamRoleName()
    {
        $teamManager = ServiceLayer::getService('TeamManager');
        $list = $teamManager->getPermissionEnum();
        return $list[$this->getTeamRole()];
    }

    public function getLevelName()
    {
        $teamManager = ServiceLayer::getService('TeamManager');
        $list = $teamManager->getLevelEnum();
        return $list[$this->getLevel()];
    }

    public function getStatusName()
    {
        $teamManager = ServiceLayer::getService('TeamManager');
        $list = $teamManager->getPlayerStatusEnum();
        return $list[$this->getStatus()];
    }

    public function getRating()
    {
        return $this->rating;
    }

    public function setRating($rating)
    {
        $this->rating = $rating;
    }

    public function getPlayer()
    {
        return $this->player;
    }

    public function setPlayer($player)
    {
        $this->player = $player;
    }

    public function getPlayerSports()
    {
        $player = $this->getPlayer();
        if (null != $player)
        {
            return $player->getSports();
        }
        return array();
    }

    public function getStats()
    {
        return $this->stats;
    }

    public function setStats($stats)
    {
        $this->stats = $stats;
    }

    public function getEventAttendance($index)
    {
        if (null == $index)
        {
            return $this->eventAttendance;
        }
        else
        {
            return $this->eventAttendance[$index];
        }
    }

    public function setEventAttendance($eventAttendance)
    {
        $this->eventAttendance = $eventAttendance;
    }

    public function addEventAttendance($index, $eventAttendance)
    {
        $this->eventAttendance[$index] = $eventAttendance;
    }
    
    public function getEventAttendanceStatus($index)
    {
        if(null == $this->getEventAttendance($index))
        {
            return null;
        }
        else
        {
            return $this->getEventAttendance($index)->getStatus();
        }
    }
    
    public function getHash()
    {
        return sha1($this->getCreatedAt()->format('Ymdhis').$this->getId());
    }
    
    public  function getLineupPosition() {
return $this->lineupPosition;
}

public  function setLineupPosition($lineupPosition) {
$this->lineupPosition = $lineupPosition;
return $this;
}

public  function getPlayerName() {
return $this->playerName;
}

public  function setPlayerName($playerName) {
$this->playerName = $playerName;
return $this;
}




}
