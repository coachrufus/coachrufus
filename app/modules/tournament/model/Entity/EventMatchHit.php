<?php

namespace CR\Tournament\Model;

use Core\EntityMapper as EntityMapper;
use Core\ServiceLayer as ServiceLayer;

class EventMatchHit {

    private $repository;
    private $mapper_rules = array(
        'id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => true
        ),
        'user_id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'hit_type' => array(
            'type' => 'string',
            'max_length' => '50',
            'required' => false
        ),
        'hit_time' => array(
            'type' => '',
            'max_length' => '',
            'required' => false
        ),
        'note' => array(
            'type' => 'string',
            'max_length' => '',
            'required' => false
        ),
        'lineup_id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'match_team_name' => array(
            'type' => 'string',
            'max_length' => '100',
            'required' => false
        ),
        'event_id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'team_position' => array(
            'type' => 'string',
            'max_length' => '50',
            'required' => false
        ),
        'event_date' => array(
            'type' => 'datetime',
            'max_length' => '',
            'required' => false
        ),
        'lineup_position' => array(
            'type' => 'string',
            'max_length' => '255',
            'required' => false
        ),
        'lineup_player_id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'team_id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'points' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'duration_time' => array(
            'type' => '',
            'max_length' => '',
            'required' => false
        ),
        'set_position' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'hit_group' => array(
            'type' => 'string',
            'max_length' => '100',
            'required' => false
        ),
        'player_name' => array(
            'type' => 'string',
            'max_length' => '255',
            'required' => false
        ),
    );

    public function getDataMapperRules()
    {
        return $this->mapper_rules;
    }

    protected $id;
    protected $userId;
    protected $hitType;
    protected $hitTime;
    protected $note;
    protected $lineupId;
    protected $matchTeamName;
    protected $eventId;
    protected $teamPosition;
    protected $eventDate;
    protected $lineupPosition;
    protected $lineupPlayerId;
    protected $teamId;
    protected $points;
    protected $durationTime;
    protected $setPosition;
    protected $hitGroup;
    protected $playerName;

    public function setId($val)
    {
        $this->id = $val;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setUserId($val)
    {
        $this->userId = $val;
    }

    public function getUserId()
    {
        return $this->userId;
    }

    public function setHitType($val)
    {
        $this->hitType = $val;
    }

    public function getHitType()
    {
        return $this->hitType;
    }

    public function setHitTime($val)
    {
        $this->hitTime = $val;
    }

    public function getHitTime()
    {
        return $this->hitTime;
    }

    public function setNote($val)
    {
        $this->note = $val;
    }

    public function getNote()
    {
        return $this->note;
    }

    public function setLineupId($val)
    {
        $this->lineupId = $val;
    }

    public function getLineupId()
    {
        return $this->lineupId;
    }

    public function setMatchTeamName($val)
    {
        $this->matchTeamName = $val;
    }

    public function getMatchTeamName()
    {
        return $this->matchTeamName;
    }

    public function setEventId($val)
    {
        $this->eventId = $val;
    }

    public function getEventId()
    {
        return $this->eventId;
    }

    public function setTeamPosition($val)
    {
        $this->teamPosition = $val;
    }

    public function getTeamPosition()
    {
        return $this->teamPosition;
    }

    public function setEventDate($val)
    {
        $this->eventDate = $val;
    }

    public function getEventDate()
    {
        return $this->eventDate;
    }

    public function setLineupPosition($val)
    {
        $this->lineupPosition = $val;
    }

    public function getLineupPosition()
    {
        return $this->lineupPosition;
    }

    public function setLineupPlayerId($val)
    {
        $this->lineupPlayerId = $val;
    }

    public function getLineupPlayerId()
    {
        return $this->lineupPlayerId;
    }

    public function setTeamId($val)
    {
        $this->teamId = $val;
    }

    public function getTeamId()
    {
        return $this->teamId;
    }

    public function setPoints($val)
    {
        $this->points = $val;
    }

    public function getPoints()
    {
        return $this->points;
    }

    public function setDurationTime($val)
    {
        $this->durationTime = $val;
    }

    public function getDurationTime()
    {
        return $this->durationTime;
    }

    public function setSetPosition($val)
    {
        $this->setPosition = $val;
    }

    public function getSetPosition()
    {
        return $this->setPosition;
    }

    public function setHitGroup($val)
    {
        $this->hitGroup = $val;
    }

    public function getHitGroup()
    {
        return $this->hitGroup;
    }

    function getPlayerName()
    {
        return $this->playerName;
    }

    function setPlayerName($playerName)
    {
        $this->playerName = $playerName;
    }
    
    public function getPhaseHitCodes()
    {
        return  array('match_phase_3-1-end','match_phase_3-2-start','match_phase_3-2-end','match_phase_3-3-start');
    }

    public function getHitTypeAcr()
    {
        $acr = strtoupper(substr($this->getHitType(), 0, 1));
        
        /*
        $phaseHitCodes = $this->getPhaseHitCodes();
        if(in_array($this->getHitType(), $phaseHitCodes))
        {
            $acr = 'tournament.timeline.'.$this->getHitType();
        }
        */
        
        return $acr;
    }
    
    public function isPhaseHit()
    {
        $phaseHitCodes = $this->getPhaseHitCodes();
        
        if(in_array($this->getHitType(), $phaseHitCodes))
        {
            return true;
        }
        
        return false;
                
    }
    
    public function getTimelineHitTime()
    {
        if(null != $this->getHitTime())
        {
             $time = \DateTime::createFromFormat( 'H:i:s', $this->getHitTime() );
              $addMinutes =  $time->format('H')*60;
        
            return $time->format('i')+$addMinutes.':'.$time->format('s');
      
        }
       
        return '';
    }

}
