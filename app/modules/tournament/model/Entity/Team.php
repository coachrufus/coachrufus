<?php
	
namespace CR\Tournament\Model;
use Core\EntityMapper as EntityMapper;
use Core\ServiceLayer as ServiceLayer;
		
class Team extends \Webteamer\Team\Model\Team {	

	
	
		
private $repository;
private $mapper_rules  = array(
	'id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => true
							
					),
'tournament_id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => true
							
					),
'name' => array(
					'type' => 'string',
					'max_length' => '255',
					'required' => false
							
					),
'email' => array(
					'type' => 'string',
					'max_length' => '255',
					'required' => false
							
					),
'locality' => array(
					'type' => 'string',
					'max_length' => '255',
					'required' => false
							
					),
'external_id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => false
							
					),
'division' => array(
					'type' => 'string',
					'max_length' => '200',
					'required' => false
							
					),
'contact_name' => array(
					'type' => 'string',
					'max_length' => '200',
					'required' => false
							
					),
    'gdpr_marketing' => array(
					'type' => 'int',
					'max_length' => '200',
					'required' => false
							
					),
    'photo' => array(
					'type' => 'string',
					'max_length' => '255',
					'required' => false
							
					),
'description' => array(
					'type' => 'string',
					'max_length' => '2000',
					'required' => false
							
					),
        'locality_id' => array(
                                            'type' => 'int',
                                            'required' => false

                                            ), 
);
	
public function getDataMapperRules()
{
	return $this->mapper_rules;
}

public function getRepository()
{
	if(null == $this->repository)
	{
		$this->repository = ServiceLayer::getService("TeamRepository"); 
	}
	return $this->repository;
}

public function setRepository($repository)
{
	$this->repository = $repository;
}
			

		
		
		
protected $id;

protected $tournamentId;

protected $name;

protected $externalId;
protected $email;

protected $players;
protected $locality;
protected $eventNomination = array('in' => array(),'out' => array());

protected $adminList;
protected $division;
protected $contactName;
protected $gdprMarketing;
protected $photo;
protected $description;
protected $status;
protected $creatorEmail;
protected $localityId;
				
public function setId($val){$this->id = $val;}

public function getId(){return $this->id;}
	
public function setTournamentId($val){$this->tournamentId = $val;}

public function getTournamentId(){return $this->tournamentId;}
	
public function setName($val){$this->name = $val;}

public function getName(){return $this->name;}
	
public function setExternalId($val){$this->externalId = $val;}

public function getExternalId(){return $this->externalId;}

public  function getEmail() {
return $this->email;
}

public  function setEmail($email) {
$this->email = $email;
return $this;
}

public  function getPlayers() {
return $this->players;
}

public  function setPlayers($players) {
$this->players = $players;
return $this;
}

public  function getEventNomination() {
return $this->eventNomination;
}

public  function setEventNomination($eventNomination) {
$this->eventNomination = $eventNomination;
}


public function getEventNominationIn()
{
    return $this->eventNomination['in'];
}
public function getEventNominationOut()
{
    return $this->eventNomination['out'];
}

public function getHash()
{
    return substr(sha1($this->getId()),0,3).$this->getId().substr(md5($this->getId()),0,16);
}

public  function getAdminList() {
return $this->adminList;
}

public  function setAdminList($adminList) {
$this->adminList = $adminList;
return $this;
}

public  function getLocality() {
return $this->locality;
}

public  function setLocality($locality) {
$this->locality = $locality;
return $this;
}

public  function getDivision() {
return $this->division;
}

public  function getContactName() {
return $this->contactName;
}

public  function setDivision($division) {
$this->division = $division;
return $this;
}

public  function setContactName($contactName) {
$this->contactName = $contactName;
return $this;
}

function getGdprMarketing() {
return $this->gdprMarketing;
}

 function setGdprMarketing($gdprMarketing) {
$this->gdprMarketing = $gdprMarketing;
}

function getPhoto() {
return $this->photo;
}

 function getDescription() {
return $this->description;
}

 function setPhoto($photo) {
$this->photo = $photo;
}

 function setDescription($description) {
$this->description = $description;
}


function getStatus() {
return $this->status;
}

 function setStatus($status) {
$this->status = $status;
}

public function getStatusName()
{
    return $this->getStatus();
}

function getCreatorEmail() {
return $this->creatorEmail;
}

 function setCreatorEmail($creatorEmail) {
$this->creatorEmail = $creatorEmail;
}


public  function getLocalityId() {
return $this->localityId;
}

public  function setLocalityId($localityId) {
$this->localityId = $localityId;
}



}

		