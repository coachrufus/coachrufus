<?php

namespace CR\Tournament\Model;

use Core\EntityMapper as EntityMapper;
use Core\ServiceLayer as ServiceLayer;

class EventMatch {

    private $repository;
    private $mapper_rules = array(
        'id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => true
        ),
        'event_id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'event_date' => array(
            'type' => 'datetime',
            'max_length' => '',
            'required' => false
        ),
        'sport_id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'tournament_id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'created_at' => array(
            'type' => 'datetime',
            'max_length' => '',
            'required' => false
        ),
        'first_line_name' => array(
            'type' => 'string',
            'max_length' => '255',
            'required' => false
        ),
        'second_line_name' => array(
            'type' => 'string',
            'max_length' => '255',
            'required' => false
        ),
        'status' => array(
            'type' => 'string',
            'max_length' => '255',
            'required' => false
        ),
        'date2' => array(
            'type' => 'datetime',
            'max_length' => '',
            'required' => false
        ),
    );

    public function getDataMapperRules()
    {
        return $this->mapper_rules;
    }

    protected $id;
    protected $eventId;
    protected $eventDate;
    protected $sportId;
    protected $tournamentId;
    protected $createdAt;
    protected $firstLineName;
    protected $secondLineName;
    protected $status;
    protected $date2;
    protected $firstLineGoals;
    protected $secondLineGoals;
    protected $matchResult;

    public function setId($val)
    {
        $this->id = $val;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setEventId($val)
    {
        $this->eventId = $val;
    }

    public function getEventId()
    {
        return $this->eventId;
    }

    public function setEventDate($val)
    {
        $this->eventDate = $val;
    }

    public function getEventDate()
    {
        return $this->eventDate;
    }

    public function setSportId($val)
    {
        $this->sportId = $val;
    }

    public function getSportId()
    {
        return $this->sportId;
    }

    public function setCreatedAt($val)
    {
        $this->createdAt = $val;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setFirstLineName($val)
    {
        $this->firstLineName = $val;
    }

    public function getFirstLineName()
    {
        return $this->firstLineName;
    }

    public function setSecondLineName($val)
    {
        $this->secondLineName = $val;
    }

    public function getSecondLineName()
    {
        return $this->secondLineName;
    }

    public function setStatus($val)
    {
        $this->status = $val;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setDate2($val)
    {
        $this->date2 = $val;
    }

    public function getDate2()
    {
        return $this->date2;
    }

    public function getTournamentId()
    {
        return $this->tournamentId;
    }

    public function setTournamentId($tournamentId)
    {
        $this->tournamentId = $tournamentId;
    }

    public function getFirstLineGoals()
    {
        $matchResult = $this->getMatchResult();
        if (!empty($matchResult))
        {
            return $matchResult['first_line']['points'];
        }
    }

    public function getSecondLineGoals()
    {
        $matchResult = $this->getMatchResult();
        if (!empty($matchResult))
        {
            return $matchResult['second_line']['points'];
        }
    }

    public function getMatchResult()
    {
        return $this->matchResult;
    }

    public function setMatchResult($matchResult)
    {
        $this->matchResult = $matchResult;
    }

    public function hasOvertime()
    {
       $matchResult = $this->getMatchResult();
       if( $matchResult['first_line']['overtime'] == 1 or $matchResult['second_line']['overtime'] == 1)
       {
           return true;
       }
       return false;
    }

}
