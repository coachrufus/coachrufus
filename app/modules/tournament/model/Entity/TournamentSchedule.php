<?php

namespace CR\Tournament\Model;

use Core\EntityMapper as EntityMapper;
use Core\ServiceLayer as ServiceLayer;

class TournamentSchedule {

    private $repository;
    private $mapper_rules = array(
        'id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => true
        ),
        'created_at' => array(
            'type' => 'datetime',
            'max_length' => '',
            'required' => false
        ),
        'definition' => array(
            'type' => 'string',
            'max_length' => '',
            'required' => false
        ),
        'result' => array(
            'type' => 'string',
            'max_length' => '',
            'required' => false
        ),
        'csv' => array(
            'type' => 'string',
            'max_length' => '',
            'required' => false
        ),
        'html_a' => array(
            'type' => 'string',
            'max_length' => '',
            'required' => false
        ),
        'html_b' => array(
            'type' => 'string',
            'max_length' => '',
            'required' => false
        ),
        'html_c' => array(
            'type' => 'string',
            'max_length' => '',
            'required' => false
        ),
        'json' => array(
            'type' => 'string',
            'max_length' => '',
            'required' => false
        ),
        'analysis' => array(
            'type' => 'string',
            'max_length' => '',
            'required' => false
        ),
        'tournament_id' => array(
            'type' => 'int',
            'max_length' => '',
            'required' => false
        ),
        'schedule_type' => array(
            'type' => 'string',
            'max_length' => '',
            'required' => false
        ),
        'name' => array(
            'type' => 'string',
            'max_length' => '',
            'required' => false
        ),
    );

    public function getDataMapperRules()
    {
        return $this->mapper_rules;
    }

    public function getRepository()
    {
        if (null == $this->repository)
        {
            $this->repository = ServiceLayer::getService("TournamentScheduleRepository");
        }
        return $this->repository;
    }

    public function setRepository($repository)
    {
        $this->repository = $repository;
    }

    protected $id;
    protected $createdAt;
    protected $definition;
    protected $result;
    protected $csv;
    protected $htmlA;
    protected $htmlB;
    protected $htmlC;
    protected $json;
    protected $analysis;
    protected $tournamentId;
    protected $scheduleType;
    protected $name;
    protected $events;

    public function setId($val)
    {
        $this->id = $val;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setCreatedAt($val)
    {
        $this->createdAt = $val;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setDefinition($val)
    {
        $this->definition = $val;
    }

    public function getDefinition()
    {
        return $this->definition;
    }

    public function setResult($val)
    {
        $this->result = $val;
    }

    public function getResult()
    {
        return $this->result;
    }

    public function setCsv($val)
    {
        $this->csv = $val;
    }

    public function getCsv()
    {
        return $this->csv;
    }

    public function setHtmlA($val)
    {
        $this->htmlA = $val;
    }

    public function getHtmlA()
    {
        return $this->htmlA;
    }

    public function setHtmlB($val)
    {
        $this->htmlB = $val;
    }

    public function getHtmlB()
    {
        return $this->htmlB;
    }

    public function setHtmlC($val)
    {
        $this->htmlC = $val;
    }

    public function getHtmlC()
    {
        return $this->htmlC;
    }

    public function setJson($val)
    {
        $this->json = $val;
    }

    public function getJson()
    {
        return $this->json;
    }

    public function getAnalysis()
    {
        return $this->analysis;
    }

    public function setAnalysis($analysis)
    {
        $this->analysis = $analysis;
    }

    public function getTournamentId()
    {
        return $this->tournamentId;
    }

    public function setTournamentId($tournamentId)
    {
        $this->tournamentId = $tournamentId;
    }

    public function getScheduleType()
    {
        return $this->scheduleType;
    }

    public function setScheduleType($scheduleType)
    {
        $this->scheduleType = $scheduleType;
    }

    public function getTeamsCount()
    {
        $definition = json_decode($this->getDefinition());
        $definition->teams_count;
    }

    public function getScheduleTypeName()
    {
        
    }

    public function getGroups()
    {
        $definition = json_decode($this->getDefinition());
        return $definition->groups;
    }

    public function getFieldsCount()
    {
        $definition = json_decode($this->getDefinition());
        return $definition->fields_count;
    }

    public function getEvents()
    {
        return $this->events;
    }

    public function setEvents($events)
    {
        $this->events = $events;
        return $this;
    }

    public function addEvent($event)
    {
        $this->events[$event->getId()] = $event;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }
    
    public function getRoundEvents($round)
    {
        $list = array();
        foreach($this->getEvents() as $event)
        {
            if($event->getRound() == $round)
            {
                $list[] = $event;
            }
        }
        
        return $list;
    }
    
    /*
    public function generateBracketSchemaEvents($schema,$roundIndex,$roundEvents)
    {
         echo '$roundIndex = '.$roundIndex.'<br />';
         
         
         $nextRoundEvents = array_chunk($roundEvents, 2);
         

        
        
        $nextRoundIndex  = $roundIndex+1;
        echo '$nextRoundIndex = '.$nextRoundIndex.'<br />';
        foreach($nextRoundEvents as $roundEvent)
        {
            $event = new TournamentEvent();
            $event->setId(rand(1000,2000));
            $event->setName($roundEvent[0]->getId().' vs '.$roundEvent[1]->getId().'  ');
            $schema[$nextRoundIndex][$roundEvent[0]->getId().'-'.$roundEvent[1]->getId()] = $event;
        }
        
        echo 'count = '.count($schema[$nextRoundIndex]);
        t_dump($schema[$nextRoundIndex]);

        if(count($schema[$nextRoundIndex]) > 1 and $nextRoundIndex < 20)
        {
            
            $schema = $this->generateBracketSchemaEvents($schema,$nextRoundIndex,$schema[$nextRoundIndex]);
        }
        
        return $schema;
    }
    */
    public function getPlayoffBracketSchema()
    {
        $events  = $this->getEvents();
        
        $bracket = array();
        $orderIndex = 0;
        
        foreach($events as $event)
        {
            if($event->getRoundOrder() > 0)
            {
              
                 $bracket[$event->getRound()][ $event->getRoundOrder()] = $event;
            }
            else 
            {
                 $bracket[$event->getRound()][$orderIndex++] = $event;
            }
        }
        
        
        
        foreach($bracket as $roundId => $roundEvents)
        {
            ksort($roundEvents,true);
            $bracket[$roundId] = $roundEvents;
        }
        
        //t_dump($bracket);
         ksort($bracket,true);
        
       
        return $bracket;
    }

}
