<?php
	
namespace CR\Tournament\Model;
use Core\EntityMapper as EntityMapper;
use Core\ServiceLayer as ServiceLayer;
		
class TournamentEvent {	

	
	
		
private $repository;
private $mapper_rules  = array(
	'id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => true
							
					),
'name' => array(
					'type' => 'string',
					'max_length' => '255',
					'required' => true
							
					),
'start' => array(
					'type' => 'datetime',
					'max_length' => '',
					'required' => false
							
					),
'end' => array(
					'type' => 'datetime',
					'max_length' => '',
					'required' => false
							
					),
'tournament_id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => false
							
					),
'team_id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => false
							
					),
'opponent_team_id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => false
							
					),
'created_at' => array(
					'type' => 'datetime',
					'max_length' => '',
					'required' => false
							
					),
'author_id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => false
							
					),
'playground_id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => false
							
					),
'period' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => false
							
					),
'round' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => false
							
					),
'note' => array(
					'type' => 'string',
					'max_length' => '',
					'required' => false
							
					),
'schedule_id' => array(
					'type' => 'int',
					'max_length' => '',
					'required' => false
							
					),
     'playground_name' => array(
            'type' => 'string',
            'max_length' => '',
            'required' => false
        ),
     'match_type' => array(
            'type' => 'string',
            'max_length' => '255',
            'required' => false
        ),
    'round_order' => array(
            'type' => 'int',
            'max_length' => '',
            'required' => false
        ),

);
	
public function getDataMapperRules()
{
	return $this->mapper_rules;
}
		
	
protected $id;

protected $name;

protected $start;

protected $end;

protected $tournamentId;

protected $teamId;

protected $opponentTeamId;

protected $createdAt;

protected $authorId;

protected $playgroundId;

protected $note;
protected $period;
protected $round;
protected $team1;
protected $team2;
protected $match;
protected $scheduleName;				
protected $scheduleId;	
protected $adminList;
protected $playgroundName;
protected $matchType;
protected $roundOrder;

public function setId($val){$this->id = $val;}

public function getId(){return $this->id;}
	
public function setName($val){$this->name = $val;}

public function getName(){return $this->name;}
	
public function setStart($val){$this->start = $val;}

public function getStart(){return $this->start;}
	
public function setEnd($val){$this->end = $val;}

public function getEnd(){return $this->end;}
	
public function setTournamentId($val){$this->tournamentId = $val;}

public function getTournamentId(){return $this->tournamentId;}
	
public function setTeamId($val){$this->teamId = $val;}

public function getTeamId(){return $this->teamId;}
	
public function setOpponentTeamId($val){$this->opponentTeamId = $val;}

public function getOpponentTeamId(){return $this->opponentTeamId;}
	
public function setCreatedAt($val){$this->createdAt = $val;}

public function getCreatedAt(){return $this->createdAt;}
	
public function setAuthorId($val){$this->authorId = $val;}

public function getAuthorId(){return $this->authorId;}
	
public function setPlaygroundId($val){$this->playgroundId = $val;}

public function getPlaygroundId(){return $this->playgroundId;}
	
public function setNote($val){$this->note = $val;}

public function getNote(){return $this->note;}

function getPeriod() {
return $this->period;
}

 function getRound() {
return $this->round;
}

 function setPeriod($period) {
$this->period = $period;
}

 function setRound($round) {
$this->round = $round;
}

public  function getTeam1() {
return $this->team1;
}

public  function getTeam2() {
return $this->team2;
}

public  function setTeam1($team1) {
$this->team1 = $team1;
return $this;
}

public  function setTeam2($team2) {
$this->team2 = $team2;
return $this;
}


public  function getMatch() {
return $this->match;
}

public  function setMatch($match) {
$this->match = $match;
return $this;
}

public  function getScheduleName() {
return $this->scheduleName;
}

public  function setScheduleName($scheduleName) {
$this->scheduleName = $scheduleName;
return $this;
}


public  function getScheduleId() {
return $this->scheduleId;
}

public  function setScheduleId($scheduleId) {
$this->scheduleId = $scheduleId;
return $this;
}

public  function getAdminList() {
return $this->adminList;
}

public  function setAdminList($adminList) {
$this->adminList = $adminList;
return $this;
}

public  function getPlaygroundName() {
return $this->playgroundName;
}

public  function setPlaygroundName($playgroundName) {
$this->playgroundName = $playgroundName;
return $this;
}

function getMatchType() {
return $this->matchType;
}

 function setMatchType($matchType) {
$this->matchType = $matchType;
}



public function getNominationCount()
{
    $count = count($this->getTeam1()->getEventNominationIn()) + count($this->getTeam2()->getEventNominationIn());
    return $count;
   
}



public function getListName()
{
    $name = explode(':',$this->getName());
    return $name[0].' : '.$name[1];
}

public function getFirstTeamName()
{
     $name = explode(':',$this->getName());
     return $name[0];
}

public function getSecondTeamName()
{
     $name = explode(':',$this->getName());
     return $name[1];
}

  function getRoundOrder()
    {
        return $this->roundOrder;
    }

    function setRoundOrder($roundOrder)
    {
        $this->roundOrder = $roundOrder;
    }


}

		