<?php
	
namespace CR\Tournament\Model;
use Core\EntityMapper as EntityMapper;
use Core\ServiceLayer as ServiceLayer;
		
class EventNomination {	

	
	
		
private $repository;
private $mapper_rules  = array(
	'id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => true
							
					),
'lineup_id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => true
							
					),
'created_at' => array(
					'type' => 'datetime',
					'max_length' => '',
					'required' => false
							
					),
'player_id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => false
							
					),
'player_name' => array(
					'type' => 'string',
					'max_length' => '255',
					'required' => false
							
					),
'player_crs' => array(
					'type' => '',
					'max_length' => '',
					'required' => false
							
					),
'player_rating' => array(
					'type' => '',
					'max_length' => '',
					'required' => false
							
					),
'event_id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => false
							
					),
'team_id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => false
							
					),
'event_date' => array(
					'type' => 'datetime',
					'max_length' => '',
					'required' => false
							
					),
'attendance_id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => false
							
					),
'lineup_position' => array(
					'type' => 'string',
					'max_length' => '50',
					'required' => false
							
					),
'lineup_name' => array(
					'type' => 'string',
					'max_length' => '50',
					'required' => false
							
					),
'team_player_id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => false
							
					),
'player_position' => array(
					'type' => 'string',
					'max_length' => '50',
					'required' => false
							
					),
'tournament_id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => false
					),
'player_number' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => false
							
					),

);
	
public function getDataMapperRules()
{
	return $this->mapper_rules;
}
	
		
protected $id;

protected $lineupId;

protected $createdAt;

protected $playerId;

protected $playerName;

protected $playerCrs;

protected $playerRating;

protected $eventId;

protected $eventDate;

protected $attendanceId;

protected $lineupPosition;

protected $lineupName;

protected $teamPlayerId;

protected $playerPosition;

protected $tournamentId;

protected $teamId;
protected  $playerNumber;
				
public function setId($val){$this->id = $val;}

public function getId(){return $this->id;}
	
public function setLineupId($val){$this->lineupId = $val;}

public function getLineupId(){return $this->lineupId;}
	
public function setCreatedAt($val){$this->createdAt = $val;}

public function getCreatedAt(){return $this->createdAt;}
	
public function setPlayerId($val){$this->playerId = $val;}

public function getPlayerId(){return $this->playerId;}
	
public function setPlayerName($val){$this->playerName = $val;}

public function getPlayerName(){return $this->playerName;}
	
public function setPlayerCrs($val){$this->playerCrs = $val;}

public function getPlayerCrs(){return $this->playerCrs;}
	
public function setPlayerRating($val){$this->playerRating = $val;}

public function getPlayerRating(){return $this->playerRating;}
	
public function setEventId($val){$this->eventId = $val;}

public function getEventId(){return $this->eventId;}
	
public function setEventDate($val){$this->eventDate = $val;}

public function getEventDate(){return $this->eventDate;}
	
public function setAttendanceId($val){$this->attendanceId = $val;}

public function getAttendanceId(){return $this->attendanceId;}
	
public function setLineupPosition($val){$this->lineupPosition = $val;}

public function getLineupPosition(){return $this->lineupPosition;}
	
public function setLineupName($val){$this->lineupName = $val;}

public function getLineupName(){return $this->lineupName;}
	
public function setTeamPlayerId($val){$this->teamPlayerId = $val;}

public function getTeamPlayerId(){return $this->teamPlayerId;}
	
public function setPlayerPosition($val){$this->playerPosition = $val;}

public function getPlayerPosition(){return $this->playerPosition;}
	
public function setTournamentId($val){$this->tournamentId = $val;}

public function getTournamentId(){return $this->tournamentId;}

function getTeamId() {
return $this->teamId;
}

 function setTeamId($teamId) {
$this->teamId = $teamId;
}

function getPlayerNumber() {
return $this->playerNumber;
}

 function setPlayerNumber($playerNumber) {
$this->playerNumber = $playerNumber;
}




}

		