<?php

namespace CR\Tournament\Model;

use Core\EntityMapper as EntityMapper;
use Core\ServiceLayer as ServiceLayer;

class Tournament {

    private $mapper_rules = array(
        'id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => true
        ),
        'seo_id' => array(
            'type' => 'string',
            'max_length' => '255',
            'required' => true
        ),
        'name' => array(
            'type' => 'string',
            'max_length' => '255',
            'required' => false
        ),
        'start_date' => array(
            'type' => 'datetime',
            'required' => true
        ),
        'end_date' => array(
            'type' => 'datetime',
            'required' => true
        ),
        'sport_id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'created_at' => array(
            'type' => 'datetime',
            'max_length' => '',
            'required' => false
        ),
        'author_id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'age_from' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'age_to' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'status' => array(
            'type' => 'string',
            'max_length' => '50',
            'required' => false
        ),
        'description' => array(
            'type' => 'string',
            'max_length' => '',
            'required' => false
        ),
        'phone' => array(
            'type' => 'string',
            'max_length' => '50',
            'required' => false
        ),
        'email' => array(
            'type' => 'string',
            'max_length' => '50',
            'required' => false
        ),
        'gender' => array(
            'type' => 'string',
            'max_length' => '50',
            'required' => false
        ),
        'level' => array(
            'type' => 'string',
            'max_length' => '50',
            'required' => false
        ),
        'photo' => array(
            'type' => 'string',
            'max_length' => '50',
            'required' => false
        ),
        'address_city' => array(
            'type' => 'string',
            'max_length' => '50',
            'required' => false
        ),
        'address_zip' => array(
            'type' => 'string',
            'max_length' => '50',
            'required' => false
        ),
        'address_street' => array(
            'type' => 'string',
            'max_length' => '50',
            'required' => false
        ),
        'address_street_num' => array(
            'type' => 'string',
            'max_length' => '50',
            'required' => false
        ),
        'full_address' => array(
            'type' => 'string',
            'max_length' => '255',
            'required' => false
        ),
        'privacy' => array(
            'type' => 'string',
            'max_length' => '50',
            'required' => false
        ),
        'age_group' => array(
            'type' => 'string',
            'max_length' => '50',
            'required' => false
        ),
        'locality_id' => array(
            'type' => 'int',
            'required' => false
        ),
        'playground_alias' => array(
            'type' => 'non-persist',
            'required' => false
        ),
        'register_status' => array(
            'type' => 'string',
            'required' => false
        ),
    );

    public function getDataMapperRules()
    {
        return $this->mapper_rules;
    }

    protected $id;
    protected $name;
    protected $sportId;
    protected $createdAt;
    protected $authorId;
    protected $ageFrom;
    protected $ageTo;
    protected $status;
    protected $description;
    protected $phone;
    protected $email;
    protected $playgroundAlias;
    protected $gender;
    protected $level;
    protected $photo;
    protected $addressCity;
    protected $addressZip;
    protected $addressStreet;
    protected $addressStreetNum;
    protected $fullAddress;
    protected $privacy;
    protected $ageGroup;
    protected $localityId;
    protected $sportName;
    protected $startDate;
    protected $endDate;
    protected $seoId;
    protected $adminList;
    protected $metadata;
    protected $registerStatus;
    protected $userTeamsInfo;
    protected $userPermissions;

    public function setId($val)
    {
        $this->id = $val;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setName($val)
    {
        $this->name = $val;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setSportId($val)
    {
        $this->sportId = $val;
    }

    public function getSportId()
    {
        return $this->sportId;
    }

    public function setCreatedAt($val)
    {
        $this->createdAt = $val;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setAuthorId($val)
    {
        $this->authorId = $val;
    }

    public function getAuthorId()
    {
        return $this->authorId;
    }

    public function setAgeFrom($val)
    {
        $this->ageFrom = $val;
    }

    public function getAgeFrom()
    {
        return $this->ageFrom;
    }

    public function setAgeTo($val)
    {
        $this->ageTo = $val;
    }

    public function getAgeTo()
    {
        return $this->ageTo;
    }

    public function setStatus($val)
    {
        $this->status = $val;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setDescription($val)
    {
        $this->description = $val;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setPhone($val)
    {
        $this->phone = $val;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function setEmail($val)
    {
        $this->email = $val;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getPlaygroundAlias()
    {
        return $this->playgroundAlias;
    }

    public function setPlaygroundAlias($playgroundAlias)
    {
        $this->playgroundAlias = $playgroundAlias;
    }

    public function getGender()
    {
        return $this->gender;
    }

    public function getLevel()
    {
        return $this->level;
    }

    public function getPhoto()
    {
        return $this->photo;
    }

    public function getDefaultPhoto()
    {

        return '/img/team/users.svg';
    }

    public function getMidPhoto()
    {

        if (null == $this->getPhoto())
        {
            $midPhoto = $this->getDefaultPhoto();
        }
        else
        {
            $midPhoto = '/img/team/thumb_320_320/' . $this->getPhoto();
        }


        return $midPhoto;
    }

    public function getPrivacy()
    {
        return $this->privacy;
    }

    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    public function setLevel($level)
    {
        $this->level = $level;
    }

    public function setPhoto($photo)
    {
        $this->photo = $photo;
    }

    public function setPrivacy($privacy)
    {
        $this->privacy = $privacy;
    }

    public function getAgeGroup()
    {
        return $this->ageGroup;
    }

    public function setAgeGroup($ageGroup)
    {
        $this->ageGroup = $ageGroup;
    }

    public function getAddressCity()
    {
        return $this->addressCity;
    }

    public function getAddressZip()
    {
        return $this->addressZip;
    }

    public function getAddressStreet()
    {
        return $this->addressStreet;
    }

    public function getAddressStreetNum()
    {
        return $this->addressStreetNum;
    }

    public function setAddressCity($addressCity)
    {
        $this->addressCity = $addressCity;
    }

    public function setAddressZip($addressZip)
    {
        $this->addressZip = $addressZip;
    }

    public function setAddressStreet($addressStreet)
    {
        $this->addressStreet = $addressStreet;
    }

    public function setAddressStreetNum($addressStreetNum)
    {
        $this->addressStreetNum = $addressStreetNum;
    }

    public function getFullAddress()
    {
        return $this->fullAddress;
    }

    public function setFullAddress($fullAddress)
    {
        $this->fullAddress = $fullAddress;
    }

    public function getLocalityId()
    {
        return $this->localityId;
    }

    public function setLocalityId($localityId)
    {
        $this->localityId = $localityId;
    }

    public function getShareLinkHash()
    {
        return substr(sha1($this->getId()), 0, 3) . $this->getId() . substr(md5($this->getId()), 0, 8);
    }

    public function getSportName()
    {
        return $this->sportName;
    }

    public function setSportName($sportName)
    {
        $this->sportName = $sportName;
    }

    public function getStartDate()
    {
        return $this->startDate;
    }

    public function getEndDate()
    {
        return $this->endDate;
    }

    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
        return $this;
    }

    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
        return $this;
    }

    function getSeoId()
    {
        return $this->seoId;
    }

    function setSeoId($seoId)
    {
        $this->seoId = $seoId;
    }

    public function getAdminList()
    {
        return $this->adminList;
    }

    public function setAdminList($adminList)
    {
        $this->adminList = $adminList;
    }

    public function getMetadata()
    {
        return $this->metadata;
    }

    public function setMetadata($metadata)
    {
        $this->metadata = $metadata;
        return $this;
    }

    public function getTeamsCount()
    {
        $metadata = $this->getMetadata();
        return $metadata['teams_count'];
    }

    public function getPlayersCount()
    {
        $metadata = $this->getMetadata();
        return $metadata['players_count'];
    }

    public function getTotalMatchCount()
    {
        $metadata = $this->getMetadata();
        return $metadata['match_total'];
    }

    public function getClosedMatchCount()
    {
        $metadata = $this->getMetadata();
        return $metadata['match_closed'];
    }

    public function getRegisterStatus()
    {
        return $this->registerStatus;
    }

    public function setRegisterStatus($registerStatus)
    {
        $this->registerStatus = $registerStatus;
    }

    public function getUserTeamsInfo($index = null)
    {
        if(null == $index)
        {
            return $this->userTeamsInfo;
        }
        else 
        {
             return $this->userTeamsInfo[$index];
        }
        
    }

    public function setUserTeamsInfo($userTeamsInfo)
    {
        $this->userTeamsInfo = $userTeamsInfo;
    }
    
    public function getUserPermissions()
    {
        return $this->userPermissions;
    }

    public function setUserPermissions($userPermissions)
    {
        $this->userPermissions = $userPermissions;
    }
    
    
    public function getFormatedStartDate($format)
    {
        if(null != $this->getStartDate())
        {
            return $this->getStartDate()->format($format);
        }
        else
        {
            return '';
        }
    }
    
    public function getFormatedEndDate($format)
    {
        if(null != $this->getEndDate())
        {
            return $this->getEndDate()->format($format);
        }
        else
        {
            return '';
        }
    }

}
