<?php

namespace CR\Tournament\Model;

class EventMatchOverview {

    private $timeline;
    private $stats;

    public function getTimeline()
    {
        return $this->timeline;
    }

    public function getStats()
    {
        return $this->stats;
    }

    public function setTimeline($timeline)
    {
        $this->timeline = $timeline;
        return $this;
    }

    public function setStats($stats)
    {
        $this->stats = $stats;
        return $this;
    }

    public function getFirstLineGoals()
    {
        $stats = $this->getStats();
        return $stats['first_line']['goals'];
    }

    public function getSecondLineGoals()
    {
        $stats = $this->getStats();
        return $stats['second_line']['goals'];
    }
    
    public function getPlayersStats()
    {
        $statsTable = array();
        $stats = $this->getStats();

        foreach($stats['first_line']['players'] as $key =>  $player)
        {
            $points = $player['goals'] +$player['assist'] ;
            $pointsIndex = $points;
            
           
            $statsTable[$pointsIndex.'_'.md5($key)] = $player;
        }
        
        
        foreach($stats['second_line']['players'] as $key =>$player)
        {
            $points = $player['goals'] +$player['assist'] ;
            $pointsIndex = $points;
            
            $statsTable[$pointsIndex.'_'.md5($key)] = $player;
        }
        krsort($statsTable);
        return $statsTable;
    }

}
