<?php

namespace Base\Model;

use Core\Repository as Repository;

class StatsRepository extends Repository {

    protected $storage;

    public function __construct($storage)
    {
        $this->setStorage($storage);
    }

    public function getStorage()
    {
        return $this->storage;
    }

    public function setStorage($storage)
    {
        $this->storage = $storage;
    }

    public function getTeamStats($dateFrom, $dateTo)
    {
        return $this->getStorage()->getStats($dateFrom, $dateTo);
    }
    
    public function getUserSumStats()
    {
        $data = $this->getStorage()->getUserSumStats();
        
        $list = array();
        
        foreach($data as $d)
        {
            
            if($d['user_type'] == '')
            {
                $d['user_type'] = 'Direct';
            }
            if($d['default_lang'] == '')
            {
                $d['default_lang'] = 'sk';
            }
            
            $list[$d['month']]['total'] += $d['count'];
            $list[$d['month']]['user_types'][$d['user_type']] += $d['count'];
            $list[$d['month']]['default_langs'][$d['default_lang']] += $d['count'];
        }
        

       return $list;
    }
    
    public function getTeamSumStats()
    {
        $data = $this->getStorage()->getTeamSumStats();
        $list = array();
        
        foreach($data as $d)
        {
            
            if($d['age_group'] == '')
            {
                $d['age_group'] = 'Undefined';
            }
            if($d['level'] == '')
            {
                $d['level'] = 'Undefined';
            }
            
            $list[$d['month']]['total'] += $d['count'];
            $list[$d['month']]['age_groups'][$d['age_group']] += $d['count'];
            $list[$d['month']]['levels'][$d['level']] += $d['count'];
            $list[$d['month']]['gender'][$d['gender']] += $d['count'];
        }
        
       return $list;
    }
    
    public function getTeamPlayersStats()
    {
        $registred = $this->getStorage()->getTeamPlayersStats(array('registred' => 'yes'));
        $unregistred = $this->getStorage()->getTeamPlayersStats(array('registred' => 'no'));
        $list = array();
        
        foreach($registred as $r)
        {
            $list[$r['id']]['name'] = $r['name'];
            $list[$r['id']]['total'] += $r['count'];
            $list[$r['id']]['registred']['total'] += $r['count'];
            $list[$r['id']]['registred'][$r['status']] = $r['count'];
            $list[$r['id']]['sport'] = $r['sport_id'];
            $list[$r['id']]['created_at'] = new \DateTime($r['created_at']) ;
            $list[$r['id']]['last_attendance'] = new \DateTime($r['last_attendance']) ;
        }
        
        foreach($unregistred as $r)
        {
            $list[$r['id']]['name'] = $r['name'];
            $list[$r['id']]['total'] += $r['count'];
            $list[$r['id']]['unregistred']['total'] += $r['count'];
            $list[$r['id']]['unregistred'][$r['status']] = $r['count'];
            $list[$r['id']]['sport'] = $r['sport_id'];
            $list[$r['id']]['created_at'] = new \DateTime($r['created_at']) ;
            $list[$r['id']]['last_attendance'] = new \DateTime($r['last_attendance']) ;
        }
        
        $totalOverview = array(
            'total_players_3_less' => 0,
            'total_players_3' => 0,
            'total_players_10' => 0,
            'total_players_20' => 0,
            'total_players_30_more' => 0,
            'total_players_confirmed_3_less' => 0,
            'total_players_confirmed_3' => 0,
            'total_players_confirmed_10' => 0,
            'total_players_confirmed_20' => 0,
            'total_players_confirmed_30_more' => 0,
            
        );
        
        
        foreach($list as $key => $l)
        {
            if($l['registred']['confirmed'] >= 30)
            {
                $totalOverview['total_players_confirmed_30_more']++;
            }
            elseif($l['registred']['confirmed'] >= 20)
            {
                 $totalOverview['total_players_confirmed_20']++;
            }
            elseif($l['registred']['confirmed'] >= 10)
            {
                 $totalOverview['total_players_confirmed_10']++;
            }
            elseif($l['registred']['confirmed'] >= 3)
            {
                 $totalOverview['total_players_confirmed_3']++;
            }
            else
            {
                 $totalOverview['total_players_confirmed_3_less']++;
            }
            
            /*
            if(array_key_exists('confirmed', $l['registred']))
            {
                if($l['registred']['total'] >= 30)
                {
                    $totalOverview['total_players_confirmed_30_more']++;
                }
                elseif($l['registred']['total'] >= 20)
                {
                     $totalOverview['total_players_confirmed_20']++;
                }
                elseif($l['registred']['total'] >= 10)
                {
                     $totalOverview['total_players_confirmed_10']++;
                }
                elseif($l['registred']['total'] >= 3)
                {
                     $totalOverview['total_players_confirmed_3']++;
                }
                else
                {
                     $totalOverview['total_players_confirmed_3_less']++;
                }
            }
               */
            
            //t_dump($l['registred']['total']);
            
            if($l['registred']['total'] >= 30)
            {
                $totalOverview['total_players_30_more']++;
            }
            elseif($l['registred']['total'] >= 20)
            {
                 $totalOverview['total_players_20']++;
            }
            elseif($l['registred']['total'] >= 10)
            {
                 $totalOverview['total_players_10']++;
            }
            elseif($l['registred']['total'] >= 3)
            {
                 $totalOverview['total_players_3']++;
            }
            else
            {
                 $totalOverview['total_players_3_less']++;
            }
        }
        
        
        t_dump($totalOverview);
        return array('list' => $list, 'totalOverview' => $totalOverview);
    }

}
