<?php
namespace Base\Model;
use Core\DbStorage;
class StatsStorage extends DbStorage {
    

        
        public function getStats($dateFrom,$dateTo)
        {
             $data = \Core\DbQuery::prepare('
               select * from team where created_at > STR_TO_DATE(:date_from,"%d.%m.%Y") and  created_at < STR_TO_DATE(:date_to,"%d.%m.%Y")')
                ->bindParam('date_from', $dateFrom)
                ->bindParam('date_to', $dateTo)
		->execute()
		->fetchAll(\PDO::FETCH_ASSOC);
		return $data;
        }
        
      
        public function getUserSumStats()
        {
             $data = \Core\DbQuery::prepare('
              SELECT  DATE_FORMAT(date_created,\'%Y-%m\') as month, count(*) as count, user_type, default_lang from co_users  WHERE date_created is not null
GROUP BY  DATE_FORMAT(date_created,\'%Y-%m\'), user_type, default_lang ORDER BY  month, default_lang, user_type   ')
               
		->execute()
		->fetchAll(\PDO::FETCH_ASSOC);
		return $data;
        }
        
        public function getTeamSumStats()
        {
             $data = \Core\DbQuery::prepare('
              Select  DATE_FORMAT(created_at,\'%Y-%m\') as month,age_group,level,gender, count(*) as count from team 
group by  DATE_FORMAT(created_at,\'%Y-%m\'), age_group, level order by  month')
               
		->execute()
		->fetchAll(\PDO::FETCH_ASSOC);
		return $data;
        }
        
        public function getTeamPlayersStats($criteria = array())
        {
            $criteriaSql = ' WHERE 1 = 1 ';
            if(array_key_exists('registred',$criteria))
            {
                if($criteria['registred'] == 'yes')
                {
                    $criteriaSql .= ' AND  tp.player_id > 0 ';
                }
                
                if($criteria['registred'] == 'no')
                {
                    $criteriaSql .= ' AND  tp.player_id = 0 ';
                }

                
            }
            
          
            
            $data = \Core\DbQuery::prepare('
                select  t.id, t.name, tp.status, t.sport_id, t.created_at, a.c, count(*) as count
                from team_players tp
                LEFT JOIN team t On tp.team_id = t.id
                LEFT JOIN (
                    select tp.team_id,  MAX(a.created_at) as last_attendance from team_event_attendance a 
                    LEFT JOIN team_players tp ON a.team_player_id = tp.player_id
                    GROUP BY tp.team_id
                ) a ON tp.team_id = a.team_id
                '.$criteriaSql.'
                group by tp.status, tp.team_id
                order by tp.team_id')
               
		->execute()
		->fetchAll(\PDO::FETCH_ASSOC);
                return $data;
        }
        
      
	
	
}