<?php

namespace Base\Model;

use Core\EntityMapper as EntityMapper;
use Core\ServiceLayer as ServiceLayer;

class Translation {

    private $repository;
    private $mapper_rules = array(
        'id' => array(
            'type' => 'int',
            'max_length' => '10',
            'required' => true
        ),
        'keycode' => array(
            'type' => 'string',
            'max_length' => '255',
            'required' => false
        ),
        'lang' => array(
            'type' => 'string',
            'max_length' => '20',
            'required' => false
        ),
        'sk_value' => array(
            'type' => 'string',
            'max_length' => '255',
            'required' => false
        ),
        'en_value' => array(
            'type' => 'string',
            'max_length' => '255',
            'required' => false
        ),
        'sk_tooltip' => array(
            'type' => 'string',
            'max_length' => '2000',
            'required' => false
        ),
        'en_tooltip' => array(
            'type' => 'string',
            'max_length' => '2000',
            'required' => false
        ),
    );

    public function getDataMapperRules()
    {
        return $this->mapper_rules;
    }

    protected $id;
    protected $keycode;
    protected $lang;
    protected $skValue;
    protected $enValue;
    protected $skTooltip;
    protected $enTooltip;

    public function setId($val)
    {
        $this->id = $val;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setKeycode($val)
    {
        $this->keycode = $val;
    }

    public function getKeycode()
    {
        return $this->keycode;
    }

    public function setLang($val)
    {
        $this->lang = $val;
    }

    public function getLang()
    {
        return $this->lang;
    }

    function getSkValue()
    {
        return $this->skValue;
    }

    function getEnValue()
    {
        return $this->enValue;
    }

    function setSkValue($skValue)
    {
        $this->skValue = $skValue;
    }

    function setEnValue($enValue)
    {
        $this->enValue = $enValue;
    }

    function getSkTooltip()
    {
        return $this->skTooltip;
    }

    function getEnTooltip()
    {
        return $this->enTooltip;
    }

    function setSkTooltip($skTooltip)
    {
        $this->skTooltip = $skTooltip;
    }

    function setEnTooltip($enTooltip)
    {
        $this->enTooltip = $enTooltip;
    }

}
