<?php

namespace Base\Model;

use Core\Repository as Repository;

class QueueRepository extends Repository {

    protected $storage;

    public function __construct($storage)
    {
        $this->setStorage($storage);
    }

    public function getStorage()
    {
        return $this->storage;
    }

    public function setStorage($storage)
    {
        $this->storage = $storage;
    }
    
    public function getWaitingRecords($type)
    {
         $data = $this->getStorage()->findBy(array('action_type' => $type ,'status' => 'waiting'));
         return $data;
    }

    public function addRecord($data)
    {
        $data['status'] = 'waiting';
        
        $this->getStorage()->create($data);
    }
    public function finishRecord($id)
    {
        $this->getStorage()->update($id,array('status' => 'finished'));
    }

}
