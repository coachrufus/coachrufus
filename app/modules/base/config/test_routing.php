<?php

$acl->allowRole(array('ROLE_USER'),'locality-test');
$router->addRoute('locality-test','/test/functional/locality',array(
		'controller' => 'PH\Base\BaseModule:Test:locality'
));


$acl->allowRole(array('ROLE_USER'),'flch-import');
$router->addRoute('flch-import','/flch/import',array(
		'controller' => 'PH\Base\BaseModule:Test:flchImport'
));

$acl->allowRole(array('ROLE_USER'),'fake-register-import');
$router->addRoute('fake-register-import','/fake/register-import',array(
		'controller' => 'PH\Base\BaseModule:Test:fakeRegistredPlayers'
));