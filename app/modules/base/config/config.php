<?php
namespace PH\Base;
require_once(MODUL_DIR.'/base/BaseModule.php');


use Core\ServiceLayer as ServiceLayer;



ServiceLayer::addService('TranslationRepository',array(
'class' => "Base\Model\TranslationRepository",
	'params' => array(
		new \Core\DbStorage('translations'),
		new \Core\EntityMapper('Base\Model\Translation')
	)
));

ServiceLayer::addService('StatsRepository',array(
'class' => "Base\Model\StatsRepository",
	'params' => array(
                new \Base\Model\StatsStorage('team')
	)
));

ServiceLayer::addService('QueueRepository',array(
'class' => "Base\Model\QueueRepository",
	'params' => array(
                new \Base\Model\StatsStorage('queue')
	)
));



$acl = ServiceLayer::getService('acl');
$acl->allowRole(array('ROLE_USER'),'filebrowser');

//routing
$router = ServiceLayer::getService('router');
$router->addRoute('filebrowser','/filebrowser',array(
		'controller' => 'PH\Base\BaseModule:Base:filebrowser'
));

$acl->allowRole(array('guest'),'base_error');
$router->addRoute('base_error','/error',array(
		'controller' => 'PH\Base\BaseModule:Base:error'
));

$acl->allowRole(array('guest'),'maintenance');
$router->addRoute('maintenance','/maintenance',array(
		'controller' => 'PH\Base\BaseModule:Base:maintenance'
));

$acl->allowRole(array('guest'),'base_home');
$router->addRoute('base_home','/',array(
		'controller' => 'PH\Base\BaseModule:Base:home'
));


$acl->allowRole(array('guest'),'switch_lang');
$router->addRoute('switch_lang','/switch-lang',array(
		'controller' => 'PH\Base\BaseModule:Translator:switchLang'
));

$acl->allowRole(array('guest'),'base_layout_settings');
$router->addRoute('base_layout_settings','/base/layout/settings',array(
		'controller' => 'PH\Base\BaseModule:Base:layoutSetting'
));


$acl->allowRole(array('guest'),'base_fb');
$router->addRoute('base_fb','/fb',array(
		'controller' => 'PH\Base\BaseModule:Base:fb'
));


$acl->allowRole(array('guest'),'import_kapsa');
$router->addRoute('import_kapsa','/import-kapsa',array(
		'controller' => 'PH\Base\BaseModule:Base:importKapsa'
));

$acl->allowRole(array('guest'),'import_ms');
$router->addRoute('import_ms','/import-ms',array(
		'controller' => 'PH\Base\BaseModule:Base:importMS'
));

$acl->allowRole(array('ROLE_USER'),'translator_list');
$router->addRoute('translator_list','/translator/list',array(
		'controller' => 'PH\Base\BaseModule:Translator:index'
));
$acl->allowRole(array('ROLE_USER'),'translation_edit');
$router->addRoute('translation_edit','/translator/edit',array(
		'controller' => 'PH\Base\BaseModule:Translator:edit'
));

$acl->allowRole(array('ROLE_USER'),'test_request');
$router->addRoute('test_request','/test-request',array(
		'controller' => 'PH\Base\BaseModule:Test:request'
));


$acl->allowRole(array('ROLE_USER'),'test_request_1');
$router->addRoute('test_request_1','/test-request-1',array(
		'controller' => 'PH\Base\BaseModule:Test:request1'
));

$acl->allowRole(array('ROLE_USER'),'test_request_2');
$router->addRoute('test_request_2','/test-request-2',array(
		'controller' => 'PH\Base\BaseModule:Test:request2'
));


$acl->allowRole(array('ROLE_USER'),'parse_url');
$router->addRoute('parse_url','/parse-url',array(
		'controller' => 'PH\Base\BaseModule:Test:parseUrl'
));

$acl->allowRole(array('ROLE_USER'),'stats_overview_summary');
$router->addRoute('stats_overview_summary','/stats/summary',array(
		'controller' => 'PH\Base\BaseModule:Stats:overviewSummary'
));

$acl->allowRole(array('ROLE_USER'),'stats_overview_summary_download');
$router->addRoute('stats_overview_summary_download','/stats/summary/download',array(
		'controller' => 'PH\Base\BaseModule:Stats:downloadStats'
));

$acl->allowRole(array('ROLE_USER'),'stats_overview');
$router->addRoute('stats_overview','/stats',array(
		'controller' => 'PH\Base\BaseModule:Stats:overview'
));



$acl->allowRole(array('ROLE_USER'),'sbs_overview');
$router->addRoute('sbs_overview','/sbs-stat',array(
		'controller' => 'PH\Base\BaseModule:Stats:stepByStep'
));

$acl->allowRole(array('ROLE_USER'),'teams_stats_overview');
$router->addRoute('teams_stats_overview','/team-stats',array(
		'controller' => 'PH\Base\BaseModule:Stats:allTeamsOverview'
));


$acl->allowRole(array('guest'),'update_sport');
$router->addRoute('update_sport','/update-sport',array(
		'controller' => 'PH\Base\BaseModule:Base:updateSports'
));

$acl->allowRole(array('ROLE_USER'),'mailchimp_export');
$router->addRoute('mailchimp_export','/mailchimp',array(
		'controller' => 'PH\Base\BaseModule:Base:mailchimpExport'
));

$acl->allowRole(array('ROLE_USER'),'pro_trial');
$router->addRoute('pro_trial','/pro-trial',array(
		'controller' => 'PH\Base\BaseModule:Base:trialPro'
));

$acl->allowRole(array('ROLE_USER'),'step_finish');
$router->addRoute('step_finish','/step-finish',array(
		'controller' => 'PH\Base\BaseModule:Base:finishedStepByStep'
));

$acl->allowRole(array('ROLE_USER'),'flch_pro');
$router->addRoute('flch_pro','/flch-pro',array(
		'controller' => 'PH\Base\BaseModule:Base:flchPro'
));

$acl->allowRole(array('ROLE_USER'),'test_dev');
$router->addRoute('test_dev','/test-dev',array(
		'controller' => 'PH\Base\BaseModule:Base:testDev'
));


$acl->allowRole(array('ROLE_USER'),'users_check');
$router->addRoute('users_check','/users-check',array(
		'controller' => 'PH\Base\BaseModule:Base:usersCheck'
));



include_once 'monitor_routing.php';
include_once 'test_routing.php';