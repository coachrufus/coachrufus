
<section class="content-header">
    <h1>
        <?php echo $translator->translate('Translations') ?>
    </h1>
    
   
</section>
<section class="content">
    
     <div class="box box-primary">
         <div class="box-header with-border">
             
         </div>
         <div class="box-body">
             <table id="trans-table" class="table">
                 <thead>
                     <tr>
                         <th><?php echo $translator->translate('Code') ?></th>
                         <th>SK</th>
                         <th>EN/th>
                     </tr>
                 </thead>
                 <tbody>
                     <?php foreach($list as $trans): ?>
                     <tr>
                         <td><a href="<?php echo $router->link('translation_edit',array('id' => $trans->getId())) ?>"><?php echo $trans->getKeycode() ?></a></td>
                         <td>

                             <?php echo $trans->getSkValue() ?>
                         
                         </td>
                         <td><?php echo $trans->getEnValue() ?></td>
                     </tr>
                     <?php endforeach; ?>
                 </tbody>
             </table>
             
             
         </div>
      
      </div>
        



</section>


<?php $layout->addStylesheet('plugins/datatables/dataTables.bootstrap.css') ?>
<?php $layout->addJavascript('plugins/datatables/jquery.dataTables.min.js') ?>
<?php $layout->addJavascript('plugins/datatables/dataTables.bootstrap.min.js') ?>

<?php $layout->startSlot('javascript') ?>

 <script>
      $(function () {
        $("#trans-table").DataTable({

        });
        
     
       
      });
      
      
      
    </script>
  
<?php $layout->endSlot('javascript') ?>