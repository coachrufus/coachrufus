
<section class="content-header">
    <h1>
        <?php echo $translator->translate('Edit') ?>
    </h1>
    
   
</section>
<section class="content">
    
     <div class="box box-primary">
         <div class="box-header with-border">
             <h3 ><?php echo $form->getEntity()->getKeycode()  ?></h3>
         </div>
         <div class="box-body">

        <form role="form"  action="" method="post">

             
             <div class="form-group col-sm-1">
                <label class="control-label">SK</label>
            </div>
             <div class="form-group col-sm-4">
                 <?php echo $form->renderInputTag("sk_value", array('class' => 'form-control')) ?>
            </div>
            
             <div class="form-group col-sm-1">
                <label class="control-label">EN</label>
            </div>
             <div class="form-group col-sm-4">
                <?php echo $form->renderInputTag("en_value", array('class' => 'form-control')) ?>
            </div>
            
            
              <div class="form-group col-sm-1">
                <label class="control-label">SK Tooltip</label>
            </div>
             <div class="form-group col-sm-4">
                 <?php echo $form->renderInputTag("sk_tooltip", array('class' => 'form-control')) ?>
            </div>
            
             <div class="form-group col-sm-1">
                <label class="control-label">EN tooltip</label>
            </div>
             <div class="form-group col-sm-4">
                <?php echo $form->renderInputTag("en_tooltip", array('class' => 'form-control')) ?>
            </div>
            
            
              
          
           
        
        
                 <button type="submit" class="btn btn-primary">Submit</button>

               
                </form>
     


         </div>
      </div>
        



</section>
