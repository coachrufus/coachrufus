

<section class="content">
    <a id="request1-trigger" href="">Test 1</a>
    <input style="width:100%;" id="test1-status" />
    <textarea style="width:100%; height:200px;" id="test1-response"></textarea>

    <a  id="request2-trigger" href="">Test 2</a>
    <input style="width:100%;" id="test2-status" />
    <textarea style="width:100%; height:200px;" id="test2-response"></textarea>
    
    <a  id="request3-trigger" href="">Test 3</a>
    <input style="width:100%;" id="test3-status" />
    <textarea style="width:100%; height:200px;" id="test3-response"></textarea>
</section>
<?php $layout->startSlot('javascript') ?>
<script type="text/javascript">



    $('#request1-trigger').on('click', function (e) {
        e.preventDefault();

        $.ajax({
            method: "GET",
            url: '<?php echo $router->link('test_request_1') ?>'
        }).done(function (response) {


            $('#test1-response').val(response);

        }).fail(function (xhr, ajaxOptions, thrownError) {

            $('#test1-response').val('Error: ' + xhr.status + thrownError + xhr.responseText);
        }).always(function (xhr, ajaxOptions, thrownError) {

            $('#test1-status').val('Status: ' + xhr.status);
        });
    });


    $('#request2-trigger').on('click', function (e) {
        e.preventDefault();

        $.ajax({
            method: "GET",
            url: '<?php echo $router->link('test_request_2') ?>'
        }).done(function (response) {

            $('#test2-response').val(response);

        }).fail(function (xhr, ajaxOptions, thrownError) {


            $('#test2-response').val('Error: ' + xhr.status + thrownError + xhr.responseText);
        }).always(function (xhr, ajaxOptions, thrownError) {

            $('#test2-status').val('Status: ' + xhr.status);
        });
    });

    $('#request3-trigger').on('click', function (e) {
        e.preventDefault();

        $.ajax({
            method: "GET",
            url: '/team/change-players-attendance?event_id=1021&event_date=2016-05-26&type=in&player_id=457&team_player_id=457'
        }).done(function (response) {

            $('#test3-response').val(response);

        }).fail(function (xhr, ajaxOptions, thrownError) {


            $('#test3-response').val('Error: ' + xhr.status + thrownError + xhr.responseText);
        }).always(function (xhr, ajaxOptions, thrownError) {

            $('#test3-status').val('Status: ' + xhr.status);
        });
    });


</script>

<?php $layout->endSlot('javascript') ?>