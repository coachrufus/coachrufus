
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="apple-touch-icon" href="http://app.coachrufus.com/apple_icon.png">
        <title> Coach Rufus</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css">
        <link href='/dev/theme/coachrufus/layout.css?ver=683' rel='stylesheet' type='text/css' media='screen' /> 
        <style>
            a.btn-primary {
                background-color: #00B3BE;
                border-color: #00B3BE;
                font-size: 16px;
                padding: 9px 20px;
                color:white;
            }

            a.btn {
                border-radius: 3px;
                -webkit-box-shadow: none;
                box-shadow: none;
                border: 1px solid transparent;
            }
        </style>
    </head>
    <body class='error-page'>


        <div class="container">
            <div class="row">    

                <div class="col-sm-8 col-sm-offset-2 error-wrap">
                    <div class="row">
                        <div class='col-xs-4'>
                            <img class="rufus" src='/img/error/rufus.png' />
                        </div>

                        <div class='col-xs-8 error-text'>
                            <h1>Ooops!</h1>
                            <strong><?php echo $translator->translate("We'll try to fix this ASAP!") ?></strong><br /><br />
                            <a class="btn btn-primary" href="<?php  echo $router->link('player_dashboard', array('userid' =>  Core\ServiceLayer::getService('security')->getIdentity()->getUser()->getId())) ?>"><?php echo $translator->translate('ERROR_CTA') ?></a>
                            
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>

