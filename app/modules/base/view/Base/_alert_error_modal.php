<div class="modal fade" id="base-alert-error-modal" tabindex="-1" role="dialog" aria-labelledby="<?php echo $translator->translate('System Alert') ?>">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $translator->translate('System Alert') ?></h4>
      </div>
      <div class="modal-body">
          <div class="alert alert-danger">
              <h2><?php echo $translator->translate('System error') ?></h2>
              <p><?php echo $translator->translate('SYSTEM_ERROR_TEXT') ?></p>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $translator->translate('Close') ?></button>
      </div>
    </div>
  </div>
</div>
