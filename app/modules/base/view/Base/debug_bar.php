<?php
$web_app = Core\WebApp::getInstance();
$layout = Core\ServiceLayer::getService('layout');

$debug = $web_app->getDebugInfo();
$debug['layout'] = $layout->getTemplate();
$router = Core\ServiceLayer::getService('router');


?>

<div id="debug-bar">
     <h3>Current</h3>
    <table class="table">
        <tbody>
            <tr>
                <td>controller</td>
                <td><?php echo $debug['controller'] ?></td>
            </tr>
            <tr>
                <td>action</td>
                <td><?php echo $debug['action'] ?></td>
            </tr>
            <tr>
                <td>layout</td>
                <td><?php echo $debug['layout'] ?></td>
            </tr>
        </tbody>
    </table>
     
     <h3>Session</h3>
     <?php var_dump($_SESSION)?>
     
      <h3>REQUEST</h3>
     <?php var_dump($_REQUEST)?>
     
      <h3>SERVER</h3>
     <?php var_dump($_SERVER)?>
     
    
    <h3>Routes</h3>
    <table class="table">
        <thead>
            <tr>
                <th>route</th>
                     <th>url</th> 
                     <th>action</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($router->getRoutes() as $route_name => $route): ?>
                <tr>
                    <td><?php echo$route_name  ?></td>
                    <td><?php echo $route['pattern'] ?></td>
                    <td><?php echo $route['options']['controller'] ?></td>
                </tr>
            <?php endforeach; ?>
         
        </tbody>
    </table>
   
</div>

