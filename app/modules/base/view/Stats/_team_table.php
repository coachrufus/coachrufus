 <table class="table" id="team_table">
                        <thead>
                            <tr>
                                <th>Mesiac</th>
                                <th>Celkovo</th>
                                <th>0-13</th>
                                <th>13-17</th>
                                <th>18-24</th>
                                <th>25-34</th>
                                <th>45-54</th>
                                <th>55-64</th>
                                <th>65+</th>
                                <th>Muži</th>
                                <th>Ženy</th>
                                <th>Zmiešaný</th>
                                <th>Beginner</th>
                                <th>Advanced</th>
                                <th>Expert</th>
                                <th>league</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($teamsSummary as $month => $Tsummary): ?>
                            <tr>
                                <td><?php echo $month ?></td>
                                <td>
                                    <?php echo $Tsummary['total'] ?>
                                </td>
                                <td>
                                    <?php echo $Tsummary['age_groups']['0-13'] ?>
                                </td>
                                <td>
                                    <?php echo $Tsummary['age_groups']['13-17'] ?>
                                </td>
                                <td>
                                    <?php echo $Tsummary['age_groups']['18-24'] ?>
                                </td>
                                <td>
                                    <?php echo $Tsummary['age_groups']['25-34'] ?>
                                </td>
                                <td>
                                    <?php echo $Tsummary['age_groups']['45-54'] ?>
                                </td>
                                <td>
                                    <?php echo $Tsummary['age_groups']['55-64'] ?>
                                </td>
                                <td>
                                    <?php echo $Tsummary['age_groups']['65+'] ?>
                                </td>

                                <td>
                                     <?php echo $Tsummary['gender']['man'] ?>
                                </td>
                                <td>
                                     <?php echo $Tsummary['gender']['mixed'] ?>
                                </td>
                                <td>
                                     <?php echo $Tsummary['gender']['woman'] ?>
                                </td>
                                 <td>
                                    <?php echo $Tsummary['levels']['beginner'] ?>
                                </td>
                                 <td>
                                    <?php echo $Tsummary['levels']['advanced'] ?>
                                </td>
                                 <td>
                                    <?php echo $Tsummary['levels']['expert'] ?>
                                </td>
                                 <td>
                                    <?php echo $Tsummary['levels']['league'] ?>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                   