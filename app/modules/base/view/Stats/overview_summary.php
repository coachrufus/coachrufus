


<section class="content-header">

</section>


<section class="content">
    <div class="row">
        <div class="col-xs-12 ">
            <h3>Prehľad užívateľov</h3>
            <div class="box">
                <div class="box-body">
                    <?php $layout->includePart(MODUL_DIR . '/base/view/Stats/_users_table.php',array('usersSummary' => $usersSummary)) ?>
                    <a class="btn btn-success" href="<?php echo $router->link('stats_overview_summary_download',array('t' => 'users_table','h' => 'OwwCcosFVy9K8Y4yojvz')) ?>">Stiahnut xls</a>
                   
                </div>
            </div>
        </div>
        
        
        
        <div class="col-xs-12 ">
            <h3>Prehľad tímov</h3>
            <div class="box">
                <div class="box-body">
                    <?php $layout->includePart(MODUL_DIR . '/base/view/Stats/_team_table.php',array('teamsSummary' => $teamsSummary)) ?>
                    <a class="btn btn-success" href="<?php echo $router->link('stats_overview_summary_download',array('t' => 'team_table','h' => 'OwwCcosFVy9K8Y4yojvz')) ?>">Stiahnut xls</a>
                </div>
            </div>
        </div>
        
         <div class="col-xs-12 ">
            <h3>Prehľad tímových hráčov</h3>
            <div class="box">
                <div class="box-body">

                    <h4>Ceľkový prehľad všetkých hráčov</h4>
                   <?php $layout->includePart(MODUL_DIR . '/base/view/Stats/_team_players_table.php',array('teamPlayerSummary' => $teamPlayerSummary,'sportEnum' => $sportEnum)) ?>
                    <a class="btn btn-success" href="<?php echo $router->link('stats_overview_summary_download',array('t' => 'team_players_table','h' => 'OwwCcosFVy9K8Y4yojvz')) ?>">Stiahnut xls</a>
                </div>
            </div>
        </div>

    </div>
</section>



<?php $layout->addStylesheet('plugins/datatables/dataTables.bootstrap.css') ?>
<?php $layout->addJavascript('plugins/datatables/jquery.dataTables.min.js') ?>
<?php $layout->addJavascript('plugins/datatables/dataTables.bootstrap.min.js') ?>


<?php $layout->addStylesheet('plugins/pickadate/themes/classic.css') ?>
<?php $layout->addStylesheet('plugins/pickadate/themes/classic.date.css') ?>
<?php $layout->addJavascript('plugins/pickadate/picker.js') ?>
<?php $layout->addJavascript('plugins/pickadate/picker.date.js') ?>



<?php $layout->startSlot('javascript') ?>

<script>
    
    jQuery('#date_from').pickadate({'format': 'dd.mm.yyyy'});
    jQuery('#date_to').pickadate({'format': 'dd.mm.yyyy'});
    
    $(function () {
        $("#users_table").DataTable({

        });
        $("#team_table").DataTable({

        });
        $("#team_players_table").DataTable({

        });



    });



</script>

<?php $layout->endSlot('javascript') ?>
