<table class="table" id="users_table">
    <thead>
        <tr>
            <th>Mesiac</th>
            <th>Celkovo</th>
            <th>Direct</th>
            <th>Social</th>
            <th>Sk</th>
            <th>En</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($usersSummary as $month => $Tsummary): ?>
        <tr>
            <td><?php echo $month ?></td>
            <td>
                <?php echo $Tsummary['total'] ?>
            </td>
            <td>
                <?php echo $Tsummary['user_types']['Direct']  ?>
            </td> 
            <td>
                <?php echo $Tsummary['user_types']['social_user']  ?>
            </td> 
             <td>
                <?php echo $Tsummary['default_langs']['sk']  ?>
            </td> 

             <td>
                <?php echo $Tsummary['default_langs']['en']  ?>
            </td> 
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
