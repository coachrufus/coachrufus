


<section class="content-header">

</section>


<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    
                    <form action="">
                        Od:<input type="text" id="date_from" name="date_from" value="<?php echo $dateFrom ?>" />
                        Do:<input type="text" id="date_to" name="date_to" value="<?php echo $dateTo ?>" />
                        <button type="submit">Filter</button>
                        <input type="hidden" name="t" value="OwwCcosFVy9K8Y4yojvz" />
                    </form>
                    
                    <table class="table table-bordered" id="stat_table">
                        <thead>
                            <tr>
                                <th>Názov</th>
                                <th>Dátum vytvorenia</th>
                                <th>Šport</th>
                                <th>Level</th>
                                <th>Pohlavie</th>
                                <th>Popis</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($teams as $team): ?>
                                <tr>
                                    <td><?php echo $team['name'] ?> </td>
                                    <td><?php echo date('d.m.Y H:i:s',strtotime($team['created_at'])) ?> </td>
                                    <td><?php echo $sportChoices[$team['sport_id']] ?> </td>
                                    <td><?php echo $team['level'] ?> </td>
                                    <td><?php echo $team['gender'] ?> </td>
                                    <td><?php echo $team['description'] ?> </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
</section>






<?php $layout->addStylesheet('plugins/datatables/dataTables.bootstrap.css') ?>
<?php $layout->addJavascript('plugins/datatables/jquery.dataTables.min.js') ?>
<?php $layout->addJavascript('plugins/datatables/dataTables.bootstrap.min.js') ?>


<?php $layout->addStylesheet('plugins/pickadate/themes/classic.css') ?>
<?php $layout->addStylesheet('plugins/pickadate/themes/classic.date.css') ?>
<?php $layout->addJavascript('plugins/pickadate/picker.js') ?>
<?php $layout->addJavascript('plugins/pickadate/picker.date.js') ?>



<?php $layout->startSlot('javascript') ?>

<script>
    
    jQuery('#date_from').pickadate({'format': 'dd.mm.yyyy'});
    jQuery('#date_to').pickadate({'format': 'dd.mm.yyyy'});
    
    $(function () {
        $("#stat_table").DataTable({

        });



    });



</script>

<?php $layout->endSlot('javascript') ?>