 <table class="table" id="team_players_table">
                        <thead>
                            <tr>
                                <th>Tím</th>
                                <th>Dátum reg.</th>
                                <th>Posledná dochádzka</th>
                                <th>Šport</th>
                                <th>Celkovo</th>
                                <th>Reg. celkovo</th>
                                <th>Reg. conf.</th>
                                <th>Reg. wait.conf.</th>
                                <th>Reg. unknown</th>
                                <th>Reg. unconf.</th>
                                <th>Reg. unactive</th>
                                <th>Reg. decline</th>
                                <th>Ner. celkovo</th>
                                <th>Ner. conf.d</th>
                                <th>Ner. wait.conf.</th>
                                <th>Ner. unknown</th>
                                <th>Ner. unconf.</th>
                                <th>Ner. unactive</th>
                                <th>Ner. decline</th>
                               
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($teamPlayerSummary['list'] as $teamSummary): ?>
                            <tr>
                                <td><?php echo $teamSummary['name'] ?></td>
                                <td><?php echo $teamSummary['created_at']->format('d.m. Y') ?></td>
                                <td><?php echo $teamSummary['last_attendance']->format('d.m. Y') ?></td>
                                <td><?php echo $sportEnum[$teamSummary['sport']]  ?></td>
                                <td><?php echo $teamSummary['total'] ?></td>
                                <td><?php echo $teamSummary['registred']['total']  ?></td>
                                <td><?php echo $teamSummary['registred']['confirmed']  ?></td>
                                <td><?php echo $teamSummary['registred']['waiting-to-confirm']  ?></td>
                                <td><?php echo $teamSummary['registred']['unknown']  ?></td>
                                <td><?php echo $teamSummary['registred']['unconfirmed']  ?></td>
                                <td><?php echo $teamSummary['registred']['unactive']  ?></td>
                                <td><?php echo $teamSummary['registred']['decline']  ?></td>
                                <td><?php echo $teamSummary['unregistred']['total']  ?></td>
                                <td><?php echo $teamSummary['unregistred']['confirmed']  ?></td>
                                <td><?php echo $teamSummary['unregistred']['waiting-to-confirm']  ?></td>
                                <td><?php echo $teamSummary['unregistred']['unknown']  ?></td>
                                <td><?php echo $teamSummary['unregistred']['unconfirmed']  ?></td>
                                <td><?php echo $teamSummary['unregistred']['unactive']  ?></td>
                                <td><?php echo $teamSummary['unregistred']['decline']  ?></td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                   