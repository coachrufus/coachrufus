
            <table class="table resp-table player-list-table">
                <thead>
                <th>Id</th>
                <th>Názov</th>
                <th>Vytvoreny</th>
                <th>Email</th>
                <th>Adresa</th>
                <th>Kraj</th>
                <th>Okres</th>
                <th>Mesto</th>
                <th>Pohlavie</th>
                <th>Turnaje</th>
                </thead>
                
                <tbody>
                    <?php foreach($teams as $teamData): ?>
                    <?php $team = $teamData['team'] ?>
                    
                    <tr>
                        <td><?php echo $team->getId()  ?></td>
                        <td><?php echo $team->getName()  ?></td>
                        <td>
                           <?php echo ($team->getCreatedAt() != null ) ? $team->getCreatedAt()->format('d.m.Y H:i:s') : ''  ?>
                        
                        </td>
                        <td><a href="mailto:<?php echo $team->getCreatorEmail() ?>"><?php echo $team->getCreatorEmail() ?></a></td>
                        <td>
                            <?php if($team->getLocality() != null): ?>
                                <?php echo  $team->getLocality()->getFullAddress() ?>
                            <?php endif; ?>
                        </td>
                        <td>
                            <?php if($team->getLocality() != null): ?>
                                <?php echo  $team->getLocality()->getRegion() ?>
                            <?php endif; ?>
                        </td>
                        <td>
                            <?php if($team->getLocality() != null): ?>
                                <?php echo  $team->getLocality()->getDistrict() ?>
                            <?php endif; ?>
                        </td>
                        <td>
                            <?php if($team->getLocality() != null): ?>
                                <?php echo  $team->getLocality()->getCity2() ?>
                            <?php endif; ?>
                        </td>
                        <td>
       
                                <?php echo  $team->getGender() ?>

                        </td>
                        <td><?php echo implode('###',$teamData['tournaments']) ?></td>
                      
                    </tr>
                    
                    <?php endforeach; ?>
                </tbody>
            </table>
     