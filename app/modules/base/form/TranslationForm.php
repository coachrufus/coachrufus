<?php
namespace  Base\Form;
use \Core\Form as Form;
use  Base\Model\Translation as Translation;

class TranslationForm extends Form 
{
	protected $fields = array (
  'id' => 
  array (
    'type' => 'int',
    'max_length' => '10',
    'required' => true,
  ),
  'keycode' => 
  array (
    'type' => 'string',
    'max_length' => '255',
    'required' => false,
  ),
  'lang' => 
  array (
    'type' => 'choice',
    'max_length' => '20',
    'required' => false,
	'choices' => array('' => '','sk' => 'sk','en' =>'en'),
  ),
  'sk_value' => 
  array (
    'type' => 'string',
    'max_length' => '255',
    'required' => false,
  ),
  'en_value' => 
  array (
    'type' => 'string',
    'max_length' => '255',
    'required' => false,
  ),
             'sk_tooltip' => 
  array (
    'type' => 'string',
    'max_length' => '2000',
    'required' => false,
  ),
    'en_tooltip' => 
  array (
    'type' => 'string',
    'max_length' => '2000',
    'required' => false,
  ),
);
				
	public function __construct() 
	{

	}
}