<?php
namespace PH\Base;
use Core\Module as Module;
class BaseModule extends Module
{
    public function __construct() 
    {
        $this->setBaseDir(MODUL_DIR.'/base');
        $this->setName('base');
    }
}

