<?php

namespace PH\Base\Controller;

use Base\Form\TranslationForm;
use Core\Controller as Controller;
use Core\ServiceLayer;
use Core\Validator;

class TranslatorController extends Controller {

    public function switchLangAction()
    {
        $lang = $this->getRequest()->get('lang');
        $_SESSION['app_lang'] = $lang;
        $this->getRequest()->redirect();
        
    }
    
    public function indexAction()
    {

        $repository = ServiceLayer::getService('TranslationRepository');
        $list = $repository->findAll(array('order' => ' keycode asc'));

        return $this->render('PH\Base\BaseModule:Translator:index.php', array(
                    'list' => $list,
        ));
    }

    public function editAction()
    {
        $request = ServiceLayer::getService('request');
        $router = ServiceLayer::getService('router');
        $repository = ServiceLayer::getService('TranslationRepository');

        $translation = $repository->find($request->get('id'));
        $form = new TranslationForm();
        $form->setEntity($translation);

        $validator = new Validator();
        $validator->setRules($form->getFields());

        if ('POST' == $request->getMethod())
        {
            $post_data = $request->get('record');
         
            $form->bindData($post_data);

            $validator->setData($post_data);
            $validator->validateData();

            if (!$validator->hasErrors())
            {
                $entity = $form->getEntity();
                $repository->save($entity);

                $request->redirect($router->link('translator_list'));
            }
           
        }



        return $this->render('PH\Base\BaseModule:Translator:edit.php', array(
                    'form' => $form,
                    'request' => $request,
                    'validator' => $validator,
        ));
    }

}
