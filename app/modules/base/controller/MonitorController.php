<?php

namespace PH\Base\Controller;

use Core\Controller as Controller;
use Core\ControllerResponse;
use Core\ServiceLayer;

class MonitorController extends Controller {

    public function notifyErrorAction()
    {
        
        $currentTime = time();
        $filename = APPLICATION_PATH . '/log/exception.log';
        $changeTime = filemtime($filename);
        
        
        
        $limit = 60*10; //10 minutes
        $diff = ($currentTime-$changeTime);
        
        
        $lines=array();
        $fp = fopen($filename, "r");
        while(!feof($fp))
        {
           $line = fgets($fp, 4096);
           array_push($lines, $line);
           if (count($lines)>5)
               array_shift($lines);
        }
        fclose($fp);

        $messageBody = implode("<br /><br />",$lines);
        if($diff < $limit)
        {
            $mailer = ServiceLayer::getService('mailer');
            $message = $mailer->createMessage();
            try
            {
                $message->setSubject('CR_ERROR');
                $message->setFrom(DEFAULT_MAIL_SENDER['email']);
                $message->setTo('marek.hubacek@coachrufus.com');
                $message->setBody($messageBody, 'text/html');
               
                $mailer->sendMessage($message);
            }
            catch(\Exception $e)
            {
                \MailLogger::log($message->getTo(), 'error', $e->getMessage());
            }
        }

        exit;
    }
   

}
