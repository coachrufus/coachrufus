<?php

namespace PH\Base\Controller;

use Core\Controller as Controller;
use Core\ControllerResponse;
use Core\ServiceLayer;

class TestController extends Controller {

    
    public function fakeRegistredPlayersAction()
    {
         $players  = file(APPLICATION_PATH . '/flch/team_players.csv');
         $security = \Core\ServiceLayer::getService('security');
         $sports = array(3,5,15,21,25,26,32,42,49,50,72,108);
         $levels = array('beginner','advanced','expert');
         
         $repo = ServiceLayer::getService('user_repository');
         $playerManager = ServiceLayer::getService('PlayerManager');
         
         array_shift($players);
         $imageTransform = ServiceLayer::getService('imageTransform');
         foreach ($players as $player)
        {
           

             $playerData = explode(';', $player);
            $user = new \User\Model\User();
            $user->setName($playerData[7]);
            $user->setSurname($playerData[8]);
            $user->setSalt(md5(time()));

            $encoded_password = $security->encodePassword('p_' . $playerData[7] . '_cr', $user->getSalt());
            $user->setPassword($encoded_password);
            $user->setEmail($playerData[8] . '@coachrufus.com');
            $user->setPhoto($playerData[0] . '.png');
            $repo->save($user);
            
            $baseName = $playerData[0] . '.png';
            copy(PUBLIC_DIR.'img/team-players/'.$baseName, PUBLIC_DIR.'img/users/'.$baseName);
             
             $imageTransform->resizeImage(array(
                'sourceImg' => PUBLIC_DIR.'img/users/'.$baseName,
                'width' => 90, 
                'height' => 90,
                'targetDir' =>PUBLIC_DIR.'/img/users/thumb_90_90'));

            $imageTransform->resizeImage(array(
                'sourceImg' => PUBLIC_DIR.'img/users/'.$baseName,
                'width' => 320, 
                'height' => 320,
                'targetDir' =>PUBLIC_DIR.'/img/users/thumb_320_320'));

            $imageTransform->resizeImage(array(
                'sourceImg' => PUBLIC_DIR.'img/users/'.$baseName,
                'width' => 640, 
                'height' => 640,
                'targetDir' =>PUBLIC_DIR.'/img/users/thumb_640_640'));

            $imageTransform->resizeImage(array(
                'sourceImg' => PUBLIC_DIR.'img/users/'.$baseName,
                'width' => 960, 
                'height' => 960,
                'targetDir' =>PUBLIC_DIR.'/img/users/thumb_960_960'));

            
            //update team player
             \Core\DbQuery::query("Update team_players set player_id =".$user->getId()." WHERE id=".$playerData[0]);

            //add sports
            $playerSports = array();
            $playerSport = new \Webteamer\Player\Model\PlayerSport();
            $playerSport->setSportId(1);
            $playerSport->setLevel('');
            $playerSports[] = $playerSport;

            shuffle($sports);
            $randSports = array_slice($sports, rand(0, 3),rand(1,3));
         

            foreach ($randSports as $sportId)
            {
                $playerSport = new \Webteamer\Player\Model\PlayerSport();
                $playerSport->setSportId($sportId);
                $playerSport->setLevel($levels[rand(0, 2)]);
                $playerSports[] = $playerSport;
            }
             $playerManager->savePlayerSports($user, $playerSports);

        }
    }
    
    public function flchImportAction()
    {
        $teamManger = ServiceLayer::getService('TeamManager');
        $teamPlayerManager = ServiceLayer::getService('TeamPlayerManager');
        $teamPlayerRepo = ServiceLayer::getService('TeamPlayerRepository');
        $eventManager = ServiceLayer::getService('TeamEventManager');
        $lineupRepo = ServiceLayer::getService('TeamMatchLineupRepository');
        $eventRepo = ServiceLayer::getService('TeamEventRepository');
        $seasonRepo = ServiceLayer::getService('TeamSeasonRepository');
        
        $authorId = 4;
        //$teamId = 3;
        //create team
        
        $team = new \Webteamer\Team\Model\Team();
        $team->setName('FLCH 2017 AA');
        $team->setStatus('public');
        $team->setSportId(42);
        $team->setAuthorId($authorId);
        $teamId = $teamManger->saveTeam($team);
        
        $season = new \CR\TeamSeason\Model\TeamSeason();
        $season->setName('Default');
        $season->setTeamId($teamId);
        $season->setStartDate(new \DateTime('2017-06-01'));
        $season->setEndDate(new \DateTime('2017-06-31'));
        $seasonId =  $seasonRepo->save($season);
        
        
        $players  = file(APPLICATION_PATH . '/flch/players.csv');
       
        //var_dump($players);
        
        $teamPlayer = new \Webteamer\Team\Model\TeamPlayer();
        $teamPlayer->setTeamId($teamId);
        $teamPlayer->setPlayerId(4);
        $teamPlayer->setStatus('confirmed');
        $teamPlayer->setCreatedAt(new \DateTime());
        $teamPlayer->setTeamRole('ADMIN');
        $teamPlayer->setLevel('beginner');
        $teamPlayer->setFirstName('Martin');
        $teamPlayer->setLastName('Konarsky');
        //$teamPlayer->setPlayerNumber();
        $teamPlayerManager->saveTeamPlayer($teamPlayer);
        
        
        foreach ($players as $playerData)
        {
            $playerInfo = explode(',', $playerData);
            
            $teamPlayer = new \Webteamer\Team\Model\TeamPlayer();
            $teamPlayer->setTeamId($teamId);
            $teamPlayer->setPlayerId(0);
            $teamPlayer->setStatus('confirmed');
            $teamPlayer->setCreatedAt(new \DateTime());
            

            if(substr($playerInfo[0],1,1) == 6)
            {
                $teamPlayer->setTeamRole('GOAL_KEEPER');
            }
            else
            {
                $teamPlayer->setTeamRole('PLAYER');
            }
            
            $teamPlayer->setLevel('beginner');
            $teamPlayer->setFirstName($playerInfo[1]);
            $teamPlayer->setLastName($playerInfo[2]);
            $teamPlayer->setPlayerNumber($playerInfo[0]);
            $teamPlayerManager->saveTeamPlayer($teamPlayer);
        }

        
      
        
        $teams = array(
            1 => 'Dunajská Lužná',
            2 => 'Fiľakovo',
            3 => 'Partizánske',
            4 => 'Prievidza',
            5 => 'Pruské',
            6 => 'Senica',
            7 => 'Tulčík',
            8 => 'Veľký Šariš'
        );
        
        $teamPlayers = $teamPlayerRepo->findBy(array('team_id' => $teamId));
        
        
        $rounds = array(
            'games.csv' => 'Skupiny',
            'semifinal.csv' => 'Semifinále',
            '3th.csv' => 'O 3. miesto',
            '1th.csv' => 'O 1. miesto',
        );
        
        foreach($rounds as $file => $roundName)
        {
            $eventsData  = file(APPLICATION_PATH . '/flch/'.$file);
        
            foreach($eventsData as $eventData)
            {
                $eventInfo = explode(',', $eventData);
                $event = new \Webteamer\Event\Model\Event();
                $event->setAuthorId($authorId);
                $event->setName($roundName.' - '.$teams[$eventInfo[0]].' vs. '.$teams[$eventInfo[1]]);
                $event->setPeriod('none');
                $event->setStart(new \DateTime('2017-06-10 '.$eventInfo[2]));
                $event->setTeamId($teamId);
                $event->setEventType('game');
                $event->setCapacity(50);
                $event->setSeason($seasonId);
                $eventId = $eventRepo->save($event);
                $event->setId($eventId);

                $lineup = new \Webteamer\Team\Model\TeamMatchLineup();
                $lineup->setCreatedAt(new \DateTime());
                $lineup->setEventDate(new \DateTime('2017-06-10 00:00:00'));
                $lineup->setEventId($event->getId());
                $lineup->setTeamId($event->getTeamId());
                $lineup->setFirstLineName($teams[$eventInfo[0]]);
                $lineup->setSecondLineName($teams[$eventInfo[1]]);
                $lineupId = $lineupRepo->save($lineup);
                $lineup->setId($lineupId);

                //create attendance for match
                foreach($teamPlayers as $teamPlayer)
                {
                    $teamPlayerTeamId = substr($teamPlayer->getPlayerNumber(),0,1);
                    if($teamPlayerTeamId == $eventInfo[0] or $teamPlayerTeamId == $eventInfo[1])
                    {
                         $eventManager->createEventAttendance($teamPlayer, $event, '2017-06-10', 1);
                    }

                    //create lineup
                    if($teamPlayerTeamId == $eventInfo[0])
                    {
                        $eventManager->createLineupPlayer($lineup, array(
                            'player_id' => $teamPlayer->getPlayerId(),
                            'lineup_position' => 'first_line',
                            'player_name' => $teamPlayer->getFullName(),
                            'team_player_id' => $teamPlayer->getId(),
                            'player_position' => $teamPlayer->getTeamRole()
                        ));
                    }

                     if($teamPlayerTeamId == $eventInfo[1])
                    {
                        $eventManager->createLineupPlayer($lineup, array(
                            'player_id' => $teamPlayer->getPlayerId(),
                            'lineup_position' => 'second_line',
                            'player_name' => $teamPlayer->getFullName(),
                            'team_player_id' => $teamPlayer->getId(),
                            'player_position' => $teamPlayer->getTeamRole()
                        ));
                    }
                }
            }
        }
       

        //create package
        $pm = ServiceLayer::getService('PackageManager');
        $proPackage = $pm->getPackageWithProducts(5);
        ServiceLayer::getService('UserPackageManager')->addUserPackage([
            'user_id' => 4,
            'package_id' => 5,
            'team_id' =>$teamId,
            'team_name' => $team->getName(),
            'date' => new \DateTime(),
            'start_date' => new \DateTime(),
            'end_date' => new \DateTime('2017-06-31'),
            'name' => 'PRO',
            'note' => 'TRIAL',
            'level' => 5,
            'products' => json_encode($proPackage['products']),
            'player_limit' => 500,
            'guid' => \Core\GUID::create(),
             'variant' => 0,
             'renew_payment' => 0
        ]);

        //finish alert log
        \Core\DbQuery::query("INSERT INTO team_achievement_alert_log (name, achievement_code, created_at, created_by, team_id, alert_status, place_code) VALUES ('A je to tu!', 'step3_final', '".date('Y-m-d H:i:s')."', ".$team->getAuthorId().",  ".$teamId.", 'finished', 'event_detail_rating')");
        
        
        
        exit;
        //add players
        
        //create events
        
        //add rooster
    }
    
    
    public function localityAction()
    {
        $localityManager = ServiceLayer::getService('LocalityManager');
         $currentLocation = $localityManager->getLocalityByIp();
         echo "LOCAL";
        var_dump($currentLocation);
         
        if(null != $currentLocation && null != $currentLocation->city)
        {
           echo 'OK';
        }
         
          echo "BRAZIL";
         $brazil = $localityManager->getLocalityByIp('170.66.1.233');
          var_dump($brazil);
          if(null != $brazil && null != $brazil->city)
        {
           echo 'OK';
        }
          exit;
    }
    
    public function requestAction()
    {
        return $this->render('PH\Base\BaseModule:Test:test-request.php', array(
        
             
        ));
    }
    public function request1Action()
    {
         return $this->render('PH\Base\BaseModule:Test:test-request.php', array(
        
             
        ));
    }
    public function request2Action()
    {
        $layout = ServiceLayer::getService('layout');
        $layout->setTemplate(null);
        $response = new ControllerResponse();
        $response->setStatus('success');
        $response->setContent(json_encode(array('result'=> 'success')));
        $response->setType('application/json');
        return $response;
    }
    
    public function parseUrlAction()
    {
        require_once(LIB_DIR.'/OpenGraph/OpenGraph.php');

        $graph = \OpenGraph::fetch('https://www.finvia.sk/');

        //t_dump($graph->schema);

        /*
        foreach ($graph as $key => $value) {
            echo "$key => $value";
}
         * 
         */
    }

}
