<?php

namespace PH\Base\Controller;

use Core\Controller as Controller;
use Core\ControllerResponse;
use Core\ServiceLayer;

class StatsController extends Controller {

   public function allTeamsOverviewAction()
   {
        if('OwwCcosFVy9K8Y4yojvz' != $this->getRequest()->get('h'))
        {
            exit;
        }
        $repo = ServiceLayer::getService('TeamRepository');
      $localityRepo = \Core\ServiceLayer::getService('LocalityRepository');
         $query = \Core\DbQuery::prepare('
                 SELECT t.*, u.email as creator_email, tr.name as tournament_name from team t
                 LEFT JOIN tournament_team tt on t.id = tt.team_id
                 LEFT JOIN tournament tr on tt.tournament_id = tr.id
                 LEFT JOIN co_users u ON t.author_id = u.id
                 group by t.id order by t.id desc');

        $data = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
 
        
        //$mapper = $repo->getMapper();
        $list = array();
        foreach ($data as $d)
        {
            $object = $repo->createObjectFromArray($d);
            $object->setCreatorEmail($d['creator_email']);
            
             $locality = $localityRepo->find($d['locality_id']);
             $object->setLocality($locality);
            $list[$object->getId()]['team'] = $object;
            $list[$object->getId()]['tournaments'][] = $d['tournament_name'];
        }
      
        
        //var_dump($list);
     
         $layout = ServiceLayer::getService('layout');
            $layout->setTemplate(null);
            $response =$this->render('PH\Base\BaseModule:Stats:all_teams_xls.php', array(
                    'teams' => $list,
        ));
            //return $response;
           

            
            
            header('Content-Disposition: attachment; filename=export.xls');
             $response->setType('application/octet-stream');
            header('Content-Description: File Transfer');

            header('Content-Transfer-Encoding: binary');
            header('Connection: Keep-Alive');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            return $response;
   

   }
    
    
    public function stepByStepAction()
   {
       $repo = ServiceLayer::getService('AlertLogRepository');
       $achievemenManager = ServiceLayer::getService('AchievementAlertManager');
       $steps = $achievemenManager->getAchievements();

       //t_dump($steps);
       $data = $repo->findAll();
       
       $teamSteps = array();
       
       foreach($data as $d)
       {
           if(!array_key_exists($d->getTeamId(), $teamSteps))
           {
               $teamSteps[$d->getTeamId()] = $steps;
           }
           
           $teamSteps[$d->getTeamId()][$d->getAchievementCode()] = $d;
           
       }
       
      
       
       ksort($teamSteps);

       return $this->render('PH\Base\BaseModule:Stats:stepByStep.php', array(
            'teamSteps' => $teamSteps,
            'steps' => $steps
        ));
   }
    
    public function overviewSummaryAction()
    {
         if('OwwCcosFVy9K8Y4yojvz' != $this->getRequest()->get('t'))
        {
            exit;
        }

        $usersSummary = ServiceLayer::getService('StatsRepository')->getUserSumStats();
        $teamsSummary = ServiceLayer::getService('StatsRepository')->getTeamSumStats();
        $teamPlayerSummary = ServiceLayer::getService('StatsRepository')->getTeamPlayersStats();
        
        
       
        
        $sportManager =ServiceLayer::getService('SportManager');
        $sportEnum = $sportManager->getSportFormChoices();
        
        return $this->render('PH\Base\BaseModule:Stats:overview_summary.php', array(
            'usersSummary' => $usersSummary,
            'teamsSummary' => $teamsSummary,
            'teamPlayerSummary' => $teamPlayerSummary,
            'sportEnum' => $sportEnum
        ));
    }
    
    public function downloadStatsAction()
    {
        $request = $this->getRequest();
        $type = $request->get('t');

    if('OwwCcosFVy9K8Y4yojvz' != $this->getRequest()->get('h'))
        {
            exit;
        }

        $layout = ServiceLayer::getService('layout');
        $layout->setTemplate(null);

        if ($type == 'users_table')
        {
            $usersSummary = ServiceLayer::getService('StatsRepository')->getUserSumStats();
            $response = $this->render('PH\Base\BaseModule:Stats:_users_table.php', array(
                'usersSummary' => $usersSummary
            ));

            header('Content-Disposition: attachment; filename=users_table.xls');
        }
        if ($type == 'team_table')
        {
            $teamsSummary = ServiceLayer::getService('StatsRepository')->getTeamSumStats();
            $response = $this->render('PH\Base\BaseModule:Stats:_team_table.php', array(
                'teamsSummary' => $teamsSummary
            ));

            header('Content-Disposition: attachment; filename=team_table.xls');
        }
        if ($type == 'team_players_table')
        {
            $teamPlayerSummary = ServiceLayer::getService('StatsRepository')->getTeamPlayersStats();
            $response = $this->render('PH\Base\BaseModule:Stats:_team_players_table.php', array(
                'teamPlayerSummary' => $teamPlayerSummary
            ));

            header('Content-Disposition: attachment; filename=team_players_table.xls');
        }




        $response->setType('application/octet-stream');
        header('Content-Description: File Transfer');
       
        header('Content-Transfer-Encoding: binary');
        header('Connection: Keep-Alive');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        return $response;
        
    }
   
    
    public function overviewAction()
    {
        //select * from team where created_at > STR_TO_DATE('01.01.2016','%d.%m.%Y') and  created_at < STR_TO_DATE('01.08.2016','%d.%m.%Y')
        
        //select * from co_users where date_created > STR_TO_DATE('01.01.2016','%d.%m.%Y') and  date_created < STR_TO_DATE('01.08.2016','%d.%m.%Y')
        
        
        if('OwwCcosFVy9K8Y4yojvz' != $this->getRequest()->get('t'))
        {
            exit;
        }
        
        $dateFrom = strtotime('-7 day');
        $dateFromParam = date('d.m.Y',$dateFrom);
        $dateTo = new \DateTime();
        $dateToParam = $dateTo->format('d.m.Y');
        
        
        
        if(null != $this->getRequest()->get('date_from'))
        {
            $dateFromParam = $this->getRequest()->get('date_from');
        }
        
        if(null != $this->getRequest()->get('date_to'))
        {
            $dateToParam = $this->getRequest()->get('date_to');
        }
        
        $teams = ServiceLayer::getService('StatsRepository')->getTeamStats($dateFromParam,$dateToParam);
        
         $sportManager = ServiceLayer::getService('SportManager');
         $sportChoices = $sportManager->getSportFormChoices();
        
       
        
        
        return $this->render('PH\Base\BaseModule:Stats:overview.php', array(
            'teams' => $teams,
            'sportChoices' => $sportChoices,
            'dateFrom' => $dateFromParam,
            'dateTo' => $dateToParam
        ));
    }
   

}
