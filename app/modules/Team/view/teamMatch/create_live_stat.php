<?php Core\Layout::getInstance()->startSlot('team_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/Team/view/team/_team_menu.php', array('team' => $team)) ?>
<?php Core\Layout::getInstance()->endSlot('team_menu') ?>



<section class="content-header">
    <a class="all-event-link" href="<?php echo $router->link('team_event_list', array('team_id' => $team->getId())) ?>"><i class="ico ico-event-back"></i><?php echo $translator->translate('All Events') ?></a>
    
     <?php echo $layout->renderControllerAction('CR\Gamifications\GamificationModule:Progress:progressBar')->getContent() ?>
    
    <?php
    $layout->includePart(GLOBAL_DIR . '/templates/parts/_breadcrumb.php', array(
        'crumbs' => array(
            'Teams' => $router->link('teams_overview'),
            $team->getName() => $router->link('team_overview', array('id' => $team->getId())),
            'Events' => $router->link('team_event_list', array('team_id' => $team->getId())),
            $event->getName() => ''
)))
    ?>
</section>


<section class="content">
    <div class="row">
        <div class="col-md-8">
            <div class="event-detail">
                <div class="panel panel-default ">
                    <?php
                    $layout->includePart(MODUL_DIR . '/Team/view/events/_event_detail.php', array(
                        'event' => $event,
                        'team' => $team,
                        'existLineup' => $lineup,
                        'menuActive' => 'score',
                    ))
                    ?>

                  
                        <!-- timeline time label -->

                        <div class="row panel-timeline winner-<?php echo $matchResult['winner']  ?>">

                            <div class="team_row team_row_first_line">
                                <strong><?php echo $lineup->getFirstLineName() ?></strong>
                                <span id="first_line_goals"><?php echo $matchResult['first_line'] ?></span>
                            </div>
                            <div class="team_row_colon">:</div>
                            <div class="team_row team_row_second_line">
                                <span  id="second_line_goals"><?php echo $matchResult['second_line'] ?></span>
                                <strong><?php echo $lineup->getSecondLineName() ?></strong>
                            </div>
                            
                         
                            <div class="start_triggers">
       
    
                        <?php if ('closed' != $lineup->getStatus() && $finishedProgressStep): ?> 
                         <a href="#" class="start_timeline"><i class="ico ico-event-clock-white"></i> <?php echo $translator->translate('Start live score') ?></a>
                         <p><?php echo $translator->translate('Or') ?></p>
                        <?php endif; ?>
                         
                         
                         <a class="btn btn-clear btn-clear-green add_manual_timeline add_manual_timeline_<?php echo  $lineup->getStatus() ?>" href=""><?php echo $translator->translate('Add manual score') ?></a>

                            </div>
                            
                           <?php if ('closed' != $lineup->getStatus()): ?> 
                         <div id="stopwatch-control">
                            <input id="start_timeline_time"  name="" type="text" value="00:00:00" class="form-control" data-inputmask='"mask": "99:99:99"' data-mask>
                            <a  class="time_pause" href="#"><i class="ico ico-pause"></i></a>
                             <a  class="time_start" href="#"><i class="ico ico-arr-rgt-b"></i></a>
                             <a href="#" class="add_stat_time_mom"><?php echo $translator->translate('Game over') ?></a>
                         </div>
                        <?php endif ?>
                        
                         <ul class="timeline" id="stat_timeline">
                            <div class="timeline-start-item <?php echo (empty($eventTimeline)) ? 'hide' : '' ?>">
                              
                                <?php echo $translator->translate('Start Game') ?>
                                  <i class="ico ico-clock2"></i>
                            </div>
                             
                                <?php foreach ($eventTimeline as $groupId => $group): ?>
                        <?php
                            $layout->includePart(MODUL_DIR . '/Team/view/teamMatch/_timeline_event_row.php', array(
                                'group' => $group,
                                'lineup' => $lineup,
                                'event' => $event,
                                'players' => $players,
                                'groupId' => $groupId
                            ))
                            ?>
                        <?php endforeach; ?>
                             
                             
                             
                         
                            <li id="edit_timeline_form">
                                 <div id="edit_timeline_form_loader" class="alert alert-info text-center"><i class="fa fa-refresh fa-spin fa-3x fa-fw"></i> <br /><?php echo $translator->translate('Saving data...') ?></div>
                                        <div class="text-right">
                                            <a class="close_timeline_form"><i class="fa fa-close"></i></a>
                                        </div>

                                        <form  action="<?php echo $router->link('team_match_edit_timeline_stat', array('event_id' => $event->getId(), 'event_date' => $event->getCurrentDate()->format('Y-m-d'), 'lid' => $lineup->getId())) ?>">
                                            <div class="input-group input-group-contrast">
                                                <span class="input-group-addon">G</span>
                                                 <select class="form-control form-control-contrast player_choice goal_player" name="">
                                                    <option  value="0"></option>
                                                        <?php foreach ($players as $player): ?>
                                                        <option data-lid-team="<?php echo $player->getLineupPosition() ?>"  data-pid="<?php echo $player->getPlayerId() ?>" value="<?php echo $player->getId() ?>"><?php echo $player->getPlayerName() ?></option>
                                                        <?php endforeach; ?>
                                                </select>
                                             </div>

                                            <div class="assist_player_wrap">
                                                 <div class="input-group input-group-contrast">
                                                <span class="input-group-addon">A</span>
                                                <select class="form-control form-control-contrast player_choice assist_player" name="">
                                                    <option value=""></option>
                                                    <option data-lid-team="second_line"  data-pid="" value="0"><?php echo $translator->translate('Nobody') ?></option>
                                                    <?php foreach ($players as $player): ?>
                                                        <option data-lid-team="<?php echo $player->getLineupPosition() ?>"  data-pid="<?php echo $player->getPlayerId() ?>" value="<?php echo $player->getId() ?>"><?php echo $player->getPlayerName() ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                                 </div>
                                            </div>
                                        </form>
                             
                                   
                            </li>
                      
                           
                          
                            <li id="add_timeline_form">
                                 <div class="text-right">
                                            <a class="close_timeline_form"><i class="fa fa-close"></i></a>
                                        </div>
                                <div id="add_timeline_form_loader" class="alert alert-info text-center"><i class="fa fa-refresh fa-spin fa-3x fa-fw"></i> <br /><?php echo $translator->translate('Saving data...') ?></div>
                                <form  action="<?php echo $router->link('team_match_add_timeline_stat', array('event_id' => $event->getId(), 'event_date' => $event->getCurrentDate()->format('Y-m-d'), 'lid' => $lineup->getId())) ?>"> 
                                    <input type="text" class="current_hit_time" value="00:00:00" >
                                    
                                    <div class="input-group input-group-contrast">
                                        <span class="input-group-addon">G</span>
                                        <select class="form-control form-control-contrast player_choice goal_player" name="">
                                        <option value=""></option>
                                        <?php foreach ($players as $player): ?>
                                            <option data-lid-team="<?php echo $player->getLineupPosition() ?>"  data-pid="<?php echo $player->getPlayerId() ?>" value="<?php echo $player->getId() ?>"><?php echo $player->getPlayerName() ?></option>
                                        <?php endforeach; ?>
                                        </select>
                                    </div>

                                    <div class="assist_player_wrap">
                                     <div class="input-group input-group-contrast">
                                        <span class="input-group-addon">A</span>
                                        <select class="form-control form-control-contrast player_choice assist_player" name="">
                                            <option  value="0"></option>
                                            <option data-lid-team="first_line"  data-pid="" value="0"><?php echo $translator->translate('Nobody') ?></option>
                                            <?php foreach ($players as $player): ?>
                                                <option data-lid-team="<?php echo $player->getLineupPosition() ?>"  data-pid="<?php echo $player->getPlayerId() ?>" value="<?php echo $player->getId() ?>"><?php echo $player->getPlayerName() ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    </div>
                                </form>

                            </li>
                       
                           
                    <li id="add_timeline_mom_form">
                          <div id="mom_timeline_form_loader" class="alert alert-info text-center"><i class="fa fa-refresh fa-spin fa-3x fa-fw"></i> <br /><?php echo $translator->translate('Saving data...') ?></div>
                               
                                    <?php echo $translator->translate('Man of match') ?>
                                    <select class="form-control form-control-contrast player_choice" name="">
                                        <option data-lid-team="none"  data-pid="" value="0"><?php echo $translator->translate('Nobody') ?></option>
                                        <?php foreach ($players as $player): ?>
                                            <option data-lid-team="<?php echo $player->getLineupPosition() ?>"  data-pid="<?php echo $player->getPlayerId() ?>" value="<?php echo $player->getId() ?>"><?php echo $player->getPlayerName() ?></option>
                                        <?php endforeach; ?>
                                    </select>

                                    <a href="#" class="btn btn-danger"><?php echo $translator->translate('Cancel') ?></a>
                                    <a href="<?php echo $router->link('team_match_add_timeline_mom', array('event_id' => $event->getId(), 'type' => 'hit', 'event_date' => $event->getCurrentDate()->format('Y-m-d'), 'lid' => $lineup->getId())) ?>" class="btn btn-success timeline_mom_trigger"><?php echo $translator->translate('Save') ?></a>
                               
                           
                        </li>
                        
                         
                       
                        <!-- /.timeline-label -->
                         
                        
                       
                       

                        
                        <li id="timeline-end"></li>
                            <li class="time-label add_stat_time_event_wrap">
                                <a href="#" class="add_stat_time_event" <?php echo ('closed' != $lineup->getStatus() && !empty($eventTimeline)) ? 'style="display:inline;"' : '' ?>><i class="ico ico-plus-w"></i></a>
                            </li>
                            <?php if ('closed' != $lineup->getStatus()): ?> 
                           <li class="time-label add_stat_time_event_wrap add_stat_time_event_wrap_mom <?php echo (count($eventTimeline)>0) ? 'active' : '' ?>">
                               <a href="#" class="add_stat_time_mom_manual"><?php echo $translator->translate('Game over') ?></a>
                           </li>
                           <?php endif; ?>
                           
                            <?php if ('closed' == $lineup->getStatus()): ?> 
                                <a  data-toggle="tooltip" data-placement="top" title="<?php echo $translator->translate('Delete all match stats and reopen it') ?>" class="btn btn-danger reset-trigger" href="<?php echo $router->link('team_match_reset_timeline',array('lid' => $lineup->getId() )) ?>"><?php echo $translator->translate('Reset Match') ?></a>
                            <?php endif; ?> 
                                
                              
                       
                           
                    </ul>


                        
                    </div>

                </div>
            </div>
            
            
             <div class="row">
                <div class="col-sm-3 col-md-offset-4">

                    <a  href="<?php echo $router->link('team_event_rating',array('id' => $event->getId(), 'current_date' => $event->getCurrentDate()->format('Y-m-d'))) ?>"  class="btn btn-primary pull-right btn_progress_continue continue_btn_second"><?php echo $translator->translate('Continue') ?></a>&nbsp;
                     
                </div>
            </div>
            
            
        </div>
        <div class="col-md-4">
            <?php $layout->includePart(MODUL_DIR . '/Event/view/widget/_small_calendar.php') ?>
        </div>
    </div>

</section>


<?php $layout->includePart(MODUL_DIR . '/Team/view/events/_confirm_single_delete_modal.php', array('event' => $event)) ?>
<?php $layout->includePart(MODUL_DIR . '/Team/view/events/_confirm_delete_modal.php', array('event' => $event)) ?>



<?php $layout->includePart(MODUL_DIR . '/Team/view/teamMatch/_confirm_save_modal.php') ?>
<?php $layout->includePart(MODUL_DIR . '/Team/view/teamMatch/_confirm_delete_modal.php') ?>



<?php $layout->includePart(MODUL_DIR . '/Team/view/teamMatch/_confirm_reset_modal.php') ?>
<?php $layout->addJavascript('plugins/stopwatch/stopwatch.js') ?>
<?php $layout->addJavascript('js/TeamMatchManager.js') ?>
<?php $layout->addJavascript('js/TeamEventManager.js') ?>
<?php $layout->addJavascript('js/SmallCalendar.js') ?>

<?php $layout->startSlot('javascript') ?>
<?php echo Core\ServiceLayer::getService('AchievementAlertManager')->showPlaceAchievementAlert('event_detail_score','event_score_manual_create',Core\ServiceLayer::getService('security')->getIdentity()) ?>

<?php echo Core\ServiceLayer::getService('AchievementAlertManager')->showPlaceAchievementAlert('event_detail_score','event_score_live_create',Core\ServiceLayer::getService('security')->getIdentity()) ?>

<?php echo Core\ServiceLayer::getService('AchievementAlertManager')->showPlaceAchievementAlert('event_detail_rating','step2_final',Core\ServiceLayer::getService('security')->getIdentity()) ?>

<script src="/dev/plugins/input-mask/jquery.inputmask.js"></script>
<script type="text/javascript">
    $("[data-mask]").inputmask();

    match_manger = new TeamMatchManager();
    match_manger.initTimelineTriggers();
    
    var event_manger = new TeamEventsManager();
    event_manger.init();
     function showEventLocation()
    {
        event_manger.google = google;
    }
    
      small_calendar = new SmallCalendar({
        'dataUrl': '<?php echo $router->link('rest_event_get_month_grid') ?>',
        'localityManager': new LocalityManager()
    });
    small_calendar.initLoad();
    


</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXU1duK54f1gIsRy_xc7z4uQURovsZdE8&libraries=places&callback=showEventLocation" async defer></script>

<?php $layout->endSlot('javascript') ?>