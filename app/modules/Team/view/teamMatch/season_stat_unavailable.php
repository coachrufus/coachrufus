
<?php Core\Layout::getInstance()->startSlot('stylesheet') ?>  
<link rel="stylesheet" href="/dev/plugins/datatables/dataTables.bootstrap.css">
<?php Core\Layout::getInstance()->endSlot('stylesheet') ?>

<?php Core\Layout::getInstance()->startSlot('team_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/Team/view/team/_team_menu.php', array('team' => $team)) ?>
<?php Core\Layout::getInstance()->endSlot('team_menu') ?>




<section class="content-header">

     <?php $layout->includePart(GLOBAL_DIR.'/templates/parts/_breadcrumb.php',array(
        'crumbs' => array(
            'Teams' => $router->link('teams_overview'),
            $team->getName() => $router->link('team_overview',array('id' =>$team->getId() )),
            'Statistics' => ''
            ))) ?>
</section>
<section class="content">
    
    
    
    
    <div class="row">
        <div class="col-md-4 col-md-offset-4" >
            <div class="panel">
                <div class="panel-body">
                         <div class="row">
                        <div class="col-sm-5">
                            <img src="/img/main/logo-sq.svg" />
                        </div>
                        <div class="col-sm-7 text-left">
                            
                            <?php echo $translator->translate('Skúšal som, fakt som skúšal nejaké číselka vydolovať, ale vyzerá to že nemáš ešte nič odohraté.') ?>
                            <br />
                            
                              <?php if (\Core\ServiceLayer::getService('security')->getIdentity()->hasTeamPermission('manageEvents', $team)): ?>
                             <br />
                           <?php echo $translator->translate('Tak poď do toho a vytvor si nový zápas, pridaj štatistiky a vráť sa.') ?>
                            <br />
                             <br />
                            <a href="<?php echo $router->link('create_team_event',array('team_id' =>$team->getId() )) ?>" class="btn btn-primary"><?php echo $translator->translate('Create Event') ?></a>
                            <?php endif; ?>
                         
                            
                          
                            
                            
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
     
    </div>
</section>


<?php $layout->startSlot('javascript') ?>
<?php  $layout->addJavascript('plugins/datatables/jquery.dataTables.min.js')  ?>
<?php  $layout->addJavascript('plugins/datatables/dataTables.bootstrap.min.js')  ?>
<?php  $layout->addJavascript('plugins/datatables/sk.js')  ?>
 <script>
      $(function () {
        
        //var datatables_trans = new datatablesTrans();
        $("#stat_table").DataTable({
             "scrollX": true,
             "iDisplayLength": 10,
             "aLengthMenu": [[5, 10, 25, 50,100, -1], [5, 10, 25, 50,100, "<?php echo $translator->translate('All') ?>"]],
             "language": datatablesTrans,
             "order": [[ 15, "desc" ]]
        });
       
      });
      
      $('.scroll_table_right').on('click',function(e){
          e.preventDefault();
          console.log('asdfas');
          $('#stat_table').scrollLeft = 50;
          
      });
    </script>
  
<?php $layout->endSlot('javascript') ?>
