
<?php Core\Layout::getInstance()->startSlot('stylesheet') ?>  
<link rel="stylesheet" href="/dev/plugins/datatables/dataTables.bootstrap.css">
<?php Core\Layout::getInstance()->endSlot('stylesheet') ?>

<?php Core\Layout::getInstance()->startSlot('team_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/Team/view/team/_team_menu.php', array('team' => $team)) ?>
<?php Core\Layout::getInstance()->endSlot('team_menu') ?>




<section class="content-header">
    <h1>
        <?php echo $translator->translate('Stats') ?>
    </h1>
    
     <?php $layout->includePart(GLOBAL_DIR.'/templates/parts/_breadcrumb.php',array(
        'crumbs' => array(
            'Teams' => $router->link('teams_overview'),
            $team->getName() => $router->link('team_overview',array('id' =>$team->getId() )),
            'Statistics' => ''
            ))) ?>
</section>
<section class="content">
    
    
    
    
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                
                
                
             <div class="box box-header">
                
                

            <form action="" class="form-inline pull-left" method="get">

                <?php echo $translator->translate('Season') ?>:
                <?php echo $filterForm->renderSelectTag('season_id',array('class' => 'form-control')) ?>
                <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-refresh"></i> <?php echo $translator->translate('Filter') ?></button>
            </form>
                

                    <a class="pull-right btn btn-info btn-sm" href="<?php echo $router->link('team_crs_settings',array('team_id' => $team->getId())) ?>"><?php echo $translator->translate('CRS settings') ?></a>

                
            </div>
              
              
                <div class="box-body">
                    <table class="table table-bordered " id="stat_table">
                        <thead>
                            <tr>
                               <th> <?php echo $translator->translate('Player') ?><i cl</th>
                               <th>GP</th>
                               
                                <th>W</th>
                                <th>D</th>
                                <th>L</th>
                                <th>%</th>
                                
                                <th>Ws</th>
                                <th>Ls</th>
                                <th>Ds</th>
                                
                                <th>sPlus</th>
                                <th>sMinus</th>
                                <th>+/-</th>
                                <th>CRU</th>
                                <th>CRS</th>
                                <th>avCRS</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                    <?php foreach ($teamPlayersStats as $stat): ?>
                   <tr>
                       <td><?php echo $stat->getPlayer() ?></td>
                       <?php if($stat != null): ?>
                            
                       <td data-order="<?php echo  $stat->getGamePlayed() ?>"><span class="ratio gp_ratio" style="width:<?php echo  $stat->getGamePlayedMaxPercent()?>%"></span><span class="stat_value"><?php echo  $stat->getGamePlayed() ?></span></td>
                       
                         <td data-order="<?php echo  $stat->getWins() ?>"><span class="ratio win_ratio" style="width:<?php echo  $stat->getWinsMaxPercent()?>%"></span><span class="stat_value"><?php echo  $stat->getWins() ?></span></td>
                         <td data-order="<?php echo  $stat->getDraws() ?>"><span class="ratio draw_ratio" style="width:<?php echo  $stat->getDrawsMaxPercent()?>%"></span><span class="stat_value"><?php echo  $stat->getDraws() ?></span></td>
                          <td data-order="<?php echo  $stat->getLooses() ?>"><span class="ratio loose_ratio" style="width:<?php echo  $stat->getLooseMaxPercent()?>%"></span><span class="stat_value"><?php echo  $stat->getLooses() ?></span></td>
                                       
                          <td data-order="<?php echo  $stat->getWinsPrediction() ?>" class="ratio prediction_ratio_<?php echo $stat->getWinsPredictionAreaPercent() ?>"><?php echo  $stat->getWinsPrediction() ?></td>
                          <td data-order="<?php echo  $stat->getWonSet() ?>"><?php echo  $stat->getWonSet() ?></td>         
                          <td data-order="<?php echo  $stat->getLooseSet() ?>"><?php echo  $stat->getLooseSet() ?></td>         
                          <td data-order="<?php echo  $stat->getDrawSet() ?>"><?php echo  $stat->getDrawSet() ?></td>         

                          
                          

                            <td data-order="<?php echo  $stat->getPlusPoints() ?>"><?php echo $stat->getPlusPoints() ?></td>
                            <td  data-order="<?php echo  $stat->getMinusPoints() ?>"><?php echo $stat->getMinusPoints() ?></td>
                            <td data-order="<?php echo  $stat->getPlusMinusDiff() ?>">
                                <?php if($stat->getPlusMinusDiff() > 0): ?>
                                    <span class="ratio  gp_ratio" style="left:<?php echo $stat->getPlusMinusDiffOffset('plusOffset') ?>%;width:<?php echo $stat->getPlusMinusDiffOffset('length') ?>%"></span>
                                <?php endif; ?>
                                    
                                 <?php if($stat->getPlusMinusDiff() < 0): ?>
                                    <span class="ratio loose_ratio" style="left:<?php echo $stat->getPlusMinusDiffOffset('minusMargin') ?>%;width:<?php echo $stat->getPlusMinusDiffOffset('length') ?>%"></span>
                                <?php endif; ?>
                                    
                                <span class="stat_value"><?php echo  $stat->getPlusMinusDiff() ?></span>

                            </td>
                            <td>
                                <?php echo $stat->getCRU() ?>
                            </td>
                            

                            <td data-order="<?php echo $stat->getCRS() ?>">
                                <span class="ratio win_ratio" style="width:<?php echo  $stat->getCRSMaxPercent()?>%"></span><span class="stat_value"><?php echo  $stat->getCRS() ?></span>
                              
                               
                            </td>
                            
                             <td data-order="<?php echo  $stat->getAverageCRS() ?>" class="ratio prediction_ratio_<?php echo $stat->getAverageCRSAreaPercent() ?>"><?php echo $stat->getAverageCRS() ?></td>

                            
                        
                         
                            
                       <?php else: ?>
                            
                            <td data-order="0" >-</td>
                            <td data-order="0" >-</td>
                            <td data-order="0" >-</td>
                            <td data-order="0" >-</td>
                            <td data-order="0" >-</td>
                            <td data-order="0" >-</td>
                            <td data-order="0" >-</td>
                            <td data-order="0" >-</td>
                            <td data-order="0" >-</td>
                            <td data-order="0" >-</td>
                            <td data-order="0" >-</td>
                            <td data-order="0" >-</td>
                            <td data-order="0" >-</td>
                            <td data-order="0" >-</td>
                            <td data-order="0" >-</td>
                          
                           
                           
                           
                    <?php endif; ?>
                       
                            
                        </tr>
                        
                       

                    <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
     
    </div>
</section>


<?php $layout->startSlot('javascript') ?>
<?php  $layout->addJavascript('plugins/datatables/jquery.dataTables.min.js')  ?>
<?php  $layout->addJavascript('plugins/datatables/dataTables.bootstrap.min.js')  ?>
<?php  $layout->addJavascript('plugins/datatables/sk.js')  ?>
 <script>
      $(function () {
        
        //var datatables_trans = new datatablesTrans();
        $("#stat_table").DataTable({
             "scrollX": true,
             "iDisplayLength": 10,
             "aLengthMenu": [[5, 10, 25, 50,100, -1], [5, 10, 25, 50,100, "<?php echo $translator->translate('All') ?>"]],
             "language": datatablesTrans,
             "order": [[ 14, "desc" ]]
        });
       
      });
      
     
    </script>
  
<?php $layout->endSlot('javascript') ?>
