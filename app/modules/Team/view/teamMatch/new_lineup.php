<?php Core\Layout::getInstance()->startSlot('team_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/Team/view/team/_team_menu.php', array('team' => $team)) ?>
<?php Core\Layout::getInstance()->endSlot('team_menu') ?>

<section class="content-header">
 <a class="all-event-link" href="<?php echo $router->link('team_event_list', array('team_id' => $team->getId())) ?>"><i class="ico ico-event-back"></i><?php echo $translator->translate('All Events') ?></a>
    
     <?php echo $layout->renderControllerAction('CR\Gamifications\GamificationModule:Progress:progressBar')->getContent() ?>
    
    <?php
    $layout->includePart(GLOBAL_DIR . '/templates/parts/_breadcrumb.php', array(
        'crumbs' => array(
            'Teams' => $router->link('teams_overview'),
            $team->getName() => $router->link('team_overview', array('id' => $team->getId())),
            'Events' => $router->link('team_event_list', array('team_id' => $team->getId())),
            $event->getName() => ''
)))
    ?>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-8">
            <div class="event-detail">
                <div class="panel panel-default">

                    <?php
                    $layout->includePart(MODUL_DIR . '/Team/view/events/_event_detail.php', array(
                        'event' => $event,
                        'team' => $team,
                        'existLineup' => $existLineup,
                        'menuActive' => 'lineup',
                    ))
                    ?>
                    <div class="row lineup-panel">
                        <div class="create-linuep-wrap">
                            <div class="<?php echo ($menuActive == 'lineup') ? 'active' : '' ?> btn-group" role="group">
                                <a  class="dropdown-toggle clear-dropdown create-linuep-select" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                   <i class="ico ico-book"></i> <?php echo $translator->translate('Create lineup') ?> <i class="ico ico-arr-down-green"></i>
                                </a>
                                <ul class="dropdown-menu">
                                     <li>
                                        <?php if($showEfficiencyCreate): ?>
                                             <a href="<?php echo $router->link('team_match_create_lineup', array('type' => 'efficiency', 'event_id' => $event->getId(), 'event_date' => $event->getCurrentDate()->format('Y-m-d'))) ?>#create_lineup"><?php echo $translator->translate('Players efficiency') ?></a>
                                        <?php elseif($wizardWinished): ?>
                                             <a class="team_credit_modal_trigger" href="#"><?php echo $translator->translate('Players efficiency') ?><button class="pro-icon ">PRO</button></a>
                                        <?php endif; ?>
                                    </li>
                                    
                                     <li> 
                                    <?php if($showRandomCreate): ?>
                                         <a href="<?php echo $router->link('team_match_create_lineup', array('type' => 'random', 'event_id' => $event->getId(), 'event_date' => $event->getCurrentDate()->format('Y-m-d'))) ?>#create_lineup"><?php echo $translator->translate('Random') ?></a>
                                     <?php elseif($wizardWinished): ?>
                                         <a class="team_credit_modal_trigger" href="#"><?php echo $translator->translate('Random') ?><button class="pro-icon">PRO</button></a>
                                     <?php endif; ?>
                                     </li>
                                    
                                    <li> <a href="<?php echo $router->link('team_match_create_lineup', array('type' => 'manual', 'event_id' => $event->getId(), 'event_date' => $event->getCurrentDate()->format('Y-m-d'))) ?>#create_lineup"><?php echo $translator->translate('Manual') ?></a></li>

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
              
        </div>
    <div class="col-md-4">
            <?php $layout->includePart(MODUL_DIR . '/Event/view/widget/_small_calendar.php') ?>
        </div>
</div>

</section>


<?php $layout->includePart(MODUL_DIR . '/Team/view/events/_confirm_single_delete_modal.php', array('event' => $event,'eventTimeline' => $eventTimeline)) ?>


<?php $layout->addJavascript('js/WidgetManager.js') ?>
<?php $layout->addJavascript('js/TeamEventManager.js') ?>
<?php $layout->addJavascript('js/SmallCalendar.js') ?>
<?php $layout->startSlot('javascript') ?>
<?php echo Core\ServiceLayer::getService('AchievementAlertManager')->showPlaceAchievementAlert('event_detail_roster','event_roster_manual_create',Core\ServiceLayer::getService('security')->getIdentity()) ?>
<?php echo Core\ServiceLayer::getService('AchievementAlertManager')->showPlaceAchievementAlert('event_detail_roster','event_roster_random_create',Core\ServiceLayer::getService('security')->getIdentity()) ?>
<?php echo Core\ServiceLayer::getService('AchievementAlertManager')->showPlaceAchievementAlert('event_detail_roster','event_roster_performance_create',Core\ServiceLayer::getService('security')->getIdentity()) ?>
<script type="text/javascript">
      var event_manger = new TeamEventsManager();
    event_manger.init();
   function showEventLocation()
    {
        event_manger.google = google;
    }
   
    
     small_calendar = new SmallCalendar({
        'dataUrl': '<?php echo $router->link('rest_event_get_month_grid') ?>',
        'localityManager': new LocalityManager()
    });
    small_calendar.initLoad();

</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXU1duK54f1gIsRy_xc7z4uQURovsZdE8&libraries=places&callback=showEventLocation" async defer></script>


<?php $layout->endSlot('javascript') ?>