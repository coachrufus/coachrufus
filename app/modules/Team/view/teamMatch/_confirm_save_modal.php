<div class="modal fade" id="confirm-save-modal" tabindex="-1" role="dialog" aria-labelledby="<?php echo $translator->translate('Confirm end Match') ?>">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $translator->translate('Confirm end Match') ?></h4>
      </div>
      <div class="modal-body">
          <div class="alert alert-danger"><?php echo $translator->translate('Are you sure?') ?></div>
          <div class="row">
              
              <div class="col-xs-6">
                  <a class="btn btn-lg btn-danger unconfirm-save-modal-submit" href=""><?php echo $translator->translate('No') ?></a>
              </div>
              <div class="col-xs-6">
                   <a class="btn btn-lg btn-success confirm-save-modal-submit pull-right" href=""><?php echo $translator->translate('Yes') ?></a>
              </div>
          </div>
        

      </div>
      
    </div>
  </div>
</div>

