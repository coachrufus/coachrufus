<?php Core\Layout::getInstance()->startSlot('team_menu') ?>  
   <?php $layout->includePart(MODUL_DIR.'/Team/view/team/_team_menu.php',array('team' => $team) ) ?>
<?php Core\Layout::getInstance()->endSlot('team_menu') ?>


<section class="content-header">
    <h1>
        <?php echo $translator->translate('Team match statistic') ?>
    </h1>
</section>
<section class="content">
<form action="" class="form-horizontal form-bordered" method="post">
   
   
    <table class="table table-bordered">
        <thead>
            <tr>
                <th><?php echo $translator->translate('Team 1 name') ?>:<input type="text" name="team[1][name]" /></th>
                <th><?php echo $translator->translate('Team 2 name') ?>:<input type="text" name="team[2][name]" /></th>
            </tr>
        </thead>
        
        <tbody>
            <tr>
                <td id="team1_container">
                    
                    <div id="container_template_item" style="display:none">
                        <select class="player_select" name="">
                            <option value=""></option>
                            <?php foreach($players as $player): ?>
                            <option value="<?php echo $player->getPlayerId() ?>"><?php echo $player->getName()  ?></option>
                            <?php endforeach; ?>
                        </select>
                        Goals:<input class="player_goals" type="text" name="" />
                        Assist:<input class="player_assist" type="text" name="" />
                        Saves<input class="player_saves" type="text" name="" />
                        MOM<input  class="player_mom" type="checkbox" name="" value="1" />
                    </div>
                    
                    
                    <div class="container_item_wrap"> </div>
                    <button data-group-container="team1_container" class="add_row_trigger btn btn-success" data-team-id="1"><?php echo $translator->translate('Add player') ?></button>
                  
                    
                 </td>
                
                 <td  id="team2_container">
                    <div class="container_item_wrap"> </div>
                    <button data-group-container="team2_container" class="add_row_trigger  btn btn-success" data-team-id="2"><?php echo $translator->translate('Add player') ?></button>
                </td>
            </tr>
            
        </tbody>
        
        
    </table>
    
    
    <div class="row">
        <div class="col-sm-6 col-sm-offset-3">
            <button class="btn btn-primary">Submit</button>&nbsp;
            <button class="btn btn-default">Cancel</button>
        </div>
    </div>
</form>
</section>

<?php $layout->startSlot('javascript') ?>

 <script type="text/javascript">


match_manger = new TeamMatchManager();
match_manger.init();
</script>
  
<?php $layout->endSlot('javascript') ?>