<?php Core\Layout::getInstance()->startSlot('team_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/Team/view/team/_team_menu.php', array('team' => $team)) ?>
<?php Core\Layout::getInstance()->endSlot('team_menu') ?>

<section class="content-header">

    <a class="all-event-link" href="<?php echo $router->link('team_event_list', array('team_id' => $team->getId())) ?>"><i class="ico ico-event-back"></i><?php echo $translator->translate('All Events') ?></a>
    <?php
    $layout->includePart(GLOBAL_DIR . '/templates/parts/_breadcrumb.php', array(
        'crumbs' => array(
            'Teams' => $router->link('teams_overview'),
            $team->getName() => $router->link('team_overview', array('id' => $team->getId())),
            'Events' => $router->link('team_event_list', array('team_id' => $team->getId())),
            'New' => ''
)))
    ?>
</section>



<section class="content">
    <div class="row">
        <div class="col-md-8">
            <div class="event-detail">
                <div class="panel panel-default rating-panel">
                    <?php
                    $layout->includePart(MODUL_DIR . '/Team/view/events/_event_detail.php', array(
                        'event' => $event,
                        'team' => $team,
                        'existLineup' => $lineup,
                        'menuActive' => 'score',
                    ))
                    ?>

                   <div class="box-body">
                    <form action="" method="post">
                        <?php for ($i = 1; $i < 6; $i++): ?>
                            <div class="col-sm-12"> 
                                <div class="panel panel-default  set_stat_panel <?php echo ( count($setMatch->getSets()) >= $i) ? 'set_stat_panel_active' : '' ?> <?php echo ( count($setMatch->getSets()) == $i or count($setMatch->getSets()) == 0) ? 'set_stat_panel_last' : '' ?>" id="set_stat_panel_<?php echo $i ?>">
                                    <div class="panel-heading  text-center">

                                        <h3><?php echo $i ?>. Set</h3>
                                    </div>


                                    <div class="panel-body">
                                       

                                        <div class="col-sm-6 set-col">
                                            <h3><?php echo $existLineup->getFirstLineName() ?></h3>
                                            <strong><?php echo $setMatch->getTotalSetPoints('first_line', $i) ?></strong>
                                            <?php foreach ($lineupPlayers['first_line'] as $lineupPlayer): ?>
                                                <div class="row">
                                                    <div class="form-group  col-xs-8">
                                                        <label class="control-label"> <?php echo $lineupPlayer->getPlayerName() ?></label>
                                                    </div>
                                                    <div class="form-group  col-xs-4">
                                                        <?php echo $setMatch->getPlayerSetPoints($lineupPlayer->getId(), $i) ?>
                                                    </div>
                                                </div>
                                            <?php endforeach; ?>
                                        </div>

                                        <div class="col-sm-6 set-col">
                                            <h3><?php echo $existLineup->getSecondLineName() ?></h3>
                                            <strong><?php echo $setMatch->getTotalSetPoints('second_line', $i) ?></strong>
                                            <?php foreach ($lineupPlayers['second_line'] as $lineupPlayer): ?>
                                                <div class="row">
                                                    <div class="form-group  col-xs-8">
                                                        <label class="control-label"> <?php echo $lineupPlayer->getPlayerName() ?></label>
                                                    </div>
                                                    <div class="form-group  col-xs-4">
                                                        <?php echo $setMatch->getPlayerSetPoints($lineupPlayer->getId(), $i) ?>
                                                    </div>
                                                </div>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endfor; ?>

                      
                    </form>
                </div>

                   
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <?php $layout->includePart(MODUL_DIR . '/Event/view/widget/_small_calendar.php') ?>
        </div>
    </div>
</section>




<?php $layout->addJavascript('js/TeamEventManager.js') ?>
<?php $layout->addJavascript('js/WidgetManager.js') ?>
<?php $layout->addJavascript('js/SmallCalendar.js') ?>





