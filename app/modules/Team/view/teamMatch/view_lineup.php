<?php Core\Layout::getInstance()->startSlot('team_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/Team/view/team/_team_menu.php', array('team' => $team)) ?>
<?php Core\Layout::getInstance()->endSlot('team_menu') ?>
<section class="content-header">
        <a class="all-event-link" href="<?php echo $router->link('team_event_list', array('team_id' => $team->getId())) ?>"><i class="ico ico-event-back"></i><?php echo $translator->translate('All Events') ?></a>

    <?php
    $layout->includePart(GLOBAL_DIR . '/templates/parts/_breadcrumb.php', array(
        'crumbs' => array(
            'Teams' => $router->link('teams_overview'),
            $team->getName() => $router->link('team_overview', array('id' => $team->getId())),
            'Events' => $router->link('team_event_list', array('team_id' => $team->getId())),
            $event->getName() => ''
)))
    ?>
        
        
</section>

<section class="content">

    <div class="row">
        <div class="col-md-8">
            <div class="event-detail">
                <div class="panel panel-default">

                    <?php
                    $layout->includePart(MODUL_DIR . '/Team/view/events/_event_detail.php', array(
                        'event' => $event,
                        'team' => $team,
                         'existLineup' => $existLineup,
                         'menuActive' => 'lineup',
                    ))
                    ?>
                    
                   

                    <div class="row lineup-panel view-linuep">
                        
                     
                        
                        <div class="col-sm-6 first-line-col">
                            <h3 class="box-title"><?php echo $lineup->getFirstLineName() ?></h3>
                            
                              <div class="power-row"><?php echo $translator->translate('Power') ?> 
                                (<span class="power-total"></span>)
                                <span class="power-perc"></span>
                                <span class="power-indicator"></span>
                            
                            </div>


                            <div class="progress"><div class="progress-bar progress-bar-green"  id="first_line_eff_sum" role="progressbar"></div></div>
                            <table class="table">
                                


                                <tbody id="first_line">
                                    <?php foreach ($firstLine as $attendance): ?>
                                        <tr class="lineup_row">
                                            <td>
                                                <div class="user-block pull-left">
                                                    <?php if(null !=  $attendance['teamPlayer'] ): ?>
                                                         <div class="round-50" style="background-image:url(<?php echo $attendance['teamPlayer']->getMidPhoto() ?>)"></div>
                                                    <?php endif; ?>
                                                    <span class="username"><?php echo $attendance['player']->getPlayerName() ?></span>
                                                     <?php if(null !=  $attendance['teamPlayer'] ): ?>
                                                        <span class="description"><?php echo $attendance['teamPlayer']->getTeamRoleName() ?> <?php echo ($attendance['teamPlayer']->getPlayerNumber() != null) ? '#' . $attendance['teamPlayer']->getPlayerNumber() : '' ?></span>
                                                        
                                                    <?php endif; ?>
                                                </div><!-- /.user-block -->
                                                <span class="efficiency" data-val="<?php echo $attendance['efficiency'] ?>" data-toggle="tooltip" title="<?php echo $translator->translate('Actual form') ?>">
                                                    (<?php echo $attendance['efficiency'] ?>)
                                                </span>
                                            </td>
                                            <td class="change_lineup_wrap">
                                                
                                               
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>

                                </tbody>
                            </table>
                        </div>

                        <div class="col-sm-6 second-line-col">


                            <h3 class="box-title"><?php echo $lineup->getSecondLineName() ?></h3>
                            
                            <div class="power-row"><?php echo $translator->translate('Power') ?> 
                                (<span class="power-total"></span>)
                                <span class="power-perc"></span>
                                <span class="power-indicator"></span>
                            
                            </div>
                            
                            
                            <div class="progress">
                                <div class="progress-bar progress-bar-green"  id="second_line_eff_sum" role="progressbar">
                                </div>
                            </div>

                            <table class="table">
                               


                                <tbody  id="second_line">
                                    <?php foreach ($secondLine as $attendance): ?>
                                        <tr class="lineup_row">

                                            <td class="change_lineup_wrap">
                                               
                                            </td>
                                            <td>
                                                
                                                 <div class="user-block">
                                                    <?php if(null !=  $attendance['teamPlayer'] ): ?>
                                                         <div class="round-50" style="background-image:url(<?php echo $attendance['teamPlayer']->getMidPhoto() ?>)"></div>
                                                    <?php endif; ?>
                                                    <span class="username"><?php echo $attendance['player']->getPlayerName() ?></span>
                                                     <?php if(null !=  $attendance['teamPlayer'] ): ?>
                                                        <span class="description"><?php echo $attendance['teamPlayer']->getTeamRoleName() ?> <?php echo ($attendance['teamPlayer']->getPlayerNumber() != null) ? '#' . $attendance['teamPlayer']->getPlayerNumber() : '' ?></span>
                                                        
                                                    <?php endif; ?>
                                                </div><!-- /.user-block -->
                                                
                                                <span class="efficiency" data-val="<?php echo $attendance['efficiency'] ?>" data-toggle="tooltip" title="<?php echo $translator->translate('Actual form') ?>">(<?php echo $attendance['efficiency'] ?>)</span>
                                                
                                                
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>

                    </div>

                    <?php if (\Core\ServiceLayer::getService('TeamCreditManager')->teamHasFeatureAccess($team, 'print_lineup')): ?>
                    
                        <a data-toggle="tooltip" title="<?php echo $translator->translate('Print lineup') ?>" class="print-link print-lineup" href="<?php echo $router->link('team_match_print_lineup', array('id' => $lineup->getId())) ?>"><i class="ico ico-print"></i> <?php echo $translator->translate('Print lineup') ?></a>
                    <?php else: ?>
                        <a data-toggle="tooltip" title="<?php echo $translator->translate('Print lineup') ?>" class="team_credit_modal_trigge print-lineupr" href="#"><i class="ico ico-print"></i> <?php echo $translator->translate('Print lineup') ?></a></a>
                    <?php endif; ?>


                </div>
            </div>
        </div>
         <div class="col-md-4">
            <?php $layout->includePart(MODUL_DIR . '/Event/view/widget/_small_calendar.php') ?>
        </div>
    </div>

</section>

<?php $layout->includePart(MODUL_DIR . '/Team/view/events/_confirm_single_delete_modal.php', array('event' => $event)) ?>

<?php $layout->addJavascript('js/TeamMatchManager.js') ?>
<?php $layout->addJavascript('js/TeamEventManager.js') ?>
<?php $layout->addJavascript('js/SmallCalendar.js') ?>
<?php $layout->startSlot('javascript') ?>
<script type="text/javascript">
    var event_manger = new TeamEventsManager();
    event_manger.init();
function showEventLocation()
    {
        event_manger.google = google;
    }
    var match_manger = new TeamMatchManager();
    match_manger.initLineupTriggers();
    
     var small_calendar = new SmallCalendar({
        'dataUrl': '<?php echo $router->link('rest_event_get_month_grid') ?>',
        'localityManager': new LocalityManager()
    });
    small_calendar.initLoad();
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXU1duK54f1gIsRy_xc7z4uQURovsZdE8&libraries=places&callback=showEventLocation" async defer></script>

<?php $layout->endSlot('javascript') ?>