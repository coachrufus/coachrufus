<li class="timeline_event timeline_event_<?php echo $group[0]->getLineupPosition() ?>">
    <span class="timeline_event_icon"><i class="ico ico-ball"></i></span>

    <div class="timeline-label">
       
            <div class="input-group edit_hit_time_wrap">
                <input  name="hit_time" type="text" value="<?php echo $group[0]->getHitTime() ?>" class="form-control" data-inputmask='"mask": "99:99:99"' data-mask>
                <a href="<?php echo $router->link('team_match_edit_timeline_time', array('hg' => $groupId)) ?>" class="input-group-addon  confirm_hit_time"><i  class="ico ico-check"></i></a>
            </div>
        
        
            <div class="current_hit_time"> 
           <?php if('second_line' == $group[0]->getLineupPosition()): ?>
               <i  class="ico ico-edit"></i> <span><?php echo $group[0]->getHitTime() ?></span>
           <?php else: ?> 
                <span><?php echo $group[0]->getHitTime() ?></span><i  class="ico ico-edit"></i>
           <?php endif; ?> 
           </div>
       
    </div>

     <div class="timeline_goal edit-timeline-item">
        <div class="players_group">  
        <?php foreach ($group as $item): ?>
        
            <div id="timeline_item_<?php echo $item->getId() ?>" class="timeline_player_row player_row_<?php echo $item->getHitType() ?>" data-hid="<?php echo $item->getId() ?>">

                <?php if('first_line' == $group[0]->getLineupPosition()): ?>
                    <span class="player_name"></i><?php echo $item->getPlayerName() ?></span>
                    <span class="icon">
                       <?php if(array_key_exists($item->getLineupPlayerId(), $players)  && null != $players[$item->getLineupPlayerId()]->getTeamPlayer()): ?>
                           <div class="round-50" style="background-image:url(<?php echo $players[$item->getLineupPlayerId()]->getTeamPlayer()->getMidPhoto() ?>)"></div>
                       <?php endif; ?>
                   </span>
                   <span class="acr"><?php echo $item->getHitTypeAcr() ?></span>
                <?php else: ?>
                    <span class="acr"><?php echo $item->getHitTypeAcr() ?></span>

                    <span class="icon">
                       <?php if(array_key_exists($item->getLineupPlayerId(), $players) && null != $players[$item->getLineupPlayerId()]->getTeamPlayer()): ?>
                           <div class="round-50" style="background-image:url(<?php echo $players[$item->getLineupPlayerId()]->getTeamPlayer()->getMidPhoto() ?>)"></div>
                       <?php endif; ?>
                   </span>
                   <span class="player_name"><?php echo $item->getPlayerName() ?></span>
                <?php endif; ?> 
            </div>
        
        <?php endforeach; ?>
        </div>
         
        <?php if($group[0]->getHitType() == 'mom'): ?>
             <i class="ico ico-edit edit-timeline-item-trigger" data-type="mom"  data-time="<?php echo $groupTime ?>"></i>
        <?php else: ?>
            <i class="ico ico-edit edit-timeline-item-trigger"  data-type="" data-time="<?php echo $groupTime ?>"></i>
            <a href="<?php echo $router->link('team_match_remove_timeline_stat',array('hg' => $groupId)) ?>" class="delete-timeline-item-trigger"><i class="ico ico-trash"></i></a>
        <?php endif; ?>
        
         
        
    </div>
</li>