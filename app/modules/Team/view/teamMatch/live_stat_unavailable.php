<?php Core\Layout::getInstance()->startSlot('team_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/Team/view/team/_team_menu.php', array('team' => $team)) ?>
<?php Core\Layout::getInstance()->endSlot('team_menu') ?>


<section class="content-header">
        <a class="all-event-link" href="<?php echo $router->link('team_event_list', array('team_id' => $team->getId())) ?>"><i class="ico ico-event-back"></i><?php echo $translator->translate('All Events') ?></a>
    
     <?php echo $layout->renderControllerAction('CR\Gamifications\GamificationModule:Progress:progressBar')->getContent() ?>
    
    <?php
    $layout->includePart(GLOBAL_DIR . '/templates/parts/_breadcrumb.php', array(
        'crumbs' => array(
            'Teams' => $router->link('teams_overview'),
            $team->getName() => $router->link('team_overview', array('id' => $team->getId())),
            'Events' => $router->link('team_event_list', array('team_id' => $team->getId())),
            $event->getName() => ''
)))
    ?>
        
        
</section>

<section class="content">
    <div class="row">
        <div class="col-md-8">
            <div class="event-detail">
                <div class="panel panel-default ">
                    <?php
                    $layout->includePart(MODUL_DIR . '/Team/view/events/_event_detail.php', array(
                        'event' => $event,
                        'team' => $team,
                        'existLineup' => $lineup,
                        'menuActive' => 'score',
                    ))
                    ?>

                  
                        <!-- timeline time label -->

                       <div class="row">
                        <div class="col-sm-4">
                            <img src="/img/main/logo-sq.svg" />
                        </div>
                        <div class="col-sm-8 text-left">
                            <?php echo $translator->translate('LIVE_STAT_UNAVAILABLE') ?><a style="text-decoration: underline;" href="<?php echo $router->link('team_match_new_lineup',array('event_id'=>$event->getId(),'event_date' => $event->getCurrentDate()->format('Y-m-d'))) ?>"> <?php echo $translator->translate('LIVE_STAT_UNAVAILABLE_CREATE') ?></a> <?php echo $translator->translate('LIVE_STAT_UNAVAILABLE_ROSTER') ?>. <br />
                            
                            
                        </div>
                        
                    </div>

                </div>
            </div>
        </div>
    </div>

</section>

<?php $layout->includePart(MODUL_DIR . '/Team/view/events/_confirm_single_delete_modal.php', array('event' => $event)) ?>
<?php $layout->includePart(MODUL_DIR . '/Team/view/teamMatch/_confirm_save_modal.php') ?>
<?php $layout->includePart(MODUL_DIR . '/Team/view/teamMatch/_confirm_delete_modal.php') ?>
<?php $layout->addJavascript('plugins/stopwatch/stopwatch.js') ?>
<?php $layout->addJavascript('js/TeamMatchManager.js') ?>
<?php $layout->addJavascript('js/TeamEventManager.js') ?>


<?php $layout->startSlot('javascript') ?>
<script src="/dev/plugins/input-mask/jquery.inputmask.js"></script>
<script type="text/javascript">
    $("[data-mask]").inputmask();

    match_manger = new TeamMatchManager();
    match_manger.initTimelineTriggers();
    
    var event_manger = new TeamEventsManager();
    event_manger.init();
 function showEventLocation()
    {
        event_manger.google = google;
    }

</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXU1duK54f1gIsRy_xc7z4uQURovsZdE8&libraries=places&callback=showEventLocation" async defer></script>
<?php $layout->endSlot('javascript') ?>