<h4><?php echo $event->getName() ?></a>  -  <?php echo $event->getFormatedCurrentDate() ?></a></h4>




<table class="table table-bordered  table-condensed" style="background-color: white; font-size:10px;">
    <thead>
        <tr>
            <td rowspan="2"><strong><?php echo $lineup->getFirstLineName() ?></strong></td>
            <td>1</td>
            <td>2</td>
            <td>3</td>
            <td>4</td>
            <td>5</td>
            <td>6</td>
            <td>7</td>
            <td>8</td>
            <td>9</td>
            <td>10</td>
            <td>11</td>
            <td>12</td>
            <td>13</td>
            <td>14</td>
            <td>15</td>
            <td>16</td>
            <td colspan="4">Total</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>1</td>
            <td>2</td>
            <td>3</td>
            <td>&#931;</td>
        </tr>
    </thead>
    <tbody>
        <?php $i = 1;foreach ($firstLine as $attendance): ?>
            <tr>
                <td><?php echo $attendance->getPlayerName() ?></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>	
            </tr>
    <?php endforeach; ?>
    </tbody>
</table>




<table class="table table-bordered  table-condensed" style="background-color: white;  font-size:10px;">
    <thead>
         <tr>
            <td rowspan="2"><strong><?php echo $lineup->getSecondLineName() ?></strong></td>
            <td>1</td>
            <td>2</td>
            <td>3</td>
            <td>4</td>
            <td>5</td>
            <td>6</td>
            <td>7</td>
            <td>8</td>
            <td>9</td>
            <td>10</td>
            <td>11</td>
            <td>12</td>
            <td>13</td>
            <td>14</td>
            <td>15</td>
            <td>16</td>
            <td colspan="4">Total</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>1</td>
            <td>2</td>
            <td>3</td>
            <td>&#931;</td>
        </tr>
    </thead>
    <tbody>
<?php $i = 1;foreach ($secondLine as $attendance): ?>
            <tr>
                <td><?php echo $attendance->getPlayerName() ?></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>

<?php endforeach; ?>
    </tbody>
</table>


<table class="table  table-condensed" style="background-color: white; font-size:10px;">
    <tr>
        <td>MoM:</td>
        <td></td>
        <td colspan="3"></td>
        <td>Total</td>
        <td></td>  
    </tr>
</table>

<?php $layout->startSlot('javascript') ?>

<script type="text/javascript">
window.print();
</script>

<?php $layout->endSlot('javascript') ?>
