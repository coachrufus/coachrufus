<div class="modal fade" id="confirm-stat-reset-modal" tabindex="-1" role="dialog" aria-labelledby="<?php echo $translator->translate('Reset Match') ?>">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $translator->translate('Reset Match') ?></h4>
      </div>
      <div class="modal-body">
          <?php echo $translator->translate('Are you sure?') ?>
          
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $translator->translate('Cancel') ?></button>
         <a href="" class="btn btn-danger modal_submit"><?php echo $translator->translate('Confirm') ?></a>
      </div>
    </div>
  </div>
</div>

