<?php Core\Layout::getInstance()->startSlot('team_menu') ?>  
   <?php $layout->includePart(MODUL_DIR.'/Team/view/team/_team_menu.php',array('team' => $team) ) ?>
<?php Core\Layout::getInstance()->endSlot('team_menu') ?>




<section class="content">

<ul class="timeline" id="stat_timeline">
    
    

    <!-- timeline time label -->
    <li class="time-label stopwatch_label">
        <span class="bg-aqua row">
            <span class="col-sm-2">
                <h3 id="first_line_goals">0</h3>
            </span>
            <span class="col-sm-8" id="stopwatch"></span>
            <span class="col-sm-2">
                <h3 id="second_line_goals">0</h3>
            </span>
        </span>
    </li>
    <!-- /.timeline-label -->
    
    
    <?php foreach($eventTimeline as $item): ?>
    <li class="time-label"><span class="bg-red"><?php echo $item->getHitTime()  ?></span></li>
    <li class="timeline_<?php echo $item->getHitType() ?> timeline_<?php echo $item->getLineupPosition() ?> timeline_event"><div class="timeline-item"><div class="timeline-body"><?php echo $hitEnum[$item->getHitType()]  ?>, <?php echo $item->getPlayerName() ?></div></div></li>
    <?php endforeach; ?>
    
    
    <li id="add_timeline_form">
        <div class="timeline-item">
            <div class="timeline-body">
                <a class="btn btn-primary timeline_stat_trigger" href="<?php echo $router->link('team_match_add_timeline_stat',array('event_id' => $event->getId(),'type' => 'goal','event_date' => $event->getCurrentDate()->format('Y-m-d'),'lid' => $lineup->getId() )) ?>" ><?php echo $translator->translate('Goal') ?></a> 
                <a class="btn btn-success timeline_stat_trigger" href="<?php echo $router->link('team_match_add_timeline_stat',array('event_id' => $event->getId(),'type' => 'assist','event_date' => $event->getCurrentDate()->format('Y-m-d'),'lid' => $lineup->getId())) ?>"  ><?php echo $translator->translate('Assist') ?></a>
                <a class="btn btn-info timeline_stat_trigger" href="<?php echo $router->link('team_match_add_timeline_stat',array('event_id' => $event->getId(),'type' => 'save','event_date' => $event->getCurrentDate()->format('Y-m-d'),'lid' => $lineup->getId())) ?>"  ><?php echo $translator->translate('Save') ?></a>
                <a class="btn btn-warning timeline_stat_trigger" href="<?php echo $router->link('team_match_add_timeline_stat',array('event_id' => $event->getId(),'type' => 'hit','event_date' => $event->getCurrentDate()->format('Y-m-d'),'lid' => $lineup->getId())) ?>"  ><?php echo $translator->translate('Hit') ?></a>
                
                <a class="btn btn-danger"><?php echo $translator->translate('Cancel') ?></a>
                
                <select class="form-control player_choice" name="">
                    <?php foreach($players as $player): ?>
                    <option data-lid-team="<?php echo $player->getLineupPosition()  ?>"  data-pid="<?php echo $player->getPlayerId() ?>" value="<?php echo $player->getId()  ?>"><?php echo $player->getPlayerName()  ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
    </li>
    
    <li id="add_timeline_mom_form">
        <div class="timeline-item">
            <div class="timeline-body">
                <select class="form-control player_choice" name="">
                    <?php foreach($players as $player): ?>
                    <option data-lid-team="<?php echo $player->getLineupPosition()  ?>"  data-pid="<?php echo $player->getPlayerId() ?>" value="<?php echo $player->getId()  ?>"><?php echo $player->getPlayerName()  ?></option>
                    <?php endforeach; ?>
                </select>
                
                <a href="#" class="btn btn-danger"><?php echo $translator->translate('Cancel') ?></a>
                <a href="<?php echo $router->link('team_match_add_timeline_mom',array('event_id' => $event->getId(),'type' => 'hit','event_date' => $event->getCurrentDate()->format('Y-m-d'),'lid' => $lineup->getId())) ?>" class="btn btn-success timeline_mom_trigger"><?php echo $translator->translate('Save') ?></a>
            </div>
        </div>
    </li>

  
    
     <li class="time-label ">
          <a href="#" class="btn btn-success add_stat_time_event"> <i class="fa fa-plus"></i>   <?php echo $translator->translate('Add timeline event') ?></a>
    </li>
    
     <li class="time-label">
         <a href="#" class="btn btn-info add_stat_time_mom"><?php echo $translator->translate('Set man of Match') ?></a>
    </li>
    


</ul>
    
         
</section>





<?php $layout->startSlot('javascript') ?>

 <script type="text/javascript">


match_manger = new TeamMatchManager();
match_manger.init();

$("#stopwatch").stopwatch();
</script>
  
<?php $layout->endSlot('javascript') ?>