
<?php Core\Layout::getInstance()->startSlot('stylesheet') ?>  
<link rel="stylesheet" href="/dev/plugins/datatables/dataTables.bootstrap.css">
<?php Core\Layout::getInstance()->endSlot('stylesheet') ?>

<?php Core\Layout::getInstance()->startSlot('team_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/Team/view/team/_team_menu.php', array('team' => $team)) ?>
<?php Core\Layout::getInstance()->endSlot('team_menu') ?>




<section class="content-header">
    <h1>
        <?php echo $translator->translate('Stats') ?>
    </h1>
    
     <?php $layout->includePart(GLOBAL_DIR.'/templates/parts/_breadcrumb.php',array(
        'crumbs' => array(
            'Teams' => $router->link('teams_overview'),
            $team->getName() => $router->link('team_overview',array('id' =>$team->getId() )),
            'Statistics' => ''
            ))) ?>
</section>
<section class="content">
    
    
    
    
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                
                
                
            <div class="box box-header">
                
                

            <form action="" class="form-inline pull-left" method="get" id="season_stat_filter_form">

                <?php echo $translator->translate('Season') ?>:
                <?php echo $filterForm->renderSelectTag('season_id',array('class' => 'form-control')) ?>
                <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-refresh"></i> <?php echo $translator->translate('Filter') ?></button>
            </form>
                <?php if( \Core\ServiceLayer::getService('security')->getIdentity()->hasTeamPermission('manageEvents',$team)): ?>

                    <a class="pull-right btn btn-info btn-sm" id="crs_settings_btn" href="<?php echo $router->link('team_crs_settings',array('team_id' => $team->getId())) ?>"><?php echo $translator->translate('CRS settings') ?></a>
<?php endif; ?>
                
            </div>
              
                <div class="box-body">
                    <table class="table table-bordered " id="stat_table">
                        <thead>
                            <tr>
                               <th> <?php echo $translator->translate('Player') ?><i cl</th>
                               <th>GP</th>
                                <th>W</th>
                                <th>D</th>
                                <th>L</th>
                                <th>%</th>
                                <th>sPlus</th>
                                <th>sMinus</th>
                                <th>+/-</th>
                                <th><?php echo $translator->translate('Goals') ?></th>
                                <th><?php echo $translator->translate('Assists') ?></th>
                                <th>avG</th>
                                <th>avA</th>
                                <th>G+A</th>
                                <th>avGA</th>
                                <th>CRS</th>
                                <th>avCRS</th>
                                <th><?php echo $translator->translate('GAA') ?></th>
                                <th><?php echo $translator->translate('Shotouts') ?></th>
                                <th><?php echo $translator->translate('Man of match') ?></th>
                                <th>M%M</th>
                            </tr>
                        </thead>
                        <tbody>
                    <?php foreach ($list as $player):; $stat = $player->getStats() ?>
                   <tr>
                       <td><a href="<?php echo $router->link('team_player_stats',array('player_id' => $player->getId() )) ?>"><?php echo $player->getFirstName() ?> <?php echo $player->getLastName() ?></a></td>
                       <?php if($stat != null): ?>
                            
                       <td data-order="<?php echo  $stat->getGamePlayed() ?>"><span class="ratio gp_ratio" style="width:<?php echo  $stat->getGamePlayedMaxPercent()?>%"></span><span class="stat_value"><?php echo  $stat->getGamePlayed() ?></span></td>
                         <td data-order="<?php echo  $stat->getWins() ?>"><span class="ratio win_ratio" style="width:<?php echo  $stat->getWinsMaxPercent()?>%"></span><span class="stat_value"><?php echo  $stat->getWins() ?></span></td>
                         <td data-order="<?php echo  $stat->getDraws() ?>"><span class="ratio draw_ratio" style="width:<?php echo  $stat->getDrawsMaxPercent()?>%"></span><span class="stat_value"><?php echo  $stat->getDraws() ?></span></td>
                          <td data-order="<?php echo  $stat->getLooses() ?>"><span class="ratio loose_ratio" style="width:<?php echo  $stat->getLooseMaxPercent()?>%"></span><span class="stat_value"><?php echo  $stat->getLooses() ?></span></td>
                                       
                          <td data-order="<?php echo  $stat->getWinsPrediction() ?>" class="ratio prediction_ratio_<?php echo $stat->getWinsPredictionAreaPercent() ?>"><?php echo  $stat->getWinsPrediction() ?></td>
                                       


                            <td data-order="<?php echo  $stat->getPlusPoints() ?>"><?php echo $stat->getPlusPoints() ?></td>
                            <td  data-order="<?php echo  $stat->getMinusPoints() ?>"><?php echo $stat->getMinusPoints() ?></td>
                            <td data-order="<?php echo  $stat->getPlusMinusDiff() ?>">
                                <?php if($stat->getPlusMinusDiff() > 0): ?>
                                    <span class="ratio  gp_ratio" style="left:<?php echo $stat->getPlusMinusDiffOffset('plusOffset') ?>%;width:<?php echo $stat->getPlusMinusDiffOffset('length') ?>%"></span>
                                <?php endif; ?>
                                    
                                 <?php if($stat->getPlusMinusDiff() < 0): ?>
                                    <span class="ratio loose_ratio" style="left:<?php echo $stat->getPlusMinusDiffOffset('minusMargin') ?>%;width:<?php echo $stat->getPlusMinusDiffOffset('length') ?>%"></span>
                                <?php endif; ?>
                                    
                                <span class="stat_value"><?php echo  $stat->getPlusMinusDiff() ?></span>

                            </td>
                            
                            <td data-order="<?php echo  $stat->getGoals() ?>"><span class="ratio win_ratio" style="width:<?php echo  $stat->getGoalMaxPercent()?>%"></span><span class="stat_value"><?php echo $stat->getGoals() ?></span></td>
                            <td data-order="<?php echo  $stat->getAssists() ?>"><span class="ratio draw_ratio" style="width:<?php echo  $stat->getAssistsMaxPercent()?>%"></span><span class="stat_value"><?php echo $stat->getAssists() ?></span></td>
                            <td data-order="<?php echo  $stat->getAverageGoals() ?>" class="ratio prediction_ratio_<?php echo $stat->getAverageGoalsAreaPercent() ?>"><?php echo $stat->getAverageGoals() ?></td>
                            <td data-order="<?php echo  $stat->getAverageAssists() ?>" class="ratio prediction_ratio_<?php echo $stat->getAverageAssistsAreaPercent() ?>"><?php echo $stat->getAverageAssists() ?></td>
                            <td data-order="<?php echo  $stat->getGA() ?>"><span class="ratio gp_ratio" style="width:<?php echo  $stat->getGAMaxPercent()?>%"></span><span class="stat_value"><?php echo $stat->getGA() ?></span></td>
                             
                            <td  data-order="<?php echo  $stat->getAverageGA() ?>" class="ratio prediction_ratio_<?php echo $stat->getAverageGAAreaPercent() ?>"><?php echo $stat->getAverageGA() ?></td>
                            
                           
                            <td data-order="<?php echo $stat->getCRS() ?>">
                                <span class="ratio win_ratio" style="width:<?php echo  $stat->getCRSMaxPercent()?>%"></span><span class="stat_value"><?php echo  $stat->getCRS() ?></span>
                              
                                <!--
                                <?php echo $translator->translate('Goals') ?>:<?php echo $stat->getCRSGroup()['goal'] ?>
                                <?php echo $translator->translate('assists') ?>:<?php echo $stat->getCRSGroup()['assists'] ?>
                                <?php echo $translator->translate('Win') ?>:<?php echo $stat->getCRSGroup()['game_win'] ?>
                                <?php echo $translator->translate('Draw') ?>:<?php echo $stat->getCRSGroup()['game_draw'] ?>
                                <?php echo $translator->translate('MoM') ?>:<?php echo $stat->getCRSGroup()['man_of_match'] ?>
                                -->
                            </td>
                            
                             <td data-order="<?php echo  $stat->getAverageCRS() ?>" class="ratio prediction_ratio_<?php echo $stat->getAverageCRSAreaPercent() ?>"><?php echo $stat->getAverageCRS() ?></td>
                            

                             <td data-order="<?php echo  $stat->getGoalAgainstAverage() ?>"><?php echo  ($stat->getGoalAgainstAverage() == 0) ? '-' : $stat->getGoalAgainstAverage() ?></td>
                            <td data-order="<?php echo  $stat->getGoalkeeperShotouts() ?>"><?php echo  ($stat->getGoalkeeperShotouts() == 0) ? '-' : $stat->getGoalkeeperShotouts() ?></td>
      

                            
                             <td data-order="<?php echo  $stat->getManOfMatch() ?>"><span class="ratio gp_ratio" style="width:<?php echo  $stat->getMoMMaxPercent()?>%"></span><span class="stat_value"><?php echo $stat->getManOfMatch() ?></span></td>
                            
                            <td  data-order="<?php echo  $stat->getAverageManOfMatch() ?>" class="ratio prediction_ratio_<?php echo $stat->getAverageMoMAreaPercent() ?>"><?php echo $stat->getAverageManOfMatch() ?></td>
                   
                            
                       <?php else: ?>
                            
                            <td data-order="0" >-</td>
                            <td data-order="0" >-</td>
                            <td data-order="0" >-</td>
                            <td data-order="0" >-</td>
                            <td data-order="0" >-</td>
                            <td data-order="0" >-</td>
                            <td data-order="0" >-</td>
                            <td data-order="0" >-</td>
                            <td data-order="0" >-</td>
                            <td data-order="0" >-</td>
                            <td data-order="0" >-</td>
                            <td data-order="0" >-</td>
                            <td data-order="0" >-</td>
                            <td data-order="0" >-</td>
                            <td data-order="0" >-</td>
                            <td data-order="0" >-</td>
                            <td data-order="0" >-</td>
                            <td data-order="0" >-</td>
                            <td data-order="0" >-</td>
                            <td data-order="0" >-</td>
                          
                           
                    <?php endif; ?>
                       
                            
                        </tr>
                        
                       

                    <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
     
    </div>
</section>


<?php $layout->startSlot('javascript') ?>
<?php  $layout->addJavascript('plugins/datatables/jquery.dataTables.min.js')  ?>
<?php  $layout->addJavascript('plugins/datatables/dataTables.bootstrap.min.js')  ?>
<?php  $layout->addJavascript('plugins/datatables/'.LANG.'.js')  ?>
<?php  $layout->addJavascript('plugins/datatables/extensions/FixedColumns/js/dataTables.fixedColumns.min.js')  ?>
 <script>
      $(function () {
        
        //var datatables_trans = new datatablesTrans();
        var table =  $("#stat_table").DataTable({
             "scrollX": true,
             "scrollY":false,
             "iDisplayLength": 10,
             "aLengthMenu": [[5, 10, 25, 50,100, -1], [5, 10, 25, 50,100, "<?php echo $translator->translate('All') ?>"]],
             "language": datatablesTrans,
             "order": [[ 15, "desc" ]],
             "dom": '<"top"iflp<"clear">>rt<"bottom"iflp<"clear">>',
        });
        
        new $.fn.dataTable.FixedColumns( table );
       
      });
      
      $('.scroll_table_right').on('click',function(e){
          e.preventDefault();
          $('#stat_table').scrollLeft = 50;
          
      });
    </script>
  
<?php $layout->endSlot('javascript') ?>
