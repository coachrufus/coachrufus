<?php Core\Layout::getInstance()->startSlot('team_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/Team/view/team/_team_menu.php', array('team' => $team)) ?>
<?php Core\Layout::getInstance()->endSlot('team_menu') ?>

<section class="content-header">

    <a class="all-event-link" href="<?php echo $router->link('team_event_list', array('team_id' => $team->getId())) ?>"><i class="ico ico-event-back"></i><?php echo $translator->translate('All Events') ?></a>
    <?php
    $layout->includePart(GLOBAL_DIR . '/templates/parts/_breadcrumb.php', array(
        'crumbs' => array(
            'Teams' => $router->link('teams_overview'),
            $team->getName() => $router->link('team_overview', array('id' => $team->getId())),
            'Events' => $router->link('team_event_list', array('team_id' => $team->getId())),
            'New' => ''
)))
    ?>
</section>



<section class="content">
    <div class="row">
        <div class="col-md-8">
            <div class="event-detail">
                <div class="panel panel-default rating-panel">
                    <?php
                    $layout->includePart(MODUL_DIR . '/Team/view/events/_event_detail.php', array(
                        'event' => $event,
                        'team' => $team,
                        'existLineup' => $lineup,
                        'menuActive' => 'score',
                    ))
                    ?>

                    <div class="row panel-timeline winner-<?php echo $matchResult['winner']  ?>">
                        <div class="team_row team_row_first_line">
                            <strong><?php echo $lineup->getFirstLineName() ?></strong>
                            <span id="first_line_goals"><?php echo $matchOverview->getFirstLinePoints() ?></span>
                        </div>
                        <div class="team_row_colon">:</div>
                        <div class="team_row team_row_second_line">
                            <span  id="second_line_goals"><?php echo $matchOverview->getSecondLinePoints() ?></span>
                            <strong><?php echo $lineup->getSecondLineName() ?></strong>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                             <table class="table table-bordered" style="background-color: white; font-size:10px;">
                            <thead>
                                <tr>
                                    <td></td>
                                    <td>1 <?php echo $translator->translate('Points') ?></td>
                                    <td>2 <?php echo $translator->translate('Points') ?></td>
                                    <td>3 <?php echo $translator->translate('Points') ?></td>
                                    <td><?php echo $translator->translate('Total Points') ?></td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($lineupPlayers['first_line'] as $lineupPlayer): ?>
                                    <tr class="player-points-row">
                                        <td><?php echo $lineupPlayer->getPlayerName() ?></td>


                                        <td><?php echo $matchOverview->getPlayerPoints($lineupPlayer->getTeamPlayerId(),'points1') ?></td>
                                        <td><?php echo $matchOverview->getPlayerPoints($lineupPlayer->getTeamPlayerId(),'points2') ?></td>
                                        <td><?php echo $matchOverview->getPlayerPoints($lineupPlayer->getTeamPlayerId(),'points3') ?></td>
                                        <td class="total-player-points" data-team-points="total-team-points-a"><?php echo $matchOverview->getPlayerTotalPoints($lineupPlayer->getTeamPlayerId()) ?></td>	
                                    </tr>
                            <?php endforeach; ?>
                            </tbody>
                            </table>
                        </div>
                         <div class="col-sm-6">
                             <table class="table table-bordered" style="background-color: white; font-size:10px;">
                            <thead>
                                <tr>
                                    <td></td>
                                    <td>1 <?php echo $translator->translate('Points') ?></td>
                                    <td>2 <?php echo $translator->translate('Points') ?></td>
                                    <td>3 <?php echo $translator->translate('Points') ?></td>
                                    <td><?php echo $translator->translate('Total Points') ?></td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($lineupPlayers['second_line'] as $lineupPlayer): ?>
                                    <tr class="player-points-row">
                                        <td><?php echo $lineupPlayer->getPlayerName() ?></td>


                                        <td><?php echo $matchOverview->getPlayerPoints($lineupPlayer->getTeamPlayerId(),'points1') ?></td>
                                        <td><?php echo $matchOverview->getPlayerPoints($lineupPlayer->getTeamPlayerId(),'points2') ?></td>
                                        <td><?php echo $matchOverview->getPlayerPoints($lineupPlayer->getTeamPlayerId(),'points3') ?></td>
                                        <td class="total-player-points" data-team-points="total-team-points-a"><?php echo $matchOverview->getPlayerTotalPoints($lineupPlayer->getTeamPlayerId()) ?></td>	
                                    </tr>
                            <?php endforeach; ?>
                            </tbody>
                            </table>
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <?php $layout->includePart(MODUL_DIR . '/Event/view/widget/_small_calendar.php') ?>
        </div>
    </div>
</section>




<?php $layout->addJavascript('js/TeamEventManager.js') ?>
<?php $layout->addJavascript('js/WidgetManager.js') ?>
<?php $layout->addJavascript('js/SmallCalendar.js') ?>





