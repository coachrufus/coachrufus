<?php Core\Layout::getInstance()->startSlot('team_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/Team/view/team/_team_menu.php', array('team' => $team)) ?>
<?php Core\Layout::getInstance()->endSlot('team_menu') ?>


<section class="content-header">
    <a class="all-event-link" href="<?php echo $router->link('team_event_list', array('team_id' => $team->getId())) ?>"><i class="ico ico-event-back"></i><?php echo $translator->translate('All Events') ?></a>
    
     <?php echo $layout->renderControllerAction('CR\Gamifications\GamificationModule:Progress:progressBar')->getContent() ?>
    
    <?php
    $layout->includePart(GLOBAL_DIR . '/templates/parts/_breadcrumb.php', array(
        'crumbs' => array(
            'Teams' => $router->link('teams_overview'),
            $team->getName() => $router->link('team_overview', array('id' => $team->getId())),
            'Events' => $router->link('team_event_list', array('team_id' => $team->getId())),
            $event->getName() => ''
)))
    ?>
</section>

<section class="content">

    <div class="row">
        <div class="col-md-8">
            <div class="event-detail">
                <div class="panel panel-default">

                     <?php
                        $layout->includePart(MODUL_DIR . '/Team/view/events/_event_detail_webview.php', array(
                            'event' => $event,
                            'team' => $team,
                             'existLineup' => $existLineup,
                             'menuActive' => 'lineup',
                            'user' => $user
                        ))
                    ?>
                    
                    <div class="event-detail-part">
                    <?php
                    
                        $layout->includePart(MODUL_DIR . '/Team/view/events/_event_detail.php', array(
                            'event' => $event,
                            'team' => $team,
                             'existLineup' => $existLineup,
                             'menuActive' => 'lineup',
                        ))
                    ?>
                    </div>
                    
                  

                    <div class="row lineup-panel">
                        
                        
                         <div class="create-linuep-wrap">
                            <div class="<?php echo ($menuActive == 'lineup') ? 'active' : '' ?> btn-group" role="group">
                                <a  class="dropdown-toggle clear-dropdown create-linuep-select" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                   <i class="ico ico-book"></i> <?php echo $translator->translate('Create lineup') ?> <i class="ico ico-arr-down-green"></i>
                                </a>
                                <ul class="dropdown-menu">
                                     <li>
                                        <?php if($showEfficiencyCreate): ?>
                                             <a href="<?php echo $router->link('team_match_create_lineup', array('type' => 'efficiency', 'event_id' => $event->getId(), 'event_date' => $event->getCurrentDate()->format('Y-m-d'))) ?>#create_lineup"><?php echo $translator->translate('Players efficiency') ?></a>
                                         <?php elseif($wizardWinished): ?>
                                             <a class="team_credit_modal_trigger" href="#"><?php echo $translator->translate('Players efficiency') ?><button class="pro-icon ">PRO</button></a>
                                        <?php endif; ?>
                                    </li>
                                    
                                     <li> 
                                    <?php if($showRandomCreate): ?>
                                         <a href="<?php echo $router->link('team_match_create_lineup', array('type' => 'random', 'event_id' => $event->getId(), 'event_date' => $event->getCurrentDate()->format('Y-m-d'))) ?>#create_lineup"><?php echo $translator->translate('Random') ?></a>
                                       <?php elseif($wizardWinished): ?>
                                         <a class="team_credit_modal_trigger" href="#"><?php echo $translator->translate('Random') ?><button class="pro-icon">PRO</button></a>
                                     <?php endif; ?>
                                     </li>
                                    
                                    <li> <a href="<?php echo $router->link('team_match_create_lineup', array('type' => 'manual', 'event_id' => $event->getId(), 'event_date' => $event->getCurrentDate()->format('Y-m-d'))) ?>#create_lineup"><?php echo $translator->translate('Manual') ?></a></li>

                                </ul>
                            </div>
                        </div>
                        
                        
                        
                        <div class="col-sm-6 first-line-col">
                            <h3 class="box-title"><input class="form-control inline-edit inline-edit-inactive" type="text" name="first_line_name" data-url="<?php echo $router->link('team_match_update_lineup', array('id' => $lineup->getId())) ?>"  value="<?php echo $lineup->getFirstLineName() ?>"/></h3>
                            
                              <div class="power-row"><?php echo $translator->translate('Power') ?> 
                                (<span class="power-total"></span>)
                                <span class="power-perc"></span>
                                <span class="power-indicator"></span>
                            
                            </div>


                            <div class="progress"><div class="progress-bar progress-bar-green"  id="first_line_eff_sum" role="progressbar"></div></div>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <td colspan="2">
                                            <div class="btn-group" role="group">
                                                <button type="button" class="btn dropdown-toggle add-lineup-player-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="ico ico-dress"></i> <?php echo $translator->translate('Add player') ?>
                                                </button>
                                                <ul class="dropdown-menu add-first-lineup-player-choice">
                                                    <?php foreach ($availablePlayers as $member): ?>

                                                        <li> <a data-target="first_line" data-playerid="<?php echo $member->getPlayerId() ?>" data-name="<?php echo $member->getFullName() ?>" data-lineup="<?php echo $lineup->getId() ?>"  data-eff="<?php echo $member->getStats()->getLastGamesEfficiency() ?>" data-team-player-id="<?php echo $member->getId() ?>" data-icon="<?php echo $member->getMidPhoto() ?>" data-team-role="<?php echo $member->getTeamRoleName() ?>" data-team-number="<?php echo $member->getPlayerNumber() ?>" class="add-lineup-player" href=""><?php echo $member->getFullName() ?></a></li>
                                                    <?php endforeach; ?>
                                                  
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>

                                    <tr class=" new-player-form" id="first_line_new_player">
                                        <td>
                                            <input class="new-player-name form-control" type="text" placeholder="<?php echo $translator->translate('Player name') ?>">
                                        </td>
                                        <td>
                                            <button data-target="first_line" data-lineup="<?php echo $lineup->getId() ?>" class="btn btn-success add-new-lineup-player" type="button"> <i class="fa fa-check"></i> <?php echo $translator->translate('Add') ?></button>
                                        </td>
                                    </tr>

                                </thead>


                                <tbody id="first_line">
                                    <?php foreach ($firstLine as $attendance): ?>
                                        <tr class="lineup_row">
                                            <td>
                                                <div class="user-block pull-left">
                                                    <?php if(null !=  $attendance['teamPlayer'] ): ?>
                                                         <div class="round-50" style="background-image:url(<?php echo $attendance['teamPlayer']->getMidPhoto() ?>)"></div>
                                                    <?php endif; ?>
                                                    <span class="username"><?php echo $attendance['player']->getPlayerName() ?></span>
                                                     <?php if(null !=  $attendance['teamPlayer'] ): ?>
                                                        <span class="description"><?php echo $attendance['teamPlayer']->getTeamRoleName() ?> <?php echo ($attendance['teamPlayer']->getPlayerNumber() != null) ? '#' . $attendance['teamPlayer']->getPlayerNumber() : '' ?></span>
                                                        
                                                    <?php endif; ?>
                                                </div><!-- /.user-block -->
                                                <span class="efficiency" data-val="<?php echo $attendance['efficiency'] ?>" data-toggle="tooltip" title="<?php echo $translator->translate('Actual form') ?>">
                                                    (<?php echo $attendance['efficiency'] ?>)
                                                </span>
                                            </td>
                                            <td class="change_lineup_wrap">
                                                
                                                <button class="btn btn-danger remove_player" data-eff="<?php echo $attendance['efficiency'] ?>" data-rel="<?php echo $attendance['player']->getId() ?>"><i class="ico ico-close-black"></i></button>
                                                <button class="btn change_lineup" data-rel="<?php echo $attendance['player']->getId() ?>"><i class="ico ico-arr-rgt-green2"></i></button>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>

                                </tbody>
                            </table>
                        </div>

                        <div class="col-sm-6 second-line-col">


                            <h3 class="box-title"><input class="form-control inline-edit inline-edit-inactive" type="text" name="second_line_name" data-url="<?php echo $router->link('team_match_update_lineup', array('id' => $lineup->getId())) ?>"  value="<?php echo $lineup->getSecondLineName() ?>"/></h3>
                            
                            <div class="power-row"><?php echo $translator->translate('Power') ?> 
                                (<span class="power-total"></span>)
                                <span class="power-perc"></span>
                                <span class="power-indicator"></span>
                            
                            </div>
                            
                            
                            <div class="progress">
                                <div class="progress-bar progress-bar-green"  id="second_line_eff_sum" role="progressbar">
                                </div>
                            </div>

                            <table class="table">
                                <thead>
                                    <tr>
                                          <td colspan="2">
                                            <div class="btn-group" role="group">
                                                <button type="button" class="btn dropdown-toggle add-lineup-player-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="ico ico-dress"></i> <?php echo $translator->translate('Add player') ?>
                                                </button>
                                                <ul class="dropdown-menu add-second-lineup-player-choice">
                                                    <?php foreach ($availablePlayers as $member): ?>
                                                        <li> <a data-target="second_line" data-playerid="<?php echo $member->getPlayerId() ?>" data-name="<?php echo $member->getFullName() ?>" data-lineup="<?php echo $lineup->getId() ?>" data-eff="<?php echo $member->getStats()->getLastGamesEfficiency() ?>"  data-icon="<?php echo $member->getMidPhoto() ?>" data-team-role="<?php echo $member->getTeamRoleName() ?>" data-team-number="<?php echo $member->getPlayerNumber() ?>" class="add-lineup-player" data-team-player-id="<?php echo $member->getId() ?>" href=""><?php echo $member->getFullName() ?></a></li>
                                                    <?php endforeach; ?>
                                                   
                                                </ul>
                                            </div>
                                        </td>
                                       
                                    </tr>
                                    <tr id="second_line_new_player" class="new-player-form">
                                        <td>
                                            <input class="new-player-name form-control" type="text" placeholder="<?php echo $translator->translate('Player name') ?>">
                                        </td>
                                        <td>
                                            <button data-target="second_line" data-lineup="<?php echo $lineup->getId() ?>" class="btn btn-success btn-flat add-new-lineup-player" type="button"> <i class="fa fa-check"></i> <?php echo $translator->translate('Add') ?></button>
                                        </td>
                                    </tr>
                                </thead>


                                <tbody  id="second_line">
                                    <?php foreach ($secondLine as $attendance): ?>
                                        <tr class="lineup_row">

                                            <td class="change_lineup_wrap">
                                                <button class="btn change_lineup" data-rel="<?php echo $attendance['player']->getId() ?>"><i class="ico ico-arr-lft-green"></i></button>
                                                                                               <button class="btn btn-danger remove_player" data-eff="<?php echo $attendance['efficiency'] ?>" data-rel="<?php echo $attendance['player']->getId() ?>"><i class="ico ico-close-black"></i></button>

                                            </td>
                                            <td>
                                                
                                                 <div class="user-block">
                                                    <?php if(null !=  $attendance['teamPlayer'] ): ?>
                                                         <div class="round-50" style="background-image:url(<?php echo $attendance['teamPlayer']->getMidPhoto() ?>)"></div>
                                                    <?php endif; ?>
                                                    <span class="username"><?php echo $attendance['player']->getPlayerName() ?></span>
                                                     <?php if(null !=  $attendance['teamPlayer'] ): ?>
                                                        <span class="description"><?php echo $attendance['teamPlayer']->getTeamRoleName() ?> <?php echo ($attendance['teamPlayer']->getPlayerNumber() != null) ? '#' . $attendance['teamPlayer']->getPlayerNumber() : '' ?></span>
                                                        
                                                    <?php endif; ?>
                                                </div><!-- /.user-block -->
                                                
                                                <span class="efficiency" data-val="<?php echo $attendance['efficiency'] ?>" data-toggle="tooltip" title="<?php echo $translator->translate('Actual form') ?>">(<?php echo $attendance['efficiency'] ?>)</span>
                                                
                                                
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>

                    </div>

                   

                    <div class="print-lineup-wrap mob_hide">
                    <p><?php echo $translator->translate('Na zapisovanie skóre si môžete vytlačiť predlohu a nahodiť ho do aplikácie neskôr') ?></p>
                    <a data-toggle="tooltip" title="<?php echo $translator->translate('Print lineup') ?>" class="print-link print-lineup" href="<?php echo $router->link('team_match_print_lineup', array('id' => $lineup->getId())) ?>"><i class="ico ico-print"></i> <?php echo $translator->translate('Print lineup') ?></a>
                   
                    </div>
                   
                
                <br />
 <br />

                </div>
            </div>
            
             <div class="row">
                <div class="col-sm-3 col-md-offset-4">

                <?php if($finishedProgressStep5): ?>
                    <a  href="<?php echo $router->link($event->getStatRoute(),array('event_id' => $event->getId(), 'eventdate' =>  $event->getCurrentDate()->format('Y-m-d'),'lid' =>$existLineup->getId()  )) ?>"  class="btn btn-primary pull-right btn_continue continue_btn_second"><?php echo $translator->translate('Continue') ?></a>&nbsp;
                     
                <?php endif; ?>
                </div>
            </div>
            
            
            
        </div>
        <div class="col-md-4">
            <?php $layout->includePart(MODUL_DIR . '/Event/view/widget/_small_calendar.php') ?>
        </div>
    </div>

</section>

<?php $layout->includePart(MODUL_DIR . '/Team/view/events/_confirm_single_delete_modal.php', array('event' => $event)) ?>
<?php $layout->includePart(MODUL_DIR . '/Team/view/events/_confirm_delete_modal.php', array('event' => $event)) ?>

<?php $layout->addJavascript('js/TeamMatchManager.js') ?>
<?php $layout->addJavascript('js/TeamEventManager.js') ?>
<?php $layout->addJavascript('js/SmallCalendar.js') ?>
<?php $layout->startSlot('javascript') ?>
<?php echo Core\ServiceLayer::getService('AchievementAlertManager')->showPlaceAchievementAlert('event_detail_roster','event_roster_manual_create',Core\ServiceLayer::getService('security')->getIdentity()) ?>
<?php echo Core\ServiceLayer::getService('AchievementAlertManager')->showPlaceAchievementAlert('event_detail_roster','event_roster_random_create',Core\ServiceLayer::getService('security')->getIdentity()) ?>
<?php echo Core\ServiceLayer::getService('AchievementAlertManager')->showPlaceAchievementAlert('event_detail_roster','event_roster_performance_create',Core\ServiceLayer::getService('security')->getIdentity()) ?>
<script type="text/javascript">
      small_calendar = new SmallCalendar({
        'dataUrl': '<?php echo $router->link('rest_event_get_month_grid') ?>',
        'localityManager': new LocalityManager()
    });
    small_calendar.initLoad();
    
    
    var event_manger = new TeamEventsManager();
    event_manger.init();
     function showEventLocation()
    {
        event_manger.google = google;
    }

    var match_manger = new TeamMatchManager();
    match_manger.initLineupTriggers();
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXU1duK54f1gIsRy_xc7z4uQURovsZdE8&libraries=places&callback=showEventLocation" async defer></script>

<?php $layout->endSlot('javascript') ?>
