<div class="row team-public-detail">

    <div class="col-md-12">
        <!-- Widget: user widget style 1 -->
        <div class="box box-widget widget-user">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-aqua-active">
                <h3 class="widget-user-username"><?php echo $team->getName() ?></h3>
                <h5 class="widget-user-desc"><?php echo $translator->translate($sportEnum[$team->getSportId()]) ?></h5>
            </div>
            <div >
            <div class="widget-user-image img_round_wrap team_img_round">
                <div  style="background-image: url('<?php echo $team->getMidPhoto() ?>')" href="#" >

                </div>
            </div>
                </div>
          
        </div><!-- /.widget-user -->
    </div>

    <div class="col-md-8">
        <!-- USERS LIST -->
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title"><?php echo $translator->translate('Members') ?></h3>
            </div><!-- /.box-header -->
            <div class="box-body no-padding">
                <ul class="users-list clearfix">
                      <?php foreach ($players as $player): ?>
                    
                     <li>
                          <div class="img_round_wrap member_img_round_100" style="background-image: url('<?php echo $player->getMidPhoto() ?>');">&nbsp;</div>
                          <span class="users-list-name"><?php echo $player->getFirstName() ?> <?php echo $player->getLastName() ?></span>
                           <div class="starbox-fake ghosting" data-start-value="<?php echo  $player->getRating() ?>"> </div>
                                            
                                        
                    </li>
                    <?php endforeach; ?>
                    
                    
                  
                </ul><!-- /.users-list -->
            </div><!-- /.box-body -->
           
        </div><!--/.box -->
    </div>


    <div class="col-md-4">

        <div class="panel panel-default  hp-events">
            <div class="panel-heading">
                <i class="fa fa-calendar"></i> 
                <strong><?php echo $translator->translate('Events') ?></strong>
            </div>


            <div class="panel-body">

                <?php $i = 0;
                foreach ($eventsList as $dayEvents):
                    ?>
    <?php foreach ($dayEvents as $event): ?>

                        <div class="row<?php echo (fmod($i++, 2) == 0) ? ' dark' : ' white' ?>">
                            <div class="col-xs-4 event-date">
                                <span class="month-accr"><?php echo substr($translator->translate($event->getCurrentDate()->format('F')), 0, 4) ?> </span>
                                <span class="day"><?php echo $translator->getRegionalDate('%d', $event->getCurrentDate()) ?></span>
                                <span class="dayname"><?php echo $translator->translate($event->getCurrentDate()->format('l')) ?></span>
                                <span class="time"><?php echo $event->getStart()->format('H:i'); ?> </span>
                            </div>
                            <div class="col-xs-8 event-info">
                                <h3><?php echo $event->getName() ?></h3>
                                <?php echo $event->getPlaygroundName() ?>
                                <div class="row">
                                    <div class="col-xs-8 members-attendance-wrap" >
                                        <div class="members-attendance">
                                            <span><?php echo $translator->translate('Attendance') ?>:</span> <?php echo $event->getAttendanceInSum() ?> /<?php echo $event->getCapacity() ?>
                                            <div class="progress progress-xs">
                                                <div class="progress-bar progress-bar-success progress-bar-green" role="progressbar" aria-valuenow="<?php echo $event->getAttendanceInSum() ?>" aria-valuemin="0" aria-valuemax="<?php echo $event->getCapacity() ?>" style="width:  <?php echo round($event->getAttendanceInSum() / $event->getCapacity() * 100) ?>%">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                   
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
<?php endforeach; ?>



            </div>
        </div>
    </div>

  

</div>

<?php  $layout->addStylesheet('plugins/starbox/css/jstarbox.css') ?>
<?php  $layout->addJavascript('plugins/starbox/jstarbox.js') ?>
<?php  $layout->startSlot('javascript') ?>

<script type="text/javascript"> 
$('.starbox-fake').each(function(index,val){
    $(val).starbox({
        average: $(this).attr('data-start-value'),
        changeable: false
    })
});
</script>
<?php  $layout->endSlot('javascript') ?>