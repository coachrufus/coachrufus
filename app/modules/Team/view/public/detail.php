
<?php
$translator = Core\ServiceLayer::getService('translator');
?>

<?php Core\Layout::getInstance()->startSlot('team_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/Team/view/team/_team_menu.php', array('team' => $form->getEntity())) ?>
<?php Core\Layout::getInstance()->endSlot('team_menu') ?>

<section class="content-header">
    <a class="all-event-link" href="<?php echo $router->link('teams_overview') ?>"><i class="ico ico-event-back"></i><?php echo $translator->translate('All Teams') ?></a>
</section>

<?php Core\Layout::getInstance()->startSlot('crumb') ?>
<li><a href="<?php echo $router->link('teams_overview') ?>"><?php echo $translator->translate('Teams') ?></a></li>
<li class="active"><?php echo $translator->translate('Team detail') ?></li>
<?php Core\Layout::getInstance()->endSlot('crumb') ?>


<section class="content">

    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form action="#" class="form-bordered" method="post" id="team_form"  enctype="multipart/form-data">
                        <div class="row">
                            <div class="form-group col-sm-8">
                                <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("name")) ?><?php echo $form->getRequiredLabel('name') ?></label>
                                <?php echo $form->renderInputTag("name", array('class' => 'form-control')) ?>
                                <?php echo $validator->showError("name") ?>
                                <?php echo $validator->showError("name_unique") ?>
                            </div>

                            <div class="form-group col-sm-4">
                                <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("gender")) ?><?php echo $form->getRequiredLabel('gender') ?></label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="ico ico-people"></i>
                                    </div>
                                    <?php echo $form->renderSelectTag("gender", array('class' => 'form-control')) ?>
                                </div>
                                <?php echo $validator->showError("gender") ?>
                            </div>
                        </div>  

                        <div class="row"> 

                            <div class="form-group col-sm-4">
                                <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("sport_id")) ?></label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="ico ico-ball"></i>
                                    </div>
                                    <input type="text" name="" value="<?php echo $form->getEntity()->getSportName() ?>" class="form-control" disabled="disabled" />
                                    
                                </div>
                                <?php echo $validator->showError("sport_id") ?>
                            </div>


                            <div class="form-group col-sm-4">
                                <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("level")) ?></label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="ico ico-level"></i>
                                    </div>
                                    <?php echo $form->renderSelectTag("level", array('class' => 'form-control')) ?>
                                </div>
                                <?php echo $validator->showError("level") ?>
                            </div>


                            <div class="form-group col-sm-4">
                                <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("age_group")) ?></label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="ico ico-age"></i>
                                    </div>
                                    <?php echo $form->renderSelectTag("age_group", array('class' => 'form-control')) ?>
                                </div>
                                <?php echo $validator->showError("age_group") ?>
                            </div>




                        </div>

                        <div class="row"> 
                            <div class="form-group col-sm-12">
                                <label class="control-label"><?php echo $translator->translate('Adress') ?></label>
                                <div class="input-group playground-group">
                                    <span class="input-group-addon map_trigger">
                                        <a class="" href="#">
                                            <i class="ico ico-map-marker"></i>
                                        </a>
                                    </span>
                                    <input name="locality_name" id="locality-search" class="form-control" type="text" placeholder="<?php echo $translator->translate('Adress, City name, street, etc.') ?>">
                                </div>
                                <input type="hidden" name="locality_json_data" value="" id="locality_json_data" />
                                <?php echo $validator->showError("playground_empty") ?>
                            </div>

                            <div class="col-sm-12">
                                <div id="map_wrap">
                                    <div id="map"></div>
                                </div>
                            </div>
                        </div>



                        <a data-switch-text="<?php echo $translator->translate('Hide detail') ?>" class="color-link team-more-detail-trigger" role="group" href="#team-more-detail">
                            <i class="ico ico-plus-green"></i> <span><?php echo $translator->translate('More description') ?></span>
                        </a>
                        <input id="team_detail_status" type="hidden" name="show_detail" value="<?php echo $request->get('show_detail') == 'yes' ? 'yes' : 'no' ?>" />

                        <div id="team-more-detail" class="panel-collapse collapse <?php echo $request->get('show_detail') == 'yes' ? 'in' : 'out' ?>">
                            <div class="row">
                                <div class="form-group col-sm-4">
                                    <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("team status")) ?></label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="ico ico-space"></i>
                                        </div>
                                        <?php echo $form->renderSelectTag("status", array('class' => 'form-control')) ?>
                                    </div>
                                    <?php echo $validator->showError("status") ?>
                                </div>


                                <div class="form-group col-sm-4">
                                    <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("email")) ?></label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="ico ico-envelope"></i>
                                        </div>
                                        <?php echo $form->renderInputTag("email", array('class' => 'form-control')) ?>
                                    </div>
                                    <?php echo $validator->showError("email") ?>
                                </div>

                                
                                <div class="form-group col-sm-4">
                                   <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("phone")) ?></label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="ico ico-mobil"></i>
                                        </div>
                                        <?php echo $form->renderInputTag("phone", array('class' => 'form-control numeric-only')) ?>
                                    </div>
                                   <?php echo $validator->showError("phone") ?>
                                </div>
                                
                                <div class="form-group col-sm-6">
                                    <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("full_address")) ?></label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="ico ico-map-marker"></i>
                                        </div>
                                       <?php echo $form->renderInputTag("full_address", array('class' => 'form-control')) ?>
                                    </div>
                                   <?php echo $validator->showError("full_address") ?>
                                </div> 
                                
                                 <div class="form-group col-sm-6">
                                      <a class="add_playground_modal_trigger color-link pull-right"  href="#" type="button"><i class="ico ico-plus-green"></i> <?php echo $translator->translate('Add Playground') ?></a>
                                    <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("Playground")) ?></label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="ico ico-playground"></i>
                                        </div>
                                        <?php echo $form->renderSelectTag("exist_playgrounds", array('class' => 'form-control', 'id' => 'playground_choice')) ?>
                                    </div>
                                    <?php echo $validator->showError("exist_playgrounds") ?>
                                </div>
                                
                                
                                
                            </div>
                            <div class="row">
                                 <div class="form-group col-sm-4">
                                     <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("photo")) ?></label>
                                     <div class="row">
                                         <div class="col-sm-12">
                                             
                                             
                                             
                                              <?php if ($form->getEntity()->getPhoto()  != null): ?>
                                                <div id="exist-photo-wrap" style="background-image: url('<?php echo $form->getEntity()->getMidPhoto() ?>');">
                                               <?php else: ?>
                                               <div id="exist-photo-wrap" style="background-image: url('<?php echo $form->getEntity()->getDefaultPhoto() ?>');">
                                               <?php endif; ?>
                                                    <?php echo $form->renderInputHiddenTag('photo') ?>
                                                </div>
                                         
                                      
                                              <div id="photo-action">
                                                <a class="edit-team-photo color-link" href=""><?php echo $translator->translate('Change image') ?></a>
                                                

                                                
                                                <?php if ($form->getEntity()->getPhoto()  != null && $form->getEntity()->getPhoto() != $form->getEntity()->getDefaultPhoto()): ?>
                                                    <a class="delete-team-photo" href="<?php echo $router->link('team_settings_remove_photo', array('team_id' => $form->getEntity()->getId())) ?>"><i class="fa fa-times"></i></a>
                                                <?php endif; ?>
                                            </div>
                                         </div>
                                         
                                     </div>
                                </div>


                                <div class="form-group col-sm-8">
                                    <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("description")) ?><span class="help"><?php echo $translator->translate('Max. 1000 characters') ?></span></label>

                                    <?php echo $form->renderTextareaTag("description", array('class' => 'form-control')) ?>
                                    <?php echo $validator->showError("description") ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-12 text-right">
                                <button class="btn btn-primary btn-wizard-next" type="submit"> <span><?php echo $translator->translate('Save') ?></span>  <i class="fa fa-chevron-right"></i> </button>&nbsp;
                            </div>
                        </div>

                    </form>
                      <?php $layout->includePart(MODUL_DIR . '/Team/view/team/_edit_photo_modal.php', array('form' => $form)) ?>
                      <?php $layout->includePart(MODUL_DIR . '/TeamPlayground/view/crud/add_modal.php') ?>
                    
                    
                </div>
            </div>
        </div>
    </div>
</section>

<?php $layout->startSlot('javascript') ?>
<?php $layout->addJavascript('js/TeamManager.js') ?>
<?php $layout->addJavascript('js/PlaygroundManager.js') ?>
<script src="/dev/plugins/select2/select2.full.min.js"></script>
<script src="/dev/plugins/fileupload/jquery.iframe-transport.js"></script>
<script src="/dev/plugins/fileupload/jquery.fileupload.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXU1duK54f1gIsRy_xc7z4uQURovsZdE8&libraries=places&callback=initLocalityManager" async defer></script>
<script type="text/javascript">

    var team_manager = new TeamManager();
    team_manager.init();
    
    var playground_manager = new PlaygroundManager();
    

    $(".select2").select2();
    function initLocalityManager()
    {
        team_manager.google = google;
        
        team_manager.initMap();
       
        team_manager.createSearchInput();
        
        playground_manager.google = google;
        playground_manager.initModalCreate();
    }
    
    


</script>

<?php $layout->endSlot('javascript') ?>