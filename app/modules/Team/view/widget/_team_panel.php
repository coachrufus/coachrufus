                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3><i class="ico ico-team"></i> <?php echo $team->getName() ?></h3>
                        <div class="pull-right panel-tools">
                            <a href="#" class="panel-settings-trigger" data-target="team-panel-settings-<?php echo $team->getId() ?>"><i class="ico ico-action ico-menu-list"></i></a>
                            <i class="ico ico-action ico-panel-close"></i>
                        </div>
                    </div>
                    <div  class="panel-settings" id="team-panel-settings-<?php echo $team->getId() ?>">
                        <div class="settings-row">
                            <div class="settings-cell-2">
                                <?php if (\Core\ServiceLayer::getService('security')->getIdentity()->hasTeamPermission('manageSettings', $team)): ?>
                                <a  href="<?php echo $router->link('create_team_event',array('team_id' => $team->getId())) ?>">
                                    <i class="ico ico-plus"></i><?php echo $translator->translate('Add event') ?>
                                </a>
                                <?php endif; ?>
                            </div>
                            <div class="settings-cell-2 settings-cell-right">
                                <a  href="<?php echo $router->link('team_event_list',array('team_id' => $team->getId())) ?>">
                                    <?php echo $translator->translate('All events') ?> <i class="ico ico-calendar-empty ico-action"><?php echo date('d') ?></i>
                                </a>
                            </div>
                        </div>
                        
                        <div class="settings-row">
                            <div class="settings-cell-2">
                                <?php if (\Core\ServiceLayer::getService('security')->getIdentity()->hasTeamPermission('manageSettings', $team)): ?>
                                <a  href="<?php echo $router->link('team_players_list',array('team_id' => $team->getId())) ?>">
                                    <i class="ico ico-plus"></i><?php echo $translator->translate('Add member') ?>
                                </a>
                                <?php endif; ?>
                            </div>
                            <div class="settings-cell-2 settings-cell-right">
                                <a  href="<?php echo $router->link('team_public_players_list',array('team_id' => $team->getId())) ?>">
                                    <?php echo $translator->translate('All members') ?> <i class="ico ico-dress-b"></i>
                                </a>
                            </div>
                        </div>
                        
                        <div class="settings-row">
                            <div class="settings-cell-2">
                                
                            </div>
                            <div class="settings-cell-2 settings-cell-right">
                                <a  href="<?php echo $router->link('widget_list',array('team_id' => $team->getId())) ?>">
                                    <?php echo $translator->translate('All graphs') ?> <i class="ico ico-graph"></i>
                                </a>
                            </div>
                        </div> 
                           
                        <div class="settings-row">
                            <div class="settings-cell-2">
                                
                            </div>
                            <div class="settings-cell-2 settings-cell-right">
                                <a  href="<?php echo $router->link('team_settings',array('team_id' => $team->getId())) ?>">
                                    <?php echo $translator->translate('teampanel.menu.detail') ?> <i class="ico ico-team"></i>
                                </a>
                            </div>
                        </div> 
                        <div class="settings-row">
                            <div class="settings-cell-2">
                                
                            </div>
                            <div class="settings-cell-2 settings-cell-right">
                                <a  href="<?php echo $router->link('team_season_list',array('team_id' => $team->getId())) ?>">
                                    <?php echo $translator->translate('teampanel.menu.seasons') ?> <i class="ico ico-cup"></i>
                                </a>
                            </div>
                        </div> 
                    </div>
                    <div class="panel-body  panel-full-body team-panel fake-link" data-redirect="<?php echo $router->link('team_overview', array('id' => $team->getId())) ?>">
                        
                        <div class="panel-team-header">
                             <div class="img_round_wrap team_img_round">
                                <a  style="background-image: url('<?php echo $team->getMidPhoto() ?>')"  href="<?php echo $router->link('team_overview', array('id' => $team->getId())) ?>" ></a>
                            </div>
                            <div class="team-info">
                                <h3 class=""><?php echo $team->getName() ?></h3>
                                <h4 class=""><?php echo $translator->translate($sportEnum[$team->getSportId()]) ?></h4>
                                <small>[<?php echo $playerPermissiomEnum[$userTeamRoles[$team->getId()]['role']] ?>]</small>
                            </div>
                            <a class="team-more-link" href="<?php echo $router->link('team_overview', array('id' => $team->getId())) ?>"><i class="ico ico-team-more"></i></a>
                        </div>
                        
                        <div class="team-members-info">
                            <div>
                                <strong><?php echo $teamMembersInfo['members_count'] ?></strong>
                                <?php echo $translator->translate('Memebers') ?>
                            </div>
                            <div class="middle-border">
                                <strong><?php echo $teamMembersInfo['guest_count'] ?></strong>
                                <?php echo $translator->translate('Substitute') ?>
                            </div>
                            <div>
                                <strong><?php echo $teamMembersInfo['followers'] ?></strong>
                                <?php echo $translator->translate('Followers') ?>
                            </div>
                        </div>
                        
                        
                        <div class="team-member-stats">
                            <?php if($hasStat): ?>
                            <div class="avg-crs">
                                <div class="donut-wrap direction-<?php echo $graphValues['crs']['direction'] ?>">
                                    <div id="team-member-graph-crs-<?php echo $team->getId() ?>" class="team-member-graph" data-val1="<?php echo $graphValues['crs']['player'] ?>" data-val2="<?php echo $graphValues['crs']['max'] ?>" data-direction="<?php echo $graphValues['crs']['direction'] ?>"> </div>
                                    <span class="legend">
                                        <i  data-toggle="tooltip" data-placement="top" title="<?php echo $translator->translate('Current form:').$graphValues['crs']['lastGames']  ?>" class="ico <?php echo $graphValues['crs']['icon'] ?>"></i>
                                        <span><?php echo $graphValues['crs']['player'] ?></span>
                                </div>
                                 <strong><?php echo $translator->translate('CRS Average') ?></strong>
                            </div>
                            <div class="avg-goal">
                                <div class="donut-wrap direction-<?php echo $graphValues['goal']['direction'] ?>">
                                    <div id="team-member-graph-goal-<?php echo $team->getId() ?>" class="team-member-graph" data-val1="<?php echo $graphValues['goal']['player'] ?>" data-val2="<?php echo $graphValues['goal']['max'] ?>" data-direction="<?php echo $graphValues['goal']['direction'] ?>" > </div>
                                    <span class="legend">
                                        <i  data-toggle="tooltip" data-placement="top" title="<?php echo $translator->translate('Current form:').$graphValues['goal']['lastGames']  ?>" class="ico <?php echo $graphValues['goal']['icon'] ?>"></i>
                                        <span><?php echo $graphValues['goal']['player'] ?></span>
                                </div>
                                 <strong><?php echo $translator->translate('GOALS Average') ?></strong>
                            </div>
                            <div class="avg-assist">
                                <div class="donut-wrap direction-<?php echo $graphValues['assist']['direction'] ?>">
                                    <div id="team-member-graph-assist-<?php echo $team->getId() ?>" class="team-member-graph" data-val1="<?php echo $graphValues['assist']['player'] ?>" data-val2="<?php echo $graphValues['assist']['max'] ?>" data-direction="<?php echo $graphValues['assist']['direction'] ?>"> </div>
                                    <span class="legend">
                                        <i  data-toggle="tooltip" data-placement="top" title="<?php echo $translator->translate('Current form:').$graphValues['assist']['lastGames']  ?>" class="ico <?php echo $graphValues['assist']['icon'] ?>"></i>
                                        <span><?php echo $graphValues['assist']['player'] ?></span>
                                </div>
                                 <strong><?php echo $translator->translate('ASSISTANCE Average') ?></strong>
                            </div>
                        </div>
                        <?php else: ?>
                            <div class="unavailable-stat">
                                <h3><?php echo $translator->translate('Analytics graph comming soon') ?></h3>
                                <strong><?php echo $translator->translate('statistic') ?></strong>
                                <?php echo $translator->translate('not available on this sport') ?>
                            </div>
                        <?php endif; ?>
                    </div>
                     <div class="team-panel-footer">
                         <div class="col-xs-8 col-sm-6">
                            <?php echo $translator->translate('Next event') ?>
                             
                            <?php if(null != $nearestEvent): ?>
                             <strong><?php echo $nearestEvent->getCurrentDate()->format('d.m.Y')  ?> <?php echo $translator->translate('at') ?> <?php echo $nearestEvent->getTimeInterval(\Core\ServiceLayer::getService('localizer')->getTimeFormat()) ?></strong>
                            <?php else: ?>
                             <?php echo $translator->translate('No events in the next 30 days') ?>
                            <?php endif; ?>
                         </div>
                         

                         <div class="col-xs-4 col-sm-6">
                            
                         </div>
                        </div>
                </div>

