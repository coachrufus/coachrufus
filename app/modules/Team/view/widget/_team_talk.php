<div class="panel panel-default">
    <div class="panel-heading">
        <h3><i class="ico ico-panel-event ico-team-talk"></i><?php echo $translator->translate('Team talk') ?></h3>
    </div>
    <div class="panel-body">
        
     
        <div class="post row">
            <form action="<?php echo $router->link('create_team_wall_post') ?>" method="post" class="team-wall-post-form ajax-form">
                <?php echo $wallPostForm->renderInputHiddenTag('add_content', array('id' => 'post_add_content')) ?>
                <?php echo $wallPostForm->renderInputHiddenTag('team_id') ?>
                <?php echo $wallPostForm->renderInputHiddenTag('body', array('class' => 'form-control', 'id' => 'post_body', 'style' => 'width:100%;')) ?>
                <div class="form-group col-sm-11 post_send_text" data-error-bind="body">
                    <div  class="editable_area" id="record_body" contenteditable="true" placeholder="<?php echo $translator->translate('Write comment') ?>"></div>
                </div>
                
                <div class="col-sm-1 post_send_wrap">
                     <button type="submit" class="post_send"><?php echo $translator->translate('Post') ?></button>
                </div>

                <div class="form-group col-sm-12 text-center" id="post_add_content_wrap">
                    <i class="loader fa fa-spinner fa-spin fa-3x fa-fw"></i>
                </div>

                <div class="form-group col-sm-12 text-center" id="post_add_image_wrap"></div>

                <div class="upload-progress">
                    <span class="progress-num"></span>
                    <div class="progress progress-sm active">
                        <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                        </div>
                    </div>
                </div>
             
            </form>
        </div>
        
           <div id="wall_posts">
            <?php foreach ($wallPosts as $post): ?>
                <!-- Post -->
                <?php $layout->includePart(MODUL_DIR . '/Team/view/wall/_wall_line.php', array('post' => $post, 'isOwner' => $isOwner)) ?>
            <?php endforeach; ?>
        </div>
        


        <?php if ($post != null): ?>
            <a class="btn btn-primary load-wall_posts" data-start="<?php echo $post->getId() ?>" data-team="<?php echo $team->getId() ?>" href="<?php echo $router->link('tam_wall_load_posts') ?>" ><?php echo $translator->translate('Load More') ?></a>
        <?php endif; ?>  


    </div>
</div>

<?php $layout->includePart(MODUL_DIR . '/Team/view/wall/_remove_post_modal.php') ?>