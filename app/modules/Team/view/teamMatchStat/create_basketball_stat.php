<?php Core\Layout::getInstance()->startSlot('team_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/Team/view/team/_team_menu.php', array('team' => $team)) ?>
<?php Core\Layout::getInstance()->endSlot('team_menu') ?>

<section class="content-header">

    <a class="all-event-link" href="<?php echo $router->link('team_event_list', array('team_id' => $team->getId())) ?>"><i class="ico ico-event-back"></i><?php echo $translator->translate('All Events') ?></a>
    <?php
    $layout->includePart(GLOBAL_DIR . '/templates/parts/_breadcrumb.php', array(
        'crumbs' => array(
            'Teams' => $router->link('teams_overview'),
            $team->getName() => $router->link('team_overview', array('id' => $team->getId())),
            'Events' => $router->link('team_event_list', array('team_id' => $team->getId())),
            'New' => ''
)))
    ?>
</section>

<section class="content">




    <div class="row">
        <div class="col-md-12">
             <div class="event-detail">
<div class="panel panel-default rating-panel">
               <?php
                    $layout->includePart(MODUL_DIR . '/Team/view/events/_event_detail.php', array(
                        'event' => $event,
                        'team' => $team,
                        'existLineup' => $lineup,
                        'menuActive' => 'score',
                    ))
                    ?>



<div class="box-body">
    <form action="" method="post">  
<table class="table table-bordered" style="background-color: white; font-size:10px;">
    <thead>
        <tr>
            <td rowspan="2"><strong><?php echo $lineup->getFirstLineName() ?></strong><h3 class="total-team-points-a" ><?php echo $matchOverview->getFirstLinePoints() ?></h3></td>
            <td colspan="5"><?php echo $translator->translate('basketbal.score.points') ?></td>
        </tr>
        <tr>
            <td><?php echo $translator->translate('basketbal.score.1points') ?></td>
            <td><?php echo $translator->translate('basketbal.score.2points') ?></td>
            <td><?php echo $translator->translate('basketbal.score.3points') ?></td>
            <td><?php echo $translator->translate('basketbal.score.mom') ?></td>
            <td><?php echo $translator->translate('basketbal.score.total') ?></td>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($lineupPlayers['first_line'] as $lineupPlayer): ?>
            <tr class="player-points-row">
                <td><?php echo $lineupPlayer->getPlayerName() ?></td>
                
               
                <td><input type="text" data-points="1" class="form-control points" name="points[points1][first_line][<?php echo $lineupPlayer->getId() ?>]" value="<?php echo $matchOverview->getPlayerPoints($lineupPlayer->getTeamPlayerId(),'points1') ?>" /></td>
                <td><input type="text" data-points="2"  class="form-control points" name="points[points2][first_line][<?php echo $lineupPlayer->getId() ?>]" value="<?php echo $matchOverview->getPlayerPoints($lineupPlayer->getTeamPlayerId(),'points2') ?>" /></td>
                <td><input type="text" data-points="3"  class="form-control points" name="points[points3][first_line][<?php echo $lineupPlayer->getId() ?>]" value="<?php echo $matchOverview->getPlayerPoints($lineupPlayer->getTeamPlayerId(),'points3') ?>" /></td>
                <td><input type="radio" class="form-control set-points" name="mom" value="<?php echo $lineupPlayer->getId() ?>" <?php echo ($matchOverview->isPlayerManOfMatch($lineupPlayer->getTeamPlayerId())) ? 'checked="checked"' : '' ?> /></td>	
                <td class="total-player-points" data-team-points="total-team-points-a"><?php echo $matchOverview->getPlayerTotalPoints($lineupPlayer->getTeamPlayerId()) ?></td>	
            </tr>
    <?php endforeach; ?>
    </tbody>

    <thead>
        <tr>
            <td rowspan="6"><strong><?php echo $lineup->getSecondLineName() ?></strong><h3 class="total-team-points-b" ><?php echo $matchOverview->getSecondLinePoints() ?></h3></td>
        </tr>
       
    </thead>
    <tbody>
        <?php foreach ($lineupPlayers['second_line'] as $lineupPlayer): ?>
            <tr class="player-points-row">
                <td><?php echo $lineupPlayer->getPlayerName() ?></td>
                
               
                <td><input type="text" data-points="1"  class="form-control points" name="points[points1][second_line][<?php echo $lineupPlayer->getId() ?>]" value="<?php echo $matchOverview->getPlayerPoints($lineupPlayer->getTeamPlayerId(),'points1') ?>" /></td>
                <td><input type="text" data-points="2"  class="form-control points" name="points[points2][second_line][<?php echo $lineupPlayer->getId() ?>]" value="<?php echo $matchOverview->getPlayerPoints($lineupPlayer->getTeamPlayerId(),'points2') ?>" /></td>
                <td><input type="text" data-points="3"  class="form-control points" name="points[points3][second_line][<?php echo $lineupPlayer->getId() ?>]" value="<?php echo $matchOverview->getPlayerPoints($lineupPlayer->getTeamPlayerId(),'points3') ?>" /></td>
                <td><input type="radio" class="form-control set-points" name="mom" value="<?php echo $lineupPlayer->getId() ?>" <?php echo ($matchOverview->isPlayerManOfMatch($lineupPlayer->getTeamPlayerId())) ? 'checked="checked"' : '' ?> /></td>
                <td class="total-player-points" data-team-points="total-team-points-b"><?php echo $matchOverview->getPlayerTotalPoints($lineupPlayer->getTeamPlayerId()) ?></td>	
            </tr>
    <?php endforeach; ?>
    </tbody>
</table>
        
         <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-primary" type="submit"><?php echo $translator->translate('Save') ?></button>
                            </div>
                        </div>
    </form>




                
            </div>
        </div>
        </div>
    </div>
    </div>
</section>

<?php $layout->startSlot('javascript') ?>
<script type="text/javascript">

    
    $('.points').on('keyup',function(){
        var set_col = $(this).parents('.player-points-row');
        var set_col_sum = 0;
        set_col.find('.points').each(function(key,input){
            
            input_val  = parseInt($(input).val());
            points_index = $(input).attr('data-points');
            if(!isNaN(input_val))
            {
                set_col_sum += (input_val*points_index);
            }
        });


        if(!isNaN(set_col_sum))
        {
            total_player_points =  set_col.find('.total-player-points');
            total_player_points.html(set_col_sum);
            team_points_class = total_player_points.attr('data-team-points');
        }
        
         var team_points_sum = 0;
         $('.total-player-points[data-team-points="'+team_points_class+'"]').each(function(key,input){
            
            input_val  = parseInt($(input).html());
            if(!isNaN(input_val))
            {
                team_points_sum += input_val;
            }
            
            $('.'+team_points_class).html(team_points_sum);
           
        });

        
    });
    
</script>

<?php $layout->endSlot('javascript') ?>
