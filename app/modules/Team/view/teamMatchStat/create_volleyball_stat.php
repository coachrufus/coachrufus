<?php Core\Layout::getInstance()->startSlot('team_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/Team/view/team/_team_menu.php', array('team' => $team)) ?>
<?php Core\Layout::getInstance()->endSlot('team_menu') ?>

<section class="content-header">

    <a class="all-event-link" href="<?php echo $router->link('team_event_list', array('team_id' => $team->getId())) ?>"><i class="ico ico-event-back"></i><?php echo $translator->translate('All Events') ?></a>
    <?php
    $layout->includePart(GLOBAL_DIR . '/templates/parts/_breadcrumb.php', array(
        'crumbs' => array(
            'Teams' => $router->link('teams_overview'),
            $team->getName() => $router->link('team_overview', array('id' => $team->getId())),
            'Events' => $router->link('team_event_list', array('team_id' => $team->getId())),
            'New' => ''
)))
    ?>
</section>

<section class="content">




    <div class="row">
        <div class="col-md-12">
            <div class="event-detail">
                 <div class="panel panel-default rating-panel">
                      <?php
                    $layout->includePart(MODUL_DIR . '/Team/view/events/_event_detail.php', array(
                        'event' => $event,
                        'team' => $team,
                        'existLineup' => $lineup,
                        'menuActive' => 'score',
                    ))
                    ?>
               


                <div class="box-body">
                    <form action="" method="post">
                        <?php for ($i = 1; $i < 6; $i++): ?>
                            <div class="col-sm-4"> 
                                <div class="panel panel-default  set_stat_panel <?php echo ( count($setMatch->getSets()) >= $i) ? 'set_stat_panel_active' : '' ?> <?php echo ( count($setMatch->getSets()) == $i or count($setMatch->getSets()) == 0) ? 'set_stat_panel_last' : '' ?>" id="set_stat_panel_<?php echo $i ?>">
                                    <div class="panel-heading  text-center">

                                        <h3><?php echo $i ?>. Set</h3>
                                    </div>


                                    <div class="panel-body">
                                        <div class="row  text-center">

                                            <div class="form-group  col-sm-12">
                                                <label> <?php echo $translator->translate('voleyball.stats.duration') ?>:</label>
                                                <div class="bootstrap-timepicker">
                                                    <input name="set[<?php echo $i; ?>][duration]" type="text" value="<?php echo $setMatch->getSetDuration($i) ?>" class="form-control timepicker" />
                                                </div>
                                            </div>
                                        </div> 

                                        <div class="col-sm-6 set-col">
                                            <h3><?php echo $existLineup->getFirstLineName() ?></h3>
                                            <?php foreach ($lineupPlayers['first_line'] as $lineupPlayer): ?>
                                                <div class="row">
                                                    <div class="form-group  col-xs-8">
                                                        <label class="control-label"> <?php echo $lineupPlayer->getPlayerName() ?></label>
                                                    </div>
                                                    <div class="form-group  col-xs-4">
                                                        <input type="text" class="form-control set-points" name="set[<?php echo $i; ?>][points][first_line][<?php echo $lineupPlayer->getId() ?>]" value="<?php echo $setMatch->getPlayerSetPoints($lineupPlayer->getId(), $i) ?>" />
                                                    </div>
                                                </div>
                                            <?php endforeach; ?>
                                                <div class="row">
                                                    <div class="form-group  col-xs-8">
                                                        <label class="control-label"> Total</label>
                                                    </div>
                                                    <div class="form-group  col-xs-4">
                                                        <input type="text" class="form-control  total-set-points" name="set[<?php echo $i; ?>][points][first_line][0]" value="<?php echo $setMatch->getTotalSetPoints('first_line', $i) ?>"/>
                                                    </div>
                                                </div>
                                        </div>

                                        <div class="col-sm-6 set-col">
                                            <h3><?php echo $existLineup->getSecondLineName() ?></h3>
                                            <?php foreach ($lineupPlayers['second_line'] as $lineupPlayer): ?>
                                                <div class="row">
                                                    <div class="form-group  col-xs-8">
                                                        <label class="control-label"> <?php echo $lineupPlayer->getPlayerName() ?></label>
                                                    </div>
                                                    <div class="form-group  col-xs-4">
                                                        <input type="text" class="form-control set-points" name="set[<?php echo $i; ?>][points][second_line][<?php echo $lineupPlayer->getId() ?>]" value="<?php echo $setMatch->getPlayerSetPoints($lineupPlayer->getId(), $i) ?>" />
                                                    </div>
                                                </div>
                                            <?php endforeach; ?>
                                             <div class="row">
                                                    <div class="form-group  col-xs-8">
                                                        <label class="control-label"> Total</label>
                                                    </div>
                                                    <div class="form-group  col-xs-4">
                                                        <input type="text" class="form-control total-set-points" name="set[<?php echo $i; ?>][points][second_line][0]" value="<?php echo $setMatch->getTotalSetPoints('second_line', $i) ?>" />
                                                    </div>
                                                </div>
                                        </div>
                                        
                                        
                                        
                                        
                                        
                                        <?php if ($i < 5): ?>
                                            <div class="col-sm-12 text-center">
                                                <button type="button" class="btn btn-success add_set_stat_triger" data-target="set_stat_panel_<?php echo $i + 1 ?>"><i class="fa fa-plus"></i> <?php echo $translator->translate('voleyball.stats.addNewSet') ?></button>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        <?php endfor; ?>

                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-primary" type="submit"><?php echo $translator->translate('Save') ?></button>
                            </div>
                        </div>
                    </form>
                </div>
                 </div>
            </div>
        </div>
    </div>
</section>




<?php $layout->addStylesheet('plugins/timepicker/bootstrap-timepicker.min.css') ?>
<?php $layout->addJavascript('plugins/timepicker/bootstrap-timepicker.js') ?>

<?php $layout->startSlot('javascript') ?>

<script type="text/javascript">
    $(".timepicker").timepicker({showInputs: false, showMeridian: false, minuteStep: 5});
    $('.add_set_stat_triger').on('click', function () {
        $('.set_stat_panel').removeClass('set_stat_panel_last');
        var target_id = $(this).attr('data-target');
        $('#' + target_id).addClass('set_stat_panel_last');
        $('#' + target_id).fadeIn();
    });
    
    $('.set-points').on('keyup',function(){
        var set_col = $(this).parents('.set-col');
        var set_col_sum = 0;
        set_col.find('.set-points').each(function(key,input){
            
            input_val  = parseInt($(input).val());
            if(!isNaN(input_val))
            {
                set_col_sum += input_val;
            }
        });

        if(!isNaN(set_col_sum))
        {
            set_col.find('.total-set-points').val(set_col_sum);
        }

        
    });
    
</script>

<?php $layout->endSlot('javascript') ?>
