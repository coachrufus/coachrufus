<?php if ($request->hasFlashMessage('floorball_activation_success')): ?>
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <?php echo $request->getFlashMessage('floorball_activation_success') ?>
    </div>
<?php endif; ?>

<?php Core\Layout::getInstance()->startSlot('team_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/Team/view/team/_team_menu.php', array('team' => $team)) ?>
<?php Core\Layout::getInstance()->endSlot('team_menu') ?>

<img src="https://img.joj.sk/r1010x570/ec77184057f92985623925cfe184969c.jpg" data-original="https://img.joj.sk/r1010x570/ec77184057f92985623925cfe184969c.jpg" alt="nadacia tv joj - floorball challenge 2017" class="img-responsive" style="display: block;">
