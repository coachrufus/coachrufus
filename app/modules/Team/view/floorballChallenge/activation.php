


<section class="content-header">

</section>

<section class="content">

    Vyberte tím ktorý chcete aktivovať

    <form action="" method="post" >
        <div class="form-group">
            <?php foreach ($teams as $team): ?>
                <div class="radio">
                    <label>
                        <input type="checkbox" name="team[]"  value="<?php echo $team->getId() ?>">
                        <?php echo $team->getName() ?>
                    </label>
                </div>
            <?php endforeach; ?>


            <button class="btn btn-primary" type="submit"><?php echo $translator->translate('Submit') ?></button>

    </form>


</section>