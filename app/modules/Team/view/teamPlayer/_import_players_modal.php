<div class="modal fade" id="import-players-modal" tabindex="-1" role="dialog" aria-labelledby="<?php echo $translator->translate('Import players') ?>">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $translator->translate('Import players') ?></h4>
      </div>
      <div class="modal-body">
          
         
          
          
        <form role="form" id="add_season_form" action="" >
       
         

        <h4 class="regular margin-bottom-small">1. <?php echo $translator->translate('Download template') ?> &nbsp;&nbsp;<a href="<?php echo $router->link('team_players_template') ?>" class="btn  btn-success margin-bottom-small" target="_blank" download="">
            <i class="fa fa-cloud-download blue"></i> <?php echo $translator->translate('Download') ?>
          </a></h4>

        <h4 class="regular margin-bottom-small">2. <?php echo $translator->translate(' Fill it in with your members details') ?></h4>
        
        <h4 class="regular margin-bottom-small">3. <?php echo $translator->translate('Upload file') ?>
        
          <div class="upload-wrap" style="display:inline-block;">
                            <span class="btn btn-success fileinput-button">
                               <i class="glyphicon glyphicon-plus"></i>
                               <span><?php echo $translator->translate('Select files from your computer') ?></span>
                                 <input   id="members_import_file" type="file" name="members_import_file" data-url="<?php echo $router->link('team_players_import',array('team_id' => $team->getId())) ?>" />
                           </span>
                        </div>
        
        </h4>
         
      
        

        
      
                       

                        <div class="upload-progress">
                            <span class="progress-num"></span>
                           <div class="progress progress-sm active">
                            <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                            </div>
                          </div>
                         </div>
        
              
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $translator->translate('Close') ?></button>
      </div>
    </div>
  </div>
</div>

