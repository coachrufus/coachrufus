 <!-- Modal -->
<div class="modal fade" id="unregistred-invitation-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?php echo $translator->translate('Send team invitation') ?></h4>
            </div>
            <div class="modal-body">
                 <form  id="unregistred-invitation-modal-form" action="" method="post">
                  


                         <div class="form-group">
                            <label class="control-label"><?php echo $translator->translate('Email') ?></label>
                            <input type="text" name="email" class="form-control" />
                        </div>

                        
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary add-season-submit">Submit</button>
                           </div>
                        

               
                </form>
               
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $translator->translate('Cancel') ?></button>
            </div>
        </div>
    </div>
</div>