<div class="modal fade" id="edit-player-modal" tabindex="-1" role="dialog" aria-labelledby="<?php echo $translator->translate('Edit player') ?>">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?php echo $translator->translate('Edit player') ?></h4>
            </div>

            <div class="modal-body">
                <form action="<?php echo $router->link('edit_team_player') ?>" class="form-bordered" method="post" id="edit-player-modal-form">
                    <input type="hidden" name="player_id" value="" id="edit_modal_form_player_id" />
                  

                    <div class="row">

                        <div class="form-group col-sm-6" data-error-bind="first_name">
                            <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("first_name")) ?></label>
                           <?php echo $form->renderInputTag("first_name", array( 'class' => 'form-control')) ?>
                        </div>

                        <div class="form-group col-sm-6" data-error-bind="last_name">
                            <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("last_name")) ?></label>
                            <?php echo $form->renderInputTag("last_name", array( 'class' => 'form-control')) ?>
                        </div>
                    </div>
                    <div class="row">
                         <div class="form-group col-sm-6" data-error-bind="email">
                            <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("email")) ?></label>
                            <?php echo $form->renderInputTag("email", array( 'class' => 'form-control')) ?>
                        </div>
                         <div class="form-group col-sm-6" data-error-bind="team_role">
                            <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("team_role")) ?></label>
                           <?php echo $form->renderSelectTag("team_role", array( 'class' => 'form-control')) ?>
                        </div>
                    </div>
                     <div class="row">
                         <div class="form-group col-sm-3" data-error-bind="player_number">
                            <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("player_number")) ?></label>
                           <?php echo $form->renderInputTag("player_number", array( 'class' => 'form-control')) ?>
                        </div>
                        
                         <div class="form-group col-sm-4" data-error-bind="level">
                            <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("level")) ?></label>
                           <?php echo $form->renderSelectTag("level", array( 'class' => 'form-control')) ?>
                        </div>
                         <div class="form-group col-sm-5" data-error-bind="status">
                            <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("status")) ?></label>
                           <?php echo $form->renderSelectTag("status", array( 'class' => 'form-control')) ?>
                            <span class="player_status" id="edit_modal_form_player_status_text"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <button type="submit" data-base-title="<?php echo $translator->translate('Save') ?>" data-alt-title="<?php echo $translator->translate('Save and send invitation') ?>" class="btn btn-primary"><?php echo $translator->translate('Save') ?></button>
                        </div>
                    </div>

                </form>


            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $translator->translate('Cancel') ?></button>
            </div>
        </div>
    </div>
</div>