<?php Core\Layout::getInstance()->startSlot('team_menu') ?>  
   <?php $layout->includePart(MODUL_DIR.'/Team/view/team/_team_menu.php',array('team' => $team) ) ?>
<?php Core\Layout::getInstance()->endSlot('team_menu') ?>

<?php if ($request->hasFlashMessage('team_player_save_success')): ?>
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <?php echo $request->getFlashMessage('team_player_save_success') ?>
    </div>
<?php endif; ?>

<?php if ($request->hasFlashMessage('team_create_player_success')): ?>
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <?php echo $request->getFlashMessage('team_create_player_success') ?>
    </div>
<?php endif; ?>
<?php if ($request->hasFlashMessage('team_create_player_fail')): ?>
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <?php echo $request->getFlashMessage('team_create_player_fail') ?>
    </div>
<?php endif; ?>

<?php if ($request->hasFlashMessage('team_create_create_success')): ?>
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <?php echo $request->getFlashMessage('team_create_create_success') ?>
    </div>
<?php endif; ?>

<?php if ($request->hasFlashMessage('team_create_skliga_alert')): ?>
    <div class="alert alert-danger text-center">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <?php echo $request->getFlashMessage('team_create_skliga_alert') ?>
    </div>
<?php endif; ?>


<?php Core\Layout::getInstance()->startSlot('crumb') ?>
 <?php $layout->includePart(GLOBAL_DIR.'/templates/parts/_breadcrumb.php',array(
        'crumbs' => array(
            'Teams' => $router->link('teams_overview'),
            $team->getName() => $router->link('team_overview',array('id' =>$team->getId() )),
            'Members' =>  $router->link('team_public_players_list', array('team_id' => $team->getId())),
            'Edit' => ''
            ))) ?>
<?php Core\Layout::getInstance()->endSlot('crumb') ?>


<section class="content-header">
    <a class="all-event-link" href="<?php echo $router->link('team_overview',array('id' =>$team->getId() )) ?>"><i class="ico ico-event-back"></i><?php echo $translator->translate('Back to team') ?></a>
     <?php echo $layout->renderControllerAction('CR\Gamifications\GamificationModule:Progress:progressBar')->getContent() ?>
      
    
</section>
<section class="content">

<div class="panel">
<?php if($teamLimitOverflow): ?>
        <div class="panel-body">
    <div class="alert alert-danger">
        <?php echo $translator->translate('You have reached the  maximum number of players for current package') ?>
    </div>
    </div>
<?php else: ?>
<div id="template_row_container" style="display:none;">
    <?php  $i = 0; ?>
    <div class="row player_row template_row">
        <input type="hidden" name="player[<?php echo $i ?>][player_id]" value="" id="player_id<?php echo $i ?>" />

       <div class="form-group col-sm-2">
           <label class="control-label"><?php echo $translator->translate($addForm->getFieldLabel("player_number")) ?> *</label>
           <?php echo $addForm->renderInputTag("player_number", array('name' => 'player['.$i.'][player_number]', 'class' => 'form-control player_player_number_row','data-validate-required' => 'required','data-index' => 'player_number'.$i, 'id' => 'player_player_number'.$i)) ?>
       </div>
       <div class="form-group col-sm-3">
           <label class="control-label"><?php echo $translator->translate($addForm->getFieldLabel("first_name")) ?> *</label>
           <?php echo $addForm->renderInputTag("first_name", array('name' => 'player['.$i.'][first_name]', 'class' => 'form-control player_first_name_row','data-validate-required' => 'required','data-index' => 'first_name'.$i, 'id' => 'player_first_name'.$i)) ?>
       </div>
       <div class="form-group col-sm-3">
           <label class="control-label"><?php echo $translator->translate($addForm->getFieldLabel("last_name")) ?></label>
           <?php echo $addForm->renderInputTag("last_name", array('name' => 'player['.$i.'][last_name]', 'class' => 'form-control player_last_name_row','data-index' => 'last_name'.$i,'id' => 'player_last_name'.$i)) ?>
       </div>
       <div class="form-group col-sm-4 autocomplete_container">
           <label class="control-label"><?php echo $translator->translate($addForm->getFieldLabel("email")) ?></label>
           <?php echo $addForm->renderInputTag("email", array('name' => 'player['.$i.'][email]', 'class' => 'form-control player_email_row', 'autocomplete' => 'off','data-index' => 'email'.$i,'data-row' => $i,'id' => 'player_email'.$i)) ?>
           <div class="autocomplete_wrap">
               <ul class="autocomplete_list">

               </ul>
           </div>
       </div>
        <div class=" col-sm-1 btn_col">
            <a  class="rm_player_btn" href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo $translator->translate('TEAM_PLAYERS_REMOVE_PLAYER') ?>"><i class="ico ico-cross"></i></a>
        </div>
   </div>
</div>
    
    
    
    
<form action="<?php echo $router->link('create_team_player') ?>" id="add_player_form" method="post">
    <div class="panel-body">
            <input type="hidden" name="team_id" id="player_team_id" value="<?php echo $team->getId() ?>" />
            <input type="hidden" name="type" value="<?php echo $type ?>"/>
            <div class="row">
            <div class="col-sm-6 form-group">
              
                    <label class="control-label"><?php echo $translator->translate('Share team invite link') ?></label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <a class="popup-link" href="<?php echo WEB_DOMAIN. $router->link('team_share_link',array('hash' =>  $team->getShareLinkHash())) ?>"><i class="ico ico-share-link"></i></a>
                        </div>
                        <input id="team_share_link" type="text" value="<?php echo WEB_DOMAIN. $router->link('team_share_link',array('lang' => $user->getDefaultLang(),'hash' =>  $team->getShareLinkHash())) ?>" class="form-control" />   
                         <div class="input-group-addon">
                            <a href="mailto:?subject=<?php echo $translator->translate('invitationlink.subject') ?> <?php echo $team->getName() ?>&body=<?php echo $translator->translate('invitationlink.body') ?> %0D%0A<?php echo WEB_DOMAIN. $router->link('team_share_link',array('hash' =>  $team->getShareLinkHash())) ?>" class=""><?php echo $translator->translate('invitationlink.send') ?></a>
                        </div>
                    </div>
                     <?php echo $translator->translate('team.players.edit.invitationlink.text') ?>
            </div>
            <div class="col-sm-6 text-right ">
               
                <a class="btn btn-clear btn-clear-green" id="import_players" href=""><?php echo $translator->translate('Import players') ?></a>
            </div>
            
            </div>

            
            <div id="add_player_wrap">
                <?php for($i=1;$i<2;$i++): ?>
                <div class="row player_row">
                     <input type="hidden" name="player[<?php echo $i ?>][player_id]" value="" id="player_id<?php echo $i ?>" />
                     
                      <div class="form-group col-sm-2">
                        <label class="control-label"><?php echo $translator->translate($addForm->getFieldLabel("player_number")) ?></label>
                        <?php echo $addForm->renderInputTag("player_number", array('name' => 'player['.$i.'][player_number]', 'class' => 'form-control player_player_number_row','data-index' => 'player_number'.$i, 'id' => 'player_player_number'.$i)) ?>
                    </div>

                    <div class="form-group col-sm-3 col-xs-6">
                        <label class="control-label"><?php echo $translator->translate($addForm->getFieldLabel("first_name")) ?> (<?php echo $translator->translate('team.players.edit.isRequired') ?>)</label>
                        <?php echo $addForm->renderInputTag("first_name", array('name' => 'player['.$i.'][first_name]', 'class' => 'form-control player_first_name_row','data-validate-required' => 'required','data-index' => 'first_name'.$i, 'id' => 'player_first_name'.$i)) ?>
                    </div>
                    <div class="form-group col-sm-3 col-xs-6">
                        <label class="control-label"><?php echo $translator->translate($addForm->getFieldLabel("last_name")) ?></label>
                        <?php echo $addForm->renderInputTag("last_name", array('name' => 'player['.$i.'][last_name]', 'class' => 'form-control player_last_name_row','data-index' => 'last_name'.$i,'id' => 'player_last_name'.$i)) ?>
                    </div>
                    <div class="form-group col-sm-4 col-xs-12">
                        <label class="control-label"><?php echo $translator->translate($addForm->getFieldLabel("email")) ?></label>
                        <?php echo $addForm->renderInputTag("email", array('name' => 'player['.$i.'][email]', 'class' => 'form-control  player_email_row', 'autocomplete' => 'off','data-index' => 'email'.$i,'data-row' => $i,'id' => 'player_email'.$i)) ?>
                        <div class="autocomplete_wrap">
                            <ul class="autocomplete_list">

                            </ul>
                        </div>
                    </div>
                     <div class=" col-sm-1 btn_col"></div>
                </div>
                <?php endfor; ?>
               

            </div>
            
             <div class="row" id="action_row">
         
                <div class="form-group col-sm-12 text-center">
                    <div class="gdpr-teamPlayers">
                     <?php echo $translator->translate('gdpr.teamPlayers.text') ?>
                    </div>
                    
                    <button type="submit" class="btn btn-primary" href=""><?php echo $translator->translate('TEAM_PLAYERS_ADD_CONTACT   ') ?> <i class="fa fa-chevron-right    "></i></button>
                </div>
         </div>

    </div>
   
     </form>
<?php endif; ?>
</div>

<div class="row players_list-panel-head">
    <div class="col-sm-2">
        <h3><?php echo $translator->translate('Players') ?></h3>
    </div>

    <div class="col-sm-8 players_list_filter">
        <div class="btn-group">
        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <?php echo $translator->translate('Filter') ?> <i class="ico ico-dropdown pull-right"></i>
        </button>
        <ul class="dropdown-menu">
          <li><input type="checkbox" checked="checked" name="players_filter" value="confirmed" /> <?php echo $translator->translate('Confirmed Players') ?></li>
          <li><input type="checkbox"  checked="checked"  name="players_filter" value="waiting-to-confirm" /> <?php echo $translator->translate('Waiting to confirm Players') ?></li>
          <li><input type="checkbox"  checked="checked" name="players_filter" value="unactive" /> <?php echo $translator->translate('Unactive Players') ?></li>
          <li><input type="checkbox"  checked="checked" name="players_filter" value="decline" /> <?php echo $translator->translate('Decline Players') ?></li>
          <li><input type="checkbox"  checked="checked" name="players_filter" value="unknown" /> <?php echo $translator->translate('Other Players') ?></li>
        </ul>
      </div>
    </div>
</div>
<div class="panel">
   

    <div class="panel-body">

<form action="" method="post" id="bulk_edit_form">
     <?php echo $validator->showError("admins_count") ?>


    <?php if ($request->hasFlashMessage('team_player_invitation_send')): ?>
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <?php echo $request->getFlashMessage('team_player_invitation_send') ?>
    </div>
    <?php endif; ?>
    <?php if ($request->hasFlashMessage('unactivate_player_success')): ?>
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <?php echo $request->getFlashMessage('unactivate_player_success') ?>
    </div>
    <?php endif; ?>

    <table class="table resp-table player-list-table">
        <thead>
            <tr>
                <th class="number-cell">
                    #
                </th>
                <th>
                    <span class="sort <?php echo $sortLinks['name']['down']['status'] ?>"><a href="<?php echo $sortLinks['name']['down']['link'] ?>"><i class="ico ico-arr-dwn-black"></i></a></span>
                    <span class="sort <?php echo $sortLinks['name']['up']['status'] ?>"><a href="<?php echo $sortLinks['name']['up']['link'] ?>"><i class="ico ico-arr-up-black"></i></a></span>
                    <?php echo $translator->translate('First name') ?>,  <?php echo $translator->translate('Last name') ?>,  <?php echo $translator->translate('Email') ?>
                </th>
                <th>
                    <span class="sort <?php echo $sortLinks['role']['down']['status'] ?>"><a href="<?php echo $sortLinks['role']['down']['link'] ?>"><i class="ico ico-arr-dwn-black"></i></a></span>
                    <span class="sort <?php echo $sortLinks['role']['up']['status'] ?>"><a href="<?php echo $sortLinks['role']['up']['link'] ?>"><i class="ico ico-arr-up-black"></i></a></span>
                    <?php echo $translator->translate('Role') ?>
                </th>
                <th>
                    <span class="sort <?php echo $sortLinks['level']['down']['status'] ?>"><a href="<?php echo $sortLinks['level']['down']['link'] ?>"><i class="ico ico-arr-dwn-black"></i></a></span>
                    <span class="sort <?php echo $sortLinks['level']['up']['status'] ?>"><a href="<?php echo $sortLinks['level']['up']['link'] ?>"><i class="ico ico-arr-up-black"></i></a></span>
                    <?php echo $translator->translate('Level') ?>
                </th>
                <th>
                    <span class="sort <?php echo $sortLinks['status']['down']['status'] ?>"><a href="<?php echo $sortLinks['status']['down']['link'] ?>"><i class="ico ico-arr-dwn-black"></i></a></span>
                    <span class="sort <?php echo $sortLinks['status']['up']['status'] ?>"><a href="<?php echo $sortLinks['status']['up']['link'] ?>"><i class="ico ico-arr-up-black"></i></a></span>
                    <?php echo $translator->translate('Player list Status') ?>
                </th>
                 <th>
                    <?php echo $translator->translate('Player list Invitation') ?><i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo $translator->translate('Invitation tooltip') ?>"></i>
                </th>
                <th>
                    
                </th>
            </tr>
        </thead>
        <tbody>
            
            <?php if($sorted): ?>
                 <?php foreach ($list as $player): ?>
                    <?php $layout->includePart(MODUL_DIR . '/Team/view/teamPlayer/_edit_player_row.php',array('player' => $player,'editForm' =>$editForm,'team' => $team,'validator' => $validator, 'rowClass' => $player->getStatus())) ?> 
                <?php endforeach; ?>
            <?php else: ?>
                <?php foreach ($list['confirmed'] as $player): ?>
                    <?php $layout->includePart(MODUL_DIR . '/Team/view/teamPlayer/_edit_player_row.php',array('player' => $player,'editForm' =>$editForm,'team' => $team,'validator' => $validator, 'rowClass' => 'confirmed')) ?> 
                <?php endforeach; ?>
                <?php foreach ($list['unknown'] as $player): ?>
                    <?php $layout->includePart(MODUL_DIR . '/Team/view/teamPlayer/_edit_player_row.php',array('player' => $player,'editForm' =>$editForm,'team' => $team,'validator' => $validator, 'rowClass' => 'unknown')) ?> 
                <?php endforeach; ?>
                <?php foreach ($list['waiting-to-confirm'] as $player): ?>
                    <?php $layout->includePart(MODUL_DIR . '/Team/view/teamPlayer/_edit_player_row.php',array('player' => $player,'editForm' =>$editForm,'team' => $team,'validator' => $validator,'rowClass' => 'waiting-to-confirm')) ?> 
                <?php endforeach; ?>
                <?php foreach ($list['unconfirmed'] as $player): ?>
                    <?php $layout->includePart(MODUL_DIR . '/Team/view/teamPlayer/_edit_player_row.php',array('player' => $player,'editForm' =>$editForm,'team' => $team,'validator' => $validator,'rowClass' => 'unconfirmed')) ?> 
                <?php endforeach; ?>

                <?php foreach ($list['unactive'] as $player): ?>
                    <?php $layout->includePart(MODUL_DIR . '/Team/view/teamPlayer/_edit_player_row.php',array('player' => $player,'editForm' =>$editForm,'team' => $team,'validator' => $validator,'rowClass' => 'unactive')) ?> 
                <?php endforeach; ?>
                <?php foreach ($list['decline'] as $player): ?>
                    <?php $layout->includePart(MODUL_DIR . '/Team/view/teamPlayer/_edit_player_row.php',array('player' => $player,'editForm' =>$editForm,'team' => $team,'validator' => $validator,'rowClass' => 'decline')) ?> 
                <?php endforeach; ?>
           <?php endif; ?>

        </tbody>
    </table>

   
</form>
</div>
</div>
    
    <div class="bulk_edit_form_loader">
        <img src="/img/load.gif" />
        <?php echo $translator->translate('bulk_edit_form.loader.text') ?>
        
    </div>
</section>




<?php $layout->includePart(MODUL_DIR . '/Team/view/teamPlayer/_remove_player_modal.php') ?>
<?php $layout->includePart(MODUL_DIR . '/Team/view/teamPlayer/_unactivate_player_modal.php') ?>
<?php $layout->includePart(MODUL_DIR . '/Team/view/teamPlayer/_import_players_modal.php',array('team' => $team)) ?>
<?php $layout->includePart(MODUL_DIR . '/Team/view/teamPlayer/_unregistred_invitation.php') ?>
<?php $layout->includePart(MODUL_DIR . '/Team/view/teamPlayer/_edit_player_modal.php',array('form' => $modalForm)) ?>
<?php if ($manageRole): ?>

<?php $layout->addJavascript('plugins/fileupload/jquery.ui.widget.js') ?>
<?php $layout->addJavascript('plugins/fileupload/jquery.iframe-transport.js') ?>
<?php $layout->addJavascript('plugins/fileupload/jquery.fileupload.js') ?>
<?php $layout->addJavascript('js/rufus/team/TeamPlayersManager.js') ?>




<?php $layout->startSlot('javascript') ?>

<?php echo Core\ServiceLayer::getService('AchievementAlertManager')->showPlaceAchievementAlert('team_players_list','team_player_create',Core\ServiceLayer::getService('security')->getIdentity()) ?>

<?php echo Core\ServiceLayer::getService('AchievementAlertManager')->showAndFinishPlaceAchievementAlert('team_players_list','team_player_create_confirm',Core\ServiceLayer::getService('security')->getIdentity()) ?>


<?php if ($request->hasFlashMessage('too_many_players_free')): ?>
    <?php $layout->includePart(MODUL_DIR . '/Team/view/teamPlayer/_too_many_players_free.php',array('text' => $request->getFlashMessage('too_many_players_free') )) ?>
<?php endif; ?>

<?php if ($request->hasFlashMessage('too_many_admins_free')): ?>
    <?php $layout->includePart(MODUL_DIR . '/Team/view/teamPlayer/_too_many_admins_free.php',array('text' => $request->getFlashMessage('too_many_admins_free') )) ?>
<?php endif; ?>




    <script type="text/javascript">

        TeamPlayersManager = new TeamPlayersManager();
        TeamPlayersManager.bindTriggers();
        
        $('#bulk_edit_form select').on('change',function(){
            $('.bulk_edit_form_loader').show();
            $('#bulk_edit_form').submit();
        });
        
         $('#bulk_edit_form input').on('blur',function(){
            $('.bulk_edit_form_loader').show();
            $('#bulk_edit_form').submit();
        });
        
       $('.user-find-autocomplete').each(function(key, val){
            //autocomplete = new UserSearchAutocomplete($(val));
            //autocomplete.initEmailSearch();
       });


       $('.copy-share-link').on('click',function(){
          
          $('#team_share_link').select();
          var copyTextarea = document.querySelector('#team_share_link');
          copyTextarea.select();
          document.execCommand('copy');
       });
       
      
      //scrool to player
      var hash = window.location.hash;
      if(hash != '')
      {
           $('html, body').animate({
                    scrollTop: $(hash).offset().top
                }, 500);
      }


    </script>
    <?php $layout->endSlot('javascript') ?>
<?php endif; ?>