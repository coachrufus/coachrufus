<div id="full-window">
    <div class="full-window-cnt">
    <div class="row">
        <div class="col-sm-12">
            <p><?php echo $translator->translate('gdpr.teamPlayers.text') ?></p>
            <form action="" method="post" >
                <input type="hidden" name="f_action" value="gdpr_team_players_agreement" />
                <input type="hidden" name="team_players_agreement" value="1" />
                <button class="btn btn-primary agree-clear" type="submit"><?php echo $translator->translate('gdpr.teamPlayers.agree') ?></button>
                <a href="<?php echo $router->link('team_overview',array('id' => $team->getId())) ?>" class="btn btn-primary agree-clear"><?php echo $translator->translate('gdpr.teamPlayers.disagree') ?></a>
            </form>
        </div>
    </div>
    </div>
</div>