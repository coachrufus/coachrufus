
<?php Core\Layout::getInstance()->startSlot('stylesheet') ?>  
<link rel="stylesheet" href="/dev/plugins/starbox/css/jstarbox.css">
<?php Core\Layout::getInstance()->endSlot('stylesheet') ?>

<?php Core\Layout::getInstance()->startSlot('team_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/Team/view/team/_team_menu.php', array('team' => $team)) ?>
<?php Core\Layout::getInstance()->endSlot('team_menu') ?>

<?php if ($request->hasFlashMessage('team_create_player_success')): ?>
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <?php echo $request->getFlashMessage('team_create_player_success') ?>
    </div>
<?php endif; ?>



<section class="content-header">
        <a class="all-event-link" href="<?php echo $router->link('team_overview',array('id' =>$team->getId() )) ?>"><i class="ico ico-event-back"></i><?php echo $translator->translate('Back to team') ?></a>
    
     <?php $layout->includePart(GLOBAL_DIR.'/templates/parts/_breadcrumb.php',array(
        'crumbs' => array(
            'Teams' => $router->link('teams_overview'),
            $team->getName() => $router->link('team_overview',array('id' =>$team->getId() )),
            'Members' => ''
            ))) ?>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-8">
             <div class="panel panel-default">
                 <div class="panel-heading">
                      <h3><i class="ico ico-dress-b"></i> <?php echo $translator->translate('Team members') ?></h3>
                      <div class="panel-toolbar">
                           <?php if (\Core\ServiceLayer::getService('security')->getIdentity()->hasTeamPermission('managePlayers', $team)): ?>
                          <a class="color-link tool-item tool-item-link" href="<?php echo $router->link('team_players_list', array('team_id' => $team->getId())) ?>"><i class="ico ico-plus-green"></i> <?php echo $translator->translate('Add member') ?></a>
                          <a class="tool-item" href="<?php echo $router->link('team_players_list', array('team_id' => $team->getId())) ?>"><i class="ico ico-edit"></i></a>  
                        <?php endif; ?>
                      </div>

                 </div>
                  <div class="panel-body  panel-full-body">
                <?php foreach ($list as $player): ?>
                          <div class="box box-widget collapsed-box team-players-list">
                              <div class="box-header with-border">
                                  <div class="user-block row <?php echo $player->getStatus() ?>">
                                      <span class="col-sm-12">
                                          
                                          <?php if('confirmed' == $player->getStatus()): ?>
                                             <span data-toggle="tooltip" data-placement="top" title="<?php echo $translator->translate('Confirmed') ?>" class="pull-right round_btn bg_green"><i class="ico ico-check"></i></span>
                                           <?php endif; ?>
                                          <?php if('unknown' == $player->getStatus() or 'waiting-to-confirm' == $player->getStatus() ): ?>
                                             <span data-toggle="tooltip" data-placement="top" title="<?php echo $translator->translate('Unknown') ?>" class="pull-right round_btn bg_gray"><i class="ico ico-dots-h"></i></span>
                                           <?php endif; ?>
                                             
                                           <?php if('decline' == $player->getStatus() ): ?>
                                             <span data-toggle="tooltip" data-placement="top" title="<?php echo $translator->translate('Decline') ?>" class="pull-right round_btn bg_red"><i class="ico ico-cross"></i></span>
                                           <?php endif; ?>
                                             
                                           <?php if('unactive' == $player->getStatus() ): ?>
                                             <span data-toggle="tooltip" data-placement="top" title="<?php echo $translator->translate('Unactive') ?>" class="pull-right round_btn bg_orange"><i class="ico ico-minus-w"></i></span>
                                           <?php endif; ?>
                                          
                                          

                                          <div class="img_round_wrap member_img_round" style="background-image: url('<?php echo $player->getMidPhoto() ?>');">

                                          </div>      
                                          <div class="user-info">
                                              
                                            <span class="username">
                                                <?php echo  $player->getPlayerNumber()  ?>
                                                <?php echo $player->getLastName() ?> <?php echo $player->getFirstName() ?>
                                            </span>  
                                              
                                            <span class="team-role"><?php echo $player->getTeamRoleName()  ?> <?php echo $player->getLevelName() ?> </span>
                                            
                                            <div class="user-sports">
                                                <?php foreach ($player->getPlayerSports() as $playerSport): ?>
                                                   <span class="label label-info"><?php echo $playerSport->getSport()->getName() ?></span>
                                               <?php endforeach; ?>
                                            </div>
                                            
                                          </div>
    
                                          <div data-toggle="tooltip" data-placement="top" title="<?php echo $translator->translate('STARBOX_DESC_TEXT') ?>" class="starbox-fake ghosting player-rating" data-start-value="<?php echo $player->getRating() ?>"> </div>
                                     
                                        
                                      </span>      
                                       
                                  </div><!-- /.user-block -->

                              </div><!-- /.box-header -->
                             


                          </div>

                      <?php endforeach; ?>
                 </div>
             </div>
           
            
            
            
            
            
          
        </div>
        <div class="col-md-4">
           
              <?php $layout->includePart(MODUL_DIR . '/Event/view/widget/_small_calendar.php') ?>
            
        </div>
    </div>
</section>


<?php $layout->addJavascript('js/SmallCalendar.js') ?>
<?php  $layout->startSlot('javascript') ?>
 <script src="/dev/plugins/starbox/jstarbox.js"></script>
 
 <script type="text/javascript">

     
     
$('.starbox-fake').each(function(index,val){
    $(val).starbox({
        average: $(this).attr('data-start-value'),
        changeable: false
    })
});

small_calendar = new SmallCalendar({
        'dataUrl': '<?php echo $router->link('rest_event_get_month_grid') ?>',
        'localityManager': new LocalityManager()
    });
    small_calendar.initLoad();


 

</script>

<?php  $layout->endSlot('javascript') ?>
