<tr id="player_row_<?php echo $player->getId() ?>" class="edit-player-row <?php echo $rowClass ?> static">
    <td>
        <a id="sp_<?php echo $player->getId()   ?>" name="player_<?php echo $player->getId()   ?>"></a>
        <input type="hidden" name="player[<?php echo $player->getId() ?>][id]" value="<?php echo $player->getId() ?>" />
        <?php echo $editForm->renderInputTag("player_number", array('id' => 'player_number' . $player->getId(), 'name' => 'player[' . $player->getId() . '][player_number]', 'class' => 'form-control', 'value' => $player->getPlayerNumber())) ?>

        <?php echo $validator->showError("player_number_unique_".$player->getId()) ?>
       
    </td>
    <td>
         <div id="inline-edit-<?php echo $player->getId() ?>" class="inline-edit-wrap row">
            
             <div class="col-sm-12 col-lg-3">
                  <?php echo $editForm->renderInputTag("first_name", array('id' => 'player_first_name_' . $player->getId(), 'name' => 'player[' . $player->getId() . '][first_name]', 'class' => 'form-control', 'value' => $player->getFirstName())) ?>
             </div>
             <div class="col-sm-12 col-lg-4">
                <?php echo $editForm->renderInputTag("last_name", array('id' => 'player_last_name_' . $player->getId(), 'name' => 'player[' . $player->getId() . '][last_name]', 'class' => 'form-control', 'value' => $player->getLastName())) ?>
             </div>
             <div class="col-sm-12 col-lg-4">
                 <?php echo $editForm->renderInputTag("email", array('id' => 'player_email_' . $player->getId(), 'name' => 'player[' . $player->getId() . '][email]', 'class' => 'form-control', 'value' => $player->getEmail())) ?>
             </div>
             <div class="col-sm-12 text-center col-lg-1">
                  <i data-toggle="tooltip" data-placement="top" title="<?php echo $translator->translate('Cancel changes') ?>" data-edit="<?php echo $player->getId() ?>" class="ico ico-close player-row-cancel-edit  round_btn bg_red"></i>
             </div>
             
             
         </div>
    
        
        <div id="static-data-<?php echo $player->getId() ?>">
            <span class="first_name"><?php echo $player->getFirstName() ?></span> <span class="last_name"><?php echo $player->getLastName() ?></span><br />
        <a class="color-link email" href="mailto:<?php echo $player->getEmail() ?>"><?php echo $player->getEmail() ?></a>
        </div>
    </td>


    <td>
        <?php echo $editForm->renderSelectTag("team_role", array('id' => 'team_role_' . $player->getId(), 'name' => 'player[' . $player->getId() . '][team_role]', 'class' => 'form-control', 'value' => $player->getTeamRole())) ?>
         <?php echo $validator->showError("team_role_".$player->getId()) ?>
    </td>
    <td>
        <?php echo $editForm->renderSelectTag("level", array('id' => 'player_level_' . $player->getId(), 'name' => 'player[' . $player->getId() . '][level]', 'class' => 'form-control', 'value' => $player->getLevel())) ?>
    </td>


    <td>
        <div class="input-group player_status_group" >
             <span class="input-group-addon" >
             <?php if('confirmed' == $player->getStatus()): ?>
                <span data-toggle="tooltip" data-placement="top" title="<?php echo $translator->translate('Confirmed') ?>" class="round_btn bg_green"><i class="ico ico-check"></i></span>
              <?php endif; ?>
                
              <?php if('unconfirmed' == $player->getStatus()): ?>
                <span data-toggle="tooltip" data-placement="top" title="<?php echo $translator->translate('Unconfirmed') ?>" class="round_btn bg_orange"><i class="ico ico-question"></i></span>
              <?php endif; ?>
                
             <?php if('unknown' == $player->getStatus() or 'waiting-to-confirm' == $player->getStatus() ): ?>
                <span data-toggle="tooltip" data-placement="top" title="<?php echo $translator->translate('Unknown') ?>" class="round_btn bg_gray"><i class="ico ico-dots-h"></i></span>
              <?php endif; ?>

              <?php if('decline' == $player->getStatus() ): ?>
                <span data-toggle="tooltip" data-placement="top" title="<?php echo $translator->translate('Decline') ?>" class="round_btn bg_red"><i class="ico ico-cross"></i></span>
              <?php endif; ?>

              <?php if('unactive' == $player->getStatus() ): ?>
                <span data-toggle="tooltip" data-placement="top" title="<?php echo $translator->translate('Unactive') ?>" class="round_btn bg_orange"><i class="ico ico-minus-w"></i></span>
              <?php endif; ?>
            </span>
            
            <?php if(in_array($player->getStatus(), array('confirmed'))): ?>
                <?php echo $editForm->renderSelectTag("status", array('id' => 'player_status_' . $player->getId(), 'name' => 'player[' . $player->getId() . '][status]', 'class' => 'form-control', 'value' => $player->getStatus())) ?>
            <?php elseif($player->getStatus() == 'unconfirmed'): ?>
                 <?php 
                 
                 $choices = $editForm->getFieldChoices('status');
                 $uncofirmedChoices = $choices;
                 $uncofirmedChoices['unconfirmed'] = $translator->translate('Unconfirmed');
                 $editForm->setFieldChoices('status',$uncofirmedChoices);
                  
                  
                 echo $editForm->renderSelectTag("status", array('id' => 'player_status_' . $player->getId(), 'name' => 'player[' . $player->getId() . '][status]', 'class' => 'form-control', 'value' => $player->getStatus())) ;
                     //reset choices   
                 $editForm->setFieldChoices('status',$choices); 
                         
                         ?>
            <?php elseif($player->getStatus() == 'unactive' &&  $player->getEmail() == null): ?>  
                 <?php echo $editForm->renderSelectTag("status", array('id' => 'player_status_' . $player->getId(), 'name' => 'player[' . $player->getId() . '][status]', 'class' => 'form-control', 'value' => $player->getStatus())) ?>
            <?php else: ?>
                <span id="player_status_text_<?php echo $player->getId() ?>"  class="player_status"><?php $choices = $editForm->getStatusChoices(); echo $choices[$player->getStatus() ] ?></span>
            <?php endif; ?>
            
            
            
        </div>
    </td>
    
     <td>
        <?php if($player->getEmail() != null): ?>
            <?php if ('confirmed' != $player->getStatus()): ?>
                <a data-row="player_row_<?php echo $player->getId() ?>"  data-toggle="tooltip" data-placement="top" title="<?php echo $translator->translate('Send invitation') ?>" href="<?php echo $router->link('team_send_invitation', array('id' => $player->getId(), 'hash' => $team->getShareLinkHash())) ?>" class="round_btn bg_turq"><i class="ico ico-envelope-w"></i> </a>
            <?php endif; ?>
            
        <?php endif; ?>
    </td>
    
    
    <td>
          <i data-pid="<?php echo $player->getPlayerId() ?>" data-edit="<?php echo $player->getId() ?>" class="ico ico-edit player-row-edit"></i>

        <a data-row="player_row_<?php echo $player->getId() ?>" title="<?php echo $translator->translate('Remove player from team') ?>" href="<?php echo $router->link('remove_team_player', array('pid' => $player->getId())) ?>" class="remove-team-player-trigger"><i class="ico ico-trash"></i></a>

    </td>
</tr>