<div class="modal fade progress-modal" id="too_many_players_free" tabindex="-1" role="dialog" aria-labelledby="">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="ico ico-close-black"></i></span></button>
                
                 <img src="/theme/img/icon/alert-mark.svg" alt="" title="" />
                <h4 class="modal-title modal-title-danger" id="myModalLabel"><span><?php echo $translator->translate('TOO_MANY_PLAYERS_MODAL_TITLE') ?></span></h4>
            </div>

            <div class="modal-body">
               <?php echo $text ?>
            </div>

            <div class="modal-footer">
                    <div class="row">
                        <div class="col-xs-6 col-sm-offset-4 col-sm-4"> 
                            <a class="btn btn-primary team_credit_modal_trigger" data-dismiss="modal" href="<?php echo $s2sContinueLink ?>"><?php echo $translator->translate('TOO_MANY_PLAYERS_PRO_BTN') ?></a>
                        </div>

                        <div class="col-xs-6 col-sm-4 text-left">
                            <a class="s2s_skip_trigger" data-dismiss="modal" href="#"><?php echo $translator->translate('TOO_MANY_PLAYERS_CHANGE_EDIT') ?></a>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
   $('#too_many_players_free').modal('show');
</script>