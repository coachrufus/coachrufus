 <div class="post_comments_wrap row lvl<?php echo $post->getLvl() ?>" id="post_reaction_<?php echo $post->getId() ?>">
       <div class="user-block pull-left">
            <div class="img_round_wrap member_img_round" style="background-image: url('<?php echo \Core\ServiceLayer::getService('security')->getIdentity()->getUser()->getMidPhoto() ?>');"></div>
        </div><!-- /.user-block -->
     
        <div class="post_comment_wrap_cnt col-sm-10">
            <input  id="post-comment-body-<?php echo $post->getId() ?>" class="form-control input-sm" type="text" placeholder="<?php echo $translator->translate('Type comment') ?>" />
        </div>
        <div class="col-sm-1 comment_send_wrap">
           <button data-action="<?php echo $router->link('create_team_wall_post_comment') ?>" type="button" data-post-id="<?php echo $post->getId() ?>" class="post_comment_trigger"><?php echo $translator->translate('Send') ?></button>
        </div>
    </div>
