<div class="post" id="post-<?php echo $post->getId() ?>">
    
   
    <?php if($post->getAuthorId() == \Core\ServiceLayer::getService('security')->getIdentity()->getUser()->getId()): ?>
     <div class="pull-right">
            <a  data-target="post-<?php echo $post->getId() ?>" href='<?php echo $router->link('delete_team_wall_post',array('id' => $post->getId())) ?>' class='pull-right btn-box-tool delete-post-trigger' data-toggle="tooltip" data-placement="top"  data-original-title="<?php echo $translator->translate('DELETE_WALL_POST') ?>"><i class='fa fa-times'></i></a>
    </div>
    <?php endif; ?>
   
    
    
    <div class="user-block pull-left">
        <div class="img_round_wrap member_img_round" style="background-image: url('<?php echo $post->getAuthor()->getMidPhoto() ?>');"></div>
    </div><!-- /.user-block -->
    <div class="post-cnt">
        <span class="post_author"><?php echo $post->getAuthor()->getFullName() ?></span> <?php echo $post->getHtmlBody() ?>
    <?php echo $post->getHtmlAddContent() ?>
    
    <?php foreach($post->getImagesCollection() as $image): ?>
        <img src="<?php echo $image ?>" />
    <?php endforeach; ?>
    </div>



    <ul class="list-inline">
        <li class="post_date"><?php echo $post->getFormatedCreatedAt() ?></li>
        <li>
            <?php if($post->isLikedByUser(\Core\ServiceLayer::getService('security')->getIdentity()->getUser())): ?>
              <a href="<?php echo $router->link('tam_wall_post_unlike', array('post' => $post->getId())) ?>" class="post-unlike-trigger">
                 <?php echo $translator->translate('Unlike') ?> <span class="likes_count">(<?php echo $post->getLikeCount() ?>)</span>
            </a>
        <?php else: ?>
            
            <span class="like-info"><i class="ico ico-like"></i> <span class="likes_count"><?php echo $post->getLikeCount() ?></span></span>
            <a href="<?php echo $router->link('tam_wall_post_like', array('post' => $post->getId())) ?>" class="post-like-trigger">
                <?php echo $translator->translate('Like') ?>
            </a>
            <?php endif; ?>
        </li>
        <li>
            <span class="comments-info"><i class="ico ico-reply"></i><?php echo $post->getCommentsCount() ?></span>
            <a data-target="post-<?php echo $post->getId() ?>" href="<?php echo $router->link('get_team_wall_post_comments', array('post' => $post->getId())) ?>" class="show-comments-trigger"> <?php echo $translator->translate('Reply') ?></a>
        </li>
    </ul>
    
   

   

</div><!-- /.post -->