<div class="post-comment-block lvl<?php echo $comment->getLvl() ?>" id="post-<?php echo $comment->getId() ?>">
        <div class=" pull-left comment-a-wrap"><i class="ico ico-comment-a"></i></div>
        <div class="user-block pull-left">
            <div class="img_round_wrap member_img_round" style="background-image: url('<?php echo $comment->getAuthor()->getMidPhoto() ?>');"></div>

           
        </div><!-- /.user-block -->
        <div class="post-cnt">
            <span class="post_author"><?php echo $comment->getAuthor()->getFullName() ?></span> <?php echo $comment->getBody() ?>
        </div>
         <ul class="list-inline">
            <li class="post_date"><?php echo $comment->getFormatedCreatedAt() ?></li>
            <li>
                <?php if($comment->isLikedByUser(\Core\ServiceLayer::getService('security')->getIdentity()->getUser())): ?>
                  <a href="<?php echo $router->link('tam_wall_post_unlike', array('post' => $comment->getId())) ?>" class="post-unlike-trigger">
                     <?php echo $translator->translate('Unlike') ?> <span class="likes_count">(<?php echo $comment->getLikeCount() ?>)</span>
                </a>
            <?php else: ?>

                <span class="like-info"><i class="ico ico-like"></i> <span class="likes_count"><?php echo $comment->getLikeCount() ?></span></span>
                <a href="<?php echo $router->link('tam_wall_post_like', array('post' => $comment->getId())) ?>" class="post-like-trigger">
                    <?php echo $translator->translate('Like') ?>
                </a>
                <?php endif; ?>
            </li>
            <li>
                <span class="comments-info"><i class="ico ico-reply"></i><?php echo $comment->getCommentsCount() ?></span>
                <a data-target="post-<?php echo $comment->getId() ?>" href="<?php echo $router->link('get_team_wall_post_comments', array('post' => $comment->getId())) ?>" class="show-comments-trigger"> <?php echo $translator->translate('Reply') ?></a>
            </li>
        </ul>
    </div>