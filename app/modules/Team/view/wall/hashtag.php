<?php Core\Layout::getInstance()->startSlot('team_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/Team/view/team/_team_menu.php', array('team' => $team)) ?>
<?php Core\Layout::getInstance()->endSlot('team_menu') ?>



<section class="content-header">
    <h1>
        <?php echo $team->getName() ?>
    </h1>
    
    <?php $layout->includePart(GLOBAL_DIR.'/templates/parts/_breadcrumb.php',array(
        'crumbs' => array(
            'Teams' => $router->link('teams_overview'),
            $team->getName() => ''
            ))) ?>
</section>
<section class="content">


            <div class="row">
               
                
                 <div class="col-md-4">
                       <div class="panel panel-default">
                           <div class="panel-heading">
                               <?php echo $translator->translate('Posts') ?> <?php echo $hashtag ?>
                           </div>
                            <div class="panel-body">
                 
                     
                     <div id="wall_posts">
                        <?php foreach($wallPosts as $post): ?>
                            <!-- Post -->
                           <?php $layout->includePart(MODUL_DIR.'/Team/view/wall/_wall_line.php',array('post' => $post, 'isOwner' => $isOwner )) ?>
                        <?php endforeach; ?>
                      </div>
                                
                       <?php if($post != null): ?>
                        <a class="btn btn-primary load-wall_posts" data-start="<?php echo $post->getId() ?>" data-team="<?php echo $team->getId() ?>" href="<?php echo $router->link('tam_wall_load_posts') ?>" ><?php echo $translator->translate('Load More') ?></a>
                       <?php endif; ?>  
                                
                     
                     </div>
                     </div>
                </div>
                
                
            
            </div>



</section>




<?php $layout->addJavascript('js/TeamWallManager.js') ?>




<?php  $layout->startSlot('javascript') ?>

<script type="text/javascript"> 
    
    
    
    
    



      wall_manger = new TeamWallManager();
      wall_manger.bindTriggers();
      
      
     
    
    
</script>

<?php  $layout->endSlot('javascript') ?>


