<div class="modal fade" id="wall-photo-modal" tabindex="-1" role="dialog" aria-labelledby="<?php echo $translator->translate('Add image') ?>">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $translator->translate('Add image') ?></h4>
      </div>
      <div class="modal-body">
        <div class="upload-wrap">

                          
                     

                            <span class="btn btn-success fileinput-button">
                               <i class="glyphicon glyphicon-plus"></i>
                               <span><?php echo $translator->translate('Select files from your computer') ?></span>
                                 <input id="post_fileupload" type="file" name="post_image" data-url="<?php echo $router->link('tam_wall_upload_photo') ?>" />
                           </span>
                        </div>
                       

                        <div class="upload-progress">
                            <span class="progress-num"></span>
                           <div class="progress progress-sm active">
                            <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                            </div>
                          </div>
                         </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>