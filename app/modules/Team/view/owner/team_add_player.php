<?php Core\Layout::getInstance()->startSlot('crumb') ?>
<li><a href="<?php echo $router->link('teams_overview') ?>"><?php echo $translator->translate('Teams') ?></a></li>
<li><a href="<?php echo $router->link('team_overview', array('id' => $team->getId())) ?>"><?php echo $team->getName() ?></a></li>
<li><a href="<?php echo $router->link('team_players_list', array('team_id' => $team->getId())) ?>"><?php echo $translator->translate('Zoznam hráčov') ?></a></li>
<li class="active"><?php echo $translator->translate('Pridat hráčoa') ?></li>
<?php Core\Layout::getInstance()->endSlot('crumb') ?>

<?php if ($request->hasFlashMessage('team_create_player_success')): ?>
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <?php echo $request->getFlashMessage('team_create_player_success') ?>
    </div>
<?php endif; ?>


<h1><?php echo $translator->translate('Pridať hráča') ?></h1>

<form action="" class="form-horizontal form-bordered" method="post">
    <div class="row">
        <div class="col-md-12">

            <div class="form-group">

                <label for="exampleInputEmail1">
                    <?php echo $translator->translate('Nick/meno') ?>
                </label>

                <div class="autocomplete_wrap">
                    <?php echo $form->renderInputTag('name', array('class' => 'form-control user-find-autocomplete', 'autocomplete' => 'off')); ?>
                    <ul class="autocomplete_list">

                    </ul>
                </div>

            </div>

        </div>
</form>

<table id="selected_players_template">
    <tr class="template-item">
        <td>
            <input type="hidden" name="player[][team_id]" id="player_team_id" value="<?php echo $team->getId() ?>" />
            <strong class="player_name"></strong>
        </td>
        <td><?php echo $addForm->renderSelectTag('team_role') ?></td>
        <td><?php echo $addForm->renderSelectTag('level') ?></td>
        <td><?php echo $addForm->renderSelectTag('status') ?></td>
    </tr>
</table>

<form action="<?php echo $router->link('create_team_player', array('team_id' => $team->getId())) ?>" method="post">
    <table id="selected_players" class="table">

    </table>
    <input type="submit" class="btn" value="<?php echo $translator->translate('Pridať') ?>" />
</form>



<form action="" id="add_player_form">
    <input type="hidden" name="player[0][team_id]" id="player_team_id" value="<?php echo $team->getId() ?>" />
    <div class="row">
        <div class="form-group col-sm-3">
            <label class="control-label"><?php echo $translator->translate($addForm->getFieldLabel("first_name")) ?></label>
            <?php echo $addForm->renderInputTag("first_name", array('name' => 'player[0][first_name]','class' => 'form-control')) ?>
        </div>
        <div class="form-group col-sm-3">
            <label class="control-label"><?php echo $translator->translate($addForm->getFieldLabel("last_name")) ?></label>
            <?php echo $addForm->renderInputTag("last_name", array('name' => 'player[0][last_name]','class' => 'form-control')) ?>
        </div>
        <div class="form-group col-sm-3">
            <label class="control-label"><?php echo $translator->translate($addForm->getFieldLabel("email")) ?></label>
            <?php echo $addForm->renderInputTag("email", array('name' => 'player[0][email]','class' => 'form-control')) ?>
        </div>
        <div class="form-group col-sm-3">
            <label class="control-label"><?php echo $translator->translate($addForm->getFieldLabel("level")) ?></label>
            <?php echo $addForm->renderSelectTag("level", array('name' => 'player[0][level]','class' => 'form-control')) ?>
        </div>
        <div class="form-group col-sm-1">
            <a id="add_player" href=""><?php echo $translator->translate('Add') ?></a>
        </div>
    </div>
    
    
    
    <!-- Modal -->
<div class="modal fade" id="addPlayerModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
              <input type="checkbox" name="player_notify"  value="1" />
        <?php echo $translator->translate('Send notification') ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary add_player_form_trigger">Save</button>
      </div>
    </div>
  </div>
</div>
</form>




</div>

<?php $layout->startSlot('javascript') ?>
<script type="text/javascript">

    autocomplete = new UserSearchAutocomplete($('.user-find-autocomplete'));
    autocomplete.init();
    
    teamPlayerManager = new TeamPlayerManager();
    teamPlayerManager.init();
    
</script>
<?php $layout->endSlot('javascript') ?>
    


