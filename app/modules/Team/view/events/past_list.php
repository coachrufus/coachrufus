<?php Core\Layout::getInstance()->startSlot('team_menu') ?>  
<?php $layout->includePart(MODUL_DIR.'/Team/view/team/_team_menu.php',array('team' => $team) ) ?>
<?php Core\Layout::getInstance()->endSlot('team_menu') ?>

<section class="content-header">
    <h1>
        <?php echo $translator->translate('Event list') ?>
    </h1>
    <?php $layout->includePart(GLOBAL_DIR.'/templates/parts/_breadcrumb.php',array(
        'crumbs' => array(
            'Teams' => $router->link('teams_overview'),
            $team->getName() => $router->link('team_overview',array('id' =>$team->getId() )),
            'Events' => $router->link('team_event_list', array('team_id' => $team->getId())),
            'Past events' => ''
            ))) ?>
</section>
<section class="content">



    <section class="box box-primary">
        <div class="box-header with-border">
            <?php $layout->includePart(MODUL_DIR . '/Team/view/events/_panel.php', array('calendar_date' => $calendar_date, 'team' => $team)) ?>
        </div>


        <div class="box-body">
            
            
            
            
            
          
                
                <?php foreach($eventsList as $dayEvents): ?>
                <?php foreach($dayEvents as $event): ?>
                
                
                <div class="row event-list-item">
                    <div class="col-sm-4">
                        <div class="event-list-date">
                            <?php echo  $event->getCurrentDate()->format('m/Y'); ?>
                            <h2><?php echo  $event->getCurrentDate()->format('d'); ?></h2>
                             <?php echo $event->getCurrentDate()->format('H:i') ?>
                        
                        </div>
                        <span class="label label-success"><?php echo $event->getTypeName()  ?></span>  
                        <h3>
                            <a href="<?php echo $router->link('team_event_detail',array('id' => $event->getId(),'current_date' => $event->getCurrentDate()->format('Y-m-d'))) ?>"  >
                            <?php echo $event->getName() ?>
                            </a>
                        </h3>
                        <?php echo $event->getPlaygroundName() ?> 
                    </div>
                    
                    
                    
                    <div class="col-sm-4 event-list-attendance">
                        
                        
                          
                        
                    </div>
                   
                </div>
               
               
                <?php endforeach; ?>
               <?php endforeach; ?>

        </div>
    </section>
</section>

<?php  $layout->startSlot('javascript') ?>
 <script type="text/javascript">
      event_manger = new TeamEventsManager();
      event_manger.init();
      

 </script>
<?php  $layout->endSlot('javascript') ?>