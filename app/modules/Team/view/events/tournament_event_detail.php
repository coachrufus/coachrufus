<?php Core\Layout::getInstance()->startSlot('team_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/Team/view/team/_team_menu.php', array('team' => $team)) ?>
<?php Core\Layout::getInstance()->endSlot('team_menu') ?>




<section class="content-header">
    <a class="all-event-link" href="<?php echo $router->link('team_event_list', array('team_id' => $team->getId())) ?>"><i class="ico ico-event-back"></i><?php echo $translator->translate('All Events') ?></a>
    <?php
    $layout->includePart(GLOBAL_DIR . '/templates/parts/_breadcrumb.php', array(
        'crumbs' => array(
            'Teams' => $router->link('teams_overview'),
            $team->getName() => $router->link('team_overview', array('id' => $team->getId())),
            'Events' => $router->link('team_event_list', array('team_id' => $team->getId())),
            $event->getName() => ''
)))
    ?>
</section>



<section class="content">
    <div class="row">
        <div class="col-md-8">
            <div class="event-detail">
                <div class="panel panel-default">
                    <div class="panel-body  panel-full-body">
                        <h1><?php echo $event->getName() ?>, <?php echo $event->getStart()->format('H:i'); ?></h1>
                        <span class="mute">(<?php echo $event->getSportName() ?>,<?php echo $event->getTypename() ?> )</span>
                        <?php if (null != $event->getPlayground()): ?>
                            <h4 id='event-playground-name'><?php echo $event->getPlayground()->getName() ?></h4>
                        <?php endif; ?>
                    </div>   
                    
                      <?php if(array_key_exists('api', $_SESSION) && array_key_exists('layout', $_SESSION['api'])): ?>
                        <div class="col-sm-12 sm-score-button">
                             <br /> 
                            <a  class="btn btn-primary mpopup" data-action="popup-full" data-popup-onclose-action="refresh" href="<?php echo WEB_DOMAIN.$router->generateApiWidgetUrl('api_content_games_score_widget',array(
                                'apikey' => CR_API_KEY,
                                'user_id' => $user->getId(),
                                'event_id' => $event->getId(),
                                'lineup_id' => $lineup->getId() ,
                                'event_date' => $event->getCurrentDate()->format('Y-m-d'))) ?>"><?php echo $translator->translate('widget.event.sidebar.addScore') ?></a>  
                        </div>
                        <?php else: ?>
                             <div class="col-sm-12 desk-score-button">
                            <br />
                            <a  class="btn btn-primary" href="<?php echo WEB_DOMAIN.$router->generateApiWidgetUrl('api_content_games_score_widget',array(
                                'apikey' => CR_API_KEY,
                                'user_id' => $user->getId(),
                                'event_id' => $event->getId(),
                                'lineup_id' => $lineup->getId() ,
                                'event_date' => $event->getCurrentDate()->format('Y-m-d'))) ?>"><?php echo $translator->translate('widget.event.sidebar.addScore') ?></a>  
                        </div>
                        <?php endif; ?>


                    <div class="row">
                        <div class="col-sm-6 attendance-group">
                            <?php echo $homeTeam->getName() ?>
                            <?php $homeTeamMembersIds = array(); ?>
                            <?php foreach ($homeTeamPlayers as $teamMember): ?>
                                <?php
                                $homeTeamMembersIds[] = $teamMember->getPlayerId();
                                $layout->includePart(MODUL_DIR . '/Team/view/events/_tournament_event_lineup_row.php', array(
                                    'event' => $event,
                                    'teamMember' => $teamMember,
                                    'team' => $team,
                                    'user' => $user,
                                    'enableLineup' => $enableHomeTeamLineup,
                                    'lineupCode' => 'first_line',
                                    'lineup' => $lineup,
                                    'lineupPlayersIds' => $lineupPlayersIds
                                ))
                                ?>
                            <?php endforeach; ?>
                            
                            <?php if (in_array($user->getId(), $homeTeamMembersIds)): ?>
                             <br />   
                            <a   class="btn btn-primary pull-right " href="<?php echo $router->link('team_players_list',array('team_id' => $homeTeam->getId())) ?>"><?php echo $translator->translate('tournament.event.detail.addPlayers') ?></a>
                            <?php endif ?>
                            
                            
                        </div>
                        <div class="col-sm-6">
                            <?php echo $opponentTeam->getName() ?>
                             <?php $opponentTeamMembersIds = array(); ?>
                            <?php foreach ($opponentTeamPlayers as $teamMember): ?>
                                <?php
                                 $opponentTeamMembersIds[] = $teamMember->getPlayerId();
                                $layout->includePart(MODUL_DIR . '/Team/view/events/_tournament_event_lineup_row.php', array(
                                    'event' => $event,
                                    'teamMember' => $teamMember,
                                    'team' => $team,
                                    'user' => $user,
                                    'enableLineup' => $enableOpponentTeamLineup,
                                    'lineup' => $lineup,
                                    'lineupCode' => 'second_line',
                                    'lineupPlayersIds' => $lineupPlayersIds
                                ))
                                ?>
                                <?php endforeach; ?>
                            
                            <?php if (in_array($user->getId(), $opponentTeamMembersIds)): ?>
                            <br />
                                <a  class="btn btn-primary pull-right" href="<?php echo $router->link('team_players_list',array('team_id' => $opponentTeam->getId())) ?>"><?php echo $translator->translate('tournament.event.detail.addPlayers') ?></a>
                            <?php endif ?>
                        </div>
                       
                        <?php if(array_key_exists('api', $_SESSION) && array_key_exists('layout', $_SESSION['api'])): ?>
                        <div class="col-sm-12 sm-score-button">
                             <br /> 
                            <a style="color:white;" class="btn btn-primary mpopup" data-action="popup-full" data-popup-onclose-action="refresh" href="<?php echo WEB_DOMAIN.$router->generateApiWidgetUrl('api_content_games_score_widget',array(
                                'apikey' => CR_API_KEY,
                                'user_id' => $user->getId(),
                                'event_id' => $event->getId(),
                                'lineup_id' => $lineup->getId() ,
                                'event_date' => $event->getCurrentDate()->format('Y-m-d'))) ?>"><?php echo $translator->translate('widget.event.sidebar.addScore') ?></a>  
                        </div>
                        <?php else: ?>
                             <div class="col-sm-12 desk-score-button">
                            <br />
                            <a  class="btn btn-primary" href="<?php echo WEB_DOMAIN.$router->generateApiWidgetUrl('api_content_games_score_widget',array(
                                'apikey' => CR_API_KEY,
                                'user_id' => $user->getId(),
                                'event_id' => $event->getId(),
                                'lineup_id' => $lineup->getId() ,
                                'event_date' => $event->getCurrentDate()->format('Y-m-d'))) ?>"><?php echo $translator->translate('widget.event.sidebar.addScore') ?></a>  
                        </div>
                        <?php endif; ?>
                    </div>


                </div>
            </div>




        </div>
        <div class="col-md-4">
<?php $layout->includePart(MODUL_DIR . '/Event/view/widget/_small_calendar.php') ?>
        </div>
    </div>
</section>








<?php $layout->addJavascript('js/TeamEventManager.js') ?>
<?php $layout->addJavascript('js/TeamMatchManager.js') ?>
<?php $layout->addJavascript('js/WidgetManager.js') ?>
<?php $layout->addJavascript('js/SmallCalendar.js') ?>



<?php $layout->startSlot('javascript') ?>
<?php echo Core\ServiceLayer::getService('AchievementAlertManager')->showPlaceAchievementAlert('event_detail_attendance', 'event_attendance_create', Core\ServiceLayer::getService('security')->getIdentity()) ?>


<script type="text/javascript">
    var match_manger = new TeamMatchManager();
    match_manger.initLineupTriggers();

    /*
     var event_manger = new TeamEventsManager();
     event_manger.init();
     
     function showEventLocation()
     {
     event_manger.google = google;
     }
     */
    var small_calendar = new SmallCalendar({
        'dataUrl': '<?php echo $router->link('rest_event_get_month_grid') ?>',
        'localityManager': new LocalityManager()
    });
    small_calendar.initLoad();


</script>




<?php $layout->endSlot('javascript') ?>


