<?php Core\Layout::getInstance()->startSlot('team_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/Team/view/team/_team_menu.php', array('team' => $team)) ?>
<?php Core\Layout::getInstance()->endSlot('team_menu') ?>

<section class="content-header">

    <a class="all-event-link" href="<?php echo $router->link('team_event_list', array('team_id' => $team->getId())) ?>"><i class="ico ico-event-back"></i><?php echo $translator->translate('All Events') ?></a>
    
     <?php echo $layout->renderControllerAction('CR\Gamifications\GamificationModule:Progress:progressBar')->getContent() ?>
    
    <?php
    $layout->includePart(GLOBAL_DIR . '/templates/parts/_breadcrumb.php', array(
        'crumbs' => array(
            'Teams' => $router->link('teams_overview'),
            $team->getName() => $router->link('team_overview', array('id' => $team->getId())),
            'Events' => $router->link('team_event_list', array('team_id' => $team->getId())),
            $event->getName() => ''
)))
    ?>
</section>



<section class="content">
    <div class="row">
        <div class="col-md-8">
            <div class="event-detail">
                <div class="panel panel-default rating-panel">
                    <?php
                    $layout->includePart(MODUL_DIR . '/Team/view/events/_event_detail.php', array(
                        'event' => $event,
                        'team' => $team,
                        'existLineup' => $lineup,
                        'menuActive' => 'rating',
                    ))
                    ?>
                    
                    <div class="row">
                        <div class="col-sm-4">
                            <img src="/img/main/logo-sq.svg" />
                        </div>
                        <div class="col-sm-8 text-left">
                            <?php echo $translator->translate('RATING_UNAVAILABLE') ?>
                           
                            
                           
                            
                        </div>
                        
                    </div>
                    
                    
                </div>
                
                
            </div>
        </div>
       <div class="col-md-4">
            <?php $layout->includePart(MODUL_DIR . '/Event/view/widget/_small_calendar.php') ?>
        </div>
    </div>
</section>


<?php $layout->includePart(MODUL_DIR . '/Team/view/events/_confirm_single_delete_modal.php', array('event' => $event, 'eventTimeline' => $eventTimeline)) ?>
<?php $layout->addJavascript('js/TeamEventManager.js') ?>
<?php $layout->addJavascript('js/WidgetManager.js') ?>
<?php $layout->addJavascript('plugins/starbox/jstarbox.js') ?>
<?php $layout->addJavascript('js/SmallCalendar.js') ?>

<?php Core\Layout::getInstance()->startSlot('stylesheet') ?>  
<link rel="stylesheet" href="/dev/plugins/starbox/css/jstarbox.css">
<?php Core\Layout::getInstance()->endSlot('stylesheet') ?>

<?php $layout->startSlot('javascript') ?>
<script type="text/javascript">


    var event_manger = new TeamEventsManager();
    event_manger.init();
    
     function showEventLocation()
    {
        event_manger.google = google;
    }


    var small_calendar = new SmallCalendar({
        'dataUrl': '<?php echo $router->link('rest_event_get_month_grid') ?>',
        'localityManager': new LocalityManager()
    });
    small_calendar.initLoad();

</script>


<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXU1duK54f1gIsRy_xc7z4uQURovsZdE8&libraries=places&callback=showEventLocation" async defer></script>


<?php $layout->endSlot('javascript') ?>


