<?php Core\Layout::getInstance()->startSlot('stylesheet') ?>  
<link rel="stylesheet" href="/dev/plugins/fullcalendar/fullcalendar.min.css">
<link rel="stylesheet" href="/dev/plugins/fullcalendar/fullcalendar.print.css" media="print">
<?php Core\Layout::getInstance()->endSlot('stylesheet') ?>
<?php Core\Layout::getInstance()->startSlot('team_menu') ?>  
<?php $layout->includePart(MODUL_DIR.'/Team/view/team/_team_menu.php',array('team' => $team) ) ?>
<?php Core\Layout::getInstance()->endSlot('team_menu') ?>
<section class="content-header">
   <h1>
      <?php echo $translator->translate('Event calendar') ?>
   </h1>
    <?php $layout->includePart(GLOBAL_DIR.'/templates/parts/_breadcrumb.php',array(
        'crumbs' => array(
            'Teams' => $router->link('teams_overview'),
            $team->getName() => $router->link('team_overview',array('id' =>$team->getId() )),
            'Events' => $router->link('team_event_list', array('team_id' => $team->getId())),
            'Calendar' => ''
            ))) ?>
</section>
<section class="content">
   <div class="box box-primary">
       <div class="box-header with-border">



                    <div class="panel-toolbar">
                        <?php if (\Core\ServiceLayer::getService('security')->getIdentity()->hasTeamPermission('manageEvents', $team)  ): ?>
                        <a class="color-link tool-item tool-item-link" href="<?php echo $router->link('create_team_event', array('team_id' => $team->getId())) ?>"><i class="ico ico-plus-green"></i> <span class="hide760"><?php echo $translator->translate('Add event') ?></span></a>
                        <?php endif; ?>

                        <div class="btn-group tool-item" role="group">  
                            <a class="dropdown-toggle export-event" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="<?php echo $router->link('team_events_calendar', array('calendar_date' => $calendar_date, 'team_id' => $team->getId())) ?>"><i class="ico ico-export"></i> <span><?php echo $translator->translate('Export') ?></span></a>
                            <ul class="dropdown-menu">
                                <li> <a class="popup-link" href="<?php echo $router->link('team_event_ical', array('team_id' => $team->getId())) ?>"><?php echo $translator->translate('Download .ics file') ?></a></li>
                                <li> <a class="popup-link" href="webcal://<?php echo WEB_DOMAIN_NAME ?><?php echo $router->link('team_event_ical', array('team_id' => $team->getId())) ?>">Outlook</a></li>
                                <li> <a class="popup-link" href="webcal://<?php echo WEB_DOMAIN_NAME ?><?php echo $router->link('team_event_ical', array('team_id' => $team->getId())) ?>">iCalc</a></li>
                                <li> <a class="popup-link" href="https://www.google.com/calendar/render?cid=webcal://<?php echo WEB_DOMAIN_NAME ?><?php echo $router->link('team_event_ical', array('team_id' => $team->getId())) ?>"><?php echo $translator->translate('Google calendar') ?></a></li>
                            </ul>
                        </div>
                            
                        <a class="tool-item tool-item-link" href="<?php echo $router->link('team_events_calendar', array('team_id' => $team->getId())) ?>"> <i class="ico ico-panel-event-list"></i></a> 
                    </div>
           
           
           
          
           
           
          <?php //$layout->includePart(MODUL_DIR.'/Team/view/events/_panel.php',array('calendar_date' => $calendar_date,'team' => $team) ) ?>
       </div>
       
       
      <div class="box-body no-padding">
         <div id="calendar" class="fc fc-ltr fc-unthemed">
            <div class="fc-toolbar">
               <div class="fc-left">
                  <div class="fc-button-group">
                      <a type="button" class="fc-prev-button fc-button fc-state-default fc-corner-left" href="<?php echo $router->link('team_events_calendar',array('team_id' => $team->getId(),'calendar_date' => $prev_calendar_date)) ?>">
                          <span class="fc-icon fc-icon-left-single-arrow"></span>
                      </a>
                      <a  href="<?php echo $router->link('team_events_calendar',array('team_id' => $team->getId(),'calendar_date' => $next_calendar_date)) ?>" type="button" class="fc-next-button fc-button fc-state-default fc-corner-right">
                          <span class="fc-icon fc-icon-right-single-arrow"></span>
                      </a>
                  </div>
               </div>
              
               <div class="fc-center">
                  <h2><?php echo $month_name.' '.$current_year ?></h2>
               </div>
               <div class="fc-clear"></div>
            </div>
            <div class="fc-view-container">
               <div class="fc-view fc-month-view fc-basic-view">
                  <table>
                     <thead>
                        <tr>
                           <td class="fc-widget-header">
                              <div class="fc-row fc-widget-header">
                                 <table>
                                    <thead>
                                       <tr>
                                        
                                          <th class="fc-day-header fc-widget-header fc-mon"><?php echo $translator->translate('Mon') ?></th>
                                          <th class="fc-day-header fc-widget-header fc-tue"><?php echo $translator->translate('Tue') ?></th>
                                          <th class="fc-day-header fc-widget-header fc-wed"><?php echo $translator->translate('Wed') ?></th>
                                          <th class="fc-day-header fc-widget-header fc-thu"><?php echo $translator->translate('Thu') ?></th>
                                          <th class="fc-day-header fc-widget-header fc-fri"><?php echo $translator->translate('Fri') ?></th>
                                          <th class="fc-day-header fc-widget-header fc-sat"><?php echo $translator->translate('Sat') ?></th>
                                           <th class="fc-day-header fc-widget-header fc-sun"><?php echo $translator->translate('Sun') ?></th>
                                       </tr>
                                    </thead>
                                 </table>
                              </div>
                           </td>
                        </tr>
                     </thead>
                     <tbody>
                         <tr>
                             <td class="fc-widget-content">
                                 <div class="fc-day-grid-container">
                                 <div class="fc-day-grid">
                                      <?php foreach($overview as $week): ?>
                                     
                                        <div class="fc-row fc-week fc-widget-content">
                                        <div class="fc-bg">
                                           <table>
                                              <tbody>
                                                 <tr>
                                                     <?php foreach($week as $day): ?>
                                                        <td class="fc-day fc-widget-content fc-mon fc-past">
     
                                                        </td>
                                                        <?php endforeach; ?>
                                                 </tr>
                                              </tbody>
                                           </table>
                                        </div>
                                            
                                            <div class="fc-content-skeleton">
                                            <table>
                                               <thead>
                                                  
                                                  <tr>
                                                       <?php foreach($week as $day): ?>
                                                        <td class="fc-day-number fc-mon  fc-past <?php echo ($day['month'] != $current_month) ? 'fc-other-month' : '' ?>" data-date="<?php echo $day['date']  ?>"> <?php echo $day['day'] ?></td>
                                                       <?php endforeach; ?>
                                                  </tr>
                                               </thead>
                                               <tbody>
                                                   <tr>
                                                   <?php foreach($week as $day): ?>
                                                       <td class="fc-event-container">
                                                          
                                                           <?php foreach($day['events'] as $event): ?>
                                                         
                                                           
                                                               <div class="fc-content">
                                                                    <a class="" href="<?php echo $router->link('team_event_detail',array('id' => $event->getId(),'current_date' => $event->getCurrentDate()->format('Y-m-d'))) ?>" >
                                                                   <?php echo $event->getName() ?> <?php echo $event->getNote() ?>
                                                                      </a>
                                                                   
                                                                    <?php if (\Core\ServiceLayer::getService('security')->getIdentity()->hasTeamPermission('manageEvents', $team)): ?>
                                                                    <a  data-event-type="<?php echo $event->getPeriod() ?>" class="delete-calendar-event-trigger" href="<?php echo $router->link('confirm_delete_team_event', array('event_id' => $event->getId(),'event_date' => $event->getCurrentDate()->format('Y-m-d'))) ?>"><i class="fa fa-close"></i></a>
                                                                    <?php endif; ?>
                                                               </div>
                                                         
                                                           
                                                          
                                                          
                                                           <?php endforeach; ?>
                                                           
                                                            <?php if (\Core\ServiceLayer::getService('security')->getIdentity()->hasTeamPermission('manageEvents', $team)): ?>
                                                           <a class="btn btn-sm btn-default pull-left calendar-add-event" href="<?php echo $router->link('create_team_event',array('team_id' => $team->getId(),'date'=> $day['date'] )) ?>"> <i class="fa fa-plus-circle"></i> <span><?php echo $translator->translate('Add event') ?></span></a>
                                                            <?php endif; ?>
                                                       </td> 
                                                   <?php endforeach; ?>
                                                   </tr>
                                               </tbody>
                                              
                                            </table>
                                         </div>  
                                        </div>
                                      <?php endforeach; ?>
                                 </div>
                                 
                             </td>
                             
                         </tr>   
                         
                    
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>


<?php $layout->includePart(MODUL_DIR . '/Team/view/events/_confirm_calendar_delete_modal.php') ?>
<?php $layout->addJavascript('js/TeamEventManager.js') ?>

  
<?php  $layout->startSlot('javascript') ?>
<script type="text/javascript">

$('.fc-content-skeleton td').on('mouseenter',function(){
    $(this).find('.calendar-add-event').show();
});

$('.fc-content-skeleton td').on('mouseleave',function(){
    $(this).find('.calendar-add-event').hide();
});

event_manger = new TeamEventsManager();
event_manger.init();

</script>
<?php  $layout->endSlot('javascript') ?>

    
