<div class="modal fade" id="confirm-calendar-delete-modal" tabindex="-1" role="dialog" aria-labelledby="<?php echo $translator->translate('Confirm delete') ?>">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $translator->translate('Delete  event') ?></h4>
      </div>
      <div class="modal-body">
          
          <div id="single-delete-event-wrap">
            <div class="alert alert-warning"><?php echo $translator->translate('Would you like to delete this event?') ?></div>
            <a href="" class=" btn btn-primary" id="delete_event_link_all" data-val="all" class="btn btn-primary"><?php echo $translator->translate('Delete') ?></a>
          </div>

          <div id="multi-delete-event-wrap">
          <div class="alert alert-warning"><?php echo $translator->translate('Would you like to delete this event only, all events in the series or this and all following events in the series?') ?></div>
          
           <div class="row border-row">
               <div class="col-xs-6 col-sm-6">
                  <?php echo $translator->translate('I want delete') ?>
              </div>
              <div class="col-xs-6  col-sm-6">
                 <?php echo $translator->translate('What happens') ?>
              </div>
          </div>
          
          
           <div class="row">
               <div class="col-xs-6 col-sm-6">
                  <a href="" class=" btn btn-primary" id="delete_event_link_only"  data-val="only" ><?php echo $translator->translate('Only this') ?></a>
              </div>
              <div class="col-xs-6 col-sm-6">
                  <h5><?php echo $translator->translate('All other events in the series will be retained') ?></h5>
              </div>
          </div>
          <div class="row">
               <div class="col-xs-6 col-sm-6">
                  <a href="" class=" btn btn-primary"  id="delete_event_link_next"  data-val="only" ><?php echo $translator->translate('All the following') ?></a>
              </div>
              <div class="col-xs-6 col-sm-6">
                  <h5><?php echo $translator->translate('This and all the following events will be deleted') ?></h5>
              </div>
          </div>
          <div class="row">
               <div class="col-xs-6 col-sm-6">
                  <a href="" class=" btn btn-primary"  id="delete_event_link_all_repeat"  data-val="only" ><?php echo $translator->translate('All events in the series') ?></a>
              </div>
              <div class="col-xs-6 col-sm-6">
                  <h5><?php echo $translator->translate('All events in the series will be deleted.') ?></h5>
              </div>
          </div>
          </div>
      </div>
        
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $translator->translate('Cancel') ?></button>
      </div>
    </div>
  </div>
</div>

