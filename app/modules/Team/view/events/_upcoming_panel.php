<div class="panel panel-default">
    <div class="panel-heading">
        
        <h3>
            <i class="ico ico-panel-event ico-action"></i> 
            <?php echo $translator->translate('Events') ?>
        </h3>
        <div class="pull-right">
           
            <a class="upcoming-events-trigger" href=""><i class="ico ico-panel-event-list"></i></a>
             <i class="ico ico-action ico-panel-close"></i>
        </div>
    </div>
    <div class="panel-body  panel-full-body bg-green">

       
     <?php foreach ($eventsList as $dayEvents): ?>
                        <?php foreach ($dayEvents as $event): ?>
     <?php $layout->includePart(MODUL_DIR . '/Event/view/Default/_event_panel_row.php', array(
         'i' => $i++, 
         'event' => $event, 
         'attendanceList' => $attendanceList, 
         'team' => $team)) ?>
            
<?php endforeach; ?>
                       
    <?php endforeach; ?>



    </div>
</div>







