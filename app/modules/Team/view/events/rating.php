<?php Core\Layout::getInstance()->startSlot('team_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/Team/view/team/_team_menu.php', array('team' => $team)) ?>
<?php Core\Layout::getInstance()->endSlot('team_menu') ?>





<section class="content-header">
 <a class="all-event-link" href="<?php echo $router->link('team_event_list', array('team_id' => $team->getId())) ?>"><i class="ico ico-event-back"></i><?php echo $translator->translate('All Events') ?></a>
    
     <?php echo $layout->renderControllerAction('CR\Gamifications\GamificationModule:Progress:progressBar')->getContent() ?>
    
    <?php
    $layout->includePart(GLOBAL_DIR . '/templates/parts/_breadcrumb.php', array(
        'crumbs' => array(
            'Teams' => $router->link('teams_overview'),
            $team->getName() => $router->link('team_overview', array('id' => $team->getId())),
            'Events' => $router->link('team_event_list', array('team_id' => $team->getId())),
            $event->getName() => ''
)))
    ?>
</section>



<section class="content">
    <div class="row">
        <div class="col-md-8">
            <div class="event-detail">
                <div class="panel panel-default rating-panel">
                    <?php
                    $layout->includePart(MODUL_DIR . '/Team/view/events/_event_detail.php', array(
                        'event' => $event,
                        'team' => $team,
                        'existLineup' => $lineup,
                        'menuActive' => 'rating',
                    ))
                    ?>

                    <div class="row panel-timeline winner-<?php echo $matchResult['winner'] ?>">
                        <div class="team_row team_row_first_line">
                            <strong><?php echo $lineup->getFirstLineName() ?></strong>
                            <span id="first_line_goals"><?php echo $matchOverview->getFirstLineGoals() ?></span>
                        </div>
                        <div class="team_row_colon">:</div>
                        <div class="team_row team_row_second_line">
                            <span  id="second_line_goals"><?php echo $matchOverview->getSecondLineGoals() ?></span>
                            <strong><?php echo $lineup->getSecondLineName() ?></strong>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <table class="table table-rating">
                                <tbody id="first_line">

                                    <?php foreach ($firstLine as $lineupPlayer): ?>
                                        <?php $teamMember = $lineupPlayer->getTeamPlayer() ?>
                                        <tr>
                                            <td>
                                                <div class="user-block">
                                                    <?php if (null != $teamMember): ?>
                                                        <div class="round-50" style="background-image:url(<?php echo $teamMember->getMidPhoto() ?>)"></div>
                                                        <span class="username"><?php echo $lineupPlayer->getPlayerName() ?></span>
                                                        <span class="description"><?php echo $teamMember->getTeamRoleName() ?> <?php echo ($teamMember->getPlayerNumber() != null) ? '#' . $teamMember->getPlayerNumber() : '' ?></span>
                                                    <?php else: ?>
                                                        <span class="username"><?php echo $lineupPlayer->getPlayerName() ?></span>
                                                    <?php endif; ?>
                                                </div><!-- /.user-block -->
                                            </td>
                                            <td>
                                        
                                                    <?php if ((($lineupPlayer->getPlayerId() == $user->getId()) or ! in_array($user->getId(), $approvedVoters)) && $userTeamRole != 'ADMIN' ): ?> 
                                                        <div data-toggle="tooltip" data-placement="top" title="<?php echo $translator->translate('STARBOX_DESC_TEXT') ?>" class="starbox-fake" data-start-value="<?php echo $lineupPlayer->getAverageScore() ?>"> </div> 
                                                         <?php if ($lineupPlayer->getPlayerId() != $user->getId()): ?>
                                                            <?php echo count($scoreMap[$lineupPlayer->getPlayerId()]['voters']) ?> <?php echo $translator->translate('Ratings') ?>
                                                        <span class="rating-text"><?php echo count($scoreMap[$lineupPlayer->getPlayerId()]['voters']) ?> <?php echo $translator->translate('Ratings') ?></span>
                                                         <?php endif; ?>
                                                    <?php else: ?>
                                                        <div data-toggle="tooltip" data-placement="top" title="<?php echo $translator->translate('STARBOX_DESC_TEXT') ?>" class="starbox ghosting" data-start-value="<?php echo $lineupPlayer->getAverageScore() ?>" data-event-id="<?php echo $event->getId() ?>" data-event-date="<?php echo $event->getCurrentDate()->format('Y-m-d H:i') ?>" data-team-id="<?php echo $team->getId() ?>" data-match-id="<?php echo $lineup->getId() ?>" data-player-id="<?php echo $lineupPlayer->getPlayerId() ?>" data-teamplayer-id="<?php echo $lineupPlayer->getTeamPlayerId() ?>"> 

                                                        </div>
                                                        <?php if (count($scoreMap[$lineupPlayer->getPlayerId()]['voters']) > 0): ?>
                                                            <?php echo count($scoreMap[$lineupPlayer->getPlayerId()]['voters']) ?> <?php echo $translator->translate('Ratings') ?>
                                                        <?php endif; ?>

                                                    <?php endif; ?>

                                               
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>

                        <div class="col-sm-6"> 
                            <table class="table table-rating">
                                <tbody  id="second_line">
                                    <?php foreach ($secondLine as $lineupPlayer): ?>
                                        <?php $teamMember = $lineupPlayer->getTeamPlayer() ?>
                                        <tr>
                                            <td>
                                                <div class="user-block">
                                                    <?php if (null != $teamMember): ?>
                                                        <div class="round-50" style="background-image:url(<?php echo $teamMember->getMidPhoto() ?>)"></div>
                                                        <span class="username"><?php echo $lineupPlayer->getPlayerName() ?></span>
                                                        <span class="description"><?php echo $teamMember->getTeamRoleName() ?> <?php echo ($teamMember->getPlayerNumber() != null) ? '#' . $teamMember->getPlayerNumber() : '' ?></span>
                                                    <?php else: ?>
                                                        <span class="username"><?php echo $lineupPlayer->getPlayerName() ?></span>
                                                    <?php endif; ?>
                                                </div><!-- /.user-block -->

                                            </td>
                                            <td>
                                               
                                                    <?php if ((($lineupPlayer->getPlayerId() == $user->getId()) or ! in_array($user->getId(), $approvedVoters)) && $userTeamRole != 'ADMIN'): ?> 
                                                        <div data-toggle="tooltip" data-placement="top" title="<?php echo $translator->translate('STARBOX_DESC_TEXT') ?>" class="starbox-fake" data-start-value="<?php echo $lineupPlayer->getAverageScore() ?>"> </div> 
                                                        <?php if ($lineupPlayer->getPlayerId() != $user->getId()): ?>
                                                        <span class="rating-text"><?php echo count($scoreMap[$lineupPlayer->getPlayerId()]['voters']) ?> <?php echo $translator->translate('Ratings') ?></span>
                                                        <?php endif; ?>
                                                    <?php else: ?>
                                                        <div data-toggle="tooltip" data-placement="top" title="<?php echo $translator->translate('STARBOX_DESC_TEXT') ?>" class="starbox ghosting" data-start-value="<?php echo $lineupPlayer->getAverageScore() ?>" data-event-id="<?php echo $event->getId() ?>" data-event-date="<?php echo $event->getCurrentDate()->format('Y-m-d H:i') ?>" data-team-id="<?php echo $team->getId() ?>" data-match-id="<?php echo $lineup->getId() ?>" data-player-id="<?php echo $lineupPlayer->getPlayerId() ?>"  data-teamplayer-id="<?php echo $lineupPlayer->getTeamPlayerId() ?>"> 
                                                        </div>
                                                        <?php if (count($scoreMap[$lineupPlayer->getPlayerId()]['voters']) > 0): ?>
                                                            <?php echo count($scoreMap[$lineupPlayer->getPlayerId()]['voters']) ?> <?php echo $translator->translate('Ratings') ?>
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                               
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>

                        <table class="table table-striped table-rating table-match-stat">

                            <thead>
                                <tr>
                                    <th class="player"></th>
                                    <th class="goal">G</th>
                                    <th  class="assist">A</th>
                                    <th class="mom">MoM (<?php echo $translator->translate('Most valuable player') ?>)</th>
                                </tr>
                            </thead>   

                            <tbody>

                                <?php foreach ($matchStats as $lineupPlayerId => $playerStat): ?>
                                    <?php $lineupPlayer = $players[$lineupPlayerId] ?>
                                    <?php $teamMember = $lineupPlayer->getTeamPlayer() ?>
                                    <tr>
                                        <td>
                                            <div class="user-block">
                                                <?php if (null != $teamMember): ?>
                                                    <div class="round-50" style="background-image:url(<?php echo $teamMember->getMidPhoto() ?>)"></div>
                                                    <span class="username"><?php echo $lineupPlayer->getPlayerName() ?></span>                          
                                                <?php else: ?>
                                                    <span class="username"><?php echo $lineupPlayer->getPlayerName() ?></span>
                                                <?php endif; ?>
                                            </div><!-- /.user-block -->
                                        </td>
                                        <td><?php echo $playerStat->getGoals() ?></td>
                                        <td><?php echo $playerStat->getAssists() ?></td>
                                        <td><?php echo (1 == $playerStat->getManOfMatch()) ? 'M' : '' ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>


                        <ul class="timeline" id="stat_timeline">
                            <li class="start-label"><?php echo $translator->translate('Timeline') ?></li>
                            <?php foreach ($groupedEventTimeline as $groupId => $group): ?>
                                <?php
                                $layout->includePart(MODUL_DIR . '/Team/view/teamMatch/_timeline_event_row.php', array(
                                    'group' => $group,
                                    'lineup' => $lineup,
                                    'event' => $event,
                                    'players' => $players,
                                    'groupId' => $groupId
                                ))
                                ?>
                            <?php endforeach; ?>
                        </ul>
                    </div>








                </div>
            </div>
            
            
               <div class="row">
                <div class="col-sm-3 col-md-offset-4">

               
                    <a  href="<?php echo $router->link('team_event_list',array('team_id' => $team->getId(),'type' => 'upcoming')) ?>" style="display:none;" class="btn btn-primary pull-right btn_continue"><?php echo $translator->translate('Continue') ?></a>&nbsp;
                     
             
                </div>
            </div>
            
        </div>
        <div class="col-md-4">
            <?php $layout->includePart(MODUL_DIR . '/Event/view/widget/_small_calendar.php') ?>
        </div>
    </div>
</section>



<!-- Modal -->
<div class="modal fade" id="notification-modal" tabindex="-1" role="dialog" aria-labelledby="<?php echo $translator->translate('Send notification') ?>">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-loader">
                <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                <span><?php echo $translator->translate('Sending...') ?></span>
            </div>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="<?php echo $translator->translate('Close') ?>"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?php echo $translator->translate('Send notification') ?></h4>
            </div>
            <div class="modal-body">

                <div class="alert alert-success send-success">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                </div>

                <form action="<?php echo $router->link('team_event_notify', array('event_id' => $event->getId(), 'event_date' => $event->getCurrentDate()->format('Y-m-d'))) ?>" id="notification-form">

                    <div class="form-group">
                        <textarea id="notification-message" class="form-control" name="message" rows="3" placeholder="<?php echo $translator->translate('Message text') ?>"></textarea>
                    </div>

                    <div class="row well well-sm notify-recip-select">
                        <h3 class="text-center"><?php echo $translator->translate('Sent to:') ?></h3>
                        <div class="col-sm-4">
                            <input class="recip_select" type="radio" name="recip_select" value="all" /><?php echo $translator->translate('RECIP_ALL') ?>
                        </div>
                        <div class="col-sm-4">
                            <input class="recip_select" type="radio" name="recip_select" value="player" /><?php echo $translator->translate('RECIP_PLAYERS') ?>
                        </div>
                        <div class="col-sm-4">
                            <input class="recip_select"  type="radio" name="recip_select" value="admin" /><?php echo $translator->translate('RECIP_ADMIN') ?>
                        </div>
                        <div class="col-sm-4">
                            <input class="recip_select"  type="radio" name="recip_select" value="attend3" /><?php echo $translator->translate('RECIP_ATTEND_NO') ?>
                        </div>
                        <div class="col-sm-4">
                            <input class="recip_select"  type="radio" name="recip_select" value="attend1" /><?php echo $translator->translate('RECIP_ATTEND_YES') ?>
                        </div>
                        <div class="col-sm-4">
                            <input class="recip_select"  type="radio" name="recip_select" value="attend" /><?php echo $translator->translate('RECIP_ATTEND_UNKN') ?>
                        </div>
                    </div>

                    <div class="row">
                        <?php foreach ($teamMembersList as $statusGroup): ?>
                            <?php foreach ($statusGroup as $orderGroup): ?>
                                <?php foreach ($orderGroup as $teamMembers): ?>
                                    <?php foreach ($teamMembers as $teamMember): ?>
                                        <div class="col-sm-4">
                                            <input class="recipient-<?php echo strtolower($teamMember->getTeamRole()) ?> recipient-all recipient-attend<?php echo $teamMember->getEventAttendanceStatus($event->getUid()) ?>" type="checkbox" name="recipients[]" value="<?php echo $teamMember->getId() ?>" />
                                            <span class="username"><?php echo $teamMember->getFullName() ?></span>
                                        </div>

                                    <?php endforeach; ?>
                                <?php endforeach; ?>
                            <?php endforeach; ?>
                        <?php endforeach; ?>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $translator->translate('Cancel') ?></button>
                <button type="button" class="btn btn-primary notification-send-trigger"><?php echo $translator->translate('Send') ?></button>
            </div>
        </div>
    </div>
</div>

<?php $layout->includePart(MODUL_DIR . '/Team/view/events/_confirm_single_delete_modal.php', array('event' => $event, 'eventTimeline' => $eventTimeline)) ?>
<?php $layout->addJavascript('js/TeamEventManager.js') ?>
<?php $layout->addJavascript('js/WidgetManager.js') ?>
<?php $layout->addJavascript('plugins/starbox/jstarbox.js') ?>
<?php $layout->addJavascript('js/SmallCalendar.js') ?>

<?php Core\Layout::getInstance()->startSlot('stylesheet') ?>  
<link rel="stylesheet" href="/dev/plugins/starbox/css/jstarbox.css">
<?php Core\Layout::getInstance()->endSlot('stylesheet') ?>

<?php $layout->startSlot('javascript') ?>
<?php echo Core\ServiceLayer::getService('AchievementAlertManager')->showPlaceAchievementAlert('event_detail_rating','event_rating_create',Core\ServiceLayer::getService('security')->getIdentity()) ?>

<?php echo Core\ServiceLayer::getService('AchievementAlertManager')->showPlaceAchievementAlert('event_detail_rating','step2_final',Core\ServiceLayer::getService('security')->getIdentity()) ?>

<?php if ($request->hasFlashMessage('final_step3')): ?>
<?php $request->getFlashMessage('final_step3') ?>

<?php echo Core\ServiceLayer::getService('AchievementAlertManager')->showPlaceAchievementAlert('event_detail_rating','step3_final',Core\ServiceLayer::getService('security')->getIdentity(),true) ?>
<?php endif; ?>






<script type="text/javascript">


    $('.starbox').each(function (index, val) {
        $(val).starbox({
            average: $(val).attr('data-start-value'),
            ghosting: true
        }).bind('starbox-value-changed', function (event, value) {

            player_id = $(this).attr('data-player-id');
            team_player_id = $(this).attr('data-teamplayer-id');
            match_id = $(this).attr('data-match-id');
            event_id = $(this).attr('data-event-id');
            event_date = $(this).attr('data-event-date');
            team_id = $(this).attr('data-team-id');
            current_starbox = this;
            $.ajax({
                method: "GET",
                url: '<?php echo $router->link('add_player_rating') ?>',
                data: {rating: value, player_id: player_id, match_id: match_id, event_id: event_id, event_date: event_date, team_id: team_id,team_player_id:team_player_id},
                dataType: 'json'
            }).done(function (result) {
                $(current_starbox).starbox('setOption', 'average', result.rating);
                $('.btn_continue').fadeIn();
            });
        });
    });


    $('.starbox-fake').each(function (index, val) {
        $(val).starbox({
            average: $(this).attr('data-start-value'),
            changeable: false
        })
    });





    $('.map-trigger').on('click', function () {
        $('#event_detail_map').css('position', 'relative');
        $('#event_detail_map').css('top', '0');
    });


    var event_manger = new TeamEventsManager();
    event_manger.init();

    function showEventLocation()
    {
        event_manger.google = google;
    }

    small_calendar = new SmallCalendar({
        'dataUrl': '<?php echo $router->link('rest_event_get_month_grid') ?>',
        'localityManager': new LocalityManager()
    });
    small_calendar.initLoad();

</script>


<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXU1duK54f1gIsRy_xc7z4uQURovsZdE8&libraries=places&callback=showEventLocation" async defer></script>


<?php $layout->endSlot('javascript') ?>


