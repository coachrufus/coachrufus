



                    <div class="panel-body  panel-full-body event-detail-webview">
                        <h1><?php echo $event->getName() ?>, <?php echo $event->getStart()->format('H:i'); ?></h1>
                        <span class="mute"><?php echo $team->getName() ?>  (<?php echo $event->getSportName() ?>,<?php echo $event->getTypename() ?> ), <?php echo $event->getSeasonName() ?></span>
                       
                        
                         <?php if(null != $event->getPlayground()): ?>
                            <h4 id='event-playground-name'><?php echo $event->getPlayground()->getName() ?></h4>
                        <?php endif; ?>

                            <div class="event-detail-menu">

                                <a class="<?php echo ($menuActive == 'attendance') ? 'active' : '' ?> event-detail-menu-item" href="<?php echo $router->link('team_event_detail', array('id' => $event->getId(), 'current_date' => $event->getCurrentDate()->format('Y-m-d'))) ?>"><?php echo $translator->translate('Attendance') ?></a>

                                  <a class="event-detail-menu-item"  data-action="popup-full" data-popup-onclose-action="refresh" href="<?php echo WEB_DOMAIN.$router->generateApiWidgetUrl('api_content_games_lineup_widget',array(
                                      'apikey' => CR_API_KEY,
                                      'user_id' => $user->getId(),
                                      'event_id' => $event->getId(),
                                      'event_date' => $event->getCurrentDate()->format('Y-m-d'))) ?>"><?php echo $translator->translate('Lineup') ?></a>  

                                <a name="create_lineup"></a>
                                
                                <?php if($event->getLineup() != null && $event->getStatRoute() == 'team_match_create_live_stat'): ?>
                                <a class="tool-item mpopup" data-action="popup-full" data-popup-onclose-action="refresh" href="<?php echo WEB_DOMAIN.$router->generateApiWidgetUrl('api_content_games_score_widget',array('apikey' => CR_API_KEY,'user_id' =>  $user->getId(),'event_id' => $event->getId(),'lineup_id' => $event->getLineup()->getId() ,'event_date' => $event->getCurrentDate()->format('Y-m-d'))) ?>"><i class="ico ico-edit"></i> <?php echo $translator->translate('widget.event.sidebar.addScore') ?></a>  

                            <?php endif; ?>

  
                            </div>
                    </div>


