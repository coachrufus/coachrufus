BEGIN:VCALENDAR
VERSION:2.0
PRODID:-//Coach Rufus//drozdi//EN

<?php foreach($events as $event): ?>
BEGIN:VEVENT
UID:<?php echo $event->getId()."\n" ?>
DTSTAMP:<?php echo $dateTime->toString('Ymd\THis') . "Z\n" ?>
DTSTART:<?php echo $dateTime->toString('Ymd\THis') . "Z\n" ?>
DURATION:PT2H0M0S
SUMMARY:<?php echo $event->getName()."\n" ?>
LOCATION:<?php echo $event->getPlaygroundName()."\n" ?>
END:VEVENT
<?php endforeach; ?>
END:VCALENDAR