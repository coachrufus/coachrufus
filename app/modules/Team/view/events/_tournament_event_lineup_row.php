 <div class="attendant-row attendant-row-<?php echo $teamMember->getId() ?>">
                                    <span class="ord-num">
                                        <?php echo $onum ?>
                                    </span>
                                    <div class="user-block pull-left">
                                        <div class="round-50" style="background-image:url(<?php echo $teamMember->getMidPhoto() ?>)"></div>
                                        <span class="username"><?php echo $teamMember->getFullName() ?> [<?php echo $teamMember->getPlayerNumber() ?>]</span>
                                        <span class="description"><?php echo $teamMember->getTeamRoleName() ?> <?php echo ($teamMember->getPlayerNumber() != null) ? '#' . $teamMember->getPlayerNumber() : '' ?></span>
                                    </div><!-- /.user-block -->


                                    <div class="pull-right">

                                       

                                            
 
                                        <?php if(array_key_exists($teamMember->getId(), $lineupPlayersIds)): ?>
                                        
                                         <a href="#" 
                                            class="in attendance_trigger attendance_trigger_btn accept">
                                            <i class="ico ico-check"></i>
                                         </a>
                                        
                                        
                                         <a href="#"  
                                            data-url="#" 
                                            data-rel="<?php echo $lineupPlayersIds[$teamMember->getId()]  ?>" 
                                            class="out attendance_trigger attendance_trigger_btn remove_player tournament-remove-lineup-player"  >
                                             <i class="ico ico-close"></i>
                                         </a>
                                        <?php else: ?>
                                             <a 
                                                href="#"
                                                data-target="<?php echo $lineupCode ?>" 
                                                data-playerid="<?php echo $teamMember->getPlayerId() ?>" 
                                                data-name="<?php echo $teamMember->getFullName() ?>" 
                                                data-lineup="<?php echo $lineup->getId() ?>" 
                                                data-eff="0" 
                                                class="add-tournament-lineup-player  in attendance_trigger attendance_trigger_btn <?php echo (array_key_exists($teamMember->getId(), $lineupPlayersIds)) ? 'accept' : '' ?>" 
                                                data-team-player-id="<?php echo $teamMember->getId() ?>" 
                                                data-icon="" 
                                                data-team-role="<?php echo $teamMember->getTeamRole() ?>" 
                                                data-team-number="<?php echo $teamMember->getPlayerNumber() ?>">
                                                <i class="ico ico-check"></i>
                                            </a>
                                        
                                            <a href="#"  
                                                data-url="#" 
                                                class="out attendance_trigger attendance_trigger_btn deny"  >
                                                 <i class="ico ico-close"></i>
                                             </a>
                                        
                                        <?php endif; ?>

                                      



                                    </div>



                                </div>