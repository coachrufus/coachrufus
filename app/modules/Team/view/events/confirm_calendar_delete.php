
          <?php if($event->getPeriod() == 'none'): ?>
           <div class="alert alert-warning"><?php echo $translator->translate('Would you like to delete this event?') ?></div>
           
            <a href="<?php echo $router->link('delete_team_event', array('current_date' => $event->getCurrentDate()->format('Y-m-d'),'event_id' => $event->getId(),'t' => 'all')) ?>" class=" btn btn-primary" data-val="all" class="btn btn-primary"><?php echo $translator->translate('Delete') ?></a>
          <?php else: ?>
          
          
         
          
          
          <div class="alert alert-warning"><?php echo $translator->translate('Would you like to delete this event only, all events in the series or this and all following events in the series?') ?></div>
          <div class="row">
              
              <div class="col-sm-7">
                  <h4> <?php echo $translator->translate('All other events in the series will be retained') ?></h4>
              </div>
              <div class="col-sm-5">
                  <a href="<?php echo $router->link('delete_team_event', array('current_date' => $event->getCurrentDate()->format('Y-m-d'),'event_id' => $event->getId(),'t' => 'only')) ?>" class=" btn btn-primary"  data-val="only" ><?php echo $translator->translate('Only this') ?></a>
              </div>
          </div>
          <div class="row">
              
              <div class="col-sm-7">
                  <h4> <?php echo $translator->translate('This and all the following events will be deleted') ?></h4>
                  
                 
              </div>
              <div class="col-sm-5">
                  <a href="<?php echo $router->link('delete_team_event', array('current_date' => $event->getCurrentDate()->format('Y-m-d'),'event_id' => $event->getId(),'t' => 'next')) ?>"  class=" btn btn-primary" data-val="next" class="btn btn-primary"><?php echo $translator->translate('All the following') ?></a>
              </div>
           </div>
          <div class="row">
              
              <div class="col-sm-7">
                  <h4> <?php echo $translator->translate('All events in the series will be deleted.') ?></h4>
              </div>
              <div class="col-sm-5">
                  <a href="<?php echo $router->link('delete_team_event', array('current_date' => $event->getCurrentDate()->format('Y-m-d'),'event_id' => $event->getId(),'t' => 'all')) ?>" class=" btn btn-primary" data-val="all" class="btn btn-primary"><?php echo $translator->translate('All events in the series') ?></a>
              </div>
          </div>
          
          <?php if(count($timelineDates) > 0): ?>
           <div class="alert alert-warning">
              <?php echo $translator->translate('NO_DELETE_STAT_TEXT') ?>
              
              <ul>
                  <?php foreach($timelineDates as $timelineDate): ?>
                  <li><?php echo $timelineDate  ?></li>
                  <?php endforeach; ?>
              </ul>
                
          </div>

            <?php endif; ?>
          
         
          

      </div>
         <?php endif ?>
     