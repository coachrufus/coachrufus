<div class="modal fade" id="confirm-calendar-delete-modal" tabindex="-1" role="dialog" aria-labelledby="<?php echo $translator->translate('Confirm delete') ?>">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $translator->translate('Delete  event') ?></h4>
      </div>
      <div class="modal-body">
          
          
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $translator->translate('Cancel') ?></button>
      </div>
    </div>
  </div>
</div>

