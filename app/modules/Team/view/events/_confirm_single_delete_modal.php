<div class="modal fade" id="confirm-single-delete-modal" tabindex="-1" role="dialog" aria-labelledby="<?php echo $translator->translate('Confirm delete') ?>">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $translator->translate('Delete  event') ?></h4>
      </div>
      <div class="modal-body">
          
          <h2><?php echo $translator->translate('Would you like to delete this event?') ?></h2>
           
           <div class="alert alert-warning center">
                    <h2> <?php echo $translator->translate('WARNING') ?></h2>
                    <?php echo $translator->translate('STATISTIC_DELETE_WARNING_TEXT') ?>
               </div>
          
          
           <a href="<?php echo $router->link('delete_team_event', array('current_date' => $event->getCurrentDate()->format('Y-m-d'),'event_id' => $event->getId(),'t' => 'single-with-stat')) ?>" class=" btn btn-primary" data-val="single-with-stat" class="btn btn-primary"><?php echo $translator->translate('Delete') ?></a>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $translator->translate('Cancel') ?></button>
      </div>
    </div>
  </div>
</div>

