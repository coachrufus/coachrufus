<?php Core\Layout::getInstance()->startSlot('team_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/Team/view/team/_team_menu.php', array('team' => $team)) ?>
<?php Core\Layout::getInstance()->endSlot('team_menu') ?>

<section class="content-header">
    
    <a class="all-event-link" href="<?php echo $router->link('team_event_list',array('team_id' => $team->getId())) ?>"><i class="ico ico-event-back"></i><?php echo $translator->translate('All Events') ?></a>
     <?php $layout->includePart(GLOBAL_DIR.'/templates/parts/_breadcrumb.php',array(
        'crumbs' => array(
            'Teams' => $router->link('teams_overview'),
            $team->getName() => $router->link('team_overview',array('id' =>$team->getId() )),
            'Events' => $router->link('team_event_list', array('team_id' => $team->getId())),
            'New' => ''
            ))) ?>
</section>


 
<section class="content">
    <div class="row">
        <div class="col-md-8">
            <div class="event-detail">
                
                    <div class="panel panel-default">
                    
                        <?php $layout->includePart(MODUL_DIR . '/Team/view/events/_event_detail.php', 
                                array(
                                    'event' => $event, 
                                    'team' => $team, 
                                    'eventTimeline' => $eventTimeline, 
                                    'eventSeason' => $eventSeason ,
                                    'existLineup' => $existLineup,
                                    'menuActive' => 'attendance',
                                    'addStatRoute' => $addStatRoute,
                                )) ?>
                        
                        
                        
                        <div class="event-attendance">
                            <div class="members-attendance members-attendance-<?php echo $event->getUid()  ?>">
                                <span style="left: <?php echo round($event->getAttendanceInSum()/$event->getCapacity()*100) ?>%; <?php echo (0 == $event->getAttendanceInSum()) ? 'margin-left:0' : '' ?>" class="members-attendance-text"><span class="members-attendance-in"><?php echo $event->getAttendanceInSum() ?></span>/<?php echo $event->getCapacity() ?></span>
                              <div class="progress progress-xs">
                                <div class="progress-bar progress-bar-success progress-bar-green" role="progressbar" aria-valuenow="<?php echo $event->getAttendanceInSum() ?>" aria-valuemin="0" aria-valuemax="<?php echo $event->getCapacity() ?>" style="width:  <?php echo round($event->getAttendanceInSum()/$event->getCapacity()*100) ?>%">
                                </div>
                              </div>
                          </div>
                            
                            <div class="row">
                            <div class="col-sm-6 attendance-group">
                                <h2><?php echo $translator->translate('Players') ?> (<?php echo $atttendanceSummary['accept_players'] ?>)</h2>
                                
                                <?php  $onum = 1;foreach($teamMembersList['accept'][0] as $teamMembers): ?>
                                <?php foreach($teamMembers as $teamMember): ?>
                                     <?php $layout->includePart(MODUL_DIR . '/Team/view/events/_attendant_row_closed.php', array(
                                         'event' => $event,
                                         'teamMember' => $teamMember, 
                                         'team' => $team,
                                          'user' => $user
                                       )) ?>
                                <?php $onum++; endforeach; ?>
                                <?php endforeach; ?>
                                
                                
                            </div>
                            <div class="col-sm-6 attendance-group">
                                <h2><?php echo $translator->translate('Guest') ?> (<?php echo $atttendanceSummary['accept_guest'] ?>)</h2>
                                <?php  foreach($teamMembersList['accept'][1] as $teamMembers): ?>
                                <?php foreach($teamMembers as $teamMember): ?>
                                     <?php $layout->includePart(MODUL_DIR . '/Team/view/events/_attendant_row_closed.php', array(
                                         'event' => $event,
                                         'teamMember' => $teamMember, 
                                         'team' => $team,
                                          'user' => $user
                                       )) ?>
                                <?php $onum++; endforeach; ?>
                                <?php endforeach; ?>
                            </div>
                            
                            <div class="col-sm-12 attendance-group">
                                <h2><?php echo $translator->translate('Team Members') ?> (<?php echo $atttendanceSummary['denny']+$atttendanceSummary['unknown'] ?>)</h2>
                                <?php  foreach($teamMembersList['denny'][0] as $teamMembers): ?>
                                <?php foreach($teamMembers as $teamMember): ?>
                                     <?php $layout->includePart(MODUL_DIR . '/Team/view/events/_attendant_row_closed.php', array(
                                         'event' => $event,
                                         'teamMember' => $teamMember, 
                                         'team' => $team,
                                          'user' => $user
                                       )) ?>
                                <?php endforeach; ?>
                                <?php endforeach; ?>
                                <?php  foreach($teamMembersList['unknown'][0] as $teamMembers): ?>
                                <?php foreach($teamMembers as $teamMember): ?>
                                     <?php $layout->includePart(MODUL_DIR . '/Team/view/events/_attendant_row_closed.php', array(
                                         'event' => $event,
                                         'teamMember' => $teamMember, 
                                         'team' => $team,
                                          'user' => $user
                                       )) ?>
                                <?php endforeach; ?>
                                <?php endforeach; ?>
                            </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <?php $layout->includePart(MODUL_DIR . '/Event/view/widget/_small_calendar.php') ?>
        </div>
    </div>
</section>




 <?php $layout->includePart(MODUL_DIR . '/Team/view/events/_event_notify_modal.php', array('event' => $event,'teamMembersList' => $teamMembersList)) ?>
<?php $layout->includePart(MODUL_DIR . '/Team/view/events/_confirm_single_delete_modal.php', array('event' => $event,'eventTimeline' => $eventTimeline)) ?>

<?php $layout->addJavascript('js/TeamEventManager.js') ?>
<?php $layout->addJavascript('js/SmallCalendar.js') ?>




<?php  $layout->startSlot('javascript') ?>
 <script type="text/javascript">
     

var event_manger = new TeamEventsManager();
event_manger.init();
function showEventLocation()
    {
        event_manger.google = google;
    }

 var small_calendar = new SmallCalendar({
        'dataUrl': '<?php echo $router->link('rest_event_get_month_grid') ?>',
        'localityManager': new LocalityManager()
    });
    small_calendar.initLoad();


</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXU1duK54f1gIsRy_xc7z4uQURovsZdE8&libraries=places&callback=showEventLocation" async defer></script>


<?php  $layout->endSlot('javascript') ?>


