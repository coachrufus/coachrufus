<?php Core\Layout::getInstance()->startSlot('team_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/Team/view/team/_team_menu.php', array('team' => $team)) ?>
<?php Core\Layout::getInstance()->endSlot('team_menu') ?>

<?php Core\Layout::getInstance()->startSlot('stylesheet') ?>  
<link rel="stylesheet" href="/dev/plugins/starbox/css/jstarbox.css">
<?php Core\Layout::getInstance()->endSlot('stylesheet') ?>

<section class="content-header">
    <h1>
       <?php echo $event->getName() ?>
    </h1>
    
    
     <?php $layout->includePart(GLOBAL_DIR.'/templates/parts/_breadcrumb.php',array(
        'crumbs' => array(
            'Teams' => $router->link('teams_overview'),
            $team->getName() => $router->link('team_overview',array('id' =>$team->getId() )),
            'Events' => $router->link('team_event_list', array('team_id' => $team->getId())),
            'Detail' => ''
            ))) ?>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-8">
            <section class="box box-solid">
              
                <div class="box-body">
                    <h3><?php echo $event->getFormatedCurrentDate() ?> <?php echo $event->getStart()->format('H:i') ?></h3>
                      <?php if(null != $playground): ?>
                    <h4><?php echo $event->getPlaygroundName() ?></h4>
                    <div id="event_detail_map">
                        
                    </div>

 <?php endif; ?>

                    <div class="box box-primary">
                         <?php if( \Core\ServiceLayer::getService('security')->getIdentity()->hasTeamPermission('manageEvents',$team)): ?>
                        <div class="box-header with-border">
                            <div class="btn-group">
                                <a data-event-type="<?php echo $event->getPeriod() ?>" id="delete-single-event-trigger" href="<?php echo $router->link('delete_team_event', array('event_id' => $event->getId())) ?>" type="button" class="btn btn-default"><i class="fa fa-trash-o"></i></a>
                        
                            </div>
                        </div>
                       
                        <?php endif; ?>
                        
                         <div class="box-body">
                             
                              
                             
                             <div class="text-center row">
                                 <div class="col-sm-5"><h2><?php echo $lineup->getFirstLineName() ?></h2></div>
                                    <div class="col-sm-2"> 
                                        <h1> <?php echo  $matchOverview->getResult() ?></h1>
                                        
                                         <button class="view_match_overview btn btn-success" data-href="<?php echo $router->link('player_match_detail',array('l' => $matchOverview->getLineupId() )) ?>">
                                 <?php echo $translator->translate('View match history') ?>
                                 </button>
                                    </div>
                                 <div class="col-sm-5"><h2><?php echo $lineup->getSecondLineName() ?></h2></div>
                               
                                
                            </div>
   
                               
                             <br />
                             
                                 <div class="row">
        <div class="col-sm-6">
             <div class="box box-solid">  
             <table class="table table-bordered">
                <tbody id="first_line">
                   
                    
                    
                   <?php foreach($firstLine as $lineupPlayer): ?>
                    <tr>
                        <td><?php echo $lineupPlayer->getPlayerName()?></td>
                         <td>
                             <?php if($lineupPlayer->getPlayerId() != null && $lineupPlayer->getPlayerId() != 0): ?>


                             
                             <?php if((array_key_exists($lineupPlayer->getPlayerId(), $scoreMap)  && array_key_exists($user->getId(),  $scoreMap[$lineupPlayer->getPlayerId()]['voters'])) or  ($lineupPlayer->getPlayerId() == $user->getId()) or !in_array($user->getId(), $approvedVoters) ): ?>
                                    <div class="starbox-fake" data-start-value="<?php echo $lineupPlayer->getAverageScore() ?>"> </div> 
                                        
                                        <?php if($lineupPlayer->getPlayerId() != $user->getId() && in_array($user->getId(), $approvedVoters)): ?>

                                            <?php echo $translator->translate('Your vote:') ?> <?php echo $scoreMap[$lineupPlayer->getPlayerId()]['voters'][$user->getId()]*100 ?>%
                                        <?php endif; ?>
                             <?php else: ?>
                                 <div class="starbox ghosting" data-start-value="<?php echo $lineupPlayer->getAverageScore() ?>" data-event-id="<?php echo $event->getId()  ?>" data-event-date="<?php echo $event->getCurrentDate()->format('Y-m-d H:i') ?>" data-team-id="<?php echo $team->getId() ?>" data-match-id="<?php echo $lineup->getId() ?>" data-player-id="<?php echo $lineupPlayer->getPlayerId()  ?>"> </div>
                             <?php endif; ?>
                                 
                             <?php endif; ?>
                            </td>
                    </tr>
                   <?php endforeach; ?>
                </tbody>
            </table>
             </div>
        </div>
      
        <div class="col-sm-6">
             <div class="box box-solid">
               
            <table class="table table-bordered">

                <tbody  id="second_line">
                   <?php foreach($secondLine as  $lineupPlayer): ?>
                    <tr>
                         <td><?php echo $lineupPlayer->getPlayerName() ?></td>
                         <td>
                              <?php if($lineupPlayer->getPlayerId() != null && $lineupPlayer->getPlayerId() != 0): ?> 
                             <?php if((array_key_exists($lineupPlayer->getPlayerId(), $scoreMap)  && array_key_exists($user->getId(),  $scoreMap[$lineupPlayer->getPlayerId()]['voters'])) or  $lineupPlayer->getPlayerId() == $user->getId() ): ?>
                             
                                    <div class="starbox-fake" data-start-value="<?php echo $lineupPlayer->getAverageScore() ?>"> </div> 
                                        <?php if($lineupPlayer->getPlayerId() != $user->getId()): ?>
                                            <?php echo $translator->translate('Your vote:') ?> <?php echo $scoreMap[$lineupPlayer->getPlayerId()]['voters'][$user->getId()]*100 ?>%
                                         <?php endif; ?>
                             <?php else: ?>
                                 <div class="starbox ghosting" data-start-value="<?php echo $lineupPlayer->getAverageScore() ?>" data-event-id="<?php echo $event->getId()  ?>" data-event-date="<?php echo $event->getCurrentDate()->format('Y-m-d H:i') ?>" data-team-id="<?php echo $team->getId() ?>" data-match-id="<?php echo $lineup->getId() ?>" data-player-id="<?php echo $lineupPlayer->getPlayerId()  ?>"> </div>
                             <?php endif; ?>
                              <?php endif; ?>
                         </td>
                    </tr>
                   <?php endforeach; ?>
                </tbody>
            </table>
             </div>
        </div>
        
    </div>
                         </div>
                            
                    </div>





                </div>
        </div>
        <div class="col-md-4">
           <?php $layout->includePart(MODUL_DIR . '/Team/view/events/_upcoming_panel.php', array('eventsList' => $eventsList,'team' => $team)) ?>
        </div>
</section>
</div>
</section>

<!-- Modal -->
<div class="modal fade" id="notification-modal" tabindex="-1" role="dialog" aria-labelledby="<?php echo $translator->translate('Send notification') ?>">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="<?php echo $translator->translate('Close') ?>"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $translator->translate('Send notification') ?></h4>
      </div>
      <div class="modal-body">
          <form action="<?php echo $router->link('team_event_notify',array('event_id' =>$event->getId() ,'event_date' => $event->getCurrentDate()->format('Y-m-d'))) ?>" id="notification-form">
              <div class="form-group">
                <textarea id="notification-message" class="form-control" rows="3" placeholder="<?php echo $translator->translate('Message text') ?>"></textarea>
              </div>
          </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $translator->translate('Cancel') ?></button>
        <button type="button" class="btn btn-primary notification-send-trigger"><?php echo $translator->translate('Send') ?></button>
      </div>
    </div>
  </div>
</div>

 <?php $layout->includePart(MODUL_DIR . '/Team/view/events/_confirm_single_delete_modal.php', array('event' => $event,'eventTimeline' => $eventTimeline)) ?>

<?php $layout->addJavascript('js/TeamEventManager.js') ?>
<?php  $layout->startSlot('javascript') ?>

<?php if(null != $playground): ?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXU1duK54f1gIsRy_xc7z4uQURovsZdE8&libraries=places&callback=showEventLocation" async defer></script>
<?php endif; ?>

 <script src="/dev/plugins/starbox/jstarbox.js"></script>
 
 <script type="text/javascript">
     
 $(function () {
     
        $('.view_match_overview').on('click',function(e){
            e.preventDefault();
            $('#match-overview-frame').attr('src',$(this).attr('data-href'));
            $('#match-overview-modal').modal('show');
        });
       
      });

    
$('.starbox').each(function(index,val){
     $(val).starbox({
        average: $(val).attr('data-start-value'),
        changeable: 'once',
        ghosting: true
    }).bind('starbox-value-changed', function(event, value) {

        player_id = $(this).attr('data-player-id');
        match_id = $(this).attr('data-match-id');
        event_id = $(this).attr('data-event-id');
        event_date = $(this).attr('data-event-date');
        team_id = $(this).attr('data-team-id');
        current_starbox = this;
         $.ajax({
                    method: "GET",
                    url: '<?php echo $router->link('add_player_rating') ?>',
                    data: {rating:value,player_id:player_id,match_id:match_id,event_id:event_id,event_date:event_date,team_id:team_id},
                    dataType: 'json'
                }).done(function(result){
                    $(current_starbox).starbox('setOption', 'average', result.rating);
                });
    });
});
     
     
$('.starbox-fake').each(function(index,val){
    $(val).starbox({
        average: $(this).attr('data-start-value'),
        changeable: false
    })
});



 <?php if(null != $locality): ?>    
function showEventLocation()
{
   
        var map = new google.maps.Map(document.getElementById('event_detail_map'), {
        scrollwheel: false,
        center: {lat: <?php echo $locality->getLat() ?>, lng: <?php echo $locality->getLng() ?>},
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });


    var marker = new this.google.maps.Marker({
        position:  {lat: <?php echo $locality->getLat() ?>, lng: <?php echo $locality->getLng() ?>},
        map: map
    });
    
}
<?php endif; ?>
event_manger = new TeamEventsManager();
event_manger.init();


</script>

<?php  $layout->endSlot('javascript') ?>

