<?php if($event->getClosed()): ?>
<a class="share-btn bottom" href="javascript:shareCallback('<?php $root ?>/widget/api/share-event/<?php echo $event->getId() ?>/<?php echo $event->getCurrentDate()->format('Y-m-d') ?>')">
	<i class="fa fa-facebook-square" aria-hidden="true"></i> <?php echo $translator->translate('Share') ?>
</a>
<?php endif; ?>

<div class="panel-heading">

<?php if($event->getClosed()): ?>
<a class="share-btn" href="javascript:shareCallback('<?php $root ?>/widget/api/share-event/<?php echo $event->getId() ?>/<?php echo $event->getCurrentDate()->format('Y-m-d') ?>')">
    <i class="fa fa-facebook-square" aria-hidden="true"></i> <?php echo $translator->translate('Share') ?>
</a>
<?php endif; ?>

                        <div class="panel-toolbar">

                            <?php if($event->getClosed() == false): ?>
                        <?php if( \Core\ServiceLayer::getService('security')->getIdentity()->hasTeamPermission('manageEvents',$team)): ?>
                            <?php if($menuActive == 'attendance'): ?>
                            <a href="<?php echo $router->link('team_event_notify',array('event_id' =>$event->getId() ,'event_date' => $event->getCurrentDate()->format('Y-m-d'))) ?>"  class="event-detail-notify">
                                <i class="ico ico-envelope"></i>
                            </a>
                           <?php endif; ?>
                            
                            


                            
                               <a class="tool-item" href="<?php echo $router->link('edit_team_event', array('event_id' => $event->getId(),'event_date' => $event->getCurrentDate()->format('Y-m-d'))) ?>"><i class="ico ico-edit"></i></a>  
                            
                        <a class="tool-item delete-event-trigger" data-event-type="<?php echo ($event->getPeriod() == null) ? 'none' : $event->getPeriod() ?>" id="<?php echo ('none' == $event->getPeriod() ) ? 'delete-single-event-trigger' : 'delete-calendar-event-trigger' ?>" href="<?php echo $router->link('delete_team_event', array('event_id' => $event->getId(), 'current_date' => $event->getCurrentDate()->format('Y-m-d'))) ?>"><i class="ico ico-trash"></i></a>
                        <?php endif; ?>
                        <?php endif; ?>

                         <div class="btn-group tool-item">
                            <a href="" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="ico ico-export"></i>
                            </a>
                            <ul class="dropdown-menu">
                              <li> <a class="popup-link" href="<?php echo $router->link('team_event_single_ical',array('id' => $event->getId(),'datetime' =>  $event->getCurrentDate()->format('Y-m-d').' '.$event->getStart()->format('H:i:s') )) ?>"><?php echo $translator->translate('Download .ics file') ?></a></li>
                              <li> <a class="popup-link" href="webcal://<?php echo WEB_DOMAIN_NAME ?><?php echo $router->link('team_event_single_ical',array('id' => $event->getId(),'datetime' =>  $event->getCurrentDate()->format('Y-m-d').' '.$event->getStart()->format('H:i:s') )) ?>">Outlook</a></li>
                              <li> <a class="popup-link" href="webcal://<?php echo WEB_DOMAIN_NAME ?><?php echo $router->link('team_event_single_ical',array('id' => $event->getId(),'datetime' =>  $event->getCurrentDate()->format('Y-m-d').' '.$event->getStart()->format('H:i:s') )) ?>">iCalc</a></li>
                              <li> <a class="popup-link" href="https://www.google.com/calendar/render?cid=webcal://<?php echo WEB_DOMAIN_NAME ?><?php echo urlencode($router->link('team_event_single_ical',array('id' => $event->getId(),'datetime' =>  $event->getCurrentDate()->format('Y-m-d').' '.$event->getStart()->format('H:i:s')) )) ?>"><?php echo $translator->translate('Google calendar') ?></a></li>
                            </ul>
                        </div>

                        </div>
                    </div>

                    <div class="panel-body  panel-full-body">
                        <h1><?php echo $event->getName() ?></h1>
                        <span class="mute"><?php echo $team->getName() ?>  (<?php echo $event->getSportName() ?>,<?php echo $event->getTypename() ?> ), <?php echo $event->getSeasonName() ?></span>
                        <div class="row">
                            <div class="col-sm-3 col-bord col-xs-6">
                                <i class="ico ico-event-cal"></i>
                                <strong><?php echo $event->getCurrentDate()->format('d') ?></strong>
                                <?php echo substr($translator->translate($event->getCurrentDate()->format('F')),0,3) ?> <?php echo $event->getCurrentDate()->format('Y') ?>
                            </div>
                            <div class="col-sm-3 col-bord col-xs-6">
                                <i class="ico ico-event-clock"></i>
                                <strong><?php echo $event->getStart()->format('H:i'); ?> </strong>
                               <?php echo $translator->translate($event->getCurrentDate()->format('l')) ?> 
                            </div>
                            <div class="col-sm-3 col-bord col-xs-6">
                                  <i class="ico ico-event-att"></i>
                                <div class="members-attendance  members-attendance-<?php echo $event->getUid()  ?>">
                                    <strong class="members-attendance-text"><span class="members-attendance-in"><?php echo $event->getAttendanceInSum() ?></span>/<?php echo $event->getCapacity() ?></strong>
                              </div>
                                <?php echo $translator->translate('Attendance') ?>
                            </div>
                            <div class="col-sm-3 col-xs-6">
                                <i class="ico ico-event-score"></i>
                                 <strong><?php echo $event->getMatchResult() ?></strong>
                                 <?php echo $translator->translate('Score') ?>
                                 
                                  <?php if( \Core\ServiceLayer::getService('security')->getIdentity()->hasTeamPermission('manageEvents',$team) &&  $event->getStatRoute() != null  && null !=  $existLineup && $event->getClosed() == false): ?>
                                        <a class="color-link"  href="<?php echo $router->link($event->getStatRoute(),array('event_id' => $event->getId(), 'eventdate' =>  $event->getCurrentDate()->format('Y-m-d'),'lid' =>$existLineup->getId()  )) ?>"><i class="ico ico-plus-green"></i> <?php echo $translator->translate('Score') ?>
                                                </a>
                                  <?php endif; ?>
                            </div>
                        </div>
                        
                         <?php if(null != $event->getPlayground()): ?>
                            <h4 id='event-playground-name'><?php echo $event->getPlayground()->getName() ?></h4>
                            <span class="playground-address"><?php echo $event->getPlaygroundAddress()?></span>
                            
                            <?php if(null != $event->getPlayground()->getLat()): ?>
                                <a class="btn map-trigger btn-clear btn-green btn"  href="#"><i class="ico ico-map-marker-green"></i> <?php echo $translator->translate('Map') ?><i class="ico ico-arr-down-green"></i></a>
                                <div  id="event_detail_map" data-lat=" <?php echo $event->getPlayground()->getLat() ?>" data-lng="<?php echo $event->getPlayground()->getLng() ?>"></div>
                            <?php endif; ?>
                        <?php endif; ?>

                            <div class="event-detail-menu">

                                <a class="<?php echo ($menuActive == 'attendance') ? 'active' : '' ?> event-detail-menu-item" href="<?php echo $router->link('team_event_detail', array('id' => $event->getId(), 'current_date' => $event->getCurrentDate()->format('Y-m-d'))) ?>"><?php echo $translator->translate('Attendance') ?></a>
                                    
                                 
                                <a name="create_lineup"></a>
                                    <?php if( \Core\ServiceLayer::getService('security')->getIdentity()->hasTeamPermission('manageEvents',$team) == false): ?>
                                        <?php if($existLineup != null): ?>
                                            <a class="event-detail-menu-item <?php echo ($menuActive == 'lineup') ? 'active' : '' ?>" href="<?php echo $router->link('team_match_view_lineup',array('id' => $existLineup->getId() )) ?>">
                                               <?php echo $translator->translate('Lineup') ?>
                                            </a>
                                         <?php else: ?>      
                                            <a class="event-detail-menu-item <?php echo ($menuActive == 'lineup') ? 'active' : '' ?>" data-toggle="tooltip" data-placement="top" title="<?php echo $translator->translate('Lineup is not yet  created') ?>" href="#">
                                               <?php echo $translator->translate('Lineup') ?>
                                            </a>
                                        <?php endif; ?>

                                    <?php else: ?>
                                        <?php if($existLineup != null): ?>
                                           <a class="event-detail-menu-item <?php echo ($menuActive == 'lineup') ? 'active' : '' ?>" href="<?php echo $router->link('team_match_edit_lineup',array('id' => $existLineup->getId() )) ?>">
                                               <?php echo $translator->translate('Lineup') ?>
                                           </a>
                                        <?php else: ?>                                 
                                            <a class="event-detail-menu-item <?php echo ($menuActive == 'lineup') ? 'active' : '' ?>" href="<?php echo $router->link('team_match_new_lineup',array('event_id'=>$event->getId(),'event_date' => $event->getCurrentDate()->format('Y-m-d'))) ?>">
                                                  <?php echo $translator->translate('Lineup') ?>
                                            </a>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                
                                
                                
                                
                                 <?php if( $event->getStatRoute() != null): ?>
        
                                
                                 <?php if( \Core\ServiceLayer::getService('security')->getIdentity()->hasTeamPermission('manageEvents',$team) == false): ?>
                                      <?php if(null  != $existLineup): ?>
                                         <a class="event-detail-menu-item <?php echo ($menuActive == 'score') ? 'active' : '' ?>" href="<?php echo $router->link('team_match_view_live_stat',array('event_id' => $event->getId(), 'eventdate' =>  $event->getCurrentDate()->format('Y-m-d'),'lid' =>$existLineup->getId()  )) ?>"><?php echo $translator->translate('Score') ?> </a>
                                      <?php else: ?>   
                                         <a class="event-detail-menu-item <?php echo ($menuActive == 'score') ? 'active' : '' ?>" href="#" data-toggle="tooltip" data-placement="top" title="<?php echo $translator->translate('Score not yet created') ?>"><?php echo $translator->translate('Score') ?> </a>
                                      <?php endif; ?>
                                
                                
                                 <?php else: ?>
                                    <?php if(null  != $existLineup): ?>
                                    <a class="event-detail-menu-item <?php echo ($menuActive == 'score') ? 'active' : '' ?>" href="<?php echo $router->link($event->getStatRoute(),array('event_id' => $event->getId(), 'eventdate' =>  $event->getCurrentDate()->format('Y-m-d'),'lid' =>$existLineup->getId()  )) ?>">
                                        <?php echo $translator->translate('Score') ?>
                                    </a>
                                   <?php elseif(null != $event->getStatRoute()): ?>
                                       <a class="event-detail-menu-item <?php echo ($menuActive == 'score') ? 'active' : '' ?>" href="<?php echo $router->link($event->getStatRoute(),array('event_id' => $event->getId(), 'eventdate' =>  $event->getCurrentDate()->format('Y-m-d'),'lid' =>null  )) ?>">
                                           <?php echo $translator->translate('Score') ?>
                                       </a>          
                                   <?php endif; ?>
                                <?php endif; ?>
                                
                              
                                 
                                <?php if($event->getStatRoute() == 'team_match_create_live_stat'): ?>
                                           <a class="event-detail-menu-item <?php echo ($menuActive == 'rating') ? 'active' : '' ?>" href="<?php echo $router->link('team_event_rating',array('id' => $event->getId(), 'current_date' => $event->getCurrentDate()->format('Y-m-d'))) ?>">
                                    <?php echo $translator->translate('Rating') ?>
                                </a>
                                <?php endif; ?>
                               
                              
                                <?php endif; ?>
                            </div>
                    </div>


