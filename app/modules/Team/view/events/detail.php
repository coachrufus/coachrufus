<?php Core\Layout::getInstance()->startSlot('team_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/Team/view/team/_team_menu.php', array('team' => $team)) ?>
<?php Core\Layout::getInstance()->endSlot('team_menu') ?>




<section class="content-header">
    <a class="all-event-link" href="<?php echo $router->link('team_event_list', array('team_id' => $team->getId())) ?>"><i class="ico ico-event-back"></i><?php echo $translator->translate('All Events') ?></a>
    
     <?php echo $layout->renderControllerAction('CR\Gamifications\GamificationModule:Progress:progressBar')->getContent() ?>
    
    <?php
    $layout->includePart(GLOBAL_DIR . '/templates/parts/_breadcrumb.php', array(
        'crumbs' => array(
            'Teams' => $router->link('teams_overview'),
            $team->getName() => $router->link('team_overview', array('id' => $team->getId())),
            'Events' => $router->link('team_event_list', array('team_id' => $team->getId())),
            $event->getName() => ''
)))
    ?>
</section>


 
<section class="content">
    <div class="row">
        <div class="col-md-8">
            <div class="event-detail">
                <?php if(null == $currentMember->getEventAttendanceStatus($event->getUid())): ?>
                 <div class="my-attendance">
                            <h3><?php echo $translator->translate('Are you going') ?> <?php echo \Core\ServiceLayer::getService('security')->getIdentity()->getUser()->getName() ?> ?</h3>
                           
                            
                            
                                <div class="trigger-cnt">
                                  <a  href="#"
                                                              data-url="<?php echo $router->link('change_player_attendance') ?>" 
                                                              data-rel="in" 
                                                              data-event="<?php echo $event->getId() ?>" 
                                                              data-eventdate="<?php echo  $event->getCurrentDate()->format('Y-m-d')  ?>" 
                                                              data-player="<?php echo $currentMember->getPlayerId()  ?>" 
                                                              data-team-role="<?php echo $currentMember->getTeamRole() ?>"
                                                              data-team-player="<?php echo $currentMember->getId()  ?>" class="in my_attendance_trigger attendance_trigger attendance_trigger_btn  <?php echo  ($currentMember->getEventAttendanceStatus($event->getUid()) == 1 ) ? 'accept' : '' ?>"><i class="ico ico-check"></i></a>


                                <div class="round-50" style="background-image:url(<?php echo $currentMember->getMidPhoto() ?>)"></div>



                                <a href=""  data-url="<?php echo $router->link('change_player_attendance') ?>" 
                                   data-rel="out" 
                                   data-event="<?php echo $event->getId() ?>" 
                                   data-eventdate="<?php echo  $event->getCurrentDate()->format('Y-m-d')  ?>" 
                                   data-player="<?php echo $currentMember->getPlayerId()  ?>" 
                                   data-team-role="<?php echo $currentMember->getTeamRole() ?>"
                                   data-team-player="<?php echo $currentMember->getId()  ?>" class="out my_attendance_trigger attendance_trigger attendance_trigger_btn  <?php echo ($currentMember->getEventAttendanceStatus($event->getUid()) == 3 ) ? 'deny' : '' ?>"><i class="ico ico-close"></i></a>

                                </div>   

                    </div>
                 <?php endif; ?>
                    
                    <div class="panel panel-default">
                        
                          <?php
                        $layout->includePart(MODUL_DIR . '/Team/view/events/_event_detail_webview.php', array(
                            'event' => $event,
                            'team' => $team,
                             'existLineup' => $existLineup,
                             'menuActive' => 'lineup',
                            'user' => $user
                        ))
                    ?>
                    
                         <div class="event-detail-part">
                        <?php $layout->includePart(MODUL_DIR . '/Team/view/events/_event_detail.php', 
                                array(
                                    'event' => $event, 
                                    'team' => $team, 
                                    'eventTimeline' => $eventTimeline, 
                                    'eventSeason' => $eventSeason ,
                                    'existLineup' => $existLineup,
                                    'menuActive' => 'attendance',
                                    'addStatRoute' => $addStatRoute,
                                )) ?>
                         </div>
                        
                       <a name="attendance"></a>
                        <div class="event-attendance" id="detail_event_attendance"> 
                            <div class="members-attendance members-attendance-<?php echo $event->getUid()  ?>">
                                <span style="left: <?php echo round($event->getAttendanceInSum()/$event->getCapacity()*100) ?>%; <?php echo (0 == $event->getAttendanceInSum()) ? 'margin-left:0' : '' ?>" class="members-attendance-text"><span class="members-attendance-in"><?php echo $event->getAttendanceInSum() ?></span>/<?php echo $event->getCapacity() ?></span>
                              <div class="progress progress-xs">
                                <div class="progress-bar progress-bar-success progress-bar-green" role="progressbar" aria-valuenow="<?php echo $event->getAttendanceInSum() ?>" aria-valuemin="0" aria-valuemax="<?php echo $event->getCapacity() ?>" style="width:  <?php echo round($event->getAttendanceInSum()/$event->getCapacity()*100) ?>%">
                                </div>
                              </div>
                          </div>
                            
                            <div class="row">
                            <div class="col-sm-6 attendance-group" id="attendance-group-in-players" data-event-limit="<?php echo $event->getCapacity()  ?>">
                                <h2><?php echo $translator->translate('Players') ?> (<span class="attendance_accept_players_count"><?php echo $atttendanceSummary['accept_players'] ?></span>)</h2>
                                
                                <?php  $onum = 1;foreach($teamMembersList['accept'][0] as $teamMembers): ?>
                                <?php foreach($teamMembers as $teamMember): ?>
                                     <?php $layout->includePart(MODUL_DIR . '/Team/view/events/_attendant_row.php', array(
                                         'event' => $event,
                                         'teamMember' => $teamMember, 
                                         'team' => $team,
                                          'user' => $user
                                       )) ?>
                                
                                <?php if($onum  == $event->getCapacity()): ?>
                                <div class="attendance-limit-divider"></div>
                                <?php endif; ?>
                                
                                <?php $onum++; endforeach; ?>
                                <?php endforeach; ?>
                                
                                
                            </div>
                            <div class="col-sm-6 attendance-group" id="attendance-group-in-guest">
                                <h2><?php echo $translator->translate('Guest') ?> (<span class="attendance_accept_guest_count"><?php echo $atttendanceSummary['accept_guest'] ?></span>)</h2>
                                <?php  foreach($teamMembersList['accept'][1] as $teamMembers): ?>
                                <?php foreach($teamMembers as $teamMember): ?>
                                     <?php $layout->includePart(MODUL_DIR . '/Team/view/events/_attendant_row.php', array(
                                         'event' => $event,
                                         'teamMember' => $teamMember, 
                                         'team' => $team,
                                          'user' => $user
                                       )) ?>
                                <?php $onum++; endforeach; ?>
                                <?php endforeach; ?>
                            </div>
                            
                            <div class="col-sm-12 attendance-group" id="attendance-group-out">
                                <h2><?php echo $translator->translate('Team Members') ?> (<span class="attendance_deny_count"><?php echo $atttendanceSummary['denny']+$atttendanceSummary['unknown'] ?></span>)</h2>
                                <?php  foreach($teamMembersList['denny'][0] as $teamMembers): ?>
                                <?php foreach($teamMembers as $teamMember): ?>
                                     <?php $layout->includePart(MODUL_DIR . '/Team/view/events/_attendant_row.php', array(
                                         'event' => $event,
                                         'teamMember' => $teamMember, 
                                         'team' => $team,
                                          'user' => $user
                                       )) ?>
                                <?php endforeach; ?>
                                <?php endforeach; ?>
                                <?php  foreach($teamMembersList['unknown'][0] as $teamMembers): ?>
                                <?php foreach($teamMembers as $teamMember): ?>
                                     <?php $layout->includePart(MODUL_DIR . '/Team/view/events/_attendant_row.php', array(
                                         'event' => $event,
                                         'teamMember' => $teamMember, 
                                         'team' => $team,
                                          'user' => $user
                                       )) ?>
                                <?php endforeach; ?>
                                <?php endforeach; ?>
                            </div>
                            </div>
                        </div>
                </div>
            </div>
            
            
            <div class="row">
                <div class="col-sm-3 col-md-offset-4">

                <?php if(!$finishedProgressStep): ?>
                    <a  href="<?php echo $router->link('team_match_new_lineup',array('event_id'=>$event->getId(),'event_date' => $event->getCurrentDate()->format('Y-m-d'))) ?>" style="display:none;" class="btn btn-primary pull-right btn_continue"><?php echo $translator->translate('Continue') ?></a>&nbsp;
                     
                <?php endif; ?>
                </div>
            </div>
            
            
        </div>
       <div class="col-md-4">
            <?php $layout->includePart(MODUL_DIR . '/Event/view/widget/_small_calendar.php') ?>
        </div>
    </div>
</section>




 <?php $layout->includePart(MODUL_DIR . '/Team/view/events/_event_notify_modal.php', array('event' => $event,'teamMembersList' => $teamMembersList)) ?>
<?php $layout->includePart(MODUL_DIR . '/Team/view/events/_confirm_single_delete_modal.php', array('event' => $event,'eventTimeline' => $eventTimeline)) ?>
<?php $layout->includePart(MODUL_DIR . '/Team/view/events/_confirm_delete_modal.php', array('event' => $event,'eventTimeline' => $eventTimeline)) ?>



<?php $layout->addJavascript('js/TeamEventManager.js') ?>
<?php $layout->addJavascript('js/WidgetManager.js') ?>
<?php $layout->addJavascript('js/SmallCalendar.js') ?>



<?php  $layout->startSlot('javascript') ?>
<?php echo Core\ServiceLayer::getService('AchievementAlertManager')->showPlaceAchievementAlert('event_detail_attendance','event_attendance_create',Core\ServiceLayer::getService('security')->getIdentity()) ?>


<?php if(!$finishedProgressStep): ?>
    <?php $layout->includePart(MODUL_DIR . '/Gamification/view/alert/event_attendance_confirm.php',array('event' => $event)) ?>
<?php endif; ?>

 <script type="text/javascript">
     
    
    var event_manger = new TeamEventsManager();
    event_manger.init();
   
    function showEventLocation()
    {
        event_manger.google = google;
    }

    var small_calendar = new SmallCalendar({
        'dataUrl': '<?php echo $router->link('rest_event_get_month_grid') ?>',
        'localityManager': new LocalityManager()
    });
    small_calendar.initLoad();


</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXU1duK54f1gIsRy_xc7z4uQURovsZdE8&libraries=places&callback=showEventLocation" async defer></script>


<?php  $layout->endSlot('javascript') ?>


