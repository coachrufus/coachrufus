<div class="modal fade" id="add-season-modal" tabindex="-1" role="dialog" aria-labelledby="<?php echo $translator->translate('Add season') ?>">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $translator->translate('Add season') ?></h4>
      </div>
      <div class="modal-body">
        <form role="form" id="add_season_form" action="<?php echo $router->link('team_add_season') ?>">
        <input type="hidden" name="team_id" value="<?php echo $team->getId() ?>" />

        

             

           
             <div class="form-group col-sm-12"  data-error-bind="season_unique">
                <label class="control-label"><?php echo $translator->translate($seasonForm->getFieldLabel("name")) ?><?php echo $seasonForm->getRequiredLabel('name') ?></label>
                <?php echo $seasonForm->renderinputTag("name", array('class' => 'form-control','size' => 10, 'id' => 'season_name')) ?>
            </div>
        <div class="col-sm12" data-error-bind="season_order"></div>
                <div class="form-group col-sm-6" data-error-bind="start_date">
                   <label class="control-label"><?php echo $translator->translate('start_date') ?><?php echo $seasonForm->getRequiredLabel('name') ?></label>

                   <div class="input-group">
                       <div class="input-group-addon">
                           <i class="fa fa fa-calendar"></i>
                       </div>
                        <?php echo $seasonForm->renderinputTag("start_date", array('class' => 'form-control','size' => 10,'id' => 'season_start_date')) ?>
                   </div>
               </div>
                   
                <div class="form-group col-sm-6" data-error-bind="end_date">
                   <label class="control-label"><?php echo $translator->translate('End date') ?><?php echo $seasonForm->getRequiredLabel('end_date') ?></label>

                   <div class="input-group">
                       <div class="input-group-addon">
                           <i class="fa fa fa-calendar"></i>
                       </div>
                        <?php echo $seasonForm->renderinputTag("end_date", array('class' => 'form-control','size' => 10,'id' => 'season_end_date')) ?>
                   </div>
               </div>
                   
                <div class="form-group col-sm-6">
                    <button type="submit" class="btn btn-primary add-season-submit"><?php echo $translator->translate('Submit') ?></button>
               </div>
                <div class="form-group col-sm-6">
                   <div id="error-alerts">* <?php echo $translator->translate('Please fill required fields') ?></div>  
               </div>
        
               

               
                </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $translator->translate('Cancel') ?></button>
      </div>
    </div>
  </div>
</div>

