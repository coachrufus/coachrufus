<?php Core\Layout::getInstance()->startSlot('team_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/Team/view/team/_team_menu.php', array('team' => $team)) ?>
<?php Core\Layout::getInstance()->endSlot('team_menu') ?>


<?php Core\Layout::getInstance()->startSlot('crumb') ?>
 <?php
    $layout->includePart(GLOBAL_DIR . '/templates/parts/_breadcrumb.php', array(
        'crumbs' => array(
            'Teams' => $router->link('teams_overview'),
            $team->getName() => $router->link('team_overview', array('id' => $team->getId())),
            'Events' => ''
)))
    ?>


<?php Core\Layout::getInstance()->endSlot('crumb') ?>
<section class="content-header">
        <a class="all-event-link content-header-back" href="<?php echo $router->link('team_overview',array('id' => $team->getId())) ?>"><i class="ico ico-event-back"></i><?php echo $translator->translate('Back to team') ?></a>
       <?php echo $layout->renderControllerAction('CR\Gamifications\GamificationModule:Progress:progressBar')->getContent() ?>
        
         <?php
    $layout->includePart(GLOBAL_DIR . '/templates/parts/_breadcrumb.php', array(
        'crumbs' => array(
            'Teams' => $router->link('teams_overview'),
            $team->getName() => $router->link('team_overview', array('id' => $team->getId())),
            'Events' => $router->link('team_event_list', array('team_id' => $team->getId()))
)))
    ?>
</section>


<section class="content">
    <div class="row">
        <div class="col-md-8">    
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3><i class="ico ico-panel-event ico-action"></i> <?php echo $translator->translate('Events') ?></h3>
                    <?php $layout->includePart(MODUL_DIR . '/Team/view/events/_panel.php', array('team' => $team)) ?>
                </div>
                <div class="panel-body  panel-full-body">
                    
                    <?php if (null != $lastEvent && $lastEvent->getCurrentDate()->format('Y-m-d') != date('Y-m-d') && $listType == 'upcoming' ): ?>
                        <?php $layout->includePart(MODUL_DIR . '/Event/view/Default/_last_event_list_row.php', array('lastEventScore' => $lastEventScore,'event' => $lastEvent, 'attendanceList' => $attendanceList, 'team' => $team)) ?>
                    <?php endif; ?>

                    <?php if (empty(array_filter($eventPagger->getEvents()))): ?>
                        <div class="alert alert-warning"><?php echo $translator->translate('No upcoming events in 30 days') ?></div>
                    <?php endif; ?>

                    <?php foreach ($eventPagger->getEvents() as $event): ?>
    <?php $layout->includePart(MODUL_DIR . '/Event/view/Default/_event_list_row.php', array('event' => $event, 'attendanceList' => $attendanceList, 'team' => $team)) ?>
                        <?php endforeach; ?>

                    <div class="event-list-pagging">    
                        <?php if (!empty(array_filter($eventPagger->getEvents()))): ?>
                        
                        
                            <?php if($displayPreview): ?>
                                <a class="btn btn-clear" href="<?php echo $router->link('team_event_list', array('team_id' => $team->getId(), 'calendar_date' => $calendar_date, 'to' => $eventPagger->getLastEventDate(), 'd' => 'past','type' => $listType,'lpd' => $limitPreviewDate)) ?>">
                                    <i class="ico ico-arr-lft"></i><?php echo $translator->translate('Preview') ?>
                                </a>
                            <?php endif; ?>
                        
                            <?php if($displayNext): ?>
                                <a class="btn btn-clear btn-clear-green pull-right" href="<?php echo $router->link('team_event_list', array('team_id' => $team->getId(), 'calendar_date' => $calendar_date, 'from' => $eventPagger->getLastEventDate(), 'd' => 'upcoming','type' => $listType,'lpd' => $limitPreviewDate)) ?>">
                                <?php echo $translator->translate('Next') ?> <i class="ico ico-arr-rgt-green"></i>
                                </a>
                            <?php endif; ?>
                       <?php elseif ($eventPagger->getEndDate() != null): ?>
                             <?php if($displayNext): ?>
                            <a class="btn btn-clear btn-clear-green pull-right" href="<?php echo $router->link('team_event_list', array('team_id' => $team->getId(), 'calendar_date' => $calendar_date, 'from' => $eventPagger->getEndDate()->format('Y-m-d-H-i'), 'd' => 'upcoming','type' => $listType,'lpd' => $limitPreviewDate)) ?>">
                            <?php echo $translator->translate('Next') ?> <i class="ico ico-arr-rgt-green"></i>
                            </a>
                             <?php endif; ?>
                        <?php endif; ?> 
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4"> 
            <?php $layout->includePart(MODUL_DIR . '/Event/view/widget/_small_calendar.php') ?>
        </div>
    </div>
</section>



<?php $layout->includePart(MODUL_DIR . '/Team/view/events/_confirm_delete_modal.php') ?>


<?php $layout->addJavascript('js/TeamEventManager.js') ?>
<?php $layout->addJavascript('js/SmallCalendar.js') ?>
<?php $layout->startSlot('javascript') ?>

<?php echo Core\ServiceLayer::getService('AchievementAlertManager')->showPlaceAchievementAlert('event_list','step1_final',Core\ServiceLayer::getService('security')->getIdentity()) ?>

<?php echo Core\ServiceLayer::getService('AchievementAlertManager')->showPlaceAchievementAlert('event_list','step2_final2',Core\ServiceLayer::getService('security')->getIdentity()) ?>

<script type="text/javascript">
    event_manger = new TeamEventsManager();
    event_manger.init();
    
    small_calendar = new SmallCalendar({
        'dataUrl': '<?php echo $router->link('rest_event_get_month_grid') ?>',
        'localityManager': new LocalityManager()
    });
    small_calendar.initLoad();


</script>
<?php $layout->endSlot('javascript') ?>