<!-- Modal -->
<div class="modal fade" id="notification-modal" tabindex="-1" role="dialog" aria-labelledby="<?php echo $translator->translate('Send notification') ?>">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-loader">
                <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                <span><?php echo $translator->translate('Sending...') ?></span>
            </div>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="<?php echo $translator->translate('Close') ?>"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?php echo $translator->translate('Send notification') ?></h4>
            </div>
            <div class="modal-body">

                <div class="alert alert-success send-success">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                </div>

                <form action="<?php echo $router->link('team_event_notify', array('event_id' => $event->getId(), 'event_date' => $event->getCurrentDate()->format('Y-m-d'))) ?>" id="notification-form">

                    <div class="form-group">
                        <textarea id="notification-message" class="form-control" name="message" rows="3" placeholder="<?php echo $translator->translate('Message text') ?>"></textarea>
                    </div>

                    <div class="row well well-sm notify-recip-select">
                        <h3 class="text-center"><?php echo $translator->translate('Sent to:') ?></h3>
                        <div class="col-sm-4">
                            <input class="recip_select" type="radio" name="recip_select" value="all" /><?php echo $translator->translate('RECIP_ALL') ?>
                        </div>
                        <div class="col-sm-4">
                            <input class="recip_select" type="radio" name="recip_select" value="player" /><?php echo $translator->translate('RECIP_PLAYERS') ?>
                        </div>
                        <div class="col-sm-4">
                            <input class="recip_select"  type="radio" name="recip_select" value="admin" /><?php echo $translator->translate('RECIP_ADMIN') ?>
                        </div>
                        <div class="col-sm-4">
                            <input class="recip_select"  type="radio" name="recip_select" value="attend3" /><?php echo $translator->translate('RECIP_ATTEND_NO') ?>
                        </div>
                        <div class="col-sm-4">
                            <input class="recip_select"  type="radio" name="recip_select" value="attend1" /><?php echo $translator->translate('RECIP_ATTEND_YES') ?>
                        </div>
                        <div class="col-sm-4">
                            <input class="recip_select"  type="radio" name="recip_select" value="attend" /><?php echo $translator->translate('RECIP_ATTEND_UNKN') ?>
                        </div>
                    </div>

                    <div class="row">
                        <?php foreach ($teamMembersList as $statusGroup): ?>
                            <?php foreach ($statusGroup as $orderGroup): ?>
                                <?php foreach ($orderGroup as $teamMembers): ?>
                                    <?php foreach ($teamMembers as $teamMember): ?>
                                        <div class="col-sm-4">
                                            <input class="recipient-<?php echo strtolower($teamMember->getTeamRole()) ?> recipient-all recipient-attend<?php echo $teamMember->getEventAttendanceStatus($event->getUid()) ?>" type="checkbox" name="recipients[]" value="<?php echo $teamMember->getId() ?>" />
                                            <span class="username"><?php echo $teamMember->getFullName() ?></span>
                                        </div>

                                    <?php endforeach; ?>
                                <?php endforeach; ?>
                            <?php endforeach; ?>
                        <?php endforeach; ?>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $translator->translate('Cancel') ?></button>
                <button type="button" class="btn btn-primary notification-send-trigger"><?php echo $translator->translate('Send') ?></button>
            </div>
        </div>
    </div>
</div>