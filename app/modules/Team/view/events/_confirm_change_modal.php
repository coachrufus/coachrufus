<div class="modal fade" id="confirm-change-modal" tabindex="-1" role="dialog" aria-labelledby="<?php echo $translator->translate('Confirm changes') ?>">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $translator->translate('Edit recurring event') ?></h4>
      </div>
      <div class="modal-body">
          <div class="alert alert-warning"><?php echo $translator->translate('Would you like to change this event only, all events in the series or this and all following events in the series?') ?></div>
          <div class="row border-row">
               <div class="col-sm-5">
                  <?php echo $translator->translate('I want change') ?>
              </div>
              <div class="col-sm-7">
                 <?php echo $translator->translate('What happens') ?>
              </div>
          </div>
          <div class="row">
               <div class="col-sm-5">
                  <button class="confirm-change-submit btn btn-primary"  data-val="only" ><?php echo $translator->translate('Only this event') ?></button>
              </div>
              <div class="col-sm-7">
                  <h5><?php echo $translator->translate('All other events in the series will remain the same.') ?></h5>
              </div>
             
          </div>
          <div class="row">
              
              
              <div class="col-sm-5">
                  <button class="confirm-change-submit btn btn-primary" data-val="next" class="btn btn-primary"><?php echo $translator->translate('Following events') ?></button>
              </div>
              <div class="col-sm-7">
                  <h5><?php echo $translator->translate('This and all the following events will be changed.') ?></h5>
                  
                  <h6><?php echo $translator->translate('Any changes to future events will be lost.') ?><h6>
              </div>
           </div>
          <div class="row">
              
             
              <div class="col-sm-5">
                  <button class="confirm-change-submit btn btn-primary" data-val="all" class="btn btn-primary"><?php echo $translator->translate('All events') ?></button>
              </div>
               <div class="col-sm-7">
                  <h5><?php echo $translator->translate('All events in the series will be changed.') ?></h5>
                
              </div>
          </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $translator->translate('Cancel') ?></button>
      </div>
    </div>
  </div>
</div>

