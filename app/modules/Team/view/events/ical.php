BEGIN:VCALENDAR
VERSION:2.0
PRODID:-//Coach Rufus//drozdi//EN
<?php foreach($events as $event): ?>
BEGIN:VEVENT
UID:<?php echo $event->getId()."\n" ?>
DTSTAMP:<?php echo $event->getIcalStart()."\n" ?>
DTSTART:<?php echo $event->getIcalStart()."\n" ?>
DURATION:PT2H0M0S
SUMMARY:<?php echo $event->getName()."\n" ?>
LOCATION:<?php echo $event->getPlaygroundName()."\n" ?>
<?php if($event->getPeriod() == 'daily'): ?>
RRULE:FREQ=DAILY;<?php echo ($event->getEndPeriodType() == 'after') ? ';COUNT='.$event->getEndPeriod() : '' ?><?php echo ($event->getEndPeriodType() == 'date') ? ';UNTIL='.$event->getIcalEndPeriodDate() : '' ?>;INTERVAL=<?php echo $event->getPeriodInterval() ?><?php echo "\n" ?>
<?php endif; ?>
<?php if($event->getPeriod() == 'weekly'): ?>
RRULE:FREQ=WEEKLY;BYDAY=<?php echo $event->getIcalDays() ?><?php echo ($event->getEndPeriodType() == 'after') ? ';COUNT='.$event->getEndPeriod() : '' ?><?php echo ($event->getEndPeriodType() == 'date') ? ';UNTIL='.$event->getIcalEndPeriodDate() : '' ?>;INTERVAL=<?php echo $event->getPeriodInterval() ?><?php echo "\n" ?>
<?php endif; ?>
<?php if($event->getPeriod() == 'monthly'): ?>
RRULE:FREQ=MONTHLY<?php echo ($event->getEndPeriodType() == 'after') ? ';COUNT='.$event->getEndPeriod() : '' ?><?php echo ($event->getEndPeriodType() == 'date') ? ';UNTIL='.$event->getIcalEndPeriodDate() : '' ?>;INTERVAL=<?php echo $event->getPeriodInterval() ?><?php echo "\n" ?>
<?php endif; ?>
<?php if($event->getPeriod() == 'year'): ?>
RRULE:FREQ=YEARLY<?php echo ($event->getEndPeriodType() == 'after') ? ';COUNT='.$event->getEndPeriod() : '' ?><?php echo ($event->getEndPeriodType() == 'date') ? ';UNTIL='.$event->getIcalEndPeriodDate() : '' ?>;INTERVAL=<?php echo $event->getPeriodInterval() ?><?php echo "\n" ?>
<?php endif; ?>
END:VEVENT
<?php endforeach; ?>
END:VCALENDAR