 <div class="panel-toolbar panel-toolbar-inline event-list-toolbar">
                        <div class="btn-group tool-item">
                             <?php if ($request->get('type') == 'past'): ?>
                                <button type="button" class="dropdown-toggle clear-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <?php echo $translator->translate('Past') ?>   <i class="ico ico-dropdown"></i>
                                </button>
                                <ul class="dropdown-menu select-dropdown">
                                    <li><a href="<?php echo $router->link('team_event_list', array('type' => 'upcoming','team_id' => $team->getId())) ?>"> <?php echo $translator->translate('Upcoming') ?></a></li>

                                </ul>
                            <?php else: ?>
                                <button type="button" class="dropdown-toggle clear-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <?php echo $translator->translate('Upcoming') ?>   <i class="ico ico-dropdown"></i>
                                </button>
                                <ul class="dropdown-menu select-dropdown">
                                     <li><a href="<?php echo $router->link('team_event_list', array('d' => 'past','type' => 'past','to' => date('Y-m-d-H-i'), 'team_id' => $team->getId())) ?>"><?php echo $translator->translate('Past') ?></a></li>

                                </ul>   
                            <?php endif; ?>
                        </div>
                    </div>


                    <div class="panel-toolbar event-list-toolbar">
                        <?php if (\Core\ServiceLayer::getService('security')->getIdentity()->hasTeamPermission('manageEvents', $team)  ): ?>
                        <a class="color-link tool-item tool-item-link" href="<?php echo $router->link('create_team_event', array('team_id' => $team->getId())) ?>"><i class="ico ico-plus-green"></i> <span class="hide760"><?php echo $translator->translate('Add event') ?></span></a>
                        <?php endif; ?>

                        <div class="btn-group tool-item" role="group">  
                            <a class="dropdown-toggle export-event" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="<?php echo $router->link('team_events_calendar', array('calendar_date' => $calendar_date, 'team_id' => $team->getId())) ?>"><i class="ico ico-export"></i> <span><?php echo $translator->translate('Export') ?></span></a>
                            <ul class="dropdown-menu">
                                <li> <a class="popup-link" href="<?php echo $router->link('team_event_ical', array('team_id' => $team->getId())) ?>"><?php echo $translator->translate('Download .ics file') ?></a></li>
                                <li> <a class="popup-link" href="webcal://<?php echo WEB_DOMAIN_NAME ?><?php echo $router->link('team_event_ical', array('team_id' => $team->getId())) ?>">Outlook</a></li>
                                <li> <a class="popup-link" href="webcal://<?php echo WEB_DOMAIN_NAME ?><?php echo $router->link('team_event_ical', array('team_id' => $team->getId())) ?>">iCalc</a></li>
                                <li> <a class="popup-link" href="https://www.google.com/calendar/render?cid=webcal://<?php echo WEB_DOMAIN_NAME ?><?php echo $router->link('team_event_ical', array('team_id' => $team->getId())) ?>"><?php echo $translator->translate('Google calendar') ?></a></li>
                            </ul>
                        </div>
                            
                        <a class="tool-item tool-item-link" href="<?php echo $router->link('team_events_calendar', array('team_id' => $team->getId())) ?>"><i class="ico ico-calendar"></i></a> 
                    </div>