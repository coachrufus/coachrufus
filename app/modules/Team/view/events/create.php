<?php Core\Layout::getInstance()->startSlot('team_menu') ?>  
<?php $layout->includePart(MODUL_DIR.'/Team/view/team/_team_menu.php',array('team' => $team) ) ?>
<?php Core\Layout::getInstance()->endSlot('team_menu') ?>




<section class="content-header">
    <a class="all-event-link" href="<?php echo $router->link('team_event_list', array('team_id' => $team->getId())) ?>"><i class="ico ico-event-back"></i><?php echo $translator->translate('All Events') ?></a>
    
     <?php echo $layout->renderControllerAction('CR\Gamifications\GamificationModule:Progress:progressBar')->getContent() ?>
    
    <?php
    $layout->includePart(GLOBAL_DIR . '/templates/parts/_breadcrumb.php', array(
        'crumbs' => array(
            'Teams' => $router->link('teams_overview'),
            $team->getName() => $router->link('team_overview', array('id' => $team->getId())),
            'Events' => $router->link('team_event_list', array('team_id' => $team->getId())),
            'Add event' => ''
)))
    ?>
</section>

<section class="content">

    
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                
                <div class="panel-body">
                       
<div class="box-body">

<form action="" class="form-bordered" method="post">
 <?php if ($request->hasFlashMessage('team_event_create_success')): ?>
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <?php echo $request->getFlashMessage('team_event_create_success') ?>
    </div>
<?php endif; ?>

<?php if ($request->hasFlashMessage('team_event_edit_success')): ?>
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <?php echo $request->getFlashMessage('team_event_edit_success') ?>
    </div>
<?php endif; ?>
    
<?php if($validator->hasErrors()): ?>
  <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <strong><?php echo $translator->translate('Please, correct errors and try again') ?></strong>
    </div>
<?php endif; ?>
    
    
    
    
    
    
    <input type="hidden" id="change_type" name="change_type" value="" />
    <input type="hidden" id="source_period_type"  value="<?php echo $form->getEntity()->getPeriod() ?>" />
    <?php echo $form->renderInputHiddenTag("team_id") ?>

    
    <div class="row">
        <div class="form-group col-sm-6">
            <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("name")) ?><?php echo $form->getRequiredLabel('name') ?></label>
            <?php echo $form->renderInputTag("name", array('class' => 'form-control','required' => 'required')) ?>
            <?php echo $validator->showError("name",array('message' => $translator->translate('ERROR_TEAM_NAME_MAX'))) ?>
            <?php echo $validator->showError("name_unique") ?>
        </div>
        <div class="form-group col-sm-6">
            <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("event_type")) ?></label>
             <div class="input-group">
                <div class="input-group-addon">
                    <i class="ico ico-ball ico-o2"></i>
                </div>
                <?php echo $form->renderSelectTag("event_type", array('class' => 'form-control')) ?>
            </div>
            <?php echo $validator->showError("event_type") ?>
        </div>
    </div>
    
    
     <div class="row">
        <?php if($finishedProgressStep):  ?>
       <div class="form-group col-sm-8">
           <?php if( \Core\ServiceLayer::getService('security')->getIdentity()->hasTeamPermission('manageSettings',$team) &&  Core\ServiceLayer::getService('StepsManager')->isStepFinished('event_create')): ?>
                <a id="add_season_trigger" class="color-link pull-right" href="#" type="button"><i class="ico ico-plus-green"></i> <?php echo $translator->translate('Add season') ?></a>
            <?php endif; ?>
           <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("season")) ?><?php echo $form->getRequiredLabel('season') ?></label>
            
            <div class="input-group">
                <div class="input-group-addon">
                    <i class="ico ico-cup"></i>
                </div>
                 <?php echo $form->renderSelectTag("season", array('class' => 'form-control')) ?>
            </div>
            <?php echo $validator->showError("season",array('message' => $translator->translate('Required Field!'))) ?>
        </div>
        <?php endif; ?>
        <div class="form-group col-sm-4">
            <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("capacity")) ?><?php echo $form->getRequiredLabel('capacity') ?></label>
            <div class="input-group">
                <div class="input-group-addon">
                    <i class="ico ico-people"></i>
                </div>
                 <?php echo $form->renderInputTag("capacity", array('class' => 'form-control numeric-only')) ?>
            </div>
            <?php echo $validator->showError("capacity",array('message' => $translator->translate('Only numbers allowed'))) ?>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12 form-group ">
            <?php echo $validator->showError("playground_id") ?>
          
            
             <a class="color-link pull-right add_playground_modal_trigger" href="#" type="button"><i class="ico ico-plus-green"></i> <?php echo $translator->translate('Add Playground') ?></a>
             <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("Playground")) ?></label>
            
              <div class="input-group">
                    <div class="input-group-addon">
                        <i class="ico ico-playground"></i>
                    </div>
                    <?php echo $form->renderSelectTag("playground_id", array('class' => 'form-control', 'id' => 'playground_choice' )) ?>
                </div>

         </div>
            
        
    </div>
    

    <div class="row"> 
        <div class="form-group col-sm-4">
            <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("start_date")) ?><?php echo $form->getRequiredLabel('start_date') ?></label>
            
            <div class="input-group">
                <div class="input-group-addon">
                    <i class="ico ico-event-cal ico-o2"></i>
                </div>
                <?php echo $form->renderInputTag("start_date", array('class' => 'form-control')) ?>
            </div>
            <label class="control-label control-label-error season_range_error"><?php echo $translator->translate('Date is out of range of the selected season, this event will  be not included  into the statistics') ?></label>
            <?php echo $validator->showError("start_date") ?>
        </div>
        
        <div class="form-group col-sm-4">
            <div class="bootstrap-timepicker">
                    <div class="form-group">
                      <label><?php echo $translator->translate($form->getFieldLabel("start_time")) ?><?php echo $form->getRequiredLabel('start_time') ?></label>
                      <div class="input-group"><div class="input-group-addon">
                          <i class="ico ico-event-clock"></i>
                        </div>
                       <?php echo $form->renderInputTag("start_time", array('class' => 'form-control')) ?>
                      </div>
                    </div>
                  </div>
          
            <?php echo $validator->showError("start_time") ?>
        </div>
        
        <div class="form-group col-sm-4">
            <div class="bootstrap-timepicker">
                    <div class="form-group">
                      <label><?php echo $translator->translate($form->getFieldLabel("end_time_time")) ?><?php echo $form->getRequiredLabel('end_time_time') ?></label>
                      <div class="input-group"><div class="input-group-addon">
                          <i class="ico ico-clock3"></i>
                        </div>
                       <?php echo $form->renderInputTag("end_time_time", array('class' => 'form-control')) ?>
                      </div>
                    </div>
                  </div>
          
            <?php echo $validator->showError("end_time_time") ?>
        </div>
    </div>  
    
    
    <?php if($finishedProgressStep): //start repeat event ?>
     <a id="event-repeat-options-trigger" class="color-link <?php echo ($form->getFieldvalue('period') != null) ? 'collapse-trigger-active' : 'collapse-trigger-collapsed' ?> " href="#" type="button">
         <i class="ico ico-plus-green"></i>
         <i class="ico ico-minus-g"></i>
         <?php echo $translator->translate('Add Repeat Event') ?>
     </a>
    
    <div class="row" id="event-repeat-options" <?php echo ($form->getFieldvalue('period') != null) ? 'style="display:block"' : '' ?>>   
         <div class="form-group col-sm-3">
            <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("period")) ?></label>
              <div class="input-group">
                <div class="input-group-addon">
                    <i class="ico ico-repeat"></i>
                </div>
                  <?php echo $form->renderSelectTag("period", array('class' => 'form-control')) ?>
            </div>
            <?php echo $validator->showError("period") ?>
        </div>
        
         <div class="form-group col-sm-2 <?php echo ($form->getEntity()->getPeriod() != 'none') ? 'active' : '' ?>" id="period_interval_wrapper">
            <label class="control-label"><?php echo $translator->translate('Interval') ?></label>
            <?php echo $form->renderInputTag("period_interval", array('class' => 'form-control', '')) ?> 
                <span class="event_interval_label interval_label_year"><?php echo $translator->translate('years') ?></span>
                <span class="event_interval_label interval_label_monthly"><?php echo $translator->translate('months') ?></span>
                <span class="event_interval_label interval_label_weekly"><?php echo $translator->translate('weeks') ?></span>
                <span class="event_interval_label interval_label_daily"><?php echo $translator->translate('days') ?></span>
            <?php echo $validator->showError("period_interval") ?>
        </div>
        
        <div class="form-group col-sm-3">
            <label class="control-label"><?php echo $translator->translate("End period type") ?></label>
              <?php echo $form->renderSelectTag("end_period_type", array('class' => 'form-control')) ?>
            <?php echo $validator->showError("end_period_type") ?>
        </div>
        
             
     <div class="col-sm-4 form-group end-period-type-wrapper <?php echo ($form->getEntity()->getPeriod() != 'none') ? 'active' : '' ?>"> 
         <label class="control-label">&nbsp;</label>
             <div class="end_period_type_choice end_period_type_after <?php echo ($form->getEntity()->getEndPeriodType() == 'after') ? 'active' : '' ?>">
                  <?php echo $form->renderInputTag("end_period", array('class' => 'form-control')) ?>  x <?php echo $translator->translate('repeats') ?>
             </div>
         
             <div class="end_period_type_choice end_period_type_date <?php echo ($form->getEntity()->getEndPeriodType() == 'date') ? 'active' : '' ?>">
                 <?php echo $form->renderInputTag("end", array('class' => 'form-control')) ?> 
             </div>
        </div>
          <div class="form-group col-sm-12 <?php echo ($form->getEntity()->getPeriod() == 'weekly') ? 'active' : '' ?>" id="period_interval_week_wrapper">
            <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("repeat_days")) ?></label>
             <?php echo $validator->showError("repeat_days") ?>
            <div>
             <?php foreach($form->getFieldChoices('repeat_days') as $key => $choice): ?>
            <div class="repeat_day_choice"><input id="repeat_day_<?php echo $key  ?>" name="<?php echo $form->getName() ?>[repeat_days][]" type="checkbox" value="<?php echo $key ?>" <?php echo (in_array($key, $form->getEntity()->getWeeklyPeriodDays())) ? 'checked="checked"' : '' ?>> <?php echo mb_substr($choice,0,2)  ?></div>
            <?php endforeach; ?>
            
           
            </div>
        </div>
        
        <div class="col-sm-12" id="repeat_summary">
            <?php echo $translator->translate('Repeat every') ?> <span id="repeat_summary_period_interval">?</span> 
            <span id="repeat_summary_period">
                <span class="daily"><?php echo $translator->translate('Day') ?></span>
                <span class="weekly"><?php echo $translator->translate('Week') ?></span>
                <span class="monthly"><?php echo $translator->translate('Month') ?></span>
                <span class="year"><?php echo $translator->translate('Year') ?></span>
            </span>, 
               <?php echo $translator->translate('from') ?><span id="repeat_summary_from"></span> 
                <span id="repeat_summary_end_date">, <?php echo $translator->translate('untill') ?> <span id="repeat_summary_to">?</span> </span>
                <span id="repeat_summary_end_after">, <span id="repeat_summary_end_period">?</span> <?php echo $translator->translate('repeats') ?></span>
        </div>
        
    </div>  
    <?php endif; //end repeat event ?>
    
    
 <a id="event-notification-options-trigger" class="color-link collapse-trigger-collapsed" href="#" type="button">
     <i class="ico ico-plus-green"></i>
         <i class="ico ico-minus-g"></i>
 <?php echo $translator->translate('Add Notification') ?></a>
 
   <div class="row" id="event-notification-options">
         
        <div class="form-group col-sm-3 notification">
            <div class="checkbox">
                <label>
                    <?php echo $form->renderCheckboxTag("notification", array('class' => 'form-control')) ?>
                   <?php echo $translator->translate($form->getFieldLabel("notification")) ?>
                </label>
            </div>
        </div>
       
        <div class="form-group col-sm-3">
            <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("notify_group")) ?></label>
            <?php echo $form->renderSelectTag("notify_group", array('class' => 'form-control')) ?>
            <?php echo $validator->showError("notify_group") ?>
        </div>
       
       
       
       
        <div id="notify-team-wrap"  <?php echo ($form->getEntity()->getNotification() == '1') ? 'style="display:block;"' : '' ?>>
            <div class="form-group col-sm-3">
                <label class="control-label"><?php echo $translator->translate('Notification termin') ?></label>
                <?php echo $form->renderSelectTag("notify_termin_type",array('class' => 'form-control')) ?>
            </div>
            
            <div class="form-group col-sm-3 notify_termin_type_options notify_termin_type_options_beep" <?php echo ($form->getEntity()->getNotifyBeepDays() > 0) ? 'style="display:block;"' : '' ?>>
                <label class="control-label">&nbsp;</label>
                 <?php echo $form->renderSelectTag("notify_beep_days", array('class' => 'form-control')) ?> 
                <?php echo $translator->translate('days') ?>
            </div>
            
            <div class="form-group col-sm-3 notify_termin_type_options notify_termin_type_options_date" >
                <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("Date")) ?></label>
                <?php echo $form->renderInputTag("notify_termin", array('class' => 'form-control')) ?> 
            </div>
            

            
           
        </div>
    </div>

    
  <div class="row">
        <div class="col-sm-12">

        <?php if($finishedProgressStep): ?>
             <button class="btn btn-primary pull-right"><?php echo $translator->translate('Submit') ?></button>&nbsp;
             <a class="pull-left  btn btn-border" href="<?php echo $router->link('team_event_list',array('team_id' => $team->getId())) ?>"><?php echo $translator->translate('Cancel') ?></a>
        <?php else: ?>
             <button class="btn btn-primary pull-right"><?php echo $translator->translate('Save and Continue') ?></button>&nbsp;
             <a class="pull-left  btn btn-border" href="<?php echo $router->link('team_players_list',array('team_id' => $team->getId())) ?>"><i class="fa fa-chevron-left"></i> <?php echo $translator->translate('Back') ?></a>
        <?php endif; ?>
        </div>
    </div>
   
  
     
</form>
<?php $layout->includePart(MODUL_DIR . '/Team/view/events/_add_season_modal.php',array('team' => $team,'seasonForm' => $seasonForm)) ?>
     <?php $layout->includePart(MODUL_DIR . '/TeamPlayground/view/crud/add_modal.php') ?>
    </div>
                </div>
                
            </div>
        </div>
        
    </div>
</section>

    




<?php $layout->addStylesheet('plugins/pickadate/themes/classic.css') ?>
<?php $layout->addStylesheet('plugins/pickadate/themes/classic.date.css') ?>
<?php $layout->addJavascript('plugins/pickadate/picker.js') ?>
<?php $layout->addJavascript('plugins/pickadate/picker.date.js') ?>
<?php $layout->addJavascript('plugins/pickadate/translations/'.LANG.'.js') ?>


<?php $layout->addJavascript('plugins/select2/select2.full.min.js') ?>

<?php $layout->addStylesheet('plugins/timepicker/bootstrap-timepicker.min.css') ?>
<?php $layout->addJavascript('plugins/timepicker/bootstrap-timepicker.js') ?>

<?php $layout->addJavascript('js/TeamEventManager.js') ?>
<?php $layout->addJavascript('js/PlaygroundManager.js') ?>
<?php  $layout->startSlot('javascript') ?>
<?php echo Core\ServiceLayer::getService('AchievementAlertManager')->showPlaceAchievementAlert('event_detail','event_create',Core\ServiceLayer::getService('security')->getIdentity()) ?>
<?php echo Core\ServiceLayer::getService('AchievementAlertManager')->showPlaceAchievementAlert('event_create','event_repeat_create',Core\ServiceLayer::getService('security')->getIdentity()) ?>
 <script type="text/javascript">
$("#playground_choice").select2();



      
jQuery('#record_end').pickadate({'format': 'dd.mm.yyyy'});
jQuery('#record_start_date').pickadate({'format': 'dd.mm.yyyy'});
jQuery('#record_notify_termin').pickadate({'format': 'dd.mm.yyyy'});

jQuery("#season_start_date").pickadate({'format': 'dd.mm.yyyy'});
jQuery("#season_end_date").pickadate({'format': 'dd.mm.yyyy'});
jQuery("#record_start_time").timepicker({showInputs: false,showMeridian: false, minuteStep: 5});
jQuery("#record_end_time_time").timepicker({defaultTime:'00:00',showInputs: false,showMeridian: false, minuteStep: 5});

if(jQuery("#record_end_time_time").val() == '00:00')
{
    jQuery("#record_end_time_time").val('');
}

 
event_manger = new TeamEventsManager();
event_manger.init();
var playground_manager = new PlaygroundManager();

function initLocalityManager()
{


    playground_manager.google = google;
    playground_manager.initModalCreate();
}

/*
function initLocalityManager()
{
   
    
        
   event_manger.google = google;
    
    
    
    <?php if(null != $teamPlayground): ?>
    event_manger.initMap({lat: <?php echo $teamPlayground->getLat();?>, lng: <?php echo $teamPlayground->getLng() ?>});
    <?php else: ?>
        event_manger.initMap();
    <?php endif; ?>
    event_manger.showSearchMap();
    event_manger.createSearchInput();
   
}
*/
 </script>
 <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXU1duK54f1gIsRy_xc7z4uQURovsZdE8&libraries=places&callback=initLocalityManager" async defer></script>
<?php  $layout->endSlot('javascript') ?>

 
 


