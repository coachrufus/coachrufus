<div class="attendant-row attendant-row-<?php echo $teamMember->getId() ?>">
    <span class="ord-num">
        <?php echo $onum ?>
    </span>
    <div class="user-block pull-left">
        <div class="round-50" style="background-image:url(<?php echo $teamMember->getMidPhoto() ?>)"></div>
        <span class="username"><?php echo $teamMember->getFullName() ?></span>
        <span class="description"><?php echo $teamMember->getTeamRoleName() ?> <?php echo ($teamMember->getPlayerNumber() != null) ? '#' . $teamMember->getPlayerNumber() : '' ?></span>
    </div><!-- /.user-block -->


    <div class="pull-right">


        <?php if ($user->getId() == $teamMember->getPlayerId() or \Core\ServiceLayer::getService('security')->getIdentity()->hasTeamPermission('manageEvents', $team)): ?>
           
                <a href="#"
                   data-url="<?php echo $router->link('change_player_attendance') ?>" 
                   data-rel="in" 
                   data-event="<?php echo $event->getId() ?>" 
                   data-eventdate="<?php echo $event->getCurrentDate()->format('Y-m-d') ?>" 
                   data-player="<?php echo $teamMember->getPlayerId() ?>" 
                   data-team-role="<?php echo $teamMember->getTeamRole() ?>"
                   data-team-player="<?php echo $teamMember->getId() ?>" class="in attendance_trigger attendance_trigger_btn <?php echo ($teamMember->getEventAttendanceStatus($event->getUid()) == 1 ) ? 'accept' : '' ?>"><i class="ico ico-check"></i></a>

                <a href="#"  data-url="<?php echo $router->link('change_player_attendance') ?>" 
                   data-rel="out" 
                   data-event="<?php echo $event->getId() ?>" 
                   data-eventdate="<?php echo $event->getCurrentDate()->format('Y-m-d') ?>" 
                   data-player="<?php echo $teamMember->getPlayerId() ?>" 
                   data-team-role="<?php echo $teamMember->getTeamRole() ?>"
                   data-team-player="<?php echo $teamMember->getId() ?>" class="out attendance_trigger attendance_trigger_btn    <?php echo ($teamMember->getEventAttendanceStatus($event->getUid()) == 3 ) ? 'deny' : '' ?>"><i class="ico ico-close"></i></a>

           





        <?php else: ?>

            <?php if ($teamMember->getEventAttendanceStatus($event->getUid()) == 3): ?>
                <span class="attendance_trigger_btn deny"><i class="ico ico-close"></i></span>
            <?php elseif ($teamMember->getEventAttendanceStatus($event->getUid()) == 1): ?>
                  <span class="attendance_trigger_btn accept" ><i class="ico ico-check"></i></span>
            <?php else: ?>
                
               <span class="attendance_trigger_btn" ><i class="ico ico-question"></i></span>
            <?php endif; ?>


        <?php endif; ?> 



    </div>



</div>