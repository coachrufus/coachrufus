<div class="modal fade" id="other-sport-modal" tabindex="-1" role="dialog" aria-labelledby="<?php echo $translator->translate('Other sport') ?>">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
          <?php echo $translator->translate('OTHER_SPORT_ALERT') ?>
                       

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>

