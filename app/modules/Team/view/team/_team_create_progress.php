
<div class="row wizard <?php echo 'active_step_'.$step ?>" >
    
    
                <div class="col-xs-4 wizard-step wizard-step-1 <?php echo ($step == 1) ? 'active' : '' ?>  <?php echo ($step > 1) ? 'complete' : '' ?> <?php echo ($step == 0) ? 'disabled' : '' ?>">
                    <div class="icon">
                        <i class="ico ico-wizard1"></i>
                    </div>
                    <strong><?php echo $translator->translate('Create team') ?></strong>
                     <div class="progress-line progress_step1"></div>
                </div>
               
                <div class="col-xs-4 wizard-step wizard-step-2 <?php echo ($step == 2) ? 'active' : '' ?>  <?php echo ($step > 2) ? 'complete' : '' ?> <?php echo ($step < 2) ? 'disabled' : '' ?>"><!-- complete -->
                   <div class="icon">
                        <i class="ico ico-wizard2"></i>
                    </div>
                    <strong><?php echo $translator->translate('Invite members') ?></strong>
                     <div class="progress-line progress_step2"></div>
                </div>
                
                <div class="col-xs-4 wizard-step wizard-step-3 <?php echo ($step == 3) ? 'active' : '' ?>  <?php echo ($step > 3) ? 'complete' : '' ?> <?php echo ($step < 3) ? 'disabled' : '' ?>"><!-- complete -->
                   <div class="icon">
                        <i class="ico ico-calendar"></i>
                    </div>
                    <strong><?php echo $translator->translate('New Event') ?></strong>
                </div>
    </div>

