<?php Core\Layout::getInstance()->startSlot('title') ?>  
    - <?php echo $team->getName() ?>
<?php Core\Layout::getInstance()->endSlot('title') ?>

<div class="login-box  share-link-invite-box">
    <div class="login-logo">
        <img src="/img/main/logo-white.svg" onerror="this.onerror=null; this.src='/img/logo-white.png'" title="Je čas na hru" alt="logo Coach Rufus">
    </div><!-- /.login-logo -->
    <div class="login-box-body text-center">
       

      <h2><?php echo $translator->translate('Pozvánka do tímu') ?> <?php echo $team->getName() ?></h2>
        <?php echo $team->getAddressCity() ?> <?php echo $team->getAddressZip() ?>
        <p></p>
        
        <div class="row">
            <div class="col-sm-6">
                <?php echo $translator->translate('share.invite.login.text') ?>
                 <a class="btn btn-success btn-lg" href="<?php echo $loginLink ?>"><?php echo $translator->translate('share.invite.login.button') ?></a>
            </div>
            
            <div class="col-sm-6">
                 <?php echo $translator->translate('share.invite.register.text') ?>
                 <a class="btn btn-success btn-lg" href="<?php echo $registerLink ?>"><?php echo $translator->translate('share.invite.register.button') ?></a>
            </div>
            
        </div>
        <p>
       
        </p>


       

    </div><!-- /.login-box-body -->
</div><!-- /.login-box -->


