<?php 
$sportManager = Core\ServiceLayer::getService('SportManager');
$teamManager = Core\ServiceLayer::getService('TeamManager');
$isFloorballChallengeTeam = $teamManager->isFloorballChallengeTeam($team);

$stepsManager = Core\ServiceLayer::getService('StepsManager');
$sportEnum = $sportManager->getSportFormChoices();
$stepsManager->getMenuStepClass('team_settings');
Core\ServiceLayer::getService('TeamCreditManager')->refreshTeamPackages();

$wizardWinished = $stepsManager->isStepFinished('step3_final');
$hasPro = \Core\ServiceLayer::getService('TeamCreditManager')->teamHasProPackage($team);

$hasProSport = $teamManager->teamHasProSport($team);
$menuClass = $layout->buildMenuClass();
?>

            <li class="menu-main team-header <?php echo  (!$hasPro && $wizardWinished &&  $hasProSport) ? 'upgrade_pro' : '' ?>">

                     <a class="<?php echo $menuClass['team_overview'] ?>"  href="<?php echo $router->link('team_overview', array('id' => $team->getId())) ?>"> 
                        <i class="ico ico-team" style="background-image: url('<?php echo $team->getMidPhoto() ?>');"></i>
                        <span><?php echo $team->getName() ?> (<?php echo $translator->translate($sportEnum[$team->getSportId()]) ?>)</span>
                        
                        <?php if(!$hasPro): ?>
                        <span class="team_credit_modal_trigger"><?php echo \Core\ServiceLayer::getService('TeamCreditManager')->renderProIcon($translator->translate('Upgrade to PRO')) ?></span>
                        <?php endif; ?>
                     </a>
            </li>


       
       
            <li  class="menu-main <?php echo $stepsManager->getMenuStepClass('team_settings') ?>">
                <a class="<?php echo $menuClass['team_settings'] ?>" href="<?php echo $router->link('team_settings', array('team_id' => $team->getId())) ?>"> 
                     <i class="ico ico-team-home"></i>
                <span><?php echo $translator->translate('Settings') ?> </span>
                </a>
            </li>

        <li  class="menu-main <?php echo $stepsManager->getMenuStepClass('team_event_list') ?>">
            <a class="<?php echo $menuClass['team_event'] ?>" href="<?php echo $router->link('team_event_list', array('type' => 'upcoming','team_id' => $team->getId())) ?>"> 
                <i class="ico ico-team-events"></i>
                <span><?php echo $translator->translate('Events') ?> </span>
            </a>
        </li>



         <?php if (\Core\ServiceLayer::getService('security')->getIdentity()->hasTeamPermission('manageSettings', $team)): ?>
         <li  class="menu-main <?php echo $stepsManager->getMenuStepClass('team_season_list') ?>">
            <a class="<?php echo $menuClass['team_season'] ?>" href="<?php echo $router->link('team_season_list', array('team_id' => $team->getId())) ?>"> 
                <i class="ico ico-season-color"></i>
                <span><?php echo $translator->translate('Seasons') ?> </span>
            </a>
        </li>
        <?php endif; ?>

        <li  class="menu-main <?php echo $stepsManager->getMenuStepClass('team_public_players_list') ?>">
            <a class="<?php echo $menuClass['team_members'] ?>" href="<?php echo $router->link('team_public_players_list', array('team_id' => $team->getId())) ?>">
                <i class="ico ico-team-members"></i> 
                <span><?php echo $translator->translate('Members') ?> </span>
            </a>
        </li>

        <li  class="menu-main <?php echo $stepsManager->getMenuStepClass('attendance') ?>">
            <?php if(\Core\ServiceLayer::getService('TeamCreditManager')->teamHasFeatureAccess($team,'attendance') or  $hasProSport ==  false): ?>
                 <a class="<?php echo $menuClass['team_attendance'] ?>" href="<?php echo $router->link('team_players_attendance', array('team_id' => $team->getId())) ?>">
                    <i class="ico ico-team-attend"></i> 
                    <span><?php echo $translator->translate('Attendance') ?> </span>
                </a>
            <?php else: ?>
                <a href="#" class="team_credit_modal_trigger">
                    <i class="ico ico-team-attend"></i> 
                    <span><?php echo $translator->translate('Attendance') ?> </span>
                    <?php echo \Core\ServiceLayer::getService('TeamCreditManager')->renderProIcon() ?>
                </a>
            <?php endif; ?>
        </li>

        
        <?php if($hasProSport): ?>
        <li  class="menu-main <?php echo $stepsManager->getMenuStepClass('team_player_stat') ?>">
             <?php if(\Core\ServiceLayer::getService('TeamCreditManager')->teamHasFeatureAccess($team,'stats')): ?>
                 <a class="<?php echo $menuClass['team_statistic'] ?>" href="<?php echo $router->link('team_player_stat', array('team_id' => $team->getId())) ?>">
                    <i class="ico ico-team-stat"></i>
                     <span><?php echo $translator->translate('Statistics') ?> </span>
                </a>
            <?php else: ?>
                 <a href="#" class="team_credit_modal_trigger">
                    <i class="ico ico-team-stat"></i>
                     <span><?php echo $translator->translate('Statistics') ?> </span>
                      <?php echo \Core\ServiceLayer::getService('TeamCreditManager')->renderProIcon() ?>
                </a>
            <?php endif; ?>
        </li>
        <?php endif; ?>
        
        
        
        <?php if (\Core\ServiceLayer::getService('security')->getIdentity()->hasTeamPermission('manageSettings', $team)): ?>
            <li  class="menu-main <?php echo $stepsManager->getMenuStepClass('team_playground_list') ?>">
                <a class="<?php echo $menuClass['team_playground'] ?>" href="<?php echo $router->link('team_playground_list', array('team_id' => $team->getId())) ?>">
                     <i class="ico ico-team-playgrounds"></i>
                    <span><?php echo $translator->translate('Playgrounds') ?></span>
                </a>
            </li>
        <?php endif; ?>
            
        <?php if($hasProSport): ?>
        <li  class="menu-main <?php echo $stepsManager->getMenuStepClass('widget_list') ?>">
              <?php if(\Core\ServiceLayer::getService('TeamCreditManager')->teamHasFeatureAccess($team,'widget')): ?>
                <a class="<?php echo $menuClass['team_graphs'] ?>" href="<?php echo $router->link('widget_list', array('team_id' => $team->getId())) ?>">
                    <i class="ico ico-team-charts"></i>
                    <span><?php echo $translator->translate('Charts') ?> </span>
               </a>
            <?php else: ?>
                 <a  href="#" class="team_credit_modal_trigger">
                    <i class="ico ico-team-charts"></i>
                    <span><?php echo $translator->translate('Charts') ?> </span>
                     <?php echo \Core\ServiceLayer::getService('TeamCreditManager')->renderProIcon() ?>
               </a>
            <?php endif; ?>
        </li>
         <?php endif; ?>
      
        
        <?php if($isFloorballChallengeTeam): ?>
        <li  class="menu-main">
            <a href="<?php echo $router->link('floorball_challenge_home', array('team_id' => $team->getId())) ?>">
                 <i class="ico ico-team-charts"></i>
                 <span><?php echo $translator->translate('Fllorball Challenge') ?> </span>
            </a>
        </li>
        <?php endif; ?>
      

