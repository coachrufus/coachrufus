<?php Core\Layout::getInstance()->startSlot('team_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/Team/view/team/_team_menu.php', array('team' => $team)) ?>
<?php Core\Layout::getInstance()->endSlot('team_menu') ?>

<section class="content-header">
    <h1>
        <?php echo $translator->translate('Billing') ?> - 
        <?php echo $season->getName(); ?> 
        (<?php echo $season->getStartDate()->format('m.d.Y') ?> - <?php echo $season->getEndDate()->format('m.d.Y') ?>)
    </h1>

    <?php
    $layout->includePart(GLOBAL_DIR . '/templates/parts/_breadcrumb.php', array(
        'crumbs' => array(
            'Teams' => $router->link('teams_overview'),
            $team->getName() => $router->link('team_overview', array('id' => $team->getId())),
            'Seasons' => ''
)))
    ?>
</section>

<section class="content">
    <div class="row">
        <div class="col-sm-12">
             <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li><a  href="<?php echo $router->link('team_players_attendance', array('team_id' => $team->getId())) ?>"><?php echo $translator->translate('Attendance') ?></a></li>
                <li class="active"><a href="<?php echo $router->link('team_season_billing', array('id' => $season->getId(), 'team_id' => $team->getId())) ?>"><?php echo $translator->translate('attendance.billing') ?></a>
                
                
                
                </li>
            </ul>
            <div class="tab-content">
  <div class="box-body">
<div class="row">
    
    <div class="col-sm-2">
         <label> <?php echo $translator->translate('Select season') ?></label>
         <div class="btn-group" role="group">
                        <a  class="dropdown-toggle clear-dropdown btn btn-default billing-season-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                             
                            <?php echo $season->getName() ?>
                            <i class="ico ico-arr-down-green"></i>
                        </a>
                        <ul class="dropdown-menu">
                            <?php foreach ($seasons as $seasonId => $seasonName): ?>
                                <li> 
                                    <a href="<?php echo $router->link('team_season_billing', array('id' => $seasonId, 'team_id' => $team->getId())) ?>">
                                        <?php echo $seasonName ?>
                                    </a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
    </div>
    
    <div class="col-sm-8">
                <form action="<?php echo $router->link('team_season_billing', array('id' => $season->getId(), 'team_id' => $team->getId())) ?>">
                    <label><?php echo $translator->translate('billing.way') ?></label>
                    <select id="calculationTypeSlct" name="calculationType">
                        <option value="calculated" <?php if ($calculationType == 'calculated' or is_null($calculationType))
    {
        echo 'selected="selected"';
    } ?>>Flexibilný poplatok za účasť</option>
                        <option value="fixed" <?php if ($calculationType == 'fixed')
    {
        echo 'selected="selected"';
    } ?>>Fixný poplatok za účasť</option>
                        
                        
                        <option value="permanent" <?php echo ($calculationType == 'permanent') ? 'selected="selected"' : '' ?>>Pravidelné členské</option>    
                    </select>
                    <input id="costsPerAttendanceCalculatedInpt"
                           type="hidden" 
                           disabled="disabled" 
                           value="<?php echo $bill['team']['costsPerAttendanceCalculated'] ?>" />
                    <label><?php echo ($calculationType == 'permanent') ? 'Pravidelný poplatok' : 'Poplatok za účasť' ?>  [€]: </label>
                    <input id="costsPerAttendanceInpt" 
                           name="costsPerAttendance"
                           type="text" 
                           size="4" 
                           min="0" 
                           max="9999" 
                           value="<?php echo $bill['team']['costsPerAttendance'] ?>" 
<?php if ($calculationType == 'calculated' or is_null($calculationType))
{
    echo 'disabled="disabled"';
} ?>/>
                    <button id="calculateBtn" type="submit" disabled="disabled">Prepočítať</button>
                    <br />
                    <p id="flexibleCalculationInfo" <?php if ($calculationType == 'fixed')
{
    echo 'style="display:none"';
} ?>>
                        <sub>
                            Poplatok za účasť systém vypočítal na základe celkových nákladov tímu, ktoré sú <strong><?php echo $bill['team']['costs'] ?> €.</strong>
                            Náklady možete upravovať v <a href="<?php echo $router->link('team_players_attendance', array('team_id' => $team->getId())) ?>">Dochádzke</a>.
                        </sub>
                    </p>
                </form>
                </div>
                

                <table id="table_billing_players" class="table table-condensed resp-table">
                    <thead>
                        <tr>
                            <th>
                        <?php echo $translator->translate('Player') ?>
                            </th>
                            <?php if('permanent' != $calculationType): ?>
                            <th>
                            <?php echo $translator->translate('Attendance count') ?>
                            </th>
                            <?php endif; ?>
                            <th>
                        <?php echo $translator->translate('Fees [€]') ?>
                            </th>
                            <?php if('permanent' != $calculationType): ?>
                            <th>
<?php echo $translator->translate('Balance [€]') ?>
                            </th>
                              <?php endif; ?>
                        </tr>
                    </thead>
                    <tbody>
<?php foreach ($bill['players'] as $player): ?>
                            <tr>
                                <th><?php echo $player['name'] ?></th>
                                  <?php if('permanent' != $calculationType): ?>
                                <td><?php echo $player['attendance'] ?></td>
                                 <?php endif; ?>
                                <td><?php echo $player['fees'] ?></td>
                                <?php if('permanent' != $calculationType): ?>
                                <td><?php echo $player['balance'] ?></td>
                                 <?php endif; ?>
                            </tr>
<?php endforeach; ?>
                        <tr>
                            <th><?php echo $team->getName() ?> (celý tím)</th>
                            <?php if('permanent' != $calculationType): ?>
                            <td><?php echo $bill['team']['attendance'] ?></td>
                             <?php endif; ?>
                            <td><?php echo $bill['team']['fees'] ?></td>
                            <?php if('permanent' != $calculationType): ?>
                            <td><?php echo $bill['team']['balance'] ?></td>
                             <?php endif; ?>
                        </tr>
                    </tbody>
                </table>
            </div>
            </div>
        </div>
        </div>
    </div>

</section>

<?php $layout->startSlot('javascript') ?>
<script type="text/javascript">
    $('#calculationTypeSlct').on('change',
            function ()
            {
                if ($('#calculationTypeSlct').val() == 'calculated')
                {
                    $('#costsPerAttendanceInpt').attr('disabled', 'disabled');
                    $('#costsPerAttendanceInpt').val($('#costsPerAttendanceCalculatedInpt').val());
                    $('#flexibleCalculationInfo').show();
                }
                if ($('#calculationTypeSlct').val() == 'fixed' || $('#calculationTypeSlct').val() == 'permanent')
                {
                    $('#costsPerAttendanceInpt').removeAttr('disabled');
                    $('#flexibleCalculationInfo').hide();
                }
                $('#calculateBtn').removeAttr('disabled');
            });

    $('#costsPerAttendanceInpt').on('change',
            function ()
            {
                $('#calculateBtn').removeAttr('disabled');
            })
</script>
<?php $layout->endSlot('javascript') ?>