 <?php Core\Layout::getInstance()->startSlot('team_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/Team/view/team/_team_menu.php', array('team' => $team)) ?>
<?php Core\Layout::getInstance()->endSlot('team_menu') ?>



<section class="content-header">
    <h1>
        <?php echo $team->getName() ?>
    </h1>

    <?php
    $layout->includePart(GLOBAL_DIR . '/templates/parts/_breadcrumb.php', array(
    'crumbs' => array(
    'Teams' => $router->link('teams_overview'),
    $team->getName() => ''
    )))
    ?>
    
           <?php echo $layout->renderControllerAction('CR\Gamifications\GamificationModule:Progress:progressBar')->getContent() ?>
</section>
<section class="content">

    

    <div class="row">
            <div class="col-md-8">
                
                <div class="panel panel-default">
                <div class="panel-heading">
                    <h3><i class="ico ico-panel-event ico-action"></i> <?php echo $translator->translate('Events') ?></h3>
                    
                    <div class="panel-toolbar">
                        <?php if (\Core\ServiceLayer::getService('security')->getIdentity()->hasTeamPermission('manageEvents', $team)  ): ?>
                        <a class="color-link tool-item tool-item-link" href="<?php echo $router->link('create_team_event', array('team_id' => $team->getId())) ?>"><i class="ico ico-plus-green"></i> <span class="hide760"><?php echo $translator->translate('Add event') ?></span></a>
                        <?php endif; ?>

                        <div class="btn-group tool-item" role="group">  
                            <a class="dropdown-toggle export-event" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="<?php echo $router->link('team_events_calendar', array('calendar_date' => $calendar_date, 'team_id' => $team->getId())) ?>"><i class="ico ico-export"></i> <span><?php echo $translator->translate('Export') ?></span></a>
                            <ul class="dropdown-menu">
                                <li> <a class="popup-link" href="<?php echo $router->link('team_event_ical', array('team_id' => $team->getId())) ?>"><?php echo $translator->translate('Download .ics file') ?></a></li>
                                <li> <a class="popup-link" href="webcal://<?php echo WEB_DOMAIN_NAME ?><?php echo $router->link('team_event_ical', array('team_id' => $team->getId())) ?>">Outlook</a></li>
                                <li> <a class="popup-link" href="webcal://<?php echo WEB_DOMAIN_NAME ?><?php echo $router->link('team_event_ical', array('team_id' => $team->getId())) ?>">iCalc</a></li>
                                <li> <a class="popup-link" href="https://www.google.com/calendar/render?cid=webcal://<?php echo WEB_DOMAIN_NAME ?><?php echo $router->link('team_event_ical', array('team_id' => $team->getId())) ?>"><?php echo $translator->translate('Google calendar') ?></a></li>
                            </ul>
                        </div>
                            
                        <a class="tool-item tool-item-link" href="<?php echo $router->link('team_events_calendar', array('team_id' => $team->getId())) ?>"><i class="ico ico-calendar"></i></a> 
                    </div>
                </div>
                <div class="panel-body  panel-full-body">
                    <?php if (empty(array_filter($eventsList))): ?>
                        <div class="alert alert-warning"><?php echo $translator->translate('No upcoming events in 30 days') ?></div>
                    <?php endif; ?>

                    <?php $ei= 0; foreach ($eventsList as $dayEvents): ?>
                        <?php if($ei < 3): ?>


                        <?php foreach ($dayEvents as $event): ?> 
    <?php $layout->includePart(MODUL_DIR . '/Event/view/Default/_event_list_row.php', array('event' => $event, 'attendanceList' => $attendanceList, 'team' => $team)); $ei++; ?>
                        <?php endforeach;  ?>
                        <?php endif; ?>
                    <?php endforeach; ?>
                        
                         <div class="event_row event-list-pagging">
                             <a class="btn btn-clear btn-clear-green" href="<?php echo $router->link('team_event_list',array('type'=>'upcoming','team_id' =>$team->getId() )) ?>"><?php echo $translator->translate('All events') ?></a>
                    </div>
                </div>
                   
            
            </div>

                
                
                 <?php $layout->includePart(MODUL_DIR . '/Team/view/widget/_team_talk.php',array('wallPosts' => $wallPosts, 'wallPostForm' => $wallPostForm,'team' => $team)) ?>
                
                <?php if($hasProSport): ?>
                <link rel="stylesheet" type="text/css" href="/dev/js/PlayersComparison/Sources/css/performance.css?ver=<?php VERSION ?>">
                <div id="Performance">
                    <div class="PerformanceBox panel panel-default">
                        <div class="loading"></div>
                    </div>
                </div>
                <?php endif; ?>

            <div class="panel panel-default">
                <div class="panel-heading">

                    <strong><?php echo $translator->translate('Members') ?></strong>
                    <?php if (\Core\ServiceLayer::getService('security')->getIdentity()->hasTeamPermission('managePlayers', $team)): ?>
                    <div class="panel-toolbar">
                        <a class="color-link tool-item tool-item-link" href="<?php echo $router->link('team_players_list', array('team_id' => $team->getId())) ?>" class="btn btn-default">

                            <i class="ico ico-plus-green"></i> <?php echo $translator->translate('Add members') ?></a>
                    </div>
                    <?php endif; ?>
                </div>
                <div class="panel-body">
                    <?php foreach ($players as $player): ?>
                    <div class="box box-widget collapsed-box">
                        <div class="box-header with-border">
                            <div class="user-block">

                                <div class="img_round_wrap member_img_round" style="background-image: url('<?php echo $player->getMidPhoto() ?>');">

                                </div>
                                <span class="username"><?php echo $player->getFirstName() ?> <?php echo $player->getLastName() ?></span>
                                <div class="description">

                                    <div data-toggle="tooltip" data-placement="top" title="<?php echo $translator->translate('STARBOX_DESC_TEXT') ?>" class="starbox-fake ghosting" data-start-value="<?php echo $player->getRating() ?>"> </div>

                                    <?php foreach ($player->getPlayerSports() as $playerSport): ?>
                                    <span class="label label-info"><?php echo $playerSport->getSport()->getName() ?></span>
                                    <?php endforeach; ?>
                                </div>
                            </div><!-- /.user-block -->
                        </div><!-- /.box-header -->
                    </div>

                    <?php endforeach; ?>
                </div>
            </div>        
        </div>

        <div class="col-md-4">
            <?php $layout->includePart(MODUL_DIR . '/Event/view/widget/_small_calendar.php') ?>
            <?php if($hasProSport): ?>
            <link rel="stylesheet" type="text/css" href="/dev/js/PlayersComparison/Sources/css/comparsion.css">
            <div id="PlayersComparison">
                <div class="PlayersComparsionBox panel panel-default">
                    <div class="loading"></div>
                </div>
            </div>
             <?php endif; ?>
        </div>
    </div>
</section>


<?php $layout->addJavascript('js/SmallCalendar.js') ?>
<?php $layout->addStylesheet('plugins/starbox/css/jstarbox.css') ?>
<?php $layout->addJavascript('plugins/starbox/jstarbox.js') ?>
<?php $layout->addJavascript('js/TeamEventManager.js') ?>
<?php $layout->addJavascript('js/TeamWallManager.js') ?>
<?php $layout->addJavascript('js/ScoutingManager.js') ?>
<?php $layout->addJavascript('/plugins/select2/select2.full.min.js') ?>

<?php $layout->addStylesheet('plugins/pickadate/themes/classic.css') ?>
<?php $layout->addStylesheet('plugins/pickadate/themes/classic.date.css') ?>
<?php $layout->addJavascript('plugins/pickadate/picker.js') ?>
<?php $layout->addJavascript('plugins/pickadate/picker.date.js') ?>

<?php $layout->addStylesheet('plugins/timepicker/bootstrap-timepicker.min.css') ?>
<?php $layout->addStylesheet('plugins/timepicker/bootstrap-timepicker.min.css') ?>


<?php $layout->addJavascript('plugins/fileupload/jquery.ui.widget.js') ?>
<?php $layout->addJavascript('plugins/fileupload/jquery.iframe-transport.js') ?>
<?php $layout->addJavascript('plugins/fileupload/jquery.fileupload.js') ?>
<?php $layout->addJavascript('plugins/textcomplete/jquery.textcomplete.min.js') ?>
<?php $layout->addJavascript('plugins/textcomplete/jquery.textcomplete.min.js') ?>


<?php $layout->addJavascript('js/Lib/jquery.flot.js') ?>
<?php $layout->addJavascript('js/Lib/jquery.flot.pie.js') ?>
<?php $layout->addJavascript('js/Lib/jquery.flot.stack.js') ?>
<?php $layout->addJavascript('js/Lib/jquery.flot.tooltip.min.js') ?>
<?php $layout->addJavascript('js/Lib/curvedLines.js') ?>
<?php $layout->addJavascript('js/Lib/JUMFlot/jquery.flot.JUMlib.js') ?>
<?php $layout->addJavascript('js/Lib/JUMFlot/jquery.flot.spider.js') ?>
<?php $layout->addJavascript('js/PlayersComparison/bundle/bundle.js') ?>
<?php  $layout->startSlot('javascript') ?>


<?php echo Core\ServiceLayer::getService('AchievementAlertManager')->showLastUnfinishedAchievementAlert($team->getId()) ?>

<?php $layout->includePart(MODUL_DIR . '/Team/view/events/_confirm_delete_modal.php') ?>

<script type="text/javascript">
    $('.starbox-fake').each(function (index, val) {
        $(val).starbox({
            average: $(this).attr('data-start-value'),
            changeable: false
        });
    });

    small_calendar = new SmallCalendar({
        'dataUrl': '<?php echo $router->link('rest_event_get_month_grid') ?>',
        'localityManager': new LocalityManager()
    });
    small_calendar.initLoad();

    
     wall_manger = new TeamWallManager();
     wall_manger.bindTriggers();
     

    event_manger = new TeamEventsManager();
    event_manger.init();
    var scouting_manger = new ScoutingManager({
        'dataUrl': '<?php echo $router->link('rest_last_scouting') ?>'
    });
    


    $(".select2").select2();
    $(".select2").select2();
   

    function initLocalityManager()
    {
        locality_manger = new LocalityManager();
        locality_manger.google = google;
        locality_manger.createSearchInput('scouting-form-location1');

        locality_manger2 = new LocalityManager();
        locality_manger2.google = google;
        locality_manger2.createSearchInput('scouting-form-location2');

        locality_manger3 = new LocalityManager();
        locality_manger3.google = google;
        locality_manger3.createSearchInput('scouting-form-location3');

        locality_manger4 = new LocalityManager();
        locality_manger4.google = google;
        locality_manger4.createSearchInput('scouting-form-location4');

    }

</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXU1duK54f1gIsRy_xc7z4uQURovsZdE8&libraries=places&callback=initLocalityManager" async defer></script>
<?php $layout->endSlot('javascript') ?>



