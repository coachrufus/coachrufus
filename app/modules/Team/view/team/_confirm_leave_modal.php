<div class="modal fade" id="confirm-team-leave-modal" tabindex="-1" role="dialog" aria-labelledby="<?php echo $translator->translate('Confirm leave') ?>">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $translator->translate('Leave  team') ?></h4>
      </div>
      <div class="modal-body">
           <div id="single-delete-event-wrap">
            <div class="alert alert-warning"><?php echo $translator->translate('Would you like to leave this team?') ?></div>
            <a href="<?php echo $router->link('team_leave',array('team_id' => $team->getId())) ?>" class=" btn btn-primary" class="btn btn-primary"><?php echo $translator->translate('Leave') ?></a>
          </div>
          
      </div>
        
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $translator->translate('Cancel') ?></button>
      </div>
    </div>
  </div>
</div>

