<div class="modal fade" id="confirm-team-delete-modal" tabindex="-1" role="dialog" aria-labelledby="<?php echo $translator->translate('Confirm delete') ?>">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $translator->translate('Delete  team') ?></h4>
      </div>
      <div class="modal-body">
           <div id="single-delete-event-wrap">
            <div class="alert alert-warning"><?php echo $translator->translate('Would you like to delete this team?') ?></div>
            <a href="<?php echo $router->link('team_delete',array('team_id' => $team->getId())) ?>" class=" btn btn-primary" id="delete_event_link_all" data-val="all" class="btn btn-primary"><?php echo $translator->translate('Delete') ?></a>
          </div>
          
      </div>
        
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $translator->translate('Cancel') ?></button>
      </div>
    </div>
  </div>
</div>

