<?php Core\Layout::getInstance()->startSlot('team_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/Team/view/team/_team_menu.php', array('team' => $team)) ?>
<?php Core\Layout::getInstance()->endSlot('team_menu') ?>

<section class="content-header">
    <h1>
        <?php echo $translator->translate('Team CRS settings') ?>
    </h1>
    
    <?php $layout->includePart(GLOBAL_DIR.'/templates/parts/_breadcrumb.php',array(
        'crumbs' => array(
            'Teams' => $router->link('teams_overview'),
            $team->getName() => $router->link('team_overview',array('id' =>$team->getId() )),
            'Team crs settings' => ''
            ))) ?>

</section>

<section class="content">
    <section class="box box-primary">
          <div class="box box-header">
              <a class="btn btn-primary" href="<?php echo $router->link('team_player_stat', array('team_id' => $team->getId())) ?>"><i class="fa fa-chevron-left"></i> <?php echo $translator->translate('Back to statistics') ?></a>
          </div>
        
        <div class="box-body">
            
             <?php if ($request->hasFlashMessage('crs_edit_success')): ?>
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $request->getFlashMessage('crs_edit_success') ?>
                </div>
            <?php endif; ?>
            
            <p>
            <?php echo $translator->translate('CRS INFO TEXT') ?>
            </p>
            
            <form action="" method="post">
                 <div class="row">
                    <div class="form-group col-sm-2 col-xs-4">
                        <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("win_match")) ?></label>
                        <?php echo $form->renderInputTag("win_match", array('class' => 'form-control')) ?>
                        <?php echo $validator->showError("win_match") ?>
                    </div>
                    <div class="form-group col-sm-2 col-xs-4">
                        <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("draw_match")) ?></label>
                        <?php echo $form->renderInputTag("draw_match", array('class' => 'form-control')) ?>
                        <?php echo $validator->showError("draw_match") ?>
                    </div>
                    <div class="form-group col-sm-2 col-xs-4">
                        <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("win_set")) ?></label>
                        <?php echo $form->renderInputTag("win_set", array('class' => 'form-control')) ?>
                        <?php echo $validator->showError("win_set") ?>
                    </div>
                    <div class="form-group col-sm-2 col-xs-4">
                        <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("draw_set")) ?></label>
                        <?php echo $form->renderInputTag("draw_set", array('class' => 'form-control')) ?>
                        <?php echo $validator->showError("draw_set") ?>
                    </div>
                    <div class="form-group col-sm-2 col-xs-4">
                        <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("cru")) ?></label>
                        <?php echo $form->renderInputTag("cru", array('class' => 'form-control')) ?>
                        <?php echo $validator->showError("cru") ?>
                    </div>
                   
                </div>  
                
                  <div class="row">
                    <div class="col-sm-12">
                        <button class="btn btn-primary" type="submit"> <?php echo $translator->translate('Save') ?> </button>
                    </div>
                </div>
            </form>
           
        </div>
    </section>
</section>
