<?php Core\Layout::getInstance()->startSlot('team_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/Team/view/team/_team_menu.php', array('team' => $team)) ?>
<?php Core\Layout::getInstance()->endSlot('team_menu') ?>

<section class="content-header">
    <h1>
        <?php echo $translator->translate('Team CRS settings') ?>
    </h1>
    
    <?php $layout->includePart(GLOBAL_DIR.'/templates/parts/_breadcrumb.php',array(
        'crumbs' => array(
            'Teams' => $router->link('teams_overview'),
            $team->getName() => $router->link('team_overview',array('id' =>$team->getId() )),
            'Team crs settings' => ''
            ))) ?>

</section>

<section class="content">
    <section class="box box-primary">
          <div class="box box-header">
              <a class="btn btn-primary" href="<?php echo $router->link('team_player_stat', array('team_id' => $team->getId())) ?>"><i class="fa fa-chevron-left"></i> <?php echo $translator->translate('Back to statistics') ?></a>
          </div>
        
        <div class="box-body">
            
             <?php if ($request->hasFlashMessage('crs_edit_success')): ?>
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $request->getFlashMessage('crs_edit_success') ?>
                </div>
            <?php endif; ?>
            
            <p>
            <?php echo $translator->translate('CRS INFO TEXT') ?>
            </p>
            
            <form action="" method="post">
                 <div class="row">
                    <div class="form-group col-sm-2 col-xs-4">
                        <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("goal")) ?></label>
                        <?php echo $form->renderInputTag("goal", array('class' => 'form-control')) ?>
                        <?php echo $validator->showError("goal") ?>
                    </div>
                    <div class="form-group col-sm-2 col-xs-4">
                        <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("assists")) ?></label>
                        <?php echo $form->renderInputTag("assists", array('class' => 'form-control')) ?>
                        <?php echo $validator->showError("assists") ?>
                    </div>
                    <div class="form-group col-sm-2 col-xs-4">
                        <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("game_win")) ?></label>
                        <?php echo $form->renderInputTag("game_win", array('class' => 'form-control')) ?>
                        <?php echo $validator->showError("game_win") ?>
                    </div>
                    <div class="form-group col-sm-2 col-xs-4">
                        <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("game_draw")) ?></label>
                        <?php echo $form->renderInputTag("game_draw", array('class' => 'form-control')) ?>
                        <?php echo $validator->showError("game_draw") ?>
                    </div>
                    <div class="form-group col-sm-2 col-xs-4">
                        <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("man_of_match")) ?></label>
                        <?php echo $form->renderInputTag("man_of_match", array('class' => 'form-control')) ?>
                        <?php echo $validator->showError("man_of_match") ?>
                    </div>
                  
                     <div class="form-group col-sm-2 col-xs-4">
                        <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("Shotout")) ?></label>
                        <?php echo $form->renderInputTag("shotout", array('class' => 'form-control')) ?>
                        <?php echo $validator->showError("shotout") ?>
                    </div>
                   
                </div>  
                
                  <div class="row">
                    <div class="col-sm-12">
                        <button class="btn btn-primary" type="submit"> <?php echo $translator->translate('Save') ?> </button>
                    </div>
                </div>
            </form>
           
        </div>
    </section>
</section>
