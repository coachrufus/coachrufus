
<section class="content-header">
    <h1>
        <?php echo $translator->translate('My teams') ?>
    </h1>
    
     <?php $layout->includePart(GLOBAL_DIR.'/templates/parts/_breadcrumb.php',array('crumbs' => array('Teams' => $router->link('teams_overview')))) ?>
</section>
<section class="content">
    
    <div class="row widget-grid">   
         <?php foreach ($waitingTeams as $waitingTeam): ?>
             <div class="col-md-4 col-sm-4 widget invitation-widget-panel">
             <?php $layout->includePart(MODUL_DIR . '/Player/view/Dashboard/_waiting_team.php',array('team' => $waitingTeam['team'],'teamPlayer' => $waitingTeam['player'],'sportEnum' => $sportEnum)) ?>
             </div>
          <?php endforeach; ?>
        
        
            <?php foreach ($teams as $team): ?>
              <div class="col-md-4 col-sm-6 widget team-widget-panel" data-team-id="<?php echo $team->getId() ?>">

              </div>
            <?php endforeach; ?>
         <div class="col-md-4">
                    <div class="box box-widget widget-user create-team-widget">
                        <div class="widget-user-header">
                            <h3 class="widget-user-username"><?php echo $translator->translate('Crate new team') ?></h3>
                        </div>
                        <div class="widget-user-image">
                            <a  class="ico ico-create-team" href="<?php echo $router->link('team_create') ?>"></a>
                        </div> 
                        <div class="box-footer">

                        </div>
                    </div>
                </div>
        
        <?php if(\Core\ServiceLayer::getService('security')->getIdentity()->getUser()->getResource() == 'flch'): ?>
             <?php $layout->includePart(MODUL_DIR . '/Team/view/team/_flch_team_create.php') ?>
        <?php endif; ?>
        
        <?php if(\Core\ServiceLayer::getService('security')->getIdentity()->getUser()->getResource() == 'group'): ?>
             <?php $layout->includePart(MODUL_DIR . '/Team/view/team/_group_team_create.php') ?>
        <?php endif; ?>
      </div>

    


</section>

<?php $layout->addJavascript('js/Lib/jquery.flot.js') ?>
<?php $layout->addJavascript('js/Lib/jquery.flot.pie.js') ?>
<?php $layout->addJavascript('js/TeamManager.js') ?>
<?php $layout->includePart(MODUL_DIR . '/Team/view/team/_send_invitation_modal.php') ?>

<?php $layout->startSlot('javascript') ?>
<script type="text/javascript">

    var team_manager = new TeamManager({
        'dataUrl': '<?php echo $router->link('widget_team_panel') ?>'
    });

     $('.team-widget-panel').each(function(key, element){
         team_manager.loadWidget($(element));
     });

/*
    team_manager = new TeamManager();

    $('.send-invitation').each(function (key, val) {
        team_manager.addSendInvitationTrigger($(val));
    });

*/




</script>

<?php $layout->endSlot('javascript') ?>