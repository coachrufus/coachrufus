<?php Core\Layout::getInstance()->startSlot('team_menu') ?>  
   <?php $layout->includePart(MODUL_DIR.'/Team/view/team/_team_menu.php',array('team' => $team) ) ?>
<?php Core\Layout::getInstance()->endSlot('team_menu') ?>




<div class="container-fluid">
    <?php foreach ($teamMatches as $teamMatch): ?>
        <table class="table">
            
            <thead>
                <tr><td colspan="5"><?php echo $teamMatch->getMatchDate()->format('d.m.Y H:i:s') ?></td></tr>
                <tr>
                    <td>Player</td>
                    <td>Goals</td>
                    <td>Assist</td>
                    <td>Saves</td>
                    <td>MoM</td>
                    <td>Rate</td>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($teamMatch->getPlayers() as $playerMatch): ?>
                
                
                
                
                    <tr>
                        <td><?php echo $playerMatch->getPlayerName() ?></td>
                        <td><?php echo $playerMatch->getGoals() ?></td>
                        <td><?php echo $playerMatch->getAssists() ?></td>
                        <td><?php echo $playerMatch->getSaves() ?></td>
                        <td><?php echo $playerMatch->getManOfMatch() ?></td>
                        <td>
                            <?php if($matchManager->userCanRatePlayerMatch($user,$playerMatch)): ?>
                                 <a class="score_trigger" data-score="1" data-match="<?php echo $playerMatch->getId() ?>" data-player="<?php echo $playerMatch->getPlayerId() ?>" href="#">1</a> 
                                <a class="score_trigger" data-score="2" data-match="<?php echo $playerMatch->getId() ?>" data-player="<?php echo $playerMatch->getPlayerId() ?>" href="#">2</a> 
            
                            <?php else: ?>
                                Your vote: <?php echo $matchManager->getPlayerRatingByAuthor($user,$playerMatch)  ?>, 
                            <?php endif; ?>
                                 total match vote: <?php echo $matchManager->getPlayerRating($playerMatch)  ?>
                                   </tr>
                <?php endforeach; ?>
            </tbody>

        </table>
    <?php endforeach; ?>
</div>


<?php $layout->startSlot('javascript') ?>
 <script src="/dev/js/rufus/team/team.js?ver=<?php echo VERSION ?>"></script>
    <script type="text/javascript">

        TeamPlayersManager = new TeamPlayersManager();
        TeamPlayersManager.bindTriggers();
   </script>
<?php $layout->endSlot('javascript') ?>
