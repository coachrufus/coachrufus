<?php Core\Layout::getInstance()->startSlot('team_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/Team/view/team/_team_menu.php', array('team' => $team)) ?>
<?php Core\Layout::getInstance()->endSlot('team_menu') ?>


<section class="content-header">
    <h1>
        <?php echo $translator->translate('Attendance') ?>
    </h1>

    <?php
    $layout->includePart(GLOBAL_DIR . '/templates/parts/_breadcrumb.php', array(
        'crumbs' => array(
            'Teams' => $router->link('teams_overview'),
            $team->getName() => $router->link('team_overview', array('id' => $team->getId())),
            'Attendance' => ''
)))
    ?>
</section>
<section class="content">
    
    
     <div class="row">
     <div class="col-sm-12">
            <div class="nav-tabs-custom attendance-tabs">
                <?php if('api' != $layoutTheme): ?>


                <ul class="nav nav-tabs">
                    <li class="active"><a  href="#" data-toggle="tab"><?php echo $translator->translate('Attendance') ?></a></li>
                    <li><a href="<?php echo $router->link('team_season_billing',array('id' => $season->getId(),'team_id' => $team->getId())) ?>"><?php echo $translator->translate('attendance.billing') ?></a></li>
                </ul>
                <?php endif; ?>
                <div class="tab-content">
                  
                        <form action="" class="form-inline">
                            <?php echo $filterForm->renderSelectTag('event_type', array('class' => 'form-control')) ?>
                            <?php echo $translator->translate('From') ?>:
                            <?php echo $filterForm->renderInputTag('from', array('class' => 'form-control')) ?>
                            <?php echo $translator->translate('To') ?>: <?php echo $filterForm->renderInputTag('to', array('class' => 'form-control')) ?>
                            <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-refresh"></i> <?php echo $translator->translate('Filter') ?></button>
                        </form>
                    
                    <div class="box-body" style="overflow:auto;">
            <table id="table_attendance" class="table table-condensed resp-table">
                <thead>
                    <tr>
                        <th>

                        </th>

<?php foreach ($eventsList as $dayEvents): ?>
                                <?php foreach ($dayEvents as $event): ?>
                                <th>

        <?php echo $event->getFormatedCurrentDate() ?>, <?php echo $event->getPlaygroundName() ?><br />
                                    <div class="members-attendance" id="members-attendance-<?php echo $event->getUid() ?>">
                                        <span><?php echo $translator->translate('Attendance') ?>:</span> <span class="members-attendance-in"><?php echo $event->getAttendanceInSum() ?></span> /<?php echo $event->getCapacity() ?>
                                        <div class="progress progress-xs">
                                            <div class="progress-bar progress-bar-success progress-bar-green" role="progressbar" aria-valuenow="<?php echo $event->getAttendanceInSum() ?>" aria-valuemin="0" aria-valuemax="<?php echo $event->getCapacity() ?>" style="width:  <?php echo round($event->getAttendanceInSum() / $event->getCapacity() * 100) ?>%">

                                            </div>
                                        </div>
                                    </div>
                                </th>
    <?php endforeach; ?>
<?php endforeach; ?>
                    </tr>
                </thead>
                <tbody>

<?php foreach ($attendanceMatrix['players'] as $playerId => $matrixRow): ?>
                        <tr class="<?php echo ($matrixRow['player_id'] == $user->getId()) ? 'user-row' : 'other-user-row' ?>">
                            <td class="player_name"><?php echo $matrixRow['player'] ?></td>

    <?php foreach ($matrixRow['events'] as $eventId => $cell): ?>
                                <td> 
                                    <span class="attenndance_event_name_mobil">
                                        <strong><?php echo $cell['event']->getName() ?>
        <?php echo $cell['event']->getFormatedCurrentDate() ?><br />
                                            <em><?php echo $cell['event']->getPlaygroundName() ?></em></strong>
                                    </span>

                                    <?php if (($cell['editable'] == true or \Core\ServiceLayer::getService('security')->getIdentity()->hasTeamPermission('manageEvents', $team)) && $cell['event']->getClosed() == false): ?>

                                        <a href="#"
                                           data-url="<?php echo $router->link('change_player_attendance') ?>" 
                                           data-rel="in" 
                                           data-event="<?php echo $cell['event']->getId() ?>" 
                                           data-eventdate="<?php echo $cell['event']->getCurrentDate()->format('Y-m-d') ?>" 
                                           data-player="<?php echo $playerId ?>" 
                                           data-team-role="<?php echo $cell['team_role'] ?>"
                                           data-team-player="<?php echo $cell['team_player_id'] ?>" 
                                           class="in attendance_trigger attendance_trigger_btn <?php echo ($cell['status'] == 1 ) ? 'accept' : '' ?>">
                                            <i class="ico ico-check"></i>
                                        </a>

                                        <a href="#"  data-url="<?php echo $router->link('change_player_attendance') ?>" 
                                           data-rel="out" 
                                           data-event="<?php echo $cell['event']->getId() ?>" 
                                           data-eventdate="<?php echo $cell['event']->getCurrentDate()->format('Y-m-d') ?>" 
                                           data-player="<?php echo $playerId ?>" 
                                           data-team-role="<?php echo $cell['team_role'] ?>"
                                           data-team-player="<?php echo $cell['team_player_id'] ?>" 
                                           class="out attendance_trigger attendance_trigger_btn    <?php echo ($cell['status'] == 3 ) ? 'deny' : '' ?>">
                                            <i class="ico ico-close"></i>
                                        </a>
                                    <?php else: ?>
                                        <?php if ($cell['status'] == 3): ?>
                                            <span class="attendance_trigger_btn deny"><i class="ico ico-close"></i></span>
                                        <?php elseif ($cell['status'] == 1): ?>
                                            <span class="attendance_trigger_btn accept "> <i class="ico ico-check"></i></span>
                                        <?php else: ?>
                                            <span class="attendance_trigger_btn"><i class="ico ico-question"></i></span>
                                        <?php endif; ?>                       
                                    <?php endif; ?>

                                     <?php if (\Core\ServiceLayer::getService('security')->getIdentity()->hasTeamPermission('manageEvents', $team)): ?>
                                            <br />
                                            <input 
                                                type="text" 
                                                value="<?php echo $cell['fee'] ?>" 
                                                placeholder="0"  
                                                size="3" 
                                                min="0" 
                                                max="9999" 
                                                data-url="<?php echo $router->link('change_player_fee') ?>" 
                                                data-event="<?php echo $cell['event']->getId() ?>" 
                                                data-eventdate="<?php echo $cell['event']->getCurrentDate()->format('Y-m-d') ?>" 
                                                data-player="<?php echo $playerId ?>" 
                                                data-team-player="<?php echo $cell['team_player_id'] ?>" 
                                            <?php echo ($cell['status'] !== '1' && $cell['status'] !== '3' ) ? 'disabled' : ''; ?>
                                                class="attendance_fee_trigger attendance_fee"/>
                                            <label>€</label>
                                        <?php else: ?>
                                            <?php if ($cell['status'] == 3 or $cell['status'] == 1): ?>
                                                <br />
                                                <?php echo $cell['fee'] !== null ? $cell['fee'] : 0; ?> €
                <?php endif; ?>
                                        <?php endif; ?>
                                            
                                </td>
                        <?php endforeach; ?>
                        </tr>
                <?php endforeach; ?>
                </tbody>
                <?php if (\Core\ServiceLayer::getService('security')->getIdentity()->hasTeamPermission('manageEvents', $team)): ?>
                    <tbody id="costs">
                    <th>Náklady</th>
    <?php foreach ($attendanceMatrix['eventMatrix'] as $eventIndex => $cell): ?>
                        <td> 
                            <input 
                                type="text" 
                                value="<?php echo $cell['costs'] ?>" 
                                placeholder="0"  
                                size="3" 
                                min="0" 
                                max="9999" 
                                data-url="<?php echo $router->link('change_event_costs') ?>" 
                                data-event="<?php echo $cell['event']->getId() ?>" 
                                data-eventdate="<?php echo $cell['event']->getCurrentDate()->format('Y-m-d') ?>"
                                class="costs_trigger"/>
                            <label>€</label>
                        </td>
                    <?php endforeach; ?>
                    </tbody>
<?php endif; ?>
            </table>

            <div id="resp_attendance">
<?php foreach ($eventsList as $dayIndex => $dayEvents): ?>
    <?php foreach ($dayEvents as $event): ?>
                        <div class="resp_attendance_item">
                            <h3><?php echo $event->getFormatedCurrentDate() ?><br /> <?php echo $event->getPlaygroundName() ?></h3>
                            <div class="members-attendance" id="members-attendance-<?php echo $event->getUid() ?>">
                                <span><?php echo $translator->translate('Attendance') ?>:</span> <span class="members-attendance-in"><?php echo $event->getAttendanceInSum() ?></span> /<?php echo $event->getCapacity() ?>
                                <div class="progress progress-xs">
                                    <div class="progress-bar progress-bar-success progress-bar-green" role="progressbar" aria-valuenow="<?php echo $event->getAttendanceInSum() ?>" aria-valuemin="0" aria-valuemax="<?php echo $event->getCapacity() ?>" style="width:  <?php echo round($event->getAttendanceInSum() / $event->getCapacity() * 100) ?>%">

                                    </div>
                                </div>
                            </div>

                            <table>

        <?php foreach ($attendanceMatrix['eventMatrix'][$dayIndex . '-' . $event->getId()]['players'] as $cell): ?>
                                    <tr>
                                        <td><?php echo $cell['player'] ?></td>
                                        <td>
            <?php if ($cell['editable'] == true or \Core\ServiceLayer::getService('security')->getIdentity()->hasTeamPermission('manageEvents', $team)): ?>



                                                <a href="#"
                                                   data-url="<?php echo $router->link('change_player_attendance') ?>" 
                                                   data-rel="in" 
                                                   data-event="<?php echo $event->getId() ?>" 
                                                   data-eventdate="<?php echo $event->getCurrentDate()->format('Y-m-d') ?>" 
                                                   data-player="<?php echo $cell['player_id '] ?>" 
                                                   data-team-role="<?php echo $cell['team_role'] ?>"
                                                   data-team-player="<?php echo $cell['team_player_id'] ?>" 
                                                   class="in attendance_trigger attendance_trigger_btn <?php echo ($cell['status'] == 1 ) ? 'accept' : '' ?>">
                                                    <i class="ico ico-check"></i>
                                                </a>

                                                <a href="#"  data-url="<?php echo $router->link('change_player_attendance') ?>" 
                                                   data-rel="out" 
                                                   data-event="<?php echo $event->getId() ?>" 
                                                   data-eventdate="<?php echo $event->getCurrentDate()->format('Y-m-d') ?>" 
                                                   data-player="<?php echo $cell['player_id '] ?>" 
                                                   data-team-role="<?php echo $cell['team_role'] ?>"
                                                   data-team-player="<?php echo $cell['team_player_id'] ?>" 
                                                   class="out attendance_trigger attendance_trigger_btn    <?php echo ($cell['status'] == 3 ) ? 'deny' : '' ?>">
                                                    <i class="ico ico-close"></i>
                                                </a>

                                            <?php else: ?>
                                                <?php if ($cell['status'] == 3): ?>
                                                    <span class="attendance_trigger_btn deny"><i class="ico ico-close"></i></span>
                                                <?php elseif ($cell['status'] == 1): ?>
                                                    <span class="attendance_trigger_btn accept "> <i class="ico ico-check"></i></span>
                                                    <?php else: ?>
                                                    <span class="attendance_trigger_btn"><i class="ico ico-question"></i></span>
                <?php endif; ?>
            <?php endif; ?>

                                        </td>
                                    </tr>


                        <?php endforeach; ?>
                            </table>
                        </div>
    <?php endforeach; ?>
<?php endforeach; ?>

            </div>
        </div>
                </div>
            </div>
        </div>
     </div>

   

</section>
<?php $layout->addJavascript('js/TeamEventManager.js') ?>
<?php $layout->addStylesheet('plugins/pickadate/themes/classic.css') ?>
<?php $layout->addStylesheet('plugins/pickadate/themes/classic.date.css') ?>
<?php $layout->addJavascript('plugins/pickadate/picker.js') ?>
<?php $layout->addJavascript('plugins/pickadate/picker.date.js') ?>
<?php $layout->addJavascript('plugins/pickadate/translations/' . LANG . '.js') ?>
<?php $layout->startSlot('javascript') ?>
<script type="text/javascript">


    $('#filter_from').pickadate({'format': 'dd.mm.yyyy', 'formatSubmit': 'dd.mm.yyyy'});
    $('#filter_to').pickadate({'format': 'dd.mm.yyyy', 'formatSubmit': 'dd.mm.yyyy'});

    event_manger = new TeamEventsManager();
    event_manger.init();



</script>
<?php $layout->endSlot('javascript') ?>