<div class="modal fade" id="send-invitation-modal" tabindex="-1" role="dialog" aria-labelledby="<?php echo $translator->translate('Send invitation') ?>">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $translator->translate('Send invitation') ?></h4>
      </div>
      <div class="modal-body">
          <form action="<?php echo $router->link('team_send_invitation') ?>" method="post" class="send-invitation-form">
              <input id="team_invitation" type="hidden" name="hash" value="" />
              <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                    <input name="invitation_email" type="email" class="form-control" placeholder="<?php echo $translator->translate('Email') ?>">
                     <span class="input-group-btn">
                      <button class="btn btn-primary" type="submit"><?php echo $translator->translate('Send') ?></button>
                    </span>
                  </div>
             
          </form>
                       

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>

