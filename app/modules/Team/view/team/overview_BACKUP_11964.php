<?php Core\Layout::getInstance()->startSlot('team_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/Team/view/team/_team_menu.php', array('team' => $team)) ?>
<?php Core\Layout::getInstance()->endSlot('team_menu') ?>



<section class="content-header">
    <h1>
        <?php echo $team->getName() ?>
    </h1>

    <?php
    $layout->includePart(GLOBAL_DIR . '/templates/parts/_breadcrumb.php', array(
    'crumbs' => array(
    'Teams' => $router->link('teams_overview'),
    $team->getName() => ''
    )))
    ?>
</section>
<section class="content">

    

    <div class="row">

        <div class="col-md-8">
             <link rel="stylesheet" type="text/css" href="/dev/js/PlayersComparison/Sources/css/performance.css">
                        <div id="Performance">
                                <div class="PerformanceBox panel panel-default">
                                        <div class="loading"></div>
                                </div>
                        </div>
            
            
            

            <div class="panel panel-default">
                <div class="panel-heading">

                    <strong><?php echo $translator->translate('Members') ?></strong>
                    <?php if (\Core\ServiceLayer::getService('security')->getIdentity()->hasTeamPermission('managePlayers', $team)): ?>
                    <div class="panel-toolbar">
                        <a class="color-link tool-item tool-item-link" href="<?php echo $router->link('team_players_list', array('team_id' => $team->getId())) ?>" class="btn btn-default">

                            <i class="ico ico-plus-green"></i> <?php echo $translator->translate('Add members') ?></a>
                    </div>
                    <?php endif; ?>
                </div>
                <div class="panel-body">
                    <?php foreach ($players as $player): ?>
                    <div class="box box-widget collapsed-box">
                        <div class="box-header with-border">
                            <div class="user-block">

                                <div class="img_round_wrap member_img_round" style="background-image: url('<?php echo $player->getMidPhoto() ?>');">

                                </div>
                                <span class="username"><?php echo $player->getFirstName() ?> <?php echo $player->getLastName() ?></span>
                                <div class="description"><a href="mailto:<?php echo $player->getEmail() ?>"><?php echo $player->getEmail() ?></a>

                                    <div class="starbox-fake ghosting" data-start-value="<?php echo $player->getRating() ?>"> </div>

                                    <?php foreach ($player->getPlayerSports() as $playerSport): ?>
                                    <span class="label label-info"><?php echo $playerSport->getSport()->getName() ?></span>
                                    <?php endforeach; ?>
                                </div>
                            </div><!-- /.user-block -->
                        </div><!-- /.box-header -->
                    </div>

                    <?php endforeach; ?>
                </div>
            </div>        
        </div>

        <div class="col-md-4">
            <?php $layout->includePart(MODUL_DIR . '/Event/view/widget/_small_calendar.php') ?>
            <div id="PlayersComparison">
                <div class="PlayersComparsionBox panel panel-default">
                    <div class="loading"></div>
                </div>
            </div>
        </div>
    </div>
</section>


<?php $layout->addJavascript('js/SmallCalendar.js') ?>
<?php $layout->addStylesheet('plugins/starbox/css/jstarbox.css') ?>
<?php $layout->addJavascript('plugins/starbox/jstarbox.js') ?>
<?php $layout->addJavascript('js/TeamEventManager.js') ?>
<?php $layout->addJavascript('js/TeamWallManager.js') ?>
<?php $layout->addJavascript('js/ScoutingManager.js') ?>
<?php $layout->addJavascript('/plugins/select2/select2.full.min.js') ?>

<?php $layout->addStylesheet('plugins/pickadate/themes/classic.css') ?>
<?php $layout->addStylesheet('plugins/pickadate/themes/classic.date.css') ?>
<?php $layout->addJavascript('plugins/pickadate/picker.js') ?>
<?php $layout->addJavascript('plugins/pickadate/picker.date.js') ?>

<?php $layout->addStylesheet('plugins/timepicker/bootstrap-timepicker.min.css') ?>
<?php $layout->addJavascript('plugins/timepicker/bootstrap-timepicker.js') ?>


<?php $layout->addJavascript('plugins/fileupload/jquery.ui.widget.js') ?>
<?php $layout->addJavascript('plugins/fileupload/jquery.iframe-transport.js') ?>
<?php $layout->addJavascript('plugins/fileupload/jquery.fileupload.js') ?>
<?php $layout->addJavascript('plugins/textcomplete/jquery.textcomplete.min.js') ?>
<<<<<<< HEAD
<?php  $layout->startSlot('javascript') ?>

<script src="/js/Lib/jquery.flot.js"></script> 
<script src="/js/Lib/jquery.flot.pie.js"></script> 
<script src="/js/Lib/jquery.flot.stack.js"></script> 
<script src="/js/Lib/jquery.flot.tooltip.min.js"></script>
<script src="/js/Lib/curvedLines.js"></script>
<script src="/js/Lib/JUMFlot/jquery.flot.JUMlib.js"></script>
<script src="/js/Lib/JUMFlot/jquery.flot.spider.js?<?php echo time() ?>"></script>
=======

>>>>>>> 0983677779b4c6de96d7b140d0f5e6caeabb3953
<link rel="stylesheet" type="text/css" href="/js/plugins/typehead/typeaheadjs.css"></link>
<?php $layout->addStylesheet('js/PlayersComparison/Sources/css/comparsion.css') ?>




<?php $layout->addJavascript('js/Lib/jquery.flot.js') ?>
<?php $layout->addJavascript('js/Lib/jquery.flot.pie.js') ?>
<?php $layout->addJavascript('js/Lib/jquery.flot.stack.js') ?>
<?php $layout->addJavascript('js/Lib/jquery.flot.tooltip.min.js') ?>
<?php $layout->addJavascript('js/Lib/curvedLines.js') ?>
<?php $layout->addJavascript('js/Lib/JUMFlot.min.js') ?>

<?php $layout->addJavascript('js/PlayersComparison/bundle/bundle.js') ?>

<?php $layout->startSlot('javascript') ?>




<script>
    window.fbAsyncInit = function () {
        FB.init({
            appId: '1142117885858668',
            xfbml: true,
            version: 'v2.7'
        });
    };

    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));




    $(document).on('click', '.share-fb-trigger', function (e) {
        e.preventDefault();

        /*
         FB.api(
         "/",
         {
         "id": "http:\/\/www.imdb.com\/title\/tt2015381\/"
         },
         function (response) {
         console.log(response);
         if (response && !response.error) {
         
         }
         }
         );
         */


        /*target_post_cnt = $(this).attr('data-post');
         FB.ui(
         {
         method: 'share_open_graph ',
         href: 'http://app.coachrufus.com',
         quote:  target_post_cnt,
         hashtag: '#coachrufus'
         },
         // callback
         function(response) {
         if (response && !response.error_message) {
         alert('Posting completed.');
         } else {
         alert('Error while posting.');
         }
         }
         );
         */

        /*
         FB.login(function(){
         FB.api('/me/feed', 'post', {message: 'test'});
         }, {scope: 'publish_actions'});
         */

        /*
         target_post_id = $(this).attr('data-post');
         target_post = $('#'+target_post_id);
         target_post_cnt = target_post.find('.post-cnt').html();
         console.log(target_post_cnt);
         FB.ui(
         {
         method: 'share',
         href: 'http://app.coachrufus.com',
         quote:  target_post_cnt,
         hashtag: '#coachrufus'
         },
         // callback
         function(response) {
         if (response && !response.error_message) {
         alert('Posting completed.');
         } else {
         alert('Error while posting.');
         }
         }
         );
         */
    });
</script>


<script type="text/javascript">
    $('.starbox-fake').each(function (index, val) {
        $(val).starbox({
            average: $(this).attr('data-start-value'),
            changeable: false
        })
    });

    small_calendar = new SmallCalendar({
        'dataUrl': '<?php echo $router->link('rest_event_get_month_grid') ?>',
        'localityManager': new LocalityManager()
    });
    small_calendar.initLoad();

    /*
     wall_manger = new TeamWallManager();
     wall_manger.bindTriggers();
     */

    event_manger = new TeamEventsManager();
    event_manger.init();
    var scouting_manger = new ScoutingManager({
        'dataUrl': '<?php echo $router->link('rest_last_scouting') ?>'
    });
    scouting_manger.loadWidget();


    $(".select2").select2();
    $(".select2").select2();
    jQuery('#scouting_game_time').pickadate({'format': 'dd.mm.yyyy'});
    jQuery('#scouting_valid_to').pickadate({'format': 'dd.mm.yyyy'});

    jQuery("#scouting_game_time_time").timepicker({showInputs: false, showMeridian: false, minuteStep: 5});


    function initLocalityManager()
    {
        locality_manger = new LocalityManager();
        locality_manger.google = google;
        locality_manger.createSearchInput('scouting-form-location1');

        locality_manger2 = new LocalityManager();
        locality_manger2.google = google;
        locality_manger2.createSearchInput('scouting-form-location2');

        locality_manger3 = new LocalityManager();
        locality_manger3.google = google;
        locality_manger3.createSearchInput('scouting-form-location3');

        locality_manger4 = new LocalityManager();
        locality_manger4.google = google;
        locality_manger4.createSearchInput('scouting-form-location4');

    }

</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXU1duK54f1gIsRy_xc7z4uQURovsZdE8&libraries=places&callback=initLocalityManager" async defer></script>
<?php $layout->endSlot('javascript') ?>



