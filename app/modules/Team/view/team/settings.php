<?php
$translator = Core\ServiceLayer::getService('translator');
?>

<?php Core\Layout::getInstance()->startSlot('team_menu') ?>  
    <?php $layout->includePart(MODUL_DIR . '/Team/view/team/_team_menu.php', array('team' => $team)) ?>
<?php Core\Layout::getInstance()->endSlot('team_menu') ?>


<?php if ($request->hasFlashMessage('_add_success')): ?>
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <?php echo $request->getFlashMessage('_add_success') ?>
    </div>
<?php endif; ?>

<form action="" class="form-horizontal form-bordered" method="post">
    <?php echo $validator->showAllErrors() ?>

    <div class="form-group">
        <label class="col-sm-3 control-label"><?php echo $translator->translate('Name') ?></label>
        <div class="col-sm-6">

            <?php echo $form->getEntity()->getName() ?>
        </div>
    </div>
    
    
        <div class="form-group">
        <label class="col-sm-3 control-label">Playground</label>
        <div class="col-sm-6">
            <?php echo $validator->showError("playgrounds") ?>
             <div id="playgrounds" class="well" <?php echo ($form->hasPlaygrounds()) ? 'style="display:block;"' : '' ?>>
                <strong>Team playgrounds</strong><br />
                <div id="team_playgrounds_container">
                    
                    
                    
                    <?php foreach($form->getFieldValue('playgrounds') as $playground): ?>
                    <div class="playground_item">
                        <?php echo $playground['alias_name'] ?>
                        <input type="hidden" name="exist_playgrounds[<?php echo $playground['alias_id'] ?>][id]" value="<?php echo $playground['alias_id'] ?>">
                       
                      
                        
                       
                        <a class="playground_item_remove" href=""> Remove</a>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
            
             <input id="pac-input" class="controls" type="text" placeholder="Názov obce, ulica atd ">
            <div id="map_wrap">
                <div id="map"></div>
            </div>
            
           
            
             <div id="exist_playgrounds" class="well">
                 <strong>Playground near <em id="search_term"></em></strong><br />
                  <div id="exist_playgrounds_container">
                      
                  </div>
              
                 <div>or <a class="btn btn-primary add_new_playground" href="">add new playground</a></div>
            </div>
            
            
            
            
            
            <div id="playground_add_form" class="well">
               

                <div id="playground_dialog" title="Basic dialog">
                    <strong>New Playground</strong>
                    <div class="alert  alert-danger" id="add_new_playground_alert"></div>
                    
                    
                    <input id="add_playground_locality" type="hidden" name="playground[locality" value="" />
                    <input id="add_playground_place_id" type="hidden" name="playground[place_id]" value="" />
                    <div class="row">
                     <div class="col-sm-12">
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo $translator->translate($playgroundForm->getFieldLabel("name")) ?></label>
                            <div class="col-sm-6">
                                <?php echo $playgroundForm->renderInputTag("name", array('class' => 'form-control')) ?>
                                <?php echo $playgroundValidator->showError("name") ?>
                            </div>
                        </div>
                     </div>
                         <div class="col-sm-12">
                          <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo $translator->translate($playgroundForm->getFieldLabel("city")) ?></label>
                            <div class="col-sm-6">
                                <?php echo $playgroundForm->renderInputTag("city", array('class' => 'form-control')) ?>
                                <?php echo $playgroundValidator->showError("city") ?>
                            </div>
                        </div>
                             </div>
                         <div class="col-sm-12">
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo $translator->translate($playgroundForm->getFieldLabel("street")) ?></label>
                            <div class="col-sm-6">
                                <?php echo $playgroundForm->renderInputTag("street", array('class' => 'form-control')) ?>
                                <?php echo $playgroundValidator->showError("street") ?>
                            </div>
                        </div>
                               </div>
                         <div class="col-sm-12">
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo $translator->translate($playgroundForm->getFieldLabel("street_number")) ?></label>
                            <div class="col-sm-6">
                                <?php echo $playgroundForm->renderInputTag("street_number", array('class' => 'form-control')) ?>
                                <?php echo $playgroundValidator->showError("street_number") ?>
                            </div>
                        </div>
                               </div>
                         <div class="col-sm-12">
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo $translator->translate($playgroundForm->getFieldLabel("description")) ?></label>
                            <div class="col-sm-6">
                                <?php echo $playgroundForm->renderInputTag("description", array('class' => 'form-control')) ?>
                                <?php echo $playgroundValidator->showError("description") ?>
                            </div>
                        </div>
                     </div>
                    <br />
                      <div class="col-sm-12">
                    

                        <div id="add_form_map" class="mobile-map"></div>


                     </div>
                    <button class="playground_dialog_save_trigger" type="submit">Save</button>
                      </div>
                  </div>
                
            </div>
            
           
          
           
        </div>
    </div>
    
    
    

    <div class="form-group">
        <label class="col-sm-3 control-label"><?php echo $translator->translate($form->getFieldLabel("sport_id")) ?></label>
        <div class="col-sm-6">
            <?php echo $form->renderSelectTag("sport_id", array('class' => 'form-control')) ?>
            <?php echo $validator->showError("sport_id") ?>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label"><?php echo $translator->translate($form->getFieldLabel("age_from")) ?></label>
        <div class="col-sm-6">
            <?php echo $form->renderInputTag("age_from", array('class' => 'form-control')) ?>
            <?php echo $validator->showError("age_from") ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label"><?php echo $translator->translate($form->getFieldLabel("age_to")) ?></label>
        <div class="col-sm-6">
            <?php echo $form->renderInputTag("age_to", array('class' => 'form-control')) ?>
            <?php echo $validator->showError("age_to") ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label"><?php echo $translator->translate($form->getFieldLabel("status")) ?></label>
        <div class="col-sm-6">
            <?php echo $form->renderSelectTag("status", array('class' => 'form-control')) ?>
            <?php echo $validator->showError("status") ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label"><?php echo $translator->translate($form->getFieldLabel("description")) ?></label>
        <div class="col-sm-6">
            <?php echo $form->renderTextareaTag("description", array('class' => 'form-control')) ?>
            <?php echo $validator->showError("description") ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label"><?php echo $translator->translate($form->getFieldLabel("phone")) ?></label>
        <div class="col-sm-6">
            <?php echo $form->renderInputTag("phone", array('class' => 'form-control')) ?>
            <?php echo $validator->showError("phone") ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label"><?php echo $translator->translate($form->getFieldLabel("email")) ?></label>
        <div class="col-sm-6">
            <?php echo $form->renderInputTag("email", array('class' => 'form-control')) ?>
            <?php echo $validator->showError("email") ?>
        </div>
    </div>





    <div class="row">
        <div class="col-sm-6 col-sm-offset-3">
            <button class="btn btn-primary">Submit</button>&nbsp;
            <button class="btn btn-default">Cancel</button>
        </div>
    </div>
</form>
<?php  $layout->startSlot('javascript') ?>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXU1duK54f1gIsRy_xc7z4uQURovsZdE8&libraries=places&callback=TeamLocality" async defer></script>
<?php  $layout->endSlot('javascript') ?>