<?php Core\Layout::getInstance()->startSlot('team_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/Team/view/team/_team_menu.php', array('team' => $team)) ?>
<?php Core\Layout::getInstance()->endSlot('team_menu') ?>



<section class="content-header">
    <h1>
        <?php echo $team->getName() ?>
    </h1>
    
    <?php $layout->includePart(GLOBAL_DIR.'/templates/parts/_breadcrumb.php',array(
        'crumbs' => array(
            'Teams' => $router->link('teams_overview'),
            $team->getName() => ''
            ))) ?>
</section>
<section class="content">

			<div class="row">
				<div class="col-md-12">
					<link rel="stylesheet" type="text/css" href="/dev/js/PlayersComparison/Sources/css/performance.css">
					<div id="Performance">
						<div class="PerformanceBox panel panel-default">
							<div class="loading"></div>
						</div>
					</div>
				</div>
			</div>


            <div class="row">
                <div class="col-md-4">
                    
                    <div class="panel panel-default  hp-events">
                    <div class="panel-heading">
                        <i class="fa fa-calendar"></i> 
                        <strong><?php echo $translator->translate('Events') ?></strong>
                          <?php if( \Core\ServiceLayer::getService('security')->getIdentity()->hasTeamPermission('manageEvents',$team)): ?>
                            
                            <div class="pull-right">
                               <a  href="<?php echo $router->link('team_events_calendar',array( 'team_id' => $team->getId() )) ?>" type="button" class="btn btn-default"><i class="fa fa-calendar"></i></a>
                                <a href="<?php echo $router->link('create_team_event',array('team_id' =>  $team->getId())) ?>" class="btn btn-default"><i class="fa fa-plus-square"> <?php echo $translator->translate('Add event') ?></i></a>
                              </div>

                            <?php endif; ?>
                    </div>
                    
                  
                                      <div class="panel-body">

                    <?php $i = 0;
                    foreach ($eventsList as $dayEvents): ?>
    <?php foreach ($dayEvents as $event): ?>
                           <?php $layout->includePart(MODUL_DIR . '/Event/view/Default/_event_panel_row.php', array('i' => $i,'event' => $event,'attendanceList' => $attendanceList, 'team' => $team)) ?>
                           
                        <?php endforeach; ?>
<?php endforeach; ?>



                </div>
                    </div>
                </div>
                
                 <div class="col-md-4">

                        <link rel="stylesheet" type="text/css" href="/dev/js/PlayersComparison/Sources/css/comparsion.css">
                        <div id="PlayersComparison">
                            <div class="PlayersComparsionBox panel panel-default">
                                <div class="loading"></div>
                            </div>
                        </div>

                       <div class="panel panel-default">
                           <div class="panel-heading">
                               <?php echo $translator->translate('Post') ?>
                               
                              
                               
                                
                                <span class="btn btn-default btn-file">
                                    <?php echo $translator->translate('Add image') ?><input id="post_fileupload" type="file" name="post_image" data-url="<?php echo $router->link('tam_wall_upload_photo') ?>" />
                                 </span>
                                
                                
                               
                           </div>
                            <div class="panel-body">
                                
                                 <?php $layout->includePart(MODUL_DIR.'/Team/view/wall/_remove_post_modal.php') ?>
                     <div class="post row">
                            <form action="<?php echo $router->link('create_team_wall_post') ?>" method="post" class="team-wall-post-form ajax-form">

                                <?php echo $wallPostForm->renderInputHiddenTag('add_content',array('id' => 'post_add_content')) ?>
                                <?php echo $wallPostForm->renderInputHiddenTag('team_id') ?>
                                 <?php echo $wallPostForm->renderInputHiddenTag('body', array('class' => 'form-control','id' => 'post_body', 'style' => 'width:100%;')) ?>
                                
                                
                                <div class="form-group col-sm-12" data-error-bind="body">
                                    <label><?php echo $translator->translate('Add new post') ?></label>
                                    <div  class="editable_area" id="record_body" contenteditable="true" style="width: 100%; min-height: 100px; border: 1px solid #000;"></div>
                                    
                                </div>
                                
                                 <div class="form-group col-sm-12 text-center" id="post_add_content_wrap">
                                    <i class="loader fa fa-spinner fa-spin fa-3x fa-fw"></i>
                                    
                                </div>
                                
                                 <div class="form-group col-sm-12 text-center" id="post_add_image_wrap"></div>
                                
                                 <div class="upload-progress">
                                    <span class="progress-num"></span>
                                   <div class="progress progress-sm active">
                                    <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                    </div>
                                  </div>
                                 </div>
                                

                                <div class="form-group col-sm-12">
                                    <button type="submit" class="btn btn-primary"><?php echo $translator->translate('Create') ?></button>
                                </div>
                            </form>
                        </div>
                     
                     <div id="wall_posts">
                        <?php foreach($wallPosts as $post): ?>
                            <!-- Post -->
                           <?php $layout->includePart(MODUL_DIR.'/Team/view/wall/_wall_line.php',array('post' => $post, 'isOwner' => ($post->getAuthorId() ==  \Core\ServiceLayer::getService('security')->getIdentity()->getUser()->getId()) ? true : false  )) ?>
                        <?php endforeach; ?>
                      </div>
                                
                       <?php if(!$wallPosts->allPostLoaded()): ?>
                        <a class="btn btn-primary load-wall_posts" data-start="<?php echo $post->getId() ?>" data-team="<?php echo $team->getId() ?>" href="<?php echo $router->link('tam_wall_load_posts') ?>" ><?php echo $translator->translate('Load More') ?></a>
                       <?php endif; ?>  
                                
                     
                     </div>
                     </div>
                     
                </div>
                
                <div class="col-md-4">
                   <div class="panel panel-default">
                         <div class="panel-heading">
                            
                            <strong><?php echo $translator->translate('Members') ?></strong>
                        
                        
                           
                           <?php if( \Core\ServiceLayer::getService('security')->getIdentity()->hasTeamPermission('managePlayers',$team)): ?>
                            
                            <div class="pull-right">
                               
                                <a href="<?php echo $router->link('team_players_list',array('team_id' =>  $team->getId())) ?>" class="btn btn-default"><i class="fa fa-plus-square"> <?php echo $translator->translate('Edit members') ?></i></a>
                              </div>

                            <?php endif; ?>
                        
                        
                        </div>
                      <div class="panel-body">
                    <?php foreach ($players as $player): ?>
                        <div class="box box-widget collapsed-box">
                            <div class="box-header with-border">
                                <div class="user-block">
                                    
                                    <div class="img_round_wrap member_img_round" style="background-image: url('<?php echo $player->getMidPhoto() ?>');">

                                    </div>
    
                                    
                                 
                                    <span class="username"><?php echo $player->getFirstName() ?> <?php echo $player->getLastName() ?></span>
                                    <div class="description"><a href="mailto:<?php echo $player->getEmail() ?>"><?php echo $player->getEmail() ?></a>
                                    
                                      <div class="starbox-fake ghosting" data-start-value="<?php echo  $player->getRating() ?>"> </div>
                                            
                                            <?php foreach($player->getPlayerSports() as $playerSport): ?>
                                                <span class="label label-info"><?php echo $playerSport->getSport()->getName() ?></span>
                                            <?php endforeach; ?>
                                    </div>
                                </div><!-- /.user-block -->
                               
                            </div><!-- /.box-header -->
                            
                        
                        </div>

                    <?php endforeach; ?>
                </div>
                    </div>
                </div>
                
                <div class="col-md-4">
                      <div class="panel panel-default">
                        <div class="panel-heading"><strong><?php echo $translator->translate('Team scounting') ?></strong></div>
                        <div class="panel-body">
                             <?php echo $layout->renderControllerAction('CR\Scouting\ScoutingModule:Default:teamPanel')->getContent() ?>
                            
                             <?php echo $layout->renderControllerAction('CR\Scouting\ScoutingModule:Default:teamLookingPlayer')->getContent() ?>
                             <?php echo $layout->renderControllerAction('CR\Scouting\ScoutingModule:Default:teamLookingGamePlayer')->getContent() ?>
                             <?php echo $layout->renderControllerAction('CR\Scouting\ScoutingModule:Default:teamLookingPlayground')->getContent() ?>
                        </div>
                    </div>
                    
                </div>
            </div>



</section>



<?php  $layout->addStylesheet('plugins/starbox/css/jstarbox.css') ?>
<?php  $layout->addJavascript('plugins/starbox/jstarbox.js') ?>
<?php $layout->addJavascript('js/TeamEventManager.js') ?>
<?php $layout->addJavascript('js/TeamWallManager.js') ?>
<?php $layout->addJavascript('js/ScoutingManager.js') ?>
<?php $layout->addJavascript('/plugins/select2/select2.full.min.js') ?>

<?php $layout->addStylesheet('plugins/pickadate/themes/classic.css') ?>
<?php $layout->addStylesheet('plugins/pickadate/themes/classic.date.css') ?>
<?php $layout->addJavascript('plugins/pickadate/picker.js') ?>
<?php $layout->addJavascript('plugins/pickadate/picker.date.js') ?>

<?php $layout->addStylesheet('plugins/timepicker/bootstrap-timepicker.min.css') ?>
<?php $layout->addJavascript('plugins/timepicker/bootstrap-timepicker.js') ?>


<?php $layout->addJavascript('plugins/fileupload/jquery.ui.widget.js') ?>
<?php $layout->addJavascript('plugins/fileupload/jquery.iframe-transport.js') ?>
<?php $layout->addJavascript('plugins/fileupload/jquery.fileupload.js') ?>
<?php $layout->addJavascript('plugins/textcomplete/jquery.textcomplete.min.js') ?>
<?php  $layout->startSlot('javascript') ?>

<script src="/js/Lib/jquery.flot.js"></script> 
<script src="/js/Lib/jquery.flot.pie.js"></script> 
<script src="/js/Lib/jquery.flot.stack.js"></script> 
<script src="/js/Lib/jquery.flot.tooltip.min.js"></script>
<script src="/js/Lib/curvedLines.js"></script>
<script src="/js/Lib/JUMFlot/jquery.flot.JUMlib.js"></script>
<script src="/js/Lib/JUMFlot/jquery.flot.spider.js?<?php echo time() ?>"></script>
<link rel="stylesheet" type="text/css" href="/js/plugins/typehead/typeaheadjs.css"></link>
<script src="/dev/js/PlayersComparison/bundle/bundle.js"></script>


<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '1142117885858668',
      xfbml      : true,
      version    : 'v2.7'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
   
 
   
   
   $(document).on('click','.share-fb-trigger',function(e){
       e.preventDefault();
       
       /*
         FB.api(
    "/",
    {
        "id": "http:\/\/www.imdb.com\/title\/tt2015381\/"
    },
    function (response) {
      console.log(response);
        if (response && !response.error) {
        
      }
    }
);
       */
       
       
       /*target_post_cnt = $(this).attr('data-post');
        FB.ui(
        {
          method: 'share_open_graph ',
          href: 'http://app.coachrufus.com',
          quote:  target_post_cnt,
          hashtag: '#coachrufus'
        },
        // callback
        function(response) {
          if (response && !response.error_message) {
            alert('Posting completed.');
          } else {
            alert('Error while posting.');
          }
        }
      );
      */
      
       /*
       FB.login(function(){
            FB.api('/me/feed', 'post', {message: 'test'});
          }, {scope: 'publish_actions'});
       */
      
      /*
       target_post_id = $(this).attr('data-post');
       target_post = $('#'+target_post_id);
       target_post_cnt = target_post.find('.post-cnt').html();
       console.log(target_post_cnt);
        FB.ui(
        {
          method: 'share',
          href: 'http://app.coachrufus.com',
          quote:  target_post_cnt,
          hashtag: '#coachrufus'
        },
        // callback
        function(response) {
          if (response && !response.error_message) {
            alert('Posting completed.');
          } else {
            alert('Error while posting.');
          }
        }
      );
      */
   });
</script>

<script type="text/javascript"> 
/*
    
    
    
    
    
$('.starbox-fake').each(function(index,val){
    $(val).starbox({
        average: $(this).attr('data-start-value'),
        changeable: false
    })
});



      wall_manger = new TeamWallManager();
      wall_manger.bindTriggers();
      

      event_manger = new TeamEventsManager();
      event_manger.init();
      
      scouting_manger = new ScoutingManager();
      scouting_manger.bindTriggers();
       $(".select2").select2();
       jQuery('#scouting_game_time').pickadate({'format': 'dd.mm.yyyy'});
       jQuery('#scouting_valid_to').pickadate({'format': 'dd.mm.yyyy'});

       jQuery("#scouting_game_time_time").timepicker({showInputs: false,showMeridian: false, minuteStep: 5});
    function initLocalityManager()
    {
        locality_manger = new LocalityManager();
        locality_manger.google = google;
        //locality_manger.init();
        locality_manger.createSearchInput();
        
        
        locality_manger2 = new LocalityManager();
        locality_manger2.google = google;
        locality_manger2.createSearchInput('pac-input2');
        
        locality_manger3 = new LocalityManager();
        locality_manger3.google = google;
        locality_manger3.createSearchInput('pac-input3');
        
    }
    
    
*/
</script>
<?php /*<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXU1duK54f1gIsRy_xc7z4uQURovsZdE8&libraries=places&callback=initLocalityManager" async defer></script>*/ ?>
<?php  $layout->endSlot('javascript') ?>


