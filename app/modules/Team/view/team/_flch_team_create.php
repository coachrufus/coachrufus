 <div class="col-md-4">
        <div class="box box-widget widget-user create-team-widget">
            <div class="widget-user-header">
                <h3 class="widget-user-username"><?php echo $translator->translate('Create new Floorball Challenge team') ?></h3>
            </div>
            <div class="widget-user-image">
                <a  class="ico ico-create-team" href="<?php echo $router->link('team_create',array('tt' => 'flch')) ?>"></a>
            </div> 
            <div class="box-footer">

            </div>
        </div>
    </div>