<?php

namespace Webteamer\Team\Controller;

use Core\Controller as Controller;
use Core\ControllerResponse;
use Core\ServiceLayer;
use Core\Validator;
use DateTime;
use Webteamer\Locality\Form\PlaygroundForm;
use Webteamer\Team\Form\AddPlayerForm;
use Webteamer\Team\Form\PlayerSearchForm;
use Webteamer\Team\Form\SettingsForm;
use Webteamer\Team\Form\TeamValidator;
use Webteamer\Team\Model\TeamPlayer;

class OwnerController extends Controller {

    public function playersAttendanceAction()
    {
        $request = ServiceLayer::getService('request');
        $security = ServiceLayer::getService('security');
        $repo = ServiceLayer::getService('TeamRepository');
        $teamManager = ServiceLayer::getService('TeamManager');
        $eventManager = ServiceLayer::getService('EventManager');
        $list = $teamManager->getTeamPlayers($request->get('team_id'));
        $team = $repo->find($request->get('team_id'));
        
        
        $filterForm = new \Core\Form();
        $filterForm->setName('filter');
        $filterForm->setField('from', array('type' => 'datetime'));
        $filterForm->setField('to', array('type' => 'datetime'));
        $filterForm->setField('event_type', array('type' => 'choice','choices' => $eventManager->getEventTypFormChoices()));
        
        $filterCriteria = $request->get('filter');
        $filterForm->bindData($filterCriteria);
        
        $events = $eventManager->getTeamEvents($team,$filterCriteria);
        
        //$events = $teamManager->getNearestEvent(array($team));
        
        $attendanceMatrix = $eventManager->getAttendanceEventMatrix($list,$events,$security->getIdentityUser());
        
        
        
        //var_dump($attendanceMatrix);
        
        
        return $this->render('Webteamer\Team\TeamModule:owner:team_players_attendance.php', array(
                    'list' => $list,
                    'team_id' => $request->get('team_id'),
                    'team' => $team,
                    'events' => $events,
                    'attendanceMatrix' => $attendanceMatrix,
                    'filterForm' => $filterForm

        ));
    }

    public function createPlayerAttendanceAction()
    {
        $request = ServiceLayer::getService('request');
        $eventManager = ServiceLayer::getService('EventManager');

        $playerId = $request->get('player_id');
        $eventId = $request->get('event_id');
        $comment = $request->get('comment');

        $player = ServiceLayer::getService('PlayerRepository')->find($playerId);
        $event = ServiceLayer::getService('EventRepository')->find($eventId);

        $eventManager->createEventAttendance($player, $event, $comment);

        $response = new ControllerResponse();
        $response->setStatus('success');
        $response->setContent('create_success');
        $response->setType('json');
        return $response;
    }

    public function removePlayerAttendanceAction()
    {
        $request = ServiceLayer::getService('request');
        $eventManager = ServiceLayer::getService('EventManager');

        $playerId = $request->get('player_id');
        $eventId = $request->get('event_id');

        
        $player = ServiceLayer::getService('PlayerRepository')->find($playerId);
        $event = ServiceLayer::getService('EventRepository')->find($eventId);

        $eventManager->deleteEventAttendance($player, $event);

        $response = new ControllerResponse();
        $response->setStatus('success');
        $response->setContent('create_success');
        $response->setType('json');
        return $response;
    }

    public function addPlayerAction()
    {
        $request = ServiceLayer::getService('request');
        $translator = ServiceLayer::getService('translator');
        $form = new PlayerSearchForm();
        $teamManager = ServiceLayer::getService('TeamManager');
        $teamManager->setTranslator($translator);
        $repo = ServiceLayer::getService('TeamRepository');
        $team = $repo->find($request->get('team_id'));
        $addform = new AddPlayerForm();
        //$handler = new \Webteamer\Team\Handler\WebHandler();

        $players = array();
        if ('POST' == $request->getMethod())
        {
            $post_data = $request->get($form->getName());
            $form->bindData($post_data);
            $players = $teamManager->findPlayer($post_data);
        }


        return $this->render('Webteamer\Team\TeamModule:owner:team_add_player.php', array(
                    'form' => $form,
                    'players' => $players,
                    'team' => $team,
                    'addForm' => $addform
        ));
    }
    
    

   
    
    public function editPlayersAction()
    {
        $players = $this->getRequest()->get('players');
        $teamManager = ServiceLayer::getService('TeamManager');
        
        foreach($players as $playerData)
        {
            $teamManager->updatePlayerFromArray($playerData['id'],$playerData);
        }
        
        
    }
    
   

}
