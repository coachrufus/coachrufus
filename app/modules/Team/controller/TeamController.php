<?php

namespace Webteamer\Team\Controller;

use AclException;
use Core\Controller;
use Core\ControllerResponse;
use Core\Form;
use Core\ServiceLayer;
use Core\Validator;
use DateTime;
use Webteamer\Locality\Form\PlaygroundForm;
use Webteamer\Team\Form\CreateTeamForm;
use Webteamer\Team\Form\SettingsForm;
use Webteamer\Team\Form\TeamValidator as TeamValidator;
use Webteamer\Team\Model\Team;
use Webteamer\Team\Model\TeamSettings;


/**
 * @author Marek Hubáček
 * @version 1.0
 */
class TeamController extends Controller {

     public function deleteAction()
     {
        $request = ServiceLayer::getService('request');
        $teamManager = ServiceLayer::getService('TeamManager');
        $playerManager = ServiceLayer::getService('PlayerManager');
        $notifyManager = ServiceLayer::getService('notification_manager');
        $team = $teamManager->findTeamById($request->get('team_id'));
        $this->checkSettingTeamPermission($team);
        
        $security = ServiceLayer::getService('security');
        $user = $security->getIdentity()->getUser();
        
        $team->setStatus('deleted');
        $teamManager->saveTeam($team);
        
        //send nofitifaciotn
        $players =  $teamManager->getActiveTeamPlayers($team->getId());
        foreach($players as $teamPlayer)
        {
            $player =  $playerManager->findPlayerById($teamPlayer->getPlayerId());
            if(null != $player && null != $player->getEmail())
            {
                $data['team_name'] = $team->getName();
                $data['email'] = $player->getEmail();
                $data['scouting_link'] = WEB_DOMAIN.$this->getRouter()->link('scouting');
                $data['lang'] =  (null != $player->getDefaultLang()) ? $player->getDefaultLang() : 'en';
                $notifyManager->sendTeamDeleteNotification($data);
            }
        }
        //refresh user teams
        //$security->refreshTeamRoles($user);
        
        $teams =  $security->getIdentity()->getUserTeams();
        $currentTeam = current($teams);
        if(null != $currentTeam)
        {
            $_SESSION['active_team']['id'] = current($teams)->getId();
        }
        
        
        $request->addFlashMessage('dashboard_flash_message', $this->getTranslator()->translate('DELETE_TEAM_SUCCES'));
        $request->redirect($this->getRouter()->link('player_dashboard',array('userid' => $user->getId())));
     }
    
    public function followAction()
     {
         $request = ServiceLayer::getService('request');
         $teamFollowManager = ServiceLayer::getService('TeamFollowManager');
         $teamManager = ServiceLayer::getService('TeamManager');
         $security = ServiceLayer::getService('security');
         $user = $security->getIdentity()->getUser();
         
         $team = $teamManager->findTeamById($request->get('id'));
         
         $teamFollowManager->createFollower($team,$user);
         $request->redirect($this->getRouter()->link('player_dashboard',array('user_id' => $user->getId())));
         
     }
    
    public function removePhotoAction()
    {
        $request = ServiceLayer::getService('request');
        $teamManager = ServiceLayer::getService('TeamManager');
        $team = $teamManager->findTeamById($request->get('team_id'));
        
      
        $team->setPhoto('');
        $teamManager->saveTeam($team);
        
        $layout = ServiceLayer::getService('layout');
        $layout->setTemplate(null);
        $result['file'] = $team->getDefaultPhoto();
        $response = new ControllerResponse();
        $response->setStatus('success');
        $response->setContent(json_encode($result));
        $response->setType('json');
        return $response;
    }
    
     public function changePhotoAction()
    {
        $request = ServiceLayer::getService('request');
        $response = new ControllerResponse();
        
        
        
        //validate file size
        $fileSize = $request->getFileSize('team_photo');
        $allowedSize = 2*1024*1024;
        if($allowedSize > $fileSize)
        {
            $request->uploadFile('team_photo',PUBLIC_DIR.'/img/team/',md5(time()).'_'.$_FILES['team_photo']['name']);
            $baseName =md5(time()).'_'.$_FILES['team_photo']['name'];

             $imageTransform = ServiceLayer::getService('imageTransform');
             $imageTransform->resizeImage(array(
                'sourceImg' => PUBLIC_DIR.'img/team/'.$baseName,
                'width' => 90, 
                'height' => 90,
                'targetDir' =>PUBLIC_DIR.'/img/team/thumb_90_90'));

            $imageTransform->resizeImage(array(
                'sourceImg' => PUBLIC_DIR.'img/team/'.$baseName,
                'width' => 320, 
                'height' => 320,
                'targetDir' =>PUBLIC_DIR.'/img/team/thumb_320_320'));

            $imageTransform->resizeImage(array(
                'sourceImg' => PUBLIC_DIR.'img/team/'.$baseName,
                'width' => 640, 
                'height' => 640,
                'targetDir' =>PUBLIC_DIR.'/img/team/thumb_640_640'));

            $imageTransform->resizeImage(array(
                'sourceImg' => PUBLIC_DIR.'img/team/'.$baseName,
                'width' => 960, 
                'height' => 960,
                'targetDir' =>PUBLIC_DIR.'/img/team/thumb_960_960'));

            $teamManager = ServiceLayer::getService('TeamManager');
            $team = $teamManager->findTeamById($request->get('team_id'));
            
            if(null != $team )
            {
                $team->setPhoto($baseName);
                $teamManager->saveTeam($team);
            }


           
            $result['result'] = 'SUCCESS';
            $result['file'] = '/img/team/thumb_320_320/'.$baseName;
            $result['base_name'] = $baseName;
            $response->setStatus('success');
            
        }
        else 
        {
             $response->setStatus('success');
             $result['result'] = 'ERROR';
             $result['error_message'] = ServiceLayer::getService('translator')->translate('Allowed max size is 2MB');
        }
        
        
        
        
        
        $layout = ServiceLayer::getService('layout');
        $layout->setTemplate(null);
        
        
      
        $response->setContent(json_encode($result));
        $response->setType('json');
        return $response;
    }
    
    private function getCreateForm()
    {
        $security = ServiceLayer::getService('security');
        $translator = ServiceLayer::getService('translator');
        $manager = ServiceLayer::getService('TeamManager');
        $playgroundManager = ServiceLayer::getService('PlaygroundManager');
        $user = $security->getIdentity()->getUser();
        $manager->setTranslator($translator);
        
        $entity = new Team();
        $entity->setAuthorId($security->getIdentity()->getUser()->getId());
        $entity->setCreatedAt(new DateTime());
        $entity->setStatus('public');
        
         $ageGroupChoices = array('' => '') + $manager->getAgeGroupEnum();
        if($this->getRequest()->get('tt') == 'flch')
        {
            $entity->setSportId(42);
            $ageGroupChoices = array('' => '','first_level' => 'Prvý stupeň','second_level' => 'Druhý stupeň');
        }
        
        $form = new CreateTeamForm();
        $form->setEntity($entity);
        
        $statusChoices = array('' => '') + $manager->getStatusEnum();
        $form->setFieldChoices('status', $statusChoices);
        
        $sportManager = ServiceLayer::getService('SportManager');
        
        $sportChoices = $sportManager->getTeamSportFormChoices();
       
        
        $sportChoices = array('' => array('' => '')) +$sportChoices;
        $form->setFieldChoices('sport_id',$sportChoices);
        $form->setFieldOption('sport_id', 'label', $translator->translate('Sport'));
        
        $genderChoices = array('' => '') + $manager->getGenderEnum();
        $form->setFieldChoices('gender', $genderChoices);
        
       
        $form->setFieldChoices('age_group', $ageGroupChoices);
        
        $levelChoices = array('' => '') + $manager->getLevelEnum();
        $form->setFieldChoices('level', $levelChoices);
        
        $playgrounds = $playgroundManager->getUserPlaygrounds($user);
        $playgroundsChoices = array('' => $translator->translate('Select playground'));
        foreach ($playgrounds as $playground)
        {
            $playgroundsChoices[$playground->getId()] = $playground->getName();
        }
        
        //city choices
        
        $isFloorballChallengeTeam = $manager->isFloorballChallengeTeam($entity);
        if('flch' == $this->getRequest()->get('tt') or $isFloorballChallengeTeam)
        {
            $form->setFieldOption('school_name', 'required', true);
            $form->setFieldOption('flch_city_name', 'required', true);
            $form->setFieldOption('flch_city_name_selector', 'required', true);
        }
        
        
        
        
        $form->setFieldChoices('exist_playgrounds', $playgroundsChoices);
        return $form;
         
       
    }
    
    public function createAction()
    {
        $security = ServiceLayer::getService('security');
        $request = ServiceLayer::getService('request');
        $router = ServiceLayer::getService('router');
        $translator = ServiceLayer::getService('translator');
        $security = ServiceLayer::getService('security');
        $teamManger = ServiceLayer::getService('TeamManager');
        $repository = ServiceLayer::getService('TeamRepository');
        $localityManager = ServiceLayer::getService('LocalityManager');
        $systemNotifyManager = ServiceLayer::getService('SystemNotificationManager');

        ServiceLayer::getService('ActiveTeamManager')->setActiveTeam(new Team());

        $user = $security->getIdentity()->getUser();
        $form = $this->getCreateForm();
        $playgroundForm = new PlaygroundForm();
        $validator = new TeamValidator();
        $validator->setRules($form->getFields());
        
        $playerLocality = ServiceLayer::getService('LocalityRepository')->find($user->getLocalityId());

        $playgroundValidator = new Validator();
        $playgroundData = array();

        if ('POST' == $request->getMethod())
        {
            $postData = $request->get($form->getName());
            $form->bindData($postData);

            $validator->setData($postData);
            $validator->validateData();
            //$validator->validateUniqueEmail($repository,$postData['name']);
            
            if('flch' == $this->getRequest()->get('tt'))
            {
                if(null == $postData['flch_city_name'])
                {
                    $validator->addErrorMessage('flch_city_name', $translator->translate('Required Field!'));
                }
            }
         
            if (!$validator->hasErrors())
            {
                $postData['author_id'] = $security->getIdentity()->getUser()->getId();
                $form->bindData($postData);
                $validator->setData($postData);
                $validator->validateData();
                if (!$validator->hasErrors())
                {
                    $entity = $form->getEntity();
                   
                    
                    //save locality
                    if(null != $request->get('locality_json_data'))
                    {
                        $locality =  $localityManager->createObjectFromJsonData($request->get('locality_json_data'));
                        if(null !=  $request->get('locality_name'))
                        {
                            $locality->setName($request->get('locality_name'));
                        }
                        $locality->setAuthorId($entity->getAuthorId());
                        $localityId = $localityManager->saveLocality($locality);
                        $entity->setLocalityId($localityId);
                    }
                    
                    $teamId = $teamManger->saveTeam($entity);
                    
                    if(null == $entity->getId())
                    {
                         $entity->setId($teamId);
                    }
                    ServiceLayer::getService('ActiveTeamManager')->setActiveTeam($entity);
                    
                    //save playground
                    if(!empty($postData['exist_playgrounds']))
                    {
                        $teamManger->getRepository()->saveTeamPlaygroundAlias($entity->getId(), $postData['exist_playgrounds']);
                    }
                    
                    $playerData['team_id'] = $teamId;
                    $playerData['player_id'] =  $form->getEntity()->getAuthorId();
                    $playerData['status'] = 'confirmed';
                    $playerData['team_role'] = 'ADMIN';
                    $playerData['first_name'] = $user->getName();
                    $playerData['last_name'] = $user->getSurname();
                    $playerData['email'] = $user->getEmail();
                    $teamManger->saveTeamPlayer($playerData); 

                   
                    ServiceLayer::getService('StepsManager')->finishCreateTeam();

                    //save fl team
                    if($request->get('type') == 'flch')
                    {
                        $flchManager = ServiceLayer::getService('FlchManager');
                        $flchCouponData = array();
                        $flchCouponData['city'] = $postData['flch_city_name'];
                        $flchCouponData['school'] = $postData['school_name'];
                        $registerCode = $flchManager->createCouponCode($flchCouponData);
                        $flchManager->createTeamFromCode($registerCode,$entity->getId());
                        if(ServiceLayer::getService('StepsManager')->showStepByStep() == false)
                        {
                            $flchManager->createProPackage($entity);
                            
                            ServiceLayer::getService('TeamCreditManager')->refreshTeamPackages();
                        } 
                    }
                    
                    if($request->get('type') == 'promo-group')
                    {
                        $teamManger->createPromoTeam($entity,'GROUP');
                    }
                    
                    //if unsupported sport disable step by step
                    
                    if($teamManger->teamHasProSport($entity) == false)
                    {
                        ServiceLayer::getService('StepsManager')->finishStepByStep($entity->getAuthorId(),$entity->getId());
                    }
                    $mailchipLists = array('sk' => 'ebbb119907' ,'en' => '8abc09ae7a' );
                    $queueRepo  = ServiceLayer::getService('QueueRepository');
                    $queueRepo->addRecord(array('action_key' => 'user_id','value' => $entity->getAuthorId(),'action_type' => 'mailchimp_export','data' => json_encode(array('listId' => $mailchipLists[LANG] )) ));
                
                    //email marketing
                    $emailMarketingManager = \Core\ServiceLayer::getService('EmailMarketingManager');
                    $emailMarketingManager->removeUserTag($user,'user_noteam');
                    $emailMarketingManager->addUserTag($user,'team_create');
                    
                    //add team to notifu settings
                    $systemNotifyManager->changeTeamNotificationsSetup($user,$teamId,'enable');
                    $security->reloadUser($user);
                   
                    
                
                    $request->addFlashMessage('team_create_create_success', $translator->translate('CREATE_ENTITY_SUCCESS'));
                    
                    if($_SESSION['skliga2019'] != null)
                    {
                        $tournamentRepository = ServiceLayer::getService('TournamentRepository');
                        $hash = $_SESSION['skliga2019'];
                        $id = substr($hash,3);
                        $id = substr($id,0,-8); 
                        $tournament = $tournamentRepository->find($id);

                        $alertMessage = $translator->translate('tournament.skliga.joinTeamAlert').' '.$tournament->getName().'? <br /><a class="btn btn-primary" href="'.$router->link('tournament_share_link',array('hash' => $_SESSION['skliga2019'])).'">'.$translator->translate('tournament.start.joinTournament').'</a>';
                        $request->addFlashMessage('team_create_skliga_alert',$alertMessage);
                         unset($_SESSION['skliga2019']);
                    }
                    
                    
                    $request->redirect($router->link('team_players_list',array('team_id' => $teamId,'type' => 'progress')));
                }
            }
          
        }
        
        /*
        if('api' == $request->get('layout'))
        {
            ServiceLayer::getService('layout')->setTemplate( GLOBAL_DIR.'/templates/ClearLayout.php');
        }
*/

        return $this->render('Webteamer\Team\TeamModule:team:create.php', array(
                    'form' => $form,
                    'playgroundForm' => $playgroundForm,
                    'request' => $request,
                    'validator' => $validator,
                    'playgroundValidator' => $playgroundValidator,
                    'playerLocality'  => $playerLocality,
                    
        ));
    }

    public function membersAction()
    {
       
        return $this->render('Webteamer\Team\TeamModule:team:members.php', array(

        ));
    }

    
    public function overviewAction()
    {
        $request = ServiceLayer::getService('request');
        $teamManager = ServiceLayer::getService('TeamManager');
        $teamEventManager = ServiceLayer::getService('TeamEventManager');
        $playerStatManager = ServiceLayer::getService('PlayerStatManager');
        $playerManager = ServiceLayer::getService('PlayerManager');
        $teamWallManager = ServiceLayer::getService('TeamWallManager');
         $sportManager = ServiceLayer::getService('SportManager');
        $translator = $this->getTranslator();
        $repo = ServiceLayer::getService('TeamRepository');
        $team = $repo->find($request->get('id'));
        ServiceLayer::getService('security')->getIdentity()->checkTeamAccess($team);
        ServiceLayer::getService('ActiveTeamManager')->setActiveTeam($team);
        
       
        $post = new \CR\Team\Model\TeamWallPost();
        $post->setTeamId($team->getId());
        $wallPostForm = new \CR\Team\Form\TeamWallPostForm($post);
        $wallPostForm->setEntity($post);

        
        $wallPosts = $teamWallManager->getLastWallPosts($team);
        

        $eventsList = $teamEventManager->getTeamUpcomingEvents($team);
        $userAttednanceMatrix = $teamEventManager->getPlayerEventsAttendanceMatrix(ServiceLayer::getService('security')->getIdentity()->getUser(),$eventsList);
         
        $players =  $teamManager->getActiveTeamPlayers($team->getId());
         foreach($players as $teamPlayer)
        {
            $player =  $playerManager-> findPlayerById($teamPlayer->getPlayerId());
            if(null != $player)
            {
                $playerSports = $playerManager->findPlayerSports($player);
                
                foreach($playerSports as $playerSport)
                {
                    if(null != $playerSport->getSport())
                    {
                         $playerSport->getSport()->setName(trim($translator->translate($playerSport->getSport()->getName())));
                    }
                }
                 
                 
                $player->setSports($playerSports);
                $rating = $playerStatManager->getPlayerRating($player);
                $playerStats = $playerStatManager->getTeamPlayerStat($teamPlayer);
                
                $teamPlayer->setRating($rating['average']);
                $teamPlayer->setPlayer($player);
                $teamPlayer->setStats($playerStats);
            }
        }
        
         //check steps
        $stepsManager = \Core\ServiceLayer::getService('StepsManager');
        $stepsManager->handleRedirectToTeamLastStep($team);
        
       
       

        
        $hasProSport = $teamManager->teamHasProSport($team);
        $addStatRoute = $sportManager->getTeamSportAddStatRoute($team);
        if($addStatRoute != 'team_match_create_live_stat')
        {
            $hasProSport = false;
        }
        
        return $this->render('Webteamer\Team\TeamModule:team:overview.php', array(
                    'team' => $team,
                    'eventsList' => $eventsList,
                    'players' => $players,
                    'userTeamPermission' => ServiceLayer::getService('security')->getIdentity()->getTeamPermissions($team),
                    'attendanceList' => $userAttednanceMatrix,
                    'wallPostForm' => $wallPostForm,
                    'wallPosts' => $wallPosts,
                    'hasProSport' => $hasProSport
        ));
    }
    
     public function overviewAllAction()
    {
        $teamManager = ServiceLayer::getService('TeamManager');
        $teamEventManager = ServiceLayer::getService('TeamEventManager');
        $sportManager = ServiceLayer::getService('SportManager');
        $security = ServiceLayer::getService('security');
        $user = $security->getIdentity()->getUser();
        $teams = $teamManager->getUserTeams($user);
        

        //t_dump($_SESSION);
        
        $waitingTeams = $teamManager->getUserWaitingTeams($user);
        
        $listTo = new \DateTime();
        $listTo->setTimestamp(strtotime("+ 30 day"));
        $events = $teamEventManager->findEvents($teams, array('from' => new \DateTime(), 'to' => $listTo));
        $eventsList = $teamEventManager->buildTeamEventList($events, new \DateTime(), $listTo);
        $teamInfo = $teamManager->getTeamsInfo($teams);
        $sportEnum = $sportManager->getSportFormChoices();
        $userTeamRoles = $security->getIdentity()->getTeamRoles();
        $playerStatusEnum = $teamManager->getPlayerStatusEnum();
        $playerPermissiomEnum = $teamManager->getPermissionEnum();
        return $this->render('Webteamer\Team\TeamModule:team:overview_all.php', array(
                    'teams' => $teams,
                    'eventsList' => $eventsList,
                     'teamInfo' => $teamInfo,
                    'sportEnum' => $sportEnum,
                    'userTeamRoles' => $userTeamRoles,
                    'playerStatusEnum' => $playerStatusEnum,
                    'playerPermissiomEnum' => $playerPermissiomEnum,
                    'waitingTeams' => $waitingTeams
        ));
    }

   public function matchListAction()
    {
        $teamId = $this->getRequest()->get('team_id');
        $manager = ServiceLayer::getService('TeamManager');
        $matchManager = ServiceLayer::getService('PlayerMatchManager');
        $statManager = ServiceLayer::getService('PlayerStatManager');
        
        $team = $manager->findTeamById($teamId);
        $playerMatches = $matchManager->getPlayerMatchByTeam($team);
        $security = ServiceLayer::getService('security');
        $user = $security->getIdentity()->getUser();
        
        
        $teamMatches = $manager->createTeamMatches($playerMatches);
        
        
        //add player stats
       foreach ($teamMatches as $teamMatch)
       {
           foreach ($teamMatch->getPlayers() as $playerMatch)
           {
               $ratings = $statManager->getPlayerMatchRatings($playerMatch);
               $playerMatch->setRatings($statManager->getPlayerMatchRatings($playerMatch));
               
           }
       }

        
         return $this->render('Webteamer\Team\TeamModule:team:matchList.php', array(
             'teamMatches' => $teamMatches,
             'team' => $team,
             'matchManager' => $matchManager,
             'user' => $user

        ));
        
    }
  
 public function getSettingsForm($team)
    {
        //$security = ServiceLayer::getService('security');
        $manager = ServiceLayer::getService('TeamManager');
        $form = new SettingsForm();
        $statusChoices = array('' => '') + $manager->getStatusEnum();
        $form->setFieldChoices('status', $statusChoices);

        $sportManager = ServiceLayer::getService('SportManager');
        $sportChoices = $sportManager->getSportFormChoices();
        $form->setFieldChoices('sport_id',$sportChoices);
        
        $playgrounds = $manager->getTeamPlaygroundsAlias($team);
        
       
        
        $formPlaygrounds = array();
        foreach($playgrounds as $playground)
        {
            $formPlayground = array();
            $formPlayground['alias_id'] = $playground->getAliasId();
            $formPlayground['alias_name'] = $playground->getAliasName();
            $formPlaygrounds[$playground->getId()] = $formPlayground;
            
        }
        
        
        $form->setFieldValue('playgrounds', $formPlaygrounds);
        
        return $form;
    }
    
    public function crsSettingsAction()
    {
        $request = ServiceLayer::getService('request');
        $manager = ServiceLayer::getService('TeamManager');
        $team = $manager->findTeamById($request->get('team_id'));
        $this->checkSettingTeamPermission($team);

        $form = new \CR\Team\Form\TeamCRSForm();
        
        $crsSettings = $manager->getTeamSettings($team);
        foreach($crsSettings as $crsSetting)
        {
            if($crsSetting->getSettingsGroup() == 'crs')
            {
                $form->setFieldValue($crsSetting->getName(), $crsSetting->getValue());
            }
        }
        
        
        $validator = new Validator();
        $validator->setRules($form->getFields());
        
        if ('POST' == $request->getMethod())
        {
            $postData = $request->get($form->getName());
            $form->bindData($postData);
            $validator->setData($postData);
            $validator->validateData();
            
            if(!$validator->hasError())
            {
                $manager->deleteTeamSettingsGroup($team,'crs');
                foreach($postData as $name => $val)
                {
                    $settings = new TeamSettings();
                    $settings->setTeamId($team->getId());
                    $settings->setSettingsGroup('crs');
                    $settings->setValue($val);
                    $settings->setName($name);
                    $manager->saveSetting($settings);
                }
                $request->addFlashMessage('crs_edit_success', $this->getTranslator()->translate('CRS_EDIT_SUCCESS'));
                $request->redirect();
            }
        }

        $crsTemplate = 'crs_settings.php';
        
        $sportManager = ServiceLayer::getService('SportManager');
        $statRoute = $sportManager->getTeamSportStatRoute($team);
        
        if('team_tennis_stat' == $statRoute or 'team_volleyball_stat' == $statRoute)
        {
            $crsTemplate = 'crs_set_settings.php';
        }
        if('team_basketball_stat' == $statRoute)
        {
            $crsTemplate = 'crs_basketball_settings.php';
        }
        

        return $this->render('Webteamer\Team\TeamModule:team:'.$crsTemplate, array(
            'team' => $team,
            'form' => $form,
            'validator' => $validator
            
        ));
    }

    public function settingsAction()
    {
        $request = ServiceLayer::getService('request');
        $translator = ServiceLayer::getService('translator');
        $manager = ServiceLayer::getService('TeamManager');
        $repository = ServiceLayer::getService('TeamRepository');
        $localityManager = ServiceLayer::getService('LocalityManager');
        $team = $manager->findTeamById($request->get('team_id'));
       ServiceLayer::getService('security')->getIdentity()->checkTeamAccess($team);
       
        
        $locality = null;
        if(null != $team->getLocalityId())
        {
            $locality = $localityManager->getLocalityById($team->getLocalityId());
        }
        
        $form = $this->getCreateForm();
        
        $sportManager = ServiceLayer::getService('SportManager');
        $sport = $sportManager->findSportById($team->getSportId());
        $team->setSportName($translator->translate($sport->getName()));
        
        $teamPlayground = current($manager->getTeamPlaygroundsAlias($team));
        if(null != $teamPlayground)
        {
            $form->setFieldValue('exist_playgrounds',$teamPlayground->getId());
        }

        $form->setEntity($team);
        $validator = new TeamValidator();
        $validator->setRules($form->getFields());
        
        $template = 'edit.php';
        if( !ServiceLayer::getService('security')->getIdentity()->hasTeamPermission('manageSettings',$team))
        {
            $template = 'preview.php';
        }

         $isFloorballChallengeTeam = $manager->isFloorballChallengeTeam($team);
         

        if($isFloorballChallengeTeam)
        {
            $flchManager = ServiceLayer::getService('FlchManager');
            $flchTeam = $flchManager->getFlchTeam($team);
            $form->setFieldValue('school_name', $flchTeam->getSchool());
            $form->setFieldValue('flch_city_name_selector', $flchTeam->getCity());
            $form->setFieldValue('flch_city_name', $flchTeam->getCity());
            $ageGroupChoices = array('' => '','first_level' => 'Prvý stupeň','second_level' => 'Druhý stupeň');
            $form->setFieldChoices('age_group', $ageGroupChoices);
            $form->setFieldValue('age_group', $team->getAgeGroup());
        }

        
        if ('POST' == $request->getMethod())
        {
             $this->checkSettingTeamPermission($team);
            $postData = $request->get($form->getName());
            unset($postData['sport_id']); //prevent change sport
            $validator->setData($postData);
            $validator->validateData();
            
            if($postData['name'] != $team->getName())
            {
                $validator->validateUniqueEmail($repository,$postData['name']);
            }

            if (!$validator->hasErrors())
            {
                $form->bindData($postData);
                
                    $entity = $form->getEntity();

                    //save locality
                    if(null != $request->get('locality_json_data'))
                    {
                        $locality =  $localityManager->createObjectFromJsonData($request->get('locality_json_data'));
                        if(null !=  $request->get('locality_name'))
                        {
                            $locality->setName($request->get('locality_name'));
                        }
                        $locality->setAuthorId($entity->getAuthorId());
                        $localityId = $localityManager->saveLocality($locality);
                        $entity->setLocalityId($localityId);
                    }
                    if(null == $request->get('locality_name'))
                    {
                         $entity->setLocalityId(null);
                    }
                    
                    $manager->saveTeam($entity);

                    //save playground
                    $manager->getRepository()->deleteTeamPlaygroundAlias($entity);
                    if(!empty($postData['exist_playgrounds']))
                    {
                        $manager->getRepository()->saveTeamPlaygroundAlias($entity->getId(), $postData['exist_playgrounds']);
                    }
                   
                    //flch update
                    if($isFloorballChallengeTeam)
                    {
                        $flchTeam->setSchool($postData['school_name']);
                        $flchTeam->setCity($postData['flch_city_name']);
                        $flchManager->save($flchTeam);
                    }
                  
                    
                    $request->addFlashMessage('_edit_success', $translator->translate('UPDATE_ENTITY_SUCCESS'));
                    $request->redirect($this->getRouter()->link('team_settings',array('team_id' => $entity->getId())));
                
            }
          
        }
       
          return $this->render('Webteamer\Team\TeamModule:team:'.$template, array(
                    'form' => $form,
                    'request' => $request,
                    'validator' => $validator,
                    'locality' => $locality,
                    'isFloorballChallengeTeam' => $isFloorballChallengeTeam
        ));
        
      
    }
    
    public function shareLinkInviteAction()
    {
        $request = ServiceLayer::getService('request');
        $hash = $request->get('hash');
        $teamManager = ServiceLayer::getService('TeamManager');
        $team = $teamManager->findTeamByHash($request->get('hash'));
        $user = ServiceLayer::getService('security')->getIdentity()->getUser();
        

        if(null == $user)
        {
            $link =  $this->getRouter()->link('login',array('lang' => 'sk','inv' => $hash,'acc' => 'reg'));
            $registerLink =  $this->getRouter()->link('register_start',array('lang' => 'sk','inv' => $hash));
        }
        else
        {
            $translator = ServiceLayer::getService('translator');
            $translator->setLang( $user->getDefaultLang());
            $link =  $this->getRouter()->link('team_share_link',array('lang' => $user->getDefaultLang(),'userid' => $user->getId() ,'hash' => $hash,'acc' => 'y'));
        }
        
        if('y' == $request->get('acc'))
        {
            //send admin mail
            $teamAdmins = $teamManager->getTeamAdminList($team);
            $userRepo = ServiceLayer::getService('user_repository');
            foreach($teamAdmins as $teamAdmin)
            {
               
                $teamAdminUser = $userRepo->find($teamAdmin->getPlayerId());
                $notifyManager = ServiceLayer::getService('notification_manager');
                if(null != $teamAdminUser)
                {
                    $data['sender_name'] =  $user->getFullName();
                    $data['team_name'] = $team->getName();
                    $data['management_link'] = WEB_DOMAIN.''.$this->getRouter()->link('team_players_list',array('team_id' => $team->getId() ));
                    $data['email'] = $teamAdminUser->getEmail();
                    $data['lang'] =  (null != $teamAdminUser->getDefaultLang()) ? $teamAdminUser->getDefaultLang() : 'en';
                    $notifyManager->sendShareLinkInvitationAdmin($data);
                }
            }
            //redirect to dashboard
            $this->getRequest()->redirect($this->getRouter()->link('player_dashboard',array('userid' => $user->getId() ,'inv' => $hash)));
        }
        
        
        
        
        $layout = ServiceLayer::getService('layout');
        $layout->setTemplate(GLOBAL_DIR.'/templates/LoginLayout.php');
        return $this->render('Webteamer\Team\TeamModule:team:share_link_invite.php', array(
                        'team' => $team,
                        'hash' => $hash,
                        'loginLink' => $link,
                        'registerLink' => $registerLink

            ));
        
       
    }
    
  
    public function memberInviteAction()
    {

        $request = ServiceLayer::getService('request');
        $teamManager = ServiceLayer::getService('TeamManager');
        $playerRepo = ServiceLayer::getService('PlayerRepository');
        $team = $teamManager->findTeamByHash($request->get('hash'));
        
        $teamPlayerId = $request->get('tp');
        $teamPlayerHash = $request->get('h');
        
        $_SESSION['invitation']['tp'] = $teamPlayerId;
        $_SESSION['invitation']['h'] = $teamPlayerHash;
        $_SESSION['invitation']['hash'] = $request->get('hash');
        
        $security = ServiceLayer::getService('security');
        $user = $security->getIdentity()->getUser();

        $teamPlayer = $teamManager->findTeamPlayerById($teamPlayerId);
        //add registred player, 
        if(null != $teamPlayer)
        {
             if(null !=$teamPlayer &&  $teamPlayerHash == $teamPlayer->getHash())
            {
                $teamPlayer->setStatus('confirmed');
                $teamManager->getRepository()->saveTeamPlayer($teamPlayer);
                //send push notify
                $notificationManager = ServiceLayer::getService('SystemNotificationManager');
                $notificationManager->createPushNotify('create_player',array('team' => $team,'player' => $teamPlayer));
                //notify setup
                $player = $playerRepo->find($teamPlayer->getPlayerId());
                if(null != $player)
                {
                    $notificationManager->changeTeamNotificationsSetup($player,$team->getId(),'enable');
                    
                    if(null != $user)
                    {
                        $security->reloadUser($user);
                    }
                }
               
            }
            else 
            {
                throw new AclException();
            }
            
            //is registred user, autenthicate and redirect
            if($teamPlayer->getPlayerId() > 0)
            {
                $security = ServiceLayer::getService('security');  
                $router = ServiceLayer::getService('router');
                $userRepository = ServiceLayer::getService('user_repository');
                $user = $userRepository->findOneBy(array('id' => $teamPlayer->getPlayerId()));
                $security->authenticateUserEntity($user);
                $request->redirect($router->link('team_overview',array('id' => $team->getId())));
            }
            else
            {
                $layout = ServiceLayer::getService('layout');
                $layout->setTemplate(GLOBAL_DIR.'/templates/LoginLayout.php');

                 return $this->render('Webteamer\Team\TeamModule:team:memeber_invite.php', array(
                            'team' => $team,
                            'hash' => $request->get('hash'),
                            'tp' => $request->get('tp'),
                            'h' => $request->get('h'),
                ));
            }

        }
        else
        {
            
            
            
            $layout = ServiceLayer::getService('layout');
            $layout->setTemplate(GLOBAL_DIR.'/templates/LoginLayout.php');

             return $this->render('Webteamer\Team\TeamModule:team:memeber_invite.php', array(
                        'team' => $team,
                        'hash' => $request->get('hash'),
                            'tp' => $request->get('tp'),
                            'h' => $request->get('h'),
            ));
        }
        
        
        
    }
    
    /**
     * player refuse invitation to the team from admin
     * @return type
     */
    public function memberInviteDeclineAction()
    {

        $request = ServiceLayer::getService('request');
        $teamManager = ServiceLayer::getService('TeamManager');
        $team = $teamManager->findTeamByHash($request->get('hash'));
        $teamPlayerId = $request->get('tp');
        $teamPlayerHash = $request->get('h');
        
        
        $security = ServiceLayer::getService('security');
        $user = $security->getIdentity()->getUser();
      
        
        
        $teamPlayer = $teamManager->findTeamPlayerById($teamPlayerId);
        if($teamPlayerHash == $teamPlayer->getHash())
        {
            $teamPlayer->setStatus('decline');
            $teamManager->getRepository()->saveTeamPlayer($teamPlayer);
        }
            
        $teamAdminList = $teamManager->getTeamAdminList($team);
        
        $notifyManger = ServiceLayer::getService('notification_manager');
        $notifyManger->sendMemberInvitationDecline($teamAdminList,$team,$teamPlayer );
        

        //push notify
        $pushNotificationManager = ServiceLayer::getService('SystemNotificationManager');
        $pushNotificationManager->sendTeamInvitationDeclinePushNotify($teamAdminList, array('player' => $teamPlayer, 'team' => $team));
                
        $layout = ServiceLayer::getService('layout');
        $layout->setTemplate(GLOBAL_DIR.'/templates/LoginLayout.php');
        
        if(null != $user)
        {
            $request->addFlashMessage('member_invite_decline_message', $this->getTranslator()->translate('Decline accepted'));
            $request->redirect($this->getRouter()->link('player_dashboard', array('id' => $user->getId())));
            
        }
        else
        {
            return $this->render('Webteamer\Team\TeamModule:team:member_invite_decline.php', array(
                    'team' => $team,
                    'hash' => $request->get('hash')
            ));
        }
        
        
        
         
    }
    
    public function getPlayersAttendanceActionData()
    {
        $request = ServiceLayer::getService('request');
        $security = ServiceLayer::getService('security');
        $repo = ServiceLayer::getService('TeamRepository');
        $teamManager = ServiceLayer::getService('TeamManager');
        $seasonManager = ServiceLayer::getService('TeamSeasonManager');
        $eventManager = ServiceLayer::getService('TeamEventManager');
        $members = $teamManager->getTeamPlayers($request->get('team_id'));
        $team = $repo->find($request->get('team_id'));
        ServiceLayer::getService('security')->getIdentity()->checkTeamAccess($team);
        
        $hasProSport = $teamManager->teamHasProSport($team);
        
        if(!ServiceLayer::getService('TeamCreditManager')->teamHasFeatureAccess($team,'stats') && $hasProSport == true)
        {
            $this->getRequest()->redirect($this->getRouter()->link('order_list'));
        }
        
        
        $filterForm = new Form();
        $filterForm->setName('filter');
        $filterForm->setField('from', array('type' => 'datetime'));
        $filterForm->setField('to', array('type' => 'datetime'));
        $filterForm->setField('event_type', array('type' => 'choice','choices' => array('' => $this->getTranslator()->translate('ATTENDANCE_ALL_EVENT_TYPES'))+ $eventManager->getEventTypeEnum()));
        
        
        if(null == $request->get('filter'))
        {
            $from = new \DateTime(date('Y-m-d'));
            $to = new \DateTime(date('Y-m-d'));
            $to->setTimestamp(strtotime("+ 30 day"));
            $filterCriteria['from'] = $from->format('d.m.Y');
            $filterCriteria['to'] =  $to->format('d.m.Y');
        }
        else
        {
            $filterCriteria = $request->get('filter');
        }
        
        $filterForm->bindData($filterCriteria);

        //$eventsList = $eventManager->getTeamUpcomingEvents($team);
        $eventsList = $eventManager->getTeamEvents($team,$filterCriteria);
        
        foreach($eventsList as $dayEvents)
        {
            foreach($dayEvents as $event)
            {
                 $existLineup = $eventManager->getEventLineup($event);
                 
                if(null != $existLineup && $existLineup->getStatus() == 'closed')
                {
                    $event->setClosed(true);
                }
            }
        }

        $attendanceMatrix = $eventManager->getUpcomingEventsAttendanceMatrix($members,$eventsList,$security->getIdentityUser());

        $user = ServiceLayer::getService('security')->getIdentity()->getUser();
        $season = $seasonManager->getTeamCurrentSeason($team);
  

        return array(
                    'list' => $members,
                    'team_id' => $request->get('team_id'),
                    'team' => $team,
                    'eventsList' => $eventsList,
                    'attendanceMatrix' => $attendanceMatrix,
                    'filterForm' => $filterForm,
                    'user' => $user,
                    'season' => $season,
                    'layoutTheme' => $request->get('layout')
        );
    }
    
  
    
     public function playersAttendanceAction()
    {
        $data = $this->getPlayersAttendanceActionData();
        return $this->render('Webteamer\Team\TeamModule:team:players_attendance.php', $data);
    }
    
    public function billingAction()
    {
        $request = ServiceLayer::getService('request');
        $security = ServiceLayer::getService('security');

        $teamManager = ServiceLayer::getService('TeamManager');
        $seasonManager = ServiceLayer::getService('TeamSeasonManager');

        $season = $seasonManager->getSeasonById($request->get('id'));
        $team = $teamManager->getRepository()->find($request->get('team_id'));

        //$this->checkSettingTeamPermission($team);

        
       
        $bill = $seasonManager->calculateSeasonBilling($season, $team, $request->get('costsPerAttendance'));
        
        $seasons = $seasonManager->getEventFormChoices($team);

        return $this->render('Webteamer\Team\TeamModule:team:billing.php', array(
                'request' => $request,
                'team' => $team,
                'season' => $season,
                'calculationType' => $request->get('calculationType'),
                'bill' => $bill,
                'seasons' => $seasons
        ));

    }
    
    public function addSettingsAction()
    {
        $request = ServiceLayer::getService('request');
        $teamManager = ServiceLayer::getService('TeamManager');
        $team = $teamManager->findTeamById($request->get('team_id'));
        $this->checkSettingTeamPermission($team);
        
        $settings = new TeamSettings();
        $settings->setTeamId($request->get('team_id'));
        $settings->setSettingsGroup($request->get('group'));
        $settings->setValue($request->get('val'));
         
        $settingName = (null == $request->get('name')) ? $request->get('val') : $request->get('name');
        $settings->setName($settingName);
        $teamManager->saveSetting($settings);
        
        $layout = ServiceLayer::getService('layout');
        $layout->setTemplate(null);
        $result = array('result' => 'success','value' => $settings->getValue(),'name' => $settingName);
        $response = new ControllerResponse();
        $response->setStatus('success');
        $response->setContent(json_encode($result));
        $response->setType('json');
        return $response;
    }
    
    public function hashtagAction()
    {
        $request = ServiceLayer::getService('request');
        $hashtag = $request->get('hashtag');
        
        $teamWallManager = ServiceLayer::getService('TeamWallManager');
        $repo = ServiceLayer::getService('TeamRepository');
        $team = $repo->find($request->get('team_id'));
        ServiceLayer::getService('security')->getIdentity()->checkTeamAccess($team);
        $wallPosts = $teamWallManager->getHashtagWallPosts($team,$hashtag);

        return $this->render('Webteamer\Team\TeamModule:wall:hashtag.php', array(
                    'team' => $team,
                    'wallPosts' => $wallPosts,
                    'hashtag' => $hashtag
        ));
     
    }

    
    private function checkManageTeamPermission($team)
    {
        if( !ServiceLayer::getService('security')->getIdentity()->hasTeamPermission('managePlayers',$team))
        {
             throw  new AclException();
        }
    }
    
    private function checkSettingTeamPermission($team)
    {
        if( !ServiceLayer::getService('security')->getIdentity()->hasTeamPermission('manageSettings',$team))
        {
             throw  new AclException();
        }
    }
    
    
}

?>