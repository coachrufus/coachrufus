<?php

namespace Webteamer\Team\Controller;

use AclException;
use Core\Controller;
use Core\ControllerResponse;
use Core\ServiceLayer;
use Core\Validator;
use Webteamer\Team\Form\AddPlayerForm;
use Webteamer\Team\Form\ModalEditPlayerForm;

/**
 * @author Marek Hubáček
 * @version 1.0
 */
class TeamPlayerController extends Controller {

    public function removePlayerAttendanceAction()
    {
        $request = ServiceLayer::getService('request');
        $eventManager = ServiceLayer::getService('EventManager');

        $playerId = $request->get('player_id');
        $eventId = $request->get('event_id');


        $player = ServiceLayer::getService('PlayerRepository')->find($playerId);
        $event = ServiceLayer::getService('EventRepository')->find($eventId);

        $eventManager->deleteEventAttendance($player, $event);

        $response = new ControllerResponse();
        $response->setStatus('success');
        $response->setContent('create_success');
        $response->setType('json');
        return $response;
    }

    /**
     * URL:  /team/players/:team_id 
     * ROUTE: team_public_players_list 
     * @return type
     */
    public function publicPlayersListAction()
    {
        $request = ServiceLayer::getService('request');
        $translator = ServiceLayer::getService('translator');
        $teamManager = ServiceLayer::getService('TeamManager');
        $teamEventManager = ServiceLayer::getService('TeamEventManager');
        $playerStatManager = ServiceLayer::getService('PlayerStatManager');
        $playerManager = ServiceLayer::getService('PlayerManager');

        $teamManager->setTranslator($translator);
        $list = $teamManager->getActiveTeamPlayers($request->get('team_id'));
        $team = $teamManager->findTeamById($request->get('team_id'));
        ServiceLayer::getService('security')->getIdentity()->checkTeamAccess($team);
        $eventsList = $teamEventManager->getTeamUpcomingEvents($team);

        foreach ($list as $teamPlayer)
        {
            $player = $playerManager->findPlayerById($teamPlayer->getPlayerId());
            if (null != $player)
            {
                $playerSports = $playerManager->findPlayerSports($player);
                foreach($playerSports as $playerSport)
                {
                    if(null != $playerSport->getSport())
                    {
                         $playerSport->getSport()->setName(trim($translator->translate($playerSport->getSport()->getName())));
                    }
                }
                $player->setSports($playerSports);
                $rating = $playerStatManager->getPlayerRating($player);
                $playerStats = $playerStatManager->getTeamPlayerStat($teamPlayer);

                $teamPlayer->setRating($rating['average']);
                $teamPlayer->setPlayer($player);
                $teamPlayer->setStats($playerStats);
            }
        }

        return $this->render('Webteamer\Team\TeamModule:teamPlayer:public_list.php', array(
                    'list' => $list,
                    'team_id' => $request->get('team_id'),
                    'team' => $team,
                    'eventsList' => $eventsList,
        ));
    }

    public function importAction()
    {
        $request = ServiceLayer::getService('request');
        $teamManager = ServiceLayer::getService('TeamManager');
        $notifyManger = ServiceLayer::getService('notification_manager');
        $translator = ServiceLayer::getService('translator');
        $validator = new Validator();
        $team = $teamManager->findTeamById($request->get('team_id'));
        $this->checkManageTeamPermission($team);
        $file = file($_FILES['members_import_file']['tmp_name']);
        array_shift($file);
        $result = array();
        foreach ($file as $row)
        {
            $columns = explode(';', $row);
            $data['first_name'] = trim($columns[0]);
            $data['last_name'] = trim($columns[1]);
            $data['email'] = trim($columns[2]);
            $data['player_id'] = 0;
            $data['team_role'] = 'PLAYER';
            $data['team_id'] = $request->get('team_id');

            $teamPlayer = $teamManager->saveTeamPlayer($data);

            $validEmail = $validator->validateEmail($data['email']);
            if ($validEmail)
            {
                $data['status'] = 'waiting-to-confirm';
                $notifyManger->sendMemberInvitation(ServiceLayer::getService('security')->getIdentity()->getUser(), $team, $teamPlayer);
            }
            else
            {
                $data['status'] = 'confirmed';
            }


            $result['players'][] = $data['first_name'] . ' ' . $data['last_name'] . ' ' . $data['email'];
        }
        $request->addFlashMessage('team_create_player_success', $translator->translate('CREATE_PLAYER_SUCCESS'));
        $result['result'] = 'SUCCESS';
        $result['message'] = $translator->translate('Import success!');
        $layout = ServiceLayer::getService('layout');
        $layout->setTemplate(null);

        $response = new ControllerResponse();
        $response->setStatus('success');
        $response->setContent(json_encode($result));
        $response->setType('json');
        return $response;
    }

    public function templateAction()
    {
        $layout = ServiceLayer::getService('layout');
        $layout->setTemplate(null);
        $response = $this->render('Webteamer\Team\TeamModule:teamPlayer:import_template.php', array(
        ));
        $response->setType('application/octet-stream');
        header('Content-Description: File Transfer');
        header('Content-Disposition: attachment; filename=template.csv');
        header('Content-Transfer-Encoding: binary');
        header('Connection: Keep-Alive');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        return $response;
    }

    /**
     * ROUTE: team_players_list
     * URL: /team/manage-players/:team_id
     */
    public function playersListAction()
    {
        $request = ServiceLayer::getService('request');
        $router = ServiceLayer::getService('router');
        $translator = ServiceLayer::getService('translator');
        $teamManager = ServiceLayer::getService('TeamManager');
        $teamPlayerManager = ServiceLayer::getService('TeamPlayerManager');
        $privacyRepo = \Core\ServiceLayer::getService('PlayerPrivacyRepository');
        $playerRepo = ServiceLayer::getService('PlayerRepository');
        $teamManager->setTranslator($translator);

        $team = $teamManager->findTeamById($request->get('team_id'));
        $this->checkManageTeamPermission($team);
        
        
        $security = ServiceLayer::getService('security');
        $userIdentity =$security->getIdentity();
        $user = $userIdentity->getUser();
         
       /*
        if ('POST' == $request->getMethod() && 'gdpr_team_players_agreement' == $request->get('f_action'))
        {
            $userPrivacy = new \Webteamer\Player\Model\PlayerPrivacy();
            $userPrivacy->setSection('team_admin_agreement');
            $userPrivacy->setUserGroup('coachrufus');
            $userPrivacy->setPlayerId($user->getId());
            $userPrivacy->setValue(1);
            $userPrivacy->setData(json_encode(array('team_id' =>$team->getId())));
            $privacyRepo->save($userPrivacy);
            $request->redirect();
        }

        if (!$security->hasTeamAdminAgreements($security->getIdentity()->getUser(),$team))
        {
            $layout = \Core\ServiceLayer::getService('layout');
            $layout->setTemplate(GLOBAL_DIR . '/templates/ClearLayout.php');
               $layout->setTemplateParameters(array('body-class' => 'full-overlay-window'));

            return $this->render('Webteamer\Team\TeamModule:teamPlayer:gdpr_team_players.php', array(
             'team' => $team
                
            ));
        }

*/

        $sortLinks = array(
            'name' => array(
                'up' => array('link' =>$router->link('team_players_list',array('team_id' => $team->getId(),'sort' => 'name', 'sort-dir' => 'up')),'status' => ''),
                'down' => array('link' => $router->link('team_players_list',array('team_id' => $team->getId(),'sort' => 'name', 'sort-dir' => 'down')),'status' => ''),
                ),  
            'role' => array(
                'up' => array('link' =>$router->link('team_players_list',array('team_id' => $team->getId(),'sort' => 'role', 'sort-dir' => 'up')),'status' => ''),
                'down' => array('link' =>$router->link('team_players_list',array('team_id' => $team->getId(),'sort' => 'role', 'sort-dir' => 'down')),'status' => ''),
                ), 
            'level' => array(
                'up' => array('link' =>$router->link('team_players_list',array('team_id' => $team->getId(),'sort' => 'level', 'sort-dir' => 'up')),'status' => ''),
                'down' => array('link' =>$router->link('team_players_list',array('team_id' => $team->getId(),'sort' => 'level', 'sort-dir' => 'down')),'status' => ''),
                ) , 
            'status' => array(
                'up' => array('link' =>$router->link('team_players_list',array('team_id' => $team->getId(),'sort' => 'status', 'sort-dir' => 'up')),'status' => ''),
                'down' => array('link' =>$router->link('team_players_list',array('team_id' => $team->getId(),'sort' => 'status', 'sort-dir' => 'down')),'status' => ''),
                )  
        );
        
        
        //sort criteria
        $sortCriteria = array();
        $sortCriteria['sortName']  = $request->get('sort');
        $sortCriteria['sortDir']  = $request->get('sort-dir');
        
        
        $players = $teamManager->getAllTeamPlayers($request->get('team_id'),array('sortCriteria' => $sortCriteria));
        
        if(null == $request->get('sort'))
        {
             $list = $teamPlayerManager->groupPlayersByStatus($players);
        }
        else
        {
             $list = $players;
             $sortLinks[$request->get('sort')][$request->get('sort-dir')]['status'] = 'active';
        }

        $editForm = new AddPlayerForm();
        $modalForm = new ModalEditPlayerForm();
        $addform = new AddPlayerForm();

       
        
        
        
        
        $manageRole = $teamManager->userCanManageTeam($userIdentity, $team);
        $validator = new Validator();

        $showProgress = false;
        if ('progress' == $request->get('type'))
        {
            $showProgress = true;
        }

        $stepsManager = \Core\ServiceLayer::getService('StepsManager');
        $finishedProgressStep = $stepsManager->isStepFinished('team_player_create');
        
        //check players number
        $teamCreditManager = \Core\ServiceLayer::getService('TeamCreditManager');
        $teamLimitOverflow = false;
        if(!$teamCreditManager->teamHasProPackage($team))
        {
            if(count($list['confirmed']+$list['unknown']+$list['waiting-to-confirm']) >= $teamCreditManager->getTeamPlayersLimit($team))
            {
                $teamLimitOverflow = true;
            }
        }

        if ('POST' == $request->getMethod())
        {
            $playersData = $request->get('player');

            $playerNumbers = array();
            foreach ($playersData as $id => $data)
            {
                if (in_array($data['player_number'], $playerNumbers))
                {
                    $validator->addErrorMessage('player_number_unique_' . $id, $translator->translate('Player number must be unique'));
                }
                if ('' != $data['player_number'])
                {
                    $playerNumbers[] = $data['player_number'];
                }
                //t_dump($user->getId());
                //t_dump($id);
                $teamPlayer = $teamPlayerManager->getTeamPlayerById($id);
                //check admin roles
                 if($user->getId() ==  $teamPlayer->getPlayerId() && $teamPlayer->getTeamRole() == 'ADMIN' && $data['team_role'] != 'ADMIN')
                {
                    $validator->addErrorMessage('team_role_'. $id, $translator->translate('TEAM_PLAYER_ADMIN_ROLE_ERROR'));
                }
                
            }
            
            
            //check number of admins
            if(!$teamCreditManager->teamHasProPackage($team))
            {
                $adminsCount = 0;
                $adminsLimit = $teamCreditManager->getAdminsLimit($team);
                foreach($playersData as $id => $data)
                {
                    if($data['team_role'] == 'ADMIN')
                    {
                        $adminsCount++;
                    }
                }
                
                if($adminsCount > $adminsLimit)
                {
                     $validator->addErrorMessage('admins_count',$translator->translate('You have reached the  maximum number of admins for current package'));
                }
            }
            


            if (!$validator->hasErrors())
            {
                foreach ($playersData as $id => $data)
                {
                    if ($data['player_number'] == '')
                    {
                        $data['player_number'] = null;
                    }
                    
                    $teamPlayer = $teamPlayerManager->getTeamPlayerById($id);
                     $teamPlayerStatusOld = $teamPlayer->getStatus();
                    $teamManager->updatePlayerFromArray($id, $data);
                    
                   
                    //if status changed, send notify
                    
                    if($teamPlayer->getPlayerId() > 0)
                    {
                        $player = $playerRepo->find($teamPlayer->getPlayerId());
                        
                        if($teamPlayerStatusOld != $data['status']  &&  $data['status'] == 'unactive'  &&  $teamPlayerStatusOld == 'unconfirmed')
                        {
                            //send  refused
                             $notifyManger = ServiceLayer::getService('notification_manager');
                             $notifyManger->sendTeamJoinRequestRefused($user,$team,$player );
                              $pushNotificationManager = ServiceLayer::getService('SystemNotificationManager');
                              $data = array('admin' => $user,'team' => $team);
                              $pushNotificationManager->sendTeamJoinRequestNotify($player,'refused',$data);
                            
                        }

                        if($teamPlayerStatusOld != $data['status'] && $data['status'] == 'confirmed'  &&  $teamPlayerStatusOld == 'unconfirmed')
                        {
                             $notifyManger = ServiceLayer::getService('notification_manager');
                             $notifyManger->sendTeamJoinRequestAccepted($user,$team,$player );
                             
                             $pushNotificationManager = ServiceLayer::getService('SystemNotificationManager');
                              $data = array('admin' => $user,'team' => $team);
                              $pushNotificationManager->sendTeamJoinRequestNotify($player,'accepted',$data);
                              
                              $pushNotificationManager->changeTeamNotificationsSetup($player,$team->getId(),'enable');
                        }
                    }
                    
                    
                   
                }
                

                //reload info about players counts
                ServiceLayer::getService('security')->reloadUser($user);
                
                
                $request->addFlashMessage('team_player_save_success', $translator->translate('UPDATE_ENTITY_SUCCESS'));
                $request->redirect($router->link('team_players_list', array('team_id' => $request->get('team_id'), 'type' => $request->get('type'))));
            }
        }
        return $this->render('Webteamer\Team\TeamModule:teamPlayer:team_players_list.php', array(
                    'list' => $list,
                    'team_id' => $request->get('team_id'),
                    'team' => $team,
                    'editForm' => $editForm,
                    'modalForm' => $modalForm,
                    'manageRole' => $manageRole,
                    'addForm' => $addform,
                    'showProgress' => $showProgress,
                    'type' => $request->get('type'),
                    'validator' => $validator,
                    'finishedProgressStep' => $finishedProgressStep,
                    'teamLimitOverflow' => $teamLimitOverflow,
                    'statusEnum' => $teamManager->getPlayerStatusEnum(),
                    'sorted' => (null != $request->get('sort') ? true : false),
                    'sortLinks' => $sortLinks,
            'user' => $user
        ));
    }

    private function checkManageTeamPermission($team)
    {
        if (!ServiceLayer::getService('security')->getIdentity()->hasTeamPermission('managePlayers', $team))
        {
            throw new AclException();
        }
    }

    public function createTeamPlayerAction()
    {
        $security = ServiceLayer::getService('security');
         $user = $security->getIdentity()->getUser();
        $request = ServiceLayer::getService('request');
        $router = ServiceLayer::getService('router');
        $translator = ServiceLayer::getService('translator');
        $teamManager = ServiceLayer::getService('TeamManager');
        $repo = ServiceLayer::getService('TeamRepository');
        $player_repo = ServiceLayer::getService('PlayerRepository');
        $notifyManger = ServiceLayer::getService('notification_manager');
        $validator = new Validator();
        $team = $teamManager->findTeamById($request->get('team_id'));
        $this->checkManageTeamPermission($team);
        
        $teamPlayers = $teamManager->getTeamPlayers($team->getId());
        $teamCreditManager = \Core\ServiceLayer::getService('TeamCreditManager');
        
         $errors = array();
        
        if(!$teamCreditManager->teamHasProPackage($team))
        {
            if(count($teamPlayers) >= $teamCreditManager->getTeamPlayersLimit($team))
            {
                 //$errors[] = $translator->translate('TEAM_PLAYER_TOO_MANY_PLAYERS');
                $request->addFlashMessage('too_many_players_free',$translator->translate('TOO_MANY_PLAYERS_MODAL_TEXT'));
                $request->redirect($router->link('team_players_list',array('team_id' => $team->getId())));
            }
            $adminsLimit = $teamCreditManager->getAdminsLimit($team);
            $teamAdminsCount = 0;
            foreach($teamPlayers as $teamPlayer)
            {
                if($teamPlayer->getTeamRole() == 'ADMIN')
                {
                    $teamAdminsCount++;
                }
            }
            
            if($teamAdminsCount > $adminsLimit)
            {
                //$errors[] = $translator->translate('TEAM_PLAYER_TOO_MANY_ADMINS');
                $request->addFlashMessage('too_many_admins_free',$translator->translate('TOO_MANY_ADMINS_MODAL_TEXT'));
                $request->redirect($router->link('team_players_list',array('team_id' => $team->getId())));
            }
        }
        
        

        $playersData = $request->get('player');
        foreach ($playersData as $playerId => $data)
        {
            //check only if at least one entry exist 
            if(null != trim($data['first_name']) or null != trim($data['last_name'])  or null != trim($data['email']))
            {
                $data['email'] = trim($data['email']);
                if (null == $data['first_name'])
                {
                    $errors[] = $translator->translate('CREATE_PLAYER_FAIL: name  is required');
                }

                $data['status'] = 'confirmed';
                if (null != $data['email'])
                {
                    $validEmail = $validator->validateEmail($data['email']);
                    $validEmailMessage = 'CREATE_PLAYER_FAIL: unvalid email';

                    //check unique email in team
                    foreach($teamPlayers as $teamPlayer)
                    {
                        if(strtolower($teamPlayer->getEmail()) == strtolower($data['email']))
                        {
                            $validEmail = false;
                            $validEmailMessage = 'CREATE_PLAYER_FAIL: unique email violent';
                        }
                    }

                    if ($validEmail)
                    {
                        $data['status'] = 'waiting-to-confirm';
                        //check if user with this email exist
                        $existRufusUser = $player_repo->findOneBy(array('email' => $data['email'] ));
                        if(null != $existRufusUser)
                        {
                            $data['player_id'] = $existRufusUser->getId();
                            $data['first_name'] = $existRufusUser->getName();
                            $data['last_name'] = $existRufusUser->getSurname();
                        }
                        
                        
                    }
                    else
                    {
                        $errors[] = $translator->translate($validEmailMessage);
                    }
                }

                if (!empty($errors))
                {
                    $request->addFlashMessage('team_create_player_fail', implode('<br/>', $errors));
                    $request->redirect($router->link('team_players_list', array('team_id' => $request->get('team_id'))));
                }
                else
                {
                    $data['player_id'] = $data['player_id'];
                    $data['team_id'] = $request->get('team_id');
                    $data['team_role'] = 'PLAYER';
                    $data['level'] = 'beginner';
                    $data['player_number'] = $data['player_number'];
                    $teamPlayer = $teamManager->saveTeamPlayer($data);
                 

                    if (null != $data['email'])
                    {
                        $notifyManger = ServiceLayer::getService('notification_manager');
                        $notifyManger->sendMemberInvitation(ServiceLayer::getService('security')->getIdentity()->getUser(), $team, $teamPlayer);
                        $request->addFlashMessage('team_create_player_success', $translator->translate('CREATE_PLAYER_SUCCESS'));
                    }
                    else
                    {
                         $request->addFlashMessage('team_create_player_success', $translator->translate('CREATE_PLAYER_SUCCESS_NO_EMAIL'));
                    }
                    ServiceLayer::getService('StepsManager')->finishCreateTeamPlayer($team);

                    $notificationManager = ServiceLayer::getService('SystemNotificationManager');
                    $notificationManager->createPushNotify('create_player',array('team' => $team,'player' => $teamPlayer));
                    
                    if(null != $existRufusUser)
                    {
                        $pushData = array('sender' => $user,'team' => $team,'recipient' => $existRufusUser);
                        $notificationManager->sendTeamInvitationPushNotify($pushData);
                    }

                    //$data['verify_hash'] = md5(time().$data['email']);
                }
            }
        }
        
        //email marketing
       
       
        $emailMarketingManager = \Core\ServiceLayer::getService('EmailMarketingManager');
        $emailMarketingManager->addUserTag($user,'add_member');
      

         $request->redirect($router->link('team_players_list', array('team_id' => $request->get('team_id'),'type' => $request->get('type'))));

    }

    public function removePlayerAction()
    {
        $request = ServiceLayer::getService('request');
        $teamManager = ServiceLayer::getService('TeamManager');

        $player = $teamManager->findTeamPlayerById($request->get('pid'));
        $team = $teamManager->findTeamById($player->getTeamId());
        $this->checkManageTeamPermission($team);
        $this->checkManageTeamPermission($team);
        $teamManager->removeTeamPlayer($player);
        
        //reload user because of players limits
        $security = ServiceLayer::getService('security');
        $user = $security->getIdentity()->getUser();
        $security->reloadUser($user);
        //refresh
    }

    public function unactivatePlayerAction()
    {
        $request = ServiceLayer::getService('request');

        $teamPlayerManager = ServiceLayer::getService('TeamPlayerManager');
        $teamManager = ServiceLayer::getService('TeamManager');

        $player = $teamManager->findTeamPlayerById($request->get('pid'));
        $team = $teamManager->findTeamById($player->getTeamId());
        $this->checkManageTeamPermission($team);
        $teamPlayerManager->unactivateTeamPlayer($player);
        $request->addFlashMessage('unactivate_player_success', $this->getTranslator()->translate('UNACTIVATE PLAYER SUCCESS'));
        $request->redirect();
    }
    
    public function getTeamPlayerStatsActionData($playerId = null)
    {
          $request = ServiceLayer::getService('request');
        if($playerId == null)
        {
            $playerId = $request->get('player_id');
        }
      
        $teamManager = ServiceLayer::getService('TeamManager');
        $teamPlayer = $teamManager->findTeamPlayerById($playerId);
        $team = $teamManager->findTeamById($teamPlayer->getTeamId());
        ServiceLayer::getService('security')->getIdentity()->checkTeamAccess($team);
        $playerStatManager = ServiceLayer::getService('PlayerStatManager');
        $matchOverview = $playerStatManager->getTeamPlayerAllMatchOverview($teamPlayer);
        

        return  array(
                    'team' => $team,
                    'matchOverview' => $matchOverview
        );
    }

    public function teamPlayerStatsAction()
    {
        $data = $this->getTeamPlayerStatsActionData();
        return $this->render('Webteamer\Team\TeamModule:teamPlayer:player_stat.php',$data);
    }

    /**
     * Send invitation to player
     */
    public function sendMemberInvitationAction()
    {
        $request = ServiceLayer::getService('request');
        $teamManager = ServiceLayer::getService('TeamManager');
        $teamPlayerManager = ServiceLayer::getService('TeamPlayerManager');
        $team = $teamManager->findTeamByHash($request->get('hash'));

        $this->checkManageTeamPermission($team);
        $teamPlayer = $teamPlayerManager->getTeamPlayerById($request->get('id'));
        
        //ak zadal email, tak zmenim
        if (null != $request->get(('email')))
        {
            $teamPlayer->setEmail($request->get('email'));
        }
        $teamPlayer->setStatus('waiting-to-confirm');
        $teamManager->getRepository()->saveTeamPlayer($teamPlayer);
       
        $security = ServiceLayer::getService('security');
        $user = $security->getIdentity()->getUser();
        $notification_manager = ServiceLayer::getService('notification_manager');
        $notification_manager->sendMemberInvitation($user, $team, $teamPlayer);
        $request->addFlashMessage('team_player_invitation_send', $this->getTranslator()->translate('INVITATION_SEND_SUCCESS'));
        $request->redirect();
       
    }
    
    public function leaveTeamAction()
    {
        $request = ServiceLayer::getService('request');
        $teamManager = ServiceLayer::getService('TeamManager');
        $teamPlayerManager = ServiceLayer::getService('TeamPlayerManager');
        $team = $teamManager->findTeamById($request->get('team_id'));
        $security = ServiceLayer::getService('security');
        $user = $security->getIdentity()->getUser();

        $player = $teamPlayerManager->findTeamPlayerByPlayerId($team->getId(),$user->getId());
        $player->setStatus('decline');
        $player->setPlayerId(0);
        $teamPlayerManager->saveTeamPlayer($player);
        
        //send notify to admins
         $notificationManager = ServiceLayer::getService('SystemNotificationManager');
        //send admin mail
        $teamAdmins = $teamManager->getTeamAdminList($team);
        $userRepo = ServiceLayer::getService('user_repository');
        foreach($teamAdmins as $teamAdmin)
        {
            $teamAdminUser = $userRepo->find($teamAdmin->getPlayerId());
            $notifyManager = ServiceLayer::getService('notification_manager');
            if(null != $teamAdminUser)
            {
                $data['player_name'] =  $user->getFullName();
                $data['team_name'] = $team->getName();
                $data['email'] = $teamAdminUser->getEmail();
                $data['scouting_link'] = WEB_DOMAIN.$this->getRouter()->link('scouting');
                $data['lang'] =  (null != $teamAdminUser->getDefaultLang()) ? $teamAdminUser->getDefaultLang() : 'en';
                $notifyManager->sendTeamLeaveNotification($data);
                
               
                $notificationManager->sendPlayerLeaveTeamPushNotify($teamAdminUser,array('player' => $user,'team' => $team ));
            }
        }
        
        $request->addFlashMessage('dashboard_flash_message', $this->getTranslator()->translate('LEAVE_TEAM_SUCCES'));
        
        $request->redirect($this->getRouter()->link('player_dashboard',array('userid' => $user->getId())));
        
    }
    
    public function teamJoinRequestCancelAction()
    {
        $request = ServiceLayer::getService('request');
        $teamManager = ServiceLayer::getService('TeamManager');
        $team = $teamManager->findTeamByHash($request->get('hash'));
        $security = ServiceLayer::getService('security');
        $user = $security->getIdentity()->getUser();

        $teamPlayer = $teamManager->findTeamPlayerByPlayerId($team,$user->getId());
        $teamPlayer->setStatus('decline');
        $teamManager->getRepository()->saveTeamPlayer($teamPlayer);
            
        
        $request->addFlashMessage('member_team_join_request_cancel_message', $this->getTranslator()->translate('Cancel accepted'));
        $request->redirect($this->getRouter()->link('player_dashboard', array('id' => $user->getId())));
        
      
        
    }

}

?>