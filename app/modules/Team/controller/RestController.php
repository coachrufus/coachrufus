<?php

namespace Webteamer\Team\Controller;

use Core\RestApiController;
use Core\ServiceLayer;
use Webteamer\Team\Form\ModalEditPlayerForm;
use Webteamer\Team\Form\TeamPlayerValidator;

/**
 * @author Marek Hubáček
 * @version 1.0
 * @created 17-10-2015 21:47:28
 */
class RestController extends RestApiController {

     private function checkManageTeamPermission($team)
    {
        if (!ServiceLayer::getService('security')->getIdentity()->hasTeamPermission('managePlayers', $team))
        {
            throw new AclException();
        }
    }
    
    public function editTeamPlayerAction()
    {
        $request = ServiceLayer::getService('request');
        $translator = ServiceLayer::getService('translator');
        $security = ServiceLayer::getService('security');
        $user = $security->getIdentity()->getUser();
        $playerManager = ServiceLayer::getService('PlayerManager');
        $teamPlayerManager =  ServiceLayer::getService('TeamPlayerManager');
        $teamManager = ServiceLayer::getService('TeamManager');
       
        $teamPlayer = $teamPlayerManager->getTeamPlayerById($request->get('player_id'));
        
        $team = $teamManager->findTeamById($teamPlayer->getTeamId());
        $this->checkManageTeamPermission($team);
         
        $oldPlayerEmail = $teamPlayer->getEmail();
        $oldPlayerRole = $teamPlayer->getTeamRole();
        $form = new ModalEditPlayerForm();
        $validator = new TeamPlayerValidator();
        $validator->setRules($form->getFields());
        $form->setEntity($teamPlayer);

        $post_data = $request->get($form->getName());

        if("" == $post_data['player_number'])
        {
            $post_data['player_number'] = null;
        }
        
        //registred player cannot have different mail
        if(0 != $teamPlayer->getPlayerId())
        {
            unset($post_data['email']);
        }
        
        
        $form->bindData($post_data);
        $validator->setData($post_data);
        $validator->validateData();

        //check unique email in team
        $sendInvitation = false;
        if(array_key_exists('email', $post_data) && null != $post_data['email'] )
        {
            //send invitation to players which had empty email 
            if($post_data['email'] != $oldPlayerEmail && $oldPlayerEmail == '')
            {
                $sendInvitation = true;
            }

            $teamPlayers = $teamManager->getTeamPlayers($teamPlayer->getTeamId());
            foreach($teamPlayers as $existTeamPlayer)
            {
                if(strtolower($existTeamPlayer->getEmail()) == strtolower($post_data['email']) && (strtolower($oldPlayerEmail) != strtolower($post_data['email'])))
                {
                    $validator->addErrorMessage('email', $translator->translate('TEAM_PLAYER_UNIQUE_EMAIL_ERROR'));
                }
            }
        }
        
        
        //admin can not change self to player
        if($user->getId() ==  $teamPlayer->getPlayerId() && $oldPlayerRole == 'ADMIN' && $post_data['team_role'] != 'ADMIN')
        {
            $validator->addErrorMessage('team_role', $translator->translate('TEAM_PLAYER_ADMIN_ROLE_ERROR'));
        }
      

        if (!$validator->hasErrors())
        {
            $resultData = $post_data;
            $resultData['team_player_id'] = $request->get('player_id');
            $teamPlayerManager->saveTeamPlayer($teamPlayer);
            
            //regenerate avatar
            $avatar = $playerManager->createAvatar($teamPlayer->getFullname());
            $playerManager->saveTeamPlayerAvatar($teamPlayer,$avatar);

            if($sendInvitation)
            {
                $team = $teamManager->findTeamById($teamPlayer->getTeamId());
                $notifyManger = ServiceLayer::getService('notification_manager');
                $notifyManger->sendMemberInvitation(ServiceLayer::getService('security')->getIdentity()->getUser(), $team, $teamPlayer);
                
                //set user as waiting to confirm
                $teamPlayer->setStatus('waiting-to-confirm');
                $teamPlayerManager->saveTeamPlayer($teamPlayer);
                
            }
            
            
            
            $result = array('result' => 'SUCCESS','player_data' => $resultData );
        }
        else
        {
             $result = array('result' => 'ERROR','errors' => $validator->getErrors());
        }
        
        
       $security->reloadUser($user);
        
       return $this->asJson($result);
    }
    
     public function searchAction()
    {
        $repo = ServiceLayer::getService('TeamRepository');
        $teams = $repo->fulltextNameSearch($this->getRequest()->get('phrase'));
        $result = array();
        foreach($teams as $team)
        {
            $result[] = $repo->convertToArray($team);
        }
        
        return $this->asJson($result);
        
        
    }

}

?>