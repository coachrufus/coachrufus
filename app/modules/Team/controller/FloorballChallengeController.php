<?php
namespace Webteamer\Team\Controller;

use Core\Controller;
use Core\Notifications\FloorballCouponNotificationSender;
use Core\Notifications\FloorballCouponStore;
use Core\ServiceLayer;


/**
 * @author Marek Hubáček
 * @version 1.0
 */
class FloorballChallengeController extends Controller {
   
    public function activationEmailAction()
    {
        $data = $this->getRequest()->get('flch');
        $repo = ServiceLayer::getService('FloorballChallengeTeamRepository');
        $couponRepo = ServiceLayer::getService('CouponRepository');
        
        $coupon  = $couponRepo->findOneBy(array('name' => 'FLOORBALL','charges' => 1));
        $coupon->setCharges(0);
        $couponRepo->save($coupon);
         
        $flchTeam = new \CR\Team\Model\FloorballChallengeTeam();
        $flchTeam->setCouponCode($coupon->getCode());
        $flchTeam->setIp($_SERVER['REMOTE_ADDR']);
        $flchTeam->setRegisterEmail($data['email']);
        $flchTeam->setCreatedAt(new \DateTime());
        $flchTeam->setRegisterName($data['coach']);
        $flchTeam->setSchool($data['school']);
        $flchTeam->setStatus('registration');
        $id = $repo->save($flchTeam);

        $data =  array();
        $data['email'] = 'marek.hubacek@plus421.com';
        $data['default_lang'] = (null != $this->getRequest()->get('lang')) ? $this->getRequest()->get('lang') : 'sk';
        $data['activationGuid'] =  $coupon->getCode().'@'.$id;
        $sender = new FloorballCouponNotificationSender(new FloorballCouponStore());
        $sender->send($data);
    }
    
    public function activationAction()
    {
        $security = ServiceLayer::getService('security');
        $router = $this->getRouter();
        $translator = $this->getTranslator();
        $userIdentity = $security->getIdentity();
        $user = $userIdentity->getUser();
        $teams = $userIdentity->getUserTeams();
        $request = $this->getRequest();
        $code = $this->getRequest()->get('code');

        if(empty($teams))
        {
             setcookie('flch_register_code', $code,time()+(60*60*24*90),'/');
             $request->Redirect($router->link('player_dashboard',array('userid' =>  $user->getId() )));
        }
        
        if ('POST' == $request->getMethod())
        {
             $postTeams = $this->getRequest()->get('team');
             foreach($postTeams as $postTeamId)
             {
                 $flchManager = ServiceLayer::getService('FlchManager'); 
                 $flchManager->createTeamFromCode($code,$postTeamId);
             }
            

            
        }
        
        
        return $this->render('Webteamer\Team\TeamModule:floorballChallenge:activation.php', array(
             'teams' => $teams ,
            'code' => $code
        ));
       
    }
    
    public function homeAction()
    {
        $request = $this->getRequest();
        $teamManager = ServiceLayer::getService('TeamManager');
        $flchManager = ServiceLayer::getService('FlchManager');
        $playerStatManager = ServiceLayer::getService('PlayerStatManager');
        $team = ServiceLayer::getService('TeamManager')->findTeamById($request->get('team_id'));
        
        /*
        $list = array();
        foreach($flchManager->getFlchTeams() as $flchTeamInfo)
        {
            if(null != $flchTeamInfo->getTeamId())
            {
                $flchTeam = ServiceLayer::getService('TeamManager')->findTeamById($flchTeamInfo->getTeamId());
                
                
                $flchTeam = ServiceLayer::getService('TeamManager')->findTeamById($flchTeamInfo->getTeamId());
                $flchPlayers = $teamManager->getTeamPlayers($flchTeam->getId());

                $statFrom = new \Core\Types\DateTimeEx(new \DateTime('2017-03-01'));
                $statTo = new \Core\Types\DateTimeEx(new \DateTime('2017-06-30'));
                $teamPlayersStats = $playerStatManager->getAllTeamPlayersStat($flchTeam, null, $statFrom, $statTo);

                $maxValues = $teamPlayersStats['maxValues'];
                foreach ($flchPlayers as $teamPlayer)
                {
                    if (array_key_exists($teamPlayer->getId(), $teamPlayersStats))
                    {
                        $teamPlayersStats[$teamPlayer->getId()]->setMaxValues($maxValues);
                        $teamPlayer->setStats($teamPlayersStats[$teamPlayer->getId()]);
                    }
                    $list[] = $teamPlayer;
                }

            }
           

        }
        */
        
        
        
        return $this->render('Webteamer\Team\TeamModule:floorballChallenge:home.php', array(
             'team' => $team      ,
        ));
    }
}