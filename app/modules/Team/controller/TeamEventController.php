<?php

namespace Webteamer\Team\Controller;

use AclException;
use Core\Controller;
use Core\ControllerResponse;
use Core\Notifications\CustomImmediatelyEventInvitationStore;
use Core\Notifications\EventInvitationNotificationSender;
use Core\Notifications\ImmediatelyEventInvitationStore;
use Core\ServiceLayer;
use Core\Types\DateTimeEx;
use CR\TeamSeason\Form\TeamSeasonForm;
use DateTime;
use Webteamer\Event\Form\EventForm;
use Webteamer\Event\Model\Event;
use Webteamer\Event\Model\EventCalendar;
use Webteamer\Team\Form\EventValidator;

/**
 * @author Marek HubĂˇÄŤek
 * @version 1.0
 */
class TeamEventController extends Controller {

    private function checkManageTeamPermission($team)
    {
        if (!ServiceLayer::getService('security')->getIdentity()->hasTeamPermission('manageEvents', $team))
        {
            throw new AclException();
        }
    }

    private function getCreateForm($entity, $team)
    {
        $security = ServiceLayer::getService('security');
        $translator = ServiceLayer::getService('translator');
        $manager = ServiceLayer::getService('EventManager');
        $teamManager = ServiceLayer::getService('TeamManager');
        $playgroundManager = ServiceLayer::getService('PlaygroundManager');
        $user = $security->getIdentity()->getUser();
        $playgrounds = $playgroundManager->getUserPlaygrounds($user);
        $playgroundsChoices = array('' => $translator->translate('Select locality'));
        $playgroundsDesc = array();

        foreach ($playgrounds as $playground)
        {
            $playgroundsChoices[$playground->getId()] = $playground->getName();
            $playgroundsDesc[$playground->getId()] = $playground;
            
        }
        
        
        $globalPlaygrounds = $playgroundManager->getGlobalPlaygrounds();
        foreach ($globalPlaygrounds as $playground)
        {
            $playgroundsChoices[$playground->getId()] = $playground->getName();
        }
        
        

        //$playgroundsChoices['new'] = $translator->translate('Add new');
        $playgroundsDesc['new'] = $translator->translate('Create new playground');

        $seasonManager = ServiceLayer::getService('TeamSeasonManager');
        $existSeasonChoices = $seasonManager->getEventFormChoices($team);
         
        if(empty($existSeasonChoices))
        {
            $seasonManager->createTeamDefaultSeason($team);
            $existSeasonChoices = $seasonManager->getEventFormChoices($team);
            $firstSeason = key($existSeasonChoices);
            $entity->setSeason($firstSeason);
        }
        
        if(count($existSeasonChoices) == 1)
        {
            $firstSeason = key($existSeasonChoices);
            $entity->setSeason($firstSeason);
        }
        
        
        $seasonChoices = array('' => $translator->translate('Select season')) + $existSeasonChoices;
        $form = new EventForm();
        
        $endPeriodTypeChoices = array();
        $endPeriodTypeChoices[''] = '';
        $endPeriodTypeChoices['never'] = $translator->translate('Never');
        $endPeriodTypeChoices['after'] = $translator->translate('After');
        $endPeriodTypeChoices['date'] = $translator->translate('On');
        
        $notifyTerminType = array();
        $notifyTerminType['now'] = $translator->translate('Now');
        $notifyTerminType['date'] = $translator->translate('Date');
        $notifyTerminType['beep'] = $translator->translate('Days before');
        
        
       
        
        if(null == $entity->getNotification())
        {
            $entity->setNotification(true);
            $entity->setNotifyGroup('all');
            $entity->setNotifyTerminType('beep');
            $entity->setNotifyBeepDays(1);
        }
       
        
        //$defaultPLaygroundId = current($playgrounds)->getId();
        //$entity->setPlaygroundId($defaultPLaygroundId);

        $form->setEntity($entity);
        $form->setPlaygroundsDesc($playgroundsDesc);

        $form->setFieldChoices('playground_id', $playgroundsChoices);
        $form->setFieldChoices('end_period_type', $endPeriodTypeChoices);
        $form->setFieldChoices('event_type', $manager->getEventTypFormChoices());
        $form->setFieldChoices('period', $manager->getEventPeriodFormChoices());
        $form->setFieldChoices('period_interval', $manager->getEventPeriodIntervalFormChoices());
        $form->setFieldChoices('repeat_days', $manager->getEventRepeatDaysFormChoices());
        $form->setFieldChoices('season', $seasonChoices);
        $form->setFieldChoices('notify_termin_type', $notifyTerminType);
        $form->setFieldChoices('notify_group', array('all' => $translator->translate('All')));
        
        $form->setFieldOption('capacity','message',  $translator->translate('Only numbers allowed'));
        
       

        return $form;
    }

    public function createAction()
    {
        $request = ServiceLayer::getService('request');
        $translator = ServiceLayer::getService('translator');
        $security = ServiceLayer::getService('security');
        $user = $security->getIdentity()->getUser();
        $router = ServiceLayer::getService('router');
        $teamRepo = ServiceLayer::getService('TeamRepository');
        $team = $teamRepo->find($request->get('team_id'));
        $this->checkManageTeamPermission($team);
        $repository = ServiceLayer::getService('EventRepository');
        $playerManager = ServiceLayer::getService('PlayerManager');
        $player = $playerManager->findPlayerById($user->getId());
        $playerLocality = $player->getLocality();
        $playgroundManger = ServiceLayer::getService('PlaygroundManager');


        $entity = new Event();
       
        $entity->setAuthorId($security->getIdentity()->getUser()->getId());
        $entity->setCreatedAt(new DateTime());
        $entity->setTeamId($team->getId());
        $defaultStart = (null == $request->get('date') ) ? new \DateTime(date('Y-m-d') . ' 18:00:00') : new \DateTime($request->get('date') . ' 18:00:00');
        //$entity->setStart($defaultStart);
        $entity->setEndPeriodType('');
        //$entity->setRepeatDays(array($defaultStart->format('N')));
        $form = $this->getCreateForm($entity, $team);

        $form->setFieldValue('start_date', $defaultStart->format('d.m.Y'));
        //$form->setFieldValue('start_time', $defaultStart->format('18:00'));
        $seasonForm = new TeamSeasonForm();

        $validator = new EventValidator();
        $validator->setRules($form->getFields());



        $showProgress = false;
        if ('progress' == $request->get('type'))
        {
            $showProgress = true;
        }
        
        


        if ('POST' == $request->getMethod())
        {
            $post_data = $request->get($form->getName());
            $playgroundData = $request->get('playgrounds');
            $post_data['start'] = $post_data['start_date'] . ' ' . $post_data['start_time'] . ':00';
           
         
            $form->bindData($post_data);
            $validator->setData($post_data);
            $validator->setPlaygroundData($playgroundData);

            $this->setRepeatDays($entity);
            $validator->validateData();
            



            //validacia miesta
            if (!$validator->hasErrors())
            {
                $entity = $form->getEntity();
                if(null != $post_data['end_time_time'])
                {
                    $endTime = new Datetime($post_data['start_date'] . ' ' . $post_data['end_time_time'] . ':00');
                    $entity->setEndTime($endTime);
                }

                if ('new' == $post_data['playground_id'])
                {
                    $playgroundData['author_id'] = $security->getIdentity()->getUser()->getId();
                    $playgroundData['team_id'] = $team->getId();
                    $saveAliasResult = $playgroundManger->savePlaygroundAlias($playgroundData);
                    $playgroundAlias = $saveAliasResult['entity'];
                    $entity->setPlaygroundId($playgroundAlias->getPlaygroundId());
                }

                if ($post_data['playground_id'] == null)
                {
                    $entity->setPlaygroundId(null);
                }

                if ($entity->getPlaygroundId() != null)
                {
                    $playground = $playgroundManger->findPlaygroundById($entity->getPlaygroundId());
                    $playgroundLocality = $playground->getLocality();
                    $eventPlaygroundData = $playgroundManger->convertEntityToArray($playground);
                    $eventPlaygroundData['lat'] = $playgroundLocality->getLat();
                    $eventPlaygroundData['lng'] = $playgroundLocality->getLng();
                    $entity->setPlaygroundData(serialize($eventPlaygroundData));
                }

              
                $eventId = $repository->save($entity);
                $entity->setId($eventId);
                
                $nearestTermin = $entity->getNearestTermin(new \DateTime());
                if(null == $nearestTermin)
                {
                    $nearestTermin = $entity->getNearestTermin(new \DateTime($post_data['start']));
                }
                
                $entity->setCurrentDate($nearestTermin);

                if ($entity->getNotification() == 1 && $entity->getNotifyTerminType() == 'now')
                {
                    $sender = new EventInvitationNotificationSender(new ImmediatelyEventInvitationStore($entity));
                    $sender->sendAll();
                }
                ServiceLayer::getService('StepsManager')->finishCreateEvent($team);
                ServiceLayer::getService('StepsManager')->finishRepeatEvent($team);

                $emailMarketingManager = \Core\ServiceLayer::getService('EmailMarketingManager');
                $emailMarketingManager->addUserTag($user,'create_event');
                $emailMarketingManager->setAfterEventCreateSetup($user,$team,$entity,$nearestTermin->format('Y-m-d'));

                //$request->addFlashMessage('team_event_create_success', $translator->translate('CREATE_EVENT_SUCCESS'));
                $request->redirect($router->link('team_event_detail', array('id' => $entity->getId(),'current_date'=>$nearestTermin->format('Y-m-d'))));
            }
        }

        $stepsManager = \Core\ServiceLayer::getService('StepsManager');
        $finishedProgressStep = $stepsManager->isStepFinished('event_create');

        return $this->render('Webteamer\Team\TeamModule:events:create.php', array(
                    'form' => $form,
                    'seasonForm' => $seasonForm,
                    'request' => $request,
                    'validator' => $validator,
                    'team' => $team,
                    'showProgress' => $showProgress,
                    'playerLocality' => $playerLocality,
                    'finishedProgressStep' => $finishedProgressStep,
                    'user' => $user
        ));
    }

    public function editAction()
    {
        $request = ServiceLayer::getService('request');
        $translator = ServiceLayer::getService('translator');
        $security = ServiceLayer::getService('security');
        $router = ServiceLayer::getService('router');
        $teamRepo = ServiceLayer::getService('TeamRepository');
        $localityManager = ServiceLayer::getService('LocalityManager');
        $statManager = ServiceLayer::getService('PlayerStatManager');
        $repository = ServiceLayer::getService('EventRepository');
        $playgroundManger = ServiceLayer::getService('PlaygroundManager');
        $teamEventManager = ServiceLayer::getService('TeamEventManager');
        $entity = $teamEventManager->findEventById($request->get('event_id'));
        //$entity->setStart(new \DateTime($request->get('event_date').' '.$entity->getStart()->format('H:i:s')));
        
       
        $team = $teamRepo->find($entity->getTeamId());
        //$this->checkManageTeamPermission($team);
        $form = $this->getCreateForm($entity, $team);
        $currentEventDate = new \DateTime($request->get('event_date'));
        $form->setFieldValue('start_date', $currentEventDate->format('d.m.Y'));
        $form->setFieldValue('start_time', $entity->getStart()->format('H:i'));
        
        $entity->setCurrentDate($currentEventDate);
        $eventTimeline = $statManager->getEventTimeline($entity);
         
       
        
       
         
        if(null == $entity->getEndTime())
        {
           
        }
        else
        {
            $form->setFieldValue('end_time_time', $entity->getEndTime()->format('H:i'));
        }
        
        $form->setFieldValue('playground_choice', $entity->getPlaygroundId());
        
        
        
        $seasonForm = new TeamSeasonForm();

        //$playground = $entity->getPlayground();
        //$locality = $localityManager->getLocalityById($playground->getLocalityId());
        $teamPlayground = null;
        if (null != $entity->getPlaygroundDataAsArray())
        {
            $teamPlayground = $playgroundManger->createObjectFromArray($entity->getPlaygroundDataAsArray());
        }
        


        $validator = new EventValidator();
        $validator->setRules($form->getFields());





        if ('POST' == $request->getMethod())
        {
            $post_data = $request->get($form->getName());
            $playgroundData = $request->get('playgrounds');
            $post_data['start'] = $post_data['start_date'] . ' ' . $post_data['start_time'] . ':00';
            
            if($entity->getPeriod() == 'none')
            {
                $lineup = $teamEventManager->getEventLineup($entity);
                if(!empty($lineup))
                {
                    $lineup->setEventDate(new \Datetime($post_data['start_date'].' 00:00:00'));
                    $teamEventManager->saveLineup($lineup);
                }
            }

            if("" == $post_data['playground_id'])
            {
                $post_data['playground_id'] = null;
            }

            $validator->setData($post_data);
            $validator->setPlaygroundData($playgroundData);
            $validator->validateData();
            $form->bindData($post_data);
            
            

            if (!$validator->hasErrors())
            {
                $entity = $form->getEntity();
                
                 if(null != $post_data['end_time_time'])
                {
                    $endTime = new Datetime($post_data['start_date'] . ' ' . $post_data['end_time_time'] . ':00');
                    $entity->setEndTime($endTime);
                }

                $sourceEvent = $teamEventManager->findEventById($request->get('event_id'));
                if ('new' == $post_data['playground_id'])
                {

                    $playgroundData['author_id'] = $security->getIdentity()->getUser()->getId();
                    $playgroundData['team_id'] = $team->getId();
                    $saveAliasResult = $playgroundManger->savePlaygroundAlias($playgroundData);
                    $playgroundAlias = $saveAliasResult['entity'];
                    $post_data['playground_id'] = $playgroundAlias->getPlaygroundId();
                }

                if ($entity->getPlaygroundId() != null && 0 != $entity->getPlaygroundId())
                {
                    $playground = $playgroundManger->findPlaygroundById($post_data['playground_id']);
                    $playgroundLocality = $playground->getLocality();
                    $eventPlaygroundData = $playgroundManger->convertEntityToArray($playground);
                    $eventPlaygroundData['lat'] = $playgroundLocality->getLat();
                    $eventPlaygroundData['lng'] = $playgroundLocality->getLng();
                    $entity->setPlaygroundData(serialize($eventPlaygroundData));
                }


                if ($entity->getNotification() == 1 && $entity->getNotifyTerminType() == 'now')
                {
                    $sender = new EventInvitationNotificationSender(new ImmediatelyEventInvitationStore($entity));
                    $sender->sendAll();
                }

                //days
                $this->setRepeatDays($entity);
                (null == $post_data['notification']) ? $entity->setNotification(0) : $entity->setNotification($post_data['notification']);

                //once ->repeat
                $redirectUrl = $router->link('edit_team_event', array('event_id' => $entity->getId(), 'event_date' => $entity->getStart()->format('Y-m-d')));


                if ('only' == $request->get('change_type'))
                {
                    if ($sourceEvent->getPeriod() == 'none') //just single
                    {
                        $form->bindData($post_data);
                        $entity->setEnd(null);
                        $repository->save($entity);
                    }
                    else //from repeat to single
                    {
                        //create copy of event
                        $clone = clone $entity;
                        $clone->setId(null);
                        $form->setEntity($clone);
                        $form->bindData($post_data);
                        $clone->setEnd(new \DateTime($clone->getStart()->format('Y-m-d')));
                        $clone->setPeriod('none');
                        $newId = $repository->save($clone);

                        $sourceEvent->setOmittedTermins($sourceEvent->getOmittedTermins() . ',' . serialize(new \DateTime($currentEventDate->format('Y-m-d'))));
                        $repository->save($sourceEvent);


                        $redirectUrl = $router->link('edit_team_event', array('event_id' => $newId, 'event_date' => $clone->getStart()->format('Y-m-d')));
                    }
                }
                elseif ('next' == $request->get('change_type'))
                {
                    $entity->setId(null);
                    $form->bindData($post_data);
                    $newId = $repository->save($entity);

                    $sourceEvent->setEnd($currentEventDate);
                    $repository->save($sourceEvent);

                    $redirectUrl = $router->link('edit_team_event', array('event_id' => $newId, 'event_date' => $entity->getStart()->format('Y-m-d')));
                }
                elseif ('all' == $request->get('change_type'))
                {
                    $form->bindData($post_data);
                    $startTermin = $sourceEvent->getStart()->format('Y-m-d') . ' ' . $post_data['start_time'];
                    $entity->setStart(new \DateTime($startTermin));

                    //change from once to repeat, remove end date
                    if ($sourceEvent->getPeriod() != 'none' and $entity->getEndPeriodType() != 'date')
                    {
                        $entity->setEnd(null);
                    }


                    //var_dump($entity);exit;
                    $repository->save($entity);
                }

                //check season
               
                //update lineup
                //if()
               

                $request->addFlashMessage('team_event_edit_success', $translator->translate('EDIT_EVENT_SUCCESS'));
                $request->redirect($redirectUrl);
            }
        }


        return $this->render('Webteamer\Team\TeamModule:events:edit.php', array(
                    'form' => $form,
                    'seasonForm' => $seasonForm,
                    'request' => $request,
                    'validator' => $validator,
                    'team' => $team,
                    'teamPlayground' => $teamPlayground,
                    'event' => $entity,
                    'eventTimeline' => $eventTimeline
        ));
    }

    public function getCalendarMonthInfo($calendar_date)
    {
        if ($calendar_date == null)
        {
            $current_month = date('n');
            $current_year = date('Y');
            $calendar_date = date('Y') . '-' . date('n');
        }
        else
        {
            $calendar_date_parts = explode('-', $calendar_date);
            $current_month = $calendar_date_parts[1];
            $current_year = $calendar_date_parts[0];
        }

        $days_count = date('t', mktime(0, 0, 0, $current_month, 1));
        $first_day_in_month = $calendar_date . '-1';
        $last_day_in_month = $calendar_date . '-' . $days_count;
        $from = new DateTime($first_day_in_month);
        $to = new DateTime($last_day_in_month);

        return array('from' => $from, 'to' => $to, 'current_month' => $current_month, 'current_year' => $current_year, 'days_count' => $days_count, 'calendar_date' => $calendar_date);
    }

    public function calendarAction()
    {
        $request = ServiceLayer::getService('request');
        $translator = ServiceLayer::getService('translator');
        $teamManager = ServiceLayer::getService('TeamManager');
        $teamEventManager = ServiceLayer::getService('TeamEventManager');
        $team = $teamManager->findTeamById($this->getRequest()->get('team_id'));
        ServiceLayer::getService('security')->getIdentity()->checkTeamAccess($team);
        $monthInfo = $this->getCalendarMonthInfo($request->get('calendar_date'));

        $next_calendar_date = date('Y-m', mktime(0, 0, 0, $monthInfo['current_month'], $monthInfo['days_count'] + 1, $monthInfo['current_year']));
        $prev_calendar_date = date('Y-m', mktime(0, 0, 0, $monthInfo['current_month'], -1, $monthInfo['current_year']));
        $month_name = $translator->translate(date('F', mktime(0, 0, 0, $monthInfo['current_month'], 1)));
        


        $events = $teamEventManager->findEvents($team, array('from' => $monthInfo['from'], 'to' => $monthInfo['to']));
        $calendar = new EventCalendar();
        $eventsList = $teamEventManager->buildTeamEventList($events, $monthInfo['from'], $monthInfo['to']);

        $calendar->setEvents($eventsList);

        $overview = $calendar->getCalendarMonthOverview($monthInfo['calendar_date']);
        


        return $this->render('Webteamer\Team\TeamModule:events:calendar.php', array(
                    'team' => $team,
                    'overview' => $overview,
                    'calendar_date' => $monthInfo['calendar_date'],
                    'next_calendar_date' => $next_calendar_date,
                    'prev_calendar_date' => $prev_calendar_date,
                    'month_name' => $month_name,
                    'current_year' => $monthInfo['current_year'],
                    'current_month' => $monthInfo['current_month']
        ));
    }

    public function listAction()
    {
        $request = ServiceLayer::getService('request');
        $teamManager = ServiceLayer::getService('TeamManager');
        $teamEventManager = ServiceLayer::getService('TeamEventManager');
        $statManager = ServiceLayer::getService('PlayerStatManager');
        $sportManager = ServiceLayer::getService('SportManager');
        $team = $teamManager->findTeamById($this->getRequest()->get('team_id'));
        ServiceLayer::getService('security')->getIdentity()->checkTeamAccess($team);
        $calendar_date = $request->get('calendar_date');
        $lastEvent = $teamEventManager->getTeamLastEvent($team, new \DateTime());
        $lastEventScore = null;
        $listType = (null != $request->get('type')) ? $request->get('type') : 'upcoming';
        
        
        
        
        
        $stepsManager = \Core\ServiceLayer::getService('StepsManager');
        if(false == $stepsManager->isStepFinished('event_rating_create')  && ServiceLayer::getService('security')->getIdentity()->hasTeamPermission('manageSettings',$team))
        {
            $progressChainManager = ServiceLayer::getService('ProgressChainManager');
            $currentStep = $progressChainManager->getCurrentStep();
            $lastEvent = $teamEventManager->getTeamLastEvent($team, new \DateTime());
            $router = ServiceLayer::getService('router');
            
            if(null == $lastEvent)
            {
                $link = $router->link('create_team_event',array('team_id' => $team->getId() ));
                $this->getRequest()->redirect($link);
            }
            

            if($currentStep->getTargetLink() == 'team_event_detail' or $currentStep->getTargetLink() == 'team_event_rating')
            {
                $link = $router->link($currentStep->getTargetLink(),array('id' => $lastEvent->getId(),'current_date' => $lastEvent->getCurrentDate()->format('Y-m-d')));
                $this->getRequest()->redirect($link);
            }
            
            if($currentStep->getTargetLink() == 'team_match_new_lineup')
            {
                $link = $router->link($currentStep->getTargetLink(),array('event_id' => $lastEvent->getId(),'event_date' => $lastEvent->getCurrentDate()->format('Y-m-d')));
                $this->getRequest()->redirect($link);
            }
            
            if($currentStep->getTargetLink() == 'team_match_create_live_stat')
            {
                $link = $router->link($currentStep->getTargetLink(),array('event_id' => $lastEvent->getId(),'eventdate' => $lastEvent->getCurrentDate()->format('Y-m-d')));
                $this->getRequest()->redirect($link);
            }
            
        }
        
        
        if (null != $lastEvent)
        {
            $existLineup = $teamEventManager->getEventLineup($lastEvent);
            if (null != $existLineup)
            {
                $matchOverview = $statManager->getMatchOverview($existLineup);
                $lastEventScore = $matchOverview->getResult();
            }
        }
        if ($request->get('d') == 'past')
        {
            $endDate = \DateTime::createFromFormat('Y-m-d-H-i', $request->get('to'));
            $startTime = $endDate->getTimestamp() - (180 * 24 * 60 * 60);
            $paggingStart = new DateTime();
            $paggingStart->setTimestamp($startTime);
            $eventPagger = $teamEventManager->getPaggedPastEvents($team, $paggingStart, $endDate);
            $eventPagger->setSort('reverse');
            $eventPagger->setEndDate($endDate);
        }

        if ($request->get('d') == 'upcoming')
        {
            $startDate = \DateTime::createFromFormat('Y-m-d-H-i', $request->get('from'));
            $startTime = $startDate->getTimestamp();
            $paggingStart = new DateTime();
            $paggingStart->setTimestamp($startTime);
            $eventPagger = $teamEventManager->getPaggedUpcomingEvents($team, $paggingStart);
            $eventPagger->setStartDate($startDate);
           
            
        }

        if (null == $request->get('d') && null == $request->get('d'))
        {
            $paggingStart = new DateTime();
            $paggingStart->setTime(0,0,0);
            $eventPagger = $teamEventManager->getPaggedUpcomingEvents($team, $paggingStart);
            $limitPreviewDate = $eventPagger->getFirstEventDate('Y-m-d');
        }
        
        

        //set pager limits
        if(null != $request->get('lpd'))
        {
            $limitPreviewDate =  $request->get('lpd');
        }
        
        if(null == $limitPreviewDate)
        {
            $limitPreviewDate = date('Y-m-d');
        }
        
        
        $displayPreview = true;
        $displayNext= true;
       

        if(null != $limitPreviewDate && 'upcoming' == $listType)
        {
            $lpdDate  = DateTime::createFromFormat('Y-m-d', $limitPreviewDate);
            $firstEventDate =  $eventPagger->getFirstEventDate('Y-m-d');
            
            if(null == $firstEventDate)
            {
                $displayPreview = false;
            }
            else
            {
                $firstEventDate  = DateTime::createFromFormat('Y-m-d', $firstEventDate);
                if($firstEventDate->getTimestamp() <= $lpdDate->getTimestamp())
                {
                    $displayPreview = false;
                }
            }
        }
        
       
        //limit for past events
        
        $lastEventDate = $eventPagger->getLastEventDate('Y-m-d');
        if($lastEventDate != null && 'past' == $listType)
        {
            $lpdDate  = DateTime::createFromFormat('Y-m-d', $limitPreviewDate);
            $lastEventDate =  DateTime::createFromFormat('Y-m-d', $lastEventDate);
            
            if($lastEventDate->getTimestamp() >= $lpdDate->getTimestamp())
            {
                $displayNext = false;
            }
        }

        $attendanceList = $this->buildPlayerAttendanceList($eventPagger->getEvents());
        $teamPlayer = $teamManager->findTeamPlayerByPlayerId($team, ServiceLayer::getService('security')->getIdentity()->getUser()->getId());

        $statRoute = $sportManager->getTeamSportAddStatRoute($team) ;
        $eventDates = array();
        $eventPaggerEvents = $eventPagger->getEvents();
        $matchLimitDates = array();
        foreach($eventPagger->getEvents()  as $event)
        {
            $event->setStatRoute($statRoute);
            $matchLimitDates[ $event->getCurrentDate()->getTimestamp()] =  $event->getCurrentDate();
            $eventDates[$event->getId()][] = $event->getCurrentDate()->format('Y-m-d');
        }
        $repo = ServiceLayer::getService('TeamMatchLineupRepository');
        
        
        if(!empty($eventDates))
        {
            $lineups = $repo->findEventsLineups($eventDates);
            if(!empty($matchLimitDates))
            {
                ksort($matchLimitDates);
                $resultStart =   current($matchLimitDates);
                $resultEnd =   end($matchLimitDates);
                $matchResults = $statManager->getTeamMatchResults($team,$resultStart,$resultEnd); 
            }
            
            //t_dump($matchResults);

            foreach($lineups as $lineup)
            {
                $index = $lineup->getEventDate()->format('Ymd').'-'.$lineup->getEventId();
                if(array_key_exists($index, $eventPaggerEvents))
                {
                    $eventPagger->getEvent($index)->setLineup($lineup);
                }

                if(array_key_exists($index, $matchResults))
                {
                   if(null !=  $eventPagger->getEvent($index))
                   {
                       //find opponent team
                       $matchOverview = $statManager->getMatchOverview($lineup);
                       //t_dump($matchOverview);

                      $matchResults[$index]['first_line'] = ($matchOverview->getFirstLineGoals() == null) ? '0' : $matchOverview->getFirstLineGoals();
                      $matchResults[$index]['second_line'] = ($matchOverview->getSecondLineGoals() == null) ? '0' : $matchOverview->getSecondLineGoals();

                      $eventPagger->getEvent($index)->setMatchResult($matchResults[$index]['first_line'].':'.$matchResults[$index]['second_line']);
                   }
                    
                }
            }
        }
        
        ServiceLayer::getService('StepsManager')->finishCycle2($team->getId());

        
        return $this->render('Webteamer\Team\TeamModule:events:list.php', array(
                    'calendar_date' => $calendar_date,
                    'lastEvent' => $lastEvent,
                    'team' => $team,
                    'userTeamPermission' => ServiceLayer::getService('security')->getIdentity()->getTeamPermissions($team),
                    'attendanceList' => $attendanceList,
                    'teamEventManager' => $teamEventManager,
                    'teamPlayer' => $teamPlayer,
                    'eventPagger' => $eventPagger,
                    'lastEventScore' => $lastEventScore,
                    'listType' => $listType,
                    'limitPreviewDate' =>$limitPreviewDate,
                    'displayPreview' => $displayPreview,
                    'displayNext' => $displayNext,
                    'listType' => $listType,
                    'finishedProgressStep' => $finishedProgressStep
        ));
    }
    
    /*
    public function pastListAction()
    {
        $request = ServiceLayer::getService('request');
        $teamManager = ServiceLayer::getService('TeamManager');
        $teamEventManager = ServiceLayer::getService('TeamEventManager');
        $team = $teamManager->findTeamById($this->getRequest()->get('team_id'));
        ServiceLayer::getService('security')->getIdentity()->checkTeamAccess($team);
        $currentDate = new \DateTime();
        $fromDate = new \DateTime();
        $fromDate->setTimestamp(strtotime("-30 day", $currentDate->getTimestamp()));



        $events = $teamEventManager->findEvents($team, array('from' => $fromDate, 'to' => $currentDate));
        $eventsList = $teamEventManager->buildTeamEventList($events, $fromDate, $currentDate);
        $attendanceList = $this->buildPlayerAttendanceList($eventsList);
        return $this->render('Webteamer\Team\TeamModule:events:past_list.php', array(
                    'calendar_date' => $currentDate->format('Y-m'),
                    'eventsList' => $eventsList,
                    'team' => $team,
                    'attendanceList' => $attendanceList
        ));
    }
    */
    public function icalSingleEventAction()
    {
        $request = ServiceLayer::getService('request');
        $teamEventManager = ServiceLayer::getService('TeamEventManager');
        $teamManager = ServiceLayer::getService('TeamManager');
        $event = $teamEventManager->findEventById($request->get('id'));
        $team = $teamManager->findTeamById($event->getteamId());
        $layout = ServiceLayer::getService('layout');
        $layout->setTemplate(null);
        
        $eventDate = \DateTime::createFromFormat('Y-m-d H:i:s', urldecode($request->get('datetime')));
        $eventDate->setTimezone(new \DateTimeZone("UTC"));

        $response = $this->render('Webteamer\Team\TeamModule:events:ical_single.php', [
            'events' => [$event],
            'team' => $team,
            'dateTime' => new DateTimeEx($eventDate)
        ]);
        header('Content-Disposition: inline; filename=calendar.ics');
        $response->setType('text/calendar');
        return $response;
    }

    public function iCalListAction()
    {
        $request = ServiceLayer::getService('request');
        $teamManager = ServiceLayer::getService('TeamManager');
        $teamEventManager = ServiceLayer::getService('TeamEventManager');
        $team = $teamManager->findTeamById($this->getRequest()->get('team_id'));
        $events = $teamEventManager->findEvents($team);
        $layout = ServiceLayer::getService('layout');
        $layout->setTemplate(null);
        $response = $this->render('Webteamer\Team\TeamModule:events:ical.php', array(
            'events' => $events,
            'team' => $team
        ));
        header('Content-Disposition: inline; filename=calendar.ics');
        $response->setType('text/calendar');
        return $response;
    }
    
    public function getDetailPart()
    {
        
    }
    
     /**
     * Append info to event about season, sport, attendance, match result
     */
    private function appendEventDetailInfo($event,$team,$existLineup)
    {
        $request = ServiceLayer::getService('request');
        $eventManager = ServiceLayer::getService('TeamEventManager');
        $sportManager = ServiceLayer::getService('SportManager');
        $statManager = ServiceLayer::getService('PlayerStatManager');
        $playgroundManager = ServiceLayer::getService('PlaygroundManager');
        $localityManager = ServiceLayer::getService('LocalityManager');
        $sportEnum = $sportManager->getSportFormChoices();
        $sportName =  $this->getTranslator()->translate($sportEnum[$team->getSportId()]);
        $event->setSportName($sportName);

        $eventTypeEnum = $eventManager->getEventTypeEnum();
        $event->setTypeName($eventTypeEnum[$event->getEventType()]);

        $seasonManager = ServiceLayer::getService('TeamSeasonManager');
        $eventSeason = $seasonManager->getSeasonById($event->getSeason());
        $seasonName = $eventSeason->getName() . ' ' . $this->getLocalizer()->formatDate($eventSeason->getStartDate()) . ' - ' . $this->getLocalizer()->formatDate($eventSeason->getEndDate());
        $event->setSeasonName($seasonName);
        
        $event->setStatRoute($sportManager->getTeamSportAddStatRoute($team)) ;

        $attendance = $eventManager->getEventAttendance($event);
        $event->setAttendance($attendance);
        
        if(null != $existLineup)
        {
            $matchOverview = $statManager->getMatchOverview($existLineup);
            $event->setMatchResult($matchOverview->getResult());
            $event->setMatchOverview($matchOverview);
            
            if($existLineup->getStatus() == 'closed')
            {
                 $event->setClosed(true);
            }
        }
        
        if(null != $event->getPlaygroundDataAsArray())
        {
             $playground = $playgroundManager->createObjectFromArray($event->getPlaygroundDataAsArray());
             $event->setPlayground($playground);
        }
        elseif(null != $event->getPlaygroundId())
        {
            $playground = \Core\ServiceLayer::getService('PlaygroundRepository')->find($event->getPlaygroundId());
            $locality = $localityManager->getLocalityById($playground->getLocalityId());
            $playground->setLocality($locality);
            $event->setPlayground($playground);
        }
       
        
        
        
        


        return  $event;
    }
    
    public function tournamentDetail($event,$lineup)
    {
        $teamManager = ServiceLayer::getService('TeamManager'); 
         $eventManager = ServiceLayer::getService('TeamEventManager');
        $security = ServiceLayer::getService('security');
        $userIdentity = $security->getIdentity();
        $user = $userIdentity->getUser();
        $homeTeam = $teamManager->findTeamById($event->getTeamId());
        $opponentTeam = $teamManager->findTeamById($event->getOpponentTeamId());
        $enableHomeTeamLineup = false;
        $enableOpponentTeamLineup = false;
       
        $userTeams = $userIdentity->getUserTeams();
        $userTeam = null;
        foreach($userTeams as $userTeamTmp)
        {
            if($userTeamTmp->getId() == $homeTeam->getId())
            {
                 $enableHomeTeamLineup = true;
                 $userTeam = $homeTeam;
            }
            if($userTeamTmp->getId() == $opponentTeam->getId())
            {
                 $enableOpponentTeamLineup = true;
                 if(null == $userTeam)
                 {
                     $userTeam = $opponentTeam;
                 }
            }
        }
        
        $homeTeamPlayers = $teamManager->getActiveTeamPlayers($homeTeam->getId());
        $opponentTeamPlayers = $teamManager->getActiveTeamPlayers($opponentTeam->getId());
        
        $lineupPlayers = $eventManager->getEventLineupPlayers($lineup);
        $lineupPlayersIds = array();
        foreach($lineupPlayers as $lineupPlayer)
        {
            $lineupPlayersIds[$lineupPlayer->getTeamPlayerId()] = $lineupPlayer->getId();
        }
        

        return $this->render('Webteamer\Team\TeamModule:events:tournament_event_detail.php', array(
                    'event' => $event,
                    'homeTeam' => $homeTeam,
                    'opponentTeam' => $opponentTeam,
                    'homeTeamPlayers' => $homeTeamPlayers,
                    'opponentTeamPlayers' => $opponentTeamPlayers,
                    'team' => $userTeam,
                    'user' => $user,
                    'event' => $event,
                    'enableHomeTeamLineup' => $enableHomeTeamLineup,
                    'enableOpponentTeamLineup' => $enableOpponentTeamLineup,
                    'lineup' => $lineup,
                    'lineupPlayersIds' => $lineupPlayersIds
        ));
    }

    public function detailAction()
    {
        $request = ServiceLayer::getService('request');
        $teamManager = ServiceLayer::getService('TeamManager');
        $eventManager = ServiceLayer::getService('EventManager');
        $teamEventManager = ServiceLayer::getService('TeamEventManager');
        $teamMatchManager = ServiceLayer::getService('TeamMatchManager');
        $event = $teamEventManager->findEventById($request->get('id'));
        
        
        $playgroundManager = ServiceLayer::getService('PlaygroundManager');
        $statManager = ServiceLayer::getService('PlayerStatManager');

        $eventCurrentDate = $request->get('current_date');
        $event->setCurrentDate(new \DateTime($eventCurrentDate));

        
        $attendance = $teamEventManager->getEventAttendance($event);
        $event->setAttendance($attendance);
        $existLineup = $teamEventManager->getEventLineup($event);
        $team = $teamManager->findTeamById($event->getTeamId());
        $this->appendEventDetailInfo($event,$team,$existLineup);

        if($event->getEventType() == 'tournament_game')
        {
            if(null == $existLineup)
            {
                 //$lineupId = $teamMatchManager->createLineup($event,$team,$request->get('type'));
                 $lineupId = $teamEventManager->createRandomEventLineUp($event, array());
                 $existLineup = $teamEventManager->getLineupById($lineupId);
                 $existLineup->setOpponentTeamId($event->getOpponentTeamId());
                 $teamEventManager->saveLineup($existLineup);
                 
            }
            return $this->tournamentDetail($event,$existLineup);
        }
        
       
        ServiceLayer::getService('security')->getIdentity()->checkTeamAccess($team);
        $security = ServiceLayer::getService('security');
        $user = $security->getIdentity()->getUser();
       
        $eventTimeline = $statManager->getEventTimeline($event);
         if(null != $existLineup)
        {
            $matchOverview = $statManager->getMatchOverview($existLineup);
            $event->setMatchResult($matchOverview->getResult());
        }


        $teamMembers = $teamManager->getTeamPlayers($team->getId());
        $teamMembersList = array('accept' => array(), 'denny' => array(), 'unknown' => array());
        $acceptedPlayers = array();
        $acceptedGuestPlayers = array();
        $denyPlayers = array();
        $unknownPlayers = array();
        $currentMember = null;
        $atttendanceSummary = array('accept_players' => 0, 'accept_guest' => 0, 'denny' => 0, 'unknown' => 0);
        foreach ($teamMembers as $teamMember)
        {
            if($teamMember->getStatus() != 'unactive')
            {
                foreach ($attendance as $eventAttendance)
                {
                    if ($eventAttendance->getTeamPlayerId() == $teamMember->getId())
                    {
                        $teamMember->addEventAttendance($event->getUid(), $eventAttendance);
                    }
                }

                if ($teamMember->getPlayerId() == $user->getId())
                {
                    $currentMember = $teamMember;
                }

                if ($teamMember->getEventAttendanceStatus($event->getUid()) == '1')
                {

                    if ('GUEST' == $teamMember->getTeamRole())
                    {
                        $acceptedGuestPlayers[$teamMember->getEventAttendance($event->getUid())->getCreatedAt()->getTimestamp()][$teamMember->getId()] = $teamMember;
                        $atttendanceSummary['accept_guest'] ++;
                    }
                    else
                    {
                        $acceptedPlayers[$teamMember->getEventAttendance($event->getUid())->getCreatedAt()->getTimestamp()][$teamMember->getId()] = $teamMember;
                        $atttendanceSummary['accept_players'] ++;
                    }
                }
                elseif ($teamMember->getEventAttendanceStatus($event->getUid()) == '3')
                {
                    $denyPlayers[][$teamMember->getId()] = $teamMember;
                    $atttendanceSummary['denny'] ++;
                }
                else
                {
                    $unknownPlayers[][$teamMember->getId()] = $teamMember;
                    $atttendanceSummary['unknown'] ++;
                }
            }
        }

        ksort($acceptedPlayers);
        ksort($acceptedGuestPlayers);
        $teamMembersList['accept'][0] = $acceptedPlayers;
        $teamMembersList['accept'][1] = $acceptedGuestPlayers;
        $teamMembersList['denny'][0] = $denyPlayers;
        $teamMembersList['unknown'][0] = $unknownPlayers;
        
        $template = 'Webteamer\Team\TeamModule:events:detail.php';
        if(null != $existLineup && $existLineup->getStatus() == 'closed')
        {
            $event->setClosed(true);
            $template = 'Webteamer\Team\TeamModule:events:detail_closed.php';
        }
        
       
        
        
        $stepsManager = \Core\ServiceLayer::getService('StepsManager');
        $finishedProgressStep = $stepsManager->isStepFinished('event_attendance_create');
        $finishedProgressStepFinal = $stepsManager->isStepFinished('step2_final');


        return $this->render($template, array(
                    'event' => $event,
                    'team' => $team,
                    'attendance' => $attendance,
                    'playground' => $playground,
                    'user' => $user,
                    'existLineup' => $existLineup,
                    'eventTimeline' => $eventTimeline,
                    'teamMembersList' => $teamMembersList,
                    'currentMember' => $currentMember,
                    'atttendanceSummary' => $atttendanceSummary,
                    'finishedProgressStep' => $finishedProgressStep,
                    'finishedProgressStepFinal' =>  $finishedProgressStepFinal,
                    'user' => $user,
        ));
    }

    public function ratingAction()
    {
        $request = ServiceLayer::getService('request');
        $teamEventManager = ServiceLayer::getService('TeamEventManager');
        $teamManager = ServiceLayer::getService('TeamManager');
        $statManager = ServiceLayer::getService('PlayerStatManager');
        $event = $teamEventManager->findEventById($request->get('id'));
        $eventCurrentDate = $request->get('current_date');
        $event->setCurrentDate(new \DateTime($eventCurrentDate));
        $team = $teamManager->findTeamById($event->getTeamId());
       
        $existLineup = $teamEventManager->getEventLineup($event);
        $this->appendEventDetailInfo($event, $team,$existLineup);

        ServiceLayer::getService('security')->getIdentity()->checkTeamAccess($team);
        $security = ServiceLayer::getService('security');
        $user = $security->getIdentity()->getUser();

       
        $members = $teamManager->getTeamPlayers($team->getId());
        $teamManager = null;
        foreach($members as $member)
        {
            if($member->getTeamRole() == 'ADMIN')
            {
                $teamManager = $member;
            }
        }
        


        if(null ==$existLineup or $existLineup->getStatus() != 'closed')
        {
           
            return $this->render('Webteamer\Team\TeamModule:events:rating_unavailable.php', array(
                'event' => $event,
                'team' => $team,
                'user' => $user,
                'lineup' => $existLineup,
                'teamManager' => $teamManager,
            ));
        }
        
        $eventTimeline = $statManager->getEventTimeline($event);
        $lineupPlayers = $teamEventManager->getEventLineupPlayers($existLineup);
        $lineupRatings = $statManager->getLineupRatings($existLineup);
        $scoreMap = array();
        $approvedVoters = array();
       

        $membersCollection = array();
        foreach ($members as $member)
        {
            $membersCollection[$member->getId()] = $member;
        }

        foreach ($lineupPlayers as $lineupPlayer)
        {
            $approvedVoters[] = $lineupPlayer->getPlayerId();
            foreach ($lineupRatings as $lineupRating)
            {
                $scoreMap[$lineupRating->getPlayerId()]['voters'][$lineupRating->getAuthorId()] = $lineupRating->getScore();
                if ($lineupRating->getTeamPlayerId() == $lineupPlayer->getTeamPlayerId())
                {
                    $lineupPlayer->addScore($lineupRating->getAuthorId(), $lineupRating);
                }
            }
        }

        $firstLine = array();
        $secondLine = array();
        $players = array();
        foreach ($lineupPlayers as $lineupPlayer)
        {
            if (true or array_key_exists($lineupPlayer->getTeamPlayerId(), $membersCollection))
            {
                $lineupPlayer->setTeamPlayer($membersCollection[$lineupPlayer->getTeamPlayerId()]);
                $players[$lineupPlayer->getId()] = $lineupPlayer;
            }



            if ($lineupPlayer->getLineupPosition() == 'first_line')
            {
                $firstLine[] = $lineupPlayer;
            }
            if ($lineupPlayer->getLineupPosition() == 'second_line')
            {
                $secondLine[] = $lineupPlayer;
            }
        }
        
        
       

        $groupedEventTimeline = $statManager->getGroupedEventTimeline($event);
        $matchOverview = $statManager->getMatchOverview($existLineup);
        $resultClass = array('win' => 'success', 'loose' => 'danger', 'draw' => 'info');
        $matchStats = $statManager->getMatchPlayersStat($existLineup);

        $matchResult = array('first_line' => 0, 'second_line' => 0);
        if(null !=$matchOverview)
        {
            $matchResult['first_line'] = ($matchOverview->getFirstLineGoals() == null) ? 0 : $matchOverview->getFirstLineGoals() ;
            $matchResult['second_line'] = ($matchOverview->getSecondLineGoals() == null) ? 0 : $matchOverview->getSecondLineGoals() ;
            $matchResult['winner'] = 'none';
            
            if($matchResult['first_line']  > $matchResult['second_line'])
            {
                 $matchResult['winner'] = 'first_line' ;
            }
            
            if($matchResult['first_line']  < $matchResult['second_line'])
            {
                 $matchResult['winner'] = 'second_line' ;
            }
            
        }
        
        $userIdentity = ServiceLayer::getService('security')->getIdentity();
        $userTeamRoles = $userIdentity->getTeamRoles();
        
        $userTeamRole = $userTeamRoles[$team->getId()]['role'];

        return $this->render('Webteamer\Team\TeamModule:events:rating.php', array(
                    'event' => $event,
                    'team' => $team,
                    'user' => $user,
                    'firstLine' => $firstLine,
                    'secondLine' => $secondLine,
                    'lineup' => $existLineup,
                    'scoreMap' => $scoreMap,
                    'matchOverview' => $matchOverview,
                    'resultClass' => $resultClass,
                    'eventTimeline' => $eventTimeline,
                    'approvedVoters' => $approvedVoters,
                    'groupedEventTimeline' => $groupedEventTimeline,
                    'players' => $players,
                    'matchStats' => $matchStats,
            'matchResult' => $matchResult,
            'userTeamRole' => $userTeamRole
        ));
    }

    public function pastDetailAction()
    {
        $request = ServiceLayer::getService('request');
        $teamManager = ServiceLayer::getService('TeamManager');
        $eventManager = ServiceLayer::getService('EventManager');
        $teamEventManager = ServiceLayer::getService('TeamEventManager');
        $event = $teamEventManager->findEventById($request->get('id'));
        $localityManager = ServiceLayer::getService('LocalityManager');
        $playerStatManager = ServiceLayer::getService('PlayerStatManager');





        $eventCurrentDate = $request->get('current_date');
        $event->setCurrentDate(new \DateTime($eventCurrentDate));
        $playground = $event->getPlayground();
        $locality = $localityManager->getLocalityById($playground->getLocalityId());
        $team = $teamManager->findTeamById($event->getTeamId());
        $security = ServiceLayer::getService('security');
        $user = $security->getIdentity()->getUser();


        $eventsList = $teamEventManager->getTeamUpcomingEvents($team);

        $existLineup = $teamEventManager->getEventLineup($event);
        $lineupPlayers = $teamEventManager->getEventLineupPlayers($existLineup);
        $lineupRatings = $playerStatManager->getLineupRatings($existLineup);
        $scoreMap = array();

        foreach ($lineupPlayers as $lineupPlayer)
        {
            foreach ($lineupRatings as $lineupRating)
            {
                $scoreMap[$lineupRating->getPlayerId()]['voters'][$lineupRating->getAuthorId()] = $lineupRating->getScore();
                //$lineupPlayer->addScore($lineupRating->getPlayerId(),$lineupRating);


                if ($lineupRating->getPlayerId() == $lineupPlayer->getPlayerId())
                {
                    $lineupPlayer->addScore($lineupRating->getAuthorId(), $lineupRating);
                }
            }
        }



        $firstLine = array();
        $secondLine = array();
        foreach ($lineupPlayers as $lineupPlayer)
        {
            if ($lineupPlayer->getLineupPosition() == 'first_line')
            {
                $firstLine[] = $lineupPlayer;
            }
            if ($lineupPlayer->getLineupPosition() == 'second_line')
            {
                $secondLine[] = $lineupPlayer;
            }
        }






        return $this->render('Webteamer\Team\TeamModule:events:past_detail.php', array(
                    'event' => $event,
                    'team' => $team,
                    'eventsList' => $eventsList,
                    'locality' => $locality,
                    'user' => $user,
                    'firstLine' => $firstLine,
                    'secondLine' => $secondLine,
                    'lineup' => $existLineup,
                    'scoreMap' => $scoreMap
        ));
    }

    private function buildPlayerAttendanceList($eventsList)
    {

        $teamEventManager = ServiceLayer::getService('TeamEventManager');
        $attendanceEvents = array();
        foreach ($eventsList as $dayEvent)
        {
            $attendanceEvents[] = $dayEvent;
            /*
              foreach($dayEvents as $dayEvent)
              {
              $attendanceEvents[] = $dayEvent;
              }
             * 
             */
        }
        $security = ServiceLayer::getService('security');
        $user = $security->getIdentity()->getUser();
        $player = ServiceLayer::getService('PlayerRepository')->find($user->getId());

        $playerAttendance = $teamEventManager->getPlayersAttendance(array($player));
        $attendanceList = array();
        foreach ($playerAttendance as $attendance)
        {
            $attendanceList[$attendance->getEventId()][$attendance->getEventDate()->format('Y-m-d')] = $attendance->getStatus();
        }
        return $attendanceList;
    }

    public function pastListAction()
    {
        $request = ServiceLayer::getService('request');
        $teamManager = ServiceLayer::getService('TeamManager');
        $teamEventManager = ServiceLayer::getService('TeamEventManager');
        $team = $teamManager->findTeamById($this->getRequest()->get('team_id'));
        ServiceLayer::getService('security')->getIdentity()->checkTeamAccess($team);
        $currentDate = new \DateTime();
        $fromDate = new \DateTime();
        $fromDate->setTimestamp(strtotime("-30 day", $currentDate->getTimestamp()));



        $events = $teamEventManager->findEvents($team, array('from' => $fromDate, 'to' => $currentDate));
        $eventsList = $teamEventManager->buildTeamEventList($events, $fromDate, $currentDate);
        $attendanceList = $this->buildPlayerAttendanceList($eventsList);
        return $this->render('Webteamer\Team\TeamModule:events:past_list.php', array(
                    'calendar_date' => $currentDate->format('Y-m'),
                    'eventsList' => $eventsList,
                    'team' => $team,
                    'attendanceList' => $attendanceList
        ));
    }
    
    public function changeCostsAction()
    {
        $request = ServiceLayer::getService('request');
        $security = ServiceLayer::getService('security');
        $eventManager = ServiceLayer::getService('TeamEventManager');
        
        $eventId = $request->get('event_id');
        $eventDate = $request->get('event_date');
        $costs = $request->get('costs');
        
        $eventManager->updateEventCosts($eventId, $eventDate, $costs);
        
        $layout = ServiceLayer::getService('layout');
        $layout->setTemplate(null);
        $response = new ControllerResponse();
        $response->setStatus('success');

        $result = array();
        $result['result'] = 'create_success';
        $response->setContent(json_encode($result));
        $response->setType('application/json');
        return $response;
    }
    
    public function changePlayerFeeAction()
    {
        $request = ServiceLayer::getService('request');
        $security = ServiceLayer::getService('security');
        $eventManager = ServiceLayer::getService('TeamEventManager');
        
        $eventId = $request->get('event_id');
        $eventDate = $request->get('event_date');
        $playerId = $request->get('player_id');
        $teamPlayerId = $request->get('team_player_id');
        $fee = $request->get('fee');
        
        $eventManager->updateEventAttendancePlayerFee($teamPlayerId, $eventId, $eventDate, $fee);
        
        $layout = ServiceLayer::getService('layout');
        $layout->setTemplate(null);
        $response = new ControllerResponse();
        $response->setStatus('success');

        $result = array();
        $result['result'] = 'create_success';
        $response->setContent(json_encode($result));
        $response->setType('application/json');
        return $response;
    }

    public function changePlayerAttendanceAction()
    {

        $request = ServiceLayer::getService('request');
        $security = ServiceLayer::getService('security');
        $eventManager = ServiceLayer::getService('TeamEventManager');
        $teamManager = ServiceLayer::getService('TeamManager');

        $eventId = $request->get('event_id');
        $eventDate = $request->get('event_date');
        $type = $request->get('type');
        $event = ServiceLayer::getService('EventRepository')->find($eventId);
        $event->setCurrentDate(new \DateTime($eventDate));
        $team = $teamManager->findTeamById($event->getTeamId());
        $playerId = $request->get('player_id');
        $teamPlayerId = $request->get('team_player_id');
        if (null != $playerId)
        {
            try
            {
                $this->checkManageTeamPermission($team);
            }
            catch (AclException $e)
            {
                $user = $security->getIdentity()->getUser();
                $playerId = $user->getId();
            }
        }

        if (null == $playerId)
        {
            $user = $security->getIdentity()->getUser();
            $playerId = $user->getId();
        }

        //find team player by id and team
        if (null == $teamPlayerId)
        {
            $teamPlayer = $teamManager->findTeamPlayerByPlayerId($team, $playerId);
        }
        else
        {
            $teamPlayer = $teamManager->findTeamPlayerById($teamPlayerId);
        }
        //$player = ServiceLayer::getService('PlayerRepository')->find($playerId);
        
        //$pushManager = ServiceLayer::getService('PushManager');
        $notificationManager = ServiceLayer::getService('SystemNotificationManager');
        $oldAttendence = $eventManager->getTeamPlayerEventAttendance($teamPlayer, $event, $eventDate);
        $eventManager->deleteEventAttendance($teamPlayer, $event, $eventDate);

        $teamPlayers = $teamManager->getTeamPlayers($team->getId());
        
        if ('in' == $type)
        {
            $eventManager->createEventAttendance($teamPlayer, $event, $eventDate, 1);
            //$pushManager->sendAttendanceNotify($teamPlayer,$teamPlayers,$event,'in');
            
            $notificationManager->createPushNotify('event_attendance_in',array(
                'teamPlayer' => $teamPlayer,
                'teamPlayers' => $teamPlayers,
                'senderId' => $teamPlayer->getPlayerId(),
                'event' => $event,
                'team' => $team));
        }
        elseif ('na' == $type)
        {

            $eventManager->createEventAttendance($teamPlayer, $event, $eventDate, 2);
        }
        elseif ('out' == $type)
        {
            $eventManager->createEventAttendance($teamPlayer, $event, $eventDate, 3);
            //$pushManager->sendAttendanceNotify($teamPlayer,$teamPlayers,$event,'out');
            $notificationManager->createPushNotify('event_attendance_out',array(
                'teamPlayer' => $teamPlayer,
                'teamPlayers' => $teamPlayers,
                'senderId' => $teamPlayer->getPlayerId(),
                'event' => $event,
                'team' => $team));
            //change by admin
            if (null != $playerId && $teamPlayer->getPlayerId() > 0)
            {
                $user = ServiceLayer::getService('user_repository')->find($teamPlayer->getPlayerId());
                $loggedUser = $security->getIdentity()->getUser();



                if (null != $user && $user->getId() && $loggedUser->getId() != $teamPlayer->getPlayerId())
                {
                    $notifyData['email'] = $user->getEmail();
                    $notifyData['lang'] = $user->getDefaultLang();
                    $notifyData['event'] = $event;

                    $notificationManger = ServiceLayer::getService('notification_manager');
                    $notificationManger->sendCancelPlayerAttendance($notifyData);
                }
            }
        }
        
        //update fee
        if(null != $oldAttendence)
        {
            $eventManager->updateEventAttendancePlayerFee($teamPlayer->getId(), $event->getId(), $eventDate, $oldAttendence->getFee());
        }
        
       
         
       

        //get current attendance
        $attendance = $eventManager->getEventAttendance($event);
        $event->setAttendance($attendance);
        $attendanceInfo = array();
        $attendanceInfo['uid'] = $event->getUid();
        $attendanceInfo['in'] = $event->getAttendanceInSum();
        $attendanceInfo['progress'] = round($event->getAttendanceInSum() / $event->getCapacity() * 100);
        
         //system notify
        if($attendanceInfo['in'] == $event->getCapacity())
        {
            $notificationManager->createPushNotify('event_capacity',array('team' => $team,'event' => $event,'senderId' => $teamPlayer->getPlayerId()));
        }
        
        
         //finish alert
        if($attendanceInfo['in']  > 1)
        {
             ServiceLayer::getService('StepsManager')->finishCreateAttendance($team);
             $emailMarketingManager = \Core\ServiceLayer::getService('EmailMarketingManager');
             
             $marketingTargetUser = ServiceLayer::getService('user_repository')->find($event->getAuthorId());
             $emailMarketingManager->setAfterAttendanceConfirmSetup($marketingTargetUser);

        }
       


        $layout = ServiceLayer::getService('layout');
        $layout->setTemplate(null);
        $response = new ControllerResponse();
        $response->setStatus('success');

        $result = array();
        $result['result'] = 'create_success';
        $result['attendanceInfo'] = $attendanceInfo;
        $response->setContent(json_encode($result));
        $response->setType('application/json');
        return $response;
    }

    public function setRepeatDays($entity)
    {
        $entity->setMonday(0);
        $entity->setTuesday(0);
        $entity->setWednesday(0);
        $entity->setThursday(0);
        $entity->setFriday(0);
        $entity->setSaturday(0);
        $entity->setSunday(0);
        foreach ($entity->getRepeatDays() as $repeat_day)
        {
            if ($repeat_day == 1)
            {
                $entity->setMonday(1);
            }

            if ($repeat_day == 2)
            {
                $entity->setTuesday(1);
            }

            if ($repeat_day == 3)
            {
                $entity->setWednesday(1);
            }

            if ($repeat_day == 4)
            {
                $entity->setThursday(1);
            }

            if ($repeat_day == 5)
            {
                $entity->setFriday(1);
            }

            if ($repeat_day == 6)
            {
                $entity->setSaturday(1);
            }

            if ($repeat_day == 7)
            {
                $entity->setSunday(1);
            }
        }
    }

    public function notifyAction()
    {
        $request = $this->getRequest();
        $translator = $this->getTranslator();
        $teamManager = ServiceLayer::getService('TeamManager');
        $teamEventManager = ServiceLayer::getService('TeamEventManager');
        $event = $teamEventManager->findEventById($request->get('event_id'));
        $event->setCurrentDate(new \DateTime($request->get('event_date')));
        $message = $request->get('message');

        $teamPlayerManager = ServiceLayer::getService('TeamPlayerManager');
        $recipientIds = $request->get('recipients');
        $recipients = array();
        $unvalidRecipients = array();
        $validator = new \Core\Validator();
        
        foreach ($recipientIds as $recipientId)
        {
           $teamPlayer = $teamPlayerManager->getTeamPlayerById($recipientId);
            $validEmail = $validator->validateEmail($teamPlayer->getEmail());
           
           if($validEmail == false)
           {
               $unvalidRecipients[] = $teamPlayer->getFullName() .'('.$translator->translate('Unvalid email').')' ;
           }
        else
        {
            
            if('confirmed' == $teamPlayer->getStatus())
            {
                  $recipients[] = $teamPlayer;
            }
            elseif('waiting-to-confirm')
            {
                $unvalidRecipients[] = $teamPlayer->getFullName().'('.$translator->translate('Waiting to confirm').')' ;
            }
        }
            
           
        }
        $resultMmessage = $translator->translate('Send success').'<br />';
        
        if(!empty($unvalidRecipients))
        {
            $resultMmessage .= $translator->translate('Those members was excluded from recipients: ').implode($unvalidRecipients,', ');
        }
        
        
        
      
        $senderStore = new CustomImmediatelyEventInvitationStore($event, $recipients);
        $sender = new EventInvitationNotificationSender($senderStore);
        $sender->setCustomText($message);
        $sender->sendAll();

        //create system message
/*
        $messageManager = ServiceLayer::getService('SystemNotificationManager');
        $eventAttendance = $teamEventManager->getEventAttendance($event);
        $eventAttendanceMatrix = array();
        foreach ($eventAttendance as $att)
        {
            if ($att->getStatus() == 1)
            {
                $status = 'in';
            }

            if ($att->getStatus() == 3)
            {
                $status = 'out';
            }

            $eventAttendanceMatrix[$att->getTeamPlayerId()] = $status;
        }

        foreach ($senderStore->getData() as $senderData)
        {
            $systemReg = $sender->getSystemMessageRegions($senderData);
            $messageReg = $sender->getMessageRegions($senderData);
            $message['subject'] = $systemReg['subject'];
            $message['body'] = $messageReg['body'];
            $message['rest_body'] = $systemReg['body'];
            foreach ($recipients as $recipient)
            {
                if (null != $recipient->getPlayerId())
                {
                    $message['body_data'] = array(
                        'id' => $event->getId(),
                        'date' => $event->getCurrentDate()->getTimestamp(),
                        'attendance' => (array_key_exists($recipient->getId(), $eventAttendanceMatrix)) ? $eventAttendanceMatrix[$recipient->getId()] : 'unknown'
                    );
                    $messageRecipient = new \CR\Message\Model\MessageRecipient();
                    $messageRecipient->setId($recipient->getPlayerId());
                    $messageRecipient->setName($recipient->getFirstName());
                    $messageRecipient->setSurname($recipient->getLastName());
                    $messageRecipient->setEmail($recipient->getEmail());
                    $messageManager->createMessage($message, $messageRecipient);
                }
            }
        }
*/
        $layout = ServiceLayer::getService('layout');
        $layout->setTemplate(null);
        $response = new ControllerResponse();
        $response->setStatus('success');
        $response->setContent(json_encode(array('result' => 'send_success','unvalidRecipients' => $unvalidRecipients, 'message' => $resultMmessage)));
        $response->setType('json');
        return $response;
    }

    public function deleteAction()
    {
        $request = ServiceLayer::getService('request');
        $router = ServiceLayer::getService('router');
        $teamEventManager = ServiceLayer::getService('TeamEventManager');
        $entity = $teamEventManager->findEventById($request->get('event_id'));
        $type = $request->get('t');
        $teamManager = ServiceLayer::getService('TeamManager');
        $team = $teamManager->findTeamById($entity->getTeamId());
        $this->checkManageTeamPermission($team);
        $notificationManager = ServiceLayer::getService('SystemNotificationManager');
        if ('all' == $type)
        {
            $teamEventManager->deleteEvent($entity);
            if($entity->getPeriod() == 'none')
            {
                $notificationManager->createPushNotify('delete_single_event',array('team'=>$team,'event' => $entity));
            }
            else
            {
                $notificationManager->createPushNotify('delete_repeat_event',array('team'=>$team,'event' => $entity));
            }
        }
        elseif ('next' == $type)
        {
            $endDate = new \DateTime($request->get('current_date'));
            $entity->setEnd($endDate);
            $teamEventManager->saveEvent($entity);
             $notificationManager->createPushNotify('delete_next_repeat_event',array('team'=>$team,'event' => $entity));
        }
        elseif ('only' == $type)
        {
            $ommitedDate = new \DateTime($request->get('current_date'));
            $entity->setOmittedTermins($entity->getOmittedTermins() . ',' . serialize(new \DateTime($ommitedDate->format('Y-m-d'))));
            $teamEventManager->saveEvent($entity);
            $notificationManager->createPushNotify('ommit_repeat_event',array('team'=>$team,'event' => $entity));
        }
        elseif ('single-with-stat' == $type)
        {
            $statManager = ServiceLayer::getService('PlayerStatManager');
            $entity->setCurrentDate(new\DateTime($request->get('current_date')));

            if ($entity->getPeriod() == 'none')
            {
                $teamEventManager->deleteEvent($entity);
            }
            else
            {
                $ommitedDate = new \DateTime($request->get('current_date'));
                $entity->setOmittedTermins($entity->getOmittedTermins() . ',' . serialize(new \DateTime($ommitedDate->format('Y-m-d'))));
                $teamEventManager->saveEvent($entity);
            }

            $teamEventManager->deleteEventLineup($entity);
            $statManager->clearEventTimeline($entity);
             $notificationManager->createPushNotify('delete_stat_event',array('team'=>$team));
        }



        $request->redirect($router->link('team_event_list', array('team_id' => $entity->getTeamId())));
    }

    public function confirmCalendarDeleteAction()
    {
        $request = $this->getRequest();

        $teamEventManager = ServiceLayer::getService('TeamEventManager');
        $event = $teamEventManager->findEventById($request->get('event_id'));
        $eventCurrentDate = $request->get('event_date');
        $event->setCurrentDate(new \DateTime($eventCurrentDate));
        //event timelines
        $statManager = ServiceLayer::getService('PlayerStatManager');
        $eventTimelines = $statManager->getTimelineRepository()->findBy(array('event_id' => $request->get('event_id')));

        $timelineDates = array();
        foreach ($eventTimelines as $eventTimeline)
        {
            $timelineDates[$eventTimeline->getEventDate()->format('Y-m-d')] = $eventTimeline->getEventDate()->format('d.m.Y');
        }

        $layout = ServiceLayer::getService('layout');
        $layout->setTemplate(null);


        return $this->render('Webteamer\Team\TeamModule:events:confirm_calendar_delete.php', array(
                    'event' => $event,
                    'timelineDates' => $timelineDates,
        ));
    }

}

?>