<?php

namespace Webteamer\Team\Controller;

use Core\Controller as Controller;
use Core\ControllerResponse;
use Core\ServiceLayer;
use Core\Validator;
use DateTime;
use Webteamer\Locality\Form\PlaygroundForm;
use Webteamer\Team\Form\AddPlayerForm;
use Webteamer\Team\Form\PlayerSearchForm;
use Webteamer\Team\Form\SettingsForm;
use Webteamer\Team\Form\TeamValidator;
use Webteamer\Team\Model\TeamPlayer;

class PublicController extends Controller {


    public function homepageAction()
    {

        $request = ServiceLayer::getService('request');
        $teamManager = ServiceLayer::getService('TeamManager');
        $teamEventManager = ServiceLayer::getService('TeamEventManager');
        $playerStatManager = ServiceLayer::getService('PlayerStatManager');
        $playerManager = ServiceLayer::getService('PlayerManager');
        $repo = ServiceLayer::getService('TeamRepository');
        $sportManager = ServiceLayer::getService('SportManager');
        $team = $repo->find($request->get('id'));
        
        $teamInfo = $teamManager->getTeamsInfo(array($team));
        
        $sportEnum = $sportManager->getSportFormChoices();
       

        $eventsList = $teamEventManager->getTeamUpcomingEvents($team);
        $players =  $teamManager->getTeamPlayers($team->getId());
        foreach($players as $teamPlayer)
        {
            $player =  $playerManager-> findPlayerById($teamPlayer->getPlayerId());
            if(null != $player)
            {
                $playerSports = $playerManager->findPlayerSports($player);
                $player->setSports($playerSports);
                $rating = $playerStatManager->getPlayerRating($player);
                $playerStats = $playerStatManager->getTeamPlayerStat($teamPlayer);
                
                $teamPlayer->setRating($rating['average']);
                $teamPlayer->setPlayer($player);
                $teamPlayer->setStats($playerStats);
            }
           
        }
        
        $layout = ServiceLayer::getService('layout');
        $layout->setTemplate(GLOBAL_DIR.'/templates/PublicLayout.php');
        
        return $this->render('Webteamer\Team\TeamModule:public:simpleDetail.php', array(
                    'team' => $team,
                    'eventsList' => $eventsList,
                    'players' => $players,
              'teamInfo' => $teamInfo,
                    'sportEnum' => $sportEnum,
        ));
     
        
        
    }
    
   

}
