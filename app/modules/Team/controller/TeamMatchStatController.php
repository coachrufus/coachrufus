<?php

namespace Webteamer\Team\Controller;

use AclException;
use Core\Controller;
use Core\ControllerResponse;
use Core\ServiceLayer;
use Core\Form as Form;
use DateTime;
use Webteamer\Player\Model\PlayerTimelineEvent;
use Core\Notifications\AddRateNotificationSender;
use Core\Notifications\AddRateStore;
use Core\Notifications\MatchResultsStore;
use Core\Notifications\MatchResultsNotificationSender;
use Core\Types\DateTimeEx;

/**
 * @author Marek Hubáček
 * @version 1.0
 */
class TeamMatchStatController extends Controller {

    public function canManageEvents($team)
    {
        if( !ServiceLayer::getService('security')->getIdentity()->hasTeamPermission('manageEvents',$team))
        {
             throw  new AclException();
        }
    }
     /**
     * Append info to event about season, sport, attendance
     */
    private function appendEventDetailInfo($event, $team, $existLineup = null)
    {
        $request = ServiceLayer::getService('request');
        $eventManager = ServiceLayer::getService('TeamEventManager');
        $sportManager = ServiceLayer::getService('SportManager');
        $statManager = ServiceLayer::getService('PlayerStatManager');
        $playgroundManager = ServiceLayer::getService('PlaygroundManager');
        $localityManager = ServiceLayer::getService('LocalityManager');
        $sportEnum = $sportManager->getSportFormChoices();
        $sportName = $this->getTranslator()->translate($sportEnum[$team->getSportId()]);
        $event->setSportName($sportName);

        $eventTypeEnum = $eventManager->getEventTypeEnum();
        $event->setTypeName($eventTypeEnum[$event->getEventType()]);

        $seasonManager = ServiceLayer::getService('TeamSeasonManager');
        $eventSeason = $seasonManager->getSeasonById($event->getSeason());
        $seasonName = $eventSeason->getName() . ' ' . $this->getLocalizer()->formatDate($eventSeason->getStartDate()) . ' - ' . $this->getLocalizer()->formatDate($eventSeason->getEndDate());
        $event->setSeasonName($seasonName);

        $event->setStatRoute($sportManager->getTeamSportAddStatRoute($team));

        $attendance = $eventManager->getEventAttendance($event);
        $event->setAttendance($attendance);
        
        if(null != $existLineup)
        {
            $matchOverview = $statManager->getMatchOverview($existLineup);
            $event->setMatchResult($matchOverview->getResult());
            $event->setMatchOverview($matchOverview);
             
            if($existLineup->getStatus() == 'closed')
            {
                 $event->setClosed(true);
            }
        }
        
         if(null != $event->getPlaygroundDataAsArray())
        {
             $playground = $playgroundManager->createObjectFromArray($event->getPlaygroundDataAsArray());
             $event->setPlayground($playground);
        }
        elseif(null != $event->getPlaygroundId())
        {
            $playground = \Core\ServiceLayer::getService('PlaygroundRepository')->find($event->getPlaygroundId());
            $locality = $localityManager->getLocalityById($playground->getLocalityId());
            $playground->setLocality($locality);
            $event->setPlayground($playground);
        }


        return $event;
    }

    public function createVolleyballStatAction()
    {
        $request = ServiceLayer::getService('request');
        $translator = ServiceLayer::getService('translator');
        $repo = ServiceLayer::getService('TeamRepository');
        $teamManager = ServiceLayer::getService('TeamManager');
        $eventManager = ServiceLayer::getService('TeamEventManager');
       // $matchManager = ServiceLayer::getService('PlayerMatchManager');
        $statManager = ServiceLayer::getService('PlayerStatManager');
        $teamEvent = $eventManager->findEvent($request->get('event_id'));
        $teamEvent->setCurrentDate(new \DateTime($request->get('eventdate')));
        $team = $teamManager->findTeamById($teamEvent->getTeamId());
        $this->canManageEvents($team);
        
        
       $setMatch = new \Webteamer\Player\Model\SetMatch();
       $eventTimeline = $statManager->getEventTimeline($teamEvent);
       $setPoints = array();
       foreach($eventTimeline as $timelineItem)
       {
           //total points
           if(0 == $timelineItem->getLineupPlayerId() )
           {
                $setPoints[$timelineItem->getSetPosition()]['total_points'][$timelineItem->getLineupPosition()] = $timelineItem->getPoints();
           }
           else 
           {
                $setPoints[$timelineItem->getSetPosition()]['points'][$timelineItem->getLineupPlayerId()] = $timelineItem->getPoints();
           }
          
           $setPoints[$timelineItem->getSetPosition()]['duration'] = $timelineItem->getDurationTime();
       }
       
       foreach($setPoints as $setPosition => $setData)
       {
           $setMatch->addSet($setPosition, $setData);
       }
       
       
      

        if(null != $request->get('lid'))
        {
             $existLineup = $eventManager->getLineupById($request->get('lid'));
        }
        else
        {
             $existLineup = $eventManager->getEventLineup($teamEvent);
        }
        
        $lineupPlayersList = $eventManager->getEventLineupPlayers($existLineup);
        
        
        $lineupPlayers = array();
        foreach($lineupPlayersList as $lineupPlayer)
        {
             $lineupPlayers[$lineupPlayer->getLineupPosition()][$lineupPlayer->getId()] =  $lineupPlayer;
        }
        $this->appendEventDetailInfo($teamEvent, $team,$existLineup);
        if('POST' == $request->getMethod())
        {
            $data =  $request->get('set');
            
            
            //clear old 
            $statManager->clearEventTimeline($teamEvent);
            
            //regroup linuep players
            $flatLineupPlayers = array();
            foreach($lineupPlayersList as $lineupPlayer)
            {
                 $flatLineupPlayers[$lineupPlayer->getId()] =  $lineupPlayer;
            }
            
          //t_dump($data);exit;
           foreach ($data as $setId => $setData)
            {
                $setPoints = array_sum($setData['points']['first_line']) + array_sum($setData['points']['second_line']);
                if ($setPoints > 0)
                {
                    foreach ($setData['points'] as $lineupPosition => $lineupPoints)
                    {
                        foreach ($lineupPoints as $lineupPlayerId => $points)
                        {
                            $event = new PlayerTimelineEvent();
                            $event->setEventId($request->get('event_id'));
                            $event->setHitType('volleybal_set');
                            if (0 != $lineupPlayerId && $flatLineupPlayers[$lineupPlayerId]->getPlayerId() > 0)
                            {
                                $event->setUserId($flatLineupPlayers[$lineupPlayerId]->getPlayerId());
                            }

                            $event->setLineupPlayerId($lineupPlayerId);
                            $event->setLineupPosition($lineupPosition);
                            $event->setEventDate(new \DateTime($request->get('eventdate')));
                            $event->setLineupId($request->get('lid'));
                            $event->setTeamId($teamEvent->getTeamId());
                            $event->setPoints($points);
                            $event->setDurationTime($setData['duration']);
                            $event->setSetPosition($setId);
                            $statManager->saveTimelineEvent($event);
                        }
                    }
                }
            }
            
            $existLineup->setStatus('closed');
            $eventManager->saveLineup($existLineup);
                
            $request->addFlashMessage('team_volleyball_stat_success', $translator->translate('TENNIS_VOLLEYBALL_SUCCESS'));
            $request->redirect();
        }

        return $this->render('Webteamer\Team\TeamModule:teamMatchStat:create_volleyball_stat.php', array(
               'lineupPlayers' => $lineupPlayers,
                'team' => $team,
                'event' => $teamEvent,
                'eventTimeline' => $eventTimeline,
                'lineup' => $existLineup,
                'setMatch' => $setMatch,
                'existLineup' => $existLineup
            ));
    }
    
    public function volleyballStatAction()
    {
        $request = ServiceLayer::getService('request');
        $teamManager = ServiceLayer::getService('TeamManager');
        $statManager = ServiceLayer::getService('PlayerStatSetManager');
        $seasonManager = ServiceLayer::getService('TeamSeasonManager');
        $team = $teamManager->findTeamById($request->get('team_id'));
        ServiceLayer::getService('security')->getIdentity()->checkTeamAccess($team);
        
        
        $actualSeason = $seasonManager->getTeamCurrentSeason($team);
        $seasonId =$actualSeason->getId();
        $seasonChoices = $seasonManager->getEventFormChoices($team);
       
        if(null != $request->get('filter'))
        {
            $filterData = $request->get('filter');
            $seasonId = $filterData['season_id'];
        }
        
        $currentSeason = $seasonManager->getSeasonById($seasonId);
        $statFrom = new DateTimeEx($currentSeason->getStartDate());
        $statTo = new DateTimeEx($currentSeason->getEndDate());
        $teamPlayersStats = $statManager->getAllTeamPlayersVolleyballStat($team, $statFrom, $statTo);

        $filterForm = new Form();
        $filterForm->setName('filter');
        $filterForm->setField('season_id', array(
            'type' => 'choice',
            'choices' => $seasonChoices,
            'required' => true,
        ));
        $filterForm->setFieldValue('season_id',$seasonId);

        //var_dump($teamPlayersStats);exit;

        return $this->render('Webteamer\Team\TeamModule:teamMatchStat:volleyball_stat.php', array(
            'teamPlayersStats' => $teamPlayersStats,
            'team' => $team,
            'filterForm' => $filterForm
        ));
    }
    
    
    public function createBasketballStatAction()
    {
        $request = ServiceLayer::getService('request');
        $translator = ServiceLayer::getService('translator');
        $repo = ServiceLayer::getService('TeamRepository');
        $teamManager = ServiceLayer::getService('TeamManager');
        $eventManager = ServiceLayer::getService('TeamEventManager');
       // $matchManager = ServiceLayer::getService('PlayerMatchManager');
        $statManager = ServiceLayer::getService('PlayerStatManager');
        $teamEvent = $eventManager->findEvent($request->get('event_id'));
        $teamEvent->setCurrentDate(new \DateTime($request->get('eventdate')));
        $team = $teamManager->findTeamById($teamEvent->getTeamId());
        $this->canManageEvents($team);

        if(null != $request->get('lid'))
        {
             $existLineup = $eventManager->getLineupById($request->get('lid'));
        }
        else
        {
             $existLineup = $eventManager->getEventLineup($teamEvent);
        }
        
        $lineupPlayersList = $eventManager->getEventLineupPlayers($existLineup);
        $lineupPlayers = array();
        foreach($lineupPlayersList as $lineupPlayer)
        {
             $lineupPlayers[$lineupPlayer->getLineupPosition()][$lineupPlayer->getId()] =  $lineupPlayer;
        }
        
         $matchOverview= $statManager->getBasketballMatchOverview($existLineup);
        

        
         $this->appendEventDetailInfo($teamEvent, $team,$existLineup);

        if('POST' == $request->getMethod())
        {
            $data =  $request->get('points');
            $statManager->clearEventTimeline($teamEvent);
            $flatLineupPlayers = array();
            foreach($lineupPlayersList as $lineupPlayer)
            {
                 $flatLineupPlayers[$lineupPlayer->getId()] =  $lineupPlayer;
            }


            foreach ($data as $hitType => $lines)
            {
                foreach($lines as $lineupPosition => $players)
                {
                    foreach($players as $lineupPlayerId => $points)
                    {
                        $event = new PlayerTimelineEvent();
                        $event->setEventId($request->get('event_id'));
                        $event->setHitType($hitType);
                        if (0 != $lineupPlayerId && $flatLineupPlayers[$lineupPlayerId]->getPlayerId() > 0)
                        {
                            $event->setUserId($flatLineupPlayers[$lineupPlayerId]->getPlayerId());
                        }

                        $event->setLineupPlayerId($lineupPlayerId);
                        $event->setLineupPosition($lineupPosition);
                        $event->setEventDate(new \DateTime($request->get('eventdate')));
                        $event->setLineupId($request->get('lid'));
                        $event->setTeamId($teamEvent->getTeamId());
                        $event->setPoints($points);
                        $statManager->saveTimelineEvent($event);
                    }
                }
            }
            
            //mom
            $mom = $request->get('mom');
            if(null != $mom)
            {
                $event = new PlayerTimelineEvent();
                $event->setEventId($request->get('event_id'));
                $event->setHitType('mom');
                if (0 != $lineupPlayerId && $flatLineupPlayers[$lineupPlayerId]->getPlayerId() > 0)
                {
                    $event->setUserId($flatLineupPlayers[$lineupPlayerId]->getPlayerId());
                }

                $event->setLineupPlayerId($lineupPlayerId);
                $event->setLineupPosition($flatLineupPlayers[$lineupPlayerId]->getLineupPosition());
                $event->setEventDate(new \DateTime($request->get('eventdate')));
                $event->setLineupId($request->get('lid'));
                $event->setTeamId($teamEvent->getTeamId());
                $statManager->saveTimelineEvent($event);
            }
            
            //close match
            $existLineup->setStatus('closed');
            $eventManager->saveLineup($existLineup);
            
            $request->addFlashMessage('team_basketball_stat_success', $translator->translate('TENNIS_VOLLEYBALL_SUCCESS'));
            $request->redirect();
 
        }

        return $this->render('Webteamer\Team\TeamModule:teamMatchStat:create_basketball_stat.php', array(
               'lineupPlayers' => $lineupPlayers,
                'team' => $team,
                'event' => $teamEvent,
                'lineup' => $existLineup,
                'matchOverview' => $matchOverview
            ));
    }
    
     public function basketballStatAction()
    {
        $request = ServiceLayer::getService('request');
        $teamManager = ServiceLayer::getService('TeamManager');
        $statManager = ServiceLayer::getService('PlayerStatManager');
        $seasonManager = ServiceLayer::getService('TeamSeasonManager');
        $team = $teamManager->findTeamById($request->get('team_id'));
        ServiceLayer::getService('security')->getIdentity()->checkTeamAccess($team);
        

        $actualSeason = $seasonManager->getTeamCurrentSeason($team);
        $seasonId =$actualSeason->getId();
        $seasonChoices = $seasonManager->getEventFormChoices($team);
        if(null != $request->get('filter'))
        {
            $filterData = $request->get('filter');
            $seasonId = $filterData['season_id'];
        }
        
        $currentSeason = $seasonManager->getSeasonById($seasonId);
        $statFrom = new DateTimeEx($currentSeason->getStartDate());
        $statTo = new DateTimeEx($currentSeason->getEndDate());
        $teamPlayersStats = $statManager->getAllTeamPlayersBasketballStat($team, $statFrom, $statTo);

        $filterForm = new Form();
        $filterForm->setName('filter');
        $filterForm->setField('season_id', array(
            'type' => 'choice',
            'choices' => $seasonChoices,
            'required' => true,
        ));
        $filterForm->setFieldValue('season_id',$seasonId);

        //var_dump($teamPlayersStats);exit;

        return $this->render('Webteamer\Team\TeamModule:teamMatchStat:basketball_stat.php', array(
            'teamPlayersStats' => $teamPlayersStats,
            'team' => $team,
            'filterForm' => $filterForm
        ));
    }

}

?>

