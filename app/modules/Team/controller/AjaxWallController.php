<?php

namespace Webteamer\Team\Controller;

use Core\ControllerResponse;
use Core\RestController;
use Core\ServiceLayer;
use Core\Validator;
use CR\Team\Form\TeamWallPostCommentForm;
use CR\Team\Form\TeamWallPostForm;
use CR\Team\Model\TeamWallPost;
use CR\Team\Model\TeamWallPostComment;
use DateTime;
use OpenGraph;

/**
 * @author Marek Hubáček
 * @version 1.0
 */
class AjaxWallController extends RestController {

     public function loadWidgetAction()
     {
           
          $html = $this->render('Webteamer\Team\TeamModule:widget:all_team.php', array(
                    ))->getContent();
            return $this->asHtml($html);
     }
             
    
    
    public function createWallPostAction()
    {
        $request = $this->getRequest();
        $userIdentity = ServiceLayer::getService('security')->getIdentity();
        $user = $userIdentity->getUser();
        $teamWallManager = ServiceLayer::getService('TeamWallManager');
        $teamManager = ServiceLayer::getService('TeamManager');
        
        $form = new TeamWallPostForm();
        $post = new TeamWallPost();
        $form->setEntity($post);
        $data =   $request->get($form->getName());
        $form->bindData($data);
        
        $validator = new Validator();
        $validator->setRules($form->getFields());
        $validator->setData($data);
        $validator->validateData();
        
        if(!$validator->hasErrors())
        {
            $post->setCreatedAt(new DateTime());
            $post->setStatus('visible');
            $post->setAuthorId($user->getId());
            
            
            $postImagesData = $request->get('post_images');
            if(null != $postImagesData)
            {
                $imageTransform = ServiceLayer::getService('imageTransform');
                $postImages = array();
                foreach($postImagesData as $postImage)
                {
                    $fileInfo = pathinfo($postImage);
                    $thumbName = md5($fileInfo['filename'].time()).'.'.$fileInfo['extension'];
                      
                    $imageTransform->resizeImage(array(
                        'sourceImg' => PUBLIC_DIR.$postImage,
                        'width' => 90, 
                        'height' => 90,
                        'targetDir' =>PUBLIC_DIR.$post->getSmalThumbDir(),
                        'filename' => $thumbName));
                    
                     
                     
                    
                      $thumbInfo =$imageTransform->resizeImage(array(
                        'sourceImg' => PUBLIC_DIR.$postImage,
                        'width' => 800, 
                        'height' => 800,
                        'targetDir' =>PUBLIC_DIR.$post->getLargeThumbDir(),
                        'filename' => $thumbName));
                      
                      $postImages[] = $thumbName;
                    
                }
                $post->setImages(implode(';',$postImages));
            }
            
           
            $teamWallManager->savePostWall($post);    
            $returnData = $teamWallManager->convertEntityToArray($post);
            
            
          
            $isOwner = true;
            $post->setAuthor($user);
            ob_start();
             ServiceLayer::getService('layout')->includePart(MODUL_DIR.'/Team/view/wall/_wall_line.php',array('post' => $post,'isOwner' =>$isOwner ));
            $item = ob_get_contents();
            ob_end_clean();
            
            //send system notify
           $team = $teamManager->findTeamById($data['team_id']);
           $notificationManager = ServiceLayer::getService('SystemNotificationManager');
           $notificationManager->createPushNotify('team_talk_post',array('team' => $team));
            
            $result = array('result' => 'SUCCESS','post' =>  $returnData,'item' =>$item);
        }
        else
        {
            $result = array('result' => 'ERROR','errors' => $validator->getErrors());
        }
        
        return $this->asJson($result, $status = 'success');
    }
    
     public function deleteWallPostAction()
    {
        $request = $this->getRequest();
        $userIdentity = ServiceLayer::getService('security')->getIdentity();
        $user = $userIdentity->getUser();
        $teamWallManager = ServiceLayer::getService('TeamWallManager');
        
        $post = $teamWallManager->getPostById($request->get('id'));
         
        if($user->getId() == $post->getAuthorId())
        {
             $teamWallManager->deletePostWall($post);
             $result = array('result' => 'SUCCESS');
             return $this->asJson($result, $status = 'success');
        }
        
        $result = array('result' => 'ERROR');
        return $this->asJson($result, $status = 'error');
         
    }

    public function createWallPostCommentAction()
    {
        $request = $this->getRequest();
        $userIdentity = ServiceLayer::getService('security')->getIdentity();
        $user = $userIdentity->getUser();
        $teamWallManager = ServiceLayer::getService('TeamWallManager');
        $teamManager = ServiceLayer::getService('TeamManager');
        
        $form = new TeamWallPostCommentForm();
        $comment = new TeamWallPostComment();
        $form->setEntity($comment);
        $data =   $request->get('comment');
        $form->bindData($data);
        
        $validator = new Validator();
        $validator->setRules($form->getFields());
        $validator->setData($data);
        $validator->validateData();
        
        if(!$validator->hasErrors())
        {
            $comment->setCreatedAt(new DateTime());
            $comment->setStatus('visible');
            $comment->setAuthorId($user->getId());
            $comment->setParentId($data['post_id']);

            $parentPost = $teamWallManager->getPostById($data['post_id']);
            $comment->setTeamId($parentPost->getTeamId());
            $comment->setLvl($parentPost->getLvl()+1);
           
            $postId = $teamWallManager->savePostWallComment($comment);    
            $comment->setId($postId);
            $returnData = $teamWallManager->convertEntityToArray($comment);

            $comment->setAuthor($user);
            ob_start();
             ServiceLayer::getService('layout')->includePart(MODUL_DIR.'/Team/view/wall/_comment_line.php',array('comment' => $comment ));
            $item = ob_get_contents();
            ob_end_clean();
            
             //send system notify
           $team = $teamManager->findTeamById($parentPost->getTeamId());
           $notificationManager = ServiceLayer::getService('SystemNotificationManager');
           $notificationManager->createPushNotify('team_talk_post',$team);
            
            $result = array('result' => 'SUCCESS','comment' =>  $returnData,'item' =>$item);
        }
        else
        {
            $result = array('result' => 'ERROR','errors' => $validator->getErrors());
        }
        
        return $this->asJson($result, $status = 'success');

    }
    
    public function getWallPostCommentsAction()
    {
         $request = $this->getRequest();
         $teamWallManager = ServiceLayer::getService('TeamWallManager');
         $post = $teamWallManager->getPostById($request->get('post'));
         $comments = $teamWallManager->getPostComments($post);
         
         $list = array();
         $lines = array();
         foreach ($comments as $comment)
         {
             $list[] = $teamWallManager->convertEntityToArray($comment);
             
            ob_start();
             ServiceLayer::getService('layout')->includePart(MODUL_DIR.'/Team/view/wall/_comment_line.php',array('comment' => $comment ));
            $item = ob_get_contents();
            ob_end_clean();
             
             $lines[] = $item;
         }
         
         //comment form
           ob_start();
            ServiceLayer::getService('layout')->includePart(MODUL_DIR.'/Team/view/wall/_comment_line_reaction.php',array('post' => $post ));
           $postReaction = ob_get_contents();
           ob_end_clean();
         
         
         
         $result = array('result' => 'SUCCESS','comments' =>  $list,'lines' => $lines,'postReaction' => $postReaction);
         return $this->asJson($result, $status = 'success');
    }
    
    public function getUrlDataAction()
    {
         require_once(LIB_DIR.'/OpenGraph/OpenGraph.php');
         //require_once(LIB_DIR.'/OpenGraph/OpenGraphNode.php');
        
       
         
         $request = $this->getRequest();
         $url = $request->get('url');
        
         //$meta =  get_meta_tags($url);
        
          $graph = OpenGraph::fetch($url);
          $data = array();
          foreach ($graph as $key => $value) {
                $data[$key] = $value;
          }

          $result = array('result' => 'SUCCESS','data' =>  $data);
          return $this->asJson($result, $status = 'success');
    }
    
    
    public function uploadPhotoAction()
    {
        $request = ServiceLayer::getService('request');
        
        $tmpWallWebDir = 'img/wall/tmp';
        $tmpWallUploadDir =  PUBLIC_DIR.$tmpWallWebDir;
        $baseName = md5(time()).'_'.$_FILES['post_image']['name'];
        $sourceImg = $tmpWallUploadDir.'/'.$baseName;
        
        $request->uploadFile('post_image',$tmpWallUploadDir,$baseName);
        
        $result['file'] = '/'.$tmpWallWebDir.'/'.$baseName;
        $result['file_name'] = $baseName;
        
        
        
        $result['result'] = 'SUCCESS';
        
        $layout = ServiceLayer::getService('layout');
        $layout->setTemplate(null);
        
        $response = new ControllerResponse();
        $response->setStatus('success');
        $response->setContent(json_encode($result));
        $response->setType('json');
         return $response;
    }

    public function loadPostsAction()
    {
        $teamWallManager = ServiceLayer::getService('TeamWallManager'); 
        $repo = ServiceLayer::getService('TeamRepository');
        $team = $repo->find( $this->getRequest()->get('team'));
        $start = $this->getRequest()->get('from');
         $userIdentity = ServiceLayer::getService('security')->getIdentity();
        $user = $userIdentity->getUser();
        $wallPosts = $teamWallManager->getWallPosts($team,$start);
        $items = array();
        foreach($wallPosts as $post)
        {
             ob_start();
                ServiceLayer::getService('layout')->includePart(MODUL_DIR.'/Team/view/wall/_wall_line.php',array('post' => $post,'isOwner' => ($post->getAuthorId() ==  $user->getId()) ? true : false   ));
                $item = ob_get_contents();
            ob_end_clean();
            $items[] = $item;
        }
        
        $layout = ServiceLayer::getService('layout');
        $layout->setTemplate(null);
        
        $result = array('result' => 'SUCCESS','lines' =>  $items,'start' => $post->getId(),'allPostLoaded' => $wallPosts->allPostLoaded());
        $response = new ControllerResponse();
        $response->setStatus('success');
        $response->setContent(json_encode($result));
        $response->setType('json');
        return $response;
    }
    
    public function postLikeAction()
    {
        $request = $this->getRequest();
        $userIdentity = ServiceLayer::getService('security')->getIdentity();
        $user = $userIdentity->getUser();
        $teamWallManager = ServiceLayer::getService('TeamWallManager');
        
        $like = new \CR\Team\Model\TeamWallLike();
        $like->setPostId($request->get('post'));
        $like->setUserId($user->getId());
        $like->setCreatedAt(new \DateTime());
        $like->setType('like');
        $teamWallManager->savePostWallLike($like);    
     
        
        $post = $teamWallManager->getPostById($request->get('post'));
        $likesCount = $teamWallManager->getPostLikesCount($post);
        
        $result = array('result' => 'SUCCESS','likesCount' =>  $likesCount,'linktext' => $this->getTranslator()->translate('Unlike').'('.$likesCount.')','linkurl' => $this->getRouter()->link('tam_wall_post_unlike',array('post' => $post->getId())));
        

        return $this->asJson($result, $status = 'success');
    }
    
     public function postUnlikeAction()
    {
        $request = $this->getRequest();
        $userIdentity = ServiceLayer::getService('security')->getIdentity();
        $user = $userIdentity->getUser();
        $teamWallManager = ServiceLayer::getService('TeamWallManager');
        $teamWallManager->deleteUserPostLike($request->get('post'),$user->getId());
        $post = $teamWallManager->getPostById($request->get('post'));
        $likesCount = $teamWallManager->getPostLikesCount($post);
        
         $result = array('result' => 'SUCCESS','likesCount' =>  $likesCount,'linktext' => $this->getTranslator()->translate('Like').'('.$likesCount.')','linkurl' => $this->getRouter()->link('tam_wall_post_like',array('post' => $post->getId())));
        

        return $this->asJson($result, $status = 'success');
    }



    
    
}

?>