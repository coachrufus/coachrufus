<?php

namespace Webteamer\Team\Controller;

use AclException;
use Core\Controller;
use Core\ControllerResponse;
use Core\ServiceLayer;
use Core\Form as Form;
use DateTime;
use Webteamer\Player\Model\PlayerTimelineEvent;
use Core\Notifications\AddRateNotificationSender;
use Core\Notifications\AddRateStore;
use Core\Notifications\MatchResultsStore;
use Core\Notifications\MatchResultsNotificationSender;
use Core\Types\DateTimeEx;

/**
 * @author Marek Hubáček
 * @version 1.0
 */
class TeamMatchController extends Controller {

    public function canManageEvents($team)
    {
        if (!ServiceLayer::getService('security')->getIdentity()->hasTeamPermission('manageEvents', $team))
        {
            throw new AclException();
        }
    }

    public function hasFeatureAccess($team, $feature)
    {
        if (!ServiceLayer::getService('TeamCreditManager')->teamHasFeatureAccess($team, $feature))
        {
            throw new AclException();
        }
    }

    public function printLineupAction()
    {
        $request = ServiceLayer::getService('request');
        $router = ServiceLayer::getService('router');
        $eventManager = ServiceLayer::getService('TeamEventManager');
        $teamManager = ServiceLayer::getService('TeamManager');
        $existLineup = $eventManager->getLineupById($request->get('id'));


        $event = $eventManager->findEvent($existLineup->getEventId());
        $event->setCurrentDate($existLineup->getEventDate());
        $team = $teamManager->findTeamById($event->getTeamId());
       


        $members = $teamManager->getTeamPlayers($team->getId());


        $lineupPlayers = $eventManager->getEventLineupPlayers($existLineup);
        $firstLine = array();
        $secondLine = array();
        foreach ($lineupPlayers as $lineupPlayer)
        {
            if ($lineupPlayer->getLineupPosition() == 'first_line')
            {
                $firstLine[] = $lineupPlayer;
            }
            if ($lineupPlayer->getLineupPosition() == 'second_line')
            {
                $secondLine[] = $lineupPlayer;
            }
        }

        $layout = ServiceLayer::getService('layout');
        $layout->setTemplate(GLOBAL_DIR . '/templates/PrintLayout.php');


        $template = 'print_lineup.php';

        $sportManager = ServiceLayer::getService('SportManager');
        $statRoute = $sportManager->getTeamSportStatRoute($team);


        if ('team_basketball_stat' == $statRoute)
        {
            $template = 'print_basketball_lineup.php';
        }

        /*
          if('team_volleyball_stat' == $statRoute)
          {
          $template = 'print_volleyball_lineup.php';
          }
         */
        if ('team_volleyball_stat' == $statRoute)
        {
            $template = 'print_volleyball_advanced_lineup.php';
        }

        if ('team_tennis_stat' == $statRoute)
        {
            $template = 'print_tennis_lineup.php';
        }



        return $this->render('Webteamer\Team\TeamModule:teamMatch:' . $template, array(
                    'team' => $team,
                    'event' => $event,
                    'firstLine' => $firstLine,
                    'secondLine' => $secondLine,
                    'members' => $members,
                    'lineup' => $existLineup
        ));
    }

    public function updateLineupAction()
    {
        $layout = ServiceLayer::getService('layout');
        $request = ServiceLayer::getService('request');
        $eventManager = ServiceLayer::getService('TeamEventManager');

        $eventManager->updateLineup($request->get('id'), array($request->get('k') => $request->get('v')));


        $layout->setTemplate(null);
        $response = new ControllerResponse();
        $response->setStatus('success');
        $response->setContent(json_encode(array('result' => 'success')));
        $response->setType('json');
        return $response;
    }

    public function removeLineupPlayerAction()
    {
        $layout = ServiceLayer::getService('layout');
        $request = ServiceLayer::getService('request');
        $eventManager = ServiceLayer::getService('TeamEventManager');
        $teamManager = ServiceLayer::getService('TeamManager');
        $lineupPlayer = $eventManager->getEventLineupPlayerById($request->get('item_id'));
        
       
        

        $result = array('result' => 'success');
        $result['player'] = $eventManager->convertEntityToArray($lineupPlayer);
        $result['player']['icon'] = ' ';
        $result['player']['team_role'] = ' ';
        $result['player']['team_number'] = '';
        
        $member = $teamManager->findTeamPlayerById($lineupPlayer->getTeamPlayerId());
        if(null != $member)
        {
            $result['player']['icon'] = $member->getMidPhoto();
            $result['player']['team_role'] = $member->getTeamRoleName();
            $result['player']['team_number'] = ($member->getPlayerNumber() == null) ? ' ' :  $member->getPlayerNumber() ;
        }


        $eventManager->removeLineupPlayer($request->get('item_id'));

        $layout->setTemplate(null);
        $response = new ControllerResponse();
        $response->setStatus('success');
        $response->setContent(json_encode($result));
        $response->setType('json');
        return $response;
    }

    public function addLineupPlayerAction()
    {
        $layout = ServiceLayer::getService('layout');
        $request = ServiceLayer::getService('request');
        $eventManager = ServiceLayer::getService('TeamEventManager');
        $teamManager = ServiceLayer::getService('TeamManager');
        $member = $teamManager->findTeamPlayerById($request->get('team_player_id'));
        
        $playerPosition = 'PLAYER';
        if($member->getTeamRole() == 'GOAL_KEEPER')
        {
              $playerPosition = 'GOAL_KEEPER';
        }


        $lineup = $eventManager->getLineupById($request->get('lineup_id'));
        $id = $eventManager->createLineupPlayer($lineup, array(
            'player_id' => $request->get('player_id'),
            'lineup_position' => $request->get('target_line'),
            'player_name' => $request->get('player_name'),
            'team_player_id' => $request->get('team_player_id'),
            'player_position' => $playerPosition
        ));

        $layout->setTemplate(null);
        $response = new ControllerResponse();
        $response->setStatus('success');
        $response->setContent(json_encode(array('result' => 'success', 'id' => $id)));
        $response->setType('json');
        return $response;
    }

    public function changeLineupPlayerAction()
    {
        $request = ServiceLayer::getService('request');
        $eventManager = ServiceLayer::getService('TeamEventManager');
        $eventManager->changeLineupPlayer($request->get('item_id'), $request->get('lineup'));
        $layout = ServiceLayer::getService('layout');
        $layout->setTemplate(null);
        $response = new ControllerResponse();
        $response->setStatus('success');
        $response->setContent(json_encode(array('result' => 'success')));
        $response->setType('json');
        return $response;
    }

    /**
     * Append info to event about season, sport, attendance
     */
    private function appendEventDetailInfo($event, $team, $existLineup = null)
    {
        $request = ServiceLayer::getService('request');
        $eventManager = ServiceLayer::getService('TeamEventManager');
        $sportManager = ServiceLayer::getService('SportManager');
        $statManager = ServiceLayer::getService('PlayerStatManager');
        $playgroundManager = ServiceLayer::getService('PlaygroundManager');
        $localityManager = ServiceLayer::getService('LocalityManager');
        $sportEnum = $sportManager->getSportFormChoices();
        $sportName = $this->getTranslator()->translate($sportEnum[$team->getSportId()]);
        $event->setSportName($sportName);

        $eventTypeEnum = $eventManager->getEventTypeEnum();
        $event->setTypeName($eventTypeEnum[$event->getEventType()]);

        $seasonManager = ServiceLayer::getService('TeamSeasonManager');
        $eventSeason = $seasonManager->getSeasonById($event->getSeason());
        $seasonName = $eventSeason->getName() . ' ' . $this->getLocalizer()->formatDate($eventSeason->getStartDate()) . ' - ' . $this->getLocalizer()->formatDate($eventSeason->getEndDate());
        $event->setSeasonName($seasonName);

        $event->setStatRoute($sportManager->getTeamSportAddStatRoute($team));

        $attendance = $eventManager->getEventAttendance($event);
        $event->setAttendance($attendance);
        
        if(null != $existLineup)
        {
            $matchOverview = $statManager->getMatchOverview($existLineup);
            $event->setMatchResult($matchOverview->getResult());
            $event->setMatchOverview($matchOverview);
             
            if($existLineup->getStatus() == 'closed')
            {
                 $event->setClosed(true);
            }
        }
        
         if(null != $event->getPlaygroundDataAsArray())
        {
             $playground = $playgroundManager->createObjectFromArray($event->getPlaygroundDataAsArray());
             $event->setPlayground($playground);
        }
        elseif(null != $event->getPlaygroundId())
        {
            $playground = \Core\ServiceLayer::getService('PlaygroundRepository')->find($event->getPlaygroundId());
            $locality = $localityManager->getLocalityById($playground->getLocalityId());
            $playground->setLocality($locality);
            $event->setPlayground($playground);
        }


        return $event;
    }
    
    private function getLineupCreationAccess($team)
    {
        $stepsManager = \Core\ServiceLayer::getService('StepsManager');
        $teamCreditManager = \Core\ServiceLayer::getService('TeamCreditManager');
        $showRandomCreate = false;
        $showEfficiencyCreate = false;
        $wizardWinished = $stepsManager->isStepFinished('step3_final');
        if($wizardWinished)
        {
            if($teamCreditManager->teamHasFeatureAccess($team, 'efficiency_create_linuep'))
            {
                $showRandomCreate = true;
                $showEfficiencyCreate = true;
            }
        }
        else
        {
            $showRandomCreate = $stepsManager->isStepFinished('step1_final');
            $showEfficiencyCreate = $stepsManager->isStepFinished('step2_final');
        }
        
        return  array('showRandomCreate' => $showRandomCreate, 'showEfficiencyCreate' => $showEfficiencyCreate,'wizardWinished' => $wizardWinished);

    }

    public function newLineupAction()
    {
        $request = ServiceLayer::getService('request');
        $eventManager = ServiceLayer::getService('TeamEventManager');
        $teamManager = ServiceLayer::getService('TeamManager');
        $event = $eventManager->findEventById($request->get('event_id'));
        $event->setCurrentDate(new \DateTime($request->get('event_date')));
        $team = $teamManager->findTeamById($event->getTeamId());
        $this->canManageEvents($team);
        $this->appendEventDetailInfo($event, $team);
        
         if($event->getAttendanceInSum() < 2)
        {
            $security = ServiceLayer::getService('security');
            $user = $security->getIdentity()->getUser();
            return $this->render('Webteamer\Team\TeamModule:teamMatch:new_lineup_unavailable.php', array(
                        'event' => $event,
                        'team' => $team,
                        'user' => $user,
            ));
        }
        
        $lineupCreationAccess = $this->getLineupCreationAccess($team);
        ServiceLayer::getService('StepsManager')->finishCycle2($team->getId());

        return $this->render('Webteamer\Team\TeamModule:teamMatch:new_lineup.php', array(
                    'team' => $team,
                    'event' => $event,
                    'showRandomCreate' => $lineupCreationAccess['showRandomCreate'],
                    'showEfficiencyCreate' => $lineupCreationAccess['showEfficiencyCreate'],
                    'wizardWinished' => $lineupCreationAccess['wizardWinished']
        ));
    }
    
    public function viewLineupAction()
    {
        $request = ServiceLayer::getService('request');
        $eventManager = ServiceLayer::getService('TeamEventManager');
        $teamManager = ServiceLayer::getService('TeamManager');
         $seasonManager = ServiceLayer::getService('TeamSeasonManager');
        $existLineup = $eventManager->getLineupById($request->get('id'));
        $event = $eventManager->findEvent($existLineup->getEventId());
        $team = $teamManager->findTeamById($event->getTeamId());

        $event->setCurrentDate($existLineup->getEventDate());
        $this->appendEventDetailInfo($event, $team,$existLineup);
        $playerStatManager = ServiceLayer::getService('PlayerStatManager');

       $members = $teamManager->getTeamPlayers($team->getId());
        $lineupPlayers = $eventManager->getEventLineupPlayers($existLineup);
        $players = array();

        $eventSeason = $seasonManager->getSeasonById($event->getSeason());
        $statFrom = new DateTimeEx($eventSeason->getStartDate());
        $statTo = new DateTimeEx($eventSeason->getEndDate());
        $teamPlayersStats = $playerStatManager->getAllTeamPlayersStat($team, null, $statFrom, $statTo);


        foreach ($members as $member)
        {
            //$player =  $playerManager-> findPlayerById($member->getPlayerId());
            $playerStats = $teamPlayersStats[$member->getId()];
            if(null != $playerStats)
            {
                $member->setStats($playerStats);
            }
            else {
                $member->setStats(new \Webteamer\Player\Model\PlayerStat());
            }
            //$member->getStats()->getLastGamesEfficiency();
            $players[$member->getId()] = $member;
        }

        $firstLine = array();
        $secondLine = array();
        $lineupPlayerIds = array();
        $lastGameEffeciencyDiff = 1;
        foreach ($lineupPlayers as $lineupPlayer)
        {
            
            if ($lineupPlayer->getTeamPlayerId() == null)
            {
                $lastGameEffeciency = 0;
            }
            else
            {
                //$lastGameEffeciency = $players[$lineupPlayer->getPlayerId()]->getStats()->getLastGamesEfficiency();
                $lastGameEffeciency = $players[$lineupPlayer->getTeamPlayerId()]->getStats()->getLastGamesEfficiency();
            }
            $efficiencyIndex = round($lastGameEffeciency*10000)+$lastGameEffeciencyDiff++;
            
            if ($lineupPlayer->getLineupPosition() == 'first_line')
            {
                $teamPlayer = null;
                if (null != $lineupPlayer->getTeamPlayerId())
                {
                    $teamPlayer = $players[$lineupPlayer->getTeamPlayerId()];
                }

                $firstLine[$efficiencyIndex] = array('player' => $lineupPlayer, 'teamPlayer' => $teamPlayer, 'efficiency' => $lastGameEffeciency);
            }
            if ($lineupPlayer->getLineupPosition() == 'second_line')
            {
                $teamPlayer = null;
                if (null != $lineupPlayer->getTeamPlayerId())
                {
                    $teamPlayer = $players[$lineupPlayer->getTeamPlayerId()];
                }
                $secondLine[$efficiencyIndex] = array('player' => $lineupPlayer, 'teamPlayer' => $teamPlayer, 'efficiency' => $lastGameEffeciency);
            }

            $lineupPlayerIds[] = $lineupPlayer->getTeamPlayerId();
        }
        krsort($firstLine);
        krsort($secondLine);
        $availablePlayers = array();
        foreach ($members as $member)
        {
            if (!in_array($member->getId(), $lineupPlayerIds))
            {
                $availablePlayers[] = $member;
            }
        }
        
        $template = 'Webteamer\Team\TeamModule:teamMatch:view_lineup.php';
        return $this->render($template, array(
                    'team' => $team,
                    'event' => $event,
                    'firstLine' => $firstLine,
                    'secondLine' => $secondLine,
                    'lineup' => $existLineup,
                    'members' => $members,
                    'existLineup' => $existLineup,
        ));
    }
    
    private function createLinuep()
    {
        
    }

    public function createLineupAction()
    {
        $request = ServiceLayer::getService('request');
        $router = ServiceLayer::getService('router');
        $teamMatchManager = ServiceLayer::getService('TeamMatchManager');
        
        $eventManager = ServiceLayer::getService('TeamEventManager');
        $teamManager = ServiceLayer::getService('TeamManager');
        $event = $eventManager->findEvent($request->get('event_id'));
        $event->setCurrentDate(new \DateTime($request->get('event_date')));
        $team = $teamManager->findTeamById($event->getTeamId());
        $this->canManageEvents($team);
        

        /*
        $eventSeason = $seasonManager->getSeasonById($event->getSeason());
        $statFrom = new DateTimeEx($eventSeason->getStartDate());
        $statTo = new DateTimeEx($eventSeason->getEndDate());
        $teamPlayersStats = $playerStatManager->getAllTeamPlayersStat($team, null, $statFrom, $statTo);
        $pushNotifyRecipients = array();
        foreach ($attendanceList as $attendance)
        {
            $teamPlayer = $teamManager->findTeamPlayerById($attendance->getTeamPlayerId());
            if (null != $teamPlayer)
            {
                //$playerStat = $playerStatManager->getTeamPlayerStat($teamPlayer);
                $playerStat = $teamPlayersStats[$teamPlayer->getId()];
                if(null == $playerStat)
                {
                   $playerStat = new \Webteamer\Player\Model\PlayerStat();
                }
               
                $teamPlayer->setStats($playerStat);
                $attendance->setTeamPlayer($teamPlayer);
                if($teamPlayer->getPlayerId() > 0)
                {
                    $pushNotifyRecipients[] = $teamPlayer;
                }
            }
        }

        if ('random' == $request->get('type'))
        {
            $lineupId = $eventManager->createRandomEventLineUp($event, $attendanceList, $existLineup);
            ServiceLayer::getService('StepsManager')->finishCreateRandomLineup($team);
            ServiceLayer::getService('StepsManager')->finishCreatePerformanceLineup($team);
        }
        if ('manual' == $request->get('type'))
        {
            ServiceLayer::getService('StepsManager')->finishCreateManualLineup($team);
            ServiceLayer::getService('StepsManager')->finishCreateRandomLineup($team);
            ServiceLayer::getService('StepsManager')->finishCreatePerformanceLineup($team);
            if (null == $existLineup)
            {
                $lineupId = $eventManager->createManualEventLineUp($event, $attendanceList);
            }
            else
            {
                $lineupId = $existLineup->getId();
            }
        }
        if ('efficiency' == $request->get('type'))
        {
            $lineupId = $eventManager->createEfficiencyEventLineUp($event, $attendanceList, $existLineup);
            ServiceLayer::getService('StepsManager')->finishCreatePerformanceLineup($team);
        }

       
*/

        $lineupId = $teamMatchManager->createLineup($event,$team,$request->get('type'));
                
                
       
        $request->redirect($router->link('team_match_edit_lineup', array('id' => $lineupId)));
        /*
        if (null == $existLineup or empty($existLineup))
        {
            $request->redirect($router->link('team_match_edit_lineup', array('id' => $lineupId)));
        }
        else
        {
            $request->redirect($router->link('team_match_edit_lineup', array('id' => $existLineup->getId())));
        }
         * 
         */
    }

    public function editLineupAction()
    {
        $request = ServiceLayer::getService('request');
        $eventManager = ServiceLayer::getService('TeamEventManager');
        $teamManager = ServiceLayer::getService('TeamManager');
        $seasonManager = ServiceLayer::getService('TeamSeasonManager');
        $existLineup = $eventManager->getLineupById($request->get('id'));
        $event = $eventManager->findEvent($existLineup->getEventId());
        $team = $teamManager->findTeamById($event->getTeamId());
        $security = ServiceLayer::getService('security');
        $user = $security->getIdentity()->getUser();
        $this->canManageEvents($team);

        $event->setCurrentDate($existLineup->getEventDate());
        $this->appendEventDetailInfo($event, $team,$existLineup);
        $playerStatManager = ServiceLayer::getService('PlayerStatManager');
        
       


        $members = $teamManager->getTeamPlayers($team->getId());
        $lineupPlayers = $eventManager->getEventLineupPlayers($existLineup);
        $players = array();

        $eventSeason = $seasonManager->getSeasonById($event->getSeason());
        $statFrom = new DateTimeEx($eventSeason->getStartDate());
        $statTo = new DateTimeEx($eventSeason->getEndDate());
        $teamPlayersStats = $playerStatManager->getAllTeamPlayersStat($team, null, $statFrom, $statTo);


        foreach ($members as $member)
        {
            //$player =  $playerManager-> findPlayerById($member->getPlayerId());
            $playerStats = $teamPlayersStats[$member->getId()];
            if(null != $playerStats)
            {
                $member->setStats($playerStats);
            }
            else {
                $member->setStats(new \Webteamer\Player\Model\PlayerStat());
            }
            //$member->getStats()->getLastGamesEfficiency();
            $players[$member->getId()] = $member;
        }

        $firstLine = array();
        $secondLine = array();
        $lineupPlayerIds = array();
        $lastGameEffeciencyDiff = 1;
        foreach ($lineupPlayers as $lineupPlayer)
        {
            
            if ($lineupPlayer->getTeamPlayerId() == null or !array_key_exists($lineupPlayer->getTeamPlayerId(), $players))
            {
                $lastGameEffeciency = 0;
            }
            else
            {
                //$lastGameEffeciency = $players[$lineupPlayer->getPlayerId()]->getStats()->getLastGamesEfficiency();
                $lastGameEffeciency = $players[$lineupPlayer->getTeamPlayerId()]->getStats()->getLastGamesEfficiency();
            }
            $efficiencyIndex = round($lastGameEffeciency*10000)+$lastGameEffeciencyDiff++;
            
            if ($lineupPlayer->getLineupPosition() == 'first_line')
            {
                $teamPlayer = null;
                if (null != $lineupPlayer->getTeamPlayerId())
                {
                    $teamPlayer = $players[$lineupPlayer->getTeamPlayerId()];
                }

                $firstLine[$efficiencyIndex] = array('player' => $lineupPlayer, 'teamPlayer' => $teamPlayer, 'efficiency' => $lastGameEffeciency);
            }
            if ($lineupPlayer->getLineupPosition() == 'second_line')
            {
                $teamPlayer = null;
                if (null != $lineupPlayer->getTeamPlayerId())
                {
                    $teamPlayer = $players[$lineupPlayer->getTeamPlayerId()];
                }
                $secondLine[$efficiencyIndex] = array('player' => $lineupPlayer, 'teamPlayer' => $teamPlayer, 'efficiency' => $lastGameEffeciency);
            }

            $lineupPlayerIds[] = $lineupPlayer->getTeamPlayerId();
        }
        krsort($firstLine);
        krsort($secondLine);
        
        $attendance = $eventManager->getEventAttendance($event);
        $attendancePlayers = array();
        
        foreach($attendance as $attendanceInfo)
        {
            $attendancePlayers[$attendanceInfo->getTeamPlayerId()] = $attendanceInfo; 
        }

        $availablePlayers = array();
        
        foreach ($members as $member)
        {
            if (!in_array($member->getId(), $lineupPlayerIds) && array_key_exists($member->getId(), $attendancePlayers) && $attendancePlayers[$member->getId()]->getStatus() == 1)
            {
                $availablePlayers[] = $member;
            }
        }
        
        $template = 'Webteamer\Team\TeamModule:teamMatch:edit_lineup.php';
        if($existLineup->getStatus() == 'closed')
        {
            $template = 'Webteamer\Team\TeamModule:teamMatch:view_lineup.php';
        }
        
        
        $finishedProgressStep5 = false;
        $stepsManager = \Core\ServiceLayer::getService('StepsManager');
        $wizardWinished = $stepsManager->isStepFinished('step3_final');
        
        if(!$wizardWinished)
        {
            $finishedProgressStep5 = $stepsManager->isStepFinished('event_roster_manual_create');
        }
         
        
        $lineupCreationAccess = $this->getLineupCreationAccess($team);
        
        return $this->render($template, array(
                    'team' => $team,
                    'event' => $event,
                    'firstLine' => $firstLine,
                    'secondLine' => $secondLine,
                    'availablePlayers' => $availablePlayers,
                    'lineup' => $existLineup,
                    'members' => $members,
                    'existLineup' => $existLineup,
                    'showRandomCreate' => $lineupCreationAccess['showRandomCreate'],
                    'showEfficiencyCreate' => $lineupCreationAccess['showEfficiencyCreate'],
                    'finishedProgressStep5' => $finishedProgressStep5,
                    'wizardWinished' => $wizardWinished,
                    'user' => $user
        ));
    }
    
    public function viewLiveStatAction()
    {
        $request = ServiceLayer::getService('request');
        $eventManager = ServiceLayer::getService('TeamEventManager');
        $teamManager = ServiceLayer::getService('TeamManager');
        $statManager = ServiceLayer::getService('PlayerStatManager');
        $security = ServiceLayer::getService('security');
        $user = $security->getIdentity()->getUser();
        $event = $eventManager->findEvent($request->get('event_id'));
        $event->setCurrentDate(new \DateTime($request->get('eventdate')));
        $team = $teamManager->findTeamById($event->getTeamId());
        $existLineup = $eventManager->getEventLineup($event);
        $existLineup->setStatus('closed');
        $this->appendEventDetailInfo($event, $team,$existLineup);
        //$eventTimeline = $statManager->getEventTimeline($event);
        $matchOverview = $statManager->getMatchOverview($existLineup);
        
         $matchResult = array('first_line' => 0, 'second_line' => 0);
        if(null != $matchOverview)
        {
            $matchResult['first_line'] = ($matchOverview->getFirstLineGoals() == null) ? 0 : $matchOverview->getFirstLineGoals() ;
            $matchResult['second_line'] = ($matchOverview->getSecondLineGoals() == null) ? 0 : $matchOverview->getSecondLineGoals() ;
            $matchResult['winner'] = 'none';
            
            if($matchResult['first_line']  > $matchResult['second_line'])
            {
                 $matchResult['winner'] = 'first_line' ;
            }
            
            if($matchResult['first_line']  < $matchResult['second_line'])
            {
                 $matchResult['winner'] = 'second_line' ;
            }
            
        }
        
        $groupedEventTimeline = $statManager->getGroupedEventTimeline($event);
        
        
        $template = 'view_live_stat.php';
        $lineupPlayers = array();
        if ('team_match_create_basketball_stat' == $event->getStatRoute())
        {
            $template = 'view_live_stat_basketball.php';
            $lineupPlayersList = $eventManager->getEventLineupPlayers($existLineup);
            foreach ($lineupPlayersList as $lineupPlayer)
            {
                $lineupPlayers[$lineupPlayer->getLineupPosition()][$lineupPlayer->getId()] = $lineupPlayer;
            }
            $matchOverview = $statManager->getBasketballMatchOverview($existLineup);
        }
        if('team_match_create_volleyball_stat' == $event->getStatRoute() or 'team_match_create_tennis_stat' == $event->getStatRoute()   )
        {
            $template = 'view_live_stat_set.php';
            $setMatch = new \Webteamer\Player\Model\SetMatch();
            $eventTimeline = $statManager->getEventTimeline($event);
            
            
            
            $lineupPlayersList = $eventManager->getEventLineupPlayers($existLineup);

            foreach ($lineupPlayersList as $lineupPlayer)
            {
                $lineupPlayers[$lineupPlayer->getLineupPosition()][$lineupPlayer->getId()] = $lineupPlayer;
                $lineupPlayersIndexed[$lineupPlayer->getId()] = $lineupPlayer;
            }
            
            $setPoints = array();
            foreach($eventTimeline as $timelineItem)
            {
                //total points
               if(0 != $timelineItem->getLineupPlayerId() )
               {
                    $setPoints[$timelineItem->getSetPosition()]['points'][$timelineItem->getLineupPlayerId()] = $timelineItem->getPoints();
                $timelinePlayer = $lineupPlayersIndexed[$timelineItem->getLineupPlayerId()];
                $setPoints[$timelineItem->getSetPosition()]['total_points'][$timelinePlayer->getLineupPosition()] += $timelineItem->getPoints();
                
                
                $setPoints[$timelineItem->getSetPosition()]['duration'] = $timelineItem->getDurationTime();
               }
               
            }

            foreach($setPoints as $setPosition => $setData)
            {
                $setMatch->addSet($setPosition, $setData);
            }

            return $this->render('Webteamer\Team\TeamModule:teamMatch:'.$template, array(
               'lineupPlayers' => $lineupPlayers,
                'team' => $team,
                'event' => $event,
                'eventTimeline' => $eventTimeline,
                'lineup' => $existLineup,
                'setMatch' => $setMatch,
                'existLineup' => $existLineup
            ));
             
        }
        
        
         return $this->render('Webteamer\Team\TeamModule:teamMatch:'.$template, array(
                        'event' => $event,
                        'team' => $team,
                        'user' => $user,
                        'lineup' => $existLineup,
             'matchOverview' => $matchOverview,
             'groupedEventTimeline' => $groupedEventTimeline,
              'matchResult' => $matchResult,
             'lineupPlayers' => $lineupPlayers
            ));

    }

    public function createLiveStatAction()
    {
        $request = ServiceLayer::getService('request');
        $sportManager = ServiceLayer::getService('SportManager');
        $eventManager = ServiceLayer::getService('TeamEventManager');
        $teamManager = ServiceLayer::getService('TeamManager');
        $statManager = ServiceLayer::getService('PlayerStatManager');
        $event = $eventManager->findEvent($request->get('event_id'));
        $event->setCurrentDate(new \DateTime($request->get('eventdate')));
        $team = $teamManager->findTeamById($event->getTeamId());
        $this->canManageEvents($team);
        

        if (null != $request->get('lid'))
        {
            $existLineup = $eventManager->getLineupById($request->get('lid'));
        }
        else
        {
            $existLineup = $eventManager->getEventLineup($event);
        }
        
         if (null == $existLineup)
        {
            $security = ServiceLayer::getService('security');
            $user = $security->getIdentity()->getUser();
            return $this->render('Webteamer\Team\TeamModule:teamMatch:live_stat_unavailable.php', array(
                        'event' => $event,
                        'team' => $team,
                        'user' => $user,
            ));
        }
        
        //check if correct stat route
         $addStatRoute = $sportManager->getTeamSportAddStatRoute($team);
        if($addStatRoute != 'team_match_create_live_stat')
        {
            $request->redirect($this->getRouter()->link($addStatRoute,array('event_id' => $event->getId(), 'eventdate' =>  $event->getCurrentDate()->format('Y-m-d'),'lid' =>$existLineup->getId())));
        }
        
        
        $this->appendEventDetailInfo($event, $team,$existLineup);
        $hitEnum = $statManager->getHitTypeEnum();


       

        $members = $teamManager->getTeamPlayers($team->getId());
        $lineupPlayers = $eventManager->getEventLineupPlayers($existLineup);
        $players = array();
        foreach ($members as $member)
        {
            $players[$member->getId()] = $member;
        }

        $lineupPlayersIndexed = array();
        foreach ($lineupPlayers as $lineupPlayer)
        {
            if (null != $lineupPlayer->getTeamPlayerId())
            {

                $lineupPlayer->setTeamPlayer($players[$lineupPlayer->getTeamPlayerId()]);
            }
            $lineupPlayersIndexed[$lineupPlayer->getId()] = $lineupPlayer;
        }

        $eventTimeline = $statManager->getGroupedEventTimeline($event);
        $rawEventTimeline = $statManager->getEventTimeline($event);

        $matchResult = array('first_line' => 0, 'second_line' => 0);
        if(null != $event->getMatchOverview())
        {
            $matchResult['first_line'] = ($event->getMatchOverview()->getFirstLineGoals() == null) ? 0 : $event->getMatchOverview()->getFirstLineGoals() ;
            $matchResult['second_line'] = ($event->getMatchOverview()->getSecondLineGoals() == null) ? 0 : $event->getMatchOverview()->getSecondLineGoals() ;
            $matchResult['winner'] = 'none';
            
            if($matchResult['first_line']  > $matchResult['second_line'])
            {
                 $matchResult['winner'] = 'first_line' ;
            }
            
            if($matchResult['first_line']  < $matchResult['second_line'])
            {
                 $matchResult['winner'] = 'second_line' ;
            }
        }
        
        $stepsManager = \Core\ServiceLayer::getService('StepsManager');
        $finishedProgressStep = $stepsManager->isStepFinished('event_score_manual_create');
        

        

        return $this->render('Webteamer\Team\TeamModule:teamMatch:create_live_stat.php', array(
                    'request' => $request,
                    'team' => $team,
                    'event' => $event,
                    'eventTimeline' => $eventTimeline,
                    'rawEventTimeline' => $rawEventTimeline,
                    'players' => $lineupPlayersIndexed,
                    'lineup' => $existLineup,
                    'hitEnum' => $hitEnum,
                    'matchResult' => $matchResult,
                    'finishedProgressStep' => $finishedProgressStep
        ));
    }

    public function createStatAction()
    {
        $request = ServiceLayer::getService('request');
        $repo = ServiceLayer::getService('TeamRepository');
        $teamManager = ServiceLayer::getService('TeamManager');
        $eventManager = ServiceLayer::getService('EventManager');
        $matchManager = ServiceLayer::getService('PlayerStatManager');

        $event = $eventManager->findEvent($request->get('event_id'));
        $team = $teamManager->findTeamById($event->getTeamId());
        
        
        

        //$eventTeams =  $teamManager->findTeamByEvent($event);
        $players = $teamManager->getTeamPlayers($team->getId());

        if ('POST' == $request->getMethod())
        {
            $post_data = $request->get('team');

            $teamTotal = array();
            foreach ($post_data as $teamKey => $matchTeamData)
            {
                $teamTotal[$teamKey]['goals'] = 0;
                foreach ($matchTeamData['players'] as $playerMatchData)
                {
                    $teamTotal[$teamKey]['goals'] += $playerMatchData['goals'];
                }
            }

            if ($teamTotal[1]['goals'] > $teamTotal[2]['goals'])
            {
                $teamTotal[1]['result'] = 'win';
                $teamTotal[2]['result'] = 'loose';
            }
            elseif ($teamTotal[1]['goals'] < $teamTotal[2]['goals'])
            {
                $teamTotal[1]['result'] = 'loose';
                $teamTotal[2]['result'] = 'win';
            }
            elseif ($teamTotal[1]['goals'] == $teamTotal[2]['goals'])
            {
                $teamTotal[1]['result'] = 'draw';
                $teamTotal[2]['result'] = 'draw';
            }

            $matchId = uniqid() . '-' . time();
            foreach ($post_data as $teamKey => $matchTeamData)
            {
                //first save team
                $teamData = array();
                $teamData['name'] = $matchTeamData['name'];
                $teamData['goals'] = $teamTotal[$teamKey]['goals'];
                $teamData['result'] = $teamTotal[$teamKey]['result'];
                $teamData['match_id'] = $matchId;
                $matchTeamId = $matchManager->saveTeam($teamData);



                /*
                  foreach ($matchTeamData['players'] as $playerId => $playerMatchData)
                  {
                  $playerMatch = new \Webteamer\Player\Model\PlayerMatch();
                  $playerMatch->setEventId($event->getId());
                  $playerMatch->setMatchTeamId($matchTeamId);
                  $playerMatch->setPlayerId($playerId);
                  $playerMatch->setTeamId($event->getTeam()->getId());

                  $playerMatch->setGoals($playerMatchData['goals']);
                  $playerMatch->setAssists($playerMatchData['assist']);
                  $playerMatch->setSaves($playerMatchData['saves']);
                  $mom = (array_key_exists('mom', $playerMatchData) && $playerMatchData['mom'] == '1') ? 1 : '';
                  $playerMatch->setManOfMatch($mom);
                  $playerMatch->setMatchId($matchId);
                  $playerMatch->setMatchDate(new \DateTime());
                  $matchManager->savePlayerMatch($playerMatch);
                  }
                 * 
                 */
            }
        }





        return $this->render('Webteamer\Team\TeamModule:teamMatch:create_stat.php', array(
                    'request' => $request,
                    'players' => $players,
                    'team' => $team
        ));
    }

    public function addTimeLineStatAction()
    {

        $request = ServiceLayer::getService('request');
        $statManager = ServiceLayer::getService('PlayerStatManager');
        $eventManager = ServiceLayer::getService('TeamEventManager');
        $teamManager = ServiceLayer::getService('TeamManager');
        $translator = ServiceLayer::getService('translator');
        $hitEnum = $statManager->getHitTypeEnum();

        $result = '';
        $hitGroup = date('Y-m-d-H-i-s') . '#' . md5(time() . rand(1, 100));
        $teamEvent = $eventManager->findEventById($request->get('event_id'));
        $teamEvent->setCurrentDate(new \DateTime($request->get('event_date')));
        
        
       
        
        
        if (null != $request->get('goal'))
        {
            $goalData = $request->get('goal');

           $teamId = $teamEvent->getTeamId();
           if($teamEvent->getEventType() == 'tournament_game')
           {
               if($goalData['lid_team'] == 'second_line')
               {
                   $teamId = $teamEvent->getOpponentTeamId();
               }
           }     

            $event = new PlayerTimelineEvent();
            $event->setHitGroup($hitGroup);
            $event->setEventId($request->get('event_id'));
            $event->setHitType('goal');
            if (null != $goalData['player_id'] && 0 != $goalData['player_id'])
            {
                $event->setUserId($goalData['player_id']);
            }


            $event->setLineupPlayerId($goalData['lineup_player_id']);
            $event->setEventDate(new \DateTime($request->get('event_date')));
            $event->setHitTime($request->get('time'));
            $event->setLineupId($request->get('lid'));
            $event->setLineupPosition($goalData['lid_team']);
            $event->setTeamId($teamId);
            $hitId = $statManager->saveTimelineEvent($event);
            $event->setId($hitId);

            $existLineup = $eventManager->getLineupById($request->get('lid'));
            $lineupPlayer = $eventManager->getEventLineupPlayer($existLineup, $goalData['lineup_player_id']);

            if (null == $lineupPlayer)
            {
                $lineupPlayerName = '';
            }
            else
            {
                $lineupPlayerName = $lineupPlayer->getPlayerName();
            }
            $event->setPlayerName($lineupPlayerName);
            $result .= ' <span class="player_row_goal" data-hid="' . $hitId . '">G:' . $lineupPlayerName . '</span>';
            
            //send push notify
            $pushData = array('type' => 'goal','lineup' => $existLineup,'scoringTeamId' => $teamId);
            $notificationManager = ServiceLayer::getService('SystemNotificationManager');
            $notificationManager->sendGameEventPushNotify($pushData);
        }

        $timeGroup[] = $event;

        if (null != $request->get('assist'))
        {
            $assistData = $request->get('assist');
            
             $teamId = $teamEvent->getTeamId();
            if($teamEvent->getEventType() == 'tournament_game')
            {
                if($assistData['lid_team'] == 'second_line')
                {
                    $teamId = $teamEvent->getOpponentTeamId();
                }
            }     


            $event = new PlayerTimelineEvent();
            $event->setHitGroup($hitGroup);
            $event->setEventId($request->get('event_id'));
            $event->setHitType('assist');

            if (null != $assistData['player_id'] && 0 != $assistData['player_id'])
            {
                $event->setUserId($assistData['player_id']);
            }

            $event->setLineupPlayerId($assistData['lineup_player_id']);
            $event->setEventDate(new \DateTime($request->get('event_date')));
            $event->setHitTime($request->get('time'));
            $event->setLineupId($request->get('lid'));
            $event->setLineupPosition($assistData['lid_team']);
            $event->setTeamId($teamId);
            $hitId = $statManager->saveTimelineEvent($event);
            $event->setId($hitId);
            $existLineup = $eventManager->getLineupById($request->get('lid'));
            $lineupPlayer = $eventManager->getEventLineupPlayer($existLineup, $assistData['lineup_player_id']);

            if (null == $lineupPlayer)
            {
                $lineupPlayerName = '';
            }
            else
            {
                $lineupPlayerName = $lineupPlayer->getPlayerName();
            }
            $event->setPlayerName($lineupPlayerName);
            $result .= ',<br /> <span class="player_row_assist" data-hid="' . $hitId . '"> A:' . $lineupPlayerName . '</span>';
        }
        $timeGroup[] = $event;


        $lineupPlayers = $eventManager->getEventLineupPlayers($existLineup);
        $members = $teamManager->getTeamPlayers($teamEvent->getTeamId());
        $players = array();
        foreach ($members as $member)
        {
            $players[$member->getId()] = $member;
        }

        $lineupPlayersIndexed = array();
        foreach ($lineupPlayers as $lineupPlayer)
        {
            if (null != $lineupPlayer->getTeamPlayerId())
            {

                $lineupPlayer->setTeamPlayer($players[$lineupPlayer->getTeamPlayerId()]);
            }
            $lineupPlayersIndexed[$lineupPlayer->getId()] = $lineupPlayer;
        }




        ob_start();
        ServiceLayer::getService('layout')->includePart(MODUL_DIR . '/Team/view/teamMatch/_timeline_event_row.php', array(
            'group' => $timeGroup,
            'groupId' => $hitGroup,
            'lineup' => $existLineup,
            'event' => $teamEvent,
            'players' => $lineupPlayersIndexed,
            'groupTime' => $request->get('time')
        ));
        $row = ob_get_contents();
        ob_end_clean();

        $result = $row;

        $layout = ServiceLayer::getService('layout');
        $layout->setTemplate(null);
        $response = new ControllerResponse();
        $response->setStatus('success');
        $response->setContent(json_encode($result));
        $response->setType('json');
        return $response;
    }

    public function editTimeLineStatAction()
    {

        $request = ServiceLayer::getService('request');
        $statManager = ServiceLayer::getService('PlayerStatManager');
        $eventManager = ServiceLayer::getService('TeamEventManager');
        $teamManager = ServiceLayer::getService('TeamManager');
        $translator = ServiceLayer::getService('translator');
        $hitEnum = $statManager->getHitTypeEnum();

        $result = '';

        if (null != $request->get('goal'))
        {
            $goalData = $request->get('goal');


            $teamEvent = $eventManager->findEventById($request->get('event_id'));
            $event = $statManager->getTimelineHitById($goalData['hid']);
            $event->setHitType('goal');
            if (null != $goalData['player_id'] && 0 != $goalData['player_id'])
            {
                $event->setUserId($goalData['player_id']);
            }

            $event->setLineupPlayerId($goalData['lineup_player_id']);
            $event->setLineupId($request->get('lid'));
            $event->setLineupPosition($goalData['lid_team']);
            

            if(null != $request->get('time'))
            {
                $event->setHitTime($request->get('time'));
            }

            
            
            $statManager->saveTimelineEvent($event);

            $existLineup = $eventManager->getLineupById($request->get('lid'));
            $lineupPlayer = $eventManager->getEventLineupPlayer($existLineup, $goalData['lineup_player_id']);

            $result .= '<span class="player_row_goal" data-hid="' . $goalData['hid'] . '">G:' . $lineupPlayer->getPlayerName() . '</span>';
            $event->setPlayerName($lineupPlayer->getPlayerName());

            
            $timeGroup[] = $event;
        }

        if (null != $request->get('assist'))
        {
            $assistData = $request->get('assist');

            $teamEvent = $eventManager->findEventById($request->get('event_id'));
            $event = $statManager->getTimelineHitById($assistData['hid']);
            $event->setHitType('assist');

            if (null != $assistData['player_id'] && 0 != $assistData['player_id'])
            {
                $event->setUserId($assistData['player_id']);
            }
            

            $event->setLineupPlayerId($assistData['lineup_player_id']);
            $event->setLineupId($request->get('lid'));
            $event->setLineupPosition($assistData['lid_team']);
            //$event->setTeamId($teamEvent->getTeamId());
             if(null != $request->get('time'))
            {
                $event->setHitTime($request->get('time'));
            }

            $statManager->saveTimelineEvent($event);

            $existLineup = $eventManager->getLineupById($request->get('lid'));
            $lineupPlayer = $eventManager->getEventLineupPlayer($existLineup, $assistData['lineup_player_id']);

            if (null == $lineupPlayer)
            {
                $lineupPlayerName = '';
            }
            else
            {
                $lineupPlayerName = $lineupPlayer->getPlayerName();
            }

            $result .= '<br /><span class="player_row_assist" data-hid="' . $assistData['hid'] . '">A:' . $lineupPlayerName . '</span>';
            $event->setPlayerName($lineupPlayerName);
            $timeGroup[] = $event;
        }

        $lineupPlayers = $eventManager->getEventLineupPlayers($existLineup);
        $members = $teamManager->getTeamPlayers($teamEvent->getTeamId());
        $players = array();
        foreach ($members as $member)
        {
            $players[$member->getId()] = $member;
        }

        $lineupPlayersIndexed = array();
        foreach ($lineupPlayers as $lineupPlayer)
        {
            if (null != $lineupPlayer->getTeamPlayerId())
            {

                $lineupPlayer->setTeamPlayer($players[$lineupPlayer->getTeamPlayerId()]);
            }
            $lineupPlayersIndexed[$lineupPlayer->getId()] = $lineupPlayer;
        }

        
      
        ob_start();
        ServiceLayer::getService('layout')->includePart(MODUL_DIR . '/Team/view/teamMatch/_timeline_event_row.php', array(
            'group' => $timeGroup,
            'groupId' => $goalData['hid'],
            'lineup' => $existLineup,
            'event' => $teamEvent,
            'players' => $lineupPlayersIndexed,
            'groupTime' => $event->getHitTime()
        ));
        $row = ob_get_contents();
        ob_end_clean();

        $result = $row;





        $layout = ServiceLayer::getService('layout');
        $layout->setTemplate(null);
        $response = new ControllerResponse();
        $response->setStatus('success');
        $response->setContent(json_encode($result));
        $response->setType('json');
        return $response;
    }

    public function editTimeLineTimeAction()
    {
        $request = ServiceLayer::getService('request');
        $repo = ServiceLayer::getService('TeamRepository');
        $teamManager = ServiceLayer::getService('TeamManager');
        $eventManager = ServiceLayer::getService('TeamEventManager');
        $statManager = ServiceLayer::getService('PlayerStatManager');

        $newTime = $request->get('time');
        $hitGroup = $request->get('hg');
        $timelineHits = $statManager->getTimelineHitsByHitGroup($hitGroup);
        foreach ($timelineHits as $timelineHit)
        {
            $team = $teamManager->findTeamById($timelineHit->getTeamId());
            $this->canManageEvents($team);

            $timelineHit->setHitTime($newTime);
            $statManager->saveTimelineEvent($timelineHit);
        }



        $layout = ServiceLayer::getService('layout');
        $layout->setTemplate(null);
        $response = new ControllerResponse();
        $response->setStatus('success');
        $response->setContent(json_encode(array('RESULT' => 'SUCCESS')));
        $response->setType('json');
        return $response;
    }

    public function removeTimeLineStatAction()
    {
        $request = ServiceLayer::getService('request');
        $statManager = ServiceLayer::getService('PlayerStatManager');
        $teamManager = ServiceLayer::getService('TeamManager');
        $hitGroup = $request->get('hg');
        $timelineHits = $statManager->getTimelineHitsByHitGroup($hitGroup);

        foreach ($timelineHits as $timelineHit)
        {
            $team = $teamManager->findTeamById($timelineHit->getTeamId());
            $this->canManageEvents($team);
            $statManager->deleteTimelineEvent($timelineHit);
        }

        $this->getRequest()->redirect();
    }
    
    public function createNextPlayoffEvent($existLineup,$teamEvent)
    {
            $statManager = ServiceLayer::getService('PlayerStatManager');
             $eventManager = ServiceLayer::getService('TeamEventManager');
              $teamManager = ServiceLayer::getService('TeamManager');
               $teamMatchLineupRepository = ServiceLayer::getService('TeamMatchLineupRepository');
            $matchResultData = $statManager->getMatchResult($existLineup);
            $matchResult['first_line'] = ($matchResultData['first_line'] == null) ? 0 :$matchResultData['first_line'];
            $matchResult['second_line'] = ($matchResultData['second_line'] == null) ? 0 : $matchResultData['second_line'];
            $matchResult['winner'] = array();
            $winnerTeamId = null;

            if ($matchResult['first_line'] > $matchResult['second_line'])
            {
                $matchResult['winner'] = 'first_line';
                $winnerTeamId = $teamEvent->getTeamId();
            }

            if ($matchResult['first_line'] < $matchResult['second_line'])
            {
                $matchResult['winner'] = 'second_line';
                $winnerTeamId = $teamEvent->getOpponentTeamId();
            }
            
            
            $firstLineName = $existLineup->getFirstLineName();
            $secondLineName = $existLineup->getSecondLineName();
            $firstLineId = $teamEvent->getTeamId();
            $secondLineId = $teamEvent->getOpponentTeamId();
           
            
            
            if($teamEvent->getTournamentType() == 'playoff')
            {
                //find target event
                $nextEvent = $eventManager->getRepository()->findOneBy(array('tournament_playoff_parent_event1' => $teamEvent->getId() ));
                $nextEventPosition = 'home';
                if(null == $nextEvent )
                {
                     $nextEvent = $eventManager->getRepository()->findOneBy(array('tournament_playoff_parent_event2' => $teamEvent->getId() ));
                     $nextEventPosition = 'opponent';
                     
                }
                $nextEvent->setCurrentDate($nextEvent->getStart());
                $nextLineup = $eventManager->getEventLineup($nextEvent);
                /*
                var_dump($nextEventPosition);
                var_dump($nextEvent);
                var_dump($nextLineup);exit;
                */
                if($nextEventPosition == 'home')
                {
                    if($matchResult['winner'] == 'first_line')
                    {
                        $nextEvent->setTeamId($firstLineId);
                        $nextEvent->setName($firstLineName.':'.$nextLineup->getSecondLineName());
                        $nextLineup->setFirstLineName($firstLineName);
                         $nextLineup->setTeamId($firstLineId);
                    }
                    if($matchResult['winner'] == 'second_line')
                    {
                        $nextEvent->setTeamId($secondLineId);
                        $nextEvent->setName($secondLineName.':'.$nextLineup->getSecondLineName());
                        $nextLineup->setFirstLineName($secondLineName);
                        $nextLineup->setTeamId($secondLineId);
                    }
                }
                
                if($nextEventPosition == 'opponent')
                {
                    if($matchResult['winner'] == 'first_line')
                    {
                        $nextEvent->setOpponentTeamId($firstLineId);
                        $nextEvent->setName($nextLineup->getFirstLineName().':'.$firstLineName);
                        $nextLineup->setSecondLineName($firstLineName);
                        $nextLineup->setOpponentTeamId($firstLineId);
                    }
                    if($matchResult['winner'] == 'second_line')
                    {
                        $nextEvent->setOpponentTeamId($secondLineId);
                        $nextEvent->setName($nextLineup->getFirstLineName().':'.$secondLineName);
                        $nextLineup->setSecondLineName($secondLineName);
                        $nextLineup->setOpponentTeamId($secondLineId);}
                }
                
                $eventManager->saveLineup($nextLineup);
                $eventManager->saveEvent($nextEvent);
                
                //remove old players
                 $teamMatchLineupRepository->getPlayerRepository()->deleteBy(array('lineup_id' => $nextLineup->getId()));
            
                //add new players
                $eventDate = DateTime::createFromFormat('Y-m-d H:i:s', $nextEvent->getStart()->format('Y-m-d').' 00:00:00');
                $team1Players = $teamManager->getActiveTeamPlayers($nextLineup->getTeamId());
                foreach($team1Players as $team1Player)
                {
                    $lineupPlayer = new \Webteamer\Team\Model\TeamMatchLineupPlayer();
                    $lineupPlayer->setEventId($nextEvent->getId());
                    $lineupPlayer->setLineupId($nextLineup->getId());
                    $lineupPlayer->setCreatedAt(new DateTime());
                    $lineupPlayer->setPlayerId($team1Player->getPlayerId());
                    $lineupPlayer->setPlayerName($team1Player->getFullName());
                    $lineupPlayer->setEventDate($eventDate);
                    $lineupPlayer->setLineupPosition('first_line');
                    $lineupPlayer->setLineupName($nextLineup->getFirstLineName());
                    $lineupPlayer->setTeamPlayerId($team1Player->getId());
                    $lineupPlayer->setPlayerPosition('PLAYER');

                    $teamMatchLineupRepository->getPlayerRepository()->save($lineupPlayer);
                }

                $team2Players = $teamManager->getActiveTeamPlayers($nextLineup->getOpponentTeamId());
                foreach($team2Players as $team2Player)
                {
                    $lineupPlayer = new \Webteamer\Team\Model\TeamMatchLineupPlayer();
                    $lineupPlayer->setLineupId($nextLineup->getId());
                     $lineupPlayer->setEventId($nextEvent->getId());
                    $lineupPlayer->setCreatedAt(new DateTime());
                    $lineupPlayer->setPlayerId($team2Player->getPlayerId());
                    $lineupPlayer->setPlayerName($team2Player->getFullName());
                    $lineupPlayer->setEventDate($eventDate);
                    $lineupPlayer->setLineupPosition('second_line');
                    $lineupPlayer->setLineupName($nextLineup->getSecondLineName());
                    $lineupPlayer->setTeamPlayerId($team2Player->getId());
                    $lineupPlayer->setPlayerPosition('PLAYER');
                    $teamMatchLineupRepository->getPlayerRepository()->save($lineupPlayer);
                }
                    
                
                
            }
    }
    
    public function closeMatch($existLineup,$teamEvent)
    {
       $eventManager = ServiceLayer::getService('TeamEventManager');
        $statusBeforeSave = $existLineup->getStatus();
        $existLineup->setStatus('closed');
        $eventManager->saveLineup($existLineup);
        
        
        //playoff
        if($teamEvent->getEventType() == 'tournament_game')
        {
           $this->createNextPlayoffEvent($existLineup,$teamEvent);
        }

        //save stats
        $teamManager = ServiceLayer::getService('TeamManager');
        $playerStatManager = ServiceLayer::getService('PlayerStatManager');
        $seasonManager = ServiceLayer::getService('TeamSeasonManager');

        $team = $teamManager->findTeamById($teamEvent->getTeamId());
        $list = $teamManager->getTeamPlayers($teamEvent->getTeamId());
        
        $currentSeason = $seasonManager->getSeasonById($teamEvent->getSeason());
        $statFrom = new DateTimeEx($currentSeason->getStartDate());
        $statTo = new DateTimeEx($currentSeason->getEndDate());

        $teamPlayersStats = $playerStatManager->getAllTeamPlayersStat($team, null, $statFrom, $statTo);
        foreach ($list as $teamPlayer)
        {
            if (array_key_exists($teamPlayer->getId(), $teamPlayersStats))
            {
                $teamPlayersStats[$teamPlayer->getId()]->setPlayer($teamPlayer);
                $data[$teamPlayer->getId()] = $playerStatManager->toArray($teamPlayersStats[$teamPlayer->getId()]);
            }
        }
         $data = json_encode($data);
         
         
             
         $statHistoryItem = new \CR\Team\Model\TeamStatHistory();
         $statHistoryItem->setCreatedAt(new \DateTime());
         $statHistoryItem->setData($data);
         $statHistoryItem->setMatchId($existLineup->getId());
         $statHistoryItem->setTeamId($teamEvent->getTeamId());
         $statHistoryItem->setMatchDate($existLineup->getEventDate());
         
         
         $statHistoryRepo = ServiceLayer::getService('TeamStatHistoryRepository');
         
         //remove old stats
         $statHistoryRepo->deleteBy(array('match_id' => $existLineup->getId()));
         //save actual
         $statHistoryRepo->save($statHistoryItem);
         

        $teamEvent->setLineup($existLineup);
        if ($statusBeforeSave !== "closed" && $existLineup->getStatus() === "closed")
        {
            ob_start();
            foreach ([
                new AddRateNotificationSender(new AddRateStore($teamEvent)),
                new MatchResultsNotificationSender(new MatchResultsStore($teamEvent))
            ] as $notificationSender)
                $notificationSender->sendAll();
            ob_end_clean();
        }
    }
    
    public function resetTimeLineAction()
    {
        $request = ServiceLayer::getService('request');
        $eventManager = ServiceLayer::getService('TeamEventManager');
        $statManager = ServiceLayer::getService('PlayerStatManager');
        $teamManager = ServiceLayer::getService('TeamManager');
        $existLineup = $eventManager->getLineupById($request->get('lid'));
        $team =$teamManager->findTeamById($existLineup->getTeamId());
        
        $this->canManageEvents($team);
        $statManager->clearTimeline($request->get('lid'));
        
        $existLineup->setStatus(null);
        $eventManager->saveLineup($existLineup);
        $this->getRequest()->redirect();
    }

    public function addTimeLineMomAction()
    {
        $request = ServiceLayer::getService('request');
        $statManager = ServiceLayer::getService('PlayerStatManager');
        $eventManager = ServiceLayer::getService('TeamEventManager');
        $teamManager = ServiceLayer::getService('TeamManager');
        $translator = ServiceLayer::getService('translator');
        $hitEnum = $statManager->getHitTypeEnum();
         $hpWallManager = ServiceLayer::getService('HpWallManager');
        $eventDate = new \DateTime($request->get('event_date'));
        $teamEvent = $eventManager->findEventById($request->get('event_id'));
        $teamEvent->setCurrentDate(DateTimeEx::now()
                        ->dateFrom(new DateTimeEx($eventDate))
                        ->timeFrom(new DateTimeEx($teamEvent->getStart()))
                        ->toDateTime());
        $layout = ServiceLayer::getService('layout');
        $layout->setTemplate(null);
        $response = new ControllerResponse();
        $response->setStatus('success');
       
        $response->setType('json');
        
        $existLineup = $eventManager->getLineupById($request->get('lid'));
        $lineupPlayer = $eventManager->getEventLineupPlayer($existLineup, $request->get('lineup_player_id'));
        
        if('yes' == $request->get('overtime'))
        {
            $existLineup->setOvertime(1);
        }
        else
        {
            $existLineup->setOvertime(2);
        }
        
        
        ServiceLayer::getService('StepsManager')->finishCreateManualScore($teamEvent->getTeamId());
        ServiceLayer::getService('StepsManager')->finishCreateLiveScore($teamEvent->getTeamId());
        $team =$teamManager->findTeamById($teamEvent->getTeamId());
        ServiceLayer::getService('StepsManager')->finishCycle3($team);
        
        //remove old mom
        $statManager->clearTimelineMomEvent($existLineup->getId());
        
        $attendanceList = $eventManager->getEventAttendance($teamEvent);
        $pushNotifyRecipients = array();
        $individualWidgetPlayers = array();
        foreach ($attendanceList as $attendance)
        {
            $teamPlayer = $teamManager->findTeamPlayerById($attendance->getTeamPlayerId());
            if (null != $teamPlayer)
            {
                if($teamPlayer->getPlayerId() > 0)
                {
                    $pushNotifyRecipients[] = $teamPlayer;
                    
                    $individualWidgetPlayers[] = $teamPlayer;
                }
            }
        }
       
         //end match event
        
         $teamId = $teamEvent->getTeamId();
            if($teamEvent->getEventType() == 'tournament_game')
            {
                if($request->get('lid_team')== 'second_line')
                {
                    $teamId = $teamEvent->getOpponentTeamId();
                }
            }     
        
        $endMatchEvent = new PlayerTimelineEvent();
        $endMatchEvent->setEventId($request->get('event_id'));
        $endMatchEvent->setHitType('end');
        $endMatchEvent->setEventDate($eventDate);
        $hitGroup = date('Y-m-d-H-i-s') . '#' . md5(time() . rand(1, 100));

        $endMatchEvent->setHitTime($request->get('time'));
        $endMatchEvent->setHitGroup($hitGroup);
        $endMatchEvent->setLineupId($request->get('lid'));
        $endMatchEvent->setTeamId($teamEvent->getTeamId());
         $endMatchEvent->setLineupPosition('second_line');
        $statManager->saveTimelineEvent($endMatchEvent);
       
        
        if ('none' != $request->get('lid_team'))
        {
            $teamId = $teamEvent->getTeamId();
            if($teamEvent->getEventType() == 'tournament_game')
            {
                if($request->get('lid_team')== 'second_line')
                {
                    $teamId = $teamEvent->getOpponentTeamId();
                }
            }     
            
            $event = new PlayerTimelineEvent();
            $event->setEventId($request->get('event_id'));
            $event->setHitType('mom');
            $event->setUserId($request->get('player_id'));
            $event->setLineupPlayerId($request->get('lineup_player_id'));
            $event->setEventDate($eventDate);
            
            $hitGroup = date('Y-m-d-H-i-s') . '1#' . md5(time() . rand(1, 100));
            
            $event->setHitTime($request->get('time'));
            $event->setHitGroup($hitGroup);
            $event->setLineupId($request->get('lid'));
            $event->setLineupPosition($request->get('lid_team'));
            $event->setTeamId($teamId);
            $statManager->saveTimelineEvent($event);
        }
        else
        {
            $this->closeMatch($existLineup,$teamEvent);
            
            //send notify
            $notificationManager = ServiceLayer::getService('SystemNotificationManager');
            $notificationManager->sendCloseMatchPushNotify($pushNotifyRecipients,array('eventId' => $teamEvent->getId(),'eventDate' => $eventDate->format('Y-m-d')));

             //generate individual posts
            foreach($individualWidgetPlayers as $individualWidgetPlayer)
            {
                $hpWallManager = ServiceLayer::getService('HpWallManager');

                $hpWallManager->generateTeamLadderImgPost($team,$individualWidgetPlayer,$teamEvent);
                $hpWallManager->generatePersonalPerformancePost($individualWidgetPlayer,$team,$teamEvent); 
                $hpWallManager->generateDistanceChangePost($individualWidgetPlayer, $team, $teamEvent);
                
            }
            
            $hpWallManager->generateTeamAfterMatchImgPost($existLineup);
            
            $result = '';
            $response->setContent(json_encode($result));
            return $response;
        }

        //end match
        $this->closeMatch($existLineup,$teamEvent);
        
        //send notify
        $notificationManager = ServiceLayer::getService('SystemNotificationManager');
        $notificationManager->sendCloseMatchPushNotify($pushNotifyRecipients,array('eventId' => $teamEvent->getId(),'eventDate' => $eventDate->format('Y-m-d')));
        
        //generate individual posts
        foreach($individualWidgetPlayers as $individualWidgetPlayer)
        {
           

            $hpWallManager->generateTeamLadderImgPost($team,$individualWidgetPlayer,$teamEvent);
            $hpWallManager->generatePersonalPerformancePost($individualWidgetPlayer,$team,$teamEvent);
            $hpWallManager->generateDistanceChangePost($individualWidgetPlayer, $team, $teamEvent);
        }
        
         $hpWallManager->generateTeamAfterMatchImgPost($existLineup);
        


       

        /*
        $statusBeforeSave = $existLineup->getStatus();
        $existLineup->setStatus('closed');
        $eventManager->saveLineup($existLineup);
        $teamEvent->setLineup($existLineup);
        if ($statusBeforeSave !== "closed" && $existLineup->getStatus() === "closed")
        {
            ob_start();
            foreach ([
                new MatchResultsNotificationSender(new MatchResultsStore($teamEvent))
            ] as $notificationSender)
                $notificationSender->sendAll();
            ob_end_clean();
        }
         * 
         */

        if (null == $lineupPlayer)
        {
            $lineupPlayerName = '';
        }
        else
        {
            $lineupPlayerName = $lineupPlayer->getPlayerName();
        }
        
       
        
        
        $event->setPlayerName($lineupPlayerName);
        $timeGroup[] = $event;
        /*
        $result = array(
            'type' => $request->get('type'),
            'typeName' => $translator->translate($hitEnum['mom']),
            'player' => $lineupPlayerName,
            ''
        );
        */
        
        $lineupPlayers = $eventManager->getEventLineupPlayers($existLineup);
        $members = $teamManager->getTeamPlayers($teamEvent->getTeamId());
        $players = array();
        foreach ($members as $member)
        {
            $players[$member->getId()] = $member;
        }

        $lineupPlayersIndexed = array();
        foreach ($lineupPlayers as $lineupPlayer)
        {
            if (null != $lineupPlayer->getTeamPlayerId())
            {

                $lineupPlayer->setTeamPlayer($players[$lineupPlayer->getTeamPlayerId()]);
            }
            $lineupPlayersIndexed[$lineupPlayer->getId()] = $lineupPlayer;
        }

        ob_start();
        ServiceLayer::getService('layout')->includePart(MODUL_DIR . '/Team/view/teamMatch/_timeline_event_row.php', array(
            'group' => $timeGroup,
            'lineup' => $existLineup,
            'event' => $teamEvent,
            'players' => $lineupPlayersIndexed,
            'groupTime' => $event->getHitTime()
        ));
        $row = ob_get_contents();
        ob_end_clean();


        $response->setContent(json_encode($row));
        return $response;
    }

    public function playerStatAction()
    {
        $request = ServiceLayer::getService('request');
        $translator = ServiceLayer::getService('translator');
        $teamManager = ServiceLayer::getService('TeamManager');
        $playerStatManager = ServiceLayer::getService('PlayerStatManager');
        $seasonManager = ServiceLayer::getService('TeamSeasonManager');
        $teamManager->setTranslator($translator);
        $list = $teamManager->getTeamPlayers($request->get('team_id'));
        $team = $teamManager->findTeamById($request->get('team_id'));
        
        if(!ServiceLayer::getService('TeamCreditManager')->teamHasFeatureAccess($team,'stats'))
        {
            $this->getRequest()->redirect($this->getRouter()->link('order_list'));
        }
        


        $sportManager = ServiceLayer::getService('SportManager');
        $statRoute = $sportManager->getTeamSportStatRoute($team);
       
        if(null == $statRoute)
        {
             return $this->render('Webteamer\Team\TeamModule:teamMatch:season_stat_coming_soon.php', array(
                    'list' => $list,
                    'team' => $team,
            ));
        }

        if ('team_tennis_stat' == $statRoute)
        {
            $request->redirect($this->getRouter()->link($statRoute, array('team_id' => $team->getId())));
        }

        if ('team_volleyball_stat' == $statRoute)
        {
            $request->redirect($this->getRouter()->link($statRoute, array('team_id' => $team->getId())));
        }

        if ('team_basketball_stat' == $statRoute)
        {
            $request->redirect($this->getRouter()->link($statRoute, array('team_id' => $team->getId())));
        }



        ServiceLayer::getService('security')->getIdentity()->checkTeamAccess($team);
        $seasonChoices = $seasonManager->getEventFormChoices($team);

        $actualSeason = $seasonManager->getTeamCurrentSeason($team);
        $seasonId = $actualSeason->getId();
        
        if (null != $request->get('filter'))
        {
            $filterData = $request->get('filter');
            $seasonId = $filterData['season_id'];
        }

        $currentSeason = $seasonManager->getSeasonById($seasonId);
        if(null != $currentSeason)
        {
            $statFrom = new DateTimeEx($currentSeason->getStartDate());
            $statTo = new DateTimeEx($currentSeason->getEndDate());
            $teamPlayersStats = $playerStatManager->getAllTeamPlayersStat($team, null, $statFrom, $statTo);
            
          

           
          
          
            $filterForm = new Form();
            $filterForm->setName('filter');
            $filterForm->setField('season_id', array(
                'type' => 'choice',
                'choices' => $seasonChoices,
                'required' => true,
            ));
            $filterForm->setFieldValue('season_id', $seasonId);



            $maxValues = $teamPlayersStats['maxValues'];
            foreach ($list as $teamPlayer)
            {
                if (array_key_exists($teamPlayer->getId(), $teamPlayersStats))
                {
                    $teamPlayersStats[$teamPlayer->getId()]->setMaxValues($maxValues);
                    $teamPlayer->setStats($teamPlayersStats[$teamPlayer->getId()]);
                }
            }
            
             return $this->render('Webteamer\Team\TeamModule:teamMatch:season_stat.php', array(
                    'list' => $list,
                    'team' => $team,
                    'filterForm' => $filterForm
            ));

        }
        else
        {
              return $this->render('Webteamer\Team\TeamModule:teamMatch:season_stat_unavailable.php', array(
                    'team' => $team,
            ));
        }
        


       
    }

    public function playerStatReactAction()
    {
        $request = ServiceLayer::getService('request');
        $translator = ServiceLayer::getService('translator');
        $teamManager = ServiceLayer::getService('TeamManager');
        $playerStatManager = ServiceLayer::getService('PlayerStatManager');
        $playerManager = ServiceLayer::getService('PlayerManager');

        $teamManager->setTranslator($translator);
        $list = $teamManager->getTeamPlayers($request->get('team_id'));
        $team = $teamManager->findTeamById($request->get('team_id'));
        foreach ($list as $teamPlayer)
        {
            $player = $playerManager->findPlayerById($teamPlayer->getPlayerId());
            if (null != $player)
            {
                $playerStats = $playerStatManager->getTeamPlayerStat($player, $team);
                $teamPlayer->setPlayer($player);
                $teamPlayer->setStats($playerStats);
            }
        }
        $statsMapper = new StatsMapper();
        return $this->render('Webteamer\Team\TeamModule:teamMatch:season_stat_react.php', array(
                    'listJson' => json_encode($statsMapper->createTable($list), true),
                    'list' => $list,
                    'team' => $team
        ));
    }

    public function createTennisStatAction()
    {
        $request = ServiceLayer::getService('request');
        $translator = ServiceLayer::getService('translator');
        $repo = ServiceLayer::getService('TeamRepository');
        $teamManager = ServiceLayer::getService('TeamManager');
        $eventManager = ServiceLayer::getService('TeamEventManager');
        // $matchManager = ServiceLayer::getService('PlayerMatchManager');
        $statManager = ServiceLayer::getService('PlayerStatManager');
        $teamEvent = $eventManager->findEvent($request->get('event_id'));
        $teamEvent->setCurrentDate(new \DateTime($request->get('eventdate')));
        $team = $teamManager->findTeamById($teamEvent->getTeamId());
        $this->canManageEvents($team);
       

        $setMatch = new \Webteamer\Player\Model\SetMatch();
        $eventTimeline = $statManager->getEventTimeline($teamEvent);
        $setPoints = array();
        foreach ($eventTimeline as $timelineItem)
        {
            $setPoints[$timelineItem->getSetPosition()]['points'][$timelineItem->getLineupPlayerId()] = $timelineItem->getPoints();
            $setPoints[$timelineItem->getSetPosition()]['duration'] = $timelineItem->getDurationTime();
        }

        foreach ($setPoints as $setPosition => $setData)
        {
            $setMatch->addSet($setPosition, $setData);
        }

        if (null != $request->get('lid'))
        {
            $existLineup = $eventManager->getLineupById($request->get('lid'));
        }
        else
        {
            $existLineup = $eventManager->getEventLineup($teamEvent);
        }

        $lineupPlayersList = $eventManager->getEventLineupPlayers($existLineup);
        $lineupPlayers = array();
        foreach ($lineupPlayersList as $lineupPlayer)
        {
            $lineupPlayers[$lineupPlayer->getId()] = $lineupPlayer;
        }
         $this->appendEventDetailInfo($teamEvent, $team,$existLineup);
        if ('POST' == $request->getMethod())
        {
            $data = $request->get('set');


            //clear old 
            $statManager->clearEventTimeline($teamEvent);

            foreach ($data as $setId => $setData)
            {
                if (array_sum($setData['points']) > 0)
                {
                    foreach ($setData['points'] as $lineupPlayerId => $points)
                    {
                        $event = new PlayerTimelineEvent();
                        $event->setEventId($request->get('event_id'));
                        $event->setHitType('set');
                        if ($lineupPlayers[$lineupPlayerId]->getPlayerId() > 0)
                        {
                            $event->setUserId($lineupPlayers[$lineupPlayerId]->getPlayerId());
                        }

                        $event->setLineupPlayerId($lineupPlayerId);
                        $event->setEventDate(new \DateTime($request->get('eventdate')));
                        $event->setLineupId($request->get('lid'));
                        $event->setTeamId($teamEvent->getTeamId());
                        $event->setPoints($points);
                        $event->setDurationTime($setData['duration']);
                        $event->setSetPosition($setId);
                        $statManager->saveTimelineEvent($event);
                    }
                }
                
                $existLineup->setStatus('closed');
                $eventManager->saveLineup($existLineup);
            }
            $request->addFlashMessage('team_tennis_stat_success', $translator->translate('TENNIS_STAT_SUCCESS'));
            $request->redirect();
        }

        return $this->render('Webteamer\Team\TeamModule:teamMatch:create_tennis_stat.php', array(
                    'lineupPlayers' => $lineupPlayers,
                    'team' => $team,
                    'event' => $teamEvent,
                    'eventTimeline' => $eventTimeline,
                    'lineup' => $existLineup,
                    'setMatch' => $setMatch
        ));
    }

    public function tennisStatAction()
    {
        $request = ServiceLayer::getService('request');
        $teamManager = ServiceLayer::getService('TeamManager');
        $statManager = ServiceLayer::getService('PlayerStatSetManager');
        $seasonManager = ServiceLayer::getService('TeamSeasonManager');
        $team = $teamManager->findTeamById($request->get('team_id'));
        ServiceLayer::getService('security')->getIdentity()->checkTeamAccess($team);

        $actualSeason = $seasonManager->getTeamCurrentSeason($team);
        $seasonId =$actualSeason->getId();
        $seasonChoices = $seasonManager->getEventFormChoices($team);
        
        if (null != $request->get('filter'))
        {
            $filterData = $request->get('filter');
            $seasonId = $filterData['season_id'];
        }

        $currentSeason = $seasonManager->getSeasonById($seasonId);
        $statFrom = new DateTimeEx($currentSeason->getStartDate());
        $statTo = new DateTimeEx($currentSeason->getEndDate());
        $teamPlayersStats = $statManager->getAllTeamPlayersStat($team, $statFrom, $statTo);

        $filterForm = new Form();
        $filterForm->setName('filter');
        $filterForm->setField('season_id', array(
            'type' => 'choice',
            'choices' => $seasonChoices,
            'required' => true,
        ));
        $filterForm->setFieldValue('season_id', $seasonId);

        //var_dump($teamPlayersStats);exit;

        return $this->render('Webteamer\Team\TeamModule:teamMatch:tennis_stat.php', array(
                    'teamPlayersStats' => $teamPlayersStats,
                    'team' => $team,
                    'filterForm' => $filterForm
        ));
    }
    
    
    public function addSimpleTimeLineStatAction()
    {

        $request = ServiceLayer::getService('request');
        $statManager = ServiceLayer::getService('PlayerStatManager');
        $eventManager = ServiceLayer::getService('TeamEventManager');
        $teamManager = ServiceLayer::getService('TeamManager');
        $translator = ServiceLayer::getService('translator');
        $hitEnum = $statManager->getHitTypeEnum();

       
        $teamEvent = $eventManager->findEventById($request->get('event_id'));
        $teamEvent->setCurrentDate(new \DateTime($request->get('event_date')));
        if (null != $request->get('goal'))
        {
            $goalData = $request->get('goal');
            
             $teamId = $teamEvent->getTeamId();
           if($teamEvent->getEventType() == 'tournament_game')
           {
               if($goalData['lid_team'] == 'second_line')
               {
                   $teamId = $teamEvent->getOpponentTeamId();
               }
           }     
            
            //remove all goals for this player and event
            $statManager->clearPlayerEventHits(array(
                'lineup_id' =>$request->get('lid'),
                'lineup_player_id' =>$goalData['lineup_player_id'],
                'hit_type' =>   'goal'));
            for($i=0; $i < $goalData['count'];$i++ )
            {
                $hitGroup = date('Y-m-d-H-i-s') . '#' . md5(time() . rand(1, 100).$i);
                $event = new PlayerTimelineEvent();
                $event->setHitGroup($hitGroup);
                $event->setEventId($request->get('event_id'));
                $event->setHitType('goal');
                if (null != $goalData['player_id'] && 0 != $goalData['player_id'])
                {
                    $event->setUserId($goalData['player_id']);
                }

                $event->setLineupPlayerId($goalData['lineup_player_id']);
                $event->setEventDate(new \DateTime($request->get('event_date')));
                $event->setHitTime($request->get('time'));
                $event->setLineupId($request->get('lid'));
                $event->setLineupPosition($goalData['lid_team']);
                $event->setTeamId($teamId);
                $hitId = $statManager->saveTimelineEvent($event);
            }
        }

        if (null != $request->get('assist'))
        {
            $assistData = $request->get('assist');
            
             $teamId = $teamEvent->getTeamId();
           if($teamEvent->getEventType() == 'tournament_game')
           {
               if($assistData['lid_team'] == 'second_line')
               {
                   $teamId = $teamEvent->getOpponentTeamId();
               }
           }     
            
             //remove all goals for this player and event
            $statManager->clearPlayerEventHits(array(
                'lineup_id' =>$request->get('lid'),
                'lineup_player_id' =>$assistData['lineup_player_id'],
                'hit_type' =>   'assist'));
            for($i=0; $i < $assistData['count'];$i++ )
            {
                $hitGroup = date('Y-m-d-H-i-s') . '#' . md5(time() . rand(1, 100).$i);
                $event = new PlayerTimelineEvent();
                $event->setHitGroup($hitGroup);
                $event->setEventId($request->get('event_id'));
                $event->setHitType('assist');
                if (null != $assistData['player_id'] && 0 != $assistData['player_id'])
                {
                    $event->setUserId($assistData['player_id']);
                }

                $event->setLineupPlayerId($assistData['lineup_player_id']);
                $event->setEventDate(new \DateTime($request->get('event_date')));
                $event->setHitTime($request->get('time'));
                $event->setLineupId($request->get('lid'));
                $event->setLineupPosition($assistData['lid_team']);
                $event->setTeamId($teamId);
                $hitId = $statManager->saveTimelineEvent($event);
            }
        }
        
        if (null != $request->get('mom'))
        {
            $momData = $request->get('mom');
             //remove all goals for this player and event
            $statManager->getTimelineRepository()->deleteBy(array('lineup_id' =>$request->get('lid'),'hit_type' => 'mom'));
      
            $teamId = $teamEvent->getTeamId();
           if($teamEvent->getEventType() == 'tournament_game')
           {
               if($momData['lid_team'] == 'second_line')
               {
                   $teamId = $teamEvent->getOpponentTeamId();
               }
           }     
            
            
            $hitGroup = date('Y-m-d-H-i-s') . '#' . md5(time() . rand(1, 100).$i);
                $event = new PlayerTimelineEvent();
                $event->setHitGroup($hitGroup);
                $event->setEventId($request->get('event_id'));
                $event->setHitType('mom');
                if (null != $momData['player_id'] && 0 != $momData['player_id'])
                {
                    $event->setUserId($momData['player_id']);
                }

                $event->setLineupPlayerId($momData['lineup_player_id']);
                $event->setEventDate(new \DateTime($request->get('event_date')));
                $event->setHitTime($request->get('time'));
                $event->setLineupId($request->get('lid'));
                $event->setLineupPosition($momData['lid_team']);
                $event->setTeamId($teamId);
                $hitId = $statManager->saveTimelineEvent($event);
        }
        
        if (null != $request->get('close'))
        {
           
            
            $existLineup = $eventManager->getLineupById($request->get('lid'));
            $existLineup->setStatus('closed');
            $eventManager->saveLineup($existLineup);
            
            
            //generate widgets
             //end match
            $this->closeMatch($existLineup,$teamEvent);
            

            $attendanceList = $eventManager->getEventAttendance($teamEvent);
            $pushNotifyRecipients = array();
            $individualWidgetPlayers = array();
            foreach ($attendanceList as $attendance)
            {
                $teamPlayer = $teamManager->findTeamPlayerById($attendance->getTeamPlayerId());
                if (null != $teamPlayer)
                {
                    if($teamPlayer->getPlayerId() > 0)
                    {
                        $pushNotifyRecipients[] = $teamPlayer;
                        $individualWidgetPlayers[] = $teamPlayer;
                    }
                }
            }
       
             $team =$teamManager->findTeamById($teamEvent->getTeamId());

            //send notify
            $notificationManager = ServiceLayer::getService('SystemNotificationManager');
            $notificationManager->sendCloseMatchPushNotify($pushNotifyRecipients,array('eventId' => $teamEvent->getId(),'eventDate' => $request->get('event_date')));

            //generate individual posts
            foreach($individualWidgetPlayers as $individualWidgetPlayer)
            {
                $hpWallManager = ServiceLayer::getService('HpWallManager');

                $hpWallManager->generateTeamLadderImgPost($team,$individualWidgetPlayer,$teamEvent);
                $hpWallManager->generatePersonalPerformancePost($individualWidgetPlayer,$team,$teamEvent);
                $hpWallManager->generateDistanceChangePost($individualWidgetPlayer, $team, $teamEvent);
            }

             $hpWallManager->generateTeamAfterMatchImgPost($existLineup);
        

        }
        
        
       

        exit;
    }

}
?>

