<?php

namespace Webteamer\Team\Controller;

use AclException;
use Core\Controller;
use Core\ControllerResponse;
use Core\Form as Form;
use Core\Notifications\AddRateNotificationSender;
use Core\Notifications\AddRateStore;
use Core\Notifications\MatchResultsNotificationSender;
use Core\Notifications\MatchResultsStore;
use Core\ServiceLayer;
use Core\Types\DateTimeEx;
use DateTime;
use Webteamer\Player\Model\PlayerTimelineEvent;
use function t_dump;

/**
 * @author Marek Hubáček
 * @version 1.0
 */
class TeamMatchStatTestController extends Controller {

    public function historyLadderAction()
    {
        
        $playerStatManager = ServiceLayer::getService('PlayerStatManager');

        $teamId = 1000;
        $matchId = 1839;

        $teamStatHistoryRepo = ServiceLayer::getService('TeamStatHistoryRepository');
        $teamManager = ServiceLayer::getService('TeamManager');
        $stats = $teamStatHistoryRepo->findOneBy(array('match_id' => $matchId));
        $playersStats = json_decode($stats->getData(), true);
        $team = $teamManager->findTeamById($teamId);
        $finalLadder = $playerStatManager->createLadderFromHistoryData($playersStats,$team);
        t_dump($finalLadder);
        
        $this->currentLader();
       
    }

    public function currentLader()
    {
        $playerStatManager = ServiceLayer::getService('PlayerStatManager');
        $teamManager = ServiceLayer::getService('TeamManager');
        $teamId = 1000;
        $team = $teamManager->findTeamById($teamId);
        
        $finalLadder = $playerStatManager->createCurrentLadder($team);
        t_dump($finalLadder);
        
        
        
    }

}
?>

