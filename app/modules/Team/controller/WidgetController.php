<?php
namespace Webteamer\Team\Controller;

use Core\RestController;
use Core\ServiceLayer;

class WidgetController extends RestController 
{
    public function teamPanelAction()
    {
        $teamManager = ServiceLayer::getService('TeamManager');
        $security = ServiceLayer::getService('security');
        $sportManager = ServiceLayer::getService('SportManager');
        $playerStatManager = ServiceLayer::getService('PlayerStatManager');
        $teamPlayerManager = ServiceLayer::getService('TeamPlayerManager');
        $seasonManager = ServiceLayer::getService('TeamSeasonManager');
        $team = $teamManager->findTeamById($this->getRequest()->get('id'));
        $sportEnum = $sportManager->getSportFormChoices();
        $hasStat = ($sportManager->getTeamSportAddStatRoute($team) == 'team_match_create_live_stat') ? true : false;
        $userTeamRoles = $security->getIdentity()->getTeamRoles();
        $playerPermissiomEnum = $teamManager->getPermissionEnum();
        $teamInfo = $teamManager->getTeamsInfo(array($team));
        
        $teamMembersInfo = array();
        $teamMembersInfo['members_count'] = $teamInfo[$team->getId()]['roles']['ADMIN']+$teamInfo[$team->getId()]['roles']['PLAYER'];
        $teamMembersInfo['guest_count'] = $teamInfo[$team->getId()]['roles']['GUEST'];
        $teamMembersInfo['followers'] = $teamInfo[$team->getId()]['followers'];
        
        $user = $security->getIdentity()->getUser();
       

        $currentSeason = $seasonManager->getTeamCurrentSeason($team);

        if(null != $currentSeason)
        {
             $statFrom = new \Core\Types\DateTimeEx($currentSeason->getStartDate());
             $statTo = new  \Core\Types\DateTimeEx($currentSeason->getEndDate());
        }
        else
        {
            $statFrom = new \Core\Types\DateTimeEx();
            $statTo = new \Core\Types\DateTimeEx();
        }
       
       
       
        $teamPlayersStats = $playerStatManager->getAllTeamPlayersStat($team, null,$statFrom,$statTo);
        $teamPlayer = $teamPlayerManager->findTeamPlayerByPlayerId($team->getId(),$user->getId());
        $teamPlayerStat = null;
        //$teamPlayersStats = $playerStatManager->getTeamPlayerStat($teamPlayer,$statFrom,$statTo);
        $graphValues = array();
        if(null!=$teamPlayer &&  array_key_exists($teamPlayer->getId(), $teamPlayersStats))
        {
            $teamPlayerStat = $teamPlayersStats[$teamPlayer->getId()];
            $maxValues = $teamPlayersStats['maxValues'];
            
             $graphValues['crs']['player'] =  round($teamPlayerStat->getAverageCRS(),1);
            $graphValues['crs']['max'] =  round($maxValues['avg-crs'] - $teamPlayerStat->getAverageCRS(),1);

            $graphValues['goal']['player'] =  round($teamPlayerStat->getAverageGoals(),1);
            $graphValues['goal']['max'] = round($maxValues['avg-goal'] - $teamPlayerStat->getAverageGoals(),1);

            $graphValues['assist']['player'] =  round($teamPlayerStat->getAverageAssists(),1);
            $graphValues['assist']['max'] = round($maxValues['avg-assist'] - $teamPlayerStat->getAverageAssists(),1);
        }
        else
        {
             $graphValues['crs']['player'] =  0;
            $graphValues['crs']['max'] =  1;

            $graphValues['goal']['player'] =  0;
            $graphValues['goal']['max'] =  1;

            $graphValues['assist']['player'] =  0;
            $graphValues['assist']['max'] = 1;
        }
        
        $graphValues['crs']['icon'] = 'ico-progress-gr-e';
        $graphValues['crs']['direction'] = 'neutral';
        $graphValues['crs']['lastGames'] = '-';
        $graphValues['goal']['icon'] = 'ico-progress-gr-e';
        $graphValues['goal']['direction'] = 'neutral';
        $graphValues['goal']['lastGames'] = '-';
        $graphValues['assist']['icon'] = 'ico-progress-gr-e';
        $graphValues['assist']['direction'] = 'neutral';
        $graphValues['assist']['lastGames'] = '-';
        
        
        if(null != $teamPlayerStat)
        {
            $teamPlayerStat->setPlayerLastGames($playerStatManager->getTeamPlayerLastGames($teamPlayer));
            $lastGamesCRS = $teamPlayerStat->getLastGamesCRSEfficiency();
            $graphValues['crs']['lastGames'] = $lastGamesCRS;

            if(round($lastGamesCRS,1) <  $graphValues['crs']['player'])
            {
                 $graphValues['crs']['icon'] = 'ico-progress-r-se';
                 $graphValues['crs']['direction'] = 'down';
            }
            elseif(round($lastGamesCRS,1) >  $graphValues['crs']['player'])
            {
                 $graphValues['crs']['icon'] = 'ico-progress-g-ne';
                 $graphValues['crs']['direction'] = 'up';
            }

            $lastGamesGoals = $teamPlayerStat->getLastGamesGoalEfficiency();
            $graphValues['goal']['lastGames'] = $lastGamesGoals;
            if(round($lastGamesGoals,1) <  $graphValues['goal']['player'])
            {
                 $graphValues['goal']['icon'] = 'ico-progress-r-se';
                 $graphValues['goal']['direction'] = 'down';
            }
            elseif(round($lastGamesGoals,1) >  $graphValues['goal']['player'])
            {
                 $graphValues['goal']['icon'] = 'ico-progress-g-ne';
                 $graphValues['goal']['direction'] = 'up';
            }

            
            $lastGamesAssist = $teamPlayerStat->getLastGamesAssistEfficiency();
            $graphValues['assist']['lastGames'] = $lastGamesAssist;
            if(round($lastGamesAssist,1) <  $graphValues['assist']['player'])
            {
                 $graphValues['assist']['icon'] = 'ico-progress-r-se';
                 $graphValues['assist']['direction'] = 'down';
            }
            elseif(round($lastGamesAssist,1) >  $graphValues['assist']['player'])
            {
                 $graphValues['assist']['icon'] = 'ico-progress-g-ne';
                 $graphValues['assist']['direction'] = 'up';
            }

        }
        
       
        
        
        
       $teamEventManager = ServiceLayer::getService('TeamEventManager');
       $eventsList = $teamEventManager->getTeamUpcomingEvents($team);
       $nearestEvent = null;
       foreach($eventsList as $dayEvents)
       {
           if(!empty($dayEvents))
           {
               $nearestEvent = current($dayEvents);
               break;}
       }
       
      
       
        
        
        $html = $this->render('Webteamer\Team\TeamModule:widget:_team_panel.php', array(
                    'team' => $team,
                    'sportEnum' => $sportEnum,
                    'userTeamRoles' => $userTeamRoles,
                    'playerPermissiomEnum' => $playerPermissiomEnum,
                    'teamMembersInfo' => $teamMembersInfo,
                    'graphValues' => $graphValues,
                    'nearestEvent' => $nearestEvent,
                    'hasStat' => $hasStat

                ))->getContent();
        return $this->asHtml($html);
        

    }

}
