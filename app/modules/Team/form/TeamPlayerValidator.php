<?php
namespace Webteamer\Team\Form;
use Core\Validator;

class TeamPlayerValidator  extends Validator
{
    public function validateData()
    {
        parent::validateData();

        $data = $this->getData();

       

        if(isset($data['email']) && null != $data['email'])
        {
            $validEmail = $this->validateEmail($data['email']);
            if(!$validEmail)
            {
                 $this->addErrorMessage('email', 'Unvalid email');
            }
           
        }
      
    }
}

