<?php

namespace Webteamer\Team\Form;

use Core\Form as Form;

/**
 * @author Marek Hubacek
 * @version 1.0
 * @created 21-11-2015 21:22:50
 */
class SettingsForm extends Form {

    protected $fields = array(
     
  
  'sport_id' => 
  array (
    'type' => 'int',
    'max_length' => '11',
    'required' => false,
  ),
 
 
  'age_from' => 
  array (
    'type' => 'int',
    'max_length' => '11',
    'required' => false,
  ),
  'age_to' => 
  array (
    'type' => 'int',
    'max_length' => '11',
    'required' => false,
  ),
  'status' => 
  array (
    'type' => 'choice',
    'max_length' => '50',
    'required' => false,
    'choices' => array()
  ),
  'description' => 
  array (
    'type' => 'string',
    'max_length' => '1000',
    'required' => false,
  ),
  'phone' => 
  array (
    'type' => 'string',
    'max_length' => '50',
    'required' => false,
  ),
  'email' => 
  array (
    'type' => 'string',
    'max_length' => '50',
    'required' => false,
  ),
  
    );

    function __construct()
    {
        
        $this->setField('playgrounds', array(
            'type' => 'choices',
            'choices' => array(),
            'required' => false,
        ));
    }
    
     public function hasPlaygrounds()
    {
        $playgrounds = $this->getFieldValue('playgrounds');
        if(!empty($playgrounds))
        {
            return true;
        }
        
        return false;
    }


}

?>