<?php
namespace  Webteamer\Team\Form;

use Core\Form as Form;
use Core\ServiceLayer;


class AddPlayerForm extends Form 
{
    private $statusChoices;
				
    public function __construct()
    {
        $this->setName('player');
        

        $teamManager = ServiceLayer::getService('TeamManager');
        $playerManager = ServiceLayer::getService('PlayerManager');
        $security = ServiceLayer::getService('security');
        $user = $security->getIdentity()->getUser();
        $this->statusChoices =  $teamManager->getPlayerStatusEnum();
       
        
        $this->setField('player_id', array(
            'type' => 'int',
            'required' => true,
        ));
        
        $this->setField('team_id', array(
            'type' => 'int',
            'required' => true,
        ));
        

        $this->setField('team_role', array(
            'type' => 'choice',
            'choices' =>  $teamManager->getPermissionEnum(),
            'required' => false,
        ));
        $this->setField('level', array(
            'type' => 'choice',
            'choices' =>  $playerManager->getLevelFormChoices(),
            'required' => false,
        ));
        $this->setField('status', array(
            'type' => 'choice',
            'choices' =>  $this->getAvailableStatusChoices(),
            'required' => false,
        ));
        
        $this->setField('first_name', array(
            'type' => 'string',
            'required' => true,
        ));
        
        $this->setField('last_name', array(
            'type' => 'string',
            'required' => false,
        ));
        
        $this->setField('email', array(
            'type' => 'string',
            'required' => false,
        ));
        
          $this->setField('player_number', array(
            'type' => 'int',
              'required' => false,
        ));
        
    }
    
    public function getAvailableStatusChoices()
    {
        $choices = $this->statusChoices;
         unset($choices['waiting-to-confirm']);
        unset($choices['decline']);
        unset($choices['unknown']);
        unset($choices['unconfirmed']);
        return  $choices;
    }
    
    public  function getStatusChoices() {
return $this->statusChoices;
}

public  function setStatusChoices($statusChoices) {
$this->statusChoices = $statusChoices;
}


}