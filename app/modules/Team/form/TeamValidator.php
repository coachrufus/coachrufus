<?php

namespace Webteamer\Team\Form;

use Core\Validator;

class TeamValidator extends Validator {

    public function validateData()
    {
        parent::validateData();

        //validate unique email
        $data = $this->getData();
        if(null != $data['email'])
        {
            $emailValid = $this->validateEmail($data['email']);
            if($emailValid == false)
            {
                 $this->addErrorMessage('email', 'Unvalid email');
            }
        }
        
        if(null != $data['description'])
        {
            if(mb_strlen($data['description']) > 1000)
            {
                $this->addErrorMessage('description', 'Max. 1000 characters');
            }
        }
    }

    public function validateUniqueEmail($repository, $name)
    {
        if (null != $name)
        {
            $exist = $repository->findOneBy(array('name' => $name));

            if (null != $exist)
            {
                $mesage = 'Zadane meno už existuje';
                $this->addErrorMessage('name_unique', $mesage);
            }
        }
    }
    
    public function validatePlayground($playgroundData)
    {
       return true;
        if(null == $playgroundData or empty($playgroundData))
       {
            $mesage = 'No playground selected';
            $this->addErrorMessage('playground_empty', $mesage);
       }
       elseif(empty( $playgroundData[0]['locality_json_data']))
       {
            $mesage = 'No playground selected';
            $this->addErrorMessage('playground_empty', $mesage);
       }
      
    }

}
