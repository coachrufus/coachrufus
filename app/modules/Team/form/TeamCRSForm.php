<?php

namespace CR\Team\Form;

use \Core\Form as Form;

class TeamCRSForm extends Form {

    protected $fields = array(
        'goal' =>
        array(
            'type' => 'int',
            'max_length' => '11',
            'required' => true,
            'value' => 1
        ),
        'assists' =>
        array(
            'type' => 'int',
            'max_length' => '11',
            'required' => true,
             'value' => 1
        ),
        'game_win' =>
        array(
            'type' => 'int',
            'max_length' => '11',
            'required' => true,
             'value' => 1
        ),
        'game_draw' =>
        array(
            'type' => 'int',
            'max_length' => '11',
            'required' => true,
             'value' => 1
        ),
        'man_of_match' =>
        array(
            'type' => 'int',
            'max_length' => '11',
            'required' => true,
             'value' => 1
        ),
        'win_match' =>
        array(
            'type' => 'int',
            'max_length' => '11',
            'required' => true,
             'value' => 1
        ),
        'draw_match' =>
        array(
            'type' => 'int',
            'max_length' => '11',
            'required' => true,
             'value' => 1
        ),
        'win_set' =>
        array(
            'type' => 'int',
            'max_length' => '11',
            'required' => true,
             'value' => 1
        ),
        'draw_set' =>
        array(
            'type' => 'int',
            'max_length' => '11',
            'required' => true,
             'value' => 1
        ),
        'cru' =>
        array(
            'type' => 'int',
            'max_length' => '11',
            'required' => true,
             'value' => 1
        ),
       'points1' =>
        array(
            'type' => 'int',
            'max_length' => '11',
            'required' => true,
            'value' => 1
        ),
       'points3' =>
        array(
            'type' => 'int',
            'max_length' => '11',
            'required' => true,
            'value' => 1
        ),
       'points3' =>
        array(
            'type' => 'int',
            'max_length' => '11',
            'required' => true,
            'value' => 1
        ),
       'shotout' =>
        array(
            'type' => 'int',
            'max_length' => '11',
            'required' => true,
            'value' => 1
        ),
    );

    public function __construct()
    {
        
    }

}
