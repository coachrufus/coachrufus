<?php
namespace  Webteamer\Team\Form;

use Core\Form as Form;
use Core\ServiceLayer;


class ModalEditPlayerForm extends AddPlayerForm 
{
	
				
    public function __construct()
    {
        parent::__construct();
        $this->setName('edit_modal_form');
    }
}