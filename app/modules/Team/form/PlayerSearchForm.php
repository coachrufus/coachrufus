<?php
namespace  Webteamer\Team\Form;

use Core\Form as Form;
use Core\ServiceLayer;


class PlayerSearchForm extends Form 
{
	
				
    public function __construct()
    {
        $this->setName('player_search');

        
        $this->setField('name', array(
            'type' => 'string',
            'max_length' => '255',
            'required' => true,
        ));
        
    }
}