<?php

namespace CR\Team\Form;

use \Core\Form as Form;

class TeamWallPostForm extends Form {

    protected $fields = array(
        'body' =>
        array(
            'type' => 'string',
            'max_length' => '500',
            'required' => true,
        ),
        'team_id' =>
        array(
            'type' => 'int',
            'max_length' => '500',
            'required' => true,
        ),
        'add_content' =>
        array(
            'type' => 'string',
            'max_length' => '5000',
            'required' => false,
        ),
    );

    

}
