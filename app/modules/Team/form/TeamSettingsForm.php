<?php
namespace  Webteamer\Team\Form;
use \Core\Form as Form;
use  Webteamer\Team\Model\TeamSettings as TeamSettings;

class TeamSettingsForm extends Form 
{
	protected $fields = array (
  'id' => 
  array (
    'type' => 'int',
    'max_length' => '11',
    'required' => false,
  ),
  'name' => 
  array (
    'type' => 'string',
    'max_length' => '50',
    'required' => false,
  ),
  'value' => 
  array (
    'type' => 'string',
    'max_length' => '50',
    'required' => false,
  ),
  'settings_group' => 
  array (
    'type' => 'string',
    'max_length' => '50',
    'required' => false,
  ),
  'team_id' => 
  array (
    'type' => 'int',
    'max_length' => '11',
    'required' => false,
  ),
);
				
	public function __construct() 
	{

	}
}