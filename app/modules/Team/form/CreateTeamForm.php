<?php

namespace Webteamer\Team\Form;

use Core\Form as Form;
use Core\ServiceLayer;

class CreateTeamForm extends Form {

    protected $fields = array(
        'age_from' =>
        array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false,
        ),
        'age_to' =>
        array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false,
        ),
        'status' =>
        array(
            'type' => 'choice',
            'max_length' => '50',
            'required' => true,
            'choices' => array()
        ),
        'description' =>
        array(
            'type' => 'string',
            'max_length' => '1000',
            'required' => false,
        ),
        'phone' =>
        array(
            'type' => 'string',
            'max_length' => '50',
            'required' => false,
        ),
        'email' =>
        array(
            'type' => 'string',
            'max_length' => '50',
            'required' => false,
        ),
    );

    public function __construct()
    {
        $this->setName('team');

        $this->setField('name', array(
            'type' => 'string',
            'max_length' => '255',
            'required' => true,
            'label' => 'Team name'
        ));


        $this->setField('sport_id', array(
            'type' => 'choice',
            'choices' => array(),
            'required' => true,
        ));

        $this->setField('exist_playgrounds', array(
            'type' => 'choices',
            'choices' => array(),
            'required' => false,
        ));
        
        

        $this->setField('gender', array(
            'type' => 'choices',
            'choices' => array(),
            'required' => false,
        ));

        $this->setField('age_group', array(
            'type' => 'choices',
            'choices' => array(),
            'required' => false,
        ));


        $this->setField('level', array(
            'type' => 'choices',
            'choices' => array(),
            'required' => false,
        ));



        $this->setField('photo', array(
            'type' => 'string',
            'max_length' => '255',
            'required' => false,
            'label' => 'Team photo'
        ));
        $this->setField('address_city', array(
            'type' => 'string',
            'max_length' => '255',
            'required' => false,
        ));
        $this->setField('address_zip', array(
            'type' => 'string',
            'max_length' => '255',
            'required' => false,
        ));
        $this->setField('address_street', array(
            'type' => 'string',
            'max_length' => '255',
            'required' => false,
        ));
        $this->setField('address_street_num', array(
            'type' => 'string',
            'max_length' => '255',
            'required' => false,
        ));
        $this->setField('full_address', array(
            'type' => 'string',
            'max_length' => '255',
            'required' => false,
            'label' => 'Address'
        ));
        
         $this->setField('school_name', array(
            'type' => 'string',
            'max_length' => '255',
            'required' => false,
            'label' => 'School name'
        ));
         $this->setField('flch_city_name', array(
            'type' => 'string',
            'max_length' => '255',
            'required' => false,
            'label' => 'City'
        ));
         $this->setField('flch_city_name_selector', array(
            'type' => 'string',
            'max_length' => '255',
            'required' => false,
            'label' => 'City'
        ));
    }

    public function hasPlaygrounds()
    {
        $playgrounds = $this->getFieldValue('playgrounds');
        if (!empty($playgrounds))
        {
            return true;
        }

        return false;
    }
    
    public function renderSportSelect($index,$options)
    {
      $translator = ServiceLayer::getService('translator');
        $options_string = '';
		$html_options_string = $this->getFieldOptionString($index,$options);
		$value = $this->getFieldValue($index);
                $field_name =  $this->getFieldName($index);
                
                if(array_key_exists('value',$options))
                {
                    $value = $options['value'];
                }
                
                if(array_key_exists('name',$options))
                {
                    $field_name = $options['name'];
                }
                
                
              
		if(array_key_exists('choices', $this->fields[$index]))
		{
			foreach($this->fields[$index]['choices'] as $key => $name)
			{
				$options_string .= '<optgroup label="'.$translator->translate($key).'">';

                                foreach($name as $key_val => $val)
                                {
                                        if($value == $key_val)
                                        {
                                                $options_string .= '<option class="'.$key.'" selected="selected" value="'.$key_val.'">'.$translator->translate($val).'</option>'."\n";
                                        }
                                        else
                                        {
                                                $options_string .= '<option class="'.$key.'" value="'.$key_val.'">'.$translator->translate($val).'</option>'."\n";
                                        }
                                }
                                $options_string .= '</optgroup>';
                                
			}
		}
		else
		{
			//TODO throw exception
		}
		
		return '<select name="'. $field_name.'"'.$html_options_string.'>'."\n".$options_string.'</select>';

        
        
       
    }

}
