<?php

namespace Webteamer\Team\Form;

use Core\Validator;

class EventValidator extends Validator {

    private $playgroundData;

    public function validateData()
    {
        parent::validateData();

        $data = $this->getData();
        //validate new playground

        if ($data['playground_id'] == 'new')
        {
            $playgroundData = $this->getPlaygroundData();
            if ($playgroundData['name'] == '')
            {
                $this->addErrorMessage('new_playground_name', 'Required field');
            }
            if ($playgroundData['locality_json_data'] == '')
            {
                $this->addErrorMessage('new_playground_locality', 'Mark playground on map');
            }
        }
        
        if($data['period'] != 'none' && $data['period'] != null)
        {
            if($data['end_period_type'] == null)
            {
                 $this->addErrorMessage('end_period_type', 'Required field');
            }
        }
        
        if($data['period'] == 'weekly')
        {
            if(empty($data['repeat_days']))
            {
                 $this->addErrorMessage('repeat_days', 'Select repeat days');
            }
        }
        

    }

    function getPlaygroundData()
    {
        return $this->playgroundData;
    }

    function setPlaygroundData($playgroundData)
    {
        $this->playgroundData = $playgroundData;
    }

}
