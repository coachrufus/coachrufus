<?php

namespace CR\Team\Form;

use \Core\Form as Form;

class TeamWallPostCommentForm extends Form {

    protected $fields = array(
        'body' =>
        array(
            'type' => 'string',
            'max_length' => '300',
            'required' => false,
        ),
    );

}
