<?php
namespace CR\Team\Model;
use Core\Repository as Repository;




class TeamStatHistoryRepository extends Repository
{
   public function findNearestPreviewMatch($date,$teamId)
   {
       $data = $this->getStorage()->findNearestPreviewMatch($date,$teamId);
       return $this->createObjectFromArray($data);
   }
}
