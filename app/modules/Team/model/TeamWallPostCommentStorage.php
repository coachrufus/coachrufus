<?php
namespace CR\Team\Model;
use Core\DbStorage;
class TeamWallPostCommentStorage extends TeamWallPostStorage {
    
	 protected function getBaseQuery()
       {
          $query = 'SELECT p.*, u.name, u.surname, u.photo, count(c.id) as comments_count, lc.count as like_count
                FROM '.$this->getTableName().' p
                LEFT JOIN co_users u ON p.author_id = u.id
                LEFT JOIN  '.$this->getTableName().' c on p.id = c.parent_id 
                LEFT JOIN (select post_id, count(*) as count from team_wall_like group by post_id) lc ON lc.post_id = p.id
                WHERE p.team_id = :team_id
                AND p.parent_id is not null
                ';
           return $query;
       }
      
    
        public function getPostComments($postId,$teamId)
       {
            
          $query = $this->getBaseQuery().' 
                AND p.parent_id = :parent_id
                GROUP by p.id
                ORDER  by p.id asc';
           
           $data = \Core\DbQuery::prepare($query)
                ->bindParam(':team_id', $teamId)
                ->bindParam(':parent_id', $postId)
		->execute()
		->fetchAll(\PDO::FETCH_ASSOC);
		return $data;
                
          
         
       }
       
    
	
}