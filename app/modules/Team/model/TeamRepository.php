<?php

namespace Webteamer\Team\Model;

use Core\DbStorage;
use Core\EntityMapper;
use Core\Repository as Repository;
use Core\ServiceLayer;
use DateTime;
use Webteamer\Locality\Model\Locality;

class TeamRepository extends Repository {

    protected $storage;
    protected $matchStorage;

    public function getMatchStorage()
    {
        return $this->matchStorage;
    }

    public function setMatchStorage($matchStorage)
    {
        $this->matchStorage = $matchStorage;
    }

    public function getTeamPlayerStorage()
    {
        return new DbStorage('team_players');
    }

    public function getTeamPlaygroundAliasStorage()
    {
        return new DbStorage('team_playground_alias');
    }
    
    public function getTeamInvitationStorage()
    {
        return new DbStorage('team_players_invitation');
    }

    public function getTeamPlayerMapper()
    {
        return new EntityMapper('Webteamer\Team\Model\TeamPlayer');
    }

    public function updateTeamPlayerFromArray($id, $data)
    {
        unset($data['id']);
        $this->getTeamPlayerStorage()->update($id, $data);
    }

    public function saveTeamPlayer($object)
    {
        $data = $this->convertToStorageData($object);
        if ($object->getId() == null)
        {
            $id = $this->getTeamPlayerStorage()->create($data);
            $object->setId($id);
        }
        else
        {
            return $this->getTeamPlayerStorage()->update($object->getId(), $data);
        }
    }
    
    
    public function findUserTeams($user)
    {
        if(null != $user)
        {
             $data = $this->getStorage()->findUserTeams($user->getId());
            return $this->createObjectList($data);
        }
       
    }
    /**
     * Find team in which user is waiting for confirmation, this are users, who have been registred throught invitation link
     * @param type $user
     * @return type
     */
    public function findUserUnconfirmedTeams($user)
    {
        $data = $this->getStorage()->findUserUnconfirmedTeams($user->getId());
        return $this->createObjectList($data);
    }
    
    
    public function findUserWaitingTeams($user)
    {
        $data = $this->getStorage()->findUserWaitingTeams($user->getId());
      
        $list = array();
        foreach($data as $d)
        {
            $list[$d['id']]['team'] = $object = $this->factory->createEntityFromArray($d);
            $teamPlayer = new TeamPlayer();
            $teamPlayer->setId($d['team_player_id']);
            $teamPlayer->setCreatedAt(new DateTime($d['team_player_created_at']));
            
            
            $list[$d['id']]['player'] = $teamPlayer;
        }
        return $list;
    }
    
    public function getUserFollowingTeams($user)
    {
        $data = $this->getStorage()->getUserFollowingTeams($user->getId());
        return $this->createObjectList($data);
    }
    
    public function getTeamsInfo($teams)
    {
        $ids = array();
        $list = array();
        foreach($teams as $team)
        {
            $ids[] = $team->getId();
            $list[$team->getId()]['roles']['ADMIN'] = 0;
            $list[$team->getId()]['roles']['PLAYER'] = 0;
            $list[$team->getId()]['roles']['GUEST'] = 0;
            $list[$team->getId()]['roles']['GOAL_KEEPER'] = 0;
        }
       
        if(!empty($ids))
        {
            $data = $this->getStorage()->findTeamsInfo($ids);
            foreach($data as $d)
            {
                if($d['followers'] == null)
                {
                    $d['followers'] = 0;
                }
                $list[$d['id']]['roles'][$d['team_role']] = $d['members_count'];
                $list[$d['id']]['followers'] = $d['followers'];
                $list[$d['id']]['totalMembers'] =  $list[$d['id']]['roles']['ADMIN']+$list[$d['id']]['roles']['PLAYER']+$list[$d['id']]['roles']['GUEST']+$list[$d['id']]['roles']['GOAL_KEEPER'];
                
                //$list[$d['id']] = $teamInfo['ADMIN']+$teamInfo['PLAYER']+$teamInfo['GUEST']+$teamInfo['GOAL_KEEPER'];
            }
        }
        
        

        
        return $list;
    }

    public function findTeamPlayers($team_id)
    {
        $data = $this->getStorage()->findTeamPlayers($team_id);
        $entityMapper = $this->getTeamPlayerMapper();
        $list = array();
        foreach ($data as $d)
        {
            $object = $entityMapper->createEntityFromArray($d);
            $list[] = $object;
        }
        return $list;
    }
    
     public function findActiveTeamPlayers($team_id)
    {
        $data = $this->getStorage()->findActiveTeamPlayers($team_id);
        $entityMapper = $this->getTeamPlayerMapper();
        $list = array();
        foreach ($data as $d)
        {
            $object = $entityMapper->createEntityFromArray($d);
            $list[] = $object;
        }
        return $list;
    }
    
    
    public function findAllTeamPlayers($team_id,$options = array())
    {
        $data = $this->getStorage()->findAllTeamPlayers($team_id,$options);
        $entityMapper = $this->getTeamPlayerMapper();
        $list = array();
        foreach ($data as $d)
        {
            $object = $entityMapper->createEntityFromArray($d);
            $list[] = $object;
        }
        return $list;
    }
    
    public function findTeamAdmins($team_id)
    {
        $data = $this->getStorage()->findTeamAdmins($team_id);
        $entityMapper = $this->getTeamPlayerMapper();
        $list = array();
        foreach ($data as $d)
        {
            $object = $entityMapper->createEntityFromArray($d);
            $list[] = $object;
        }
        return $list;
    }
    
    public function deleteTeamPlayer($player)
    {
        $this->getTeamPlayerStorage()->delete($player->getId());
    }
    
     public function findTeamPlayerById($id)
    {
        $data = $this->getTeamPlayerStorage()->find($id);
        $entityMapper = $this->getTeamPlayerMapper();
        $object = $entityMapper->createEntityFromArray($data);
        return $object;
    }
    
     public function findTeamPlayerByEmail($teamId,$email)
    {
        $data = $this->getTeamPlayerStorage()->findOneBy(array('team_id' =>$teamId,'email' => $email ));
        $entityMapper = $this->getTeamPlayerMapper();
        $object = $entityMapper->createEntityFromArray($data);
        return $object;
    }
    
     public function findTeamPlayerByPlayerId($teamId,$playerId)
    {
        $data = $this->getTeamPlayerStorage()->findOneBy(array('team_id' =>$teamId,'player_id' => $playerId ));
        $entityMapper = $this->getTeamPlayerMapper();
        $object = $entityMapper->createEntityFromArray($data);
        return $object;
    }
    
    
 

    public function saveTeamPlaygroundAlias($team_id, $alias_id)
    {
        $storage = $this->getTeamPlaygroundAliasStorage();
        $aliasData = array();
        $aliasData['team_id'] = $team_id;
        $aliasData['alias_id'] = $alias_id;
        $storage->create($aliasData);
    }

    public function deleteTeamPlaygroundAlias($team)
    {
        $storage = $this->getTeamPlaygroundAliasStorage();
        $storage->deleteByParams(array('team_id' => $team->getId()));
    }

    public function findTeamsByPlayground($playground)
    {
        $data = $this->getStorage()->findTeamsByPlayground($playground->getId());
        return $this->createObjectList($data);
    }
    
    public function findTeamsByArea($center, $distance)
    {
        $data = $this->getStorage()->findTeamsByArea($center, $distance);
        return $this->createObjectList($data);
    }
    
    public function findTeamsByLocality($locality)
    {
        $data = $this->getStorage()->findTeamsByLocality($locality->getId());
        return $this->createObjectList($data);
    }

    public function getStorage()
    {
        return $this->storage;
    }
    
    /**
     * 
     * @param newVal
     */
    public function setStorage($newVal)
    {
        $this->storage = $newVal;
    }
    
    public function saveTeamPlayerInvitation($data)
    {
        $storage = $this->getTeamInvitationStorage();
        $storage->create($data);
    }
    
     public function fulltextNameSearch($phrase)
    {
        $list = $this->getStorage()->fulltextNameSearch($phrase);
        
        return $this->createObjectList($list);
    }

}
