<?php
namespace CR\Team\Model;
use Core\Repository as Repository;

class TeamWallPostRepository extends Repository
{
    public function getPostLikeStorage()
    {
        return new \Core\DbStorage('team_wall_like');
    }
    
    public function buildPost($data)
    {
        $object = $this->factory->createEntityFromArray($data); 
        $player = new \Webteamer\Player\Model\Player();
        $player->setName($data['name']);
        $player->setSurname($data['surname']);
        $player->setPhoto($data['photo']);
        $object->setAuthor($player);
        $object->setCommentsCount($data['comments_count']);
        $object->setLikeCount($data['like_count']);
        return $object;
    }
    
    public function getLastWallPosts($team,$limit = 5)
    {
        $data = $this->getStorage()->getTeamLastWallPosts($team->getId(),$limit);
        
        
        
        
        $list = array();
        foreach ($data as $d)
        { 
            $list[] = $this->buildPost($d);
        }
        
        $wallPostsLikesInfo = $this->getPostLikesInfo($list);
        
        foreach($list as $post)
        {
            if(array_key_exists($post->getId(), $wallPostsLikesInfo))
            {
                $post->setLikeInfo($wallPostsLikesInfo[$post->getId()]);
            }
            
        }

        return $list;
    }
    
    
    public function getWallPosts($team,$start,$limit = 5)
    {
        $data = $this->getStorage()->getTeamWallPosts($team->getId(),$start,$limit);
        
        $list = array();
        foreach ($data as $d)
        {
            $list[] = $this->buildPost($d);
        }
        return $list;
    }
    
    public function getHashtagWallPosts($team,$hashtag,$start)
    {
        $data = $this->getStorage()->getTeamHashtagWallPosts($team->getId(),$hashtag,$start);
        
        $list = array();
        foreach ($data as $d)
        {
            $list[] = $this->buildPost($d);
        }
        return $list;
    }
    
    public function savePostLike($entity)
    {
        $data = $this->convertToStorageData($entity);
        return $this->getPostLikeStorage()->create($data);
        
    }
    public function deleteUserPostLike($postId,$userId)
    {
        return $this->getPostLikeStorage()->deleteByParams(array('post_id' => $postId,'user_id' => $userId));
    }
    
    public function getPostLikesCount($entity)
    {
        $data = $this->getStorage()->getPostLikesCount($entity->getId());
        return $data['likes_count'];
    }
    
    public function getPostLikesInfo($posts)
    {
         $ids = array();
         foreach($posts as $post)
         {
             $ids[] = $post->getId();
         }
         
         if(empty($ids))
         {
             return array();
         }
        
        $data = $this->getStorage()->getPostLikesInfo($ids);
        
        $likes = array();
        foreach($data as $d)
        {
            $likes[$d['post_id']]['user_ids'][] = $d['user_id'];
        }
         return $likes;
    }
}
