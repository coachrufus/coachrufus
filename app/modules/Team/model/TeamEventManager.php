<?php

namespace Webteamer\Team\Model;

use Core\Manager;
use DateTime;
use Webteamer\Event\Model\EventAttendance;
use Webteamer\Event\Model\EventCosts;

/**
 * 
 * @author Marek Hubacek
 * @version 1.0
 * @created 18-12-2015 11:11:39
 * 
 * 
 */
class TeamEventManager extends Manager {

    private $attendanceRepository;
    private $costsRepository;
    private $lineupRepository;
    private $translator;

    /**
     * 
     * @param type $repository Webteamer\Event\Model\EventRepository
     * @param type $attendaceRepo Webteamer\Event\Model\EventAttendanceRepository
     * @param type $lineupRepository Webteamer\Team\Model\TeamMatchLineupRepository
     * @param type $translator
     */
    public function __construct($repository = null, $attendaceRepo = null, $costsRepository = null, $lineupRepository = null,$translator)
    {
        if (null != $repository)
        {
            $this->setRepository($repository);
        }
        if (null != $attendaceRepo)
        {
            $this->setAttendanceRepository($attendaceRepo);
        }
        if (null != $costsRepository)
        {
            $this->setCostsRepository($costsRepository);
        }
        if (null != $lineupRepository)
        {
            $this->setLineupRepository($lineupRepository);
        }
        $this->setTranslator($translator);
    }
    
    public  function getTranslator() {
return $this->translator;
}

public  function setTranslator($translator) {
$this->translator = $translator;
}



    public function getLineupRepository()
    {
        return $this->lineupRepository;
    }

    public function setLineupRepository($lineupRepository)
    {
        $this->lineupRepository = $lineupRepository;
    }

    /**
     * 
     * @return type "Webteamer\Event\Model\EventAttendanceRepository
     */
    public function getAttendanceRepository()
    {
        return $this->attendanceRepository;
    }

    public function setAttendanceRepository($attendanceRepository)
    {
        $this->attendanceRepository = $attendanceRepository;
    }
    
    public function getCostsRepository()
    {
        return $this->costsRepository;
    }

    public function setCostsRepository($costsRepository)
    {
        $this->costsRepository = $costsRepository;
    }
    
    public function getEventTypeEnum()
    {
        $translator = \Core\ServiceLayer::getService('translator');
        $enum = array(
            'training' => $translator->translate('Training'),
            'game' => $translator->translate('Game'),
            'race' => $translator->translate('Race'),
            'competition' => $translator->translate('Competition'),
             'social' => $translator->translate('Social event type'));
        return $enum;
    }
    
    public function deleteEvent($event)
    {
        $this->getRepository()->delete($event->getId());
    }

    public function findEvent($eventId)
    {
        return $this->getRepository()->find($eventId);
    }

    /**
     * Create team event from data
     * 
     * @param data
     */
    public function createTeamEvent(array $data)
    {
        $validateResult = $this->getRepository()->validateEntityData($data);
        if (true !== $validateResult)
        {
            var_dump($validateResult);
            throw new \Exception('Unvalid data');
        }

        $entity = $this->createTeamEventFromArray($data);
        $result = $this->getRepository()->save($entity);
        return $result;
    }

    public function createTeamEventFromArray($data)
    {
        $entity = $this->getRepository()->createObjectFromArray($data);
        $entity->setCreatedAt(new DateTime());
        if (!array_key_exists('start', $data))
        {
            $entity->setStart(new DateTime());
        }


        return $entity;
    }

    public function createTeamEventAttendanceFromArray($data)
    {
        $entity = $this->getAttendanceRepository()->createObjectFromArray($data);
        $entity->setCreatedAt(new DateTime());
        return $entity;
    }

    /**
     * 
     * @param data
     */
    public function createTeamEventAttendance(array $data)
    {
        $validateResult = $this->getAttendanceRepository()->validateEntityData($data);
        var_dump($validateResult);
        if (true !== $validateResult)
        {
            var_dump($validateResult);
            throw new \Exception('Unvalid data');
        }

        $entity = $this->createTeamEventAttendanceFromArray($data);
        $result = $this->getAttendanceRepository()->save($entity);
        return $result;
    }

    public function getTeamUpcomingEvents($team)
    {
        $listTo = new \DateTime(date('Y-m-d'));
        $listTo->setTimestamp(strtotime("+ 30 day"));
        $events = $this->findEvents($team, array('from' => new \DateTime(), 'to' => $listTo));

        
        $types = $this->getEventTypeEnum();
        foreach($events as $event)
        {
            $event->setTypeName($types[$event->getEventType()]);
        }

        $eventsList = $this->buildTeamEventList($events, new \DateTime(date('Y-m-d')), $listTo);
        return $eventsList;
    }
    
    public function getTeamEvents($team, $filterCriteria = array())
    {
        $criteria = array();
        if(null != $filterCriteria)
        {
            if(array_key_exists('from', $filterCriteria))
            {
                $criteria['from'] =  DateTime::createFromFormat('d.m.Y h:i:s', $filterCriteria['from'].' 00:00:00');
            }

            if(array_key_exists('to', $filterCriteria))
            {
                $criteria['to'] =  DateTime::createFromFormat('d.m.Y h:i:s', $filterCriteria['to'].' 00:00:00');
            }

            if(array_key_exists('event_type', $filterCriteria) && '' != $filterCriteria['event_type'])
            {
                $criteria['event_type'] =  $filterCriteria['event_type'];
            }
        
        }
         $events = $this->findEvents($team,$criteria);
         
        $eventsList = $this->buildTeamEventList($events, $criteria['from'], $criteria['to']);
        return $eventsList;
    }
    
    public function getBeepEvents($days = 5)
    {
        $listTo = new \DateTime(date('Y-m-d'));
        $listTo->setTimestamp(strtotime("+ {$days} day"));
        $events = $this->getRepository()->findBeepEvents(array('from' => new \DateTime(), 'to' => $listTo));
        $eventsList = $this->buildTeamEventList($events, new \DateTime(date('Y-m-d')), $listTo);
        return $eventsList;
    }
    
    public function getDateNotifyEvents()
    {
        $listTo = new \DateTime(date('Y-m-d'));
        $listTo->setTimestamp(strtotime("+ 5 day"));
        $events = $this->getRepository()->findDateNotifyEvents(array('from' => new \DateTime(), 'to' => $listTo));
        //$eventsList = $this->buildTeamEventList($events, new \DateTime(date('Y-m-d')), $listTo);
        return $events;
    }
    
    public function findUpcomingEvents( $days)
    {
        $from = new \DateTime();
        $to = new \DateTime();
        $to->modify('+'.$days.' day');

        $events = $this->getRepository()->findUpcomingEvents(array('from' => $from, 'to' => $to));
        $eventsList = $this->buildTeamEventList($events, $from, $to);
        return $eventsList;
    }

    public function findEvents($team, $criteria = array())
    {
        if(null == $team)
        {
            return array();
        }
        $events = $this->getRepository()->findTeamEvents($team, $criteria);
        return $events;
    }

    public function findEventById($event_id)
    {
        $event = $this->getRepository()->find($event_id);
        return $event;
    }

    public function buildTeamEventList($events, $from, $to)
    {
        $list = array();
        $diffDays = $from->diff($to)->days;

        //loob for every day
        for ($i = 0; $i <= $diffDays; $i++)
        {
            $currentDate = new DateTime();
            $currentDate->setTimestamp(strtotime("+" . $i . " day", $from->getTimestamp()));
            //$list[$currentDate->format('Y-m-d')] = array();
            //loop for every event

            foreach ($events as $event)
            {
                $dayEvent = clone $event;
                $nearestTermin = $dayEvent->getNearestTermin($currentDate);
                if (null != $nearestTermin && $nearestTermin->format('Y-m-d') == $currentDate->format('Y-m-d'))
                {

                    $eventCurrentDate = new DateTime($currentDate->format('Y-m-d').' '.$event->getStart()->format('H:i:s'));
                    $dayEvent->setCurrentDate($eventCurrentDate);
                    
                    $attendance = $this->getEventAttendance($dayEvent);
                    $dayEvent->setAttendance($attendance);
                    
                    
                    $list[$currentDate->format('Y-m-d')][$event->getId()] = $dayEvent;
                    /*
                      $tmp = $this->convertEntityToArray($event);
                      $tmp['current_date'] = $currentDate;
                      $list[$currentDate->format('Y-m-d')][$event->getId()] =$tmp;
                     * 
                     */
                }
            }
        }
        //exit; 
        //var_dump($list);
        return $list;
    }
    
    public function updateEventCosts($eventId, $eventDate, $costs)
    {
        $repo = $this->getCostsRepository();
        $costsObj = $repo->findOneBy(array('event_id' => $eventId, 'event_date' => $eventDate));
        if ($costsObj == null)
        {
            $costsObj = new EventCosts();
            $costsObj->setEventId($eventId);
            $costsObj->setEventDate(new DateTime($eventDate));
        }
        
        $costsObj->setCosts($costs);
        
        $repo->save($costsObj);
    }

    public function createEventAttendance($teamPlayer, $event, $eventDate, $status)
    {
        $repo = $this->getAttendanceRepository();
        $exist = $repo->findOneBy(array('team_player_id' => $teamPlayer->getId(),'participant_id' => $teamPlayer->getPlayerId(), 'event_id' => $event->getId(), 'event_date' => $eventDate));
        if (null == $exist)
        {
            $eventAttendance = new EventAttendance();
            $eventAttendance->setParticipantId($teamPlayer->getPlayerId());
            $eventAttendance->setTeamPlayerId($teamPlayer->getId());
            $eventAttendance->setEventId($event->getId());
            $eventAttendance->setParticipantType('player');
            $eventAttendance->setStatus($status);
            $eventAttendance->setEventDate(new DateTime($eventDate));
            $eventAttendance->setCreatedAt(new DateTime());
            $repo->save($eventAttendance);
        }
    }
    
    public function updateEventAttendancePlayerFee($teamPlayerId, $eventId, $eventDate, $fee)
    {
        $repo = $this->getAttendanceRepository();
        $exist = $repo->findOneBy(array('team_player_id' => $teamPlayerId, 'event_id' => $eventId, 'event_date' => $eventDate));
        $exist->setFee($fee);
        $repo->save($exist);
    }

    public function deleteEventAttendance($teamPlayer, $event, $eventDate)
    {


        $this->getAttendanceRepository()->deleteByParams(array('team_player_id' => $teamPlayer->getId(), 'event_id' => $event->getId(), 'event_date' => $eventDate));

        //ServiceLayer::getService('EventAttendanceRepository')
    }
    
    public function getTeamPlayerEventAttendance($teamPlayer, $event, $eventDate)
    {
        $attendence = $this->getAttendanceRepository()->findOneBy(array('team_player_id' => $teamPlayer->getId(), 'event_id' => $event->getId(), 'event_date' => $eventDate));
        return $attendence;
        //ServiceLayer::getService('EventAttendanceRepository')
    }

    public function getEventAttendance($event)
    {
        $list = $this->getAttendanceRepository()->findBy(array('event_id' => $event->getId(), 'event_date' => $event->getCurrentDate()->format('Y-m-d 00:00:00')));
        return $list;
    }
    
    public function getEventCosts($event)
    {
        $list = $this->getCostsRepository()->findOneBy(array('event_id' => $event->getId(), 'event_date' => $event->getCurrentDate()->format('Y-m-d 00:00:00')));
        return $list;
    }

    public function getPlayersAttendanceEventMatrix($players = array())
    {
        $list = $this->getPlayersAttendance($players);
        $matrix = array();
        foreach ($list as $atttendance)
        {
            //$matrix[$atttendance->getEventId()][] = array('participant_id' => $atttendance->getParticipantId(),'comment' => $atttendance->getComment());
            //$matrix[$atttendance->getEventId()]['participant_ids'][$atttendance->getId()] = $atttendance->getParticipantId();
            //$matrix[$atttendance->getEventId()]['attendance'][$atttendance->getId()] = $atttendance;
            //$matrix[$atttendance->getEventId()]['comments'][] = $atttendance->getParticipantId();
            $matrix[$atttendance->getEventId()][$atttendance->getParticipantId()] = $atttendance;
        }

        return $matrix;
    }
    

    public function getPlayersAttendance($players = array())
    {
        $attendance = array();
        $playerIds = array();
        foreach ($players as $player)
        {
            $playerIds[] = $player->getId();
        }

        $list = $this->getAttendanceRepository()->findByParticipants($playerIds);
        return $list;
    }
    
    public function getPlayerEventsAttendanceMatrix($player,$events)
    {
        $attendance = array();
        $list = $this->getAttendanceRepository()->getByParticipantAndEvents($player,$events);
        $matrix = array();
        foreach($list as $attend)
        {
            $matrix[$attend->getEventId()][$attend->getEventDate()->format('Y-m-d')] = $attend->getStatus();
        }
        
        
        
        return $matrix;
    }
    
    public function getEventsAttendance($events)
    {
        $attendance = array();
        $eventIds = array();
        foreach ($players as $player)
        {
            $playerIds[] = $player->getId();
        }

        $list = $this->getAttendanceRepository()->findByParticipants($playerIds);
        return $list;
    }
    
    public function getAttendanceBillingMatrix($players, $eventsList, $user)
    {
        $matrix = array();
        $guestMatrix = array();
        $playersMatrix = array();
        $eventMatrix = array();
        
        
        $eventsIds = array();
        foreach($eventsList as $dayEvents)
        {
            foreach($dayEvents as $event)
            {
                $eventsIds[$event->getId()] = $event->getId();
            }
        }
        
        
        //$existAttendance = $this->getPlayersAttendance($players);
        $existAttendancePlayerIndexed = array();
        $existAttendanceEventIndexed = array();
        
        
        $existAttendance = $this->getAttendanceRepository()->findByEvents($eventsIds);
        
       
        
        foreach($existAttendance as $att)
        {
            $existAttendancePlayerIndexed[$att->getTeamPlayerId()][$att->getEventId()][$att->getEventDate()->format('Y-m-d')] = $att;
            $existAttendanceEventIndexed[$att->getEventId()][$att->getEventDate()->format('Y-m-d')] = $att;
        }
//t_dump($existAttendanceEventIndexed);
         //$attendance = $teamEventManager->getEventAttendance($event);

        foreach ($players as $player)
        {
            $playerAttendances = array();
           
            if(array_key_exists($player->getId(), $existAttendancePlayerIndexed))
            {
                $playerAttendances = $existAttendancePlayerIndexed[$player->getId()];
            }
            
            $playerIndex = $player->getId();
            $playersMatrix[$playerIndex]['player'] = $player->getFullName();
            $playersMatrix[$playerIndex]['player_id'] = $player->getPlayerId();
            foreach ($eventsList as $dayEvents)
            {
                foreach($dayEvents as $event)
                {
                    $status = 'NO';
                    $fee = null;
                    $comment = '';
                    $editable = false;
                    $createdAt = null;
                    $index = $event->getId().'-'.$event->getCurrentDate()->format('Y-m-d');
                    //$existAttendance = $this->getEventAttendance($event); // attendance for one day
                    
                    //t_dump($event->getId().' in '.implode(',',array_keys($existAttendanceEventIndexed)));
                    
                    if(array_key_exists($event->getId(), $playerAttendances) && array_key_exists($event->getCurrentDate()->format('Y-m-d'), $playerAttendances[$event->getId()]))
                    {
                        $eventAttendance = $playerAttendances[$event->getId()][$event->getCurrentDate()->format('Y-m-d')];
                        if(null != $eventAttendance)
                        {
                            if($eventAttendance->getTeamPlayerId() == $player->getId())
                            {
                                $status = $eventAttendance->getStatus();
                                $fee = $eventAttendance->getFee();
                            }
                        }
                    }
                   
                    if($user->getId() == $player->getPlayerId())
                    {
                         $editable = true;
                    }
                    
                    $playersMatrix[$playerIndex]['events'][$index]['team_player_id'] = $player->getId();
                    $playersMatrix[$playerIndex]['events'][$index]['event'] = $event;
                    $playersMatrix[$playerIndex]['events'][$index]['status'] = $status;
                    $playersMatrix[$playerIndex]['events'][$index]['fee'] = $fee;
                    $playersMatrix[$playerIndex]['events'][$index]['editable'] = $editable;
                    $playersMatrix[$playerIndex]['events'][$index]['comment'] = $comment;
                    $playersMatrix[$playerIndex]['events'][$index]['created_at'] = $createdAt;
                    $playersMatrix[$playerIndex]['events'][$index]['event_date'] = $event->getFormatedCurrentDate();
                }
            }
        }
       
        
        foreach ($eventsList as $dayIndex => $dayEvents)
        {
            foreach($dayEvents as $event)
            {
                $eventIndex = $dayIndex.'-'.$event->getId();
                $eventMatrix[$eventIndex]['event'] = $event;
                $eventMatrix[$eventIndex]['event_date'] = $event->getFormatedCurrentDate();
                $eventMatrix[$eventIndex]['costs'] = 0;
                
                $eventCosts = $this->getEventCosts($event);
                
                if ($eventCosts != null)
                {
                    $eventMatrix[$eventIndex]['costs'] = $eventCosts->getCosts();
                }
                
                
                $eventPlayers = array();
                //$existAttendance = $this->getEventAttendance($event);
               if(array_key_exists($event->getId(), $existAttendanceEventIndexed) && array_key_exists($event->getCurrentDate()->format('Y-m-d'), $existAttendanceEventIndexed[$event->getId()]))
               {
                   
                   $existAttendance = $existAttendanceEventIndexed[$event->getId()][$event->getCurrentDate()->format('Y-m-d')];
                    foreach($players as $player)
                    {
                        $playerIndex = $player->getId();
                        $status = 'NO';
                        $comment = '';
                        $editable = false;
                        $createdAt = null;

                       if(null !=$existAttendance && $existAttendance->getTeamPlayerId() == $player->getId())
                           {
                               $status = $existAttendance->getStatus();
                           }

                        if($user->getId() == $player->getPlayerId())
                        {
                             $editable = true;
                        }

                         $eventPlayers[$playerIndex]['player'] =  $player->getFullName();
                         $eventPlayers[$playerIndex]['player_id'] = $player->getPlayerId();
                         $eventPlayers[$playerIndex]['team_player_id'] = $player->getId();
                         $eventPlayers[$playerIndex]['status'] = $status;
                         $eventPlayers[$playerIndex]['editable'] = $editable;
                         $eventPlayers[$playerIndex]['comment'] = $comment;
                         $eventPlayers[$playerIndex]['created_at'] = $createdAt;
                         $eventPlayers[$playerIndex]['team_role'] = $player->getTeamRole();
                    }
                
               }
                $eventMatrix[$eventIndex]['players'] = $eventPlayers; 
            }
        }

        return array('players' => $playersMatrix,'eventMatrix' => $eventMatrix);
    }
    
    

    /**
     * Return attendance matrix for upcomig events, with status, comment, editable flag, event date for every event and player 
     * @param type $players
     * @param type $eventsList
     * @param type $user
     * @return type
     */
    public function getUpcomingEventsAttendanceMatrix($players, $eventsList, $user)
    {
        $matrix = array();
        $guestMatrix = array();
        $playersMatrix = array();
        $eventMatrix = array();
        //$existAttendance = $this->getPlayersAttendanceEventMatrix($players);
         //$attendance = $teamEventManager->getEventAttendance($event);

        foreach ($players as $player)
        {
            $playerIndex = $player->getId();
            $playersMatrix[$playerIndex]['player'] = $player->getFullName();
            $playersMatrix[$playerIndex]['player_id'] = $player->getPlayerId();
            foreach ($eventsList as $dayEvents)
            {
                foreach($dayEvents as $event)
                {
                    $existAttendance = $this->getEventAttendance($event);
                    $status = 'NO';
                    $fee = null;
                    $comment = '';
                    $editable = false;
                    $createdAt = null;
                    $index = $event->getId().'-'.$event->getCurrentDate()->format('Y-m-d');
                    
                    
                    foreach($existAttendance as $eventAttendance)
                    {
                        if($eventAttendance->getTeamPlayerId() == $player->getId())
                        {
                            $status = $eventAttendance->getStatus();
                            $fee = $eventAttendance->getFee();
                        }
                    }
                    
                   
                    if($user->getId() == $player->getPlayerId())
                    {
                         $editable = true;
                    }
                    
                    $playersMatrix[$playerIndex]['events'][$index]['team_player_id'] = $player->getId();
                    $playersMatrix[$playerIndex]['events'][$index]['event'] = $event;
                    $playersMatrix[$playerIndex]['events'][$index]['status'] = $status;
                    $playersMatrix[$playerIndex]['events'][$index]['fee'] = $fee;
                    $playersMatrix[$playerIndex]['events'][$index]['editable'] = $editable;
                    $playersMatrix[$playerIndex]['events'][$index]['comment'] = $comment;
                    $playersMatrix[$playerIndex]['events'][$index]['created_at'] = $createdAt;
                    $playersMatrix[$playerIndex]['events'][$index]['event_date'] = $event->getFormatedCurrentDate();
                }
            }
        }
        
        
        foreach ($eventsList as $dayIndex => $dayEvents)
        {
            foreach($dayEvents as $event)
            {
                $eventIndex = $dayIndex.'-'.$event->getId();
                $eventMatrix[$eventIndex]['event'] = $event;
                $eventMatrix[$eventIndex]['event_date'] = $event->getFormatedCurrentDate();
                $eventMatrix[$eventIndex]['costs'] = 0;
                
                $eventCosts = $this->getEventCosts($event);
                
                if ($eventCosts != null)
                {
                    $eventMatrix[$eventIndex]['costs'] = $eventCosts->getCosts();
                }
                
                
                $eventPlayers = array();
                $existAttendance = $this->getEventAttendance($event);
                foreach($players as $player)
                {
                    $playerIndex = $player->getId();
                    $status = 'NO';
                    $comment = '';
                    $editable = false;
                    $createdAt = null;
                     foreach($existAttendance as $eventAttendance)
                    {
                        if($eventAttendance->getTeamPlayerId() == $player->getId())
                        {
                            $status = $eventAttendance->getStatus();
                        }
                    }
                    
                    if($user->getId() == $player->getPlayerId())
                    {
                         $editable = true;
                    }
                    
                     $eventPlayers[$playerIndex]['player'] =  $player->getFullName();
                     $eventPlayers[$playerIndex]['player_id'] = $player->getPlayerId();
                     $eventPlayers[$playerIndex]['team_player_id'] = $player->getId();
                     $eventPlayers[$playerIndex]['status'] = $status;
                     $eventPlayers[$playerIndex]['editable'] = $editable;
                     $eventPlayers[$playerIndex]['comment'] = $comment;
                     $eventPlayers[$playerIndex]['created_at'] = $createdAt;
                     $eventPlayers[$playerIndex]['team_role'] = $player->getTeamRole();
                     
                     $eventMatrix[$eventIndex]['players'] = $eventPlayers;
                }
                
                 
                
                
                
            }
        }




        return array('players' => $playersMatrix,'eventMatrix' => $eventMatrix);
    }

    public function createLineupPlayer($lineup, $data)
    {
        $repo = $this->getLineupRepository();
        $lineupPlayer = new TeamMatchLineupPlayer();
        $lineupPlayer->setAttendanceId($data['attendance_id']);
        $lineupPlayer->setCreatedAt(new \DateTime());
        $lineupPlayer->setEventId($lineup->getEventId());
        $lineupPlayer->setEventDate($lineup->getEventDate());
        $lineupPlayer->setPlayerId($data['player_id']);
        $lineupPlayer->setTeamPlayerId($data['team_player_id']);
        $lineupPlayer->setPlayerName($data['player_name']);
        $lineupPlayer->setLineupPosition($data['lineup_position']);
        $lineupPlayer->setPlayerPosition($data['player_position']);
        $lineupPlayer->setLineupId($lineup->getId());
        $lineupPlayer->setLineupName($data['lineup_name']);
        $id = $repo->savePlayer($lineupPlayer);
        return $id;
    }

    public function createLineup($event)
    {
        $translator =\Core\ServiceLayer::getService('translator');
        $repo = $this->getLineupRepository();
        $lineup = new TeamMatchLineup();
        $lineup->setCreatedAt(new \DateTime());
        $lineup->setEventDate($event->getCurrentDate());
        $lineup->setEventId($event->getId());
        $lineup->setTeamId($event->getTeamId());
        $lineup->setFirstLineName( $translator->translate('lineup.bright'));
        $lineup->setSecondLineName($translator->translate('lineup.dark'));
        $lineupId = $repo->save($lineup);
        $lineup->setId($lineupId);
        return $lineup;
    }
    
    public function createLineupLine($lineup,$line,$position)
    {
        //$repo = $this->getLineupRepository();
        $translator =\Core\ServiceLayer::getService('translator');

        $name = ($position == 'first_line') ? $translator->translate('lineup.bright') : $translator->translate('lineup.dark');
        foreach ($line as $attendance)
        {
            $playerPosition = 'PLAYER';
            if($attendance->getTeamPlayer()->getTeamRole() == 'GOAL_KEEPER')
            {
                  $playerPosition = 'GOAL_KEEPER';
            }
        
            $data = array();
            $data['attendance_id'] = $attendance->getId();
            $data['player_id'] = $attendance->getParticipantId();
            $data['team_player_id'] = $attendance->getTeamPlayer()->getId();
            $data['player_name'] = $attendance->getTeamPlayer()->getFullName();
            $data['lineup_position'] = $position;
            $data['player_position'] = $playerPosition;
            $data['lineup_name'] = $name;
            
            $this->createLineupPlayer($lineup, $data);
            
            /*
            $lineupPlayer = new TeamMatchLineupPlayer();
            $lineupPlayer->setAttendanceId($attendance->getId());
            $lineupPlayer->setCreatedAt(new \DateTime());
            $lineupPlayer->setEventId($lineup->getEventId());
            $lineupPlayer->setEventDate($lineup->getEventDate());
            $lineupPlayer->setPlayerId($attendance->getParticipantId());
            $lineupPlayer->setPlayerName($attendance->getTeamPlayer()->getFullName());
            $lineupPlayer->setTeamPlayerId($attendance->getTeamPlayer()->getId());
            $lineupPlayer->setLineupPosition($position);
            $lineupPlayer->setLineupName($name);
            $lineupPlayer->setLineupId($lineup->getId());
            $repo->savePlayer($lineupPlayer);
             * 
             */
        }
    }
    
    
    /**
     * 
     * @param type $event
     * @param array $attendanceList array of Webteamer\Event\Model\EventAttendance objects
     * @return int created lineup id
     */
    public function createEfficiencyEventLineUp($event,$attendanceRawList,$lineup=null)
    {
        $repo = $this->getLineupRepository();
        $attendanceList = array();
        $goalKeepersList = array();
        foreach($attendanceRawList as $attendant)
        {
            if($attendant->getStatus() == 1)
            {
                if($attendant->getTeamPlayer()->getTeamRole() == 'GOAL_KEEPER')
                {
                    $goalKeepersList[] = $attendant;
                }
                else
                {
                     $attendanceList[] = $attendant;
                }
            }
        }
        
       
        $eficientList= array();
        foreach($attendanceList as $key => $attendance)
        {
            $playerStat = $attendance->getTeamPlayer()->getStats();
            $efficiencyIndex = round($playerStat->getLastGamesEfficiency()*10000)+$key;
            $eficientList[$efficiencyIndex] = $attendance;

        }
       
        krsort($eficientList);
        $firstLine = array();
        $secondLine = array();
        $i = 0;
        foreach($eficientList as $attendance)
        {
            if($i == 0)
            {
                $firstLine[] = $attendance;
                $i++;
            }
            elseif($i == 1)
            {
                $secondLine[] = $attendance;
                $i = 0;
            }
        }
        
        if(null == $lineup)
        {
            $lineup =$this->createLineup($event);
        }
        
        $goalKeepersCount = count($goalKeepersList); 
        $goalKeepersLineDel = ceil($goalKeepersCount / 2);
        $firstLineGoalKeepers = array_slice($goalKeepersList, 0, $goalKeepersLineDel);
        $secondLineGoalKeepers = array_slice($goalKeepersList, $goalKeepersLineDel);
        
        //append goal keeperes
        $firstLine = array_merge($firstLine,$firstLineGoalKeepers);
        $secondLine = array_merge($secondLine,$secondLineGoalKeepers);
        
        
       
        $this->createLineupLine($lineup,$firstLine , 'first_line');
        $this->createLineupLine($lineup,$secondLine , 'second_line');
        return $lineup->getId();
    }
    
    public function createManualEventLineUp($event,$attendanceRawList)
    {
        $lineup = $this->createLineup($event);
        return $lineup->getId();
    }

    public function createRandomEventLineUp($event,$attendanceRawList,$lineup = null)
    {
        $translator =\Core\ServiceLayer::getService('translator');
        $repo = $this->getLineupRepository();
        $attendanceList = array();
        $goalKeepersList = array();
        foreach($attendanceRawList as $attendant)
        {
            if($attendant->getStatus() == 1)
            {
                if($attendant->getTeamPlayer()->getTeamRole() == 'GOAL_KEEPER')
                {
                    $goalKeepersList[] = $attendant;
                }
                else
                {
                     $attendanceList[] = $attendant;
                }
               
            }
        }

        $attendanceCount = count($attendanceList);
        $lineDel = ceil($attendanceCount / 2);
        $firstLine = array_slice($attendanceList, 0, $lineDel);
        $secondLine = array_slice($attendanceList, $lineDel);
        
        
        $goalKeepersCount = count($goalKeepersList); 
        $goalKeepersLineDel = ceil($goalKeepersCount / 2);
        $firstLineGoalKeepers = array_slice($goalKeepersList, 0, $goalKeepersLineDel);
        $secondLineGoalKeepers = array_slice($goalKeepersList, $goalKeepersLineDel);
        
        //append goal keeperes
        $firstLine = array_merge($firstLine,$firstLineGoalKeepers);
        $secondLine = array_merge($secondLine,$secondLineGoalKeepers);
        

        
        if(null == $lineup)
        {
            $lineup = new TeamMatchLineup();
        }
        
        $lineup->setCreatedAt(new \DateTime());
        $lineup->setEventDate($event->getCurrentDate());
        $lineup->setEventId($event->getId());
        $lineup->setTeamId($event->getTeamId());
        $lineup->setFirstLineName($translator->translate('lineup.bright'));
        $lineup->setSecondLineName($translator->translate('lineup.dark'));
        
        if(null == $lineup->getId())
        {
            $lineupId = $repo->save($lineup);
            $lineup->setId($lineupId);
        }
        else
        {
            $lineupId = $lineup->getId();
        }

        $this->createLineupLine($lineup,$firstLine , 'first_line');
        $this->createLineupLine($lineup,$secondLine , 'second_line');
        
        /*
        foreach ($firstLine as $attendance)
        {
            $lineupPlayer = new TeamMatchLineupPlayer();
            $lineupPlayer->setAttendanceId($attendance->getId());
            $lineupPlayer->setCreatedAt(new \DateTime());
            $lineupPlayer->setEventId($event->getId());
            $lineupPlayer->setEventDate($event->getCurrentDate());
            $lineupPlayer->setPlayerId($attendance->getParticipantId());
            $lineupPlayer->setPlayerName($attendance->getTeamPlayer()->getFullName());
            $lineupPlayer->setTeamPlayerId($attendance->getTeamPlayer()->getId());
            $lineupPlayer->setLineupPosition('first_line');
            $lineupPlayer->setLineupName('Team A');
            $lineupPlayer->setLineupId($lineupId);
            $repo->savePlayer($lineupPlayer);
        }

        foreach ($secondLine as $attendance)
        {
            $lineupPlayer = new TeamMatchLineupPlayer();
            $lineupPlayer->setAttendanceId($attendance->getId());
            $lineupPlayer->setCreatedAt(new \DateTime());
            $lineupPlayer->setEventId($event->getId());
            $lineupPlayer->setEventDate($event->getCurrentDate());
            $lineupPlayer->setPlayerId($attendance->getParticipantId());
            $lineupPlayer->setPlayerName($attendance->getTeamPlayer()->getFullName());
            $lineupPlayer->setTeamPlayerId($attendance->getTeamPlayer()->getId());
            $lineupPlayer->setLineupPosition('second_line');
            $lineupPlayer->setLineupName('Team B');
            $lineupPlayer->setLineupId($lineupId);
            $repo->savePlayer($lineupPlayer);
        }
        */
        return $lineupId;
    }

    public function getEventLineup($event)
    {
        $object = $this->getLineupRepository()->findEventLineup($event);
        return $object;
    }

    public function getLineupById($id)
    {
        $object = $this->getLineupRepository()->find($id);
        return $object;
    }

    public function getEventLineupPlayers($lineup)
    {
        $list = $this->getLineupRepository()->findEventLineupPlayers($lineup);
        return $list;
    }
    
     public function getEventLineupPlayer($lineup,$playerId)
    {
        $list = $this->getLineupRepository()->findEventLineupPlayer($lineup,$playerId);
        return $list;
    }
    
    public function getEventLineupPlayerById($id)
    {
        $player = $this->getLineupRepository()->findLineupPlayer($id);
        return $player;
    }

    public function changeLineupPlayer($id, $lineupPosition)
    {
        $lineupPlayer = $this->getLineupRepository()->findLineupPlayer($id);

        
        $lineup = $this->getLineupById($lineupPlayer->getLineupId());

        
       $lineupData['lineup_position'] = $lineupPosition;
        if($lineupPosition == 'first_line')
        {
             $lineupData['lineup_name'] = $lineup->getFirstLineName();
        }
        if($lineupPosition == 'second_line')
        {
             $lineupData['lineup_name'] = $lineup->getSecondLineName();
        }
        
        
        $this->getLineupRepository()->updateLineupPlayer($id, $lineupData);
    }

    public function removeLineupPlayer($id)
    {
        $this->getLineupRepository()->removeLineupPlayer($id);
    }

    public function updateLineup($id, $data)
    {
        $this->getLineupRepository()->updateLineup($id, $data);
    }
    
    public function saveLineup($lineup)
    {
        $this->getLineupRepository()->save($lineup);
    }
    
    public function saveEvent($event)
    {
        $this->getRepository()->save($event);
    }
    
    /**
     * Return last team event before date
     * @param type $team Webteamer\Team\Model\Team
     * @param date Datetime 
     */
    public function getTeamLastEvent($team,$date)
    {

        $to = $date;
        $from = clone $date;
        $from->setTimestamp(strtotime("- 30 day"));
        
        $events = $this->findEvents($team, array('from' => $from , 'to' => $to));
        $eventsList = $this->buildTeamEventList($events,$from, $to);
        if(is_array($eventsList))
        {
            return current(end($eventsList));
        }
    }
    
    /**
     * Delete whole linuep
     * @param type $event
     */
    public function deleteEventLineup($event)
    {
        $lineupRepo =  $this->getLineupRepository();
        $lineup = $lineupRepo->findEventLineup($event);
        
        if(null != $lineup)
        {
            $lineupPlayers = $lineupRepo->findEventLineupPlayers($lineup);
            $lineupRepo->delete($lineup->getId());
            $lineupPlayerRepo = $lineupRepo->getPlayerRepository();
            foreach($lineupPlayers as $lineupPlayer)
            {
                $lineupPlayerRepo->delete($lineupPlayer->getId());
            }
        }
        
        
    }
    /**
     * remove players form lienup
     * @param type $event
     */
    public function resetEventLineup($event)
    {
        $lineupRepo =  $this->getLineupRepository();
        $lineup = $lineupRepo->findEventLineup($event);
        $lineupPlayers = $lineupRepo->findEventLineupPlayers($lineup);
        $lineupPlayerRepo = $lineupRepo->getPlayerRepository();
        foreach($lineupPlayers as $lineupPlayer)
        {
            $lineupPlayerRepo->delete($lineupPlayer->getId());
        }
    }
    
    private function appendPageEvents($team,$pageLimit,$interval,$list)
    {
        $diff = $pageLimit-count($list);
        /*
        $nextMonth = date('Y-m', mktime(0, 0, 0, $monthInfo['current_month'], $monthInfo['days_count'] + 1,$monthInfo['current_year'])).'-1';
        $nextMonthInfo = $this->getPagerMonthInfo(new \DateTime($nextMonth));
        */
        $from = $interval['to'];
        $to = clone $from;
        $to->modify('+30 day');
        $interval = array('from' => $from, 'to' => $to);
        
        
        $nextEvents = $this->findEvents($team, array('from' => $from, 'to' => $to));
        $nextEventsList = $this->buildTeamEventList($nextEvents, $from, $to);

        $nextPageEvents = array();
        foreach($nextEventsList as $dayEvents)
        {
             foreach($dayEvents as $event)
             {
                 $nextPageEvents[$event->getCurrentDate()->format('Ymd').'-'.$event->getId()] = $event;
             }
        }

        $appendEvents = array_slice($nextPageEvents, 0,$diff);
        
        $list = array_merge($list, $appendEvents);
        
        //var_dump($nextEventsList);
        
        
        if(count($nextEventsList) == 0)
        {
            return $list;
        }
        
        if(count($list) < $pageLimit)
        {
            $diff = $pageLimit-count($list);
            $list =  $this->appendPageEvents($team,$pageLimit,$interval,$list);
        }

        return $list;
    }
    
    private function prependPageEvents($team,$pageLimit,$monthInfo,$list)
    {
        $diff = $pageLimit-count($list);

        
        $prevMonth = clone $monthInfo['from'];
        $prevMonth->sub(new \DateInterval('P180D'));
        $prevMonthInfo = array(
            'from' => $prevMonth, 
            'to' => $monthInfo['from'], 
            'start_date' => $prevMonth
        );
        /*
        $prevMonth = date('Y-m-d', mktime(0, 0, 0, $monthInfo['current_month']-1,1,$monthInfo['current_year']));
        $prevMonthInfo = $this->getPagerMonthInfo(new \DateTime($prevMonth));
        */
        
        $prevEvents = $this->findEvents($team, array('from' => $prevMonthInfo['from'], 'to' => $prevMonthInfo['to']));
        

        $prevEventsList = $this->buildTeamEventList($prevEvents, $prevMonthInfo['from'], $prevMonthInfo['to']);
        $prevEventsList = array_reverse($prevEventsList, true);
        $prevPageEvents = array();
        foreach($prevEventsList as $dayEvents)
        {
             foreach($dayEvents as $event)
             {
                 $prevPageEvents[$event->getCurrentDate()->format('Ymd').'-'.$event->getId()] = $event;
             }
        }

        $prependEvents = array_slice($prevPageEvents, 0,$diff);
        
        $list = array_merge($prependEvents,$list);
        if(count($list) < $pageLimit && !empty($prevEventsList))
        {
            $diff = $pageLimit-count($list);
            $list =  $this->prependPageEvents($team,$pageLimit,$this->getPagerMonthInfo($prevMonth),$list);
        }

        return $list;
    }
    
    /**
     * Get upcoming events
     * @param type $team
     * @param DateTime $paggingStart
     * @return \Webteamer\Event\Model\EventPagger
     */
    public function getPaggedUpcomingEvents($team,\DateTime $paggingStart)
    {
        $monthInfo = $this->getPagerMonthInfo($paggingStart);
        $from = $paggingStart;
        $to = clone $paggingStart;
        $to->modify('+30 day');
        $interval = array('from' => $from, 'to' => $to);

        $events = $this->findEvents($team, array('from' => $from, 'to' => $to));
        //$pageEventsList = $this->buildTeamEventList($events, $monthInfo['from'], $monthInfo['to']);
        $pageEventsList = $this->buildTeamEventList($events, $from, $to);
        $pageEvents = array();
        


        foreach($pageEventsList as $dayEvents)
        {
             foreach($dayEvents as $event)
             {
                 if($event->getCurrentDate()->getTimestamp() > $monthInfo['start_date']->getTimestamp())
                {
                     $pageEvents[$event->getCurrentDate()->format('Ymd').'-'.$event->getId()] = $event;
                }
             }
        }
        
        $list = $pageEvents;
        $pageLimit = 9;
        


        
        if(count($list) < $pageLimit && !empty($list))
        {
            $list =  $this->appendPageEvents($team,$pageLimit,$interval,$list);
        }
        
        
        $pagger = new \Webteamer\Event\Model\EventPagger();
        
        if(count($list) >= $pageLimit)
        {
            $nextPageEvent = array_pop ($list);
            $pagger->setEvents($list);
            $pagger->setNextPageEvent($nextPageEvent);
        }
        else{

        
            $pagger->setEvents($list);
        }

        

        return $pagger;
    }
    
    
    
     /**
     * Get past events
     * @param type $team
     * @param DateTime $paggingStart
     * @return \Webteamer\Event\Model\EventPagger
     */
    public function getPaggedPastEvents($team,\DateTime $paggingStart,\DateTime $paggingEnd)
    {

         $monthInfo = array(
            'from' => $paggingStart, 
            'to' => $paggingEnd,
        );
        
        
        
        
        $events = $this->findEvents($team, array('from' => $monthInfo['from'], 'to' => $monthInfo['to']));
        $pageEventsList = $this->buildTeamEventList($events, $monthInfo['from'], $monthInfo['to']);
        $pageEvents = array();
       
        $pageEventsList = array_reverse($pageEventsList, true);
        
       

        foreach($pageEventsList as $dayEvents)
        {
             foreach($dayEvents as $event)
             {
                 if($event->getCurrentDate()->getTimestamp() < $monthInfo['to']->getTimestamp())
                {
                     $pageEvents[$event->getCurrentDate()->format('Ymd').'-'.$event->getId()] = $event;
                }
             }
        }

        $list = array_reverse(array_slice($pageEvents,0, 8, true));
        
        $pageLimit = 8;
        

        if(count($list) < $pageLimit)
        {
            $list =  $this->prependPageEvents($team,$pageLimit,$monthInfo,$list);
        }
        
        
        $pagger = new \Webteamer\Event\Model\EventPagger();
        
        $pagger->setEvents($list);

        

        return $pagger;
    }
    
    public function getPagerMonthInfo($paggingStart)
    {

        
        if ($paggingStart == null)
        {
            $current_month = date('n');
            $current_year = date('Y');
            $current_day = date('d');
            $calendar_date = date('Y') . '-' . date('n');
        }
        else
        {
            //$calendar_date_parts = explode('-', $startDate);
            $current_day = $paggingStart->format('d');
            $current_month =$paggingStart->format('m');
            $current_year = $paggingStart->format('Y');
        }
        


        $days_count = date('t', mktime(0, 0, 0, $current_month, 1));
        $first_day_in_month = $current_year.'-'.$current_month. '-1';
        $last_day_in_month =  $current_year.'-'.$current_month. '-' . $days_count;
        $from = new DateTime($first_day_in_month);
        $to = new DateTime($last_day_in_month);

        return array(
            'from' => $from, 
            'to' => $to, 
            'current_day' => $current_day, 
            'current_month' => $current_month, 
            'current_year' => $current_year, 
            'days_count' => $days_count, 
            'start_date' => $paggingStart
        );
    }

    
    public function getLastGames()
    {

        $repo = $this->getLineupRepository();
        $list = $repo->findLastClosedLinups();
        

       
    }
   


}

?>