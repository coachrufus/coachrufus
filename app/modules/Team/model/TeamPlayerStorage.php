<?php
namespace CR\Team\Model;
use Core\DbStorage;
class TeamPlayerStorage extends DbStorage {
    
	public function fulltextFindPlayers($phrase, $team_id)
        {
            $data = \Core\DbQuery::prepare('
                SELECT tp.id, tp.player_id, tp.team_id, tp.created_at, tp.status, tp.team_role, tp.level, tp.first_name, tp.last_name, tp.email,  p.name, p.surname, p.email as player_email, p.photo
                FROM team_players tp
                LEFT JOIN co_users p ON tp.player_id = p.id 
                WHERE tp.team_id = :id
                AND (tp.first_name like :first_name OR tp.last_name like :last_name  OR tp.email like :email )
                order by tp.last_name asc, tp.first_name asc')
		->bindParam('id', $team_id)
                ->bindParam('first_name', $phrase."%")
                ->bindParam('last_name', $phrase."%")
                ->bindParam('email', $phrase."%")
		->execute()
		->fetchAll(\PDO::FETCH_ASSOC);
		return $data;
        }
        
        public function findPlayerTeamMatesByFirstName($playerId,$phrase) {
         

            $query = \Core\DbQuery::prepare('
                SELECT mention.*
                     FROM   team_players user_teams 
                            LEFT JOIN team_players mention 
                                   ON user_teams.team_id = mention.team_id 
                     WHERE  user_teams.player_id = :player_id
                     AND mention.first_name like :term or mention.last_name like :term');
                         
           $query->bindParam('player_id', $playerId);
           $query->bindParam('term',  $phrase."%");
           
           
	   $data = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
           return $data;
        }


        
	
	
      
	
	
}