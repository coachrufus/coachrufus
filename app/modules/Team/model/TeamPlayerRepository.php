<?php

namespace Webteamer\Team\Model;

use Core\DbStorage;
use Core\EntityMapper;
use Core\Repository as Repository;
use Core\ServiceLayer;


class TeamPlayerRepository extends Repository {
    public function fulltextFindPlayers($phrase,$teamId)
    {
        $data = $this->getStorage()->fulltextFindPlayers($phrase,$teamId);
        return $this->createObjectList($data);
    }
    
    public function unactivateTeamPlayer($player)
    {
        $player->setStatus('unactive');
        $this->save($player);
    }
    
    public function findPlayerTeamMatesByFirstName($player,$term)
    {
        $data =  $this->getStorage()->findPlayerTeamMatesByFirstName($player->getId(),$term);
        return $this->createObjectList($data);

    }
}