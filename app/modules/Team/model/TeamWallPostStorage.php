<?php
namespace CR\Team\Model;
use Core\DbStorage;
class TeamWallPostStorage extends DbStorage {
    
	
       protected function getBaseQuery()
       {
          $query = 'SELECT p.*, u.name, u.surname, u.photo, count(c.id) as comments_count, lc.count as like_count
                FROM '.$this->getTableName().' p
                LEFT JOIN co_users u ON p.author_id = u.id
                LEFT JOIN  '.$this->getTableName().' c on p.id = c.parent_id 
                LEFT JOIN (select post_id, count(*) as count from team_wall_like group by post_id) lc ON lc.post_id = p.id
                WHERE p.team_id = :team_id
                AND p.parent_id is null
                ';
           return $query;
       }
    
       public function getTeamLastWallPosts($teamId,$limit = 5)
       {
           $query = $this->getBaseQuery().' 
                GROUP by p.id
                ORDER  by p.id desc LIMIT 0,'.$limit;
           
           
           $data = \Core\DbQuery::prepare($query)
                 ->bindParam(':team_id', $teamId)
		->execute()
		->fetchAll(\PDO::FETCH_ASSOC);
		return $data;
       }
       
       public function getTeamWallPosts($teamId,$start,$limit = 5)
       {
            $query = $this->getBaseQuery().' 
                AND p.id < :start
                GROUP by p.id
                ORDER  by p.id desc LIMIT 0,'.$limit;
           
           $data = \Core\DbQuery::prepare($query)
                 ->bindParam(':team_id', $teamId)
                 ->bindParam(':start', $start)
		->execute()
		->fetchAll(\PDO::FETCH_ASSOC);
		return $data;
       }
	
       public function getTeamHashtagWallPosts($teamId,$hashtag,$start,$limit = 10)
       {
            
           $startQuery = '';
           if($start > 0)
           {
                $startQuery = ' AND p.id < :start ';
           }
           
            $query = $this->getBaseQuery().' 
                '.$startQuery.'
                 AND p.body like :hashtag 
                GROUP by p.id
                ORDER  by p.id desc LIMIT 0,'.$limit;
           
           
           $query = \Core\DbQuery::prepare( $query)
                 ->bindParam(':team_id', $teamId)
                 ->bindParam(':hashtag',  "%#".$hashtag."%");

                if($start > 0)
                {
                    $query->bindParam(':start', $start);
                }
           
		$data = $query->execute()
		->fetchAll(\PDO::FETCH_ASSOC);

		return $data;
       }
	
       
       public function getPostLikesCount($postId)
       {
           $query = \Core\DbQuery::prepare('Select count(*) as likes_count from team_wall_like where post_id = :post_id');         
           $query->bindParam('post_id', $postId);
	   $data = $query->execute()->fetchOne(\PDO::FETCH_ASSOC);
           return $data;
       }
       
       public function getPostLikesInfo($ids)
       {
           
           $ids = implode(',',$ids);
           $query = \Core\DbQuery::prepare('SELECT post_id,user_id from team_wall_like where post_id in ('.$ids.')');         
	   $data = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
           return $data;
       }
	
}