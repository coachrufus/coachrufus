<?php

namespace Webteamer\Team\Model;

use Core\ServiceLayer;
use Webteamer\Locality\Model\Playground;
use Webteamer\Locality\Model\PlaygroundAlias;

class TeamPlayerManager  extends \Core\Manager
{
    /**
     * Grup players by status
     * @param array $players
     * @return array grouped array
     */
    public function groupPlayersByStatus($players)
    {
        $list = array('confirmed' => array(), 'unknown' => array(), 'waiting-to-confirm' => array(),'unconfirmed' => array(),'decline' => array());
        foreach($players as $player)
        {
            $list[$player->getStatus()][$player->getId()] = $player;
        }
        
        return $list;
    }
    
    /**
     * Return team player by his id
     * @param type $id
     * @return type
     */
    public function getTeamPlayerById($id)
    {
        $teamPlayer = $this->getRepository()->find($id);
        return $teamPlayer;
    }
    
    public function fulltextFindPlayer($phrase,$teamId)
    {
        $teamPlayers = $this->getRepository()->fulltextFindPlayers($phrase,$teamId);
        return $teamPlayers;
    }
    
    /**
     * 
     * @param type $teamId
     * @param type $playerId
     * @return type
     */
    public function findTeamPlayerByPlayerId($teamId,$playerId)
    {
       $teamPlayer = $this->getRepository()->findOneBy(array('team_id' => $teamId,'player_id' => $playerId ));
       return $teamPlayer;
    }
    
      public function unactivateTeamPlayer($player)
    {
        $this->getRepository()->unactivateTeamPlayer($player);
    }
    
    public function findPlayerTeamMates($player,$term)
    {
        //team players
        $playerTeamMates = $this->getRepository()->findPlayerTeamMatesByFirstName($player,$term);
        return $playerTeamMates;
    }
    
    public function saveTeamPlayer($teamPlayer)
    {
        $this->getRepository()->save($teamPlayer);
    }
    
   
}
