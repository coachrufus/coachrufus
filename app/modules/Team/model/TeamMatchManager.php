<?php
namespace Webteamer\Team\Model;

use Core\Manager;
use Core\ServiceLayer;
use Core\Types\DateTimeEx;
use Webteamer\Player\Model\PlayerStat;



/**
 * Manage business logic related to lineup
 */

class TeamMatchManager extends Manager 
{
    
    public function getTeamMatchStatusInfo($team)
    {
        $teamId = $team->getId();
        $info = $this->getRepository()->findTeamMatchStatusInfo($teamId);
        return $info;
    }
    
    public function createLineup($event,$team,$type)
    {
        $eventManager = ServiceLayer::getService('TeamEventManager');
        $teamManager = ServiceLayer::getService('TeamManager');
        $playerStatManager = ServiceLayer::getService('PlayerStatManager');
        $seasonManager = ServiceLayer::getService('TeamSeasonManager');
        

        $existLineup = $eventManager->getEventLineup($event);

        if (null != $existLineup)
        {
            $eventManager->resetEventLineup($event);
        }

        $attendanceList = $eventManager->getEventAttendance($event);
        $eventSeason = $seasonManager->getSeasonById($event->getSeason());
        $statFrom = new DateTimeEx($eventSeason->getStartDate());
        $statTo = new DateTimeEx($eventSeason->getEndDate());
        $teamPlayersStats = $playerStatManager->getAllTeamPlayersStat($team, null, $statFrom, $statTo);
        foreach ($attendanceList as $attendance)
        {
            $teamPlayer = $teamManager->findTeamPlayerById($attendance->getTeamPlayerId());
            if (null != $teamPlayer)
            {
                //$playerStat = $playerStatManager->getTeamPlayerStat($teamPlayer);
                $playerStat = $teamPlayersStats[$teamPlayer->getId()];
                if(null == $playerStat)
                {
                   $playerStat = new PlayerStat();
                }
               
                $teamPlayer->setStats($playerStat);
                $attendance->setTeamPlayer($teamPlayer);
            }
        }

        if ('random' == $type)
        {
            $lineupId = $eventManager->createRandomEventLineUp($event, $attendanceList, $existLineup);
            ServiceLayer::getService('StepsManager')->finishCreateRandomLineup($team);
            ServiceLayer::getService('StepsManager')->finishCreatePerformanceLineup($team);
        }
        if ('manual' == $type)
        {
            ServiceLayer::getService('StepsManager')->finishCreateManualLineup($team);
            ServiceLayer::getService('StepsManager')->finishCreateRandomLineup($team);
            ServiceLayer::getService('StepsManager')->finishCreatePerformanceLineup($team);
            if (null == $existLineup)
            {
                $lineupId = $eventManager->createManualEventLineUp($event, $attendanceList);
            }
            else
            {
                $lineupId = $existLineup->getId();
            }
        }
        if ('efficiency' == $type)
        {
            $lineupId = $eventManager->createEfficiencyEventLineUp($event, $attendanceList, $existLineup);
            ServiceLayer::getService('StepsManager')->finishCreatePerformanceLineup($team);
        }
        
        //send notify
         //$notificationManager = ServiceLayer::getService('SystemNotificationManager');
         //$notificationManager->sendCreateLineupPushNotify($pushNotifyRecipients,array('lineupId' => $lineupId));
        
        return $lineupId;

        //$notificationManager = ServiceLayer::getService('SystemNotificationManager');
        //$notificationManager->sendCreateLineupPushNotify($pushNotifyRecipients);
    }
}
