<?php
	
namespace Webteamer\Team\Model;
use Core\EntityMapper as EntityMapper;
use Core\ServiceLayer as ServiceLayer;
		
class TeamMatchLineup {	

	
	
		
private $repository;
private $mapper_rules = array(
        'id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'event_id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'event_date' => array(
            'type' => 'datetime',
            'max_length' => '',
            'required' => false
        ),
        'sport_id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'team_id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'created_at' => array(
            'type' => 'datetime',
            'max_length' => '',
            'required' => false
        ),
        'first_line_name' => array(
            'type' => 'string',
            'max_length' => '255',
            'required' => false
        ),
        'second_line_name' => array(
            'type' => 'string',
            'max_length' => '255',
            'required' => false
        ),
        'status' => array(
            'type' => 'string',
            'max_length' => '255',
            'required' => false
        ),
        'started_at' => array(
            'type' => 'datetime',
            'max_length' => '',
            'required' => false
        ),
        'seconds_past' => array(
            'type' => 'int',
            'max_length' => '',
            'required' => false
        ),
       'opponent_team_id' => array(
            'type' => 'int',
            'max_length' => '10',
            'required' => false
        ),
       'overtime' => array(
            'type' => 'int',
            'max_length' => '10',
            'required' => false
        ),
    );

    public function getDataMapperRules()
{
	return $this->mapper_rules;
}

public function getRepository()
{
	if(null == $this->repository)
	{
		$this->repository = ServiceLayer::getService("TeamMatchLineupRepository"); 
	}
	return $this->repository;
}

public function setRepository($repository)
{
	$this->repository = $repository;
}
			

		
		
		
protected $id;

protected $eventId;

protected $eventDate;

protected $sportId;

protected $teamId;

protected $createdAt;
protected $firstLineName;
protected $secondLineName;
protected $status;

protected $lineupPlayers;
protected $startedAt;
protected $secondsPast;
protected $opponentTeamId;
protected $overtime;
				
public function setId($val){$this->id = $val;}

public function getId(){return $this->id;}
	
public function setEventId($val){$this->eventId = $val;}

public function getEventId(){return $this->eventId;}
	
public function setEventDate($val){$this->eventDate = $val;}

public function getEventDate(){return $this->eventDate;}
	
public function setSportId($val){$this->sportId = $val;}

public function getSportId(){return $this->sportId;}
	
public function setTeamId($val){$this->teamId = $val;}

public function getTeamId(){return $this->teamId;}
	
public function setCreatedAt($val){$this->createdAt = $val;}

public function getCreatedAt(){return $this->createdAt;}

public  function getFirstLineName() {
return $this->firstLineName;
}

public  function getSecondLineName() {
return $this->secondLineName;
}

public  function setFirstLineName($firstLineName) {
$this->firstLineName = $firstLineName;
}

public  function setSecondLineName($secondLineName) {
$this->secondLineName = $secondLineName;
}

function getStatus() {
return $this->status;
}

 function setStatus($status) {
$this->status = $status;
}

function getOpponentTeamId() {
return $this->opponentTeamId;
}

 function setOpponentTeamId($opponentTeamId) {
$this->opponentTeamId = $opponentTeamId;
}

function getOvertime() {
return $this->overtime;
}

 function setOvertime($overtime) {
$this->overtime = $overtime;
}




public function isClosed()
{
    if($this->getStatus() == 'closed')
    {
        return true;
    }
    return false;
}

public function getLineupPlayers($position = null) {

    $players = $this->lineupPlayers;
    if(null != $position)
    {
         $players = array();
         foreach($this->lineupPlayers as  $lineupPlayer)
         {
             if($lineupPlayer->getLineupPosition()  == $position)
             {
                 $players[] = $lineupPlayer;
             }
         }
    }
    
    
    return $players;
    
}

 public function setLineupPlayers($lineupPlayers) {
$this->lineupPlayers = $lineupPlayers;
}

function getStartedAt() {
return $this->startedAt;
}

 function setStartedAt($startedAt) {
$this->startedAt = $startedAt;
}

function getSecondsPast() {
return $this->secondsPast;
}

 function setSecondsPast($secondsPast) {
$this->secondsPast = $secondsPast;
}




}

		