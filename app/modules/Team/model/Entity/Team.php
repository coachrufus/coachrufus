<?php
	
namespace Webteamer\Team\Model;
use Core\EntityMapper as EntityMapper;
use Core\ServiceLayer as ServiceLayer;
		
class Team {	

	
	
private $mapper_rules  = array(
'id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => true
							
					),
'name' => array(
					'type' => 'string',
					'max_length' => '255',
					'required' => false
							
					),
'sport_id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => false
							
					),
'created_at' => array(
					'type' => 'datetime',
					'max_length' => '',
					'required' => false
							
					),
'author_id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => false
							
					),
'age_from' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => false
							
					),
'age_to' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => false
							
					),
'status' => array(
					'type' => 'string',
					'max_length' => '50',
					'required' => false
							
					),
'description' => array(
					'type' => 'string',
					'max_length' => '',
					'required' => false
							
					),
'phone' => array(
					'type' => 'string',
					'max_length' => '50',
					'required' => false
							
					),
'email' => array(
					'type' => 'string',
					'max_length' => '50',
					'required' => false
							
					), 
'gender' => array(
					'type' => 'string',
					'max_length' => '50',
					'required' => false
							
					), 
'level' => array(
					'type' => 'string',
					'max_length' => '50',
					'required' => false
							
					), 
'photo' => array(
					'type' => 'string',
					'max_length' => '50',
					'required' => false
							
					), 
'address_city' => array(
					'type' => 'string',
					'max_length' => '50',
					'required' => false
							
					), 
    'address_zip' => array(
                                            'type' => 'string',
                                            'max_length' => '50',
                                            'required' => false

                                            ), 
    'address_street' => array(
                                            'type' => 'string',
                                            'max_length' => '50',
                                            'required' => false

                                            ), 
    'address_street_num' => array(
                                            'type' => 'string',
                                            'max_length' => '50',
                                            'required' => false

                                            ), 
    'full_address' => array(
                                            'type' => 'string',
                                            'max_length' => '255',
                                            'required' => false

                                            ), 
    'privacy' => array(
                                            'type' => 'string',
                                            'max_length' => '50',
                                            'required' => false

                                            ), 
    'age_group' => array(
                                            'type' => 'string',
                                            'max_length' => '50',
                                            'required' => false

                                            ), 
    'locality_id' => array(
                                            'type' => 'int',
                                            'required' => false

                                            ), 
  'playground_alias' => array(
					'type' => 'non-persist',
					'required' => false
							
					),  

);
	
public function getDataMapperRules()
{
	return $this->mapper_rules;
}

		

		
	protected $id;	
		
protected $name;

protected $sportId;

protected $createdAt;

protected $authorId;

protected $ageFrom;

protected $ageTo;

protected $status;

protected $description;

protected $phone;

protected $email;
protected $playgroundAlias;


protected $gender;
protected $level;
protected $photo;
protected $addressCity;
protected $addressZip;
protected $addressStreet;
protected $addressStreetNum;
protected $fullAddress;
protected $privacy;
protected $ageGroup;
protected $localityId;
protected $sportName;
protected $locality;
protected $creatorEmail;

function getCreatorEmail() {
return $this->creatorEmail;
}

 function setCreatorEmail($creatorEmail) {
$this->creatorEmail = $creatorEmail;
}



public function setId($val){$this->id = $val;}

public function getId(){return $this->id;}
	
public function setName($val){$this->name = $val;}

public function getName(){return $this->name;}
	
public function setSportId($val){$this->sportId = $val;}

public function getSportId(){return $this->sportId;}
	
public function setCreatedAt($val){$this->createdAt = $val;}

public function getCreatedAt(){return $this->createdAt;}
	
public function setAuthorId($val){$this->authorId = $val;}

public function getAuthorId(){return $this->authorId;}
	
public function setAgeFrom($val){$this->ageFrom = $val;}

public function getAgeFrom(){return $this->ageFrom;}
	
public function setAgeTo($val){$this->ageTo = $val;}

public function getAgeTo(){return $this->ageTo;}
	
public function setStatus($val){$this->status = $val;}

public function getStatus(){return $this->status;}
	
public function setDescription($val){$this->description = $val;}

public function getDescription(){return $this->description;}
	
public function setPhone($val){$this->phone = $val;}

public function getPhone(){return $this->phone;}
	
public function setEmail($val){$this->email = $val;}

public function getEmail(){return $this->email;}
				



public  function getPlaygroundAlias() {
return $this->playgroundAlias;
}

public  function setPlaygroundAlias($playgroundAlias) {
$this->playgroundAlias = $playgroundAlias;
}


public  function getGender() {
return $this->gender;
}

public  function getLevel() {
return $this->level;
}

public  function getPhoto() {
    return $this->photo;
}

public  function getDefaultPhoto() {

   return  '/img/team/users.svg';

}

public  function getMidPhoto() {
    
    if (null == $this->getPhoto())
    {
         $midPhoto = $this->getDefaultPhoto();
    }
    else
    {
        $midPhoto = '/img/team/thumb_320_320/'.$this->getPhoto();
    }


    return  $midPhoto ;
   
}

public  function getPrivacy() {
return $this->privacy;
}

public  function setGender($gender) {
$this->gender = $gender;
}

public  function setLevel($level) {
$this->level = $level;
}

public  function setPhoto($photo) {
$this->photo = $photo;
}

public  function setPrivacy($privacy) {
$this->privacy = $privacy;
}



public  function getAgeGroup() {
return $this->ageGroup;
}

public  function setAgeGroup($ageGroup) {
$this->ageGroup = $ageGroup;
}

public  function getAddressCity() {
return $this->addressCity;
}

public  function getAddressZip() {
return $this->addressZip;
}

public  function getAddressStreet() {
return $this->addressStreet;
}

public  function getAddressStreetNum() {
return $this->addressStreetNum;
}

public  function setAddressCity($addressCity) {
$this->addressCity = $addressCity;
}

public  function setAddressZip($addressZip) {
$this->addressZip = $addressZip;
}

public  function setAddressStreet($addressStreet) {
$this->addressStreet = $addressStreet;
}

public  function setAddressStreetNum($addressStreetNum) {
$this->addressStreetNum = $addressStreetNum;
}

public  function getFullAddress() {
return $this->fullAddress;
}

public  function setFullAddress($fullAddress) {
$this->fullAddress = $fullAddress;
}

public  function getLocalityId() {
return $this->localityId;
}

public  function setLocalityId($localityId) {
$this->localityId = $localityId;
}




public function getShareLinkHash()
{
    return substr(sha1($this->getId()),0,3).$this->getId().substr(md5($this->getId()),0,8);
}

public  function getSportName() {
return $this->sportName;
}

public  function setSportName($sportName) {
$this->sportName = $sportName;
}


function getLocality() {
return $this->locality;
}

 function setLocality($locality) {
$this->locality = $locality;
}









}

		