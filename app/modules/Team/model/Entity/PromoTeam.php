<?php
	
namespace CR\Team\Model;
use Core\EntityMapper as EntityMapper;
use Core\ServiceLayer as ServiceLayer;
		
class PromoTeam {	

	
	
		
private $mapper_rules  = array(
	'id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => false
							
					),
'team_id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => false
							
					),
'created_at' => array(
					'type' => 'datetime',
					'max_length' => '',
					'required' => false
							
					),
'author_id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => false
							
					),
'promo_code' => array(
					'type' => 'string',
					'max_length' => '50',
					'required' => false
							
					),
'status' => array(
					'type' => 'string',
					'max_length' => '50',
					'required' => false
							
					),

);
	
public function getDataMapperRules()
{
	return $this->mapper_rules;
}

		
		
		
protected $id;

protected $teamId;

protected $createdAt;

protected $authorId;

protected $promoCode;

protected $status;

				
public function setId($val){$this->id = $val;}

public function getId(){return $this->id;}
	
public function setTeamId($val){$this->teamId = $val;}

public function getTeamId(){return $this->teamId;}
	
public function setCreatedAt($val){$this->createdAt = $val;}

public function getCreatedAt(){return $this->createdAt;}
	
public function setAuthorId($val){$this->authorId = $val;}

public function getAuthorId(){return $this->authorId;}
	
public function setPromoCode($val){$this->promoCode = $val;}

public function getPromoCode(){return $this->promoCode;}
	
public function setStatus($val){$this->status = $val;}

public function getStatus(){return $this->status;}

}

		