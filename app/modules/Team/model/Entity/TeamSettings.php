<?php
	
namespace Webteamer\Team\Model;
use Core\EntityMapper as EntityMapper;
use Core\ServiceLayer as ServiceLayer;
		
class TeamSettings {	

	
	
		
private $repository;
private $mapper_rules  = array(
	'id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => false
							
					),
'name' => array(
					'type' => 'string',
					'max_length' => '50',
					'required' => false
							
					),
'value' => array(
					'type' => 'string',
					'max_length' => '50',
					'required' => false
							
					),
'settings_group' => array(
					'type' => 'string',
					'max_length' => '50',
					'required' => false
							
					),
'team_id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => false
							
					),

);
	
public function getDataMapperRules()
{
	return $this->mapper_rules;
}
		

		
protected $id;

protected $name;

protected $value;

protected $settings_group;

protected $teamId;

				
public function setId($val){$this->id = $val;}

public function getId(){return $this->id;}
	
public function setName($val){$this->name = $val;}

public function getName(){return $this->name;}
	
public function setValue($val){$this->value = $val;}

public function getValue(){return $this->value;}
	
public function setSettingsGroup($val){$this->settings_group = $val;}

public function getSettingsGroup(){return $this->settings_group;}
	
public function setTeamId($val){$this->teamId = $val;}

public function getTeamId(){return $this->teamId;}

}

		