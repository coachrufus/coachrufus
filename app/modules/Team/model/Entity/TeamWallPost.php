<?php

namespace CR\Team\Model;

use Core\EntityMapper as EntityMapper;
use Core\ServiceLayer as ServiceLayer;

class TeamWallPost {

    private $repository;
    protected $smalThumbDir = '/img/wall/thumb_90_90';
    protected $largeThumbDir = '/img/wall/thumb_800_800';
    private $mapper_rules = array(
        'id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => true
        ),
        'created_at' => array(
            'type' => 'datetime',
            'max_length' => '',
            'required' => true
        ),
        'author_id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => true
        ),
        'status' => array(
            'type' => 'string',
            'max_length' => '50',
            'required' => true
        ),
        'body' => array(
            'type' => 'string',
            'max_length' => '',
            'required' => true
        ),
        'team_id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => true
        ),
        'add_content' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => true
        ),
        'images' => array(
            'type' => 'string',
            'max_length' => '1000',
            'required' => true
        ),
        'parent_id' => array(
            'type' => 'int',
            'required' => false
        ),
        'lvl' => array(
            'type' => 'int',
            'required' => false
        ),
    );

    public function getDataMapperRules()
    {
        return $this->mapper_rules;
    }

    protected $id;
    protected $createdAt;
    protected $authorId;
    protected $status;
    protected $body;
    protected $teamId;
    protected $author;
    protected $lvl;
    protected $comments;
    protected $commentsCount;
    protected $addContent;
    protected $images;
    protected $likeCount;
    protected $likeInfo;
    protected $parentId;

    public function getLikeCount()
    {
        if(null == $this->likeCount)
        {
             $this->likeCount = 0;
        }
        
        return $this->likeCount;
    }

    public function setLikeCount($likeCount)
    {
        $this->likeCount = $likeCount;
    }

    public function setId($val)
    {
        $this->id = $val;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setCreatedAt($val)
    {
        $this->createdAt = $val;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setAuthorId($val)
    {
        $this->authorId = $val;
    }

    public function getAuthorId()
    {
        return $this->authorId;
    }

    public function setStatus($val)
    {
        $this->status = $val;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setBody($val)
    {
        $this->body = $val;
    }

    public function getBody()
    {
        return $this->body;
    }

    public function setTeamId($val)
    {
        $this->teamId = $val;
    }

    public function getTeamId()
    {
        return $this->teamId;
    }

    function getAuthor()
    {
        return $this->author;
    }

    function setAuthor($author)
    {
        $this->author = $author;
    }
    
    public  function getParentId() {
return $this->parentId;
}

public  function setParentId($parentId) {
$this->parentId = $parentId;
}

public  function getLvl() {
return $this->lvl;
}

public  function setLvl($lvl) {
$this->lvl = $lvl;
}




    public function getFormatedCreatedAt()
    {
        return $this->getCreatedAt()->format('d.m.Y H:i:s');
    }

    function getComments()
    {
        return $this->comments;
    }

    function setComments($comments)
    {
        $this->comments = $comments;
    }

    function getCommentsCount()
    {
        return $this->commentsCount;
    }

    function setCommentsCount($commentsCount)
    {
        $this->commentsCount = $commentsCount;
    }

    function getAddContent()
    {
        return $this->addContent;
    }

    function setAddContent($addContent)
    {
        $this->addContent = $addContent;
    }

    function getImages()
    {
        return $this->images;
    }

    function setImages($images)
    {
        $this->images = $images;
    }

    function getSmalThumbDir()
    {
        return $this->smalThumbDir;
    }

    function getLargeThumbDir()
    {
        return $this->largeThumbDir;
    }

    function setSmalThumbDir($smalThumbDir)
    {
        $this->smalThumbDir = $smalThumbDir;
    }

    function setLargeThumbDir($largeThumbDir)
    {
        $this->largeThumbDir = $largeThumbDir;
    }
    
    public  function getLikeInfo() {
return $this->likeInfo;
}

public  function setLikeInfo($likeInfo) {
$this->likeInfo = $likeInfo;
}





    public function getHtmlAddContent()
    {
        $dataJson = $this->getAddContent();
        $html = '';
        if (null != $this->getAddContent())
        {
            $data = json_decode($dataJson);

            $html = '<div class="form-group col-sm-12 text-center post-add-content"><div><div class="col-sm-4  text-center"><img src="' . $data->image . '"></div><div class="col-sm-8 text-left"><strong><a href="' . $data->url . '">' . $data->title . '</a></strong><p>' . $data->description . '</p></div></div></div>';
        }


        return $html;
    }

    public function getImagesCollection()
    {
        $collection = array();
        if (null != $this->getImages())
        {
            $images = explode(';', $this->getImages());
            foreach ($images as $image)
            {
                $collection[] = $this->getSmalThumbDir() . '/' . $image;
            }
        }
        return $collection;
    }

    public function getHtmlBody()
    {
        $body = $this->getBody();
        //$wallManager = ServiceLayer::getService('TeamWallManager');
        //getHtmlBody
        $hashPattern = '/#([\p{Pc}\p{N}\p{L}\p{Mn}]+)/u';
        $replace = '<a href="/team/' . $this->getTeamId() . '/hashtag/\\1">\\1</a>';

        $line = preg_replace($hashPattern, $replace, $body);

        return $line;
    }
    
    public function isLikedByUser($user)
    {
        $likeInfo = $this->getLikeInfo();
        

        if(is_array($likeInfo['user_ids']) &&  in_array($user->getId(), $likeInfo['user_ids']))
        {
            return true;
        }
        
        return false;
    }

}
