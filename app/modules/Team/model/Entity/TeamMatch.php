<?php

namespace Webteamer\Team\Model;

/**
 * Detail about match
 */
class TeamMatch {

    protected $players;
    protected $playersStat;

    /**
     * 
     * @param int $index
     * @param \Webteamer\Player\Model\PlayerMatchStat $playerStat
     */
    public function addPlayerStat(int $index, \Webteamer\Player\Model\PlayerMatchStat $playerStat)
    {
        $this->playersStat[$index] = $playerStat;
    }

    public function getPlayersStat()
    {
        return $this->playersStat;
    }

    public function setPlayersStat($playersStat)
    {
        $this->playersStat = $playersStat;
    }

    /**
     * Return info about player stat in match
     * @param int $index team player id
     * @return type
     */
    public function getPlayerStat($index)
    {
        return $this->playersStat[$index];
    }

    public function getPlayers()
    {
        return $this->players;
    }

    /**
     * 
     * @param newVal
     */
    public function setPlayers($newVal)
    {
        $this->players = $newVal;
    }

    public function getMatchDate()
    {
        return $this->players[0]->getMatchDate();
    }

}
