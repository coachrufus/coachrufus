<?php
	
namespace Webteamer\Team\Model;
use Core\EntityMapper as EntityMapper;
use Core\ServiceLayer as ServiceLayer;
		
class TeamMatchLineupPlayer {	

	
	
		
private $repository;
private $mapper_rules  = array(
	'id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => false
							
					),
'created_at' => array(
					'type' => 'datetime',
					'max_length' => '',
					'required' => false
							
					),
'player_id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => false
							
					),
'player_name' => array(
					'type' => 'string',
					'max_length' => '255',
					'required' => false
							
					),
'player_crs' => array(
					'type' => '',
					'max_length' => '',
					'required' => false
							
					),
'player_rating' => array(
					'type' => '',
					'max_length' => '',
					'required' => false
							
					),
'event_id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => false
							
					),
'event_date' => array(
					'type' => 'datetime',
					'max_length' => '',
					'required' => false
							
					),
'attendance_id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => false
							
					),
'lineup_position' => array(
					'type' => 'string',
					'max_length' => '50',
					'required' => false
							
					),
'lineup_name' => array(
					'type' => 'string',
					'max_length' => '50',
					'required' => false
							
					),
'lineup_id' => array(
					'type' => 'int',
					'max_length' => '50',
					'required' => false
							
					),
'team_player_id' => array(
					'type' => 'int',
					'max_length' => '50',
					'required' => false
							
					),
'player_position' => array(
					'type' => 'string',
					'max_length' => '50',
					'required' => false
							
					),

);
	
public function getDataMapperRules()
{
	return $this->mapper_rules;
}

public function getRepository()
{
	if(null == $this->repository)
	{
		$this->repository = ServiceLayer::getService("TeamMatchLineupPlayerRepository"); 
	}
	return $this->repository;
}

public function setRepository($repository)
{
	$this->repository = $repository;
}
			

		
		
		
protected $id;

protected $createdAt;

protected $playerId;

protected $playerName;

protected $playerCrs;

protected $playerRating;

protected $eventId;

protected $eventDate;

protected $attendanceId;

protected $lineupPosition;

protected $lineupName;
protected $lineupId;
protected $teamPlayerId;
protected $teamPlayer;
protected $playerPosition;

/*
 * current lienup scores, it is not global player score!!!
 */
protected $score = array();

				
public function setId($val){$this->id = $val;}

public function getId(){return $this->id;}
	
public function setCreatedAt($val){$this->createdAt = $val;}

public function getCreatedAt(){return $this->createdAt;}
	
public function setPlayerId($val){$this->playerId = $val;}

public function getPlayerId(){return $this->playerId;}
	
public function setPlayerName($val){$this->playerName = $val;}

public function getPlayerName(){return $this->playerName;}
	
public function setPlayerCrs($val){$this->playerCrs = $val;}

public function getPlayerCrs(){return $this->playerCrs;}
	
public function setPlayerRating($val){$this->playerRating = $val;}

public function getPlayerRating(){return $this->playerRating;}
	
public function setEventId($val){$this->eventId = $val;}

public function getEventId(){return $this->eventId;}
	
public function setEventDate($val){$this->eventDate = $val;}

public function getEventDate(){return $this->eventDate;}
	
public function setAttendanceId($val){$this->attendanceId = $val;}

public function getAttendanceId(){return $this->attendanceId;}
	
public function setLineupPosition($val){$this->lineupPosition = $val;}

public function getLineupPosition(){return $this->lineupPosition;}
	
public function setLineupName($val){$this->lineupName = $val;}

public function getLineupName(){return $this->lineupName;}

public  function getLineupId() {
return $this->lineupId;
}

public  function setLineupId($lineupId) {
$this->lineupId = $lineupId;
}

public  function getScore() {
return $this->score;
}

public  function setScore($score) {
$this->score = $score;
}

public  function addScore($key,$score) {
$this->score[$key] = $score;
}

function getTeamPlayerId() {
return $this->teamPlayerId;
}

 function setTeamPlayerId($teamPlayerId) {
$this->teamPlayerId = $teamPlayerId;
}

public  function getTeamPlayer() {
return $this->teamPlayer;
}

public  function setTeamPlayer($teamPlayer) {
$this->teamPlayer = $teamPlayer;
}


public  function getPlayerPosition() {
return $this->playerPosition;
}

public  function setPlayerPosition($playerPosition) {
$this->playerPosition = $playerPosition;
}






public function getAverageScore()
{
    $sum = 0;
    $count = 0;
    foreach($this->getScore() as $score)
    {
        $sum += $score->getScore();
        $count++;
    }
    if($sum == 0)
    {
        return 0;
    }
    
    $avg = str_replace(',','.',(round($sum/$count,1)));
    return $avg;
}




}

		