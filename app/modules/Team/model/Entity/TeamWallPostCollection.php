<?php

namespace CR\Team\Model;

use Core\Collections\Queue;

class TeamWallPostCollection extends Queue {

    protected $allPostLoaded = false;

    public function getAllPostLoaded()
    {
        return $this->allPostLoaded;
    }

    public function setAllPostLoaded($allPostLoaded)
    {
        $this->allPostLoaded = $allPostLoaded;
    }
    
    public function allPostLoaded()
    {
        return $this->allPostLoaded;
    }

}
