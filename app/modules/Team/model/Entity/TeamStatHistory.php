<?php
	
namespace CR\Team\Model;
use Core\EntityMapper as EntityMapper;
use Core\ServiceLayer as ServiceLayer;
		
class TeamStatHistory {	

	
	
		
private $repository;
private $mapper_rules  = array(
	'id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => true
							
					),
'team_id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => false
							
					),
'match_id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => false
							
					),
'created_at' => array(
					'type' => 'datetime',
					'max_length' => '',
					'required' => false
							
					),
'match_date' => array(
					'type' => 'datetime',
					'max_length' => '',
					'required' => false
							
					),
'data' => array(
					'type' => 'string',
					'max_length' => '',
					'required' => false
							
					),

);
	
public function getDataMapperRules()
{
	return $this->mapper_rules;
}

public function getRepository()
{
	if(null == $this->repository)
	{
		$this->repository = ServiceLayer::getService("TeamStatHistoryRepository"); 
	}
	return $this->repository;
}

public function setRepository($repository)
{
	$this->repository = $repository;
}
			

		
		
		
protected $id;

protected $teamId;

protected $matchId;

protected $createdAt;

protected $data;
protected $matchDate;

				
public function setId($val){$this->id = $val;}

public function getId(){return $this->id;}
	
public function setTeamId($val){$this->teamId = $val;}

public function getTeamId(){return $this->teamId;}
	
public function setMatchId($val){$this->matchId = $val;}

public function getMatchId(){return $this->matchId;}
	
public function setCreatedAt($val){$this->createdAt = $val;}

public function getCreatedAt(){return $this->createdAt;}
	
public function setData($val){$this->data = $val;}

public function getData(){return $this->data;}

function getMatchDate() {
return $this->matchDate;
}

 function setMatchDate($matchDate) {
$this->matchDate = $matchDate;
}



}

		