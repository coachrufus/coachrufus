<?php
namespace Webteamer\Team\Model;
/**
 * Statistic data about team
 */

class TeamStat 
{
    private $closedMatchCount;
    private $openedMatchCount;
    
    public  function getClosedMatchCount() {
return $this->closedMatchCount;
}

public  function getOpenedMatchCount() {
return $this->openedMatchCount;
}

public  function setClosedMatchCount($closedMatchCount) {
$this->closedMatchCount = $closedMatchCount;
}

public  function setOpenedMatchCount($openedMatchCount) {
$this->openedMatchCount = $openedMatchCount;
}


    
}
