<?php

namespace Webteamer\Team\Model;


/**
 * Detail about match
 */
class BasketballMatchOverview extends TeamMatch
{
    public function getPlayerPoints($playerIndex,$pointType)
    {
        $playerStat = $this->getPlayerStat($playerIndex);
        
        if(null == $playerStat)
        {
            return null;
        }
        
        $points = null;
        
        
        
        if('points1' == $pointType)
        {
            $points =  $playerStat->getPoints1();
        }
        if('points2' == $pointType)
        {
            $points =  $playerStat->getPoints2();
        }
        if('points3' == $pointType)
        {
            $points =  $playerStat->getPoints3();
        }
        
        return $points;
    }
    
    public function getPlayerTotalPoints($playerIndex)
    {
        $playerStat = $this->getPlayerStat($playerIndex);
        
        if(null == $playerStat)
        {
            return null;
        }
        
        $points = $playerStat->getPoints1()+($playerStat->getPoints2()*2)+($playerStat->getPoints3()*3);
        
        
        
     
        
        return $points;
    }
    
    
    
    public function isPlayerManOfMatch($playerIndex)
    {
        $playerStat = $this->getPlayerStat($playerIndex);

        
        if(null == $playerStat)
        {
            return null;
        }
        
        return $playerStat->isManOfMatch();
    }
	
    
    public function getFirstLinePoints()
    {
        $playerStats = $this->getPlayersStat();
        $playerStat = current($playerStats);
        
        if(null != $playerStat)
        {
             return $playerStat->getFirstLineGoals();
        }
       return 0;
    }
    public function getSecondLinePoints()
    {
        $playerStats = $this->getPlayersStat();
        $playerStat = current($playerStats);
        if(null != $playerStat)
        {
             return $playerStat->getSecondLineGoals();
        }
       return 0;
    }
}


