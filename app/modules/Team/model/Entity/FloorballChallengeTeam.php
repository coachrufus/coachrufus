<?php

namespace CR\Team\Model;

use Core\EntityMapper as EntityMapper;
use Core\ServiceLayer as ServiceLayer;

class FloorballChallengeTeam {

    private $repository;
    private $mapper_rules = array(
        'id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => true
        ),
        'team_id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'coupon_code' => array(
            'type' => 'string',
            'max_length' => '50',
            'required' => false
        ),
        'activation_date' => array(
            'type' => 'datetime',
            'max_length' => '',
            'required' => false
        ),
        'created_at' => array(
            'type' => 'datetime',
            'max_length' => '',
            'required' => false
        ),
        'ip' => array(
            'type' => 'string',
            'max_length' => '50',
            'required' => false
        ),
        'school' => array(
            'type' => 'string',
            'max_length' => '255',
            'required' => false
        ),
        'status' => array(
            'type' => 'string',
            'max_length' => '255',
            'required' => false
        ),
        'register_email' => array(
            'type' => 'string',
            'max_length' => '255',
            'required' => false
        ),
        'register_name' => array(
            'type' => 'string',
            'max_length' => '255',
            'required' => false
        ),
        'city' => array(
            'type' => 'string',
            'max_length' => '255',
            'required' => false
        ),
        'author_id' => array(
            'type' => 'int',
            'required' => false
        ),
    );

    public function getDataMapperRules()
    {
        return $this->mapper_rules;
    }

    protected $id;
    protected $teamId;
    protected $couponCode;
    protected $activationDate;
    protected $ip;
    protected $school;
    protected $status;
    protected $registerEmail;
    protected $registerName;
    protected $city;
    protected $createdAt;
    protected $authorId;

    public function setId($val)
    {
        $this->id = $val;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setTeamId($val)
    {
        $this->teamId = $val;
    }

    public function getTeamId()
    {
        return $this->teamId;
    }

    public function setCouponCode($val)
    {
        $this->couponCode = $val;
    }

    public function getCouponCode()
    {
        return $this->couponCode;
    }

    public function setActivationDate($val)
    {
        $this->activationDate = $val;
    }

    public function getActivationDate()
    {
        return $this->activationDate;
    }

    public function setIp($val)
    {
        $this->ip = $val;
    }

    public function getIp()
    {
        return $this->ip;
    }

    public function getSchool()
    {
        return $this->school;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function getRegisterEmail()
    {
        return $this->registerEmail;
    }

    public function getRegisterName()
    {
        return $this->registerName;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function setSchool($school)
    {
        $this->school = $school;
    }

    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function setRegisterEmail($registerEmail)
    {
        $this->registerEmail = $registerEmail;
    }

    public function setRegisterName($registerName)
    {
        $this->registerName = $registerName;
    }

    public function setCity($city)
    {
        $this->city = $city;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }
    
    public  function getAuthorId() {
return $this->authorId;
}

public  function setAuthorId($authorId) {
$this->authorId = $authorId;
}



}
