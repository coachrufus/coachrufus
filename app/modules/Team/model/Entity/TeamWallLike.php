<?php
	
namespace CR\Team\Model;
use Core\EntityMapper as EntityMapper;
use Core\ServiceLayer as ServiceLayer;
		
class TeamWallLike {	

	
	
		
private $repository;
private $mapper_rules  = array(
	'id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => true
							
					),
'post_id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => false
							
					),
'user_id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => false
							
					),
'created_at' => array(
					'type' => 'datetime',
					'max_length' => '',
					'required' => false
							
					),
'type' => array(
					'type' => 'string',
					'max_length' => '50',
					'required' => false
							
					),

);
	
public function getDataMapperRules()
{
	return $this->mapper_rules;
}

public function getRepository()
{
	if(null == $this->repository)
	{
		$this->repository = ServiceLayer::getService("TeamWallLikeRepository"); 
	}
	return $this->repository;
}

public function setRepository($repository)
{
	$this->repository = $repository;
}
			

		
		
		
protected $id;

protected $postId;

protected $userId;

protected $createdAt;

protected $type;

				
public function setId($val){$this->id = $val;}

public function getId(){return $this->id;}
	
public function setPostId($val){$this->postId = $val;}

public function getPostId(){return $this->postId;}
	
public function setUserId($val){$this->userId = $val;}

public function getUserId(){return $this->userId;}
	
public function setCreatedAt($val){$this->createdAt = $val;}

public function getCreatedAt(){return $this->createdAt;}
	
public function setType($val){$this->type = $val;}

public function getType(){return $this->type;}

}

		