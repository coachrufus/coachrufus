<?php

namespace Webteamer\Team\Model;

use Core\ServiceLayer;
use Webteamer\Locality\Model\Playground;
use Webteamer\Locality\Model\PlaygroundAlias;

class TeamManager {

    private $repository;
    private $teamPlayerManager;
    private $playgroundManger;
    private $settingRepository;
    private $translator;
    
    public function __construct($repository, $teamPlayerManager, $playgroundManger,$settingRepository)
    {

        $this->setRepository($repository);
        $this->setTeamPlayerManager($teamPlayerManager);
        $this->setPlaygroundManger($playgroundManger);
        $this->setSettingRepository($settingRepository);
    }
    
    function getTeamPlayerManager()
    {
        return $this->teamPlayerManager;
    }

    function setTeamPlayerManager($teamPlayerManager)
    {
        $this->teamPlayerManager = $teamPlayerManager;
    }

    function getSettingRepository()
    {
        return $this->settingRepository;
    }

    function setSettingRepository($settingRepository)
    {
        $this->settingRepository = $settingRepository;
    }
    
    public function getFloorballChallengeRepository()
    {
        return ServiceLayer::getService('FloorballChallengeTeamRepository');
    }
    
    public function getPromoTeamRepository()
    {
        return ServiceLayer::getService('PromoTeamRepository');
    }

    public function getPlayerMatchManager()
    {
        return $this->playerMatchManager;
    }

    public function setPlayerMatchManager($playerMatchManager)
    {
        $this->playerMatchManager = $playerMatchManager;
    }
    
    public function getTranslator()
    {
        return $this->translator;
    }

    public function setTranslator($translator)
    {
        $this->translator = $translator;
    }

   public function getLevelEnum()
    {
        $translator =  $this->getTranslator();
        return array(
            'beginner' => $translator->translate('Beginner') ,
            'advanced' => $translator->translate('Advanced') ,
            'expert' => $translator->translate('Expert') , 
            'league' => $translator->translate('League') , 
       );
    }

    public function getPermissionEnum()
    {
        $translator = ServiceLayer::getService('translator');
         return array(
            '' => $translator->translate(''),
            'ADMIN' => $translator->translate('Admin'),
            'PLAYER' => $translator->translate('Player'),
            'GOAL_KEEPER' => $translator->translate('Goal Keeper'),
            'GUEST' => $translator->translate('Guest'),
        );
    }

    public function getStatusEnum()
    {
        $translator = ServiceLayer::getService('translator');
        return array(
            'public' => $translator->translate('Public'),
            'closed' => $translator->translate('Closed'),
            'private' => $translator->translate('Private'),
        );
    }
    

    
    
    public function getPlayerStatusEnum()
    {
       $translator = ServiceLayer::getService('translator');
        return array(
            'confirmed' => $translator->translate('Confirmed'),
            'waiting-to-confirm' => $translator->translate('Waiting to confirm'), // invitaiton was sent from admin to player 
            'decline' => $translator->translate('Decline'),
            'unknown' => $translator->translate('Unknown'),
            'unactive' => $translator->translate('Unactive'),
            'unconfirmed' => $translator->translate('Unconfirmed'), // invitaiton was sent from player to admin 
        );
    }
    
    public function getGenderEnum()
    {
        $translator = $this->getTranslator();
        return array(
            'man' => $translator->translate('Man'),
            'woman' => $translator->translate('Woman'),
            'mixed' => $translator->translate('Mixed'),
        );
    }
    
    public function getAgeGroupEnum()
    {
         $translator = $this->getTranslator();
        return array(
            '0-13' =>' 0-13',
            '13-17' => '13-17',
            '18-24' =>  '18-24',
            '25-34' =>  '25-34',
            '35-44' =>  '35-44',
            '45-54' =>  '45-54',
            '55-64' =>  '55-64',
            '65+' =>  '65+',
        );
        
    }
    
   
    
    
    public function userCanManageTeam($userIdentity,$team)
    {
        /*
        $teamRoles = $userIdentity->getTeamRoles();
        if($teamRoles[$team->getId()] != 'admin' and $teamRoles[$team->getId()] != 'manager')
        {
            return false;
        }
         * */
        return true;
    }

    /**
     * Return  teams, where is player a meber of 
     * @param type $user
     * @return type
     */
    public function getUserTeams($user)
    {
        $repo = ServiceLayer::getService('TeamRepository');
        $records = $repo->findUserTeams($user);
        
        $list = array();
        foreach($records as $record)
        {
            $list[$record->getId()] = $record;
        }

        return $list;
    }
    
     public function getUserUnconfirmedTeams($user)
    {
        $repo = ServiceLayer::getService('TeamRepository');
        $records = $repo->findUserUnconfirmedTeams($user);
        
        $list = array();
        foreach($records as $record)
        {
            $list[$record->getId()] = $record;
        }

        return $list;
    }
    
    
     public function getUserWaitingTeams($user)
    {
        $repo = ServiceLayer::getService('TeamRepository');
        $list = $repo->findUserWaitingTeams($user);
        return $list;
    }
    
    
    
    public function getUserFollowingTeams($user)
    {
        $repo = ServiceLayer::getService('TeamRepository');
        $records = $repo->getUserFollowingTeams($user);

        return $records;
    }
    
    
    /**
     * Get info about team, members count, followers count
     * @param type $team
     */
    public function getTeamsInfo($teams)
    {
       $info = $this->getRepository()->getTeamsInfo($teams);
       return $info;
    }
    
    public function filterActiveTeams($identity,$teams)
    {
        $activeTeams = array();
        foreach($teams as $team)
        {
            if($identity->hasTeamPermission('activePlayer',$team))
            {
                $activeTeams[] = $team;
            }
        }
        
        return $activeTeams;
    }

    public function getUserTeam($team_id, $user)
    {
        $repo = ServiceLayer::getService('TeamRepository');
        $record = $repo->findOneBy(array('author_id' => $user->getId(), 'id' => $team_id));

        return $record;
    }
    
    public function removeTeamPlayer($player)
    {
        $this->getRepository()->deleteTeamPlayer($player);
    }
    
    public function saveTeamPlayer($data)
    {
        $repo = $this->getRepository();
        $teamPlayer = new TeamPlayer();
 
        if(array_key_exists('id', $data))
        {
             $teamPlayer->setId($data['id']);
        }
        else
        {
             $teamPlayer->setCreatedAt(new \DateTime());
        }
        
         if(array_key_exists('team_id', $data))
        {
             $teamPlayer->setTeamId($data['team_id']);
        }
         if(array_key_exists('player_id', $data))
        {
             $teamPlayer->setPlayerId($data['player_id']);
        }
         if(array_key_exists('status', $data))
        {
             $teamPlayer->setStatus($data['status']);
        }
         if(array_key_exists('level', $data))
        {
             $teamPlayer->setLevel($data['level']);
        }
         if(array_key_exists('team_role', $data))
        {
             $teamPlayer->setTeamRole($data['team_role']);
        }

         if(array_key_exists('first_name', $data))
        {
             $teamPlayer->setFirstName($data['first_name']);
        }
        
        if(array_key_exists('last_name', $data))
        {
             $teamPlayer->setLastName($data['last_name']);
        }
        
          if(array_key_exists('email', $data))
        {
              $teamPlayer->setEmail($data['email']);
        }
        
          if(array_key_exists('player_number', $data))
        {
              $teamPlayer->setPlayerNumber($data['player_number']);
        }
        
         

            $repo->saveTeamPlayer($teamPlayer);
            return $teamPlayer;
    }
    
    /**
     * Save team player invitation
     */
    public function saveTeamPlayerInvitation($data)
    {
        $repo = $this->getRepository();
        $repo->saveTeamPlayerInvitation($data);
    }
    
    /**
     * return  team players except unactive players
     * @param type $team_id
     * @return type
     */
    public function getTeamPlayers($team_id)
    {
        $repo = ServiceLayer::getService('TeamRepository');

        $list = $repo->findTeamPlayers($team_id);
        return $list;
    }
    
     /**
     * return only active  team players
     * @param type $team_id
     * @return type
     */
    public function getActiveTeamPlayers($team_id)
    {
        $repo = ServiceLayer::getService('TeamRepository');
        $list = $repo->findActiveTeamPlayers($team_id);
        return $list;
    }
    
    /**
     * return only registred players
     * @param type $teamId
     * @return type
     */
    public function getActiveRegistredTeamPlayers($teamId)
    {
       $players = $this->getActiveTeamPlayers($teamId);
       $list = array();
       foreach($players as $player)
       {
           if($player->getPlayerId() > 0)
           {
               $list[] = $player;
           }
       }
       
       return $list;
    }
    
    
      /**
     * return all  team players
     * @param type $team_id
     * @return type
     */
    public function getAllTeamPlayers($team_id,$options = array())
    {
        $repo = ServiceLayer::getService('TeamRepository');
        $list = $repo->findAllTeamPlayers($team_id,$options);
        return $list;
    }
    
    public function getTeamAdminList($team)
    {
        $repo = ServiceLayer::getService('TeamRepository');
        $list = $repo->findTeamAdmins($team->getId());
        return $list;
    }
    
    /**
     * find team player by his database id 
     * @param int $id
     * @return Webteamer\Team\Model\TeamPlayer;
     */
     public function findTeamPlayerById($id)
    {
        //$repo = ServiceLayer::getService('TeamRepository');
         
        $player = $this->getTeamPlayerManager()->getTeamPlayerById($id);
        return $player;
    }
    
     public function findTeamPlayerByEmail($team,$email)
    {
        $repo = ServiceLayer::getService('TeamRepository');

        $player = $repo->findTeamPlayerByEmail($team->getId(),$email);
        return $player;
    }
    
     public function findTeamPlayerByPlayerId($team,$playerId)
    {
        $repo = ServiceLayer::getService('TeamRepository');

        $player = $repo->findTeamPlayerByPlayerId($team->getId(),$playerId);
        return $player;
    }
    
   

    public function findPlayer($criteria = array())
    {

        $repo_criteria = array();
        if (array_key_exists('name', $criteria))
        {
            $repo_criteria['name'] = $criteria['name'];
        }
        if (array_key_exists('player_id', $criteria))
        {
            $repo_criteria['player_id'] = $criteria['player_id'];
        }
       

        $player_repo = ServiceLayer::getService('PlayerRepository');
        $list = $player_repo->findBy($repo_criteria);
        return $list;
    }

    public function updatePlayerFromArray($id, $data)
    {
        //$object = $this->getRepository()->findTeamPlayer($id);

        $this->getRepository()->updateTeamPlayerFromArray($id, $data);
    }

    /*
      public function createTeamPlayer($team,$player)
      {
      $repo =  ServiceLayer::getService('TeamRepository');
      $teamPlayer = new TeamPlayer();
      $teamPlayer->setPlayerId($player->getId());
      $teamPlayer->setTeamId($team->getId());
      $repo->saveTeamPlayer($teamPlayer);
      }
     */

    
    public function getNearestEvent($teams)
    {
        $eventRepo = ServiceLayer::getService('EventRepository');

        $team_ids = array();
        foreach ($teams as $team)
        {
            $team_ids[] = $team->getId();
        }

        $events = $eventRepo->findNearestTeamEvents($team_ids);

        return $events;
    }

    

    /**
     * 
     * @param type $team
     * @return Playground
     */
    public function getTeamPlaygrounds($team)
    {
        $manager = $this->getPlaygroundManger();
        $playgrounds = $manager->findPlaygroundsByTeam($team);
        return $playgrounds;
    }

    /**
     * 
     * @param type $team
     * @return PlaygroundAlias
     */
    public function getTeamPlaygroundsAlias($team)
    {
        $manager = $this->getPlaygroundManger();
        $playgrounds = $manager->findPlaygroundsAliasByTeam($team);
        return $playgrounds;
    }

    public function getTeamsByPlayground($playground)
    {
        $teams = $this->getRepository()->findTeamsByPlayground($playground);
        return $teams;
    }
    
     public function getTeamsByArea($center, $distance = 5)
    {
        $teams = $this->getRepository()->findTeamsByArea($center, $distance);
        return $teams;
    }

    public function handleFormRequest($post_data, $form, $validator)
    {
        $form->bindData($post_data);
        $validator->setData($post_data);
        $validator->validateData();

        $playgroundManger = $this->getPlaygroundManger();
        if (!$validator->hasErrors())
        {
            $entity = $form->getEntity();
            $repository = $this->getRepository();
            $team_id = $repository->save($entity);
            
            if(null == $entity->getId())
            {
                 $entity->setId($team_id);
            }
            //save playgrounds
            //vymaze vsetky stare 
            $this->getRepository()->deleteTeamPlaygroundAlias($entity);

            //exist playgrounds
            if (array_key_exists('exist_playgrounds', $post_data) && null == $post_data['playgrounds'][0]['locality_json_data'])
            {
                foreach ($post_data['exist_playgrounds'] as $playgroundAliasId => $playgroundAliasIdData)
                {
                    $this->getRepository()->saveTeamPlaygroundAlias($entity->getId(), $playgroundAliasId);
                    
                    //update playground name
                    $existPlaygroundAlias = $playgroundManger->findPlaygroundAliasById($playgroundAliasId);
                    $existPlayground = $playgroundManger->findPlaygroundById($existPlaygroundAlias->getPlaygroundId());
                    $existPlayground->setName($playgroundAliasIdData);
                    $playgroundManger->savePlayground($existPlayground);
                }
            }

           
            //save new playground and create alias and locality
            if (array_key_exists('playgrounds', $post_data) && null != $post_data['playgrounds'])
            {
                
                $savedPlaygroundAlias = array();

                foreach ($post_data['playgrounds'] as $playground_data)
                {
                    if(null != $playground_data['locality_json_data'])
                    {
                        $playground_data['author_id'] = $entity->getAuthorId();
                        $playground_data['team_id'] = $entity->getId();
                        $result = $playgroundManger->savePlaygroundAlias($playground_data);
                        $savedPlaygroundAlias[] = $result['entity'];
                    }
                   
                }

                foreach ($savedPlaygroundAlias as $alias)
                {
                    $this->getRepository()->saveTeamPlaygroundAlias($entity->getId(), $alias->getId());
                }
            }


            return array('STATUS' => 'SUCCESS','TEAM_ID' => $team_id );
        }
        else
        {
            return array('STATUS' => 'ERROR');
        }
    }

    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * 
     * @param newVal
     */
    public function setRepository($repository)
    {
        $this->repository = $repository;
    }

   

    public function getPlaygroundManger()
    {
        return $this->playgroundManger;
    }

    /**
     * 
     * @param newVal
     */
    public function setPlaygroundManger($newVal)
    {
        $this->playgroundManger = $newVal;
    }

    /**
     * Find team by id
     * @param type $id
     * @return Webteamer\Team\Model\Team
     */
    public function findTeamById($id)
    {
        $team = $this->getRepository()->find($id);
        
        if($team != null && $team->getStatus() == 'deleted')
        {
            throw new \Exception();
        }
        
        return $team;
    }
    
    public function findTeamByHash($hash)
    {
         $id = substr($hash,3);
         $id = substr($id,0,-8);
         return $this->findTeamById($id);
    }

    public function createTeamMatches($playerMatches)
    {
        $list = array();
        foreach ($playerMatches as $playerMatch)
        {
            //$teamMatch = new TeamManger();
            $list[$playerMatch->getMatchId()][] = $playerMatch;
        }

        //var_dump($list);


        $matches = array();
        foreach ($list as $l)
        {
            $teamMatch = new TeamMatch();
            $teamMatch->setPlayers($l);
            $matches[] = $teamMatch;
        }

        return $matches;
    }
    
     /**
     * Return  teams by locality
     * @param type Webteamer\Locality\Model\Locality $locality
     * @return type
     */
    public function getLocalityTeams($locality)
    {
       $teams = $this->getRepository()->findTeamsByLocality($locality);
       return $teams;
    }
    
    public function sendAddPlayerNotifyEmail($data)
    {
        $notification = new Notification();
        $notification->setSubject('Coach Rufus - Verify Your Membership');
        $notification->setMessage('Verify <a href=""></a>');

        try 
        {
            $notification->setRecipient($data['email']);          
            $this->object->sendNotification($notification);
        }
        catch(Exception $exception)
        {

        }
    }
    
    public function sendNewPlayerNotification($playerData)
    {
        
    }
    
    public function saveTeam($team)
    {
        return $this->getRepository()->save($team);
    }
    
    public function saveSetting($setting)
    {
        $this->getSettingRepository()->save($setting);
    }
    
    public function getTeamSettings($team)
    {
        $settings = $this->getSettingRepository()->getTeamSettings($team);
        return $settings;
    }
    
    public function deleteTeamSettingsGroup($team,$groupName)
    {
        $this->getSettingRepository()->deleteBy(array('team_id' => $team->getId(),'settings_group' => $groupName));
    }

    /*
      public function getTeamMatches($team)
      {
      $teamMatches = $matchManager = $this->getRepository()->findTeamMatches($team);

      return $teamMatches;

      }
     */
    
    public function isFloorballChallengeTeam($team)
    {
       $record = $this->getFloorballChallengeRepository()->findOneBy(array('team_id' => $team->getId()));
       
       if(null != $record)
       {
           return true;
       }
       return false;
    }
    
    /**
     * Check if team has sport with pro statitstics
     * @param type $team
     */
    public function teamHasProSport($team)
    {
        $sportManager = \Core\ServiceLayer::getService('SportManager');
        $hasProSport = false;
        $proSports =  $sportManager->getProSportIds();
        if(in_array($team->getSportId(), $proSports))
        {
            $hasProSport = true;
        }
        
        return $hasProSport;
    }
    
    public function createPromoTeam(Team $team,$promoCode) 
    {
        $repo = $this->getPromoTeamRepository();
        $entity = new \CR\Team\Model\PromoTeam();
        $entity->setAuthorId($team->getAuthorId());
        $entity->setCreatedAt(new \DateTime());
        $entity->setPromoCode($promoCode);
        $entity->setStatus('CREATED');
        $entity->setTeamId($team->getId());
        
        $repo->save($entity);
    }
    
     public function isPromoTeam($team,$code)
    {
       
        if(null != $team)
        {
             $record = $this->getPromoTeamRepository()->findOneBy(array('team_id' => $team->getId(),'promo_code' => $code));
            if(null != $record && $record->getStatus() == 'CREATED')
            {
                return true;
            }
        }
      
       return false;
    }
}
