<?php

namespace CR\Team\Model;

use AclException;
use Core\Manager;
use Core\ServiceLayer;
use CR\Team\Model\FloorballChallengeTeam;
use DateTime;

class FlchManager extends Manager {

    public function createCouponCode($data)
    {
        $repo = ServiceLayer::getService('FloorballChallengeTeamRepository');
        $couponRepo = ServiceLayer::getService('CouponRepository');
        
        $security = ServiceLayer::getService('security');
        $userIdentity = $security->getIdentity();
        $user = $userIdentity->getUser();
        
        $coupon  = $couponRepo->findOneBy(array('name' => 'FLOORBALL','charges' => 1));
        $coupon->setCharges(0);
        $couponRepo->save($coupon);
         
        $flchTeam = new FloorballChallengeTeam();
        $flchTeam->setCouponCode($coupon->getCode());
        $flchTeam->setIp($_SERVER['REMOTE_ADDR']);
        $flchTeam->setAuthorId($user->getId());
        $flchTeam->setCreatedAt(new \DateTime());
        $flchTeam->setCity($data['city']);
        $flchTeam->setSchool($data['school']);
        $flchTeam->setStatus('registration');
        $flchTeamId = $repo->save($flchTeam);
        
        return $coupon->getCode().'@'.$flchTeamId;
    }
    
    public function createTeamFromCode($code,$teamId)
    {
        $codeParts = explode('@', $code);
        $flchTeamId = $codeParts[1];
        $couponCode = $codeParts[0];

        $registerFlchTeam = $this->getRepository()->find($flchTeamId);


        if ($couponCode == $registerFlchTeam->getCouponCode())
        {
            if (null == $registerFlchTeam->getActivationDate())
            {
                $registerFlchTeam->setActivationDate(new DateTime());
                $this->getRepository()->save($registerFlchTeam);
            }

            $flchTeam = new FloorballChallengeTeam();
            $flchTeam->setCouponCode($couponCode);
            $flchTeam->setTeamId($teamId);
            $flchTeam->setIp($_SERVER['REMOTE_ADDR']);
            $flchTeam->setRegisterEmail($registerFlchTeam->getRegisterEmail());
            $flchTeam->setCreatedAt(new DateTime());
            $flchTeam->setRegisterName($registerFlchTeam->getRegisterName());
            $flchTeam->setSchool($registerFlchTeam->getSchool());
            $flchTeam->setCity($registerFlchTeam->getCity());
            $flchTeam->setStatus('active');
            $id = $this->getRepository()->save($flchTeam);
            return $id;
        }
        else
        {
            throw new AclException();
        }
    }
    
    public function createProPackage($team)
    {
        $security = ServiceLayer::getService('security');
        $identity = $security->getIdentity();
        
        $end = new DateTime('2017-06-31');
        $end->setTime(0, 0, 0);
        $start = new DateTime();
        $start->setTime(0, 0, 0);
        $pm = ServiceLayer::getService('PackageManager');
        $proPackage = $pm->getPackageWithProducts(5);
        ServiceLayer::getService('UserPackageManager')->addUserPackage([
            'user_id' => $identity->getUser()->getId(),
            'package_id' => 5,
            'team_id' => $team->getId(),
            'team_name' => $team->getName(),
            'date' => new DateTime(),
            'start_date' => $start,
            'end_date' => $end,
            'name' => 'PRO',
            'note' => 'FLOORBALL',
            'level' => 5,
            'products' => json_encode($proPackage['products']),
            'player_limit' => 100,
            'guid' => \Core\GUID::create()
        ]);
    }
    
    public function getFlchTeam($team)
    {
       return $this->getRepository()->findOneBy(array('team_id' => $team->getId(),'status' => 'active' ));
    }
    
    public function getFlchTeams()
    {
       return $this->getRepository()->findby(array('status' => 'active' ));
    }
    
    public function save($flch)
    {
        $this->getRepository()->save($flch);
    }

}
