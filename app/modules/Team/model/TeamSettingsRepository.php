<?php
namespace Webteamer\Team\Model;
use Core\Repository as Repository;




class TeamSettingsRepository extends Repository
{
    public function getTeamSettings($team)
    {
        $settingsData = $this->getStorage()->findBy(array('team_id' => $team->getId()));
        $list = array();
        if(null != $settingsData)
        {
            $list = $this->createObjectList($settingsData);
        }
        return $list;

    }
}
