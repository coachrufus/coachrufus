<?php

namespace CR\Team\Model;

use Core\Manager;
use DateTime;

/**
 * @author Marek Hubacek
 * @version 1.0
 * @created 18-12-2015 11:11:39
 */
class TeamFollowManager extends Manager {

   public function createFollower($team,$user)
   {
       $this->getRepository()->createFollower($team,$user);
   }


}

?>