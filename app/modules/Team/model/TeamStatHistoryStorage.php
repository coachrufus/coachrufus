<?php
namespace CR\Team\Model;
use Core\DbStorage;
class TeamStatHistoryStorage extends DbStorage {
    
	
       public function findNearestPreviewMatch($date,$teamId)
       {


           $data = \Core\DbQuery::prepare('
               SELECT * from team_stat_history where team_id = :team_id and match_date <= :match_date order by match_date desc limit 0,1')
		->bindParam('team_id', $teamId)
		->bindParam('match_date', $date->format('Y-m-d'))
		->execute()
		->fetchOne(\PDO::FETCH_ASSOC);
		return $data;
           
           
       }
    
       
	
}