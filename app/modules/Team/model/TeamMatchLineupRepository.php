<?php

namespace Webteamer\Team\Model;

use Core\Repository as Repository;

class TeamMatchLineupRepository extends Repository {

    protected $playerRepository;

    public function __construct($storage, $factory, $playerStorage, $playerFactory)
    {
        $this->storage = $storage;
        $this->factory = $factory;
        $playerRepository = new Repository($playerStorage, $playerFactory);
        $this->setPlayerRepository($playerRepository);
    }

    public function getPlayerRepository()
    {
        return $this->playerRepository;
    }

    public function setPlayerRepository($playerRepository)
    {
        $this->playerRepository = $playerRepository;
    }

    public function savePlayer($object)
    {
        return $this->getPlayerRepository()->save($object);
    }
    
    public function findEventLineup($event)
    {
        $lineup = $this->findOneBy(array('event_id' => $event->getId(), 'event_date' => $event->getCurrentDate()->format('Y-m-d 00:00:00') ));
        if(null != $lineup)
        {
             return $lineup;
        }
        else 
        {
           return  array();
        }
       
    }
    
    public function findEventLineupPlayers($lineup)
    {
        if(null != $lineup)
        {
             $list = $this->getPlayerRepository()->findBy(array('lineup_id' => $lineup->getId() ),array('order' => 'player_name asc'));
            return $list;
        }
        else
        {
            return array();
        }
       
    }
    
    public function findEventLineupPlayer($lineup,$playerId)
    {
        $player = $this->getPlayerRepository()->findOneBy(array('lineup_id' => $lineup->getId(), 'id' => $playerId ));
        return $player;
    }
    
    public function findLineupPlayer($id)
    {
        $player = $this->getPlayerRepository()->find($id);
        return $player;
    }
    
    public function updateLineupPlayer($id,$lineupData)
    {
        if(is_array($lineupData))
        {
            $this->getPlayerRepository()->getStorage()->update($id, $lineupData);
        }
        else
        {
            $this->getPlayerRepository()->getStorage()->update($id, array('lineup_position' =>$lineupData['lineup_position'] ));
        }
        
        
    }
    public function removeLineupPlayer($id)
    {
        $this->getPlayerRepository()->delete($id);
    }
    public function updateLineup($id,$data)
    {
        $this->getStorage()->update($id, $data);
    }
    
    /**
     * Find events lineup for specific date
     */
    public function findEventsLineups($eventDates)
    {
        $list = array();
        $data = $this->getStorage()->findEventsLineups($eventDates);
        if(null != $data)
        {
            $list = $this->createObjectList($data);
        }
        return $list;
    }
    
    /**
     * 
     * @param type $team
     * @param type $dateFrom
     * @param type $dateTo
     * @return array array of linups with information about goals
     */
    public function findTeamMatchResults($team,$dateFrom,$dateTo)
    {
        $data = $this->getStorage()->getTeamMatchResults($team->getId(),$dateFrom->format('Y-m-d'),$dateTo->format('Y-m-d'));
        
        $list = array();
        foreach($data as $d)
        {
            $eventDate = new \DateTime($d['event_date']);
            $index = $eventDate->format('Ymd').'-'.$d['event_id'];
            
            
            $list[$index][$d['lineup_position']] = $d['points'];
        }
        
        return $list;
    }
    
        public function  findTeamMatchStatusInfo($teamId)
    {
         $data = $this->getStorage()->findTeamMatchStatusInfo($teamId);
         
         $teamStat = new TeamStat();
         foreach($data as $d)
         {
             if($d['status'] == 'closed')
             {
                 $teamStat->setClosedMatchCount($d['count']);
             }
             else
             {
                 $teamStat->setOpenedMatchCount($d['count']);
             }
         }
         
         
         
         return $teamStat; 
    }
    
    public function findTeamMatchs($team,$dateFrom,$dateTo)
    {
        $data = $this->getStorage()->getTeamMatchs($team->getId(),$dateFrom->format('Y-m-d'),$dateTo->format('Y-m-d'));
        
        $list = $this->createObjectList($data);
        return $list;
    }
    
    
    public function findPlayerNearestPreviewMatch($date,$playerId,$count = 1)
   {
       $data = $this->getStorage()->findPlayerNearestPreviewMatch($date,$playerId,$count);
       return $this->createObjectList($data);
   }
}
