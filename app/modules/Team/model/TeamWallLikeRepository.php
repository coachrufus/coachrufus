<?php
namespace CR\Team\Model;
use Core\Repository as Repository;


ServiceLayer::addService('TeamWallLikeRepository',array(
'class' => "CR\Team\Model\TeamWallLikeRepository",
	'params' => array(
		new \Core\DbStorage('team_wall_like'),
		new \Core\EntityMapper('CR\Team\Model\TeamWallLike')
	)
));

class TeamWallLikeRepository extends Repository
{

}
