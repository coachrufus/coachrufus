<?php
namespace Webteamer\Team\Model;
use Core\Repository as Repository;


ServiceLayer::addService('TeamGenRepository',array(
'class' => "Webteamer\Team\Model\TeamGenRepository",
	'params' => array(
		new \Core\DbStorage('team'),
		new \Core\EntityMapper('Webteamer\Team\Model\TeamGen')
	)
));

class TeamGenRepository extends Repository
{

}
