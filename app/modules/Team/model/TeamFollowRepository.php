<?php

namespace CR\Team\Model;

use Core\DbStorage;
use Core\EntityMapper;
use Core\Repository as Repository;
use Core\ServiceLayer;


class TeamFollowRepository extends Repository {
    
  public function __construct($storage) 
  {
        $this->storage = $storage;
   }
   
   public function createFollower($team,$user)
   {
       $data['user_id'] = $user->getId();
       $data['team_id'] = $team->getId();
       $data['created_at'] = date('Y-m-d H:i:s');
       $this->getStorage()->create($data);
   }
}