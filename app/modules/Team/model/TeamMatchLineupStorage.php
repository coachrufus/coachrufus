<?php
namespace CR\Team\Model;
use Core\DbStorage;
class TeamMatchLineupStorage extends DbStorage {
    
	
      public function findPlayerNearestPreviewMatch($date,$playerId,$count = 1)
       {


           $data = \Core\DbQuery::prepare('
                SELECT tl.* from team_match_lineup tl
                LEFT JOIN team_match_lineup_player tp ON tl.id = tp.lineup_id
                where tp.team_player_id = :player_id
                and tl.event_date < :match_date
                and tl.status = "closed" 
                group by tl.id
                order by tl.event_date desc 
                limit 0,'.$count)
		->bindParam('player_id', $playerId)
		->bindParam('match_date', $date->format('Y-m-d'))
		->execute()
		->fetchAll(\PDO::FETCH_ASSOC);
		return $data;
           
           
       }
    
    
        public function findLastTeamClosedLinups($teamId)
        {
                $data = \Core\DbQuery::prepare('
                select * from team_match_lineup l 
                WHERE l.team_id = :team_id and l.status = "closed"
                order by id desc
                limit 0,4')
		->bindParam('team_id', $teamId)
		->execute()
		->fetchAll(\PDO::FETCH_ASSOC);
		return $data;
        }
        
        public function findEventsLineups($eventDates)
        {
            $eventIds = array();
            $dates = array();
            foreach($eventDates as $eventId => $eventDates)
            {
                $eventIds[] = $eventId;
                foreach($eventDates as $eventDate)
                {
                    $dates[] = $eventDate;
                }
            } 
            $eventIds = array_unique($eventIds);
            $dates = array_unique($dates);
            
            $dateCriteria = 'DATE_FORMAT(event_date,"%Y-%m-%d") ="'.implode('" OR DATE_FORMAT(event_date,"%Y-%m-%d") = "',$dates).'"';
            $eventCriteria = 'event_id = '.implode(' OR event_id = ',$eventIds);
            
            $sql = ' SELECT * from team_match_lineup  WHERE ('.$dateCriteria.') AND ('.$eventCriteria.')';
            
             $data = \Core\DbQuery::prepare($sql)
		->execute()
		->fetchAll(\PDO::FETCH_ASSOC);
		return $data;
        }
        
        
        /*
         * Get data about gols for linuep
         */
        public function getTeamMatchResults($teamId,$dateFrom,$dateTo)
        {
            
            $sql = 'SELECT lineup_id,
                            lineup_position,
                            event_date,
                            event_id,
                            count(hit_type) as points
                     FROM player_timeline_event
                     WHERE team_id = :team_id
                       AND hit_type = "goal"
                       AND DATE(event_date) >= :date_from
                       AND DATE(event_date) <= :date_to
                     GROUP BY lineup_id,
                              lineup_position,
                              event_date';
            $data = \Core\DbQuery::prepare($sql)
                ->bindParam('team_id', $teamId)
                ->bindParam('date_from', $dateFrom)
                ->bindParam('date_to', $dateTo)
		->execute()
		->fetchAll(\PDO::FETCH_ASSOC);
		return $data;
        }
        
        public function getTeamMatchs($teamId,$dateFrom,$dateTo)
        {
            
            $sql = 'SELECT *
                     FROM team_match_lineup
                     WHERE team_id = :team_id
                       AND DATE(event_date) >= :date_from
                       AND DATE(event_date) <= :date_to order by event_date';
            $data = \Core\DbQuery::prepare($sql)
                ->bindParam('team_id', $teamId)
                ->bindParam('date_from', $dateFrom)
                ->bindParam('date_to', $dateTo)
		->execute()
		->fetchAll(\PDO::FETCH_ASSOC);
		return $data;
        }
        
        public function findTeamMatchStatusInfo($teamId)
        {
            $sql = 'SELECT status, 
                            Count(*)  as count
                     FROM   team_match_lineup 
                     WHERE  team_id = :id 
                     GROUP  BY status ';
            $data = \Core\DbQuery::prepare($sql)
                ->bindParam('id', $teamId)
		->execute()
		->fetchAll(\PDO::FETCH_ASSOC);
		return $data;
        }
     
	
	
}