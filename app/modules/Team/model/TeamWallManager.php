<?php

namespace CR\Team\Model;

use Core\ServiceLayer;

class TeamWallManager extends \Core\Manager {

    protected $commentRepository;
    protected $smalThumbDir = 'img/wall/thumb_90_90';
    protected $largeThumbDir = 'img/wall/thumb_800_800';
    protected $postPerPage = 2;

    public function __construct($repository = null, $commentRepo)
    {
        parent::__construct($repository);
        
        $this->setCommentRepository($commentRepo);
    }

    function getCommentRepository()
    {
        return $this->commentRepository;
    }

    function setCommentRepository($commentRepository)
    {
        $this->commentRepository = $commentRepository;
    }
    
    public function savePostWall($post)
    {
        $postId = $this->getRepository()->save($post);
        $post->setId($postId);
    }
    
    public function deletePostWall($post)
    {
        $this->getRepository()->delete($post->getId());
    }

    public function savePostWallComment($comment)
    {
        return $this->getCommentRepository()->save($comment);
    }

  
     public function savePostWallLike($entity)
    {
        $this->getRepository()->savePostLike($entity);
    }
  
    
    public  function getPostPerPage() {
return $this->postPerPage;
}

public  function setPostPerPage($postPerPage) {
$this->postPerPage = $postPerPage;
}


    private function buildPostCollection($posts)
    {
        $displayPosts = array_slice($posts,0,$this->getPostPerPage());
        $nextPost = array_slice($posts,$this->getPostPerPage(),1);
        
        //t_dump($displayPosts);
        
        
        $collection = new TeamWallPostCollection($displayPosts);
        if(empty($nextPost))
        {
            $collection->setAllPostLoaded(true);
        }
        return $collection;
    }


    /**
     * Get last wall posts
     * @param \Webteamer\Team\Model\Team $team
     * @return type
     */
    public function getLastWallPosts($team)
    {
        $posts = $this->getRepository()->getLastWallPosts($team,$this->getPostPerPage()+1);
        $collection = $this->buildPostCollection($posts);
        return $collection;
    }
    
    /**
     * Get wall posts from specific place
     * @param type $team
     * @param type $start
     * @return type
     */
    public function getWallPosts($team,$start)
    {
        $posts = $this->getRepository()->getWallPosts($team,$start,$this->getPostPerPage()+1);
        $collection = $this->buildPostCollection($posts);
        return $collection;
    }
    
    
    
    public function getPostById($id)
    {
        return $this->getRepository()->find($id);
    }
    
    public function getPostComments($post)
    {
        $comments = $this->getCommentRepository()->findPostComments($post);
        return $comments;
    }
    
    public function getHashtagWallPosts($team, $hashtag,$start=0)
    {
         $posts = $this->getRepository()->getHashtagWallPosts($team,$hashtag,$start);
         return $posts;
    }
    
    public function getPostLikesCount($post)
    {
        $count = $this->getRepository()->getPostLikesCount($post);
        return $count;
    }
    
    /**
     * Get info about user likes for every object in collection
     * @param array $posts colection of CR\Team\Model\TeamWallPost
     */
    public function getWallPostsLikeInfo($posts)
    {
        $data = $this->getRepository()->getPostLikesInfo($posts);
        return $data;
    }
    
    /**
     * Remove user like 
     * @param type $postId
     * @param type $userId
     */
    public function deleteUserPostLike($postId,$userId)
    {
        $this->getRepository()->deleteUserPostLike($postId,$userId);
    }
    
   

}
