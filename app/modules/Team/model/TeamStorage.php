<?php
namespace Webteamer\Team\Model;
use Core\DbStorage;
class TeamStorage extends DbStorage {
    
	public function findTeamPlayers($team_id)
        {
            $data = \Core\DbQuery::prepare('
                SELECT tp.id, tp.player_id, tp.team_id, tp.created_at, tp.status, tp.team_role, tp.level, tp.first_name, tp.last_name, tp.email, tp.player_number,  p.name, p.surname, p.email as player_email, p.photo
                FROM team_players tp
                LEFT JOIN co_users p ON tp.player_id = p.id 
                WHERE tp.team_id = :id
                AND tp.status != "unactive"
                order by tp.last_name asc, tp.first_name asc')
		->bindParam('id', $team_id)
		->execute()
		->fetchAll(\PDO::FETCH_ASSOC);
		return $data;
        }
        
	public function findActiveTeamPlayers($team_id)
        {
            $data = \Core\DbQuery::prepare('
                SELECT tp.id, tp.player_id, tp.team_id, tp.created_at, tp.status, tp.team_role, tp.level, tp.first_name, tp.last_name, tp.email, tp.player_number,  p.name, p.surname, p.email as player_email, p.photo
                FROM team_players tp
                LEFT JOIN co_users p ON tp.player_id = p.id 
                WHERE tp.team_id = :id
                AND tp.status = "confirmed"
                order by tp.last_name asc, tp.first_name asc')
		->bindParam('id', $team_id)
		->execute()
		->fetchAll(\PDO::FETCH_ASSOC);
		return $data;
        }
        
	public function findAllTeamPlayers($team_id,$criteria = array())
        {
            if(array_key_exists('sortCriteria', $criteria) && null != $criteria['sortCriteria']['sortName'])
            {
                if('up' == $criteria['sortCriteria']['sortDir'])
                {
                    $orderDir = 'asc';
                }
                if('down' == $criteria['sortCriteria']['sortDir'])
                {
                    $orderDir = 'desc';
                }
                
                
                
                if('name' == $criteria['sortCriteria']['sortName'])
                {
                     $orderCol = 'tp.last_name '.$orderDir.', tp.first_name ';
                }  
                if('role' == $criteria['sortCriteria']['sortName'])
                {
                     $orderCol = 'tp.team_role ';
                }  
                if('level' == $criteria['sortCriteria']['sortName'])
                {
                     $orderCol = 'tp.level ';
                }  
                if('status' == $criteria['sortCriteria']['sortName'])
                {
                     $orderCol = 'tp.status ';
                }  
            }
            else
            {
                $orderCol = 'tp.last_name asc, tp.first_name';
                $orderDir = 'asc';
            }

            
          
            $data = \Core\DbQuery::prepare('
                SELECT tp.id, tp.player_id, tp.team_id, tp.created_at, tp.status, tp.team_role, tp.level, tp.first_name, tp.last_name, tp.email, tp.player_number,  p.name, p.surname, p.email as player_email, p.photo
                FROM team_players tp
                LEFT JOIN co_users p ON tp.player_id = p.id 
                WHERE tp.team_id = :id
                order by  '.$orderCol.' '.$orderDir)
		->bindParam('id', $team_id)
		->execute()
		->fetchAll(\PDO::FETCH_ASSOC);
		return $data;
        }
        
	
	public function findTeamAdmins($team_id)
        {
            $data = \Core\DbQuery::prepare('
                SELECT tp.id, tp.player_id, tp.team_id, tp.created_at, tp.status, tp.team_role, tp.level, tp.first_name, tp.last_name, tp.email,  p.name, p.surname, p.email, p.photo
                FROM team_players tp
                LEFT JOIN co_users p ON tp.player_id = p.id 
                WHERE tp.team_id = :id
                AND tp.team_role = "ADMIN"')
		->bindParam('id', $team_id)
		->execute()
		->fetchAll(\PDO::FETCH_ASSOC);
		return $data;
        }
        
	
        
        
	public function findTeamsByPlayground($playgroundId)
        {
             $data = \Core\DbQuery::prepare('
               SELECT t.id, t.name
                FROM team_playground_alias tpa
                LEFT JOIN playground_alias pa ON tpa.alias_id = pa.id
                LEFT JOIN team t ON tpa.team_id = t.id
                
                WHERE pa.playground_id = :id')
		->bindParam('id', $playgroundId)
		->execute()
		->fetchAll(\PDO::FETCH_ASSOC);
		return $data;
        }
        
         public function findTeamsByArea($center, $distance)
        {
            $data = \Core\DbQuery::prepare('SELECT  t.*,  (6371 * ACOS(COS(RADIANS('.$center['lat'].')) * COS(RADIANS(l.lat)) * COS(RADIANS(l.lng) - RADIANS('.$center['lng'].')) + SIN(RADIANS('.$center['lat'].')) * SIN(RADIANS(l.lat)))) AS distance 
FROM locality l 
LEFT JOIN playground p ON l.id = p.locality_id 

LEFT JOIN playground_alias pa ON pa.playground_id = p.id
LEFT JOIN team_playground_alias tpa ON tpa.alias_id = pa.id
LEFT JOIN team t ON tpa.team_id = t.id
WHERE t.id is not null
HAVING distance < '.$distance)
		->execute()
		->fetchAll(\PDO::FETCH_ASSOC);
		return $data;
        }

        
	public function findUserTeams($userId)
        {
             $cols = $this->prefixColumns('t.','id, name, sport_id, created_at, author_id, age_from, age_to, status, description, phone, email, photo,locality_id, gender');
             
            
            
            $data = \Core\DbQuery::prepare('
                SELECT tp.status as "team_status", tp.team_role,  '.implode(',',$cols).'
               FROM team t
                LEFT JOIN team_players tp ON tp.team_id = t.id
                
                WHERE ((tp.player_id = :id and tp.status = "confirmed") OR t.author_id =:author_id) AND t.status != "deleted" group by t.id')
		->bindParam('id', $userId)
		->bindParam('author_id', $userId)
		->execute()
		->fetchAll(\PDO::FETCH_ASSOC);

            
		return $data;
        }
        
	public function findUserUnconfirmedTeams($userId)
        {
             $cols = $this->prefixColumns('t.','id, name, sport_id, created_at, author_id, age_from, age_to, status, description, phone, email, photo');
             
            
            
            $data = \Core\DbQuery::prepare('
                SELECT tp.status as "team_status", tp.team_role,  '.implode(',',$cols).'
               FROM team t
                LEFT JOIN team_players tp ON tp.team_id = t.id
                
                WHERE ((tp.player_id = :id and tp.status = "unconfirmed")) AND t.status != "deleted" group by t.id')
		->bindParam('id', $userId)
		->execute()
		->fetchAll(\PDO::FETCH_ASSOC);
		return $data;
        }
        
        
	public function findUserWaitingTeams($userId)
        {
             $cols = $this->prefixColumns('t.','id, name, sport_id, created_at, author_id, age_from, age_to, status, description, phone, email, photo');
             
            
            
            $data = \Core\DbQuery::prepare('
                SELECT tp.status as "team_status", tp.team_role as "team_role", tp.id as "team_player_id", tp.created_at as "team_player_created_at",   '.implode(',',$cols).'
               FROM team t
                LEFT JOIN team_players tp ON tp.team_id = t.id
                
                WHERE ((tp.player_id = :id and tp.status = "waiting-to-confirm")) AND t.status != "deleted" group by t.id')
		->bindParam('id', $userId)
		->execute()
		->fetchAll(\PDO::FETCH_ASSOC);
		return $data;
        }
        
        
	public function getUserFollowingTeams($userId)
        {
             $cols = $this->prefixColumns('t.','id, name, sport_id, created_at, author_id, age_from, age_to, status, description, phone, email, photo');
            $data = \Core\DbQuery::prepare('
                SELECT  '.implode(',',$cols).'
               FROM team t
                LEFT JOIN team_follow tf ON tf.team_id = t.id
                
                WHERE tf.user_id = :id group by t.id')
		->bindParam('id', $userId)
		->execute()
		->fetchAll(\PDO::FETCH_ASSOC);

            
		return $data;
        }
        
        public function findTeamsByLocality($localityId)
        {
              $cols = $this->prefixColumns('t.','id, name, sport_id, created_at, author_id, age_from, age_to, status, description, phone, email');
                $data = \Core\DbQuery::prepare('
                SELECT  '.implode(',',$cols).'
                FROM team_playground_alias tpa
                LEFT JOIN playground_alias pa ON tpa.alias_id = pa.id
                LEFT JOIN playground p ON pa.playground_id = p.id
                LEFT JOIN team t ON tpa.team_id = t.id
                
                WHERE p.locality_id = :id')
		->bindParam('id', $localityId)
		->execute()
		->fetchAll(\PDO::FETCH_ASSOC);
		return $data;
        }
        
        public function findTeamsInfo($ids)
        {
             
            $data = \Core\DbQuery::prepare('
                SELECT t.id, count(m.id) AS members_count,m.team_role,  f.followers
                 FROM team t
                 LEFT JOIN team_players m ON t.id = m.team_id
                 LEFT JOIN (select team_id, count(*) as followers from team_follow group by team_id) f ON t.id = f.team_id
                 WHERE t.id in ('.implode(',',$ids).')
                 AND m.status != "unactive"
                 GROUP BY t.id, m.team_role')
		->execute()
		->fetchAll(\PDO::FETCH_ASSOC);
		return $data;
        }
        
        public function fulltextNameSearch($phrase)
        {
             $query = \Core\DbQuery::prepare('SELECT u.*  '
                        . ' FROM '.$this->getTableName().' u WHERE  u.name like :name');
           $query->bindParam('name', "%".$phrase."%");
           $data = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
           return $data;
           
        }
        
      
	
	
}