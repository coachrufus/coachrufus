<?php

namespace CR\Team\Model;

use Core\ServiceLayer;
use CR\Pay\Model\ActualPackage;

class TeamCreditManager
{
    
    
    public function getActualPackage($team)
    {
        if(!isset($_SESSION['user_team_packages']))
        {
             $this->refreshTeamPackages();
        }
        $actualPackage = null;
        if(isset($_SESSION['user_team_packages'][$team->getId()]))
        {
             $actualPackage = unserialize($_SESSION['user_team_packages'][$team->getId()]);
        }
        
        return $actualPackage;
    }
    
    public function getActualPackageName($team)
    {
        if($this->teamHasProPackage($team))
        {
            return 'PRO';
        }
        else
        {
            return 'FREE';
        }
    }
    
    public function getActualPackageExpiration($team)
    {
        $actualPackage = $this->getActualPackage($team);
        if($actualPackage->isFree())
        {
            return ' - ';
        }
        else
        {
           return $actualPackage->getEndDate()->toDateTime()->format('d.m.Y');
        }
    }
    
    
    public function teamHasProPackage($team)
    {
        return true;
        
        if(!isset($_SESSION['user_team_packages']))
        {
             $this->refreshTeamPackages();
        }
        $actualPackage = null;
        if(isset($_SESSION['user_team_packages'][$team->getId()]))
        {
             $actualPackage = unserialize($_SESSION['user_team_packages'][$team->getId()]);
        }
        

        
        if(null !=$actualPackage && $actualPackage->isPro())
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    
    public function teamHasFeatureAccess($team,$featureName)
    {
        return true;

        //$this->refreshTeamPackages();
        if(!isset($_SESSION['user_team_packages']))
        {
             $this->refreshTeamPackages();
        }
        $actualPackage = null;
        if(isset($_SESSION['user_team_packages'][$team->getId()]))
        {
             $actualPackage = unserialize($_SESSION['user_team_packages'][$team->getId()]);
        }
        
        if(null !=$actualPackage && !$actualPackage->isFree())
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    public function getTeamPlayersLimit($team)
    {
        return 10000;
        if($this->teamHasProPackage($team))
        {

            $package = $this->getActualPackage($team);
            if($package->player_limit > 100)
            {
                return $package->player_limit;
            }
            else 
            {
                 return 100;
            }
           
        }
        else 
        {
            return 30;
        }
        
    }
    
     public function getAdminsLimit($team)
    {
        return 10000;
        if($this->teamHasProPackage($team))
        {
            return 10000;
        }
        else 
        {
            return 2;
        }
        
    }
  
    
    public function teamHasPlayersLimit()
    {
  
    }
    
    public function renderProIcon($text = 'PRO')
    {
        return '<button class="pro-icon">'.$text.'</button>';
    }
    
    public function refreshTeamPackages()
    {
        $security = ServiceLayer::getService('security');
        $userIdentity = $security->getIdentity();
        $teams = $userIdentity->getUserTeams();
        $actualTeamId = $userIdentity->getUserCurrentTeamId();

        $teamPackages = array();
        foreach($teams as $team)
        {
            $teamPackage = ServiceLayer::getService('ActualPackageManager')->loadActualPackage($team->getId());

            
            $teamPackages[$team->getId()] = serialize($teamPackage);
            
            if($actualTeamId == $team->getId())
            {
                 $_SESSION['actualPackage'] = (string)($teamPackage);
            }
        }

        $_SESSION['user_team_packages'] = $teamPackages;
        
        
        //set if user had pro package for steb by step purpose
        ServiceLayer::getService('StepsManager')->refreshStepByStepViewCriteria($userIdentity->getUser());
    }

    public function getProPrice()
    {
        return 20;
    }
            
}
