<?php

$acl->allowRole(array('ROLE_USER'),'tam_wall_load_widget');
$router->addRoute('tam_wall_load_widget','/wall/load-widget',array(
		'controller' => 'Webteamer\Team\TeamModule:AjaxWall:loadWidget'
));

$acl->allowRole(array('ROLE_USER'),'create_team_wall_post');
$router->addRoute('create_team_wall_post','/team/wall/create_post',array(
		'controller' => 'Webteamer\Team\TeamModule:AjaxWall:createWallPost'
));

$acl->allowRole(array('ROLE_USER'),'delete_team_wall_post');
$router->addRoute('delete_team_wall_post','/team/wall/delete_wall_post',array(
		'controller' => 'Webteamer\Team\TeamModule:AjaxWall:deleteWallPost'
));

$acl->allowRole(array('ROLE_USER'),'create_team_wall_post_comment');
$router->addRoute('create_team_wall_post_comment','/team/wall/create-wall-post-comment',array(
		'controller' => 'Webteamer\Team\TeamModule:AjaxWall:createWallPostComment'
));

$acl->allowRole(array('ROLE_USER'),'get_team_wall_post_comments');
$router->addRoute('get_team_wall_post_comments','/team/wall/get-wall-post-comments',array(
		'controller' => 'Webteamer\Team\TeamModule:AjaxWall:getWallPostComments'
));

$acl->allowRole(array('ROLE_USER'),'get_url_data');
$router->addRoute('get_url_data','/wall/get-url-data',array(
		'controller' => 'Webteamer\Team\TeamModule:AjaxWall:getUrlData'
));


$acl->allowRole(array('ROLE_USER'),'tam_wall_upload_photo');
$router->addRoute('tam_wall_upload_photo','/wall/upload-photo',array(
		'controller' => 'Webteamer\Team\TeamModule:AjaxWall:uploadPhoto'
));

$acl->allowRole(array('ROLE_USER'),'tam_wall_load_posts');
$router->addRoute('tam_wall_load_posts','/wall/load-posts',array(
		'controller' => 'Webteamer\Team\TeamModule:AjaxWall:loadPosts'
));

$acl->allowRole(array('ROLE_USER'),'tam_wall_hashtag');
$router->addRoute('tam_wall_hashtag','/team/:team_id/hashtag/:hashtag',array(
		'controller' => 'Webteamer\Team\TeamModule:Team:hashtag'
));

$acl->allowRole(array('ROLE_USER'),'tam_wall_post_like');
$router->addRoute('tam_wall_post_like','/wall/post/like',array(
		'controller' => 'Webteamer\Team\TeamModule:AjaxWall:postLike'
));

$acl->allowRole(array('ROLE_USER'),'tam_wall_post_unlike');
$router->addRoute('tam_wall_post_unlike','/wall/post/unlike',array(
		'controller' => 'Webteamer\Team\TeamModule:AjaxWall:postUnlike'
));



