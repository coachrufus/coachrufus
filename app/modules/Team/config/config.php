<?php
namespace Webteamer\Team;
require_once(MODUL_DIR.'/Team/TeamModule.php');


use Core\ServiceLayer as ServiceLayer;


ServiceLayer::addService('TeamMatchLineupRepository',array(
'class' => "Webteamer\Team\Model\TeamMatchLineupRepository",
	'params' => array(
		new \CR\Team\Model\TeamMatchLineupStorage('team_match_lineup'),
                new \Core\EntityMapper('Webteamer\Team\Model\TeamMatchLineup'),
		new \Core\DbStorage('team_match_lineup_player'),
		new \Core\EntityMapper('Webteamer\Team\Model\TeamMatchLineupPlayer'),   
	)
));


ServiceLayer::addService('TeamMatchManager',array(
'class' => "Webteamer\Team\Model\TeamMatchManager",
	'params' => array(
		ServiceLayer::getService('TeamMatchLineupRepository'),
	)
));

ServiceLayer::addService('TeamRepository',array(
'class' => "Webteamer\Team\Model\TeamRepository",
	'params' => array(
		new  \Webteamer\Team\Model\TeamStorage('team'),
		new \Core\EntityMapper('Webteamer\Team\Model\Team'),
	)
));

ServiceLayer::addService('TeamSettingsRepository',array(
'class' => "Webteamer\Team\Model\TeamSettingsRepository",
	'params' => array(
		new \Core\DbStorage('team_settings'),
		new \Core\EntityMapper('Webteamer\Team\Model\TeamSettings')
	)
));

ServiceLayer::addService('TeamPlayerRepository',array(
'class' => "Webteamer\Team\Model\TeamPlayerRepository",
	'params' => array(
		new \CR\Team\Model\TeamPlayerStorage('team_players'),
		new \Core\EntityMapper('Webteamer\Team\Model\TeamPlayer')
	)
));


ServiceLayer::addService('TeamPlayerManager',array(
'class' => "Webteamer\Team\Model\TeamPlayerManager",
    'params' => array(
		ServiceLayer::getService('TeamPlayerRepository'),
	)
));

ServiceLayer::addService('TeamManager',array(
'class' => "Webteamer\Team\Model\TeamManager",
    'params' => array(
		ServiceLayer::getService('TeamRepository'),
                ServiceLayer::getService('TeamPlayerManager'),
                ServiceLayer::getService('PlaygroundManager'),
                ServiceLayer::getService('TeamSettingsRepository'),
	)
));



$teamEventMapper = new \Core\EntityMapper('Webteamer\Event\Model\Event');
$teamEventMapper->addAssocEntity(array(
    'class' => 'Webteamer\Locality\Model\Playground' , 
    'method' => 'setPlayground',
    'mapper' => '\Core\EntityMapper'));

$teamEventStorage = new   \Webteamer\Event\Model\EventStorage('team_event');
$teamEventStorage->addAssocTable('a_','playground');



ServiceLayer::addService('TeamEventRepository',array(
'class' => "Webteamer\Event\Model\EventRepository",
	'params' => array(
		$teamEventStorage,
		$teamEventMapper
	)
));


$eventAttendanceMapper = new \Core\EntityMapper('Webteamer\Event\Model\EventAttendance');
$eventAttendanceMapper->addAssocEntity(array(
    'class' => '\Webteamer\Player\Model\Player' , 
    'method' => 'setPlayer',
    'mapper' => '\Core\EntityMapper'));



$eventAttendanceStorage = new   \Webteamer\Event\Model\EventAttendanceStorage('team_event_attendance');
$eventAttendanceStorage->addAssocTable('a_','co_users');



ServiceLayer::addService('TeamEventAttendanceRepository',array(
'class' => "Webteamer\Event\Model\EventAttendanceRepository",
	'params' => array(
		$eventAttendanceStorage ,
		$eventAttendanceMapper
	)
));

$eventCostsMapper = new \Core\EntityMapper('Webteamer\Event\Model\EventCosts');
$eventCostsStorage = new   \Webteamer\Event\Model\EventCostsStorage('team_event_costs');

ServiceLayer::addService('TeamEventCostsRepository',array(
'class' => "Webteamer\Event\Model\EventCostsRepository",
	'params' => array(
		$eventCostsStorage ,
		$eventCostsMapper,
	)
));


ServiceLayer::addService('TeamEventManager',array(
'class' => "Webteamer\Team\Model\TeamEventManager",
    'params' => array(
		ServiceLayer::getService('TeamEventRepository'),
		ServiceLayer::getService('TeamEventAttendanceRepository'),
                ServiceLayer::getService('TeamEventCostsRepository'),
		ServiceLayer::getService('TeamMatchLineupRepository'),
                ServiceLayer::getService('translator'),
  
	)
));


ServiceLayer::addService('TeamFollowRepository',array(
'class' => "CR\Team\Model\TeamFollowRepository",
	'params' => array(
		new  \CR\Team\Model\TeamFollowStorage('team_follow')
	)
));

ServiceLayer::addService('TeamFollowManager',array(
'class' => "CR\Team\Model\TeamFollowManager",
    'params' => array(
		ServiceLayer::getService('TeamFollowRepository'),
	)
));


ServiceLayer::addService('TeamCreditManager',array(
'class' => "CR\Team\Model\TeamCreditManager",
    
));

ServiceLayer::addService('TeamWallPostRepository',array(
'class' => "CR\Team\Model\TeamWallPostRepository",
	'params' => array(
		new \CR\Team\Model\TeamWallPostStorage('team_wall_post'),
		new \Core\EntityMapper('CR\Team\Model\TeamWallPost')
	)
));


ServiceLayer::addService('TeamWallPostCommentRepository',array(
'class' => "CR\Team\Model\TeamWallPostCommentRepository",
	'params' => array(
		new \CR\Team\Model\TeamWallPostCommentStorage('team_wall_post'),
		new \Core\EntityMapper('CR\Team\Model\TeamWallPostComment')
	)
));


ServiceLayer::addService('TeamWallManager',array(
'class' => "CR\Team\Model\TeamWallManager",
    'params' => array(
        ServiceLayer::getService('TeamWallPostRepository'),
        ServiceLayer::getService('TeamWallPostCommentRepository'),
       
    )
));

ServiceLayer::addService('FloorballChallengeTeamRepository',array(
'class' => "CR\Team\Model\FloorballChallengeTeamRepository",
	'params' => array(
		new \Core\DbStorage('fllorball_challenge_teams'),
		new \Core\EntityMapper('CR\Team\Model\FloorballChallengeTeam')
	)
));

ServiceLayer::addService('FlchManager',array(
'class' => "CR\Team\Model\FlchManager",
    'params' => array(
        ServiceLayer::getService('FloorballChallengeTeamRepository'),      
    )
));

ServiceLayer::addService('PromoTeamRepository',array(
'class' => "CR\Team\Model\PromoTeamRepository",
	'params' => array(
		new \Core\DbStorage('promo_team'),
		new \Core\EntityMapper('CR\Team\Model\PromoTeam')
	)
));

ServiceLayer::addService('TeamStatHistoryRepository',array(
'class' => "CR\Team\Model\TeamStatHistoryRepository",
	'params' => array(
		new \CR\Team\Model\TeamStatHistoryStorage('team_stat_history'),
		new \Core\EntityMapper('CR\Team\Model\TeamStatHistory')
	)
));



$acl = ServiceLayer::getService('acl');

//routing
$router = ServiceLayer::getService('router');

include_once('public_routing.php');

$acl->allowRole('ROLE_USER','team_autocomplete_search');
$router->addRoute('team_autocomplete_search','/team/autocomplete/search',array(
		'controller' => 'Webteamer\Team\TeamModule:Rest:search'
));

$acl->allowRole('ROLE_USER','team_create');
$router->addRoute('team_create','/team/create',array(
		'controller' => 'Webteamer\Team\TeamModule:Team:create'
));







$acl->allowRole(array('ROLE_USER'),'team_manage');
$router->addRoute('team_manage','/team/manage/:id',array(
		'controller' => 'Webteamer\Team\TeamModule:Owner:manage'
));




$acl->allowRole(array('ROLE_USER'),'teams_overview');
$router->addRoute('teams_overview','/team/overview-all',array(
		'controller' => 'Webteamer\Team\TeamModule:Team:overviewAll'
));

$acl->allowRole(array('ROLE_USER'),'team_overview');
$router->addRoute('team_overview','/team/overview/:id',array(
		'controller' => 'Webteamer\Team\TeamModule:Team:overview'
));

include_once('route_members.php');

$acl->allowRole(array('ROLE_USER'),'change_event_costs');
$router->addRoute('change_event_costs','/team/change-event-costs',array(
		'controller' => 'Webteamer\Team\TeamModule:TeamEvent:changeCosts'
));

$acl->allowRole(array('ROLE_USER'),'team_players_attendance');
$router->addRoute('team_players_attendance','/team/players-attendance/:team_id',array(
		'controller' => 'Webteamer\Team\TeamModule:Team:playersAttendance'
));

$acl->allowRole(array('ROLE_USER'),'change_player_attendance');
$router->addRoute('change_player_attendance','/team/change-players-attendance',array(
		'controller' => 'Webteamer\Team\TeamModule:TeamEvent:changePlayerAttendance'
));

$acl->allowRole(array('ROLE_USER'),'change_player_fee');
$router->addRoute('change_player_fee','/team/change-players-fee',array(
		'controller' => 'Webteamer\Team\TeamModule:TeamEvent:changePlayerFee'
));

$acl->allowRole(array('ROLE_USER'),'remove_player_attendance');
$router->addRoute('remove_player_attendance','/team/remove-players-attendance',array(
		'controller' => 'Webteamer\Team\TeamModule:Owner:removePlayerAttendance'
));

$acl->allowRole(array('ROLE_USER'),'team_player_stat');
$router->addRoute('team_player_stat','/team/players/stat/:team_id',array(
		'controller' => 'Webteamer\Team\TeamModule:TeamMatch:playerStat'
));


$acl->allowRole(array('ROLE_USER'),'team_player_react_stat');
$router->addRoute('team_player_react_stat','/team/players/stat-react/:team_id',array(
		'controller' => 'Webteamer\Team\TeamModule:TeamMatch:playerStatReact'
));

$acl->allowRole(array('ROLE_USER'),'team_match_list');
$router->addRoute('team_match_list','/team/match',array(
		'controller' => 'Webteamer\Team\TeamModule:Team:matchList'
));

$acl->allowRole(array('ROLE_USER'),'team_match_new_lineup');
$router->addRoute('team_match_new_lineup','/team-match/new-lineup',array(
		'controller' => 'Webteamer\Team\TeamModule:TeamMatch:newLineup'
));

$acl->allowRole(array('ROLE_USER'),'team_match_create_lineup');
$router->addRoute('team_match_create_lineup','/team-match/:event_id/:event_date/create-lineup',array(
		'controller' => 'Webteamer\Team\TeamModule:TeamMatch:createLineup'
));

$acl->allowRole(array('ROLE_USER'),'team_match_view_lineup');
$router->addRoute('team_match_view_lineup','/team-match/view-lineup/:id',array(
		'controller' => 'Webteamer\Team\TeamModule:TeamMatch:viewLineup'
));
/*
$acl->allowRole(array('ROLE_USER'),'team_match_unavailable_lineup');
$router->addRoute('team_match_unavailable_lineup','/team-match/unavailable-lineup/:id',array(
		'controller' => 'Webteamer\Team\TeamModule:TeamMatch:unavailableLineup'
));
*/
$acl->allowRole(array('ROLE_USER'),'team_match_edit_lineup');
$router->addRoute('team_match_edit_lineup','/team-match/edit-lineup/:id',array(
		'controller' => 'Webteamer\Team\TeamModule:TeamMatch:editLineup'
));

$acl->allowRole(array('ROLE_USER'),'team_match_update_lineup');
$router->addRoute('team_match_update_lineup','/team-match/update-lineup',array(
		'controller' => 'Webteamer\Team\TeamModule:TeamMatch:updateLineup'
));

$acl->allowRole(array('ROLE_USER'),'team_match_print_lineup');
$router->addRoute('team_match_print_lineup','/team-match/print-lineup',array(
		'controller' => 'Webteamer\Team\TeamModule:TeamMatch:printLineup'
));

$acl->allowRole(array('ROLE_USER'),'team_match_change_lineup_player');
$router->addRoute('team_match_change_lineup_player','/team-match/change-lineup-player',array(
		'controller' => 'Webteamer\Team\TeamModule:TeamMatch:changeLineupPlayer'
));

$acl->allowRole(array('ROLE_USER'),'team_match_add_lineup_player');
$router->addRoute('team_match_add_lineup_player','/team-match/add-lineup-player',array(
		'controller' => 'Webteamer\Team\TeamModule:TeamMatch:addLineupPlayer'
));

$acl->allowRole(array('ROLE_USER'),'team_match_remove_lineup_player');
$router->addRoute('team_match_remove_lineup_player','/team-match/remove-lineup-player',array(
		'controller' => 'Webteamer\Team\TeamModule:TeamMatch:removeLineupPlayer'
));

$acl->allowRole(array('ROLE_USER'),'team_match_create_stat');
$router->addRoute('team_match_create_stat','/team/match/create-stat',array(
		'controller' => 'Webteamer\Team\TeamModule:TeamMatch:createStat'
));

$acl->allowRole(array('ROLE_USER'),'team_match_view_live_stat');
$router->addRoute('team_match_view_live_stat','/team/match/view-live-stat',array(
		'controller' => 'Webteamer\Team\TeamModule:TeamMatch:viewLiveStat'
));

$acl->allowRole(array('ROLE_USER'),'team_match_create_live_stat');
$router->addRoute('team_match_create_live_stat','/team/match/create-live-stat',array(
		'controller' => 'Webteamer\Team\TeamModule:TeamMatch:createLiveStat'
));

$acl->allowRole(array('ROLE_USER'),'team_match_add_timeline_stat');
$router->addRoute('team_match_add_timeline_stat','/team/match/add-timeline-stat',array(
		'controller' => 'Webteamer\Team\TeamModule:TeamMatch:addTimeLineStat'
));

$acl->allowRole(array('ROLE_USER'),'team_match_add_simple_timeline_stat');
$router->addRoute('team_match_add_simple_timeline_stat','/team/match/add-simple-timeline-stat',array(
		'controller' => 'Webteamer\Team\TeamModule:TeamMatch:addSimpleTimeLineStat'
));

$acl->allowRole(array('ROLE_USER'),'team_match_edit_timeline_stat');
$router->addRoute('team_match_edit_timeline_stat','/team/match/edit-timeline-stat',array(
		'controller' => 'Webteamer\Team\TeamModule:TeamMatch:editTimeLineStat'
));


$acl->allowRole(array('ROLE_USER'),'team_match_remove_timeline_stat');
$router->addRoute('team_match_remove_timeline_stat','/team/match/remove-timeline-stat',array(
		'controller' => 'Webteamer\Team\TeamModule:TeamMatch:removeTimeLineStat'
));

$acl->allowRole(array('ROLE_USER'),'team_match_edit_timeline_time');
$router->addRoute('team_match_edit_timeline_time','/team/match/edit-timeline-time',array(
		'controller' => 'Webteamer\Team\TeamModule:TeamMatch:editTimeLineTime'
));

$acl->allowRole(array('ROLE_USER'),'team_match_add_timeline_mom');
$router->addRoute('team_match_add_timeline_mom','/team/match/add-timeline-mom',array(
		'controller' => 'Webteamer\Team\TeamModule:TeamMatch:addTimeLineMom'
));

$acl->allowRole(array('ROLE_USER'),'team_match_reset_timeline');
$router->addRoute('team_match_reset_timeline','/team/match/reset-timeline',array(
		'controller' => 'Webteamer\Team\TeamModule:TeamMatch:resetTimeLine'
));

$acl->allowRole(array('ROLE_USER'),'team_match_create_tennis_stat');
$router->addRoute('team_match_create_tennis_stat','/team/tennis/create-stat',array(
		'controller' => 'Webteamer\Team\TeamModule:TeamMatch:createTennisStat'
));

$acl->allowRole(array('ROLE_USER'),'team_tennis_stat');
$router->addRoute('team_tennis_stat','/team/tennis/stat/:team_id',array(
		'controller' => 'Webteamer\Team\TeamModule:TeamMatch:tennisStat'
));

$acl->allowRole(array('ROLE_USER'),'team_match_create_volleyball_stat');
$router->addRoute('team_match_create_volleyball_stat','/team/volleyball/create-stat',array(
		'controller' => 'Webteamer\Team\TeamModule:TeamMatchStat:createVolleyballStat'
));

$acl->allowRole(array('ROLE_USER'),'team_volleyball_stat');
$router->addRoute('team_volleyball_stat','/team/volleyball/stat/:team_id',array(
		'controller' => 'Webteamer\Team\TeamModule:TeamMatchStat:volleyballStat'
));


$acl->allowRole(array('ROLE_USER'),'team_match_create_basketball_stat');
$router->addRoute('team_match_create_basketball_stat','/team/basketball/create-stat',array(
		'controller' => 'Webteamer\Team\TeamModule:TeamMatchStat:createBasketballStat'
));

$acl->allowRole(array('ROLE_USER'),'team_basketball_stat');
$router->addRoute('team_basketball_stat','/team/basketball/stat/:team_id',array(
		'controller' => 'Webteamer\Team\TeamModule:TeamMatchStat:basketballStat'
));



$acl->allowRole(array('ROLE_USER'),'team_settings');
$router->addRoute('team_settings','/team/settings/:team_id',array(
		'controller' => 'Webteamer\Team\TeamModule:Team:settings'
));

$acl->allowRole(array('ROLE_USER'),'team_crs_settings');
$router->addRoute('team_crs_settings','/team/crs-settings/:team_id',array(
		'controller' => 'Webteamer\Team\TeamModule:Team:crsSettings'
));

$acl->allowRole(array('ROLE_USER'),'team_settings_add');
$router->addRoute('team_settings_add','/team/add-settings',array(
		'controller' => 'Webteamer\Team\TeamModule:Team:addSettings'
));

$acl->allowRole(array('ROLE_USER'),'team_settings_photo');
$router->addRoute('team_settings_photo','/team/change-photo',array(
		'controller' => 'Webteamer\Team\TeamModule:Team:changePhoto'
));

$acl->allowRole(array('ROLE_USER'),'team_settings_remove_photo');
$router->addRoute('team_settings_remove_photo','/team/remove-photo/:team_id',array(
		'controller' => 'Webteamer\Team\TeamModule:Team:removePhoto'
));

$acl->allowRole(array('ROLE_USER'),'team_delete');
$router->addRoute('team_delete','/team/delete/:team_id',array(
		'controller' => 'Webteamer\Team\TeamModule:Team:delete'
));

include_once('events_routing.php');
include_once('wall_routing.php');
include_once('widget_routing.php');
include_once('floorball_routing.php');
include_once('attendance_routing.php');
include_once('test_routing.php');

$acl->allowRole('ROLE_USER','team_follow');
$router->addRoute('team_follow','/team/follow/:id',array(
		'controller' => 'Webteamer\Team\TeamModule:Team:follow'
));


require_once MODUL_DIR . "/notification/lib/NotificationStore.php";
require_once MODUL_DIR . "/notification/lib/NotificationSender.php";
require_once MODUL_DIR . "/notification/lib/NotificationSendQueue.php";
require_once MODUL_DIR . "/notification/lib/NotificationBlacklist.php";
require_once MODUL_DIR . "/notification/lib/utils/PlayerEventInfosLang.php";

require_once MODUL_DIR . "/notification/senders/model/AddRate.php";
require_once MODUL_DIR . "/notification/senders/AddRate.php";
require_once MODUL_DIR . "/notification/senders/model/MatchResults.php";
require_once MODUL_DIR . "/notification/senders/MatchResults.php";
















