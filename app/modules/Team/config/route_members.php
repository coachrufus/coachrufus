<?php

$acl->allowRole(array('ROLE_USER'),'team_players_edit');
$router->addRoute('team_players_edit','/team/players/edit',array(
		'controller' => 'Webteamer\Team\TeamModule:Owner:editPlayers'
));

$acl->allowRole(array('ROLE_USER'),'team_players_template');
$router->addRoute('team_players_template','/team/players/template',array(
		'controller' => 'Webteamer\Team\TeamModule:TeamPlayer:template'
));

$acl->allowRole(array('ROLE_USER'),'team_players_import');
$router->addRoute('team_players_import','/team/players/import',array(
		'controller' => 'Webteamer\Team\TeamModule:TeamPlayer:import'
));



$acl->allowRole(array('ROLE_USER'),'team_new_players_list');
$router->addRoute('team_new_players_list','/team/manage-players/:team_id/:type',array(
		'controller' => 'Webteamer\Team\TeamModule:TeamPlayer:playersList'
));

$acl->allowRole(array('ROLE_USER'),'team_players_list');
$router->addRoute('team_players_list','/team/manage-players/:team_id',array(
		'controller' => 'Webteamer\Team\TeamModule:TeamPlayer:playersList'
));

$acl->allowRole(array('ROLE_USER'),'remove_team_player');
$router->addRoute('remove_team_player','/team/members/removePlayer',array(
		'controller' => 'Webteamer\Team\TeamModule:TeamPlayer:removePlayer'
));
$acl->allowRole(array('ROLE_USER'),'unactivate_team_player');
$router->addRoute('unactivate_team_player','/team/members/unactivatePlayer',array(
		'controller' => 'Webteamer\Team\TeamModule:TeamPlayer:unactivatePlayer'
));

$acl->allowRole(array('ROLE_USER'),'team_public_players_list');
$router->addRoute('team_public_players_list','/team/players/:team_id',array(
		'controller' => 'Webteamer\Team\TeamModule:TeamPlayer:publicPlayersList'
));

$acl->allowRole('ROLE_USER','team_members');
$router->addRoute('team_members','/team/team_members/',array(
		'controller' => 'Webteamer\Team\TeamModule:Team:members'
));

$acl->allowRole('guest','team_share_link');
$router->addRoute('team_share_link','/share-link-invite/:lang/:hash',array(
		'controller' => 'Webteamer\Team\TeamModule:Team:shareLinkInvite'
));


$acl->allowRole('guest','team_member_invite');
$router->addRoute('team_member_invite','/invite/:hash',array(
		'controller' => 'Webteamer\Team\TeamModule:Team:memberInvite'
));

$acl->allowRole('guest','team_member_invite_decline');
$router->addRoute('team_member_invite_decline','/invite-decline/:hash',array(
		'controller' => 'Webteamer\Team\TeamModule:Team:memberInviteDecline'
));





$acl->allowRole('ROLE_USER','team_add_player');
$router->addRoute('team_add_player','/team/add-player/:team_id',array(
		'controller' => 'Webteamer\Team\TeamModule:Owner:addPlayer'
));

$acl->allowRole('ROLE_USER','create_team_player');
$router->addRoute('create_team_player','/team/create-player',array(
		'controller' => 'Webteamer\Team\TeamModule:TeamPlayer:createTeamPlayer'
));

$acl->allowRole('ROLE_USER','edit_team_player');
$router->addRoute('edit_team_player','/team/edit-player',array(
		'controller' => 'Webteamer\Team\TeamModule:Rest:editTeamPlayer'
));

$acl->allowRole('ROLE_USER','team_send_invitation');
$router->addRoute('team_send_invitation','/send-invitation',array(
		'controller' => 'Webteamer\Team\TeamModule:TeamPlayer:sendMemberInvitation'
));

$acl->allowRole('ROLE_USER','team_player_stats');
$router->addRoute('team_player_stats','/team/player-stats',array(
		'controller' => 'Webteamer\Team\TeamModule:TeamPlayer:teamPlayerStats'
));

$acl->allowRole('ROLE_USER','team_leave');
$router->addRoute('team_leave','/team/leave',array(
		'controller' => 'Webteamer\Team\TeamModule:TeamPlayer:leaveTeam'
));

$acl->allowRole('ROLE_USER','team_join_request_canceled');
$router->addRoute('team_join_request_canceled','/team/join-request-cancel',array(
		'controller' => 'Webteamer\Team\TeamModule:TeamPlayer:teamJoinRequestCancel'
));
