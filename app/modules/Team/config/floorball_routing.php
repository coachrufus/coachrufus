<?php



$acl->allowRole('guest','floorball_challenge_activation_email');
$router->addRoute('floorball_challenge_activation_email','/floorball-challenge/activation-mail',array(
		'controller' => 'Webteamer\Team\TeamModule:FloorballChallenge:activationEmail'
));

$acl->allowRole('ROLE_USER','floorball_challenge_activation');
$router->addRoute('floorball_challenge_activation','/floorball-challenge/activation/:code',array(
		'controller' => 'Webteamer\Team\TeamModule:FloorballChallenge:activation'
));

$acl->allowRole('ROLE_USER','floorball_challenge_home');
$router->addRoute('floorball_challenge_home','/floorball-challenge',array(
		'controller' => 'Webteamer\Team\TeamModule:FloorballChallenge:home'
));
