<?php
$acl->allowRole(array('ROLE_USER'),'team_events_calendar');
$router->addRoute('team_events_calendar','/team/events-calendar',array(
		'controller' => 'Webteamer\Team\TeamModule:TeamEvent:calendar'
));


$acl->allowRole('ROLE_USER','team_event_list');
$router->addRoute('team_event_list','/event/list',array(
		'controller' => 'Webteamer\Team\TeamModule:TeamEvent:list'
));

$acl->allowRole('ROLE_USER','team_event_past_list');
$router->addRoute('team_event_past_list','/event/list',array(
		'controller' => 'Webteamer\Team\TeamModule:TeamEvent:list'
));

$acl->allowRole('guest','team_event_ical');
$router->addRoute('team_event_ical','/event/ical/:team_id/event/subscription',array(
		'controller' => 'Webteamer\Team\TeamModule:TeamEvent:icalList'
));
$acl->allowRole('guest','team_event_single_ical');
$router->addRoute('team_event_single_ical','/event/ical/:id/subscription',array(
		'controller' => 'Webteamer\Team\TeamModule:TeamEvent:icalSingleEvent'
));





$acl->allowRole('ROLE_USER','team_event_rating');
$router->addRoute('team_event_rating','/event/rating',array(
		'controller' => 'Webteamer\Team\TeamModule:TeamEvent:rating'
));



$acl->allowRole('ROLE_USER','create_team_event_progress');
$router->addRoute('create_team_event_progress','/team/create-event/:team_id/:type',array(
		'controller' => 'Webteamer\Team\TeamModule:TeamEvent:create'
));

$acl->allowRole('ROLE_USER','create_team_event');
$router->addRoute('create_team_event','/team/create-event/:team_id',array(
		'controller' => 'Webteamer\Team\TeamModule:TeamEvent:create'
));

$acl->allowRole('ROLE_USER','edit_team_event');
$router->addRoute('edit_team_event','/team/edit-event/:event_id',array(
		'controller' => 'Webteamer\Team\TeamModule:TeamEvent:edit'
));

$acl->allowRole('ROLE_USER','delete_team_event');
$router->addRoute('delete_team_event','/team/delete-event/:event_id',array(
		'controller' => 'Webteamer\Team\TeamModule:TeamEvent:delete'
));

$acl->allowRole('ROLE_USER','confirm_delete_team_event');
$router->addRoute('confirm_delete_team_event','/team/confirm-delete-event/:event_id/:event_date',array(
		'controller' => 'Webteamer\Team\TeamModule:TeamEvent:confirmCalendarDelete'
));

$acl->allowRole('ROLE_USER','team_event_detail');
$router->addRoute('team_event_detail','/event/:id/:current_date',array(
		'controller' => 'Webteamer\Team\TeamModule:TeamEvent:detail'
));

$acl->allowRole('ROLE_USER','team_event_past_detail');
$router->addRoute('team_event_past_detail','/event-past/:id/:current_date',array(
		'controller' => 'Webteamer\Team\TeamModule:TeamEvent:pastDetail'
));

$acl->allowRole('ROLE_USER','team_event_notify');
$router->addRoute('team_event_notify','/event/:event_id/:event_date/notify',array(
		'controller' => 'Webteamer\Team\TeamModule:TeamEvent:notify'
));