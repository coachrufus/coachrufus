<?php
namespace Webteamer\Sport;
require_once(MODUL_DIR.'/Sport/SportModule.php');


use Core\ServiceLayer as ServiceLayer;


ServiceLayer::addService('SportRepository',array(
'class' => "Webteamer\Sport\Model\SportRepository",
	'params' => array(
		new \Core\DbStorage('sport'),
		new \Core\EntityMapper('Webteamer\Sport\Model\Sport')
	)
));

ServiceLayer::addService('SportManager',array(
'class' => "Webteamer\Sport\Model\SportManager",
     'params' => array(
        ServiceLayer::getService('SportRepository')
    )
));



$acl = ServiceLayer::getService('acl');


//routing
$router = ServiceLayer::getService('router');






