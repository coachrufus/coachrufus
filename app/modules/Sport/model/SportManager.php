<?php
namespace Webteamer\Sport\Model;

use Core\Manager;
use Core\ServiceLayer;


class SportManager extends Manager
{
    public function getSportFormChoices()
    {
        $repo = $this->getRepository();
        $all = $repo->getFormChoices('id','name');
        return $all;
    }
    
    public function getTeamSportFormChoices()
    {
        $repo = $this->getRepository();
        $sports = $repo->findAll();
        
        
        //$all = $repo->getFormChoices('id','name');
        
        $choices = array();
        foreach($sports as $sport)
        {
            if($sport->getPriority() == 1)
            {
                $choices['main-sports-choices'][$sport->getId()] = $sport->getName();
            }
            
            if($sport->getPriority() == 2)
            {
                if(77 != $sport->getId())
                {
                     $choices['other-sports-choices'][$sport->getId()] = $sport->getName();
                }
            }
            
        }
        
         $choices['other-sports-choices']['77'] = 'Other';
        
        
        return $choices;
    }
    
    public function findSportById($id)
    {
         $repo = $this->getRepository();
         return $repo->find($id);
    }
    
    /**
     * Return route to creating stat based on team sport
     */
    public function getTeamSportAddStatRoute($team)
    {
        $sport = $this->findSportById($team->getSportId());
        $route = null;
        if(null != $sport->getAddStatRoute())
        {
            $route = $sport->getAddStatRoute();
        }
        
        return $route;
    }
    
    /**
     * Return route to creating stat based on team sport
     */
    public function getTeamSportStatRoute($team)
    {
        $sport = $this->findSportById($team->getSportId());
        $route = null;
        if(null != $sport->getStatRoute())
        {
            $route = $sport->getStatRoute();
        }
        
        return $route;
    }
    
    public function getProSportIds()
    {
        return array(1,2,3,4,16,41,42,43,44,45,53,68,5,6,112,19,15,103,106,109);
        
    }
}