<?php

namespace Webteamer\Sport\Model;

use Core\EntityMapper as EntityMapper;
use Core\ServiceLayer as ServiceLayer;

class Sport {

    private $repository;
    private $mapper_rules = array(
        'id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => true
        ),
        'name' => array(
            'type' => 'string',
            'max_length' => '255',
            'required' => true
        ),
        'add_stat_route' => array(
            'type' => 'string',
            'max_length' => '255',
            'required' => true
        ),
        'stat_route' => array(
            'type' => 'string',
            'max_length' => '255',
            'required' => true
        ),
        'priority' => array(
            'type' => 'int',
            'required' => false
        ),
    );

    public function getDataMapperRules()
    {
        return $this->mapper_rules;
    }

    public function getRepository()
    {
        if (null == $this->repository)
        {
            $this->repository = ServiceLayer::getService("SportRepository");
        }
        return $this->repository;
    }

    public function setRepository($repository)
    {
        $this->repository = $repository;
    }

    protected $id;
    protected $name;
    protected $addStatRoute;
    protected $statRoute;
    protected $priority;

    public function setId($val)
    {
        $this->id = $val;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setName($val)
    {
        $this->name = $val;
    }

    public function getName()
    {
        return $this->name;
    }

    function getStatRoute()
    {
        return $this->statRoute;
    }

    function setStatRoute($statRoute)
    {
        $this->statRoute = $statRoute;
    }
    
    function getAddStatRoute() {
return $this->addStatRoute;
}

 function setAddStatRoute($addStatRoute) {
$this->addStatRoute = $addStatRoute;
}

public  function getPriority() {
return $this->priority;
}

public  function setPriority($priority) {
$this->priority = $priority;
}




}
