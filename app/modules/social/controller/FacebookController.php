<?php

namespace Social\Controller;


use Core\Controller as Controller;
use Core\ServiceLayer as ServiceLayer;
use User\Handler\RegisterHandler as RegisterHandler;
use Core\GUID;

class FacebookController extends Controller {

    public function remoteRegisterAction()
    {
        $fbManager =  ServiceLayer::getService('facebook_manager');
        $loginUrl = htmlspecialchars_decode($fbManager->generateRegisterUrl());
        $this->getRequest()->redirect($loginUrl);
    }
    
    
    public function shareChartDetailAction()
    {
          
        
        if (
            strpos($_SERVER["HTTP_USER_AGENT"], "facebookexternalhit/") !== false ||          
            strpos($_SERVER["HTTP_USER_AGENT"], "Facebot") !== false
        ) 
        {
               
            $layout = ServiceLayer::getService('layout');
            $layout->setTemplate(GLOBAL_DIR.'/templates/ClearLayout.php');
            $img = $this->getRequest()->get('img_name');
            $imgSize =  getimagesize(APPLICATION_PATH.'/img/share/'.$img.'.png');
                return $this->render('Social\SocialModule:Facebook:share_chart_detail.php', array(
                'img' => $img,
                'width' => $imgSize[0],
                'height' => $imgSize[1]
            ));
         }
         else {
            file_put_contents($filename, 'redirect');
           $this->getRequest()->redirect($this->getRouter()->link('login',array('lang' =>  $_SESSION['app_lang'] ))); 
            
        }
       
    }

    public function shareEventAction()
    {
        ServiceLayer::getService('layout')->setTemplate(GLOBAL_DIR . '/templates/ClearLayout.php');
            $guid = $this->getRequest()->get('guid');
            list($width, $height) = getImageSize(APPLICATION_PATH . "/img/share/events/{$guid}.png");
            return $this->render([
                'guid' => $guid,
                'width' => $width,
                'height' => $height,
                'title' => $_GET['t'],
                'note' => $_GET['n'],
                'backLink' => $_GET['b'],
            ]);
    }
    
    public function registerAction()
    {
        $fbManager =  ServiceLayer::getService('facebook_manager');
        $loginUrl = $fbManager->generateRegisterUrl();
        return $this->render('Social\SocialModule:Facebook:register.php', array(
            'loginUrl' => $loginUrl
        ));
    }
    
    
    public function loginAction()
    {
        $fbManager =  ServiceLayer::getService('facebook_manager');
        $loginUrl = $fbManager->generateLoginUrl();
        return $this->render('Social\SocialModule:Facebook:login.php', array(
            'loginUrl' => $loginUrl
        ));
    }

    public function loginResponseAction()
    {
        $fbManager = ServiceLayer::getService('facebook_manager');
        $security = ServiceLayer::getService('security');
        $request = ServiceLayer::getService('request');
        $accessToken = $fbManager->getAccessToken();
        $_SESSION['fb_access_token'] = $accessToken;  
        
        $user = $fbManager->getUserInfo( $accessToken);
        $registredUser = array();
       
        
        //$exist_user = $fbManager->findUserByFbId( $user->getId());
        $exist_user = $security->getIdentityProvider()->findUserByUsername($user['email']);
        if(null ==  $exist_user)
        {
            $registerData = $fbManager->createRegisterDataset($user);
            $activationGuid = GUID::create();
            $registerData['facebook_id'] =  $user->getId();
            $registerData['status'] =  1;
            
            $ip = $_SERVER['REMOTE_ADDR'];
            $details = json_decode(file_get_contents("http://ipinfo.io/".$ip."/json"));
             if(null != $details)
            {
                $registerData['country'] = $details->country;
                
                if($user->getField('locale') == 'sk_SK')
                {
                     $registerData['default_lang'] = 'sk';
                }
                else
                {
                     $registerData['default_lang'] = 'en';
                }

                $location = $details->loc;
                $locationInfo = explode(',',$location);
                $countryInfo = json_decode(file_get_contents("http://api.geonames.org/timezoneJSON?lat=".$locationInfo[0]."&lng=".$locationInfo[1]."&username=coachrufus"));

                if(null != $countryInfo)
                {
                     $registerData['timezone'] =  $countryInfo->timezoneId;
                }

            }
            
            $registerData['resource'] = $request->get('promo');
        
       
            
            
            $register_handler = new RegisterHandler();
            $register_handler->persistData($registerData,$activationGuid);
            $exist_user = $security->getIdentityProvider()->findUserByUsername($user['email']);
            

            $emailMarketingManager = \Core\ServiceLayer::getService('EmailMarketingManager');
            $emailMarketingManager->createUser($exist_user);
            $emailMarketingManager->addUserTag($exist_user,'user_noteam');
            
            
            $register_handler->sendVerifyEmail($exist_user);
        }
        else
        {
            if(null == $exist_user->getFacebookId())
            {
                $exist_user->setFacebookId($user->getId());
                $repo = $exist_user->getRepository();
                $repo->save($exist_user);
            }
            //set avatar
            $exist_user->setFacebookData($user);
        }



        //prihlasi
        
	
        $router = ServiceLayer::getService('router');
        $auth_result = $security->authenticateFacebookUser($exist_user);
        
        if('flch' == $request->get('promo'))
        {
            $exist_user->setResource('flch');
            $userRepo = ServiceLayer::getService('user_repository');
            $userRepo->save($exist_user);
            $security->reloadUser($exist_user);
        }
        
        if('group' == $request->get('promo'))
        {
            $exist_user->setResource('group');
            $userRepo = ServiceLayer::getService('user_repository');
            $userRepo->save($exist_user);
            $security->reloadUser($exist_user);
        }
        
        
        $redirectData = array(
            'sl'=>'fb',
            'reg'=> $request->get('reg'),
            'userid' =>  $auth_result['ID'],
            'inv' => $request->get('inv'),
            'tp' => $request->get('tp'),
            'h' => $request->get('h'),
        );

        
       
        $redirectUrl = $router->link('player_dashboard',$redirectData);
      
        
        
        $request->Redirect($redirectUrl);

    }

    public function userInfoAction()
    {
        $accessToken = (isset($_SESSION['fb_access_token'])) ? $_SESSION['fb_access_token'] : null;
        $fbManager =  ServiceLayer::getService('facebook_manager');
        $user = $fbManager->getUserInfo($accessToken);
        t_dump($user);
    }
    
    
    
    
     public function inviteFriendsAction()
    {
        $fbManager =  ServiceLayer::getService('facebook_manager');
		  
        $user = $fbManager->getUserInfo($_SESSION['fb_access_token']);
        t_dump($user->getField('locale'));
        
        var_dump($user->getField('picture')->getField('url'));
		
		
         $response = $fbManager->getUserFriends($_SESSION['fb_access_token']);
        exit;
        //$token = $fbManager->getAccessToken();
        $fbLoginUrl = $fbManager->generateLoginUrl();
        
    }
    

            
}
