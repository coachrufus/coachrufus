<?php

namespace Social\Controller;

use Core\Controller as Controller;
use Core\ServiceLayer;
use Google_Client;
use Google_Service_Oauth2;
use User\Handler\RegisterHandler;
use Core\GUID;



class GoogleController extends Controller {

     
    public function remoteRegisterAction()
    {
        require_once LIB_DIR.'/googleApi/autoload.php'; // or wherever autoload.php is located
        $router = ServiceLayer::getService('router');
        $client_id = GOOGLE_CLIENT_ID; 
        $client_secret = GOOGLE_CLIENT_SECRET;
        $redirect_uri = WEB_DOMAIN.$router->link('google_login_response');
        //$redirect_uri = 'http://app.coachrufus.com/social/google/login-response';ň
        //http://app.coachrufus.lamp/social/google/login-response
        

        $request = ServiceLayer::getService('request');
        
        $redirectData = array(
            'reg' => $request->get('g'),
            'inv' => $request->get('inv'),
            'tp' => $request->get('tp'),
            'h' => $request->get('h'),
            'promo' => $request->get('promo'),
        );
        $stateString = $request->base64UrlEncode(json_encode($redirectData));
        

        $client = new Google_Client();
        $client->setClientId($client_id);
        $client->setClientSecret($client_secret);
        $client->setRedirectUri($redirect_uri);
        $client->addScope("email");
        $client->addScope("profile");
        $client->setState($stateString);
        $service = new Google_Service_Oauth2($client);
        
     
        $loginUrl = $client->createAuthUrl();
        
        $this->getRequest()->redirect($loginUrl);
    }
    
    
    public function registerAction()
    {
        //require_once LIB_DIR.'/googleApi/autoload.php'; // or wherever autoload.php is located
        $router = ServiceLayer::getService('router');
        $client_id = GOOGLE_CLIENT_ID; 
        $client_secret = GOOGLE_CLIENT_SECRET;
        $redirect_uri = WEB_DOMAIN.$router->link('google_login_response');
        //$redirect_uri = 'http://app.coachrufus.com/social/google/login-response';ň
        //http://app.coachrufus.lamp/social/google/login-response
        

        $request = ServiceLayer::getService('request');
        
        $redirectData = array(
            'reg' => $request->get('g'),
            'inv' => $request->get('inv'),
            'tp' => $request->get('tp'),
            'h' => $request->get('h'),
            'promo' => $request->get('promo'),
        );
        $stateString = $request->base64UrlEncode(json_encode($redirectData));
        

        $client = new Google_Client();
        $client->setAuthConfig(APP_DIR.'/config/google_credentials.json');
        $client->setRedirectUri($redirect_uri);
        $client->addScope("email");
        $client->addScope("profile");
        
     
        $loginUrl = $client->createAuthUrl();
        
        return $this->render('Social\SocialModule:Google:register.php', array(
             'loginUrl' => $loginUrl
            
        ));
    }
    
    public function loginAction()
    {
        //require_once LIB_DIR.'/googleApi/autoload.php'; // or wherever autoload.php is located

        $router = ServiceLayer::getService('router');
        $client_id = GOOGLE_CLIENT_ID; 
        $client_secret = GOOGLE_CLIENT_SECRET;
        $redirect_uri = WEB_DOMAIN.$router->link('google_login_response');
        //$redirect_uri = 'http://app.coachrufus.com/social/google/login-response';
        

        $request = ServiceLayer::getService('request');
        
        $redirectData = array(
            'inv' => $request->get('inv'),
            'tp' => $request->get('tp'),
            'h' => $request->get('h'),
            'promo' => $request->get('promo'),
        );
        $stateString = $request->base64UrlEncode(json_encode($redirectData));
        
        $client = new Google_Client();
        $client->setAuthConfig(APP_DIR.'/config/google_credentials.json');
        $client->setRedirectUri($redirect_uri);
        $client->addScope("email");
        $client->addScope("profile");

        /*
        $client = new Google_Client();
        $client->setClientId($client_id);
        $client->setClientSecret($client_secret);
        $client->setRedirectUri($redirect_uri);
        $client->addScope("email");
        $client->addScope("profile");
        $client->setState($stateString);
        $service = new Google_Service_Oauth2($client);
        */
     
        $loginUrl = $client->createAuthUrl();
        
        return $this->render('Social\SocialModule:Google:login.php', array(
             'loginUrl' => $loginUrl
            
        ));
    }

    public function loginResponseAction()
    {
        $request = ServiceLayer::getService('request');
        $security = ServiceLayer::getService('security');
        $code = $request->get('code');
        
        
        //ak existuje code
        
        $googleManager =  ServiceLayer::getService('google_manager');
        
        if (isset($_SESSION['google_access_token'])) {
           $accessToken = $_SESSION['google_access_token'];
        }
        else {
            $accessToken = $googleManager->getAccessToken($code);
            $_SESSION['google_access_token'] = $accessToken;  
        }
         
       
        
        $user = $googleManager->getUserInfo( $accessToken);
        $registredUser = array();

        //$exist_user = $googleManager->findUserByGoogleId( $user->getId());
        $exist_user = $security->getIdentityProvider()->findUserByUsername($user['email']);
        
        $statParams = json_decode($request->base64UrlDecode($request->get('state')));
        if(null ==  $exist_user)
        {
            $registerData = $googleManager->createRegisterDataset($user);
            $activationGuid = GUID::create();
            $registerData['google_id'] =  $user->getId();
            $registerData['status'] =  1;
            $registerData['resource'] = $statParams->promo;
            
            
            if($user->getLocale() == 'sk')
            {
                 $registerData['default_lang'] = 'sk';
            }
            else
            {
                 $registerData['default_lang'] = 'en';
            }
            
            
            $register_handler = new RegisterHandler();
            $register_handler->persistData($registerData,$activationGuid);
            $exist_user = $security->getIdentityProvider()->findUserByUsername($user['email']);
            $security->remeberUser($exist_user);
            
            $emailMarketingManager = \Core\ServiceLayer::getService('EmailMarketingManager');
            $emailMarketingManager->createUser($exist_user);
            $emailMarketingManager->addUserTag($exist_user,'user_noteam');
          
            $register_handler->sendVerifyEmail($exist_user);
        }
        else
        {
            //overi ci ma priradene google id
            if(null == $exist_user->getGoogleId())
            {
                $exist_user->setGoogleId($user->getId());
                $repo = $exist_user->getRepository();
                $repo->save($exist_user);
            }
            $security->remeberUser($exist_user);
            
        }



        //prihlasi
	
        $router = ServiceLayer::getService('router');
        //$security->authenticateUser($registredUser['email'],$registredUser['password']);
         $auth_result = $security->authenticateGoogleUser($exist_user);
         
         $redirectData = array(
            'sl'=>'g',
            'userid' =>  $auth_result['ID']
        );
          
         
          $redirectData['reg'] = $statParams->reg;
          $redirectData['inv'] = $statParams->inv;
          $redirectData['tp'] = $statParams->tp;
          $redirectData['h'] = $statParams->h;
          
          if($statParams->promo == 'flch')
          {
                $exist_user->setResource('flch');
                $userRepo = ServiceLayer::getService('user_repository');
                $userRepo->save($exist_user);
                $security->reloadUser($exist_user);
          }
          
           if('group' == $statParams->promo)
        {
            $exist_user->setResource('group');
            $userRepo = ServiceLayer::getService('user_repository');
            $userRepo->save($exist_user);
            $security->reloadUser($exist_user);
        }


         $redirectUrl = $router->link('player_dashboard',$redirectData);
           
         
         
         $request->Redirect($redirectUrl);
       // $request->Redirect($router->link('base_home'));
    }
    
     public function userInfoAction()
    {
        $accessToken = (isset($_SESSION['google_access_token'])) ? $_SESSION['google_access_token'] : null;
        $googleManager =  ServiceLayer::getService('google_manager');
        $user = $googleManager->getUserInfo($accessToken);
        var_dump($user);
       
    }

}
