<?php

namespace Social\Manager;

use Core\ServiceLayer;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\Facebook;



class FacebookManager {

    private $apiProvider;

    function __construct()
    {
        //require_once LIB_DIR . '/Facebook/autoload.php';
        
        $fb = new Facebook([
            'app_id' => FB_APP_ID,
            'app_secret' => FB_APP_SECRET,
            'default_graph_version' => 'v2.4',
        ]);

        $this->setApiProvider($fb);
    }

    function getApiProvider()
    {
        return $this->apiProvider;
    }

    function setApiProvider($apiProvider)
    {
        $this->apiProvider = $apiProvider;
    }
    
    public function generateResponseUrl($url)
    {
        $router = ServiceLayer::getService('router');
        $fb = $this->getApiProvider();
        $helper = $fb->getRedirectLoginHelper();

        $permissions = ['email']; // Optional permissions
        $returnUrl = htmlspecialchars($helper->getLoginUrl($url, $permissions));
        return $returnUrl;
    }
    
    public function generateRegisterUrl()
    {
        $router = ServiceLayer::getService('router');

        $fb = $this->getApiProvider();
        $helper = $fb->getRedirectLoginHelper();

        $permissions = ['email','user_friends','user_likes']; // Optional permissions
        $request = ServiceLayer::getService('request');
        $redirectData = array(
            'reg' => 'fb',
            'inv' => $request->get('inv'),
            'tp' => $request->get('tp'),
            'h' => $request->get('h'),
            'promo' => $request->get('promo'),
        );

        $access_token = $helper->getAccessToken();
        //$loginUrl = htmlspecialchars($helper->getLoginUrl(WEB_DOMAIN . $router->link('fb_login_response',$redirectData), $permissions));
        
        $loginUrl = htmlspecialchars($helper->getLoginUrl(WEB_DOMAIN . $router->link('fb_login_response'), $permissions).'&'.http_build_query($redirectData));
      
        return $loginUrl;
    }


    public function generateLoginUrl()
    {
        $router = ServiceLayer::getService('router');

        $fb = $this->getApiProvider();
        $helper = $fb->getRedirectLoginHelper();

        $permissions = ['email','user_friends','user_likes']; // Optional permissions
        $request = ServiceLayer::getService('request');
        $redirectData = array(
            'inv' => $request->get('inv'),
            'tp' => $request->get('tp'),
            'h' => $request->get('h'),
            'promo' => $request->get('promo'),
        );
        
        

        $access_token = $helper->getAccessToken();
        $loginUrl = htmlspecialchars($helper->getLoginUrl(WEB_DOMAIN . $router->link('fb_login_response'), $permissions).'&'.http_build_query($redirectData));
      
        return $loginUrl;
    }

    public function getUserInfo($access_token)
    {
        if (null == $access_token)
        {
            return null;
        }
        $fb = $this->getApiProvider();
        $helper = $fb->getRedirectLoginHelper();
        $response = $fb->get('/me?fields=id,first_name,last_name,email,birthday,gender,hometown,languages,cover,about,picture,locale', $access_token);
        $user = $response->getGraphUser();
        
        return $user;
    }
    
    public function postScouting($access_token)
    {
        $fb = $this->getApiProvider($access_token);
        $helper = $fb->getRedirectLoginHelper();
        t_dump($access_token);exit;
        //EAAQOwArg02wBAHXjaFSd8sorEVny4cIhf1Kv5zd24blz2lGHuCAtGaUBfmxImdwYopSNsMwK4jBnUVagnaMrZBVj0dcZChBcvaprEYexoPWqspOGzBQ8TPSPFSemZCtBiZC5IkKoMAzfT1AidZCODwptBtmsnAht6a91rvmb7ggZDZD
        echo 'https://www.facebook.com/dialog/oauth?client_id='.FB_APP_ID.'&redirect_uri=http://app.coachrufus.lamp/social/fb/login-response&response_type=token&scope=manage_pages';exit;
        //https://www.facebook.com/dialog/oauth?client_id=YOUR_APP_ID&client_secret=YOUR_APP_SECRET&redirect_uri=YOUR_REDIRECT_URL&scope=read_stream,publish_stream,offline_access,manage_pages
        $response = $fb->get('/166447643435287?fields=access_token', $access_token);
         //$response = $fb->get('/me/accounts?access_token', $access_token);
        $response = $response->getDecodedBody();
        $args = array(
            'message' => 'TEST'
          );
        $post_id = $fb->post("/166447643435287/feed", $args ,$response['access_token']);
       //t_dump($response->getDecodedBody());
    }
    
      public function getUserFriends($access_token)
    {
        if (null == $access_token)
        {
            return null;
        }

        $fb = $this->getApiProvider();
        $helper = $fb->getRedirectLoginHelper();
         $response = $fb->get('/me/friends', $access_token);
        $edge = $response->getGraphEdge();
        foreach($edge as $node)
        {
            var_dump($node);
        }
        //var_dump($edge);

        
        
        return $response;
    }

    public function getAccessToken()
    {
        $helper = $this->getApiProvider()->getRedirectLoginHelper();
        $_SESSION['FBRLH_state']= $_GET['state'];
        try
        {
            $accessToken = $helper->getAccessToken();
        }
        catch (FacebookResponseException $e)
        {
            // When Graph returns an error  
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        }
        catch (FacebookSDKException $e)
        {
            // When validation fails or other local issues  
            
            //check if user authenticated
            $user = ServiceLayer::getService('security')->getIdentity()->getUser();
            
            $request = ServiceLayer::getService('request');
            $router = ServiceLayer::getService('router');
            
            if(null != $user)
            {
                  $request->redirect($router->link('player_dashboard',array('userid' => $user->getId() )));
            }
            
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        if (!isset($accessToken))
        {
            if ($helper->getError())
            {
                header('HTTP/1.0 401 Unauthorized');
                echo "Error: " . $helper->getError() . "\n";
                echo "Error Code: " . $helper->getErrorCode() . "\n";
                echo "Error Reason: " . $helper->getErrorReason() . "\n";
                echo "Error Description: " . $helper->getErrorDescription() . "\n";
            }
            else
            {
                header('HTTP/1.0 400 Bad Request');
                echo 'Bad request';
            }
            exit;
        }

        $oAuth2Client = $this->getApiProvider()->getOAuth2Client();

        // Get the access token metadata from /debug_token  
        //$tokenMetadata = $oAuth2Client->debugToken($accessToken);  
        // Validation (these will throw FacebookSDKException's when they fail)  
        //$tokenMetadata->validateAppId($config['app_id']);  
        // If you know the user ID this access token belongs to, you can validate it here  
        // $tokenMetadata->validateUserId('123');  
        //$tokenMetadata->validateExpiration();   

        if (!$accessToken->isLongLived())
        {
            // Exchanges a short-lived access token for a long-lived one  
            try
            {
                $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
            }
            catch (FacebookSDKException $e)
            {
                echo "<p>Error getting long-lived access token: " . $helper->getMessage() . "</p>";
                exit;
            }
        }
        
        return (string) $accessToken;  
    }
    
    /**
     * conver user data to register data
     * @param array $fbUserData
     * @return array
     */
    public function createRegisterDataset($fbUserData)
    {
        $result = array();
        $result['facebook_id'] = $fbUserData['id'];
        $result['name'] = $fbUserData['first_name'];
        $result['surname'] = $fbUserData['last_name'];
        $result['email'] = $fbUserData['email'];
        $result['user_type'] = 'social_user';
        $result['password'] = md5($fbUserData['email'].$fbUserData['id']);
        
        if(null != $fbUserData->getField('picture'))
        {
            $imageStream = file_get_contents($fbUserData->getField('picture')->getField('url'));	
            $img_info = pathinfo($fbUserData->getField('picture')->getField('url'));
            $extension = explode('?',$img_info['extension']);
            $extension = $extension[0];
            $filename= $img_info['filename'].'.'.$extension;
            file_put_contents(PUBLIC_DIR.'/img/users/'.$filename,$imageStream);
            file_put_contents(PUBLIC_DIR.'/img/users/thumb_90_90/'.$filename,$imageStream);
            file_put_contents(PUBLIC_DIR.'/img/users/thumb_320_320/'.$filename,$imageStream);
            file_put_contents(PUBLIC_DIR.'/img/users/thumb_640_640/'.$filename,$imageStream);
            file_put_contents(PUBLIC_DIR.'/img/users/thumb_960_960/'.$filename,$imageStream);
            $result['photo'] = $filename;
        }
        
       
		
         
        
        
        return $result;
    }
    
    
    /**
     * Find registred user with facebook id
     * @param int $fbId
     * @return object 
     */
    public function findUserByFbId($fbId)
    {
         $repository = ServiceLayer::getService('user_repository');
         $exist_user = $repository->findOneBy(array('facebook_id' => $fbId));
         return $exist_user;
    }
    
    
    
    

}
