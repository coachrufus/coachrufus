<?php

namespace Social\Manager;

use Core\ServiceLayer;
use Google_Client;
use Google_Service_Oauth2;


class GoogleManager 
{
     private $apiProvider;
    
    function __construct() {
        
        $router = ServiceLayer::getService('router');
        $client_id = GOOGLE_CLIENT_ID; 
        $client_secret = GOOGLE_CLIENT_SECRET;
        $redirect_uri = WEB_DOMAIN.$router->link('google_login_response');
        
        $client = new Google_Client();
        $client->setClientId($client_id);
        $client->setClientSecret($client_secret);
        $client->setRedirectUri($redirect_uri);
        $client->addScope("email");
        $client->addScope("profile");
        $this->setApiProvider($client);
        
    }
    
     function getApiProvider()
    {
        return $this->apiProvider;
    }

    function setApiProvider($apiProvider)
    {
        $this->apiProvider = $apiProvider;
    }
    
     public function getAccessToken($code)
    {
         $client = $this->getApiProvider();
         $client->authenticate($code);
          return  $client->getAccessToken();
    }
    
      public function getUserInfo($access_token)
    {
        if (null == $access_token)
        {
            return null;
        }
        $client = $this->getApiProvider();
        
        $client->setAccessToken($access_token);
        
        $service = new Google_Service_Oauth2($client);
        $user = $service->userinfo->get(); //get user info 
 
       
        return $user;
    }
    
     /**
     * conver user data to register data
     * @param array $data
     * @return array
     */
    public function createRegisterDataset($data)
    {
        $result = array();
        $result['google_id'] = $data['id'];
        $result['name'] = $data['givenName'];
        $result['surname'] = $data['familyName'];
        $result['email'] = $data['email'];
       // $result['photo'] = $data['picture'];
        $result['gender'] = $data['gender'];
        $result['user_type'] = 'social_user';
        $result['password'] = md5($data['email'].$data['id']);
        
        if(null != $data['picture'])
        {
            $imageStream = file_get_contents( $data['picture']);	
            $img_info = pathinfo($data['picture']);
            $extension = explode('?',$img_info['extension']);
            $extension = $extension[0];
            $filename= $img_info['filename'].md5($result['email']).'.'.$extension;
            file_put_contents(PUBLIC_DIR.'/img/users/'.$filename,$imageStream);
            file_put_contents(PUBLIC_DIR.'/img/users/thumb_90_90/'.$filename,$imageStream);
            file_put_contents(PUBLIC_DIR.'/img/users/thumb_320_320/'.$filename,$imageStream);
            file_put_contents(PUBLIC_DIR.'/img/users/thumb_640_640/'.$filename,$imageStream);
            file_put_contents(PUBLIC_DIR.'/img/users/thumb_960_960/'.$filename,$imageStream);
            $result['photo'] = $filename;
        }
        
        
        return $result;
    }
    
    
     /**
     * Find registred user with google id
     * @param int $id
     * @return object 
     */
    public function findUserByGoogleId($id)
    {
         $repository = ServiceLayer::getService('user_repository');
         $exist_user = $repository->findOneBy(array('google_id' => $id));
         return $exist_user;
    }
    
}
