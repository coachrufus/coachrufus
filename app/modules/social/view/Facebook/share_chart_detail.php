<?php $layout->startSlot('meta') ?>      
<meta property="og:image" content="<?php echo WEB_DOMAIN ?>/img/share/<?php echo $img ?>.png" />
<meta property="og:image:width" content="<?php echo $width ?>" />
<meta property="og:image:height " content="<?php echo $height ?>" />
<meta property="fb:app_id" content="<?php echo FB_APP_ID ?>" />

<meta property="og:url" content="<?php echo WEB_DOMAIN ?>/share-chart/<?php echo $img ?>" />
<meta property="og:type" content="article" />
<meta property="og:title" content="CoachRufus" />
<meta property="og:description" content="" />
<?php $layout->endSlot('meta') ?>