<?php $layout->startSlot('meta') ?>      
<meta property="og:image" content="<?php echo WEB_DOMAIN ?>/img/share/events/<?php echo $guid ?>.png" />
<meta property="og:image:width" content="<?php echo $width ?>" />
<meta property="og:image:height " content="<?php echo $height ?>" />
<meta property="fb:app_id" content="<?php echo FB_APP_ID ?>" />
<meta property="og:url" content="<?php echo $backLink ?>" />
<meta property="og:type" content="article" />
<meta property="og:title" content="<?php echo $title ?>" />
<meta property="og:description" content="<?php echo $note ?>" />
<?php $layout->endSlot('meta') ?>