<?php
namespace Social;
require_once(MODUL_DIR.'/social/SocialModule.php');
use Core\ServiceLayer as ServiceLayer;

ServiceLayer::addService('facebook_manager',array(
'class' => "Social\Manager\FacebookManager",
));

ServiceLayer::addService('google_manager',array(
'class' => "Social\Manager\GoogleManager",
));


//security
$acl = ServiceLayer::getService('acl');
$router = ServiceLayer::getService('router');

$acl->allowRole('guest','fb_login');
$router->addRoute('fb_login','/social/fb/login',array(
		'controller' => 'Social\SocialModule:Facebook:login'
));

$acl->allowRole('guest','fb_remote_register');
$router->addRoute('fb_remote_register','/social/fb/remote-register',array(
		'controller' => 'Social\SocialModule:Facebook:remoteRegister'
));



$acl->allowRole('guest','fb_login_response');
$router->addRoute('fb_login_response','/social/fb/login-response',array(
		'controller' => 'Social\SocialModule:Facebook:loginResponse'
));


$acl->allowRole('guest','google_login');
$router->addRoute('google_login','/social/google/login',array(
		'controller' => 'Social\SocialModule:Google:login'
));

$acl->allowRole('guest','google_remote_register');
$router->addRoute('google_remote_register','/social/google/remote-register',array(
		'controller' => 'Social\SocialModule:Google:remoteRegister'
));

$acl->allowRole('guest','google_login_response');
$router->addRoute('google_login_response','/social/google/login-response',array(
		'controller' => 'Social\SocialModule:Google:loginResponse'
));

$acl->allowRole('guest','invite_fb_friends');
$router->addRoute('invite_fb_friends','/social/fb/invite-friends',array(
		'controller' => 'Social\SocialModule:Facebook:inviteFriends'
));

$acl->allowRole('guest', 'chart_share_detail');
$router->addRoute('chart_share_detail', '/share-chart/:img_name', [
    'controller' => 'Social\SocialModule:Facebook:shareChartDetail'
]);

$acl->allowRole('guest', 'event_share');
$router->addRoute('event_share', '/share-event/:guid', [
    'controller' => 'Social\SocialModule:Facebook:shareEvent'
]);






