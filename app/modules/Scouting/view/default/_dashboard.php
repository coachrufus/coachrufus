<div class="panel panel-default">
    <div class="panel-heading">

        <h3>
            <i class="ico ico-search"></i> 
            <?php echo $translator->translate('Find team or player') ?>
        </h3>
        <div class="pull-right panel-tools">
            <a href="#" class="panel-settings-trigger" data-target="scouting-panel-settings"><i class="ico ico-plus-green"></i></a>
            <i class="ico ico-action ico-panel-close"></i>
        </div>
    </div>
    <div  class="panel-settings" id="scouting-panel-settings">
        <div class="settings-row">
            <a class="add-scouting-trigger" data-form="scouting-form-wrap3" href="#"><i class="ico ico-scouting-player-looking-team"></i> <?php echo $translator->translate('Player looking for team') ?></a>
        </div>
        <div class="settings-row">
            <a class="add-scouting-trigger" data-form="scouting-form-wrap1" href="#"><i class="ico ico-scouting-team-looking-player"></i> <?php echo $translator->translate('Team looking for player') ?></a>
        </div>
        <div class="settings-row">
            <a class="add-scouting-trigger" data-form="scouting-form-wrap2" href="#"><i class="ico ico-scouting-team-looking-game-player"></i> <?php echo $translator->translate('Team looking for game player') ?></a>
        </div>
        <div class="settings-row">
            <a class="add-scouting-trigger" data-form="scouting-form-wrap4" href="#"><i class="ico ico-scouting-team-looking-playground"></i> <?php echo $translator->translate('Team looking for playground') ?></a>
        </div> 
    </div>
    <?php $layout->includePart(MODUL_DIR . '/Scouting/view/default/_panel_search.php',array('filterForm' => $filterForm)) ?>
    <div class="panel-body  panel-full-body">

        <div id="scouting-cnt">






<div  id="team_scouting_panel">

    <?php if(!$list->isEmpty()): ?>
         <?php foreach ($list->getItems() as $item): ?>
            <?php $layout->includePart(MODUL_DIR . '/Scouting/view/default/_scouting_row.php', 
                    array(
                        'item' => $item, 
                    )) ?>
         <?php endforeach; ?>
    <?php else: ?>
    
    <div class="empty-scouting">
        <?php echo $translator->translate('No offers near you') ?>
        <strong><?php echo $translator->translate('Add your offer!') ?></strong>
        <div class="settings-row">
            <a class="add-scouting-trigger btn btn-green" data-form="scouting-form-wrap3" href="#"> <?php echo $translator->translate('Player looking for team') ?></a>
        </div>
        <div class="settings-row">
            <a class="add-scouting-trigger  btn btn-green" data-form="scouting-form-wrap1" href="#"> <?php echo $translator->translate('Team looking for player') ?></a>
        </div>
        <div class="settings-row">
            <a class="add-scouting-trigger  btn btn-green" data-form="scouting-form-wrap2" href="#"> <?php echo $translator->translate('Team looking for game player') ?></a>
        </div>
        <div class="settings-row">
            <a class="add-scouting-trigger btn btn-green" data-form="scouting-form-wrap4" href="#"><?php echo $translator->translate('Team looking for playground') ?></a>
        </div> 
    </div>
   
     <?php endif; ?>
    
     <?php $layout->includePart(MODUL_DIR . '/Scouting/view/default/_reply_modal.php',array('form' => $replyForm)) ?>

</div>
 <a class="all-sr-link" href="<?php echo $router->link('scouting') ?>"><?php echo $translator->translate('View All Request') ?></a>
    </div>
</div>
</div>

       

