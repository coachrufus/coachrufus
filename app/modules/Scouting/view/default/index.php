<section class="content">
    <div class="row">
        <div class="col-sm-8">
            <div class="panel panel-default">
                <div class="panel-heading">

                    <h3>
                        <i class="ico ico-panel-event"></i> 
                        <?php echo $translator->translate('Find team or player') ?>
                    </h3>
                    <div class="pull-right panel-tools">
                        <a href="#" class="panel-settings-trigger" data-target="scouting-panel-settings"><i class="ico ico-plus-green"></i></a>
                        <i class="ico ico-action ico-panel-close"></i>
                    </div>
                </div>
                <div  class="panel-settings" id="scouting-panel-settings">
                    <div class="settings-row">
                        <a class="add-scouting-trigger" data-form="scouting-form-wrap3" href="#"><i class="ico ico-scouting-player-looking-team"></i> <?php echo $translator->translate('Player looking for team') ?></a>
                    </div>
                    <div class="settings-row">
                        <a class="add-scouting-trigger" data-form="scouting-form-wrap1" href="#"><i class="ico ico-scouting-team-looking-player"></i> <?php echo $translator->translate('Team looking for player') ?></a>
                    </div>
                    <div class="settings-row">
                        <a class="add-scouting-trigger" data-form="scouting-form-wrap2" href="#"><i class="ico ico-scouting-team-looking-game-player"></i> <?php echo $translator->translate('Team looking for game player') ?></a>
                    </div>
                    <div class="settings-row">
                        <a class="add-scouting-trigger" data-form="scouting-form-wrap4" href="#"><i class="ico ico-scouting-team-looking-playground"></i> <?php echo $translator->translate('Team looking for playground') ?></a>
                    </div> 
                </div>
                
                <?php $layout->includePart(MODUL_DIR . '/Scouting/view/default/_panel_search.php',array('filterForm' => $filterForm)) ?>
                

                <div class="panel-body  panel-full-body">
                    
                    
                    <div id="map" style="width:100%; height:400px;"></div>
                    


                    <div id="scouting-cnt">
                        <div  id="team_scouting_panel">
                            
                          
                            
                          
                            
                             <?php $items  = $scoutingItems->getItems();
                             if(empty($items)): ?>
                            <div class="text-center alert"><?php echo $translator->translate('SCOUTING_NULL_RESULTS') ?></div>
                           
                             <?php else: ?>
                            
                                <?php foreach ($scoutingItems->getItems() as $item): ?>

                                    <?php if($showReplyForm): ?>
                                        <?php $layout->startSlot('meta') ?>
                                        <meta property="og:image" content="<?php echo WEB_DOMAIN ?>/theme/img/fb_logo.png" />
                                        <meta property="og:image:width" content="650" />
                                        <meta property="og:image:height" content="650" />
                                         <meta name="description" content="<?php echo $item->getShareContent() ?>" />            
                                        <meta property="fb:app_id" content="944943362242789" />
                                        <meta property="og:url" content="<?php echo WEB_DOMAIN ?><?php echo $router->link('scouting',array('id' => $item->getId())) ?>" />
                                        <meta property="og:type" content="article" />
                                        <meta property="og:title" content="<?php echo $item->getPerex() ?>" />
                                        <meta property="og:description" content="<?php echo $item->getShareContent() ?>" />
                                        <?php $layout->endSlot('meta') ?>
                                    <?php endif; ?>


                                    <?php
                                    $layout->includePart(MODUL_DIR . '/Scouting/view/default/_scouting_row.php', array(
                                        'item' => $item,
                                    ))
                                    ?>
                                <?php endforeach; ?>
                            <?php endif; ?>

<?php $layout->includePart(MODUL_DIR . '/Scouting/view/default/_reply_modal.php', array('form' => $replyForm)) ?>

                        </div>
                        <?php if (!$scoutingItems->allItemsLoaded()): ?>
                            <a class="btn btn-primary load-scouting" data-start="<?php echo $item->getId() ?>"  href="<?php echo $router->link('rest_search_scouting_items') ?>" ><?php echo $translator->translate('Load More') ?></a>
<?php endif; ?>  
                    </div>
                </div>
            </div>
        </div>
          <div class="col-md-4">
            <?php $layout->includePart(MODUL_DIR . '/Event/view/widget/_small_calendar.php') ?>
        </div>
    </div>
</section>

<div class="modal fade" id="add-scouting-modal" tabindex="-1" role="dialog" aria-labelledby="<?php echo $translator->translate('Add Scouting') ?>">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?php echo $translator->translate('Add scouting') ?></h4>
            </div>
            <div class="modal-body">
                <?php echo $layout->renderControllerAction('CR\Scouting\ScoutingModule:Default:playerLookingTeam')->getContent() ?>
                <?php echo $layout->renderControllerAction('CR\Scouting\ScoutingModule:Default:teamLookingPlayer')->getContent() ?>
                <?php echo $layout->renderControllerAction('CR\Scouting\ScoutingModule:Default:teamLookingGamePlayer')->getContent() ?>
<?php echo $layout->renderControllerAction('CR\Scouting\ScoutingModule:Default:teamLookingPlayground')->getContent() ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $translator->translate('Cancel') ?></button>
            </div>
        </div>
    </div>
</div>

<?php $layout->includePart(MODUL_DIR . '/Scouting/view/default/_remove_modal.php') ?>


<?php $layout->addJavascript('js/TeamEventManager.js') ?>
<?php $layout->addJavascript('js/ScoutingManager.js') ?>
<?php $layout->addJavascript('js/SmallCalendar.js') ?>
<?php $layout->addJavascript('/plugins/select2/select2.full.min.js') ?>

<?php $layout->addStylesheet('plugins/pickadate/themes/classic.css') ?>
<?php $layout->addStylesheet('plugins/pickadate/themes/classic.date.css') ?>
<?php $layout->addJavascript('plugins/pickadate/picker.js') ?>
<?php $layout->addJavascript('plugins/pickadate/picker.date.js') ?>

<?php $layout->addStylesheet('plugins/timepicker/bootstrap-timepicker.min.css') ?>
<?php $layout->addJavascript('plugins/timepicker/bootstrap-timepicker.js') ?>

<?php $layout->addJavascript('plugins/textcomplete/jquery.textcomplete.min.js') ?>
<?php $layout->addJavascript('js/fb.js') ?>

<?php $layout->startSlot('javascript') ?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXU1duK54f1gIsRy_xc7z4uQURovsZdE8&libraries=places&callback=initLocalityManager" async defer></script>
<script type="text/javascript">
     var small_calendar = new SmallCalendar({
        'dataUrl': '<?php echo $router->link('rest_event_get_month_grid') ?>',
        'localityManager': new LocalityManager()
    });
    small_calendar.initLoad();
    
    
    event_manger = new TeamEventsManager();
    event_manger.init();
    
    $('#test').textcomplete([
        {// mention strategy
            match: /(^|\s)@(\w*)$/,
            search: function (term, callback) {
                $.getJSON('/scouting/filter/sports', {q: term})
                        .done(function (resp) {
                            callback(resp.mentions); // `resp` must be an Array
                        })
                        .fail(function () {
                            callback([]); // Callback must be invoked even if something went wrong.
                        });
                /*
                 mentions = [
                 {id:10,fullName:'Tomáš Nagy',nickname:'nagy'},
                 {id:5,fullName:'marek',nickname:'hulvat'},
                 {id:5, fullName:'martin konarsky',nickname:'tiko'}
                 ];
                  
                 term_length = term.length;
                 callback($.map(mentions, function (mention) {
                 if(mention.nickname.substring(0,term_length) === term )
                 {
                 return mention.fullName;
                 }
                 else
                 {
                 return null;
                 }
                 }));
                 */
            },
            replace: function (value) {
                return '$1' + value + ' ';
            },
            cache: true
        },
        {// emoji strategy
            match: /(^|\s):(\w*)$/,
            search: function (term, callback) {
                var regexp = new RegExp('^' + term);
                callback($.grep(emojies, function (emoji) {
                    return regexp.test(emoji);
                }));
            },
            replace: function (value) {
                return '$1:' + value + ': ';
            }
        }
    ], {maxCount: 10, debounce: 100});

    


    var scouting_manger = new ScoutingManager({
        'dataUrl': '<?php echo $router->link('rest_last_scouting') ?>'
    });
    scouting_manger.bindTriggers();
    
    <?php if($showReplyForm): ?>
        $('.scouting-reply').trigger('click');
    <?php endif; ?>
    


    $(".select2").select2();
    jQuery('#scouting_game_time').pickadate({'format': 'dd.mm.yyyy'});
    jQuery('#scouting_valid_to').pickadate({'format': 'dd.mm.yyyy'});

    jQuery("#scouting_game_time_time").timepicker({showInputs: false, showMeridian: false, minuteStep: 5});
    jQuery("#scouting_game_time_to_time").timepicker({defaultTime:'00:00',showInputs: false,showMeridian: false, minuteStep: 5});
     if(jQuery("#scouting_game_time_to_time").val() == '00:00')
        {
            jQuery("#scouting_game_time_to_time").val('');
        }

    function initLocalityManager()
    {

        locality_manger = new LocalityManager();
        locality_manger.google = google;
        locality_manger.createSearchInput('scouting-form-location1');

        locality_manger2 = new LocalityManager();
        locality_manger2.google = google;
        locality_manger2.createSearchInput('scouting-form-location2');

        locality_manger3 = new LocalityManager();
        locality_manger3.google = google;
        locality_manger3.createSearchInput('scouting-form-location3');

        locality_manger4 = new LocalityManager();
        locality_manger4.google = google;
        locality_manger4.createSearchInput('scouting-form-location4');
        
       scouting_manger.bindSearchFormLocationTrigger(google);
        
       
    }



</script>
<?php $layout->endSlot('javascript') ?>