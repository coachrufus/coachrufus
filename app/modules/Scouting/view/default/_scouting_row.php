 <div class="scouting-row <?php echo ($item->getAuthorId() == \Core\ServiceLayer::getService('security')->getIdentity()->getUser()->getId()) ? 'scouting-row-user' : ''  ?>" id="scouting-row-<?php echo $item->getId() ?>"> 

     
   
     
     
            <div class="avatar" style="background-image: url('<?php echo $item->getAuthor()->getMidPhoto() ?>')"> </div>
                <div class="sr-sidebar">
                     <?php if ($item->getAuthorId() == \Core\ServiceLayer::getService('security')->getIdentity()->getUser()->getId()): ?>
                    <a href="#" class="sr-sidebar-trigger" data-target="sr-sidebar-cnt-<?php echo $item->getId() ?>"><i class="ico ico-more-b"  aria-hidden="true"></i></a>
                     <?php endif; ?>
                     <i class="ico scouting-type-icon ico-scouting-<?php echo $item->getType()  ?>"></i>
                </div>
                 <?php if ($item->getAuthorId() == \Core\ServiceLayer::getService('security')->getIdentity()->getUser()->getId()): ?>
                    <div class="sr-sidebar-cnt" id="sr-sidebar-cnt-<?php echo $item->getId() ?>" >
                        <a class="remove-scouting-trigger" href="<?php echo $router->link('delete_scouting', array('id' => $item->getId())) ?>"><?php echo $translator->translate('Delete') ?> <i class="ico ico-trash"></i></a>
                    </div>
                 <?php endif; ?>
            
            
            
            <div class="sr-cnt">
                <h3><a class="scouting-reply" href="<?php echo $router->link('scouting_send_contact_message',array('id' => $item->getId() )) ?>"><?php echo $item->getPerex() ?></a></h3>
                <p><?php echo $item->getContent() ?></p>
            </div>
            
            <div class="sr-footer">
               
                <a class="scouting-reply" href="<?php echo $router->link('scouting_send_contact_message',array('id' => $item->getId() )) ?>"><i class="ico ico-reply"></i> <?php echo $translator->translate('reply') ?></a>
            
                <a title="<?php echo $translator->translate('Share on facebook') ?>" class="share-scouting" href="<?php echo WEB_DOMAIN ?><?php echo $router->link('scouting_detail',array('id' => $item->getId())) ?>"><?php echo $translator->translate('share') ?> <i class="ico ico-share"></i></a>
                
                <span class="created-date"><?php echo $translator->translate('Created at') ?>: <?php echo $item->getCreatedAt()->format('d.m.Y H:i')  ?></span>
            </div>

        </div>