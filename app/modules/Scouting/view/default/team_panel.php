<div  id="team_scouting_panel">
<?php foreach ($items as $item): ?>
    
     <?php if( \Core\ServiceLayer::getService('security')->getIdentity()->hasTeamPermission('manageScouting',$team)): ?>
     <div class="pull-right">
            <a class="remove-scouting-trigger" href="<?php echo $router->link('delete_scouting',array('id' => $item['scouting']->getId())) ?>"><i class="fa fa-trash"></i></a>
        </div>
    <?php endif; ?>
    
    <strong><?php echo $item['scouting']->getCreatedAt()->format('d.m.Y') ?></strong>
    <p class='text-muted'>
    <?php echo $item['content'] ?>
    </p>
    <hr />
<?php endforeach; ?>
</div>

<div class="well well-sm">
    <div class="row">
         <div class="col-sm-4">
             <label><?php echo $translator->translate('New scouting') ?></label>
 </div>
        <div class="col-sm-8">
            <select class="form-control" id="scouting-form-choice">
                <option value=""><?php echo $translator->translate('Select scounting type') ?></option>
                <option value="1"><?php echo $translator->translate('Team looking for Team player') ?></option>
                <option value="2"><?php echo $translator->translate('Team looking for game player') ?></option>
                <option value="3"><?php echo $translator->translate('Team looking for playground') ?></option>
            </select>
        </div>
       
    </div>
</div>




 <!-- Modal -->
<div class="modal fade" id="removeScoutingModal" tabindex="-1" role="dialog" aria-labelledby="<?php echo $translator->translate('Remove scouting') ?>">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?php echo $translator->translate('Remove scouting') ?></h4>
            </div>
            <div class="modal-body">
                <?php echo $translator->translate('Are you sure?') ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $translator->translate('Cancel') ?></button>
                <button type="button" class="btn btn-danger modal_submit"><?php echo $translator->translate('Confirm') ?></button>
            </div>
        </div>
    </div>
</div>