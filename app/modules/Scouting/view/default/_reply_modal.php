<div class="modal fade" id="reply-scouting-modal" tabindex="-1" role="dialog" aria-labelledby="<?php echo $translator->translate('Reply') ?>">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
         <div class="modal-loader">
                  <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                  <span><?php echo $translator->translate('Sending...') ?></span>
              </div>
         
        
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $translator->translate('Reply') ?></h4>
      </div>
      <div class="modal-body">
          
        
          
        <div class="alert alert-success send-success" style="display: none;">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $translator->translate('REPLY_SCOUTING_SUCCES_MESSAGE') ?>
        </div>
          
          
               <form action="" method="post" class="scouting-reply-form">
                           <blockquote id="reply-scouting">
     
                           </blockquote>
                 <div class="row">

                    <div class="form-group col-sm-12" data-error-bind="subject">
                         <label><?php echo $translator->translate('Subject') ?></label>
                         <?php echo $form->renderInputTag('subject', array('class' => 'form-control', 'style' => 'width:100%;')) ?>
                     </div>

                     <div class="form-group col-sm-12" data-error-bind="text">
                         <label><?php echo $translator->translate('Message') ?></label>
                         <?php echo $form->renderTextareaTag('text', array('maxlength' => 1000,'class' => 'form-control')) ?>
                     </div>
                 </div>



             
                <div class="row">
                 <div class="form-group col-sm-12">
                     <button type="submit" class="btn btn-primary"><?php echo $translator->translate('Send') ?></button>
                 </div>
                 </div>
         </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $translator->translate('Close') ?></button>
      </div>
    </div>
  </div>
</div>