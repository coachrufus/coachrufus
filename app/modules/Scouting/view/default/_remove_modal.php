 <!-- Modal -->
<div class="modal fade" id="removeScoutingModal" tabindex="-1" role="dialog" aria-labelledby="<?php echo $translator->translate('Remove scouting') ?>">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?php echo $translator->translate('Remove scouting') ?></h4>
            </div>
            <div class="modal-body">
                <?php echo $translator->translate('Are you sure?') ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $translator->translate('Cancel') ?></button>
                <button type="button" class="btn btn-danger modal_submit"><?php echo $translator->translate('Confirm') ?></button>
            </div>
        </div>
    </div>
</div>