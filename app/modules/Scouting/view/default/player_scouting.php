<div  id="player_scouting_panel">
<?php foreach ($items as $item): ?>
    
    
     <div class="pull-right">
            <a class="remove-scouting-trigger" href="<?php echo $router->link('delete_player_scouting',array('id' => $item['scouting']->getId())) ?>"><i class="fa fa-trash"></i></a>
        </div>

    
    <strong><?php echo $item['scouting']->getCreatedAt()->format('d.m.Y') ?></strong>
    <p class='text-muted'>
    <?php echo $item['content'] ?>
    </p>
    <hr />
<?php endforeach; ?>
</div>




