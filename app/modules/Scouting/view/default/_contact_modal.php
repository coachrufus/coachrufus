<div class="modal fade" id="contact-scouting-modal" tabindex="-1" role="dialog" aria-labelledby="<?php echo $translator->translate('Send message') ?>">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $translator->translate('Send message') ?></h4>
      </div>
      <div class="modal-body">
        <form role="form" id="contact_scouting_form" action="<?php echo $router->link('scouting_send_contact_message') ?>">
        <input type="hidden" name="id" id="contact_scouting_form_id" value="" />

             <div class="form-group col-sm-12"  data-error-bind="season_unique">
                <label class="control-label"><?php echo $translator->translate("Message") ?></label>
                <textarea class="form-control" name="msg"></textarea>
            </div>
 
                <div class="form-group col-sm-6">
                    <button type="submit" class="btn btn-primary add-season-submit">Submit</button>
               </div>
              

               
                </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>

