<div class="panel-search">
    <form  class="scouting-search-form" action="<?php echo $router->link('scouting') ?>">
    <div id="current-selected-types" class="selected-choices <?php echo (!empty($filterForm->getCurrentTypes())) ? 'active' : '' ?>">
       <?php foreach($filterForm->getCurrentTypes() as $typeVal => $typeName): ?>
        <span><i data-target="selected-types" class="ico ico-close"></i><input type="hidden" name="type[]" value="<?php echo $typeVal  ?>"><?php echo $typeName  ?></span>
        <?php endforeach; ?>
    </div>
        
     <div id="current-selected-sports" class="selected-choices <?php echo (!empty($filterForm->getCurrentSports())) ? 'active' : '' ?>">
       <?php foreach($filterForm->getCurrentSports() as $sportVal => $sportName): ?>
        <span><i data-target="selected-sports" class="ico ico-close"></i><input type="hidden" name="sport[]" value="<?php echo $sportVal  ?>"><?php echo $sportName  ?></span>
        <?php endforeach; ?>
        
    </div>
   
        <div class="scouting-select-criteria">
    
    <div class="btn-group scouting-type-dropdown">
        <button type="button" class="dropdown-toggle clear-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <?php echo $translator->translate($filterForm->getCurrentTypeName()) ?> 
           <i class="ico ico-dropdown"></i>
        </button>
        <ul class="dropdown-menu" id="selected-types">
             <?php foreach($filterForm->getAvailableTypeChoices() as $typeId => $typeName): ?>
            <li><a class="add_scouting_criteria_trigger"  data-type="type" data-target="current-selected-types" data-type-val="<?php echo $typeId ?>" href="#" ><?php echo $translator->translate($typeName) ?></a></li>
           <?php endforeach; ?>
        </ul>
    </div>
        
   
    
    <div class="btn-group">
        <button type="button" class="dropdown-toggle clear-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
           <?php echo $translator->translate($filterForm->getCurrentSportName()) ?> 
           <i class="ico ico-dropdown"></i>
        </button>
        
        <ul class="dropdown-menu"  id="selected-sports">
           <?php foreach($filterForm->getAvailableSportChoices() as $sportChoiceId => $sportChoiceName): ?>
            <li><a class="add_scouting_criteria_trigger" href="#" data-type="sport" data-target="current-selected-sports" data-type-val="<?php echo $sportChoiceId ?>" ><?php echo $translator->translate($sportChoiceName) ?></a></li>
           <?php endforeach; ?>
        </ul>
    </div>
            


        <input type="text" name="location_name" id="search-scouting-form" placeholder="<?php echo $translator->translate('Čo hľadáš?') ?>"  value="<?php echo ($filterForm->getFieldValue('location_name') == null) ? '' : $filterForm->getFieldValue('location_name') ?>" />
         <input type="hidden" name="coordinates" id="search-scouting-form-coordinates" value="<?php echo $filterForm->getFieldValue('coordinates') ?>" />



        
            
    
        
    <button class="btn btn-scouting-search btn-green" type="submit">  <?php echo $translator->translate('Search') ?> </button>
     </div>
    </form>
</div>
