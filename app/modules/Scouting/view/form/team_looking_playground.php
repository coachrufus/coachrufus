<div class="scouting-form-wrap scouting-form-wrap4">
    <form action="<?php echo $router->link('create_scouting') ?>" method="post" class="scouting-form ajax-form" >
        <input type="hidden" name="st" value="4" />


        <div class="row">
            <div class="form-group col-sm-6" data-error-bind="player_count">
                <label><?php echo $translator->translate('Team') ?></label>
                <?php echo $form->renderSelectTag('team_id', array('class' => 'form-control')) ?>
            </div>

            <div class="form-group col-sm-6" data-error-bind="sport_id">
                <label><?php echo $translator->translate('Sport') ?></label>
                <?php echo $form->renderSelectTag('sport_id', array('class' => 'select2 form-control', 'style' => 'width:100%;','required' => 'required',)) ?>
            </div>



        </div>
        <div class="row">
            <div class="form-group col-sm-6" data-error-bind="location_name">
                <label><?php echo $translator->translate('Location') ?></label>
                <?php echo $form->renderInputTag('location_name', array('class' => 'form-control', 'id' => 'scouting-form-location4', 'placeholder' => $translator->translate('City name, street, etc.'))) ?>
            </div>

        </div>

        <div class="row">
            <?php echo $form->renderInputHiddenTag('locality_json_data', array('class' => 'playground_locality')) ?>
            <div class="form-group col-sm-12">
                <button type="submit" class="btn btn-primary"><?php echo $translator->translate('Create') ?></button>
            </div>
        </div>
</form>
</div>