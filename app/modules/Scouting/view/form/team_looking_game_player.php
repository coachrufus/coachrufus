<div class="scouting-form-wrap scouting-form-wrap2">
    <form action="<?php echo $router->link('create_scouting') ?>" method="post" class="scouting-form ajax-form" >
        <input type="hidden" name="st" value="2" />
        <div class="row">
             <div class="form-group col-sm-5" data-error-bind="player_count">
                <label><?php echo $translator->translate('Team') ?></label>
                <?php echo $form->renderSelectTag('team_id', array('class' => 'form-control')) ?>
            </div>
            
            <div class="form-group col-sm-3" data-error-bind="player_count">
                <label><?php echo $translator->translate('Players count') ?></label>
                <?php echo $form->renderInputTag('player_count', array('class' => 'form-control')) ?>
            </div>

            <div class="form-group col-sm-4" data-error-bind="level">
                <label><?php echo $translator->translate('Player level') ?></label>
                <?php echo $form->renderSelectTag('level', array('class' => 'form-control','required' => 'required',)) ?>
            </div>
        </div>

        <div class="row">
           


            <div class="form-group col-sm-6" data-error-bind="location_name">
                <label><?php echo $translator->translate('Location') ?></label>
                <?php echo $form->renderInputTag('location_name', array('class' => 'form-control', 'id' => 'scouting-form-location2', 'placeholder' => $translator->translate('City name, street, etc.'))) ?>
            </div>
            
              <div class="form-group col-sm-6" data-error-bind="playground_id">
                <label><?php echo $translator->translate('Playground') ?></label>
                <?php echo $form->renderSelectTag('playground_id', array('class' => 'form-control','required' => 'required',)) ?>
            </div>

        </div>
        
        <div class="row">
          


            <div class="form-group col-sm-6" data-error-bind="game_time">
                <label><?php echo $translator->translate('Termin') ?></label>
                
                   
            <div class="input-group">
                <div class="input-group-addon">
                    <i class="fa fa fa-calendar"></i>
                </div>
                 <?php echo $form->renderInputTag('game_time', array('class' => 'form-control','required' => 'required',)) ?>
            </div>
                
                
               
            </div>
            
        
                
                 
                    <div class="form-group col-sm-6" data-error-bind="game_time_time">
                        <div class="bootstrap-timepicker">
                      <label><?php echo $translator->translate($form->getFieldLabel("game_time_time")) ?></label>
                      <div class="input-group"><div class="input-group-addon">
                          <i class="fa fa-clock-o"></i>
                        </div>
                       <?php echo $form->renderInputTag("game_time_time", array('class' => 'form-control','required' => 'required',)) ?>
                        
                      </div>
                    </div>
                  </div>
                
                
              
            
            

        </div>
        
        
        <div class="row">
            
              <div class="form-group col-sm-6" data-error-bind="game_time_to_time">
                        <div class="bootstrap-timepicker">
                      <label><?php echo $translator->translate($form->getFieldLabel("game_time_to_time")) ?></label>
                      <div class="input-group"><div class="input-group-addon">
                          <i class="fa fa-clock-o"></i>
                        </div>
                       <?php echo $form->renderInputTag("game_time_to_time", array('class' => 'form-control','required' => 'required',)) ?>
                        
                      </div>
                    </div>
                  </div>
            

            <div class="form-group col-sm-6" data-error-bind="valid_to">
                <label><?php echo $translator->translate('Valid to') ?></label>
                <?php echo $form->renderInputTag('valid_to', array('class' => 'form-control','required' => 'required',)) ?>
            </div>

       
        
        
        
        
         <?php echo $form->renderInputHiddenTag('locality_json_data', array('class' => 'playground_locality')) ?>
        <div class="form-group col-sm-12">
            <button type="submit" class="btn btn-primary"><?php echo $translator->translate('Create') ?></button>
        </div>
             </div>
    </form>
</div>