<div class="scouting-widget">
    
</div>



<div class="modal fade" id="add-scouting-modal" tabindex="-1" role="dialog" aria-labelledby="<?php echo $translator->translate('Add Scouting') ?>">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $translator->translate('Add scouting') ?></h4>
      </div>
      <div class="modal-body">
            <?php echo $layout->renderControllerAction('CR\Scouting\ScoutingModule:Default:playerLookingTeam')->getContent() ?>
            <?php echo $layout->renderControllerAction('CR\Scouting\ScoutingModule:Default:teamLookingPlayer')->getContent() ?>
            <?php echo $layout->renderControllerAction('CR\Scouting\ScoutingModule:Default:teamLookingGamePlayer')->getContent() ?>
            <?php echo $layout->renderControllerAction('CR\Scouting\ScoutingModule:Default:teamLookingPlayground')->getContent() ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $translator->translate('Cancel') ?></button>
      </div>
    </div>
  </div>
</div>

 <?php $layout->includePart(MODUL_DIR . '/Scouting/view/default/_remove_modal.php') ?>







