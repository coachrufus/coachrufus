<?php

namespace CR\Scouting\Controller;

use AclException;
use Core\Controller as Controller;
use Core\ServiceLayer;
use CR\Scouting\Form\PlayerLookingTeamForm;
use CR\Scouting\Form\TeamLookingGamePlayerForm;
use CR\Scouting\Form\TeamLookingPlayerForm as TeamLookingPlayerForm;
use CR\Scouting\Form\TeamLookingPlaygroundForm;
use Core\RestApiController;

class RestController extends RestApiController {

    public function lastScoutingAction()
    {
        
        $responseType = (null == $this->getRequest()->get('rt')) ? 'json' : $this->getRequest()->get('rt');
        $manager = ServiceLayer::getService('ScoutingManager');
        $localityManager = ServiceLayer::getService('LocalityManager');

        //last scouting
        $manager->setItemPerPage(2);
       
        $replyForm = new \CR\Scouting\Form\ReplyForm();
        
        $filterForm = new \CR\Scouting\Form\FilterForm();
        $sports = $manager->getFilterSportChoices();
        $filterForm->setFieldChoices('sport', $sports);
        
        $criteria = array();
        $currentLocation = $localityManager->getLocalityByIp();
       
        
        if(null != $currentLocation && null != $currentLocation->city)
        {
            $filterForm->setFieldValue('location_name', $currentLocation->city);
             $filterForm->setFieldValue('coordinates', $currentLocation->latitude.','.$currentLocation->longitude);
             $criteria['filter']['coordinates'] =  $currentLocation->latitude.','.$currentLocation->longitude;
        }
       
        $list = $manager->getLastScouting($criteria);
        


        if ($responseType == 'html')
        {
            $html = $this->render('CR\Scouting\ScoutingModule:default:_dashboard.php', array(
                       'list' => $list,
                       'replyForm' => $replyForm,
                       'filterForm' => $filterForm
                  
                    ))->getContent();
            return $this->asHtml($html);
        }

    }
    
    
    
    public function searchAction()
    {
        $manager = ServiceLayer::getService('ScoutingManager');
        $filterForm = new \CR\Scouting\Form\FilterForm();
        $criteria = $filterForm->handleSearchRequest($this->getRequest());

        
        $criteria['start'] = $this->getRequest()->get('from');
        $scoutingItems = $manager->getScouting($criteria);
        $lines = array();
        foreach($scoutingItems->getItems() as $item)
        {
          ob_start();
                ServiceLayer::getService('layout')->includePart(MODUL_DIR.'/Scouting/view/default/_scouting_row.php',array('item' => $item ));
               $lines[] = ob_get_contents();
          ob_end_clean();
        }
       
      $result = array('result' => 'SUCCESS','lines' =>  $lines,'start' => $item->getId(),'allItemsLoaded' => $scoutingItems->allItemsLoaded());
      return $this->asJson($result);
       
    }
            
    
     public function sendContactMessageAction()
    {
        $manager = ServiceLayer::getService('ScoutingManager');
        $messageManager = ServiceLayer::getService('MessageManager');
        $playerManager = ServiceLayer::getService('PlayerManager');
        
        $scoutingData = $this->getRequest()->get('reply');
        
        $scouting = $manager->getScoutingById($this->getRequest()->get('id'));


                
        $message = new \CR\Message\Model\Message();
        $message->setSubject($scoutingData['subject']);
        $message->setBody($scoutingData['text']);
        $message->setReplyGroup(md5(time().$scouting->getId()));
        
        $recipient = $playerManager->findPlayerById($scouting->getAuthorId());
        $sender = ServiceLayer::getService('security')->getIdentity()->getUser();
        
        $mid  = $messageManager->createMessage($message,$sender,$recipient);
        $message->setId($mid);
        $message->setSenderName($sender->getFullName());
        $message->setRecipientEmail($recipient->getEmail());
        $messageManager->sendMessageNotify($message);
        
        $response = array();
        $response['result'] = 'SUCCESS';
         return $this->asJson($response);
    }
}
