<?php

namespace CR\Scouting\Controller;

use AclException;
use Core\Controller as Controller;
use Core\ServiceLayer;
use CR\Scouting\Form\PlayerLookingTeamForm;
use CR\Scouting\Form\TeamLookingGamePlayerForm;
use CR\Scouting\Form\TeamLookingPlayerForm as TeamLookingPlayerForm;
use CR\Scouting\Form\TeamLookingPlaygroundForm;

class DefaultController extends Controller {

    public function detailAction()
    {
        if (
            strpos($_SERVER["HTTP_USER_AGENT"], "facebookexternalhit/") !== false ||          
            strpos($_SERVER["HTTP_USER_AGENT"], "Facebot") !== false
        ) 
        {
            //facebook creawler
            $manager = ServiceLayer::getService('ScoutingManager');
            $scouting = $manager->getScoutingById($this->getRequest()->get('id'));

            $scouting = $manager->getScoutingById($this->getRequest()->get('id'));
            $scoutingItems = $manager->buildScoutingCollection(array($scouting));
            $items = $scoutingItems->getItems();
            $item = $items[0];

            $layout = ServiceLayer::getService('layout');
            $layout->setTemplate(GLOBAL_DIR.'/templates/ClearLayout.php');

            return  $this->render('CR\Scouting\ScoutingModule:default:share_detail.php', array(
                          'item' => $item
                        ));
        }
        else {
           
           $this->getRequest()->redirect($this->getRouter()->link('scouting',array('id' => $this->getRequest()->get('id') ))); 
            
        }

       
    }
    
    public function indexAction()
    {
        $manager = ServiceLayer::getService('ScoutingManager');
        $replyForm = new \CR\Scouting\Form\ReplyForm();
        $filterForm = new \CR\Scouting\Form\FilterForm();
        $sports = $manager->getAllSportChoices(); 
        $filterForm->setFieldChoices('sport', $sports);

        $showReplyForm = false;

        if(null != $this->getRequest()->get('id'))
        {
            $scouting = $manager->getScoutingById($this->getRequest()->get('id'));
            $scoutingItems = $manager->buildScoutingCollection(array($scouting));
            $showReplyForm = true;
        }
        else
        {
            $criteria = $filterForm->handleSearchRequest($this->getRequest());
            $scoutingItems = $manager->getScouting($criteria);
        }
        
       
        

        return  $this->render('CR\Scouting\ScoutingModule:default:index.php', array(
                       'scoutingItems' => $scoutingItems,
                       'replyForm' => $replyForm,
                       'type' => $this->getRequest()->get('type'),
                       'filterForm' => $filterForm,
                       'showReplyForm' =>  $showReplyForm
                    ));
    }
    
    public function teamPanelAction()
    {
        $manager = ServiceLayer::getService('ScoutingManager');
        $teamManager = ServiceLayer::getService('TeamManager');

        $team = $teamManager->findTeamById($_SESSION['active_team']['id']);
        $list = $manager->getTeamScouting($team);
        
        foreach($list as $scouting)
        {
            $item = $manager->getScoutingLine($scouting);
            $items[] = array('content' => $item,'scouting' => $scouting);
        }
        
        return $this->render('CR\Scouting\ScoutingModule:default:team_panel.php', array(
            'items' => $items,
            'team' => $team
        ));
    }
    
    public function teamPanelPublicAction($params)
    {

        $manager = ServiceLayer::getService('ScoutingManager');
        $teamManager = ServiceLayer::getService('TeamManager');
        $team = $teamManager->findTeamById($params['team_id']);
        $list = $manager->getTeamScouting($team);
        
        foreach($list as $scouting)
        {
            $item = $manager->getScoutingLine($scouting);
            $items[] = array('content' => $item,'scouting' => $scouting);
        }
        
        return $this->render('CR\Scouting\ScoutingModule:default:team_panel_public.php', array(
            'items' => $items,
            'team' => $team
        ));
    }
    
    public function globalTeamsScoutingAction()
    {
        $manager = ServiceLayer::getService('ScoutingManager');
        $teamManager = ServiceLayer::getService('TeamManager');
        $userIdentity = ServiceLayer::getService('security')->getIdentity();
        $user = $userIdentity->getUser();

        $list = $manager->getGlobalTeamScouting();
            
             
        foreach($list as $scouting)
        {
            $item = $manager->getScoutingLine($scouting);
            $items[$scouting->getId() ] = array('content' => $item,'scouting' => $scouting);
        }


        return $this->render('CR\Scouting\ScoutingModule:default:global_team_scouting.php', array(
            'items' => $items,
            'user' => $user,
        ));
    }
    
    
    
    public function playerScoutingAction()
    {
        $manager = ServiceLayer::getService('ScoutingManager');
        $userIdentity = ServiceLayer::getService('security')->getIdentity();
        $user = $userIdentity->getUser();
       
        
        $list = $manager->getPlayerScouting($user);
        $items = array();
        foreach($list as $scouting)
        {
            $item = $manager->getScoutingLine($scouting);
            $items[] = array('content' => $item,'scouting' => $scouting);
        }

        return $this->render('CR\Scouting\ScoutingModule:default:player_scouting.php', array(
            'items' => $items,
            'user' => $user,
        ));
    }
    
    private function getTeamChoices()
    {
        $security = ServiceLayer::getService('security');
        $userIdentity = $security->getIdentity();
        $teamsInfo = $userIdentity->getUserTeamsInfo();
        
        $choice = array();
        foreach($teamsInfo as $team)
        {
            if('ADMIN' == $team['role'])
            {
                $choice[$team['team']->getId()] = $team['team']->getName();
            }
        }
        
        return $choice;
    }
    
    public function teamLookingPlayerAction()
    {
        $sportEnum = ServiceLayer::getService('SportManager')->getSportFormChoices();
        $levelEnum = ServiceLayer::getService('PlayerManager')->getLevelEnum();
        
        $scountingForm = new TeamLookingPlayerForm();
        $scountingForm->setFieldChoices('sport_id', array('' => '')+$sportEnum);
        $scountingForm->setFieldChoices('level',   array('' => '')+$levelEnum);
        $scountingForm->setFieldChoices('team_id', $this->getTeamChoices()  );
        
        return $this->render('CR\Scouting\ScoutingModule:form:team_looking_player.php', array(
           'form' => $scountingForm,
        ));
    }
    
    public function teamLookingGamePlayerAction()
    {
       
        
        
        $team = ServiceLayer::getService('TeamManager')->findTeamById(ServiceLayer::getService('security')->getIdentity()->getUserCurrentTeamId());
        $sportEnum = ServiceLayer::getService('SportManager')->getSportFormChoices();
        $levelEnum = ServiceLayer::getService('PlayerManager')->getLevelEnum();
        $playgroundManager = ServiceLayer::getService('PlaygroundManager');
        $playgroundChoice = array();
        if(null != $team)
        {
            $playgrounds = $playgroundManager->getTeamPlaygrounds($team);
            foreach($playgrounds as $playground)
            {
                $playgroundChoice[$playground->getId()] = $playground->getName();
            }
        }
        
        
        $scountingForm = new TeamLookingGamePlayerForm();
        $scountingForm->setFieldChoices('sport_id', array('' => '')+$sportEnum);
        $scountingForm->setFieldChoices('level',   array('' => '')+$levelEnum);
        $scountingForm->setFieldChoices('playground_id',   array('' => '')+$playgroundChoice);
        $scountingForm->setFieldChoices('team_id', $this->getTeamChoices()  );
        
        return $this->render('CR\Scouting\ScoutingModule:form:team_looking_game_player.php', array(
           'form' => $scountingForm,
        ));
    }
    
     public function playerLookingTeamAction()
    {
        $sportEnum = ServiceLayer::getService('SportManager')->getSportFormChoices();
        $levelEnum = ServiceLayer::getService('PlayerManager')->getLevelEnum();
        $scountingForm = new PlayerLookingTeamForm();
        $scountingForm->setFieldChoices('sport_id', array('' => '')+$sportEnum);
        $scountingForm->setFieldChoices('level',   array('' => '')+$levelEnum);
        
        return $this->render('CR\Scouting\ScoutingModule:form:player_looking_team.php', array(
           'form' => $scountingForm,
        ));
    }
    
     public function teamLookingPlaygroundAction()
    {
        $sportEnum = ServiceLayer::getService('SportManager')->getSportFormChoices();
      
        $scountingForm = new TeamLookingPlaygroundForm();
        $scountingForm->setFieldChoices('sport_id', array('' => '')+$sportEnum);
        $scountingForm->setFieldChoices('team_id', $this->getTeamChoices()  );

        return $this->render('CR\Scouting\ScoutingModule:form:team_looking_playground.php', array(
           'form' => $scountingForm,
        ));
    }
    
    public function deleteAction()
    {
        $request = ServiceLayer::getService('request');
        $teamManager = ServiceLayer::getService('TeamManager');
        $team = $teamManager->findTeamById($_SESSION['active_team']['id']);
        $this->checkSettingTeamPermission($team);
         
        $manager = ServiceLayer::getService('ScoutingManager');
        $scouting = $manager->getScoutingById($request->get('id'));
        $manager->deleteScouting($scouting);
        $request->redirect();

    }
    
   
    
     public function deletePlayerScoutingAction()
    {
        $request = ServiceLayer::getService('request');
       
        $userIdentity = ServiceLayer::getService('security')->getIdentity();
        $user = $userIdentity->getUser();
        
        
        
         
        $manager = ServiceLayer::getService('ScoutingManager');
        $scouting = $manager->getScoutingById($request->get('id'));
        
        if($user->getId() == $scouting->getAuthorId())
        {
             $manager->deleteScouting($scouting);
        }
        else 
        {
             throw  new AclException();
        }
        
       
        $request->redirect($this->getRouter()->link('player_dashboard'));
    }
    
    
      private function checkSettingTeamPermission($team)
    {
        if( !ServiceLayer::getService('security')->getIdentity()->hasTeamPermission('manageScouting',$team))
        {
             throw  new AclException();
        }
    }
}
