<?php

namespace CR\Scouting\Controller;

use Core\RestController;
use Core\ServiceLayer;
use Core\Validator;
use CR\Scouting\Form\PlayerLookingTeamForm;
use CR\Scouting\Form\TeamLookingGamePlayerForm;
use CR\Scouting\Form\TeamLookingPlayerForm as TeamLookingPlayerForm;
use CR\Scouting\Model\Scouting;
use DateTime;

class FormController extends RestController {

    public function createAction()
    {
        $request = $this->getRequest();
       
        $userIdentity = ServiceLayer::getService('security')->getIdentity();
        $user = $userIdentity->getUser();
        
        $type =  $request->get('st');
        $data = $request->get('scouting');
        $data['author_id'] = $user->getId();
        
        $teamId = (array_key_exists('team_id', $data)) ? $data['team_id'] : null;
        $userTeamRoles = $userIdentity->getTeamRoles();
        
        //if team scouting check access
        if(3 != $type)
        {
             if($userTeamRoles[$teamId]['role'] != 'ADMIN')
            {
                throw new \AclException();
            }
        }
        
       
        
        /*
        if(isset($_SESSION['active_team']['id']))
        {
             $data['team_id'] = $_SESSION['active_team']['id'];
        }
       */
        
        if($type == 1)
        {
            return $this->createType1($data);
        }
        
         if($type == 2)
        {
            return $this->createType2($data);
        }
        
        if($type == 3)
        {
            return $this->createType3($data);
        }
        
        if($type == 4)
        {
            return $this->createType4($data);
        }

    }
    

    private function createType1($data)
    {
        $manager = ServiceLayer::getService('ScoutingManager');
        $localityManager = ServiceLayer::getService('LocalityManager');
        $teamManager = ServiceLayer::getService('TeamManager');
        $team = $teamManager->findTeamById($data['team_id']);
        $form = new TeamLookingPlayerForm();
        $validator = new Validator();
        $validator->setRules($form->getFields());
        $validator->setData($data);
        $validator->validateData();
        
        if(!$validator->hasErrors())
        {
            $scounting = new Scouting();
            $scounting->setCreatedAt(new DateTime());
            $scounting->setType('team-looking-player');
            $scounting->setStatus('new');
            $scounting->setSportId($team->getSportId());
            $scounting->setTeamId($data['team_id']);
            $scounting->setTeamName($team->getName());
            $scounting->setPlayerCount($data['player_count']);
            $scounting->setLocationName($data['location_name']);
            $scounting->setlevel($data['level']);
            $scounting->setAuthorId($data['author_id']);
            $scounting->setLocationData(serialize($data['locality_json_data']));
            
            $locality =  $localityManager->createObjectFromJsonData($data['locality_json_data']);
            $scounting->setLat($locality->getLat());
            $scounting->setLng($locality->getLng());
            
            $manager->saveScouting($scounting);    
            
            $result = $this->buildCreateSuccessResult($scounting);
           
        }
        else
        {
            $result = array('result' => 'ERROR','errors' => $validator->getErrors());
        }
        
        return $this->asJson($result, $status = 'success');

    }
    
     private function createType2($data)
    {
        $playgroundManager = ServiceLayer::getService('PlaygroundManager');
         $localityManager = ServiceLayer::getService('LocalityManager');
        $manager = ServiceLayer::getService('ScoutingManager');
        $teamManager = ServiceLayer::getService('TeamManager');
        $team = $teamManager->findTeamById($data['team_id']);
        $form = new TeamLookingGamePlayerForm();
        $scounting = new Scouting();
        $form->setEntity($scounting);
        $form->bindData($data);
        
        $validator = new Validator();
        $validator->setRules($form->getFields());
        $validator->setData($data);
        $validator->validateData();
        
        if(!$validator->hasErrors())
        {
            $scounting->setCreatedAt(new DateTime());
            $scounting->setType('team-looking-game-player');
            $scounting->setStatus('new');
            $scounting->setLocationData(serialize($data['locality_json_data']));
            $scounting->setTeamName($team->getName());
            $playground = $playgroundManager->findPlaygroundById($scounting->getPlaygroundId());
            $scounting->setPlaygroundName($playground->getName());
            
            $locality =  $localityManager->createObjectFromJsonData($data['locality_json_data']);
            $scounting->setLat($locality->getLat());
            $scounting->setLng($locality->getLng());
            $scounting->setSportId($team->getSportId());

            $gameTime = $scounting->getGameTime()->format('Y-m-d').' '.$data['game_time_time'].':00';
            $scounting->setGameTime(new \DateTime($gameTime));
            
            if(null != $data['game_time_to_time'])
            {
                $gameTimeTo = $scounting->getGameTime()->format('Y-m-d').' '.$data['game_time_to_time'].':00';
                $scounting->setGameTimeTo(new \DateTime($gameTimeTo));
            }
            
           
            
            $manager->saveScouting($scounting);    
            $result = $this->buildCreateSuccessResult($scounting);
           
        }
        else
        {
            $result = array('result' => 'ERROR','errors' => $validator->getErrors());
        }
        
        return $this->asJson($result, $status = 'success');

    }
    
    
    
    private function createType3($data)
    {
        $playgroundManager = ServiceLayer::getService('PlaygroundManager');
        $localityManager = ServiceLayer::getService('LocalityManager');
        $manager = ServiceLayer::getService('ScoutingManager');
        $teamManager = ServiceLayer::getService('TeamManager');
        $form = new PlayerLookingTeamForm();
        $scounting = new Scouting();
        $form->setEntity($scounting);
        $form->bindData($data);
        
        $validator = new Validator();
        $validator->setRules($form->getFields());
        $validator->setData($data);
        $validator->validateData();

        if(!$validator->hasErrors())
        {
            $userIdentity = ServiceLayer::getService('security')->getIdentity();
            $user = $userIdentity->getUser();
            
            $scounting->setCreatedAt(new DateTime());
            $scounting->setType('player-looking-team');
            $scounting->setStatus('new');
            $scounting->setLocationData(serialize($data['locality_json_data']));
            $scounting->setPlayerName($user->getFullName());
            $locality =  $localityManager->createObjectFromJsonData($data['locality_json_data']);
            $scounting->setLat($locality->getLat());
            $scounting->setLng($locality->getLng());

            $manager->saveScouting($scounting);    
            $result = $this->buildCreateSuccessResult($scounting);
        }
        else
        {
            $result = array('result' => 'ERROR','errors' => $validator->getErrors());
        }
        
        return $this->asJson($result, $status = 'success');
    }
    
    
     private function createType4($data)
    {
        $playgroundManager = ServiceLayer::getService('PlaygroundManager');
         $localityManager = ServiceLayer::getService('LocalityManager');
        $manager = ServiceLayer::getService('ScoutingManager');
        $teamManager = ServiceLayer::getService('TeamManager');
        $team = $teamManager->findTeamById($data['team_id']);
        $form = new TeamLookingGamePlayerForm();
        $scounting = new Scouting();
        $form->setEntity($scounting);
        $form->bindData($data);
        
        $validator = new Validator();
        $validator->setRules($form->getFields());
        $validator->setData($data);
        $validator->validateData();
        
        if(!$validator->hasErrors())
        {
            $scounting->setCreatedAt(new DateTime());
            $scounting->setType('team-looking-playground');
            $scounting->setStatus('new');
            $scounting->setLocationData(serialize($data['locality_json_data']));
             $locality =  $localityManager->createObjectFromJsonData($data['locality_json_data']);
            $scounting->setLat($locality->getLat());
            $scounting->setLng($locality->getLng());
            $scounting->setTeamName($team->getName());
            $manager->saveScouting($scounting);    
            $result = $this->buildCreateSuccessResult($scounting);
        }
        else
        {
            $result = array('result' => 'ERROR','errors' => $validator->getErrors());
        }
        
        return $this->asJson($result, $status = 'success');

    }
    
    private function buildCreateSuccessResult($scounting)
    {
        $manager = ServiceLayer::getService('ScoutingManager');
        $playerManager = ServiceLayer::getService('PlayerManager');
        $scoutingData = $manager->convertEntityToArray($scounting);
        $scoutingData['formatted_date'] = $scounting->getCreatedAt()->format('d.m.Y');

        $item = $manager->getScoutingLine($scounting);
        $perex = $manager->getScoutingLinePerex($scounting);
        $author = $playerManager->findPlayerById($scounting->getAuthorId());
        
         $scounting->setContent($item);
         $scounting->setPerex($perex);
         $scounting->setAuthor($author);
        ob_start();
            ServiceLayer::getService('layout')->includePart(MODUL_DIR . '/Scouting/view/default/_scouting_row.php', 
                array(
                    'item' => $scounting, 
                ));
            $row = ob_get_contents();
        ob_end_clean();
        $result = array('result' => 'SUCCESS','scouting' =>  $scoutingData,'row' => $row);
        return $result;
    }
   

   

}
