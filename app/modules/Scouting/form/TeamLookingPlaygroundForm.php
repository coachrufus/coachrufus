<?php
namespace CR\Scouting\Form;
use Core\Form as Form;
use Core\ServiceLayer;

class TeamLookingPlaygroundForm  extends Form
{
    public function __construct()
    {
        $this->setName('scouting');

        $this->setField('location_name', array(
            'type' => 'string',
            'required' => true,
        ));
        
          $this->setField('author_id', array(
            'type' => 'int',
            'required' => false,
        ));
        
        $this->setField('locality_json_data', array(
            'type' => 'string',
            'required' => false,
        ));
     
        $this->setField('sport_id', array(
            'type' => 'choice',
            'required' => true,
        ));
     
        
        $this->setField('locality', array(
            'type' => 'string',
            'required' => true,
        ));
        
        $this->setField('team_id', array(
            'type' => 'choice',
            'required' => true,
        ));
     
        
    }
}
