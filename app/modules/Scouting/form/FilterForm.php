<?php

namespace CR\Scouting\Form;

use Core\Form as Form;
use Core\ServiceLayer;

class FilterForm extends Form {

    private $currentSportName = 'Select sport';
    private $currentTypeName = 'Select type';
    private $currentTypes = array();

    public function __construct()
    {
        $this->setName('filter');
        $this->setField('sport', array(
            'type' => 'choice',
            'required' => false,
        ));
        $this->setField('location_name', array(
            'type' => 'text',
            'required' => false,
        ));
        $this->setField('coordinates', array(
            'type' => 'text',
            'required' => false,
        ));
        $this->setField('type', array(
            'type' => 'choice',
            'required' => false,
            'choices' => array(
                'team-looking-player' => 'Team looking for player', 
                'team-looking-game-player' => 'Team looking for game player', 
                'player-looking-team' => 'Player looking for team', 
                'team-looking-playground' => 'Team looking for playground', 
                'my' => 'My Scouting')
        ));
        
    }

    public function getCurrentSportName()
    {
        return $this->currentSportName;
    }

    public function setCurrentSportName($currentSportName)
    {
        $this->currentSportName = $currentSportName;
    }

    public function getCurrentTypeName()
    {
        return $this->currentTypeName;
    }

    public function setCurrentTypeName($currentTypeName)
    {
        $this->currentTypeName = $currentTypeName;
    }

    public  function getCurrentTypes() {
        $currentTypes = array();
        
        $typeChoices = $this->getFieldChoices('type');

        if($this->getFieldValue('type') != null)
        {
            
            foreach($this->getFieldValue('type') as $type)
            {
                if(null != $type)
                {
                     $currentTypes[$type] = $typeChoices[$type];
                }
            }
        }
       
        return $currentTypes;
    }
    
    public function getAvailableTypeChoices()
    {
        $choices =$this->getFieldChoices('type');
        
        $typeChoices = $this->getFieldChoices('type');
        if(null != $this->getFieldValue('type'))
        {
            foreach($this->getFieldValue('type') as $type)
            {
               unset($choices[$type]);
            }
        }
       
        return $choices;
    }

    public function getCurrentSports()
    {
        $current = array();
        $choices = $this->getFieldChoices('sport');
        if($this->getFieldValue('sport') != null)
        {
            foreach($this->getFieldValue('sport') as $type)
            {
                if(null != $type && array_key_exists($type, $choices))
                {
                      $current[$type] = $choices[$type];
                }
            }
        }      
       
        return $current;
    }
    
    public function getAvailableSportChoices()
    {
        $availableChoices =$this->getFieldChoices('sport');
        
        $choices = $this->getFieldChoices('sport');
        if(null != $this->getFieldValue('sport'))
        {
            foreach($this->getFieldValue('sport') as $type)
            {
               unset($availableChoices[$type]);
            }
        }
       
        return $availableChoices;
    }


    
    public function getFilterUriData($index,$value)
    {
        $parts = array($index => $value);
        foreach($this->getFields() as $currentIndex => $currentData)
        {
            if($currentData['value'] !== null && $currentIndex != $index)
            {
                $parts[$currentIndex] = $currentData['value'];
            }
        }
        return $parts;
    }
    
    public function handleSearchRequest($request)
    {
        $criteria = array('filter' => array());
        $filterData = array();
        $filterSportData = array();
        if(null != $request->get('type'))
        {
            foreach($request->get('type') as $type)
            {
                 if('my' == $type)
                {
                    $criteria['filter']['author_id'] =  ServiceLayer::getService('security')->getIdentity()->getUser()->getId();
                    $filterSportData['author_id'] =  ServiceLayer::getService('security')->getIdentity()->getUser()->getId();
                }
                elseif('all' != $type)
                {
                    $criteria['filter']['type'][] =  $type;
                    $filterSportData['type'][] =  $type;
                }
            }
            $filterData['type'] =$request->get('type'); 
        }
        
        
        if(null != $request->get('sport'))
        {
           $criteria['filter']['sport_id'] = $request->get('sport');
           $filterData['sport'] = $request->get('sport');
        }
        
         if(null != $request->get('coordinates') && null !=  $request->get('location_name') )
        {
            $coordinates = $request->get('coordinates');
            $criteria['filter']['coordinates'] = $coordinates;
            $filterData['location_name'] = $request->get('location_name');
            $filterData['coordinates'] =$coordinates;
        }
        $this->bindData($filterData);
        return $criteria;
    }

}
