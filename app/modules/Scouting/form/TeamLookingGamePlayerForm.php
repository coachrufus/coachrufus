<?php
namespace CR\Scouting\Form;
use Core\Form as Form;
use Core\ServiceLayer;

class TeamLookingGamePlayerForm  extends Form
{
    public function __construct()
    {
        $this->setName('scouting');

        $this->setField('location_name', array(
            'type' => 'string',
            'required' => true,
        ));
        
          $this->setField('author_id', array(
            'type' => 'int',
            'required' => false,
        ));
        
        $this->setField('locality_json_data', array(
            'type' => 'string',
            'required' => false,
        ));
        
        $this->setField('player_count', array(
            'type' => 'int',
            'required' => true,
        ));
        
        $this->setField('sport_id', array(
            'type' => 'choice',
            'required' => true,
        ));
        
        $this->setField('team_id', array(
            'type' => 'int',
            'required' => false,
        ));
        
        $this->setField('level', array(
            'type' => 'choice',
            'required' => true,
        ));
        
        $this->setField('locality', array(
            'type' => 'string',
            'required' => true,
        ));
        
        $this->setField('playground_id', array(
            'type' => 'choice',
            'required' => true,

        ));
        
        $this->setField('game_time', array(
            'type' => 'datetime',
            'required' => true,
            'format' => 'd.m.Y'
        ));
        
        $this->setField('game_time_time', array(
            'type' => 'time',
            'required' => true,
        ));
        
        $this->setField('game_time_to_time', array(
            'type' => 'time',
            'required' => true,
        ));
        
        $this->setField('valid_to', array(
            'type' => 'datetime',
            'required' => true,
            'format' => 'd.m.Y'
        ));
        
         $this->setField('team_id', array(
            'type' => 'choice',
            'required' => true,
        ));

     
        
    }
}
