<?php
namespace CR\Scouting\Form;
use Core\Form as Form;
use Core\ServiceLayer;

class ReplyForm  extends Form
{
    public function __construct()
    {
        $this->setName('reply');


        
        $this->setField('subject', array(
            'type' => 'string',
            'required' => false,
        ));
        
         $this->setField('text', array(
            'type' => 'string',
            'required' => true,
        ));
        
       
     
        
    }
}
