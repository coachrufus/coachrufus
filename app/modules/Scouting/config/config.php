<?php
namespace CR\Scouting;
require_once(MODUL_DIR.'/Scouting/ScoutingModule.php');


use Core\ServiceLayer as ServiceLayer;

ServiceLayer::addService('ScoutingRepository',array(
'class' => "CR\Scouting\Model\ScoutingRepository",
	'params' => array(
		new  \CR\Scouting\Model\ScoutingStorage('scouting'),
		new \Core\EntityMapper('CR\Scouting\Model\Scouting'),
	)
));

ServiceLayer::addService('ScoutingManager',array(
'class' => "CR\Scouting\Model\ScoutingManager",
	'params' => array(
		ServiceLayer::getService('ScoutingRepository'),
		ServiceLayer::getService('SportManager'),
		ServiceLayer::getService('PlayerManager'),
	)
));


$acl = ServiceLayer::getService('acl');


//routing
$router = ServiceLayer::getService('router');
include_once('rest_routing.php');

$acl->allowRole('ROLE_USER','scouting');
$router->addRoute('scouting','/scouting',array(
		'controller' => 'CR\Scouting\ScoutingModule:Default:index'
));


$acl->allowRole('ROLE_USER','create_scouting');
$router->addRoute('create_scouting','/scouting/create',array(
		'controller' => 'CR\Scouting\ScoutingModule:Form:create'
));

$acl->allowRole('ROLE_USER','delete_scouting');
$router->addRoute('delete_scouting','/scouting/delete',array(
		'controller' => 'CR\Scouting\ScoutingModule:Default:delete'
));


$acl->allowRole('ROLE_USER','player_scouting');
$router->addRoute('player_scouting','/scouting/my-scouting',array(
		'controller' => 'CR\Scouting\ScoutingModule:Default:playerScouting'
));



$acl->allowRole('ROLE_USER','delete_player_scouting');
$router->addRoute('delete_player_scouting','/scouting/player/delete',array(
		'controller' => 'CR\Scouting\ScoutingModule:Default:deletePlayerScouting'
));

$acl->allowRole('guest','scouting_detail');
$router->addRoute('scouting_detail','/scouting/detail/:id',array(
		'controller' => 'CR\Scouting\ScoutingModule:Default:detail'
));

/*
$acl->allowRole('ROLE_USER','me_scouting');
$router->addRoute('me_scouting','/scouting',array(
		'controller' => 'CR\Scouting\ScoutingModule:Default:index'
));

*/
