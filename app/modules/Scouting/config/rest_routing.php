<?php

$acl->allowRole('ROLE_USER','rest_last_scouting');
$router->addRoute('rest_last_scouting','/api/scouting/last',array(
    'controller' => 'CR\Scouting\ScoutingModule:Rest:lastScouting'
));

$acl->allowRole('ROLE_USER','rest_search_scouting_items');
$router->addRoute('rest_search_scouting_items','/api/scouting/search',array(
    'controller' => 'CR\Scouting\ScoutingModule:Rest:search'
));

$acl->allowRole('ROLE_USER','scouting_send_contact_message');
$router->addRoute('scouting_send_contact_message','/scouting/send-message',array(
		'controller' => 'CR\Scouting\ScoutingModule:Rest:sendContactMessage'
));