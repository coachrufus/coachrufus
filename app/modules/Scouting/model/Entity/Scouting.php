<?php

namespace CR\Scouting\Model;

use Core\EntityMapper as EntityMapper;
use Core\ServiceLayer as ServiceLayer;

class Scouting {

    private $mapper_rules = array(
        'id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => true
        ),
        'created_at' => array(
            'type' => 'datetime',
            'max_length' => '',
            'required' => false
        ),
        'type' => array(
            'type' => 'string',
            'max_length' => '50',
            'required' => false
        ),
        'status' => array(
            'type' => 'string',
            'max_length' => '50',
            'required' => false
        ),
        'team_id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'team_name' => array(
            'type' => 'string',
            'max_length' => '255',
            'required' => false
        ),
        'player_id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'player_name' => array(
            'type' => 'string',
            'max_length' => '255',
            'required' => false
        ),
        'playground_id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'playground_name' => array(
            'type' => 'string',
            'max_length' => '250',
            'required' => false
        ),
        'sport_id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'author_id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'level' => array(
            'type' => 'string',
            'max_length' => '50',
            'required' => false
        ),
        'location_data' => array(
            'type' => 'string',
            'max_length' => '5000',
            'required' => false
        ),
        'player_count' => array(
            'type' => 'string',
            'max_length' => '50',
            'required' => false
        ),
        'location_name' => array(
            'type' => 'string',
            'max_length' => '250',
            'required' => false
        ),
        'valid_to' => array(
            'type' => 'datetime',
            'max_length' => '',
            'required' => false
        ),
        'game_time' => array(
            'type' => 'datetime',
            'max_length' => '',
            'required' => false
        ),
        'game_time_to' => array(
            'type' => 'datetime',
            'max_length' => '',
            'required' => false
        ),
        'lat' => array(
            'type' => 'int',
            'max_length' => '',
            'required' => false
        ),
        'lng' => array(
            'type' => 'int',
            'max_length' => '',
            'required' => false
        ),
    );

    public function getDataMapperRules()
    {
        return $this->mapper_rules;
    }

    protected $id;
    protected $createdAt;
    protected $type;
    protected $status;
    protected $teamId;
    protected $playerId;
    protected $playerName;
    protected $playgroundId;
    protected $playgroundName;
    protected $sportId;
    protected $authorId;
    protected $level;
    protected $locationData;
    protected $playerCount;
    protected $locationName;
    protected $lat;
    protected $lng;
    protected $validTo;
    protected $gameTime;
    protected $gameTimeTo;
    protected $teamName;
    
    protected $perex;
    protected $content;
    protected $author;
    
    public  function getPerex() {
return $this->perex;
}

public  function getContent() {
return $this->content;
}

public  function getAuthor() {
return $this->author;
}

public  function setPerex($perex) {
$this->perex = $perex;
}

public  function setContent($content) {
$this->content = $content;
}

public  function setAuthor($author) {
$this->author = $author;
}



    public function setId($val)
    {
        $this->id = $val;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setCreatedAt($val)
    {
        $this->createdAt = $val;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setType($val)
    {
        $this->type = $val;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setStatus($val)
    {
        $this->status = $val;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setTeamId($val)
    {
        $this->teamId = $val;
    }

    public function getTeamId()
    {
        return $this->teamId;
    }

    public function setPlayerId($val)
    {
        $this->playerId = $val;
    }

    public function getPlayerId()
    {
        return $this->playerId;
    }

    public function setPlaygroundId($val)
    {
        $this->playgroundId = $val;
    }

    public function getPlaygroundId()
    {
        return $this->playgroundId;
    }

    public function setSportId($val)
    {
        $this->sportId = $val;
    }

    public function getSportId()
    {
        return $this->sportId;
    }

    public function setAuthorId($val)
    {
        $this->authorId = $val;
    }

    public function getAuthorId()
    {
        return $this->authorId;
    }

    function getLevel()
    {
        return $this->level;
    }

    function setLevel($level)
    {
        $this->level = $level;
    }

    function getLocationData()
    {
        return $this->locationData;
    }

    function setLocationData($locationData)
    {
        $this->locationData = $locationData;
    }

    public function getLocationInfo()
    {
        $jsonData = unserialize($this->getLocationData());
        return json_decode($jsonData);
    }

    function getPlayerCount()
    {
        return $this->playerCount;
    }

    function setPlayerCount($playerCount)
    {
        $this->playerCount = $playerCount;
    }

    function getLocationName()
    {
        return $this->locationName;
    }

    function setLocationName($locationName)
    {
        $this->locationName = $locationName;
    }

    function getValidTo()
    {
        return $this->validTo;
    }

    function getGameTime()
    {
        return $this->gameTime;
    }

    function setValidTo($validTo)
    {
        $this->validTo = $validTo;
    }

    function setGameTime($gameTime)
    {
        $this->gameTime = $gameTime;
    }

    function getPlaygroundName()
    {
        return $this->playgroundName;
    }

    function setPlaygroundName($playgroundName)
    {
        $this->playgroundName = $playgroundName;
    }

    
    function getPlayerName() {
return $this->playerName;
}

 function setPlayerName($playerName) {
$this->playerName = $playerName;
}

function getTeamName() {
return $this->teamName;
}

 function setTeamName($teamName) {
$this->teamName = $teamName;
}

function getLat() {
return $this->lat;
}

 function getLng() {
return $this->lng;
}

 function setLat($lat) {
$this->lat = $lat;
}

 function setLng($lng) {
$this->lng = $lng;
}


public  function getGameTimeTo() {
return $this->gameTimeTo;
}

public  function setGameTimeTo($gameTimeTo) {
$this->gameTimeTo = $gameTimeTo;
}


public function getShareContent()
{
    $content = $this->getContent();
    $content = str_replace('<br />',' ',$content);
    $content = strip_tags($content);
    return $content;
}





}
