<?php
namespace CR\Scouting\Model;
use Core\Repository as Repository;

class ScoutingRepository extends Repository
{
    public function findGlobalTeamScouting($limit)
    {
        $data = $this->getStorage()->findGlobalTeamScouting($limit);
        $list = array();
        foreach ($data as $d)
        {
            $object = $this->factory->createEntityFromArray($d);
            $list[] = $object;
        }
        return $list;
    }
    
    public function findScoutingByArea($center, $distance)
    {
        $data = $this->getStorage()->findScoutingByArea($center, $distance);
        return $this->createObjectList($data);
    }
    
    public function findLastScouting()
    {
        $data = $this->getStorage()->findLastScouting();
        return $this->createObjectList($data);
    }
    
    public function findScouting($start,$limit,$criteria)
    {
        $data = $this->getStorage()->findScouting($start,$limit,$criteria);
        return $this->createObjectList($data);
    }
    
    public function findFilterSports($filterData = array())
    {
        $data = $this->getStorage()->findFilterSports($filterData);
       return $data;
    }
}
