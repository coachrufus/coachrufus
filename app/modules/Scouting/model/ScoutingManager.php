<?php

namespace CR\Scouting\Model;

use Core\Manager;
use Core\ServiceLayer;

class ScoutingManager extends Manager {

    private $sportManager;
    private $playerManger;
    protected $itemPerPage = 8;

    public function __construct($repository = null, $sportManager, $playerManger)
    {
        parent::__construct($repository);
        $this->setPlayerManger($playerManger);
        $this->setSportManager($sportManager);
    }

    public function getItemPerPage()
    {
        return $this->itemPerPage;
    }

    public function setItemPerPage($itemPerPage)
    {
        $this->itemPerPage = $itemPerPage;
    }

    public function buildScoutingCollection($items)
    {
        $displayItems = array_slice($items, 0, $this->getItemPerPage());
        $nextItem = array_slice($items, $this->getItemPerPage(), 1);
        $collection = new ScoutingCollection($displayItems);
        
        foreach($collection->getItems() as $scouting)
        {
            
            $item = $this->getScoutingLine($scouting);
            $perex = $this->getScoutingLinePerex($scouting);
            $author = $this->getPlayerManger()->findPlayerById($scouting->getAuthorId());
            
            $scouting->setContent($item);
            $scouting->setPerex($perex);
            $scouting->setAuthor($author);
        }
        
        if (empty($nextItem))
        {
            $collection->setAllItemsLoaded(true);
        }
        return $collection;
    }

    public function saveScouting($object)
    {
        return $this->getRepository()->save($object);
    }

    public function getPlayerScouting($player)
    {
        return $this->getRepository()->findBy(array('author_id' => $player->getId(), 'type' => 'player-looking-team'), array('order' => ' id desc '));
    }

    public function getScoutingById($id)
    {
        return $this->getRepository()->find($id);
    }

    public function deleteScouting($scouting)
    {
        return $this->getRepository()->delete($scouting->getId());
    }

    public function getTeamScouting($team)
    {
        return $this->getRepository()->findBy(array('team_id' => $team->getId()), array('order' => ' id desc '));
    }

    public function getLastScouting($criteria)
    {
        return  $this->getScouting($criteria);

    }

    public function getScouting($criteria = array())
    {
        if(!array_key_exists('limit', $criteria))
        {
            $limit = $this->getItemPerPage();
        }
        else
        {
            $limit = $criteria['limit'];
        }
        
        if(!array_key_exists('start', $criteria))
        {
            $start = null;
        }
        else
        {
            $start = $criteria['start'];
        }
        
        if(!array_key_exists('filter', $criteria))
        {
            $filter = array();
        }
        else
        {
            $filter = $criteria['filter'];
        }

        $items = $this->getRepository()->findScouting($start, $limit+1, $filter);
        $collection = $this->buildScoutingCollection($items);
        return $collection;
    }

    public function getGlobalTeamScouting($limit = 10)
    {
        return $this->getRepository()->findGlobalTeamScouting($limit);
    }

    function getSportManager()
    {
        return $this->sportManager;
    }

    function getPlayerManger()
    {
        return $this->playerManger;
    }

    function setSportManager($sportManager)
    {
        $this->sportManager = $sportManager;
    }

    function setPlayerManger($playerManger)
    {
        $this->playerManger = $playerManger;
    }

    public function getScoutingLine($scouting)
    {
        $sportEnum = ServiceLayer::getService('SportManager')->getSportFormChoices();
        $levelEnum = ServiceLayer::getService('PlayerManager')->getLevelEnum();
        $translator = ServiceLayer::getService('translator');

        if ('team-looking-player' == $scouting->getType())
        {
            $item = $sportEnum[$scouting->getSportId()] . ',' . $levelEnum[$scouting->getLevel()] . '<br />' . $scouting->getLocationName();
        }

        if ('team-looking-game-player' == $scouting->getType())
        {
            $today = new \DateTime();
            $expireDate = $scouting->getValidTo();
            $timeLeft = $today->diff($expireDate);
            $gameTime =  $scouting->getGameTime()->format('H:i');
            if(null !=  $scouting->getGameTimeTo())
            {
                $gameTime = $gameTime.' - '.$scouting->getGameTimeTo()->format('H:i');
            }
            

            $item = $sportEnum[$scouting->getSportId()] . ',' . $levelEnum[$scouting->getLevel()] . '<br />' . $gameTime . '<br />' . $scouting->getPlaygroundName() . '<br />'
                    . '<em>' . $translator->translate('Time left:') . $timeLeft->format(' %dd %hh %im') . '</em>';
        }

        if ('team-looking-playground' == $scouting->getType())
        {
            $item = $sportEnum[$scouting->getSportId()] . '<br />' . $scouting->getLocationName();
        }

        if ('player-looking-team' == $scouting->getType())
        {
            $item = $sportEnum[$scouting->getSportId()] . ', ' . $levelEnum[$scouting->getLevel()] . '<br />' . $scouting->getLocationName();
        }

        return $item;
    }

    public function getScoutingLinePerex($scouting)
    {
        $sportEnum = ServiceLayer::getService('SportManager')->getSportFormChoices();
        $levelEnum = ServiceLayer::getService('PlayerManager')->getLevelEnum();
        $translator = ServiceLayer::getService('translator');

        if ('team-looking-player' == $scouting->getType())
        {
            $item = $scouting->getTeamName() . ' ' . $translator->translate('hľadá') . ' ' . $scouting->getPlayerCount() . ' ' . $translator->translate('hráčov');
        }

        if ('team-looking-game-player' == $scouting->getType())
        {
            $item = $scouting->getTeamName() . ' ' . $translator->translate('hľadá') . ' ' . $scouting->getPlayerCount() . $translator->translate('hráčov');
        }

        if ('team-looking-playground' == $scouting->getType())
        {
            $item = $scouting->getTeamName() . ' ' . $translator->translate('hľadá športovisko');
        }

        if ('player-looking-team' == $scouting->getType())
        {
            $item = $scouting->getPlayerName() . ' ' . $translator->translate('hľadá team');
        }

        return $item;
    }

    public function findScoutingByArea($center, $distance)
    {
        return $this->getRepository()->findScoutingByArea($center, $distance);
    }
    
    public function getAllSportChoices()
    {
        $data = $this->getRepository()->findFilterSports();
        $choices = array();
        foreach($data as $d)
        {
            $choices[$d['id']] = $d['name'].' ('.$d['count'].') ';
        }
        
        return $choices;
            
    }
    public function getFilterSportChoices($filterData = array())
    {
        $data = $this->getRepository()->findFilterSports($filterData);
        $choices = array();
        foreach($data as $d)
        {
            $choices[$d['id']] = $d['name'].' ('.$d['count'].') ';
        }
        
        return $choices;
            
    }

}
