<?php

namespace CR\Scouting\Model;

use Core\DbStorage;

class ScoutingStorage extends DbStorage {

   public function findGlobalTeamScouting($limit)
   {
        $data = \Core\DbQuery::prepare('
                SELECT *
                FROM '.$this->getTableName().'
                WHERE type != "player-looking-team"
                ORDER  by id desc
                LIMIT 0,'.$limit)
		->execute()
		->fetchAll(\PDO::FETCH_ASSOC);
		return $data;
   }
   
   public function findLastScouting()
   {
       $data = \Core\DbQuery::prepare('
                SELECT *
                FROM '.$this->getTableName().'
                     LEFT JOIN team ON scouting.team_id = team.id
                    WHERE (team.status != "deleted" OR team.id is null) 
                ORDER  by id desc
                LIMIT 0,2')
		->execute()
		->fetchAll(\PDO::FETCH_ASSOC);
		return $data;
   }
   
   public function findScoutingByArea($center, $distance)
   {
        $data = \Core\DbQuery::prepare('SELECT  * , (6371 * ACOS(COS(RADIANS('.$center['lat'].')) * COS(RADIANS(lat)) * COS(RADIANS(lng) - RADIANS('.$center['lng'].')) + SIN(RADIANS('.$center['lat'].')) * SIN(RADIANS(lat)))) AS distance 
                    FROM scouting
                    LEFT JOIN team ON scouting.team_id = team.id
                    WHERE (team.id != "deleted" OR team.id is null) 
                    HAVING distance < '.$distance)
		->execute()
		->fetchAll(\PDO::FETCH_ASSOC);
		return $data;
   }
   
    public function findScouting($start,$limit = 5,$criteria=null)
   {
        $coordinatesCriteria = '';
        $coordinatesCond = '';

        if(array_key_exists('coordinates', $criteria))
        {
            $coordinates = $criteria['coordinates'];
            $coordinates = explode(',',$coordinates);
            $lat = $coordinates[0];
            $lng = $coordinates[1];
            
            $coordinatesCriteria = ' ,(6371 * ACOS(COS(RADIANS('.$lat.')) * COS(RADIANS(lat)) * COS(RADIANS(lng) - RADIANS('.$lng.')) + SIN(RADIANS('.$lat.')) * SIN(RADIANS(lat)))) AS distance ';
            $coordinatesCond = ' HAVING distance < 20 ';
            
            unset($criteria['coordinates']);
        }

        $where_criteria_items  = array();
        foreach ($criteria as $column => $column_val)
        {
                if(is_string($column_val))
                {
                        $where_criteria_items[] = ' s.'.$column.'=:'.$column.' ';
                }
                 elseif(is_array($column_val))
                {
                    $where_criteria_items[] = ' s.'.$column.' in ("'.implode('","',$column_val).'") ';
                }
                else
                {
                        $where_criteria_items[] = ' s.'.$column.'=:'.$column.' ';
                }

        }
        if(!empty($where_criteria_items))
        {
            $whereCriteria = ' AND ' .implode(' AND ',$where_criteria_items);
        }
        else
        {
             $whereCriteria = '';
        }

        
        if(null == $start)
        {
            $query = \Core\DbQuery::prepare('SELECT s.* '.$coordinatesCriteria.' FROM '.$this->getTableName().' s   LEFT JOIN team ON s.team_id = team.id
            LEFT JOIN co_users u ON s.author_id = u.id
                    WHERE (team.status != "deleted" OR team.id is null) AND u.id is not null  '.$whereCriteria.' '.$coordinatesCond.' ORDER  by s.id desc LIMIT 0,'.$limit);

           
            foreach($criteria as $column => $value)
            {
                    if("" != $value)
                    {
                            $query->bindParam(':'.$column, $value);
                    }
                    elseif(is_array($value))
                    {
                            
                    }
                    else 
                    {
                         $query->bindParam(':'.$column, null);
                    }

            }
            $data = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
            return $data;
        }
        else
        {
            //t_dump( 'SELECT * FROM '.$this->getTableName().'  WHERE id < :start '.$whereCriteria.' ORDER  by id desc LIMIT 0,'.$limit);

            $query = \Core\DbQuery::prepare('SELECT s.* '.$coordinatesCriteria.' FROM '.$this->getTableName().' s LEFT JOIN team ON s.team_id = team.id 
            LEFT JOIN co_users u ON s.author_id = u.id
                    WHERE (team.status != "deleted" OR team.id is null)  AND u.id is not null   AND  s.id < :start '.$whereCriteria.' '.$coordinatesCond.' ORDER  by s.id desc LIMIT 0,'.$limit);
            $query->bindParam(':start', $start);

            
            foreach($criteria as $column => $value)
            {
                    if(is_array($value))
                    {

                    }
                    elseif("" != $value)
                    {
                         $query->bindParam(':'.$column, $value);
                    }
                    else 
                    {
                         $query->bindParam(':'.$column, null);
                    }

            }

            $data = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
            return $data;
          
        }
        
        
        
   }
   
   public function findFilterSports($criteria)
    {
        $where_criteria_items  = array();
        
        
        foreach ($criteria as $column => $column_val)
        {
                if(is_string($column_val))
                {
                        $where_criteria_items[] = ' s.'.$column.'=:'.$column.' ';
                }
                elseif(is_array($column_val))
                {
                    $where_criteria_items[] = ' s.'.$column.' in ("'.implode('","',$column_val).'") ';
                }
                else
                {
                        $where_criteria_items[] = ' s.'.$column.'=:'.$column.' ';
                }

        }
        if(!empty($where_criteria_items))
        {
            $whereCriteria = ' AND ' .implode(' AND ',$where_criteria_items);
        }
        else
        {
             $whereCriteria = '';
        }

        
        
         $query = \Core\DbQuery::prepare(' SELECT sp.id, 
                        sp.name, 
                        Count(sp.id) AS count 
                 FROM   scouting s 
                        LEFT JOIN sport sp 
                               ON s.sport_id = sp.id 
                  WHERE 1=1 '.$whereCriteria.'            
                 GROUP  BY sp.id ');
          
       
         
            foreach($criteria as $column => $value)
            {
                    if("" != $value)
                    {
                            $query->bindParam(':'.$column, $value);
                    }
                    elseif(is_array($value))
                    {
                        
                    }
                    else 
                    {
                         $query->bindParam(':'.$column, null);
                    }

            }
            $data = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
            

            return $data;
          
        
       
       /*
       $data = \Core\DbQuery::prepare('
                SELECT sp.id, 
                        sp.name, 
                        Count(sp.id) AS count 
                 FROM   scouting s 
                        LEFT JOIN sport sp 
                               ON s.sport_id = sp.id 
                 GROUP  BY sp.id ')
		->execute()
		->fetchAll(\PDO::FETCH_ASSOC);
		return $data;
       */
       
     
       
    }
   
  
}
