<?php
namespace CR\Socializing;
require_once MODUL_DIR . '/Socializing/SocializingModule.php';

use Core\EntityMapper;
use Core\ServiceLayer as ServiceLayer;

ServiceLayer::addService('ChatRepository', [
    'class' => "CR\Socializing\Model\ChatRepository",
    'params' => []
]);

ServiceLayer::addService('ChatManager', [
    'class' => "CR\Socializing\Model\ChatManager",
    'params' => [ ServiceLayer::getService('ChatRepository') ]
]);

ServiceLayer::addService('ContactRepository', [
    'class' => "CR\Socializing\Model\ContactRepository",
    'params' => []
]);

ServiceLayer::addService('ImageManager', [
    'class' => "CR\Socializing\Model\ImageManager"
]);

ServiceLayer::addService('ContactManager', [
    'class' => "CR\Socializing\Model\ContactManager",
    'params' => [
        ServiceLayer::getService('ContactRepository'),
        ServiceLayer::getService('ImageManager')
     ]
]);

$acl = ServiceLayer::getService('acl');
$router = ServiceLayer::getService('router');

$acl->allowRole('ROLE_USER', 'chat_send_message');
$router->addRoute('chat_send_message', '/socializing/chat/send', [
    'controller' => 'CR\Socializing\SocializingModule:Chat:send'
]);

$acl->allowRole('ROLE_USER', 'chat_mark_as_readed');
$router->addRoute('chat_mark_as_readed', '/socializing/chat/mark-as-readed', [
    'controller' => 'CR\Socializing\SocializingModule:Chat:markAsReaded'
]);

$acl->allowRole('ROLE_USER', 'chat_refresh');
$router->addRoute('chat_refresh', '/socializing/chat/refresh', [
    'controller' => 'CR\Socializing\SocializingModule:Chat:refresh'
]);