<?php namespace CR\Socializing\Controller;

use Core\Controller as Controller;
use Core\Request as Request;
use Core\ServiceLayer;
use Core\GUID;
use Core\RestController;
use CR\Socializing\Model\ChatMessage;
use \__ as us;

class ChatController extends RestController
{
    private $request;
    private $chatManager;
    private $userIdentity;
    
    function __construct()
    {
        $this->request = ServiceLayer::getService('request');
        $this->manager = ServiceLayer::getService('ChatManager');
        $this->userIdentity = ServiceLayer::getService('security')->getIdentity();
    }
    
    public function addMessageAction()
    {
        $datas = json_decode(file_get_contents("php://input"), false);
        $fromUserId = $this->userIdentity->getId();
        foreach ($datas as $data)
        {
            $message = new ChatMessage();
            $message->fromUserId = $fromUserId;
            $message->toUserId = $data->toUserId;
            $message->body = $data->body;
            $this->manager->sendMessage($message);
        }
        return $this->asJson(true);
    }
    
    public function messagesForUserAction()
    {
        $data = json_decode(file_get_contents("php://input"), false);
        $fromUserId = $this->userIdentity->getId();
        $messages = $this->manager->receiveMessagesForUser($fromUserId);
        return $this->asJson(true);
    }
    
    public function sendAction()
    {
        $recipients = $_POST['recipients'];
        $body = $_POST['message'];
        $activeTeamManager = ServiceLayer::getService('ActiveTeamManager');
        $team = $activeTeamManager->getActiveTeam();
        $teamId = (int)$team['id'];
        $fromUserId = $this->userIdentity->getUser()->getId();
        $chatManager = ServiceLayer::getService('ChatManager');
        $now = new \DateTime();
        foreach ($recipients as $toUserId)
        {
            $chatMessage = new ChatMessage();
            $chatMessage->fromUserId = $fromUserId;
            $chatMessage->toUserId = $toUserId;
            $chatMessage->body = $body;
            $chatMessage->teamId = $teamId;
            $chatMessage->dateTime = $now;
            $chatManager->sendMessage($chatMessage);
        }
        $chatManager->markMessagesAsReaded($fromUserId);
        return $this->refreshAction();
    }
    
    public function markAsReadedAction()
    {
        $fromUserId = $this->userIdentity->getUser()->getId();
        $chatManager = ServiceLayer::getService('ChatManager');
        $chatManager->markMessagesAsReaded($fromUserId);
        return $this->asJson(true);
    }
    
    public function refreshAction()
    {
        $activeTeamManager = ServiceLayer::getService('ActiveTeamManager');
        $team = $activeTeamManager->getActiveTeam();
        if (is_null($team))
        {
            return $this->asJson(false);
        }
        else
        {
            $teamId = (int)$team['id'];
            $userId = $this->userIdentity->getUser()->getId();
            $contactManager = ServiceLayer::getService('ContactManager');
            $chatManager = ServiceLayer::getService('ChatManager');
            $contacts = $contactManager->getContacts($teamId);
            $messages = $chatManager->receiveMessagesForUser($teamId, $userId);
            return $this->asJson([
                'contacts' => $contacts,
                'messages' => us::map($messages, function($message)
                {
                    return $message->toArray();
                }),
                'unreaded' => (int)$chatManager->getUnreadedMessageCount($userId)['unreaded']
            ]);
        }
    }
}