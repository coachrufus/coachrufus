<?php namespace CR\Socializing\Model;

class ContactManager
{
    private $repository;
    private $imageManager;
    
    function __construct
    (
        ContactRepository $repository,
        ImageManager $imageManager
    )
    {
        $this->repository = $repository;
        $this->imageManager = $imageManager;
    }
	
    function getContacts($teamId)
    {
        $users = $this->repository->findTeamUsers($teamId, function($teamUsers)
        {
            return $teamUsers->order('nickname ASC');
        });
        $result = iterator_to_array((function($users)
        {
            foreach ($users as $user)
            {
                $u = iterator_to_array($user);
				if (empty($u['nickname']))
				{
					$u['nickname'] = "{$u['name']} {$u['surname']}";
				}
                $u['image'] = $this->imageManager->getImageFor($u);
                unset($u['photo']);
                yield $u;
            }
        })($users));
		usort($result, function($a, $b) { return strcmp($a['nickname'], $b['nickname']); });
		return $result;
    }
}