<?php  namespace CR\Socializing\Model;

class ChatMessage
{
    private $array;
    private $fields;
    
    function __construct($array = [])
    {
        $this->array = $array;
        $this->fields = [
            'id' => 'id',
            'fromUserId' => 'from_user_id',
            'toUserId' => 'to_user_id',
            'body' => 'body',
            'dateTime' => 'datetime',
            'isReaded' => 'is_readed',
            'isActive' => 'is_active',
            'teamId' => 'team_id',
        ];
    }
    
    function __isset($name) { return isset($this->array[$this->fields[$name]]); }
    
    function __get($name)
    {
        if (isset($this->fields[$name]))
        {
            return $this->array[$this->fields[$name]];
        }
        else throw new \Exception("Invalid property {$name}");
    }
    
    function __set($name, $value)
    {
        if (isset($this->fields[$name]))
        {
            $this->array[$this->fields[$name]] = $value;
        }
        else throw new \Exception("Invalid property {$name}");
    }
    
    function toArray() { return $this->array; }
}
