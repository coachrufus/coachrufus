<?php namespace CR\Socializing\Model;

use Core\Db;

class ChatRepository
{
    use Db;
    
    private $db;
    
    function __construct()
    {
        $this->db = $this->getDb();
    }
    
    function addMessage($message)
    {
        $this->db->chat()->insert($message);
        return $message;
    }
    
    function findMessages(Callable $finder)
    {
        return $finder($this->db->chat());
    }
    
    function getUnreadedMessageCount($userId)
    {
        return $this->db->chat()
            ->select('count(id) as unreaded')
            ->where('to_user_id', $userId)
            ->where('is_readed = 0')
            ->fetch();
    }
    
    function markMessagesAsReaded($userId)
    {
        return $this->db->chat()
            ->where('to_user_id', $userId)
            ->where('is_readed = 0')
            ->update(['is_readed' => 1]);
    }
}