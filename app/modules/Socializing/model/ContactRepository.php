<?php namespace CR\Socializing\Model;

use Core\Db;

class ContactRepository
{
    use Db;
    
    private $db;
    
    function __construct()
    {
        $this->db = $this->getDb();
    }
    
    private function findTeamUserIds($teamId)
    {
        $result = $this->db->team_players()
            ->select('player_id')
            ->where('team_id', $teamId)
            ->where('player_id <> 0');
        return iterator_to_array((function($result)
        {
            foreach ($result as $p)
            {
                yield $p['player_id'];
            }
        })($result));
    }
	
    function findTeamUsers($teamId, $finder)
    {
        $result = $this->db->co_users()
            ->select('id, nickname, photo, name, surname, email')
            ->where('id', $this->findTeamUserIds($teamId))
            ->where('status = 1');
        return is_null($finder) ? $result : $finder($result);
    }
}