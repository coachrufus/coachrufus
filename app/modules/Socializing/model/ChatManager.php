<?php namespace CR\Socializing\Model;

class ChatManager
{
    private $repository;
    
    function __construct(ChatRepository $repository)
    {
        $this->repository = $repository;
    }
    
    function sendMessage(ChatMessage $message)
    {
        if (!isset($message->dateTime)) $message->dateTime = new \DateTime();
        if (!isset($message->isReaded)) $message->isReaded = false;
        if (!isset($message->isActive)) $message->isActive = true;
        return new ChatMessage($this->repository->addMessage($message->toArray()));
    }
    
    function receiveMessages(Callable $finder)
    {
        return iterator_to_array((function($finder)
        {
            foreach ($this->repository->findMessages($finder) as $message)
            {
                yield new ChatMessage(iterator_to_array($message));
            }
        })($finder));
    }
    
    function receiveMessagesForUser(int $teamId, int $toUserId, $top = 30, $finder = null)
    {
        return $this->receiveMessages(function($messages) use($teamId, $toUserId, $top, $finder)
        {
            $messages = $messages
                ->where('team_id', $teamId)
                ->where('from_user_id = ? OR to_user_id = ?', $toUserId, $toUserId)
                ->order('id DESC')
                ->group('from_user_id, datetime, body');
            if (!is_null($finder)) $messages = $finder($messages);
            if (!is_null($top)) $messages = $messages->limit($top);
            return $messages;
        });
    }
    
    function getUnreadedMessageCount($userId)
    {
        return $this->repository->getUnreadedMessageCount($userId);
    }
    
    function markMessagesAsReaded($userId)
    {
        return $this->repository->markMessagesAsReaded($userId);
    }
}