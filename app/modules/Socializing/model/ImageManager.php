<?php namespace CR\Socializing\Model;

class ImageManager
{
    function getImageFor($contact)
    {
        $dir = APPLICATION_PATH . '/img/users';
        $thumbDir = "{$dir}/thumb_90_90";
        $path = "{$thumbDir}/{$contact['photo']}";
        return is_file($path) ? $contact['photo'] : 'default.png';
    }
}