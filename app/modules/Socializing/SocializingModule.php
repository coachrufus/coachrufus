<?php namespace CR\Socializing;

use Core\Module as Module;

class SocializingModule extends Module
{
    public function __construct() 
    {
        $this->setBaseDir(MODUL_DIR . '/Socializing');
        $this->setName('Socializing');
    }
}