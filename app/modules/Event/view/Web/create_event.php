

<?php if ($request->hasFlashMessage('_add_success')): ?>
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <?php echo $request->getFlashMessage('_add_success') ?>
    </div>
<?php endif; ?>


<section class="content-header">
    <h1>
        <?php echo $translator->translate('Create event') ?>
    </h1>
</section>
<section class="content">

    
    
<section class="box box-primary">
    <div class="box-body">

<form action="" class="form-bordered" method="post">
    <?php echo $validator->showAllErrors() ?>
    <?php echo $form->renderInputHiddenTag("team_id") ?>
    
    <div class="row">
        <div class="form-group col-sm-4">
            <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("name")) ?></label>
            <?php echo $form->renderInputTag("name", array('class' => 'form-control')) ?>
            <?php echo $validator->showError("name") ?>
            <?php echo $validator->showError("name_unique") ?>
        </div>
        <div class="form-group col-sm-4">
            <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("event_type")) ?></label>
            <?php echo $form->renderSelectTag("event_type", array('class' => 'form-control')) ?>
            <?php echo $validator->showError("event_type") ?>
        </div>
        <div class="form-group col-sm-4">
            <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("season")) ?></label>
            <?php echo $form->renderSelectTag("season", array('class' => 'form-control')) ?>
            <?php echo $validator->showError("season") ?>
        </div>
        
        <div class="form-group col-sm-4">
            <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("start")) ?></label>
            <?php echo $form->renderInputTag("start", array('class' => 'form-control')) ?>
            <?php echo $validator->showError("start") ?>
        </div>
        
        
        <div class="form-group col-sm-4">
            <label class="control-label"><?php echo $translator->translate($form->getFieldLabel("period")) ?></label>
            <?php echo $form->renderSelectTag("period", array('class' => 'form-control')) ?>
            <?php echo $validator->showError("period") ?>
        </div>
    </div>  
    
    <div class="row">

    </div>
    
    
    <div class="form-group">
        <label class="col-sm-3 control-label"><?php echo $translator->translate($form->getFieldLabel("playground_id")) ?></label>
        <div class="col-sm-6">
            <?php echo $form->renderSelectTag("playground_id", array('class' => 'form-control')) ?>
            <?php echo $validator->showError("playground_id") ?>
        </div>
    </div>
    
 
    
    <div class="form-group">
        <label class="col-sm-3 control-label"><?php echo $translator->translate($form->getFieldLabel("period")) ?></label>
        <div class="col-sm-6">
            <?php echo $form->renderSelectTag("period", array('class' => 'form-control')) ?>
            <?php echo $validator->showError("period") ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label"><?php echo $translator->translate($form->getFieldLabel("capacity")) ?></label>
        <div class="col-sm-6">
            <?php echo $form->renderInputTag("capacity", array('class' => 'form-control')) ?>
            <?php echo $validator->showError("capacity") ?>
        </div>
    </div>
    
    <div class="form-group">
        <label class="col-sm-3 control-label"><?php echo $translator->translate($form->getFieldLabel("period_interval")) ?></label>
        <div class="col-sm-6">
            <?php echo $form->renderSelectTag("period_interval", array('class' => 'form-control')) ?>
            <?php echo $validator->showError("period_interval") ?>
        </div>
    </div>
    
    <div class="form-group">
        <label class="col-sm-3 control-label"><?php echo $translator->translate($form->getFieldLabel("repeat_days")) ?></label>
        <div class="col-sm-6">
            <ul>
            <?php foreach($form->getFieldChoices('repeat_days') as $key => $choice): ?>
            <li><input name="<?php echo $form->getName() ?>[repeat_days][]" type="checkbox" value="<?php echo $key ?>" <?php echo (in_array($key, $form->getEntity()->getRepeatDays())) ? 'checked="checked"' : '' ?>> <?php echo $choice ?></li>
            <?php endforeach; ?>
            </ul>
            
          
            <?php echo $validator->showError("repeat_days") ?>
        </div>
    </div>
    
   
    
     <div class="form-group">
        <label class="col-sm-3 control-label"><?php echo $translator->translate($form->getFieldLabel("end")) ?></label>
        <div class="col-sm-6">
            <ul>
                <li><?php echo $form->renderRadioTag("end_period_type",'never', array('class' => 'form-control')) ?><?php echo $translator->translate('Nikdy') ?></li>
                <li><?php echo $form->renderRadioTag("end_period_type",'after', array('class' => 'form-control')) ?><?php echo $translator->translate('Po') ?>
                    <?php echo $form->renderInputTag("end_period", array('class' => 'form-control')) ?> opakovaniach
                    
                </li>
                 <li><?php echo $form->renderRadioTag("end_period_type",'date', array('class' => 'form-control')) ?><?php echo $translator->translate('Dňa') ?>
                 <?php echo $form->renderInputTag("end", array('class' => 'form-control')) ?>
                 
                 </li>
            </ul>
            
            
            
            <?php echo $validator->showError("end") ?>
        </div>
    </div>
    
   
    
    
    <div class="row">
        <div class="col-sm-6 col-sm-offset-3">
            <button class="btn btn-primary">Submit</button>&nbsp;
            <button class="btn btn-default">Cancel</button>
        </div>
    </div>
</form>
    </div>
</section>
</section>
<?php  $layout->startSlot('javascript') ?>
 <script type="text/javascript">
 jQuery('#record_end').datepicker({'dateFormat': 'dd.mm.yy'});
 jQuery('#record_start').datepicker({'dateFormat': 'dd.mm.yy'});

 </script>
<?php  $layout->endSlot('javascript') ?>