<div class="event_row event_list_row last_event_row">
    <div class="event-date" data-href="<?php echo $router->link('team_event_detail', array('id' => $event->getId(), 'current_date' => $event->getCurrentDate()->format('Y-m-d'))) ?>">
       
        <span class="day"><?php echo $translator->getRegionalDate('%d', $event->getCurrentDate()) ?></span>
         <span class="month-accr"><?php echo substr($translator->translate($event->getCurrentDate()->format('F')),0,3) ?> </span>
        <span class="dayname"><?php echo  substr($translator->translate($event->getCurrentDate()->format('l')),0,3) ?></span>
       
    </div>
    <div class="event-info">
        <div class="main-info">
            <h3><a href="<?php echo $router->link('team_event_detail', array('id' => $event->getId(), 'current_date' => $event->getCurrentDate()->format('Y-m-d'))) ?>"><?php echo $event->getName() ?></a></h3>
            <span class="time"><?php echo $event->getTimeInterval(\Core\ServiceLayer::getService('localizer')->getTimeFormat()) ?>  </span>
            <span class="playground"><?php if(null !=$event->getPlaygroundData()):  ?>,  <?php echo $event->getPlaygroundName() ?>, <?php echo $event->getPlaygroundAddress() ?><?php endif;  ?></span>  
        </div>
        
    </div>
	
   
    
    <?php if($event->getClosed() == true): ?>
        <div class="score-cell">
           <?php echo $lastEventScore ?>
       </div>
        <a class="share-cell" href="javascript:shareCallback('<?php $root ?>/widget/api/share-event/<?php echo $event->getId() ?>/<?php echo $event->getCurrentDate()->format('Y-m-d') ?>')">
		<i class="fa fa-facebook-square" aria-hidden="true"></i> <?php echo $translator->translate('Share') ?>
	</a>
    <?php endif; ?>
</div>







