<div id="small-calendar-wrap">

<div class="row header-row">
    <div class="col-xs-3 col-sm-3 text-right">
        <a class="small-calendar-move-month-trigger" href="<?php echo $router->link('rest_event_get_month_grid',array('month' => $calendar->getPrevMonthDateCursor())) ?>"><i class="fa fa-chevron-left"></i></a>
        
    </div>
    <div class="col-xs-6 col-sm-6"><?php echo $translator->translate($calendar->getCurrentMonthName()) ?> <?php echo  $calendar->getDateFrom()->format('Y') ?></div>
    <div class="col-xs-3 col-sm-3 text-left">
        <a class="small-calendar-move-month-trigger" href="<?php echo $router->link('rest_event_get_month_grid',array('month' => $calendar->getNextMonthDateCursor())) ?>"><i class="fa fa-chevron-right"></i></a>
        
    </div>
</div>






<table class="small-calendar">
    <thead>
        <tr class="days">       
            <th><?php echo mb_substr($translator->translate('Monday'),0,3) ?></th>
            <th><?php echo mb_substr($translator->translate('Tuesday'),0,3) ?></th>
            <th><?php echo mb_substr($translator->translate('Wednesday'),0,3) ?></th>
            <th><?php echo mb_substr($translator->translate('Thursday'),0,3) ?></th>
            <th><?php echo mb_substr($translator->translate('Friday'),0,3) ?></th>
            <th><?php echo mb_substr($translator->translate('Saturday'),0,3) ?></th>
            <th><?php echo mb_substr($translator->translate('Sunday'),0,3)?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($calendar->getCalendarMonthOverview() as $week): ?>
            <tr>
                <?php foreach ($week as $day): ?>
                    <td class="day_events_show_trigger">
                        <div class="cell-wrap <?php echo $day['class']  ?> ">
                        <strong><?php echo $day['day'] ?></strong>
                        </div>
                        <div class="day_events_mark">
                            <?php if(array_key_exists('events', $day)): ?>
                            <?php $atendanceRows = array('unknown'=>array(),'in' => array(),'out' => array()); ?>
                            <?php foreach ($day['events'] as $event): ?>
                            
                                <?php 
                                $attendance_status = 'unknown';
                                 if (
                                            array_key_exists($event->getId(), $attendanceList) &&
                                            array_key_exists($event->getCurrentDate()->format('Y-m-d'), $attendanceList[$event->getId()]) &&
                                            $attendanceList[$event->getId()][$event->getCurrentDate()->format('Y-m-d')] == 1)
                                    {
                                        $attendance_status = 'in';
                                    }

                                    if (
                                            array_key_exists($event->getId(), $attendanceList) &&
                                            array_key_exists($event->getCurrentDate()->format('Y-m-d'), $attendanceList[$event->getId()]) &&
                                            $attendanceList[$event->getId()][$event->getCurrentDate()->format('Y-m-d')] == 3)
                                    {
                                        $attendance_status = 'out';
                                    }
                                $atendanceRows[$attendance_status][] = ' <i class="fa fa-circle attendance_type_'.$attendance_status.'" aria-hidden="true"></i>';
                                ?>
                            
                                
                            
                               
                            <?php endforeach; ?>
                            
                            <?php 
                              $final = $atendanceRows['in']+$atendanceRows['unknown']+$atendanceRows['out'];
                                $final1 = array_slice($final,0,4);
                                $final2 = array_slice($final,4,4);
                                echo implode($final1,'').'<br />';
                                echo implode($final2,'');
                            
                            ?>
                            
                            
                            
                                <?php endif; ?>
                        </div>
                        
                        <div class="day_events">
 <?php if(array_key_exists('events', $day)): ?>
                            <?php foreach ($day['events'] as $event): ?>

                                <?php $layout->includePart(MODUL_DIR . '/Event/view/Default/_event_panel_row.php', array( 'event' => $event, 'attendanceList' => $attendanceList, 'team' => $teams[$event->getTeamId()])) ?>
                            
                            <?php endforeach; ?>
                             <?php endif; ?>
                        </div>
                        
                    </td>
                <?php endforeach; ?>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<div id="small-calendar-info-panel">
   
    
   
</div>
</div>
<div id="upcoming-events">
     <?php foreach ($calendar->getUpcomingEvents() as $day => $events): ?>
    <?php foreach($events as $event): ?>
     <?php $layout->includePart(MODUL_DIR . '/Event/view/Default/_event_panel_row.php', array('event' => $event, 'attendanceList' => $attendanceList, 'team' => $teams[$event->getTeamId()])) ?>
            
<?php endforeach; ?>
                       
    <?php endforeach; ?>
</div>
 