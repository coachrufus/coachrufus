<div class="event_row event_list_row <?php echo (array_key_exists($event->getId(), $attendanceList) && array_key_exists($event->getCurrentDate()->format('Y-m-d'), $attendanceList[$event->getId()])  &&  $attendanceList[$event->getId()][$event->getCurrentDate()->format('Y-m-d')] == 3 ) ? 'deny_row' : '' ?> <?php echo (array_key_exists($event->getId(), $attendanceList) && array_key_exists($event->getCurrentDate()->format('Y-m-d'), $attendanceList[$event->getId()])  &&  $attendanceList[$event->getId()][$event->getCurrentDate()->format('Y-m-d')] == 1 ) ? 'accept_row' : '' ?>" >
    <div class="event-date" data-href="<?php echo $router->link('team_event_detail', array('id' => $event->getId(), 'current_date' => $event->getCurrentDate()->format('Y-m-d'))) ?>">
       
        <span class="day"><?php echo $translator->getRegionalDate('%d', $event->getCurrentDate()) ?></span>
         <span class="month-accr"><?php echo mb_substr($translator->translate($event->getCurrentDate()->format('F')),0,3) ?> </span>
        <span class="dayname"><?php echo  mb_substr($translator->translate($event->getCurrentDate()->format('l')),0,3) ?></span>
       
    </div>
    <div class="event-info">
        <div class="main-info">
            <h3><a href="<?php echo $router->link('team_event_detail', array('id' => $event->getId(), 'current_date' => $event->getCurrentDate()->format('Y-m-d'))) ?>"><?php echo $event->getName() ?></a></h3>
            
             <span class="time"><?php echo $event->getTimeInterval(\Core\ServiceLayer::getService('localizer')->getTimeFormat()) ?> </span>
             <span class="playground"><?php if(null !=$event->getPlaygroundData()):  ?> <?php echo $event->getPlaygroundName() ?> (<?php echo $event->getPlaygroundAddress() ?>)<?php endif;  ?></span>  
        </div>
        
    </div>
    
    <?php if($event->getLineup() !=null && $event->getLineup()->getStatus() == 'closed'): ?>
     <div class="score-cell">
        <?php echo $event->getMatchResult() ?>
    </div>
	<a class="share-cell" href="javascript:shareCallback('<?php $root ?>/widget/api/share-event/<?php echo $event->getId() ?>/<?php echo $event->getCurrentDate()->format('Y-m-d') ?>')">
		<i class="fa fa-facebook-square" aria-hidden="true"></i> <?php echo $translator->translate('Share') ?>
	</a>
<?php else: ?>


    <div class="attendance-cell">

        <div class="row-sidebar">
            <a href="#" class="row-sidebar-trigger" data-target="row-sidebar-cnt-<?php echo $event->getUid() ?>"><i class="ico ico-more-b"  aria-hidden="true"></i></a>
        </div>

        <div class="row-sidebar-cnt" id="row-sidebar-cnt-<?php echo $event->getUid() ?>" >
            
            
             <?php if( \Core\ServiceLayer::getService('security')->getIdentity()->hasTeamPermission('manageEvents',$team)): ?>
            
            <?php if($event->getLineup() == null or $event->getLineup()->getStatus() != 'closed'): ?>
            <a class="tool-item delete-event-trigger" data-event-type="<?php echo $event->getPeriod() ?>" href="<?php echo $router->link('delete_team_event', array('current_date' => $event->getCurrentDate()->format('Y-m-d'),'event_id' => $event->getId())) ?>"><span><i class="ico ico-trash"></i> <?php echo $translator->translate('Delete event') ?> </span></a>
           
                
                <?php if(null != $event->getStatRoute() && $event->getLineup() != null): ?>
                    <a class="event-detail-menu-item <?php echo ($menuActive == 'score') ? 'active' : '' ?>" href="<?php echo $router->link($event->getStatRoute(),array('event_id' => $event->getId(), 'eventdate' =>  $event->getCurrentDate()->format('Y-m-d'),'lid' =>$event->getLineup()->getId()  )) ?>">
                        <span><i class="ico ico-event-score"></i> <?php echo $translator->translate('Score') ?></span>
                    </a>
                <?php endif; ?>
            
                
          <?php endif; ?>
             <?php endif; ?>
            
           
            <a href="webcal://<?php echo WEB_DOMAIN_NAME ?><?php echo urlencode($router->link('team_event_single_ical',array('id' => $event->getId(),'datetime' =>  $event->getCurrentDate()->format('Y-m-d').' '.$event->getStart()->format('H:i:s') ))) ?>"><span><i class="ico ico-export"></i> <?php echo $translator->translate('Outlook') ?></span> </a>
            <a href="webcal://<?php echo WEB_DOMAIN_NAME ?><?php echo urlencode($router->link('team_event_single_ical',array('id' => $event->getId(),'datetime' =>  $event->getCurrentDate()->format('Y-m-d').' '.$event->getStart()->format('H:i:s') ))) ?>"><span><i class="ico ico-export"></i> <?php echo $translator->translate('iCalc') ?></span> </a>
            <a href="https://www.google.com/calendar/render?cid=webcal://<?php echo WEB_DOMAIN_NAME ?><?php echo urlencode($router->link('team_event_single_ical',array('id' => $event->getId(),'datetime' =>  $event->getCurrentDate()->format('Y-m-d').' '.$event->getStart()->format('H:i:s')) )) ?>"><span><i class="ico ico-export"></i> <?php echo $translator->translate('Google calendar') ?></span> </a>
        </div>
                
        
        
        
        
            <div class="members-attendance-wrap">
                <div class="members-attendance" id="members-attendance-<?php echo $event->getUid()  ?>">
                    <span class="members-attendance-in"><?php echo $event->getAttendanceInSum() ?></span> <span class="members-attendance-capacity"><?php echo $event->getCapacity() ?></span>                 
                </div>
            </div>
            <div class="event-list-attendance">
                
                
                
               
                <a href="#"
                    data-url="<?php echo $router->link('change_player_attendance') ?>" 
                    data-rel="in" 
                    data-event="<?php echo $event->getId() ?>" 
                    data-eventdate="<?php echo  $event->getCurrentDate()->format('Y-m-d')  ?>" 
                    data-team-id="<?php echo $event->getTeamId() ?>"
                    data-toggle="tooltip" data-placement="top" title="<?php echo $translator->translate('In') ?>" 
                    class="in attendance_trigger attendance_trigger_btn  <?php echo (array_key_exists($event->getId(), $attendanceList) && array_key_exists($event->getCurrentDate()->format('Y-m-d'), $attendanceList[$event->getId()])  &&  $attendanceList[$event->getId()][$event->getCurrentDate()->format('Y-m-d')] == 1 ) ? 'accept' : '' ?>"><i class="ico ico-check"></i></a>





                    <a href="#"
                            data-url="<?php echo $router->link('change_player_attendance') ?>" 
                            data-rel="out" 
                            data-event="<?php echo $event->getId() ?>" 
                            data-eventdate="<?php echo  $event->getCurrentDate()->format('Y-m-d')  ?>"                                   
                            data-team-id="<?php echo $event->getTeamId() ?>" 
                            data-toggle="tooltip" data-placement="top" title="<?php echo $translator->translate('out') ?>" 
                             class="out attendance_trigger attendance_trigger_btn  <?php echo (array_key_exists($event->getId(), $attendanceList) && array_key_exists($event->getCurrentDate()->format('Y-m-d'), $attendanceList[$event->getId()])  &&  $attendanceList[$event->getId()][$event->getCurrentDate()->format('Y-m-d')] == 3 ) ? 'deny' : '' ?>"><i class="ico ico-close"></i></a>
                    
            </div>
    </div>
   <?php endif; ?>
</div>







