<div class="event_row">
    <div class="event-date" data-href="<?php echo $router->link('team_event_detail', array('id' => $event->getId(), 'current_date' => $event->getCurrentDate()->format('Y-m-d'))) ?>">
        <a href="<?php echo $router->link('team_event_detail', array('id' => $event->getId(), 'current_date' => $event->getCurrentDate()->format('Y-m-d'))) ?>">
        <span class="day"><?php echo $translator->getRegionalDate('%d', $event->getCurrentDate()) ?></span>
         <span class="month-accr"><?php echo mb_substr($translator->translate($event->getCurrentDate()->format('F')),0,3) ?> </span>
        <span class="dayname"><?php echo  mb_substr($translator->translate($event->getCurrentDate()->format('l')),0,3) ?></span>
       </a>
    </div>
    <div class="event-info">
        <div class="main-info">
            <h3><a href="<?php echo $router->link('team_event_detail', array('id' => $event->getId(), 'current_date' => $event->getCurrentDate()->format('Y-m-d'))) ?>"><?php echo $event->getName() ?></a></h3>
            <a class="team-name" href="<?php echo $router->link('team_event_detail', array('id' => $event->getId(), 'current_date' => $event->getCurrentDate()->format('Y-m-d'))) ?>"><?php echo $event->getTeamName() ?></a>  
             <span class="time"><?php echo $event->getTimeInterval(\Core\ServiceLayer::getService('localizer')->getTimeFormat()) ?> </span>
            
        </div>
        
    </div>
    
    
    <?php if($event->getMatchResult() != null): ?>
        <div class="score-cell">
            <?php echo $event->getMatchResult() ?>
			<a class="share-btn-mini" href="javascript:shareCallback('<?php $root ?>/widget/api/share-event/<?php echo $event->getId() ?>/<?php echo $event->getCurrentDate()->format('Y-m-d') ?>')">
				<i class="fa fa-facebook-square" aria-hidden="true"></i>
			</a>
        </div>
    <?php else: ?>

    <div class="attendance-cell">
            <div class="members-attendance-wrap">
                <div class="members-attendance" id="members-attendance-<?php echo $event->getUid()  ?>">
                    <span class="members-attendance-in"><?php echo $event->getAttendanceInSum() ?></span> <span class="members-attendance-capacity"><?php echo $event->getCapacity() ?></span>                 
                </div>
            </div>
        
          
            <div class="event-list-attendance">
                <?php if(\Core\ServiceLayer::getService('TeamCreditManager')->teamHasFeatureAccess($team,'attendance')): ?>
                <a href="#"
                    data-url="<?php echo $router->link('change_player_attendance') ?>" 
                    data-rel="in" 
                    data-event="<?php echo $event->getId() ?>" 
                    data-eventdate="<?php echo  $event->getCurrentDate()->format('Y-m-d')  ?>" 
                    data-team-id="<?php echo $event->getTeamId() ?>"
                    data-toggle="tooltip" data-placement="top" title="<?php echo $translator->translate('In') ?>" 
                    class="in attendance_trigger attendance_trigger_btn  <?php echo (array_key_exists($event->getId(), $attendanceList) && array_key_exists($event->getCurrentDate()->format('Y-m-d'), $attendanceList[$event->getId()])  &&  $attendanceList[$event->getId()][$event->getCurrentDate()->format('Y-m-d')] == 1 ) ? 'accept' : '' ?>"><i class="ico ico-check"></i></a>

                    <a href="#"
                            data-url="<?php echo $router->link('change_player_attendance') ?>" 
                            data-rel="out" 
                            data-event="<?php echo $event->getId() ?>" 
                            data-eventdate="<?php echo  $event->getCurrentDate()->format('Y-m-d')  ?>"                                   
                            data-team-id="<?php echo $event->getTeamId() ?>" 
                            data-toggle="tooltip" data-placement="top" title="<?php echo $translator->translate('out') ?>" 
                             class="out attendance_trigger attendance_trigger_btn  <?php echo (array_key_exists($event->getId(), $attendanceList) && array_key_exists($event->getCurrentDate()->format('Y-m-d'), $attendanceList[$event->getId()])  &&  $attendanceList[$event->getId()][$event->getCurrentDate()->format('Y-m-d')] == 3 ) ? 'deny' : '' ?>"><i class="ico ico-close"></i></a>
                     <?php else: ?>

                         <a href="#" class="in team_credit_modal_trigger attendance_trigger_btn"><i class="ico ico-check"></i></i></a>
                         <a href="#" class="in team_credit_modal_trigger attendance_trigger_btn"><i class="ico ico-close"></i></a>
                     <?php endif; ?>     
            </div>
          
    </div>
      <?php endif; ?>
</div>







