<div class="panel panel-default panel-calendar">
    <div class="panel-heading"
        <h3>
            <i class="ico ico-calendar-empty ico-action"><?php echo date('d') ?></i>
            <?php echo $translator->translate('Events') ?>
        </h3>

        <div class="panel-toolbar">
           
            <a class="upcoming-events-trigger tool-item" href=""><i class="ico ico-panel-event-list"></i></a>
            <a class="calendar-events-trigger tool-item" href=""><i class="ico ico-event-cal"></i></a>
            <i class="ico ico-action ico-panel-close"></i>
        </div>
        
         <div class="pull-right panel-tools">
            <a href="#" class="panel-settings-trigger" data-target="calendar-panel-settings"> <i class="ico ico-plus-green"></i></a>
        </div>
    </div>
    
     <div  class="panel-settings" id="calendar-panel-settings">
        <?php foreach(\Core\ServiceLayer::getService('security')->getIdentity()->getUserTeamsInfo() as $teamId => $teamInfo): ?>
        <?php if($teamInfo['role'] == 'ADMIN'): ?>
        <div class="settings-row">
            <a  href="<?php echo $router->link('create_team_event',array('team_id' => $teamId)) ?>"><i class="ico ico-plus"></i> <?php echo $translator->translate('Add event for team') ?> <?php echo $teamInfo['team']->getName()  ?></a>
        </div>
         <?php endif; ?>
        <?php endforeach; ?>
    </div>
    
    
    <div class="panel-body  panel-full-body bg-green">
        <div id="small-calendar-cnt" class="text-center">
        </div>
    </div>
</div>

