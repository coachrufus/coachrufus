<div class="panel panel-default">
    <div class="panel-heading">
        
        <h3>
            <i class="ico ico-calendar-empty ico-action"><?php echo date('d') ?></i> 
            <?php echo $translator->translate('Event Calendar') ?>
        </h3>
        <div class="pull-right">
           
            <a class="upcoming-events-trigger" href="#"><i class="ico ico-panel-event-list"></i></a>
             <i class="ico ico-action ico-panel-close"></i>
        </div>
    </div>
    <div class="panel-body  panel-full-body bg-green">

<?php  $calendar->getCalendarMonthOverview()  ?>

     <?php foreach ($calendar->getUpcomingEvents() as $day => $events): ?>
    <?php foreach($events as $event): ?>
     <?php $layout->includePart(MODUL_DIR . '/Event/view/Default/_event_panel_row.php', array('event' => $event, 'attendanceList' => $attendanceList, 'team' => $teams[$event->getTeamId()])) ?>
            
<?php endforeach; ?>
                       
    <?php endforeach; ?>




    </div>
</div>







