<?php

namespace Webteamer\Event\Form;

use \Core\Form as Form;

class EventForm extends Form {

    private $playgroundsDesc;
    protected $fields = array(
        'period' =>
        array(
            'type' => 'choice',
            'max_length' => '50',
            'required' => false,
            'label' => 'Repeat event'
        ),
        'period_interval' =>
        array(
            'type' => 'choice',
            'max_length' => '50',
            'required' => false,
            'label' => 'Repeat every'
        ),
        'repeat_days' =>
        array(
            'type' => 'choice',
            'required' => false,
        ),
        'start' =>
        array(
            'type' => 'datetime',
            'max_length' => '',
            'required' => false,
            'label' => 'Date',
            'format' => 'd.m.Y H:i:s'
        ),
        'start_date' =>
        array(
            'type' => 'datetime',
            'max_length' => '',
            'required' => true,
            'label' => 'Start from',
            'format' => 'd.m.Y'
        ),
        'start_time' =>
        array(
            'type' => 'time',
            'max_length' => '',
            'required' => true,
            'label' => 'Start time',
        ),
        'end_time_time' =>
        array(
            'type' => 'time',
            'max_length' => '',
            'required' => false,
            'label' => 'End time',
        ),
        'end' =>
        array(
            'type' => 'datetime',
            'max_length' => '',
            'required' => false,
        ),
        'end_period' =>
        array(
            'type' => 'string',
            'max_length' => '50',
            'required' => false,
        ),
        'end_period_type' =>
        array(
            'type' => 'string',
            'type' => 'choice',
            'max_length' => '50',
            'required' => false,
        ),
        'team_id' =>
        array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false,
        ),
        'capacity' =>
        array(
            'type' => 'int',
            'max_length' => '11',
            'required' => true,
        ),
        'event_type' =>
        array(
            'type' => 'string',
            'max_length' => '50',
            'required' => false,
        ),
        'name' =>
        array(
            'type' => 'string',
            'max_length' => '50',
            'required' => true,
            'label' => 'Event name'
        ),
        'season' =>
        array(
            'type' => 'string',
            'max_length' => '50',
            'required' => true,
        ),
        'playground_id' =>
        array(
            'type' => 'choice',
            'label' => 'Playground',
            'max_length' => '11',
            'required' => false,
        ),
        'notification' =>
        array(
            'type' => 'int',
            'label' => 'Notification team',
            'max_length' => '11',
            'required' => false,
        ),
        'notify_termin' =>
        array(
            'type' => 'datetime',
            'required' => false,
        ),
        'notify_termin_type' =>
        array(
            'type' => 'choice',
            'required' => false,
        ),
        'notify_group' =>
        array(
            'type' => 'string',
            'required' => false,
        ),
        'notify_beep_days' =>
        array(
            'type' => 'choice',
            'choices' => array('1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6', '7' => '7'),
            'required' => false,
        ),
        'playground_choice' =>
        array(
            'type' => 'choice',
            'required' => false,
        ),
    );

    public function __construct()
    {
        
    }

    public function getPlaygroundsDesc()
    {
        return $this->playgroundsDesc;
    }

    public function setPlaygroundsDesc($playgroundsDesc)
    {
        $this->playgroundsDesc = $playgroundsDesc;
    }

    public function getPlaygoroundChoices()
    {
        $choices = array();
        foreach ($this->getPlaygroundsDesc() as $playgroundId => $playground)
        {
            $address = (',' != trim($playground->getFullAddress())) ? '(' . $playground->getFullAddress() . ')' : '';
            $choices[$playgroundId] = $playground->getName() . ' ' . $address;
        }
        return $choices;
    }

}
