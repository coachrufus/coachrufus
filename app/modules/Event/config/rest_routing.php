<?php

$acl->allowRole('guest','rest_event_get_month_grid');
$router->addRoute('rest_event_get_month_grid','/api/event/month-grid',array(
		'controller' => 'Webteamer\Event\EventModule:EventRest:monthGridOverview'
));

$acl->allowRole('guest','rest_event_get_year_grid');
$router->addRoute('rest_event_get_year_grid','/api/event/year-grid',array(
		'controller' => 'Webteamer\Event\EventModule:EventRest:yearGridOverview'
));

$acl->allowRole('guest','rest_event_change_attendance');
$router->addRoute('rest_event_change_attendance','/api/event/change-player-attendance',array(
		'controller' => 'Webteamer\Event\EventModule:EventRest:changePlayerAttendance'
));

$acl->allowRole('guest','rest_event_detail');
$router->addRoute('rest_event_detail','/api/event/detail',array(
		'controller' => 'Webteamer\Event\EventModule:EventRest:eventDetail'
));

$acl->allowRole('guest','rest_event_test');
$router->addRoute('rest_event_test','/api/event/test',array(
		'controller' => 'Webteamer\Event\EventModule:EventRest:test'
));

$acl->allowRole('ROLE_USER','widget_player_upcoming_events');
$router->addRoute('widget_player_upcoming_events','/widget/event/player-upcoming-events',array(
		'controller' => 'Webteamer\Event\EventModule:EventRest:monthGridOverview'
));
