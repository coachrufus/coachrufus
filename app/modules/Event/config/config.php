<?php
namespace Webteamer\Event;
require_once(MODUL_DIR.'/Event/EventModule.php');


use Core\ServiceLayer as ServiceLayer;



$teamEventMapper = new \Core\EntityMapper('Webteamer\Event\Model\Event');
$teamEventMapper->addAssocEntity(array(
    'class' => 'Webteamer\Locality\Model\Playground' , 
    'method' => 'setPlayground',
    'mapper' => '\Core\EntityMapper'));

$teamEventStorage = new   \Webteamer\Event\Model\EventStorage('team_event');
$teamEventStorage->addAssocTable('a_','playground');



ServiceLayer::addService('EventRepository',array(
'class' => "Webteamer\Event\Model\EventRepository",
	'params' => array(
		$teamEventStorage,
		$teamEventMapper
	)
));


$eventAttendanceMapper = new \Core\EntityMapper('Webteamer\Event\Model\EventAttendance');
$eventAttendanceMapper->addAssocEntity(array(
    'class' => '\Webteamer\Player\Model\Player' , 
    'method' => 'setPlayer',
    'mapper' => '\Core\EntityMapper'));

$eventAttendanceStorage = new   \Webteamer\Event\Model\EventAttendanceStorage('team_event_attendance');
$eventAttendanceStorage->addAssocTable('a_','co_users');


ServiceLayer::addService('EventAttendanceRepository',array(
'class' => "Webteamer\Event\Model\EventAttendanceRepository",
	'params' => array(
		$eventAttendanceStorage ,
		$eventAttendanceMapper
	)
));

$eventCostsMapper = new \Core\EntityMapper('Webteamer\Event\Model\EventCosts');
$eventCostsStorage = new \Webteamer\Event\Model\EventCostsStorage('team_event_costs');

ServiceLayer::addService('EventCostsRepository', array(
'class' => "Webteamer\Event\Model\EventCostsRepository",
	'params' => array(
		$eventCostsStorage ,
		$eventCostsMapper
	)
));

ServiceLayer::addService('EventManager',array(
'class' => "Webteamer\Event\Model\EventManager",
    'params' => array(
		ServiceLayer::getService('EventRepository'),
                ServiceLayer::getService('EventAttendanceRepository'),
                ServiceLayer::getService('EventCostsRepository'),
	)
));

ServiceLayer::addService('RepeatEventManager',array(
'class' => "Webteamer\Event\Model\RepeatEventManager",
   
));


$acl = ServiceLayer::getService('acl');


//routing
$router = ServiceLayer::getService('router');
include_once('rest_routing.php');




$acl->allowRole('ROLE_USER','edit_event');
$router->addRoute('edit_event','/event/edit/:team_id/:event_id',array(
		'controller' => 'Webteamer\Event\EventModule:Web:editEvent'
));


$acl->allowRole('ROLE_USER','edit_event');
$router->addRoute('edit_event','/event/edit/:id',array(
		'controller' => 'Webteamer\Event\EventModule:Owner:editEvent'
));

$acl->allowRole(array('ROLE_USER'),'event_match_stat');
$router->addRoute('event_match_stat','/event/match/:event_id',array(
		'controller' => 'Webteamer\Event\EventModule:Web:match'
));


//TESTS
$acl->allowRole('ROLE_USER','test_create_event');
$router->addRoute('test_create_event','/event/test/create',array(
		'controller' => 'Webteamer\Event\EventModule:Test:createTeamEvent'
));

$acl->allowRole('ROLE_USER','test_create_event_attendance');
$router->addRoute('test_create_event_attendance','/event/test/create-attendance',array(
		'controller' => 'Webteamer\Event\EventModule:Test:createTeamEventAttendance'
));






