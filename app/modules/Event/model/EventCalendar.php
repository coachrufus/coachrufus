<?php

namespace Webteamer\Event\Model;

/**
 * @author Marek Hubacek
 * @version 1.0
 * @created 21-12-2015 13:25:05
 */
class EventCalendar {

    private $events;
    private $dateFrom;
    private $dateTo;
    private $monthGrid;
    private $currentMonth;
    private $currenDayEvents;
    private $upcomingEvents = array();

    public function getEvents()
    {
        return $this->events;
    }

    public function setEvents($events)
    {
        $this->events = $events;
    }
    
    public  function getCurrenDayEvents() {
return $this->currenDayEvents;
}

public  function setCurrenDayEvents($currenDayEvents) {
$this->currenDayEvents = $currenDayEvents;
}

public  function getUpcomingEvents() {
return $this->upcomingEvents;
}

public  function setUpcomingEvents($upcomingEvents) {
$this->upcomingEvents = $upcomingEvents;
}

public function addUpcomingEvents($index,$events)
{
    $this->upcomingEvents[$index]=$events;
}

    

    public function getDateFrom()
    {
        return $this->dateFrom;
    }

    public function getDateTo()
    {
        return $this->dateTo;
    }

    public function setDateFrom($dateFrom)
    {
        $this->dateFrom = $dateFrom;
    }

    public function setDateTo($dateTo)
    {
        $this->dateTo = $dateTo;
    }
    
 

    public function getCurrentMonthName()
    {
        return $this->getDateFrom()->format('F');
    }
    
    public function getNextMonthDateCursor()
    {
        $dateTo = clone $this->getDateTo();
        $dateTo->modify('+1 day');
        return $dateTo->format('Y-m');
    }
    
    public function getPrevMonthDateCursor()
    {
        $dateTo = clone $this->getDateFrom();
        $dateTo->modify('-1 day');
        return $dateTo->format('Y-m');
    }

    public function getDatesDiff($date1, $date2, $type)
    {


        if ('weeks' == $type)
        {
            return floor($date1->diff($date2)->days / 7);
        }
        if ('days' == $type)
        {
            return $date1->diff($date2)->days;
        }
    }

    public function getEventNearestTermins($event, $date = null, $range = 5)
    {
        $start = $event->getStart();
        if (null == $date)
        {
            $date = new \DateTime();
        }

        if ('weekly' == $event->getPeriod())
        {
            $weeksDiff = $this->getDatesDiff($start, $date, 'weeks');
            $offsetStart = $event->offsetStart('+' . $weeksDiff . ' weeks');
            var_dump($offsetStart);
        }
    }

    /**
     * Returns the amount of weeks into the month a date is - thks Marty :-) (http://stackoverflow.com/questions/5853380/php-get-number-of-week-for-month)
     * @param $date a YYYY-MM-DD formatted date
     * @param $rollover The day on which the week rolls over
     */
    function getWeekInMonth($date, $rollover = 'sunday')
    {
        $cut = substr($date, 0, 8);
        $daylen = 86400;

        $timestamp = strtotime($date);
        $first = strtotime($cut . "00");
        $elapsed = ($timestamp - $first) / $daylen;

        $weeks = 1;

        for ($i = 1; $i <= $elapsed; $i++)
        {
            $dayfind = $cut . (strlen($i) < 2 ? '0' . $i : $i);
            $daytimestamp = strtotime($dayfind);

            $day = strtolower(date("l", $daytimestamp));

            if ($day == strtolower($rollover))
                $weeks ++;
        }

        return $weeks;
    }

    /**
     * first date in calendar grid
     * @return \DateTime
     */
    public function getGridStartDate()
    {
        $monthGrid = $this->buildMonthGrid();
        return new \DateTime($monthGrid[0][0]['date']);
    }

    /**
     * last date in calendar gridd
     * @return \DateTime
     */
    public function getGridEndDate()
    {
        $monthGrid = $this->buildMonthGrid();
        $lastWeek = end($monthGrid);
        $lastDay = end($lastWeek);
        return new \DateTime($lastDay['date']);
    }

    /*
     * Return days grid 5x7 grid for current month
     */

    public function buildMonthGrid($month = null)
    {
        if (null != $this->monthGrid)
        {
            return $this->monthGrid;
        }


        if (null == $month)
        {
            $month = new \DateTime();
        }
        else
        {
            $month = \DateTime::createFromFormat('Y-m-d', $month . '-01');
        }

        $current_month = $month->format('n');
        $current_year = $month->format('Y');
        $days_count = date('t', mktime(0, 0, 0, $current_month, 1));

        $first_day_in_month = mktime(0, 0, 0, $current_month, 1,$current_year);
        $last_day_in_month = mktime(0, 0, 0, $current_month, $days_count,$current_year);
        

        //zistim na aky den pripada prveho tohto mesiaca
        $difference = date('w', $first_day_in_month);
        if (0 == $difference)
        {
            $difference = 7;
        }

        $class = [];
        for ($i = 1; $i <= $days_count; $i++)
        {
            $day_stamp = mktime(0, 0, 0, $current_month, $i, $current_year);
            $day_index = date('Y-m-d', $day_stamp);

            if (date('Y-m-d') == $day_index)
            {
                $class[] = 'current_day';
            }
            elseif($day_stamp < time())
            {
                $class[] = 'prev_day';
            }
            
           
            
            
            

            $days[$difference + $i - 2] = array(
                'day' => date('d', $day_stamp),
                'month' => date('n', $day_stamp),
                'date' => date('Y-m-d', $day_stamp),
                'day_name' => date('l', $day_stamp),
                'class' => implode(' ',$class),
            );

            $class = [];
        }


        //najprv doplnim zaciatok kalendara
        $first_calendar_day = date('Y-m-d', mktime(0, 0, 0, $current_month, 2 - $difference, $current_year));
        
        

        for ($i = 0; $i < $difference - 1; $i++)
        {
            $day_stamp = mktime(0, 0, 0, $current_month, 2 - $difference + $i, $current_year);
            $day_index = date('Y-m-d', $day_stamp);
            $class = 'prev_month_day';

            $days[$i] = array(
                'day' => date('d', $day_stamp),
                'month' => date('n', $day_stamp),
                'date' => date('Y-m-d', $day_stamp),
                'day_name' => date('l', $day_stamp),
                'class' => $class,
            );
        }



        $end_month_difference = 7 - date('w', $last_day_in_month);
        $current_days_length = count($days);


        for ($i = 1; $i <= $end_month_difference; $i++)
        {
            $day_stamp = mktime(0, 0, 0, $current_month + 1, $i, $current_year);
            $day_index = date('Y-m-d', $day_stamp);
            $days[$current_days_length + $i] = array(
                'day' => date('d', $day_stamp),
                'month' => date('n', $day_stamp),
                'date' => date('Y-m-d', $day_stamp),
                'day_name' => date('l', $day_stamp),
                'class' => 'next_month_day',
                'events' => array()
            );
        }
        ksort($days);

        $this->monthGrid = array_chunk($days, 7);
        return $this->monthGrid;
    }

    public function getCalendarMonthOverview($calendar_date = null)
    {
        $baseEvents = $this->getEvents();
        $monthGrid = $this->buildMonthGrid($calendar_date);
        foreach ($monthGrid as $weekIndex => $week)
        {
            foreach ($week as $dayI => $day)
            {
               
                $dayIndex = $day['date'];
                if (array_key_exists($dayIndex, $baseEvents))
                {
                    $monthGrid[$weekIndex][$dayI]['events'] = $baseEvents[$dayIndex];

                    if(date('Y-m-d') == $dayIndex)
                    {
                        $this->setCurrenDayEvents($baseEvents[$dayIndex]);
                    }
                    
                    $dayDate = new \DateTime($day['date']);
                    
                    if($dayDate->getTimestamp() > time())
                    {
                        $this->addUpcomingEvents($dayIndex, $baseEvents[$dayIndex]);
                    }
                    
                    
                }
            }
        }

        return $monthGrid;
    }

    /**
     * 
     * @param month month in format Y-m
     */
    public function getCalendarMonthOverviewOld($month = null)
    {

        if (null == $month)
        {
            $month = new \DateTime();
        }
        else
        {
            $month = \DateTime::createFromFormat('Y-m-d', $month . '-01');
        }

        $current_month = $month->format('n');
        $current_year = $month->format('Y');
        $days_count = date('t', mktime(0, 0, 0, $current_month, 1));

        //$events = $this->getEvents($current_month, $current_year);
        $baseEvents = $this->getEvents();
        $events = array();
        $first_day_in_month = mktime(0, 0, 0, $current_month, 1);
        $last_day_in_month = mktime(0, 0, 0, $current_month, $days_count);

        //var_dump($baseEvents);exit;
        //zistim na aky den pripada prveho tohto mesiaca
        $difference = date('w', $first_day_in_month);
        if (0 == $difference)
        {
            $difference = 7;
        }

        $class = '';
        for ($i = 1; $i <= $days_count; $i++)
        {
            $day_stamp = mktime(0, 0, 0, $current_month, $i, $current_year);
            //attach event
            $day_index = date('Y-m-d', $day_stamp);

            if (date('j') == $i)
            {
                $class = $class . ' active';
            }

            $days[$difference + $i - 2] = array(
                'day' => date('d', $day_stamp),
                'month' => date('n', $day_stamp),
                'date' => date('Y-m-d', $day_stamp),
                'day_name' => date('l', $day_stamp),
                'class' => $class,
                'events' => (array_key_exists($day_index, $baseEvents)) ? $baseEvents[$day_index] : array()
            );

            $class = '';
        }



        //
        //najprv doplnim zaciatok kalendara
        $first_calendar_day = date('Y-m-d', mktime(0, 0, 0, $current_month, 2 - $difference, $current_year));
        for ($i = 0; $i < $difference - 1; $i++)
        {
            $day_stamp = mktime(0, 0, 0, $current_month, 2 - $difference + $i, $current_year);
            $day_index = date('Y-m-d', $day_stamp);


            $days[$i] = array(
                'day' => date('d', $day_stamp),
                'month' => date('n', $day_stamp),
                'date' => date('Y-m-d', $day_stamp),
                'day_name' => date('l', $day_stamp),
                'class' => $class,
                'events' => (array_key_exists($day_index, $events)) ? $events[$day_index] : array()
            );
        }



        $end_month_difference = 7 - date('w', $last_day_in_month);
        $current_days_length = count($days);


        for ($i = 1; $i <= $end_month_difference; $i++)
        {
            $day_stamp = mktime(0, 0, 0, $current_month + 1, $i, $current_year);
            $day_index = date('Y-m-d', $day_stamp);
            $days[$current_days_length + $i] = array(
                'day' => date('d', $day_stamp),
                'month' => date('n', $day_stamp),
                'date' => date('Y-m-d', $day_stamp),
                'day_name' => date('l', $day_stamp),
                'class' => $class,
                'events' => (array_key_exists($day_index, $events)) ? $events[$day_index] : array()
            );
        }
        ksort($days);
        return array_chunk($days, 7);
    }
    
    public function getCurrentMonth() {
return $this->currentMonth;
}

 public function setCurrentMonth($currentMonth) {
$this->currentMonth = $currentMonth;
}



}

?>