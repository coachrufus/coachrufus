<?php
namespace Webteamer\Event\Model;
use Core\DbStorage;
class EventAttendanceStorage extends DbStorage {
    
	
	public function findByParticipants($playerIds)
        {
            $ids = implode(',',$playerIds);
            $data = \Core\DbQuery::prepare('SELECT * FROM '.$this->getTableName().' WHERE participant_id in ('.$ids.')')
		->execute()
		->fetchAll(\PDO::FETCH_ASSOC);

		return $data;
        }
        
        public function findByEvents($eventIds)
        {
            $ids = implode(',',$eventIds);
            $data = \Core\DbQuery::prepare('SELECT * FROM '.$this->getTableName().' WHERE event_id in ('.$ids.')')
		->execute()
		->fetchAll(\PDO::FETCH_ASSOC);

		return $data;
        }
        
        public function findByParticipantAndEvents($playerId,$eventCriteria)
        {
            $criteriaParams = array();
            foreach($eventCriteria as $key => $criteria)
            {
                $criteriaSql[] = '(event_id = :event_id_'.$key.' and event_date = :event_date_'.$key.')';
            }
            $criteriaSqlString = '';
            if(!empty($criteriaSql))
            {
                $criteriaSqlString = 'AND ('. implode('OR',$criteriaSql).')';
            }
            
            
            $query = \Core\DbQuery::prepare('SELECT * FROM '.$this->getTableName().' WHERE participant_id = :participant_id '.$criteriaSqlString);
            
            $query->bindParam(':participant_id', $playerId);
            
            foreach($eventCriteria as $key => $criteria)
            {
                 $query->bindParam(':event_id_'.$key, $criteria['event_id']);
                 $query->bindParam(':event_date_'.$key, $criteria['event_date']);
            }
            
            $data = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
            return $data;
        }
	
        
         public function findBy($query_params = array(),$options = array())
	{


            $where_criteria_items  = array();
		foreach ($query_params as $column => $column_val)
		{
			if(is_string($column_val))
			{
				$where_criteria_items[] = ' u.'.$column.'=:'.$column.' ';
			}
			else
			{
				$where_criteria_items[] = ' u.'.$column.'=:'.$column.' ';
			}
			
		}
		$where_criteria = ' WHERE '. implode(' AND ',$where_criteria_items);
        
                $sort_criteria = '';
                if(array_key_exists('order',$options))
                {
                    $sort_criteria = ' order by u.'.$options['order'];
                }

		$query = \Core\DbQuery::prepare('SELECT u.* , '.$this->getAssocColumnsQuery().' '
                        . ' FROM '.$this->getTableName().' u '
                        . 'LEFT JOIN co_users a ON u.participant_id = a.id '.
                       
                        $where_criteria.$sort_criteria);
                
                
                
		foreach($query_params as $column => $value)
		{
			if("" != $value)
			{
				$query->bindParam(':'.$column, $value);
			}
		}
		$data = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
		return $data;
	}
}