<?php
namespace Webteamer\Event\Model;
use Core\DbStorage;
class EventStorage extends DbStorage {
    
	
         public function findNotifyTeamEvents($criteria)
        {
            $criteriaString = '';
            $whereCriteriaItems = array('e.notification = 1');

            $whereCriteriaItems[] = " (
                            (e.start <= :start1 AND ( (e.end >= :start2 AND  e.end <= :end1) OR  e.end is null)) 
                        OR 
                            (e.start >= :start3 AND ( (e.end >= :end2 AND  e.start <= :end3) OR  e.end is null)) 
                        OR
                            (e.start >= :start4 AND e.end <= :end4)
                        OR
                            (e.start <= :start5 AND e.end >= :end5) )";


            $criteriaString = implode(' AND ', $whereCriteriaItems);

            $sql = '
                  SELECT e.* , ' . $this->getAssocColumnsQuery() . '
                    FROM ' . $this->getTableName() . ' e
                   LEFT JOIN playground a ON e.playground_id = a.id
                    WHERE 1=1 AND ' . $criteriaString;
            $query = \Core\DbQuery::prepare($sql);
            $query->bindParam(':start1', $criteria['from']->format('Y-m-d H:i:s'));
            $query->bindParam(':start2', $criteria['from']->format('Y-m-d H:i:s'));
            $query->bindParam(':start3', $criteria['from']->format('Y-m-d H:i:s'));
            $query->bindParam(':start4', $criteria['from']->format('Y-m-d H:i:s'));
            $query->bindParam(':start5', $criteria['from']->format('Y-m-d H:i:s'));

            $query->bindParam(':end1', $criteria['to']->format('Y-m-d H:i:s'));
            $query->bindParam(':end2', $criteria['to']->format('Y-m-d H:i:s'));
            $query->bindParam(':end3', $criteria['to']->format('Y-m-d H:i:s'));
            $query->bindParam(':end4', $criteria['to']->format('Y-m-d H:i:s'));
            $query->bindParam(':end5', $criteria['to']->format('Y-m-d H:i:s'));



            $data = $query->execute()
                    ->fetchAll(\PDO::FETCH_ASSOC);
            return $data;
        }
    
    
	
        public function findNearestTeamEvents($team_ids)
        {
            $ids = implode(',',$team_ids);
            $data = \Core\DbQuery::prepare('
               SELECT e.id, e.start, e.playground_id, l.name
                FROM team_event e
                LEFT JOIN playground l ON e.playground_id = l.id
                WHERE e.team_id in ('.$ids.')
                ORDER BY e.start
                ')
		->execute()
		->fetchAll(\PDO::FETCH_ASSOC);
		return $data;
        }
        
        
        
        
        public function findBeepTeamEvents($criteria)
    {
        $criteriaString = '';
        $whereCriteriaItems = array('e.notification = 1 and e.notify_termin_type = "beep" AND t.status != "deleted"');

        $whereCriteriaItems[] = " (
                        (e.start <= :start1 AND ( (e.end >= :start2 AND  e.end <= :end1) OR  e.end is null)) 
                    OR 
                        (e.start >= :start3 AND ( (e.end >= :end2 AND  e.start <= :end3) OR  e.end is null)) 
                    OR
                        (e.start >= :start4 AND e.end <= :end4)
                    OR
                        (e.start <= :start5 AND e.end >= :end5) )";


        $criteriaString = implode(' AND ', $whereCriteriaItems);

        $sql = '
              SELECT e.* , ' . $this->getAssocColumnsQuery() . '
                FROM ' . $this->getTableName() . ' e
               LEFT JOIN playground a ON e.playground_id = a.id
               LEFT JOIN team t on e.team_id = t.id
                WHERE 1=1 AND ' . $criteriaString;
        $query = \Core\DbQuery::prepare($sql);
        $query->bindParam(':start1', $criteria['from']->format('Y-m-d H:i:s'));
        $query->bindParam(':start2', $criteria['from']->format('Y-m-d H:i:s'));
        $query->bindParam(':start3', $criteria['from']->format('Y-m-d H:i:s'));
        $query->bindParam(':start4', $criteria['from']->format('Y-m-d H:i:s'));
        $query->bindParam(':start5', $criteria['from']->format('Y-m-d H:i:s'));

        $query->bindParam(':end1', $criteria['to']->format('Y-m-d H:i:s'));
        $query->bindParam(':end2', $criteria['to']->format('Y-m-d H:i:s'));
        $query->bindParam(':end3', $criteria['to']->format('Y-m-d H:i:s'));
        $query->bindParam(':end4', $criteria['to']->format('Y-m-d H:i:s'));
        $query->bindParam(':end5', $criteria['to']->format('Y-m-d H:i:s'));



        $data = $query->execute()
                ->fetchAll(\PDO::FETCH_ASSOC);
        return $data;
    }
        public function findDateNotifyEvents($criteria)
    {
        $criteriaString = '';
        $whereCriteriaItems = array(' DATE_FORMAT(e.notify_termin,"%Y-%m-%d 00:00") = "'. $criteria['from']->format('Y-m-d H:i').'" and e.notify_termin_type = "date"');

        /*
        $whereCriteriaItems[] = " (
                        (e.start <= :start1 AND ( (e.end >= :start2 AND  e.end <= :end1) OR  e.end is null)) 
                    OR 
                        (e.start >= :start3 AND ( (e.end >= :end2 AND  e.start <= :end3) OR  e.end is null)) 
                    OR
                        (e.start >= :start4 AND e.end <= :end4)
                    OR
                        (e.start <= :start5 AND e.end >= :end5) )";

*/
        $criteriaString = implode(' AND ', $whereCriteriaItems);
        
        $sql = '
              SELECT e.* , ' . $this->getAssocColumnsQuery() . '
                FROM ' . $this->getTableName() . ' e
               LEFT JOIN playground a ON e.playground_id = a.id
                WHERE 1=1 AND ' . $criteriaString;
        $query = \Core\DbQuery::prepare($sql);
        $query->bindParam(':start1', $criteria['from']->format('Y-m-d H:i:s'));
        $query->bindParam(':start2', $criteria['from']->format('Y-m-d H:i:s'));
        $query->bindParam(':start3', $criteria['from']->format('Y-m-d H:i:s'));
        $query->bindParam(':start4', $criteria['from']->format('Y-m-d H:i:s'));
        $query->bindParam(':start5', $criteria['from']->format('Y-m-d H:i:s'));

        $query->bindParam(':end1', $criteria['to']->format('Y-m-d H:i:s'));
        $query->bindParam(':end2', $criteria['to']->format('Y-m-d H:i:s'));
        $query->bindParam(':end3', $criteria['to']->format('Y-m-d H:i:s'));
        $query->bindParam(':end4', $criteria['to']->format('Y-m-d H:i:s'));
        $query->bindParam(':end5', $criteria['to']->format('Y-m-d H:i:s'));



        $data = $query->execute()
                ->fetchAll(\PDO::FETCH_ASSOC);
        return $data;
    }

    public function findTeamEvents($team_ids,$criteria)
        {

            $criteriaString = '';
            $whereCriteriaItems = array('(e.team_id in ('. implode(',',$team_ids).') or e.opponent_team_id in ('. implode(',',$team_ids).') )');
            
            
            if(array_key_exists('from', $criteria) and array_key_exists('to', $criteria))
            {
                
                $whereCriteriaItems[] = " (
                        (e.start <= :start1 AND ( (e.end >= :start2 AND  e.end <= :end1) OR  e.end is null)) 
                    OR 
                        (e.start >= :start3 AND ( (e.end >= :end2 AND  e.start <= :end3) OR  e.end is null)) 
                    OR
                        (e.start >= :start4 AND e.end <= :end4)
                    OR
                        (e.start <= :start5 AND e.end >= :end5) )";
            }
           elseif(array_key_exists('from', $criteria)) 
           {
                 $whereCriteriaItems[] = '(e.start <= :from)';
           }
           elseif(array_key_exists('to', $criteria)) 
           {
                 $whereCriteriaItems[] = '(e.end <= :to )';
           }
           
           if(array_key_exists('event_type', $criteria)) 
           {
                 $whereCriteriaItems[] = '(e.event_type = :event_type )';
           }

            $criteriaString = implode(' AND ',$whereCriteriaItems);
            
            $sql = '
              SELECT e.* , '.$this->getAssocColumnsQuery().'
                FROM '.$this->getTableName().' e
               LEFT JOIN playground a ON e.playground_id = a.id
                WHERE 1=1 AND '.$criteriaString;
            $query = \Core\DbQuery::prepare($sql);
            //$query->bindParam(':team_id', implode(',',$team_ids));
            
            
            if(array_key_exists('from', $criteria) and array_key_exists('to', $criteria))
            {
                $query->bindParam(':start1', $criteria['from']->format('Y-m-d H:i:s'));
                $query->bindParam(':start2', $criteria['from']->format('Y-m-d H:i:s'));
                $query->bindParam(':start3', $criteria['from']->format('Y-m-d H:i:s'));
                $query->bindParam(':start4', $criteria['from']->format('Y-m-d H:i:s'));
                $query->bindParam(':start5', $criteria['from']->format('Y-m-d H:i:s'));
                
                $query->bindParam(':end1', $criteria['to']->format('Y-m-d H:i:s'));
                $query->bindParam(':end2', $criteria['to']->format('Y-m-d H:i:s'));
                $query->bindParam(':end3', $criteria['to']->format('Y-m-d H:i:s'));
                $query->bindParam(':end4', $criteria['to']->format('Y-m-d H:i:s'));
                $query->bindParam(':end5', $criteria['to']->format('Y-m-d H:i:s'));
            }
           elseif(array_key_exists('from', $criteria)) 
           {
                  $query->bindParam(':from',  $criteria['from']->format('Y-m-d H:i:s'));
           }
           elseif(array_key_exists('to', $criteria)) 
           {
                 $query->bindParam(':to',  $criteria['from']->format('Y-m-d H:i:s'));
           }
            
           if(array_key_exists('event_type', $criteria)) 
           {
                 $query->bindParam(':event_type',  $criteria['event_type']);
           }

            $data = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
	    return $data;
        }
        
        public function findPlaygroundsEvents($ids)
        {
            $ids = implode(',',$ids);
            $data = \Core\DbQuery::prepare('
               SELECT e.id, e.start, e.team_id, e.event_type, t.name
                FROM team_event e
                LEFT JOIN team t ON e.team_id = t.id
                WHERE e.playground_id in ('.$ids.')
                ORDER BY e.start
                ')
		->execute()

		->fetchAll(\PDO::FETCH_ASSOC);
		return $data;
        }
        
        
        
	public function findBy($query_params = array(),$options = array())
	{

		$where_criteria_items  = array();
		foreach ($query_params as $column => $column_val)
		{
                        if(is_string($column_val))
			{
				$where_criteria_items[] = ' e.'.$column.'=:'.$column.' ';
			}
			else
			{
				$where_criteria_items[] = ' e.'.$column.'=:'.$column.' ';
			}
			
		}
		$where_criteria = ' WHERE '. implode(' AND ',$where_criteria_items);
        
                $sort_criteria = '';
                if(array_key_exists('order',$options))
                {
                    $sort_criteria = ' order by '.$options['order'];
                }
                

                
                $query = \Core\DbQuery::prepare('SELECT e.* , '.$this->getAssocColumnsQuery().'
                FROM '.$this->getTableName().' e
               LEFT JOIN playground a ON e.playground_id = a.id
                '. $where_criteria.$sort_criteria);
		
             
                
		
		foreach($query_params as $column => $value)
		{
			if("" != $value)
			{
				$query->bindParam(':'.$column, $value);
			}
                        else 
                        {
                             $query->bindParam(':'.$column, null);
                        }
                       
		}
		$data = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
		return $data;
	}
        
        public function findLocalityEvents($id)
        {
           $sql = '
               SELECT e.id, e.start, e.end, e.period, e.period_interval,e.monday,e.tuesday, e.wednesday, e.thursday,e.friday,e.saturday, e.sunday, e.playground_id, e.note, e.team_id, l.name
                FROM team_event e
                LEFT JOIN playground l ON e.playground_id = l.id
                WHERE l.locality_id = :locality_id';
            $query = \Core\DbQuery::prepare($sql);
            $query->bindParam(':locality_id', $id);
            
            $data = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
            return $data;
        }
        
        public function findUpcomingEvents($criteria)
        {
            $criteriaString = '';
            $whereCriteriaItems = array();
            
            
            if(array_key_exists('from', $criteria) and array_key_exists('to', $criteria))
            {
                
                $whereCriteriaItems[] = " (
                        (e.start <= :start1 AND ( (e.end >= :start2 AND  e.end <= :end1) OR  e.end is null)) 
                    OR 
                        (e.start >= :start3 AND ( (e.end >= :end2 AND  e.start <= :end3) OR  e.end is null)) 
                    OR
                        (e.start >= :start4 AND e.end <= :end4)
                    OR
                        (e.start <= :start5 AND e.end >= :end5) )";
            }
           elseif(array_key_exists('from', $criteria)) 
           {
                 $whereCriteriaItems[] = '(e.start <= :from)';
           }
           elseif(array_key_exists('to', $criteria)) 
           {
                 $whereCriteriaItems[] = '(e.end <= :to )';
           }
           
           if(array_key_exists('event_type', $criteria)) 
           {
                 $whereCriteriaItems[] = '(e.event_type = :event_type )';
           }

            $criteriaString = implode(' AND ',$whereCriteriaItems);
            
            $sql = '
              SELECT e.* , '.$this->getAssocColumnsQuery().'
                FROM '.$this->getTableName().' e
               LEFT JOIN playground a ON e.playground_id = a.id
                WHERE 1=1 AND '.$criteriaString;
            $query = \Core\DbQuery::prepare($sql);
            //$query->bindParam(':team_id', implode(',',$team_ids));
            
            
            if(array_key_exists('from', $criteria) and array_key_exists('to', $criteria))
            {
                $query->bindParam(':start1', $criteria['from']->format('Y-m-d H:i:s'));
                $query->bindParam(':start2', $criteria['from']->format('Y-m-d H:i:s'));
                $query->bindParam(':start3', $criteria['from']->format('Y-m-d H:i:s'));
                $query->bindParam(':start4', $criteria['from']->format('Y-m-d H:i:s'));
                $query->bindParam(':start5', $criteria['from']->format('Y-m-d H:i:s'));
                
                $query->bindParam(':end1', $criteria['to']->format('Y-m-d H:i:s'));
                $query->bindParam(':end2', $criteria['to']->format('Y-m-d H:i:s'));
                $query->bindParam(':end3', $criteria['to']->format('Y-m-d H:i:s'));
                $query->bindParam(':end4', $criteria['to']->format('Y-m-d H:i:s'));
                $query->bindParam(':end5', $criteria['to']->format('Y-m-d H:i:s'));
            }
           elseif(array_key_exists('from', $criteria)) 
           {
                  $query->bindParam(':from',  $criteria['from']->format('Y-m-d H:i:s'));
           }
           elseif(array_key_exists('to', $criteria)) 
           {
                 $query->bindParam(':to',  $criteria['from']->format('Y-m-d H:i:s'));
           }
            
           if(array_key_exists('event_type', $criteria)) 
           {
                 $query->bindParam(':event_type',  $criteria['event_type']);
           }

            $data = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
	    return $data;
        }
        
       
	
}