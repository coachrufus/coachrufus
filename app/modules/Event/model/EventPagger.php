<?php

namespace Webteamer\Event\Model;

class EventPagger {

    private $events;
    private $limitDate;
    private $nextPageEvent;
    private $prevPageEvent;
    private $endDate;
    private $startDate;
    private $sort = 'normal';

    function getEvents()
    {

        if ('reverse' == $this->getSort())
        {

            $list = array_reverse($this->events);
            array_reverse($this->events);
            return $list;

            //return $this->events;
        }
        else
        {
            return $this->events;
        }
    }

    function setEvents($events)
    {
        $this->events = $events;
    }

    public function getSort()
    {
        return $this->sort;
    }

    public function setSort($sort)
    {
        $this->sort = $sort;
    }

    public function getFirstEventDate($format = 'Y-m-d-H-i')
    {
        $firstEvent = current($this->getEvents());

        if (null == $firstEvent)
        {
            return null;
        }


        return $firstEvent->getCurrentDate()->format($format);
    }

    public function getLastEventDate($format = 'Y-m-d-H-i')
    {
        $lastEvent = end($this->getEvents());
        if (null == $lastEvent)
        {
            return null;
        }
        return $lastEvent->getCurrentDate()->format($format);
    }

    function getEndDate()
    {
        return $this->endDate;
    }

    function getStartDate()
    {
        return $this->startDate;
    }

    function setEndDate($endDate)
    {
        $this->endDate = $endDate;
    }

    function setStartDate($startDate)
    {
        $this->startDate = $startDate;
    }

    function getLimitDate()
    {
        return $this->limitDate;
    }

    function setLimitDate($limitDate)
    {
        $this->limitDate = $limitDate;
    }

    function getNextPageEvent()
    {
        return $this->nextPageEvent;
    }

    function setNextPageEvent($nextPageEvent)
    {
        $this->nextPageEvent = $nextPageEvent;
    }

    public function getPrevPageEvent()
    {
        return $this->prevPageEvent;
    }

    public function setPrevPageEvent($prevPageEvent)
    {
        $this->prevPageEvent = $prevPageEvent;
    }

    public function getEvent($index)
    {
        return $this->events[$index];
    }
    
    public function setEvent($index,$event)
    {
        $this->events[$index] = $event;
    }
}
