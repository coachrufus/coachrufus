<?php
namespace Webteamer\Event\Model;

use Core\Manager;
use Core\ServiceLayer;
use DateTime;



class EventManager extends Manager
{ 
    private $eventAttendanceRepository;
    private $eventCostsRepository;
    
    public function __construct($repository = null,$eventAttendanceRepository = null, $eventCostsRepository = null)
    {
        if(null != $repository)
        {
            $this->setRepository($repository);
        }
        
        if(null != $eventAttendanceRepository)
        {
            $this->setEventAttendanceRepository($eventAttendanceRepository);
        }
        
        if(null != $eventCostsRepository)
        {
            $this->setEventCostsRepository($eventCostsRepository);
        }
    }
    
    public function getEventAttendanceRepository()
    {
        return $this->eventAttendanceRepository;
    }

    public function setEventAttendanceRepository($eventAttendanceRepository)
    {
        $this->eventAttendanceRepository = $eventAttendanceRepository;
    }
    
    public function getEventCostsRepository()
    {
        return $this->eventCostsRepository;
    }

    public function setEventCostsRepository($eventCostsRepository)
    {
        $this->eventCostsRepository = $eventCostsRepository;
    }

    public function getEventTypeEnum()
    {
        $translator = ServiceLayer::getService('translator');
        $enum = array(
            'training' => $translator->translate('Training'),
            'game' => $translator->translate('Game'),
            'race' => $translator->translate('Race'),
            'competition' => $translator->translate('Competition'),
             'social' => $translator->translate('Social event type'));
        return $enum;
    }
    
    public function getEventPeriodEnum()
    {
        $translator = ServiceLayer::getService('translator');
        $enum = array(
            'none' => $translator->translate('Not repeat'),
            'daily' => $translator->translate('Daily'),
             'weekly' => $translator->translate('Weekly'),
            'monthly' => $translator->translate('Monthly'),
            'year' => $translator->translate('Yearly'),
            
           
            
                );
        return $enum;
    }
    
    public function getEventPeriodIntervalEnum()
    {
        $translator = ServiceLayer::getService('translator');
        for ($i=1;$i<=30;$i++)
        {
            $enum[$i] = $i;
        }
       
        return $enum;
    }
    
    public function getEventRepeatDaysEnum()
    {
        $translator = ServiceLayer::getService('translator');
        $enum = array(
            '1' => $translator->translate('Monday'),
            '2' => $translator->translate('Tuesday'),
            '3' => $translator->translate('Wednesday'),
            '4' => $translator->translate('Thursday'),
            '5' => $translator->translate('Friday'),
            '6' => $translator->translate('Saturday'),
            '7' => $translator->translate('Sunday'),
            );
        return $enum;
    }

    public function getEventTypFormChoices()
    {
        return $this->getEventTypeEnum();
    }
    
    public function getEventPeriodFormChoices()
    {
        return $this->getEventPeriodEnum();
    }
    
    public function getEventPeriodIntervalFormChoices()
    {
        return $this->getEventPeriodIntervalEnum();
    }
    
    public function getEventRepeatDaysFormChoices()
    {
        return $this->getEventRepeatDaysEnum();
    }
    
   
    
    public function getPlayersAttendance($players = array())
    {
        $attendance = array();
        $playerIds = array();
        foreach($players as $player)
        {
            $playerIds[] = $player->getId();
        }

        $list = $this->getEventAttendanceRepository()->findByParticipants($playerIds);
        return $list;
    }
    
    public function getAttendanceEventMatrix($players,$events,$user)
    {
        $matrix = array();
        $guestMatrix = array();
        $playersMatrix = array();
        $existAttendance = $this->getPlayersAttendanceEventMatrix($players);


        
        foreach($players as $player)
        {
            //$matrix[$player->getId()]['player'] = $player->getName();
            
            if('guest' == $player->getTeamRole())
            {
                 $guestMatrix[$player->getId()]['player'] = $player->getName();
            }
            else
            {
                $playersMatrix[$player->getId()]['player'] = $player->getName();
            }
                     
            foreach($events as $event)
            {
                $status= 'NO';
                $comment = '';
                $editable = false;
                $createdAt = null;
                
                if(array_key_exists($event->getId(), $existAttendance) && array_key_exists($player->getId(), $existAttendance[$event->getId()]))
                {
                    $status = 'YES';
                    $comment = $existAttendance[$event->getId()][$player->getId()]->getComment();
                    $createdAt = $existAttendance[$event->getId()][$player->getId()]->getCreatedAt()->format('d.m.Y H:i:s');
                }
               
              
                 
                 if($user->getId() == $player->getId())
                 {
                     $editable = true;
                 }
                 
                 if($user->getRole() == 'TEAM_MANAGER')
                 {
                     $editable = true;
                 }

                if('guest' == $player->getTeamRole())
                {
                    $guestMatrix[$player->getId()]['events'][$event->getId()]['status'] = $status; 
                    $guestMatrix[$player->getId()]['events'][$event->getId()]['editable'] = $editable; 
                    $guestMatrix[$player->getId()]['events'][$event->getId()]['comment'] = $comment; 
                    $guestMatrix[$player->getId()]['events'][$event->getId()]['created_at'] = $createdAt; 
                }
                else
                {
                    $playersMatrix[$player->getId()]['events'][$event->getId()]['status'] = $status; 
                    $playersMatrix[$player->getId()]['events'][$event->getId()]['editable'] = $editable; 
                    $playersMatrix[$player->getId()]['events'][$event->getId()]['comment'] = $comment; 
                    $playersMatrix[$player->getId()]['events'][$event->getId()]['created_at'] = $createdAt; 
                }
                
                /*
                $matrix[$player->getId()]['events'][$event->getId()]['status'] = $status; 
                $matrix[$player->getId()]['events'][$event->getId()]['editable'] = $editable; 
                $matrix[$player->getId()]['events'][$event->getId()]['comment'] = $comment; 
                */
            }
        }
        

        
        return array('players' => $playersMatrix, 'guest' => $guestMatrix);
    }
    
    public function getPlayersAttendanceEventMatrix($players = array())
    {
        $list = $this->getPlayersAttendance($players);
        $matrix = array();
        foreach($list as $atttendance)
        {
            //$matrix[$atttendance->getEventId()][] = array('participant_id' => $atttendance->getParticipantId(),'comment' => $atttendance->getComment());
            //$matrix[$atttendance->getEventId()]['participant_ids'][$atttendance->getId()] = $atttendance->getParticipantId();
            //$matrix[$atttendance->getEventId()]['attendance'][$atttendance->getId()] = $atttendance;
            //$matrix[$atttendance->getEventId()]['comments'][] = $atttendance->getParticipantId();
            $matrix[$atttendance->getEventId()][$atttendance->getParticipantId()] = $atttendance;
        }
        
        return $matrix;
    }
    
    public function findEvent($eventId)
    {
        return $this->getRepository()->find($eventId);
    }
    
    public function getPlaygroundsEvents($playgrounds)
    {
        $ids = array();
        foreach($playgrounds as $playground)
        {
            $ids[] = $playground->getId();
        }
        $events  = $this->getRepository()->findNearestPlaygroundsEvents($ids);
        return $events;
    }
    
    public function getTeamEvents($team, $filterCriteria = array())
    {
        $criteria = array();
        if(null != $filterCriteria)
        {
            if(array_key_exists('from', $filterCriteria))
            {
                $criteria['from'] =  DateTime::createFromFormat('d.m.Y', $filterCriteria['from']);
            }

            if(array_key_exists('to', $filterCriteria))
            {
                $criteria['to'] =  DateTime::createFromFormat('d.m.Y', $filterCriteria['to']);
            }

            if(array_key_exists('event_type', $filterCriteria))
            {
                $criteria['event_type'] =  $filterCriteria['event_type'];
            }
        
        }
        
        
        $events  = $this->getRepository()->findTeamEvents($team, $criteria);
         return $events;
    }
    
    
    
    public function getLocalityEvents($locality,$criteria = null) 
    {
        $events  = $this->getRepository()->findLocalityEvents($locality,$criteria);
        return  $events;
    }
    
   
}