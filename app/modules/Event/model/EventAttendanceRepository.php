<?php
namespace Webteamer\Event\Model;
use Core\Repository as Repository;




class EventAttendanceRepository extends Repository
{
    public function deleteByParams($params)
    {
        $this->storage->deleteByParams($params);
    }
    
    public function findByParticipants($playerIds)
    {
         $list = array();
            if(!empty($playerIds))
         {
             $data = $this->storage->findByParticipants($playerIds);
             $list = $this->createObjectList($data);
         
         }
         return $list;
    }
    
    public function getByParticipantAndEvents($player,$eventList)
    {
        $list = array();
        
        $eventCriteria = array();
        foreach($eventList as $dayEvents)
        {
            foreach($dayEvents as $event)
            {
                $eventCriteria[] = array('event_date' => $event->getCurrentDate()->format('Y-m-d'),'event_id' => $event->getId());
            }
        }
            
        $data = $this->storage->findByParticipantAndEvents($player->getId(),$eventCriteria);
        $list = $this->createObjectList($data);
        return $list;
    }
    
     public function findByEvents($eventsIds)
    {
         $list = array();
         if(!empty($eventsIds))
         {
             $data = $this->storage->findByEvents($eventsIds);
             $list = $this->createObjectList($data);
         
         }
         return $list;
    }
    
    
}
