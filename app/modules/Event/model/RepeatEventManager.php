<?php
namespace Webteamer\Event\Model;

class RepeatEventManager 
{
    public function getDayDiff($event,$terminDate)
    {
        $startDay = $event->getStart();
        $dayDiffInterval = $startDay->diff($terminDate);
        $dayDiff = intval($dayDiffInterval->format('%R%a'));
        return $dayDiff;
    }
    
    public function getSecondsDiff($event,$terminDate)
    {
        $startDay = $event->getStart();
        $dayDiffInterval = $startDay->diff($terminDate);
        $dayDiff = intval($dayDiffInterval->format('%s%a'));
        return $dayDiff;
    }
    
    
    
    /**
     * Return finished cycles
     * @param type $event
     * @param type $terminDate
     * @return type
     */
    public function getEventFinishedCycles($event,$terminDate)
    {
        $dayDiff = $this->getDayDiff($event, $terminDate);
        $periodUnit = $event->getPeriodInterval();
        $cyclesModulo = fmod($dayDiff,$periodUnit);
        $completePastCycles = floor($dayDiff/$periodUnit);
        
        if($cyclesModulo !=0 && $completePastCycles>0)
        {
            $completePastCycles = $completePastCycles+1;
        }

        //condition when termin hour is bigger then hour of  the event 
        if($terminDate->getTimestamp() >=  $event->getStart()->getTimestamp())
        {
            $completePastCycles = $completePastCycles + 1;
        }
      
        return $completePastCycles;
    }
    
    public function isCycleDay($event,$terminDate)
    {
        //set at the end of the day, beacause we check DAY not TIME
        $terminDate->setTime(23,59,59);
        
        $dayDiff = $this->getDayDiff($event, $terminDate);
        $periodUnit = $event->getPeriodInterval();
        $cyclesModulo = fmod($dayDiff,$periodUnit);
 
        
        if($cyclesModulo == 0)
        {
            return true;
        }
        
        return false;
    }
    
    public function isOmittedTermin($event,$terminDate)
    {
       $omittedTermins = explode(',', $event->getOmittedTermins());
        $isOmitted = false;
        if (null != $omittedTermins)
        {
            foreach ($omittedTermins as $omittedTerminData)
            {
                $omittedTermin = unserialize($omittedTerminData);
                if (null != $omittedTermin)
                {
                    if ($omittedTermin->format('Y-m-d') == $terminDate->format('Y-m-d'))
                    {
                        $isOmitted = true;
                    }
                }
            }
        }
        
        return $isOmitted;
    }
    
    public function teminInDateRange($event,$terminDate)
    {

        $startDay = $event->getStart();
        $startDayStamp = $startDay->getTimestamp();
        $endDay = $event->getEnd();
        if($terminDate->getTimestamp() >= $startDayStamp && ($endDay == null or $endDay->getTimestamp() >= $terminDate->getTimestamp()))
        {
            return true;
        }
        return false;
        
    }

}

