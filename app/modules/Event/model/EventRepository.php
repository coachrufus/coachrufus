<?php

namespace Webteamer\Event\Model;

use Core\Repository as Repository;

class EventRepository extends Repository {

    public function findNearestTeamEvents($team_ids)
    {
        $events = array();
        if (!empty($team_ids))
        {
            $data = $this->getStorage()->findNearestTeamEvents($team_ids);
            $events = $this->createObjectList($data);
        }
        return $events;
    }
    
    public function findUpcomingEvents($criteria)
    {
        $data = $this->getStorage()->findUpcomingEvents($criteria);
        $events = $this->createObjectList($data);
        return $events;
    }
    
    
    
    public function findTeamEvents($teams,$criteria = array())
    {
        $events = array();
        
         if(!is_array($teams))
        {
            $teams = array($teams);
        }
        $ids = array();
        foreach($teams as $team)
        {
            $ids[] = $team->getId();
        }
        
        
        $data = $this->getStorage()->findTeamEvents($ids,$criteria);
        $events = $this->createObjectList($data);

            
        return $events;
    }
    
     public function findNotifyTeamEvents($criteria = array())
    {
        $data = $this->getStorage()->findNotifyTeamEvents($criteria);
        $events = $this->createObjectList($data);
        return $events;
    }
    
    public function findBeepEvents($criteria = array())
    {
        $data = $this->getStorage()->findBeepTeamEvents($criteria);
        $events = $this->createObjectList($data);
        return $events;
    }

    public function findDateNotifyEvents($criteria = array())
    {
        $data = $this->getStorage()->findDateNotifyEvents($criteria);
        $events = $this->createObjectList($data);
        return $events;
    }


    public function findNearestPlaygroundsEvents($ids)
    {
        $events = array();
        if (!empty($ids))
        {
            $data = $this->getStorage()->findPlaygroundsEvents($ids);
            $events = $this->createObjectList($data);
        }
        return $events;
    }
    
      public function findLocalityEvents($locality,$criteria)
    {
        $events = array();
        $data = $this->getStorage()->findLocalityEvents($locality->getId(),$criteria);
        $events = $this->createObjectList($data);

            
        return $events;
    }
    
   

  

}
