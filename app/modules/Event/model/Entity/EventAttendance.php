<?php
	
namespace Webteamer\Event\Model;
use Core\EntityMapper as EntityMapper;
use Core\ServiceLayer as ServiceLayer;
		
class EventAttendance {	

	
	
		
private $repository;
private $mapper_rules  = array(
	'id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => false
							
					),
'participant_id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => false
							
					),
'team_player_id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => false
							
					),
'event_id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => false
							
					),
'event_date' => array(
					'type' => 'datetime',
					'max_length' => '',
					'required' => false
							
					),
'status' => array(
					'type' => 'string',
					'max_length' => '50',
					'required' => false
							
					),
'fee' => array(
                                    'type' => 'float',
                                    'max_length' => '11',
                                    'required' => false

                                    ),
'participant_type' => array(
					'type' => 'string',
					'max_length' => '50',
					'required' => false
							
					),
'comment' => array(
					'type' => 'string',
					'max_length' => '255',
					'required' => false
							
					),
'created_at' => array(
					'type' => 'datetime',
					'required' => false
							
					),

);
	
public function getDataMapperRules()
{
	return $this->mapper_rules;
}


			

		
		
		
protected $id;

protected $participantId;
protected $teamPlayerId;

protected $eventId;

protected $eventDate;

protected $status;
protected $fee;

protected $participantType;
protected $comment;
protected $createdAt;
protected $player;
protected $teamPlayer;

				
public function setId($val){$this->id = $val;}

public function getId(){return $this->id;}
	
public function setParticipantId($val){$this->participantId = $val;}

public function getParticipantId(){return $this->participantId;}
	
public function setEventId($val){$this->eventId = $val;}

public function getEventId(){return $this->eventId;}
	
public function setEventDate($val){$this->eventDate = $val;}

public function getEventDate(){return $this->eventDate;}
	
public function setStatus($val){$this->status = $val;}

public function getStatus(){return $this->status;}
	
public function setParticipantType($val){$this->participantType = $val;}

public function getParticipantType(){return $this->participantType;}

public  function getComment() {
return $this->comment;
}

public  function setComment($comment) {
$this->comment = $comment;
}

public  function getCreatedAt() {
return $this->createdAt;
}

public  function setCreatedAt($createdAt) {
$this->createdAt = $createdAt;
}

public  function getPlayer() {
return $this->player;
}

public  function setPlayer($player) {
$this->player = $player;
}



public  function getTeamPlayerId() {
return $this->teamPlayerId;
}

public  function setTeamPlayerId($teamPlayerId) {
$this->teamPlayerId = $teamPlayerId;
}

public  function getTeamPlayer() {
return $this->teamPlayer;
}

public  function setTeamPlayer($teamPlayer) {
$this->teamPlayer = $teamPlayer;
}

function getFee() {
    return $this->fee;
}

function setFee($fee) {
    $this->fee = $fee;
}







}

		