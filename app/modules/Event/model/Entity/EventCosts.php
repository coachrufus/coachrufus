<?php
	
namespace Webteamer\Event\Model;
use Core\EntityMapper as EntityMapper;
use Core\ServiceLayer as ServiceLayer;
		
class EventCosts {	

private $repository;
private $mapper_rules  = array(
'id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => false
							
					),
'event_id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => false
							
					),
'event_date' => array(
					'type' => 'datetime',
					'max_length' => '',
					'required' => false
							
					),
'costs' => array(
                                    'type' => 'float',
                                    'max_length' => '11',
                                    'required' => false

                                    ),
'created_at' => array(
					'type' => 'datetime',
					'required' => false
							
					),

);
	
public function getDataMapperRules()
{
	return $this->mapper_rules;
}

protected $id;
protected $eventId;
protected $eventDate;
protected $costs;
protected $createdAt;
				
public function setId($val){$this->id = $val;}

public function getId(){return $this->id;}
	
public function setEventId($val){$this->eventId = $val;}

public function getEventId(){return $this->eventId;}
	
public function setEventDate($val){$this->eventDate = $val;}

public function getEventDate(){return $this->eventDate;}

public function getCosts() {return $this->costs;}

public function setCosts($costs) {$this->costs = $costs;}
	
public  function getCreatedAt() {return $this->createdAt;}

public  function setCreatedAt($createdAt) {$this->createdAt = $createdAt;}

}		