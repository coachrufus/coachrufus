<?php

namespace Webteamer\Event\Model;

use Core\EntityMapper as EntityMapper;
use Core\ServiceLayer as ServiceLayer;

class Event {

    private $repository;
    private $mapper_rules = array(
        'id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'name' => array(
            'type' => 'string',
            'required' => false
        ),
        'season' => array(
            'type' => 'string',
            'required' => false
        ),
        'period' => array(
            'type' => 'string',
            'max_length' => '50',
            'required' => false
        ),
        'period_interval' => array(
            'type' => 'string',
            'max_length' => '50',
            'required' => false
        ),
        'monday' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'tuesday' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'wednesday' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'thursday' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'friday' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'saturday' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'sunday' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'start' => array(
            'type' => 'datetime',
            'max_length' => '',
            'required' => false
        ),
        'end_time' => array(
            'type' => 'datetime',
            'max_length' => '',
            'required' => false
        ),
        'end' => array(
            'type' => 'datetime',
            'max_length' => '',
            'required' => false
        ),
        'end_period' => array(
            'type' => 'string',
            'max_length' => '50',
            'required' => false
        ),
        'end_period_type' => array(
            'type' => 'string',
            'max_length' => '50',
            'required' => false
        ),
        'team_id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'event_type' => array(
            'type' => 'string',
            'max_length' => '50',
            'required' => false
        ),
        'opponent_team_id' => array(
            'type' => 'int',
            'max_length' => '10',
            'required' => false
        ),
        'playground_id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'created_at' => array(
            'type' => 'datetime',
            'required' => false
        ),
        'author_id' => array(
            'type' => 'int',
            'required' => false
        ),
        'capacity' => array(
            'type' => 'int',
            'required' => false
        ),
        'note' => array(
            'type' => 'text',
            'required' => false
        ),
        'notification' => array(
            'type' => 'int',
            'required' => false
        ),
        'notify_termin' => array(
            'type' => 'datetime',
            'required' => false
        ),
        'notify_termin_type' => array(
            'type' => 'string',
            'required' => false
        ),
        'notify_group' => array(
            'type' => 'string',
            'required' => false
        ),
        'omitted_termins' => array(
            'type' => 'string',
            'max_length' => '1000',
            'required' => false
        ),
        'season' => array(
            'type' => 'string',
            'max_length' => '1000',
            'required' => false
        ),
        'notify_beep_days' => array(
            'type' => 'int',
            'required' => false
        ),
         'playground_data' => array(
            'type' => 'string',
             'max_length' => '1000',
            'required' => false
        ),
        'repeat_days' => array(
            'type' => 'non-persist',
            'required' => false
        ),
       
        'tournament_round' => array(
            'type' => 'int',
            'max_length' => '1000',
            'required' => false
        ),
        'tournament_type' => array(
            'type' => 'string',
            'max_length' => '1000',
            'required' => false
        ),
        'tournament_group' => array(
            'type' => 'string',
            'max_length' => '1000',
            'required' => false
        ),
        'tournament_id' => array(
            'type' => 'string',
            'max_length' => '1000',
            'required' => false
        ),
        'tournament_playoff_parent_event1' => array(
            'type' => 'int',
            'max_length' => '1000',
            'required' => false
        ),
        'tournament_playoff_parent_event2' => array(
            'type' => 'int',
            'max_length' => '1000',
            'required' => false
        ),
        'tournament_target_event' => array(
            'type' => 'int',
            'max_length' => '1000',
            'required' => false
        ),
        
    );

    public function getDataMapperRules()
    {
        return $this->mapper_rules;
    }

    protected $id;
    protected $name;
    protected $period;
    protected $periodInterval;
    protected $monday;
    protected $tuesday;
    protected $wednesday;
    protected $thursday;
    protected $friday;
    protected $saturday;
    protected $sunday;
    protected $start;
    protected $endTime;
    protected $end;
    protected $endPeriod;
    protected $endPeriodType;
    protected $teamId;
    protected $eventType;
    protected $playgroundId;
    protected $createdAt;
    protected $authorId;
    protected $repeatDays = array();
    protected $playground;
    protected $playgroundTeamAlias;
    protected $team;
    protected $attendance;
    protected $note;
    protected $capacity;
    protected $notification;
    protected $notifyTermin;
    protected $notifyBeepDays;
    protected $notifyGroup;
    protected $notifyTerminType;
    protected $currentDate;
    protected $typeName;
    protected $sportName;
    protected $seasonName;
    protected $season;
    protected $omittedTermins;
    protected $playgroundData;
    protected $statRoute;
    protected $matchOverview;
    protected $matchResult = '0:0';
    protected $closed = false;
    protected $lineup;
    protected $tournamentRound;
    protected $tournamentType;
    protected $opponentTeamId;
    protected $tournamentGroup;
    protected $tournamentId;
    protected $tournamentPlayoffParentEvent1;
    protected $tournamentPlayoffParentEvent2;
    protected $tournamentTargetEvent;
    
    public function getPlaygroundData()
    {
        return $this->playgroundData;
    }
    
    public function getPlaygroundDataAsArray()
    {
        return unserialize($this->playgroundData);
    }
    

    public function setPlaygroundData($playgroundData)
    {
        $this->playgroundData = $playgroundData;
    }

    public function setId($val)
    {
        $this->id = $val;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function setPeriod($val)
    {
        $this->period = $val;
    }

    public function getPeriod()
    {
        return $this->period;
    }

    public function setPeriodInterval($val)
    {
        $this->periodInterval = $val;
    }

    public function getPeriodInterval()
    {
        return $this->periodInterval;
    }

    public function setMonday($val)
    {
        $this->monday = $val;
    }

    public function getMonday()
    {
        return $this->monday;
    }

    public function setTuesday($val)
    {
        $this->tuesday = $val;
    }

    public function getTuesday()
    {
        return $this->tuesday;
    }

    public function setWednesday($val)
    {
        $this->wednesday = $val;
    }

    public function getWednesday()
    {
        return $this->wednesday;
    }

    public function setThursday($val)
    {
        $this->thursday = $val;
    }

    public function getThursday()
    {
        return $this->thursday;
    }

    public function setFriday($val)
    {
        $this->friday = $val;
    }

    public function getFriday()
    {
        return $this->friday;
    }

    public function setSaturday($val)
    {
        $this->saturday = $val;
    }

    public function getSaturday()
    {
        return $this->saturday;
    }

    public function setSunday($val)
    {
        $this->sunday = $val;
    }

    public function getSunday()
    {
        return $this->sunday;
    }

    public function setStart($val)
    {
        $this->start = $val;
    }

    public function getStart()
    {
        return $this->start;
    }

    public function setEnd($val)
    {
        $this->end = $val;
    }

    public function getEnd()
    {
        return $this->end;
    }

    public function setEndPeriod($val)
    {
        $this->endPeriod = $val;
    }

    public function getEndPeriod()
    {
        return $this->endPeriod;
    }

    public function setTeamId($val)
    {
        $this->teamId = $val;
    }

    public function getTeamId()
    {
        return $this->teamId;
    }

    public function setEventType($val)
    {
        $this->eventType = $val;
    }

    public function getEventType()
    {
        return $this->eventType;
    }

    public function getNote()
    {
        return $this->note;
    }

    public function getCapacity()
    {
        return $this->capacity;
    }

    public function setNote($note)
    {
        $this->note = $note;
    }

    public function setCapacity($capacity)
    {
        $this->capacity = $capacity;
    }

    public function getPlaygroundId()
    {
        return $this->playgroundId;
    }

    public function setPlaygroundId($playgroundId)
    {
        $this->playgroundId = $playgroundId;
    }

    function getCreatedAt()
    {
        return $this->createdAt;
    }

    function getAuthorId()
    {
        return $this->authorId;
    }

    function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    function setAuthorId($authorId)
    {
        $this->authorId = $authorId;
    }

    public function getRepeatDays()
    {
        return $this->repeatDays;
    }

    public function setRepeatDays($val)
    {
        $this->repeatDays = $val;
    }

    function getEndPeriodType()
    {
        return $this->endPeriodType;
    }

    function setEndPeriodType($endPeriodType)
    {
        $this->endPeriodType = $endPeriodType;
    }

    public function setPlayground($val)
    {
        $this->playground = $val;
    }

    public function getPlayground()
    {
        
        if (null == $this->playground)
        {
            $this->playground = \Core\ServiceLayer::getService('PlaygroundRepository')->find($this->getPlaygroundId());
        }

        return $this->playground;
    }
    
     public function getPlaygroundTeamAlias()
    {
        if (null == $this->playgroundTeamAlias)
        {
            $this->playgroundTeamAlias = \Core\ServiceLayer::getService('PlaygroundAliasRepository')->findOneBy(array('playground_id' => $this->getPlaygroundId(),'team_id' => $this->getTeamId()));
        }

        return $this->playgroundTeamAlias;
    }

    public function getPlaygroundName()
    {
        
        $playgroundAlias = $this->getPlaygroundTeamAlias();
        if (null != $playgroundAlias)
        {
            return $playgroundAlias->getName();
        }

        $playgroundData = $this->getPlaygroundDataAsArray();
        if(null != $playgroundData)
        {
            return $playgroundData['name'];
        }
    }
    
    
     public function getPlaygroundAddress()
    {
        $playgroundData = $this->getPlaygroundDataAsArray();
        if(null != $playgroundData)
        {
            return $playgroundData['street'].' '.$playgroundData['street_number'].', '.$playgroundData['city'];
        }
    }
    
    public function setTeam($val)
    {
        $this->team = $val;
    }

            

    public function getTeam()
    {
        if (null == $this->team)
        {
            $this->team = \Core\ServiceLayer::getService('TeamRepository')->find($this->getTeamId());
        }

        return $this->team;
    }

    public function getTeamName()
    {
        return $this->getTeam()->getName();
    }

    function getAttendance()
    {
        return $this->attendance;
    }

    function setAttendance($attendance)
    {
        $this->attendance = $attendance;
    }

    public function getLocalityCapacity()
    {
        return ($this->getCapacity() > 0) ? $this->getCapacity() : '10';
    }

    public function getParticipantCount()
    {
        $attendance = $this->getAttendance();
        return count($attendance);
    }

    public function getPeriodDays()
    {
        
    }

    public function offsetStart($offset)
    {
        $start = clone $this->getStart();
        $start->modify($offset);
        return $start;
    }

    public function getNotifyTermin()
    {
        return $this->notifyTermin;
    }

    public function getNotifyGroup()
    {
        return $this->notifyGroup;
    }

    public function setNotifyTermin($notifyTermin)
    {
        $this->notifyTermin = $notifyTermin;
    }

    public function setNotifyGroup($notifyGroup)
    {
        $this->notifyGroup = $notifyGroup;
    }

    public function getNotification()
    {
        return $this->notification;
    }

    public function setNotification($notification)
    {
        $this->notification = $notification;
    }

    public function getNotifyTerminType()
    {
        return $this->notifyTerminType;
    }

    public function setNotifyTerminType($notifyTerminType)
    {
        $this->notifyTerminType = $notifyTerminType;
    }

    public function getCurrentDate()
    {
        return $this->currentDate;
    }

    public function setCurrentDate($currentDate)
    {
        $this->currentDate = $currentDate;
    }

    public function getFormatedCurrentDate()
    {
        
        return $this->getCurrentDate()->format('d.m.Y').' '.$this->getStart()->format('H:i');
    }
    
    //public funciton getFormatedCurrentDate
    
    public  function getTypeName() {
return $this->typeName;
}

public  function setTypeName($typeName) {
$this->typeName = $typeName;
}

public  function getOmittedTermins() {
return $this->omittedTermins;
}

public  function setOmittedTermins($omittedTermins) {
$this->omittedTermins = $omittedTermins;
}




public  function getSeason() {
return $this->season;
}

public  function setSeason($season) {
$this->season = $season;
}


public  function getNotifyBeepDays() {
return $this->notifyBeepDays;
}

public  function setNotifyBeepDays($notifyBeepDays) {
$this->notifyBeepDays = $notifyBeepDays;
}

public  function getEndTime() {
return $this->endTime;
}

public  function setEndTime($endTime) {
$this->endTime = $endTime;
}



public  function getClosed() {
return $this->closed;
}

public  function setClosed($closed) {
$this->closed = $closed;
}


function getTournamentRound() {
return $this->tournamentRound;
}

 function getTournamentType() {
return $this->tournamentType;
}

 function setTournamentRound($tournamentRound) {
$this->tournamentRound = $tournamentRound;
}

 function setTournamentType($tournamentType) {
$this->tournamentType = $tournamentType;
}

function getOpponentTeamId() {
return $this->opponentTeamId;
}

 function setOpponentTeamId($opponentTeamId) {
$this->opponentTeamId = $opponentTeamId;
}


function getTournamentGroup() {
return $this->tournamentGroup;
}

 function setTournamentGroup($tournamentGroup) {
$this->tournamentGroup = $tournamentGroup;
}


function getTournamentId() {
return $this->tournamentId;
}

 function setTournamentId($tournamentId) {
$this->tournamentId = $tournamentId;
}





public function getAttendanceInSum()
{
    $attendance = $this->getAttendance();
    $sum = 0;
    foreach($attendance as $a)
    {
        if(1 == $a->getStatus())
        {
            $sum++;
        }
    }
    
    return $sum;
}


public function getAttendanceOutSum()
{
    $attendance = $this->getAttendance();
    $sum = 0;
    foreach($attendance as $a)
    {
        if(3 == $a->getStatus())
        {
            $sum++;
        }
    }
    
    return $sum;
}




    public function getWeeklyPeriodDays()
    {
        $days = array();
        if ('1' == $this->getMonday())
        {
            $days[1] = 1;
        }
        if ('1' == $this->getTuesday())
        {
            $days[2] = 2;
        }
        if ('1' == $this->getWednesday())
        {
            $days[3] = 3;
        }
        if ('1' == $this->getThursday())
        {
            $days[4] = 4;
        }
        if ('1' == $this->getFriday())
        {
            $days[5] = 5;
        }
        if ('1' == $this->getSaturday())
        {
            $days[6] = 6;
        }
        if ('1' == $this->getSunday())
        {
            $days[7] = 7;
        }

        return $days;
    }
    
    public function isOmittedTermin($termin)
    {
        $omittedTermins = explode(',',$this->getOmittedTermins());
        $isOmitted = false;
        if(null != $omittedTermins)
        {
            foreach($omittedTermins as $omittedTerminData)
            {
                $omittedTermin = unserialize($omittedTerminData);
                if(null !=$omittedTermin )
                {
                    if($omittedTermin->format('Y-m-d') == $termin->format('Y-m-d'))
                    {
                        $isOmitted = true;
                    }
                }

            }
        }
        
        return $isOmitted;
    }
    
    /**
     * get nearest event termin
     * @param type $date
     * @return \DateTime
     */
    public function getNearestTermin($date)
    {
        
        $dateDayIndex = $date->format('N');
        $startDay = $this->getStart();
        //$startTime - $event->getStart()->format('H:i:s');
        $endDay = $this->getEnd();
        $startDayStamp = $startDay->getTimestamp();
        
        if($date->format('m-d-Y') == $startDay->format('m-d-Y'))
        {
            if(!$this->isOmittedTermin($date))
            {
                return $date;
            }
            
        }

        if ('weekly' == $this->getPeriod())
        {
            $weekly_days = $this->getWeeklyPeriodDays();
            foreach ($weekly_days as $weekly_day)
            {
                if ($weekly_day >= $dateDayIndex)
                {
                    $diff = $weekly_day - $dateDayIndex;
                    //$termin =  new \DateTime($date->format('Y-m-').($date->format('d')+$diff));

                    $termin = new \DateTime();
                    
                    //var_dump($date);
                    
                    //var_dump($startDay->format('s')+(60*$startDay->format('i'))+(60*60*$startDay->format('H')));
                    $terminStamp = mktime($startDay->format('H'), $startDay->format('i'), $startDay->format('s'), $date->format('m'), $date->format('d'), $date->format('Y'));
                    //$terminStamp =  $date->getTimestamp()+$startDay->format('s')+(60*$startDay->format('i'))+(60*60*$startDay->format('H'));
                    $termin->setTimestamp(strtotime("+" . ($diff) . " day", $terminStamp));
                    
                    //check period repeat
                    $period = $this->getPeriodInterval();
                    if(null == $period)
                    {
                        $period = 1;
                    }
                    $periodUnit = 7*$period;
                    $weekDiff  = $startDay->diff($termin)->days;
                    $initstartDayDif = $startDay->format('N')-$weekly_day;

                    $startDayDif = $weekDiff+$initstartDayDif;
                    $completePastCycles = $startDayDif/$periodUnit;
                    
                    
                    if($period > 1)
                    {
                        $completePastCyclesModulo = fmod($startDayDif,$periodUnit);
                    }
                    else
                    {
                        $completePastCyclesModulo = 0;
                    }
                    
                    
                    $omittedTermins = explode(',',$this->getOmittedTermins());
                    $isOmitted = false;
                    if(null != $omittedTermins)
                    {
                        foreach($omittedTermins as $omittedTerminData)
                        {
                            $omittedTermin = unserialize($omittedTerminData);
                            if(null !=$omittedTermin )
                            {
                                if($omittedTermin->format('Y-m-d') == $termin->format('Y-m-d'))
                                {
                                    $isOmitted = true;
                                }
                            }
                            
                        }
                    }

                    $afterPeriod = false;
                    if($this->getEndPeriodType() == 'after')
                    {
                        
                        if($completePastCycles >= $this->getEndPeriod())
                        {
                            $afterPeriod =true;
                        }
                    }
                
                

                    if (
                            $termin->getTimestamp() >= $startDayStamp && 
                            ($endDay == null or $endDay->getTimestamp() >= $termin->getTimestamp())
                            && $completePastCyclesModulo == 0
                            && $isOmitted == false
                            && $afterPeriod ==false
                           
                    )
                    {
                        return $termin;
                    }
                }
            }
        }

        if ('year' == $this->getPeriod())
        {
            if ($date->format('m-d') == $startDay->format('m-d'))
            {

                if ($date->getTimestamp() > $startDayStamp && ($endDay == null or $endDay->getTimestamp() > $date->getTimestamp()))
                {
                    return $date;
                }
            }
            else
            {
                //zisti rozdiel medzi dnesnym rokom a startovacim rokom
                $yearDiff = $date->format('Y') - $startDay->format('Y');
                //zisti rozdiel medzi opakovanim
                $diff = $this->getPeriodInterval() - $yearDiff;

                $date = new \DateTime(($date->format('Y') + $diff) . '-' . $startDay->format('m-d'));
                if ($date->getTimestamp() > $startDayStamp && ($endDay == null or $endDay->getTimestamp() > $date->getTimestamp()))
                {
                    return $date;
                }
            }
        }

        if ('monthly' == $this->getPeriod())
        {
            //zisti rozdiel mesiacov medzi dnesnym mesiacom a startovacim mesiacom
            //period intervals
            
           

            $monthDiff = $date->diff($startDay);
            $fullMonthDiff = (($monthDiff->format('%y') * 12) + $monthDiff->format('%m'));
            $period = $this->getPeriodInterval();


            //ak je den mesiaca vyssi den konania treba pripocitat mesiac
            
            if (intval($date->format('d')) <= intval($startDay->format('d')))
            {
                $fullMonthDiff = $fullMonthDiff + 1;
            }
            
            $periodIntervals = ceil($fullMonthDiff / $period);
            $addMonths = $periodIntervals * intval($period);

            $date = new \DateTime();
            $date->setTimestamp(strtotime("+" . $addMonths . " months", $startDay->getTimestamp()));
            $afterPeriod = false;
            if($this->getEndPeriodType() == 'after')
            {
                if($periodIntervals >= $this->getEndPeriod())
                {
                    $afterPeriod =true;
                }
            }
            
            if ($date->getTimestamp() >= $startDayStamp && ($endDay == null or $endDay->getTimestamp() > $date->getTimestamp()) && $afterPeriod == false)
            {
                return $date;
            }


            //return $date;
        }
        
        if('daily' == $this->getPeriod())
        {
            
            $terminDate = new \DateTime($date->format('Y-m-d').' '.$startDay->format('H:i:s'));
            $dayDiffInterval = $startDay->diff($terminDate);
            $dayDiff = intval($dayDiffInterval->format('%R%a'));
            $repeatEventManager = ServiceLayer::getService('RepeatEventManager');
            $isOmitted = $repeatEventManager->isOmittedTermin($this,$terminDate);
            $isInDateRange =  $repeatEventManager->teminInDateRange($this,$terminDate);
            
            if(!$isOmitted)
            {
                 //check period
                if('after' == $this->getEndPeriodType())
                {
                    
                    $finishedCycles = $repeatEventManager->getEventFinishedCycles($this,new \DateTime($date->format('Y-m-d').' 23:59:59'));


                    if($finishedCycles <= $this->getEndPeriod() && $repeatEventManager->isCycleDay($this,$terminDate) && $isInDateRange)
                    {
                         $date->setTimestamp(strtotime("+" . $dayDiff . " day", $startDay->getTimestamp()));
                         return $date;
                    }

                }
                elseif('date' == $this->getEndPeriodType())
                {
                    $endDay->setTime(23,59,59);
                    if($endDay->getTimestamp() > $terminDate->getTimestamp() && $isInDateRange && $repeatEventManager->isCycleDay($this,$terminDate))
                    {
                         $date->setTimestamp(strtotime("+" . $dayDiff . " day", $startDay->getTimestamp()));
                         return $date;
                    }
                }
                else
                {
                     if($dayDiff >= 0)
                    {
                        if(fmod($dayDiff,$this->getPeriodInterval()) == 0 && $isInDateRange)
                        {
                             $date->setTimestamp(strtotime("+" . $dayDiff . " day", $startDay->getTimestamp()));
                             return $date;
                        }
                    }
                }
            }
           
        }

        if ('none' == $this->getPeriod())
        {
            return $this->getStart();
        }
    }

    public function getFormatedNearestTermin($date = null)
    {
        if (null == $date)
        {
            $date = new \DateTime();
        }
        $nearestTermin = $this->getNearestTermin($date);
        if (null != $nearestTermin)
        {
            return $nearestTermin->format('d.m.Y');
        }
    }

    public function getIcalStart()
    {
        $date = $this->getStart();
        
        //$eventDate =$date;
        $date->setTimezone(new \DateTimeZone("UTC")); 
        
        //$date->setTimezone( new \DateTimeZone('Europe/Bratislava'));    
        return $date->format('Ymd\THis')."Z";
    }

    public function getIcalEnd($timezone)
    {
        $date = new \DateTime();
        $date->setTimestamp(strtotime("+2 hours 59 seconds", $this->getCurrentDate()->getTimestamp()));
        ///$eventdate->setTimezone(  new \DateTimeZone('Europe/Bratislava') );
        return $date->format('Ymd\THis');
    }
    
    public function getIcalEndPeriodDate()
    {
         $date = $this->getEnd();
         //$date->setTimezone( new \DateTimeZone('Europe/Bratislava'));    
        return $date->format('Ymd\THis');
    }

    public function isPast()
    {
        $today = new \DateTime();
        if($this->getCurrentDate()->getTimestamp() < $today->getTimestamp() )
        {
            return true;
        }
        
        return false;
    }
    
    public function getIcalDays()
    {
        $days = $this->getWeeklyPeriodDays();
        $dayNames = array('','MO','TU','WE','TH','FR','SA','SU');
        $icalDays = array();
        foreach($days as $day)
        {
            $icalDays[] = $dayNames[$day];
        }
        return implode(',',$icalDays);
       
    }
    
    /**
     * Return unique hash for combination event - day
     */
    public function getUid()
    {
        return md5($this->getId().$this->getCurrentDate()->format('Y-m-d'));
    }
    
    public  function getSportName() {
return $this->sportName;
}

public  function getSeasonName() {
return $this->seasonName;
}

public  function setSportName($sportName) {
$this->sportName = $sportName;
}

public  function setSeasonName($seasonName) {
$this->seasonName = $seasonName;
}

public  function getStatRoute() {
return $this->statRoute;
}

public  function setStatRoute($statRoute) {
$this->statRoute = $statRoute;
}


public  function getMatchResult() {
return $this->matchResult;
}

public  function setMatchResult($matchResult) {
$this->matchResult = $matchResult;
}

public  function getMatchOverview() {
return $this->matchOverview;
}

public  function setMatchOverview($matchOverview) {
$this->matchOverview = $matchOverview;
}

public function getTimeInterval($format)
{

    $interval = $this->getStart()->format($format);
    if(null !=  $this->getEndTime())
    {
         $endTime = $this->getEndTime()->format($format);
         $interval = $interval.' - '.$endTime;
    }
    
    return $interval;
}

public  function getLineup() {
return $this->lineup;
}

public  function setLineup($lineup) {
$this->lineup = $lineup;
}

public function isRepeated()
{
    if($this->getPeriod() == 'none')
    {
        return false;
    }
    return true;
}

function getTournamentPlayoffParentEvent1() {
return $this->tournamentPlayoffParentEvent1;
}

 function getTournamentPlayoffParentEvent2() {
return $this->tournamentPlayoffParentEvent2;
}

 function setTournamentPlayoffParentEvent1($tournamentPlayoffParentEvent1) {
$this->tournamentPlayoffParentEvent1 = $tournamentPlayoffParentEvent1;
}

 function setTournamentPlayoffParentEvent2($tournamentPlayoffParentEvent2) {
$this->tournamentPlayoffParentEvent2 = $tournamentPlayoffParentEvent2;
}



function getTournamentTargetEvent() {
return $this->tournamentTargetEvent;
}

 function setTournamentTargetEvent($tournamentTargetEvent) {
$this->tournamentTargetEvent = $tournamentTargetEvent;
}





}
