<?php

namespace Webteamer\Event\Controller;

use Core\Controller;
use Core\ServiceLayer;
use Core\Validator;
use DateTime;
use Webteamer\Event\Form\EventForm;
use Webteamer\Event\Model\Event;

/**
 * @author hubacek
 * @version 1.0
 * @created 29-10-2015 10:43:56
 */
class TestController extends Controller {

    public function createTeamEventAction()
    {
       \Core\DbQuery::switchDb('test');
        $manager = \Core\ServiceLayer::getService('TeamEventManager');
        $data = array(
            'period' => 'weekly',
            'period_interval'=> 1,
            'team_id' => '1000',
            'event_type' => 'training',
            'playground_id' => 31,
            'note' => 'TEST');
        $manager->createTeamEvent($data);
    }
    
    
    public function createTeamEventAttendanceAction()
    {
       \Core\DbQuery::switchDb('test');
        $manager = \Core\ServiceLayer::getService('TeamEventManager');
        $data = array(
            'participant_id' => '0',
            'event_id'=> 1000,
            'status' => '1',
            'participant_type' => 'training',
            'playground_id' => 31,
            'note' => 'TEST');
        $manager->createTeamEventAttendance($data);
    }
    
    
   
}

?>