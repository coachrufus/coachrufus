<?php

namespace Webteamer\Event\Controller;

use Core\RestController;
use Core\ServiceLayer;
use DateTime;
use Webteamer\Event\Model\EventCalendar;

class WidgetController extends RestController {

    public function playerUpcomingEventsAction()
    {
        $security = ServiceLayer::getService('security');
        $userIdentity = $security->getIdentity();
        $user = $userIdentity->getUser();
        //$month = ($this->getRequest()->get('month') == null) ? date('Y-m') : $this->getRequest()->get('month') ;
        $month = $this->getRequest()->get('month') ;
        $startDate = new DateTime($month . '-01');
        $teamManager = ServiceLayer::getService('TeamManager');
        $teams = $teamManager->getUserTeams($user);
        $teamEventManager = ServiceLayer::getService('TeamEventManager');
        $monthInfo = $teamEventManager->getPagerMonthInfo($startDate);
        $calendarEvents = $teamEventManager->findEvents($teams, array('from' => $monthInfo['from'], 'to' => $monthInfo['to']));

        $calendar = new EventCalendar();
        $calendar->setDateFrom($monthInfo['from']);
        $calendar->setDateTo($monthInfo['to']);
        $calendar->buildMonthGrid($month);
        $firstGridDay = $calendar->getGridStartDate();
        $lastGridDay = $calendar->getGridEndDate();
        $calendarEventsList = $teamEventManager->buildTeamEventList($calendarEvents, $firstGridDay, $lastGridDay);
        $userAttednanceMatrix = $teamEventManager->getPlayerEventsAttendanceMatrix($user, $calendarEventsList);
        $calendar->setEvents($calendarEventsList);
        $calendar->getCalendarMonthOverview();

         $html = $this->render('Webteamer\Event\EventModule:widget:player_upcoming_events.php', array(
                        'attendanceList' => $userAttednanceMatrix,
                        'calendar' => $calendar,
                        'teams' => $teams
                    ))->getContent();
            return $this->asHtml($html);
        
    }

  

}
