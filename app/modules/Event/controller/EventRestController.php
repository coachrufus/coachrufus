<?php

namespace Webteamer\Event\Controller;

use Core\RestApiController;
use Core\ServiceLayer;
use Webteamer\Event\Model\EventCalendar;

class EventRestController extends RestApiController {

    public function jsonMonthGridOverview($calendar,$userAttednanceMatrix)
    {
        $calendarOverview = $calendar->getCalendarMonthOverview();
        
        $monthOverview = array();
        foreach ($calendarOverview as $weekIndex => $week)
        {
            foreach ($week as $dayIndex => $day)
            {
                $dayEvents = null;
                foreach ($day['events'] as $eventId => $event)
                {
                    //$monthOverview[$weekIndex][$dayIndex]
                    $attendance = array();
                    $attendance['in'] = $event->getAttendanceInSum();
                    $attendance['capacity'] = $event->getCapacity();
                    $attendance['userStatus'] = 'unknown';
                    if (
                            array_key_exists($event->getId(), $userAttednanceMatrix) &&
                            array_key_exists($event->getCurrentDate()->format('Y-m-d'), $userAttednanceMatrix[$event->getId()]) &&
                            $userAttednanceMatrix[$event->getId()][$event->getCurrentDate()->format('Y-m-d')] == 1)
                    {
                        $attendance['userStatus'] = 'in';
                    }

                    if (
                            array_key_exists($event->getId(), $userAttednanceMatrix) &&
                            array_key_exists($event->getCurrentDate()->format('Y-m-d'), $userAttednanceMatrix[$event->getId()]) &&
                            $userAttednanceMatrix[$event->getId()][$event->getCurrentDate()->format('Y-m-d')] == 3)
                    {
                        $attendance['userStatus'] = 'out';
                    }


                    $playgroundData = $event->getPlaygroundDataAsArray();

                    $playground = null;
                    if(false != $playgroundData)
                    {
                         $playground['name'] = $playgroundData['name'];
                         $playground['lat'] = $playgroundData['lat'];
                         $playground['lng'] = $playgroundData['lng'];
                    }
            
                    $currentDate = new \DateTime( $event->getCurrentDate()->format('Y-m-d') . ' ' . $event->getStart()->format('H:i:s'));
                    $dayEvents[$eventId] = array(
                        'name' => $event->getName(),
                        'teamName' => $event->getTeamName(),
                        'teamId' => $event->getTeamId(),
                        'termin' => $currentDate->getTimestamp(),
                        'playground' => $playground,
                        'attendance' => $attendance
                    );
                }
                $day['events'] = $dayEvents;
                $monthOverview[$weekIndex][$dayIndex] = $day;
            }
        }
        return $monthOverview;
    }
    
    public function yearGridOverviewAction()
    {
        $teamManager = ServiceLayer::getService('TeamManager');
        $teamEventManager = ServiceLayer::getService('TeamEventManager');
        
        $request= $this->getRequest();
        
        $result = array();
        $result['params'] = $this->getRequestParams();
        if(false == $this->checkApiKey())
        {
            return $this->unvalidApiKeyResult();
        }
        $user = ServiceLayer::getService('user_repository')->find($request->get('user_id'));
        $year = $request->get('year');
        
        $monthInfo['from'] = new \DateTime($year.'-01-01');
        $monthInfo['to'] = new \DateTime($year.'-12-31');

        
        $teams = $teamManager->getUserTeams($user);
        $calendarEvents = $teamEventManager->findEvents($teams, array('from' => $monthInfo['from'], 'to' => $monthInfo['to']));
        
        for($i=1;$i<=12;$i++)
        {
            $calendar = new EventCalendar();
            $calendar->setDateFrom($monthInfo['from']);
            $calendar->setDateTo($monthInfo['to']);
            $calendar->buildMonthGrid($year.'-'.sprintf('%02d', $i));
            
            $firstGridDay = $calendar->getGridStartDate();
            $lastGridDay = $calendar->getGridEndDate();
            $calendarEventsList = $teamEventManager->buildTeamEventList($calendarEvents, $firstGridDay, $lastGridDay);
            $userAttednanceMatrix = $teamEventManager->getPlayerEventsAttendanceMatrix($user, $calendarEventsList);
            $calendar->setEvents($calendarEventsList);
            $yearOverview[$i] = $this->jsonMonthGridOverview($calendar,$userAttednanceMatrix);
 
        }
        $result['myear_overview'] = $yearOverview;
        return $this->asJson($result);
      
    }

    public function monthGridOverviewAction()
    {
        $apiKey = $this->getRequest()->get('apikey');
        $defaultKey = 'logamic@40106144409386391';

        $responseType = (null == $this->getRequest()->get('rt')) ? 'json' : $this->getRequest()->get('rt');

        if ($responseType == 'json')
        {
            $user = ServiceLayer::getService('user_repository')->find($this->getRequest()->get('user_id'));

            if(false == $this->checkApiKey())
            {
                return $this->unvalidApiKeyResult();
            }
        }

        if ($responseType == 'html')
        {
            $security = ServiceLayer::getService('security');
            $userIdentity = $security->getIdentity();
            $user = $userIdentity->getUser();
        }


        $month = $this->getRequest()->get('month');
        $startDate = new \DateTime($month . '-01');
        $teamManager = ServiceLayer::getService('TeamManager');
        $teams = $teamManager->getUserTeams($user);
        $teamEventManager = ServiceLayer::getService('TeamEventManager');
        $monthInfo = $teamEventManager->getPagerMonthInfo($startDate);

        $calendarEvents = $teamEventManager->findEvents($teams, array('from' => $monthInfo['from'], 'to' => $monthInfo['to']));

        $calendar = new EventCalendar();
        $calendar->setDateFrom($monthInfo['from']);
        $calendar->setDateTo($monthInfo['to']);
        $calendar->buildMonthGrid($month);
        $firstGridDay = $calendar->getGridStartDate();
        $lastGridDay = $calendar->getGridEndDate();
        $calendarEventsList = $teamEventManager->buildTeamEventList($calendarEvents, $firstGridDay, $lastGridDay);
        $userAttednanceMatrix = $teamEventManager->getPlayerEventsAttendanceMatrix($user, $calendarEventsList);
        
      
        //t_dump($calendarEventsList);
        
        $statManager = ServiceLayer::getService('PlayerStatManager');
        foreach($calendarEventsList as $eventDate => $dayEvents)
        {
            foreach($dayEvents as $eventId => $dayEvent)
            {
                $eventMatchResult = $statManager->getMatchOverviewByEvent($dayEvent);
                if($eventMatchResult != null)
                {
                    $dayEvent->setMatchResult($eventMatchResult->getFirstLineGoals().':'.$eventMatchResult->getSecondLineGoals());
                }
                else
                {
                     $dayEvent->setMatchResult(null);
                }
               
            }
        }
        $calendar->setEvents($calendarEventsList);
        

       

        if ($responseType == 'html')
        {
            
             
            $template = 'Webteamer\Event\EventModule:Default:_small_calendar.php';
            if('player-upcoming-events' == $this->getRequest()->get('view'))
            {
                $template = 'Webteamer\Event\EventModule:widget:player_upcoming_events.php';
            }
            
            $html = $this->render($template, array(
                        'attendanceList' => $userAttednanceMatrix,
                        'calendar' => $calendar,
                        'teams' => $teams
                    ))->getContent();
            return $this->asHtml($html);
        }
        
        


        if ($responseType == 'json')
        {
            $monthOverview = $this->jsonMonthGridOverview($calendar,$userAttednanceMatrix);
            $result = array();
            $result['status'] = 'success';
            $result['params'] = $this->getRequestParams();
           
            $result['month_overview'] = $monthOverview;
            return $this->asJson($result);
        }
    }
    
    
    public function changePlayerAttendanceAction()
    {
        $request= $this->getRequest();
        $result = array();
        $result['params'] = $this->getRequestParams();
        if(false == $this->checkApiKey())
        {
            return $this->unvalidApiKeyResult();
        }
        
        $eventManager = ServiceLayer::getService('TeamEventManager');
        $teamManager = ServiceLayer::getService('TeamManager');
        
        $eventId = $request->get('event_id');
        $eventDate = $request->get('event_date');
        $type = $request->get('type');
        $event = ServiceLayer::getService('EventRepository')->find($eventId);
        $event->setCurrentDate(new \DateTime($eventDate));

        $teamPlayerId = $request->get('team_player_id');
        $teamPlayer = $teamManager->findTeamPlayerById($teamPlayerId);
        
        $eventManager->deleteEventAttendance($teamPlayer, $event, $eventDate);

        
        if('in' == $type)
        {
            $eventManager->createEventAttendance($teamPlayer, $event, $eventDate,1);
        }
        elseif('na' == $type)
        {
            $eventManager->createEventAttendance($teamPlayer, $event, $eventDate,2);
        }
        elseif('out' == $type)
        {
            $eventManager->createEventAttendance($teamPlayer, $event, $eventDate,3);
        }

        $attendance = $eventManager->getEventAttendance($event);
        $event->setAttendance($attendance);
        $result['uid'] = $event->getUid();
        $result['in'] = $event->getAttendanceInSum();
        $result['progress'] = round($event->getAttendanceInSum()/$event->getCapacity()*100) ;
       
        return $this->asJson($result);
    }
    
    public function eventDetailAction()
    {
        $request= $this->getRequest();
        $translator = $this->getTranslator();
        (null == $request->get('lang')) ? $translator->setLang('en') : $translator->setLang( $request->get('lang'));
        
        $result = array();
        $result['params'] = $this->getRequestParams();
        if(false == $this->checkApiKey())
        {
            return $this->unvalidApiKeyResult();
        }
        
        $request = ServiceLayer::getService('request');
        $teamManager = ServiceLayer::getService('TeamManager');
        $eventManager = ServiceLayer::getService('EventManager');
        $teamEventManager = ServiceLayer::getService('TeamEventManager');
        $event = $teamEventManager->findEventById($request->get('id'));
        
        $playgroundManager =  ServiceLayer::getService('PlaygroundManager');
        $statManager = ServiceLayer::getService('PlayerStatManager');
        $sportManager = ServiceLayer::getService('SportManager');
        $seasonManager = ServiceLayer::getService('TeamSeasonManager');
        $eventSeason = $seasonManager->getSeasonById($event->getSeason());
        $eventTypeEnum = $eventManager->getEventTypeEnum();
        $eventType = $eventTypeEnum[$event->getEventType()];
        
        $eventCurrentDate = $request->get('date').' '.$event->getStart()->format('H:i:00');
        $event->setCurrentDate(new \DateTime($eventCurrentDate));
        $playground = $playgroundManager->createObjectFromArray($event->getPlaygroundDataAsArray());
        $team = $teamManager->findTeamById($event->getTeamId());
        

        
        if(null != $request->get('uid'))
        {
            $user = ServiceLayer::getService('security')->getIdentityProvider()->findUserById($request->get('uid'));
            
        }
        else
        {
            $security = ServiceLayer::getService('security');
            $user = $security->getIdentity()->getUser();
        }
        
        //ServiceLayer::getService('security')->getIdentity()->checkTeamAccess($team);
       
        $eventsList = $teamEventManager->getTeamUpcomingEvents($team);
        
        $attendance = $teamEventManager->getEventAttendance($event);
        $event->setAttendance($attendance);
        $existLineup = $teamEventManager->getEventLineup($event);
        $eventTimeline = $statManager->getEventTimeline($event);
        
        $teamMembers = $teamManager->getTeamPlayers($team->getId());
            $teamMembersList = array('accept'=>array(),'denny' => array(),'unknown' =>array());
            $acceptedPlayers = null;
            $acceptedGuestPlayers = null;
            $denyPlayers = null;
            $unknownPlayers = null;
            $currentMember = null;
            foreach($teamMembers as $teamMember)
            {
                foreach($attendance as $eventAttendance)
                {
                    if($eventAttendance->getTeamPlayerId() == $teamMember->getId())
                    {
                         $teamMember->addEventAttendance($event->getUid(),$eventAttendance);
                    }
                }
                
                $myAttendance = false;
                if($teamMember->getPlayerId() == $user->getId())
                {
                    $currentMember = $teamMember;
                    $myAttendance = true;
                }
                
                //t_dump($teamMember->getEventAttendance());
                
                 $teamMemberData = array(
                    'team_player_id' => $teamMember->getId(),
                    'name' => $teamMember->getFirstname(),
                    'surname' => $teamMember->getLastname(),
                    'player_number' => $teamMember->getPlayerNumber(),
                    'team_role' => $teamMember->getTeamRoleName(),
                    'team_role_code' => $teamMember->getTeamRole(),
                    'icon' => WEB_DOMAIN.$teamMember->getMidPhoto(),
                     'myAttendance' => $myAttendance
                );

                
                //sort members to groups
                if($teamMember->getEventAttendanceStatus($event->getUid()) == '1')
                {
                     $teamMemberData['attendance'] = 'in';
                     if('GUEST' == $teamMember->getTeamRole())
                     {
                          $acceptedGuestPlayers[$teamMember->getEventAttendance($event->getUid())->getCreatedAt()->getTimestamp()][$teamMember->getId()] =  $teamMemberData;
                     }
                     else 
                     {
                         $acceptedPlayers[$teamMember->getEventAttendance($event->getUid())->getCreatedAt()->getTimestamp()][$teamMember->getId()] =  $teamMemberData;
                     }       
                }
                elseif($teamMember->getEventAttendanceStatus($event->getUid()) == '3')
                {
                    $teamMemberData['attendance'] = 'out';
                    $denyPlayers[][$teamMember->getId()] =  $teamMemberData;
                }
                else
                {
                    $teamMemberData['attendance'] = 'unknown';
                    $unknownPlayers[][$teamMember->getId()] =  $teamMemberData;
                }
            }

            ksort($acceptedPlayers);
            ksort($acceptedGuestPlayers);
            $sportEnum = $sportManager->getSportFormChoices();
            
            
            $teamMembersList['accept']['players'] = $acceptedPlayers;
            $teamMembersList['accept']['guest'] = $acceptedGuestPlayers;
            $teamMembersList['denny']['all'] = $denyPlayers;
            $teamMembersList['unknown']['all'] = $unknownPlayers;
            
            $result['status'] = 'success';
            $result['params']['id'] = $this->getRequest()->get('id');
            $result['params']['date'] = $this->getRequest()->get('date');
            $result['params']['apikey'] = $this->getRequest()->get('apikey');
            $result['name'] = $event->getName();
            $result['team_name'] = $team->getName();
            $result['season'] =$eventSeason->getName().' '.$this->getLocalizer()->formatDate( $eventSeason->getStartDate()).'-'.$this->getLocalizer()->formatDate( $eventSeason->getEndDate());
            $result['sport'] = strtoupper($translator->translate( $sportEnum[$team->getSportId()]));
            $result['type'] =strtoupper($translator->translate($eventType));
            $result['termin'] =$event->getCurrentDate()->getTimestamp();
            $result['capacity'] = $event->getCapacity();
            $result['playground'] = array();
            $result['id'] = $event->getId();
            $result['current_date'] = $eventCurrentDate;
            if(null != $playground)
            {
                 $result['playground']['name'] = $playground->getName();
                 $result['playground']['lat'] = $playground->getLat();
                 $result['playground']['lng'] = $playground->getLng();
            }
            
            $result['attendance'] = $teamMembersList;

            
            return $this->asJson($result);

            
    }

    public function testAction()
    {
        //$response = file_get_contents('http://app.coachrufus.com/api/event/month-grid');



        $endpoint = 'http://app.coachrufus.lamp/api/event/month-grid';
        $data = array(
            'month' => '2016-01-01',
            'user_id' => 3
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

        //echo $callUrl = $endpoint . '?' . http_build_query($data);
        $callUrl = $endpoint . '?' . http_build_query($data);
        curl_setopt($ch, CURLOPT_URL, $callUrl);
        $result = curl_exec($ch);
        curl_close($ch);
        //var_dump($result);



        $result_arr = json_decode($result, true);
        t_dump($result_arr);
        return $result_arr;
    }

}
