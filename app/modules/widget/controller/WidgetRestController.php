<?php
namespace CR\Widget\Controller;
use Core\Controller as Controller;
use Core\Request as Request;
use Core\ServiceLayer;
use Core\GUID;
use Core\RestController;
use Core\Images\ImageMarginFix;
use CR\Widget;
use Core\Db;
use Core\RenderOptions;
use Core\Types\DateTimeEx;

class WidgetRestController extends RestController
{
    use Db;

    private $request;
    private $UIManager;
    private $activeTeam;
    private $actualPackageManager;

    function __construct()
    {
        $this->activeTeam = (object)$activeTeamManager = ServiceLayer::getService('ActiveTeamManager')->getActiveTeam();
        $this->request = ServiceLayer::getService('request');
        $this->UIManager = ServiceLayer::getService('UIManager');
        $this->actualPackageManager = ServiceLayer::getService('ActualPackageManager');
        $this->actualPackageManager->checkPermissions();
    }
    
    public function hasSettingsAction()
    {
        $userId = (int)$this->request->get('userId');
        return $this->asJson($this->UIManager->settings->has($userId, $this->activeTeam->id));
    }
    
    public function loadSettingsAction()
    {
        $userId = (int)$this->request->get('userId');
        return $this->asJson($this->UIManager->settings->load($userId, $this->activeTeam->id));
    }
    
    public function saveSettingsAction()
    {
        try
        {
            $userId = (int)$this->request->get('userId');
            $settings = $this->UIManager->settings->load($userId, $this->activeTeam->id);
            $newSettings = json_decode(file_get_contents("php://input"), false);
            foreach ($newSettings as $k => $v) $settings->{$k} = $v;
            $this->UIManager->settings->save($userId, $this->activeTeam->id, $settings);
            return $this->asJson(true);
        }
        catch (\Exception $e)
        {
            return $this->asJson(false);
        }
    }
    
    public function uploadImageAction()
    {
        $rawPostData = file_get_contents("php://input");
        $data = base64_decode(substr($rawPostData, strpos($rawPostData, ",") + 1));
        $guid = GUID::create();
        $dir = APPLICATION_PATH . "/img/share";
        if (!file_exists($dir)) mkdir($dir);
        $path = "{$dir}/{$guid}.png";
        $file = fopen($path, 'wb');
        fwrite($file, $data);
        fclose($file);
        $fix = new ImageMarginFix();
        $image = $fix->apply(\WideImage::load($path))->saveToFile($path);
        return $this->asJson(array('guid' => $guid,'fb_id' => FB_APP_ID, 'root' => WEB_DOMAIN,'desc' => $this->getTranslator()->translate('Overal Performance Statistics')));
    }

    private function getFirstName($fullName)
    {
        $arr = explode(' ', $fullName);
        return count($arr) > 0 ? $arr[0] : $fullName;
    }

    private function getLineupRows($lineupId)
    {
        $db = $this->getDb();
        $result = [];
        foreach ($db->team_match_lineup_player('lineup_id', $lineupId) as $row)
        {
            $row = iterator_to_array($row);
            $result[$row['id']] = $row + [
                'goal' => 0,
                'mom' => 0,
                'assist' => 0,
                'first_name' => $this->getFirstName($row['player_name'])
            ];
        }
        return $result;
    }

    private function getPlayerTimeLineEventRows($lineupId)
    {
        $db = $this->getDb();
        $result = [];
        foreach ($db->player_timeline_event('lineup_id', $lineupId) as $row)
        {
            $result[] = iterator_to_array($row);
        }
        return $result;
    }

    private function maxBy(array $arr, Callable $mapper)
    {
        $max = -1;
        $count = 0;
        $item = null;
        foreach ($arr as $i)
        {
            $value = $mapper($i);
            if ($value > $max)
            {
                $max = $value;
                $count = 1;
                $item = $i;
            }
            else if ($value == $max) $count++;
        }
        return $count === 1 ? $item : null;
    }

    private function prepareTopPlayers($topPlayers, $firstLineName, $secondLineName, $vld)
    {
        $result = [];
        foreach ($topPlayers as $key => $player)
        {
            if (is_null($player)) $result[$key] = null;
            else
            {
                $result[$key] = [
                    'first_name' => $player['first_name'],
                    'player_name' => $player['player_name'],
                    'score' => "{$player['goal']}+{$player['assist']}",
                    'lineup_position' => $player['lineup_position'],
                    'team' => $player['lineup_position'] === 'first_line' ? $firstLineName :  $secondLineName,
                    'state' => $vld[$player['lineup_position']]
                ];
            }
        }
        return $result;
    }
    
    private function getBestStriker($teamPlayerMap)
    {
         $strikersLadder = array();
        foreach($teamPlayerMap as $teamPlayer)
        {
             $strikersLadder[$teamPlayer['goal']][$teamPlayer['id']] = $teamPlayer;
        }
        
        krsort($strikersLadder);
        
        $bestStrikers = current($strikersLadder);
        
        $striker = null;
        if(count($bestStrikers) == 1)
        {
            $striker = current($bestStrikers);
        }
        else
        {
            foreach($bestStrikers as $bestStriker)
            {
                if($bestStriker['mom'] == 1)
                {
                    $striker = $bestStriker;
                }
            }
        }
        if($striker == null)
        {
            $striker = current($bestStrikers);
            foreach($bestStrikers as $bestStriker)
            {
                
                if($bestStriker['assist'] > $striker['assist'])
                {
                    $striker = $bestStriker;
                }
            }
        }
        return $striker;
    }
    
    private function getBestAssist($teamPlayerMap)
    {
        $ladder = array();
        foreach($teamPlayerMap as $teamPlayer)
        {
             $ladder[$teamPlayer['assist']][$teamPlayer['id']] = $teamPlayer;
        }
        
        krsort($ladder);
        $best = current($ladder);
        
        $assist = null;
        if(count($best) == 1)
        {
            $assist = current($best);
        }
        else
        {
            foreach($best as $b)
            {
                if($b['mom'] == 1)
                {
                    $assist = $b;
                }
            }
        }
        if($assist == null)
        {
            $assist = current($best);
            foreach($best as $b)
            {
                
                if($b['assist'] > $assist['goal'])
                {
                    $assist = $b;
                }
            }
        }
        return $assist;
    }
    
    

    private function getTopPlayers($lineupId, $user, $firstLineName, $secondLineName, $vld)
    {
        $teamPlayerMap = $this->getLineupRows($lineupId);
        $rows = $this->getPlayerTimeLineEventRows($lineupId);
        
        foreach ($rows as $row)
        {
            if ($row['lineup_player_id'] != 0) $teamPlayerMap[$row['lineup_player_id']][$row['hit_type']] += 1;
        }
        
       
        $striker = $this->getBestStriker($teamPlayerMap);
        $assist = $this->getBestAssist($teamPlayerMap);

        
        $currentUserId = $user->getId();
        $currentUser = array_filter($teamPlayerMap, function($i) use($currentUserId) { return $i['player_id'] == $currentUserId; });
        return $this->prepareTopPlayers([
            'striker' =>$striker,
            'mom' => $this->maxBy($teamPlayerMap, function($i) { return $i['mom']; }),
            'assist' =>$assist,
            'currentPlayer' => empty($currentUser) ? [
                'goal' => 0,
                'mom' => 0,
                'assist' => 0,
                'first_name' => $user->getName(),
                'player_name' => $user->getName() . ' ' . $user->getSurname()
            ] : array_values($currentUser)[0]
        ], $firstLineName, $secondLineName, $vld);
    }

    private function getGetWLD($matchOverview)
    {
        $g1 = $matchOverview->getFirstLineGoals();
        $g2 = $matchOverview->getSecondLineGoals();
        if ($g1 > $g2)
        {
            return [
                'first_line' => 'win',
                'second_line' => 'loss',
            ];
        }
        else if ($g1 < $g2)
        {
            return [
                'first_line' => 'loss',
                'second_line' => 'win',
            ];
        }
        else
        {
            return [
                'first_line' => 'draw',
                'second_line' => 'draw',
            ];
        }
    }

    private function getEventStart($matchOverview)
    {
        $db = $this->getDb();
        $date = new DateTimeEx($matchOverview->getEventDate());
        $startTime = DateTimeEx::parse($db->team_event('id', $matchOverview->getEventId())->fetch()['start']);
        return $date->timeFrom($startTime);
    }

    private function getSport(int $eventId)
    {
        $result = \Core\DbQuery::prepare("SELECT s.name FROM sport s JOIN team t ON s.id = t.sport_id JOIN team_event e ON e.team_id = t.id WHERE e.id = :eventId")
            ->bindParam('eventId', $eventId)
            ->execute()
            ->fetchAll(\PDO::FETCH_ASSOC);
        return empty($result) ? null : $result[0]['name'];
    }

    public function shareEventAction()
    {
        $eventManager = ServiceLayer::getService('TeamEventManager');
        $event = $eventManager->findEvent($this->request->get('eventId'));
        if (is_null($event)) return $this->asJson(null);
        $event->setCurrentDate(new \DateTime($this->request->get('eventDate')));
        $lineup = $eventManager->getEventLineup($event);
        if (is_null($lineup)) return $this->asJson(null);
        else 
        {
            $matchOverview = ServiceLayer::getService('PlayerStatManager')->getMatchOverview($lineup);
            $lineupId = (int)$matchOverview->getLineupId();
            $user = ServiceLayer::getService('security')->getIdentity()->getUser();
            $vld = $this->getGetWLD($matchOverview);
            $topPlayers = $this->getTopPlayers($lineupId, $user, $matchOverview->getFirstLineName(), $matchOverview->getSecondLineName(), $vld);
            $lineupPlayers = $eventManager->getEventLineupPlayers($lineup);
            $isAttendant = false;
            foreach($lineupPlayers as $lineupPlayer)
            {
                if($lineupPlayer->getPlayerId() ==  $user->getId())
                {
                    $isAttendant = true;
                }
            }
            if($isAttendant == false)
            {
                $topPlayers['currentPlayer']['state'] = 'win';
                
            }
          
            
            $eventId = $matchOverview->getEventId();
            $translator = ServiceLayer::getService('translator');
            return $this->render([
                'data' => [
                    'eventId' => $matchOverview->getEventId(),
                    'eventDate' => $this->getEventStart($matchOverview)->toString('d. M Y H:i'),
                    'eventCurrentDate' => $event->getCurrentDate()->format('Y-m-d'),
                    'sport' => $translator->translate($this->getSport($eventId)),
                    'result' => $matchOverview->getResult(),
                    'firstLineGoals' => $matchOverview->getFirstLineGoals(),
                    'secondLineGoals' => $matchOverview->getSecondLineGoals(),
                    'firstLineName' => $matchOverview->getFirstLineName(),
                    'secondLineName' => $matchOverview->getSecondLineName(),
                    'teamName' => $matchOverview->getTeamName(),
                    'topPlayers' => $topPlayers,
                    'referrer' => isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : null,
                ],
                'width' => 1212,
                'height' => 642,
                'TEAM' => $translator->translate('Team'),
                'BEST_PLAYERS' => $translator->translate('BEST_PLAYERS'),
                'STRIKER' => $translator->translate('Striker'),
                'ASISTANCE' => $translator->translate('Asistance'),
                'STATE' => $translator->translate($topPlayers['currentPlayer']['state']),
                'WEB_DOMAIN' => WEB_DOMAIN,
                'FB_APP_ID' => FB_APP_ID,
                'isAttendant' => $isAttendant
            ], new RenderOptions(['layout' => false]));
        }
    }

    public function uploadEventImageAction()
    {
        $rawPostData = file_get_contents("php://input");
        $data = base64_decode(substr($rawPostData, strpos($rawPostData, ",") + 1));
        $guid = GUID::create();
        $dir = APPLICATION_PATH . "/img/share/events";
        if (!file_exists($dir)) mkdir($dir);
        $path = "{$dir}/{$guid}.png";
        $file = fopen($path, 'wb');
        fwrite($file, $data);
        fclose($file);
        $fix = new ImageMarginFix();
        $image = $fix->apply(\WideImage::load($path))->saveToFile($path);
        return $this->asJson($guid);
    }
}