<?php
namespace CR\Widget\Controller;
use Core\Controller as Controller;
use Core\Request as Request;
use Core\ServiceLayer;
use Core\RestController;
use CR\Widget;
use Core\Collections\HashSet as Set;
use Core\Types\DateTimeEx;
use Core\NameCleaner;

class StatsRestController extends RestController
{
    private $teamManager;
    private $translator;
    private $playerStatManager;
    private $activeTeam;
    private $actualPackageManager;
    
    private function getSeasonStats($team, $columnMapper, $seasonId)
    {
        $teamPlayers = $this->teamManager->getTeamPlayers($team->getId());
        $seasonStatsMapper = new \CR\Widget\SeasonStatsMapper($teamPlayers);
        $statsByDate = $this->playerStatManager->getAllTeamPlayersStatByDate($team, $seasonId);
        return $seasonStatsMapper->getSeasonStats($statsByDate, $columnMapper);
    }
    
    private function getActualSeason($team)
    {
        
        $UIManager = ServiceLayer::getService('UIManager');
        $seasonManager = ServiceLayer::getService('SeasonManager');
        $user = ServiceLayer::getService('security')->getIdentity()->getUser();
        $UISettings = $UIManager->settings->load($user->getId(), $this->activeTeam->id);
        $seasons = $seasonManager->getSeasonsFor($team->getId());
        $teamSettings = isset($UISettings->{$team->getId()}) ? $UISettings->{$team->getId()} : [];
        return $seasonManager->getActualSeason($seasons, $teamSettings);
    }
    
    private function getUserWidgetData($team, $userWidget)
    {
        $columnSet = Set::of($userWidget->columns);
        $options = (array)$userWidget->options;
        $season = $this->getActualSeason($team);
        $seasonId = is_null($season) ? null : $season['id'];
        if ($columnSet->contains('SeasonKBI'))
        {
            return ["column" => "SeasonKBI", "data" => $this->getSeasonStats($team, function($stats) { return $stats->getKbi(); }, $seasonId)];
        }
        else if ($columnSet->contains('SeasonRate'))
        {
            return ["column" => "SeasonRate", "data" => $this->getSeasonStats($team, function($stats) { return $stats->getGamePlayed(); }, $seasonId)];
        }
        else
        {
            $dateFrom = isset($options['dateFrom']) ? DateTimeEx::parse($options['dateFrom'], 'Y-m-d') : null;
            $dateTo = isset($options['dateTo']) ? DateTimeEx::parse($options['dateTo'], 'Y-m-d') : null;
            return $this->getStats($team, $userWidget->columns, $seasonId, $dateFrom, $dateTo);
        }
    }
    
    private function getUserWidgetsData($team, $userWidgets)
    {
        foreach ($userWidgets as $userWidget)
        {
            yield $userWidget->GUID => $this->getUserWidgetData($team, $userWidget);
        }
    }

    private function getSeasonTeamPlayers($currentSeason)
    {
       $dateFrom =$currentSeason->getStartDate();
        $dateTo = $currentSeason->getEndDate();
        
        //var_dump($dateTo->format('Y-m-d'));
        
        //exit;
        
        $sql = "SELECT p.team_player_id "
                . "FROM team_event e "
                . "JOIN team_match_lineup l ON e.id = l.event_id "
                . "JOIN team_match_lineup_player p ON l.id = p.lineup_id "
                . "WHERE Date(l.event_date) <=  :date_to and Date(l.event_date) >= :date_from AND l.team_id = :team_id";
        $result = \Core\DbQuery::prepare($sql)
            ->bindParam('date_to', $dateTo->format('Y-m-d'))
            ->bindParam('date_from', $dateFrom->format('Y-m-d'))
            ->bindParam('team_id', $currentSeason->getTeamId())
            ->execute()
            ->fetchAll(\PDO::FETCH_ASSOC);
        $teamPlayers = [];
        foreach ($result as $row)
        {
            $teamPlayers[$row['team_player_id']] = true;
        }
        return $teamPlayers;
    }
    
    private function getStats($team, $columns, $seasonId, DateTimeEx $dateFrom = null, DateTimeEx $dateTo = null)
    {
        $currentSeason = ServiceLayer::getService('TeamSeasonManager')->getSeasonById($seasonId);

        if (empty($dateFrom)) $dateFrom = new DateTimeEx($currentSeason->getStartDate());
        if (empty($dateTo)) $dateTo = new DateTimeEx($currentSeason->getEndDate());
        $seasonTeamPlayers = $this->getSeasonTeamPlayers($currentSeason);
        $playersStats = $this->playerStatManager->getAllTeamPlayersStat($team, null, $dateFrom, $dateTo);
        $teamPlayers = $this->teamManager->getTeamPlayers($team->getId());
        $tp = [];
        foreach($teamPlayers as $teamPlayer)
        {
            $teamPlayerId = $teamPlayer->getId();
            if(isset($playersStats[$teamPlayerId]) && isset($seasonTeamPlayers[$teamPlayerId]))
            {
                $teamPlayer->setStats($playersStats[$teamPlayerId]);
                $tp[] = $teamPlayer;
            }
        }
        $mapper = new \CR\Widget\StatsMapper($columns);
        return $mapper->createTable($tp);
    }
    
    function __construct()
    {
        $this->activeTeam = (object)$activeTeamManager = ServiceLayer::getService('ActiveTeamManager')->getActiveTeam();
        $this->translator = ServiceLayer::getService('translator');
        $this->playerStatManager = ServiceLayer::getService('PlayerStatManager');
        $this->teamManager = ServiceLayer::getService('TeamManager');
        $this->teamManager->setTranslator($this->translator);
        $this->actualPackageManager = ServiceLayer::getService('ActualPackageManager');
        $this->actualPackageManager->checkPermissions();
    }
    
    function changeSeasonAction()
    {
        try
        {
            $request = ServiceLayer::getService('request');
            $teamId = (int)$request->get('team_id');
            $seasonId = $request->get('season_id');
            $user = ServiceLayer::getService('security')->getIdentity()->getUser();
            $UIManager = ServiceLayer::getService('UIManager');
            $UISettings = $UIManager->settings->load($user->getId(), $this->activeTeam->id);
            $UISettings->{$teamId}->seasonId = $seasonId;
            $UIManager->settings->save($user->getId(), $this->activeTeam->id, $UISettings);
            ob_end_clean();
            return $this->asJson(true);
        }
        catch (Exception $e)
        {
            ob_end_clean();
            return $this->asJson(false);
        }
    }
    
    function widgetDataAction()
    {
        $request = ServiceLayer::getService('request');
        $teamId = $request->get('team_id');
        $team = $this->teamManager->findTeamById($teamId);
        $userWidgets = isset($_POST['userWidgets']) ? json_decode($_POST['userWidgets']) : [];
        return $this->asJson([
            'team' => ['name' => $team->getName()],
            'data' => iterator_to_array($this->getUserWidgetsData($team, $userWidgets))
        ]);
    }
}