<?php namespace CR\Widget\Controller;

use Core\Controller as Controller;
use Core\Request as Request;
use Core\ServiceLayer;
use Core\GUID;
use Core\RestController;
use CR\Widget\StatsMapper;
use Core\Types\DateTimeEx;
use Core\Collections\Iter;
use Core\Events\EventInfo;
use Core\NameCleaner;
use Core\HashSet as Set;
use \__;
use Core\Db;

class AnalyticsController extends RestController
{
    use Db;
    
    private $translator;
    private $request;
    private $activeTeam;
    private $actualPackageManager;

    function __construct()
    {
        $this->translator = ServiceLayer::getService('translator');
        $this->request = ServiceLayer::getService('request');
        
        if(null != $this->request->get('team_id'))
        {
            $teamManager = ServiceLayer::getService('TeamManager');
            $team = $teamManager->findTeamById($this->request->get('team_id'));
            ServiceLayer::getService('ActiveTeamManager')->setActiveTeam($team);
        }

        $this->activeTeam = (object)$activeTeamManager = ServiceLayer::getService('ActiveTeamManager')->getActiveTeam();
        $this->db = $this->getDb(); //neviem najst teamPlayerId findTeamPlayerByPlayerId takze si to vyberem z DB
        $this->actualPackageManager = ServiceLayer::getService('ActualPackageManager');
        //$this->actualPackageManager->checkPermissions();
    }
    
    private function getTeamPlayers($teamId)
    {
        $getterMapper = ServiceLayer::getService('GetterMapper');
        $teamPlayers = ServiceLayer::getService('TeamPlayerManager')->fulltextFindPlayer("", $teamId);
        return iterator_to_array((function($teamPlayers) use($getterMapper)
        {
            foreach ($teamPlayers as $teamPlayer)
            {
                $tp = $getterMapper->mapToArray($teamPlayer, ['dataMapperRules', 'photo', 'midPhoto']);
                $tp['fullName'] = NameCleaner::clean("{$tp['firstName']} {$tp['lastName']}");
                yield $tp;
            }
        })($teamPlayers));
    }
    
    private function getTeamPlayer($teamPlayers, $user)
    {
        foreach ($teamPlayers as $tp)
        {
            if ($tp['playerId'] === $user->getId())
            {
                return $tp;
            }
        }
        return null;
    }
    
    function formatSeason($season)
    {
        $startDate = date('d.m.Y', strtotime($season['start_date']));
        $endDate = date('d.m.Y', strtotime($season['end_date']));
        return "{$season['name']} ({$startDate} - {$endDate})";
    }
    
    private function prepareSeason(&$season)
    {
        $season['fullName'] = $this->formatSeason($season);
        return $season;
    }
    
    private function prepareSeasons($seasons)
    {
        foreach ($seasons as &$season)
        {
            $this->prepareSeason($season);
        }
        return $seasons;
    }
    
    function getListActionData()
    {
        $request = ServiceLayer::getService('request');
        $teamManager = ServiceLayer::getService('TeamManager');
        $teamManager->setTranslator($this->translator);
        
        $team = $teamManager->findTeamById($request->get('team_id'));
        ServiceLayer::getService('ActiveTeamManager')->setActiveTeam($team);
        ServiceLayer::getService('ActualPackageManager')->saveToSession();
        $this->activeTeam = (object)$activeTeamManager = ServiceLayer::getService('ActiveTeamManager')->getActiveTeam();
         if(!ServiceLayer::getService('TeamCreditManager')->teamHasFeatureAccess($team,'widget'))
        {
            $this->getRequest()->redirect($this->getRouter()->link('order_list',array('team_id' =>  $team->getId())));
        }

        ServiceLayer::getService('security')->getIdentity()->checkTeamAccess($team);
        $teamPlayers = $this->getTeamPlayers($team->getId());
        $user = ServiceLayer::getService('security')->getIdentity()->getUser();
        $teamPlayer = $this->getTeamPlayer($teamPlayers, $user);
        $UIManager = ServiceLayer::getService('UIManager');
        $seasonManager = ServiceLayer::getService('SeasonManager');
        $UISettings = $UIManager->settings->load($user->getId(), $this->activeTeam->id);

        $seasons = $seasonManager->getSeasonsFor($team->getId());
        $teamSettings = isset($UISettings->{$team->getId()}) ? $UISettings->{$team->getId()} : null;
        $actualSeason = $seasonManager->getActualSeason($seasons, $teamSettings);
        $sportManager = ServiceLayer::getService('SportManager');
        $statRoute = $sportManager->getTeamSportAddStatRoute($team);
        
        if(null == $statRoute)
        {
            return array('status' => 'NO_STAT_ROUTE','team' => $team);
        }
        
        if(null == $actualSeason)
        {
            return array('status' => 'NO_ACTUAL_SEASON','team' => $team);
        }
        
        $teamMatesUserWidget = TeamMates::load($this->activeTeam->id);
        $personalFormUserWidget = PersonalForm::load($this->activeTeam->id);
        $personalFormManager = ServiceLayer::getService('PersonalFormManager');
        if (is_null($teamMatesUserWidget) || !isset($teamMatesUserWidget->options->teamPlayerId))
        {
            $teamMatesOptions = (object)['teamPlayerId' => $teamPlayer['id']];
        }
        else
        {
            $teamMatesOptions = $teamMatesUserWidget->options;
        }

        if (empty($teamMatesOptions->dateFrom)) $teamMatesOptions->dateFrom = $actualSeason['start_date'];
        if (empty($teamMatesOptions->dateTo)) $teamMatesOptions->dateTo = $actualSeason['end_date'];

        if (is_null($personalFormUserWidget))
        {
            $personalFormOptions = (object)[];
        }
        else
        {
            $personalFormOptions = $personalFormUserWidget->options;
        }

        $fromDate = isset($personalFormUserWidget->options->dateFrom) ? DateTimeEx::parse("{$personalFormUserWidget->options->dateFrom} 00:00:00") : null;
        $toDate = isset($personalFormUserWidget->options->dateTo) ? DateTimeEx::parse("{$personalFormUserWidget->options->dateTo} 00:00:00") : null;
        
        if (empty($fromDate)) $fromDate = DateTimeEx::parse("{$actualSeason['start_date']} 00:00:00");
        if (empty($toDate)) $toDate = DateTimeEx::parse("{$actualSeason['end_date']} 00:00:00");
        

        list($seasonStats, $individualStats) = $this->getSeasonAndIndividualStats($team, $actualSeason); 
        
        return [
            'status' => 'SUCCESS',
            'team' => $team,
            'lang' => $this->translator->lang,
            'actualSeason' => $this->prepareSeason($actualSeason),
            'seasons' => $this->prepareSeasons($seasons),
            'UISettings' => $UISettings,
            'teamPlayers' => $teamPlayers,
            'teamMates' => TeamMates::getList($teamMatesOptions),
            'personalForm' => $personalFormManager->getPersonalFormFor([$teamPlayer['id']], $fromDate, $toDate),
            'seasonStats' => $seasonStats,
            'individualStats' => $individualStats,
            'packageProducts' => (string)$this->actualPackageManager->getActualPackage()->products
        ];
        
        
    }
    
    function listAction()
    {
        $data = $this->getListActionData();
        if($data['status'] == 'NO_STAT_ROUTE')
        {
            return $this->render('Webteamer\Team\TeamModule:teamMatch:season_stat_coming_soon.php', array(
                'team' => $data['team'],
            ));
        }
        if($data['status'] == 'NO_ACTUAL_SEASON')
        {
            return $this->render('Webteamer\Team\TeamModule:teamMatch:season_stat_unavailable.php', array(
                'team' =>  $data['team'],
            ));
        }
        

        return $this->render('CR\Widget\WidgetModule:analytics:list.php',  $data);
    }
    
    private function getSeasonAndIndividualStats($team, $actualSeason)
    {
        $individualStatsManager = ServiceLayer::getService('IndividualStatsManager');
        $seasonStatsManager = ServiceLayer::getService('SeasonStatsManager');
        $statFrom = DateTimeEx::parse($actualSeason['start_date'], 'Y-m-d');
        $statTo = DateTimeEx::parse($actualSeason['end_date'], 'Y-m-d');
        return [
            $seasonStatsManager->getSeasonStatsFor($team, $statFrom, $statTo),
            $individualStatsManager->getIndividualStatsFor($team, $statFrom, $statTo)
        ];
    }

    function changeSeasonAction()
    {
        $request = ServiceLayer::getService('request');
        $teamId = $request->get('team_id');
        $seasonId = $request->get('season_id');
        $user = ServiceLayer::getService('security')->getIdentity()->getUser();
        $UIManager = ServiceLayer::getService('UIManager');
        $UISettings = $UIManager->settings->load($user->getId(), $this->activeTeam->id);
        $UISettings->{$teamId}->seasonId = $seasonId;
        $UIManager->settings->save($user->getId(), $this->activeTeam->id, $UISettings);
        $request->redirect("/widget/analytics/list/{$teamId}");
    }

    function getSeasonDateRange($user, $team)
    {
        $seasonManager = ServiceLayer::getService('SeasonManager');
        $UIManager = ServiceLayer::getService('UIManager');
        $UISettings = $UIManager->settings->load($user->getId(), $this->activeTeam->id);
        $seasons = $seasonManager->getSeasonsFor($team->getId());
        $teamSettings = isset($UISettings->{$team->getId()}) ? $UISettings->{$team->getId()} : null;
        $actualSeason = $seasonManager->getActualSeason($seasons, $teamSettings);
        $fromDate = isset($options->dateFrom) ? DateTimeEx::parse("{$options->dateFrom} 00:00:00") : DateTimeEx::parse("{$actualSeason['start_date']} 00:00:00");
        $toDate = isset($options->dateTo) ? DateTimeEx::parse("{$options->dateTo} 00:00:00") : DateTimeEx::parse("{$actualSeason['end_date']} 00:00:00");
        return [$fromDate, $toDate];
    }

    function getActualTeamPlayerId($user)
    {
        $teamPlayers = $this->getTeamPlayers($this->activeTeam->id);
        $teamPlayer = $this->getTeamPlayer($teamPlayers, $user);
        return $teamPlayer['id'];
    }
    
    function personalFormAction()
    {
        $userWidgets = (array)json_decode(file_get_contents('php://input'), false);
        foreach ($userWidgets as $userWidget)
        {
            if ($userWidget->name === "PersonalForm") break;
        }
        $options = is_null($userWidget) ? (object)[] : $userWidget->options;
        TeamMates::persist($userWidgets, $this->activeTeam->id);
        $personalFormManager = ServiceLayer::getService('PersonalFormManager');
        $user = ServiceLayer::getService('security')->getIdentity()->getUser();
        $teamManager = ServiceLayer::getService('TeamManager');
        $teamManager->setTranslator($this->translator);
        $team = $teamManager->findTeamById($this->activeTeam->id);
        $teamPlayer = $this->db->team_players()
            ->where('team_id', $team->getId())
            ->where('player_id', $user->getId())
            ->fetch();
        list($fromDate, $toDate) = $this->getSeasonDateRange($user, $team);
        $fromDate = isset($options->dateFrom) ? DateTimeEx::parse("{$options->dateFrom} 00:00:00") : $fromDate;
        $toDate = isset($options->dateTo) ? DateTimeEx::parse("{$options->dateTo} 00:00:00") : $toDate;
        return $this->asJson($personalFormManager->getPersonalFormFor([$teamPlayer['id']], $fromDate, $toDate));
    }

    function teamMatesAction()
    {
        $user = ServiceLayer::getService('security')->getIdentity()->getUser();
        $userWidgets = (array)json_decode(file_get_contents('php://input'), false);
        foreach ($userWidgets as $userWidget)
        {
            if ($userWidget->name === "TeamMates") break;
        }
        TeamMates::persist($userWidgets, $this->activeTeam->id);
		$options = $userWidget->options;

        $teamManager = ServiceLayer::getService('TeamManager');
        $teamManager->setTranslator($this->translator);
        $team = $teamManager->findTeamById($this->activeTeam->id);
        list($fromDate, $toDate) = $this->getSeasonDateRange($user, $team);

        if (empty($options->dateFrom)) $options->dateFrom = $fromDate->toString('Y-m-d');
        if (empty($options->dateTo)) $options->dateTo = $toDate->toString('Y-m-d');

        if (empty($options->teamPlayerId)) $options->teamPlayerId = $this->getActualTeamPlayerId($user);
        return $this->asJson(TeamMates::getList($options));
    }
}