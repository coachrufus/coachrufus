<?php namespace CR\Widget\Controller;

use Core\Controller as Controller;
use Core\Request as Request;
use Core\ServiceLayer;
use Core\GUID;
use Core\RestController;
use CR\Widget\StatsMapper;
use Core\Types\DateTimeEx;
use Core\Collections\Iter;
use Core\Events\EventInfo;
use Core\NameCleaner;
use Core\HashSet as Set;
use \__;
use Core\Db;

class PlayersComparsionController extends RestController
{
    use Db;
    
    private $translator;
    private $request;
    private $activeTeam;
    private $actualPackageManager;

    function __construct()
    {
        $this->translator = ServiceLayer::getService('translator');
        $this->request = ServiceLayer::getService('request');
        $this->activeTeam = (object)$activeTeamManager = ServiceLayer::getService('ActiveTeamManager')->getActiveTeam();
        $this->db = $this->getDb();
        $this->actualPackageManager = ServiceLayer::getService('ActualPackageManager');
        //$this->actualPackageManager->checkPermissions();
    }

    private function getTeamPlayers($teamId)
    {
        return iterator_to_array((function($teamPlayers)
        {
            $getterMapper = ServiceLayer::getService('GetterMapper');
            foreach ($teamPlayers as $teamPlayer)
            {
                $tp = $getterMapper->mapToArray($teamPlayer, ['dataMapperRules', 'photo']);
                $tp['fullName'] = NameCleaner::clean("{$tp['firstName']} {$tp['lastName']}");
                yield $tp;
            }
        })(ServiceLayer::getService('TeamPlayerManager')->fulltextFindPlayer("", $teamId)));
    }

    private function getCurrentSeasonRange()
    {
        $seasonManager = ServiceLayer::getService('TeamSeasonManager');
        $teamManager = ServiceLayer::getService('TeamManager');
        $teamManager->setTranslator($this->translator);
        $team = $teamManager->findTeamById($this->activeTeam->id);
        
        $currentSeason = $seasonManager->getTeamCurrentSeason($team);
        if(null != $currentSeason)
        {
             return [
                    new DateTimeEx($currentSeason->getStartDate()),
                    new DateTimeEx($currentSeason->getEndDate()),
                    $currentSeason
                ];
        }
       
    }

    private function getTeamPlayer($teamId, $userId)
    {
        return $this->db->team_players()
            ->where('team_id', $teamId)
            ->where('player_id', $userId)
            ->fetch();
    }

    function teamPlayersAction()
    {
        $teamManager = ServiceLayer::getService('TeamManager');
        $teamManager->setTranslator($this->translator);
        $teamPlayers = $this->getTeamPlayers($this->activeTeam->id);
        usort($teamPlayers, function($a, $b)
        {
            return strcmp($a['fullName'], $b['fullName']);
        });
        $user = ServiceLayer::getService('security')->getIdentity()->getUser();
        $currentTeamPlayer = $this->getTeamPlayer($this->activeTeam->id, $user->getId());
        $result = [
            ['avg', 'TEAM'],
            null,
        ];
        foreach ($teamPlayers as $teamPlayer)
        {
            $teamPlayerId = $teamPlayer['id'];
            if ($teamPlayerId == $currentTeamPlayer['id'])
            {
                $result[1] = [$teamPlayerId, $teamPlayer['fullName'],$teamPlayer['fullName']];
                //$result[1] = [$teamPlayerId, $this->getTranslator()->translate('ME'), $teamPlayer['fullName']];
            }
            else
            {
                $result[] = [$teamPlayerId, $teamPlayer['fullName']];
            }
        }
        ob_end_clean();
        
        $seasonRange = $this->getCurrentSeasonRange();
        
        if(null != $seasonRange)
        {
             list($dateFrom, $dateTo, $season) = $seasonRange;
             $seasonName = $season->getName();
        }
        else
        {
            $dateFrom = new DateTimeEx();
            $dateTo = new DateTimeEx();
            $season = '';
            $seasonName = '';
        }
        
        $response = array();
        
        $response['seasonName'] = $seasonName;
        
        $response['players'] = $result;
        
        return $this->asJson($response);
    }

    private function getResult($personalForm, $teamPlayerId)
    {
        if ($teamPlayerId === 'avg') return $personalForm['avg'] + ['teamPlayerId' => 'avg'];
        else
        {
            foreach ($personalForm as $value)
            {
                if ($value['teamPlayerId'] == $teamPlayerId)
                {
                    return $value;
                }
            }
        }
    }

    function playersComparsionAction()
    {
        $options = new \stdClass();
        $interval = isset($_GET['interval']) ? $_GET['interval'] : 'season';
        if ($interval === "season")
        {
            list($dateFrom, $dateTo) = $this->getCurrentSeasonRange();
            $options->dateFrom = $dateFrom;
            $options->dateTo = $dateTo;
        }
        else
        {
            $options->dateFrom = DateTimeEx::now()->addDay(-30);
            $options->dateTo = DateTimeEx::now();
        }
        $personalFormManager = ServiceLayer::getService('PersonalFormManager');
        $user = ServiceLayer::getService('security')->getIdentity()->getUser();
        $teamManager = ServiceLayer::getService('TeamManager');
        $teamManager->setTranslator($this->translator);
        $teamPlayer = $this->getTeamPlayer($this->activeTeam->id, $user->getId());
        $playerIds = [$teamPlayer['id']];
        $playerParams = ['player1', 'player2'];
        foreach ($playerParams as $param)
        {
            if ($teamPlayer['id'] != $_GET[$param] && $_GET[$param] != 'avg')
            {
                $playerIds[] = $_GET[$param];
            }
        }
        $personalForm = $personalFormManager->getPersonalFormFor($playerIds, $options->dateFrom, $options->dateTo);
        $result = ['max' => $personalForm['max']];
        foreach ($playerParams as $param)
        {
            $result[$param] = $this->getResult($personalForm, $_GET[$param]);
        }
       
        return $this->asJson($result);
    }

    function seasonStatsAction()
    {
        $team = ServiceLayer::getService('TeamManager')->findTeamById($this->activeTeam->id);
        
        $seasonRange = $this->getCurrentSeasonRange();
        
        if(null != $seasonRange)
        {
             list($dateFrom, $dateTo, $season) = $seasonRange;
             $seasonName = $season->getName();
        }
        else
        {
            $dateFrom = new DateTimeEx();
            $dateTo = new DateTimeEx();
            $season = '';
            $seasonName = '';
        }
        
        $performanceManager = ServiceLayer::getService('PerformanceManager');
        $user = ServiceLayer::getService('security')->getIdentity()->getUser();
        $currentTeamPlayer = $this->getTeamPlayer($this->activeTeam->id, $user->getId());
        
        $players = (null == $this->getRequest()->get('players')) ? array() : $this->getRequest()->get('players');
        
        $teamPlayersStats = $performanceManager->getTeamPlayersStats($team, $players, $dateFrom, $dateTo);
        $teamPlayersStats->seasonName = $seasonName;
        $teamPlayersStats->teamId = $team->getId();
        
        ob_end_clean();
        return $this->asJson($teamPlayersStats);
    }
}