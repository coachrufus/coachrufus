<?php

namespace CR\Widget\Model;

use Core\EntityMapper as EntityMapper;
use Core\ServiceLayer as ServiceLayer;

class SampleEntity {

    private $mapper_rules = array(
        'id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => true
        ),
        'hit_type' => array(
            'type' => 'string',
            'max_length' => '50',
            'required' => false
        ),
    );

    public function getDataMapperRules()
    {
        return $this->mapper_rules;
    }

    protected $id;
    protected $hitType;

    public function setId($val)
    {
        $this->id = $val;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setHitType($val)
    {
        $this->hitType = $val;
    }

    public function getHitType()
    {
        return $this->hitType;
    }

}
