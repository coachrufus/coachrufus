<?php namespace CR\Widget\Model;

use Webteamer\Team\Model\TeamPlayer;
use Core\Types\DateTimeEx;
use Core\Events\EventInfo;
use Webteamer\Player\Model\PlayerStatManager;
use Core\Mappers\GetterMapper;
use Core\NameCleaner;
use Webteamer\Player\Model\PlayerStat;
use Webteamer\Player\Model\PlayerTimelineEventRepository;
use Webteamer\Team\Model\TeamEventManager;
use Webteamer\Team\Model\TeamManager;
use Core\Db;

class IndividualStatsManager
{
    use Db;
    private $db;

    function __construct()
    {
        $this->db = $this->getDb();
    }

    private function addDateFilter($sql, $tableName, $dateFrom, $dateTo)
    {
        foreach ([
            'AND_CONDITION' => "AND DATE({$tableName}.event_date) >= :dateFrom AND DATE({$tableName}.event_date) <= :dateTo"
        ] as $key => $value)
        {
            $sql = str_replace("[DATE.{$key}]",
                is_null($dateFrom) || is_null($dateTo)  ? "" : $value, $sql);
        }
        return $sql;
    }

    protected function getMaxGAData($team, $dateFrom, $dateTo)
    {
        $sql = $this->addDateFilter("
        SELECT
            lineup_id,
            lineup_player_id,
            SUM(goals) AS goals,
            SUM(assists) AS assists,
            event_date,
            team_player_id
        FROM
        (
            SELECT
                e.lineup_id,
                e.lineup_player_id,
                IF(hit_type = 'goal', 1, 0) as goals,
                IF(hit_type = 'assist', 1, 0) as assists,
                e.event_date,
                p.team_player_id
            FROM player_timeline_event e
            JOIN team_match_lineup_player p ON p.id = e.lineup_player_id
            WHERE
                e.team_id = :teamId AND
                e.lineup_player_id <> 0
                [DATE.AND_CONDITION]
        ) t
        GROUP BY
            lineup_id,
            lineup_player_id,
            event_date,
            team_player_id
        ", 'e', $dateFrom, $dateTo);
        return \Core\DbQuery::prepare($sql)
            ->bindParam('teamId', $team->getId())
            ->bindParam('dateFrom', $dateFrom->toString('Y-m-d'))
            ->bindParam('dateTo', $dateTo->toString('Y-m-d'))
            ->execute()
            ->fetchAll(\PDO::FETCH_ASSOC);
    }

    protected function getContinualData($team, $dateFrom, $dateTo)
    {
        $sql = $this->addDateFilter("
        SELECT *
        FROM player_timeline_event
        WHERE team_id = :teamId [DATE.AND_CONDITION]
        ", 'player_timeline_event', $dateFrom, $dateTo);
        $result = \Core\DbQuery::prepare($sql)
            ->bindParam('teamId', $team->getId())
            ->bindParam('dateFrom', $dateFrom->toString('Y-m-d'))
            ->bindParam('dateTo', $dateTo->toString('Y-m-d'))
            ->execute()
            ->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($result as &$row)
        {
            $row['event_date'] = DateTimeEx::parse($row['event_date'])->toString('d.m.Y');
        }
        return $result;
    }

    private function calculatePlayerContinualStats($team, $dateFrom, $dateTo, $teamPlayerData)
    {
        $continualData = $this->getContinualData($team, $dateFrom, $dateTo);

        $lineUps = [];

        foreach ($continualData as $row)
        {
            $lineUps[$row['lineup_id']]['event_date'] = $row['event_date'];
            if ($row['hit_type'] == 'goal')
            {
                $lineUps[$row['lineup_id']][$row['lineup_position']]['goals'] += 1;
            }
            if ($row['hit_type'] == 'assit')
            {
                $lineUps[$row['lineup_id']][$row['lineup_position']]['assits'] += 1;
            }
            if ($row['hit_type'] == 'mom')
            {
                $lineUps[$row['lineup_id']][$row['lineup_position']]['mom'] += 1;
            }
        }

        foreach ($lineUps as $lineUpId => &$lineUp)
        {
            if ($lineUp['first_line']['goals'] > $lineUp['second_line']['goals'])
            {
                $lineUp['first_line']['win'] = true;
                $lineUp['second_line']['win'] = false;
                $lineUp['second_line']['loose'] = false;
                $lineUp['second_line']['loose'] = true;
                $lineUp['first_line']['draw'] = false;
                $lineUp['second_line']['draw'] = false;
            }
            else if ($lineUp['first_line']['goals'] < $lineUp['second_line']['goals'])
            {
                $lineUp['first_line']['win'] = false;
                $lineUp['second_line']['win'] = true;
                $lineUp['first_line']['loose'] = true;
                $lineUp['second_line']['loose'] = false;
                $lineUp['first_line']['draw'] = false;
                $lineUp['second_line']['draw'] = false;
            }
            else
            {
                $lineUp['first_line']['win'] = false;
                $lineUp['second_line']['win'] = false;
                $lineUp['first_line']['loose'] = false;
                $lineUp['second_line']['loose'] = false;
                $lineUp['first_line']['draw'] = true;
                $lineUp['second_line']['draw'] = true;
            }
        }

        $players = [];

        foreach ($teamPlayerData as $LineUpId => &$teamPlayerLineUp)
        {
            foreach ($teamPlayerLineUp as $lineupPosition => &$teamPlayers)
            {
                foreach ($teamPlayers as &$teamPlayer)
                {
                    if ($lineUps[$LineUpId][$lineupPosition]['win'] === true)
                    {
                        $i = count($players[$teamPlayer['team_player_id']]['continual_wins']);
                        $win = &$players[$teamPlayer['team_player_id']]['continual_wins'][$i == 0 ? 0 : $i - 1];
                        if (!isset($win['start_date']))
                        {
                            $win['counter'] = 1;
                            $win['start_date'] = $lineUps[$LineUpId]['event_date'];
                            $win['end_date'] = $lineUps[$LineUpId]['event_date'];
                        }
                        else
                        {
                            $win['counter'] += 1;
                            $win['end_date'] = $lineUps[$LineUpId]['event_date'];
                        }
                    }
                    else
                    {
                        $players[$teamPlayer['team_player_id']]['continual_wins'][] = [];
                    }

                    if ($lineUps[$LineUpId][$lineupPosition]['loose'] === true)
                    {
                        $i = count($players[$teamPlayer['team_player_id']]['continual_looses']);
                        $loose = &$players[$teamPlayer['team_player_id']]['continual_looses'][$i == 0 ? 0 : $i - 1];
                        if (!isset($loose['start_date']))
                        {
                            $loose['counter'] = 1;
                            $loose['start_date'] = $lineUps[$LineUpId]['event_date'];
                            $loose['end_date'] = $lineUps[$LineUpId]['event_date'];
                        }
                        else
                        {
                            $loose['counter'] += 1;
                            $loose['end_date'] = $lineUps[$LineUpId]['event_date'];
                        }
                    }
                    else
                    {
                        $players[$teamPlayer['team_player_id']]['continual_looses'][] = [];
                    }

                    if ($lineUps[$LineUpId][$lineupPosition]['win'] === true || $lineUps[$LineUpId][$lineupPosition]['draw'] === true)
                    {
                        $i = count($players[$teamPlayer['team_player_id']]['continual_no_looses']);
                        $noLoose = &$players[$teamPlayer['team_player_id']]['continual_no_looses'][$i == 0 ? 0 : $i - 1];
                        if (!isset($noLoose['start_date']))
                        {
                            $noLoose['counter'] = 1;
                            $noLoose['start_date'] = $lineUps[$LineUpId]['event_date'];
                            $noLoose['end_date'] = $lineUps[$LineUpId]['event_date'];
                        }
                        else
                        {
                            $noLoose['counter'] += 1;
                            $noLoose['end_date'] = $lineUps[$LineUpId]['event_date'];
                        }
                    }
                    else
                    {
                        $players[$teamPlayer['team_player_id']]['continual_no_looses'][] = [];
                    }

                    if ($lineUps[$LineUpId][$lineupPosition]['loose'] === true || $lineUps[$LineUpId][$lineupPosition]['draw'] === true)
                    {
                        $i = count($players[$teamPlayer['team_player_id']]['continual_no_wins']);
                        $noWin = &$players[$teamPlayer['team_player_id']]['continual_no_wins'][$i == 0 ? 0 : $i - 1];
                        if (!isset($noWin['start_date']))
                        {
                            $noWin['counter'] = 1;
                            $noWin['start_date'] = $lineUps[$LineUpId]['event_date'];
                            $noWin['end_date'] = $lineUps[$LineUpId]['event_date'];
                        }
                        else
                        {
                            $noWin['counter'] += 1;
                            $noWin['end_date'] = $lineUps[$LineUpId]['event_date'];
                        }
                    }
                    else
                    {
                        $players[$teamPlayer['team_player_id']]['continual_no_wins'][] = [];
                    }
                }
            }
        }

        return $players;
    }

    private function calculateContinualStatsMaxes($playerContinualStats)
    {
        $maxes = [];
        foreach ($playerContinualStats as $playerId => $subjects)
        {
            foreach ($subjects as $subject => $ranges)
            {
                foreach ($ranges as $range)
                {
                    if (!isset($maxes[$subject]))
                    {
                        $maxes[$subject] = 0;
                    }
                    if ($maxes[$subject] < $range['counter'])
                    {
                        $maxes[$subject] = $range['counter'];
                    }
                }
            }
        }
        return $maxes;
    }

    private function calculateContinualStatsPlayerMaxes($playerContinualStats, $continualStatsMaxes, $players)
    {
        $result = [];
        foreach ($playerContinualStats as $playerId => $subjects)
        {
            foreach ($subjects as $subject => $ranges)
            {
                foreach ($ranges as $range)
                {
                    if ($continualStatsMaxes[$subject] == $range['counter'])
                    {
                        $range['teamPlayer'] = $players[$playerId];
                        $result[$subject][] = $range;
                    }
                }
            }
        }
        return $result;
    }

    protected function getTeamPlayerData($team, $dateFrom, $dateTo)
    {
        $sql = $this->addDateFilter("
        SELECT
            p.lineup_id,
            p.lineup_position,
            p.team_player_id,
            player_name
        FROM team_match_lineup l
        JOIN team_match_lineup_player p ON l.id = p.lineup_id
        WHERE team_id = :teamId [DATE.AND_CONDITION]
        ", 'l', $dateFrom, $dateTo);
        $result = \Core\DbQuery::prepare($sql)
            ->bindParam('teamId', $team->getId())
            ->bindParam('dateFrom', $dateFrom->toString('Y-m-d'))
            ->bindParam('dateTo', $dateTo->toString('Y-m-d'))
            ->execute()
            ->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($result as &$row)
        {
            $row['player_name'] = NameCleaner::clean($row['player_name']);
        }
        return $result;
    }

    protected function groupTeamPlayerData($teamPlayerData)
    {
        $result = [];
        foreach ($teamPlayerData as $row)
        {
            $result[$row['lineup_id']][$row['lineup_position']][] = $row;
        }
        return $result;
    }

    protected function groupTeamPlayerDataKv($teamPlayerData)
    {
        $result = [];
        foreach ($teamPlayerData as $row)
        {
            $result[$row['team_player_id']] = $row;
        }
        return $result;
    }

    protected function getPlayerPairs($teamPlayerData, $teamPlayerDataKv)
    {
        $pairs = [];
        foreach ($teamPlayerData as $key1 => $player1)
        {
            foreach ($teamPlayerData as $key2 => $player2)
            {
                if (
                    $key1 !== $key2 &&
                    $player1['lineup_id'] === $player2['lineup_id'] &&
                    $player1['lineup_position'] === $player2['lineup_position']
                )
                {
                    $pairs[$player1['team_player_id']][$player2['team_player_id']] += 1;
                }
            }
        }
        $max = 0;
        foreach ($pairs as $player1 => $players)
        {
            foreach ($players as $player2 => $count)
            {
                if ($max < $count) $max = $count;
            }
        }
        $maxPairs = [];
        foreach ($pairs as $player1 => $players)
        {
            foreach ($players as $player2 => $count)
            {
                if ($max == $count)
                {
                    if (!isset($maxPairs["{$player2}:{$player1}"]))
                    {
                        $maxPairs["{$player1}:{$player2}"] = [
                            'teamPlayer1' => $teamPlayerDataKv[$player1],
                            'teamPlayer2' => $teamPlayerDataKv[$player2],
                            'count' => $count
                        ];
                    }
                }
            }
        }
        $result = [];
        foreach ($maxPairs as $pair) $result[] = $pair;
        return $result;
    }

    protected function getIndividualInfo($team, $dateFrom, $dateTo)
    {
        $data = $this->getMaxGAData($team, $dateFrom, $dateTo);
        $teamPlayerData = $this->getTeamPlayerData($team, $dateFrom, $dateTo);
        $teamPlayerDataGrouped = $this->groupTeamPlayerData($teamPlayerData);
        $teamPlayerDataKv = $this->groupTeamPlayerDataKv($teamPlayerData);
        $maxGoals = 0;
        $maxAssists = 0;
        $maxGA = 0;
        foreach ($data as $lineupData)
        {
            if ($maxGoals['goals'] < $lineupData['goals']) $maxGoals = $lineupData['goals'];
            if ($maxAssists['assists'] < $lineupData['assists']) $maxAssists = $lineupData['assists'];
            $ga = $lineupData['goals'] + $lineupData['assists'];
            if ($maxGA < $ga)
            {
                $maxGA = $ga;
            }
        }
        $result = [
            'maxGoals' => [],
            'maxAssists' => [],
            'maxGA' => [],
        ];
        foreach ($data as $lineupData)
        {
            $lineupData['player'] = $teamPlayerDataKv[$lineupData['team_player_id']];
            if ($maxGoals['goals'] == $lineupData['goals']) $result['maxGoals'][] = $lineupData;
            if ($maxAssists['assists'] == $lineupData['assists']) $result['maxAssists'][]  = $lineupData;
            if ($maxGA == ($lineupData['goals'] + $lineupData['assists'])) $result['maxGA'][] = $lineupData;
        }

        $playerPairs = $this->getPlayerPairs($teamPlayerData, $teamPlayerDataKv);

        $playerContinualStats = $this->calculatePlayerContinualStats($team, $dateFrom, $dateTo, $teamPlayerDataGrouped);
        $continualStatsMaxes = $this->calculateContinualStatsMaxes($playerContinualStats);
        $continualStatsPlayerMaxes = $this->calculateContinualStatsPlayerMaxes($playerContinualStats, $continualStatsMaxes, $teamPlayerDataKv);
        return $result + $continualStatsPlayerMaxes + [
            'playerPairs' => $playerPairs
        ];
    }

    function getIndividualStatsFor($team, $dateFrom, $dateTo): array
    {
        return $this->getIndividualInfo($team, $dateFrom, $dateTo);
    }
}