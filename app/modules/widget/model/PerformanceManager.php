<?php namespace CR\Widget\Model;

use Webteamer\Team\Model\TeamManager;
use Webteamer\Player\Model\PlayerStatManager;
use Core\Types\DateTimeEx;
use Core\NameCleaner;

class PerformanceManager
{
    private $teamManager;
    private $playerStatManager;
    private $performanceRepository;
    private $counterFields;

    function __construct
    (
        TeamManager $teamManager,
        PlayerStatManager $playerStatManager,
        PerformanceRepository $performanceRepository
    )
    {
        $this->teamManager = $teamManager;
        $this->playerStatManager = $playerStatManager;
        $this->performanceRepository = $performanceRepository;
        $this->counterFields = ['goals', 'assists', 'CRS', 'wins', 'draws'];
    }

    private function getUpDowns($max)
    {
        if (count($max) == 0)
        {
            $result = [];
            foreach ($this->counterFields as $col)
            {
                $result[$col] = 'eq';
            }
            return $result;
        }
        else
        {
            $prev = null;
            $result = [];
            foreach ($max as $item)
            {
                foreach ($this->counterFields as $col)
                {
                    if (!isset($prev[$col]))
                    {
                        $result[$col] = 'eq';
                    }
                    else if ($prev[$col] < $item[$col])
                    {
                        $result[$col] = 'up';
                    }
                    else if ($prev[$col] > $item[$col])
                    {
                        $result[$col] = 'down';
                    }
                    else
                    {
                        $result[$col] = 'eq';
                    }
                }
                $prev = $item;
            }
            return $result;
        }
    }

    private function getLastMatch($max)
    {
        if (count($max) == 0)
        {
            $result = [];
            foreach ($this->counterFields as $col)
            {
                $result[$col] = 0;
            }
            return $result;
        }
        else
        {
            foreach ($max as $item);
            return $item;
        }
    }

    function getTeamPlayersStats($team, array $teamPlayerIds, DateTimeEx $dateFrom = null, DateTimeEx $dateTo = null)
    {
        $teamPlayers = $this->teamManager->getTeamPlayers($team->getId());
        $playersStats = $this->playerStatManager->getAllTeamPlayersStat($team, null, $dateFrom, $dateTo);
        $keys = ['goals', 'assists', 'wins', 'draws', 'CRS'];
        $total = [];
        $playersKv = [];
        $players = [];
        foreach($teamPlayers as $teamPlayer)
        {
            $tpId = $teamPlayer->getId();
            if(isset($playersStats[$tpId]))
            {
                $playersKv[$tpId]['name'] = NameCleaner::clean($teamPlayer->getFullName());
                $playersKv[$tpId]['avatar'] = $teamPlayer->getMidPhoto();
                $stats = $playersStats[$tpId];
                foreach ($keys as $key)
                {
                    if (!isset($playersKv[$tpId][$key])) $playersKv[$tpId][$key] = 0;
                    if (!isset($total[$key])) $total[$key] = 0;
                    $value = $stats->{"get{$key}"}();
                    $total[$key] += $value;
                    $playersKv[$tpId][$key] += $value;
                }
                foreach ($teamPlayerIds as $teamPlayerId)
                {
                    if ($tpId == $teamPlayerId)
                    {
                        $playersKv[$tpId]['isActive'] = true;
                    }
                }
            }
        }
        foreach ($playersKv as $playerId => $item)
        {
            $item['playerId'] = $playerId;
            $players[] = $item;
        }
        $chartData = $this->getTeamPlayersStatsByEvent($team, $teamPlayerIds, $dateFrom, $dateTo);
        $upDowns = $this->getUpDowns($chartData['max']);

        //dump($chartData['max']);
        $last = $this->getLastMatch($chartData['max']);
        //dump($last);exit;
        return (object)[
            'total' => $total,
            'players' => $players,
            'chartData' => $chartData,
            'upDowns' => $upDowns,
            'last' => $last
        ];
    }

    private function prepare($stats)
    {
        $arr = [];
        foreach ($stats as $k => $v)
        {
            $v['lineupId'] = $k;
            $arr[] = $v; 
        }
        return $arr;
    }

    function getTeamPlayersStatsByEvent($team, array $teamPlayerIds, DateTimeEx $dateFrom = null, DateTimeEx $dateTo = null)
    {
        $data = $this->performanceRepository->getPlayerTimeLineEventPlayerStat($team, $teamPlayerIds, $dateFrom, $dateTo);
        $result = [];
        foreach ($data as $key => $value)
        {
            if ($key === "avg") continue;
            if ($key === "teamPlayers")
            {
                foreach ($teamPlayerIds as $temaPlayerId)
                {
                    $result["teamPlayers"][$temaPlayerId] = $temaPlayerId === "avg" ?
                        $this->prepare($data["avg"]) : $this->prepare($data['teamPlayers'][$temaPlayerId]);
                }
            }
            else $result[$key] = $this->prepare($value);
        }
        return $result;
    }
}