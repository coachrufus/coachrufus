<?php namespace CR\Widget\Model;

use Webteamer\Team\Model\TeamPlayer;
use Core\Types\DateTimeEx;
use Core\Events\EventInfo;
use Webteamer\Player\Model\PlayerStatManager;
use Core\Mappers\GetterMapper;
use Core\NameCleaner;
use Webteamer\Player\Model\PlayerStat;
use Webteamer\Player\Model\PlayerTimelineEventRepository;
use Webteamer\Team\Model\TeamEventManager;

class TeamMatesManager
{
    private $playerStatManager;
    private $columns;
    private $timelineRepository;
    private $teamEventManager;
    
    function __construct
    (
        PlayerStatManager $playerStatManager,
        PlayerTimelineEventRepository $timelineRepository,
        TeamEventManager $teamEventManager
    )
    {
        $this->playerStatManager = $playerStatManager;
        $this->timelineRepository = $timelineRepository;
        $this->teamEventManager = $teamEventManager;
        $this->columns = [
            'goals' => 'Goals',
            'assists' => 'Assists',
            'hits' => 'Hits',
            'manOfMatch' => 'MoM',
            'gamePlayed' => 'GP',
            'wins' => 'W',
            'looses' => 'L',
            'draws' => 'D',
            'plusPoints' => 'sPlus',
            'minusPoints' => 'sMinus',
            'plusMinusDiff' => '+/-',
            'winsPrediction' => '%',
            'averageGoals' => 'avG',
            'averageAssists' => 'avA',
            'GA' => 'G+A',
            'averageGA' => 'avGA',
            'kbi' => 'KBI',
            'averageKbi' => 'avKBI', 
            'manOfMatch' => 'Man of match',
            'averageManOfMatch' => 'M%M'
        ];
    }

    private function assignStat($teamPlayer, &$teamPlayerItem, $playerMatchStat, $playerMatchResults)
    {
        $playerStat = isset($teamPlayerItem['stats']) ? $teamPlayerItem['stats'] : new PlayerStat();
        $playerStat->setGoals($playerStat->getGoals() + $playerMatchStat->getGoals());
        $playerStat->setAssists($playerStat->getAssists() + $playerMatchStat->getGoals());
        $playerStat->setHits($playerStat->getHits() + $playerMatchStat->getHits());
        $playerStat->setSaves($playerStat->getSaves() + $playerMatchStat->getSaves());
        $playerStat->setManOfMatch($playerStat->getManOfMatch() + $playerMatchStat->getManOfMatch());
        $playerStat->setGamePlayed($playerStat->getGamePlayed() + 1);
        $lineupId = $playerMatchStat->getLineupId();
        $wins = 0;
        $looses = 0;
        $draws = 0;
        $plusPoints = 0;
        $minusPoints = 0;
        //dump($playerMatchResults);
        if (isset($playerMatchResults[$lineupId]))
        {
            $match = $playerMatchResults[$lineupId];
            //dump($match);
            if(is_null($match['first_line_goals']))
            {
                $match['first_line_goals'] = 0;
            }
            if(is_null($match['second_line_goals']))
            {
                $match['second_line_goals'] = 0;
            }
            if($match['first_line_goals'] == $match['second_line_goals'])
            {
                $draws++;
            }
            else 
            {
                if($match['first_line_goals'] > $match['second_line_goals'] && $match['lineup_position'] == 'first_line')
                {
                   $wins++;
                }
                elseif($match['first_line_goals'] < $match['second_line_goals'] && $match['lineup_position'] == 'second_line')
                {
                    $wins++;
                }
                else 
                {
                    $looses++;
                }
            }
            if($match['lineup_position'] == 'first_line')
            {
                $plusPoints += $match['first_line_goals'];
                $minusPoints += $match['second_line_goals'];
            }
            elseif($match['lineup_position'] == 'second_line') 
            {
                $plusPoints += $match['second_line_goals'];
                $minusPoints += $match['first_line_goals'];
            }
            $playerStat->setWins($playerStat->getWins() + $wins);
            $playerStat->setLooses($playerStat->getLooses() + $looses);
            $playerStat->setDraws($playerStat->getDraws() + $draws);
            $playerStat->setPlusPoints($playerStat->getPlusPoints() + $plusPoints);
            $playerStat->setMinusPoints($playerStat->getMinusPoints() + $minusPoints);
        }
        $teamPlayerItem['stats'] = $playerStat;
    }
        
    private function addStats($teamPlayer, &$teamPlayersMap, $relevantSubjects, $playerMatchStat, $playerMatchResults)
    {
        foreach ($relevantSubjects as $id => $subject)
        {
            if ((int)$teamPlayer->getId() === $id) continue;
            if (!isset($teamPlayersMap[$id]['teamPlayer']))
            {
                $teamPlayersMap[$id]['teamPlayer'] = $subject;
            }
            $this->assignStat($teamPlayer, $teamPlayersMap[$id], $playerMatchStat, $playerMatchResults);
        }
    }
    
    function orderByLineUpId($playerMatchResults)
    {
        return iterator_to_array((function($playerMatchResults)
        {
            foreach ($playerMatchResults as $result)
            {
                yield $result['lineup_id'] => $result;
            }
        })($playerMatchResults));
    }
    
    private function getEventLineupPlayers($lineup)
    {
        $result = ['first_line' => [], 'second_line' => []];
        $lineupPlayers = $this->teamEventManager->getEventLineupPlayers($lineup);
        foreach ($lineupPlayers as $lineupPlayer)
        {
            $result[$lineupPlayer->getLineupPosition()][$lineupPlayer->getTeamPlayerId()] = $lineupPlayer;
        }
        return $result;
    }
    
    function getTeamMatesFor(TeamPlayer $teamPlayer, DateTimeEx $fromDate = null, DateTimeEx $toDate = null): array
    {
        $playerMatchResults = $this->orderByLineUpId($this->timelineRepository->getTeamPlayerMatchResults($teamPlayer));
        $playerMatchStats = $this->playerStatManager->getTeamPlayerAllMatchOverview($teamPlayer, $fromDate, $toDate);
        $teamPlayersMap = [];
        //dump($playerMatchResults);
        //dump($playerMatchStats);exit;
        //dump($teamPlayer);
        foreach ($playerMatchStats as $playerMatchStat)
        {
            $eventInfo = EventInfo::create($playerMatchStat->getEventId(), DateTimeEx::parse($playerMatchStat->getEventDate()));
            $lineup = $this->teamEventManager->getEventLineup($eventInfo->getEvent());
            $subjects = $this->getEventLineupPlayers($lineup);
            //dump($subjects);
            $lineupPosition = $playerMatchStat->getLineupPosition();
            //dump($playerMatchStat->getEventDate());
            //dump($eventInfo->getEvent());
            //dump($lineupPosition);
            //dump($subjects);
            if (isset($subjects[$lineupPosition]))
            {
                $this->addStats($teamPlayer, $teamPlayersMap, $subjects[$lineupPosition], $playerMatchStat, $playerMatchResults);
            }
        }
        return $teamPlayersMap;
    }
    
    private function teamPlayerToArray($tp)
    {
        return [
            'id' => $tp->getId(),
            'playerId' => $tp->getPlayerId(),
            'name' => NameCleaner::clean($tp->getPlayerName())
        ];
    }
    
    private function getStatRow($columns, $teamPlayer, $stats)
    {
        foreach ($columns as $col)
        {
            yield $col === "tp" ? $this->teamPlayerToArray($teamPlayer) :
                (is_null($stats) ? null : $stats->{"get{$col}"}());
        }
    }
    
    function getTeamMatesTableFor(TeamPlayer $teamPlayer, DateTimeEx $fromDate = null, DateTimeEx $toDate = null): array
    {
        $map = $this->getTeamMatesFor($teamPlayer, $fromDate, $toDate);
        $columns = array_keys($this->columns);
        array_unshift($columns, "tp");
        return iterator_to_array((function($columns, $map)
        {
            yield $columns;
            foreach ($map as $i)
            {
                $info = (object)$i;
                $teamPlayer = $info->teamPlayer;
                yield iterator_to_array(
                    $this->getStatRow($columns, $teamPlayer,
                        isset($info->stats) ? $info->stats : null)
                );
            }
        })($columns, $map));
    }
}