<?php namespace CR\Widget\Model;

use Core\Db;

class UIRepository
{
    use Db;
    
    private $db;
    
    function __construct()
    {
        $this->db = $this->getDb();
    }
    
    function hasSettings(int $userId, int $teamId)
    {
        return $this->getSettings($userId, $teamId) !== false;
    }
    
    function getSettings(int $userId, int $teamId)
    {
        return $this->db->ui()
            ->where('user_id', $userId)
            ->where('team_id', $teamId)
            ->fetch();
    }
    
    function addSettings(int $userId, int $teamId, string $settings)
    {
        $entity = [
            'user_id' => $userId,
            'team_id' => $teamId,
            'settings' => $settings
        ];
        $this->db->ui()->insert($entity);
        return $entity;
    }
    
    function updateSettings(int $userId, int $teamId, string $settings)
    {
        $entity = [
            'settings' => $settings
        ];
        $this->db->ui()
            ->where('user_id', $userId)
            ->where('team_id', $teamId)
            ->update($entity);
        return $entity;
    }
}