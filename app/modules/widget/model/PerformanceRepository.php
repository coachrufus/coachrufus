<?php namespace CR\Widget\Model;

use Core\Db;
use Core\Types\DateTimeEx;
use Webteamer\Player\Model\PlayerStat;

class LastKeyFinder
{
    private $keys;
    private $keyPosition;
    
    function __construct($arr)
    {
        $this->keys = array_keys($arr);
        $this->keyPosition = array_flip($this->keys);
    }
    
    function get($currentKey, $count)
    {
        $position = $this->keyPosition[$currentKey];
        $result = [];
        for($i = $position; $i > ($position - $count); $i--)
        {
            $p = $i - 1;
            $result[] = isset($this->keys[$p]) ? $this->keys[$p] : null;
        }
        return array_reverse($result);
    }
}

class PerformanceRepository
{
    use Db;
    private $db;
    private $defaults;
    private $dataDefaults;
    
    function __construct()
    {
        $this->db = $this->getDb();
        $this->defaults = [
            'goals' => 0,
            'assists' => 0,
            'hits' => 0,
            'saves' => 0,
            
        ];
        $this->dataDefaults = [
            'goals' => 0,
            'assists' => 0,
            'CRS' => 0,
            'efficiency' => 0,
            'wins' => 0,
            'draws' => 0,
        ];
    }

    private function addDateFilter($sql, $tableName, $dateFrom, $dateTo)
    {
        foreach ([
            'AND_CONDITION' => "AND DATE({$tableName}.event_date) >= :dateFrom AND DATE({$tableName}.event_date) <= :dateTo"
        ] as $key => $value)
        {
            $sql = str_replace("[DATE.{$key}]",
                is_null($dateFrom) || is_null($dateTo)  ? "" : $value, $sql);
        }
        return $sql;
    }

    protected function getPlayerTimeLineEvent($team, $dateFrom, $dateTo)
    {
        $sql = $this->addDateFilter("
        SELECT e.*, p.team_player_id
        FROM player_timeline_event e
        JOIN team_match_lineup_player p ON p.id = e.lineup_player_id
        WHERE e.team_id = :teamId AND e.lineup_player_id <> 0 [DATE.AND_CONDITION]
        ORDER BY e.lineup_id
        ", 'e', $dateFrom, $dateTo);
        $result = \Core\DbQuery::prepare($sql)
            ->bindParam('teamId', $team->getId())
            ->bindParam('dateFrom', $dateFrom->toString('Y-m-d'))
            ->bindParam('dateTo', $dateTo->toString('Y-m-d'))
            ->execute()
            ->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($result as &$row)
        {
            $row['event_date'] = DateTimeEx::parse($row['event_date'])->toString('d.m.Y');
        }
        return $result;
    }

    function addItem(&$result, $lineupId, $lineupPosition, $item)
    {
        if (!isset($result[$lineupId][$lineupPosition]))
        {
            $result[$lineupId][$lineupPosition] = $this->defaults + ['team_players' => []];
        }
        
        if(array_key_exists("{$item['hit_type']}s", $result[$lineupId][$lineupPosition]))
        {
             $result[$lineupId][$lineupPosition]["{$item['hit_type']}s"] += 1;
        }
        
       
        if (!isset($result[$lineupId][$lineupPosition]['team_players'][$item['team_player_id']]))
        {
            $result[$lineupId][$lineupPosition]['team_players'][$item['team_player_id']] = $this->defaults + ['moms' => 0];
        }
        $result[$lineupId][$lineupPosition]['team_players'][$item['team_player_id']]["{$item['hit_type']}s"] += 1;
    }

    function addWinsDrawsLooses(&$items)
    {
        foreach ($items as &$item)
        {
            if ($item['first_line']['goals'] > $item['second_line']['goals'])
            {
                $item['first_line']['result'] = 'win';
                $item['second_line']['result'] = 'loose';
            }
            else if ($item['first_line']['goals'] < $item['second_line']['goals'])
            {
                $item['first_line']['result'] = 'loose';
                $item['second_line']['result'] = 'win';
            }
            else
            {
                $item['first_line']['result'] = 'draw';
                $item['second_line']['result'] = 'draw';
            }
            foreach (['first_line', 'second_line'] as $lineupPosition)
            {
                foreach ($item[$lineupPosition]['team_players'] as &$teamPlayer)
                {
                    $teamPlayer['result'] = $item[$lineupPosition]['result'];
                }
            }
        }
    }

    private function getAllTeamPlayers($lineups) 
    {
        $result = [];
        foreach ($lineups as $lineup)
        {
            foreach ($lineup as $playerId => $item)
            {
                $result[$playerId] = true;
            }
        }
        return array_keys($result);
    }

    private function getLastGames($lineups) 
    {
        $lastKeyFinder = new LastKeyFinder($lineups);
        $teamPlayersIds = $this->getAllTeamPlayers($lineups);
        $lastGames = [];
        $default = ['goal' => 0, 'assist' => 0, 'save' => 0, 'hit' => 0];
        $copyData = function($data) use($default)
        {
            $result = [];
            foreach ($default as $key => $value)
            {
                $sourceKey = "{$key}s";
                $result[$key] = isset($data[$sourceKey]) ? (float)$data[$sourceKey] : 0.0;
            }
            return $result;
        };
        foreach ($lineups as $lineupId => $players)
        {
            $lastkeys = $lastKeyFinder->get($lineupId, 4);
            $lastkeys[] = $lineupId;
            foreach($teamPlayersIds as $teamPlayerId)
            {
                foreach ($lastkeys as $lastLineUpId)
                {
                    if ($lastLineUpId === null)
                    {
                        $lastGames[$lineupId][$teamPlayerId][] = $default;
                    }
                    else
                    {
                        $lastGames[$lineupId][$teamPlayerId][] = isset($lineups[$lastLineUpId][$teamPlayerId]) ?
                            $copyData($lineups[$lastLineUpId][$teamPlayerId]) : $default;
                    }
                }
            }
        }
        return $lastGames;
    }

    function getPlayerStats($items)
    {
        $lineups = [];
        foreach ($items as $lineupId => $lineup)
        {
            $firstLine = isset($lineup['first_line']['team_players']) ? $lineup['first_line']['team_players'] : [];
            $secondLine = isset($lineup['second_line']['team_players']) ? $lineup['second_line']['team_players'] : [];
            $lineups[$lineupId] = $firstLine + $secondLine;
        }
        $lastGames = $this->getLastGames($lineups);
        $result = [];
        foreach ($lineups as $lineupId => $teamPlayers)
        {
            foreach ($teamPlayers as $teamPlayerId => $stat)
            {
                
                $stat['mom'] = array_key_exists('mom', $stat) ? $stat['mom'] : 0;
                
                $playerStat = new PlayerStat();
                $playerStat->setWins($stat['result'] == 'win' ? 1 : 0);
                $playerStat->setLooses($stat['result'] == 'loose' ? 1 : 0);
                $playerStat->setDraws($stat['result'] == 'draw' ? 1 : 0);
                $playerStat->setGoals((float)$stat['goals']);
                $playerStat->setAssists((float)$stat['assists']);
                $playerStat->setHits((float)$stat['hits']);
                $playerStat->setSaves((float)$stat['saves']);
                $playerStat->setManOfMatch((float)$stat['mom']);
                $playerStat->setPlayerLastGames($lastGames[$lineupId][$teamPlayerId]);
                $playerStat->setGamePlayed(1);
                $result[$lineupId][$teamPlayerId] = $playerStat;
            }
        }
        return $result;
    }

    function calculateAvgData($maxData)
    {
        $result = [];
        foreach ($maxData as $lineupId => $stats)
        {
            $playerCount = $stats['playerCount'];
            foreach ($stats as $key => $value)
            {
                if ($key != "playerCount")
                {
                    $result[$lineupId][$key] = (float)$value / (float)$playerCount;
                }
            }
        }
        return $result;
    }

    function getPlayerTimeLineEventPlayerStat($team, array $teamPlayerIds, DateTimeEx $dateFrom = null, DateTimeEx $dateTo = null)
    {
        $playerTimeLineEvent = $this->getPlayerTimeLineEvent($team, $dateFrom, $dateTo);
        $result = [];
        foreach ($playerTimeLineEvent as $item)
        {
            $this->addItem($result, $item['lineup_id'], $item['lineup_position'], $item);
        }
        $this->addWinsDrawsLooses($result);
        $playerStats = $this->getPlayerStats($result);
        $maxData = [];
        $teamPlayerData = [];
        foreach ($teamPlayerIds as $teamPlayerId) $teamPlayerData[$teamPlayerId] = [];
        foreach ($playerStats as $lineupId => $teamPlayers)
        {
            $maxData[$lineupId] = $this->dataDefaults;
            foreach ($teamPlayerIds as $teamPlayerId)
            {
                $teamPlayerData[$teamPlayerId][$lineupId] = $this->dataDefaults;
            }
            foreach ($teamPlayers as $tpId => $playerStat)
            {
                $maxData[$lineupId]['goals'] += $playerStat->getGoals();
                $maxData[$lineupId]['assists'] += $playerStat->getAssists();
                $maxData[$lineupId]['wins'] += $playerStat->getWins();
                $maxData[$lineupId]['draws'] += $playerStat->getDraws();
                $maxData[$lineupId]['CRS'] += $playerStat->getCRS();
                $maxData[$lineupId]['efficiency'] += $playerStat->getLastGamesEfficiency();
                $maxData[$lineupId]['playerCount'] += 1;
                foreach ($teamPlayerIds as $teamPlayerId)
                {
                    if ($tpId == $teamPlayerId)
                    {
                        $teamPlayerData[$teamPlayerId][$lineupId]['goals'] = $playerStat->getGoals();
                        $teamPlayerData[$teamPlayerId][$lineupId]['assists'] = $playerStat->getAssists();
                        $teamPlayerData[$teamPlayerId][$lineupId]['wins'] += $playerStat->getWins();
                        $teamPlayerData[$teamPlayerId][$lineupId]['draws'] += $playerStat->getDraws();
                        $teamPlayerData[$teamPlayerId][$lineupId]['CRS'] += $playerStat->getCRS();
                        $teamPlayerData[$teamPlayerId][$lineupId]['efficiency'] += $playerStat->getLastGamesEfficiency();
                    }
                }

            }
        }
        return [
            'max' => $maxData,
            'teamPlayers' => $teamPlayerData,
            'avg' => $this->calculateAvgData($maxData)
        ];
    }
}