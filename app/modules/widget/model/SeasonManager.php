<?php namespace CR\Widget\Model;

class SeasonManager
{
    private $repository;
    
    function __construct(SeasonRepository $repository)
    {
        $this->repository = $repository;
    }
    
    public function toArray($seasons)
    {
        return iterator_to_array((function($seasons)
        {
            foreach ($seasons as $value)
            {
                yield iterator_to_array($value);
            }
        })($seasons));
    }
    
    public function getTeamNearestSeason($seasons)
    {
        $currentTime = time();
        $currentSeason  = null;
        $seasonDiffList = array();
        foreach($seasons as $season)
        {
            $startDate = new \DateTime($season['start_date']);
            $endDate = new \DateTime($season['end_date']);
            
            
            $endDiff = abs($currentTime-$endDate->getTimestamp());
            $seasonDiffList[$endDiff] = $season;
            
            $startDiff = abs($startDate->getTimestamp()-$currentTime);
            $seasonDiffList[$startDiff] = $season;
            
            
            
            if($startDate->getTimestamp() < $currentTime && $currentTime < $endDate->getTimestamp())
            {
                $currentSeason = $season;
            }
        }
        
        if(null == $currentSeason)
        {
            $nearestKeyId =  min(array_keys($seasonDiffList));
            
            $currentSeason = $seasonDiffList[$nearestKeyId];
        }
        
        return $currentSeason;
    }         
    
    function getActualSeason($seasons, $teamSettings)
    {
        if (count($seasons) === 0) return null;
        
        $season = null;
        if (isset($teamSettings->seasonId))
        {
            foreach ($seasons as $season)
            {
                if ($season['id'] == $teamSettings->seasonId)
                {
                    return $season;
                }
            }
        }
        
        if(null == $season)
        {
           $season =   $this->getTeamNearestSeason($seasons);
           return $season;
            
        }
        
        //return $seasons[0];

        /*
       
         * 
         */
    }
    
    function getSeasonsFor(int $teamId)
    {
        return $this->toArray($this->repository->findSeasonsBy(function($season) use($teamId)
        {
            return $season->where('team_id', $teamId)->order('id DESC');
        }));
    }
}