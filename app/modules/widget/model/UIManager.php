<?php namespace CR\Widget\Model;

class UISettings
{
    private $repository;
    
    function __construct(UIRepository $repository)
    {
        $this->repository = $repository;
    }
    
    function load(int $userId, int $teamId)
    {
        $entity = $this->repository->getSettings($userId, $teamId);
        return $entity === false || empty($entity['settings']) ? new \stdClass() : json_decode($entity['settings']);
    }
    
    function has(int $userId, int $teamId)
    {
        return $this->repository->hasSettings($userId, $teamId);
    }
    
    function save(int $userId, int $teamId, $settings)
    {
        if ($this->has($userId, $teamId))
        {
            $this->repository->updateSettings($userId, $teamId, json_encode($settings));
        }
        else
        {
            $this->repository->addSettings($userId, $teamId, json_encode($settings));
        }
    }
}

class UIManager
{
    private $repository;
    private $settings;
    
    function __construct(UIRepository $repository)
    {
        $this->repository = $repository;
        $this->settings = new UISettings($repository);
    }
    
    function getSettings() { return $this->settings; }
    
    function __get($name)
    {
        switch ($name)
        {
            case "settings": return $this->getSettings();
            default: throw new \Exception("Property {$name} not exists");
        }
    }
}