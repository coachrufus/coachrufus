<?php

namespace CR\Widget\Model;

use Core\DbQuery;
use Core\DbStorage;
use PDO;

class WidgetStorage extends DbStorage {

    public function getList()
    {
        $sql = 'SELECT t.id, t.hit_type  FROM ' . $this->getTableName() . ' t LIMIT 0,10 ';
        $query = \Core\DbQuery::prepare($sql);
        $data = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
        return $data;
    }

}
