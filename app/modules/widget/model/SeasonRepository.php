<?php namespace CR\Widget\Model;

use Core\Db;

class SeasonRepository
{
    use Db;
    
    private $db;
    
    function __construct()
    {
        $this->db = $this->getDb();
    }
    
    function findSeasonsBy($finder)
    {
        return $finder($this->db->team_season());
    }
}