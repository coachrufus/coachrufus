<?php namespace CR\Widget\Model;

use Core\Repository as Repository;

class WidgetRepository extends Repository
{
   public function getListAsArray()
   {
       return $this->getStorage()->getList();
   }
   
   public function getListAsEntity()
   {
       $data =  $this->getStorage()->getList();
       $list = $this->createObjectList($data);
       return $list;
   }
   
}
