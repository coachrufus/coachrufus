<?php namespace CR\Widget\Model;

use Webteamer\Team\Model\TeamPlayer;
use Core\Types\DateTimeEx;
use Core\Events\EventInfo;
use Webteamer\Player\Model\PlayerStatManager;
use Core\Mappers\GetterMapper;
use Core\NameCleaner;
use Webteamer\Player\Model\PlayerStat;
use Webteamer\Player\Model\PlayerTimelineEventRepository;
use Webteamer\Team\Model\TeamEventManager;
use Webteamer\Team\Model\TeamManager;
use Core\Db;

class SeasonStatsManager
{
    use Db;
    private $db;

    function __construct()
    {
        $this->db = $this->getDb();
    }

    private function addDateFilter($sql, $tableName, $dateFrom, $dateTo)
    {
        foreach ([
            'AND_CONDITION' => "AND DATE({$tableName}.event_date) >= :dateFrom AND DATE({$tableName}.event_date) <= :dateTo"
        ] as $key => $value)
        {
            $sql = str_replace("[DATE.{$key}]",
                is_null($dateFrom) || is_null($dateTo)  ? "" : $value, $sql);
        }
        return $sql;
    }

    protected function getSeasonData($team, $dateFrom, $dateTo)
    {
        $sql = $this->addDateFilter("
            SELECT
                lineup_id,
                SUM(if(hit_type = 'goal', 1, 0)) AS goals,
                lineup_position
            FROM player_timeline_event
            WHERE team_id = :teamId [DATE.AND_CONDITION]
            GROUP BY lineup_id, lineup_position
        ", 'player_timeline_event', $dateFrom, $dateTo);
        $data = \Core\DbQuery::prepare($sql)
            ->bindParam('teamId', $team->getId())
            ->bindParam('dateFrom', $dateFrom->toString('Y-m-d'))
            ->bindParam('dateTo', $dateTo->toString('Y-m-d'))
            ->execute()
            ->fetchAll(\PDO::FETCH_ASSOC);
        return $data;
    }

    protected function getTeamPlayerCount($team, $dateFrom, $dateTo): int
    {
        $sql = $this->addDateFilter("
        SELECT COUNT(team_player_id) as team_player_count FROM
        (
            SELECT team_player_id
            FROM
                team_match_lineup l JOIN
                team_match_lineup_player p ON l.id = p.lineup_id
            WHERE team_id = :teamId [DATE.AND_CONDITION]
            GROUP BY team_player_id
        ) AS t", 'l', $dateFrom, $dateTo);
        $data = \Core\DbQuery::prepare($sql)
            ->bindParam('teamId', $team->getId())
            ->bindParam('dateFrom', $dateFrom->toString('Y-m-d'))
            ->bindParam('dateTo', $dateTo->toString('Y-m-d'))
            ->execute()
            ->fetchAll(\PDO::FETCH_ASSOC);
        return $data[0]['team_player_count'];
    }

    protected function getSeasonInfo($team, $dateFrom, $dateTo)
    {
        $data = $this->getSeasonData($team, $dateFrom, $dateTo);
        if (empty($data)) return null;
        $lineUps = [];
        $goalsTotal = 0;
        $goalsMax = 0;
        $goalsMin = null;
        $drawsTotal = 0;
        $goalDiffs = [];
        foreach ($data as $row)
        {
            $lineUps[$row['lineup_id']][$row['lineup_position']] += $row['goals'];
            $goalsTotal += $row['goals'];
        }
        $maxGoalDiffArr = [];
        foreach ($lineUps as &$lineUp)
        {
            if (!isset($lineUp['first_line'])) $lineUp['first_line'] = 0;
            if (!isset($lineUp['second_line'])) $lineUp['second_line'] = 0;
            $lineUp['goals'] = $lineUp['first_line'] + $lineUp['second_line'];
            if ($goalsMax < $lineUp['goals']) $goalsMax = $lineUp['goals'];
            if (is_null($golasMin) || $goalsMin > $lineUp['goals']) $goalsMin = $lineUp['goals'];
            if ($lineUp['first_line'] == $lineUp['second_line']) $drawsTotal++;
            $goalDiff = $lineUp['first_line'] - $lineUp['second_line'];
            if ($goalDiff < 0) $goalDiff *= -1;
            if ($maxGoalDiff < $goalDiff)
            {
                $curr = empty($maxGoalDiffArr) ? 9999999 : $maxGoalDiffArr[0] + $maxGoalDiffArr[1];
                if (($lineUp['first_line'] + $lineUp['second_line']) < $curr)
                {
                    $maxGoalDiffArr = [$lineUp['first_line'], $lineUp['second_line']];
                }
            }
            $goalDiffs[$goalDiff] += 1;
        }
        $goalDiffsR = []; foreach ($goalDiffs as $count => $diff) $goalDiffsR[] = [$count, $diff];
        $eventCount = count($lineUps);
        return [
            'dateFrom' => $dateFrom->toString('d.m.Y'),
            'dateTo' => $dateTo->toString('d.m.Y'),
            'eventCount' => $eventCount,
            'goalsTotal' => $goalsTotal,
            'teamPlayerCount' => $this->getTeamPlayerCount($team, $dateFrom, $dateTo),
            'goalsAvg' => number_format((float)$goalsTotal / (float)$eventCount, 2, ',', ' '),
            'goalsMax' => $goalsMax,
            'goalsMin' => is_null($golasMin) ? 0 : $goalsMin,
            'maxDiff' => empty($maxGoalDiffArr) ? null : implode(':', $maxGoalDiffArr),
            'drawsTotal' => $drawsTotal,
            'goalDiffs' => $goalDiffsR
        ];
    }

    function getSeasonStatsFor($team, $dateFrom, $dateTo)
    {
        return $this->getSeasonInfo($team, $dateFrom, $dateTo);
    }
}