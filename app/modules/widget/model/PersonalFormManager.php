<?php namespace CR\Widget\Model;

use Webteamer\Team\Model\TeamManager;
use Webteamer\Player\Model\PlayerStatManager;
use Core\Types\DateTimeEx;
use Core\NameCleaner;

class PersonalFormManager
{
    private $teamManager;
    private $playerStatManager;
    private $columns;
    
    function __construct(TeamManager $teamManager, PlayerStatManager $playerStatManager)
    {
        $this->teamManager = $teamManager;
        $this->playerStatManager = $playerStatManager;
        $this->stats = [
            'CRS' => 'CRS',
            'A' => 'assists',
            'D' => 'draws',
            'W' => 'wins',
            'G' => 'goals',
            'GP' => 'gamePlayed'
        ];
    }
    
    private function getDefaultStats()
    {
        foreach ($this->stats as $stat => $val)
        {
            yield $stat => 0;
        }
    }
    
    private function calculateAverageStats(&$sums, $playerCount)
    {
        foreach ($sums as $k => &$v)
        {
            $v = $playerCount === 0 ? 0 : $v / $playerCount;
        }
    }

    private function getPlayerKey($i)
    {
        return $i == 0 ? "player" : "player{$i}";
    }

    function getPersonalFormFor(array $teamPlayerIds, DateTimeEx $dateFrom = null, DateTimeEx $dateTo = null)
    {
        $teamPlayer = $this->teamManager->findTeamPlayerById((int)$teamPlayerIds[0]);
        $team = $this->teamManager->findTeamById($teamPlayer->getTeamId());

        $playersStats = $this->playerStatManager->getAllTeamPlayersStat($team, null, $dateFrom, $dateTo);
        $teamPlayers = $this->teamManager->getTeamPlayers($team->getId());

        $playerIdCount = count($teamPlayerIds);

        for ($i = 0; $i < $playerIdCount; $i++)
        {
            $playerKey = $this->getPlayerKey($i);
            $result[$playerKey] = iterator_to_array($this->getDefaultStats());
            $result[$playerKey]['teamPlayerId'] = (int)$teamPlayerIds[$i];
        }

        $result += [
            'avg' => iterator_to_array($this->getDefaultStats()),
            'max' => iterator_to_array($this->getDefaultStats())
        ];

        $playerCount = 0;
        foreach($teamPlayers as $tp)
        {
            $id = $tp->getId();
            if(isset($playersStats[$id]))
            {
                $stats = $playersStats[$id];
                foreach ($this->stats as $stat => $getter)
                {
                    $value = $stats->{"get{$getter}"}();
                    for ($i = 0; $i < $playerIdCount; $i++)
                    {
                        if ($teamPlayerIds[$i] == $id)
                        {
                            $playerKey = $this->getPlayerKey($i);
                            $result[$playerKey][$stat] = (float)$value;
                        }
                    }
                    $result['avg'][$stat] += (float)$value;
                    if ($result['max'][$stat] < $value)
                    {
                        $result['max'][$stat] = (float)$value;
                    }
                }
                $playerCount++;
            }
        }
        $this->calculateAverageStats($result['avg'], $playerCount);
        return $result;
    }
}