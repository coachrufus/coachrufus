<?php
namespace CR\Widget;
require_once(MODUL_DIR . '/widget/WidgetModule.php');

use Core\EntityMapper;
use Core\ServiceLayer as ServiceLayer;
use CR\Widget\Model\WidgetStorage;

require_once(MODUL_DIR . '/widget/lib/TeamMates.php');
require_once(MODUL_DIR . '/widget/lib/StatsMapper.php');
require_once(MODUL_DIR . '/widget/lib/SeasonStatsMapper.php');

$acl = ServiceLayer::getService('acl');
ServiceLayer::addService('WidgetRepository', [
    'class' => "CR\Widget\Model\WidgetRepository",
    'params' => [
        new  WidgetStorage('player_timeline_event'),
        new EntityMapper('CR\Widget\Model\SampleEntity'),
    ]
]);

ServiceLayer::addService('UIRepository', [
    'class' => "CR\Widget\Model\UIRepository",
    'params' => []
]);

ServiceLayer::addService('UIManager', [
    'class' => "CR\Widget\Model\UIManager",
    'params' => [ ServiceLayer::getService('UIRepository') ]
]);

ServiceLayer::addService('SeasonRepository', [
    'class' => "CR\Widget\Model\SeasonRepository",
    'params' => []
]);

ServiceLayer::addService('SeasonManager', [
    'class' => "CR\Widget\Model\SeasonManager",
    'params' => [ ServiceLayer::getService('SeasonRepository') ]
]);

ServiceLayer::addService('TeamMatesManager', [
    'class' => "CR\Widget\Model\TeamMatesManager",
    'params' => [
        ServiceLayer::getService('PlayerStatManager'),
        ServiceLayer::getService('PlayerTimelineEventRepository'),
        ServiceLayer::getService('TeamEventManager')
    ]
]);

ServiceLayer::addService('PersonalFormManager', [
    'class' => "CR\Widget\Model\PersonalFormManager",
    'params' => [
        ServiceLayer::getService('TeamManager'),
        ServiceLayer::getService('PlayerStatManager')
    ]
]);

ServiceLayer::addService('PerformanceRepository', [
    'class' => "CR\Widget\Model\PerformanceRepository",
    'params' => []
]);

ServiceLayer::addService('PerformanceManager', [
    'class' => "CR\Widget\Model\PerformanceManager",
    'params' => [
        ServiceLayer::getService('TeamManager'),
        ServiceLayer::getService('PlayerStatManager'),
        ServiceLayer::getService('PerformanceRepository')
    ]
]);

ServiceLayer::addService('SeasonStatsManager', [
    'class' => "CR\Widget\Model\SeasonStatsManager",
    'params' => []
]);

ServiceLayer::addService('IndividualStatsManager', [
    'class' => "CR\Widget\Model\IndividualStatsManager",
    'params' => []
]);

//routing
$router = ServiceLayer::getService('router');

$acl->allowRole('ROLE_USER', 'widget_list');
$router->addRoute('widget_list', '/widget/analytics/list', [
    'controller' => 'CR\Widget\WidgetModule:Analytics:list'
]);

$acl->allowRole('ROLE_USER', 'change_season');
$router->addRoute('change_season', '/widget/analytics/season/:team_id/:season_id', [
    'controller' => 'CR\Widget\WidgetModule:Analytics:changeSeason'
]);

$acl->allowRole('ROLE_USER', 'sandbox');
$router->addRoute('sandbox', '/widget/analytics/sandbox', [
    'controller' => 'CR\Widget\WidgetModule:Analytics:sandbox'
]);

$acl->allowRole('ROLE_USER', 'widget_data');
$router->addRoute('widget_data', '/widget/api/widget-data/:team_id', [
    'controller' => 'CR\Widget\WidgetModule:StatsRest:widgetData'
]);

$acl->allowRole('ROLE_USER', 'widget_team_mates');
$router->addRoute('widget_team_mates', '/widget/api/team-mates', [
    'controller' => 'CR\Widget\WidgetModule:Analytics:teamMates'
]);

$acl->allowRole('ROLE_USER', 'widget_personal_form');
$router->addRoute('widget_personal_form', '/widget/api/personal-form', [
    'controller' => 'CR\Widget\WidgetModule:Analytics:personalForm'
]);

$acl->allowRole('ROLE_USER', 'widget_team_players');
$router->addRoute('widget_team_players', '/widget/api/team-players', [
    'controller' => 'CR\Widget\WidgetModule:PlayersComparsion:teamPlayers'
]);

$acl->allowRole('ROLE_USER', 'widget_players_comparsion');
$router->addRoute('widget_players_comparsion', '/widget/api/players-comparsion', [
    'controller' => 'CR\Widget\WidgetModule:PlayersComparsion:playersComparsion'
]);

$acl->allowRole('ROLE_USER', 'widget_season');
$router->addRoute('widget_season', '/widget/api/season', [
    'controller' => 'CR\Widget\WidgetModule:PlayersComparsion:seasonStats'
]);

$acl->allowRole('ROLE_USER', 'change_season_rest');
$router->addRoute('change_season_rest', '/widget/api/season/:team_id/:season_id', [
    'controller' => 'CR\Widget\WidgetModule:StatsRest:changeSeason'
]);

// UI

$acl->allowRole('ROLE_USER', 'has_settings');
$router->addRoute('has_settings', '/widget/api/has-settings/:userId', [
    'controller' => 'CR\Widget\WidgetModule:WidgetRest:hasSettings'
]);

$acl->allowRole('ROLE_USER', 'load_settings');
$router->addRoute('load_settings', '/widget/api/load-settings/:userId', [
    'controller' => 'CR\Widget\WidgetModule:WidgetRest:loadSettings'
]);

$acl->allowRole('ROLE_USER', 'save_settings');
$router->addRoute('save_settings', '/widget/api/save-settings/:userId', [
    'controller' => 'CR\Widget\WidgetModule:WidgetRest:saveSettings'
]);

$acl->allowRole('ROLE_USER', 'widget_upload_image');
$router->addRoute('widget_upload_image', '/widget/api/upload-image', [
    'controller' => 'CR\Widget\WidgetModule:WidgetRest:uploadImage'
]);

$acl->allowRole('ROLE_USER', 'share_event');
$router->addRoute('share_event', '/widget/api/share-event/:eventId/:eventDate', [
    'controller' => 'CR\Widget\WidgetModule:WidgetRest:shareEvent'
]);

$acl->allowRole('ROLE_USER', 'widget_event_upload_image');
$router->addRoute('widget_event_upload_image', '/widget/api/upload-event-image', [
    'controller' => 'CR\Widget\WidgetModule:WidgetRest:uploadEventImage'
]);

