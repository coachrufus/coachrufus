<?php
namespace CR\Widget;
use Core\Mappers\ObjectListMapper;
use Core\Collections\HashSet;
use Core\NameCleaner;

class StatsMapper extends ObjectListMapper
{
    function __construct(array $columns = null)
    {
        parent::__construct($this->filterColumns([
            'ID' => null,
            'Player' => null,
            'GP' => 'getGamePlayed',
            'W' => 'getWins',
            'D' => 'getDraws',
            'L' => 'getLooses',
            '%' => 'getWinsPrediction',
            'sPlus' => 'getPlusPoints',
            'sMinus' => 'getMinusPoints',
            '+/-' => 'getPlusMinusDiff',
            'Goals' => 'getGoals',
            'Assists' => 'getAssists',
            'avG' => 'getAverageGoals',
            'avA' => 'getAverageAssists',
            'G+A' => 'getGA',
            'avGA' => 'getAverageGA',
            'KBI' => 'getKbi',
            'avKBI' => 'getAverageKbi', 
            'Saves' => 'getSaves',
            'Man of match' => 'getManOfMatch',
            'M%M' => 'getAverageManOfMatch',
            'CRS.Goals' => 'getCRSGroup|goal',
            'CRS.Assists' => 'getCRSGroup|assists',
            'CRS.Wins' => 'getCRSGroup|game_win',
            'CRS.Draws' => 'getCRSGroup|game_draw',
            'CRS.MOM' => 'getCRSGroup|man_of_match'
        ], $columns));
        $this->registerObjectMapper(function($player)
        {
            $stats = $player->getStats();
            return is_null($stats) ? null : $stats;
        });
        $this->registerItemFactory(function($iterator, $player)
        {
            $row = iterator_to_array($iterator);
            $row[0] = $player->getPlayerId();
            $row[1] = NameCleaner::clean("{$player->getFirstName()} {$player->getLastName()}");
            return $row;
        });
    }
    
    private function filterColumns($getters, $columns)
    {
        $func = function($getters, $columnSet)
        {
            foreach ($getters as $k => $v)
            {
                if ($columnSet->contains($k)) yield $k => $v; 
            }
        };
        return is_null($columns) ? $getters :
            iterator_to_array($func($getters, HashSet::of(['ID', 'Player'], $columns)));
    }
    
    function createTable($objList)
    {
        return parent::createTable($objList);
    }
}