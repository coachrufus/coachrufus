<?php namespace CR\Widget\Controller;

use Core\ServiceLayer;
use Core\Types\DateTimeEx;

class TeamMates
{
    static function persist($userWidgets, $activeTeamId)
    {
        $user = ServiceLayer::getService('security')->getIdentity()->getUser();
        $UIManager = ServiceLayer::getService('UIManager');
        $settings = $UIManager->settings->load($user->getId(), $activeTeamId);
        $settings->userWidgets = $userWidgets;
        $UIManager->settings->save($user->getId(), $activeTeamId, $settings);
    }
    
    static function load($activeTeamId)
    {
        $user = ServiceLayer::getService('security')->getIdentity()->getUser();
        $UIManager = ServiceLayer::getService('UIManager');
        $settings = $UIManager->settings->load($user->getId(), $activeTeamId);
        foreach ($settings->userWidgets as $uw)
        {
            if ($uw->name === "TeamMates") return $uw;
        }
        return null;
    }
    
    static function getList($options)
    {
        //dump($options);exit;
        $teamMatesManager = ServiceLayer::getService('TeamMatesManager');
        $teamPlayer = ServiceLayer::getService('TeamManager')->findTeamPlayerById($options->teamPlayerId);
        $fromDate = isset($options->dateFrom) ? DateTimeEx::parse("{$options->dateFrom} 00:00:00") : null;
        $toDate = isset($options->dateTo) ? DateTimeEx::parse("{$options->dateTo} 00:00:00") : null;
        return $teamMatesManager->getTeamMatesTableFor($teamPlayer, $fromDate, $toDate);
    }
}

class PersonalForm
{
    static function load($activeTeamId)
    {
        $user = ServiceLayer::getService('security')->getIdentity()->getUser();
        $UIManager = ServiceLayer::getService('UIManager');
        $settings = $UIManager->settings->load($user->getId(), $activeTeamId);
        foreach ($settings->userWidgets as $uw)
        {
            if ($uw->name === "PersonalForm") return $uw;
        }
        return null;
    }
}