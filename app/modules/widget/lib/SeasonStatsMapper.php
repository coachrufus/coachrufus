<?php
namespace CR\Widget;
use Core\Mappers\ObjectListMapper;
use Core\Collections\HashSet;

class SeasonStatsMapper
{
    private $teamPlayersById;
    
    function __construct($temaPlayers)
    {
        $this->teamPlayersById = iterator_to_array($this->groupTeamPlayersById($temaPlayers));
    }
    
    private function groupTeamPlayersById($teamPlayers)
    {
        foreach ($teamPlayers as $teamPlayer)
        {
            yield $teamPlayer->getPlayerId() => $teamPlayer;
        }
    }
    
    function getSeasonStats($teamPlayersStatsByDate, $columnMapper)
    {
        $seasonStats = [];
        foreach ($teamPlayersStatsByDate as $playerId => $dates)
        {
            $player = $this->teamPlayersById[$playerId];
            $seasonStats[$playerId] = [
                'player' => [
                    'id' => $playerId,
                    'name' => trim("{$player->getFirstName()} {$player->getLastName()}")
                ],
                'items' => []
            ];
            foreach ($dates as $date => $stats)
            {
                $seasonStats[$playerId]['items'][$date] = $columnMapper($stats);
            }
        }
        return $seasonStats;
    }
}