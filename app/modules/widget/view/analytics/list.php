<?php Core\Layout::getInstance()->startSlot('stylesheet') ?>  
<link rel="stylesheet" href="/dev/plugins/datatables/dataTables.bootstrap.css">
<?php Core\Layout::getInstance()->endSlot('stylesheet') ?>

<?php Core\Layout::getInstance()->startSlot('team_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/Team/view/team/_team_menu.php', array('team' => $team)) ?>
<?php Core\Layout::getInstance()->endSlot('team_menu') ?>
<input type="hidden" id="team-id" value="<?php echo $team->getId() ?>">
<section class="content-header">
    <h1><?php echo $translator->translate('Analytics') ?></h1>
     <?php $layout->includePart(GLOBAL_DIR.'/templates/parts/_breadcrumb.php',array(
        'crumbs' => array(
            'Teams' => $router->link('teams_overview'),
            $team->getName() => $router->link('team_overview',array('id' =>$team->getId() )),
            'Analytics' => ''
            ))) ?>
</section>
<div class="col-md-12" style="margin-top: 20px">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">
                    <?php echo $translator->translate('Season') ?>
                </a>
            </div>
            
         
           
                <form class="navbar-form navbar-left" role="search">
                    <div class="form-group">
                        <div class="dropdown" id="seasonDropdownMenu">
                            <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                <span class="value"><?php echo is_null($actualSeason) ? $translator->translate('Season') : $actualSeason['fullName']; ?></span>&nbsp;&nbsp;<span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                <?php $isFirstSeason = true; foreach ($seasons as $season): ?>
                                    <li><a href="#" class="change-sesason-trigger" data-value="<?php echo $season['id'] ?>"><?php echo $season['fullName']; ?></a></li>
                                    <?php if ($isFirstSeason): ?>
                                        <?php if (count($seasons) > 1): ?>
                                            <li role="separator" class="divider"></li>
                                        <?php endif; ?>
                                        <?php $isFirstSeason = false; ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                </form>
                <span id="seasonLoading" class="loading"></span>
            </div>
        </div>
    </nav>
</div>

<section class="content" id="charts-container"></section>
<?php $layout->startSlot('javascript') ?>
<?php $layout->addJavascript('js/fb.js') ?>
<script>
    window.UISettings = <?php echo json_encode($UISettings) ?>;
    window.lang = '<?php echo $lang ?>';
    window.actualSeason = <?php echo json_encode($actualSeason) ?>;
    window.teamId = <?php echo $team->getId() ?>;
    window.teamPlayers = <?php echo json_encode($teamPlayers) ?>;
    window.teamMates = <?php echo json_encode($teamMates) ?>;
    window.personalForm = <?php echo json_encode($personalForm) ?>;
    window.seasons = <?php echo json_encode($seasons) ?>;
    
    window.seasonStats = <?php echo json_encode($seasonStats) ?>;
    window.individualStats = <?php echo json_encode($individualStats) ?>;
    window.packageProducts = <?php echo $packageProducts; ?>;
    
    
    $('.change-sesason-trigger').on('click',function(){
        location.reload();
    });
</script> 
<script src="/js/Lib/jquery.flot.js"></script> 
<script src="/js/Lib/jquery.flot.pie.js"></script> 
<script src="/js/Lib/jquery.flot.stack.js"></script> 
<script src="/js/Lib/jquery.flot.tooltip.min.js"></script>
<script src="/js/Lib/JUMFlot.min.js"></script>


<script src="/js/plugins/daterangepicker/moment.min.js"></script> 
<script src="/js/plugins/daterangepicker/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="/js/plugins/daterangepicker/daterangepicker-bs3.css"></link>
<?php $layout->endSlot('javascript') ?>