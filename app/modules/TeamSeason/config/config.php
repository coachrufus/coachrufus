<?php
namespace CR\TeamSeason;
require_once(MODUL_DIR.'/TeamSeason/TeamSeasonModule.php');


use Core\ServiceLayer as ServiceLayer;

ServiceLayer::addService('TeamSeasonRepository',array(
'class' => "CR\TeamSeason\Model\TeamSeasonRepository",
	'params' => array(
		new  \Core\DbStorage('team_season'),
		new \Core\EntityMapper('CR\TeamSeason\Model\TeamSeason'),
	)
));

ServiceLayer::addService('TeamSeasonManager',array(
'class' => "CR\TeamSeason\Model\TeamSeasonManager",
	'params' => array(
		ServiceLayer::getService('TeamSeasonRepository'),
	)
));


$acl = ServiceLayer::getService('acl');


//routing
$router = ServiceLayer::getService('router');





$acl->allowRole('ROLE_USER','team_add_season');
$router->addRoute('team_add_season','/team-season/add',array(
		'controller' => 'CR\TeamSeason\TeamSeasonModule:Crud:add'
));

$acl->allowRole('ROLE_USER','team_create_season');
$router->addRoute('team_create_season','/team-season/create',array(
		'controller' => 'CR\TeamSeason\TeamSeasonModule:Crud:create'
));


$acl->allowRole('ROLE_USER','team_season_list');

$router->addRoute('team_season_list','/team-season/list/:team_id',array(
		'controller' => 'CR\TeamSeason\TeamSeasonModule:Crud:list'
));

$acl->allowRole('ROLE_USER','team_season_edit');
$router->addRoute('team_season_edit','/team-season/edit/:id/:team_id',array(
		'controller' => 'CR\TeamSeason\TeamSeasonModule:Crud:edit'
));








