<?php Core\Layout::getInstance()->startSlot('team_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/Team/view/team/_team_menu.php', array('team' => $team)) ?>
<?php Core\Layout::getInstance()->endSlot('team_menu') ?>



<section class="content-header">
    <h1>
        <?php echo $team->getName() ?> - <?php echo $translator->translate('Season') ?>
    </h1>
    
     <?php $layout->includePart(GLOBAL_DIR.'/templates/parts/_breadcrumb.php',array(
        'crumbs' => array(
            'Teams' => $router->link('teams_overview'),
            $team->getName() => $router->link('team_overview',array('id' =>$team->getId() )),
            'Seasons' => $router->link('team_season_list', array('team_id' => $team->getId())),
            'Edit' => ''
            ))) ?>
    
</section>
<section class="content">
    
     <div class="box box-primary">
         <div class="box-header with-border">
             
         </div>
         <div class="box-body">

        <form role="form" id="add_season_form" action="" method="post">
        <input type="hidden" name="team_id" value="<?php echo $team->getId() ?>" />

           
             <div class="form-group col-sm-12">
                <label class="control-label"><?php echo $translator->translate($seasonForm->getFieldLabel("name")) ?><?php echo $seasonForm->getRequiredLabel('name') ?></label>
                <?php echo $seasonForm->renderinputTag("name", array('class' => 'form-control','size' => 10, 'id' => 'season_name','required' => 'required')) ?>
                 <?php echo $validator->showError("season_unique") ?>
                <?php echo $validator->showError("season_order") ?>
            </div>

                <div class="form-group col-sm-6">
                   <label class="control-label"><?php echo $translator->translate('Start date') ?><?php echo $seasonForm->getRequiredLabel('start_date') ?></label>

                   <div class="input-group">
                       <div class="input-group-addon">
                           <i class="fa fa fa-calendar season_start_date_trigger"></i>
                       </div>
                        <?php echo $seasonForm->renderinputTag("start_date", array('class' => 'form-control','size' => 10,'id' => 'season_start_date','required' => 'required', 'placeholder' => 'DD.MM.YYYY', )) ?>
                   </div>
               </div>
                   
                <div class="form-group col-sm-6">
                   <label class="control-label"><?php echo $translator->translate('End date') ?><?php echo $seasonForm->getRequiredLabel('end_date') ?></label>

                   <div class="input-group">
                       <div class="input-group-addon">
                           <i class="fa fa fa-calendar season_end_date_trigger"></i>
                       </div>
                        <?php echo $seasonForm->renderinputTag("end_date", array('class' => 'form-control','size' => 10,'id' => 'season_end_date','required' => 'required')) ?>
                   </div>
               </div>
                   
               
        
        
        
                 <button type="submit" class="btn btn-primary">Submit</button>

               
                </form>
     


         </div>
      </div>
        



</section>
<?php $layout->addStylesheet('plugins/pickadate/themes/classic.css') ?>
<?php $layout->addStylesheet('plugins/pickadate/themes/classic.date.css') ?>
<?php $layout->addJavascript('plugins/pickadate/picker.js') ?>
<?php $layout->addJavascript('plugins/pickadate/picker.date.js') ?>





<?php  $layout->startSlot('javascript') ?>
<script type="text/javascript">
    jQuery("#season_start_date").pickadate({format:'dd.mm.yyyy'});
    jQuery("#season_end_date").pickadate({format:'dd.mm.yyyy'});
    
</script>
<?php  $layout->endSlot('javascript') ?>
