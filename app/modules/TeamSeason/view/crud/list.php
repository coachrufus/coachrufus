<?php Core\Layout::getInstance()->startSlot('team_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/Team/view/team/_team_menu.php', array('team' => $team)) ?>
<?php Core\Layout::getInstance()->endSlot('team_menu') ?>



<section class="content-header">
    <h1>
        <?php echo $team->getName() ?> - <?php echo $translator->translate('Season') ?>
    </h1>
    
     <?php $layout->includePart(GLOBAL_DIR.'/templates/parts/_breadcrumb.php',array(
        'crumbs' => array(
            'Teams' => $router->link('teams_overview'),
            $team->getName() => $router->link('team_overview',array('id' =>$team->getId() )),
            'Seasons' => ''
            ))) ?>
</section>
<section class="content">
    
     <div class="box box-primary">
         <div class="box-header with-border">
             
         </div>
         <div class="box-body">
             <table class="table">
                 <thead>
                     <tr>
                         <th><?php echo $translator->translate('Season name') ?></th>
                         <th><?php echo $translator->translate('From') ?></th>
                         <th><?php echo $translator->translate('To') ?></th>
                         <th></th>
                     </tr>
                 </thead>
                 <tbody>
                     <?php foreach($list as $season): ?>
                     <tr>
                         <td><a href="<?php echo $router->link('team_season_edit',array('id' => $season->getId(),'team_id' => $team->getId())) ?>"><?php echo $season->getName() ?></a></td>
                         <td><?php echo $season->getStartDate()->format('d.m.Y') ?></td>
                         <td><?php echo $season->getFormatedEndDate()?></td>
                         <td><a href="<?php echo $router->link('team_season_edit',array('id' => $season->getId(),'team_id' => $team->getId())) ?>"><i class="fa fa-edit"></i> <?php echo $translator->translate('Edit') ?></a></td>
                        
                     </tr>
                     <?php endforeach; ?>
                 </tbody>
             </table>
             
             
         </div>
         <div class="box-footer">
             <a class="btn btn-primary" href="<?php echo $router->link('team_create_season',array('team_id' => $team->getId())) ?>"><?php echo $translator->translate('Create season') ?></a>
         </div>
      </div>
        



</section>