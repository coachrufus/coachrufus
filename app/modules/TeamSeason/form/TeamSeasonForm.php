<?php
namespace CR\TeamSeason\Form;
use \Core\Form as Form;

class TeamSeasonForm extends Form 
{
	protected $fields = array (
  'id' => 
  array (
    'type' => 'int',
    'max_length' => '11',
    'required' => true,
  ),
  'team_id' => 
  array (
    'type' => 'int',
    'max_length' => '11',
    'required' => false,
  ),
  'name' => 
  array (
    'type' => 'string',
    'max_length' => '100',
    'required' => true,
     'label' => 'Season name'
  ),
  'start_date' => 
  array (
    'type' => 'datetime',
    'max_length' => '',
    'required' => true,
  ),
  'end_date' => 
  array (
    'type' => 'datetime',
    'max_length' => '',
    'required' => true,
  ),
);
				
	
}