<?php
namespace CR\TeamSeason\Validator;
class SeasonValidator extends \Core\Validator
{
    
    
    public function validateDateRange($existSeasons,$newSeason)
    {
        $from = $newSeason->getStartDate();
        $to = $newSeason->getEndDate();
        $translator = \Core\ServiceLayer::getService('translator');
        $result = true;
        
        foreach($existSeasons as $season)
        {
        if($newSeason->getId() != $season->getId() && $season->getEndDate()->format('Y-m-d') != '2500-01-01')
            {
                
            


                //interva in
                if($from >  $season->getStartDate() && $to < $season->getEndDate()  )
                {
                    $result =  false;
                }
                //interval out
                if($from <  $season->getStartDate() && $to > $season->getEndDate()  )
                {
                    $result = false;
                }
                //left out
                if($to >  $season->getStartDate() && $to < $season->getEndDate()  )
                {
                    $result = false;
                }
                 //right out
                if($from  >  $season->getStartDate() && $from < $season->getEndDate()  )
                {
                    $result = false;
                }
                
                if($from ==  $season->getStartDate() && $to == $season->getEndDate() )
                {
                    $result = false;
                }
            }
        }
        
        if($result == false)
        {
            
            
            $this->addErrorMessage('season_unique', $translator->translate('SEASON.VALIDATOR.UNIQUE_MESAGE'));
        }
        
        if(null != $from && $to != null)
        {
            if($from->getTimestamp() > $to->getTimestamp() )
            {
                $this->addErrorMessage('season_order', $translator->translate('SEASON.VALIDATOR.ORDER_MESAGE'));
            }
        }
        
        
    }
    
}
