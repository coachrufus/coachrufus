<?php
namespace CR\TeamSeason\Controller;

use AclException;
use Core\Controller as Controller;
use Core\ControllerResponse;
use Core\ServiceLayer;
use Core\Validator;
use CR\TeamSeason\Form\TeamSeasonForm;
use CR\TeamSeason\Model\TeamSeason;


class CrudController extends Controller {
	
	public function addAction()
	{
            $request = ServiceLayer::getService('request');
            $teamManager = ServiceLayer::getService('TeamManager');
            $team = $teamManager->findTeamById($request->get('team_id'));
            $this->checkSettingTeamPermission($team);

            $data = $request->get('record');
            $form = new TeamSeasonForm();
            $season = new TeamSeason();
            $season->setTeamId($team->getId());
             
            $form->setEntity($season);
            $form->bindData($data);	
            
            $validator = new \CR\TeamSeason\Validator\SeasonValidator();
            $validator->setRules($form->getFields());
            $validator->setData($data);
            $validator->validateData();

            
            $repository = ServiceLayer::getService('TeamSeasonRepository');
            $list = $repository->findBy(array('team_id' => $team->getId()));
            $entity = $form->getEntity();
            
            $validator->validateDateRange($list,$entity);

            $layout = ServiceLayer::getService('layout');
            $layout->setTemplate(null);

            if(!$validator->hasErrors())
            {
                    $seasonId = $repository->save($season);
                    $result = array('result' => 'SUCCESS','value' => $seasonId,'name' =>  $season->getName());

            }
            else
            {
                $result = array('result' => 'ERROR','errors' => $validator->getErrors());
            }

            $response = new ControllerResponse();
            $response->setStatus('success');
            $response->setContent(json_encode($result));
            $response->setType('json');
            return $response;
            
            
            
            
           
	}
        
        
        public function listAction()
	{
                $request = ServiceLayer::getService('request');
                $repository = ServiceLayer::getService('TeamSeasonRepository');
                $teamManager = ServiceLayer::getService('TeamManager');
                $team = $teamManager->findTeamById($request->get('team_id'));
                $this->checkSettingTeamPermission($team);
		$list = $repository->findBy(array('team_id' => $team->getId()));
                ServiceLayer::getService('security')->getIdentity()->checkTeamAccess($team);
                
                return $this->render('CR\TeamSeason\TeamSeasonModule:crud:list.php', array(
                    'list' => $list,
                    'team' => $team
                ));
	}
        
        public function editAction()
	{
		$request = ServiceLayer::getService('request');
                $router = ServiceLayer::getService('router');
		$repository = ServiceLayer::getService('TeamSeasonRepository');
		$translator = ServiceLayer::getService('translator');
                $teamManager = ServiceLayer::getService('TeamManager');
                $team = $teamManager->findTeamById($request->get('team_id'));
		$this->checkSettingTeamPermission($team);
		$season = $repository->find($request->get('id'));
		$form = new  TeamSeasonForm();
		$form->setEntity($season);
		
		$validator = new \CR\TeamSeason\Validator\SeasonValidator();
		$validator->setRules($form->getFields());
                
                $list = $repository->findBy(array('team_id' => $team->getId()));

		if('POST' == $request->getMethod())
		{
			$post_data = $request->get('record');
			$form->bindData($post_data);
			
			$validator->setData($post_data);
			$validator->validateData();
                        
                        $entity = $form->getEntity();
                        $validator->validateDateRange($list,$entity);
			
			if(!$validator->hasErrors())
			{
				$repository->save($entity);
				$request->addFlashMessage('season_edit_success', $translator->translate('season.crud_edit.success'));
				$request->redirect($router->link('team_season_list',array('team_id' => $team->getId())));
			}
		}
	

	
                return $this->render('CR\TeamSeason\TeamSeasonModule:crud:edit.php', array(
                                              'seasonForm' => $form,
                                              'request' => $request,
                                              'validator' => $validator,
                    'team' => $team
                              ));
	}
        
         public function createAction()
	{
		$request = ServiceLayer::getService('request');
                $router = ServiceLayer::getService('router');
		$repository = ServiceLayer::getService('TeamSeasonRepository');
		$translator = ServiceLayer::getService('translator');
                $teamManager = ServiceLayer::getService('TeamManager');
                $team = $teamManager->findTeamById($request->get('team_id'));
		 $this->checkSettingTeamPermission($team);
		$season = new TeamSeason();
		$form = new  TeamSeasonForm();
		$form->setEntity($season);
                
                $list = $repository->findBy(array('team_id' => $team->getId()));
		
		$validator = new \CR\TeamSeason\Validator\SeasonValidator();
		$validator->setRules($form->getFields());
	
		if('POST' == $request->getMethod())
		{
			$post_data = $request->get('record');
			$form->bindData($post_data);
			$season->setTeamId($request->get('team_id'));
			$validator->setData($post_data);
			$validator->validateData();
                        
                        //get other seasons range
                        $entity = $form->getEntity();
                        $validator->validateDateRange($list,$entity);

			if(!$validator->hasErrors())
			{
				$entity = $form->getEntity();
				$repository->save($entity);
				$request->addFlashMessage('season_edit_success', $translator->translate('season.crud_edit.success'));
				$request->redirect($router->link('team_season_list',array('team_id' => $team->getId())));
			}
		}
	

	
                return $this->render('CR\TeamSeason\TeamSeasonModule:crud:create.php', array(
                    'seasonForm' => $form,
                    'request' => $request,
                    'validator' => $validator,
                    'team' => $team
               ));
	}
        
       
	
        private function checkSettingTeamPermission($team)
        {
            if( !ServiceLayer::getService('security')->getIdentity()->hasTeamPermission('manageSettings',$team))
            {
                 throw  new AclException();
            }
        }
}