<?php
	
namespace CR\TeamSeason\Model;
use Core\EntityMapper as EntityMapper;
use Core\ServiceLayer as ServiceLayer;
		
class TeamSeason {	

	
	
		
private $repository;
private $mapper_rules  = array(
	'id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => true
							
					),
'team_id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => false
							
					),
'name' => array(
					'type' => 'string',
					'max_length' => '100',
					'required' => false
							
					),
'start_date' => array(
					'type' => 'datetime',
					'max_length' => '',
					'required' => false
							
					),
'end_date' => array(
					'type' => 'datetime',
					'max_length' => '',
					'required' => false
							
					),

);
	
public function getDataMapperRules()
{
	return $this->mapper_rules;
}
	
		
protected $id;

protected $teamId;

protected $name;

protected $startDate;

protected $endDate;

				
public function setId($val){$this->id = $val;}

public function getId(){return $this->id;}
	
public function setTeamId($val){$this->teamId = $val;}

public function getTeamId(){return $this->teamId;}
	
public function setName($val){$this->name = $val;}

public function getName(){return $this->name;}
	
public function setStartDate($val){$this->startDate = $val;}

public function getStartDate(){return $this->startDate;}
	
public function setEndDate($val){$this->endDate = $val;}

public function getEndDate(){return $this->endDate;}

public function getFormatedEndDate()
{
    if($this->getEndDate()->format('Y') == 2500)
    {
        return 'N/A';
    }
    else
    {
        return $this->getEndDate()->format('d.m.Y');
    }
}

}

		