<?php
namespace CR\TeamSeason\Model;

use Core\Manager;
use Core\ServiceLayer;

class TeamSeasonManager extends Manager {
    
    public function getEventFormChoices($team)
    {
        $data = $this->getRepository()->findBy(array('team_id' => $team->getId()));
        $options = array();
        foreach($data as $d)
        {
            if($d->getEndDate()->format('Y') == 2500)
            {
                $name =  $d->getName().' ('.$d->getStartDate()->format('d.m.y').' - N/A)';
            }
            else 
            {
                $name =  $d->getName().' ('.$d->getStartDate()->format('d.m.y').'-'.$d->getEndDate()->format('d.m.y').')';
            }
            
            
            $options[$d->getId()] = $name;
        }
        
        return $options;
    }
    
    public function getSeasonById($id)
    {
        return $this->getRepository()->find($id);
    }    

    public function createTeamDefaultSeason($team)
    {
         $season = new TeamSeason();
         $season->setName('Default');
         $season->setTeamId($team->getId());
         $startDate = new \DateTime();
         $endDate = new \DateTime('2500-01-01');
         $season->setStartDate($startDate);
         $season->setEndDate($endDate);
         $this->getRepository()->save($season);
    }
    

    public function calculateSeasonBilling($season, $team, $costsPerAttendance = null)
    {
        $security = ServiceLayer::getService('security');
        $teamManager = ServiceLayer::getService('TeamManager');
        $eventManager = ServiceLayer::getService('TeamEventManager');
        
        $members = $teamManager->getTeamPlayers($team->getId());
        
        $to = $season->getEndDate()->format('d.m.Y');
        if( $season->getEndDate()->format('Y') > date('Y')+5 )
        {
            $to = '31.12.'.(date('Y')+5);
        }

        
        $eventsList = $eventManager->getTeamEvents(
            $team,
            array('from' => $season->getStartDate()->format('d.m.Y'),
                  'to'   =>$to)
        );

        $attendanceMatrix = $eventManager->getAttendanceBillingMatrix($members,$eventsList,$security->getIdentityUser());
        

        // setup bill info structure
        $bill = [
            'team' => [
                'events' => 0,
                'costs' => 0,
                'fees' => 0,
                'attendance' => 0,
                'balance' => 0
            ],
            'players' => []
        ];
             
        // team costs - sum of all events costs
        foreach ($attendanceMatrix['eventMatrix'] as $eventId => $event)
        {
            $bill['team']['costs'] += $event['costs'];

            if (floatval($event['costs'])>floatval(0))
            {
                $bill['team']['events']++;
            }
        }

        // for each player sum attendance and fees he/she paid
        foreach ($attendanceMatrix['players'] as $playerId => $player)
        {
            $bill['players'][$playerId]['name'] = $player['player'];
            $bill['players'][$playerId]['fees'] = 0;
            $bill['players'][$playerId]['attendance'] = 0;
            
            foreach ($player['events'] as $eventId => $event)
            {
                $bill['players'][$playerId]['fees'] += $event['fee'];
                if ($event['status'] === "1")
                {
                    $bill['players'][$playerId]['attendance']++;
                }
            }
            
            // team attendance and fees summing
            $bill['team']['attendance'] += $bill['players'][$playerId]['attendance'];
            $bill['team']['fees'] += $bill['players'][$playerId]['fees'];
        }
 
        // calculate costsPerAttendance
        $bill['team']['costsPerAttendanceCalculated'] = 0;
        if ($bill['team']['attendance'] > 0)
        {
            $bill['team']['costsPerAttendanceCalculated'] = round($bill['team']['costs'] / $bill['team']['attendance'],2);
        }
        
        // use fixed or calculated costPerAttendance
        if (is_null($costsPerAttendance)) 
        {
            $bill['team']['costsPerAttendance'] = $bill['team']['costsPerAttendanceCalculated'];
        }
        else
        {
            $bill['team']['costsPerAttendance'] = floatval($costsPerAttendance);
        }

       
        
        // calculate balances for players
        foreach ($attendanceMatrix['players'] as $playerId => $player)
        {
            $bill['players'][$playerId]['balance']
                    = round($bill['players'][$playerId]['fees'] 
                    - $bill['players'][$playerId]['attendance'] 
                    * $bill['team']['costsPerAttendance'],2);
        }
        // calculate balance for whole team
        $bill['team']['balance'] 
                =round( $bill['team']['fees'] 
                - $bill['team']['attendance'] 
                * $bill['team']['costsPerAttendance'],2);
        return $bill;
    }
    
    public function getTeamNearestSeason($seasons)
    {
        $currentTime = time();
        $currentSeason  = null;
        $seasonDiffList = array();
        foreach($seasons as $season)
        {
            $endDiff = abs($currentTime-$season->getEndDate()->getTimestamp());
            $seasonDiffList[$endDiff] = $season;
            
            $startDiff = abs($season->getStartDate()->getTimestamp()-$currentTime);
            $seasonDiffList[$startDiff] = $season;
            
            
            
            if($season->getStartDate()->getTimestamp() < $currentTime && $currentTime < $season->getEndDate()->getTimestamp())
            {
                $currentSeason = $season;
            }
        }
        
        if(null == $currentSeason && !empty($seasonDiffList))
        {
            $nearestKeyId =  min(array_keys($seasonDiffList));
            
            $currentSeason = $seasonDiffList[$nearestKeyId];
        }
        
        return $currentSeason;
    }         
    
     public function getTeamCurrentSeason($team)
    {
        $seasons  =  $this->getRepository()->findBy(array('team_id' => $team->getId()));
        $currentSeason = $this->getTeamNearestSeason($seasons);
        return $currentSeason;
    }
}

