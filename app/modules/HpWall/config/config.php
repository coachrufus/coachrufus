<?php
namespace CR\HpWall;
require_once(MODUL_DIR.'/HpWall/HpWallModule.php');


use Core\ServiceLayer as ServiceLayer;


$acl = ServiceLayer::getService('acl');


ServiceLayer::addService('HpWallManager',array(
'class' => "CR\HpWall\Manager\HpWallManager",
));


//routing
$router = ServiceLayer::getService('router');


ServiceLayer::addService('HpWallPostRepository',array(
'class' => "CR\HpWall\Model\HpWallPostRepository",
	'params' => array(
		new  Model\HpWallPostStorage('hp_wall'),
		new \Core\EntityMapper('CR\HpWall\Model\HpWallPost'),
	)
));



ServiceLayer::addService('HpWallPostCommentRepository',array(
'class' => "CR\HpWall\Model\HpWallPostCommentRepository",
	'params' => array(
		new Model\HpWallPostCommentStorage('hp_wall_post_comment'),
		new \Core\EntityMapper('CR\HpWall\Model\HpWallPostComment')
	)
));

ServiceLayer::addService('HpWallLikeRepository',array(
'class' => "CR\HpWall\Model\HpWallLikeRepository",
	'params' => array(
		new \Core\DbStorage('hp_wall_like'),
		new \Core\EntityMapper('CR\HpWall\Model\HpWallLike')
	)
));


$acl->allowRole('guest','hpwall_modul_info');

$router->addRoute('hpwall_modul_info','/hpwall/info',array(
		'controller' => 'CR\HpWall\HpWallModule:Default:info'
));






