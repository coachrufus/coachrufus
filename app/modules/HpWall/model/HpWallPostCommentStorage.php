<?php
namespace  CR\HpWall\Model;
use Core\DbStorage;
class HpWallPostCommentStorage extends DbStorage {
    
   
	
    
      
	public function findBy($query_params = array(),$options = array())
	{

		$where_criteria_items  = array();
		foreach ($query_params as $column => $column_val)
		{
                        $columnNameParts = explode('.',$column);
                        $columnNameBindName = $columnNameParts[1];
                        $columnNameTablePrefix = $columnNameParts[1];
                    
                        if(is_string($column_val))
			{
				$where_criteria_items[] = ' '.$column.'=:'.$columnNameBindName.' ';
			}
			else
			{
				$where_criteria_items[] = ' '.$column.'=:'.$columnNameBindName.' ';
			}
			
		}
		$where_criteria = ' WHERE '. implode(' AND ',$where_criteria_items);
        
                $sort_criteria = '';
                if(array_key_exists('order',$options))
                {
                    $sort_criteria = ' order by '.$options['order'];
                }
		
                 if(array_key_exists('custom_query',$options))
                {
                    $where_criteria .= $options['custom_query'] ;
                }
                
                
		//$query = \Core\DbQuery::prepare('SELECT * FROM '.$this->getTableName(). $where_criteria.$sort_criteria);
                
                        $query = \Core\DbQuery::prepare('
                           select hp.* , 
                            u.photo as "user_photo", u.name  as "user_name" , u.surname as "user_surname"
                            from hp_wall_post_comment hp
                            LEFT JOIN co_users u ON hp.author_id = u.id
                             '.$where_criteria.' '.$sort_criteria);
                
                
		foreach($query_params as $column => $value)
		{
			$columnNameParts = explode('.',$column);
                        $columnNameBindName = $columnNameParts[1];
                        $columnNameTablePrefix = $columnNameParts[1];
                    
                        if("" != $value)
			{
				$query->bindParam(':'.$columnNameBindName, $value);
			}
                        else 
                        {
                             $query->bindParam(':'.$columnNameBindName, null);
                        }
                       
		}
		$data = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
		return $data;
	}
	
	public function findOneBy($query_params = array())
	{
                $list = $this->findBy($query_params);
                
                if(!empty($list))
                {
                     return $list[0];
                }
               
                return null;
            
	}
        
        public function find($index)
	{
            $list = $this->findBy(array('hp.id' => $index));
            if(isset($list[0]))
            {
                return $list[0];
            }
            return null;
	}
	
	
}