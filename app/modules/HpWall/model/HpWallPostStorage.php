<?php
namespace  CR\HpWall\Model;
use Core\DbStorage;
class HpWallPostStorage extends DbStorage {
    
    public function clearVisibility($object)
    {

         \Core\DbQuery::prepare(' DELETE from hp_wall_visibility WHERE post_id = :id ')
		->bindParam('id', $object->getId())
		->execute();
                
    }
    
    public function getPostVisibility($object)
    {
         return \Core\DbQuery::prepare(' SELECT * from hp_wall_visibility WHERE post_id = :id ')
		->bindParam('id', $object->getId())
		->execute()->fetchAll(\PDO::FETCH_ASSOC);
         

    }
    
    /**
     * Delete preview generated widget of the same type
     */
    public function deletePreviewGeneratedWidget($object)
    {
         $bodyData = str_replace('"','\"',$object->getBodyData());
        
        \Core\DbQuery::prepare(' DELETE from hp_wall WHERE
                 type = :type
                 and author_id = :author_id
                 and team_id = :team_id 
                 and body_data = :body_datas ')
		->bindParam('type', "'".$object->getType()."'")
		->bindParam('author_id', $object->getAuthorId())
		->bindParam('team_id', $object->getTeamId())
		->bindParam('body_data', "'".$bodyData."'")
		->execute();
    }
    
    public function saveVisibility($data)
    {
                    \Core\DbQuery::insertFromArray($data, 'hp_wall_visibility');
    }
	
    
      
	public function findBy($query_params = array(),$options = array())
	{

		$where_criteria_items  = array();
		foreach ($query_params as $column => $column_val)
		{
                        $columnNameParts = explode('.',$column);
                        $columnNameBindName = $columnNameParts[1];
                        $columnNameTablePrefix = $columnNameParts[1];
                    
                        if(is_string($column_val))
			{
				$where_criteria_items[] = ' '.$column.'=:'.$columnNameBindName.' ';
			}
			else
			{
				$where_criteria_items[] = ' '.$column.'=:'.$columnNameBindName.' ';
			}
			
		}
		$where_criteria = ' WHERE '. implode(' AND ',$where_criteria_items);
        
                $sort_criteria = '';
                if(array_key_exists('order',$options))
                {
                    $sort_criteria = ' order by '.$options['order'];
                }
                
                $limitCriteria = '';
                if(array_key_exists('limit',$options))
                {
                    $limitCriteria = ' limit  '.$options['limit']['from'].','.$options['limit']['length'];
                }
		
                 if(array_key_exists('custom_query',$options))
                {
                    $where_criteria .= $options['custom_query'] ;
                }
                
                
		//$query = \Core\DbQuery::prepare('SELECT * FROM '.$this->getTableName(). $where_criteria.$sort_criteria);
                
                        $query = \Core\DbQuery::prepare('
                           select hp.* , 
                            u.photo as "user_photo", u.name  as "user_name" , u.surname as "user_surname", 
                            t.name as "team_name", t.photo as "team_photo",hpm.comments_count
                            from hp_wall_visibility hpv 
                            LEFT JOIN hp_wall hp ON hpv.post_id = hp.id
                            LEFT JOIN co_users u ON hp.author_id = u.id
                            LEFT JOIN team t ON hp.team_id  = t.id
                            LEFT JOIN (select post_id,count(id) as comments_count from hp_wall_post_comment group by post_id) hpm ON hp.id = hpm.post_id
                             '.$where_criteria.' group by hpv.post_id '.$sort_criteria.' '.$limitCriteria);
                
                
		foreach($query_params as $column => $value)
		{
			$columnNameParts = explode('.',$column);
                        $columnNameBindName = $columnNameParts[1];
                        $columnNameTablePrefix = $columnNameParts[1];
                    
                        if("" != $value)
			{
				$query->bindParam(':'.$columnNameBindName, $value);
			}
                        else 
                        {
                             $query->bindParam(':'.$columnNameBindName, null);
                        }
                       
		}
		$data = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
		return $data;
	}
	
	public function findOneBy($query_params = array())
	{
                $list = $this->findBy($query_params);
                
                if(!empty($list))
                {
                     return $list[0];
                }
               
                return null;
            
	}
        
        public function find($index)
	{
            $list = $this->findBy(array('hp.id' => $index));
            if(isset($list[0]))
            {
                return $list[0];
            }
            return null;
	}
	
	
}