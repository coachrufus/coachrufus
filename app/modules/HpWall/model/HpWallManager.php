<?php

namespace CR\HpWall\Manager;

use Core\Manager;
use Core\ServiceLayer;
use Core\Types\DateTimeEx;
use CR\HpWall\Model\HpWallPost;
use DateTime;
use const APPLICATION_PATH;
use const PUBLIC_DIR;
use function t_dump;


class HpWallManager extends Manager {

   public function generateFaceImg($sourceImg,$newwidth=80,$newheight=80)
   {
        $sourceImgParts = explode('.',$sourceImg);
        $extension = end($sourceImgParts);
        
        $mimeType = mime_content_type(APPLICATION_PATH.$sourceImg);
        
        if('image/png' == $mimeType)
        {
            $extension = 'png';
        }
        if('image/gif' == $mimeType)
        {
            $extension = 'gif';
        }
        
        if('image/jpg' == $mimeType or 'image/jpeg' == $mimeType)
        {
            $extension = 'jpg';
        }
        
        
        if('jpg' == $extension or 'jpeg' == $extension)
        {
             $goalFaceAvater = @imagecreatefromjpeg(APPLICATION_PATH.$sourceImg);
        }
        if('png' == $extension)
        {
             $goalFaceAvater = @imagecreatefrompng(APPLICATION_PATH.$sourceImg);
        }
        if('gif' == $extension)
        {
             $goalFaceAvater = @imagecreatefromgif(APPLICATION_PATH.$sourceImg);
        }
       
        $width = imagesx($goalFaceAvater);
        $height = imagesy($goalFaceAvater);
       
        $goalFace = imagecreatetruecolor($newwidth, $newheight);
        imagealphablending($goalFace, true);
        imagecopyresampled($goalFace, $goalFaceAvater, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
        $mask = imagecreatetruecolor($newwidth, $newheight);
        $transparent = imagecolorallocate($mask, 255, 0, 0);
        imagecolortransparent($mask,$transparent);
        imagefilledellipse($mask, $newwidth/2, $newheight/2, $newwidth, $newheight, $transparent);
        $red = imagecolorallocate($mask, 0, 0, 0);
        imagecopymerge($goalFace, $mask, 0, 0, 0, 0, $newwidth, $newheight, 100);
      
        imagecolortransparent($goalFace,$red);
        imagefill($goalFace, 0, 0, $red);
        return $goalFace;
   }
   
   public function getFontFile($type)
   {
       switch ($type) {
    case 'normal':
        return APPLICATION_PATH.'/img/hpWallPosts/template/montserrat.ttf';
    case 'bold':
        return APPLICATION_PATH.'/img/hpWallPosts/template/montserrat-bold.ttf';
    case 'light':
        return APPLICATION_PATH.'/img/hpWallPosts/template/montserrat-light.ttf';
    default:
        break;
}
   }
    
   public function generateTeamAfterMatchImg($lineupId)
   {
        $teamManager = ServiceLayer::getService('TeamManager');
        $teamEventManager = ServiceLayer::getService('TeamEventManager');
        $statManager = ServiceLayer::getService('PlayerStatManager');
        $translator =  ServiceLayer::getService('translator');
        $lineup = $teamEventManager->getLineupById($lineupId);
        $matchOverview = $statManager->getMatchOverview($lineup);
        $matchStats = $statManager->getMatchPlayersStat($lineup);
        $lineupPlayers = $teamEventManager->getEventLineupPlayers($lineup);
        $teamMembers = $teamManager->getTeamPlayers($lineup->getTeamId());
        $indexedTeamMembers  = array();
        foreach($teamMembers as $teamMember)
        {
            $indexedTeamMembers[$teamMember->getId()] = $teamMember;
        }
        
        $teamMembers2 = $teamManager->getTeamPlayers($lineup->getOpponentTeamId());
        foreach($teamMembers2 as $teamMember)
        {
            $indexedTeamMembers[$teamMember->getId()] = $teamMember;
        }

        

        
        $widgetData = array();
        $widgetData['firstLineGoals'] = $matchOverview->getFirstLineGoals();
        $widgetData['firstLineName'] = $matchOverview->getFirstLinename();
        $widgetData['secondLineGoals'] = $matchOverview->getSecondLineGoals();
        $widgetData['secondLineName'] = $matchOverview->getSecondLineName();
        $widgetData['goals'] = array('name'=> '','points' => 0,'total_points' => 0,'img' => '');
        $widgetData['assists'] = array('name'=> '','points' => 0,'total_points' => 0,'img' => '');
        $widgetData['mom'] = array('name'=> '','points' => '','total_points' => '','img' => '');
        

        //$widgetData['goals'] = array('name'=> '','points' => 0,'total_points' => 0,'img' => '');
        foreach ($lineupPlayers as $lineupPlayer)
        {
           if(array_key_exists($lineupPlayer->getId(), $matchStats))
           {
               $nameParts = explode(' ', $lineupPlayer->getPlayerName());
               $firstName = $nameParts[0];

               if($matchStats[$lineupPlayer->getId()]->getGoals() > $widgetData['goals']['points'])
               {
                   $widgetData['goals'] = array('name'=> $firstName,'points' => $matchStats[$lineupPlayer->getId()]->getGoals() ,'total_points' => $matchStats[$lineupPlayer->getId()]->getGoals() .'+'.$matchStats[$lineupPlayer->getId()]->getAssists(),'img' => $indexedTeamMembers[$lineupPlayer->getTeamPlayerId()]->getMidPhoto()  );
               }

               if($matchStats[$lineupPlayer->getId()]->getAssists() > $widgetData['assists']['points'])
               {
                     $widgetData['assists'] = array('name'=> $firstName,'points' => $matchStats[$lineupPlayer->getId()]->getAssists() ,'total_points' => $matchStats[$lineupPlayer->getId()]->getGoals() .'+'.$matchStats[$lineupPlayer->getId()]->getAssists(),'img' => $indexedTeamMembers[$lineupPlayer->getTeamPlayerId()]->getMidPhoto());
               }

               if($matchStats[$lineupPlayer->getId()]->getManOfMatch() == 1)
               {
                     $widgetData['mom'] = array('name'=> $firstName,'points' => 1 ,'total_points' => $matchStats[$lineupPlayer->getId()]->getGoals() .'+'.$matchStats[$lineupPlayer->getId()]->getAssists(),'img' =>  $indexedTeamMembers[$lineupPlayer->getTeamPlayerId()]->getMidPhoto());
               }
           }
          
          
        }
         //$widgetData['mom'] = $widgetData['assists'];

        //$target = APPLICATION_PATH.'/img/hpWallPosts/generated/.png';
        
        $fontFile = $this->getFontFile('normal'); 
        $fontFileBold =$this->getFontFile('bold');
        
        $im = imagecreatefrompng(APPLICATION_PATH.'/img/hpWallPosts/template/team_after.png');
        $whiteColor = imagecolorallocate($im, 255, 255, 255);
        
        imagettftext($im, 54, 0,120 , 130, $whiteColor, $fontFile, $widgetData['firstLineGoals'] );
        imagettftext($im, 20, 0,100 , 170, $whiteColor, $fontFile, $widgetData['firstLineName'] );
        imagettftext($im, 54, 0,530, 130, $whiteColor, $fontFile, $widgetData['secondLineGoals'] );
        imagettftext($im, 20, 0,500 , 170, $whiteColor, $fontFile, $widgetData['secondLineName'] );
        
        imagettftext($im, 18, 0,130 , 264, $whiteColor, $fontFile, $translator->translate('hpWallImg.Goals'));
        imagettftext($im, 18, 0,330 , 264, $whiteColor, $fontFile, $translator->translate('hpWallImg.Mom'));
        imagettftext($im, 18, 0,495 , 264, $whiteColor, $fontFile, $translator->translate('hpWallImg.Asssits'));
        
        //goals
        $goalFace = $this->generateFaceImg($widgetData['goals']['img']);
        imagecopymerge($im, $goalFace, 120,290, 0, 0, 80, 80, 100);
        $possDif1 = 160-(strlen($widgetData['goals']['name'])*8);
        $possDif2 = 160-(strlen($widgetData['goals']['total_points'])*8);
        imagettftext($im, 18, 0,$possDif1 , 404, $whiteColor, $fontFileBold, $widgetData['goals']['name']);
        imagettftext($im, 18, 0,$possDif2 , 434, $whiteColor, $fontFileBold, $widgetData['goals']['total_points']);
        
        //mom
         $momFace = $this->generateFaceImg($widgetData['mom']['img']);
        imagecopymerge($im, $momFace, 316,290, 0, 0, 80, 80, 100);
        $possDif3 = 350-(strlen($widgetData['mom']['name'])*7);
        $possDif4 = 350-(strlen($widgetData['mom']['total_points'])*5);
        imagettftext($im, 18, 0,$possDif3 , 404, $whiteColor, $fontFileBold, $widgetData['mom']['name']);
        imagettftext($im, 18, 0,$possDif4 , 434, $whiteColor, $fontFileBold, $widgetData['mom']['total_points']);
        
        //assist
        $assistFace = $this->generateFaceImg($widgetData['assists']['img']);
        imagecopymerge($im, $assistFace, 514,290, 0, 0, 80, 80, 100);
        $possDif5 = 550-(strlen($widgetData['assists']['name'])*7);
        $possDif6 = 550-(strlen($widgetData['assists']['total_points'])*5);
        imagettftext($im, 18, 0,$possDif5 , 404, $whiteColor, $fontFileBold, $widgetData['assists']['name']);
        imagettftext($im, 18, 0,$possDif6 , 434, $whiteColor, $fontFileBold, $widgetData['assists']['total_points']);

        ob_start();
        imagepng($im);
        $imgBody = ob_get_contents();
        ob_end_clean();
        imagedestroy($im);
        
        return $imgBody;
       
   }
   
    public function generateTeamLadderImg($team,$teamPlayer)
    {
        $teamManager = ServiceLayer::getService('TeamManager');
        $playerStatManager = ServiceLayer::getService('PlayerStatManager');
        $seasonManager = ServiceLayer::getService('TeamSeasonManager');
        $translator =  ServiceLayer::getService('translator');
         
        $actualSeason = $seasonManager->getTeamCurrentSeason($team);
        $seasonId = $actualSeason->getId();
        $currentSeason = $seasonManager->getSeasonById($seasonId);

        $statFrom = new DateTimeEx($currentSeason->getStartDate());
        $statTo = new DateTimeEx($currentSeason->getEndDate());
        $teamPlayersStats = $playerStatManager->getAllTeamPlayersStat($team, null, $statFrom, $statTo);
        
        $teamMembers = $teamManager->getTeamPlayers($team->getId());
        $indexedTeamMembers  = array();
        foreach($teamMembers as $teamMember)
        {
            $indexedTeamMembers[$teamMember->getId()] = $teamMember;
        }

        //t_dump($indexedTeamMembers);
        //t_dump($teamPlayersStats);
        
        $ladder = array();
        $finalLadder2 =null;
        foreach ($teamPlayersStats as $index => $teamPlayersStat)
        {
            if (is_a($teamPlayersStat, 'Webteamer\Player\Model\PlayerStat'))
            {
                $crs = $teamPlayersStat->getCRS() * 1000;
                if (array_key_exists($crs, $ladder))
                {
                    $crs = ($teamPlayersStat->getCRS() * 1000) + 1;
                }

                $ladder[$crs] = array(
                    'goals' => $teamPlayersStat->getGoals(),
                    'assists' => $teamPlayersStat->getAssists(),
                    'name' => $indexedTeamMembers[$index]->getFullName(),
                    'img' => $indexedTeamMembers[$index]->getMidPhoto(),
                    'teamPlayerId' => $indexedTeamMembers[$index]->getId(),
                );
                
                if($index == $teamPlayer->getId())
                {
                    $finalLadder2 = array(
                    'goals' => $teamPlayersStat->getGoals(),
                    'assists' => $teamPlayersStat->getAssists(),
                    'name' => $indexedTeamMembers[$index]->getFullName(),
                    'img' => $indexedTeamMembers[$index]->getMidPhoto(),
                    'teamPlayerId' => $indexedTeamMembers[$index]->getId(),
                );
                }
            }
        }
        krsort($ladder);
        $ladder = array_values($ladder);
        
      
        
        foreach($ladder as $lOrder => $lPlayer)
        {
            if($lPlayer['teamPlayerId'] == $finalLadder2['teamPlayerId'])
            {
                $finalLadder2['position'] = $lOrder+1;
            }
        }


        $finalLadder1 = $ladder[0];
        $finalLadder1['position'] = 1;
        $finalLadder3 = end($ladder);
        $finalLadder3['position'] = count($ladder);
        $template = 'ladder_gradient_2.png';
        
        if($finalLadder1['teamPlayerId'] == $finalLadder2['teamPlayerId'])
        {
            $finalLadder1 = $finalLadder2;
            $finalLadder2 = $ladder[1];
            $finalLadder2['position'] = 2;
            $template = 'ladder_gradient_1.png';
        }
        
        if($finalLadder3['teamPlayerId'] == $finalLadder2['teamPlayerId'])
        {
            $finalLadder3 = $finalLadder2;
            $finalLadder2 = $ladder[count($ladder)-2];
            $finalLadder2['position'] = count($ladder)-1;
             $template = 'ladder_gradient_3.png';
            //$finalLadder['position'] =  count($ladder)-1;
        }
        
        $finalLadder = array($finalLadder1,$finalLadder2,$finalLadder3);



        $im = imagecreatefrompng(APPLICATION_PATH . '/img/hpWallPosts/template/'.$template);
        $whiteColor = imagecolorallocate($im, 255, 255, 255);
        $greyColor = imagecolorallocate($im, 211, 211, 211);
        
        $fontFile = $this->getFontFile('normal'); 
        $fontFileBold =$this->getFontFile('bold');

        imagettftext($im, 36, 0,360-(strlen($translator->translate('hpWallImg.teamLadderTitle'))*12) , 80, $whiteColor, $fontFileBold,$translator->translate('hpWallImg.teamLadderTitle') );
        
        $face1 = $this->generateFaceImg($finalLadder[0]['img']);
        imagecopymerge($im, $face1, 200,120, 0, 0, 80, 80, 100);
        imagettftext($im, 26, 0,130, 170, $whiteColor, $fontFileBold,$finalLadder[0]['position'].'.' );
        imagettftext($im, 26, 0,325, 160, $whiteColor, $fontFile,$finalLadder[0]['name'] );
        imagettftext($im, 18, 0,325, 190, $greyColor, $fontFile,$finalLadder[0]['goals'].' '.$translator->translate('hpWallImg.teamLadder.goals')  );
        imagettftext($im, 18, 0,455, 190, $greyColor, $fontFile,$finalLadder[0]['assists'].' '.$translator->translate('hpWallImg.teamLadder.assists')  );
        
        $face2 = $this->generateFaceImg($finalLadder[1]['img']);
        imagecopymerge($im, $face2, 200,235, 0, 0, 80, 80, 100);
        
        imagettftext($im, 26, 0,130, 285, $whiteColor, $fontFileBold,$finalLadder[1]['position'].'.' );
        imagettftext($im, 26, 0,325, 275, $whiteColor, $fontFile,$finalLadder[1]['name'] );
        imagettftext($im, 18, 0,325, 305, $greyColor, $fontFile,$finalLadder[1]['goals'].' '.$translator->translate('hpWallImg.teamLadder.goals')  );
        imagettftext($im, 18, 0,455, 305, $greyColor, $fontFile,$finalLadder[1]['assists'].' '.$translator->translate('hpWallImg.teamLadder.assists')  );
        
        $face3 = $this->generateFaceImg($finalLadder[2]['img']);
        imagecopymerge($im, $face3, 200,350, 0, 0, 80, 80, 100);
        
        imagettftext($im, 26, 0,130, 400, $whiteColor, $fontFileBold,$finalLadder[2]['position'].'.' );
        imagettftext($im, 26, 0,325, 390, $whiteColor, $fontFile,$finalLadder[2]['name'] );
        imagettftext($im, 18, 0,325, 420, $greyColor, $fontFile,$finalLadder[2]['goals'].' '.$translator->translate('hpWallImg.teamLadder.goals')  );
        imagettftext($im, 18, 0,455, 420, $greyColor, $fontFile,$finalLadder[2]['assists'].' '.$translator->translate('hpWallImg.teamLadder.assists')  );
        ob_start();
        imagepng($im);
        $imgBody = ob_get_contents();
        ob_end_clean();
        imagedestroy($im);

        return $imgBody;
    }
    
    public function generatePersonalPerformancePost($teamPlayer,$team,$event)
    {
        $repo = ServiceLayer::getService('HpWallPostRepository'); 
        $teamEventManager = ServiceLayer::getService('TeamEventManager');
        $playerStatManager = ServiceLayer::getService('PlayerStatManager');
        $seasonManager = ServiceLayer::getService('TeamSeasonManager');
         $teamMatchLineupRepository = ServiceLayer::getService('TeamMatchLineupRepository');
        //$teamStatHistoryRepo = ServiceLayer::getService('TeamStatHistoryRepository');
        
          
        $currentMatch = $teamEventManager->getEventLineup($event);
        


        $currentMatchStats = $playerStatManager->getMatchPlayersStat($currentMatch);
        $crsCriteria = current($currentMatchStats)->getCRSCriteria();

        $currentMatchPlayers = $teamEventManager->getEventLineupPlayers($currentMatch);
        $currentMatchPlayerStats = array('goals' => 0,'assist' => 0);
        foreach($currentMatchPlayers as $currentMatchPlayer)
        {
            if($currentMatchPlayer->getTeamPlayerId() == $teamPlayer->getId() && array_key_exists($currentMatchPlayer->getId(), $currentMatchStats))
            {
                $currentMatchPlayerStats =   array('goals' => $currentMatchStats[$currentMatchPlayer->getId()]->getGoals(),'assist' => $currentMatchStats[$currentMatchPlayer->getId()]->getAssists()); 
            }
        }

        $previewMatch  = $teamMatchLineupRepository->findPlayerNearestPreviewMatch($event->getCurrentDate(),$teamPlayer->getId())[0];
        if(empty($previewMatch))
        {
            return;
        }
        
        
        $previewMatchStats = $playerStatManager->getMatchPlayersStat($previewMatch);
        $previewMatchPlayers = $teamEventManager->getEventLineupPlayers($previewMatch);
        $previewMatchPlayerStats = null;
        foreach($previewMatchPlayers as $previewMatchPlayer)
        {
            if($previewMatchPlayer->getTeamPlayerId() == $teamPlayer->getId())
            {
                $previewMatchPlayerStats = $previewMatchStats[$previewMatchPlayer->getId()];
            }
        }
        
        if(null== $previewMatchPlayerStats)
        {
            return;
        }

        
       
        $currentMatchPlayerPerformance = ($crsCriteria['goal']*$currentMatchPlayerStats['goals']) + ($crsCriteria['assists']*$currentMatchPlayerStats['assist']);
        $previewMatchPlayerPerformance = ($crsCriteria['goal']*$previewMatchPlayerStats->getGoals())+ ($crsCriteria['assists']*$previewMatchPlayerStats->getAssists());

      
        
        if($currentMatchPlayerPerformance > $previewMatchPlayerPerformance)
        {
            $type = 'up';
        }
        
        if($currentMatchPlayerPerformance < $previewMatchPlayerPerformance)
        {
            $type = 'down';
        }
        
        if($currentMatchPlayerPerformance == $previewMatchPlayerPerformance)
        {
            return;
        }
   
        $post = new HpWallPost();
        $post->setCreatedAt(new DateTime(date('Y-m-d H:i:s')));
        $post->setAuthorId($teamPlayer->getPlayerId());
        $post->setVisibility(array($teamPlayer->getTeamId()));
        $post->setBody('');
        $post->setTeamId($teamPlayer->getTeamId());
        $post->setStatus('proposal'); 
        $post->setType('personal_performance'); 
        $post->setBodyData(json_encode(array(
            'team_id' => $team->getId(),
            'team_name' => $team->getName(),
            'event_date' => $event->getCurrentDate(),
            'event_time' => $event->getStart()->format('H:i'),
            'event_name' => $event->getName(),
            'event_id' => $event->getId()))); 
        

        $postId = $repo->saveNew($post);
        $uploadDir  = $this->getPostDir($postId);
        $post->setId($postId);

        $img = $this->generatePersonalPerformanceImg($teamPlayer,array('currentPerformance' => $currentMatchPlayerPerformance,'previewPerformance' => $previewMatchPlayerPerformance),$type);
        
        file_put_contents( $uploadDir['absolute'].'/personal_performance_'.$type.'.png', $img);
        $post->setImg($uploadDir['relative'].'/personal_performance_'.$type.'.png');
        $repo->save($post);
        return  $uploadDir['relative'].'/personal_performance_'.$type.'.png';
    }
    
    public function generateTeamLadderImgPost($team,$teamPlayer,$event)
    {
        $repo = ServiceLayer::getService('HpWallPostRepository'); 
        $post = new HpWallPost();
        $post->setCreatedAt(new DateTime(date('Y-m-d H:i:s')));
        $post->setAuthorId($teamPlayer->getPlayerId());
        $post->setVisibility(array($teamPlayer->getTeamId()));
        $post->setBody('');
        $post->setTeamId($teamPlayer->getTeamId());
        $post->setStatus('proposal'); 
        $post->setType('team_ladder'); 
        $post->setBodyData(json_encode(array(
            'team_id' => $team->getId(),
            'team_name' => $team->getName(),
            'event_date' => $event->getCurrentDate(),
            'event_time' => $event->getStart()->format('H:i'),
            'event_name' => $event->getName(),
            'event_id' => $event->getId()))); 
        

        $postId = $repo->saveNew($post);
        $uploadDir  = $this->getPostDir($postId);
        $post->setId($postId);

        $img = $this->generateTeamLadderImg($team,$teamPlayer);
        file_put_contents( $uploadDir['absolute'].'/ladder.png', $img);
        $post->setImg($uploadDir['relative'].'/ladder.png');
        $repo->save($post);
        return  $uploadDir['relative'].'/ladder.png';
    }
    
     public function generateTeamAfterMatchImgPost($lineup)
    {
        $repo = ServiceLayer::getService('HpWallPostRepository'); 
        $router =  ServiceLayer::getService('router'); 
        $teamEventManager = ServiceLayer::getService('TeamEventManager');
        $event = $teamEventManager->findEventById($lineup->getEventId());
        
       
        
        $post = new HpWallPost();
        $post->setCreatedAt(new DateTime(date('Y-m-d H:i:s')));
        $post->setPublishedAt(new DateTime(date('Y-m-d H:i:s')));   
        $post->setAuthorId(0);
        $post->setTeamId($lineup->getTeamId());
        $post->setVisibility(array($lineup->getTeamId()));
        $post->setStatus('published'); 
        $post->setType('team_after_match'); 
        $post->setBodyData(json_encode(array(
            'lineup_id' => $lineup->getId(),
            'event_date' => $lineup->getEventDate(),
            'event_time' => $event->getStart()->format('H:i'),
            'event_name' => $event->getName(),
            'event_id' => $lineup->getEventId()))); 
        $post->setBody('');
        $postId = $repo->saveNew($post);
        $uploadDir  = $this->getPostDir($postId);
        $post->setId($postId);

        $img = $this->generateTeamAfterMatchImg($lineup->getId());
        file_put_contents( $uploadDir['absolute'].'/team_after_match.png', $img);
        $post->setImg($uploadDir['relative'].'/team_after_match.png');
        //$bodyData = json_encode(array('id' =>$lineup->getEventId(),'event_date' => $lineup->getEventDate(), 'linuep_id' => $lineup->getId()));
        //$post->setBodyData($bodyData);
        $repo->save($post);
    }
    
    public function getImgTextCenterX($img,$fontSize,$weight,$text)
    {
        $fontFile = $this->getFontFile($weight); 
        $textBox = imagettfbbox($fontSize,0,$fontFile,$text);
        $length =$textBox[2] - $textBox[0];
        
        $center = round((imagesx($img)-$length)/2);
        return $center;
      
    }
    
    
    public function generateLineupImg($lineupId)
    {
        $teamManager = ServiceLayer::getService('TeamManager');
        $teamEventManager = ServiceLayer::getService('TeamEventManager');
        $statManager = ServiceLayer::getService('PlayerStatManager');
        $translator =  ServiceLayer::getService('translator');
        $lineup = $teamEventManager->getLineupById($lineupId);
        $lineupPlayers = $teamEventManager->getEventLineupPlayers($lineup);
        $lineup->setLineupPlayers($lineupPlayers);
        $event = $teamEventManager->findEventById($lineup->getEventId());
        $fontFile = $this->getFontFile('normal'); 
        $fontFileBold =$this->getFontFile('bold');
        
        $im = imagecreatefrompng(APPLICATION_PATH.'/img/hpWallPosts/template/lineup.png');
        $whiteColor = imagecolorallocate($im, 255, 255, 255);
        
        $headCenter =  $this->getImgTextCenterX($im,34,'normal',$translator->translate('hpWallImg.lineup.lineup'));
        imagettftext($im, 34, 0,$headCenter , 80, $whiteColor, $fontFileBold, $translator->translate('hpWallImg.lineup.lineup')  );
        
        $eventInfo = $event->getName().' '.$event->getStart()->format('d.m.Y H:i');
        $eventInfoCenter =  $this->getImgTextCenterX($im,19,'normal',$eventInfo);
        imagettftext($im, 19, 0,$eventInfoCenter , 115, $whiteColor, $fontFile, $eventInfo  );
        
        
        imagettftext($im, 22, 0,210-(strlen($lineup->getFirstLineName())*11) , 186, $whiteColor, $fontFileBold, $lineup->getFirstLineName()  );
        imagettftext($im, 22, 0,530-(strlen($lineup->getSecondLineName())*11) , 186, $whiteColor, $fontFileBold, $lineup->getSecondLineName()  );
        
        
        $firstLine =  array_chunk($lineup->getLineupPlayers('first_line'),2);
        $secondLine = array_chunk($lineup->getLineupPlayers('second_line'),2);

        $leftTopOffset = 260;
        foreach ($firstLine as $row)
        {
            $leftOffset = 80;
            foreach ($row as $key => $lineupPlayer)
            {
                $playerNameParts = explode(' ', $lineupPlayer->getPlayerName());
                $playerNameDot = ($playerNameParts[1] != '') ? '.' : '';
                $playerName = $playerNameParts[0] . ' ' . mb_substr($playerNameParts[1], 0, 1) . $playerNameDot;
                imagettftext($im, 18, 0, $leftOffset, $leftTopOffset, $whiteColor, $fontFile, $playerName);
                $leftOffset += 144;
            }
            $leftTopOffset += 30;
        }
        $leftTopOffset = 260;
        foreach ($secondLine as $row)
        {
            $leftOffset = 400;
            foreach ($row as $key => $lineupPlayer)
            {
                $playerNameParts = explode(' ', $lineupPlayer->getPlayerName());
                $playerNameDot = (count($playerNameParts > 1)) ? '.' : '';
                $playerName = $playerNameParts[0] . ' ' . mb_substr($playerNameParts[1], 0, 1) . $playerNameDot;
                imagettftext($im, 18, 0, $leftOffset, $leftTopOffset, $whiteColor, $fontFile, $playerName);
                $leftOffset += 144;
            }
            $leftTopOffset += 30;
        }

      
        ob_start();
        imagepng($im);
        $imgBody = ob_get_contents();
        ob_end_clean();
        imagedestroy($im);
        
        return $imgBody;
    }
    
    public function generatePersonalPerformanceImg($teamPlayer,$stats,$type)
    {
        $translator =  ServiceLayer::getService('translator');
        $fontFile = $this->getFontFile('normal'); 
        $fontFileBold =$this->getFontFile('bold');
        $fontFileLight =$this->getFontFile('light');
        
        if('up' == $type)
        {
            $template = APPLICATION_PATH.'/img/hpWallPosts/template/personal_performance_up.png';
            $text = $translator->translate('hpWallImg.personal.performance.up');
        }
        
        if('down' == $type)
        {
            $template = APPLICATION_PATH.'/img/hpWallPosts/template/personal_performance_down.png';
            $text = $translator->translate('hpWallImg.personal.performance.down');
        }
        
        $im = imagecreatefrompng($template);
        $whiteColor = imagecolorallocate($im, 255, 255, 255);
        $blackColor = imagecolorallocate($im, 0, 0, 0);
        
        
        
         $headCenter =  $this->getImgTextCenterX($im,30,'bold',strtoupper($translator->translate('hpWallImg.personal.performance.title')) );
         imagettftext($im, 30, 0,$headCenter , 85, $whiteColor, $fontFileBold, strtoupper($translator->translate('hpWallImg.personal.performance.title')) );
         
         $subheadCenter =  $this->getImgTextCenterX($im,22,'light',$text);
        imagettftext($im, 22, 0,$subheadCenter , 120, $whiteColor, $fontFileLight, $text);
        
        $goalFace = $this->generateFaceImg($teamPlayer->getMidPhoto(),124,124);
        imagecopymerge($im, $goalFace, 298,160, 0, 0, 124, 124, 100);

      
        
        
        imagettftext($im, 22, 0,260 , 400, $blackColor, $fontFileBold, $stats['previewPerformance'] );
        imagettftext($im, 14, 0,246 , 420, $blackColor, $fontFileLight, 'G+A' );
        
        imagettftext($im, 22, 0,450 , 440, $blackColor, $fontFileBold, $stats['currentPerformance'] );
        imagettftext($im, 14, 0,436 , 460, $blackColor, $fontFileLight, 'G+A' );

         
         
        $playerNameCenter =  $this->getImgTextCenterX($im,22,'light',$teamPlayer->getFullName());

         imagettftext($im, 22, 0,$playerNameCenter , 320, $whiteColor, $fontFile, $teamPlayer->getFullName()  );
    
        ob_start();
        imagepng($im);
        $imgBody = ob_get_contents();
        ob_end_clean();
        imagedestroy($im);
        
        return $imgBody;
    }
    
    public function generateDistanceChangeImg($targetPlayer,$targetPlayerCrs,$distancePlayer,$distancePlayerCrs,$type)
    {
        $translator =  ServiceLayer::getService('translator');
        $fontFile = $this->getFontFile('normal'); 
        $fontFileBold =$this->getFontFile('bold');
        $fontFileLight =$this->getFontFile('light');

        
       
        if('lowerPlayerDistanceBigger' == $type)
        {
            $template = APPLICATION_PATH.'/img/hpWallPosts/template/lowerPlayerDistanceBigger.png';
            $targetFaceWidth = 120;
            $targetFaceX = 364;
            $targetFaceY = 190;
            $targetFaceNameX = 334;
            $targetFaceNameY = 360;      
            $targetFaceFontSize  = 24;        
            
            $distanceFaceWidth = 80;
            $distanceFaceX = 140;
            $distanceFaceY = 230;
            $distanceFaceNameX = 100;
            $distanceFaceNameY = 360;
            $distanceFaceFontSize = 20;
            $title = $translator->translate('hpWallImg.personal.lowerPlayerDistanceBigger.title') ;
        }
        
        if('lowerPlayerDistanceLesser' == $type)
        {
            $template = APPLICATION_PATH.'/img/hpWallPosts/template/lowerPlayerDistanceLesser.png';
            $targetFaceWidth = 120;
            $targetFaceX = 420;
            $targetFaceY = 190;
            $targetFaceNameX = 400;
            $targetFaceNameY = 360;      
            $targetFaceFontSize  = 24;        
            
            $distanceFaceWidth = 80;
            $distanceFaceX = 140;
            $distanceFaceY = 230;
            $distanceFaceNameX = 100;
            $distanceFaceNameY = 360;
            $distanceFaceFontSize = 20;
             $title = $translator->translate('hpWallImg.personal.lowerPlayerDistanceLesser.title') ;
        }
        

        if('higherPlayerDistanceBigger' == $type)
        {
            $targetFaceWidth = 80;
            $targetFaceX = 140;
            $targetFaceY = 230;
            $targetFaceNameX = 104;
            $targetFaceNameY = 360;     
            $targetFaceFontSize  = 20;    
            
            $distanceFaceWidth = 120;
            $distanceFaceX = 420;
            $distanceFaceY = 190;
            $distanceFaceNameX = 400;
            $distanceFaceNameY = 360;
            $distanceFaceFontSize = 24;

            $template = APPLICATION_PATH.'/img/hpWallPosts/template/higherPlayerDistanceBigger.png';
             $title = $translator->translate('hpWallImg.personal.higherPlayerDistanceBigger.title') ;
            
        }

        if('higherPlayerDistanceLesser' == $type)
        {
            $targetFaceWidth = 80;
            $targetFaceX = 140;
            $targetFaceY = 230;
            $targetFaceNameX = 104;
            $targetFaceNameY = 360;     
            $targetFaceFontSize  = 20;    
            
            $distanceFaceWidth = 120;
            $distanceFaceX = 480;
            $distanceFaceY = 190;
            $distanceFaceNameX = 460;
            $distanceFaceNameY = 360;
            $distanceFaceFontSize = 24;

            $template = APPLICATION_PATH.'/img/hpWallPosts/template/higherPlayerDistanceLesser.png';
             $title = $translator->translate('hpWallImg.personal.higherPlayerDistanceLesser.title') ;
            
        }
        
        
        $targetFace = $this->generateFaceImg($targetPlayer->getMidPhoto(),$targetFaceWidth,$targetFaceWidth);
        $distanceFace = $this->generateFaceImg($distancePlayer->getMidPhoto(),$distanceFaceWidth,$distanceFaceWidth);
        
        $im = imagecreatefrompng($template);
        $whiteColor = imagecolorallocate($im, 255, 255, 255);
        $headCenter =  $this->getImgTextCenterX($im,30,'bold',$title );
        imagettftext($im, 30, 0,$headCenter , 105, $whiteColor, $fontFileBold, $title );
        
        imagecopymerge($im, $targetFace, $targetFaceX,$targetFaceY, 0, 0, $targetFaceWidth, $targetFaceWidth, 100);
        imagecopymerge($im, $distanceFace, $distanceFaceX,$distanceFaceY, 0, 0, $distanceFaceWidth, $distanceFaceWidth, 100);
        
        imagettftext($im, $targetFaceFontSize, 0,$targetFaceNameX , $targetFaceNameY, $whiteColor, $fontFileBold, $targetPlayer->getFullName() );
        imagettftext($im, 18, 0,$targetFaceNameX+30 , $targetFaceNameY+34, $whiteColor, $fontFileBold, $targetPlayerCrs );
        imagettftext($im, 18, 0,$targetFaceNameX+72 , $targetFaceNameY+34, $whiteColor, $fontFile, 'CRS' );
        
        imagettftext($im, $distanceFaceFontSize, 0,$distanceFaceNameX , $distanceFaceNameY, $whiteColor, $fontFileBold, $distancePlayer->getFullName() );
        imagettftext($im, 18, 0,$distanceFaceNameX+30 , $distanceFaceNameY+34, $whiteColor, $fontFileBold, $distancePlayerCrs );
        imagettftext($im, 18, 0,$distanceFaceNameX+72 , $distanceFaceNameY+34, $whiteColor, $fontFile, 'CRS' );
        
        ob_start();
        imagepng($im);
        $imgBody = ob_get_contents();
        ob_end_clean();
        imagedestroy($im);


        return $imgBody;
    }

    public function generateDistanceChangePost($teamPlayer,$team,$event)
    {
        $repo = ServiceLayer::getService('HpWallPostRepository'); 
        $teamStatHistoryRepo = ServiceLayer::getService('TeamStatHistoryRepository');
        $playerStatManager = ServiceLayer::getService('PlayerStatManager');
        $teamPlayerManager = ServiceLayer::getService('TeamPlayerManager');

        $types = array();

        
        //preview ladder
        $previewStats = $teamStatHistoryRepo->findNearestPreviewMatch(new \DateTime(),$team->getId());
        $previewPlayersStats = json_decode($previewStats->getData(),true);
        $seasonManager = ServiceLayer::getService('TeamSeasonManager');
        $currentSeason = $seasonManager->getSeasonById($event->getSeason());
        
        //var_dump($currentSeason);exit;
        $statFrom = new DateTimeEx($currentSeason->getStartDate());
        $statTo = new DateTimeEx($currentSeason->getEndDate());
        $teamPlayersStats = $playerStatManager->getAllTeamPlayersStat($team, null, $statFrom, $statTo);
        
        $previewLadder = $playerStatManager->createLadderFromHistoryData($previewPlayersStats,$team);
        
     
        
        $currentLadder = $playerStatManager->createCurrentLadder($team);
        $playerPreviewLadderPosition = $previewLadder['players'][$teamPlayer->getId()];
        
        /*
        if(1 == $playerPreviewLadderPosition['position'])
        {
            $higherPlayerPreviewLadderPosition = null;
        }
        else 
        {
             $higherPlayerPreviewLadderPosition = $previewLadder['position'][$playerPreviewLadderPosition['position']-1];
        }
        */
        $higherPlayerPreviewLadderPosition = $previewLadder['position'][$playerPreviewLadderPosition['position']-1]; 
        $lowerPlayerPreviewLadderPosition = $previewLadder['position'][$playerPreviewLadderPosition['position']+1];

         
         //current position
          $playerCurrentLadderPosition = $currentLadder['players'][$teamPlayer->getId()];
           
          //current Lower player
          if(array_key_exists($lowerPlayerPreviewLadderPosition['teamPlayerId'], $currentLadder['players']))
          {
              $currentLowerPlayer = $currentLadder['players'][$lowerPlayerPreviewLadderPosition['teamPlayerId']];
          }
          else //player didnt play in current match
          {
               $currentLowerPlayer = $lowerPlayerPreviewLadderPosition;
          }
          
          if(array_key_exists($higherPlayerPreviewLadderPosition['teamPlayerId'], $currentLadder['players']))
          {
              $currentHigherPlayer = $currentLadder['players'][$higherPlayerPreviewLadderPosition['teamPlayerId']];
          }
           else //player didnt play in current match
          {
               $currentHigherPlayer = $higherPlayerPreviewLadderPosition;
          }
          

          $lowerPlayerPreviewDiff = $playerPreviewLadderPosition['crs'] - $lowerPlayerPreviewLadderPosition['crs'];
          $lowerPlayerCurrentDiff = $playerCurrentLadderPosition['crs'] - $currentLowerPlayer['crs'];
          
          
        
          
          
         //vzdalujes sa
         if($lowerPlayerCurrentDiff > $lowerPlayerPreviewDiff && $lowerPlayerPreviewLadderPosition != null)
         {
             $types[] = 'lowerPlayerDistanceBigger';
             $distancePlayer = $teamPlayerManager->getTeamPlayerById($currentLowerPlayer['teamPlayerId']);
             $distancePlayerCrs =  $currentLowerPlayer['crs'];
         }
         
         //dobieha ta
          if($lowerPlayerCurrentDiff < $lowerPlayerPreviewDiff && $currentLowerPlayer['position']+1 == $playerCurrentLadderPosition['position']  && $lowerPlayerPreviewLadderPosition != null)
         {
              $types[] = 'lowerPlayerDistanceLesser';
              $distancePlayer = $teamPlayerManager->getTeamPlayerById($currentLowerPlayer['teamPlayerId']);
              $distancePlayerCrs =  $currentLowerPlayer['crs'];
         }

        $higherPlayerPreviewDiff = $higherPlayerPreviewLadderPosition['crs'] - $playerPreviewLadderPosition['crs'];
        $higherPlayerCurrentDiff = $currentHigherPlayer['crs'] - $playerCurrentLadderPosition['crs'] ;

        //vzdaluje sa ti
        if($higherPlayerPreviewDiff < $higherPlayerCurrentDiff  && $higherPlayerPreviewLadderPosition != null)
        {
             $types[] = 'higherPlayerDistanceBigger';
              $distancePlayer = $teamPlayerManager->getTeamPlayerById($currentHigherPlayer['teamPlayerId']);
              $distancePlayerCrs =  $currentHigherPlayer['crs'];
        }
        
        
        //dobiehas
        if($higherPlayerPreviewDiff < $higherPlayerCurrentDiff  && $currentHigherPlayer['position']-1 == $playerCurrentLadderPosition['position']  && $higherPlayerPreviewLadderPosition != null)
        {
             $types[] = 'higherPlayerDistanceLesser';
             $distancePlayer = $teamPlayerManager->getTeamPlayerById($currentHigherPlayer['teamPlayerId']);
              $distancePlayerCrs =  $currentHigherPlayer['crs'];
        }
        


        if (empty($types))
        {
            return;
        }
        else
        {
            foreach ($types as $type)
            {
                $post = new HpWallPost();
                $post->setCreatedAt(new DateTime(date('Y-m-d H:i:s')));
                $post->setAuthorId($teamPlayer->getPlayerId());
                $post->setVisibility(array($teamPlayer->getTeamId()));
                $post->setBody('');
                $post->setTeamId($teamPlayer->getTeamId());
                $post->setStatus('proposal');
                $post->setType('ladder_distance_change');
                $post->setBodyData(json_encode(array(
                    'team_id' => $team->getId(),
                    'team_name' => $team->getName(),
                    'event_date' => $event->getCurrentDate(),
                    'event_time' => $event->getStart()->format('H:i'),
                    'event_name' => $event->getName(),
                    'event_id' => $event->getId())));


                $postId = $repo->saveNew($post);
                $uploadDir = $this->getPostDir($postId);
                $post->setId($postId);

                $img = $this->generateDistanceChangeImg($teamPlayer, $playerCurrentLadderPosition['crs'], $distancePlayer, $distancePlayerCrs, $type);

                
                file_put_contents($uploadDir['absolute'] . '/' . $type . '.png', $img);
                $post->setImg($uploadDir['relative'] . '/' . $type . '.png');
                $repo->save($post);
            }
        }

        //return  $uploadDir['relative'].'/'.$type.'.png';
    }
    
    public function generatePositionChangeImg($targetPlayer,$targetPlayerCrs,$distancePlayer,$distancePlayerCrs,$type)
    {
        $translator =  ServiceLayer::getService('translator');
        $fontFile = $this->getFontFile('normal'); 
        $fontFileBold =$this->getFontFile('bold');
        $fontFileLight =$this->getFontFile('light');

        
       
        if('up' == $type)
        {
            $template = APPLICATION_PATH.'/img/hpWallPosts/template/PositionChangeUp.png';
            $targetFaceWidth = 100;
            $targetFaceX = 220;
            $targetFaceY = 190;
            $targetFaceNameX = 190;
            $targetFaceNameY = 330;      
            $targetFaceFontSize  = 20;        
            
            $distanceFaceWidth = 100;
            $distanceFaceX = 410;
            $distanceFaceY = 160;
            $distanceFaceNameX = 400;
            $distanceFaceNameY = 300;
            $distanceFaceFontSize = 20;
            $title = strtoupper($translator->translate('hpWallImg.personal.PositionChangeUp.title')) ;
            $subtitle = $translator->translate('hpWallImg.personal.PositionChangeUp.subtitle') ;
        }
        
        if('down' == $type)
        {
            $template = APPLICATION_PATH.'/img/hpWallPosts/template/PositionChangeDown.png';
            $targetFaceWidth = 100;
            $targetFaceX = 220;
            $targetFaceY = 160;
            $targetFaceNameX = 190;
            $targetFaceNameY = 300;      
            $targetFaceFontSize  = 20;        
            
            $distanceFaceWidth = 100;
            $distanceFaceX = 410;
            $distanceFaceY = 190;
            $distanceFaceNameX = 400;
            $distanceFaceNameY = 330;
            $distanceFaceFontSize = 20;
            $title = strtoupper($translator->translate('hpWallImg.personal.PositionChangeDown.title')) ;
            $subtitle =  $translator->translate('hpWallImg.personal.PositionChangeDown.subtitle') ;
        }

        $targetFace = $this->generateFaceImg($targetPlayer->getMidPhoto(),$targetFaceWidth,$targetFaceWidth);
        $distanceFace = $this->generateFaceImg($distancePlayer->getMidPhoto(),$distanceFaceWidth,$distanceFaceWidth);
        
        $im = imagecreatefrompng($template);
        $whiteColor = imagecolorallocate($im, 255, 255, 255);
        $blackColor = imagecolorallocate($im, 0, 0, 0);
        $headCenter =  $this->getImgTextCenterX($im,30,'bold',$title );
        imagettftext($im, 30, 0,$headCenter , 90, $whiteColor, $fontFileBold, $title );
        
        $headCenterSubtitle =  $this->getImgTextCenterX($im,18,'normal',$subtitle );
        imagettftext($im, 18, 0,$headCenterSubtitle , 125, $whiteColor, $fontFile, $subtitle );
        
        
        imagecopymerge($im, $targetFace, $targetFaceX,$targetFaceY, 0, 0, $targetFaceWidth, $targetFaceWidth, 100);
        imagecopymerge($im, $distanceFace, $distanceFaceX,$distanceFaceY, 0, 0, $distanceFaceWidth, $distanceFaceWidth, 100);
        

        $targetPlayerNameCenter = $this->getImgTextCenterX($im,24,'normal',$targetPlayer->getFullName())-88;
        imagettftext($im, $targetFaceFontSize, 0,$targetPlayerNameCenter , $targetFaceNameY, $whiteColor, $fontFileBold, $targetPlayer->getFullName() );
        
        imagettftext($im, 24, 0,$targetFaceNameX+58 , $targetFaceNameY+84, $blackColor, $fontFileBold, $targetPlayerCrs );
        imagettftext($im, 14, 0,$targetFaceNameX+58 , $targetFaceNameY+108, $blackColor, $fontFile, 'CRS' );
        
        
        $distancePlayerNameCenter = $this->getImgTextCenterX($im,24,'normal',$distancePlayer->getFullName())+108;
        imagettftext($im, $distanceFaceFontSize, 0,$distancePlayerNameCenter , $distanceFaceNameY, $whiteColor, $fontFileBold, $distancePlayer->getFullName() );
        imagettftext($im, 24, 0,$distanceFaceNameX+36 , $distanceFaceNameY+84, $blackColor, $fontFileBold, $distancePlayerCrs );
        imagettftext($im, 14, 0,$distanceFaceNameX+36 , $distanceFaceNameY+108, $blackColor, $fontFile, 'CRS' );
        
        ob_start();
        imagepng($im);
        $imgBody = ob_get_contents();
        ob_end_clean();
        imagedestroy($im);


        return $imgBody;
    }
    
    public function generatePositionChangePost($teamPlayer,$team)
    {
         $repo = ServiceLayer::getService('HpWallPostRepository'); 
        $teamStatHistoryRepo = ServiceLayer::getService('TeamStatHistoryRepository');
        $playerStatManager = ServiceLayer::getService('PlayerStatManager');
        $teamPlayerManager = ServiceLayer::getService('TeamPlayerManager');

        $types = array();
        $currentLadder = $playerStatManager->createCurrentLadder($team);
       
         //current position
          $playerCurrentLadderPosition = $currentLadder['players'][$teamPlayer->getId()];
          $currentLowerPlayer = $currentLadder['position'][$playerCurrentLadderPosition['position']+1];
          $currentHigherPlayer = $currentLadder['position'][$playerCurrentLadderPosition['position']-1];

          if(null != $currentLowerPlayer)
          {
               $lowerPlayerCurrentDiff = $playerCurrentLadderPosition['crs'] - $currentLowerPlayer['crs'];
          }
          if(null != $currentHigherPlayer)
          {
               $higherPlayerCurrentDiff = $currentHigherPlayer['crs'] - $playerCurrentLadderPosition['crs'];
          }
         

          $limit = 5;

         //ohrozenie pozicie
        if ($lowerPlayerCurrentDiff < $limit && $lowerPlayerCurrentDiff > 0)
        {
            $types[] = 'down';
            $distancePlayer = $teamPlayerManager->getTeamPlayerById($currentLowerPlayer['teamPlayerId']);
            $distancePlayerCrs = $currentLowerPlayer['crs'];
        }

        
        //sanca na postup
        if ($higherPlayerCurrentDiff < $limit && $higherPlayerCurrentDiff > 0 )
        {
            $types[] = 'up';
            $distancePlayer = $teamPlayerManager->getTeamPlayerById($currentHigherPlayer['teamPlayerId']);
            $distancePlayerCrs = $currentHigherPlayer['crs'];
        }

        if (empty($types))
        {
            return;
        }
        else
        {
            foreach ($types as $type)
            {
                $post = new HpWallPost();
                $post->setCreatedAt(new DateTime(date('Y-m-d H:i:s')));
                $post->setAuthorId($teamPlayer->getPlayerId());
                $post->setVisibility(array($teamPlayer->getTeamId()));
                $post->setBody('');
                $post->setTeamId($teamPlayer->getTeamId());
                $post->setStatus('proposal');
                $post->setType('position_change');
                $post->setBodyData(json_encode(array(
                    'team_id' => $team->getId(),
                    'team_name' => $team->getName(),
                )));


                $postId = $repo->saveNew($post);
                $uploadDir = $this->getPostDir($postId);
                $post->setId($postId);

                $img = $this->generatePositionChangeImg($teamPlayer, $playerCurrentLadderPosition['crs'], $distancePlayer, $distancePlayerCrs, $type);

                
                file_put_contents($uploadDir['absolute'] . '/' . $type . '.png', $img);
                $post->setImg($uploadDir['relative'] . '/' . $type . '.png');
                $repo->save($post);
            }
        }
    }
    
    
    public function getPostDir($postId)
    {
        $salt = substr(md5($postId),0,8);
        $postDir = $postId.'_'.md5($postId.$salt);
        
        $uploadDir = PUBLIC_DIR.'/img/hpWallPosts/'.$postDir;
        if(!file_exists($uploadDir))
        {
            mkdir($uploadDir);
            mkdir($uploadDir.'/thumb_320_320');
        }
        
        return array(
            'relative' => '/img/hpWallPosts/'.$postDir,
            'absolute' =>  PUBLIC_DIR.'/img/hpWallPosts/'.$postDir
         );
    }
    
   
}
