<?php
	
namespace CR\HpWall\Model;
use Core\EntityMapper as EntityMapper;
use Core\ServiceLayer as ServiceLayer;
		
class HpWallLike {	

	
	
		
private $repository;
private $mapper_rules  = array(
	'id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => true
							
					),
'created_at' => array(
					'type' => 'datetime',
					'max_length' => '',
					'required' => true
							
					),
'author_id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => true
							
					),
'type' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => true
							
					),
'record_id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => true
							
					),

);
	
public function getDataMapperRules()
{
	return $this->mapper_rules;
}

public function getRepository()
{
	if(null == $this->repository)
	{
		$this->repository = ServiceLayer::getService("HpWallLikeRepository"); 
	}
	return $this->repository;
}

public function setRepository($repository)
{
	$this->repository = $repository;
}
			

		
		
		
protected $id;

protected $createdAt;

protected $authorId;

protected $type;

protected $recordId;

				
public function setId($val){$this->id = $val;}

public function getId(){return $this->id;}
	
public function setCreatedAt($val){$this->createdAt = $val;}

public function getCreatedAt(){return $this->createdAt;}
	
public function setAuthorId($val){$this->authorId = $val;}

public function getAuthorId(){return $this->authorId;}
	
public function setType($val){$this->type = $val;}

public function getType(){return $this->type;}
	
public function setRecordId($val){$this->recordId = $val;}

public function getRecordId(){return $this->recordId;}

}

		