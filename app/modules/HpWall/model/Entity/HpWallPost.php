<?php
	
namespace CR\HpWall\Model;
use Core\EntityMapper as EntityMapper;
use Core\ServiceLayer as ServiceLayer;
		
class HpWallPost {	

	
	
		
private $repository;
private $mapper_rules  = array(
	'id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => true
							
					),
'created_at' => array(
					'type' => 'datetime',
					'max_length' => '',
					'required' => true
							
					),
'published_at' => array(
					'type' => 'datetime',
					'max_length' => '',
					'required' => true
							
					),
'status' => array(
					'type' => 'string',
					'max_length' => '50',
					'required' => false
							
					),
'body' => array(
					'type' => 'string',
					'max_length' => '',
					'required' => false
							
					),
'img' => array(
					'type' => 'string',
					'max_length' => '',
					'required' => false
							
					),
'uid' => array(
					'type' => 'string',
					'max_length' => '',
					'required' => false
							
					),
'body_data' => array(
					'type' => 'string',
					'max_length' => '',
					'required' => false
							
					),
'author_id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => false
							
					),
'team_id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => false
							
					),
'type' => array(
					'type' => 'string',
					'max_length' => '100',
					'required' => false		
					),
'like_count' => array(
					'type' => 'int',
					'max_length' => '100',
					'required' => false		
					),
'user_photo' => array(
					'type' => 'non-persist',
					'max_length' => '100',
					'required' => false
							
					),
'user_name' => array(
					'type' => 'non-persist',
					'max_length' => '100',
					'required' => false
							
					),
'user_surname' => array(
					'type' => 'non-persist',
					'max_length' => '100',
					'required' => false
							
					),
'team_name' => array(
					'type' => 'non-persist',
					'max_length' => '100',
					'required' => false
							
					),
'team_photo' => array(
					'type' => 'non-persist',
					'max_length' => '100',
					'required' => false
							
					),
'comments_count' => array(
					'type' => 'non-persist',
					'max_length' => '100',
					'required' => false
							
					),

);
	
public function getDataMapperRules()
{
	return $this->mapper_rules;
}

public function getRepository()
{
	if(null == $this->repository)
	{
		$this->repository = ServiceLayer::getService("HpWallPostRepository"); 
	}
	return $this->repository;
}

public function setRepository($repository)
{
	$this->repository = $repository;
}
			

		
		
		
protected $id;

protected $createdAt;
protected $publishedAt;

protected $status;

protected $body;
protected $img;
protected $bodyData;

protected $authorId;
protected $teamId;


protected $type;

protected $likeCount;

protected $userPhoto;
protected $userName;
protected $userSurname;
protected $teamPhoto;
protected $teamName;
protected $visibility;
protected $commentsCount;
protected $likes;
protected $comments;
protected $uid;

				
public function setId($val){$this->id = $val;}

public function getId(){return $this->id;}
	
public function setCreatedAt($val){$this->createdAt = $val;}

public function getCreatedAt(){return $this->createdAt;}
	
public function setStatus($val){$this->status = $val;}

public function getStatus(){return $this->status;}
	
public function setBody($val){$this->body = $val;}

public function getBody(){return $this->body;}
	
public function setAuthorId($val){$this->authorId = $val;}

public function getAuthorId(){return $this->authorId;}
	
public function setTeamId($val){$this->teamId = $val;}

public function getTeamId(){return $this->teamId;}

function getType() {
return $this->type;
}

 function getUserPhoto() {
return $this->userPhoto;
}

 function setType($type) {
$this->type = $type;
}

 function setUserPhoto($userPhoto) {
$this->userPhoto = $userPhoto;
}

function getUserName() {
return $this->userName;
}

 function getUserSurname() {
return $this->userSurname;
}

 function getTeamPhoto() {
return $this->teamPhoto;
}

 function getTeamName() {
return $this->teamName;
}

 function setUserName($userName) {
$this->userName = $userName;
}

 function setUserSurname($userSurname) {
$this->userSurname = $userSurname;
}

 function setTeamPhoto($teamPhoto) {
$this->teamPhoto = $teamPhoto;
}

 function setTeamName($teamName) {
$this->teamName = $teamName;
}

function getLikeCount() {
return $this->likeCount;
}

 function setLikeCount($likeCount) {
$this->likeCount = $likeCount;
}

function getVisibility() {
return $this->visibility;
}

 function setVisibility($visibility) {
$this->visibility = $visibility;
}

function getPublishedAt() {
return $this->publishedAt;
}

 function setPublishedAt($publishedAt) {
$this->publishedAt = $publishedAt;
}






function getCommentsCount() {
return $this->commentsCount;
}

 function setCommentsCount($commentsCount) {
$this->commentsCount = $commentsCount;
}



function getLikes() {
return $this->likes;
}

 function setLikes($likes) {
$this->likes = $likes;
}

function getComments() {
return $this->comments;
}

 function setComments($comments) {
$this->comments = $comments;
}


function getBodyData() {
return $this->bodyData;
}

 function setBodyData($bodyData) {
$this->bodyData = $bodyData;
}

function getImg() {
return $this->img;
}

 function setImg($img) {
$this->img = $img;
}




function getUid() {
return $this->uid;
}

 function setUid($uid) {
$this->uid = $uid;
}





}

		