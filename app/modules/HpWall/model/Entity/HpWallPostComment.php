<?php
	
namespace CR\HpWall\Model;
use Core\EntityMapper as EntityMapper;
use Core\ServiceLayer as ServiceLayer;
		
class HpWallPostComment {	

	
	
		
private $repository;
private $mapper_rules  = array(
	'id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => true
							
					),
'post_id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => false
							
					),
'author_id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => false
							
					),
'body' => array(
					'type' => 'string',
					'max_length' => '',
					'required' => false
							
					),
'created_at' => array(
					'type' => 'datetime',
					'max_length' => '',
					'required' => false
							
					),
'status' => array(
					'type' => 'string',
					'max_length' => '50',
					'required' => false
							
					),
    'user_photo' => array(
					'type' => 'non-persist',
					'max_length' => '100',
					'required' => false
							
					),
'user_name' => array(
					'type' => 'non-persist',
					'max_length' => '100',
					'required' => false
							
					),
'user_surname' => array(
					'type' => 'non-persist',
					'max_length' => '100',
					'required' => false
							
					),

);
	
public function getDataMapperRules()
{
	return $this->mapper_rules;
}

public function getRepository()
{
	if(null == $this->repository)
	{
		$this->repository = ServiceLayer::getService("HpWallPostCommentRepository"); 
	}
	return $this->repository;
}

public function setRepository($repository)
{
	$this->repository = $repository;
}
			

		
		
		
protected $id;

protected $postId;

protected $authorId;

protected $body;

protected $createdAt;

protected $status;
protected $userPhoto;
protected $userName;
protected $userSurname;

				
public function setId($val){$this->id = $val;}

public function getId(){return $this->id;}
	
public function setPostId($val){$this->postId = $val;}

public function getPostId(){return $this->postId;}
	
public function setAuthorId($val){$this->authorId = $val;}

public function getAuthorId(){return $this->authorId;}
	
public function setBody($val){$this->body = $val;}

public function getBody(){return $this->body;}
	
public function setCreatedAt($val){$this->createdAt = $val;}

public function getCreatedAt(){return $this->createdAt;}
	
public function setStatus($val){$this->status = $val;}

public function getStatus(){return $this->status;}

function getUserPhoto() {
return $this->userPhoto;
}

 function getUserName() {
return $this->userName;
}

 function getUserSurname() {
return $this->userSurname;
}

 function setUserPhoto($userPhoto) {
$this->userPhoto = $userPhoto;
}

 function setUserName($userName) {
$this->userName = $userName;
}

 function setUserSurname($userSurname) {
$this->userSurname = $userSurname;
}



}

		