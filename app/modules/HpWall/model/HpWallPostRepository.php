<?php

namespace  CR\HpWall\Model;

use Core\DbStorage;
use Core\EntityMapper;
use Core\Repository as Repository;
use Core\ServiceLayer;


class HpWallPostRepository extends Repository {
    
    public function saveNew(HpWallPost $object)
    {

//first check if exist
        $uid = md5($object->getBodyData().$object->getType().$object->getAuthorId().$object->getTeamId());
        
        $object->setUid($uid);
        
        $existWidget = $this->findOneBy(array('hp.uid' => $uid));
        
        
        if(null != $existWidget)
        {
            //$existWidget->setCreatedAt($object->getCreatedAt());
            //$existWidget->set
            $object->setId($existWidget->getId());
        }
        else
        {
            $id = parent::save($object);
            $object->setId($id);
        }

        
       
       $visibility = $object->getVisibility();
       $storage = $this->getStorage();

       //first delte old visibility
       $storage->clearVisibility($object);
       foreach($visibility as $teamId)
       {
           $storage->saveVisibility(array(
               'post_id' => $object->getId(),
               'team_id' => $teamId,
           ));
       }
       
       //$storage->deletePreviewGeneratedWidget($object);
       
       
       //remove old widget
       //$this->getRepository
       
       return $object->getId();
       
    }
    
    private function getLikeRepository()
    {
        return ServiceLayer::getService('HpWallLikeRepository');
    }
    
    private function getCommentsRepository()
    {
        return ServiceLayer::getService('HpWallPostCommentRepository');
    }

    
    
    
    public function createArrayList($objectList)
    {
        $list = array();
        $router = ServiceLayer::getService('router');
        foreach ($objectList as $object)
        {
            $data_mapper = $object->getDataMapperRules();
            $values = array();

            foreach ($data_mapper as $key => $props)
            {
                $method_name_parts = explode('_', $key);
                $method_name_parts = array_map('ucfirst', $method_name_parts);
                $method_name = 'get' . implode($method_name_parts);


                if (method_exists($object, $method_name))
                {
                    if ('datetime' == $props['type'])
                    {
                        $date_object = call_user_func(array($object, $method_name));
                        if (null == $date_object)
                        {
                            $values[$key] = null;
                        }
                        else
                        {
                            $values[$key] = $date_object->format('Y-m-d H:i:s');
                        }
                    }
                    elseif ('boolean' == $props['type'])
                    {
                        $bool_data = call_user_func(array($object, $method_name));
                        if (null !== $bool_data)
                        {
                            $values[$key] = ($bool_data === true) ? 1 : 0;
                        }
                        else
                        {
                            $values[$key] = null;
                        }
                    }
                    else
                    {
                        $values[$key] = call_user_func(array($object, $method_name));
                    }
                }
                
                if('user_photo' == $key)
                {
                    $values[$key] =  WEB_DOMAIN.'/player/icon?file='.$values[$key];
                }
                
                if('team_photo' == $key)
                {
                    if (null == $values[$key])
                    {
                         $values[$key] =  WEB_DOMAIN.'/img/team/users.svg';
                    }
                    else
                    {
                        $values[$key]  = WEB_DOMAIN.'/img/team/thumb_320_320/'.$values[$key];
                    }

                }
            }
            
             //likes
            $likeRepo = $this->getLikeRepository();
            $likes = $likeRepo->findBy(array('record_id' => $object->getId()));
            
            //comments
            $commentRepo = $this->getCommentsRepository();
            $comments = $commentRepo->findBy(array('hp.post_id' => $object->getId()));
            
            $values['likes'] = $likeRepo->createArrayList($likes);
            $values['comments'] = $commentRepo->createArrayList($comments);
            
            //add data
            if('team_after_match' == $values['type'] )
            {
                $eventData = json_decode($values['body_data'],true);
                $eventDate = new \DateTime($eventData['event_date']['date']);
                $values['link'] =
                        $router->link('api_content_event_detail',
                                array(
                                    'apikey' => CR_API_KEY,
                                    'user_id' => 0,
                                    'id' => $eventData['event_id'],
                                    'current_date' => $eventDate->format('Y-m-d')
                                ));
            }
            
            $list[] = $values;
        }
        return $list;
        
        
      
    }
    
    public function getPostVisibility($post)
    {
        return $this->getStorage()->getPostVisibility($post);
    }


}
