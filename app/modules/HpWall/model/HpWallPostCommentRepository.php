<?php
namespace CR\HpWall\Model;
use Core\Repository as Repository;


class HpWallPostCommentRepository extends Repository
{
     public function createArrayList($objectList)
    {
        $list = array();
        foreach ($objectList as $object)
        {
            $data_mapper = $object->getDataMapperRules();
            $values = array();

            foreach ($data_mapper as $key => $props)
            {
                $method_name_parts = explode('_', $key);
                $method_name_parts = array_map('ucfirst', $method_name_parts);
                $method_name = 'get' . implode($method_name_parts);


                if (method_exists($object, $method_name))
                {
                    if ('datetime' == $props['type'])
                    {
                        $date_object = call_user_func(array($object, $method_name));
                        if (null == $date_object)
                        {
                            $values[$key] = null;
                        }
                        else
                        {
                            $values[$key] = $date_object->format('Y-m-d H:i:s');
                        }
                    }
                    elseif ('boolean' == $props['type'])
                    {
                        $bool_data = call_user_func(array($object, $method_name));
                        if (null !== $bool_data)
                        {
                            $values[$key] = ($bool_data === true) ? 1 : 0;
                        }
                        else
                        {
                            $values[$key] = null;
                        }
                    }
                    else
                    {
                        $values[$key] = call_user_func(array($object, $method_name));
                    }
                }
                
                if('user_photo' == $key)
                {
                    $values[$key] =  WEB_DOMAIN.'/player/icon?file='.$values[$key];
                }
                
                if('team_photo' == $key)
                {
                    if (null == $values[$key])
                    {
                         $values[$key] =  WEB_DOMAIN.'/img/team/users.svg';
                    }
                    else
                    {
                        $values[$key]  = WEB_DOMAIN.'/img/team/thumb_320_320/'.$values[$key];
                    }

                }
                
                
                ///img/team/thumb_320_320
                 
            }
            
            
            $list[] = $values;
        }
        return $list;
        
        
      
    }
}
