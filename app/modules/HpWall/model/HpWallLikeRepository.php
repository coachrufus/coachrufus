<?php
namespace CR\HpWall\Model;
use Core\Repository as Repository;




class HpWallLikeRepository extends Repository
{
    public function createArrayList($objectList)
    {
        $list = array();
        foreach($objectList as $object)
        {
            $array = array();
            $array['id'] = $object->getId();
            $array['created_at'] = $object->getCreatedAt()->format('Y-m-d H:i:s');
            $array['author_id'] = $object->getAuthorId();
            $array['type'] = $object->getType();
            $array['record_id'] = $object->getRecordId();
            $list[] = $array;
        }
       
        
        return $list;
    }
}
