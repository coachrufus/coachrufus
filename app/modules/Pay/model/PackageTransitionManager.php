<?php namespace CR\Pay\Model;

use CR\Pay\Model\UserPackageRepository;
use Core\Types\DateTimeEx;
use CR\Pay\Lib\PackageTransitionType;

class PackageTransition
{
    private $data;
    function __construct(array $data) { $this->data = $data; }
    function getTransitionType(): PackageTransitionType { return $this->data['transitionType']; }
    function getPeriod(): string { return $this->data['period']; }
    function getStartDate(): DateTimeEx { return $this->data['startDate']; }
    function getEndDate(): DateTimeEx { return $this->data['endDate']; }
    function getHasPeriod(): bool { return $this->data['period'] !== null; }
    function getHasEndDate(): bool { return $this->data['period'] !== null; }
    function getPackage(): array { return $this->data['package']; }
    function getUpdateInfo() { return isset($this->data['updateInfo']) ? $this->data['updateInfo'] : null; }
    function getHasUpdateInfo(): bool { return $this->data['updateInfo'] !== null; }
    function __get($name)
    {
        switch ($name)
        {
            case "transitionType": return $this->getTransitionType();
            case "period": return $this->getPeriod();
            case "startDate": return $this->getStartDate();
            case "endDate": return $this->getEndDate();
            case "hasPeriod": return $this->getHasPeriod();
            case "hasEndDate": return $this->getHasEndDate();
            case "package": return $this->getPackage();
            case "updateInfo": return $this->getUpdateInfo();
            case "hasUpdateInfo": return $this->getHasUpdateInfo();
            default: throw new \Exception('Invalid property name: {$name}');
        }
    }
}

class PackageTransitionManager
{
    private $userPackageRepository;
    private $packageUpdater;

    function __construct
    (
        UserPackageRepository $userPackageRepository,
        PackageUpdater $packageUpdater
    )
    {
        $this->userPackageRepository = $userPackageRepository;
        $this->packageUpdater = $packageUpdater;
    }

    private function getDateAfterLastPackageDate(DateTimeEx $now, $lastUserPackage, $orderData): DateTimeEx
    {
        $lastEndDate = DateTimeEx::parse($lastUserPackage['end_date']);
        return $lastEndDate->toTimeStamp() > $now->toTimeStamp() ? $lastEndDate : $now;
    }

    private function getFirstPackageStartDate(DateTimeEx $now, $lastUserPackage, $orderData): DateTimeEx
    {
        return $now;
    }
    
    private function getUpperPackageStartDate(DateTimeEx $now, $lastUserPackage, $orderData): DateTimeEx
    {
        return $now;
    }

    private function getLowerPackageStartDate(DateTimeEx $now, $lastUserPackage, $orderData): DateTimeEx
    {
        return $this->getDateAfterLastPackageDate($now, $lastUserPackage, $orderData);
    }

    private function getSamePackageStartDate(DateTimeEx $now, $lastUserPackage, $orderData): DateTimeEx
    {
        return $this->getDateAfterLastPackageDate($now, $lastUserPackage, $orderData);
    }

    private function asResult(array $result)
    {
        switch ($result['period'])
        {
            case null: return new PackageTransition($result);
            case "year": return new PackageTransition($result + [
                "endDate" => $result['startDate']->addYear(1)
            ]);
            case "month": return new PackageTransition($result + [
                "endDate" => $result['startDate']->addMonth(1)
            ]);
            case "custom": return new PackageTransition($result + [
                "endDate" => DateTimeEx::of($result['updateInfo']->endDate)
            ]);
        }
    }

    function getPackageTransition(int $teamId, DateTimeEx $now, $orderData)
    {
        $newPackage = $orderData['package'];
        $period = isset($orderData['package']['period']) ? $orderData['package']['period'] : null;
        $lastUserPackage = $this->userPackageRepository->findLastUserPackage($teamId);
        if ($lastUserPackage === false)
        {
            return $this->asResult([
                'transitionType' => new PackageTransitionType(PackageTransitionType::FirstPackage),
                'startDate' => $this->getFirstPackageStartDate($now, $lastUserPackage, $orderData),
                'period' => $period,
                'package' => $newPackage
            ]);
        }
        else
        {
            $lastLavel = (int)$lastUserPackage['level'];
            $newLevel = (int)$newPackage['level'];
            if ($newLevel > $lastLavel) // || $period == "custom"
            {
                $updateInfo = $this->packageUpdater->getUpdatePackageInfo($newPackage['id']);
                if ($updateInfo->credits === 0)
                {
                    return $this->asResult([
                        'transitionType' => new PackageTransitionType(PackageTransitionType::FirstPackage),
                        'startDate' => $this->getFirstPackageStartDate($now, $lastUserPackage, $orderData),
                        'period' => $period,
                        'package' => $newPackage
                    ]);
                }
                else
                {
                    return $this->asResult([
                        'transitionType' => new PackageTransitionType(PackageTransitionType::UpperPackage),
                        'startDate' => $this->getUpperPackageStartDate($now, $lastUserPackage, $orderData),
                        'period' => 'custom',
                        'package' => $newPackage,
                        'updateInfo' => $updateInfo
                    ]);
                }
            }
            else if ($newLevel < $lastLavel)
            {
                return $this->asResult([
                    'transitionType' => new PackageTransitionType(PackageTransitionType::LowerPackage),
                    'startDate' => $this->getLowerPackageStartDate($now, $lastUserPackage, $orderData),
                    'period' => $period,
                    'package' => $newPackage
                ]);
            }
            else
            {
                return $this->asResult([
                    'transitionType' => new PackageTransitionType(PackageTransitionType::SamePackage),
                    'startDate' => $this->getSamePackageStartDate($now, $lastUserPackage, $orderData),
                    'period' => $period,
                    'package' => $newPackage
                ]);
            }
        }
    }
}