<?php namespace CR\Pay\Model;

use Core\GUID;
use Core\Types\DateTimeEx;

class CouponGenerator
{
    function __construct()
    {
    }

    function generateCoupons(int $count, float $discountPercentage, DateTimeEx $expirationDate, $name = '')
    {
        for($i = 0; $i < $count; $i++)
        {
            yield [
                'discount_percentage' => $discountPercentage,
                'code' => GUID::create(),
                'used' => 0,
                'name' => $name,
                'expiration_date' => (string)$expirationDate
            ];
        }
    }
}