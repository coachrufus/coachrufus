<?php namespace CR\Pay\Model;

class PaymentManager
{
    function __construct()
    {
        
    }
    
    public function getProYearPrice()
    {
        return 129;
    }
    
    public function getProMonthPrice()
    {
        return 18;
    }
    
    public function getProPrices()
    {
        return array('year' => 129,'month' => 18);
    }
    
    public function getVariantProPrices()
    {
        return array(12 => 129,1 => 18);
    }
    
    public function createOnDemandSubscription($data)
    {
        $storage = new \Core\DbStorage('subscriptions');
        $storage->create($data);
    }
    
    public function getRenewParamsByVariant($variant)
    {
        $data = array();
        if($variant == 1)
        {
            $data['renew_cycle'] = 'MONTH';
            $data['renew_period'] = 1;
        }
        if($variant == 12)
        {
            $data['renew_cycle'] = 'MONTH';
            $data['renew_period'] = 12;
        }
        
        return $data;
    }
    
    public function enlargeEndDate($endDate,$renewCycle,$renewPeriod)
    {
        if($renewCycle == 'MONTH')
        {
             $endDate->modify('+'.$renewPeriod.' months');
        }
        if($renewCycle == 'WEEK')
        {
             $endDate->modify('+'.$renewPeriod.' weeks');
        }
        if($renewCycle == 'DAY')
        {
             $endDate->modify('+'.$renewPeriod.' days');
        }
        
        return $endDate;         
    }
    
    
}