<?php

namespace CR\Pay\Model;

use Core\ServiceLayer;

class OrderManager extends \Core\Manager {

   
    
    public function generateOrderNumber()
    {
        
    }
    
    public function getUserSubscriptions($user)
    {
        //$orders = $this->getRepository()->findBy(array('user_id' => $user->getId(),'status' => 'paid'));
        $orders = $this->getRepository()->getUserSubscriptions(array('user_id' => $user->getId()));
        return $orders;
    }
    
    public function getOrderByNumber($no)
    {
        $order = $this->getRepository()->findOneBy(array('id' => $no));
        return $order;
    }
    
   
    
    public function getOrderById($no)
    {
        $order = $this->getRepository()->findOneBy(array('id' => $no));
        return $order;
    }
    
   
    
    public function getPaidOrderByNumber($no)
    {
        $order = $this->getRepository()->findOneBy(array('id' => $no,'status' => 'paid'));
        return $order;
    }
    
    public function getOrderByPaymentId($id)
    {
        $order = $this->getRepository()->findOneBy(array('payment_id' => $id));
        return $order;
    }
    
    public function getOrdersByParentId($id)
    {
        $order = $this->getRepository()->findBy(array('parent_id' => $id));
        return $order;
    }
    
    public function saveOrder($order)
    {
       return  $this->getRepository()->save($order);
    }
    
}
