<?php namespace CR\Pay\Model;

use Core\Db;
use CR\Pay\Invoices\IInvoiceNumberStore;
use Core\Types\DateTimeEx;

class InvoiceRepository implements IInvoiceNumberStore
{
    use Db;
    
    private $db;
    
    function __construct()
    {
        $this->db = $this->getDb();
        $this->db->debug = true;
    }

    function transactionBegin()
    {
        $this->db->transaction = "BEGIN";
    }

    function transactionCommit()
    {
        $this->db->transaction = "COMMIT";
    }
    
    function getLastInvoiceNumberFor(\DateTime $date, $prefix)
    {
        $result = $this->db->invoices()
            ->where('type', $prefix)
            ->where('YEAR(invoice_date) = ?', $date->format('Y'))
            ->order('id DESC')
            ->fetch();
        return $result === false || empty($result['invoice_no']) ?
            false : $result['invoice_no'];
    }
    
    function getLastInvoiceData($userId, $teamId)
    {
        return $this->db->invoices()
            ->where('user_id', $userId)
            ->where('team_id', $teamId)
            ->order('id DESC')
            ->fetch();
    }

    function addInvoice($invoice)
    {
        return $this->db->invoices()
            ->insert($invoice);
    }

    function getInvoicesForTeam($teamId)
    {
        $invoices = $this->db->invoices()
            ->where('team_id', $teamId)
            ->order('id DESC');
        $result = [];
        foreach ($invoices as $invoice)
        {
            $invoice = iterator_to_array($invoice);
            $invoice['start_date'] = DateTimeEx::parse($invoice['start_date']);
            $invoice['end_date'] = DateTimeEx::parse($invoice['end_date']);
            $result[$invoice['vs']] = $invoice;
        }
        return $result;
    }

    function getInvoicesWithUserPackagesForTeam($teamId)
    {
        $invoices = $this->getInvoicesForTeam($teamId);
        $invoiceIds = [];
        foreach ($invoices as $invoice)
        {
            $invoiceIds[] = $invoice['id'];
        }
        $invoicesUserPackages = $this->db->invoices_user_packages()
            ->where('invoice_id', $invoiceIds)
            ->order('id DESC');
        $invoicesUserPackageKV = [];
        foreach ($invoicesUserPackages as $invoicesUserPackage)
        {
            $invoicesUserPackageKV[$invoicesUserPackage['invoice_id']] = $invoicesUserPackage['user_package_id'];
        }
        $userPackages = $this->db->user_packages()
            ->where('id', $invoicesUserPackageKV)
            ->order('id DESC');
        $invoicesUserPackageKV = array_flip($invoicesUserPackageKV);
        $invoiceIdUserPackages = [];
        foreach ($userPackages as $userPackage)
        {
            $invoiceId = $invoicesUserPackageKV[$userPackage['id']];
            $invoiceIdUserPackages[$invoiceId] = iterator_to_array($userPackage);
        }
        foreach ($invoices as &$invoice)
        {
            $userPackage = $invoiceIdUserPackages[$invoice['id']];
            $invoice['userPackage'] = $userPackage;
        }
        return $invoices;
    }

    function getInvoiceByVS($vs)
    {
        return $this->db->invoices()
            ->where('vs', $vs)
            ->fetch();
    }

    function getCountries($lang)
    {
        echo  $lang.'_name';
        
        return $this->db->country()
            ->fetchPairs('id', $lang.'_name');
    }
    
     function getCountry($id)
    {
        return $this->db->country()
            ->where('id', $id)
            ->fetch();
    }
    
     function getCountryByCode($code)
    {
        return $this->db->country()
            ->where('country_code', $code)
            ->fetch();
    }

    function addPackageRelations($invoicesUserPackages)
    {
        $result = $this->db->invoices_user_packages();
        return call_user_func_array(array($result, 'insert'), $invoicesUserPackages);
    }
}