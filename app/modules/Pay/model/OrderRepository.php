<?php
namespace CR\Pay\Model;
use Core\Repository as Repository;


class OrderRepository extends Repository
{
    public function getUserSubscriptions($criteria)
    {
        $data = $this->getStorage()->getUserSubscriptions($criteria['user_id']);

        $list = array();
        foreach ($data as $d)
        {
            $object = $this->factory->createEntityFromArray($d);
            $object->setTeamName($d['team_name']);
            
            
            $list[] = $object;
        }
        return $list;
    }
    
    
}
