<?php namespace CR\Pay\Model;

use \__;

class OrderStateManager
{
    private $packageManager;
    
    function __construct(PackageManager $packageManager)
    {
        $this->packageManager = $packageManager;
    }
    
    function getPackageOrderStates()
    {
        return iterator_to_array((function($packageList)
        {
            foreach ($packageList as $package)
            {
                $package['isOrderable'] = !($package['price_month'] == 0 && $package['price_year'] == 0);
                yield $package['id'] => $package;
            }
        })($this->packageManager->getPackageList()));
    }
}