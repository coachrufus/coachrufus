<?php

namespace CR\Pay\Model;

use Core\EntityMapper as EntityMapper;
use Core\ServiceLayer as ServiceLayer;

class Coupon {

    private $repository;
    private $mapper_rules = array(
        'id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => true
        ),
        'expiration_date' => array(
            'type' => 'datetime',
            'max_length' => '',
            'required' => false
        ),
        'name' => array(
            'type' => 'string',
            'max_length' => '255',
            'required' => false
        ),
        'code' => array(
            'type' => 'string',
            'max_length' => '50',
            'required' => false
        ),
        'charges' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'created_at' => array(
            'type' => 'datetime',
            'max_length' => '',
            'required' => false
        ),
        'value_sum' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'value_percent' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'month_period' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'discount_type' => array(
            'type' => 'string',
            'max_length' => '100',
            'required' => false
        ),
    );

    public function getDataMapperRules()
    {
        return $this->mapper_rules;
    }

    protected $id;
    protected $expirationDate;
    protected $name;
    protected $code;
    protected $charges;
    protected $createdAt;
    protected $valueSum;
    protected $valuePercent;
    protected $monthPeriod;
    protected $discountType;

    public function setId($val)
    {
        $this->id = $val;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setExpirationDate($val)
    {
        $this->expirationDate = $val;
    }

    public function getExpirationDate()
    {
        return $this->expirationDate;
    }

    public function setName($val)
    {
        $this->name = $val;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setCode($val)
    {
        $this->code = $val;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function setCharges($val)
    {
        $this->charges = $val;
    }

    public function getCharges()
    {
        return $this->charges;
    }

    public function setCreatedAt($val)
    {
        $this->createdAt = $val;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setValueSum($val)
    {
        $this->valueSum = $val;
    }

    public function getValueSum()
    {
        return $this->valueSum;
    }

    public function setValuePercent($val)
    {
        $this->valuePercent = $val;
    }

    public function getValuePercent()
    {
        return $this->valuePercent;
    }

    public function getMonthPeriod()
    {
        return $this->monthPeriod;
    }

    public function setMonthPeriod($monthPeriod)
    {
        $this->monthPeriod = $monthPeriod;
    }

    public function getDiscountType()
    {
        return $this->discountType;
    }

    public function setDiscountType($discountType)
    {
        $this->discountType = $discountType;
    }

}
