<?php

namespace CR\Pay\Model;

use Core\EntityMapper as EntityMapper;
use Core\ServiceLayer as ServiceLayer;

class Order {

    private $repository;
    private $mapper_rules = array(
        'id' => array(
            'type' => 'int',
            'max_length' => '10',
            'required' => true
        ),
        'order_no' => array(
            'type' => 'string',
            'max_length' => '20',
            'required' => false
        ),
        'data' => array(
            'type' => 'string',
            'max_length' => '',
            'required' => true
        ),
        'payment_id' => array(
            'type' => 'string',
            'max_length' => '100',
            'required' => false
        ),
        'team_id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'user_id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'renew_payment' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'created_at' => array(
            'type' => 'datetime',
            'max_length' => '',
            'required' => false
        ),
        'total_price' => array(
            'type' => 'float',
            'max_length' => '',
            'required' => false
        ),
        'paid' => array(
            'type' => 'float',
            'max_length' => '',
            'required' => false
        ),
        'currency' => array(
            'type' => 'string',
            'max_length' => '',
            'required' => false
        ),
        'country' => array(
            'type' => 'string',
            'max_length' => '',
            'required' => false
        ),
        'variant' => array(
            'type' => 'int',
            'max_length' => '',
            'required' => false
        ),
        'user_package_id' => array(
            'type' => 'int',
            'max_length' => '',
            'required' => false
        ),
        'renew_date' => array(
            'type' => 'datetime',
            'max_length' => '',
            'required' => false
        ),
        'valid_to' => array(
            'type' => 'datetime',
            'max_length' => '',
            'required' => false
        ),
        'status' => array(
            'type' => 'string',
            'max_length' => '',
            'required' => false
        ),
        'discount' => array(
            'type' => 'float',
            'max_length' => '',
            'required' => false
        ),
        'renew_type' => array(
            'type' => 'string',
            'max_length' => '',
            'required' => false
        ),
        'parent_id' => array(
            'type' => 'string',
            'max_length' => '',
            'required' => false
        ),
    );

    public function getDataMapperRules()
    {
        return $this->mapper_rules;
    }

    protected $id;
    protected $orderNo;
    protected $data;
    protected $paymentId;
    protected $teamId;
    protected $userId;
    protected $renewPayment;
    protected $createdAt;
    protected $totalPrice;
    protected $paid;
    protected $currency;
    protected $country;
    protected $variant;
    protected $userPackageId;
    protected $renewDate;
    protected $validTo;
    protected $status;
    protected $discount;
    protected $renewType;
    protected $teamName;
    protected $parentId;

    public function setId($val)
    {
        $this->id = $val;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setOrderNo($val)
    {
        $this->orderNo = $val;
    }

    public function getOrderNo()
    {
        return $this->orderNo;
    }

    public function setData($val)
    {
        $this->data = $val;
    }

    public function getData()
    {
        return $this->data;
    }

    public function setPaymentId($val)
    {
        $this->paymentId = $val;
    }

    public function getPaymentId()
    {
        return $this->paymentId;
    }

    public function setTeamId($val)
    {
        $this->teamId = $val;
    }

    public function getTeamId()
    {
        return $this->teamId;
    }

    public function setUserId($val)
    {
        $this->userId = $val;
    }

    public function getUserId()
    {
        return $this->userId;
    }

    public function setRenewPayment($val)
    {
        $this->renewPayment = $val;
    }

    public function getRenewPayment()
    {
        return $this->renewPayment;
    }

    public function setCreatedAt($val)
    {
        $this->createdAt = $val;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function getCreateDate()
    {
        return $this->getCreatedAt()->format('d.m.Y');
    }

    public function getPaid()
    {
        return $this->paid;
    }

    public function getCurrency()
    {
        return $this->currency;
    }

    public function getCountry()
    {
        return $this->country;
    }

    public function getVariant()
    {
        return $this->variant;
    }

    public function setPaid($paid)
    {
        $this->paid = $paid;
    }

    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    public function setCountry($country)
    {
        $this->country = $country;
    }

    public function setVariant($variant)
    {
        $this->variant = $variant;
    }

    public function getTotalPrice()
    {
        return $this->totalPrice;
    }

    public function setTotalPrice($totalPrice)
    {
        $this->totalPrice = $totalPrice;
    }

    public function getUserPackageId()
    {
        return $this->userPackageId;
    }

    public function setUserPackageId($userPackageId)
    {
        $this->userPackageId = $userPackageId;
    }

    public function getRenewDate()
    {
        return $this->renewDate;
    }

    public function getValidTo()
    {
        return $this->validTo;
    }

    public function setRenewDate($renewDate)
    {
        $this->renewDate = $renewDate;
    }

    public function setValidTo($validTo)
    {
        $this->validTo = $validTo;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function getFormatedValidTo()
    {
        if (null != $this->getValidTo())
        {
            return $this->getValidTo()->format('d.m.Y H:i');
        }
    }

    public function getFormatedRenewDate()
    {
        if (null != $this->getRenewDate())
        {
            return $this->getRenewDate()->format('d.m.Y H:i');
        }
    }

    public function getDiscount()
    {
        return $this->discount;
    }

    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }

    public function isChangable()
    {
        if (date('Y-m-d') == $this->getValidTo()->format('Y-m-d'))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function getRenewType()
    {
        return $this->renewType;
    }

    public function setRenewType($renewType)
    {
        $this->renewType = $renewType;
    }
    
    public  function getTeamName() {
return $this->teamName;
}

public  function setTeamName($teamName) {
$this->teamName = $teamName;
}

public  function getParentId() {
return $this->parentId;
}

public  function setParentId($parentId) {
$this->parentId = $parentId;
}




}
