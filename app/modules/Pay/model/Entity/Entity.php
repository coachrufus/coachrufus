<?php namespace CR\Pay;

class Entity
{
    private $props;
    
    function __construct($props = [])
    {
        $this->props = $props;
    }
    
    protected function toKey($value)
    {
        return strToLower(implode('', iterator_to_array((function($value)
        {
            $lastIsLower = false;
            for ($i = 0, $len = strlen($value); $i < $len; $i++)
            {
                $c = $value[$i];
                $isLower = ctype_lower($c);
                yield !$isLower && $lastIsLower ? "_{$c}" : $c;
                $lastIsLower = $isLower;
            }
        })($value))));
    }
    
    function __isset($name) { return isset($this->props[$this->toKey($name)]); }
    
    function __get($name)
    {
        var_dump(isset($this->$name));
        
        if (isset($this->$name))
        {
            return $this->props[$this->toKey($name)];
        }
        else throw new \Exception("Invalid property {$name}");
    }
    
    function __set($name, $value)
    {
        $this->props[$this->toKey($name)] = $value;
    }
    
    function toArray()
    {
        return $this->props;
    }
}