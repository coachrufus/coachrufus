<?php

namespace CR\Pay\Model;

use Core\EntityMapper as EntityMapper;
use Core\ServiceLayer as ServiceLayer;

class UserPackage {

    private $repository;
    private $mapper_rules = array(
        'id' => array(
            'type' => 'int',
            'max_length' => '10',
            'required' => true
        ),
        'user_id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => true
        ),
        'package_id' => array(
            'type' => 'int',
            'max_length' => '10',
            'required' => true
        ),
        'team_id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => true
        ),
        'team_name' => array(
            'type' => 'string',
            'max_length' => '75',
            'required' => true
        ),
        'date' => array(
            'type' => 'datetime',
            'max_length' => '',
            'required' => true
        ),
        'start_date' => array(
            'type' => 'datetime',
            'max_length' => '',
            'required' => true
        ),
        'end_date' => array(
            'type' => 'datetime',
            'max_length' => '',
            'required' => true
        ),
        'name' => array(
            'type' => 'string',
            'max_length' => '75',
            'required' => true
        ),
        'note' => array(
            'type' => 'string',
            'max_length' => '',
            'required' => true
        ),
        'level' => array(
            'type' => 'int',
            'max_length' => '10',
            'required' => true
        ),
        'price_month' => array(
            'type' => '',
            'max_length' => '',
            'required' => false
        ),
        'price_year' => array(
            'type' => '',
            'max_length' => '',
            'required' => false
        ),
        'products' => array(
            'type' => 'string',
            'max_length' => '',
            'required' => true
        ),
        'player_limit' => array(
            'type' => 'int',
            'max_length' => '10',
            'required' => false
        ),
        'is_canceled' => array(
            'type' => 'boolean',
            'max_length' => '',
            'required' => true
        ),
        'guid' => array(
            'type' => '',
            'max_length' => '',
            'required' => true
        ),
        'order_id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => true
        ),
        'variant' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => true
        ),
        'renew_payment' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
    );

    public function getDataMapperRules()
    {
        return $this->mapper_rules;
    }

    protected $id;
    protected $userId;
    protected $packageId;
    protected $teamId;
    protected $teamName;
    protected $date;
    protected $startDate;
    protected $endDate;
    protected $name;
    protected $note;
    protected $level;
    protected $priceMonth;
    protected $priceYear;
    protected $products;
    protected $playerLimit;
    protected $isCanceled;
    protected $guid;
    protected $orderId;
    protected $variant;
    protected $renewPayment;

    public function setId($val)
    {
        $this->id = $val;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setUserId($val)
    {
        $this->userId = $val;
    }

    public function getUserId()
    {
        return $this->userId;
    }

    public function setPackageId($val)
    {
        $this->packageId = $val;
    }

    public function getPackageId()
    {
        return $this->packageId;
    }

    public function setTeamId($val)
    {
        $this->teamId = $val;
    }

    public function getTeamId()
    {
        return $this->teamId;
    }

    public function setTeamName($val)
    {
        $this->teamName = $val;
    }

    public function getTeamName()
    {
        return $this->teamName;
    }

    public function setDate($val)
    {
        $this->date = $val;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function setStartDate($val)
    {
        $this->startDate = $val;
    }

    public function getStartDate()
    {
        return $this->startDate;
    }

    public function setEndDate($val)
    {
        $this->endDate = $val;
    }

    public function getEndDate()
    {
        return $this->endDate;
    }

    public function setName($val)
    {
        $this->name = $val;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setNote($val)
    {
        $this->note = $val;
    }

    public function getNote()
    {
        return $this->note;
    }

    public function setLevel($val)
    {
        $this->level = $val;
    }

    public function getLevel()
    {
        return $this->level;
    }

    public function setPriceMonth($val)
    {
        $this->priceMonth = $val;
    }

    public function getPriceMonth()
    {
        return $this->priceMonth;
    }

    public function setPriceYear($val)
    {
        $this->priceYear = $val;
    }

    public function getPriceYear()
    {
        return $this->priceYear;
    }

    public function setProducts($val)
    {
        $this->products = $val;
    }

    public function getProducts()
    {
        return $this->products;
    }

    public function setPlayerLimit($val)
    {
        $this->playerLimit = $val;
    }

    public function getPlayerLimit()
    {
        return $this->playerLimit;
    }

    public function setIsCanceled($val)
    {
        $this->isCanceled = $val;
    }

    public function getIsCanceled()
    {
        return $this->isCanceled;
    }

    public function setGuid($val)
    {
        $this->guid = $val;
    }

    public function getGuid()
    {
        return $this->guid;
    }

    public function setOrderId($val)
    {
        $this->orderId = $val;
    }

    public function getOrderId()
    {
        return $this->orderId;
    }

    public function setVariant($val)
    {
        $this->variant = $val;
    }

    public function getVariant()
    {
        return $this->variant;
    }

    public function setRenewPayment($val)
    {
        $this->renewPayment = $val;
    }

    public function getRenewPayment()
    {
        return $this->renewPayment;
    }
    
    public function getFormattedEndDate($format)
    {
        return $this->getEndDate()->format($format);
    }
    
    

}
