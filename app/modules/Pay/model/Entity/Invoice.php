<?php

namespace CR\Pay\Model;

use Core\EntityMapper as EntityMapper;
use Core\ServiceLayer as ServiceLayer;

class Invoice {

    private $repository;
    private $mapper_rules = array(
        'id' => array(
            'type' => 'int',
            'max_length' => '10',
            'required' => true
        ),
        'vs' => array(
            'type' => 'string',
            'max_length' => '20',
            'required' => true
        ),
        'order_id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => true
        ),
        'invoice_no' => array(
            'type' => 'string',
            'max_length' => '10',
            'required' => true
        ),
        'type' => array(
            'type' => 'int',
            'max_length' => '1',
            'required' => true
        ),
        'user_id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => true
        ),
        'team_id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => true
        ),
        'package_id' => array(
            'type' => 'int',
            'max_length' => '10',
            'required' => true
        ),
        'package_name' => array(
            'type' => 'string',
            'max_length' => '75',
            'required' => true
        ),
        'package_note' => array(
            'type' => 'string',
            'max_length' => '',
            'required' => false
        ),
        'amount' => array(
            'type' => '',
            'max_length' => '',
            'required' => false
        ),
        'price_month' => array(
            'type' => '',
            'max_length' => '',
            'required' => false
        ),
        'price_year' => array(
            'type' => '',
            'max_length' => '',
            'required' => false
        ),
        'price_custom' => array(
            'type' => '',
            'max_length' => '',
            'required' => false
        ),
        'player_limit' => array(
            'type' => 'int',
            'max_length' => '10',
            'required' => false
        ),
        'products' => array(
            'type' => 'string',
            'max_length' => '',
            'required' => false
        ),
        'count' => array(
            'type' => 'int',
            'max_length' => '10',
            'required' => false
        ),
        'invoice_date' => array(
            'type' => 'datetime',
            'max_length' => '',
            'required' => false
        ),
        'payment_due_date' => array(
            'type' => 'datetime',
            'max_length' => '',
            'required' => false
        ),
        'supply_date' => array(
            'type' => 'datetime',
            'max_length' => '',
            'required' => false
        ),
        'full_name' => array(
            'type' => 'string',
            'max_length' => '120',
            'required' => false
        ),
        'tel' => array(
            'type' => 'string',
            'max_length' => '45',
            'required' => false
        ),
        'email' => array(
            'type' => 'string',
            'max_length' => '120',
            'required' => false
        ),
        'street' => array(
            'type' => 'string',
            'max_length' => '120',
            'required' => false
        ),
        'city' => array(
            'type' => 'string',
            'max_length' => '120',
            'required' => false
        ),
        'zip' => array(
            'type' => 'string',
            'max_length' => '10',
            'required' => false
        ),
        'company_name' => array(
            'type' => 'string',
            'max_length' => '120',
            'required' => false
        ),
        'ico' => array(
            'type' => 'string',
            'max_length' => '45',
            'required' => false
        ),
        'icdph' => array(
            'type' => 'string',
            'max_length' => '45',
            'required' => false
        ),
        'dic' => array(
            'type' => 'string',
            'max_length' => '45',
            'required' => false
        ),
        'vat_percentage' => array(
            'type' => '',
            'max_length' => '',
            'required' => false
        ),
        'vat' => array(
            'type' => '',
            'max_length' => '',
            'required' => false
        ),
        'note' => array(
            'type' => 'string',
            'max_length' => '',
            'required' => false
        ),
        'issued_by' => array(
            'type' => 'string',
            'max_length' => '120',
            'required' => false
        ),
        'guid' => array(
            'type' => 'string',
            'max_length' => '36',
            'required' => false
        ),
        'country_id' => array(
            'type' => 'int',
            'max_length' => '10',
            'required' => false
        ),
        'coupon_id' => array(
            'type' => 'int',
            'max_length' => '10',
            'required' => false
        ),
        'start_date' => array(
            'type' => 'datetime',
            'max_length' => '',
            'required' => false
        ),
        'end_date' => array(
            'type' => 'datetime',
            'max_length' => '',
            'required' => false
        ),
        'ip' => array(
            'type' => 'string',
            'max_length' => '50',
            'required' => false
        ),
        'ip_country' => array(
            'type' => 'string',
            'max_length' => '100',
            'required' => false
        ),
        'pay_country' => array(
            'type' => 'string',
            'max_length' => '100',
            'required' => false
        ),
        'transaction_id' => array(
            'type' => 'string',
            'max_length' => '50',
            'required' => false
        ),
        'description' => array(
            'type' => 'string',
            'max_length' => '255',
            'required' => false
        ),
        'currency' => array(
            'type' => 'string',
            'max_length' => '255',
            'required' => false
        ),
        'payment_info' => array(
            'type' => 'string',
            'max_length' => '',
            'required' => false
        ),
    );

    public function getDataMapperRules()
    {
        return $this->mapper_rules;
    }

    protected $id;
    protected $vs;
    protected $orderId;
    protected $invoiceNo;
    protected $type;
    protected $userId;
    protected $teamId;
    protected $packageId;
    protected $packageName;
    protected $packageNote;
    protected $amount;
    protected $priceMonth;
    protected $priceYear;
    protected $priceCustom;
    protected $playerLimit;
    protected $products;
    protected $count;
    protected $invoiceDate;
    protected $paymentDueDate;
    protected $supplyDate;
    protected $fullName;
    protected $tel;
    protected $email;
    protected $street;
    protected $city;
    protected $zip;
    protected $companyName;
    protected $ico;
    protected $icdph;
    protected $dic;
    protected $vatPercentage;
    protected $vat;
    protected $note;
    protected $issuedBy;
    protected $guid;
    protected $countryId;
    protected $couponId;
    protected $startDate;
    protected $endDate;
    protected $ip;
    protected $ipCountry;
    protected $payCountry;
    protected $transactionId;
    protected $description;
    protected $currency;
    protected $paymentInfo;

    public function setId($val)
    {
        $this->id = $val;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setVs($val)
    {
        $this->vs = $val;
    }

    public function getVs()
    {
        return $this->vs;
    }

    public function setOrderId($val)
    {
        $this->orderId = $val;
    }

    public function getOrderId()
    {
        return $this->orderId;
    }

    public function setInvoiceNo($val)
    {
        $this->invoiceNo = $val;
    }

    public function getInvoiceNo()
    {
        return $this->invoiceNo;
    }

    public function setType($val)
    {
        $this->type = $val;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setUserId($val)
    {
        $this->userId = $val;
    }

    public function getUserId()
    {
        return $this->userId;
    }

    public function setTeamId($val)
    {
        $this->teamId = $val;
    }

    public function getTeamId()
    {
        return $this->teamId;
    }

    public function setPackageId($val)
    {
        $this->packageId = $val;
    }

    public function getPackageId()
    {
        return $this->packageId;
    }

    public function setPackageName($val)
    {
        $this->packageName = $val;
    }

    public function getPackageName()
    {
        return $this->packageName;
    }

    public function setPackageNote($val)
    {
        $this->packageNote = $val;
    }

    public function getPackageNote()
    {
        return $this->packageNote;
    }

    public function setAmount($val)
    {
        $this->amount = $val;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function setPriceMonth($val)
    {
        $this->priceMonth = $val;
    }

    public function getPriceMonth()
    {
        return $this->priceMonth;
    }

    public function setPriceYear($val)
    {
        $this->priceYear = $val;
    }

    public function getPriceYear()
    {
        return $this->priceYear;
    }

    public function setPriceCustom($val)
    {
        $this->priceCustom = $val;
    }

    public function getPriceCustom()
    {
        return $this->priceCustom;
    }

    public function setPlayerLimit($val)
    {
        $this->playerLimit = $val;
    }

    public function getPlayerLimit()
    {
        return $this->playerLimit;
    }

    public function setProducts($val)
    {
        $this->products = $val;
    }

    public function getProducts()
    {
        return $this->products;
    }

    public function setCount($val)
    {
        $this->count = $val;
    }

    public function getCount()
    {
        return $this->count;
    }

    public function setInvoiceDate($val)
    {
        $this->invoiceDate = $val;
    }

    public function getInvoiceDate()
    {
        return $this->invoiceDate;
    }

    public function setPaymentDueDate($val)
    {
        $this->paymentDueDate = $val;
    }

    public function getPaymentDueDate()
    {
        return $this->paymentDueDate;
    }

    public function setSupplyDate($val)
    {
        $this->supplyDate = $val;
    }

    public function getSupplyDate()
    {
        return $this->supplyDate;
    }

    public function setFullName($val)
    {
        $this->fullName = $val;
    }

    public function getFullName()
    {
        return $this->fullName;
    }

    public function setTel($val)
    {
        $this->tel = $val;
    }

    public function getTel()
    {
        return $this->tel;
    }

    public function setEmail($val)
    {
        $this->email = $val;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setStreet($val)
    {
        $this->street = $val;
    }

    public function getStreet()
    {
        return $this->street;
    }

    public function setCity($val)
    {
        $this->city = $val;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function setZip($val)
    {
        $this->zip = $val;
    }

    public function getZip()
    {
        return $this->zip;
    }

    public function setCompanyName($val)
    {
        $this->companyName = $val;
    }

    public function getCompanyName()
    {
        return $this->companyName;
    }

    public function setIco($val)
    {
        $this->ico = $val;
    }

    public function getIco()
    {
        return $this->ico;
    }

    public function setIcdph($val)
    {
        $this->icdph = $val;
    }

    public function getIcdph()
    {
        return $this->icdph;
    }

    public function setDic($val)
    {
        $this->dic = $val;
    }

    public function getDic()
    {
        return $this->dic;
    }

    public function setVatPercentage($val)
    {
        $this->vatPercentage = $val;
    }

    public function getVatPercentage()
    {
        return $this->vatPercentage;
    }

    public function setVat($val)
    {
        $this->vat = $val;
    }

    public function getVat()
    {
        return $this->vat;
    }

    public function setNote($val)
    {
        $this->note = $val;
    }

    public function getNote()
    {
        return $this->note;
    }

    public function setIssuedBy($val)
    {
        $this->issuedBy = $val;
    }

    public function getIssuedBy()
    {
        return $this->issuedBy;
    }

    public function setGuid($val)
    {
        $this->guid = $val;
    }

    public function getGuid()
    {
        return $this->guid;
    }

    public function setCountryId($val)
    {
        $this->countryId = $val;
    }

    public function getCountryId()
    {
        return $this->countryId;
    }

    public function setCouponId($val)
    {
        $this->couponId = $val;
    }

    public function getCouponId()
    {
        return $this->couponId;
    }

    public function setStartDate($val)
    {
        $this->startDate = $val;
    }

    public function getStartDate()
    {
        return $this->startDate;
    }

    public function setEndDate($val)
    {
        $this->endDate = $val;
    }

    public function getEndDate()
    {
        return $this->endDate;
    }

    public function setIp($val)
    {
        $this->ip = $val;
    }

    public function getIp()
    {
        return $this->ip;
    }

    public function setIpCountry($val)
    {
        $this->ipCountry = $val;
    }

    public function getIpCountry()
    {
        return $this->ipCountry;
    }

    public function setPayCountry($val)
    {
        $this->payCountry = $val;
    }

    public function getPayCountry()
    {
        return $this->payCountry;
    }

    public function setTransactionId($val)
    {
        $this->transactionId = $val;
    }

    public function getTransactionId()
    {
        return $this->transactionId;
    }

    public function setDescription($val)
    {
        $this->description = $val;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setCurrency($val)
    {
        $this->currency = $val;
    }

    public function getCurrency()
    {
        return $this->currency;
    }

    public function setPaymentInfo($val)
    {
        $this->paymentInfo = $val;
    }

    public function getPaymentInfo()
    {
        return $this->paymentInfo;
    }

    public function getPaymentCard()
    {
        $paymentInfo = unserialize($this->getPaymentInfo());
        return $paymentInfo->payer->payment_card->card_brand . ' ' . $paymentInfo->payer->payment_card->card_number;
    }
    
    public function getInvoiceDateFormated()
{
    return $this->getInvoiceDate()->format('d.m.Y');
}


    
    public function getRawPrice()
    {
        $price = $this->getAmount();
        $vat = $this->getVat();
        $raw = $price-$vat;
        return $raw;
    }

     public function getFormatedDescription()
    {
        $descData = json_decode($this->description,true);
        return $descData['team_name'].', ID '.$this->getTeamId().', '.$descData['package'].', ('. $descData['start'].'-'.$descData['end'].') ';
    }
    
    public function getAddress()
    {
        $street = explode(',', $this->getStreet());
        $address = implode('<br />',$street);
        return $address;

    }
    
    public function getPaymentCountry()
    {
        $paymentInfo = unserialize($this->getPaymentInfo());
        return $paymentInfo->payer->payment_card->card_issuer_country  ;
    }
}
