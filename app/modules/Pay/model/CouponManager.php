<?php

namespace CR\Pay\Model;

use Core\Manager;

class CouponManager extends Manager {

    public function findByCode($code)
    {
        $coupon = $this->getRepository()->findOneBy(array('code' => $code));
        return $coupon;
    }

    public function addCouponUse($coupon)
    {
        $charges = $coupon->getCharges();
        $newCharges = $charges-1;
        
        $coupon->setCharges($newCharges);
        $this->getRepository()->save($coupon);
    }

}
