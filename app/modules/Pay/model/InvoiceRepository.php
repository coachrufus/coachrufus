<?php
namespace CR\Pay\Model;
use Core\Repository as Repository;

class InvoiceRepository extends Repository
{
    public function getLastCounterNumber($prefix)
    {
        $data = $this->getStorage()->findLastCounterNumber($prefix);
        return $data['counter_num'];
    }
    
    public function getCountryVatByCode($code)
    {
        $data = $this->getStorage()->getCountryByCode($code);
        return $data['vat'];
    }
    
    public function getCountryByCode($code)
    {
        $data = $this->getStorage()->getCountryByCode($code);
        return $data;
    }
    
    public function createInvoicePackage($invoiceId,$packageId)
    {
         $this->getStorage()->createInvoicePackage($invoiceId,$packageId);
    }
}
