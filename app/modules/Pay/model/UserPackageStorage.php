<?php
namespace CR\Pay\Model;
use Core\DbStorage;
class UserPackageStorage extends DbStorage {
    
	public function findYearNotifyUsers()
        {
            
            $data = \Core\DbQuery::prepare("
                SELECT u.* 
                FROM   user_packages up 
                       LEFT JOIN co_users u 
                              ON up.user_id = u.id 
                WHERE  up.variant = 12 
                       AND Date_format(up.end_date, '%Y-%m-%d') = Date_format(( Now() + INTERVAL  8 day ), '%Y-%m-%d')" )
		->execute()
		->fetchAll(\PDO::FETCH_ASSOC);
		return $data;
        }
        

	
    
	
	
}