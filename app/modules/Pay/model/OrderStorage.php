<?php
namespace CR\Pay\Model;
use Core\DbStorage;
class OrderStorage extends DbStorage {
    
	public function getUserSubscriptions($userId)
        {
            
            $data = \Core\DbQuery::prepare('SELECT a.*, t.name as team_name from '.$this->getTableName().' a '
                    . 'LEFT JOIN team t ON a.team_id = t.id '
                    . 'WHERE a.user_id = :user_id AND (a.status = "paid" or a.status="canceled") and a.valid_to >= now()')
                ->bindParam('user_id', $userId)
		->execute()
		->fetchAll(\PDO::FETCH_ASSOC);
		return $data;
        }
        

	
    
	
	
}