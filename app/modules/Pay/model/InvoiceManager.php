<?php namespace CR\Pay\Model;

use Core\NameCleaner;
use Core\Notifications\BuyNotificationSender;
use Core\Notifications\BuyStore;
use Core\Types\DateTimeEx;


class InvoiceManager extends \Core\Manager
{
   
    
    function getInvoiceDefaultData($user, int $teamId): DefaultValueAccessor
    {
        $invoice = $this->invoiceRepository->getLastInvoiceData($user->getId(), $teamId);
        return new DefaultValueAccessor($invoice === false ? [
            'full_name' => NameCleaner::clean("{$user->getName()} {$user->getSurname()}"),
            'tel' => $user->getPhone(),
            'email' => $user->getEmail(),
        ] : $invoice);
    }
    
    function getInvoiceMainFields()
    {
        return [ 
            'vs' => $this->translator->translate('Order no.'),
            'invoice_no' => $this->translator->translate('Invoice no.'),
            'invoice_date' => $this->translator->translate('Invoice date')
        ];
    }

    function getInvoiceFields()
    {
        return [ 
            'full_name' => $this->translator->translate('Full name'),
            'tel' => $this->translator->translate('Phone num.'),
            'email' => $this->translator->translate('E-mail'),
            'street' => $this->translator->translate('Street'),
            'city' => $this->translator->translate('City'),
            'zip' => $this->translator->translate('ZIP'),
            'country_id' => $this->translator->translate('Country'),
            'company_name' => $this->translator->translate('Company'),
            'ico' => $this->translator->translate('Business ID'),
            'icdph' => $this->translator->translate('VAT reg.no.'),
            'dic' => $this->translator->translate('Tax ID'),
            'note' => $this->translator->translate('Note')
        ];
    }

    private function addInvoice($invoice)
    {
        return $this->invoiceRepository->addInvoice($invoice);
    }

    public function createInvoicePackage($invoiceId, $userPackage)
    {
        $this->getRepository()->createInvoicePackage($invoiceId,$userPackage['id']);
        
        /*
        $invoicesUserPackages = [];
        foreach ($userPackages as $userPackage)
        {
            $invoicesUserPackages[] = [
                'invoice_id' => $invoice['id'],
                'user_package_id' => $userPackage['id']
            ];
        }
        $this->invoiceRepository->addPackageRelations($invoicesUserPackages);
         * 
         */
    }
    
    public function saveInvoice($invoice)
    {

        $invoiceId = $this->getRepository()->save($invoice);
        return $invoiceId;
    }
    
    //NEW
    
    
    
    public function generateInvoiceNumber($country,$variant,$discount)
    {
        $prefix = $country.date('y');
        $lastCounterNumber = $this->getRepository()->getLastCounterNumber($prefix);
        $counterNumber = $lastCounterNumber+1; 
        $counterNumber = str_pad($counterNumber,5,'0',STR_PAD_LEFT);
        $type = ($variant == 1) ? 'M' : 'Y';
        $discount = str_pad($discount, 2,'0',STR_PAD_LEFT);
        
        $number = $prefix.$counterNumber.$type.$discount;
        return $number;
    }
    
    public function generateVariableSymbol($country)
    {
        $prefix = $country.date('y');
        $lastCounterNumber = $this->getRepository()->getLastCounterNumber($prefix);
        $counterNumber = $lastCounterNumber+1; 
        $counterNumber = str_pad($counterNumber,5,'0',STR_PAD_LEFT);
        $number = $prefix.$counterNumber;
        return $number;
    }

    public function createInvoiceFromOrder($order)
    {
        $now = new \DateTime();
        $invoice = new Invoice();
        $invoice->setType(1);
        $invoice->setUserId($order->getUserId());
        $invoice->setTeamId($order->getTeamId());
        if($order->getVariant() == 12)
        {
            $invoice->setPriceYear($order->getTotalPrice());
        }
        
        if($order->getVariant() == 11)
        {
            $invoice->setPriceMonth($order->getTotalPrice());
        }
        $invoice->setOrderId($order->getId());
        $invoice->setCount(1);
        $invoice->setInvoiceDate($now);
        $invoice->setPaymentDueDate($now);
        $invoice->setSupplyDate($now);
        $invoice->setVatPercentage(0);
        $invoice->setIp($_SERVER['REMOTE_ADDR']);
        
        $invoice->setPackageId($order->getUserPackageId());
       
        
        return $invoice;
    }
    
    public function getUserInvoices($user)
    {
         $repo = new \Core\Repository( new \Core\DbStorage('invoices'),new \Core\EntityMapper('CR\Pay\Model\Invoice'));
         $invoices = $repo->findBy(array('user_id' => $user->getId()),array('order' => ' ID desc'));
         return $invoices;
    }
    
     function getInvoiceByOrderNo($orderNo)
    {
        $repo = new \Core\Repository( new \Core\DbStorage('invoices'),new \Core\EntityMapper('CR\Pay\Model\Invoice'));
         $invoice = $repo->findOneBy(array('invoice_no' => $orderNo));
         return $invoice;
    }
    
     function getInvoiceById($id)
    {
        $repo = new \Core\Repository( new \Core\DbStorage('invoices'),new \Core\EntityMapper('CR\Pay\Model\Invoice'));
         $invoice = $repo->findOneBy(array('id' => $id));
         return $invoice;
    }


    function getInvoiceByVS($vs)
    {
         $repo = new \Core\Repository( new \Core\DbStorage('invoices'),new \Core\EntityMapper('CR\Pay\Model\Invoice'));
         $invoice = $repo->findOneBy(array('vs' => $vs));
         return $invoice;
        //return iterator_to_array($this->invoiceRepository->getInvoiceByVS($vs));
    }

    function getInvoicesForTeam($teamId)
    {
        return $this->invoiceRepository->getInvoicesForTeam($teamId);
    }

    function getInvoicesWithUserPackagesForTeam($teamId)
    {
        return $this->invoiceRepository->getInvoicesWithUserPackagesForTeam($teamId);
    }

    function getCountries($lang)
    {
        return $this->invoiceRepository->getCountries($lang);
    }
    
    function getCountryInfo($id)
    {
        return $this->invoiceRepository->getCountry($id);
    }
    
    function getCountryVat($code)
    {
        return $this->getRepository()->getCountryVatByCode($code);
    }
    
    function getCountryByCode($code)
    {
        return $this->getRepository()->getCountryByCode($code);
    }
    
    public function generatePdf($invoice)
    {
      require_once LIB_DIR.'/tcpdf/tcpdf.php';
       $countryCode = $invoice->getPaymentCountry();
       $country = $this->getCountryByCode($countryCode);
       
       $layout = \Core\ServiceLayer::getService('layout');
        $layout->setTemplate(null);
        
        ob_start();
        $layout->includePart(MODUL_DIR.'/Pay/view/invoice/reciept.php',
                        array(
                                'invoice' => $invoice,
                                'countryName' => $country[LANG.'_name'],
                        ));
        $html = ob_get_contents();
        ob_get_clean();

        $pdf = new \TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->SetCreator('COACHRUFUS');
        $pdf->SetTitle('');
        $pdf->SetSubject('');
        $pdf->SetKeywords('');
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        $pdf->SetMargins(10, 10, 10);
        $pdf->SetHeaderMargin(0);
        $pdf->SetFooterMargin(0);
        $pdf->SetPrintHeader(false);
        $pdf->SetPrintFooter(false);
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        $pdf->setFontSubsetting(true);
        $pdf->SetFont('freesans', '', 10, '', true);
        $pdf->AddPage();
        $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 0, $ln = 0, $fill = 0, $reseth = true, $align = '', $autopadding = true);
        return $pdf->Output('invoice.pdf', 'S');
    }
    
    public function sendInvoiceAdminNotification($invoice)
    {
        $fromName = DEFAULT_MAIL_SENDER['name'];
        $fromEmail = DEFAULT_MAIL_SENDER['email'];
        $pdf = $this->generatePdf($invoice);
        $mailer = \Core\ServiceLayer::getService('mailer');
        $message = $mailer->createMessage();
        $message->setSubject('INVOICE '.$invoice->getInvoiceNo().' #'.$invoice->getTransactionId());
        $message->setFrom(array($fromEmail => $fromName));
        $message->setTo(array('steinhauserova.eva3@gmail.com','billing-support@coachrufus.com'));
        $message->setBcc(array('marek.hubacek@coachrufus.com'));
        
        $message->setBody('Bola vygenerovaná faktúra', 'text/html');
        $attachment = \Swift_Attachment::newInstance($pdf, 'invoice_'.$invoice->getInvoiceNo().'.pdf', 'application/pdf');
        $message->attach($attachment);
        $mailer->sendMessage($message);
    }
    
    
    public function sendToEmail($invoice)
    {
         $buyNotificationSender = new BuyNotificationSender(new BuyStore($invoice));
         $buyNotificationSender->sendAll();
    }
    
}