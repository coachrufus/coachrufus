<?php namespace CR\Pay\Model;

use Core\Types\DateTimeEx;

class UserPackageManager
{
    private $invoiceRepository;
    private $actualPackageManager;

    function __construct
    (
        UserPackageRepository $userPackageRepository,
        ActualPackageManager $actualPackageManager
    )
    {
        $this->userPackageRepository = $userPackageRepository;
        $this->actualPackageManager = $actualPackageManager;
    }

    function addUserPackage($userPackage)
    {
        return $this->userPackageRepository->addUserPackage($userPackage);
    }

    function getUserPackageById($id)
    {
        $result = $this->userPackageRepository->getUserPackageById($id);
        if ($result)
        {
            $result = iterator_to_array($result);
            if (!empty($result['products']))
            {
                $result['products'] = json_decode($result['products'], true);
            }
        }
        return $result;
    }

    function getInvoiceUserPackageInfo(int $invoiceId)
    {
        $userPackages = $this->userPackageRepository->getUserPackagesByInvoiceId($invoiceId);
        $result = ['userPackages' => []];
        $isFirst = true;
        foreach ($userPackages as $userPackage)
        {
            $userPackage = iterator_to_array($userPackage);
            if (!empty($userPackage['products']))
            {
                $userPackage['products'] = json_decode($userPackage['products'], true);
            }
            $result['userPackages'][] = $userPackage;
            if ($isFirst)
            {
                $result['start_date'] = $userPackage['start_date'];
                $isFirst = false;
            }
        }
        $result['end_date'] = $userPackage['end_date'];
        $result['lastUserPackage'] = $userPackage;
        return $result;
    }

    function getLastUserPackage($teamId)
    {
        return $this->userPackageRepository->findLastUserPackage($teamId);
    }

    function getActualPackageLevel($teamId): int
    {
        $lastPackage = $this->getLastUserPackage($teamId);
        return $lastPackage === false ||
            DateTimeEx::now()->toTimeStamp() > DateTimeEx::parse($lastPackage['end_date'])->toTimeStamp() ?
                0 : $lastPackage['level'];
    }

    function getUserPackagesForTeam($teamId)
    {
        $userPackages = $this->userPackageRepository->getUserPackagesForTeam($teamId);
        if (!$userPackages) $userPackages = [];
        $result = [];
        $actualPackage = $this->actualPackageManager->getActualPackage();
        if ($actualPackage->isFree())
        {
            $freePackage = $actualPackage->toArray();
            $freePackage['isActual'] = true;
            $freePackage['actualPackage'] = $actualPackage;
            $result['free'] = $freePackage;
        }
        foreach ($userPackages as $userPackage)
        {
            $userPackage = iterator_to_array($userPackage);
            foreach (['date', 'start_date', 'end_date'] as $col)
            if (isset($userPackage[$col]))
            {
                $userPackage[$col] = DateTimeEx::parse($userPackage[$col]);
            }
            $userPackage['products'] = json_decode($userPackage['products'], true);
            if ($actualPackage->id == $userPackage['id'])
            {
                $userPackage['isActual'] = true;
                $userPackage['actualPackage'] = $actualPackage;
            }
            else
            {
                $userPackage['isActual'] = false;
                $userPackage['actualPackage'] = null;
            }
            $result[$userPackage['id']] = $userPackage;
        }
        return $result;
    }
    
    public function getUserPackages($user)
    {
         $userPackages = $this->userPackageRepository->getUserPackages($user->getId());
         return $userPackages;
    }
    
    public function enlargeUserPackage($packageId,$date)
    {
        $storage  = new \Core\DbStorage('user_packages');
        $data['end_date']  = $date->format('Y-m-d H:i:s');
        $storage->update($packageId, $data);
    }
    
    /**
     * get user who should be notified min 7 days before next year reccurency payment
     */
    public function getYearNotifyUsers()
    {
        return $this->userPackageRepository->YearNotifyUsers();
    }

}