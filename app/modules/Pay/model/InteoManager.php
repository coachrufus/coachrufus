<?php namespace CR\Pay\Model;

class InteoManager
{
    public function __construct()
    {
        
    }
    
    public function sendInvoice(\CR\Pay\Model\Invoice $invoice)
    {
        $items = array();
        $items['name'] = $invoice->getPackageName();
        $items['description'] = $invoice->getPackageName();
        $items['count'] = 1;
        $items['unitPrice'] = $invoice->getRawPrice();
        $items['unitPriceWithVat'] = $invoice->getAmount();
        $items['totalPrice'] =  $invoice->getRawPrice();
        $items['totalPriceWithVat'] = $invoice->getAmount();
        $items['vat'] = $invoice->getVatPercentage();
        $items['typeId'] = 2;
        
        $createDate = new \DateTime();
        
        $omegaData = array();
        $omegaData['documentNumber'] = $invoice->getInvoiceNo();
        $omegaData['orderNumber'] =  $invoice->getVs();
        $omegaData['totalPrice'] =$invoice->getRawPrice();
        $omegaData['totalPriceWithVat'] = $invoice->getAmount();
        $omegaData['createDate'] = $createDate->format('c');
        $omegaData['completionDate'] = $createDate->format('c');
        $omegaData['clientName'] = $invoice->getFullName();
        $omegaData['clientContactName'] = $invoice->getFullName();
        $omegaData['clientContactSurname'] = '';
        $omegaData['clientStreet'] = $invoice->getStreet();
        $omegaData['clientPostCode'] =$invoice->getZip();
        $omegaData['clientTown'] = $invoice->getCity();
        $omegaData['clientCountry'] = $invoice->getPayCountry();
        $omegaData['clientPhone'] =  '';
        $omegaData['clientEmail'] = $invoice->getEmail();
        $omegaData['clientRegistrationId'] = '';
        $omegaData['clientTaxId'] = '';
        $omegaData['clientVatId'] ='';
        $omegaData['variableSymbol'] = $invoice->getVs();
        $omegaData['senderBankAccount'] = '0000';
        $omegaData['paymentType'] = 'paypal';
        $omegaData['deliveryType'] = 'online';
        $omegaData['currency'] = 'EUR';
        $omegaData['items'] = array($items);
        $curlData = json_encode(array($omegaData));
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://eshops.inteo.sk/api/v1/invoices/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $curlData);
       

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          "Content-Type: application/json",
          "Authorization: Bearer 7346b8c3-b33a-4e10-8898-8fe141154b6d"
        ));

        $response = json_decode(curl_exec($ch));
        curl_close($ch);

        t_dump($response);
    }
}