<?php namespace CR\Pay\Model;

use Core\Db;

class OrderDataRepository
{
    use Db;
    
    private $db;
    
    function __construct()
    {
        $this->db = $this->getDb();
    }

    private function getOrderNo(int $id)
    {
        $timeStamp = time();
        $now = new \DateTime();
        return "{$id}{$now->format('y')}{$timeStamp}";
    }

    function save($data)
    {
        $this->db->transaction = "BEGIN";
        $item = $this->db->order_data()->insert([
            'data' => $data['data'],
            'payment_id' => $data['payment_id'],
            'team_id' => $data['team_id'],
            'user_id' => $data['user_id'],
            'user_package_id' => $data['user_package_id'],
            'renew_payment' => $data['renew_payment'],
            'created_at' => $data['created_at'],
            'variant' => $data['variant'],
            'total_price' => $data['total_price'],
            'currency' => $data['currency'],
            'paid' => $data['paid'],
            'status' => $data['status'],
            'discount' => $data['discount'],
        ]);
        $orderNo = $this->getOrderNo($item['id']);
        $item->update(['order_no' => $orderNo]);
        $this->db->transaction = "COMMIT";
        return $orderNo;
    }
    
    function loadByOrderNo($orderNumber)
    {
        return $this->db
            ->order_data('order_no', $orderNumber)
            ->fetch();
    }
    
     function loadByOrderPaymentId($id)
    {
        return $this->db
            ->order_data('payment_id', $id)
            ->fetch();
    }
}