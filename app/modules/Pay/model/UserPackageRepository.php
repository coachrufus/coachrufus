<?php namespace CR\Pay\Model;

use Core\Db;
use Core\Types\DateTimeEx;

class PackageTimeCalculator
{
    private $startDate;
    private $endDate;
    private $now;
    
    function __construct(DateTimeEx $startDate, DateTimeEx $endDate, DateTimeEx $now)
    {
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->now = $now;
    }

    function getUsedSeconds(): int
    {
        return $this->now->toTimeStamp() - $this->startDate->toTimeStamp();
    }

    function getUnusedSeconds(): int
    {
        return $this->endDate->toTimeStamp() - $this->now->toTimeStamp();
    }

    function getTotalSeconds(): int
    {
        return $this->endDate->toTimeStamp() - $this->startDate->toTimeStamp();
    }
}

class PackageCreditCalculator
{
    function calculateCredit(float $price, int $totalSeconds, int $unusedSeconds): float
    {
        return ((float)$price / (float)$totalSeconds) * (float)$unusedSeconds;
    }
}

class UserPackageRepository
{
    use Db;
    
    private $db;
    private $packageManager;

    function __construct(PackageManager $packageManager)
    {
        $this->db = $this->getDb();
        $this->packageManager = $packageManager;
    }

    function transactionBegin()
    {
        $this->db->transaction = "BEGIN";
    }

    function transactionCommit()
    {
        $this->db->transaction = "COMMIT";
    }
    
    public function getStorage()
    {
        $storage = new \CR\Pay\Model\UserPackageStorage();
        return $storage;
    }


    function addUserPackage($userPackage)
    {
        return $this->db->user_packages()->insert($userPackage);
    }

    function findLastUserPackage($teamId)
    {
        $now = DateTimeEx::now();
        return $this->db->user_packages()
            ->where('team_id', $teamId)
            ->where('is_canceled', 0)
            ->where('end_date > ?', $now->toDateTime())
            ->order('id DESC')
            ->limit(1)
            ->fetch();
    }

    function toArray($result, $replacer)
    {
        if (!$result) return [];
        else
        {
            $arr = [];
            foreach ($result as $row) $arr[] = $replacer(iterator_to_array($row));
            return $arr;
        }
    }

    function getUsedUserPackages($teamId, $now)
    {
        return $this->toArray($this->db->user_packages()
            ->where('is_canceled', 0)
            ->where('team_id', $teamId)
            ->where('? >= start_date AND ? < end_date', $now->toDateTime(), $now->toDateTime())
            ->order('id ASC'), function($userPackage) use($now)
            {
                $userPackage['isUsed'] = true;
                $timeIntervals = new PackageTimeCalculator(
                    DateTimeEx::parse($userPackage['start_date']),
                    DateTimeEx::parse($userPackage['end_date']),
                    $now
                );
                $userPackage['usedSecs'] = $timeIntervals->getUsedSeconds();
                $userPackage['unusedSecs'] = $timeIntervals->getUnusedSeconds();
                $userPackage['totalSecs'] = $timeIntervals->getTotalSeconds();
                $userPackage['price'] = (float)(is_null($userPackage['price_month']) ? $userPackage['price_year'] : $userPackage['price_month']);
                $priceCalculator = new PackageCreditCalculator();
                $userPackage['credit'] = $priceCalculator->calculateCredit($userPackage['price'], $userPackage['totalSecs'], $userPackage['unusedSecs']);
                return $userPackage;
            });
    }

    function getUnusedUserPackages($teamId, $now)
    {
        return $this->toArray($this->db->user_packages()
            ->where('is_canceled', 0)
            ->where('team_id', $teamId)
            ->where('? < start_date', $now->toDateTime())
            ->order('id ASC'), function($userPackage) use($now)
            {
                $userPackage['isUsed'] = false;
                $timeIntervals = new PackageTimeCalculator(
                    DateTimeEx::parse($userPackage['start_date']),
                    DateTimeEx::parse($userPackage['end_date']),
                    $now
                );
                $userPackage['usedSecs'] = 0;
                $userPackage['unusedSecs'] = $timeIntervals->getTotalSeconds();
                $userPackage['totalSecs'] = $timeIntervals->getTotalSeconds();
                $userPackage['price'] = (float)(is_null($userPackage['price_month']) ? $userPackage['price_year'] : $userPackage['price_month']);
                $userPackage['credit'] = $userPackage['price'];
                return $userPackage;
            });
    }

    function getActualLowerPackages($teamId, $now = null)
    {
        if (is_null($now)) $now = DateTimeEx::now();
        $used = $this->getUsedUserPackages($teamId, $now);
        $unused = $this->getUnusedUserPackages($teamId, $now);
        return array_merge($used, $unused);
    }

    function getUserPackageById($id)
    {
        return $this->db->user_packages()
            ->where('id', $id)
            ->fetch();
    }

    function getUserPackagesByInvoiceId($invoiceId)
    {
        $userPackageIds = [];
        foreach ($this->db->invoices_user_packages('invoice_id', $invoiceId) as $userPackage)
        {
            $userPackageIds[] = (int)$userPackage['user_package_id'];
        }
        return $this->db->user_packages()
            ->where('id', $userPackageIds)
            ->order('id');
    }

    function getUserPackagesByGuids($guids)
    {
        return $this->db->user_packages()
            ->where('guid', $guids)
            ->order('id');
    }

    function cancelUserPackages($ids, $value = 1)
    {
        $this->db->user_packages()
            ->where('id', $ids)
            ->update(['is_canceled' => $value]);
    }

    function addUserPackages($userPackages)
    {
        $result = $this->db->user_packages();
        return call_user_func_array(array($result, 'insert'), $userPackages);
    }

    function getUserPackagesForTeam($teamId)
    {
        return $this->db->user_packages()
            ->where('team_id', $teamId)
            ->order('id DESC');
    }
    
    function getUserPackages($userId)
    {
        $data =  $this->db->user_packages()
            ->where('user_id', $userId)
            ->order('id DESC');
        
        $data =iterator_to_array($data);

        $list = array();
        $mapper = new \Core\EntityMapper('CR\Pay\Model\UserPackage');
        foreach($data as $d)
        {
            $a = iterator_to_array($d);
            $package = new \CR\Pay\Model\UserPackage();
            $mapper->updateEntityFromArray($package, $d);
            $list[] =$package; 
        }
        return $list;
    }
    
    public function userHadProPackage($userId)
    {
        $data =  $this->db->user_packages()
            ->where('user_id', $userId)
            ->where('name', 'PRO');
        $data =iterator_to_array($data);
        
        return $data;
    }
    
    public function YearNotifyUsers()
    {
        $userData = $this->getStorage()->findYearNotifyUsers();
        $users = array();
        $mapper = new \Core\EntityMapper('\Webteamer\Player\Model\Player');
        foreach($userData as $data)
        {
             $users[] = $mapper->createEntityFromArray($data);
             
        }
        return $users;
    }
}
