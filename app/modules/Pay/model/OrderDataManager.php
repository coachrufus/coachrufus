<?php namespace CR\Pay\Model;

class OrderDataManager
{
    private $orderDataRepository;
    
    function __construct(OrderDataRepository $orderDataRepository)
    {
        $this->orderDataRepository = $orderDataRepository;
    }

    function save($data)
    {
        $data['data'] = json_encode($data);
        
        
        return $this->orderDataRepository->save($data);
    }
    
    function load($orderNumber)
    {
        $result = $this->orderDataRepository->loadByOrderNo($orderNumber);
        return json_decode($result['data'], true);
    }
    
    function loadByPaymentId($id)
    {
        $result = $this->orderDataRepository->loadByOrderPaymentId($id);
        return json_decode($result['data'], true);
    }
}