<?php namespace CR\Pay\Model;

use Core\ActiveTeamManager;
use Core\Types\DateTimeEx;
use Core\Collections\Iter;
use Core\GUID;

class PackageUpdater
{
    private $packageManager;
    private $userPackageRepository;
    private $activeTeam;
    private $user;
    
    function __construct
    (
        PackageManager $packageManager,
        UserPackageRepository $userPackageRepository,
        $security,
        ActiveTeamManager $activeTeamManager
    )
    {
        $this->packageManager = $packageManager;
        $this->userPackageRepository = $userPackageRepository;
        $this->activeTeam = $activeTeamManager->getActiveTeam();
        $this->user = $security->getIdentity()->getUser();
    }

    private function getCredits($lowerUserPackages)
    {
        $total = 0.0;
        foreach ($lowerUserPackages as $p) $total += $p['credit'];
        return $total;
    }

    private function toUserPackageIds($lowerUserPackages)
    {
        $ids = [];
        foreach ($lowerUserPackages as $p) $ids[] += $p['id'];
        return $ids;
    }

    private function cancelUserPackages($lowerUserPackages, $value = 1)
    {
        $ids = $this->toUserPackageIds($lowerUserPackages);
        $this->userPackageRepository->cancelUserPackages($ids, $value);
    }

    private function getPackagePeriods(float $credits, float $priceYear, float $priceMonth)
    {
        while ($credits !== 0)
        {
            if ($credits > $priceYear)
            {
                $credits -= $priceYear;
                yield ['year', 1, $priceYear];
            }
            else if ($credits > $priceMonth)
            {
                $credits -= $priceMonth;
                yield ['month', 1, $priceMonth];
            }
            else
            {
                $monthSeconds = 60 * 60 * 24 * 30;
                yield ['second', ((float)$monthSeconds / $priceMonth) * $credits, round($credits, 2)];
                break;
            }
        }
    }

    private function getEndDate(DateTimeEx $startDate, $periodInfo)
    {
        list($period, $value, $price) = $periodInfo;
        return $startDate->{"add{$period}"}($value);
    }

    private function toUserPackage($periodInfo, $package, DateTimeEx $startDate, DateTimeEx $now)
    {
        list($period, $value, $price) = $periodInfo;
        $period = $period === 'second' ? 'month' : $period;
        return [
            'user_id' => $this->user->getId(),
            'package_id' => $package['id'],
            'team_id' => $this->activeTeam['id'],
            'team_name' => $this->activeTeam['name'],
            'date' => $now,
            'start_date' => $startDate->toDateTime(),
            'end_date' => $this->getEndDate($startDate, $periodInfo)->toDateTime(),
            'name' => $package['name'],
            'note' => $package['note'],
            'level' => $package['level'],
            "price_{$period}" => $price,
            'products' => json_encode($package['products']),
            'player_limit' => $package['player_limit'],
            'guid' => GUID::create()
        ];
    }



    private function toUserPackages($package, $periodInfos, DateTimeEx $now)
    {
        $userPackages = [];
        $startDate = $now;
        foreach ($periodInfos as $periodInfo)
        {
            $userPackage = $this->toUserPackage($periodInfo, $package, $startDate, $now);
            $userPackages[] = $userPackage;
            $startDate = DateTimeEx::of($userPackage['end_date']);
        }
        return $userPackages;
    }

    function getUpdatePackageInfo(int $packageId)
    {
        $now = DateTimeEx::now();
        $products = $this->packageManager->getProducts();
        $package = $this->packageManager->getPackageWithProducts($packageId, $products);
        $activeTeamId = $this->activeTeam['id'];
        if (is_null($activeTeamId)) throw new \Exception('Team not selected');
        $lowerPackages = $this->userPackageRepository->getActualLowerPackages($activeTeamId, $now);
        $credits = $this->getCredits($lowerPackages);
        $periodInfos = $this->getPackagePeriods($credits, $package['price_year'], $package['price_month']);
        $userPackages = $this->toUserPackages($package, $periodInfos, $now);
        $iter = new Iter($userPackages);
        return (object)[
            'lowerPackages' => $lowerPackages,
            'credits' => $credits,
            'userPackages' => $userPackages,
            'startDate' => $iter->first()['start_date'],
            'endDate' => $iter->last()['end_date']
        ];
    }

    private function reloadPackagesByGuids($updatePackageInfo)
    {
        $guids = [];
        foreach ($updatePackageInfo->userPackages as $userPackage) $guids[] = $userPackage['guid'];
        $userPackages = $this->userPackageRepository->getUserPackagesByGuids($guids);
        $result = [];
        foreach ($userPackages as $userPackage) $result[] = iterator_to_array($userPackage);
        $updatePackageInfo->userPackages = $result;
        return $updatePackageInfo;
    }

    function updatePackagesTo(int $packageId)
    {
        $updatePackageInfo = $this->getUpdatePackageInfo($packageId);
        $this->userPackageRepository->transactionBegin();
        $this->cancelUserPackages($updatePackageInfo->lowerPackages);
        $this->userPackageRepository->addUserPackages($updatePackageInfo->userPackages);
        $this->userPackageRepository->transactionCommit();
        return $this->reloadPackagesByGuids($updatePackageInfo);
    }
}