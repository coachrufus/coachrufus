<?php
namespace CR\Pay\Model;
use Core\DbStorage;
class InvoiceStorage extends DbStorage {
    
	public function findLastCounterNumber($prefix)
        {
            
            $data = \Core\DbQuery::prepare('SELECT count(*) as counter_num from invoices where invoice_no like "'.$prefix.'%" ')
		->execute()
		->fetchOne(\PDO::FETCH_ASSOC);
		return $data;
        }
        

	
    
	public function getCountryByCode($code)
        {
            
            $data = \Core\DbQuery::prepare('SELECT *  from country where country_code =  "'.$code.'" or iso_code = "'.$code.'" ')
		->execute()
		->fetchOne(\PDO::FETCH_ASSOC);
		return $data;
        }
        
        
        public function createInvoicePackage($invoiceId,$packageId)
        {
             \Core\DbQuery::insertFromArray(array('invoice_id' => $invoiceId,'user_package_id' => $packageId), 'invoices_user_packages' );
        }

	
	
}