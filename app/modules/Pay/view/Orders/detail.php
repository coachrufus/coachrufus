<style>
.price-list th,
.price-list td
{
    text-align: center;
}
.price-list .glyphicon-ok,
.price-list .glyphicon-ok-circle
{
    color: #00a300;
}
.price-list .glyphicon-remove
{
    color: #777;
}
.price-list th.caption,
.price-list td.caption
{
    text-align: left;
    color: #333;
    font-weight: normal;
}
.price-list td.invoice-data-field
{
    text-align: left;
    font-weight: bold;
}
.price-list thead tr th,
.price-list tfoot tr th
{
    color: #00b3be;
}
.price-list tbody strong
{
    color: #00759a;
}
</style>

<?php function renderPackageNames($userPackage, $translator) { ob_start(); ?>
<tr>
    <th class="caption"><?php echo $translator->translate('Package') ?></th>
    <th><?php echo $userPackage['name'] ?></th>
</tr>
<?php return ob_get_clean(); } ?>

<?php function renderInvoicingData($translator) { ob_start(); ?>
<tr>
    <th colspan="3"><?php echo $translator->translate('Invoicing data') ?></th>
</tr>
<?php return ob_get_clean(); } ?>

<?php function renderBuyButton($translator) { ob_start(); ?>
<tr>
    <td colspan="3">
        <button
            type="button"
            class="buy-button btn btn-success">
            <?php echo $translator->translate('Invoice PDF') ?>
        </button>
    </td>
</tr>
<?php return ob_get_clean(); } ?>

<?php Core\Layout::getInstance()->startSlot('team_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/Team/view/team/_team_menu.php', ['team' => $team]) ?>
<?php Core\Layout::getInstance()->endSlot('team_menu') ?>
<section class="content-header">
    <h1><?php echo $translator->translate('Order') . ": <strong>{$invoice['vs']}</strong>" ?></h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <table class="table table-bordered price-list" id="table">
                    <thead>
                        <?php echo renderPackageNames($userPackage, $translator) ?>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan="2"><?php echo $startDate->toString('d.m.Y') . ' - ' . $endDate->toString('d.m.Y') ?></td>
                        </tr>
                        <?php if (!is_null($invoice['price_custom'])): ?>
                        <tr>
                            <td class="caption"><?php echo $translator->translate('Price') ?></td>
                            <td><?php echo "<strong>0 &euro;</strong>" ?></td>
                        </tr>
                        <?php elseif (!is_null($invoice['price_year'])): ?>
                        <tr>
                            <td class="caption"><?php echo $translator->translate('Price') ?></td>
                            <td><?php echo "<strong>{$invoice['price_year']} &euro;</strong> per {$translator->translate('year')}" ?></td>
                        </tr>
                        <?php else: ?>
                        <tr>
                            <td class="caption"><?php echo $translator->translate('Price') ?></td>
                            <td><?php echo "<strong>{$invoice['price_month']} &euro;</strong> per {$translator->translate('month')}" ?></td>
                        </tr>
                        <?php endif; ?>
                        <tr>
                            <td class="caption"><?php echo $translator->translate('Player limit') ?></td>
                            <td><?php echo is_null($invoice['player_limit']) ? "{$translator->translate('unlimited')}<br>{$translator->translate('players')}" : "&lt; {$invoice['player_limit']}<br>{$translator->translate('players')}" ?></td>
                        </tr>
                        <?php foreach ($products as $product): ?>
                        <tr>
                            <td class="caption product-name"><?php echo $product['name'] ?></td>
                            <td><i class="glyphicon <?php echo $userPackage['products'][$product['id']]['is_enabled'] ? 'glyphicon-ok' : 'glyphicon-remove' ?>"></i></td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
        
        <div class="col-md-6">
            <div class="box">
                <table class="table table-bordered price-list" id="table">
                    <thead>
                        <?php echo renderInvoicingData($translator) ?>
                    </thead>
                    <tbody>
                        <?php foreach ($invoiceFields1 as $k => $label): ?>
                        <tr>
                            <td class="caption product-name"><?php echo $label ?></td>
                            <td class="invoice-data-field"><?php echo $k === 'invoice_date' ? Core\Types\DateTimeEx::parse($invoice[$k])->toString('d.m.Y') : $invoice[$k] ?></td>
                            <td><i class="glyphicon <?php echo empty($invoice[$k]) ? '' : 'glyphicon-ok-circle' ?>"></i></td>
                        </tr>
                        <?php endforeach; ?>
                        <tr><td colspan="3"></td></tr>
                        <?php foreach ($invoiceFields2 as $k => $label): ?>
                        <tr>
                            <td class="caption product-name"><?php echo $label ?></td>
                            <td class="invoice-data-field"><?php echo $k === 'country_id' ? $countries[$invoice[$k]] : $invoice[$k] ?></td>
                            <td><i class="glyphicon <?php echo empty($invoice[$k]) ? '' : 'glyphicon-ok-circle' ?>"></i></td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                    <tfoot>
                        <?php echo renderBuyButton($translator) ?>
                    </tfoot>
                </table>
            </div>
        </div>
        
    </div>
</section>
<?php $layout->startSlot('javascript') ?>
<script>
    $(function()
    {
        $("#table button.buy-button").click(function()
        {
            window.location = '/invoices/<?php echo $invoice['invoice_no']; ?>.pdf';
        });
    });
</script>
<?php $layout->endSlot('javascript') ?>