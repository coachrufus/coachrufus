<style>
.price-list thead tr th,
.price-list tfoot tr th
{
    color: #00b3be;
}
.price-list tbody strong,
.price-list tbody a
{
    color: #00759a;
}
.price-list tbody strike strong,
.price-list tbody strike a
{
    color: #999;
}
</style>

<?php Core\Layout::getInstance()->startSlot('team_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/Team/view/team/_team_menu.php', ['team' => $team]) ?>
<?php Core\Layout::getInstance()->endSlot('team_menu') ?>
<section class="content-header">
    <h1><?php echo $translator->translate('My orders') ?></h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <table class="table table-bordered price-list" id="table">
                    <thead>
                        <tr>
                            <th><?php echo $translator->translate('Package') ?></th>
                            <th></th>
                            <th></th>
                            <th><?php echo $translator->translate('Order No. / VS') ?></th>
                            <th><?php echo $translator->translate('Invoice No.') ?></th>
                            <th><?php echo $translator->translate('Availibility') ?></th>
                            <th><?php echo $translator->translate('Price') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($invoices as $invoice): ?>
                        <?php 
                            if (!is_null($invoice['price_year'])) $period = "year";
                            if (!is_null($invoice['price_month'])) $period = "month";
                            if (!is_null($invoice['price_custom'])) $period = "custom";
                            $periodText = $period === "custom" ? "upgrade" : $period;
                        ?>
                        <tr>
                            <td class="caption product-name"><a href="/orders/detail/<?php echo $invoice['vs'] ?>"><?php echo "<strong>{$invoice['package_name']}</strong> ({$periodText})";  ?></a></td>
                            <td class=""><a href="/orders/detail/<?php echo $invoice['vs'] ?>"><?php echo $translator->translate('detail') ?></a></td>
                            <td class=""><a href="/invoices/<?php echo $invoice['invoice_no'] ?>.pdf"><?php echo $translator->translate('PDF') ?></a></td>
                            <td class=""><?php echo $invoice['vs'] ?></td>
                            <td class=""><?php echo $invoice['invoice_no'] ?></td>
                            <?php if ($invoice['userPackage']['is_canceled'] == 1): ?>
                            <td class=""><strike><?php echo $invoice['start_date']->toString('d.m.Y') . ' - ' . $invoice['end_date']->toString('d.m.Y') ?></strike></td>
                            <?php else: ?>
                            <td class=""><?php echo $invoice['start_date']->toString('d.m.Y') . ' - ' . $invoice['end_date']->toString('d.m.Y') ?></td>
                            <?php endif; ?>
                            <td class=""><?php echo $invoice["price_{$period}"] . ' &euro;'; ?></td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            
             <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" target="_top">
                <input type="hidden" name="cmd" value="_xclick">
                <input type="hidden" name="business" value="marek.hubacek-facilitator@gmail.com">

                <input type="hidden" name="amount" value="<?php echo Core\ServiceLayer::getService('TeamCreditManager')->getProPrice() ?>">
                <input type="hidden" name="item_name" value="PRO">
                <input type="hidden" name="item_number" value="<?php echo $team->getId() ?>">




                <input type="image" src="https://www.sandbox.paypal.com/en_US/i/btn/btn_buynow_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                <img alt="" border="0" src="https://www.sandbox.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1">
            </form>
        </div>
    </div>
</section>
<?php $layout->startSlot('javascript') ?>
<script>
</script>
<?php $layout->endSlot('javascript') ?>