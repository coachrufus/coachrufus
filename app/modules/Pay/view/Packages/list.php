<style>
.price-list thead tr th,
.price-list tfoot tr th
{
    color: #00b3be;
}
.price-list tbody strong,
.price-list tbody a
{
    color: #00759a;
}
.price-list tbody strike strong,
.price-list tbody strike a
{
    color: #999;
}
tr.actual td
{
    background: #d2ffd5;
}
tr.expired td
{
    /*background: #fefefe;*/
}
.price-list tbody tr.expired td,
.price-list tbody tr.expired td strong,
.price-list tbody tr.expired td a
{
    color: #999;
}
</style>

<?php Core\Layout::getInstance()->startSlot('team_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/Team/view/team/_team_menu.php', ['team' => $team]) ?>
<?php Core\Layout::getInstance()->endSlot('team_menu') ?>
<section class="content-header">
    <h1><?php echo $translator->translate('My Packages') ?></h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <table class="table table-bordered price-list" id="table">
                    <thead>
                        <tr>
                            <th><?php echo $translator->translate('Package') ?></th>
                            <th></th>
                            <th><?php echo $translator->translate('Availibility') ?></th>
                            <th>Duration</th>
                            <th>Remains</th>
                            <th><?php echo $translator->translate('Price') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $isExpired = false; ?>
                        <?php foreach ($userPackages as $userPackage): ?>
                        <?php 
                            if (!is_null($userPackage['price_year'])) $period = "year";
                            elseif (!is_null($userPackage['price_month'])) $period = "month";
                            else $period = "custom";
                            $periodText = $period === "custom" ? "upgrade" : $period;
                        ?>
                        <tr class="<?php echo $isExpired ? ' expired' : '' ?><?php echo $userPackage['isActual'] ? ' actual' : '' ?>">
                            <?php if (isset($userPackage['package_id'])): ?>
                            <td class="caption product-name"><?php echo "<strong>{$userPackage['name']} ({$periodText})</strong>";  ?></td>
                            <?php else: ?>
                            <td class="caption product-name"><?php echo "<strong>{$userPackage['name']}</strong>";  ?></td>
                            <?php endif; ?>
                            <td class=""><strong><?php echo $userPackage['isActual'] ? 'Actual' : '' ?><?php echo $userPackage['is_canceled'] ? 'Upgraded' : '' ?></strong></td>
                            <?php if ($userPackage['is_canceled'] == 1): ?>
                            <td class=""><strike><?php echo $userPackage['start_date']->toString('d.m.Y') . ' - ' . $userPackage['end_date']->toString('d.m.Y') ?></strike></td>
                            <?php else: ?>
                                <?php if (isset($userPackage['package_id'])): ?>
                                <td class=""><?php echo $userPackage['start_date']->toString('d.m.Y') . ' - ' . $userPackage['end_date']->toString('d.m.Y') ?></td>
                                <?php else: ?>
                                <td class=""><?php echo $translator->translate('Now') ?></td>
                                <?php endif; ?>
                            <?php endif; ?>

                            <?php if ($userPackage['isActual']): ?>
                            <td class="">
                                <?php $duration = $userPackage['actualPackage']->getDuration(); ?>
                                <span class="duration"><?php echo (is_null($duration) ? '-' : "{$duration} {$translator->translate('days')}"); ?></span>
                            </td>
                            <td class="">
                                <?php $remains = $userPackage['actualPackage']->getRemains(); ?>
                                <span class="remains"><?php echo (is_null($remains) ? '-' : "{$remains} {$translator->translate('days')}"); ?></span></td>
                            </td>
                            <?php else: ?>
                            <td></td>
                            <td></td>
                            <?php endif; ?>

                            <td class=""><?php echo $userPackage["price_{$period}"] . ' &euro;'; ?></td>
                        </tr>
                        <?php if ($userPackage['isActual']) $isExpired = true; ?>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
<?php $layout->startSlot('javascript') ?>
<?php $layout->endSlot('javascript') ?>