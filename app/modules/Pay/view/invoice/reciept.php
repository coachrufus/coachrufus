<!DOCTYPE html>
<html>
    <head>
        <style>
            body {
                font-size:3.5mm;
                color:#47545B;
            }

            td.divide {
                border-top:0.2mm solid #E3E4E5;
                height:2mm;
            }
        </style>
    </head>

    <body>



        <table cellpadding="0" cellspacing="0">
            <tr>
                <td colspan="2" style="font-size:6mm; height:20mm;"><?php echo $translator->translate('Invoice ID') ?> <?php echo $invoice->getInvoiceNo() ?></td>
                <td></td>
                <td  colspan="2" style="text-align:right;"></td>
            </tr>
            <tr>
                <td style="height:20mm;" colspan="2" >Team Management, s.r.o.,<br />
                    Cezmínova 5, Bratislava, Slovakia
                </td>
                <td  colspan="3">
                       <?php if('SVK' != $invoice->getPayCountry() and 'SK' !=  $invoice->getPayCountry()): ?>
                    EU VAT ID: SK2120304263 <br />
                     <?php endif; ?>
                    billing-support@coachrufus.com
                </td>
            </tr>

            <tr style="font-weight:bold;">
                <td style="height:6mm; width:49mm"><?php echo $translator->translate('INVOICE_TO') ?></td>
                <td style="width:25mm;"><?php echo $translator->translate('INVOICE_DATE') ?></td>
                <td><?php echo $translator->translate('INVOICE_PAYMENT') ?></td>
                <td><?php echo $translator->translate('INVOICE_TR_ID') ?></td>
                <td style="text-align:right;"><?php echo $translator->translate('INVOICE_AMOUNT') ?></td>
            </tr>
            <tr><td colspan="5" class="divide"></td></tr>
            <tr>
                <td ><?php echo $invoice->getEmail() ?><br />
                    <?php echo $invoice->getFullName() ?><br />
                    <?php echo $invoice->getAddress() ?><br />
                   
                </td>
                <td><?php echo $invoice->getInvoiceDateFormated() ?></td>
                <td><?php echo $invoice->getPaymentCard() ?></td>
                <td><?php echo $invoice->getTransactionId() ?></td>
                <td style="text-align:right;"><?php echo $invoice->getAmount() ?> &euro;</td>
            </tr>
            <tr><td colspan="5" class="divide"></td></tr>

            <tr><td colspan="5" style="height:10mm;"></td></tr>
            <tr style="font-weight:bold;">
                <td colspan="4" style="height:8mm;"><?php echo $translator->translate('INVOICE_PRODUCT') ?></td>
                <td style="text-align:right;"><?php echo $translator->translate('INVOICE_AMOUNT') ?></td>
            </tr>
            <tr><td colspan="5" class="divide"></td></tr>
            <tr>
                <td  colspan="4"><?php echo $invoice->getFormatedDescription() ?>
                </td>
                <td style="text-align:right;"><?php echo $invoice->getAmount() ?> &euro;
                </td>
            </tr>
            
            <tr><td colspan="5" style="height:5mm;"></td></tr>
            <tr><td colspan="5" class="divide"></td></tr>

            <tr>
                <td  colspan="4"><?php echo $translator->translate('Subtotal') ?></td>
                <td style="text-align:right;"><?php echo $invoice->getRawPrice() ?> &euro;</td>
            </tr>
            
         
            <tr>
                <td  colspan="4">+ <?php echo $translator->translate('VAT') ?> (<?php echo $invoice->getVatPercentage()  ?> %)</td>
                <td style="text-align:right;"><?php echo $invoice->getVat() ?> &euro;</td>
            </tr>
           
            <tr><td colspan="5" style="height:5mm;"></td></tr>
            <tr><td colspan="5" class="divide"></td></tr>

            <tr style="font-weight:bold;">
                <td  colspan="4" ><?php echo $translator->translate('Total') ?>
                </td>
                <td style="text-align:right;"><?php echo $invoice->getAmount() ?> &euro;
                </td>
            </tr>
            <tr><td colspan="5" style="height:10mm;"></td></tr>
            <tr><td colspan="5"><?php echo $translator->translate('INVOICE_DELIVERY_COUNTRY') ?> <?php echo $countryName  ?>, <?php echo $translator->translate('INVOICE_PRICE_VAT_NOTE') ?><br />
                    
                    <?php if('SVK' == $invoice->getPayCountry() or 'SK' ==  $invoice->getPayCountry() ): ?>
                    <?php echo $translator->translate('SK_NON_VAT_PAYERS') ?><br />
                    <?php endif; ?>
                    
                    <?php echo $translator->translate('INVOICE_CREATE_NOTE') ?><br />
                    <?php echo $translator->translate('INVOICE_ORIGINAL_VERSION_NOTE') ?>
                </td></tr>
        </table>
    </body>
</html>