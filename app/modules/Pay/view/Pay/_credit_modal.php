<?php
$showTeamChoice = false;
$security = \Core\ServiceLayer::getService('security');
$userIdentity = $security->getIdentity();
$user = $userIdentity->getUser();
if ('player_dashboard' == $router->getCurrentRouteName())
{
    $teams = $userIdentity->getUserTeams();
    $showTeamChoice = true;
}
?>

    
<div class="modal fade" id="team-credit-modal" tabindex="-1" role="dialog" aria-labelledby="<?php echo $translator->translate('Credit') ?>">
    <div class="modal-dialog" role="document">
        <button class="close-trigger" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="ico ico-close"></i></span></button>
        <div class="modal-content">

            <div class="modal-body">
                <div id="team-credit-modal-loader">
                     <i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><br />
                    <?php echo $translator->translate('Intializing payment gate ...') ?>
                </div>
                
                
                <form action="<?php echo $router->link('gopay_payment') ?>" method="post" target="_top" target="_blank" id="credit_modal_form">
                    <input type="hidden" name="promo_code" id="discountCode" value="">
                    <input type="hidden" name="item_name" value="PRO">
                    <input type="hidden" name="team_id" value="<?php echo $activeTeam['id'] ?>">
                    <input type="hidden" name="payer" value="<?php echo $user->getId() ?>">
                    <input type="hidden" name="variant" id="variant_id" value="1">
                    <input type="hidden" name="" id="credit_modal_form_url" value="<?php echo $router->link('gopay_payment_status') ?>">


                    <?php $activeTeam = Core\ServiceLayer::getService('ActiveTeamManager')->getActiveTeam() ?>
                    <div class="payment-text"><?php echo $translator->translate('PAYMENT_DIALOG_TEXT') ?></div>

                    <?php if ($showTeamChoice): ?>
                        <div class="form-group col-sm-12">
                                <select name="team_id" class="form-control" id="payement_team_id">
                                    <option value=""><?php echo $translator->translate('Choose your team') ?></option>
                                    <?php foreach ($teams as $team): ?>
                                        <option value="<?php echo $team->getId() ?>"><?php echo $team->getName() ?></option>
                                    <?php endforeach; ?>
                                </select>                               
                        </div>
                    <?php else: ?>
                        <h2><?php echo $activeTeam['name'] ?></h2>
                         <input type="hidden" name="team_id" value="<?php echo $activeTeam['id'] ?>">
                    <?php endif; ?>
                       
                         <div class="row">
                             <div class="col-sm-12 text-center cr-modal-tx2"><?php echo $translator->translate('Subscription') ?></div>
                         </div>

                    <div class="row pay-label-row">
                         <div class="col-xs-6 text-center">
                            <span data-variant="1" data-price="18" class="pay-label  pay-label-active" type="submit"><strong>18 &euro;</strong> <?php echo $translator->translate('per month') ?> / <?php echo $translator->translate('team') ?></span>
                        </div>
                        <div class="col-xs-6 text-center">
                            <span class="save"><i class="ico ico-40-save"></i></span>
                            <span data-variant="12" data-price="129" class="pay-label" type="submit"><strong>129 &euro;</strong><?php echo $translator->translate('per year') ?> / <?php echo $translator->translate('team') ?></span>
                        </div>
                    </div>
                         <p class="payment-text-sm"><?php echo $translator->translate('Iba 8,60  &euro; na hráča ročne') ?>* <i class="ico ico-question" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo $translator->translate('PAYMENT_TOOLTIP') ?>"></i></p>
                         
                      
                    <div class="row" id="promo-result-row">
                    
                    </div>
                
                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <button class="pay-button pay-button-w" type="submit"><?php echo $translator->translate('Buy') ?></button>
                        </div>
                    </div>
                         
                    <div class="row">
                       <div class="col-sm-12 text-center cr-modal-vop-text"><?php echo $translator->translate('CREDIT_MODAL_VOP_TEXT') ?></div>
                   </div>
                </form>
                <!--
                <div class="row" id="promo-form-row">
                    <form action="<?php echo $router->link('coupon_payment_info') ?>" method="post" id="promo-code-form">
                        <input type="hidden" name="team_id" value="<?php echo $activeTeam['id'] ?>" />
                        <p><?php echo $translator->translate('Have a gift or promo code?') ?></p>
                        <div class="col-sm-8">
                            <input type="text" name="promo_code" />
                        </div>
                        <div class="col-sm-4">
                            <button type="submit"><?php echo $translator->translate('Apply') ?></button>
                        </div>
                    </form>
                </div>
                -->
              


            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="team-credit-modal-vop" tabindex="-1" role="dialog" aria-labelledby="">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="ico ico-close-black"></i></span></button>
                <h4 class="modal-title " id="myModalLabel"><span><?php echo $translator->translate('TEAM_CREDIT_MODAL_VOP') ?></span></h4>
            </div>

            <div class="modal-body">
                <iframe id="team-credit-modal-vop-frame" src="" style="width:100%;height:400px;border:0; overflow: scroll !important;"></iframe>

            </div>

            <div class="modal-footer">
                
            </div>
        </div>
    </div>
</div>


