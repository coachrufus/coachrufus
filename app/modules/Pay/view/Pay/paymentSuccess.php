<style>
    #invoice-form input[type=text],
    #invoice-form input[type=email]
    {
    }
    .form-group.required label:before
    { 
        content: "*";
        color: red;
    }
</style>
<?php Core\Layout::getInstance()->startSlot('team_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/Team/view/team/_team_menu.php', ['team' => $team]) ?>
<?php Core\Layout::getInstance()->endSlot('team_menu') ?>
<section class="content-header">
    <h1><?php echo $translator->translate('Ďakujeme za nákup') ?></h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-body form-horizontal">
                    <?php echo $translator->translate('Lorem ipsum...') ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $layout->addJavascript('plugins/validator/validator.min.js') ?>
<?php $layout->startSlot('javascript') ?>
<script>
</script>
<?php $layout->endSlot('javascript') ?>