<style>

</style>

<?php Core\Layout::getInstance()->startSlot('team_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/Team/view/team/_team_menu.php', ['team' => $team]) ?>
<?php Core\Layout::getInstance()->endSlot('team_menu') ?>
<section class="content-header">
    <h1><?php echo "{$translator->translate('Package')}: {$package['name']}" ?></h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <form class="box-body form-horizontal" id="select-period-form" action="/pay/package-period-update" method="POST">
                    <div class="form-group required">
                        <label class="col-sm-2 control-label" for="month"><?php echo $translator->translate('Select period') ?></label>
                        <div class="col-sm-10">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="period" value="year">
                                    <?php echo $translator->translate('Year') ?> <strong><?php echo $package['price_year'] ?> &euro;</strong>
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="period" value="month">
                                    <?php echo $translator->translate('Month') ?> <strong><?php echo $package['price_month'] ?> &euro;</strong>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-2 control-label"></div>
                        <div class="col-sm-10">
                            <button
                                id="next"
                                type="submit"
                                class="btn btn-success"
                                disabled="disabled"><?php echo $translator->translate('Next') ?>&nbsp;&raquo;</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<?php $layout->startSlot('javascript') ?>
<script>
$(function()
{
    $('input').iCheck('destroy');
    $('input[name="period"]').click(function()
    {
        $('#next').removeAttr('disabled');
    });
});
</script>
<?php $layout->endSlot('javascript') ?>