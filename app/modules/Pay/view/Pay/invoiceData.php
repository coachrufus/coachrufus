<style>
    #invoice-form input[type=text],
    #invoice-form input[type=email]
    {
    }
    .form-group.required label:before
    { 
        content: "*";
        color: red;
    }
</style>

<?php Core\Layout::getInstance()->startSlot('team_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/Team/view/team/_team_menu.php', ['team' => $team]) ?>
<?php Core\Layout::getInstance()->endSlot('team_menu') ?>
<section class="content-header">
    <h1><?php echo $translator->translate('Invoicing data') ?></h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <form class="box-body form-horizontal" id="invoice-form" method="POST">
                    <div class="form-group required">
                        <label class="col-sm-2 control-label" for="full_name"><?php echo $invoiceFields['full_name'] ?></label>
                        <div class="col-sm-10">
                            <?php echo $form->renderInputTag('full_name',array('minlength' => 4, 'maxlength' => 120, 'class' => 'form-control')) ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="tel"><?php echo $invoiceFields['tel'] ?></label>
                        <div class="col-sm-10">
                             <?php echo $form->renderInputTag('tel',array('minlength' => 4, 'maxlength' => 45, 'class' => 'form-control')) ?>

                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-2 control-label" for="email"><?php echo $invoiceFields['email'] ?></label>
                        <div class="col-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon">@</span>
                                 <?php echo $form->renderEmailTag('email',array('minlength' => 4, 'maxlength' => 120,'pattern' => '\S+@\S+\.\S+', 'class' => 'form-control')) ?>
                                
                               
                            </div>
                        </div>
                    </div>
                    
                      <div class="form-group required">
                        <label class="col-sm-2 control-label" for="street"><?php echo $invoiceFields['country_id'] ?></label>
                        <div class="col-sm-10">
                           <?php echo $form->renderSelectTag('country_id',array('class' => 'form-control', 'required' => true)) ?>
                        </div>
                    </div>
                    
                    <div class="form-group required">
                        <label class="col-sm-2 control-label" for="street"><?php echo $invoiceFields['street'] ?></label>
                        <div class="col-sm-10">
                             <?php echo $form->renderInputTag('street',array('minlength' => 2, 'maxlength' => 45, 'class' => 'form-control')) ?>
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-2 control-label" for="city"><?php echo $invoiceFields['city'] ?></label>
                        <div class="col-sm-10">
                             <?php echo $form->renderInputTag('city',array('minlength' => 2, 'maxlength' => 120, 'class' => 'form-control')) ?>
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-2 control-label" for="zip"><?php echo $invoiceFields['zip'] ?></label>
                        <div class="col-sm-10">
                            <?php echo $form->renderInputTag('zip',array('minlength' => 5, 'maxlength' => 10, 'class' => 'form-control')) ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="company_name"><?php echo $invoiceFields['company_name'] ?></label>
                        <div class="col-sm-10">
                             <?php echo $form->renderInputTag('company_name',array('minlength' => 4, 'maxlength' => 120, 'class' => 'form-control')) ?>

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="ico"><?php echo $invoiceFields['ico'] ?></label>
                        <div class="col-sm-10">
                             <?php echo $form->renderNumberTag('ico',array('minlength' => 4, 'maxlength' => 45, 'class' => 'form-control')) ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="icdph"><?php echo $invoiceFields['icdph'] ?></label>
                        <div class="col-sm-10">
                             <?php echo $form->renderInputTag('icdph',array('minlength' => 4, 'maxlength' => 45, 'class' => 'form-control')) ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="dic"><?php echo $invoiceFields['dic'] ?></label>
                        <div class="col-sm-10">
                             <?php echo $form->renderNumberTag('dic',array('minlength' => 4, 'maxlength' => 45, 'class' => 'form-control')) ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="note"><?php echo $invoiceFields['note'] ?></label>
                        <div class="col-sm-10">
                            <?php echo $form->renderTextareaTag('note',array('maxlength' => 1000, 'class' => 'form-control')) ?>
                           
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-2 control-label"></div>
                        <div class="col-sm-10">
                            <button
                                id="next"
                                type="button"
                                class="btn btn-success"
                                disabled="disabled"><?php echo $translator->translate('Next') ?>&nbsp;&raquo;</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<?php $layout->addJavascript('plugins/validator/validator.min.js') ?>
<?php $layout->startSlot('javascript') ?>
<script>
$(function()
{
    var invoiceForm = $('#invoice-form');
    var requiredInputs = invoiceForm.find('input[required=true], textarea[required=true]');
    var nextButton = $('#next');
    var checkBoxes = function()
    {
        var allFilled = true;
        requiredInputs.each(function()
        {
            if (!$(this).val()) allFilled = false;
        });
        if (allFilled) nextButton.removeAttr('disabled');
        else nextButton.attr('disabled', 'disabled');
    };
    requiredInputs.keyup(checkBoxes);
    requiredInputs.focus(checkBoxes);
    requiredInputs.blur(checkBoxes);
    checkBoxes();
    invoiceForm.validator();
    nextButton.click(function()
    {
        invoiceForm.attr("action", "/pay/invoice-data-update")
        invoiceForm.submit();
    });
});
</script>
<?php $layout->endSlot('javascript') ?>