<div class="modal fade subscription-modal" id="subscription-disabled-modal" tabindex="-1" role="dialog" aria-labelledby="">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="ico ico-close-black"></i></span></button>
                 <img src="/theme/img/icon/alert-mark.svg" alt="" title="" />
                <h4 class="modal-title  modal-title-danger" id="myModalLabel"><span><?php echo $translator->translate('MODAL_DISABLED_SUBSCRIPTION_TITLE') ?></span></h4>
            </div>

            <div class="modal-body">
                <?php echo $translator->translate('MODAL_DISABLED_SUBSCRIPTION_TEXT') ?>

            </div>

            <div class="modal-footer">
                
            </div>
        </div>
    </div>
</div>




