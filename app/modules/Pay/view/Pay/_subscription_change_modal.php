
<div class="modal fade subscription-modal" id="subscription-change-modal" tabindex="-1" role="dialog" aria-labelledby="">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="ico ico-close-black"></i></span></button>
                <h4 class="modal-title" id="myModalLabel"><span><?php echo $translator->translate('MODAL_CHANGE_SUBSCRIPTION_TITLE') ?></span></h4>
            </div>

            <div class="modal-body">
                <form action="<?php echo $router->link('gopay_change_subscription') ?>" id="subscription-change-modal-form">
                    <input type="hidden" name="rurl" id="subscription-change-rurl" value="<?php echo $router->link('gopay_change_subscription_status') ?>" />
                    <input type="hidden" name="tid" id="subscription-change-tid" value="" />
                    <input type="hidden" name="oid" id="subscription-change-oid" value="" />
                    <input type="hidden" id="subscription-change-modal-variant" name="variant" value="">



                    <div class="row text-left">
                        <div class="col-xs-1"></div>
                    <div class="col-xs-5 subscription-change-modal-desc">

                    </div>

                    <div class="col-xs-6 subscription-change-row">
                        <div class="subscription-change-btn-row">
                            <button data-variant="12" class="subscription-change-trigger active" type="button"></button>
                            <?php echo $prices['year'] ?> &euro; <?php echo $translator->translate('SUBSCRIPTION_YEARLY') ?>/<?php echo $translator->translate('team') ?>
                            <span class="subscription-save"><?php echo $translator->translate('Save 40%') ?></span>
                        </div>  
                        <div class="subscription-change-btn-row">
                            <button data-variant="1"  class="subscription-change-trigger" type="button"></button>
                            <?php echo $prices['month'] ?> &euro; <?php echo $translator->translate('SUBSCRIPTION_MONTHLY') ?>/<?php echo $translator->translate('team') ?>

                        </div>  
                    </div>
                    </div>

                   
                </form>

            </div>

            <div class="modal-footer">
                 <div class="row">
                        <div class="col-sm-12 text-center subscription-text">
                           
                        </div>
                    </div>


                    <div class="row action-row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-5 text-right">
                            <button class="subscription-change-submit btn btn-primary" type="submit"><?php echo $translator->translate('SUBSCRIPTION_CHANGE_BTN') ?></button>
                        </div>
                        <div class="col-xs-4 text-left">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $translator->translate('Cancel') ?></button>
                        </div>
                        <div class="col-xs-1"></div>
                    </div>
            </div>
        </div>
    </div>
</div>




