<style>
.price-list th,
.price-list td
{
    text-align: center;
}
.price-list .glyphicon-ok,
.price-list .glyphicon-ok-circle
{
    color: #00a300;
}
.price-list .glyphicon-remove
{
    color: #777;
}
.price-list th.caption,
.price-list td.caption
{
    text-align: left;
    color: #333;
    font-weight: normal;
}
.price-list td.invoice-data-field
{
    text-align: left;
    font-weight: bold;
}
.price-list thead tr th,
.price-list tfoot tr th
{
    color: #00b3be;
}
.price-list tbody strong
{
    color: #00759a;
}
</style>

<?php function renderPackageNames($package, $translator) { ob_start(); ?>
<tr>
    <th class="caption"><?php echo $translator->translate('Package') ?></th>
    <th><?php echo $package['name'] ?></th>
</tr>
<?php return ob_get_clean(); } ?>

<?php function renderInvoicingData($translator) { ob_start(); ?>
<tr>
    <th colspan="3"><?php echo $translator->translate('Invoicing data') ?></th>
</tr>
<?php return ob_get_clean(); } ?>



<?php Core\Layout::getInstance()->startSlot('team_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/Team/view/team/_team_menu.php', ['team' => $team]) ?>
<?php Core\Layout::getInstance()->endSlot('team_menu') ?>
<section class="content-header">
    <h1><?php echo $translator->translate('Summary') ?></h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <table class="table table-bordered price-list" id="table">
                    <thead>
                        <?php echo renderPackageNames($package, $translator) ?>
                    </thead>
                    <tbody>
                        <?php if ($order['package']['period'] === "month"): ?>
                        <tr>
                            <td class="caption"><?php echo $translator->translate('Price') ?></td>
                            <td><?php echo "<strong>{$package['price_month']} &euro;</strong> per {$translator->translate('month')}" ?></td>
                        </tr>
                        <?php else: ?>
                        <tr>
                            <td class="caption"><?php echo $translator->translate('Price') ?></td>
                            <td><?php echo "<strong>{$package['price_year']} &euro;</strong> per {$translator->translate('year')}" ?></td>
                        </tr>
                        <?php endif; ?>
                        <tr>
                            <td class="caption"><?php echo $translator->translate('Player limit') ?></td>
                            <td><?php echo is_null($package['player_limit']) ? "{$translator->translate('unlimited')}<br>{$translator->translate('players')}" : "&lt; {$package['player_limit']}<br>{$translator->translate('players')}" ?></td>
                        </tr>
                        <?php foreach ($products as $product): ?>
                        <tr>
                            <td class="caption product-name"><?php echo $product['name'] ?></td>
                            <td><i class="glyphicon <?php echo $package['products'][$product['id']]['is_enabled'] ? 'glyphicon-ok' : 'glyphicon-remove' ?>"></i></td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
        
        <div class="col-md-6">
            <div class="box">
                <table class="table table-bordered price-list" id="table">
                    <thead>
                        <?php echo renderInvoicingData($translator) ?>
                    </thead>
                    <tbody>
                        <?php foreach ($invoiceFields as $k => $label): ?>
                        <tr>
                            <td class="caption product-name"><?php echo $label ?></td>
                            <td class="invoice-data-field"><?php echo $order['package']['invoice'][$k] ?></td>
                            <td><i class="glyphicon <?php echo empty($order['package']['invoice'][$k]) ? '' : 'glyphicon-ok-circle' ?>"></i></td>
                        </tr>
                        <?php endforeach; ?>
                       
                        
                        
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="2">
                        <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" target="_top">
                        <input type="hidden" name="cmd" value="_xclick">
                        <input type="hidden" name="business" value="marek.hubacek-facilitator@gmail.com">

                        <input type="hidden" name="amount" value="<?php echo $order['price'] ?>">
                        <input type="hidden" name="item_name" value="Package name">
                        <input type="hidden" name="item_number" value="<?php echo $order['id'] ?>">
                        
                        


                        <input type="image" src="https://www.sandbox.paypal.com/en_US/i/btn/btn_buynow_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                        <img alt="" border="0" src="https://www.sandbox.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1">

                        </form>
                            </td>
                </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        
    </div>
</section>
