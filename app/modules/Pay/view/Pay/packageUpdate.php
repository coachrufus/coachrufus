<style>
    #availability
    {
        padding-top: 7px
    }
    .form-group.required label.control-label:before
    { 
        content: "*";
        color: red;
    }
</style>

<?php Core\Layout::getInstance()->startSlot('team_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/Team/view/team/_team_menu.php', ['team' => $team]) ?>
<?php Core\Layout::getInstance()->endSlot('team_menu') ?>
<section class="content-header">
    <h1><?php echo "{$translator->translate('Upgrade to Package')}: {$package['name']}" ?></h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <form class="box-body form-horizontal" id="select-period-form" action="/pay/package-period-update" method="POST">
                    
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="month"><?php echo $translator->translate('Availability') ?></label>
                        <div class="col-sm-10">
                            <div id="availability"><span id="availability-from"><?php echo $startDate ?></span> - <span id="availability-to"><?php echo $endDate ?></span></div>
                        </div>
                    </div>
                    
                    <input type="hidden" name="period" value="custom">

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="month"><?php echo $translator->translate('Coupon code') ?></label>
                        <div class="col-sm-5">
                            <input type="text" name="coupon" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-2 control-label"></div>
                        <div class="col-sm-10">
                            <button
                                id="next"
                                type="submit"
                                class="btn btn-success"><?php echo $translator->translate('Next') ?>&nbsp;&raquo;</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<?php $layout->startSlot('javascript') ?>
<script>
$(function()
{

});
</script>
<?php $layout->endSlot('javascript') ?>