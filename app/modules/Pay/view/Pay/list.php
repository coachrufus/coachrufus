<style>
.price-list th,
.price-list td
{
    text-align: center;
}
.price-list .glyphicon-ok
{
    color: #00a300;
}
.price-list .glyphicon-remove
{
    color: #777;
}
.price-list td.product-name
{
    text-align: left;
}
.price-list thead tr th,
.price-list tfoot tr th
{
    color: #00b3be;
}
.price-list tbody strong
{
    color: #00759a;
}
</style>

<?php function renderPackageNames($packageList, $translator) { ob_start(); ?>
<tr>
    <?php foreach ($packageList as $package): ?>
    <th><?php echo $package['name'] ?></th>
    <?php endforeach; ?>
    <th></th>
</tr>
<?php return ob_get_clean(); } ?>

<?php function renderOrderButtons($packageList, $packageOrderStates, $translator) { ob_start(); ?>
<tr>
    <?php foreach ($packageList as $package): ?>
    <td>
        <button
            type="button"
            class="order-button btn btn-success"
            data-package-id="<?php echo $package['id'] ?>"
            <?php echo $packageOrderStates[$package['id']]['isOrderable'] ? '' : 'disabled="disabled"' ?>>
            <?php echo $translator->translate('Order') ?>
        </button>
    </td>
    <?php endforeach; ?>
    <td></td>
</tr>
<?php return ob_get_clean(); } ?>

<?php Core\Layout::getInstance()->startSlot('team_menu') ?>  
<?php $layout->includePart(MODUL_DIR . '/Team/view/team/_team_menu.php', ['team' => $team]) ?>
<?php Core\Layout::getInstance()->endSlot('team_menu') ?>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
            <table class="table table-bordered price-list" id="table">
                <thead>
                    <?php echo renderPackageNames($packageList, $translator) ?>
                </thead>
                <tbody>
                    <?php echo renderOrderButtons($packageList, $packageOrderStates, $translator) ?>
                    <tr>
                        <?php foreach ($packageList as $package): ?>
                        <td><?php echo "<strong>{$package['price_month']} &euro;</strong> per {$translator->translate('month')}" ?></td>
                        <?php endforeach; ?>
                        <td></td>
                    </tr>
                    <tr>
                        <?php foreach ($packageList as $package): ?>
                        <td><?php echo "<strong>{$package['price_year']} &euro;</strong> per {$translator->translate('year')}" ?></td>
                        <?php endforeach; ?>
                        <td></td>
                    </tr>
                    <tr>
                        <?php foreach ($packageList as $package): ?>
                        <td><?php echo is_null($package['player_limit']) ? "{$translator->translate('unlimited')}<br>{$translator->translate('players')}" : "&lt; {$package['player_limit']}<br>{$translator->translate('players')}" ?></td>
                        <?php endforeach; ?>
                        <td></td>
                    </tr>
                    <?php foreach ($products as $product): ?>
                    <tr>
                        <?php foreach ($packageList as $package): ?>
                        <td><i class="glyphicon <?php echo $package['products'][$product['id']]['is_enabled'] ? 'glyphicon-ok' : 'glyphicon-remove' ?>"></i></td>
                        <?php endforeach; ?>
                        <td class="product-name"><?php echo $product['name'] ?></td>
                    </tr>
                    <?php endforeach; ?>
                    <?php echo renderOrderButtons($packageList, $packageOrderStates, $translator) ?>
                </tbody>
                <tfoot>
                    <?php echo renderPackageNames($packageList, $translator) ?>
                </tfoot>
            </table>
            </div>
        </div>
    </div>
</section>
<?php $layout->startSlot('javascript') ?>
<script>
    $(function()
    {
        $("#table button.order-button").click(function()
        {
            var button = $(this);
            var packageId = parseInt(button.data("package-id"));
            window.location = "/pay/order-package/" + packageId;
        });
    });
</script>
<?php $layout->endSlot('javascript') ?>