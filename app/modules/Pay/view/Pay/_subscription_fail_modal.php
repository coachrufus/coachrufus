<div class="modal fade subscription-modal" id="subscription-fail-modal" tabindex="-1" role="dialog" aria-labelledby="">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="ico ico-close-black"></i></span></button>
                 <img src="/theme/img/icon/alert-mark.svg" alt="" title="" />
                <h4 class="modal-title modal-title-danger" id="myModalLabel"><span><?php echo $translator->translate('MODAL_FAIL_SUBSCRIPTION_TITLE') ?></span></h4>
                
                <div><br /><?php echo $translator->translate('MODAL_FAIL_SUBSCRIPTION_TITLE_TEXT') ?></div>
            </div>

            <div class="modal-body">
                <form action="<?php echo $router->link('gopay_payment') ?>" id="subscription-fail-modal-form">
                    <input type="hidden" name="rurl" id="subscription-fail-modal-rurl" value="<?php echo $router->link('gopay_payment_status') ?>" />
                    

                    <input type="hidden" class="subscription-variant" name="variant" value="<?php echo $failPaymentData['variant'] ?>">
                    <input type="hidden" class="subscription-team-id" name="team_id" value="<?php echo $failPaymentData['teamId'] ?>">


                    <div class="row text-left">
                        <div class="col-xs-1"></div>
                    <div class="col-xs-5 subscription-fail-modal-desc">
                        <?php echo $failPaymentData['desc'] ?>
                    </div>

                    <div class="col-xs-6 subscription-change-row">
                        <div class="subscription-change-btn-row">
                            <button data-variant="12" class="<?php echo $failPaymentData['variant'] == 12 ? 'active' : '' ?> subscription-price-trigger" type="button"></button>
                            <?php echo $failPaymentData['prices']['year'] ?> &euro; <?php echo $translator->translate('SUBSCRIPTION_YEARLY') ?>/<?php echo $translator->translate('team') ?>
                            <span class="subscription-save"><?php echo $translator->translate('Save 40%') ?></span>
                        </div>  
                        <div class="subscription-change-btn-row">
                            <button data-variant="1"  class="<?php echo $failPaymentData['variant'] == 1 ? 'active' : '' ?> subscription-price-trigger" type="button"></button>
                            <?php echo $failPaymentData['prices']['month'] ?> &euro; <?php echo $translator->translate('SUBSCRIPTION_MONTHLY') ?>/<?php echo $translator->translate('team') ?>

                        </div>  
                    </div>
                    </div>

                   
                </form>

            </div>

            <div class="modal-footer">
              


                    <div class="row action-row">
                        <div class="col-xs-6 text-right">
                            <button class="subscription-fail-payment-submit btn btn-primary" type="submit"><?php echo $translator->translate('Pay') ?></button>
                        </div>
                        <div class="col-xs-6 text-left">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $translator->translate('Cancel') ?></button>
                        </div>
                      
                    </div>
            </div>
        </div>
    </div>
</div>




