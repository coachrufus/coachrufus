<div class="modal fade subscription-modal" id="subscription-cancel-modal" tabindex="-1" role="dialog" aria-labelledby="">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="ico ico-close-black"></i></span></button>
                
                
                <img src="/theme/img/icon/alert-mark.svg" alt="" title="" />
                
                <h4 class="modal-title modal-title-danger" id="myModalLabel"><span><?php echo $translator->translate('MODAL_CANCEL_SUBSCRIPTION_TITLE') ?></span></h4>
            </div>

            <div class="modal-body">
                <div id="subscription-modal-body-text"></div>
            </div>

            <div class="modal-footer text-center">
                <p><?php echo $translator->translate('MODAL_CANCEL_SUBSCRIPTION_TEXT') ?></p>
                   <div class="row">
                    <div class="col-sm-6">
                         <a href="" class=" btn btn-primary" id="subscription-cancel-link"  class="btn btn-primary"><?php echo $translator->translate('MODAL_CANCEL_SUBSCRIPTION_YES') ?></a>
                    </div>
                    <div class="col-sm-6">
                         <a href="/profil#subscriptions" data-dismiss="modal" class=" btn btn-danger" class="btn btn-primary"><?php echo $translator->translate('MODAL_CANCEL_SUBSCRIPTION_NO') ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




