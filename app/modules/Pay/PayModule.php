<?php namespace CR\Pay;

use Core\Module as Module;

class PayModule extends Module
{
    public function __construct()
    {
        $this->setBaseDir(MODUL_DIR . '/Pay');
        $this->setName('Pay');
    }
}

