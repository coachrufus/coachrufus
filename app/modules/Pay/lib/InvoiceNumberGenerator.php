<?php namespace CR\Pay\Invoices;

interface IInvoiceNumberStore
{
    function getLastInvoiceNumberFor(\DateTime $date, $prefix);
}

class InvoiceNumberGenerator
{
    private $invoiceNumberStore;
    
    function __construct(IInvoiceNumberStore $invoiceNumberStore)
    {
        $this->invoiceNumberStore = $invoiceNumberStore;
    }
    
    private function toSeqNo($invoiceNo)
    {
        return (int)subStr($invoiceNo, -7);
    }
    
    private function getLastSeqNo($date, $prefix)
    {
        $no = $this->invoiceNumberStore->getLastInvoiceNumberFor($date, $prefix);
        return $no === false ? 0 : $this->toSeqNo($no);
    }
    
    function getNewInvoiceNumber(\DateTime $date, $prefix = 1)
    {
        return sprintf("%s%02d%07d", $prefix, $date->format("y"),
            $this->getLastSeqNo($date, $prefix) + 1);
    }
}