<?php namespace CR\Pay\Invoices;

use Core\ServiceLayer;
use Core\DateNames;

interface IInvoiceRenderer 
{
    function render($templatePath, $marks);
}

class LatteInvoiceRenderer implements IInvoiceRenderer
{
    private $latte;
    private $defaults;
    
    function __construct()
    {
        $this->latte = new \Latte\Engine;
        $this->latte->setTempDirectory(LATTE_TEMP_DIR);
        $translator = ServiceLayer::getService('translator');
        $this->latte->addFilter('translate', function ($value) use($translator)
        {
            return $translator->translate($value);
        });
        $this->latte->addFilter('toMoney', function ($value)
        {
            return number_format(round($value, 2), 2, '.', ' ');
        });
    }
    
    function render($templatePath, $marks)
    {
        return $this->latte->renderToString($templatePath, $marks);
    }
}

class InvoiceTemplate
{
    private $path;
    private $marks;
    private $renderer;
    
    private function getFilePath($name)
    {
        $fileNames = [
            TEMPLATE_DIR . "/{$name}.latte",
            "{$name}.latte",
            TEMPLATE_DIR . "/{$name}",
            $name
        ];
        foreach ($fileNames as $fileName)
        {
            if (is_file($fileName)) return $fileName;
        }
        throw new \Exception("Template \"{$name}\" not exists.");
    }
    
    function __construct($name, $marks = [], IInvoiceRenderer $renderer = null)
    {
        $this->path = $this->getFilePath($name);
        $this->marks = $marks;
        $this->renderer = is_null($renderer) ? new LatteInvoiceRenderer() : $renderer;
    }
    function assignMarks($marks) { foreach ($marks as $k => $v) $this->marks[$k] = $v; }
    function assignObject($object) { $this->assignMarks((array)$object); }
    function __get($name) { return $this->marks[$name]; }
    function __isset($name) { return isset($this->marks[$name]); }
    function __unset($name) { unset($this->marks[$name]); }
    function __set($name, $value) { $this->marks[$name] = $value; }
    function render() { return $this->renderer->render($this->path, $this->marks); }
    function __toString() { return $this->render(); }
}