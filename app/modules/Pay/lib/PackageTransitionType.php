<?php namespace CR\Pay\Lib;

use Core\Types\Enum;

class PackageTransitionType extends Enum
{
    const FirstPackage = 0;
    const SamePackage = 1;
    const UpperPackage = 2;
    const LowerPackage = 3;
}