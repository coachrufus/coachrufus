<?php

namespace CR\Pay\Form;

use Core\Form as Form;

/**
 * @author Marek Hubacek
 * @version 1.0
 * @created 21-11-2015 21:22:50
 */
class InvoiceForm extends Form {

    protected $fields = array(
        'full_name' =>
        array(
            'type' => 'string',
            'max_length' => '255',
            'required' => true,
        ),
        
        'tel' =>
        array(
            'type' => 'string',
            'max_length' => '255',
            'required' => false,
        ),
        
        'email' =>
        array(
            'type' => 'string',
            'max_length' => '255',
            'required' => true,
        ),
        'street' =>
        array(
            'type' => 'string',
            'max_length' => '255',
            'required' => true,
        ),
        
        'city' =>
        array(
            'type' => 'string',
            'max_length' => '255',
            'required' => true,
        ),

        'zip' =>
        array(
            'type' => 'string',
            'max_length' => '255',
            'required' => true,
        ),

        'company_name' =>
        array(
            'type' => 'string',
            'max_length' => '255',
            'required' => false,
        ),
        'ico' =>
        array(
            'type' => 'string',
            'max_length' => '255',
            'required' => false,
        ),
        
        'icdph' =>
        array(
            'type' => 'string',
            'max_length' => '255',
            'required' => false,
        ),
        'dic' =>
        array(
            'type' => 'string',
            'max_length' => '255',
            'required' => false,
        ),
        'note' =>
        array(
            'type' => 'string',
            'max_length' => '1000',
            'required' => false,
        ),
        
        'country_id' =>
        array(
            'type' => 'choice',
            'max_length' => '11',
            'required' => false,
        ),
    );

   

   

}

?>