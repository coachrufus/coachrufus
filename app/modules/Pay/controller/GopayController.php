<?php

namespace CR\Pay\Controller;

use Core\Controller;
use Core\ControllerResponse;
use Core\GUID;
use Core\ServiceLayer;
use Core\Types\DateTimeEx;

class GopayController extends Controller {

    public function preparePayment()
    {
        $request = $this->getRequest();

        $orderManager = ServiceLayer::getService('OrderManager');
        $paymentManager = ServiceLayer::getService('PaymentManager');
        $couponManager = ServiceLayer::getService('CouponManager');
        $teamId = $this->getRequest()->get('team_id');
        $security = \Core\ServiceLayer::getService('security');
        $userIdentity = $security->getIdentity();
        $user = $userIdentity->getUser();


        $variant = $request->get('variant');
        $renewParams = $paymentManager->getRenewParamsByVariant($variant);
        $data['renew_cycle'] = $renewParams['renew_cycle'];
        $data['renew_period'] = $renewParams['renew_period'];


        $prices = array(1 => $paymentManager->getProMonthPrice() * 100, 12 => $paymentManager->getProYearPrice() * 100);
        $data['price'] = $prices[$variant];
        $data['discount'] = 0;
        $data['renew_payment'] = 1;
        $data['on_demand_renew_payment'] = 0;

        $promoCode = $this->getRequest()->get('promo_code');
        if (null != $this->getRequest()->get('promo_code'))
        {
            $promoCode = $couponManager->findByCode($this->getRequest()->get('promo_code'));


            if ($promoCode->getCharges() > 0)
            {
                if ($promoCode->getDiscountType() == 'percent')
                {
                    $basePrice = $data['price'] * $promoCode->getMonthPeriod();
                    $discountSum = $basePrice * ($promoCode->getValuePercent() / 100);
                    $data['price'] = $basePrice - $discountSum;
                    $data['discount'] = $discountSum;
                    //first payment have to by not reccurent
                    $data['renew_payment'] = 0;
                    $data['on_demand_renew_payment'] = 1;

                    $data['additional_params'][] = array('name' => 'discount', 'value' => $discountSum);
                    $data['additional_params'][] = array('name' => 'on_demand_renew', 'value' => 'yes');
                    $data['additional_params'][] = array('name' => 'renew_cycle', 'value' => $data['renew_cycle']);
                    $data['additional_params'][] = array('name' => 'renew_period', 'value' => $data['renew_period']);
                }
            }
        }

        //$data['renew_payment'] = (null == $this->getRequest()->get('autopayment')) ? 0 : 1 ;
        $order = new \CR\Pay\Model\Order();
        $order->setTeamId($teamId);
        $order->setUserId($user->getId());
        $order->setRenewPayment($data['renew_payment']);
        $order->setCreatedAt(new \DateTime());
        $order->setTotalPrice($data['price'] / 100);
        $order->setDiscount($data['discount']);
        $order->setPaid(0);
        $order->setCurrency('EUR');
        $order->setStatus('waiting');
        $order->setVariant($variant);


        $orderId = $orderManager->saveOrder($order);
        /*
          $order = array();
          $order['date'] = (string)DateTimeEx::now();
          $order['user_id'] = $user->getId();
          $order['user_package_id'] = 0;
          $order['ip'] =  $_SERVER['REMOTE_ADDR'];
          $order['payment_id'] =  '0' ;
          $order['team_id'] =  $teamId ;
          $order['renew_payment'] =  $data['renew_payment'];
          $order['created_at'] =  date('Y-m-d H:i:s') ;
          $order['variant'] = $variant;
          $order['total_price'] = $data['price']/100;
          $order['currency'] = 'EUR';
          $order['paid'] = 0;
          $order['status'] = 'waiting';
          $order['discount'] =  $data['discount'];
          $orderNo = $orderDataManager->save($order);
          $data['order_number'] = $orderNo;
         */
        $data['order_number'] = $orderId;
        $data['lang'] = LANG;
        $data['contact'] = array(
                    'first_name' => $user->getName(),
                    'last_name' => $user->getSurname(),
                    'email' => $user->getEmail(),
                    'phone_number' => $user->getPhone(),
                    'city' => '',
                    'street' => '',
                    'postal_code' => '',
                    'country_code' => '',);

        return $data;
    }

    /**
     * First step of payment, create gopay payment, implemented https://doc.gopay.com/cs/#opakovaná-platba
     * @return ControllerResponse
     */
    public function paymentAction()
    {
        //4188030000000003
        $data = $this->preparePayment();
        $gopay = new \GoPay\Api();
        $redirectUrl = $gopay->createPayment($data);
        $data = array('redirectUrl' => $redirectUrl);
        header("Content-Type: application/json; charset=utf-8");

        ServiceLayer::getService('layout')->setTemplate(null);
        $response = new ControllerResponse();
        $response->setStatus('success');
        $response->setContent(json_encode($data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
        $response->setType('application/json');
        return $response;
    }

    /**
     * Second step, check payment status
     */
    public function paymentStatusAction()
    {
        $gopay = new \GoPay\Api();
        $orderStatus = $gopay->getPaymentInfo($this->getRequest()->get('id'));
        $result = array('status' => $orderStatus->state, 'redirectUrl' => $this->getRouter()->link('gopay_success_payment', array('id' => $this->getRequest()->get('id'))));
        ServiceLayer::getService('layout')->setTemplate(null);
        $response = new ControllerResponse();
        $response->setStatus('success');
        $response->setContent(json_encode($result, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
        $response->setType('application/json');
        return $response;
    }

    public function finishOrderSuccess($order, $orderStatus)
    {
        $paymentManager = ServiceLayer::getService('PaymentManager');
        $ordeManager = ServiceLayer::getService('OrderManager');
        $end = new \DateTime();
        //on demand recurrent payment
        if ($orderStatus->recurrence->recurrence_cycle == 'ON_DEMAND')
        {
            foreach ($orderStatus->additional_params as $additionalParam)
            {
                $renew_cycle = '';
                $renew_period = '';
                if ($additionalParam->name == 'renew_cycle')
                {
                    $renew_cycle = $additionalParam->value;
                }
                if ($additionalParam->name == 'renew_period')
                {
                    $renew_period = $additionalParam->value;
                }
            }

            if ($renew_cycle == 'MONTH')
            {
                $end->modify('+' . $renew_period . ' months');
            }
            if ($renew_cycle == 'WEEK')
            {
                $end->modify('+' . $renew_period . ' weeks');
            }
            if ($renew_cycle == 'DAY')
            {
                $end->modify('+' . $renew_period . ' days');
            }

            //create on demand subsrciption
            $prices = $paymentManager->getVariantProPrices();

            $onDemandData['team_id'] = $order->getTeamId();
            $onDemandData['user_id'] = $order->getUserId();
            $onDemandData['variant'] = $order->getVariant();
            $onDemandData['price'] = $prices[$order->getVariant()];
            $onDemandData['renew_on'] = $end->format('Y-m-d H:i:s');
            $onDemandData['renew_type'] = 'on_demand';
            $onDemandData['status'] = 'waiting';

            $paymentManager->createOnDemandSubscription($onDemandData);

            //$order->setRenewType('manual');
        }
        else
        {
            $paymentManager->enlargeEndDate($end, $orderStatus->recurrence->recurrence_cycle, $orderStatus->recurrence->recurrence_period);
        }



        //$end->setTime(0, 0, 0);
        $start = new \DateTime();
        //$start->setTime(0, 0, 0);
        $pm = ServiceLayer::getService('PackageManager');
        $proPackage = $pm->getPackageWithProducts(5);
        $userPackage = ServiceLayer::getService('UserPackageManager')->addUserPackage([
            'user_id' => $order->getUserId(),
            'package_id' => 5,
            'team_id' => $order->getTeamId(),
            'team_name' => $team->getName(),
            'date' => new \DateTime(),
            'start_date' => $start,
            'end_date' => $end,
            'name' => 'PRO',
            'note' => 'PAY SUCCESS',
            'level' => 5,
            'products' => json_encode($proPackage['products']),
            'player_limit' => 100,
            'guid' => GUID::create(),
            'variant' => $order->getVariant(),
            'renew_payment' => 0
        ]);


        //create subscriptioén
        if (null != $orderStatus->recurrence)
        {
            $order->setRenewDate($end);
            $order->setValidTo($end);
        }


        //$order->setPackage($proPackage);
        $order->setUserPackageId($userPackage['id']);
        $order->setPaid($amount / 100);
        $order->setPaymentId($orderStatus->id);
        $order->setValidTo($end);
        $order->setStatus('paid');
        $ordeManager->saveOrder($order);

        return $order;
    }

    public function paymentSuccessAction()
    {
        $gopay = new \GoPay\Api();
        $orderStatus = $gopay->getPaymentInfo($this->getRequest()->get('id'));
        $orderNumber = $orderStatus->order_number;
        $amount = $orderStatus->amount;
        $status = $orderStatus->state;
        $paymentManager = ServiceLayer::getService('PaymentManager');
        $ordeManager = ServiceLayer::getService('OrderManager');
        $order = $ordeManager->getOrderByNumber($orderNumber);

        $teamManager = ServiceLayer::getService('TeamManager');
        $team = $teamManager->findTeamById($order->getTeamId());


        if ($status == 'PAID' && ($orderStatus->amount / 100) == $order->getTotalPrice())
        {
            $end = new \DateTime();
            //on demand recurrent payment
            if ($orderStatus->recurrence->recurrence_cycle == 'ON_DEMAND')
            {
                foreach ($orderStatus->additional_params as $additionalParam)
                {
                    $renew_cycle = '';
                    $renew_period = '';
                    if ($additionalParam->name == 'renew_cycle')
                    {
                        $renew_cycle = $additionalParam->value;
                    }
                    if ($additionalParam->name == 'renew_period')
                    {
                        $renew_period = $additionalParam->value;
                    }
                }

                if ($renew_cycle == 'MONTH')
                {
                    $end->modify('+' . $renew_period . ' months');
                }
                if ($renew_cycle == 'WEEK')
                {
                    $end->modify('+' . $renew_period . ' weeks');
                }
                if ($renew_cycle == 'DAY')
                {
                    $end->modify('+' . $renew_period . ' days');
                }

                //create on demand subsrciption
                $prices = $paymentManager->getVariantProPrices();

                $onDemandData['team_id'] = $order->getTeamId();
                $onDemandData['user_id'] = $order->getUserId();
                $onDemandData['variant'] = $order->getVariant();
                $onDemandData['price'] = $prices[$order->getVariant()];
                $onDemandData['renew_on'] = $end->format('Y-m-d H:i:s');
                $onDemandData['renew_type'] = 'on_demand';
                $onDemandData['status'] = 'waiting';

                $paymentManager->createOnDemandSubscription($onDemandData);

                //$order->setRenewType('manual');
            }
            else
            {
                $paymentManager->enlargeEndDate($end, $orderStatus->recurrence->recurrence_cycle, $orderStatus->recurrence->recurrence_period);
            }



            //$end->setTime(0, 0, 0);
            $start = new \DateTime();
            //$start->setTime(0, 0, 0);
            $pm = ServiceLayer::getService('PackageManager');
            $proPackage = $pm->getPackageWithProducts(5);
            $userPackage = ServiceLayer::getService('UserPackageManager')->addUserPackage([
                'user_id' => $order->getUserId(),
                'package_id' => 5,
                'team_id' => $order->getTeamId(),
                'team_name' => $team->getName(),
                'date' => new \DateTime(),
                'start_date' => $start,
                'end_date' => $end,
                'name' => 'PRO',
                'note' => 'PAY SUCCESS',
                'level' => 5,
                'products' => json_encode($proPackage['products']),
                'player_limit' => 100,
                'guid' => GUID::create(),
                'variant' => $order->getVariant(),
                'renew_payment' => 0
            ]);


            //create subscriptioén
            if (null != $orderStatus->recurrence)
            {
                $order->setRenewDate($end);
                $order->setValidTo($end);
            }


            //$order->setPackage($proPackage);
            $order->setUserPackageId($userPackage['id']);
            $order->setPaid($amount / 100);
            $order->setPaymentId($orderStatus->id);
            $order->setValidTo($end);
            $order->setStatus('paid');
            $ordeManager->saveOrder($order);

            //create invoice
            $invoiceManager = ServiceLayer::getService('InvoiceManager');
            $invoiceData['orderStatus'] = $orderStatus;
            $invoiceData['order'] = $order;
            $invoiceData['team'] = $team;
            $invoiceData['start'] = $start;
            $invoiceData['end'] = $end;
            $invoiceData['proPackage'] = $proPackage;

            $invoiceId = $this->createInvoice($invoiceData);
            $invoiceManager->createInvoicePackage(array('id' => $invoiceId), array('id' => $userPackage['id']));

            \Core\ServiceLayer::getService('StepsManager')->finishStepByStep($order->getUserId(), $team->getId());
            \Core\ServiceLayer::getService('TeamCreditManager')->refreshTeamPackages();

            //send notification
            $invoice = $invoiceManager->getInvoiceById($invoiceId);
            $invoiceManager->sendInvoiceAdminNotification($invoice);
            
            $user = ServiceLayer::getService('PlayerManager')->findPlayerById($order->getUserId());
            $notificationManager = ServiceLayer::getService('notification_manager');
            $notificationManager->sendPaymentSuccessNotification(array('email' =>$user->getEmail(), 'lang' => LANG));


            $this->getRequest()->addFlashMessage('payment_success', $this->getTranslator()->translate('PAYMENT_SUCCESS'));
            $this->getRequest()->redirect($this->getRouter()->link('profil') . '#subscriptions');
        }
        else
        {

            
            $this->getRequest()->addFlashMessage('payment_fail', '');
            $this->getRequest()->redirect($this->getRouter()->link('profil',array('subscription-fail' => $orderStatus->id)) . '#subscriptions');
        }
    }

    public function cancelSubscriptionAction()
    {
        $gopay = new \GoPay\Api();
        $transactionId = $this->getRequest()->get('tid');

        //check if order owner
        $security = \Core\ServiceLayer::getService('security');
        $userIdentity = $security->getIdentity();
        $user = $userIdentity->getUser();

        $ordeManager = ServiceLayer::getService('OrderManager');
        $order = $ordeManager->getOrderByPaymentId($transactionId);

        if ($order->getUserId() != $user->getId())
        {
            throw new \AclException();
        }

        $result = $gopay->cancelRecurrence($transactionId);

        if ($result->result == 'FINISHED')
        {
            $order->setRenewPayment(3);
            $order->setStatus('canceled');
            $ordeManager->saveOrder($order);

            $this->getRequest()->addFlashMessage('subscription_cancel_success', $this->getTranslator()->translate('CANCEL_SUBSCRIPTION_SUCCESS'));
            $this->getRequest()->redirect($this->getRouter()->link('profil') . '#subscriptions');
        }
        else
        {
            var_dump($result);
            die('Chyba');
        }
    }

    /**
     * Change exist active subscription
     */
    public function changeSubscriptionAction()
    {
        //4188030000000003
        $request = $this->getRequest();
        $orderId = $request->get('oid');
        $tId = $request->get('tid');
        $data = $this->preparePayment();
        $data['additional_params'][] = array('name' => 'change_subscription', 'value' => $orderId);
        $data['return_url'] = $this->getRouter()->link('gopay_change_subscription_status', array('oid' => $orderId, 'tid' => $tId));
        $gopay = new \GoPay\Api();
        $redirectUrl = $gopay->createPayment($data);
        $data = array('redirectUrl' => $redirectUrl);
        header("Content-Type: application/json; charset=utf-8");

        ServiceLayer::getService('layout')->setTemplate(null);
        $response = new ControllerResponse();
        $response->setStatus('success');
        $response->setContent(json_encode($data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
        $response->setType('application/json');
        return $response;
    }

    public function changeSubscriptionStatusAction()
    {
        $invoiceManager = ServiceLayer::getService('InvoiceManager');
        $ordeManager = ServiceLayer::getService('OrderManager');
        $teamManager = ServiceLayer::getService('TeamManager');
        $paymentManager = ServiceLayer::getService('PaymentManager');
        $packageManager = ServiceLayer::getService('UserPackageManager');
         
        $gopay = new \GoPay\Api();
        $orderStatus = $gopay->getPaymentInfo($this->getRequest()->get('id'));

        $orderNumber = $orderStatus->order_number;
        $amount = $orderStatus->amount;
        $status = $orderStatus->state;

        //get original order id 
        $changedOrderid = 0;
        foreach($orderStatus->additional_params  as $additional_param )
        {
            if('change_subscription' == $additional_param->name)
            {
                $changedOrderid = $additional_param->value;
            }
        }

        $changedOrder =  $ordeManager->getOrderById($changedOrderid);
        
        $start = new \DateTime();
        $endDate = new \DateTime();
        
        $order = $ordeManager->getOrderByNumber($orderNumber);
        $team = $teamManager->findTeamById($changedOrder->getTeamId());
        

        if ($status == 'PAID' && ($orderStatus->amount / 100) == $order->getTotalPrice())
        {
           
            $renewParams = $paymentManager->getRenewParamsByVariant($order->getVariant());
            //$endDate = clone $newOrder->getValidTo();
            $paymentManager->enlargeEndDate($endDate, $renewParams['renew_cycle'], $renewParams['renew_period']);

            if (null != $orderStatus->recurrence)
            {
                $order->setRenewDate($endDate);
                $order->setValidTo($endDate);
            }
            $order->setTeamId($changedOrder->getTeamId());
            $order->setUserPackageId($changedOrder->getUserPackageId());
            $order->setPaid($amount / 100);
            $order->setPaymentId($orderStatus->id);
            $order->setValidTo($endDate);
            $order->setStatus('paid');
            $ordeManager->saveOrder($order);
            
            //cancel old order
            $cancelResult = $gopay->cancelRecurrence($changedOrder->getPaymentId());

            if ($cancelResult->result == 'FINISHED')
            {
                $changedOrder->setStatus('canceled_by_change');
                $changedOrder->setRenewPayment(3);
                $ordeManager->saveOrder($changedOrder);
            }
            else
            {
                //die('Chyba');
            }
            
   
            //enlarge package
            $packageManager->enlargeUserPackage($order->getUserPackageId(), $endDate);

            //create invoice
            $invoiceManager = ServiceLayer::getService('InvoiceManager');
            $invoiceData['orderStatus'] = $orderStatus;
            $invoiceData['order'] = $order;
            $invoiceData['team'] = $team;
            $invoiceData['start'] = $start;
            $invoiceData['end'] = $endDate;
            $invoiceId = $this->createInvoice($invoiceData);
            
            $invoiceManager->createInvoicePackage($invoiceId, array('id' => $order->getUserPackageId()));
            
            
            $result = array('status' => $orderStatus->state, 'redirectUrl' => $this->getRouter()->link('gopay_success_payment', array('id' => $this->getRequest()->get('id'))));
           
        }
        else
        {
             $result = array('status' => $orderStatus->state, 'redirectUrl' => $this->getRouter()->link('gopay_success_payment', array('id' => $this->getRequest()->get('id'))));
        }
        
        
        ServiceLayer::getService('layout')->setTemplate(null);
        $response = new ControllerResponse();
        $response->setStatus('success');
        $response->setContent(json_encode($result, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
        $response->setType('application/json');
        return $response;
    }

    public function createRecurrencePaymentAction()
    {
        $gopay = new \GoPay\Api();
        $id = $this->getRequest()->get('tid');
        $paymentData = array(
            'id' => $id,
            'price' => 10,
            'order_number' => '0001'
        );
        $result = $gopay->createRecurrencePayment($paymentData);
    }

    /**
     * 
     */
    public function notifyPaymentAction()
    {
        $ordeManager = ServiceLayer::getService('OrderManager');
        $gopay = new \GoPay\Api();
        $orderStatus = $gopay->getPaymentInfo($this->getRequest()->get('id'));
        //log
        $logger = ServiceLayer::getService('system_logger');
        $log_data['activity'] = 'gopay_notify_payment';
        $log_data['scope'] = $_SERVER['REQUEST_URI'];

        //check if recurrence stopped
        if($orderStatus->recurrence->recurrence_state == 'STOPPED')
        {
            $log_data['detail'] = 'STOPPED: reccurence stopped, transaction_id'.$this->getRequest()->get('id');
            $logger->addSystemRecord($log_data);
            $order = $ordeManager->getOrderByPaymentId($this->getRequest()->get('id'));
            $order->setRenewPayment(3);
            $order->setStatus('canceled');
            $ordeManager->saveOrder($order);
            
            exit;
        }

        
        
        //if parent id not exist, stop it
        $parentId = $this->getRequest()->get('parent_id');
        if(null == $parentId)
        {
            $log_data['detail'] = 'ERROR: not exist parent_id';
            $logger->addSystemRecord($log_data);
            exit;
        }
        $parentOrder = $ordeManager->getOrderByPaymentId($parentId);

        if(null == $parentOrder)
        {
            $log_data['detail'] = 'ERROR: not exist parent order';
            $logger->addSystemRecord($log_data);
            exit;
        }

        //check if exist paid  order with this payment id
        
        $existOrder = $ordeManager->getOrderByPaymentId($this->getRequest()->get('id'));
        if(null != $existOrder)
        {
            //already paid
            $log_data['detail'] = 'ERROR: already paid, order id '.$existOrder->getId();
            $logger->addSystemRecord($log_data);
            exit;
        }

       
        $paymentManager = ServiceLayer::getService('PaymentManager');
        $packageManager = ServiceLayer::getService('UserPackageManager');
       
        $orderNumber = $orderStatus->order_number;
        $amount = $orderStatus->amount;
        $status = $orderStatus->state;
        $newOrder = clone $parentOrder;
        
       

        if (true)
        {
            //create new Order
            $newOrder->setId(null);
            $newOrder->setCreatedAt(new \DateTime());
            $renewParams = $paymentManager->getRenewParamsByVariant($parentOrder->getVariant());
            
            //get end date
            $userPackage = $packageManager->getUserPackageById($newOrder->getUserPackageId());
            $endDate = new \DateTime($userPackage['end_date']);

            $paymentManager->enlargeEndDate($endDate, $renewParams['renew_cycle'], $renewParams['renew_period']);
            $newOrder->setValidTo($endDate);
            $newOrder->setRenewDate($endDate);
            $newOrder->setPaymentId($orderStatus->id);
            $newOrder->setParentId($parentOrder->getId());
            $newOrder->setStatus('paid');

            $newOrderId = $ordeManager->saveOrder($newOrder);
            $newOrder->setId($newOrderId);

            //set all preview order as expired
            $childOrders = $ordeManager->getOrdersByParentId($parentOrder->getId());

            foreach($childOrders as $childOrder)
            {
                if($childOrder->getId() != $newOrderId)
                {
                     $childOrder->setStatus('expired');
                     $ordeManager->saveOrder($childOrder);
                }
            }
            
            if($parentOrder->getStatus() == 'paid')
            {
                $parentOrder->setStatus('expired');
                $ordeManager->saveOrder($parentOrder);
            }
           
            
            $log_data['detail'] = 'SUCCESS: create order '.$newOrderId;
            $logger->addSystemRecord($log_data);

            //predlz balik
            //$userPackage = $packageManager->getUserPackageById($order->getUserPackageId());

            $packageManager->enlargeUserPackage($parentOrder->getUserPackageId(), $endDate);
            $log_data['detail'] = 'SUCCESS: enlarge package to '.$endDate->format('d.m.Y H:i:s');
            $logger->addSystemRecord($log_data);
            

            //create Invoice
            $teamManager = ServiceLayer::getService('TeamManager');
            $team = $teamManager->findTeamById($newOrder->getTeamId());

            $invoiceManager = ServiceLayer::getService('InvoiceManager');
            $invoiceData['orderStatus'] = $orderStatus;
            $invoiceData['order'] = $newOrder;
            $invoiceData['team'] = $team;
            $invoiceData['start'] = $parentOrder->getValidTo();
            $invoiceData['end'] = $endDate;
            //$invoiceData['proPackage'] = $proPackage;

            $invoiceId = $this->createInvoice($invoiceData);
            $invoice = $invoiceManager->getInvoiceById($invoiceId);
            $invoiceManager->sendInvoiceAdminNotification($invoice);
            
            $user = ServiceLayer::getService('PlayerManager')->findPlayerById($parentOrder->getUserId());
            $notificationManager = ServiceLayer::getService('notification_manager');
            $notificationManager->sendPaymentSuccessNotification(array('email' =>$user->getEmail(), 'lang' => LANG));
            
            $log_data['detail'] = 'SUCCESS: create invoice'.$invoiceId;
            $logger->addSystemRecord($log_data);
            
            $invoiceManager->createInvoicePackage(array('id' => $invoiceId), array('id' => $newOrder->getUserPackageId()));
            
            $log_data['detail'] = 'SUCCESS: renew payment';
            $logger->addSystemRecord($log_data);
            
            
        }
        
        
    }

    public function paymentFailAction()
    {
        $orderDataManager = ServiceLayer::getService('OrderDataManager');
        $teamId = $this->getRequest()->get('team_id');
        $security = \Core\ServiceLayer::getService('security');
        $userIdentity = $security->getIdentity();
        $user = $userIdentity->getUser();

        $teamManager = ServiceLayer::getService('TeamManager');
        $team = $teamManager->findTeamById($teamId);

        $variant = $this->getRequest()->get('variant');
        $renew = $this->getRequest()->get('custom');

        $actualPackageData = ServiceLayer::getService('UserPackageManager')->getLastUserPackage($teamId);



        if (null == $actualPackageData)
        {
            $end = new \DateTime();
            $end->modify('+14 day');
            $end->setTime(0, 0, 0);
            $start = new \DateTime();
            $start->setTime(0, 0, 0);
            $pm = ServiceLayer::getService('PackageManager');
            $proPackage = $pm->getPackageWithProducts(5);
            $userPackage = ServiceLayer::getService('UserPackageManager')->addUserPackage([
                'user_id' => $user->getId(),
                'package_id' => 5,
                'team_id' => $team->getId(),
                'team_name' => $team->getName(),
                'date' => new \DateTime(),
                'start_date' => $start,
                'end_date' => $end,
                'name' => 'PRO',
                'note' => 'PAY FAIL',
                'level' => 5,
                'products' => json_encode($proPackage['products']),
                'player_limit' => 100,
                'guid' => GUID::create(),
                'variant' => $variant,
                'renew_payment' => $renew
            ]);

            $totalPrice = ($variant == 12) ? 129 : 18;

            $order = array();
            $order['package '] = $proPackage;
            $order['date'] = (string) DateTimeEx::now();
            $order['user_id'] = $user->getId();
            $order['user_package_id'] = $userPackage['id'];
            $order['ip'] = $_SERVER['REMOTE_ADDR'];
            $order['payment_id'] = '0';
            $order['team_id'] = $teamId;
            $order['renew_payment'] = $renew;
            $order['created_at'] = date('Y-m-d H:i:s');
            $order['variant'] = $variant;
            $order['total_price'] = $totalPrice;
            $order['currency'] = 'EUR';
            $order['paid'] = 0;
            $orderNo = $orderDataManager->save($order);

            \Core\DbQuery::query("INSERT INTO team_achievement_alert_log (name, achievement_code, created_at, created_by, team_id, alert_status, place_code) VALUES ('A je to tu!', 'step3_final', '" . date('Y-m-d H:i:s') . "', " . $user->getId() . ",  " . $teamId . ", 'finished', 'event_detail_rating')");
        }
        ServiceLayer::getService('TeamCreditManager')->refreshTeamPackages();

        //create invoice

        return $this->render('CR\Pay\PayModule:gopay:payment.php', array(
        ));
    }

    public function createInvoice($invoiceData)
    {
        $invoiceManager = ServiceLayer::getService('InvoiceManager');
        $order = $invoiceData['order'];
        $orderStatus = $invoiceData['orderStatus'];
        $team = $invoiceData['team'];
        $start = $invoiceData['start'];
        $end = $invoiceData['end'];
        $products = '';
        if(array_key_exists('proPackage', $invoiceData))
        {
            $products = json_encode($invoiceData['proPackage']['products']);
        }
        

        //$vat = $invoiceManager
        $vatPercentage = $invoiceManager->getCountryVat($orderStatus->payer->payment_card->card_issuer_country);

        $user = ServiceLayer::getService('PlayerManager')->findPlayerById($order->getUserId());
        $invoice = $invoiceManager->createInvoiceFromOrder($order);
        $invoice->setTransactionId($orderStatus->id);
        $invoice->setPayCountry($orderStatus->payer->payment_card->card_issuer_country);
        $invoice->setPaymentInfo(serialize($orderStatus));
        $invoice->setCurrency($orderStatus->currency);
        $invoice->setDescription(json_encode(array('team_name' => $team->getName(), 'package' => 'PRO', 'start' => $start->format('d.m.Y'), 'end' => $end->format('d.m.Y')), JSON_UNESCAPED_UNICODE));
        $invoice->setVatPercentage($vatPercentage);
        $invoice->setStartDate($start);
        $invoice->setEndDate($end);
        $invoice->setFullName($user->getFullName());
        $invoice->setTel($user->getPhone());
        $invoice->setEmail($user->getEmail());
        $invoice->setStreet($user->getAddress());
        $invoice->setProducts($products);
        $invoice->setPackageName('PRO');
        $invoice->setAmount($order->getTotalPrice());

        $vatCoef = (round($vatPercentage, 2) + 100) / 100;
        $rawPrice = $invoice->getAmount() / $vatCoef;
        $vatPrice = round($order->getTotalPrice() - $rawPrice, 2);
        $invoice->setVat($vatPrice);

        $countryData = $invoiceManager->getCountryByCode($orderStatus->payer->payment_card->card_issuer_country);
        $countryId =$countryData['country_code'];
        $invoiceNo = $invoiceManager->generateInvoiceNumber($countryId, $order->getVariant(), 00);
        $invoice->setInvoiceNo($invoiceNo);

        //$variableSymbol = $invoiceManager->generateVariableSymbol($orderStatus->payer->payment_card->card_issuer_country);
        $variableSymbol = $orderStatus->id;
        $invoice->setVs($variableSymbol);

        $invoiceId = $invoiceManager->saveInvoice($invoice);
        return $invoiceId;
    }

}
