<?php namespace CR\Pay\Controller;

use Core\Db;
use Core\Controller;
use Core\ServiceLayer;
use Core\Types\DateTimeEx;
use CR\Pay\Invoices\InvoiceTemplate;

class TestController extends Controller
{
    use Db;

    private $request;
    private $teamManager;
    private $packageManager;
    private $orderStateManager;
    private $activeTeam;
    private $team;
    private $invoiceManager;
    private $userPackageManager;
    private $packageTransitionManager;

    function __construct()
    {
        $this->request = ServiceLayer::getService('request');
        $this->teamManager = ServiceLayer::getService('TeamManager');
        $this->packageManager = ServiceLayer::getService('PackageManager');
        $this->orderStateManager = ServiceLayer::getService('OrderStateManager');
        $activeTeamManager = ServiceLayer::getService('ActiveTeamManager');
        $this->activeTeam = $activeTeamManager->getActiveTeam();
        $activeTeamId = (int)$this->activeTeam['id'];
        $this->team = $this->teamManager->findTeamById($activeTeamId);
        $this->invoiceManager = ServiceLayer::getService('InvoiceManager');
        $this->userPackageManager = ServiceLayer::getService('UserPackageManager');
        $this->packageTransitionManager = ServiceLayer::getService('PackageTransitionManager');
    }
    
    public function testInvoiceNumber()
    {
        $response = 'O:8:"stdClass":12:{s:2:"id";d:3049596134;s:9:"parent_id";d:3049547826;s:12:"order_number";s:3:"150";s:5:"state";s:4:"PAID";s:18:"payment_instrument";s:12:"PAYMENT_CARD";s:6:"amount";i:1800;s:8:"currency";s:3:"EUR";s:5:"payer";O:8:"stdClass":2:{s:12:"payment_card";O:8:"stdClass":5:{s:11:"card_number";s:16:"418803******0003";s:15:"card_expiration";s:4:"2008";s:10:"card_brand";s:13:"VISA Electron";s:19:"card_issuer_country";s:3:"CZE";s:16:"card_issuer_bank";s:20:"KOMERCNI BANKA, A.S.";}s:7:"contact";O:8:"stdClass":2:{s:5:"email";s:23:"marek.hubacek@gmail.com";s:12:"country_code";s:3:"CZE";}}s:6:"target";O:8:"stdClass":2:{s:4:"type";s:7:"ACCOUNT";s:4:"goid";d:8439859384;}s:4:"lang";s:2:"en";s:6:"gw_url";s:67:"https://gw.sandbox.gopay.com/gw/v3/0b1f1aebbbdbaa0a1169da14e18e5807";s:8:"eet_code";O:8:"stdClass":3:{s:3:"fik";s:39:"6b143011-a77b-47ba-bad8-9defcaa148d4-ff";s:3:"bkp";s:44:"7A5BECEA-FC51C4D4-9ECFD17A-CF7E2B4B-210AACAC";s:3:"pkp";s:344:"eNJfb6YeW+cioZAjmch/g8UnHFmx04aBTzJARYoczz0tbH6q/MIvffsqeAOXQSA2aT8bSc1JwV6YVxSD63SX2XEvgEIlCkWkSRg6xwYrNaviYNTs5AQCsT6ubju8akF6clF94DsW2SqJDurGlPTd7H7w5Ua8YxB7gWWFKnnhtYyBQv7gO37QDK+9axlrX7Tz3iGP4bl7qbV6M1u6mZEgxy9uKkeg/pfOBHbJCHQaK9jwiFlxfKtxgWcJv9uCD5SXEWchYKN0FduDedSk44wcmZRqxqSlvqeHIQTAPcHzFqlIvI57rdadsFcAiL/Vkzi4v4Tg2gIq5Cx29ZNN8IlEug==";}}';
        
        $object = unserialize($response);
        
        var_dump($object);
        
    }
    
    
    public function sendInvoiceEmailAction()
    {
        
        
        
        $invoiceManager = ServiceLayer::getService('InvoiceManager');
        $countryData = $invoiceManager->getCountryByCode('CZ');
        
        echo $test = serialize($countryData);exit;
        
        $invoice = $invoiceManager->getInvoiceById(57);
        $invoiceManager->sendInvoiceAdminNotification($invoice);

    }
    
    public function sandboxAction()
    {
        $actualPackageManager = ServiceLayer::getService('ActualPackageManager');
        dump($actualPackageManager->getTeamPlayerCount($this->activeTeam['id']));
        exit;
    }
    
    public function inteoConnectionAction()
    {
          //send to inteo
        $invoiceManager = ServiceLayer::getService('InvoiceManager');
        $inteoManager = \Core\ServiceLayer::getService('InteoManager');
        $invoice = $invoiceManager->getInvoiceById(57);
        $inteoManager->sendInvoice($invoice);
            
        /*
        ServiceLayer::getService('layout')->setTemplate(null);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://eshops.inteo.sk/api/v1/eshops/license/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
       

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          "Content-Type: application/json",
          "Authorization: Bearer 7346b8c3-b33a-4e10-8898-8fe141154b6d"
        ));

        $response = json_decode(curl_exec($ch));
        curl_close($ch);

        var_dump($response);
        exit;
         * 
         */
    }
    

    
    public function inteoAddInvoiceAction()
    {
         ServiceLayer::getService('layout')->setTemplate(null);
        $items = array();
        $items['name'] = 'test';
        $items['description'] = 'test item';
        $items['count'] = 1;
        $items['unitPrice'] = 10;
        $items['unitPriceWithVat'] = 12;
        $items['totalPrice'] = 10;
        $items['totalPriceWithVat'] = 12;
        $items['vat'] = 20;
        $items['typeId'] = 2;
        
        $omegaData = array();
        $omegaData['documentNumber'] = 0000;
        $omegaData['orderNumber'] = 1111;
        $omegaData['totalPrice'] =10;
        $omegaData['totalPriceWithVat'] = 12;
        $omegaData['createDate'] = '2015-04-28T08:23:13';
        $omegaData['completionDate'] = '2015-04-28T08:23:13';
        $omegaData['clientName'] = 'jan tester';
        $omegaData['clientContactName'] = '';
        $omegaData['clientContactSurname'] = '';
        $omegaData['clientStreet'] = 'testovacia 23';
        $omegaData['clientPostCode'] ='00001';
        $omegaData['clientTown'] = 'Bratislava';
        $omegaData['clientCountry'] = 'Slovakia';
        $omegaData['clientPhone'] =  '0908111222';
        $omegaData['clientEmail'] = 'jan.tester@coachrufus.com';
        $omegaData['clientRegistrationId'] = '00001';
        $omegaData['clientTaxId'] = '00002';
        $omegaData['clientVatId'] ='00003';
        $omegaData['variableSymbol'] = '1111';
        $omegaData['senderBankAccount'] = '0000';
        $omegaData['paymentType'] = 'paypal';
        $omegaData['deliveryType'] = 'online';
        $omegaData['currency'] = 'EUR';
        $omegaData['items'] = array($items);
        
        $curlData = json_encode(array($omegaData));
        
        //print_r($curlData);exit;

        
        ServiceLayer::getService('layout')->setTemplate(null);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://eshops.inteo.sk/api/v1/invoices/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $curlData);
       

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          "Content-Type: application/json",
          "Authorization: Bearer 7346b8c3-b33a-4e10-8898-8fe141154b6d"
        ));

        $response = json_decode(curl_exec($ch));
        curl_close($ch);

        var_dump($response);
        exit;
    }
}