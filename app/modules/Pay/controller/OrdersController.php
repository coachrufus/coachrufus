<?php namespace CR\Pay\Controller;

use Core\Controller;
use Core\ServiceLayer;
use Core\Types\DateTimeEx;
use CR\Pay\Invoices\InvoiceTemplate;
use Core\Notifications\BuyStore;
use Core\Notifications\BuyNotificationSender;

class OrdersController extends Controller
{
    private $request;
    private $teamManager;
    private $packageManager;
    private $activeTeam;
    private $team;
    private $invoiceManager;
    private $userPackageManager;

    function __construct()
    {
        $this->request = ServiceLayer::getService('request');
        $this->teamManager = ServiceLayer::getService('TeamManager');
        $activeTeamManager = ServiceLayer::getService('ActiveTeamManager');
        
         if(null != $this->request->get('team_id'))
        {
            $teamManager = ServiceLayer::getService('TeamManager');
            $team = $teamManager->findTeamById($this->request->get('team_id'));
            ServiceLayer::getService('ActiveTeamManager')->setActiveTeam($team);
        }
        
        
        $this->activeTeam = $activeTeamManager->getActiveTeam();
        $activeTeamId = (int)$this->activeTeam['id'];
        $this->team = $this->teamManager->findTeamById($activeTeamId);
        $this->invoiceManager = ServiceLayer::getService('InvoiceManager');
        $this->packageManager = ServiceLayer::getService('PackageManager');
        $this->userPackageManager = ServiceLayer::getService('UserPackageManager');
    }

    public function listAction()
    { 
         $this->getRequest()->redirect($this->getRouter()->link('player_dashboard',array('payment' => 'pro')));
        
        ServiceLayer::getService('security')->getIdentity()->checkTeamAccess($this->team);
        $invoices = $this->invoiceManager->getInvoicesWithUserPackagesForTeam($this->activeTeam['id']);
        return $this->render([
            'team' => $this->team,
            'invoices' => $invoices
        ]);
    }

    public function detailAction()
    {
        $orderNo = $this->request->get('order_no');
        $invoice = $this->invoiceManager->getInvoiceByVs($orderNo);
        $userPackage = $this->userPackageManager->getInvoiceUserPackageInfo($invoice['id'])['lastUserPackage'];
        return $this->render([
            'team' => $this->team,
            'invoice' => $invoice,
            'userPackage' => $userPackage,
            'startDate' => DateTimeEx::parse($invoice['start_date']),
            'endDate' => DateTimeEx::parse($invoice['end_date']),
            'invoiceFields1' => $this->invoiceManager->getInvoiceMainFields(),
            'invoiceFields2' => $this->invoiceManager->getInvoiceFields(),
            'products' => $this->packageManager->getProducts(),
            'countries' => $this->invoiceManager->getCountries()
        ]);
    }
}