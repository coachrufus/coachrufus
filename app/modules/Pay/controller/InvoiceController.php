<?php namespace CR\Pay\Controller;

use Core\Controller;
use Core\ServiceLayer;
use Core\Types\DateTimeEx;
use CR\Pay\Invoices\InvoiceTemplate;
use Core\Notifications\BuyStore;
use Core\Notifications\BuyNotificationSender;

class InvoiceController extends Controller
{
    private $request;
    private $teamManager;
    private $packageManager;
    private $activeTeam;
    private $team;
    private $invoiceManager;
    private $userPackageManager;

    function __construct()
    {
        $this->request = ServiceLayer::getService('request');
        $this->teamManager = ServiceLayer::getService('TeamManager');
        $activeTeamManager = ServiceLayer::getService('ActiveTeamManager');
        $this->activeTeam = $activeTeamManager->getActiveTeam();
        $activeTeamId = (int)$this->activeTeam['id'];
        $this->team = $this->teamManager->findTeamById($activeTeamId);
        $this->invoiceManager = ServiceLayer::getService('InvoiceManager');
        $this->userPackageManager = ServiceLayer::getService('UserPackageManager');
    }

    public function printToPdfAction()
    {
        $invoice = $this->invoiceManager->getInvoiceByVS($this->request->get('order_no'));
        
        
        $invoiceUserPackageInfo = $this->userPackageManager->getInvoiceUserPackageInfo($invoice['id']);
        foreach (['invoice_date', 'supply_date', 'payment_due_date'] as $dateField)
        {
            $invoice[$dateField] = DateTimeEx::parse($invoice[$dateField])->toString('d.m.Y');
        }
        include LIB_DIR . '/mpdf/mpdf.php';
        $template = new InvoiceTemplate('invoice');
        $template->assignMarks($invoice);
        $template->startDate = DateTimeEx::parse($invoiceUserPackageInfo['start_date'])->toString('d.m.Y');
        $template->endDate = DateTimeEx::parse($invoiceUserPackageInfo['end_date'])->toString('d.m.Y');
        $template->countryName = $this->invoiceManager->getCountries(LANG)[$invoice['country_id']];
        $mpdf = new \mPdf('utf-8');
        $mpdf->SetDisplayMode('fullpage');
        $mpdf->allow_charset_conversion = true;
        $mpdf->charset_in='UTF-8';
        $mpdf->WriteHTML((string)$template);
        $mpdf->Output(INVOICE_DIR . "/{$invoice['invoice_no']}.pdf", 'F');
        $_SESSION['email_order_no'] = $invoice['vs'];
        $this->request->redirect('/invoice/send-to-email');
    }
    
    public function pdfAction()
    {
        $layout = ServiceLayer::getService('layout');
        $layout->setTemplate(null);
        

        require_once LIB_DIR.'/tcpdf/tcpdf.php';
        
         $invoice = $this->invoiceManager->getInvoiceByOrderNo($this->request->get('order_no'));
         $countryCode = $invoice->getPaymentCountry();
         
         $country = $this->invoiceManager->getCountryByCode($countryCode);

       
         $html = $this->render('CR\Pay\PayModule:invoice:reciept.php',
                        array(
                                'invoice' => $invoice,
                                'countryName' => $country[LANG.'_name'],
                        ));


          $pdf = new \TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);


// set document information
        $pdf->SetCreator('COACHRUFUS');
        $pdf->SetTitle('');
        $pdf->SetSubject('');
        $pdf->SetKeywords('');

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING);
// set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
        $pdf->SetMargins(10, 10, 10);
        $pdf->SetHeaderMargin(0);
        $pdf->SetFooterMargin(0);
        
        $pdf->SetPrintHeader(false);
        $pdf->SetPrintFooter(false);


//set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
// ---------------------------------------------------------
// set default font subsetting mode
        $pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
        $pdf->SetFont('freesans', '', 10, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
        
        $pdf->AddPage();
           
        $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html->getContent(), $border = 0, $ln = 0, $fill = 0, $reseth = true, $align = '', $autopadding = true);
        
        $pdf->ImageSVG($file=PUBLIC_DIR.'/theme/img/logo_invoice.svg', $x=100, $y=-30, $w=90, $h='',$link='', $align='', $palign='', $border=0, $fitonpage=false);
       
        //$pdf->writeHTML($html);
// ---------------------------------------------------------
// Close and output PDF document
// This method has several options, check the source code documentation for more information.
        $pdf->Output($invoice->getInvoiceNo() , 'I');

        //$response = new Response($fileContent);

        //return $response->headers->set('Content-type', 'application/pdf');
         
         
         
    }

    public function sendToEmailAction()
    {
        if (isset($_SESSION['email_order_no']))
        {
            $orderNo = $_SESSION['email_order_no'];
            if (!PAY_DEBUG)
            {
                unset($_SESSION['email_order_no']);
            }
            $invoice = $this->invoiceManager->getInvoiceByVS($orderNo);
            $buyNotificationSender = new BuyNotificationSender(new BuyStore($invoice));
            $buyNotificationSender->sendAll();
            $this->request->redirect("/pay/payment-success/{$orderNo}");
        }
    }
}