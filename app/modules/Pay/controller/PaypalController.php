<?php namespace CR\Pay\Controller;

use Core\Db;
use Core\Controller;
use Core\ServiceLayer;
use Core\Types\DateTimeEx;
use CR\Pay\Invoices\InvoiceTemplate;
use CR\Pay\Lib\PackageTransitionType;
use CR\Pay\Model\ActualPackage;
use Core\GUID;

class PaypalController extends Controller
{
    
  
    
    public function ipnAction()
    {
        $layout = \Core\ServiceLayer::getService('layout');
        $layout->setTemplate(null);
        $paymentInfoParam = $this->getRequest()->get('item_number');
        
        $paymentInfo = explode('-',$paymentInfoParam);
        $teamId =$paymentInfo[0];
        $authorId =$paymentInfo[1];
        $paymentId = $this->getRequest()->get('txn_id');
        $paymentData = $_SERVER['QUERY_STRING'];
        $renew = $this->getRequest()->get('custom');
        $paid = $this->getRequest()->get('mc_gross');
        $currency = $this->getRequest()->get('mc_currency');
        $variant = $this->getRequest()->get('option_name1');
        
        
        //first check if already paid
        $orderDataManager = ServiceLayer::getService('OrderDataManager');
        $existPayment = $orderDataManager->loadByPaymentId($paymentId);
        
        if(null != $existPayment)
        {
            die('Order already paid');
        }

        $teamManager = ServiceLayer::getService('TeamManager');
        $team = $teamManager->findTeamById($teamId);

         //send invoice to kros
        
        /*
        $invoiceData = ServiceLayer::getService('InvoiceManager')->getInvoiceByVs('149161483090959');
        $invoice =  new \CR\Pay\Model\Invoice();
        $mapper = new \Core\EntityMapper('\CR\Pay\Model\Invoice');        
        $mapper->updateEntityFromArray($invoice, $invoiceData);
        
        
        
        ServiceLayer::getService('InteoManager')->sendInvoice($invoice);
        exit;
        */
       
        //finish step progres
        $progressChainManager = ServiceLayer::getService('ProgressChainManager');
      
        if(false == $progressChainManager->allStepsFinished($teamId))
        {
            $achievementAlertManager = ServiceLayer::getService('AchievementAlertManager');
            $log = new \CR\Gamification\Model\AlertLog();
            $log->setName('step3_final');
            $log->setAchievementCode('step3_final');
            $log->setPlaceCode('event_detail_rating');
            $log->setCreatedAt(new \DateTime());
            $log->setCreatedBy($authorId);
            $log->setTeamId($teamId);
            $achievementAlertManager->finishAlertLog($log);
        }
       
        
        //create order
        
        
        $actualPackage = ServiceLayer::getService('UserPackageManager')->getLastUserPackage($teamId);

        if(null == $actualPackage)
        {
            $end = new \DateTime();
            $end->modify('+30 day');
            $pm = ServiceLayer::getService('PackageManager');
            $proPackage = $pm->getPackageWithProducts(5);
            $userPackage = ServiceLayer::getService('UserPackageManager')->addUserPackage([
                'user_id' => $authorId,
                'package_id' => 5,
                'team_id' => $team->getId(),
                'team_name' => $team->getName(),
                'date' => new \DateTime(),
                'start_date' => new \DateTime(),
                'end_date' => $end,
                'name' => 'PRO',
                'note' => '',
                'level' => 5,
                'products' => json_encode($proPackage['products']),
                'player_limit' => 100,
                'guid' => GUID::create(),
                'variant' => $variant,
                'renew_payment' => $renew
            ]);
        }
        else
        {

            $test = iterator_to_array($actualPackage);
            $validFrom = new \DateTime($test['end_date']);
            $validTo= clone $validFrom;
            $validTo->modify('+30 day');
            $pm = ServiceLayer::getService('PackageManager');
            $proPackage = $pm->getPackageWithProducts(5);
            $userPackage = ServiceLayer::getService('UserPackageManager')->addUserPackage([
                'user_id' => $authorId,
                'package_id' => 5,
                'team_id' => $team->getId(),
                'team_name' => $team->getName(),
                'date' => new \DateTime(),
                'start_date' => $validFrom,
                'end_date' => $validTo,
                'name' => 'PRO',
                'note' => '',
                'level' => 5,
                'products' => json_encode($proPackage['products']),
                'player_limit' => 100,
                'guid' => GUID::create(),
                'variant' => $variant,
                'renew_payment' => $renew
            ]);
        }

        
         $totalPrice = ($variant == 12) ? 100 : 10;
         
         $order = array();
         $order['package '] = $proPackage;
         $order['date'] = (string)DateTimeEx::now();
         $order['user_id'] = $authorId;
         $order['user_package_id'] = $userPackage['id'];
         $order['ip'] =  $_SERVER['REMOTE_ADDR'];
         $order['payment_id'] =  $paymentId ;
         $order['team_id'] =  $teamId ;
         $order['renew_payment'] =  $renew ;
         $order['created_at'] =  date('Y-m-d H:i:s') ;
         $order['variant'] = $variant;
         $order['total_price'] = $totalPrice;
         $order['currency'] = $currency;
         $order['paid'] = $paid;
         $orderNo = $orderDataManager->save($order);
        //create invoice
         $proPackage['period'] = 'month';
         $proPackage['teamId'] = $team->getId();
         $proPackage['start_date'] =$validFrom;
         $proPackage['end_date'] =$validTo;
         //$user = ServiceLayer::getService('security')->getIdentity()->getUser();
         $user = ServiceLayer::getService('PlayerManager')->findPlayerById($authorId);
         $proPackage['invoice']['full_name'] = $user->getFullName();
         $proPackage['invoice']['tel'] = $user->getPhone();
         $proPackage['invoice']['email'] = $user->getEmail();
         $proPackage['invoice']['street'] = $user->getAddress();

         
         $invoiceManager = ServiceLayer::getService('InvoiceManager');
         $invoiceManager->createInvoice($orderNo,  new \DateTime(), $proPackage, [$userPackage],$order);
         
        
        
    }
    
    
    public function ipnOldAction()
    {
        $layout = \Core\ServiceLayer::getService('layout');
        $layout->setTemplate(null);
        
        $packageTransitionManager = ServiceLayer::getService('PackageTransitionManager');
        $userPackageManager =  ServiceLayer::getService('UserPackageManager');
        $teamManager = ServiceLayer::getService('TeamManager');
        $invoiceManager = ServiceLayer::getService('InvoiceManager');
        $orderNo = $this->getRequest()->get('item_number');

        $orderData = ServiceLayer::getService('OrderDataManager')->load($orderNo);
        $activeTeam = $teamManager->findTeamById($orderData['package']['teamId']);

        $packageTransition = $packageTransitionManager->getPackageTransition($orderData['package']['teamId'], DateTimeEx::now(), $orderData);
        $packageData = $orderData['package'];

        
        $country = $invoiceManager->getCountryInfo($orderData['package']['invoice']['country_id']);
        $ipData = ServiceLayer::getService('LocalityManager')->getLocalityByIp($orderData['ip']);
        //compute vat
        $countryInfo = array();
        $countryInfo['payment'] = $this->getRequest()->get('address_country_code');
        $countryInfo['ip'] = $ipData->country_code;
        $countryInfo['invoice'] = $country['country_code'];

        $countryCount = array_count_values($countryInfo);
        arsort($countryCount);
        $finalCountry = key($countryCount);

        $vatCountry = $invoiceManager->getCountryInfoByCode($finalCountry);


        $orderData['ip_country'] = $ipData->country_code;
        $orderData['pay_country'] = $this->getRequest()->get('address_country_code');
        $orderData['vat_percentage'] = $vatCountry['vat'];

       
        $now = new \DateTime();
        if ($packageTransition->transitionType->hasValue(PackageTransitionType::UpperPackage))
        {
            $packageUpdater = ServiceLayer::getService('PackageUpdater');
            $updateResult = $packageUpdater->updatePackagesTo($packageData['id']);
            $userPackages = $updateResult->userPackages;
        }
        else
        {
            $userPackage = $userPackageManager->addUserPackage([
                'user_id' => $orderData['author_id'],
                'package_id' => $packageData['id'],
                'team_id' => $activeTeam->getId(),
                'team_name' => $activeTeam->getName(),
                'date' => $now,
                'start_date' => DateTimeEx::parse($packageData['start_date'])->toDateTime(),
                'end_date' => DateTimeEx::parse($packageData['end_date'])->toDateTime(),
                'name' => $packageData['name'],
                'note' => $packageData['note'],
                'level' => $packageData['level'],
                "price_{$packageData['period']}" => $packageData["price_{$packageData['period']}"],
                'products' => json_encode($packageData['products']),
                'player_limit' => $packageData['player_limit'],
                'guid' => GUID::create()
            ]);
            $userPackages = [$userPackage];
        }
        
        
           /*
        $country = $this->invoiceManager->getCountryInfo($order['package']['invoice']['country_id']);
        $creditCardData  = json_decode(file_get_contents('https://binlist.net/json/433703'));
        $ipData = ServiceLayer::getService('LocalityManager')->getLocalityByIp('95.102.78.107');

        
        $countryInfo = array();
        $countryInfo['credit_card'] = $creditCardData->country_code;
        $countryInfo['ip'] = $ipData->country_code;
        $countryInfo['invoice'] = $country['country_code'];
        
        $countryCount = array_count_values($countryInfo);
        arsort($countryCount);
        $finalCountry = key($countryCount);
*/
        
        

        $invoiceManager->createInvoice($orderNo, $now, $packageData, $userPackages,$orderData);
        $invoiceData = $invoiceManager->getInvoiceByVS($orderNo);
        $invoiceUserPackageInfo = $userPackageManager->getInvoiceUserPackageInfo($invoiceData['id']);
        $invoiceManager->createPdf($invoiceData,$invoiceUserPackageInfo);
        $invoiceManager->sendToEmail($invoiceData);
        
        $invoice = new \CR\Pay\Model\Invoice();
        
        $invoiceMapper = new \Core\EntityMapper('\CR\Pay\Model\Invoice');
        $invoiceMapper->updateEntityFromArray($invoice,$invoiceData);
        
      
        
        $items = array();
        $items['name'] = $invoice->getPackageName();
        $items['description'] = '';
        $items['count'] = $invoice->getCount();
        $items['unitPrice'] = $invoice->getRawPrice();
        $items['unitPriceWithVat'] = $invoice->getPrice();
        $items['totalPrice'] =$invoice->getRawPrice();
        $items['totalPriceWithVat'] =$invoice->getPrice();
        $items['vat'] = $invoice->getVatPercentage();
        $items['typeId'] = 2;
        
        $omegaData = array();
        $omegaData['documentNumber'] = $invoice->getInvoiceNo();
        $omegaData['orderNumber'] = $invoice->getVs();
        $omegaData['totalPrice'] = $invoice->getRawPrice();
        $omegaData['totalPriceWithVat'] = $invoice->getPrice();
        $omegaData['createDate'] = $invoice->getInvoiceDate();
        $omegaData['completionDate'] = $invoice->getInvoiceDate();
        $omegaData['clientName'] = $invoice->getFullName();
        $omegaData['clientContactName'] = '';
        $omegaData['clientContactSurname'] = '';
        $omegaData['clientStreet'] = $invoice->getStreet();
        $omegaData['clientPostCode'] = $invoice->getZip();
        $omegaData['clientTown'] = $invoice->getCity();
        $omegaData['clientCountry'] = $invoice->getCountryName();
        $omegaData['clientPhone'] = $invoice->getTel();
        $omegaData['clientEmail'] = $invoice->getEmail();
        $omegaData['clientRegistrationId'] = $invoice->getIco();
        $omegaData['clientTaxId'] = $invoice->getDic();
        $omegaData['clientVatId'] = $invoice->getIcdph();
        $omegaData['variableSymbol'] = $invoice->getVs();
        $omegaData['senderBankAccount'] = '0000';
        $omegaData['paymentType'] = 'paypal';
        $omegaData['deliveryType'] = 'online';
        $omegaData['currency'] = 'EUR';
        $omegaData['items'] = array($items);
        
        
        
        //kross id
        //7346b8c3-b33a-4e10-8898-8fe141154b6d

    }
    public function returnAction()
    {
       $this->getRequest()->addFlashMessage('payment_success',$this->getTranslator()->translate('Payment success'));
       
       $this->getRequest()->Redirect($this->getRouter()->link('profil').'#orders');
        
       
        
        /*
        $packageTransitionManager = ServiceLayer::getService('PackageTransitionManager');
        $userPackageManager =  ServiceLayer::getService('UserPackageManager');
        $invoiceManager = ServiceLayer::getService('InvoiceManager');
        $teamManager = ServiceLayer::getService('TeamManager');
        $team = $teamManager->findTeamById($this->getRequest()->get('item_number'));
        $activeTeamManager = ServiceLayer::getService('ActiveTeamManager');
        $activeTeamManager->setActiveTeam($team);
 */
         
      
   
    }
}