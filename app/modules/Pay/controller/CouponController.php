<?php

namespace CR\Pay\Controller;

use Core\Controller;
use Core\ControllerResponse;
use Core\DbStorage;
use Core\GUID;
use Core\ServiceLayer;
use CR\Pay\Model\Coupon;
use DateTime;

class CouponController extends Controller {

    public function paymentInfoAction()
    {
        $couponManager =  ServiceLayer::getService('CouponManager');
        $paymentManager =  ServiceLayer::getService('PaymentManager');
        $promoCode =$couponManager->findByCode($this->getRequest()->get('promo_code'));
 
        if($promoCode->getCharges() > 0)
        {
            if($promoCode->getDiscountType() == 'percent')
            {
                 $basePrice = ($paymentManager->getProMonthPrice()*$promoCode->getMonthPeriod())*100;
                
                 $discountSum =  ($paymentManager->getProMonthPrice()*$promoCode->getMonthPeriod()*($promoCode->getValuePercent()/100))*100;
                 $data['basePrice']  = round($basePrice/100,2);
                 $data['price']  =  round(($basePrice- $discountSum)/100,2);
                 $data['discount']  = $promoCode->getValuePercent();
                 $data['discountSum']  =  round($discountSum/100,2);
                 $data['discountCode']  =  $promoCode->getCode();
                
            }
        }
        
        $layout = ServiceLayer::getService('layout');
        $layout->setTemplate(null);
        
        $response = new ControllerResponse();
        $response->setStatus('success');
        $response->setContent(json_encode($data));
        $response->setType('json');
        return $response;
    }
    
    
    function generateAction()
    {
        $repo = ServiceLayer::getService('CouponRepository');
        for ($i = 0; $i < 5000; $i++)
        {
            $coupon = new Coupon();
            $coupon->setExpirationDate(new DateTime('2017-06-01'));
            $coupon->setCharges(1);
            $coupon->setCode(substr(md5(time() . $i), 0, 8));
            $coupon->setCreatedAt(new DateTime());
            $coupon->setName('FLOORBALL');
            $repo->save($coupon);
        }
    }

    public function activateAction()
    {
        $teamId = $this->getRequest()->get('team_id');
        $teamManager = ServiceLayer::getService('TeamManager');
        $couponManager =  ServiceLayer::getService('CouponManager');
        //create pro package
        $actualPackage = ServiceLayer::getService('UserPackageManager')->getLastUserPackage($teamId);
        $coupon =$couponManager->findByCode($this->getRequest()->get('promo_code'));


        if (null == $actualPackage)
        {
            if ('FLOORBALL' == $coupon->getName())
            {
                $team = $teamManager->findTeamById($teamId);
                $this->createFloorballChallengePackage($team, $coupon);
                
                $couponManager->addCouponUse($coupon);
            }
        }
    }

    private function createFloorballChallengePackage($team, $coupon)
    {
        $security = ServiceLayer::getService('security');
        $identity = $security->getIdentity();
        
        $end = new DateTime('2017-05-31');
        $pm = ServiceLayer::getService('PackageManager');
        $proPackage = $pm->getPackageWithProducts(5);
        ServiceLayer::getService('UserPackageManager')->addUserPackage([
            'user_id' => $identity->getUser()->getId(),
            'package_id' => 5,
            'team_id' => $team->getId(),
            'team_name' => $team->getName(),
            'date' => new DateTime(),
            'start_date' => new DateTime(),
            'end_date' => $end,
            'name' => 'PRO',
            'note' => 'FLOORBALL',
            'level' => 5,
            'products' => json_encode($proPackage['products']),
            'player_limit' => 100,
            'guid' => GUID::create()
        ]);

        $storage = new DbStorage('fllorball_challenge_teams');
        $storage->setTableName('fllorball_challenge_teams');
        $data['team_id'] = $team->getId();
        $data['coupon_code'] = $coupon->getCode();
        $data['activation_date'] = date('Y-m-d H:i:s');
        $data['ip'] = $_SERVER['REMOTE_ADDR'];
        $storage->create($data);
    }

}
