<?php namespace CR\Pay\Controller;

use Core\Db;
use Core\Controller;
use Core\ServiceLayer;
use Core\Types\DateTimeEx;
use CR\Pay\Invoices\InvoiceTemplate;
use CR\Pay\Lib\PackageTransitionType;
use Core\GUID;

class PayController extends Controller
{
    use Db;

    private $request;
    private $teamManager;
    private $packageManager;
    private $orderStateManager;
    private $activeTeam;
    private $team;
    private $invoiceManager;
    private $userPackageManager;
    private $packageTransitionManager;

    function __construct()
    {
        $this->request = ServiceLayer::getService('request');
        $this->teamManager = ServiceLayer::getService('TeamManager');
        $this->packageManager = ServiceLayer::getService('PackageManager');
        $this->orderStateManager = ServiceLayer::getService('OrderStateManager');
        $activeTeamManager = ServiceLayer::getService('ActiveTeamManager');
        
        if(null != $this->request->get('team_id'))
        {
            $teamManager = ServiceLayer::getService('TeamManager');
            $team = $teamManager->findTeamById($this->request->get('team_id'));
            ServiceLayer::getService('ActiveTeamManager')->setActiveTeam($team);
        }
        
       
        
        
        $this->activeTeam = $activeTeamManager->getActiveTeam();
        $activeTeamId = (int)$this->activeTeam['id'];
        $this->team = $this->teamManager->findTeamById($activeTeamId);
        $this->invoiceManager = ServiceLayer::getService('InvoiceManager');
        $this->userPackageManager = ServiceLayer::getService('UserPackageManager');
        $this->packageTransitionManager = ServiceLayer::getService('PackageTransitionManager');
    }

    public function listAction()
    {
        /*if (PAY_DEBUG)
        {
            $db = $this->getDb();
            $db->user_packages()->where('id <= 25')->update(['is_canceled' => 0]);
            $db->user_packages()->where('id > 25')->delete();
        }*/
        
        ServiceLayer::getService('security')->getIdentity()->checkTeamAccess($this->team);
        
        
        return $this->render([
            'packageList' => $this->packageManager->getPackageList(),
            'products' => $this->packageManager->getProducts(),
            'team' => $this->team,
            'actualLevel' => $this->userPackageManager->getActualPackageLevel($this->activeTeam['id']),
            'packageOrderStates' => $this->orderStateManager->getPackageOrderStates()
        ]);
    }
    
    public function orderPackageAction()
    {
        $packageId = (int)$this->request->get('package_id');
        $products = $this->packageManager->getProducts();
        $_SESSION['order'] = [
            'package' => $this->packageManager->getPackageWithProducts($packageId, $products) + ['teamId' => $this->activeTeam['id']]
        ];
        $this->request->redirect("/pay/package-period");
    }
    
    public function packagePeriodAction()
    {
        $order = $_SESSION['order'];
        $packageTransition = $this->packageTransitionManager->getPackageTransition($order['package']['teamId'], DateTimeEx::now(), $order);
        if ($packageTransition->transitionType->hasValue(PackageTransitionType::UpperPackage))
        {
            $this->request->redirect('/pay/package-update');
        }
        return $this->render([
            'team' => $this->team,
            'package' => $order['package'],
            'startDate' => $packageTransition->startDate->toString('d.m.Y')
        ]);
    }

    public function packageUpdateAction()
    {
        $order = $_SESSION['order'];
        $packageTransition = $this->packageTransitionManager->getPackageTransition($order['package']['teamId'], DateTimeEx::now(), $order);
        return $this->render([
            'team' => $this->team,
            'package' => $order['package'],
            'startDate' => DateTimeEx::of($packageTransition->updateInfo->startDate)->toString('d.m.Y'),
            'endDate' => DateTimeEx::of($packageTransition->updateInfo->endDate)->toString('d.m.Y')
        ]);
    }
    
    public function packagePeriodUpdateAction()
    {
        $order = $_SESSION['order'];
        foreach ([
            'period',
            'coupon'
        ] as $field)
        {
            $order['package'][$field] = $_POST[$field];
        }
        $packageTransition = $this->packageTransitionManager->getPackageTransition($order['package']['teamId'], DateTimeEx::now(), $order);
        $order['package']['start_date'] = (string)$packageTransition->startDate;
        $order['package']['end_date'] = (string)$packageTransition->endDate;
        $order['package']['start_date_string'] = $packageTransition->startDate->toString('d.m.Y');
        $order['package']['end_date_string'] = $packageTransition->endDate->toString('d.m.Y');
        $_SESSION['order'] = $order;
        $this->request->redirect("/pay/invoice-data");
    }
    
    public function invoiceDataAction()
    {
        $user = ServiceLayer::getService('security')->getIdentity()->getUser();
        $invoiceDefaultData = $this->invoiceManager->getInvoiceDefaultData($user, $_SESSION['order']['package']['teamId']);
        $formData = array();
        foreach($invoiceDefaultData->getValues() as $key => $val)
        {
            $formData[$key] = $val;
        }

        $form = new \CR\Pay\Form\InvoiceForm($invoiceDefaultData);
        $form->updateValues($formData);
        $form->setFieldChoices('country_id',array('' => '') + $this->invoiceManager->getCountries(LANG));

        return $this->render([
            'team' => $this->team,
            'default' => $invoiceDefaultData,
            'invoiceFields' => $this->invoiceManager->getInvoiceFields(),
            'form' => $form
        ]);
    }
    
    public function invoiceDataUpdateAction()
    {
        $order = $_SESSION['order'];
        
        $requestData = $this->getRequest()->get('record');
        
        $order['package']['invoice'] = $requestData;

        //$order['package']['invoice']['country_id'] = $request->get;
        $_SESSION['order'] = $order;
        $this->request->redirect("/pay/package-summary");
    }
    
    public function packageSummaryAction()
    {
        if (!isset($_SESSION['order']))
        {
            $this->request->redirect("/pay/package/list");
        }
        $order = $_SESSION['order'];
         $user = ServiceLayer::getService('security')->getIdentity()->getUser();
        //save order
        if(!array_key_exists('id', $order))
        {
            $order['date'] = (string)DateTimeEx::now();
            $order['author_id'] =  $user->getId();
            $order['ip'] =  $_SERVER['REMOTE_ADDR'];
            
            $orderNo = ServiceLayer::getService('OrderDataManager')->save($order);
            $order['id'] = $orderNo;
            if($order['package']['period'] === "month")
            {
                $order['price'] = $order['package']['price_month'];
            }
            else
            {
                $order['price'] = $order['package']['price_year'];
            }
           
            $_SESSION['order'] = $order;
        }
        
       t_dump($order);
        
     
        return $this->render([
            'team' => $this->team,
            'order' => $order,
            'package' => $order['package'],
            'products' => $this->packageManager->getProducts(),
            'invoiceFields' => $this->invoiceManager->getInvoiceFields(),
            'countryInfo' => $countryInfo,
        ]);
    }

    /*
    public function packageBuyAction()
    {
        $order = $_SESSION['order'];
        $order['date'] = (string)DateTimeEx::now();
        if (!PAY_DEBUG)
        {
            unset($_SESSION['order']);
        }
        $orderNo = ServiceLayer::getService('OrderDataManager')->save($order);
        $this->request->redirect("/pay/package-payment/{$orderNo}");
    }
*/
    public function packagePaymentAction()
    {
        $orderNo = $this->request->get('order_no');
        $orderData = ServiceLayer::getService('OrderDataManager')->load($orderNo);
        if (IGNORE_PAYMENT)
        {
            $this->request->redirect("/pay/package-after-payment/{$orderNo}");
        }
        else
        {
            
        }
    }

    public function packageAfterPaymentAction()
    {
        $orderNo = $this->request->get('order_no');
        $orderData = ServiceLayer::getService('OrderDataManager')->load($orderNo);
        $packageTransition = $this->packageTransitionManager->getPackageTransition($orderData['package']['teamId'], DateTimeEx::now(), $orderData);
        $packageData = $orderData['package'];
        $user = ServiceLayer::getService('security')->getIdentity()->getUser();
        $now = new \DateTime();
        if ($packageTransition->transitionType->hasValue(PackageTransitionType::UpperPackage))
        {
            $packageUpdater = ServiceLayer::getService('PackageUpdater');
            $updateResult = $packageUpdater->updatePackagesTo($packageData['id']);
            $userPackages = $updateResult->userPackages;
        }
        else
        {
            $userPackage = $this->userPackageManager->addUserPackage([
                'user_id' => $user->getId(),
                'package_id' => $packageData['id'],
                'team_id' => $this->activeTeam['id'],
                'team_name' => $this->activeTeam['name'],
                'date' => $now,
                'start_date' => DateTimeEx::parse($packageData['start_date'])->toDateTime(),
                'end_date' => DateTimeEx::parse($packageData['end_date'])->toDateTime(),
                'name' => $packageData['name'],
                'note' => $packageData['note'],
                'level' => $packageData['level'],
                "price_{$packageData['period']}" => $packageData["price_{$packageData['period']}"],
                'products' => json_encode($packageData['products']),
                'player_limit' => $packageData['player_limit'],
                'guid' => GUID::create()
            ]);
            $userPackages = [$userPackage];
        }
        $this->invoiceManager->createInvoice($orderNo, $now, $packageData, $userPackages);
        
        $this->request->redirect("/invoice/print-to-pdf/{$orderNo}");
    }

    function paymentSuccessAction()
    {
        $actualPackageManager = ServiceLayer::getService('ActualPackageManager');
        $actualPackageManager->saveToSession();
        $orderNo = $this->request->get('order_no');
        $invoice = $this->invoiceManager->getInvoiceByVS($orderNo);
        return $this->render([
            'team' => $this->team,
            'invoice' => $invoice
        ]);
    }
}