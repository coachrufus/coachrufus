<?php

namespace CR\Pay\Controller;

use Core\Controller;
use Core\ControllerResponse;
use Core\GUID;
use Core\ServiceLayer;
use Core\Types\DateTimeEx;

class CronController extends Controller {

    /**
     * Send notify 7 days before next year reccurence payment
     * @return ControllerResponse
     */
    public function YearReccurenceNotifyAction()
    {
        $users = ServiceLayer::getService('UserPackageManager')->getYearNotifyUsers();
        
        $logger = ServiceLayer::getService('system_logger');
        $log_data['activity'] = 'year_reccurence_notify';
        $log_data['detail'] = 'start';
        $logger->addSystemRecord($log_data);
         
        foreach($users as $user)
        {
            $notificationManager = ServiceLayer::getService('notification_manager');
            $notificationManager->sendPaymentYearNotification(array('email' =>$user->getEmail(), 'lang' => LANG));
            
            $log_data['activity'] = 'year_reccurence_notify';
            $log_data['detail'] = 'send notify to: ' .$user->getEmail();
            $logger->addSystemRecord($log_data);
        }
    }


}
