<?php namespace CR\Pay\Controller;

use Core\Controller;
use Core\ServiceLayer;
use Core\Types\DateTimeEx;
use CR\Pay\Invoices\InvoiceTemplate;
use Core\Notifications\BuyStore;
use Core\Notifications\BuyNotificationSender;

class PackagesController extends Controller
{
    private $request;
    private $teamManager;
    private $packageManager;
    private $activeTeam;
    private $team;
    private $invoiceManager;
    private $userPackageManager;

    function __construct()
    {
        $this->request = ServiceLayer::getService('request');
        $this->teamManager = ServiceLayer::getService('TeamManager');
        $activeTeamManager = ServiceLayer::getService('ActiveTeamManager');
        $this->activeTeam = $activeTeamManager->getActiveTeam();
        $activeTeamId = (int)$this->activeTeam['id'];
        $this->team = $this->teamManager->findTeamById($activeTeamId);
        $this->invoiceManager = ServiceLayer::getService('InvoiceManager');
        $this->packageManager = ServiceLayer::getService('PackageManager');
        $this->userPackageManager = ServiceLayer::getService('UserPackageManager');
        $this->actualPackageManager = ServiceLayer::getService('ActualPackageManager');
    }

    function listAction()
    {
        $userPackages = $this->userPackageManager->getUserPackagesForTeam($this->activeTeam['id']);
        return $this->render([
            'team' => $this->team,
            'userPackages' => $userPackages
        ]);
    }

    function packageConfigAction()
    {
        $actualPackage = $this->actualPackageManager->getActualPackage();
        return $this->render([
            'team' => $this->team,
            'activeTeam' => $this->activeTeam,
            'actualPackage' => $actualPackage,
            'teamPlayerCount' => $this->actualPackageManager->getTeamPlayerCount($this->activeTeam['id'])
        ]);
    }
}