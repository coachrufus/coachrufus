<?php

$acl->allowRole('guest', 'pay_paypal_ipn');
$router->addRoute('pay_paypal_ipn', '/pay/paypal/ipn', [
    'controller' => 'CR\Pay\PayModule:Paypal:ipn'
]);

$acl->allowRole('ROLE_USER', 'pay_paypal_return');
$router->addRoute('pay_paypal_return', '/pay/paypal/return', [
    'controller' => 'CR\Pay\PayModule:Paypal:return'
]);