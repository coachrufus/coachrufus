<?php


$acl->allowRole('ROLE_USER', 'test_send_invoice_email');
$router->addRoute('test_send_invoice_email', '/test/invoice/email', [
    'controller' => 'CR\Pay\PayModule:Test:sendInvoiceEmail'
]);

$acl->allowRole('ROLE_USER', 'pay_test_inteo');
$router->addRoute('pay_test_inteo', '/pay/test/inteo', [
    'controller' => 'CR\Pay\PayModule:Test:inteoConnection'
]);

$acl->allowRole('ROLE_USER', 'pay_test_inteo_add_invoice');
$router->addRoute('pay_test_inteo_add_invoice', '/pay/test/inteo/invoice/add', [
    'controller' => 'CR\Pay\PayModule:Test:inteoAddInvoice'
]);