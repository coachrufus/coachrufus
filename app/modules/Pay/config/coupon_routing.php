<?php

$acl->allowRole('ROLE_USER', 'coupon_generate');
$router->addRoute('coupon_generate', '/coupon/generate', [
    'controller' => 'CR\Pay\PayModule:Coupon:generate'
]);

$acl->allowRole('ROLE_USER', 'coupon_activate');
$router->addRoute('coupon_activate', '/coupon/activate', [
    'controller' => 'CR\Pay\PayModule:Coupon:activate'
]);

$acl->allowRole('ROLE_USER', 'coupon_payment_info');
$router->addRoute('coupon_payment_info', '/coupon/payment-info', [
    'controller' => 'CR\Pay\PayModule:Coupon:paymentInfo'
]);