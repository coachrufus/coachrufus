<?php

$acl->allowRole('guest', 'cron_payment_year_reccurence_notify');
$router->addRoute('cron_payment_year_reccurence_notify', '/pay/cron/year-reccurence-notify', [
    'controller' => 'CR\Pay\PayModule:Cron:YearReccurenceNotify'
]);

