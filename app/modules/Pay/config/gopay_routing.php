<?php

$acl->allowRole('guest', 'gopay_payment');
$router->addRoute('gopay_payment', '/pay/gopay/payment', [
    'controller' => 'CR\Pay\PayModule:Gopay:payment'
]);

$acl->allowRole('guest', 'gopay_payment_status');
$router->addRoute('gopay_payment_status', '/pay/gopay/payment-status', [
    'controller' => 'CR\Pay\PayModule:Gopay:paymentStatus'
]);

$acl->allowRole('guest', 'gopay_success_payment');
$router->addRoute('gopay_success_payment', '/pay/gopay/success-payment', [
    'controller' => 'CR\Pay\PayModule:Gopay:paymentSuccess'
]);

$acl->allowRole('guest', 'gopay_notify_payment');
$router->addRoute('gopay_notify_payment', '/pay/gopay/notify-payment', [
    'controller' => 'CR\Pay\PayModule:Gopay:notifyPayment'
]);


$acl->allowRole('guest', 'gopay_cancel_subscription');
$router->addRoute('gopay_cancel_subscription', '/pay/gopay/cancel-recurrence', [
    'controller' => 'CR\Pay\PayModule:Gopay:cancelSubscription'
]);

/*
 * Change exist active subscripiton
 */
$acl->allowRole('guest', 'gopay_change_subscription');
$router->addRoute('gopay_change_subscription', '/pay/gopay/change-subscription', [
    'controller' => 'CR\Pay\PayModule:Gopay:changeSubscription'
]);

$acl->allowRole('guest', 'gopay_change_subscription_status');
$router->addRoute('gopay_change_subscription_status', '/pay/gopay/change-subscription-status', [
    'controller' => 'CR\Pay\PayModule:Gopay:changeSubscriptionStatus'
]);


$acl->allowRole('guest', 'gopay_create_recurrence');
$router->addRoute('gopay_create_recurrence', '/pay/gopay/recurrence', [
    'controller' => 'CR\Pay\PayModule:Gopay:createRecurrencePayment'
]);