<?php namespace CR\Pay;

require_once MODUL_DIR . '/Pay/PayModule.php';

use Core\ServiceLayer as ServiceLayer;
$acl = ServiceLayer::getService('acl');

require_once MODUL_DIR . '/notification/senders/Buy.php';
require_once MODUL_DIR . '/notification/senders/model/Buy.php';

require_once MODUL_DIR . '/Pay/lib/InvoiceNumberGenerator.php';
require_once MODUL_DIR . "/Pay/lib/InvoiceTemplate.php";
require_once MODUL_DIR . "/Pay/lib/PackageTransitionType.php";

ServiceLayer::addService('OrderStateManager', [
    'class' => 'CR\Pay\Model\OrderStateManager',
    'params' => [ ServiceLayer::getService('PackageManager') ]
]);

ServiceLayer::addService('UserPackageRepository', [
    'class' => 'CR\Pay\Model\UserPackageRepository',
    'params' => [
        ServiceLayer::getService('PackageManager')
    ]
]);

ServiceLayer::addService('UserPackageManager', [
    'class' => 'CR\Pay\Model\UserPackageManager',
    'params' => [
        ServiceLayer::getService('UserPackageRepository'),
        ServiceLayer::getService('ActualPackageManager')
    ]
]);

ServiceLayer::addService('InvoiceRepository',array(
'class' => "CR\Pay\Model\InvoiceRepository",
	'params' => array(
		new Model\InvoiceStorage('invoices'),
		new \Core\EntityMapper('CR\Pay\Model\Invoice')
	)
));



ServiceLayer::addService('InvoiceManager', [
    'class' => 'CR\Pay\Model\InvoiceManager',
    'params' => [
        ServiceLayer::getService('InvoiceRepository'),
    ]
]);


/*
 * depracted, 
 */
ServiceLayer::addService('OrderDataRepository', [
    'class' => 'CR\Pay\Model\OrderDataRepository',
    'params' => []
]);

/**
 * depracted
 */
ServiceLayer::addService('OrderDataManager', [
    'class' => 'CR\Pay\Model\OrderDataManager',
    'params' => [
        ServiceLayer::getService('OrderDataRepository')
    ]
]);



ServiceLayer::addService('OrderRepository',array(
'class' => "CR\Pay\Model\OrderRepository",
	'params' => array(
		new \CR\Pay\Model\OrderStorage('order_data'),
		new \Core\EntityMapper('CR\Pay\Model\Order')
	)
));

ServiceLayer::addService('OrderManager',array(
'class' => "CR\Pay\Model\OrderManager",
    'params' => array(
		ServiceLayer::getService('OrderRepository'),
	)
));


ServiceLayer::addService('PackageUpdater', [
    'class' => 'CR\Pay\Model\PackageUpdater',
    'params' => [
        ServiceLayer::getService('PackageManager'),
        ServiceLayer::getService('UserPackageRepository'),
        ServiceLayer::getService('security'),
        ServiceLayer::getService('ActiveTeamManager')
    ]
]);

ServiceLayer::addService('PackageTransitionManager', [
    'class' => 'CR\Pay\Model\PackageTransitionManager',
    'params' => [
        ServiceLayer::getService('UserPackageRepository'),
        ServiceLayer::getService('PackageUpdater')
    ]
]);


ServiceLayer::addService('CouponRepository',array(
'class' => "CR\Pay\Model\CouponRepository",
	'params' => array(
		new \Core\DbStorage('coupon'),
		new \Core\EntityMapper('CR\Pay\Model\Coupon')
	)
));


ServiceLayer::addService('CouponManager',array(
'class' => "CR\Pay\Model\CouponManager",
    'params' => array(
		ServiceLayer::getService('CouponRepository'),
	)
));

ServiceLayer::addService('PaymentManager', [
    'class' => 'CR\Pay\Model\PaymentManager',
]);

ServiceLayer::addService('InteoManager', [
    'class' => 'CR\Pay\Model\InteoManager',
]);

$router = ServiceLayer::getService('router');

// PackageController

$acl->allowRole('ROLE_USER', 'pay_package_list');
$router->addRoute('pay_package_list', '/pay/package/list', [
    'controller' => 'CR\Pay\PayModule:Pay:list'
]);

$acl->allowRole('ROLE_USER', 'pay_package_list');
$router->addRoute('pay_package_list', '/pay/package/list', [
    'controller' => 'CR\Pay\PayModule:Pay:list'
]);

$acl->allowRole('ROLE_USER', 'pay_order_package');
$router->addRoute('pay_order_package', '/pay/order-package/:package_id', [
    'controller' => 'CR\Pay\PayModule:Pay:orderPackage'
]);

$acl->allowRole('ROLE_USER', 'pay_package_period');
$router->addRoute('pay_package_period', '/pay/package-period', [
    'controller' => 'CR\Pay\PayModule:Pay:packagePeriod'
]);

$acl->allowRole('ROLE_USER', 'pay_package_update');
$router->addRoute('pay_package_update', '/pay/package-update', [
    'controller' => 'CR\Pay\PayModule:Pay:packageUpdate'
]);

$acl->allowRole('ROLE_USER', 'pay_package_period_update');
$router->addRoute('pay_package_period_update', '/pay/package-period-update', [
    'controller' => 'CR\Pay\PayModule:Pay:packagePeriodUpdate'
]);

$acl->allowRole('ROLE_USER', 'pay_invoice_data');
$router->addRoute('pay_invoice_data', '/pay/invoice-data', [
    'controller' => 'CR\Pay\PayModule:Pay:invoiceData'
]);

$acl->allowRole('ROLE_USER', 'pay_invoice_data_update');
$router->addRoute('pay_invoice_data_update', '/pay/invoice-data-update', [
    'controller' => 'CR\Pay\PayModule:Pay:invoiceDataUpdate'
]);

$acl->allowRole('ROLE_USER', 'pay_package_summary');
$router->addRoute('pay_package_summary', '/pay/package-summary', [
    'controller' => 'CR\Pay\PayModule:Pay:packageSummary'
]);

$acl->allowRole('ROLE_USER', 'pay_package_buy');
$router->addRoute('pay_package_buy', '/pay/package-buy', [
    'controller' => 'CR\Pay\PayModule:Pay:packageBuy'
]);

$acl->allowRole('ROLE_USER', 'pay_package_payment');
$router->addRoute('pay_package_payment', '/pay/package-payment/:order_no', [
    'controller' => 'CR\Pay\PayModule:Pay:packagePayment'
]);

$acl->allowRole('ROLE_USER', 'pay_package_after_payment');
$router->addRoute('pay_package_after_payment', '/pay/package-after-payment/:order_no', [
    'controller' => 'CR\Pay\PayModule:Pay:packageAfterPayment'
]);

$acl->allowRole('ROLE_USER', 'pay_payment_success');
$router->addRoute('pay_payment_success', '/pay/payment-success/:order_no', [
    'controller' => 'CR\Pay\PayModule:Pay:paymentSuccess'
]);

//InvoiceController

$acl->allowRole('ROLE_USER', 'invoice_print_to_pdf');
$router->addRoute('invoice_print_to_pdf', '/invoice/print-to-pdf/:order_no', [
    'controller' => 'CR\Pay\PayModule:Invoice:printToPdf'
]);

$acl->allowRole('ROLE_USER', 'invoice_pdf');
$router->addRoute('invoice_pdf', '/invoice/pdf/:order_no', [
    'controller' => 'CR\Pay\PayModule:Invoice:pdf'
]);

$acl->allowRole('ROLE_USER', 'invoice_send_to_email');
$router->addRoute('invoice_send_to_email', '/invoice/send-to-email', [
    'controller' => 'CR\Pay\PayModule:Invoice:sendToEmail'
]);

$acl->allowRole('ROLE_USER', 'order_list');
$router->addRoute('order_list', '/orders/:team_id', [
    'controller' => 'CR\Pay\PayModule:Orders:list'
]);

$acl->allowRole('ROLE_USER', 'order_detail');
$router->addRoute('order_detail', '/orders/detail/:order_no', [
    'controller' => 'CR\Pay\PayModule:Orders:detail'
]);

$acl->allowRole('ROLE_USER', 'package_list');
$router->addRoute('package_list', '/packages', [
    'controller' => 'CR\Pay\PayModule:Packages:list'
]);

$acl->allowRole('ROLE_USER', 'package_config');
$router->addRoute('package_config', '/package-config', [
    'controller' => 'CR\Pay\PayModule:Packages:packageConfig'
]);

include_once('coupon_routing.php');

if (DEBUG)
{
    $acl->allowRole('ROLE_USER', 'test_sandbox');
    $router->addRoute('test_sandbox', '/test', [
        'controller' => 'CR\Pay\PayModule:Test:sandbox'
    ]);
}

include 'paypal_routing.php';
include 'gopay_routing.php';
include 'cron_routing.php';
include 'test_routing.php';