<?php

$acl->allowRole('guest','test_send_message');
$router->addRoute('test_send_message','/api/test/message/send',array(
		'controller' => 'CR\Message\MessageModule:PushTest:sendMessage'
));

$acl->allowRole('guest','test_create_push_notify');
$router->addRoute('test_create_push_notify','/api/test/create-push-notify',array(
		'controller' => 'CR\Message\MessageModule:PushTest:testCreatePushNotify'
));



