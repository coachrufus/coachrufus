<?php
/**
 * test
 */
namespace CR\Message;
require_once(MODUL_DIR.'/Message/MessageModule.php');


use Core\ServiceLayer as ServiceLayer;


$acl = ServiceLayer::getService('acl');

ServiceLayer::addService('MessageRepository',array(
'class' => "CR\Message\Model\MessageRepository",
	'params' => array(
		new  \CR\Message\Model\MessageStorage('message'),
		new \Core\EntityMapper('CR\Message\Model\Message'),
	)
));

ServiceLayer::addService('MessageManager',array(
'class' => "CR\Message\Model\MessageManager",
	'params' => array(
		ServiceLayer::getService('MessageRepository'),
	)
));

ServiceLayer::addService('SystemNotificationManager',array(
'class' => "CR\Message\Model\SystemNotificationManager",
	'params' => array(
		ServiceLayer::getService('MessageRepository'),
	)
));

ServiceLayer::addService('PushManager',array(
'class' => "CR\Message\Model\PushManager",
	
));


 
//routing
$router = ServiceLayer::getService('router');
include_once('rest_routing.php');
include_once('test_routing.php');
include_once('system_notify_routing.php');
include_once('push_notify_routing.php');


$acl->allowRole('ROLE_USER','message_user_list');
$router->addRoute('message_user_list','/message/team-player/list',array(
		'controller' => 'CR\Message\MessageModule:Default:list'
));

/**
 * @category Routing
 * @example  message/team-player/sent-list list of system notifications
 * /message/team-player/sent-list
 */
$acl->allowRole('ROLE_USER','message_user_sent_list');
$router->addRoute('message_user_sent_list','/message/team-player/sent-list',array(
		'controller' => 'CR\Message\MessageModule:Default:sentList'
));

/**
 * @category Routing
 * /message/team-player/ajax-list-data
 */
$acl->allowRole('ROLE_USER','message_list_ajax_data');
$router->addRoute('message_list_ajax_data','/message/team-player/ajax-list-data',array(
		'controller' => 'CR\Message\MessageModule:Ajax:listData'
));

$acl->allowRole('ROLE_USER','message_sent_list_ajax_data');
$router->addRoute('message_sent_list_ajax_data','/message/team-player/ajax-list-sent-data',array(
		'controller' => 'CR\Message\MessageModule:Ajax:listSentData'
));


$acl->allowRole('ROLE_USER','message_user_create');
$router->addRoute('message_user_create','/message/team-player/create',array(
		'controller' => 'CR\Message\MessageModule:Default:create'
));

$acl->allowRole('ROLE_USER','message_reply');
$router->addRoute('message_reply','/message/team-player/reply',array(
		'controller' => 'CR\Message\MessageModule:Default:create'
));

$acl->allowRole('ROLE_USER','message_delete');
$router->addRoute('message_delete','/message/team-player/delete',array(
		'controller' => 'CR\Message\MessageModule:Default:delete'
));

$acl->allowRole('ROLE_USER','message_read');
$router->addRoute('message_read','/message/team-player/read',array(
		'controller' => 'CR\Message\MessageModule:Default:read'
));
$acl->allowRole('ROLE_USER','message_unread');
$router->addRoute('message_unread','/message/team-player/unread',array(
		'controller' => 'CR\Message\MessageModule:Default:unread'
));
$acl->allowRole('ROLE_USER','message_detail');
$router->addRoute('message_detail','/message/team-player/detail',array(
		'controller' => 'CR\Message\MessageModule:Default:detail'
));

$acl->allowRole('ROLE_USER','message_search_recipient');
$router->addRoute('message_search_recipient','/message/search-recipient',array(
		'controller' => 'CR\Message\MessageModule:Ajax:searchRecipient'
));






