<?php

$acl->allowRole('ROLE_USER','system_notify_list');
$router->addRoute('system_notify_list','/system-notify/list',array(
		'controller' => 'CR\Message\MessageModule:SystemNotify:list'
));

$acl->allowRole('ROLE_USER','system_notify_redirect');
$router->addRoute('system_notify_redirect','/system-notify/redirect',array(
		'controller' => 'CR\Message\MessageModule:SystemNotify:redirect'
));


$acl->allowRole('ROLE_USER','system_notify_change_status');
$router->addRoute('system_notify_change_status','/system-notify/change-status',array(
		'controller' => 'CR\Message\MessageModule:SystemNotify:changeStatus'
));

$acl->allowRole('ROLE_USER','system_notify_cron');
$router->addRoute('system_notify_cron','/system-notify/cron',array(
		'controller' => 'CR\Message\MessageModule:Cron:sendPushNotifyMissingPlayers'
));


$acl->allowRole('guest','rest_system_notify_list');
$router->addRoute('rest_system_notify_list','/api/system-notify/list',array(
		'controller' => 'CR\Message\MessageModule:SystemNotifyRest:list'
));

