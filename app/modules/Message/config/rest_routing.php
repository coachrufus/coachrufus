<?php

$acl->allowRole('guest','rest_messages_list');
$router->addRoute('rest_messages_list','/api/message/list',array(
		'controller' => 'CR\Message\MessageModule:Rest:list'
));

$acl->allowRole('guest','rest_notifications_list');
$router->addRoute('rest_notifications_list','/api/notification/list',array(
		'controller' => 'CR\Message\MessageModule:Rest:systemNotificationlist'
));


$acl->allowRole('guest','rest_messages_detail');
$router->addRoute('rest_messages_detail','/api/message/detail',array(
		'controller' => 'CR\Message\MessageModule:Rest:messageDetail'
));

$acl->allowRole('guest','rest_messages_status');
$router->addRoute('rest_messages_status','/api/message/status',array(
		'controller' => 'CR\Message\MessageModule:Rest:changeStatus'
));

$acl->allowRole('guest','rest_messages_delete');
$router->addRoute('rest_messages_delete','/api/message/delete',array(
		'controller' => 'CR\Message\MessageModule:Rest:delete'
));

$acl->allowRole('guest','rest_messages_delete_all');
$router->addRoute('rest_messages_delete_all','/api/message/deleteAll',array(
		'controller' => 'CR\Message\MessageModule:Rest:deleteAll'
));

