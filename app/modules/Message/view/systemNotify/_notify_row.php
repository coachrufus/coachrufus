<a name="<?php echo $notify->getId() ?>"></a>
<div class="system-notify-row <?php echo $notify->getStatus() ?> "> 
    <div class="sn-cnt">
         <a href="<?php echo $notify->getLink() ?>">
        <strong><?php echo $notify->getSubject() ?></strong>
        <span><?php echo $notify->getBody() ?></span>
         </a>
    </div>
    
       <div class="sn-footer">
            <span class="created-date"><?php echo $notify->getCreatedAt()->format('d.m.Y H:i:s') ?></span>
            <?php if('fullread' == $notify->getStatus()): ?>
                <a class="pull-right" href="<?php echo $router->link('system_notify_change_status',array('status' => 'read','mid' => $notify->getId())) ?>"><?php echo $translator->translate('system_notify.list.mark_as_unread') ?></a>
            <?php else: ?>
                <a class="pull-right" href="<?php echo $router->link('system_notify_change_status',array('status' => 'fread','mid' => $notify->getId())) ?>"><?php echo $translator->translate('system_notify.list.mark_as_read') ?></a>
            <?php endif; ?>
        </div>

</div>