<section class="content">
    <div class="row">
        <div class="col-sm-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3>
                        <i class="ico ico-panel-event"></i> 
                        <?php echo $translator->translate('System notify') ?>
                    </h3>
                </div>
                <div class="panel-body  panel-full-body">
                    <div id="system-notify-cnt">
                        <div  id="team_notify_panel">
                                <?php foreach ($list->getItems() as $notify): ?>
                                   <?php $layout->includePart(MODUL_DIR . '/Message/view/systemNotify/_notify_row.php',array('notify' => $notify)) ?>
                                <?php endforeach; ?>
                        </div>
                    </div>
                    
                     <?php if (!$list->allItemsLoaded()): ?>
                            <a class="btn btn-primary load-notifications" data-start="<?php echo $notify->getId() ?>"  href="<?php echo $router->link('rest_system_notify_list') ?>" ><?php echo $translator->translate('Load More') ?></a>
                            <?php endif ?>
                </div>
            </div>
        </div>
          <div class="col-md-4">
            <?php $layout->includePart(MODUL_DIR . '/Event/view/widget/_small_calendar.php') ?>
        </div>
    </div>
</section>



<?php $layout->includePart(MODUL_DIR . '/Scouting/view/default/_remove_modal.php') ?>




<?php $layout->addJavascript('js/SmallCalendar.js') ?>
<?php $layout->addJavascript('js/SystemNotifyManager.js') ?>
<?php $layout->startSlot('javascript') ?>

<script type="text/javascript">
     var small_calendar = new SmallCalendar({
        'dataUrl': '<?php echo $router->link('rest_event_get_month_grid') ?>',
        'localityManager': new LocalityManager()
    });
    small_calendar.initLoad();
    

    var notify_manger = new SystemNotifyManager({
        'dataUrl': '<?php echo $router->link('rest_system_notify_list') ?>'
    });
    notify_manger.bindTriggers();
    

</script>
<?php $layout->endSlot('javascript') ?>