<section class="content-header">
    <h1>
        <?php echo $translator->translate('Read Mail') ?>
    </h1>


</section>
<section class="content">
     <div class="col-md-3">
             <?php $layout->includePart(MODUL_DIR.'/Message/view/default/_left_panel.php',array('info' => $info,'active' => '')) ?>
            </div><!-- /.col -->
    <div class="col-md-9">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="mailbox-read-info">
                    <h3><?php echo $message->getSubject() ?></h3>
                    <h5><?php echo $translator->translate('From') ?>: <?php echo $message->getSenderName()  ?> (<?php echo $message->getSenderEmail()  ?>) <span class="mailbox-read-time pull-right"><?php echo $message->getCreatedAt()->format('d.m.Y H:i:s')  ?></span></h5>
                  </div><!-- /.mailbox-read-info -->
                  <div class="mailbox-controls with-border text-center">
                      
                      
                    <?php if($message->getRecipientId() == $user->getId()): ?>
                    <a data-toggle="tooltip" data-placement="top" title="<?php echo $translator->translate('Reply') ?>" href="<?php echo $router->link('message_reply',array('r' => $message->getId())) ?>" class="btn btn-default btn-sm"><i class="fa fa-reply"></i></a>
                    <?php endif; ?>
                    
                    <a data-toggle="tooltip" data-placement="top" title="<?php echo $translator->translate('Delete message') ?>" href="<?php echo $router->link('message_delete',array('mid' => $message->getId())) ?>" class="btn btn-default btn-sm remove-message-trigger"><i class="fa  fa-trash-o"></i></a>
                  
                  </div><!-- /.mailbox-controls -->
                  <div class="mailbox-read-message">
                    <?php echo $message->getBody() ?>
                  </div><!-- /.mailbox-read-message -->
            </div>
        </div>
    </div>
</section>

 <!-- Modal -->
<div class="modal fade" id="removeMessageModal" tabindex="-1" role="dialog" aria-labelledby="<?php echo $translator->translate('Remove message') ?>">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?php echo $translator->translate('Remove message') ?></h4>
            </div>
            <div class="modal-body">
                <?php echo $translator->translate('Are you sure?') ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $translator->translate('Cancel') ?></button>
                <button type="button" class="btn btn-danger modal_submit"><?php echo $translator->translate('Confirm') ?></button>
            </div>
        </div>
    </div>
</div>

<?php $layout->startSlot('javascript') ?>
<script type="text/javascript">
    //var delete_url;
    $('.remove-message-trigger').on('click',function(e){
        e.preventDefault();
        $('#removeMessageModal .modal_submit').attr('data-href',$(this).attr('href'));
        $('#removeMessageModal').modal('show');
       
   });
   
   $('#removeMessageModal .modal_submit').on('click',function(){
       location.href=$(this).attr('data-href');
   });
</script>
<?php $layout->endSlot('javascript') ?>