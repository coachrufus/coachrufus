 <section class="content-header">
    <h1>
        <?php echo $translator->translate('My messages') ?>
    </h1>
    
   
</section>
<section class="content">
    
<div class="col-md-3">
    <?php $layout->includePart(MODUL_DIR.'/Message/view/default/_left_panel.php',array('info' => $info,'active' => 'inbox') ) ?>
</div><!-- /.col -->

<div class="col-md-9">
              <div class="panel panel-default">
                  <div class="panel-heading">
                      <h3 ><?php echo $translator->translate('Inbox') ?></h3>
                    <div class="mailbox-controls">
                    <!-- Check all button -->
                   
                     
                  </div>
                </div>
                  
             
                <div class="panel-body">
                    
                  <div class="mailbox-messages">
                    <table class="table table-hover table-striped" id="message-table">
                        <thead>
                        <tr>
                            <th></th>
                            <th><?php echo $translator->translate('Sender') ?></th>
                            <th><?php echo $translator->translate('Subject') ?></th>
                            <th><?php echo $translator->translate('Date') ?></th>
                            <th></th>
                        </tr>
                    </thead>
                      <tbody>
                         
                          
                       
                      </tbody>
                    </table><!-- /.table -->
                  </div><!-- /.mail-box-messages -->
                </div><!-- /.box-body -->
               
              </div><!-- /. box -->
            </div><!-- /.col -->
</section>         

 <!-- Modal -->
<div class="modal fade" id="removeMessageModal" tabindex="-1" role="dialog" aria-labelledby="<?php echo $translator->translate('Remove message') ?>">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?php echo $translator->translate('Remove message') ?></h4>
            </div>
            <div class="modal-body">
                <?php echo $translator->translate('Are you sure?') ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $translator->translate('Cancel') ?></button>
                <button type="button" class="btn btn-danger modal_submit"><?php echo $translator->translate('Confirm') ?></button>
            </div>
        </div>
    </div>
</div>

<?php echo $layout->addStylesheet('plugins/datatables/dataTables.bootstrap.css') ?>
<?php  $layout->addJavascript('plugins/datatables/jquery.dataTables.min.js')  ?>
<?php  $layout->addJavascript('plugins/datatables/dataTables.bootstrap.min.js')  ?>
 <?php  $layout->addJavascript('plugins/datatables/'.LANG.'.js')  ?>
 
 
<?php $layout->startSlot('javascript') ?>
<script type="text/javascript">
    //var delete_url;
    $(document).on('click','.remove-message-trigger',function(e){
        e.preventDefault();
        e.stopPropagation();
        $('#removeMessageModal .modal_submit').attr('data-href',$(this).attr('href'));
        $('#removeMessageModal').modal('show');
       
   });
   
   $('#removeMessageModal .modal_submit').on('click',function(){
       location.href=$(this).attr('data-href');
   });
   
     //var datatables_trans = new datatablesTrans();
        $("#message-table").DataTable({
            "scrollX": true,
        "columns": [
                { "orderable": false, "name": "id"  },
                { "name": "sender_name" },
                { "name": "subject" },
                { "name": "created_at" },
                { "orderable": false  }
              ],
             "searching": false,
             "order": [[ 3, "desc" ]],
             "iDisplayLength": 10,
             "language": datatablesTrans,
             "processing": true,
            "serverSide": true,
            "ajax": "<?php echo $router->link('message_list_ajax_data') ?>"
        });
       
       $(document).on('click','#message-table .dt_row_trigger',function(){
		var $id = $(this).attr('id');	
		var $id_parts = $id.split('_');
		location.href = '/message/team-player/detail?mid='+$id_parts[1];
	})
   
   
</script>
<?php $layout->endSlot('javascript') ?>