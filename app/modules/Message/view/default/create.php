 <section class="content-header">
    <h1>
        <?php echo $translator->translate('My messages') ?>
    </h1>
    
   
</section>
<section class="content">
    
    
     <div class="col-md-3">
             <?php $layout->includePart(MODUL_DIR.'/Message/view/default/_left_panel.php',array('info' => $info,'active' => '') ) ?>
            </div><!-- /.col -->
            
<div class="col-md-9">
<form action="#" method="post">

              <div class="panel panel-default">

        <div class="panel-body">
            <div class="row">
            <?php echo  $validator->showAllErrors() ?>

                   
             <div class="form-group  autocomplete_container col-sm-12">
                  <label class="control-label"><?php echo $translator->translate('Recipient') ?></label>
            <div class="label-badget-autocomplete">
                <div class="col-sm-12 recipient-list">
                    <?php foreach($defaultRecipients as $defaultRecipient): ?>
                                <span class="label label-info label-message-recipient">
                                    <i class="fa fa-close"></i>
                                    <input type="hidden" name="recipients[]" value="<?php echo $defaultRecipient->getId()  ?>"> <?php echo $defaultRecipient->getFullName() ?>
                                </span>
                            <?php endforeach; ?>
                    
                    <input type="text" name="search" value="" autocomplete="off" class="form-control mesage-recipient-find" id="record_email" placeholder="<?php echo $translator->translate('Type name or email') ?>">
                    
                     <div class="autocomplete_wrap">
                            <ul class="autocomplete_list">

                            </ul>
                        </div>
                </div>
              
            </div>
            </div>
                
         
            
            
            
            <div class="form-group col-sm-12">
                 <?php echo $form->renderInputTag('subject',array('class' => 'form-control', 'placeholder' => 'Subject:')) ?>
            </div>
            <div class="form-group col-sm-12">
                <textarea name="message" rows="5" cols="5" class="form-control" id="compose-textarea" style="height: 300px;"><?php echo $form->getFieldValue('body')  ?></textarea>

               
            </div>
            </div>
        </div><!-- /.box-body -->
        <div class="box-footer">
            <div class="pull-right">
                
                <button type="submit" class="btn btn-primary"><i class="fa fa-envelope-o"></i> <?php echo $translator->translate('Send') ?></button>
            </div>
            
        </div><!-- /.box-footer -->
    </div><!-- /. box -->

</form>
    </div><!-- /.col -->
</section>
<?php $layout->addStylesheet('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') ?>
<?php $layout->addJavascript('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') ?>
<?php $layout->addJavascript('plugins/ckeditor/ckeditor.js') ?>
<?php $layout->addJavascript('js/MessageManager.js') ?>
<?php  $layout->startSlot('javascript') ?>
<script type="text/javascript">
     $('#compose-textarea').focus();
      
      var message_manger = new MessageManager();
      message_manger.bindTriggers();
    </script>

    <?php  $layout->endSlot('javascript') ?>