<a data-toggle="tooltip" data-placement="top" title="<?php echo $translator->translate('Create new message') ?>"  href="<?php echo $router->link('message_user_create') ?>" class="btn btn-primary btn-block margin-bottom"><?php echo $translator->translate('Compose') ?></a>


<div class="box box-solid">
    <div class="box-header with-border">
        <h3 class="box-title"><?php echo $translator->translate('Folders') ?></h3>
        <div class="box-tools">
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div class="box-body no-padding">
        <ul class="nav nav-pills nav-stacked">
            <li <?php echo ($active == 'inbox') ? 'class="active"' : '' ?>><a href="<?php echo $router->link('message_user_list') ?>"><i class="fa fa-inbox"></i> <?php echo $translator->translate('Inbox') ?> <span class="label label-primary pull-right"><?php echo $info['unread'] ?></span></a></li>
            <li <?php echo ($active == 'sent') ? 'class="active"' : '' ?>><a href="<?php echo $router->link('message_user_sent_list') ?>"><i class="fa fa-envelope-o"></i> <?php echo $translator->translate('Sent') ?></a></li>
           
            <!--<li><a href="#"><i class="fa fa-trash-o"></i> Trash</a></li>-->
        </ul>
    </div><!-- /.box-body -->
</div><!-- /. box -->
