<?php
namespace CR\Message\Controller;

use Core\ControllerResponse;
use Core\ServiceLayer;
use Core\RestApiController;



class RestController extends RestApiController {

   private function buildDataFromEntity($message)
   {
        $messageManager = ServiceLayer::getService('MessageManager');
       $replyGroup = $message->getReplyGroup();
        $replyGroupMessages =  $messageManager->getReplyGroupMessages($replyGroup);
        if(count($replyGroupMessages)>1)
        {
           foreach($replyGroupMessages as $replyGroupMessage)
           {
             $group[] = $replyGroupMessage->getRecipientName();
           }
           $to = implode(', ', $group);
        }
        else
        {
            $to = $message->getRecipientName();
        }
       
        
       
       
        $data = array();
        $data['id'] = $message->getId();
        $data['date'] = $message->getCreatedAt()->getTimestamp();
        $data['subject'] = $message->getSubject();
        $data['body'] = ($message->getRestBody() == null) ? $message->getBody() : $message->getRestBody();
        $data['status'] = $message->getStatus();
        $data['from'] = $message->getSenderName();
        $data['to'] = $to;
        $data['type'] = $message->getType();
        $bodyData = unserialize($message->getBodyData());
        
        $data['data'] = ($bodyData == false) ? null : $bodyData;
        $data['sender_icon'] = null;
        if(null != $message->getReplyId())
        {
             $sender = ServiceLayer::getService('user_repository')->find($message->getReplyId());
             $data['sender_icon'] = WEB_DOMAIN.$sender->getMidPhoto();
        }

        return $data;
   }
   
   public function systemNotificationlistAction()
   {
        $request= $this->getRequest();
        $result = array();
        $result['params'] = $this->getRequestParams();
        if(false == $this->checkApiKey())
        {
            return $this->unvalidApiKeyResult();
        }
        
        $messageManager = ServiceLayer::getService('SystemNotificationManager');
        $user = ServiceLayer::getService('security')->getIdentityProvider()->findUserById($request->get('user_id'));
        

        $criteria = array();
        $criteria['from'] = (null ==  $request->get('start')) ? 0 : $request->get('start');
        $criteria['length'] =  ($request->get('length') == null) ? 10 : $request->get('length');
        $criteria['order_col'] = ($request->get('order_index') == null) ? 'id' :  $request->get('order_index');
        $criteria['order_dir'] = ($request->get('order_direction') == null) ? 'ASC' : $request->get('order_direction');
        
        $list = $messageManager->getSystemNotifications($user,$criteria);
        $info = $messageManager->getNotificationsInfo($user);
        
        $result['total'] = $info['total'];
        $result['read'] = $info['read'];
        $result['unread'] = $info['unread'];
        $messagesData = array();
        foreach($list as $message)
        {
             $messagesData[$message->getId()] = $this->buildDataFromEntity($message);

        }
        
        $result['messages'] = $messagesData;
        return $this->asJson($result);  
   }
    
    public function listAction()
   {
        $request= $this->getRequest();
        $result = array();
        $result['params'] = $this->getRequestParams();
        if(false == $this->checkApiKey())
        {
            return $this->unvalidApiKeyResult();
        }
        
        $messageManager = ServiceLayer::getService('MessageManager');
        $user = ServiceLayer::getService('security')->getIdentityProvider()->findUserById($request->get('user_id'));
        

        $criteria = array();
        $criteria['from'] = (null ==  $request->get('start')) ? 0 : $request->get('start');
        $criteria['length'] =  ($request->get('length') == null) ? 10 : $request->get('length');
        $criteria['order_col'] = ($request->get('order_index') == null) ? 'id' :  $request->get('order_index');
        $criteria['order_dir'] = ($request->get('order_direction') == null) ? 'ASC' : $request->get('order_direction');
        
        $list = $messageManager->getRecipientMessages($user,$criteria);
        $info = $messageManager->getRecipientMessagesInfo($user);

        
        $result['total'] = $info['total'];
        $result['read'] = $info['read'];
        $result['unread'] = $info['unread'];
        $messagesData = array();
        foreach($list as $message)
        {
             $messagesData[$message->getId()] = $this->buildDataFromEntity($message);

        }
        
        $result['messages'] = $messagesData;
        return $this->asJson($result);  
   }
   
   public function messageDetailAction()
   {
        $request= $this->getRequest();
        $result = array();
         $result['params'] = $this->getRequestParams();
        if(false == $this->checkApiKey())
        {
            return $this->unvalidApiKeyResult();
        }
        
        $messageManager = ServiceLayer::getService('MessageManager');
        $message = $messageManager->getMessageById($request->get('id'));
        
        $replyGroup = $message->getReplyGroup();
        $replyGroupMessages =  $messageManager->getReplyGroupMessages($replyGroup);
        if(count($replyGroupMessages)>1)
        {
           foreach($replyGroupMessages as $replyGroupMessage)
           {
             $group[] = $replyGroupMessage->getRecipientName();
           }
           $to = implode(', ', $group);
        }
        else
        {
            $to = $message->getRecipientName();
        }
        $messageData = $this->buildDataFromEntity($message);
        $result['message'] = $messageData;
        return $this->asJson($result);  
   }
   
   public function changeStatusAction()
   {

      
        $request= $this->getRequest();
        $result = array();
        $result['params'] = $this->getRequestParams();
        if(false == $this->checkApiKey())
        {
            return $this->unvalidApiKeyResult();
        }
        $messageManager = ServiceLayer::getService('MessageManager');
        
        $ids = $request->get('ids');
        
        foreach($ids as $id)
        {
              $message = $messageManager->getMessageById($id);
                $status = $request->get('status');

                if('read' == $status)
                {
                     $messageManager->readMessage($message);
                }

                if('unread' == $status)
                {
                     $messageManager->unreadMessage($message);
                }
                
                $message = $messageManager->getMessageById($id);
                $result['new_status'][$id] = $message->getStatus();
        }
        
        
      
        
         return $this->asJson($result);  
   }
   
   public function deleteAction()
   {
        $request= $this->getRequest();
        $result = array();
         $result['params'] = $this->getRequestParams();
        $result['status'] = 'success';
        if(false == $this->checkApiKey())
        {
            return $this->unvalidApiKeyResult();
        }
        $messageManager = ServiceLayer::getService('MessageManager');
        
        $ids = $request->get('ids');
        
        foreach($ids as $id)
        {
             $message = $messageManager->getMessageById($id);
             if(null == $message )
            {
                  $result['status'] = 'error';
                  $result['error_message'][$id] = 'Message with id '.$id.' not exist';
            }
            else
            {
                 $messageManager->deleteMessage($message);
                 $result['deleted'][$id] = 'deleted';
                
            }
        }

         return $this->asJson($result); 
   }       
   
   public function deleteAllAction()
   {
        /*
       $request= $this->getRequest();
        $result = array();
        if(false == $this->checkApiKey())
        {
            return $this->unvalidApiKeyResult();
        }
        $messageManager = ServiceLayer::getService('MessageManager');
        $user = ServiceLayer::getService('security')->getIdentityProvider()->findUserById($request->get('user_id'));
        $messageManager->deleteAllUserMessages($user);
        
       $result['status'] = 'success';

         return $this->asJson($result); 
         * 
         */
   }       
   
   

}
