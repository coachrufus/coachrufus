<?php
namespace CR\Message\Controller;

use Core\Controller;
use Core\ServiceLayer;
use Core\Validator;
use CR\Message\Form\MessageForm;
use CR\Message\Model\Message;



class DefaultController extends Controller {

    public function detailAction()
    {
        
         $request = ServiceLayer::getService('request');
         $manager = ServiceLayer::getService('MessageManager');
         $message = $manager->getMessageById($request->get('mid'));
        
         $this->checkRecipientMessageOwner($message);
         $manager->readMessage($message);
         $user = ServiceLayer::getService('security')->getIdentity()->getUser();
          $info = $manager->getRecipientMessagesInfo($user);
          return $this->render('CR\Message\MessageModule:default:detail.php', array(
                   'message' => $message,
              'info' =>$info,
                  'user' => $user
        ));
    }
    
    public function listAction()
    {
        $user = ServiceLayer::getService('security')->getIdentity()->getUser();
        $messageManager = ServiceLayer::getService('MessageManager');
        $info = $messageManager->getRecipientMessagesInfo($user);
        return $this->render('CR\Message\MessageModule:default:list.php', array(
                'info' => $info
        ));
    }
    
    public function sentListAction()
    {
        $user = ServiceLayer::getService('security')->getIdentity()->getUser();
        $messageManager = ServiceLayer::getService('MessageManager');
        $info = $messageManager->getRecipientMessagesInfo($user);
        return $this->render('CR\Message\MessageModule:default:sentList.php', array(
                'info' => $info
        ));
    }

    public function replyAction()
    {
        return $this->createAction();
    }

    public function createAction()
    {
        $request = ServiceLayer::getService('request');
        $router = ServiceLayer::getService('router');
        $translator = ServiceLayer::getService('translator');
        $manager = ServiceLayer::getService('MessageManager');
        $playerManager = ServiceLayer::getService('PlayerManager');
        $user = ServiceLayer::getService('security')->getIdentity()->getUser();
        $info = $manager->getRecipientMessagesInfo($user);
        
        $message = new Message();
        $form = new MessageForm();
        $form->setEntity($message);

        $validator = new Validator();
        $validator->setRules($form->getFields());
        $recipients =  $request->get('recipients');
       
        
        $defaultRecipients = array();
        if(null != $request->get('r'))
        {
             $replyMessage = $manager->getMessageById($request->get('r'));
             $player = $playerManager->findPlayerById($replyMessage->getReplyId());
             $defaultRecipients[] = $player;
             $form->setFieldValue('subject', 'RE:'.$replyMessage->getSubject());
             
             $replyBody = "\n\n\n".$replyMessage->getBody();
             /*
             $array = explode( "\n", wordwrap( $replyMessage->getBody(), 20));
             $replyBody = "\n >> ".implode("\n >> ",$array);
             */
             
             $form->setFieldValue('body', $replyBody);
        }
       
        if(null != $request->get('ra'))
        {
             $replyMessage = $manager->getMessageById($request->get('ra'));
             $form->setFieldValue('subject', 'RE:'.$replyMessage->getSubject());
             
             $replyBody = "\n\n\n".$replyMessage->getBody();
             $form->setFieldValue('body', $replyBody);
             
             $player = $playerManager->findPlayerById($replyMessage->getReplyId());
             $defaultRecipients[] = $player;
             
             
             $replyGroup = $replyMessage->getReplyGroup();
             $replyGroupMessages =  $manager->getReplyGroupMessages($replyGroup);
             foreach($replyGroupMessages as $replyGroupMessage)
             {
                $player = $playerManager->findPlayerById($replyGroupMessage->getRecipientId());
                if($player->getId() != $user->getId())
                {
                     $defaultRecipients[] = $player;
                }
             }
        }
       
       
        
        if ('POST' == $request->getMethod())
        {
            $post_data = $request->get('record');
            $post_data['body'] = strip_tags($request->get('message',false),'<b><i><u><small><a><blockquote><ul><li><small><img>');
            $form->bindData($post_data);
            $validator->setData($post_data);
            $validator->validateData();

            $pushManager = ServiceLayer::getService('PushManager');
            if (!$validator->hasErrors())
            {
                $entity = $form->getEntity();
                $replyGroup = md5(time().$user->getId());
                foreach($recipients as $recipient)
                {
                    $player = $playerManager->findPlayerById($recipient);

                    //sender message
                    $message = new Message();
                    $message->setBody($entity->getBody());
                    $message->setCreatedAt(new \DateTime());
                    $message->setStatus('unread');
                    $message->setSubject($entity->getSubject());
                    $message->setRecipientName($player->getFullName());
                    $message->setRecipientEmail($player->getEmail());
                    $message->setSenderId($user->getId());
                    $message->setSenderName($user->getFullname());
                    $message->setSenderEmail($user->getEmail());
                    $manager->saveMessage($message);

                    //recipient message
                    $message = new Message();
                    $message->setBody($entity->getBody());
                    $message->setCreatedAt(new \DateTime());
                    $message->setStatus('unread');
                    $message->setSubject($entity->getSubject());
                    
                    $message->setRecipientName($player->getFullName());
                    $message->setRecipientEmail($player->getEmail());
                    $message->setRecipientId($player->getId());
                    $message->setReplyId($user->getId());
                    $message->setSenderName($user->getFullname());
                    $message->setSenderEmail($user->getEmail());
                  
                    $message->setReplyGroup($replyGroup);
                    $mid = $manager->saveMessage($message);
                    
                    $notifyData = array();
                    $notifyData['lang'] = LANG;
                    $notifyData['sender_name'] = $message->getSenderName();
                    $notifyData['email'] =  $message->getRecipientEmail();
                    $notifyData['mid'] = $mid;
                    $notificationManger = ServiceLayer::getService('notification_manager');
                    $notificationManger->sendMessageNewEmail($notifyData);
                    
                    //send push notification
                     $pushManager->sendNewMessageNotify($user,$player);
                }
                
                
                
                $request->addFlashMessage('message_create_success', $translator->translate('message.create.success'));
                $request->redirect($router->link('message_user_list'));
            }

        }

       
        return $this->render('CR\Message\MessageModule:default:create.php', array(
                    'form' => $form,
                    'request' => $request,
                    'validator' => $validator,
                    'defaultRecipients' => $defaultRecipients,
                    'info' => $info
        ));
    }
    
    public function deleteAction()
    {
         $request = ServiceLayer::getService('request');
         $router = ServiceLayer::getService('router');
         $manager = ServiceLayer::getService('MessageManager');
         $playerManager = ServiceLayer::getService('TeamPlayerManager');
         $user = ServiceLayer::getService('security')->getIdentity()->getUser();
         
         
         if(null != $request->get('sid'))
         {
              $message = $manager->getMessageById($request->get('sid'));
              $this->checkSenderMessageOwner($message);
              $manager->deleteMessage($message);
              $request->redirect($router->link('message_user_sent_list'));
         }
         
         
         if(null != $request->get('mid'))
         {
              $message = $manager->getMessageById($request->get('mid'));
              $this->checkRecipientMessageOwner($message);
              $manager->deleteMessage($message);
              $request->redirect($router->link('message_user_list'));
         }
         

         
         
    }
    
    public function readAction()
    {
         $router = ServiceLayer::getService('router');
         $request = ServiceLayer::getService('request');
         $manager = ServiceLayer::getService('MessageManager');
         $message = $manager->getMessageById($request->get('mid'));
         
         $this->checkRecipientMessageOwner($message);
         $manager->readMessage($message);
         $request->redirect($router->link('message_user_list'));
         
    }
    
    public function unreadAction()
    {
         
         $router = ServiceLayer::getService('router');
         $request = ServiceLayer::getService('request');
         $manager = ServiceLayer::getService('MessageManager');
         $message = $manager->getMessageById($request->get('mid'));
         
         $this->checkRecipientMessageOwner($message);
         $manager->unreadMessage($message);
         $request->redirect($router->link('message_user_list'));
         
    }
    
    public function checkRecipientMessageOwner($message)
    {
         $user = ServiceLayer::getService('security')->getIdentity()->getUser();
         if($message->getRecipientId() != $user->getId() && $message->getSenderId() != $user->getId() )
         {
              throw  new \AclException();
         }
       
    }
    
    public function checkSenderMessageOwner($message)
    {
         $user = ServiceLayer::getService('security')->getIdentity()->getUser();
         if($message->getSenderId() != $user->getId()  )
         {
           
              throw  new \AclException();
         }
       
    }

}
