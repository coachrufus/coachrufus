<?php

namespace CR\Message\Controller;

use Core\Controller;
use Core\ServiceLayer;
use Core\Validator;
use CR\Message\Form\MessageForm;
use CR\Message\Model\Message;
use Core\RestApiController;
/**
 * System notify controller
 */
class SystemNotifyRestController  extends RestApiController  {

   
    public function listAction()
    {
        $user = ServiceLayer::getService('security')->getIdentity()->getUser();
        $manager = ServiceLayer::getService('SystemNotificationManager');
        $list = $manager->getNoticationList($user,array('start' => $this->getRequest()->get('from'),'length' => 15));
        $lines = array();
          foreach($list->getItems() as $notify)
        {
          ob_start();
                ServiceLayer::getService('layout')->includePart(MODUL_DIR.'/Message/view/systemNotify/_notify_row.php',array('notify' => $notify ));
               $lines[] = ob_get_contents();
          ob_end_clean();
        }
        
       $result = array('result' => 'SUCCESS','lines' =>  $lines,'start' => $notify->getId(),'allItemsLoaded' => $list->allItemsLoaded());
      return $this->asJson($result);
    }
    

}

