<?php
namespace CR\Message\Controller;

use Core\ControllerResponse;
use Core\ServiceLayer;
use Core\RestController;



class AjaxController extends RestController {

     private function setupTemplate()
    {
        $layout = ServiceLayer::getService('layout');
        $layout->setTemplate(null);
    }
    
    
    
    
    public function listSentDataAction()
    {
        $request = $this->getRequest();
        $router = $this->getRouter();
        $translator = $this->getTranslator();
        $messageManager = ServiceLayer::getService('MessageManager');
        $user = ServiceLayer::getService('security')->getIdentity()->getUser();
        
      
        
        $start = (null ==  $request->get('start')) ? 0 : $request->get('start');
        $itemsPerPage =  $request->get('length');
        
        $orderData = $request->get('order');
        $columns = $request->get('columns');
        $criteria = array();
        $criteria['from'] = $start;
        $criteria['length'] = $itemsPerPage;
        $criteria['order_col'] =$columns[$orderData[0]['column']]['name'] ;
        $criteria['order_dir'] = $orderData[0]['dir'];
        
        $list = $messageManager->getSenderMessages($user,$criteria);
        $info = $messageManager->getSenderMessagesInfo($user);

        $data = array(
            'recordsTotal' => (int)$info['total'],
            'recordsFiltered' => (int)$info['total']
        );
        $data['data'] = array() ;
        
        foreach($list as $message)
        {
            $row = array();
            $row['DT_RowClass'] = 'dt_row_trigger';
            $row['DT_RowId'] = 'row_'.$message->getId();
            $row[] = $message->getRecipientName();
            $row[] =  $message->getSubject();
            $row[] = $message->getCreatedAt()->format('d.m.Y H:i:s');
            $row[] =  '<a class="btn btn-default btn-sm remove-message-trigger" href="'.$router->link('message_delete',array('sid' => $message->getId())).'"><i class="fa  fa-trash-o"></i></a>';
           
            $data['data'][] = $row;

        }
        
       return $this->asJson($data, $status = 'success');

    }
    
    public function listDataAction()
    {
        $request = $this->getRequest();
        $router = $this->getRouter();
        $translator = $this->getTranslator();
        $messageManager = ServiceLayer::getService('MessageManager');
        $user = ServiceLayer::getService('security')->getIdentity()->getUser();
        
      
        
        $start = (null ==  $request->get('start')) ? 0 : $request->get('start');
        $itemsPerPage =  $request->get('length');
        
        $orderData = $request->get('order');
        $columns = $request->get('columns');
        $criteria = array();
        $criteria['from'] = $start;
        $criteria['length'] = $itemsPerPage;
        $criteria['order_col'] =$columns[$orderData[0]['column']]['name'] ;
        $criteria['order_dir'] = $orderData[0]['dir'];
        
        $list = $messageManager->getRecipientMessages($user,$criteria);
        $info = $messageManager->getRecipientMessagesInfo($user);
        
        
        $data = array(
            'recordsTotal' => (int)$info['total'],
            'recordsFiltered' => (int)$info['total']
        );
        $data['data'] = array() ;
         foreach($list as $message)
        {
             $replyButton =  '<a data-toggle="tooltip" data-placement="top" title="'.$translator->translate('Reply').'" href="'.$router->link('message_reply',array('r' => $message->getId())).'" class="btn btn-default btn-sm"><i class="fa fa-reply"></i></a>';
             $replyAllButton =  '<a data-toggle="tooltip" data-placement="top" title="'.$translator->translate('Reply All').'" href="'.$router->link('message_reply',array('ra' => $message->getId())).'" class="btn btn-default btn-sm"><i class="fa fa-reply-all"></i></a>';
             
            $row = array();
            $row['DT_RowClass'] = 'dt_row_trigger message_status_'.$message->getStatus();
            $row['DT_RowId'] = 'row_'.$message->getId();
            $row[] =  $replyAllButton.''.$replyButton;
            $row[] = $message->getSenderName();
            $row[] =  $message->getSubject();
            $row[] = $message->getCreatedAt()->format('d.m.Y H:i:s');
            $row[] =  '<a class="btn btn-default btn-sm remove-message-trigger" href="'.$router->link('message_delete',array('mid' => $message->getId())).'"><i class="fa  fa-trash-o"></i></a>';
           
            $data['data'][] = $row;

        }
       
      
        return $this->asJson($data, $status = 'success');

    }
    
    public function searchRecipientAction()
    {
        $this->setupTemplate();
        $user = ServiceLayer::getService('security')->getIdentity()->getUser();
        
        $playerManager = ServiceLayer::getService('PlayerManager');
        $players = $playerManager->fulltextFindPlayer($this->getRequest()->get('phrase'),'messages');
        
        
        $result = array();
        foreach($players as $player)
        {
            $playerData = $playerManager->convertEntityToArray($player);
            $playerData['mid_photo'] =  $player->getMidPhoto();
            $result[] =$playerData;
        }

        $response = new ControllerResponse();
        $response->setStatus('success');
        $response->setContent(json_encode($result));
        $response->setType('json');
        return $response;
    }

}
