<?php

namespace CR\Message\Controller;

use Core\Controller;
use Core\ServiceLayer;
use Core\Validator;
use CR\Message\Form\MessageForm;
use CR\Message\Model\Message;

/**
 * System notify controller
 */
class SystemNotifyController extends Controller {

    /**
     * Display list of notifications
     * @global Route /system-notify/list
     */
    public function listAction()
    {
        $user = ServiceLayer::getService('security')->getIdentity()->getUser();
        $manager = ServiceLayer::getService('SystemNotificationManager');
        $list = $manager->getNoticationList($user,array('length' => 15));
        
         $request= $this->getRequest();
         if(null != $request->get('mid'))
         {
              $message = $manager->getMessageById($request->get('mid'));
              $message->setStatus('read');
              $manager->saveMessage($message);
         }
        
        
        return $this->render('CR\Message\MessageModule:systemNotify:list.php', array(
                'list' => $list
        ));
    }
    
    /**
     * Display detail of notifications
     * @global Route /system-notify/detail
     */
    public function detailAction()
    {
        
    }
    
    public function redirectAction()
    {
        $manager = ServiceLayer::getService('SystemNotificationManager');
        $request= $this->getRequest();
        
        $notify = $manager->getMessageById($request->get('mid'));
        $notify->setStatus('fullread');
        $manager->saveMessage($notify);
         
        $redirectLink = $notify->getLink();
        $request->redirect($redirectLink);
        
        
    }
    
    public function changeStatusAction()
    {
        $user = ServiceLayer::getService('security')->getIdentity()->getUser();
        $manager = ServiceLayer::getService('SystemNotificationManager');
        $request= $this->getRequest();
        $newStatus = null;
        if('read' == $request->get('status'))
        {
            $newStatus = 'read';
        }
        
        
        if('fread' == $request->get('status'))
        {
            $newStatus = 'fullread';
        }
       
        
        if(null != $newStatus)
        {
            if('a' == $request->get('mid'))
            {
                foreach($manager->getUnreadSystemNotifications($user,1000000) as $notify)
                {
                    $notify->setStatus($newStatus);
                    $manager->saveMessage($notify);
                }
            }
            else
            {
                $notify = $manager->getMessageById($request->get('mid'));
                if($notify->getRecipientId() == $user->getId())
                {
                    $notify->setStatus($newStatus);
                    $manager->saveMessage($notify);
                }
                else
                {
                    throw  new \AclException();
                }
            }
            
            
        }
        
        $request->redirect();
        
    }

}
