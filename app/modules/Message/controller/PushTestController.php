<?php
namespace CR\Message\Controller;

use Core\Controller;
use Core\ServiceLayer;
use Core\Validator;
use CR\Message\Form\MessageForm;
use CR\Message\Model\Message;



class PushTestController extends Controller {

    public function listAction()
    {
        $request = ServiceLayer::getService('request');
         $list = array(
             'hp_wall_post' => array('name' => 'upozornenie na nový príspevok' ,'action' => 'hpWallPostTest'),
             'create_player' => array('name' => 'nový člen v tíme' ,'action' => 'newPlayerTest'),
             'team_invitation' => array('name' => 'pozvánka do tímu' ,'action' => 'teamInvitationTest'),
             'team_invitation_decline' => array('name' => 'pozvánka do tímu odmietnutá' ,'action' => 'teamInvitationDeclineTest'),
             'player_leave_team' => array('name' => 'hráč sám opustil tím' ,'action' => 'playerLeaveTest'),
             'delete_single_event' => array('name' => 'Zmazana udalost' ,'action' => 'eventDeletedSingleTest'),
             'delete_repeat_event' => array('name' => 'Zmazana udalost' ,'action' => 'eventDeletedRepeatTest'),
             'delete_next_repeat_event' => array('name' => 'Zmazana udalost' ,'action' => 'eventDeletedNextRepeatTest'),
             'ommit_repeat_event' => array('name' => 'Zmazana udalost' ,'action' => 'eventOmmitRepeatTest'),
             'event_capacity' => array('name' => 'Dochadzka plna' ,'action' => 'eventAttendanceFullTest'),
             'missing_players' => array('name' => 'Nedostatok hráčov' ,'action' => 'missingPlayersTest'),
             'post_comment' => array('name' => 'Komentár k príspevku' ,'action' => 'postCommentTest'),
             'event_attendance_in' => array('name' => 'Potvrdenie dochadzky' ,'action' => 'eventAttendanceInTest'),
             'event_attendance_out' => array('name' => 'Zamietnutie dochadzky' ,'action' => 'eventAttendanceOutTest'),
              'close_maatch' => array('name' => 'Ukončený zápas' ,'action' => 'closeMatchTest'),
              'generated_post' => array('name' => 'Vygenerovaný widget' ,'action' => 'generatedPersonalHpWallPostTest'),
              'tournament_join_admin' => array('name' => 'Notifikácia adminovi turnaja' ,'action' => 'tournamentJoinAdminTest'),
              'tournament_team_confirmed' => array('name' => 'Notifikácia adminovi timu o prijati' ,'action' => 'tournamentTeamConfirmedTest'),
              'tournament_team_rejected' => array('name' => 'Notifikácia adminovi timu o zamietnuti' ,'action' => 'tournamentTeamRejectedTest'),
             'player_invitation' => array('name' => 'žiadost o prijatie do timu' ,'action' => 'playerInvitationTest'),
              'team_join_request_refused' => array('name' => 'žiadost o prijatie do timu zamietnuta adminom' ,'action' => 'teamJoinRequestRefusedTest'),
              'team_join_request_accepted' => array('name' => 'žiadost o prijatie do timu potvrdena adminom' ,'action' => 'teamJoinRequestAcceptedTest'),
             'game_event' => array('name' => 'udalost v zapase' ,'action' => 'gameEventTest'),
             );
         
        
        if('POST' == $request->getMethod())
        {
            $result = call_user_func(array($this,$request->get('action')));
             return $this->render('CR\Message\MessageModule:pushTest:test_result.php', array(
                'result' => $result    
          ));
        }


        
        return $this->render('CR\Message\MessageModule:pushTest:list.php', array(
              'list' => $list    
        ));
    }

    public function hpWallPostTest()
    {
        $teamPlayers = $this->getTeamRecipientsMock();
        $data = array('teamPlayers' => $teamPlayers,'senderId' => 3,'postId' => 34);
        
        $notificationManager = ServiceLayer::getService('SystemNotificationManager');   
        
        $result = array();
        $result['notifyList'] = $notificationManager->buildPushNotifyList('hp_wall_post', $data);
        $result['response'] = $notificationManager->createPushNotify('hp_wall_post',$data);
        
         return $result;
    }
    
    public function newPlayerTest()
    {
        $player = new \Webteamer\Player\Model\Player();
        $player->setName('jan');
        $player->setSurname('tester');
        
        $teamPlayers = $this->getTeamRecipientsMock();
        $team = $this->getTeamMock();
        $data = array('teamPlayers' => $teamPlayers,'team' => $team,'player' => $player);
        
        $notificationManager = ServiceLayer::getService('SystemNotificationManager');   
        
        $result = array();
        $result['notifyList'] = $notificationManager->buildPushNotifyList('create_player', $data);
        $result['response'] = $notificationManager->createPushNotify('create_player',$data);

         return $result;
    }
    
    public function playerInvitationTest()
    {
        $user = new \User\Model\User();
        $user->setId(3);
        
        $player = new \Webteamer\Player\Model\Player();
        $player->setName('jan');
        $player->setSurname('tester');
        
        $team = new \Webteamer\Team\Model\Team();
        $team->setId('0');
        $team->setName('test tim');

        
        
        $data = array('player' => $player,'team' => $team);
        
        $notificationManager = ServiceLayer::getService('SystemNotificationManager');   
        
        $result = array();
        $result['notifyList'] = $notificationManager->buildPlayerInvitationPushNotify($user, $data);
        

        $result['response'] = $notificationManager->sendPlayerInvitationPushNotify($user,$data);
         return $result;
    }
    
    public function teamInvitationTest()
    {
        $recipient= new \User\Model\User();
        $recipient->setId(3);
        
        $sender = new \Webteamer\Player\Model\Player();
        $sender->setName('jan');
        $sender->setSurname('tester');

        $team = $this->getTeamMock();
        $data = array('sender' => $sender,'team' => $team,'recipient' => $recipient);
        
        $notificationManager = ServiceLayer::getService('SystemNotificationManager');   
        $result = array();
        $result['response'] = $notificationManager->sendTeamInvitationPushNotify($data);

         return $result;
    }
    
    public function gameEventTest()
    {
        $recipient= new \User\Model\User();
        $recipient->setId(3);
        
        $sender = new \Webteamer\Player\Model\Player();
        $sender->setName('jan');
        $sender->setSurname('tester');

        $lineup = new \Webteamer\Team\Model\TeamMatchLineup();
        $lineup->setId(2042);
        $lineup->setFirstLineName('Team 1');
        $lineup->setSecondLineName('Team 2');
        $lineup->setTeamId(1);
        $lineup->setOpponentTeamId(2);

        $data = array('type' => 'goal','lineup' => $lineup,'scoringTeamId' => 2,'score' => '1:1');
        
        $notificationManager = ServiceLayer::getService('SystemNotificationManager');   
        $result = array();
        $result['notifyList'] = $notificationManager->buildGameEventNotify($data);
        $result['response'] = $notificationManager->sendGameEventPushNotify($data);

         return $result;
    }
    
   
    
    public function teamInvitationDeclineTest()
    {
        $user = new \User\Model\User();
        $user->setId(3);

        $player = new \Webteamer\Player\Model\Player();
        $player->setName('jan');
        $player->setSurname('tester');

        $team = new \Webteamer\Team\Model\Team();
        $team->setId('0');
        $team->setName('test');

        $data = array('player' => $player, 'team' => $team);

        $notificationManager = ServiceLayer::getService('SystemNotificationManager');

        $result = array();
        $result['notifyList'] = $notificationManager->buildTeamInvitationDeclinePushNotify($user, $data);
        $result['response'] = $notificationManager->sendTeamInvitationDeclinePushNotify(array($user), $data);
        return $result;
    }

    public function playerLeaveTest()
    {
        $user = new \User\Model\User();
        $user->setId(3);
        
        $player = new \Webteamer\Player\Model\Player();
        $player->setName('jan');
        $player->setSurname('tester');
        
        $team = new \Webteamer\Team\Model\Team();
        $team->setId('0');
        $team->setName('test tim');

        
        
        $data = array('player' => $player,'team' => $team);
        
        $notificationManager = ServiceLayer::getService('SystemNotificationManager');   
        
        $result = array();
        $result['notifyList'] = $notificationManager->buildPlayerLeaveTeamPushNotify($user, $data);
        $result['response'] = $notificationManager->sendPlayerLeaveTeamPushNotify($user,$data);
         return $result;
    }
    
    public function eventDeletedSingleTest()
    {
        $teamPlayers = $this->getTeamRecipientsMock();
        $team = $this->getTeamMock();
        $event = new \Webteamer\Event\Model\Event();
        $event->setName('Zrusena udalost');
        $event->setStart(new \Datetime());
        $data = array('teamPlayers' => $teamPlayers,'team' => $team,'event' => $event);
        
        $notificationManager = ServiceLayer::getService('SystemNotificationManager');   
        
        $result = array();
        $result['notifyList'] = $notificationManager->buildPushNotifyList('delete_single_event', $data);
        $result['response'] = $notificationManager->createPushNotify('delete_single_event',$data);

         return $result;
    }
    
    public function eventDeletedRepeatTest()
    {
        $teamPlayers = $this->getTeamRecipientsMock();
        $team = $this->getTeamMock();
        $event = new \Webteamer\Event\Model\Event();
        $event->setName('Zrusena udalost');
        $event->setStart(new \Datetime());
        $data = array('teamPlayers' => $teamPlayers,'team' => $team,'event' => $event);
        
        $notificationManager = ServiceLayer::getService('SystemNotificationManager');   
        
        $result = array();
        $result['notifyList'] = $notificationManager->buildPushNotifyList('delete_repeat_event', $data);
        $result['response'] = $notificationManager->createPushNotify('delete_repeat_event',$data);

         return $result;
    }
    public function eventDeletedNextRepeatTest()
    {
        $teamPlayers = $this->getTeamRecipientsMock();
        $team = $this->getTeamMock();
        $event = new \Webteamer\Event\Model\Event();
        $event->setName('Zrusena udalost');
        $event->setEnd(new \Datetime('+1 month'));
        $event->setStart(new \Datetime());
        
        $data = array('teamPlayers' => $teamPlayers,'team' => $team,'event' => $event);
        
        $notificationManager = ServiceLayer::getService('SystemNotificationManager');   
        
        $result = array();
        $result['notifyList'] = $notificationManager->buildPushNotifyList('delete_next_repeat_event', $data);
        $result['response'] = $notificationManager->createPushNotify('delete_next_repeat_event',$data);

         return $result;
    }
    
    public function eventOmmitRepeatTest()
    {
        $teamPlayers = $this->getTeamRecipientsMock();
        $team = $this->getTeamMock();
        $event = new \Webteamer\Event\Model\Event();
        $event->setName('Zrusena udalost');
        $event->setStart(new \Datetime());
        $ommitedDate = new \DateTime('+1 month');
        $ommitedDate2 = new \DateTime('-1 month');
        $ommitedDate3 = new \DateTime('+2 month');
        $event->setOmittedTermins(serialize(new \DateTime($ommitedDate->format('Y-m-d'))).','.serialize(new \DateTime($ommitedDate2->format('Y-m-d'))).','.serialize(new \DateTime($ommitedDate3->format('Y-m-d'))));
        $data = array('teamPlayers' => $teamPlayers,'team' => $team,'event' => $event);
        
        $notificationManager = ServiceLayer::getService('SystemNotificationManager');   
        
        $result = array();
        $result['notifyList'] = $notificationManager->buildPushNotifyList('ommit_repeat_event', $data);
        $result['response'] = $notificationManager->createPushNotify('ommit_repeat_event',$data);

         return $result;
    }
    
    public function eventAttendanceFullTest()
    {
        $event = new \Webteamer\Event\Model\Event();
        $event->setName('Zrusena udalost');
        $event->setStart(new \Datetime());
        $event->setId(194);
        $event->setCurrentDate(new \DateTime('2018-08-07'));
         $team = $this->getTeamMock();
        $teamPlayers = $this->getTeamRecipientsMock();
        $data = array('event' => $event,'teamPlayers' => $teamPlayers,'team' => $team);
        
        $notificationManager = ServiceLayer::getService('SystemNotificationManager');   
        
        $result = array();
        $result['notifyList'] = $notificationManager->buildPushNotifyList('event_capacity', $data);
        $result['response'] = $notificationManager->createPushNotify('event_capacity',$data);

         return $result;
    }
    
    public function eventAttendanceInTest()
    {
        $event = new \Webteamer\Event\Model\Event();
        $event->setName('Udalost');
        $event->setStart(new \Datetime());
        $event->setId(194);
        $event->setCurrentDate(new \DateTime('2018-08-07'));
        
         $team = $this->getTeamMock();
        
        $teamPlayers = $this->getTeamRecipientsMock();
        $teamPlayer = $teamPlayers[0];
        $data = array('event' => $event,'teamPlayers' => $teamPlayers,'teamPlayer' => $teamPlayer,'team' => $team);
        
        $notificationManager = ServiceLayer::getService('SystemNotificationManager');   
        
        $result = array();
        $result['notifyList'] = $notificationManager->buildPushNotifyList('event_attendance_in', $data);
        $result['response'] = $notificationManager->createPushNotify('event_attendance_in',$data);

         return $result;
    }
    
    public function missingPlayersTest()
    {
        $user = new \User\Model\User();
        $user->setId(3);
        
        $event = new \Webteamer\Event\Model\Event();
        $event->setName('Udalost');
        $event->setStart(new \Datetime());
        $event->setId(194);
        $event->setCurrentDate(new \DateTime('2018-08-07'));
        
        $notificationManager = ServiceLayer::getService('SystemNotificationManager');
        
        $result = array();
        $result['notifyList'] = $notificationManager->buildMissingPlayerPushNotify($user,$event);
        $result['response'] = $notificationManager->sendMissingPlayerPushNotify(array($user),$event);
         return $result;
    }
    
    public function postCommentTest()
    {
        $user = new \User\Model\User();
        $user->setId(3);
        $user->setName('MAREK');
      
        
        $post = new \CR\HpWall\Model\HpWallPost();
        $post->setId(8);
        $post->setAuthorId(3);
        
        $notificationManager = ServiceLayer::getService('SystemNotificationManager');
        
        $result = array();
        $result['notifyList'] = $notificationManager->buildPostCommentPushNotify($post,$user);
        $result['response'] = $notificationManager->sendPostCommentPushNotify($post,$user);
         return $result;
    }
    
    public function closeMatchTest()
    {
        $notificationManager = ServiceLayer::getService('SystemNotificationManager');
        $teamPlayers = $this->getTeamRecipientsMock();
        $teamEvent = $this->getEventMock();
        $eventDate = $teamEvent->getCurrentDate();
        
       
        $result['response'] =  $notificationManager->sendCloseMatchPushNotify($teamPlayers,array('eventId' => $teamEvent->getId(),'eventDate' => $eventDate->format('Y-m-d')));
        return $result;
    }
    
    public function generatedPersonalHpWallPostTest()
    {
        $teamPlayer = new \Webteamer\Team\Model\TeamPlayer();
        $teamPlayer->setPlayerId(3);
        $notificationManager = ServiceLayer::getService('SystemNotificationManager');

       
        $result['response'] =  $notificationManager->sendGeneratedPersonalPostPushNotify($teamPlayer);
        return $result;
    }
    
    public function tournamentJoinAdminTest()
    {
        $recipient = new \User\Model\User();
        $recipient->setId(3);
        $notificationManager = ServiceLayer::getService('SystemNotificationManager');
     
        $tournament = new \CR\Tournament\Model\Tournament();
        $tournament->setId(1425);
        $tournament->setName('Turnaj xy');
        $tournament->setAdminList(array(3));
        
        $team = $this->getTeamMock();
        
        $result['notifyList'] = $notificationManager->buildTournamentJoinAdminNotify($tournament,$team);
        $result['response'] = $notificationManager->sendTournamentJoinAdminNotify($tournament,$team);
        
        return $result;
    }
    
    public function tournamentTeamConfirmedTest()
    {
        $recipient = new \User\Model\User();
        $recipient->setId(3);
        $notificationManager = ServiceLayer::getService('SystemNotificationManager');
     
        $tournament = new \CR\Tournament\Model\Tournament();
        $tournament->setId(1425);
        $tournament->setName('Turnaj xy');
        $tournament->setAdminList(array(3));
        
        $team = $this->getTeamMock();
        
        $result['notifyList'] = $notificationManager->buildTournamentTeamConfirmedNotify($tournament,$team);
        $result['response'] = $notificationManager->sendTournamentTeamConfirmedNotify($tournament,$team);
        
        return $result;
    }
    
    public function tournamentTeamRejectedTest()
    {
         $recipient = new \User\Model\User();
        $recipient->setId(3);
        $notificationManager = ServiceLayer::getService('SystemNotificationManager');
     
        $tournament = new \CR\Tournament\Model\Tournament();
        $tournament->setId(1425);
        $tournament->setName('Turnaj xy');
        $tournament->setAdminList(array(3));
        
        $team = $this->getTeamMock();
        
        $result['notifyList'] = $notificationManager->buildTournamentTeamRejectedNotify($tournament,$team);
        $result['response'] = $notificationManager->sendTournamentTeamRejectedNotify($tournament,$team);
        
        return $result;
    }
    
    public function teamJoinRequestRefusedTest()
    {
        $player = new \User\Model\User();
        $player->setId(3);
        $player->setName('Hrac');
        $notificationManager = ServiceLayer::getService('SystemNotificationManager');

        $team = $this->getTeamMock();
        
        $admin = new \User\Model\User();
        $admin->setId(3);
        $admin->setName('Admin');
        
        $data = array('admin' => $admin,'team' => $team);
        
        $result['notifyList'] = $notificationManager->buildTeamJoinRequestNotify($player,'refused',$data);
        $result['response'] = $notificationManager->sendTeamJoinRequestNotify($player,'refused',$data);
        
        return $result;
    }
    
    public function teamJoinRequestAcceptedTest()
    {
         $player = new \User\Model\User();
        $player->setId(3);
        $player->setName('Hrac');
        $notificationManager = ServiceLayer::getService('SystemNotificationManager');

        $team = $this->getTeamMock();
        
        $admin = new \User\Model\User();
        $admin->setId(3);
        $admin->setName('Admin');
        
        $data = array('admin' => $admin,'team' => $team);
        
        $result['notifyList'] = $notificationManager->buildTeamJoinRequestNotify($player,'accepted',$data);
        $result['response'] = $notificationManager->sendTeamJoinRequestNotify($player,'accepted',$data);
        
        return $result;
    }
    
    
    
    public function getTeamRecipientsMock()
    {
         $teamManager = \Core\ServiceLayer::getService('TeamManager');
        $teamPlayer = $teamManager->findTeamPlayerById(418);
        $teamPlayers = array(
            $teamPlayer
        );
        
        return $teamPlayers;
    }
    
    public function getEventMock()
    {
        $event = new \Webteamer\Event\Model\Event();
        $event->setName('Udalost');
        $event->setStart(new \Datetime());
        $event->setId(194);
        $event->setCurrentDate(new \DateTime('2018-08-07'));
        
        return $event;
    }
    
    public function getTeamMock()
    {
        $teamManager = \Core\ServiceLayer::getService('TeamManager');
        $team = $teamManager->findTeamById(3);
        return $team;
    }
    
    
    public function testCreatePushNotifyAction()
    {
        $event = ServiceLayer::getService('EventRepository')->find(194);
        $event->setCurrentDate(new \DateTime('2018-08-14'));
        $teamManager = \Core\ServiceLayer::getService('TeamManager');
        $team = $teamManager->findTeamById($event->getTeamId());
         $notificationManager = ServiceLayer::getService('SystemNotificationManager');
        
        $notifyList = $notificationManager->buildPushNotifyList('create_player',array('team' => $team,'event' => $event));
        
    }
    
    /*
    public function testMissingPlayerPushNotify($team,$event)
    {
        $teamManager = \Core\ServiceLayer::getService('TeamManager');
        $notificationManager = ServiceLayer::getService('SystemNotificationManager');
        $users =$teamManager->getTeamAdminList($team);
        $notificationManager->sendMissingPlayerPushNotify($users,$event);
    }
    */
    
    
    public function sendMessageAction()
    {
        
        $pushManager = ServiceLayer::getService('PushManager');
        
        $teamPlayer = new \Webteamer\Team\Model\TeamPlayer();
        $teamPlayer->setName('jan');
        $teamPlayer->setLastName('tester');
        $teamPlayer->setPlayerId(11);
        
        $event = new \Webteamer\Event\Model\Event();
        $event->setName('test');
        $event->setStart(new \DateTime());
        $event->setCurrentDate(new \DateTime());
        
        $pushMessage = new \CR\Message\Model\PushMessage();
        $pushMessage->setSubject(array(
            'sk' => 'Confirmed attendance',
            'en' => 'Potvrdená účasť'
            ));
        
        $pushMessage->setBody(array(
          'sk' => $teamPlayer->getFullName().' potvrdil svoju účasť pre '.$event->getName().','.$event->getCurrentDate()->format('d.m.Y').' '.$event->getStart()->format('H:i'),
          'en' => $teamPlayer->getFullName().' has confirmed attendance for '.$event->getName().','.$event->getCurrentDate()->format('d.m.Y').' '.$event->getStart()->format('H:i'),
          ));
        
        
        $response = $pushManager->sendAttendanceNotify($teamPlayer,$event,'in');
        
        var_dump($response);
        
        //$oneSignal->request('https://onesignal.com/api/v1/notifications',array());
        
        
        //$oneSignal->sendAttendancePush($team,$teamPlayer, $event,1);
        
        
        exit;
    }
    
  
}
