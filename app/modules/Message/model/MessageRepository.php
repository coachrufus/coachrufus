<?php
namespace CR\Message\Model;
use Core\Repository as Repository;

class MessageRepository extends Repository
{
    public function createObjectList($data)
    {
        $list = array();
        foreach ($data as $d)
        {
            $object = $this->factory->createEntityFromArray($d);
            
            //user photo
            $user = new \User\Model\User();
            $user->setPhoto($d['user_photo']);
            $object->setUserPhoto($user->getMidPhoto());
            $list[] = $object;
        }
        return $list;
    }
    
    
    public function getRecipientMessagesInfo($recipient)
    {
        $info = $this->getStorage()->getRecipientMessagesInfo($recipient->getId());
        return $info;
    }
    
    public function getNotificationsInfo($recipient)
    {
        $info = $this->getStorage()->getNotificationsInfo($recipient->getId());
        return $info;
    }
    
    public function getSenderMessagesInfo($sender)
    {
        $info = $this->getStorage()->getSenderMessagesInfo($sender->getId());
        return $info;
    }
    
    public function  getRecipientMessages($recipient,$criteria)
    {
        $data = $this->getStorage()->getRecipientMessages($recipient->getId(),$criteria);
        return $this->createObjectList($data);
    }    
    
    public function  getSystemNotifications($recipient,$criteria)
    {
        $data = $this->getStorage()->getSystemNotifications($recipient->getId(),$criteria);
        return $this->createObjectList($data);
    }    
    
    public function  getSenderMessages($sender,$criteria)
    {
        $data = $this->getStorage()->getSenderMessages($sender->getId(),$criteria);
        return $this->createObjectList($data);
    }    
    
    public function markUserAllRead($userId)
    {
         $this->getStorage()->markUserAllRead($userId);
    }
    
    
    
}
