<?php

namespace CR\Message\Model;

use Core\DbStorage;

class MessageStorage extends DbStorage {

    public function getRecipientMessagesInfo($recipientId)
    {

        $query = \Core\DbQuery::prepare('
                 SELECT status, 
                        Count(*) AS count 
                 FROM   message 
                 WHERE  recipient_id = :id 
                 AND type = "message"
                 GROUP  BY status ')
                ->bindParam('id', $recipientId );


        $data = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);

        return $data;
    }
    public function getNotificationsInfo($recipientId)
    {

        $query = \Core\DbQuery::prepare('
                 SELECT status, 
                        Count(*) AS count 
                 FROM   message 
                 WHERE  recipient_id = :id 
                 AND type = "system"
                 GROUP  BY status ')
                ->bindParam('id', $recipientId );


        $data = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);

        return $data;
    }
    
    public function getSenderMessagesInfo($senderId)
    {


        $query = \Core\DbQuery::prepare('
                 SELECT Count(*) AS count 
                 FROM   message 
                 WHERE  sender_id = :id ')
                ->bindParam('id', $senderId );


        $data = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);

        return $data;
    }
    
    public function getRecipientMessages($recipientId,$criteria)
    {
        $status_criteria = '';
        if(array_key_exists('status', $criteria))
        {
            $status_criteria = ' AND status = "'.$criteria['status'].'" ';
        }
        
        $query = \Core\DbQuery::prepare('
                 SELECT *
                 FROM   message 
                 WHERE  recipient_id = :id 
                 AND type = "message"
                 '.$status_criteria.'
                 ORDER BY '.$criteria['order_col'].' '.$criteria['order_dir']. ',  id desc 
                 LIMIT '.$criteria['from'].', '.$criteria['length'])
                ->bindParam('id', $recipientId );


        $data = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);

        return $data;
    }
    public function getSystemNotifications($recipientId,$criteria)
    {
         $status_criteria = '';
        if(array_key_exists('status', $criteria))
        {
            $status_criteria = ' AND m.status = "'.$criteria['status'].'" ';
        }
        
        $start_criteria = '';
        if(array_key_exists('start', $criteria) && $criteria['start']  >0)
        {
            $start_criteria = ' AND m.id <  "'.$criteria['start'].'" ';
        }
        
         if(array_key_exists('date_limit', $criteria))
        {
            $start_criteria = ' AND m.created_at >  "'.$criteria['date_limit']->format('Y-m-d 00:00:00').'" ';
        }
        
        
        $query = \Core\DbQuery::prepare('
                 SELECT m.*, t.photo as "team_photo", u.photo as "user_photo"
                 FROM   message m
                 LEFT JOIN team t ON m.team_id = t.id
                LEFT JOIN co_users u ON m.sender_id = u.id
                 WHERE  m.recipient_id = :id 
                 AND m.type = "system"
                  '.$status_criteria.$start_criteria.'
                 ORDER BY m.'.$criteria['order_col'].' '.$criteria['order_dir']. '
                 LIMIT '.$criteria['from'].', '.$criteria['length'])
                ->bindParam('id', $recipientId );


        $data = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);

        return $data;
    }
    
    public function getSenderMessages($senderId,$criteria)
    {
        $query = \Core\DbQuery::prepare('
                 SELECT *
                 FROM   message 
                 WHERE  sender_id = :id 
                 ORDER BY '.$criteria['order_col'].' '.$criteria['order_dir']. '
                 LIMIT '.$criteria['from'].', '.$criteria['length'])
                ->bindParam('id', $senderId );


        $data = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);

        return $data;
    }
    
    public function markUserAllRead($recipient_id)
    {

        \Core\DbQuery::prepare(' UPDATE  message set status = "fullread" WHERE recipient_id = :id ')
		->bindParam('id', $recipient_id)
		->execute();
    }
    
}