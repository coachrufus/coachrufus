<?php

namespace CR\Message\Model;

use Core\Manager;

class PushManager {

    private $appId = '6786fc89-81da-4f7c-a5d7-8828299c322e';
    private $auth = 'OTk3OGQ5MmYtMzBlNi00ODBlLWE5YjEtOGE1YWVmYzc4NTM2';

    public function getAuth()
    {
        return $this->auth;
    }

    public function setAuth($auth)
    {
        $this->auth = $auth;
    }
    
    public function getAppId()
    {
        return $this->appId;
    }

    public function setAppId($appId)
    {
        $this->appId = $appId;
    }
    
    public function makeRequest($action,$fields)
    {
        $data = json_encode($fields);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/".$action);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
            'Authorization: Basic OTk3OGQ5MmYtMzBlNi00ODBlLWE5YjEtOGE1YWVmYzc4NTM2'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }
    

    public function sendNewMessageNotify($user,$player)
    {
        $pushMessage = new \CR\Message\Model\PushMessage();
        $pushMessage->setRecipientId($player->getId());
        
          $pushMessage->setSubject(array(
                    'sk' => 'Nová správa',
                    'en' => 'New Message'
                ));
          
           $pushMessage->setBody(array(
                  'sk' => $user->getFullName().' ti poslal správu ',
                  'en' => $user->getFullName().' has sent you message',
                  ));
           
            $this->sendMessage($pushMessage,'notifications');
    }
    
    
    /**
     * Send info about attendance 
     * @param type $teamPlayer
     * @param type $event
     * @param type $type
     */
    /*
    public function sendAttendanceNotify($currentTeamPlayer,$teamPlayers,$event,$type)
    {
        if($currentTeamPlayer->getPlayerId() != null)
        {
            $pushMessage = new \CR\Message\Model\PushMessage();

            if($type=='in')
            {
                 $pushMessage->setSubject(array(
                    'sk' => 'Potvrdená účasť',
                    'en' => 'Confirmed attendance'
                ));

                $pushMessage->setBody(array(
                  'sk' => $currentTeamPlayer->getFullName().' potvrdil svoju účasť pre '.$event->getName().','.$event->getCurrentDate()->format('d.m.Y').' '.$event->getStart()->format('H:i'),
                  'en' => $currentTeamPlayer->getFullName().' has confirmed attendance for '.$event->getName().','.$event->getCurrentDate()->format('d.m.Y').' '.$event->getStart()->format('H:i'),
                  ));
            }

            if($type=='out')
            {
                 $pushMessage->setSubject(array(
                    'sk' => 'Zamietnutá účasť',
                    'en' => 'Refused attendance'
                ));

                $pushMessage->setBody(array(
                  'sk' => $currentTeamPlayer->getFullName().' zamietol svoju účasť pre '.$event->getName().','.$event->getCurrentDate()->format('d.m.Y').' '.$event->getStart()->format('H:i'),
                  'en' => $currentTeamPlayer->getFullName().' has refused attendance for '.$event->getName().','.$event->getCurrentDate()->format('d.m.Y').' '.$event->getStart()->format('H:i'),
                  ));
            }
        }
        
         foreach($teamPlayers as $teamPlayer)
        {
            if(null != $teamPlayer->getPlayerId() && $teamPlayer->getPlayerId() != $currentTeamPlayer->getPlayerId())
            {
                 $pushMessage->setRecipientId($teamPlayer->getPlayerId());
                $this->sendMessage($pushMessage,'notifications');
            }
        }
    }
    */
    
    public function sendMessage($pushMessage,$action)
    {
        $content = $pushMessage->getBody();
        $headings = $pushMessage->getSubject();

        $fields = array(
            'app_id' => "6786fc89-81da-4f7c-a5d7-8828299c322e",
            //'included_segments' => array('All'),
            'contents' => $content,
            'headings' => $headings,
            'tags' => array(array("field" => "tag", "key" => "user_id", "relation" => "=", "value" => $pushMessage->getRecipientId())),
            'data' => $pushMessage->getData(),
            'content_available' => true,
            'ios_badgeType' => 'Increase',
            'ios_badgeCount' => 1
            
        );
        
        //var_dump($fields);exit;
        
        
       $response = $this->makeRequest($action,$fields);
       return $response;
    }


    
    
}
