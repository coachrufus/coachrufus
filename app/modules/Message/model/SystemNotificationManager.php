<?php

namespace CR\Message\Model;

use Core\Manager;

class SystemNotificationManager extends MessageManager {

    protected $itemPerPage = 20;

    public function getItemPerPage()
    {
        return $this->itemPerPage;
    }

    public function setItemPerPage($itemPerPage)
    {
        $this->itemPerPage = $itemPerPage;
    }

    public function userBulkAction($user, $action)
    {
        if ($action == 'allread')
        {
            $this->getRepository()->markUserAllRead($user->getId());
        }
    }

    public function getNotificationsInfo($recipient)
    {
        $data = $this->getRepository()->getNotificationsInfo($recipient);
        $info = array('unread' => null);
        $sum = 0;
        foreach ($data as $key => $i)
        {
            $sum += $i['count'];
            $info[$i['status']] = intval($i['count']);
        }

        $info['total'] = $sum;
        return $info;
    }

    public function buildNoticationCollection($items)
    {
        $displayItems = array_slice($items, 0, $this->getItemPerPage());
        $nextItem = array_slice($items, $this->getItemPerPage(), 1);
        $collection = new SystemNotifyCollection($displayItems);

        if (empty($nextItem))
        {
            $collection->setAllItemsLoaded(true);
        }
        return $collection;
    }

    public function getNoticationList($recipient, $criteria = array())
    {
        //return $this->getRepository()->findBy(array('status' => 'unread','recipient_id' => $user->getId() ));

        $defaultCriteria = array(
            'order_col' => 'id',
            'order_dir' => 'desc',
            'from' => 0,
            'length' => 20,
            'start' => 0
        );

        $criteria = array_merge($defaultCriteria, $criteria);
        $criteria['length'] = $criteria['length'] + 1;
        $this->setItemPerPage($criteria['length'] - 1);

        $criteria['date_limit'] = new \DateTime('- 2 weeks');


        $items = $this->getRepository()->getSystemNotifications($recipient, $criteria);
        $list = $this->buildNoticationCollection($items);
        return $list;
    }

    public function getReadSystemNotifications($recipient, $limit = 4)
    {
        //return $this->getRepository()->findBy(array('status' => 'unread','recipient_id' => $user->getId() ));
        $criteria = array(
            'order_col' => 'id',
            'order_dir' => 'desc',
            'from' => 0,
            'length' => $limit,
            'status' => 'read'
        );
        return $this->getRepository()->getSystemNotifications($recipient, $criteria);
    }

    public function getUnreadSystemNotifications($recipient, $limit = 4)
    {
        //return $this->getRepository()->findBy(array('status' => 'unread','recipient_id' => $user->getId() ));
        $criteria = array(
            'order_col' => 'id',
            'order_dir' => 'desc',
            'from' => 0,
            'length' => $limit,
            'status' => 'unread'
        );
        return $this->getRepository()->getSystemNotifications($recipient, $criteria);
    }

    private function getMessageSubjectCode($codeKey)
    {

        $codes = array(
            'team_talk_post' => 'PUSH_SUBJECT_TEAM_TALK_POST',
            'hp_wall_post' => 'PUSH_SUBJECT_HP_WALL_POST',
            'hp_wall_post_comment' => 'PUSH_SUBJECT_HP_WALL_POST_COMMENT',
            'event_capacity' => 'PUSH_SUBJECT_EVENT_CAPACITY',
            'create_player' => 'PUSH_SUBJECT_CREATE_PLAYER',
            'delete_single_event' => 'PUSH_SUBJECT_DELETE_SINGLE_EVENT',
            'delete_repeat_event' => 'PUSH_SUBJECT_DELETE_REPEAT_EVENT',
            'delete_next_repeat_event' => 'PUSH_SUBJECT_DELETE_NEXT_REPEAT_EVENT',
            'ommit_repeat_event' => 'PUSH_SUBJECT_OMMIT_REPEAT_EVENT',
            'delete_stat_event' => 'PUSH_SUBJECT_DELETE_STAT_EVENT',
            'player_leave_team' => 'PUSH_SUBJECT_PLAYER_LEAVE_TEAM',
            'player_rating' => 'PUSH_SUBJECT_PLAYER_RATING',
            'create_lineup' => 'PUSH_SUBJECT_CREATE_LINEUP',
            'close_match' => 'PUSH_SUBJECT_CLOSE_MATCH',
            'missing_players' => 'PUSH_SUBJECT_MISSING_PLAYERS',
            'event_attendance_in' => 'PUSH_SUBJECT_EVENT_ATTENDANCE_IN',
            'event_attendance_out' => 'PUSH_SUBJECT_EVENT_ATTENDANCE_OUT',
            'hp_wall_post_generated' => 'PUSH_SUBJECT_HP_WALL_POST_GENERATED',
            'team_invitation' => 'PUSH_SUBJECT_TEAM_INVITATION',
            'team_invitation_decline' => 'PUSH_SUBJECT_TEAM_INVITATION_DECLINE',
            'tournament_join_admin' => 'PUSH_SUBJECT_TOURNAMENT_JOIN_ADMIN',
            'tournament_team_confirmed' => 'PUSH_SUBJECT_TOURNAMENT_TEAM_CONFIRMED',
            'tournament_team_rejected' => 'PUSH_SUBJECT_TOURNAMENT_TEAM_REJECTED',
            'player_invitation' => 'PUSH_SUBJECT_PLAYER_INVITATION',
            'team_join_request_refused' => 'PUSH_SUBJECT_TEAM_JOIN_REQUEST_REFUSED',
            'team_join_request_accepted' => 'PUSH_SUBJECT_TEAM_JOIN_REQUEST_ACCEPTED',
            'game_event' => 'PUSH_SUBJECT_GAME_EVENT',
        );

        return $codes[$codeKey];
    }

    private function getMessageBodyCode($codeKey)
    {
        $codes = array(
            'team_talk_post' => 'PUSH_BODY_TEAM_TALK_POST',
            'hp_wall_post' => 'PUSH_BODY_HP_WALL_POST',
            'hp_wall_post_comment' => 'PUSH_BODY_HP_WALL_POST_COMMENT',
            'event_capacity' => 'PUSH_BODY_EVENT_CAPACITY',
            'create_player' => 'PUSH_BODY_CREATE_PLAYER',
            'delete_single_event' => 'PUSH_BODY_DELETE_SINGLE_EVENT',
            'delete_repeat_event' => 'PUSH_BODY_DELETE_REPEAT_EVENT',
            'delete_next_repeat_event' => 'PUSH_BODY_DELETE_NEXT_REPEAT_EVENT',
            'ommit_repeat_event' => 'PUSH_BODY_OMMIT_REPEAT_EVENT',
            'delete_stat_event' => 'PUSH_BODY_DELETE_STAT_EVENT',
            'player_leave_team' => 'PUSH_BODY_PLAYER_LEAVE_TEAM',
            'player_rating' => 'PUSH_BODY_PLAYER_RATING',
            'create_lineup' => 'PUSH_BODY_CREATE_LINEUP',
            'close_match' => 'PUSH_BODY_CLOSE_MATCH',
            'missing_players' => 'PUSH_BODY_MISSING_PLAYERS',
            'event_attendance_in' => 'PUSH_BODY_EVENT_ATTENDANCE_IN',
            'event_attendance_out' => 'PUSH_BODY_EVENT_ATTENDANCE_OUT',
            'hp_wall_post_generated' => 'PUSH_BODY_HP_WALL_POST_GENERATED',
            'team_invitation' => 'PUSH_BODY_TEAM_INVITATION',
            'team_invitation_decline' => 'PUSH_BODY_TEAM_INVITATION_DECLINE',
            'tournament_join_admin' => 'PUSH_BODY_TOURNAMENT_JOIN_ADMIN',
            'tournament_team_confirmed' => 'PUSH_BODY_TOURNAMENT_TEAM_CONFIRMED',
            'tournament_team_rejected' => 'PUSH_BODY_TOURNAMENT_TEAM_REJECTED',
            'player_invitation' => 'PUSH_BODY_PLAYER_INVITATION',
            'team_join_request_refused' => 'PUSH_BODY_TEAM_JOIN_REQUEST_REFUSED',
            'team_join_request_accepted' => 'PUSH_BODY_TEAM_JOIN_REQUEST_ACCEPTED',
            'game_event' => 'PUSH_BODY_GAME_EVENT',
        );

        return $codes[$codeKey];
    }

    private function getMessageSubject($codeKey,$data = array(),$lang=null)
    {
        $translator = \Core\ServiceLayer::getService('translator');
        $subject = $translator->translate($this->getMessageSubjectCode($codeKey),$lang);
        
         if('game_event' == $codeKey)
        {
             if($data['type'] == 'goal')
             {
                  $subject=  str_replace('{EVENT_TYPE}', $translator->translate('push_notify.game_event.goal'), $subject);
             }
        }
        
        return $subject;
    }

    private function getMessageBody($codeKey,$data = array(),$lang = null)
    {
        $translator = \Core\ServiceLayer::getService('translator');
        $messageBody = $translator->translate($this->getMessageBodyCode($codeKey),$lang);
        
        if('delete_single_event' == $codeKey)
        {
             $messageBody=  str_replace('{EVENT_NAME}', $data['event']->getName(), $messageBody);
             $messageBody=  str_replace('{EVENT_TERMIN}', $data['event']->getStart()->format('d.m.Y').' o '.$data['event']->getStart()->format('H:i'), $messageBody);
             $messageBody=  str_replace('{TEAM_NAME}', $data['team']->getName(), $messageBody);
        }
        
        if('event_capacity' == $codeKey)
        {
             $messageBody=  str_replace('{EVENT_NAME}', $data['event']->getName(), $messageBody);
             $messageBody=  str_replace('{EVENT_TERMIN}', $data['event']->getCurrentDate()->format('d.m.Y').' o '.$data['event']->getStart()->format('H:i'), $messageBody);
             $messageBody=  str_replace('{TEAM_NAME}', $data['team']->getName(), $messageBody);
        }
        
        if('delete_repeat_event' == $codeKey)
        {
             $messageBody=  str_replace('{EVENT_NAME}', $data['event']->getName(), $messageBody);
             $messageBody=  str_replace('{EVENT_TERMIN}', $data['event']->getStart()->format('d.m.Y').' o '.$data['event']->getStart()->format('H:i'), $messageBody);
             $messageBody=  str_replace('{TEAM_NAME}', $data['team']->getName(), $messageBody);
        }
        if('delete_next_repeat_event' == $codeKey)
        {
             $messageBody=  str_replace('{EVENT_NAME}', $data['event']->getName(), $messageBody);
             $messageBody=  str_replace('{EVENT_TERMIN_END}', $data['event']->getEnd()->format('d.m.Y'), $messageBody);
             $messageBody=  str_replace('{TEAM_NAME}', $data['team']->getName(), $messageBody);
        }
        
        if('ommit_repeat_event' == $codeKey)
        {
             $messageBody=  str_replace('{EVENT_NAME}', $data['event']->getName(), $messageBody);
             
             $ommitedTermins = explode(',',$data['event']->getOmittedTermins());
             $validOmmitedTermins = array();
             foreach($ommitedTermins as $ommitedTermin)
             {
                $termin = unserialize($ommitedTermin);
                if($termin->getTimestamp() > time())
                {
                    $validOmmitedTermins[] = $termin->format('d.m.Y H:i');
                }
                
             }
             $messageBody=  str_replace('{EVENT_TERMIN_OMMITED}',implode(',',$validOmmitedTermins), $messageBody);
             $messageBody=  str_replace('{TEAM_NAME}', $data['team']->getName(), $messageBody);
        }
        
        if('event_attendance_in' == $codeKey or 'event_attendance_out' == $codeKey)
        {
            $messageBody=  str_replace('{TEAM_PLAYER_NAME}', $data['teamPlayer']->getFullName(), $messageBody);
            $messageBody=  str_replace('{EVENT_NAME}', $data['event']->getName(), $messageBody);
            $messageBody=  str_replace('{EVENT_TERMIN}', $data['event']->getCurrentDate()->format('d.m.Y').' o '.$data['event']->getStart()->format('H:i'), $messageBody);
            $messageBody=  str_replace('{TEAM_NAME}', $data['team']->getName(), $messageBody);
        }
        
        //var_dump($data['author']);exit;
        
        if('hp_wall_post_comment' == $codeKey)
        {
             $messageBody=  str_replace('{AUTHOR_NAME}', $data['author']->getName(), $messageBody); 
        }
        if('team_invitation' == $codeKey)
        {
            $messageBody=  str_replace('{AUTHOR_NAME}', $data['author']->getName().' '.$data['author']->getSurname(), $messageBody);
            $messageBody=  str_replace('{TEAM_NAME}', $data['team']->getName(), $messageBody);
        }
        if('team_invitation_decline' == $codeKey)
        {
            $messageBody=  str_replace('{TEAM_NAME}', $data['team']->getName(), $messageBody);
            $messageBody=  str_replace('{PLAYER_NAME}', $data['player']->getFullName(), $messageBody);
        }
        
        if('tournament_join_admin' == $codeKey or 'tournament_team_confirmed' == $codeKey or 'tournament_team_rejected' == $codeKey)
        {
             $messageBody=  str_replace('{TEAM_NAME}', $data['team']->getName(), $messageBody);
             $messageBody=  str_replace('{TOURNAMENT_NAME}', $data['tournament']->getName(), $messageBody);
        }
        
        if('player_leave_team' == $codeKey)
        {
            $messageBody=  str_replace('{TEAM_NAME}', $data['team']->getName(), $messageBody);
            $messageBody=  str_replace('{PLAYER_NAME}', $data['player']->getFullName(), $messageBody);
        }
        if('player_invitation' == $codeKey)
        {
            $messageBody=  str_replace('{TEAM_NAME}', $data['team']->getName(), $messageBody);
            $messageBody=  str_replace('{PLAYER_NAME}', $data['player']->getFullName(), $messageBody);
        }
        if('create_player' == $codeKey)
        {
            $messageBody=  str_replace('{TEAM_NAME}', $data['team']->getName(), $messageBody);
            $messageBody=  str_replace('{PLAYER_NAME}', $data['player']->getFullName(), $messageBody);
        }
        if('team_join_request_refused' == $codeKey or 'team_join_request_accepted' == $codeKey )
        {
            $messageBody=  str_replace('{ADMIN_NAME}',$data['admin']->getName() , $messageBody);
            $messageBody=  str_replace('{TEAM_NAME}', $data['team']->getName(), $messageBody);
        }
        if('game_event' == $codeKey  )
        {
            $messageBody=  str_replace('{TEAM_NAME1}',$data['lineup']->getFirstLineName() , $messageBody);
            $messageBody=  str_replace('{TEAM_NAME2}',$data['lineup']->getSecondLineName() , $messageBody);
            $messageBody=  str_replace('{SCORE}',$data['score'], $messageBody);    
        }
        
        return $messageBody;
        
        
    }

    /*
     * Create system notification  message
     */
    public function createMessage($messageData, $recipient, $sender = null)
    {
        //sender message
        $message = new Message();
        $message->setBody($messageData['body']);
        $message->setCreatedAt(new \DateTime());
        $message->setStatus('unread');
        $message->setType('system');
        $message->setSubject($messageData['subject']);
        $message->setSenderName('Coachrufus');
        $message->setSenderEmail('noreply@coachrufus.com');
        $message->setRecipientId($recipient->getId());

        if (array_key_exists('team_id', $messageData))
        {
            $message->setTeamId($messageData['team_id']);
        }
        if (array_key_exists('rest_body', $messageData))
        {
            $message->setRestBody($messageData['rest_body']);
        }
         if (array_key_exists('senderId', $messageData))
        {
            $message->setSenderId($messageData['senderId']);
        }

        $mid = $this->saveMessage($message);
        return $mid;
    }
    
    public function changeTeamNotificationsSetup($user,$teamId,$type)
    {
        $userRepository = \Core\ServiceLayer::getService('user_repository');
        $settings = json_decode($user->getNotifySettings(), true);
        
        if(null == $settings)
        {
            $settings = array();
            $settings['push_notify'] = array();
            $settings['email_notify'] = array();
            $settings['team_notify'] = array();
        }
        
        
        $settings['team_notify'][$teamId] = '1';
        $user->setNotifySettings(json_encode($settings));
        $userRepository->save($user);
    }
           
    
    public function hasAllowedPushNotify($userId,$notifySetupCode)
    {
        $security = \Core\ServiceLayer::getService('security');
        $identityProvider = $security->getIdentityProvider();
        $user = $identityProvider->findUserById($userId);
        if(null != $user)
        {
             $playerNotifySettings = json_decode($user->getNotifySettings(), true);
            if($notifySetupCode == 'event_attendance_in' or $notifySetupCode == 'event_attendance_out')
            {
                $notifySetupCode = 'event_attendance';
            }

            if (array_key_exists($notifySetupCode, $playerNotifySettings['push_notify']))
            {
               return true;
            }
        }
       
        
        return false;
    }

    protected function sendPushNotify($data)
    {
        $pushManager = \Core\ServiceLayer::getService('PushManager');
        $pushMessage = new \CR\Message\Model\PushMessage();
        $pushMessage->setRecipientId($data['recipient_id']);
        $translator = \Core\ServiceLayer::getService('translator');
        $codeKey = $data['codeKey'];
        
       //t_dump($data);exit;
        $subjectSk = $this->getMessageSubject($codeKey,$data['message_data'],'sk');
        $subjectEn = $this->getMessageSubject($codeKey,$data['message_data'],'en');
        
        
        
       // $subjectSk  = $translator->translate($this->getMessageSubjectCode($codeKey), 'sk');
        //$subjectEn = $translator->translate($this->getMessageSubjectCode($codeKey), 'en');
        
        $sbodySk = $this->getMessageBody($codeKey,$data['message_data'],'sk');
        $sbodyEn = $this->getMessageBody($codeKey,$data['message_data'],'en');

        //$sbodySk = $translator->translate($this->getMessageBodyCode($codeKey), 'sk');
        //$sbodyEn = $translator->translate($this->getMessageBodyCode($codeKey), 'en');


        $pushMessage->setSubject(array(
            'sk' => $subjectSk,
            'en' => $subjectEn
        ));
        $pushMessage->setBody(array(
            'sk' => $sbodySk,
            'en' => $sbodyEn,
        ));
        


        $pushMessage->setData($data['push_data']);

        return $pushManager->sendMessage($pushMessage, 'notifications');
    }
    
   
    
    public function buildPushNotifyList($code, $data)
    {
        $teamManager = \Core\ServiceLayer::getService('TeamManager');
       
        $router = \Core\ServiceLayer::getService('router');
        $team = $data['team'];
        
        if(!array_key_exists('teamPlayers', $data))
        {
            $teamPlayers = $teamManager->getActiveRegistredTeamPlayers($team->getId());
        }
        else 
        {
            $teamPlayers = $data['teamPlayers'];
        }
        
        
        
        
        $list  = array();
        foreach ($teamPlayers as $teamPlayer)
        {
           if($teamPlayer->getPlayerId() > 0) 
           {
            $recipient = new \CR\Message\Model\MessageRecipient();
            $recipient->setId($teamPlayer->getPlayerId());
            $messageData['subject'] = $this->getMessageSubject($code);
            $messageData['body'] = $this->getMessageBody($code,$data);
            $messageData['team_id'] = $teamPlayer->getTeamId();

            $mobileLink = '';
            $screen = '';
            $metadata = array();
            $window = null;
            $notifySetupCode = '';
            switch ($code) {
                case 'team_talk_post':
                    $link = $router->link('team_overview', array('id' => $team->getId()));
                    break;
                case 'hp_wall_post':
                    $link =$router->link('api_content_home', array('apikey' => CR_API_KEY, 'user_id' => $teamPlayer->getPlayerId(),'a' => 'comments','post-edit' =>$data['postId']));
                    $mobileLink = WEB_DOMAIN.$router->link('api_content_home', array('apikey' => CR_API_KEY, 'user_id' => $teamPlayer->getPlayerId(),'a' => 'comments','post-edit' =>$data['postId']));
                    $screen = 'home';
                    $window = 'fullpopup';
                    $notifySetupCode = 'hp_wall_post';
                    break;
                case 'hp_wall_post_generated':
                    $link =$router->link('api_content_home', array('apikey' => CR_API_KEY, 'user_id' => $teamPlayer->getPlayerId(),'a' => 'comments','post-edit' =>$data['postId']));
                    $mobileLink = WEB_DOMAIN.$router->link('api_content_home', array('apikey' => CR_API_KEY, 'user_id' => $teamPlayer->getPlayerId(),'a' => 'comments','post-edit' =>$data['postId']));
                    $screen = 'home';
                    $window = 'fullpopup';
                    $notifySetupCode = 'hp_wall_post';
                    break;
                case 'event_capacity':
                    $event = $data['event'];
                    $link = $router->link('team_event_detail', array('id' => $event->getId(), 'current_date' => $event->getCurrentDate()->format('Y-m-d')));
                    $mobileLink = WEB_DOMAIN.$router->generateApiWidgetUrl(
                            'api_content_webview',
                            array(
                                'apikey' => CR_API_KEY,
                                'view' => $router->link('team_event_detail',array(
                                    'id' => $event->getId(),
                                    'apikey' => CR_API_KEY,
                                    'user_id' => $teamPlayer->getPlayerId(),
                                    'current_date' => $event->getCurrentDate()->format('Y-m-d'))).'#attendance',
                                'user_id' => $teamPlayer->getPlayerId()));
                    $screen = 'event';
                    $window = 'fullpopup';
                    $notifySetupCode = 'event_capacity';
                    break;
                case 'create_player':
                    $link = $router->link('team_public_players_list', array('team_id' => $team->getId()));
                     $mobileLink = WEB_DOMAIN.$router->generateApiWidgetUrl(
                            'api_content_webview',
                            array(
                                'apikey' => CR_API_KEY,
                                'view' => $router->link('team_public_players_list',array(
                                    'team_id' => $team->getId()
                                        )
                                ),
                                'user_id' => $teamPlayer->getPlayerId()));
                    $screen = 'home';
                    $window = 'fullpopup';
                    $notifySetupCode = 'create_player';
                    break;
                case 'delete_single_event':
                    $link = $router->link('team_event_list', array('team_id' => $team->getId(), 'type' => 'upcoming'));
                    $mobileLink = '';
                    $screen = 'events';
                    $window = 'webview';
                    $notifySetupCode = 'delete_event';
                    break;
                case 'delete_repeat_event':
                    $link = $router->link('team_event_list', array('team_id' => $team->getId(), 'type' => 'upcoming'));
                     $mobileLink = '';
                    $screen = 'events';
                    $window = 'webview';
                    $notifySetupCode = 'delete_event';
                    break;
                case 'delete_next_repeat_event':
                    $link = $router->link('team_event_list', array('team_id' => $team->getId(), 'type' => 'upcoming'));
                    $mobileLink = '';
                    $screen = 'events';
                    $window = 'webview';
                    $notifySetupCode = 'delete_event';
                    break;
                case 'ommit_repeat_event':
                    $link = $router->link('team_event_list', array('team_id' => $team->getId(), 'type' => 'upcoming'));
                    $mobileLink = '';
                    $screen = 'events';
                    $window = 'webview';
                    $notifySetupCode = 'delete_event';
                    break;
                case 'event_attendance_in':
                    $event = $data['event'];
                    $link = $router->link('team_event_detail', array('id' => $event->getId(), 'current_date' => $event->getCurrentDate()->format('Y-m-d')));
                    $mobileLink = WEB_DOMAIN.$router->generateApiWidgetUrl(
                            'api_content_webview',
                            array(
                                'apikey' => CR_API_KEY,
                                'view' => $router->link('team_event_detail',array(
                                    'id' => $event->getId(),
                                    'apikey' => CR_API_KEY,
                                    'user_id' => $teamPlayer->getPlayerId(),
                                    'current_date' => $event->getCurrentDate()->format('Y-m-d'))).'#attendance',
                                'user_id' => $teamPlayer->getPlayerId()));
                    $screen = 'event';
                    $window = 'fullpopup';
                    $notifySetupCode = 'event_attendance_in';
                    break;
                 case 'event_attendance_out':
                    $event = $data['event'];
                    $link = $router->link('team_event_detail', array('id' => $event->getId(), 'current_date' => $event->getCurrentDate()->format('Y-m-d')));
                    $mobileLink = WEB_DOMAIN.$router->generateApiWidgetUrl(
                            'api_content_webview',
                            array(
                                'apikey' => CR_API_KEY,
                                'view' => $router->link('team_event_detail',array(
                                    'id' => $event->getId(),
                                    'apikey' => CR_API_KEY,
                                    'user_id' => $teamPlayer->getPlayerId(),
                                    'current_date' => $event->getCurrentDate()->format('Y-m-d'))).'#attendance',
                                'user_id' => $teamPlayer->getPlayerId()));
                    $screen = 'event';
                    $window = 'fullpopup';
                    $notifySetupCode = 'event_attendance_in';
                    break;

                default:
                    break;
            }
            $metadata['link'] = $link;
            $metadata['mobile']['url'] = $mobileLink;
            $metadata['mobile']['screen'] = $screen;
            $metadata['mobile']['window'] = $window;
            $metadata['code'] = $code;
            $messageData['rest_body'] = json_encode($metadata);
            
            $notifyData =array();
            $notifyData['messageData'] = $messageData;
            $notifyData['notifySetupCode'] = $notifySetupCode;
           
            $notifyData['metadata'] = $metadata;
            $notifyData['recipient'] = $recipient;
            $notifyData['code'] = $code;
             
             
            $list[$teamPlayer->getPlayerId()] = $notifyData;
           }
        }
        return $list;
    }

    public function createPushNotify($code, $data)
    {
    
        $notifyList = $this->buildPushNotifyList($code, $data);


        
        foreach($notifyList as $recipientId => $notifyData)
        {
           
            //create notify message
            if (!array_key_exists('senderId', $data) or $recipientId != $data['senderId'])
            {
                if($data['senderId']> 0)
                {
                    $notifyData['messageData']['senderId'] = $data['senderId'];
                }
                
                
                $this->createMessage($notifyData['messageData'], $notifyData['recipient']);
            }

            if ($this->hasAllowedPushNotify($recipientId,$notifyData['notifySetupCode']) && $recipientId != $data['senderId'] )
            {
                $response = $this->sendPushNotify(array(
                    'recipient_id' => $recipientId,
                    'codeKey' => $notifyData['code'],
                    'push_data' => $notifyData['metadata']['mobile'],
                    'message_data' => $data
                ));

                //return $response;
            }
        }


    }
    
    public function buildPlayerLeaveTeamPushNotify($user, $data)
    {
        $router = \Core\ServiceLayer::getService('router');
        $recipient = new \CR\Message\Model\MessageRecipient();
        $recipient->setId($user->getId());
        $messageData['subject'] = $this->getMessageSubject('player_leave_team',$data);
        $messageData['body'] =  $this->getMessageBody('player_leave_team',$data);
        //$messageData['body'] =  str_replace('{PLAYER_NAME}', $data['player']->getFullName(), $messageData['body'] );


        $metadata['link'] = $router->link('scouting');
        $metadata['mobile']['url'] =  WEB_DOMAIN.$router->generateApiWidgetUrl(
                        'api_content_webview',
                        array(
                            'apikey' => CR_API_KEY,
                            'view' => $router->link('scouting'),
                            'user_id' =>$user->getId()));
        $metadata['mobile']['screen'] = 'home';
        $metadata['mobile']['window'] = 'fullpopup';
        $metadata['code'] = 'player_leave_team';
        $messageData['metadata'] = $metadata;
        $messageData['rest_body'] = json_encode($metadata);
        $messageData['recipient'] = $recipient;
      
        
        return  $messageData;
    }

    /**
     * Send notify to admins, when player leave team
     * @param type $user
     */
    public function sendPlayerLeaveTeamPushNotify($user, $data)
    {
        $notifyData = $this->buildPlayerLeaveTeamPushNotify($user, $data);

        $this->createMessage($notifyData, $notifyData['recipient']);
        if ($this->hasAllowedPushNotify($user->getId(), 'player_leave_team'))
        {
            return $this->sendPushNotify(array(
                'recipient_id' =>  $notifyData['recipient']->getId(),
                'codeKey' => 'player_leave_team',
                'push_data' => $notifyData['metadata']['mobile'],
                'message_data' => $data
            ));

        }
    }
    
     
    public function buildTeamInvitationDeclinePushNotify($user, $data)
    {
        $router = \Core\ServiceLayer::getService('router');
        $recipient = new \CR\Message\Model\MessageRecipient();
        $recipient->setId($user->getId());
        $messageData['subject'] = $this->getMessageSubject('team_invitation_decline');
        $messageData['body'] =  $this->getMessageBody('team_invitation_decline',$data);
        $messageData['body'] =  str_replace('{PLAYER_NAME}', $data['player']->getFullName(), $messageData['body'] );


        $metadata['link'] = $router->link('team_public_players_list', array('team_id' => $data['team']->getId()));
        $metadata['mobile']['url'] =  WEB_DOMAIN.$router->generateApiWidgetUrl(
                        'api_content_webview',
                        array(
                            'apikey' => CR_API_KEY,
                            'view' => $router->link('team_public_players_list', array('team_id' => $data['team']->getId())),
                            'user_id' =>$user->getId()));
        $metadata['mobile']['screen'] = 'home';
        $metadata['mobile']['window'] = 'fullpopup';
        $metadata['code'] = 'player_invitation_decline';
        $messageData['metadata'] = $metadata;
        $messageData['rest_body'] = json_encode($metadata);
        $messageData['recipient'] = $recipient;
         $messageData['team'] = $data['team'];
         $messageData['player'] = $data['player'];
      
        
        return  $messageData;
    }


      /**
     * Send notify to admins, when player decline invitation
     * @param type $user
     */
    public function sendTeamInvitationDeclinePushNotify($adminList, $data)
    {
          foreach($adminList as $admin)
        {
               $notifyData = $this->buildTeamInvitationDeclinePushNotify($admin, $data);
               $this->createMessage($notifyData, $notifyData['recipient']);

                return $this->sendPushNotify(array(
                    'recipient_id' =>  $notifyData['recipient']->getId(),
                    'codeKey' => 'team_invitation_decline',
                    'message_data' =>  $notifyData,
                    'push_data' => $notifyData['metadata']['mobile']
                ));
          }
    }
    
    
    
    /**
     * Send rated player notify 
     * @param type $user
     */
    public function sendPlayerRatingPushNotify($user, $data)
    {
        $router = \Core\ServiceLayer::getService('router');
        $eventId = $data['eventId'];
        $eventDate = $data['eventDate'];

        $recipient = new \CR\Message\Model\MessageRecipient();
        $recipient->setId($user->getId());
        $messageData['subject'] = $this->getMessageSubject('player_rating');
        $messageData['body'] = $this->getMessageBody('player_rating');

        $metadata['link'] = $router->link('team_event_rating', array('id' => $eventId, 'current_date' => $eventDate));
        $metadata['mobile']['url'] = WEB_DOMAIN . $router->generateApiWidgetUrl(
                        'api_content_webview', array(
                    'apikey' => CR_API_KEY,
                    'view' => $router->link('api_content_event_detail', array(
                        'id' => $eventId,
                        'apikey' => CR_API_KEY,
                        'user_id' => $user->getId(),
                        'current_date' => $eventDate)),
                    'user_id' => $user->getId()));
        $metadata['mobile']['screen'] = 'event';
        $metadata['mobile']['window'] = 'fullpopup';
        $metadata['mobile']['action'] = 'fullpopup';
        $metadata['code'] = 'player_rating';
        $messageData['rest_body'] = json_encode($metadata);

        $this->createMessage($messageData, $recipient);

        if ($this->hasAllowedPushNotify($user->getId(), 'player_rating'))
        {
            $this->sendPushNotify(array(
                'recipient_id' => $user->getId(),
                'codeKey' => 'player_rating',
                'push_data' => $metadata['mobile']
            ));
        }
    }

    /**
     * send notify about lineup creation to attendees
     * @param type $teamPlayers
     */
    public function sendCreateLineupPushNotify($teamPlayers, $data)
    {
        $router = \Core\ServiceLayer::getService('router');

        foreach ($teamPlayers as $teamPlayer)
        {
            $recipient = new \CR\Message\Model\MessageRecipient();
            $recipient->setId($teamPlayer->getPlayerId());
            $messageData['subject'] = $this->getMessageSubject('create_lineup');
            $messageData['body'] = $this->getMessageBody('create_lineup');

            $link = $router->link('team_match_view_lineup', array('id' => $data['lineupId']));
            $metadata['link'] = $link;
            $metadata['code'] = 'create_lineup';
            $messageData['rest_body'] = json_encode($metadata);


            $this->createMessage($messageData, $recipient);

            if ($this->hasAllowedPushNotify($teamPlayer->getPlayerId(), 'create_lineup'))
            {
                $this->sendPushNotify(array(
                    'recipient_id' => $teamPlayer->getPlayerId(),
                    'codeKey' => 'create_lineup'
                ));
            }
        }
    }

    /**
     * send notify about match closing to attendees
     * @param type $teamPlayers
     */
    public function sendCloseMatchPushNotify($teamPlayers, $data)
    {
        $router = \Core\ServiceLayer::getService('router');
       
        $eventId = $data['eventId'];
        $eventDate = $data['eventDate'];
        foreach ($teamPlayers as $teamPlayer)
        {

            $recipient = new \CR\Message\Model\MessageRecipient();
            $recipient->setId($teamPlayer->getPlayerId());
            $messageData['subject'] = $this->getMessageSubject('close_match');
            $messageData['body'] = $this->getMessageBody('close_match');
            
            
            $metadata['link'] = $router->link('team_event_rating', array('id' => $eventId, 'current_date' => $eventDate));
            $metadata['mobile']['url'] =  WEB_DOMAIN.$router->generateApiWidgetUrl(
                            'api_content_webview',
                            array(
                                'apikey' => CR_API_KEY,
                                'view' => $router->link('team_event_rating',array(
                                    'apikey' => CR_API_KEY,
                                    'user_id' =>$teamPlayer->getPlayerId(),
                                    'id' =>$eventId,
                                    'current_date' =>$eventDate,
                                    )),
                                'user_id' =>$teamPlayer->getPlayerId()));
            $metadata['mobile']['screen'] = 'event';
            $metadata['mobile']['window'] = 'fullpopup';
            $metadata['mobile']['action'] = 'fullpopup';
            $metadata['code'] = 'close_match';
            $messageData['rest_body'] = json_encode($metadata);

            $this->createMessage($messageData, $recipient);

            if ($this->hasAllowedPushNotify($teamPlayer->getPlayerId(),'close_match'))
            {
                $result = $this->sendPushNotify(array(
                    'recipient_id' => $teamPlayer->getPlayerId(),
                    'codeKey' => 'close_match',
                    'push_data' => $metadata['mobile']
                ));
            }
            return $result;
        }
    }
    
   
    
    public function buildMissingPlayerPushNotify($user,$event)
    {
        $router = \Core\ServiceLayer::getService('router');
        $recipient = new \CR\Message\Model\MessageRecipient();
        $recipient->setId($user->getId());
        $messageData['subject'] = $this->getMessageSubject('missing_players');
        $messageData['body'] = $this->getMessageBody('missing_players');


        $metadata['link'] = $router->link('team_event_detail', array('id' => $event->getId(), 'current_date' => $event->getCurrentDate()->format('Y-m-d')));
        $metadata['mobile']['url'] =  WEB_DOMAIN.$router->generateApiWidgetUrl(
                        'api_content_webview',
                        array(
                            'apikey' => CR_API_KEY,
                            'view' => $router->link('team_event_detail',array(
                                'id' => $event->getId(),
                                'apikey' => CR_API_KEY,
                                'user_id' =>$user->getId(),
                                'current_date' => $event->getCurrentDate()->format('Y-m-d'))).'#attendance',
                            'user_id' =>$user->getId()));
        $metadata['mobile']['screen'] = 'event';
        $metadata['mobile']['window'] = 'fullpopup';
        $metadata['code'] = 'missing_players';
         $messageData['metadata'] = $metadata;
        $messageData['recipient'] = $recipient;
        $messageData['rest_body'] = json_encode($metadata);
       
        
        return $messageData;
    }
    
    
    /**
     * send notify to admin, if attendance is not full
     * @param type $teamPlayers
     * @param Event $event
     */
    public function sendMissingPlayerPushNotify($users,$event)
    {
        
        
        foreach ($users as $user)
        {
            $notifyData = $this->buildMissingPlayerPushNotify($user,$event);

            $this->createMessage($notifyData, $notifyData['recipient']);
            if ($this->hasAllowedPushNotify($user->getId(), 'missing_players'))
            {
                $this->sendPushNotify(array(
                    'recipient_id' =>  $notifyData['recipient']->getId(),
                    'codeKey' => 'missing_players',
                    'push_data' => $notifyData['metadata']['mobile']
                ));

            }
        }
    }
    
    public function  buildPostCommentPushNotify($post,$author)
    {
        $repo =  \Core\ServiceLayer::getService('HpWallPostCommentRepository'); 
        $router = \Core\ServiceLayer::getService('router');
        $comments = $repo->findBy(array('hp.post_id' => $post->getId()));
        $recipientIds = array();
        $recipientIds[] = $post->getAuthorId();
        
        
        foreach($comments as $comment)
        {
            if($author->getId() != $comment->getAuthorId())
            {
                $recipientIds[] = $comment->getAuthorId();
            }
        }
        
        $list = array();
        
        foreach ($recipientIds as $recipientId)
        {

            $recipient = new \CR\Message\Model\MessageRecipient();
            $recipient->setId($recipientId);
            $messageData['subject'] = $this->getMessageSubject('hp_wall_post_comment');
            $messageData['body'] = $this->getMessageBody('hp_wall_post_comment',array('author' => $author));
            $messageData['author'] = $author;
            $messageData['post'] = $post;

           
            $metadata['link'] = $router->link('api_content_home', array('apikey' => CR_API_KEY, 'user_id' =>$recipientId,'a' => 'comments','post-edit' => $post->getId() ));
            //$metadata['mobile']['url'] = WEB_DOMAIN.$router->link('api_content_home', array('apikey' => CR_API_KEY, 'user_id' =>$recipientId,'a' => 'comments','post-edit' => $post->getId() ));
            $metadata['mobile']['url'] = 'https://app.coachrufus.com'.$router->link('api_content_home', array('apikey' => CR_API_KEY, 'user_id' =>$recipientId,'a' => 'comments','post-edit' => $post->getId() ));
            $metadata['mobile']['screen'] = 'home';
            $metadata['mobile']['window'] = 'fullpopup';
            $metadata['code'] = 'hp_wall_post_comment';
            $messageData['rest_body'] = json_encode($metadata);
            $this->createMessage($messageData, $recipient);
            
            $messageData['recipientId'] = $recipient->getId();
            $messageData['metadata'] = $metadata;

           $list[] = $messageData;
        }
        return $list;
    }
    
    public function sendPostCommentPushNotify($post,$author)
    {
        $list = $this->buildPostCommentPushNotify($post,$author);
        foreach($list as $notify)
        {
           
            if ($this->hasAllowedPushNotify($notify['recipientId'],'hp_wall_post'))
            {
                $result = $this->sendPushNotify(array(
                    'recipient_id' => $notify['recipientId'],
                    'codeKey' => 'hp_wall_post_comment',
                    'push_data' => $notify['metadata']['mobile'],
                    'message_data' => $notify,
                    'author' => $notify['author'],
                    'post' => $notify['post']
                ));
            }
        }
        
        return $result;
    }
    
  
    
    public function sendGeneratedPersonalPostPushNotify($teamPlayer)
    {
        $router = \Core\ServiceLayer::getService('router');
        $recipient = new \CR\Message\Model\MessageRecipient();
        $recipient->setId($teamPlayer->getPlayerId());
        $messageData['subject'] = $this->getMessageSubject('hp_wall_post_generated');
        $messageData['body'] = $this->getMessageBody('hp_wall_post_generated');
        //$messageData['author'] = $author;
        //$messageData['post'] = $post;


        $metadata['link'] = $router->link('api_content_home', array('apikey' => CR_API_KEY, 'user_id' => $teamPlayer->getPlayerId(),  'tab' => 'personal'));
        //$metadata['mobile']['url'] = WEB_DOMAIN.$router->link('api_content_home', array('apikey' => CR_API_KEY, 'user_id' =>$recipientId,'a' => 'comments','post-edit' => $post->getId() ));
        $metadata['mobile']['url'] = 'https://app.coachrufus.com' . $router->link('api_content_home', array('apikey' => CR_API_KEY, 'user_id' =>$teamPlayer->getPlayerId(), 'tab' => 'personal'));
        $metadata['mobile']['screen'] = 'home';
        $metadata['mobile']['window'] = 'webview';
        $metadata['code'] = 'hp_wall_post_generated';
        $messageData['rest_body'] = json_encode($metadata);
        $this->createMessage($messageData, $recipient);

        $messageData['recipientId'] = $teamPlayer->getPlayerId();
        $messageData['metadata'] = $metadata;
        

        
         $result = $this->sendPushNotify(array(
                    'recipient_id' =>$teamPlayer->getPlayerId(),
                    'codeKey' => 'hp_wall_post_generated',
                    'push_data' =>  $metadata['mobile'],
                    'message_data' => $messageData,
                ));
         
         return $result;
    }
    

    public function sendTeamInvitationPushNotify($data)
    {
       // $list = $this->buildPostCommentPushNotify($post,$author);
        $router = \Core\ServiceLayer::getService('router');
        $sender = $data['sender'];
        $team = $data['team'];
        $recipientData = $data['recipient'];
        

        
         $recipient = new \CR\Message\Model\MessageRecipient();
            $recipient->setId($recipientData->getId());
            $messageData['subject'] = $this->getMessageSubject('team_invitation').' '.$team->getName();
            $messageData['body'] = $this->getMessageBody('team_invitation',array('author' => $sender,'team' => $team));
            $messageData['author'] = $sender;
            $messageData['team'] = $team;

           
           
        $metadata['link'] = $router->link('api_content_home', array('apikey' => CR_API_KEY, 'user_id' => $recipientData->getId(),  'tab' => 'team'));
        //$metadata['mobile']['url'] = WEB_DOMAIN.$router->link('api_content_home', array('apikey' => CR_API_KEY, 'user_id' =>$recipientId,'a' => 'comments','post-edit' => $post->getId() ));
        $metadata['mobile']['url'] = 'https://app.coachrufus.com' . $router->link('api_content_home', array('apikey' => CR_API_KEY, 'user_id' =>$recipientData->getId(), 'tab' => 'team'));
        $metadata['mobile']['screen'] = 'home';
        $metadata['mobile']['window'] = 'webview';
        $metadata['code'] = 'team_invitation';
        $messageData['rest_body'] = json_encode($metadata);
        $this->createMessage($messageData, $recipient);
        
        $messageData['recipientId'] = $recipientData->getId();
        $messageData['metadata'] = $metadata;
        

        
         $result = $this->sendPushNotify(array(
                    'recipient_id' =>$recipientData->getId(),
                    'codeKey' => 'team_invitation',
                    'push_data' =>  $metadata['mobile'],
                    'message_data' => $messageData,
                ));
         
         return $result;
    }

        

    public function buildTournamentJoinAdminNotify($tournament,$team)
    {
        $router = \Core\ServiceLayer::getService('router');
        $tournamentAdminIds = $tournament->getAdminList();
        $userRepo = \Core\ServiceLayer::getService('user_repository');
        $list = array();
        foreach ($tournamentAdminIds as $tournamentAdminId)
        {
            $user = $userRepo->find($tournamentAdminId);
            $recipient = new \CR\Message\Model\MessageRecipient();
            $recipient->setId($user->getId());

            //$data = array();
            $messageData['team'] = $team;
            $messageData['tournament'] = $tournament;

            $messageData['subject'] = $this->getMessageSubject('tournament_join_admin',$messageData);
            $messageData['body'] = $this->getMessageBody('tournament_join_admin',$messageData);

            $metadata['link'] = $router->link('tournament_detail', array('id' => $tournament->getId()));
            $metadata['mobile']['url'] =  'https://app.coachrufus.com'.$router->generateApiWidgetUrl(
                            'api_content_webview',
                            array(
                                'apikey' => CR_API_KEY,
                                'view' => $router->link('tournament_detail',array(
                                    'id' => $tournament->getId(),
                                    'apikey' => CR_API_KEY,
                                    'user_id' =>$user->getId(),
                                    )),
                                'user_id' =>$user->getId()));
            $metadata['mobile']['screen'] = 'home';
            $metadata['mobile']['window'] = 'fullpopup';
             $metadata['mobile']['action'] = 'fullpopup';
            $metadata['code'] = 'tournament_join_admin';
            $messageData['metadata'] = $metadata;
            $messageData['recipient'] = $recipient;
            $messageData['rest_body'] = json_encode($metadata);
            $this->createMessage($messageData, $user);
            $list[] = $messageData;
        }
        return $list;
    }

    
    public function sendTournamentJoinAdminNotify($tournament, $team)
    {
        $notifyList = $this->buildTournamentJoinAdminNotify($tournament, $team);
        foreach($notifyList as $notifyData)
        {
            $result = $this->sendPushNotify(array(
                'recipient_id' => $notifyData['recipient']->getId(),
                'codeKey' => 'tournament_join_admin',
                'push_data' => $notifyData['metadata']['mobile'],
                'message_data' => $notifyData,
            ));
        }
        return $result;
    }
    
    public function buildTournamentTeamConfirmedNotify($tournament, $team)
    {
        $router = \Core\ServiceLayer::getService('router');
        $teamManager = \Core\ServiceLayer::getService('TeamManager');
        $teamAdmins = $teamManager->getTeamAdminList($team);
        $userRepo = \Core\ServiceLayer::getService('user_repository');
        $list = array();
        foreach ($teamAdmins as $teamAdmin)
        {
            $user = $userRepo->find($teamAdmin->getPlayerId());
            
            if(null != $user)
            {
                $recipient = new \CR\Message\Model\MessageRecipient();
                $recipient->setId($user->getId());

                //$data = array();
                $messageData['team'] = $team;
                $messageData['tournament'] = $tournament;

                $messageData['subject'] = $this->getMessageSubject('tournament_team_confirmed',$messageData);
                $messageData['body'] = $this->getMessageBody('tournament_team_confirmed',$messageData);

                $metadata['link'] = $router->link('tournament_detail', array('id' => $tournament->getId()));
                $metadata['mobile']['url'] =  'https://app.coachrufus.com'.$router->generateApiWidgetUrl(
                                'api_content_webview',
                                array(
                                    'apikey' => CR_API_KEY,
                                    'view' => $router->link('tournament_detail',array(
                                        'id' => $tournament->getId(),
                                        'apikey' => CR_API_KEY,
                                        'user_id' =>$user->getId(),
                                        )),
                                    'user_id' =>$user->getId()));
                $metadata['mobile']['screen'] = 'home';
                $metadata['mobile']['window'] = 'fullpopup';
                 $metadata['mobile']['action'] = 'fullpopup';
                $metadata['code'] = 'tournament_team_confirmed';
                $messageData['metadata'] = $metadata;
                $messageData['recipient'] = $recipient;
                $messageData['rest_body'] = json_encode($metadata);
                $this->createMessage($messageData, $user);
                $list[] = $messageData;
            }
        }
        return $list;
    }
    
    public function sendTournamentTeamConfirmedNotify($tournament, $team)
    {
        $notifyList = $this->buildTournamentTeamConfirmedNotify($tournament, $team);
        foreach($notifyList as $notifyData)
        {
            $result = $this->sendPushNotify(array(
                'recipient_id' => $notifyData['recipient']->getId(),
                'codeKey' => 'tournament_team_confirmed',
                'push_data' => $notifyData['metadata']['mobile'],
                'message_data' => $notifyData,
            ));
        }
        return $result;
    }
    
     public function buildTournamentTeamRejectedNotify($tournament, $team)
    {
        $router = \Core\ServiceLayer::getService('router');
        $teamManager = \Core\ServiceLayer::getService('TeamManager');
        $teamAdmins = $teamManager->getTeamAdminList($team);
        $userRepo = \Core\ServiceLayer::getService('user_repository');
        $list = array();
        foreach ($teamAdmins as $teamAdmin)
        {
            $user = $userRepo->find($teamAdmin->getPlayerId());
            
            if(null != $user)
            {
                $recipient = new \CR\Message\Model\MessageRecipient();
                $recipient->setId($user->getId());

                //$data = array();
                $messageData['team'] = $team;
                $messageData['tournament'] = $tournament;

                $messageData['subject'] = $this->getMessageSubject('tournament_team_rejected',$messageData);
                $messageData['body'] = $this->getMessageBody('tournament_team_rejected',$messageData);

                $metadata['link'] = $router->link('tournament_detail', array('id' => $tournament->getId()));
                $metadata['mobile']['url'] =  'https://app.coachrufus.com'.$router->generateApiWidgetUrl(
                                'api_content_webview',
                                array(
                                    'apikey' => CR_API_KEY,
                                    'view' => $router->link('tournament_detail',array(
                                        'id' => $tournament->getId(),
                                        'apikey' => CR_API_KEY,
                                        'user_id' =>$user->getId(),
                                        )),
                                    'user_id' =>$user->getId()));
                $metadata['mobile']['screen'] = 'home';
                $metadata['mobile']['window'] = 'fullpopup';
                 $metadata['mobile']['action'] = 'fullpopup';
                $metadata['code'] = 'tournament_team_rejected';
                $messageData['metadata'] = $metadata;
                $messageData['recipient'] = $recipient;
                $messageData['rest_body'] = json_encode($metadata);
                $this->createMessage($messageData, $user);
                $list[] = $messageData;
            }
        }
        return $list;
    }
    
     public function sendTournamentTeamRejectedNotify($tournament, $team)
    {
        $notifyList = $this->buildTournamentTeamRejectedNotify($tournament, $team);
        foreach($notifyList as $notifyData)
        {
            $result = $this->sendPushNotify(array(
                'recipient_id' => $notifyData['recipient']->getId(),
                'codeKey' => 'tournament_team_rejected',
                'push_data' => $notifyData['metadata']['mobile'],
                'message_data' => $notifyData,
            ));
        }
        return $result;
    }
    
    
     public function buildPlayerInvitationPushNotify($user, $data)
    {
        $router = \Core\ServiceLayer::getService('router');
        $recipient = new \CR\Message\Model\MessageRecipient();
        $recipient->setId($user->getId());
        $messageData['subject'] = $this->getMessageSubject('player_invitation',$data);
        $messageData['body'] =  $this->getMessageBody('player_invitation',$data);

        $metadata['link'] = $router->link('team_players_list',array('team_id' => $data['team']->getId())).'#sp_'.$data['player']->getId();
        $metadata['mobile']['url'] =  WEB_DOMAIN.$router->generateApiWidgetUrl(
                        'api_content_webview',
                        array(
                            'apikey' => CR_API_KEY,
                            'view' => $router->link('team_players_list',array('team_id' => $data['team']->getId())).'#sp_'.$data['player']->getId(),
                            'user_id' =>$user->getId()));
        $metadata['mobile']['screen'] = 'home';
        $metadata['mobile']['window'] = 'fullpopup';
        $metadata['code'] = 'player_invitation';
        $messageData['metadata'] = $metadata;
        $messageData['rest_body'] = json_encode($metadata);
        $messageData['recipient'] = $recipient;
      
        
        return  $messageData;
    }

    /**
     * Send notify to admins, when player want to by member of team
     * @param type $user
     */
    public function sendPlayerInvitationPushNotify($user, $data)
    {
        $notifyData = $this->buildPlayerInvitationPushNotify($user, $data);

        $this->createMessage($notifyData, $notifyData['recipient']);
        return  $this->sendPushNotify(array(
                'recipient_id' =>  $notifyData['recipient']->getId(),
                'codeKey' => 'player_invitation',
                'push_data' => $notifyData['metadata']['mobile'],
                'message_data' => $data
            ));

    }
    
    
    public function buildTeamJoinRequestNotify($user,$type,$data)
    {
        $router = \Core\ServiceLayer::getService('router');
        $recipient = new \CR\Message\Model\MessageRecipient();
        $recipient->setId($user->getId());
        
        
        if('refused' == $type)
        {
            $messageData['subject'] = $this->getMessageSubject('team_join_request_refused',$data);
            $messageData['body'] =  $this->getMessageBody('team_join_request_refused',$data);
            $code = 'team_join_request_refused';
            
        }
        
         if('accepted' == $type)
        {
            $messageData['subject'] = $this->getMessageSubject('team_join_request_accepted',$data);
            $messageData['body'] =  $this->getMessageBody('team_join_request_accepted',$data);
             $code = 'team_join_request_accepted';
        }
        
        
        

        $metadata['link'] = $router->link('api_content_home', array('apikey' => CR_API_KEY, 'user_id' => $user->getId(),  'tab' => 'team'));
        $metadata['mobile']['url'] =  WEB_DOMAIN.$router->generateApiWidgetUrl(
                        'api_content_webview',
                        array(
                            'apikey' => CR_API_KEY,
                            'view' => $router->link('api_content_home', array('apikey' => CR_API_KEY, 'user_id' => $user->getId(),  'tab' => 'team')),
                            'user_id' =>$user->getId()));
        $metadata['mobile']['screen'] = 'home';
        $metadata['mobile']['window'] = 'webview';
        $metadata['code'] = $code;
        $messageData['metadata'] = $metadata;
        $messageData['rest_body'] = json_encode($metadata);
        $messageData['recipient'] = $recipient;
      
        
        return  $messageData;
    }
    
    public function sendTeamJoinRequestNotify($user,$type,$data)
    {
        $notifyData = $this->buildTeamJoinRequestNotify($user,$type,$data);
         if('refused' == $type) {
             $code = 'team_join_request_refused';
         }
         if('accepted' == $type) {
             $code = 'team_join_request_accepted';
         }
        

        $this->createMessage($notifyData, $notifyData['recipient']);
        return  $this->sendPushNotify(array(
                'recipient_id' =>  $notifyData['recipient']->getId(),
                'codeKey' => $code,
                'push_data' => $notifyData['metadata']['mobile'],
                'message_data' => $data
            ));

    }
    
     public function buildGameEventNotify($data)
    {
        $router = \Core\ServiceLayer::getService('router');
        $list = array();
        
        /*
        $statManager = \Core\ServiceLayer::getService('PlayerStatManager');
        $matchOverview = $statManager->getMatchOverview($data['lineup']);
        $firstLineGoal = ($matchOverview->getFirstLineGoals() == null) ? 0 : $matchOverview->getFirstLineGoals();
        $secondLineGoal = ($matchOverview->getSecondLineGoals() == null) ? 0 : $matchOverview->getSecondLineGoals();

        
        $scoreFirstLine = ($data['scoringTeamId'] == $data['lineup']->getTeamId()) ? '['.$firstLineGoal.']' : $firstLineGoal;
        $scoreSecondLine = ($data['scoringTeamId'] == $data['lineup']->getOpponentTeamId()) ? '['.$secondLineGoal.']' : $secondLineGoal;
        
         $score = $scoreFirstLine.':'.$scoreSecondLine;
         $data['score'] = $score;
         * 
         */
         $subscribedUsers = \Core\ServiceLayer::getService('TournamentRepository')->getSubscribedEventUsers($data['lineup']->getId());
         
         foreach($subscribedUsers as $subscribedUser)
         {
            $recipient = new \CR\Message\Model\MessageRecipient();
            $recipient->setId($subscribedUser['user_id']);


            $messageData['subject'] = $this->getMessageSubject('game_event',$data);
            $messageData['body'] =  $this->getMessageBody('game_event',$data);
            $code = 'game_event';



            $metadata['link'] = $router->link('api_content_home', array('apikey' => CR_API_KEY, 'user_id' => $subscribedUser['user_id'],  'tab' => 'team'));
            $metadata['mobile']['url'] =  WEB_DOMAIN.$router->generateApiWidgetUrl(
                            'api_content_webview',
                            array(
                                'apikey' => CR_API_KEY,
                                'view' => $router->link('api_content_home', array('apikey' => CR_API_KEY, 'user_id' => $subscribedUser['user_id'],  'tab' => 'team')),
                                'user_id' =>$subscribedUser['user_id']));
            $metadata['mobile']['screen'] = 'home';
            $metadata['mobile']['window'] = 'webview';
            $metadata['code'] = $code;
            $messageData['metadata'] = $metadata;
            $messageData['rest_body'] = json_encode($metadata);
            $messageData['recipient'] = $recipient;
            
            $list[] = $messageData;
         }
        

        return  $list;
    }
    
    
    public function sendGameEventPushNotify($data)
    {
        
        
        
        $statManager = \Core\ServiceLayer::getService('PlayerStatManager');
        $matchOverview = $statManager->getMatchOverview($data['lineup']);
        $firstLineGoal = ($matchOverview->getFirstLineGoals() == null) ? 0 : $matchOverview->getFirstLineGoals();
        $secondLineGoal = ($matchOverview->getSecondLineGoals() == null) ? 0 : $matchOverview->getSecondLineGoals();

        
        $scoreFirstLine = ($data['scoringTeamId'] == $data['lineup']->getTeamId()) ? '['.$firstLineGoal.']' : $firstLineGoal;
        $scoreSecondLine = ($data['scoringTeamId'] == $data['lineup']->getOpponentTeamId()) ? '['.$secondLineGoal.']' : $secondLineGoal;
        
         $score = $scoreFirstLine.':'.$scoreSecondLine;
         $data['score'] = $score;
        
        $notifyList = $this->buildGameEventNotify($data);
        
        foreach($notifyList as $notifyData)
        {
            
            
            $result =  $this->sendPushNotify(array(
                'recipient_id' =>  $notifyData['recipient']->getId(),
                'codeKey' => 'game_event',
                'push_data' => $notifyData['metadata']['mobile'],
                'message_data' => $data
            ));
        }
        return $result;
        
    
    }

}

