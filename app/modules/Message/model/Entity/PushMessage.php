<?php

namespace CR\Message\Model;

use Core\EntityMapper as EntityMapper;
use Core\ServiceLayer as ServiceLayer;

class PushMessage {

    protected $subject;
    protected $body;
    protected $recipientId;
    protected $data;

    public function getSubject()
    {
        return $this->subject;
    }

    public function getBody()
    {
        return $this->body;
    }

    public function getRecipientId()
    {
        return $this->recipientId;
    }

    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    public function setBody($body)
    {
        $this->body = $body;
    }

    public function setRecipientId($recipientId)
    {
        $this->recipientId = $recipientId;
    }
    
    function getData() {
return $this->data;
}

 function setData($data) {
$this->data = $data;
}



}
