<?php

namespace CR\Message\Model;
use Core\Collections\Queue;
class SystemNotifyCollection  {
    protected $allItemsLoaded = false;
    protected $items;
    
     function __construct($items = [])
    {
        $this->items = $items;
    }

    public function getItems()
    {
        return $this->items;
    }

    public function setItems($items)
    {
        $this->items = $items;
    }

    public function getAllItemsLoaded()
    {
        return $this->allItemsLoaded;
    }

    public function setAllItemsLoaded($allItemsLoaded)
    {
        $this->allItemsLoaded = $allItemsLoaded;
    }

    public function allItemsLoaded()
    {
        return $this->allItemsLoaded;
    }
    
    public function addItem($index,$item)
    {
        $this->items[$index] = $item;
    }
    
    public function isEmpty()
    {
        if(empty($this->items))
        {
            return true;
        }
        return false;
    }

}
