<?php

namespace CR\Message\Model;

use Core\EntityMapper as EntityMapper;
use Core\ServiceLayer as ServiceLayer;

class Message {

    private $repository;
    private $mapper_rules = array(
        'id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => true
        ),
        'created_at' => array(
            'type' => 'datetime',
            'max_length' => '',
            'required' => false
        ),
        'subject' => array(
            'type' => 'string',
            'max_length' => '100',
            'required' => false
        ),
        'body' => array(
            'type' => 'string',
            'max_length' => '2000',
            'required' => false
        ),
        'sender_id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'reply_id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'sender_name' => array(
            'type' => 'string',
            'max_length' => '255',
            'required' => false
        ),
        'sender_email' => array(
            'type' => 'string',
            'max_length' => '255',
            'required' => false
        ),
        'recipient_name' => array(
            'type' => 'string',
            'max_length' => '255',
            'required' => false
        ),
        'recipient_email' => array(
            'type' => 'string',
            'max_length' => '255',
            'required' => false
        ),
        'recipient_id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'status' => array(
            'type' => 'string',
            'max_length' => '50',
            'required' => false
        ),
        'type' => array(
            'type' => 'string',
            'max_length' => '50',
            'required' => false
        ),
        'team_id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'recipient_team_player_id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'sender_team_player_id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'reply_group' => array(
            'type' => 'string',
            'max_length' => '11',
            'required' => false
        ),
        'body_data' => array(
            'type' => 'string',
            'max_length' => '11',
            'required' => false
        ),
        'rest_body' => array(
            'type' => 'string',
            'max_length' => '11',
            'required' => false
        ),
         'team_photo' => array(
            'type' => 'non-persist',
            'max_length' => '11',
            'required' => false
        ),
    );

    public function getDataMapperRules()
    {
        return $this->mapper_rules;
    }

    protected $id;
    protected $createdAt;
    protected $subject;
    protected $body;
    protected $senderId;
    protected $replyId;
    protected $senderName;
    protected $senderEmail;
    protected $recipientId;
    protected $recipientName;
    protected $recipientEmail;
    protected $status;
    protected $type = 'message';
    protected $teamId;
    protected $recipientTeamPlayerId;
    protected $senderTeamPlayerId;
    protected $replyGroup;
    protected $bodyData;
    protected $restBody;
    protected $teamPhoto;
    protected $userPhoto;

    public function setId($val)
    {
        $this->id = $val;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setCreatedAt($val)
    {
        $this->createdAt = $val;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setSubject($val)
    {
        $this->subject = $val;
    }

    public function getSubject()
    {
        return $this->subject;
    }

    public function setBody($val)
    {
        $this->body = $val;
    }

    public function getBody()
    {
        return $this->body;
    }

    public function setSenderId($val)
    {
        $this->senderId = $val;
    }

    public function getSenderId()
    {
        return $this->senderId;
    }

    public function setRecipientId($val)
    {
        $this->recipientId = $val;
    }

    public function getRecipientId()
    {
        return $this->recipientId;
    }

    public function setStatus($val)
    {
        $this->status = $val;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setType($val)
    {
        $this->type = $val;
    }

    public function getType()
    {
        return $this->type;
    }

    function getTeamId()
    {
        return $this->teamId;
    }

    function setTeamId($teamId)
    {
        $this->teamId = $teamId;
    }

    function getSenderName()
    {
        return $this->senderName;
    }

    function getSenderEmail()
    {
        return $this->senderEmail;
    }

    function getRecipientName()
    {
        return $this->recipientName;
    }

    function getRecipientEmail()
    {
        return $this->recipientEmail;
    }

    function setSenderName($senderName)
    {
        $this->senderName = $senderName;
    }

    function setSenderEmail($senderEmail)
    {
        $this->senderEmail = $senderEmail;
    }

    function setRecipientName($recipientName)
    {
        $this->recipientName = $recipientName;
    }

    function setRecipientEmail($recipientEmail)
    {
        $this->recipientEmail = $recipientEmail;
    }

    function getRecipientTeamPlayerId()
    {
        return $this->recipientTeamPlayerId;
    }

    function getSenderTeamPlayerId()
    {
        return $this->senderTeamPlayerId;
    }

    function setRecipientTeamPlayerId($recipientTeamPlayerId)
    {
        $this->recipientTeamPlayerId = $recipientTeamPlayerId;
    }

    function setSenderTeamPlayerId($senderTeamPlayerId)
    {
        $this->senderTeamPlayerId = $senderTeamPlayerId;
    }

    function getReplyGroup()
    {
        return $this->replyGroup;
    }

    function setReplyGroup($replyGroup)
    {
        $this->replyGroup = $replyGroup;
    }

    function getReplyId()
    {
        return $this->replyId;
    }

    function setReplyId($replyId)
    {
        $this->replyId = $replyId;
    }

    public function getBodyData()
    {
        return $this->bodyData;
    }

    public function setBodyData($bodyData)
    {
        $this->bodyData = $bodyData;
    }

    public function getRestBody()
    {
        return $this->restBody;
    }

    public function setRestBody($restBody)
    {
        $this->restBody = $restBody;
    }
    
    function getTeamPhoto()
    {
        if(null == $this->teamPhoto)
        {
            $this->teamPhoto = '/img/team/users.svg';
        }
        else
        {
            $this->teamPhoto = '/img/team/thumb_320_320/'.$this->teamPhoto;
        }
        return  $this->teamPhoto;
    }
    
    function getUserPhoto()
    {
        return $this->userPhoto;
    }

    function setUserPhoto($userPhoto)
    {
        $this->userPhoto = $userPhoto;
    }

    function setTeamPhoto($teamPhoto)
    {
        $this->teamPhoto = $teamPhoto;
    }

    public function getPlainBody()
    {
        return strip_tags($this->getBody());    
    }
    
     public function getTruncatedBody()
    {
        return mb_substr($this->getBody(), 0,40).'...';
    }
    
    public function getLink()
    {
        $metadata = json_decode($this->getRestBody(),true);
        return $metadata['link'];
    }
    
     public function getMobileLinkData()
    {
        $metadata = json_decode($this->getRestBody(),true);
        return $metadata['mobile'];
    }
    
    
    
}
