<?php

namespace CR\Message\Model;

use Core\Manager;

class MessageManager extends Manager {

    public function getRecipientMessages($recipient,$criteria)
    {
        return $this->getRepository()->getRecipientMessages($recipient,$criteria);
    }
    
    public function getSenderMessages($sender,$criteria)
    {
        return $this->getRepository()->getSenderMessages($sender,$criteria);
    }
    
    public function getRecipientMessagesInfo($recipient)
    {
        $data=  $this->getRepository()->getRecipientMessagesInfo($recipient);
        $info = array('unread' => null);
        $sum = 0;
        foreach($data as $key => $i)
        {
            $sum += $i['count'];
            $info[$i['status']] = intval($i['count']);
        }
        
        $info['total'] = $sum;
        return $info;
    }
    
    public function getSenderMessagesInfo($sender)
    {
        $data =  $this->getRepository()->getSenderMessagesInfo($sender);
        return array('total' => $data[0]['count']);
    }
    
    public function saveMessage($message)
    {
        return $this->getRepository()->save($message);
    }
    
    public function deleteMessage($message)
    {
        $this->getRepository()->delete($message->getId());
    }
    
    public function readMessage($message)
    {
        $message->setStatus('read');
        $this->getRepository()->save($message);
    }
    
    public function unreadMessage($message)
    {
        $message->setStatus('unread');
        $this->getRepository()->save($message);
    }
    
    public function getMessageById($id)
    {
        return $this->getRepository()->find($id);
    }
    
    public function getReplyGroupMessages($replyGroup)
    {
         return $this->getRepository()->findBy(array('reply_group' => $replyGroup));
    }
    
    public function createMessage($messageData,$sender,$recipient)
    {
        //sender message
        $message = new Message();
        $message->setBody($messageData->getBody());
        $message->setCreatedAt(new \DateTime());
        $message->setStatus('unread');
        $message->setSubject($messageData->getSubject());
        $message->setRecipientName($recipient->getFullName());
        $message->setRecipientEmail($recipient->getEmail());
        $message->setSenderId($sender->getId());
        $message->setSenderName($sender->getFullname());
        $message->setSenderEmail($sender->getEmail());
        $this->saveMessage($message);

        //recipient message
        $message = new Message();
        $message->setBody($messageData->getBody());
        $message->setCreatedAt(new \DateTime());
        $message->setStatus('unread');
        $message->setSubject($messageData->getSubject());

        $message->setRecipientName($recipient->getFullName());
        $message->setRecipientEmail($recipient->getEmail());
        $message->setRecipientId($recipient->getId());
        $message->setReplyId($sender->getId());
        $message->setSenderName($sender->getFullname());
        $message->setSenderEmail($sender->getEmail());

        $message->setReplyGroup($messageData->getReplyGroup());
        $mid = $this->saveMessage($message);
        return $mid;
    }
    
    public function sendMessageNotify($message)
    {
        $notifyData = array();
        $notifyData['lang'] = 'sk';
        $notifyData['sender_name'] = $message->getSenderName();
        $notifyData['email'] =  $message->getRecipientEmail();
        $notifyData['mid'] = $message->getId();
        $notificationManger = \Core\ServiceLayer::getService('notification_manager');
        $notificationManger->sendMessageNewEmail($notifyData);
    }
    
    public function deleteAllUserMessages($user)
    {
        $this->getRepository()->deleteBy(array('recipient_id' => $user->getId()));
    }
    
    public function getUnreadMessages($recipient,$limit = 4)
    {
        //return $this->getRepository()->findBy(array('status' => 'unread','recipient_id' => $user->getId() ));
        $criteria = array(
            'order_col'  => 'id',
            'order_dir' => 'asc',
            'from' => 0,
            'length' => $limit,
            'status' => 'unread'
            );
        return $this->getRepository()->getRecipientMessages($recipient,$criteria);
    }
    
    
    
    
    /*
    public function searchSenderAvailableRecipients($phrase,$sender)
    {
        return $this->getRepository()->searchSenderAvailableRecipients($phrase,$sender);
    }
    */
}
