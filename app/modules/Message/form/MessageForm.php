<?php
namespace  CR\Message\Form;
use \Core\Form as Form;
use  CR\Message\Model\Message as Message;


class MessageForm extends Form 
{
	protected $fields = array (
  'id' => 
  array (
    'type' => 'int',
    'max_length' => '11',
    'required' => true,
  ),
  'created_at' => 
  array (
    'type' => 'datetime',
    'max_length' => '',
    'required' => false,
  ),
  'subject' => 
  array (
    'type' => 'string',
    'max_length' => '100',
    'required' => false,
  ),
  'body' => 
  array (
    'type' => 'string',
    'max_length' => '2000',
    'required' => false,
  ),
  'sender_id' => 
  array (
    'type' => 'int',
    'max_length' => '11',
    'required' => false,
  ),
  'recipient_id' => 
  array (
    'type' => 'choice',
    'max_length' => '11',
    'required' => false,
  ),
  'status' => 
  array (
    'type' => 'string',
    'max_length' => '50',
    'required' => false,
  ),
  'type' => 
  array (
    'type' => 'string',
    'max_length' => '50',
    'required' => false,
  ),
  'team_id' => 
  array (
    'type' => 'int',
    'max_length' => '11',
    'required' => false,
  ),
  'team_player_id' => 
  array (
    'type' => 'int',
    'max_length' => '11',
    'required' => false,
  ),
  'email' => 
  array (
    'type' => 'int',
    'max_length' => '11',
    'required' => false,
  ),
);
				
	public function __construct() 
	{

	}
}