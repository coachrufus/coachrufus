<?php

namespace Notification\Model;

use Core\EntityMapper as EntityMapper;
use Core\ServiceLayer as ServiceLayer;

class Notification
{
    
    private $repository;
    private $mapper_rules = array('id' => array('type' => 'int', 'max_length' => '11', 'required' => true), 'demand_status_id' => array('type' => 'int', 'max_length' => '11', 'required' => false), 'notification_type' => array('type' => 'string', 'max_length' => '128', 'required' => true), 'subject' => array('type' => 'string', 'max_length' => '150', 'required' => true), 'message' => array('type' => 'string', 'max_length' => '', 'required' => false), 'is_active' => array('type' => 'boolean', 'max_length' => '', 'required' => true), 'lang' => array('type' => 'string', 'max_length' => '20', 'required' => false));
    protected $id;
    protected $demandStatusId;
    protected $notificationType;
    protected $subject;
    protected $message;
    protected $isActive;
    protected $statusName;
    protected $recipient;
    protected $recipientName;
    protected $bcc;
    protected $senderEmail;
    protected $senderName;
    protected $lang;
    
    public function getDataMapperRules()
    {
        return $this->mapper_rules;
    }
    
    public function getRepository()
    {
        if (null == $this->repository) {
            $this->repository = ServiceLayer::getService("notification_repository");
        }
        return $this->repository;
    }
    
    public function setRepository($repository)
    {
        $this->repository = $repository;
    }
    
    public function setId($val)
    {
        $this->id = $val;
    }
    
    public function getId()
    {
        return $this->id;
    }
    
    public function setDemandStatusId($val)
    {
        $this->demandStatusId = $val;
    }
    
    public function getDemandStatusId()
    {
        return $this->demandStatusId;
    }
    
    public function setNotificationType($val)
    {
        $this->notificationType = $val;
    }
    
    public function getNotificationType()
    {
        return $this->notificationType;
    }
    
    public function setSubject($val)
    {
        $this->subject = $val;
    }
    
    public function getSubject()
    {
        return $this->subject;
    }
    
    public function setMessage($val)
    {
        $this->message = $val;
    }
    
    public function getMessage()
    {
        return $this->message;
    }
    
    public function setIsActive($val)
    {
        $this->isActive = $val;
    }
    
    public function getIsActive()
    {
        return $this->isActive;
    }
    
    public function getRecipient()
    {
        return $this->recipient;
    }
    
    public function setRecipient($recipient)
    {
        $this->recipient = $recipient;
    }
    
    public function getStatusName()
    {
        return $this->statusName;
    }
    
    public function setStatusName($statusName)
    {
        $this->statusName = $statusName;
    }
    
    /**
     * bcc
     * @return unkown
     */
    public function getBcc()
    {
        return $this->bcc;
    }
    
    /**
     * bcc
     * @param unkown $bcc
     * @return Notification
     */
    public function setBcc($bcc)
    {
        $this->bcc = $bcc;
        return $this;
    }
    
    public function getSenderEmail()
    {
        return $this->senderEmail;
    }
    
    public function getSenderName()
    {
        return $this->senderName;
    }
    
    public function setSenderEmail($senderEmail)
    {
        $this->senderEmail = $senderEmail;
    }
    
    public function setSenderName($senderName)
    {
        $this->senderName = $senderName;
    }
    
    public function getLang()
    {
        return $this->lang;
    }
    
    public function setLang($lang)
    {
        $this->lang = $lang;
    }
    
    public function getRecipientName()
    {
        return $this->recipientName;
    }
    
    public function setRecipientName($recipientName)
    {
        $this->recipientName = $recipientName;
    }
}