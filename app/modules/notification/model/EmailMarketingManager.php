<?php

namespace Notification\Model;

use Core\ServiceLayer;
use const WEB_DOMAIN;

class EmailMarketingManager {

    private $provider;
    private $campaignId = 'T7rPO';

    public function __construct($provider)
    {
        $this->provider = $provider;
    }

    public function getProvider()
    {
        return $this->provider;
    }

    public function getCampaignId()
    {
        return $this->campaignId;
    }

    public function setCampaignId($campaignId)
    {
        $this->campaignId = $campaignId;
    }

    public function setProvider($provider)
    {
        $this->provider = $provider;
    }

    public function createUser($user)
    {
        $userData = array();
        if(null != trim($user->getFullName()))
        {
             $userData['name'] = $user->getFullName();
        }
       
        $userData['email'] = $user->getEmail();
        $userData['campaign']['campaignId'] = $this->getCampaignId();
        $result = $this->getProvider()->addContact($userData);
        return $result;
    }
    
    public function addUserTag($user,$tagName)
    {
        //first check user tags
        return;
        $provider = $this->getProvider();
        $userInfo = $provider->getContactByEmail($user->getEmail());
        $fullUserInfo = $provider->getContact($userInfo->contactId);
        $userTags = array();
        foreach($fullUserInfo->tags as $existTag)
        {
            $userTags[] = array('tagId' => $existTag->tagId);
        }

        $tags = $this->getProvider()->getTags();
        foreach($tags as $tag)
        {
            if($tag->name == $tagName)
            {
                //$tagId = $tag->tagId;
                 $userTags[] = array('tagId' =>  $tag->tagId);
            }
        }
        $updateTags = array('tags' => $userTags);
        $result = $provider->updateContact($userInfo->contactId ,$updateTags);
        return $result;
    }
    
    public function removeUserTag($user,$tagName)
    {
         return;
        $provider = $this->getProvider();
        $userInfo = $provider->getContactByEmail($user->getEmail());
        $fullUserInfo = $provider->getContact($userInfo->contactId);
        $userTags = array();
        foreach($fullUserInfo->tags as $existTag)
        {
            if($tagName !=$existTag->name )
            {
                $userTags[] = array('tagId' => $existTag->tagId);
            }
        }

        $updateTags = array('tags' => $userTags);
        $result = $provider->updateContact($userInfo->contactId ,$updateTags);
        return $result;
    }

    
    /**
     * Send tags and custom field after user registration
     * @param type $user
     */
    public function setAfterRegisterSetup($user)
    {
         return;
        $router = ServiceLayer::getService('router');
        $this->createUser($user);
        $this->addUserTag($user,'user_noteam');
        $this->setUserCustomField($user,'create_team_url',WEB_DOMAIN.''.$router->link('team_create'));
    }
    
    /**
     * Send tags and custom field after team create
     * @param type $user
     * @param type $team
     */
    public function setAfterTeamCreateSetup($user,$team)
    {
         return; 
        $stepsManager = ServiceLayer::getService('StepsManager');
         $router = ServiceLayer::getService('router');
         if($stepsManager->isStepFinished('step3_final') == false)
         {
            $this->removeUserTag($user,'user_noteam');
            $this->addUserTag($user,'team_create');
            $this->setUserCustomField($user,'invite_mebers',WEB_DOMAIN.''.$router->link('team_players_list',array('team_id' => $team->getId())));
            $this->setUserCustomField($user,'create_event',WEB_DOMAIN.''.$router->link('create_team_event',array('team_id' => $team->getId())));
            $this->setUserCustomField($user,'create_pro_url',WEB_DOMAIN.''.$router->link('player_dashboard',array('userid' => $user->getId())));
         }

    }
    
    /**
     * Send tags and custom field after event create
     * @param type $user \User\Model\User
     * @param type $team Team
     * @param type $event  \Webteamer\Event\Model\Event
     * @param type $eventDate in format Y-m-d
     */
    public function setAfterEventCreateSetup($user,$team,$event,$eventDate)
    {
         return;
        $stepsManager = ServiceLayer::getService('StepsManager');
         $router = ServiceLayer::getService('router');
         if($stepsManager->isStepFinished('step3_final') == false)
         {
            $teamEventManager = ServiceLayer::getService('TeamEventManager');
            $teamEvents = $teamEventManager->findEvents($team);
            
            if(count($teamEvents) == 1)
            {
                 $this->addUserTag($user,'create_event');
            }
            
            if(count($teamEvents) == 2)
            {
                 $this->addUserTag($user,'create_event2');
            }
             if(count($teamEvents) == 3)
            {
                 $this->addUserTag($user,'create_event3');
            }
         }
         
         $this->setUserCustomField($user,'confirm_attendance',WEB_DOMAIN.''.$router->link('team_event_detail',array('id' => $event->getId(),'current_date' => $eventDate)));
         $this->setUserCustomField($user,'create_lineup',WEB_DOMAIN.''.$router->link('team_match_new_lineup',array('event_id' => $event->getId(),'event_date' => $eventDate)));
         
         
    }
    
    public function setAfterAttendanceConfirmSetup($user)
    {
         return; 
        $stepsManager = ServiceLayer::getService('StepsManager');
          if($stepsManager->isStepFinished('step3_final') == false)
          {
              $this->addUserTag($user,'attedance');
          }
        
    }
    
    public function setAfterLineupCreateSetup($user,$event,$eventDate,$lineupId)
    {
         return;
        $router = ServiceLayer::getService('router');
        $this->addUserTag($user,'create_roster');
        $this->setUserCustomField($user,'team_match_create_live_stat',WEB_DOMAIN.''.$router->link('team_match_new_lineup',array('event_id' => $event->getId(),'eventdate' => $eventDate,'lid'=>$lineupId)));
    }
    
    /**
     * 
     */
    public function sendAfterScoreSetup($user,$team)
    {
         return;
        $stepsManager = ServiceLayer::getService('StepsManager');
        if($stepsManager->isStepFinished('step3_final') == false)
        {
            $teamEventManager = ServiceLayer::getService('TeamEventManager');
            $teamEvents = $teamEventManager->findEvents($team);
            $teamMatchManager = ServiceLayer::getService('TeamMatchManager');
            $teamMatchStatusInfo = $teamMatchManager->getTeamMatchStatusInfo($team);

            if($teamMatchStatusInfo->getClosedMatchCount() == 1)
            {
                  $this->addUserTag($user,'add_score1');
            }

             
            if($teamMatchStatusInfo->getClosedMatchCount() == 2)
            {
                  $this->addUserTag($user,'add_score2');
                  
                  //has repeated event
                  $hasRepeatedEvent = false;
                  foreach($teamEvents as $teamEvent)
                  {
                      if($teamEvent->isRepeated())
                      {
                          $hasRepeatedEvent = true;
                      }
                  }
                  
                  if($hasRepeatedEvent)
                  {
                      $this->addUserTag($user,'create_event3');
                  }
                  
            }
            
            if($teamMatchStatusInfo->getClosedMatchCount() == 3)
            {
                  $this->addUserTag($user,'add_score3');
            }
        }
    }
      

}
