<?php
namespace  Conference\Notification\Model;
use Core\DatagridTable;
use Core\ServiceLayer as ServiceLayer;
class NotificationDatagrid extends DatagridTable {
	
	public  function __construct()
	{
		$this->setTableName('co_notifications');
		$this->setVisibleColumns(array('notification_type','subject','demand_status_name','is_active'));
		
		$this->setTableRows(array(
				'Typ notifikácie' => 'notification_type',
				'Predmet' => 'subject',
				'stav dopytu' => 'demand_status_name',
				'aktívny' => 'is_active',
		));

		$this->query = 'SELECT  a.*, s.name as demand_status_name
				FROM '.$this->getTableName().' a
				LEFT JOIN ph_statuses s ON a.demand_status_ID = s.id
				';
	
		$this->setDefaultSortCriteria(' Order by a.id asc ');
	
	}
	
	
}
