<?php
namespace  Notification\Model;
use Core\DbStorage;
class NotificationStorage extends DbStorage {
		
	public function find($index)
	{
		
		$data = \Core\DbQuery::prepare('SELECT
				d.*,
				s.name as status_name
				FROM '.$this->getTableName().' d
				LEFT JOIN ph_statuses s ON d.demand_status_ID = s.id
				WHERE d.id = :id
	
				')
					->bindParam('id', $index)
					->execute()
					->fetchOne(\PDO::FETCH_ASSOC);
		return $data;
		
	}
}