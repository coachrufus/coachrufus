<?php
namespace Notification\Model;
use Core\Repository as Repository;

class NotificationRepository extends Repository
{
	public function createSendLog($data)
	{
		$storage = new \Core\DbStorage('ph_notifications_sent');
		$storage->create($data);
	}
}
