<?php

namespace Notification\Model;

use Core\Manager;
use Core\Notifications\ActivationAccountNotificationSender;
use Core\Notifications\ActivationAccountStore;
use Core\Notifications\BaseRegisterNotificationSender;
use Core\Notifications\BaseRegisterStore;
use Core\Notifications\EventCancelPlayerAttendanceNotificationSender;
use Core\Notifications\EventCancelPlayerAttendanceStore;
use Core\Notifications\LastActivationAccountNotificationSender;
use Core\Notifications\LastActivationAccountStore;
use Core\Notifications\MessageNewNotificationSender;
use Core\Notifications\MessageNewStore;
use Core\Notifications\PaymentSuccessNotificationSender;
use Core\Notifications\PaymentSuccessStore;
use Core\Notifications\PaymentYearNotifyNotificationSender;
use Core\Notifications\PaymentYearNotifyStore;
use Core\Notifications\ShareLinkInvitationAdminNotificationSender;
use Core\Notifications\ShareLinkInvitationAdminStore;
use Core\Notifications\TeamDeleteNotificationSender;
use Core\Notifications\TeamDeleteStore;
use Core\Notifications\TeamInvitationDeclineNotificationSender;
use Core\Notifications\TeamInvitationNotificationSender;
use Core\Notifications\TeamInvitationStore;
use Core\Notifications\TeamInvitationUnregistredNotificationSender;
use Core\Notifications\TeamInvitationUnregistredStore;
use Core\Notifications\TeamJoinRequestAcceptedNotificationSender;
use Core\Notifications\TeamJoinRequestAcceptedStore;
use Core\Notifications\TeamJoinRequestRefusedNotificationSender;
use Core\Notifications\TeamJoinRequestRefusedStore;
use Core\Notifications\TeamLeaveNotificationSender;
use Core\Notifications\TeamLeaveStore;
use Core\Notifications\TournamentChangedStatusConfirmedNotificationSender;
use Core\Notifications\TournamentChangedStatusConfirmedStore;
use Core\Notifications\TournamentChangedStatusRejectedNotificationSender;
use Core\Notifications\TournamentChangedStatusRejectedStore;
use Core\Notifications\TournamentJoinAdminNotificationSender;
use Core\Notifications\TournamentJoinAdminStore;
use Core\Notifications\TournamentTeamAdminInvitationNotificationSender;
use Core\Notifications\TournamentTeamAdminInvitationStore;
use Core\Notifications\VerifyEmailNotificationSender;
use Core\Notifications\VerifyEmailStore;
use Core\ServiceLayer as ServiceLayer;
use DOMDocument;
use Exception;
use stdClass;
use Swift_Image;
use const WEB_DOMAIN;

class NotificationManager extends Manager {

    private $mailer;
    private $router;
    private $lang = 'sk';

    public function __construct($repository, $router, $mailer = null)
    {
        if (null != $repository)
        {
            $this->setRepository($repository);
        }
        $this->router = $router;
        $this->mailer = $mailer;
    }

    public function getLang()
    {
        return $this->lang;
    }

    public function setLang($lang)
    {
        $this->lang = $lang;
    }

    public function getRouter()
    {
        return $this->router;
    }

    public function setRouter($router)
    {
        $this->router = $router;
    }
    
    public function embedMessageImages($message_body, $mail_message, $images_base_path)
    {
        $dom = new DOMDocument();
        $dom->loadHTML("<?xml encoding='UTF-8'>{$message_body}");
        $images = $dom->getElementsByTagName('img');
        $xpath = new \DOMXpath($dom);
        foreach($images as $image)
        {
            $path = "{$images_base_path}{$image->getAttribute('src')}";
            if (is_file($path))
            {
                $embed_image = $mail_message->embed(Swift_Image::fromPath($path)); 
                $message_body = str_replace($image->getAttribute('src'), $embed_image, $message_body);
            }
        }
        return $message_body;
    }

    public function sendNotification($mail_notification)
    {
        $message = $this->getMailer();
        $message = $this->getMailer()->createMessage();
        $message->setSubject($mail_notification->getSubject());

        
        $fromName = DEFAULT_MAIL_SENDER['name'];
        $fromEmail = DEFAULT_MAIL_SENDER['email'];


        if (null != $mail_notification->getSenderName())
        {
            $fromName = $mail_notification->getSenderName();
        }
        if (null != $mail_notification->getSenderEmail())
        {
            $fromEmail = $mail_notification->getSenderEmail();
        }

        $recipient = $mail_notification->getRecipient();

       

        $body = $mail_notification->getMessage();



        if (RUN_ENV == 'dev')
        {
            //$recipient = DEV_EMAIL;
            //$body = 'TEST MAIL:<br />' . $mail_notification->getRecipient() . $body;
            $message->setBcc('marek.hubacek@gmail.com');
        }

        $message->setFrom(array($fromEmail => $fromName));
        $message->setTo($recipient);

        if (null != $mail_notification->getBcc())
        {
            $message->setBcc($mail_notification->getBcc());
        }
        $body = $this->embedMessageImages($body, $message, PUBLIC_DIR);
        try
        {
            //$message->setTo($mail_notification->getRecipient());
            $message->setBody($body, 'text/html');
            $send_result = $this->getMailer()->sendMessage($message);
        }
        catch (Exception $e)
        {
            $send_result = array('ERROR' => $e, 'RESULT' => 0);
        }


        if ($send_result['RESULT'] == 1)
        {
            return array(
                'RESULT' => 1,
                'MESSAGE' => 'NOTE_EMAIL_SENT_SUCCESS',
            );
        }
        else
        {
             return array(
              'RESULT' => 0,
              'MESSAGE' => 'NOTE_EMAIL_SENT_ERROR',
              );
           
        }
    }

    public function getMailer()
    {
        if (null == $this->mailer)
        {
            $this->mailer = ServiceLayer::getService('mailer');
        }
        return $this->mailer;
    }

    public function setMailer($mailer)
    {
        $this->mailer = $mailer;
    }

    public function sendCancelPlayerAttendance($baseData)
    {
        
        $data = new \stdClass();
        $data->email = $baseData['email'];
        $data->lang = $baseData['lang'];
        $data->event = $baseData['event'];
        $store =  new EventCancelPlayerAttendanceStore();
        $sender = new EventCancelPlayerAttendanceNotificationSender($store);
        $sender->send($data);
    }
   
    
    public function sendBaseRegisterEmail($data)
    {
        $sender = new BaseRegisterNotificationSender(new BaseRegisterStore());
        $sender->send($data);
    }
    
    public function sendActivationEmail($data)
    {
        $sender = new ActivationAccountNotificationSender(new ActivationAccountStore());
        $sender->send($data);
    }
    
    public function sendLastActivationEmail($data)
    {
        $sender = new LastActivationAccountNotificationSender(new LastActivationAccountStore());
        $sender->send($data);
    }
    
    public function sendMessageNewEmail($data)
    {
        $sender = new MessageNewNotificationSender(new MessageNewStore());
        $sender->send($data);
    }
    
    public function sendVerifyEmail($data)
    {
        $sender = new VerifyEmailNotificationSender(new VerifyEmailStore());
        $sender->send($data);
    }
    
    
    public function sendShareLinkInvitationAdmin($data)
    {
        $sender = new ShareLinkInvitationAdminNotificationSender(new ShareLinkInvitationAdminStore());
        $sender->send($data);
    }
    
    
        
    public function sendTeamDeleteNotification($data)
    {
        $sender = new TeamDeleteNotificationSender(new TeamDeleteStore());
        $sender->send($data);
    }
    
    public function sendTeamLeaveNotification($data)
    {
        $sender = new TeamLeaveNotificationSender(new TeamLeaveStore());
        $sender->send($data);
    }
    
    public function sendMemberInvitation($user, $team, $teamPlayer)
    {
        if(null != $user->getDefaultLang() )
        {
            $notifyLang = $user->getDefaultLang();
        }

//registred
        if($teamPlayer->getPlayerId() > 0)
        {
             $sender = new TeamInvitationNotificationSender(new TeamInvitationStore());
             $recipient = ServiceLayer::getService('security')->getIdentityProvider()->findUserById($teamPlayer->getPlayerId());
             if(null != $recipient && null != $recipient->getDefaultLang() )
             {
                 $notifyLang = $recipient->getDefaultLang();
             }
        }
        else 
        {
             $sender = new TeamInvitationUnregistredNotificationSender(new TeamInvitationUnregistredStore());
        }
        $translator = ServiceLayer::getService('translator');
        $sportManager = ServiceLayer::getService('SportManager');
        $sportEnum = $sportManager->getSportFormChoices();
        $sportName =  $translator->translate($sportEnum[$team->getSportId()]);
        $notifyData = new stdClass();
        $notifyData->email = $teamPlayer->getEmail();
        $notifyData->default_lang =  $notifyLang;
        $notifyData->sender_name = $user->getFullName();
        $notifyData->team_name = $team->getName();
        $notifyData->sportName = $sportName;
        $notifyData->invite_link = WEB_DOMAIN . $this->getRouter()->link('team_member_invite', array('hash' => $team->getShareLinkHash(),'tp' => $teamPlayer->getId(),'h' => $teamPlayer->getHash()));
        //$message = str_replace('{INVITE_LINK}', WEB_DOMAIN . $this->getRouter()->link('team_member_invite', array('hash' => $team->getShareLinkHash())), $message);
        $notifyData->accept_link = WEB_DOMAIN . $this->getRouter()->link('team_member_invite',array('tp' => $teamPlayer->getId() ,'hash' => $team->getShareLinkHash(),'h' => $teamPlayer->getHash() ));
        $notifyData->decline_link = WEB_DOMAIN . $this->getRouter()->link('team_member_invite_decline', array('tp' => $teamPlayer->getId() ,'hash' => $team->getShareLinkHash(),'h' => $teamPlayer->getHash() ));
        $sender->send($notifyData);
    }
    
    public function sendMemberInvitationDecline($adminList, $team, $teamPlayer)
    {
        foreach($adminList as $admin)
        {
            $sender = new TeamInvitationDeclineNotificationSender(new TeamInvitationStore());
            $notifyData = new stdClass();
            $notifyData->email = $admin->getEmail();
            $notifyData->default_lang =  $this->getLang();
            $notifyData->sender_name = $teamPlayer->getFullName();
            $notifyData->team_name = $team->getName();
            $sender->send($notifyData);
        }
    }
    
    public function sendTeamJoinRequestRefused($admin, $team, $player)
    {
        $sender = new TeamJoinRequestRefusedNotificationSender(new TeamJoinRequestRefusedStore());
        $notifyData = new stdClass();
        $notifyData->email = $player->getEmail();
        $notifyData->default_lang =  $this->getLang();
        $notifyData->team_player_name = $player->getFullName();
        $notifyData->team_name = $team->getName();
        $notifyData->sender_name = $admin->getName();
        
        $sender->send($notifyData);
    }
    public function sendTeamJoinRequestAccepted($admin, $team, $player)
    {
        $sender = new TeamJoinRequestAcceptedNotificationSender(new TeamJoinRequestAcceptedStore());
        $notifyData = new stdClass();
        $notifyData->email = $player->getEmail();
        $notifyData->default_lang =  $this->getLang();
        $notifyData->team_player_name = $player->getFullName();
        $notifyData->team_name = $team->getName();
        $notifyData->sender_name = $admin->getName();
        
        $sender->send($notifyData);
    }
    
    /*
    public function buildActivationEmailNotification($lang,$user)
    {
         if (null == $lang) $lang = 'sk';
        $notificationType = "activation_email_{$lang}";
        $notification_repository = ServiceLayer::getService('notification_repository');
        $notification = $notification_repository->findOneBy([
            'notification_type' => $notificationType
        ]);
        $template = new \Core\Notifications\NotificationTemplate($notificationType);
        $template->userEmail =$user->getEmail();
        $template->name =$user->getFullName();
        $template->lang = $lang;
        $template->activationGuid = $user->getActivationGuid();
        $notification->setMessage((string)$template);
        return $notification;
    }
    */
    
    
    
   

    public function sendTeamEventReminder($team, $event, $recipientEmail)
    {
        $lang = $this->getLang();

        $notification = $this->getRepository()->findOneBy(array('notification_type' => 'event_team_notify_reminder', 'lang' => $lang));
        $notification->setSenderName('Coach Rufus');
        $notification->setRecipient($recipientEmail);

        $this->replaceTeamMarks($notification, $team);
        $this->replaceEventMarks($notification, $event);
        $this->replaceSenderMarks($notification);

        $this->sendNotification($notification);
    }
    
    
    public function sendPaymentSuccessNotification($data)
    {
        $sender = new PaymentSuccessNotificationSender(new PaymentSuccessStore());
        $sender->send($data);
    }
  
    
    public function sendPaymentYearNotification($data)
    {
        $sender = new PaymentYearNotifyNotificationSender(new PaymentYearNotifyStore());
        $sender->send($data);
    }
    
     public function sendTournamentAdminInvitation($user,$targetEmail,$team,$tournament,$hash)
    {
        if(null != $user->getDefaultLang() )
        {
            $notifyLang = $user->getDefaultLang();
        }
        
        $sender = new TournamentTeamAdminInvitationNotificationSender(new TournamentTeamAdminInvitationStore());
        $notifyData = new stdClass();
        $notifyData->email = $targetEmail;
        $notifyData->teamName = $team->getName();
        $notifyData->senderName = $user->getFullName();
        $notifyData->tournamentName = $tournament->getName();
        $notifyData->default_lang =  $notifyLang;
        $notifyData->acceptLink =   WEB_DOMAIN.$this->getRouter()->link('tournament_team_invite_response',array('t' => $hash,'r' => 'accept'));
        $notifyData->declineLink =  WEB_DOMAIN.$this->getRouter()->link('tournament_team_invite_response',array('t' => $hash,'r' => 'reject'));
        $sender->send($notifyData);
    }
    
    public function sendTournamentJoinAdminNotification($tournament,$team)
    {
        $tournamentAdminIds = $tournament->getAdminList();
        $userRepo = ServiceLayer::getService('user_repository');
        foreach($tournamentAdminIds as $tournamentAdminId)
        {
            $user = $userRepo->find($tournamentAdminId);
            if(null != $user->getDefaultLang() )
            {
                $notifyLang = $user->getDefaultLang();
            }
            else
            {
                 $notifyLang = 'sk';
            }

            $notifyData['email'] = $user->getEmail();
            $notifyData['default_lang']  = $notifyLang;
            $notifyData['tournament_name']  = $tournament->getName();
            $notifyData['team_name']  = $team->getName();
            $notifyData['user_name']  = $user->getName();

            $sender = new TournamentJoinAdminNotificationSender(new TournamentJoinAdminStore());
            $sender->send($notifyData);
        }
    }
  
    public function sendTournamentTeamConfirmed($tournament,$team)
    {
        $teamManager = ServiceLayer::getService('TeamManager');
        $admins = $teamManager->getTeamAdminList($team);
         $userRepo = ServiceLayer::getService('user_repository');

        foreach($admins as $admin)
        {
            $user = $userRepo->find($admin->getPlayerId());
            if(null != $user->getDefaultLang() )
            {
                $notifyLang = $user->getDefaultLang();
            }
            else
            {
                 $notifyLang = 'sk';
            }

            $notifyData['email'] = $user->getEmail();
            $notifyData['default_lang']  = $notifyLang;
            $notifyData['tournament_name']  = $tournament->getName();
            $notifyData['team_name']  = $team->getName();
            $notifyData['user_name']  = $user->getName();

             $sender = new TournamentChangedStatusConfirmedNotificationSender(new TournamentChangedStatusConfirmedStore());
            $sender->send($notifyData);
        }

    }
    public function sendTournamentTeamRejected($tournament,$team)
    {
        $teamManager = ServiceLayer::getService('TeamManager');
        $admins = $teamManager->getTeamAdminList($team);
         $userRepo = ServiceLayer::getService('user_repository');

        foreach($admins as $admin)
        {
            $user = $userRepo->find($admin->getPlayerId());
            if(null != $user->getDefaultLang() )
            {
                $notifyLang = $user->getDefaultLang();
            }
            else
            {
                 $notifyLang = 'sk';
            }

            $notifyData['email'] = $user->getEmail();
            $notifyData['default_lang']  = $notifyLang;
            $notifyData['tournament_name']  = $tournament->getName();
            $notifyData['team_name']  = $team->getName();
            $notifyData['user_name']  = $user->getName();

           $sender = new TournamentChangedStatusRejectedNotificationSender(new TournamentChangedStatusRejectedStore());
            $sender->send($notifyData);
        }

    }
  

}
