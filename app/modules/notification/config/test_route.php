<?php

$acl->allowRole(array('ROLE_USER'),'test_event_cancel_player_attendance');
$router->addRoute('test_event_cancel_player_attendance','/test/notification/event-cancel-player-attendance',array(        
    'controller' => 'Notification\NotificationModule:Test:EventCancelPlayerAttendanceNotification'
));


$acl->allowRole(array('ROLE_USER'),'test_message_new_email');
$router->addRoute('test_message_new_email','/test/notification/message-new-email',array(        
    'controller' => 'Notification\NotificationModule:Test:messageNewEmail'
));

$acl->allowRole(array('ROLE_USER'),'test_verify_email');
$router->addRoute('test_verify_email','/test/notification/verify-email',array(        
    'controller' => 'Notification\NotificationModule:Test:verifyEmail'
));

$acl->allowRole(array('ROLE_USER'),'test_registration_email');
$router->addRoute('test_registration_email','/test/notification/registration-email',array(        
    'controller' => 'Notification\NotificationModule:Test:registrationEmail'
));

$acl->allowRole(array('ROLE_USER'),'test_activation_email');
$router->addRoute('test_activation_email','/test/notification/activation-email',array(        
    'controller' => 'Notification\NotificationModule:Test:activationEmail'
));

$acl->allowRole(array('ROLE_USER'),'test_last_activation_email');
$router->addRoute('test_last_activation_email','/test/notification/last-activation-email',array(        
    'controller' => 'Notification\NotificationModule:Test:lastActivationEmail'
));


$acl->allowRole(array('ROLE_USER'),'test_admin_without_players');
$router->addRoute('test_admin_without_players','/test/notification/admin-without-players',array(        
    'controller' => 'Notification\NotificationModule:Test:adminWithoutPlayersEmail'
));

$acl->allowRole(array('ROLE_USER'),'test_user_with_team');
$router->addRoute('test_user_with_team','/test/notification/user-with-team',array(        
    'controller' => 'Notification\NotificationModule:Test:userWithTeamEmail'
));

$acl->allowRole(array('ROLE_USER'),'test_user_with_team');
$router->addRoute('test_user_with_team','/test/notification/user-with-team',array(        
    'controller' => 'Notification\NotificationModule:Test:userWithTeamEmail'
));

$acl->allowRole(array('ROLE_USER'),'test_user_without_team');
$router->addRoute('test_user_without_team','/test/notification/user-without-team',array(        
    'controller' => 'Notification\NotificationModule:Test:userWithoutTeamEmail'
));


/**
 * team invitation for unregistred player
 */
$acl->allowRole(array('ROLE_USER'),'test_team_invitation_unregistred');
$router->addRoute('test_team_invitation_unregistred','/test/notification/team-invitation-unregistred',array(        
    'controller' => 'Notification\NotificationModule:Test:teamInvitationUnregistredEmail'
));

$acl->allowRole(array('ROLE_USER'),'test_event_summary');
$router->addRoute('test_event_summary','/test/notification/event-summary',array(        
    'controller' => 'Notification\NotificationModule:Test:eventSummaryEmail'
));

$acl->allowRole(array('ROLE_USER'),'test_event_summary_admin');
$router->addRoute('test_event_summary_admin','/test/notification/event-summary-admin',array(        
    'controller' => 'Notification\NotificationModule:Test:eventSummaryEmailAdmin'
));

$acl->allowRole(array('ROLE_USER'),'test_team_invitation_decline');
$router->addRoute('test_team_invitation_decline','/test/notification/team-invitation-decline',array(        
    'controller' => 'Notification\NotificationModule:Test:teamInvitationDeclineEmail'
));

$acl->allowRole(array('ROLE_USER'),'test_event_invitation');
$router->addRoute('test_event_invitation','/test/notification/event-invitation',array(        
    'controller' => 'Notification\NotificationModule:Test:eventInvitationEmail'
));


$acl->allowRole(array('ROLE_USER'),'test_event_add_stat');
$router->addRoute('test_event_add_stat','/test/notification/event-add-stat',array(        
    'controller' => 'Notification\NotificationModule:Test:eventAddStatEmail'
));


$acl->allowRole(array('ROLE_USER'),'test_event_add_rate');
$router->addRoute('test_event_add_rate','/test/notification/event-add-rate',array(        
    'controller' => 'Notification\NotificationModule:Test:eventAddRateEmail'
));

$acl->allowRole(array('ROLE_USER'),'test_match_result');
$router->addRoute('test_match_result','/test/notification/match-result',array(        
    'controller' => 'Notification\NotificationModule:Test:matchResultEmail'
));




$acl->allowRole(array('ROLE_USER'),'test_team_invitation_registred');
$router->addRoute('test_team_invitation_registred','/test/notification/team-invitation-registred',array(        
    'controller' => 'Notification\NotificationModule:Test:teamInvitationRegistredEmail'
));


$acl->allowRole(array('ROLE_USER'),'test_event_invitation_store');
$router->addRoute('test_event_invitation_store','/test/notification/event-invitation-store',array(        
    'controller' => 'Notification\NotificationModule:Test:eventInvitationStore'
));

$acl->allowRole(array('ROLE_USER'),'test_email_marketing');
$router->addRoute('test_email_marketing','/test/email-marketing',array(        
    'controller' => 'Notification\NotificationModule:Test:emailMarketing'
));


$acl->allowRole(array('ROLE_USER'),'test_beep_events');
$router->addRoute('test_beep_events','/test/beep-events',array(        
    'controller' => 'Notification\NotificationModule:Test:showBeepEvents'
));


/*
$acl->allowRole(array('ROLE_USER'),'event_team_notify_reminder');
$router->addRoute('event_team_notify_reminder','/test/notification/event-reminder',array(        
    'controller' => 'Notification\NotificationModule:Test:reminderEmail'
));
 * */
