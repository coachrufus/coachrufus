<?php

$acl->allowRole(array('ROLE_USER'),'preview_index');
$router->addRoute('preview_index','/preview/index',array(        
    'controller' => 'Notification\NotificationModule:Preview:index'
));

//verify_email_sk
$acl->allowRole(array('ROLE_USER'),'preview_verify_email');
$router->addRoute('preview_verify_email','/preview/verify-email',array(        
    'controller' => 'Notification\NotificationModule:Preview:verifyEmail'
));


//registration_email_sk
$acl->allowRole(array('ROLE_USER'),'preview_registration_email');
$router->addRoute('preview_registration_email','/preview/registration-email',array(        
    'controller' => 'Notification\NotificationModule:Preview:registrationEmail'
));

//activation_email_sk
$acl->allowRole(array('ROLE_USER'),'preview_activation_email');
$router->addRoute('preview_activation_email','/preview/activation-email',array(        
    'controller' => 'Notification\NotificationModule:Preview:activationEmail'
));


//floorball_challenge_activation_email
$acl->allowRole(array('ROLE_USER'),'preview_floorball_email');
$router->addRoute('preview_floorball_email','/preview/floorball-email',array(        
    'controller' => 'Notification\NotificationModule:Preview:floorballEmail'
));

//last_activation_email_sk
$acl->allowRole(array('ROLE_USER'),'preview_last_activation_email');
$router->addRoute('preview_last_activation_email','/preview/last-activation-email',array(        
    'controller' => 'Notification\NotificationModule:Preview:lastActivationEmail'
));

//user_without_team
$acl->allowRole(array('ROLE_USER'),'preview_user_without_team');
$router->addRoute('preview_user_without_team','/preview/user-without-team',array(        
    'controller' => 'Notification\NotificationModule:Preview:userWithoutTeamEmail'
));


//user_with_team
$acl->allowRole(array('ROLE_USER'),'preview_user_with_team');
$router->addRoute('preview_user_with_team','/preview/user-with-team',array(        
    'controller' => 'Notification\NotificationModule:Preview:userWithTeamEmail'
));

//AdminWithoutPlayers_sk
$acl->allowRole(array('ROLE_USER'),'preview_admin_without_players');
$router->addRoute('preview_admin_without_players','/preview/admin-without-players',array(        
    'controller' => 'Notification\NotificationModule:Preview:adminWithoutPlayersEmail'
));


$acl->allowRole(array('ROLE_USER'),'preview_team_share_link_invitation');
$router->addRoute('preview_team_share_link_invitation','/preview/share-link-invitation',array(        
    'controller' => 'Notification\NotificationModule:Preview:shareLinkInvitation'
));


//TeamInvitation_sk
$acl->allowRole(array('ROLE_USER'),'preview_team_invitation_registred');
$router->addRoute('preview_team_invitation_registred','/preview/team-invitation-registred',array(        
    'controller' => 'Notification\NotificationModule:Preview:teamInvitationRegistredEmail'
));


//TeamInvitationDecline_sk
$acl->allowRole(array('ROLE_USER'),'preview_team_invitation_decline');
$router->addRoute('preview_team_invitation_decline','/preview/team-invitation-decline',array(        
    'controller' => 'Notification\NotificationModule:Preview:teamInvitationDeclineEmail'
));

//TeamJoinRequestRefused
$acl->allowRole(array('ROLE_USER'),'preview_team_join_request_refused');
$router->addRoute('preview_team_join_request_refused','/preview/team-join-request-refused',array(        
    'controller' => 'Notification\NotificationModule:Preview:teamJoinRequestRefusedEmail'
));

//TeamJoinRequestAccepted
$acl->allowRole(array('ROLE_USER'),'preview_team_join_request_accepted');
$router->addRoute('preview_team_join_request_accepted','/preview/team-join-request-accepted',array(        
    'controller' => 'Notification\NotificationModule:Preview:teamJoinRequestAcceptedEmail'
));


//TeamInvitation_sk
$acl->allowRole(array('ROLE_USER'),'preview_team_invitation_unregistred');
$router->addRoute('preview_team_invitation_unregistred','/preview/team-invitation-unregistred',array(        
    'controller' => 'Notification\NotificationModule:Preview:teamInvitationUnregistredEmail'
));


//EventInvitation_sk
$acl->allowRole(array('ROLE_USER'),'preview_event_invitation');
$router->addRoute('preview_event_invitation','/preview/event-invitation',array(        
    'controller' => 'Notification\NotificationModule:Preview:eventInvitationEmail'
));

//EventInvitationSingle_sk
$acl->allowRole(array('ROLE_USER'),'preview_event_invitation_single');
$router->addRoute('preview_event_invitation_single','/preview/event-invitation-single',array(        
    'controller' => 'Notification\NotificationModule:Preview:eventInvitationSingleEmail'
));


//EventSummary_admin_sk
$acl->allowRole(array('ROLE_USER'),'preview_event_summary_admin');
$router->addRoute('preview_event_summary_admin','/preview/event-summary-admin',array(        
    'controller' => 'Notification\NotificationModule:Preview:eventSummaryEmailAdmin'
));

//EventSummary_sk
$acl->allowRole(array('ROLE_USER'),'preview_event_summary');
$router->addRoute('preview_event_summary','/preview/event-summary',array(        
    'controller' => 'Notification\NotificationModule:Preview:eventSummaryEmail'
));

//MatchResults_sk
$acl->allowRole(array('ROLE_USER'),'preview_match_result');
$router->addRoute('preview_match_result','/preview/match-result',array(        
    'controller' => 'Notification\NotificationModule:Preview:matchResultEmail'
));

//AddRate_sk
$acl->allowRole(array('ROLE_USER'),'preview_event_add_rate');
$router->addRoute('preview_event_add_rate','/preview/event-add-rate',array(        
    'controller' => 'Notification\NotificationModule:Preview:eventAddRateEmail'
));

//AddStatistics_sk
$acl->allowRole(array('ROLE_USER'),'preview_event_add_stat');
$router->addRoute('preview_event_add_stat','/preview/event-add-stat',array(        
    'controller' => 'Notification\NotificationModule:Preview:eventAddStatEmail'
));

//delete team
$acl->allowRole(array('ROLE_USER'),'preview_delete_team');
$router->addRoute('preview_delete_team','/preview/delete-team',array(        
    'controller' => 'Notification\NotificationModule:Preview:deleteTeamEmail'
));

//leave team
$acl->allowRole(array('ROLE_USER'),'preview_leave_team');
$router->addRoute('preview_leave_team','/preview/leave-team',array(        
    'controller' => 'Notification\NotificationModule:Preview:leaveTeamEmail'
));

//payment
$acl->allowRole(array('ROLE_USER'),'preview_payment_success');
$router->addRoute('preview_payment_success','/preview/payment-success',array(        
    'controller' => 'Notification\NotificationModule:Preview:paymentSuccess'
));

$acl->allowRole(array('ROLE_USER'),'preview_payment_year_notify');
$router->addRoute('preview_payment_year_notify','/preview/payment-year-notify',array(        
    'controller' => 'Notification\NotificationModule:Preview:paymentYearNotify'
));


$acl->allowRole(array('ROLE_USER'),'tournament_join_admin');
$router->addRoute('tournament_join_admin','/preview/tournament-join-admin',array(        
    'controller' => 'Notification\NotificationModule:Preview:tournamentJoinAdmin'
));


$acl->allowRole(array('ROLE_USER'),'tournament_changed_status_confirmed');
$router->addRoute('tournament_changed_status_confirmed','/preview/tournament-changed-status-confirmed',array(        
    'controller' => 'Notification\NotificationModule:Preview:tournamentChangedStatusConfirmed'
));

$acl->allowRole(array('ROLE_USER'),'tournament_changed_status_rejected');
$router->addRoute('tournament_changed_status_rejected','/preview/tournament-changed-status-rejected',array(        
    'controller' => 'Notification\NotificationModule:Preview:tournamentChangedStatusRejected'
));