<?php
namespace Notification;
require_once(MODUL_DIR.'/notification/NotificationModule.php');

use Core\ServiceLayer as ServiceLayer;

/**
Ulozisko notifikacii
 */
ServiceLayer::addService('notification_repository',array(
'class' => "Notification\Model\NotificationRepository",
    'params' => array(
        new \Notification\Model\NotificationStorage('co_notifications'),
        new \Core\EntityMapper('Notification\Model\Notification')
    )
));


/**
Servis zodpovedny za odosielanie notifikacii
 */
ServiceLayer::addService('notification_manager',array(
'class' => "Notification\Model\NotificationManager",
   'params' => array(
        ServiceLayer::getService('notification_repository'),
        ServiceLayer::getService('router'),
    )
));

ServiceLayer::addService('EmailMarketingManager',array(
'class' => "Notification\Model\EmailMarketingManager",
	'params' => array(new \GetResponse\Api() )
));

/**
Nastavenie opravneni k jedntolivym akciam kontrollera
*/
$acl = ServiceLayer::getService('acl');
$acl->allowRole(array('ROLE_ADMIN'),'notification_list');
$acl->allowRole(array('ROLE_ADMIN'),'notification_add');
$acl->allowRole(array('ROLE_ADMIN'),'notification_edit');
$acl->allowRole(array('ROLE_ADMIN'),'notification_delete');


/**
Routovacie pravidla
 */
$router = ServiceLayer::getService('router');


$router->addRoute('notification_list','/notification/list',array(        
    'controller' => 'Notification\NotificationModule:Crud:index'
));

$router->addRoute('notification_add','/notification/add',array(        
    'controller' => 'Notification\NotificationModule:Crud:add'
));

$router->addRoute('notification_edit','/notification/edit',array(        
    'controller' => 'Notification\NotificationModule:Crud:edit'
));

$router->addRoute('notification_delete','/notification/delete',array(        
    'controller' => 'Notification\NotificationModule:Crud:edit'
));


$acl->allowRole(array('ROLE_USER'),'notification_preview');
$router->addRoute('notification_preview','/notification/preview',array(        
    'controller' => 'Notification\NotificationModule:Crud:preview'
));


$acl->allowRole(array('guest'),'resend_activation');
$router->addRoute('resend_activation','/notification/cron/resend-account-activation',array(        
    'controller' => 'Notification\NotificationModule:Cron:resendAccountActivation'
));

$acl->allowRole(array('guest'),'notification_cron');
$router->addRoute('notification_cron','/notification/cron',array(        
    'controller' => 'Notification\NotificationModule:Cron:sendNotifications'
));

$acl->allowRole(['guest'], 'notifications_send_every_day');
$router->addRoute('notifications_send_every_day','/notifications/send-every-day', [    
    'controller' => 'Notification\NotificationModule:Notifications:sendEveryDay'
]);

$acl->allowRole(['guest'], 'notifications_send_every_minute');
$router->addRoute('notifications_send_every_minute','/notifications/send-every-minute', [    
    'controller' => 'Notification\NotificationModule:Notifications:sendEveryMinute'
]);

$acl->allowRole(['guest'], 'deactivate_notifications_link');
$router->addRoute('deactivate_notifications_link','/notifications/deactivate/:guid', [    
    'controller' => 'Notification\NotificationModule:Notifications:deactivate'
]);

$acl->allowRole(['guest'], 'notifications_deactivated');
$router->addRoute('notifications_deactivated','/notifications/deactivated/:lang', [    
    'controller' => 'Notification\NotificationModule:Notifications:deactivated'
]);

$acl->allowRole(['guest'], 'invalid_link');
$router->addRoute('invalid_link','/notifications/invalidlink', [    
    'controller' => 'Notification\NotificationModule:Notifications:invalidlink'
]);

$acl->allowRole(['guest'], 'event_in');
$router->addRoute('event_in','/notifications/event-in/:guid', [    
    'controller' => 'Notification\NotificationModule:Notifications:eventIn'
]);

$acl->allowRole(['guest'], 'event_out');
$router->addRoute('event_out','/notifications/event-out/:guid', [    
    'controller' => 'Notification\NotificationModule:Notifications:eventOut'
]);

$acl->allowRole(['guest'], 'event_detail_redirect');
$router->addRoute('event_detail_redirect','/notifications/event-detail/:guid', [    
    'controller' => 'Notification\NotificationModule:Notifications:eventDetail'
]);

$acl->allowRole(['guest'], 'event_statistics_redirect');
$router->addRoute('event_statistics_redirect','/notifications/event-statistics/:guid', [    
    'controller' => 'Notification\NotificationModule:Notifications:eventStatistics'
]);

$acl->allowRole(['guest'], 'event_rate_redirect');
$router->addRoute('event_rate_redirect','/notifications/event-rate/:guid', [    
    'controller' => 'Notification\NotificationModule:Notifications:eventRate'
]);

$acl->allowRole(['guest'], 'event_linuep_redirect');
$router->addRoute('event_linuep_redirect','/notifications/event-lineup/:guid', [    
    'controller' => 'Notification\NotificationModule:Notifications:eventLineup'
]);

$acl->allowRole(['guest'], 'player_event_summary');
$router->addRoute('player_event_summary','/reports/player-event-summary/:id/:date', [    
    'controller' => 'Notification\NotificationModule:Reports:playerEventSummary'
]);

$acl->allowRole(['guest'], 'admin_event_summary');
$router->addRoute('admin_event_summary','/reports/admin-event-summary/:id/:date', [    
    'controller' => 'Notification\NotificationModule:Reports:adminEventSummary'
]);

$acl->allowRole(array('guest'),'email_marketing_cron');
$router->addRoute('email_marketing_cron','/notification/email-marketing-cron',array(        
    'controller' => 'Notification\NotificationModule:Cron:emailMarketingEvent'
));

$acl->allowRole(array('guest'),'queue_cron');
$router->addRoute('queue_cron','/notification/queue-cron',array(        
    'controller' => 'Notification\NotificationModule:Cron:sendQueueEmails'
));

require_once MODUL_DIR . "/notification/lib/NotificationStore.php";
require_once MODUL_DIR . "/notification/lib/NotificationSender.php";
require_once MODUL_DIR . "/notification/lib/NotificationSendQueue.php";
require_once MODUL_DIR . "/notification/lib/NotificationBlacklist.php";
require_once MODUL_DIR . "/notification/lib/utils/PlayerEventInfosLang.php";


$notificationSendQueue = new \Core\Notifications\NotificationSendQueue();
$dir = MODUL_DIR . '/notification/senders';
$ignoredSenders = \Core\Collections\HashSet::of(['AddRate', 'MatchResults', 'Buy']);
foreach (glob("{$dir}/*.*") as $sender)
{
    $name = basename($sender, '.php');
    if (!$ignoredSenders->contains($name))
    {
        require_once $sender;
        $notificationSenderClass = "Core\\Notifications\\{$name}NotificationSender";
        $storePath = "{$dir}/model/{$name}.php";
        if (is_file($storePath)) require_once $storePath;
        $storeClass = "Core\\Notifications\\{$name}Store";
        $notificationSendQueue[] = new $notificationSenderClass(new $storeClass());
    }
}
ServiceLayer::addServiceInstance('NotificationSendQueue', $notificationSendQueue);

include_once('test_route.php');
include_once('preview_route.php');