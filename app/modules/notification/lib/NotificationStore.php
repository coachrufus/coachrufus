<?php namespace Core\Notifications;

abstract class NotificationStore
{    
    protected abstract function dataSource();
    
    function getData() { return $this->dataSource(); }
}

abstract class UniqueItemsNotificationStore extends NotificationStore
{
    protected $distinctColumn = 'email';

    private function distinctBy($items, $column)
    {
        $set = [];
        $result = [];
        foreach ($items as $item)
        {
            $value = $item[$column];
            if (!isset($set[$value]))
            {
                $set[$value] = true;
                $result[] = $item;
            }
        }
        return $result;
    }
    
    function getData()
    {
        return $this->distinctBy(parent::getData(), $this->distinctColumn);
    }
}