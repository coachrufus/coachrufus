<?php namespace Core\Notifications;

use Core\ServiceLayer;
use Core\DateNames;

interface INotificationRenderer 
{
    function render($templatePath, $marks);
}

class LatteNotificationRenderer implements INotificationRenderer
{
    private $latte;
    private $defaults;
    
    function __construct($lang = 'sk')
    {
        $this->latte = new \Latte\Engine;
        $this->latte->setTempDirectory(LATTE_TEMP_DIR);
        $defaultTranslator = ServiceLayer::getService('translator');
        $translator = clone $defaultTranslator;
        $translator->setLang($lang);
        
        
        $this->latte->addFilter('translate', function ($value) use($translator)
        {
            return $translator->translate($value);
        });
        $dateNames = ServiceLayer::getService('DateNames');
        $dateNames->setLang($translator->getLang());
        $this->latte->addFilter('formatDate', function ($date) use($dateNames,$translator)
        {
            return "{$dateNames->toWeekDayName($date->toString('N'))} {$date->toString('d')}. {$dateNames->toMonthName($date->toString('m'))}. {$date->toString('Y')} {$translator->translate('o')} {$date->toString('H.i')} CET";
        });
        $sportNames = ServiceLayer::getService('SportNames');
        $sportNames->setLang($translator->getLang());
        $this->latte->addFilter('toSportName', function ($sportId) use($sportNames)
        {
            return $sportNames->toSportName($sportId);
        });
        $this->defaults = [
            'root' => ServiceLayer::getService('request')->getRoot()
        ];
    }
    
    function render($templatePath, $marks)
    {
        return $this->latte->renderToString($templatePath, $this->defaults + $marks);
    }
}

class NotificationTemplate
{
    private $path;
    private $marks;
    private $renderer;
    
    private function getFilePath($name)
    {
        $fileNames = [
            NOTIFICATION_TEMPLATE_DIR . "/{$name}.latte",
            "{$name}.latte",
            NOTIFICATION_TEMPLATE_DIR . "/{$name}",
            $name
        ];
        foreach ($fileNames as $fileName)
        {
            if (is_file($fileName)) return $fileName;
        }
        throw new \Exception("Template \"{$name}\" not exists.");
    }
    
    function __construct($name, $marks = [], INotificationRenderer $renderer = null)
    {
        //toto treba prerobit, cele je to zle, v konstrukore renderera plno logiky, potom sa to neda modifikovat :-((((
        $nameParts = explode('_',str_replace('.latte', '', $name));
        $lang = end($nameParts);
        
        
        $this->path = $this->getFilePath($name);
        $this->marks = $marks;
        $this->renderer = is_null($renderer) ? new LatteNotificationRenderer($lang) : $renderer;
    }
    function assignMarks($marks) { foreach ($marks as $k => $v) $this->marks[$k] = $v; }
    function assignObject($object) { $this->assignMarks((array)$object); }
    function __get($name) { return $this->marks[$name]; }
    function __isset($name) { return isset($this->marks[$name]); }
    function __unset($name) { unset($this->marks[$name]); }
    function __set($name, $value) { $this->marks[$name] = $value; }
    function render() { return $this->renderer->render($this->path, $this->marks); }
    function __toString() { return $this->render(); }
}