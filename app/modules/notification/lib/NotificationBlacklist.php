<?php namespace Core\Notifications;

class NotificationBlacklist
{
    use \Core\Db;
    
    private $db;
    
    function __construct()
    {
        $this->db = $this->getDb();
    }
    
    function add($email,$type = 'all')
    {
        if (!$this->isBlacklisted($email))
        {
            $this->db->notification_blacklist()->insert(['email' => $email,'notification_type' => $type ]);
        }
    }
    
    function isBlacklisted($email)
    {
        return $this->db->notification_blacklist('email', $email)->fetch() !== false;
    }
    
    function remove($email)
    {
        $this->db->notification_blacklist('email', $email)->delete();
    }
}