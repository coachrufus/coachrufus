<?php namespace Core\Notifications;

use Core\ServiceLayer;
use Core\Db;
use \Swift_Attachment as Attachment;
use Core\Templates\Regions;

abstract class NotificationSender
{
    use Db;
    
    private $permissionManager;
    private $store;
    
    
    function __construct(NotificationStore $store)
    {
        $this->permissionManager = ServiceLayer::getService("PermissionManager");
        $this->store = $store;
    }
    
    const suffix = 'NotificationSender';
    
    protected function getName()
    {
        $reflection = new \ReflectionClass($this);
        $className = $reflection->getShortName();
        return substr($className, 0, strlen($className) - strlen(self::suffix));
    }
    
    protected function getStore() { return $this->store; }
    
    protected function getPermissionManager() { return $this->permissionManager; }
    
    protected function getDeactivateNotificationGuid($email, $lang)
    {
        return $this->getPermissionManager()->newPermission('DEACTIVATE_NOTIFICATIONS', [
            'email' => $email, 'lang' => $lang
        ])->guid;
    }
    
    private function getSuffix($suffix)
    {
        return empty($suffix) ? '' : "_{$suffix}";
    }
    
    protected function getSystemNotification($email, $lang, $suffix = '')
    {
        $lang = empty($lang) ? LANG : $lang;
        $notificationType = "{$this->getName()}{$this->getSuffix($suffix)}_{$lang}";
        $template = new NotificationTemplate("senders/system/{$notificationType}");
        $template->email = $email;
        $template->lang = $lang;
        $template->deactivateNotificationGuid = $this->getDeactivateNotificationGuid($email, $lang);
        return (object)[
            'name' => $this->getName(),
            'message' => $template
        ];
    }
    
    protected function getNotification($email, $lang, $suffix = '')
    {
        $lang = empty($lang) ? LANG : $lang;
        $notificationType = "{$this->getName()}{$this->getSuffix($suffix)}_{$lang}";
        $template = new NotificationTemplate("senders/{$notificationType}");
        $template->email = $email;
        $template->lang = $lang;
        $template->deactivateNotificationGuid = $this->getDeactivateNotificationGuid($email, $lang);
        return (object)[
            'name' => $this->getName(),
            'message' => $template
        ];
    }

    abstract protected function send($data);
    protected function afterSendAll() {}
    
    function sendAll()
    {
        foreach ($this->store->getData() as $data)
        {
            $this->send((object)$data);
        }
        $this->afterSendAll();
    }
    
    public function getStoreData()
    {
        return $this->store->getData();
    }
}

if (DEBUG)
{
    class test_AllowedAdresses
    {
        private static $set;
        static function contains($email)
        {
            if (!isset(self::$set))
            {
                self::$set = \Core\Collections\HashSet::of([
                    //"fhliva@outlook.com",
                    "fhliva@gmail.com",
                    //"xxar3s@gmail.com",
                    //"hlivapraca@gmail.com"
                ]);
            }
            return self::$set->contains($email);
        }
    }
}

trait MailNotification
{
    protected function getMailer()
    {
        return ServiceLayer::getService("mailer");
    }
    
    protected function getBalcklist()
    {
        return ServiceLayer::getService("NotificationBlacklist");
    }
    
    protected function getSender()
    {
        return [ SENDER_MAIL => SENDER_NAME ];
    }
    
    function getInterval() { return "every day"; }
    
    function addAttachments($message, $attachments)
    {
        foreach ($attachments as $name => $attachment)
        {
            $message->attach(Attachment::newInstance(
                $attachment['data'],
                $name,
                $attachment['mimeType']
            ));
        }
    }
    
    public function getSystemMessageRegions($data)
    {
         $notification = $this->getSystemNotification($data->email, $data->default_lang);
         $notification->message->assignObject($data);
         
          $regions = \Core\Templates\Regions::parse($notification->message);
          $messageBody = $regions['default'];
          $subject = isset($regions['subject']) ? $regions['subject'] : 'Coach Rufus';
          
          $messageBody =str_replace('btn_green', '" style="margin:40px 0;display: inline-block; background:#00b3bd; padding-left:28px; padding-right:28px; line-height:50px; text-decoration: none; border-radius:5px; color:white; height:50px; font-size:16px;"', $messageBody);
          
          $messageBody =str_replace('btn_orange', '" style="padding: 10px 20px;display:inline-block; border-radius: 10px; color:white; background:#ff6d22; text-align: center; text-decoration: none; font-size:16px;"', $messageBody);
          
          $messageBody =str_replace('btn_red', '" style="padding: 10px 20px;display:inline-block; border-radius: 10px; color:white; background:#D73549; text-align: center; text-decoration: none; font-size:16px;"', $messageBody);
          
          
          
          
          $subject = isset($regions['subject']) ? $regions['subject'] : 'Coach Rufus';
          
          $messageBody =str_replace('btn_green', '" style="padding: 10px 20px;display:inline-block; border-radius: 10px; color:white; background:#7ab837; text-align: center; text-decoration: none; font-size:16px;"', $messageBody);
          
          $messageBody =str_replace('btn_orange', '" style="padding: 10px 20px;display:inline-block; border-radius: 10px; color:white; background:#ff6d22; text-align: center; text-decoration: none; font-size:16px;"', $messageBody);
          
          $messageBody =str_replace('btn_red', '" style="padding: 10px 20px;display:inline-block; border-radius: 10px; color:white; background:#D73549; text-align: center; text-decoration: none; font-size:16px;"', $messageBody);
          
          
          
          
          $subject = isset($regions['subject']) ? $regions['subject'] : 'Coach rufus';
          
          return array('body' => $messageBody,'subject' => $subject);
    }
    
    public function getMessageRegions($data)
    {
         $notification = $this->getNotification($data->email, $data->default_lang,$data->isAdmin ? 'admin' : '');
         $notification->message->assignObject($data);
         
         if(isset($data->lineupSummary))
         {
              $notification->message->summary = $data->lineupSummary;
         }
         
         
         
          $regions = \Core\Templates\Regions::parse($notification->message);
        
          return $this->replaceButtonClass($regions);
    }
    
    public function replaceButtonClass($regions)
    {
         $messageBody = $regions['default'];
         $subject = isset($regions['subject']) ? $regions['subject'] : 'Coach Rufus';
          
          $messageBody =str_replace('class="btn_green"', 'style="margin:40px 0;display: inline-block; background:#00b3bd; padding-left:28px; padding-right:28px; line-height:50px; text-decoration: none; border-radius:5px; color:white; height:50px; font-size:16px;"', $messageBody);
          
           $messageBody =str_replace('class="btn_light_green"', 'style="margin:40px 0;display: inline-block; background:#7BB838; padding-left:28px; padding-right:28px; line-height:50px; text-decoration: none; border-radius:5px; color:white; height:50px; font-size:16px;"', $messageBody);
          
          $messageBody =str_replace('class="btn_orange"', 'style="padding: 10px 20px;display:inline-block; border-radius: 10px; color:white; background:#ff6d22; text-align: center; text-decoration: none; font-size:16px;"', $messageBody);
          
          $messageBody =str_replace('class="btn_red"', 'style="margin:40px 0;display: inline-block; background:#D73549; padding-left:28px; padding-right:28px; line-height:50px; text-decoration: none; border-radius:5px; color:white; height:50px; font-size:16px;"', $messageBody);
          
          $messageBody =str_replace('class="p-text"', ' style=\'color: #47545c;font-family:"Helvetica", Roboto, Arial, sans-serif; font-size:16px;\'', $messageBody);
          
          $messageBody =str_replace('class="link"', ' style=\'color: #47545c;font-family:"Helvetica", Roboto, Arial, sans-serif; font-size:16px; text-decoration: none;color:#00b3bd;\'', $messageBody);
          
          
          
          $messageBody =str_replace('btn_green', '" style="padding: 10px 20px;display:inline-block; border-radius: 10px; color:white; background:#7ab837; text-align: center; text-decoration: none; font-size:16px;"', $messageBody);
          
          $messageBody =str_replace('btn_orange', '" style="padding: 10px 20px;display:inline-block; border-radius: 10px; color:white; background:#ff6d22; text-align: center; text-decoration: none; font-size:16px;"', $messageBody);
          
          $messageBody =str_replace('btn_red', '" style="padding: 10px 20px;display:inline-block; border-radius: 10px; color:white; background:#D73549; text-align: center; text-decoration: none; font-size:16px;"', $messageBody);
          
          return array('body' => $messageBody,'subject' => $subject);
    }
    
    
    protected function embedMessageImages($message_body, $mail_message, $images_base_path)
    {
        $dom = new \DOMDocument();
        $dom->loadHTML("<?xml encoding='UTF-8'>{$message_body}");
        $images = $dom->getElementsByTagName('img');
        $xpath = new \DOMXpath($dom);
        foreach($images as $image)
        {
            $path = "{$images_base_path}{$image->getAttribute('src')}";
            if (is_file($path))
            {
                $embed_image = $mail_message->embed(\Swift_Image::fromPath($path)); 
                $message_body = str_replace($image->getAttribute('src'), $embed_image, $message_body);
            }
        }
        return $message_body;
    }
    
    

    function sendMail($to, string $body, $attachments = null)
    {
        if (DEBUG && !test_AllowedAdresses::contains($to)) return;
        if (!$this->getBalcklist()->isBlacklisted($to))
        {
            $mailer = $this->getMailer();
            $message = $mailer->createMessage();
            try
            {
                $regions = Regions::parse($body);
                
                $emailContent =  $this->replaceButtonClass($regions);
                
                
                
                $messageBody = $emailContent['body'];
                $messageBody = $this->embedMessageImages($messageBody, $message, PUBLIC_DIR);
                $subject = isset($emailContent['subject']) ? $emailContent['subject'] : 'Coach Rufus';
                $message->setSubject($subject);
                $message->setFrom($this->getSender());
                $message->setTo($to);
                $message->setBody($messageBody, 'text/html');
                if (!is_null($attachments))
                {
                    $this->addAttachments($message, $attachments);
                }
                
                //add to queue
                $qData = array();
                $qData['created_at'] = date('Y-m-d H:i:s');
                $qData['body'] = $messageBody;
                $qData['subject'] = $subject;
                $qData['recipient'] = $to;
                $qData['status'] = 'waiting';
                $qData['sent_at'] = '';
                $qData['sender'] =implode(',',$this->getSender());
                \Core\DbQuery::insertFromArray($qData, 'email_queue');
                
                $mailer->sendMessage($message);
            }
            catch(\Exception $e)
            {
                \MailLogger::log($message->getTo(), 'error', $e->getMessage());
            }
        }
    }
}
