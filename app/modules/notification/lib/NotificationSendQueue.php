<?php namespace Core\Notifications;

use Core\Collections\Queue;

class NotificationSendQueue extends Queue
{
    function register(NotificationSender $sender)
    {
        return new self(array_merge($this->getItems(), [$sender]));
    }
    
    function send($interval = null)
    {
        foreach ($this as $sender)
        {
            if (is_null($interval) || $interval == $sender->getInterval())
            {
                $sender->sendAll();
            }
        }
        return $this;
    }
}