<?php namespace Core\Notifications;

use Core\Collections\HashSet as Set;
use \__;

trait PlayerEventInfosLang
{
    private function getLangs($playerEventInfos)
    {
        $userIds = Set::of(__::map($playerEventInfos, function($info)
        {
            return $info->subject->getTeamPlayer()->getPlayerId();
        }))->toArray();
        return $this->db->co_users('id', $userIds)
            ->select('id, default_lang')
            ->fetchPairs('id', 'default_lang');
    }
    
    protected function addData($playerEventInfos)
    {
        $langs = $this->getLangs($playerEventInfos);
        return iterator_to_array((function($playerEventInfos, $langs)
        {
            foreach ($playerEventInfos as $playerEventInfo)
            {
                $playerEventInfo->userId = $playerEventInfo->subject->getTeamPlayer()->getPlayerId();
                $playerEventInfo->lang = is_null($langs[$playerEventInfo->userId]) ?
                    LANG : $langs[$playerEventInfo->userId];
                yield $playerEventInfo;
            }
        })($playerEventInfos, $langs));
    }
}