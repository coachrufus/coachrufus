<?php namespace Core\Notifications;

class PasswordLostStore extends NotificationStore
{
    protected function dataSource()
    {
        //return array('test' => 'blawwwwwwwwwwwww');
    }
}

class PasswordLostNotificationSender extends NotificationSender
{
    use MailNotification;
    
    public function getNotificationMessage($data)
    {
        $notification = $this->getNotification($data['lang'],$data['lang']);
        $notification->message->lang = $data['lang'];
        $notification->message->name = $data['name'];
        $notification->message->activationGuid = $data['activationGuid'];
        $notification->message->password = $data['password'];
        
        return $notification->message;
    }
    

    function send($data)
    {
        $message = $this->getNotificationMessage($data);
        $this->sendMail($data['email'], $message);
    }
    
    
}