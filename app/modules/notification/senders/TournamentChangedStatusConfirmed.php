<?php namespace Core\Notifications;

class  TournamentChangedStatusConfirmedStore extends NotificationStore
{
    protected function dataSource()
    {
        //return array('test' => 'blawwwwwwwwwwwww');
    }
}

class TournamentChangedStatusConfirmedNotificationSender extends NotificationSender
{
    use MailNotification;
    
     public function getNotificationMessage($data)
    {
        $notification = $this->getNotification($data['email'],$data['lang']);
        $notification->message->lang = $data['lang'];
        $notification->message->tournament_name = $data['tournament_name'];
        $notification->message->team_name = $data['team_name'];
        $notification->message->user_name = $data['user_name'];
      
        return $notification->message;
    }
    

    function send($data)
    {
        $message = $this->getNotificationMessage($data);
        $this->sendMail($data['email'], $message);
    }
}