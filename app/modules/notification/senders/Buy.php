<?php namespace Core\Notifications;

use Core\ServiceLayer;

class BuyNotificationSender extends NotificationSender
{
    use MailNotification;
    
    function getInterval() { return "none"; }
    
    private function invoice($invoiceNo)
    {
        return file_get_contents(INVOICE_DIR . "/{$invoiceNo}.pdf");
    }

    private function getInvoicePdfLink($invoiceNo)
    {
        $request = ServiceLayer::getService('request');
        return "{$request->getRoot()}/invoices/{$invoiceNo}.pdf";
    }
    
    function send($invoice)
    {
        $lang = ServiceLayer::getService('translator')->getLang();
        $notification = $this->getNotification($invoice->email, $lang);
        $notification->message->assignObject($invoice);
        $notification->message->invoicePdfLink = $this->getInvoicePdfLink($invoice->invoice_no);
        $this->sendMail($invoice->email, $notification->message);
    }
}