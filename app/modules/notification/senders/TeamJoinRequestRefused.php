<?php namespace Core\Notifications;

class TeamJoinRequestRefusedStore extends NotificationStore
{
    protected function dataSource()
    {
        //return array('test' => 'blawwwwwwwwwwwww');
    }
}

class TeamJoinRequestRefusedNotificationSender extends NotificationSender
{
    use MailNotification;
    

    function send($userInfo)
    {
        $notification = $this->getNotification($userInfo->email, $userInfo->default_lang);
        $notification->message->assignObject($userInfo);
        $this->sendMail($userInfo->email, $notification->message);
    }
}