<?php namespace Core\Notifications;

class AddRateNotificationSender extends NotificationSender
{
    use MailNotification;
    
    function getInterval() { return "none"; }
    
    private function addEventDetailLink(&$template, $data)
    {
        $permissions = $this->getPermissionManager();
        $template->eventDetailGuid = $permissions->newPermission("EVENT_RATE_REDIRECT", $data)->guid;
    }
    
    function send($playerEventInfo)
    {
        $teamPlayer = $playerEventInfo->subject->getTeamPlayer();
        if (!empty($teamPlayer->getEmail()))
        {
            $notification = $this->getNotification($teamPlayer->getEmail(), $playerEventInfo->lang);
            $notification->message->assignObject($playerEventInfo);
            $notification->message->subject = $playerEventInfo->subject;
            $eventId = $playerEventInfo->event->getId();
            $startDateString = $playerEventInfo->start->toString('Y-m-d');
            $this->addEventDetailLink($notification->message, [
                'userId' => $playerEventInfo->userId,
                'eventId' => $eventId,
                'start' => $startDateString
            ]);
            $this->sendMail($teamPlayer->getEmail(), $notification->message);
        }
    }
}