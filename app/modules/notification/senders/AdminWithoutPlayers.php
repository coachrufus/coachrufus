<?php namespace Core\Notifications;

class AdminWithoutPlayersStore extends UniqueItemsNotificationStore
{
    protected function dataSource()
    {
        return \Core\DbQuery::prepare('
            SELECT
                u.id,
                u.email,
                u.name,
                u.surname,
                u.default_lang,
                t.team_id,
                t.team_name
            FROM (
                SELECT
                  t.id AS team_id,
                  t.name AS team_name,
                  t.author_id,
                  COUNT(tp.id) as `count`
                FROM team t
                LEFT JOIN team_players tp
                ON tp.team_id = t.id
                WHERE was_before_days(t.last_login, :days)
                GROUP BY t.id, t.name, t.author_id
            ) t
            JOIN co_users u ON u.id = t.author_id
            WHERE
              t.count <= 1')
                ->bindParam('days', 14)
                ->execute()
                ->fetchAll(\PDO::FETCH_ASSOC);
    }
}

class AdminWithoutPlayersNotificationSender extends NotificationSender
{
    use MailNotification;
    
    function send($userInfo)
    {
        $notification = $this->getNotification($userInfo->email, $userInfo->default_lang);
        $notification->message->assignObject($userInfo);
        $this->sendMail($userInfo->email, $notification->message);
    }
}