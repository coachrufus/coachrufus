<?php namespace Core\Notifications;

class EventInvitationSingleNotificationSender extends NotificationSender
{
    use MailNotification;
    
    function getInterval() { return "every minute"; }

    private function addInOutLinks(&$template, $data)
    {
        $permissions = $this->getPermissionManager();
        foreach ([
            'EVENT_IN' => 'eventInGuid',
            'EVENT_OUT' => 'eventOutGuid'
        ] as $subject => $prop)
        {
            $template->{$prop} = $permissions->newPermission($subject, $data)->guid;
        }
    }
    
    private function iCal($eventId, $dateTimeString)
    {
        $request = \Core\ServiceLayer::getService('request');
        $dateTimeString = urlencode($dateTimeString);
        return file_get_contents("{$request->getRoot()}/event/ical/{$eventId}/subscription?datetime={$dateTimeString}");
    }
    
    function send($playerEventInfo)
    {
        $notification = $this->getNotification($playerEventInfo->user->email, $playerEventInfo->user->default_lang);
        $notification->message->assignObject($playerEventInfo);
        $this->addInOutLinks($notification->message, [
            'eventId' => $playerEventInfo->event->getId(),
            'teamPlayerId' => $playerEventInfo->teamPlayer->getId(),
            'start' => $playerEventInfo->start->toString('Y-m-d'),
            'userId' => $playerEventInfo->user->id,
        ]);
        $this->sendMail($playerEventInfo->user->email, $notification->message,
        [
            'iCal' =>
            [
                'data' => $this->iCal($playerEventInfo->event->getId(), (string)$playerEventInfo->start),
                'mimeType' => 'text/calendar'
            ]
        ]);
    }
}