<?php namespace Core\Notifications;

class EventCancelPlayerAttendanceStore extends NotificationStore{
    
 
    protected function dataSource()
    {
       
    }
}

class EventCancelPlayerAttendanceNotificationSender extends NotificationSender
{
    use MailNotification;
    
    function getInterval() { return "once"; }


    function send($playerEventInfo)
    {
        $notification = $this->getNotification($playerEventInfo->email, $playerEventInfo->lang);
        $notification->message->assignObject($playerEventInfo);
        $this->sendMail($playerEventInfo->email, $notification->message);
    }
}