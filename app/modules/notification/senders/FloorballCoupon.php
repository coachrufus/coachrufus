<?php namespace Core\Notifications;

class FloorballCouponStore extends NotificationStore
{
    protected function dataSource()
    {

    }
}

class FloorballCouponNotificationSender extends NotificationSender
{
    use MailNotification;
    
    public function getNotificationMessage($data)
    {
        $notification = $this->getNotification($data['lang'],$data['lang']);
        $notification->message->lang = $data['lang'];
        $notification->message->name = $data['name'];
        $notification->message->activationGuid = $data['activationGuid'];
        
        return $notification->message;
    }
    

    function send($data)
    {
        $message = $this->getNotificationMessage($data);
        $this->sendMail($data['email'], $message);
    }
}