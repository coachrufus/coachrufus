<?php namespace Core\Notifications;

use Core\ServiceLayer;
use Core\Types\DateTimeEx;
use Core\Db;
use Core\Events\EventInfo;

class EventSummaryStore extends NotificationStore
{
    use Db;
    use PlayerEventInfosLang;
    
    private $db;
    /**
     * force add event info to data
     * @var type 
     */
    private $forceEventInfo = false;
    
    function __construct() { $this->db = $this->getDb(); }
    
     public  function getForceEventInfo() {
return $this->forceEventInfo;
}

public  function setForceEventInfo( $forceEventInfo) {
$this->forceEventInfo = $forceEventInfo;
}
    

  public  function getEvents() 
    {
        if(null == $this->events)
        {
            $teamEventManager = ServiceLayer::getService('TeamEventManager');
            $this->events = $teamEventManager->getBeepEvents(2);
        }

        return $this->events;
    }

    public  function setEvents($events) {
        $this->events = $events;
    }

    protected function getActualEventInfos()
    {
        $teamManager = ServiceLayer::getService('TeamManager');
        $teamEventManager = ServiceLayer::getService('TeamEventManager');
        $now = DateTimeEx::now();
        foreach ($this->getEvents() as $eventDateString => $dateEvents)
        {
            foreach ($dateEvents as $event)
            {
                $adminDateTime = $now->addHour(4)->setSecond(0);
                $playerDateTime = $now->addHour(2)->setSecond(0);
                $currentDateTime = DateTimeEx::of($event->getCurrentDate());
                    
                if (DEBUG)
                {
                    test_updateHoursAndMins($adminDateTime);
                    test_updateHoursAndMins($playerDateTime);
                    test_updateHoursAndMins($currentDateTime);
                }
                $eventInfo = new EventInfo($event);

                if ((string)$adminDateTime === (string)$currentDateTime  or $this->getForceEventInfo())
                {
                    foreach ($eventInfo->getAdminSubjects() as $subject)
                    {
                        if($subject->getTeamPlayer()->getStatus() != 'unactive')
                        {
                             
                            yield (object)[
                            'subject' => $subject,
                            'event' => $event,
                            'start' => $currentDateTime,
                            'isAdmin' => true
                        ];
                        }
                        
                       
                    }
                }
                
                if ((string)$playerDateTime === (string)$currentDateTime  or $this->getForceEventInfo())
                {
                    foreach ($eventInfo->getSubjectsWithoutAdmins() as $subject)
                    {
                        if ($subject->getAttendance()->getStatus() == 1 && $subject->getTeamPlayer()->getStatus() != 'unactive')
                        {
                            yield (object)[
                                'subject' => $subject,
                                'event' => $event,
                                'start' => $currentDateTime,
                                'isAdmin' => false
                            ];
                        }
                    }
                }


            }
        }
    }
    
    protected function dataSource()
    {
        return $this->addData(iterator_to_array($this->getActualEventInfos()));
    }
}