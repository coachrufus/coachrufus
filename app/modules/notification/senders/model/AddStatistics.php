<?php namespace Core\Notifications;

use Core\ServiceLayer;
use Core\Types\DateTimeEx;
use Core\Db;
use Core\Events\EventInfo;

class AddStatisticsStore extends NotificationStore
{
    use Db;
    use PlayerEventInfosLang;
    
    private $db;
    private $events;
    /**
     * force add event info to data
     * @var type 
     */
    private $forceEventInfo = false;
    
    function __construct() { $this->db = $this->getDb(); }
    
    public  function getForceEventInfo() {
return $this->forceEventInfo;
}

public  function setForceEventInfo( $forceEventInfo) {
$this->forceEventInfo = $forceEventInfo;
}



    
    public  function getEvents() 
    {
        if(null == $this->events)
        {
            $date = new \DateTime(date('Y-m-d H:i:00'));
            $date->setTimestamp(strtotime("- 1 day"));

            $teamEventManager = ServiceLayer::getService('TeamEventManager');

            $listTo = new \DateTime($date->format('Y-m-d'));
            $listTo->setTimestamp(strtotime("+ 2 day"));

            $beepEvents = $teamEventManager->getRepository()->findNotifyTeamEvents(array('from' => $date, 'to' => $listTo));
            $this->events = $teamEventManager->buildTeamEventList($beepEvents, $date, $listTo);
            
            
        }

        return $this->events;
    }

    public  function setEvents($events) {
        $this->events = $events;
    }
    


    public function getEventInfoTest($now = null)
    {
        if($now == null)
        {
            $now = DateTimeEx::now();
        }
        
        $toSend = array();
        foreach ($this->getEvents() as $eventDateString => $dateEvents)
        {
            foreach ($dateEvents as $event)
            {
                $type = $event->getEventType();
                if ($type === "competition" || $type === "training")
                {
                    $adminDateTime = $now->setSecond(0);
                    $currentDateTime = DateTimeEx::of($event->getCurrentDate())
                        ->addHour(2);
                     $currentDateTime->setSecond(0);

                    if ((string)$adminDateTime === (string)$currentDateTime or $this->getForceEventInfo())
                    {
                       $toSend[$event->getId()] = array($event->getCurrentDate()->format('Y-m-d H:i:s'),(string)$adminDateTime,(string)$currentDateTime);
                    }
                }
            }
        }
        
        return $toSend;
    }

    
    protected function getActualEventInfos()
    {
        $teamManager = ServiceLayer::getService('TeamManager');
        $teamEventManager = ServiceLayer::getService('TeamEventManager');
        $now = DateTimeEx::now();
        
        $events = $this->getEvents();
        foreach ($this->getEvents() as $eventDateString => $dateEvents)
        {
            foreach ($dateEvents as $event)
            {
                $type = $event->getEventType();
                if ($type === "competition" || $type === "training")
                {
                    $adminDateTime = $now->setSecond(0);
                    
                    $currentDateTime = DateTimeEx::of($event->getCurrentDate())
                        ->addHour(2);
                     $currentDateTime->setSecond(0);
                    
                    /*
                    $currentDateTime = DateTimeEx::of($event->getCurrentDate())
                        ->addHour(2)
                        ->dateFrom(DateTimeEx::parse($eventDateString, 'Y-m-d')->getDate())
                        ->setSecond(0);
                     * 
                     */
                    if (DEBUG)
                    {
                        test_updateHoursAndMins($adminDateTime);
                        test_updateHoursAndMins($currentDateTime);
                    }
                    $eventInfo = new EventInfo($event);

                    if ((string)$adminDateTime === (string)$currentDateTime or $this->getForceEventInfo())
                    {
                        foreach ($eventInfo->getAdminSubjects() as $subject)
                        {
                            yield (object)[
                                'subject' => $subject,
                                'event' => $event,
                                'start' => $currentDateTime,
                                'isAdmin' => true
                            ];
                        }
                    }
                }
            }
        }
    }
    
    protected function dataSource()
    {
        return $this->addData(iterator_to_array($this->getActualEventInfos()));
    }
}