<?php namespace Core\Notifications;

use Core\ServiceLayer;
use Core\Types\DateTimeEx;
use Core\Db;
use Core\Events\EventInfo;

class AddRateStore extends NotificationStore
{
    use Db;
    use PlayerEventInfosLang;
    
    private $db;
    private $event;
    
    function __construct($event)
    {
        $this->db = $this->getDb();
        $this->event = $event;
    }
    
    protected function getActualEventInfos()
    {
        $teamManager = ServiceLayer::getService('TeamManager');
        $teamEventManager = ServiceLayer::getService('TeamEventManager');
        $now = DateTimeEx::now();
        $type = $this->event->getEventType();
        
        

        
        if ($type === "competition" || $type === "training" || $type === "game")
        {
            $eventInfo = new EventInfo($this->event);
            
            
            foreach ($eventInfo->getSubjectsWithoutAdmins() as $subject)
            {
                if ($subject->getAttendance()->getStatus() == 1)
                {
                    yield (object)[
                        'subject' => $subject,
                        'event' => $this->event,
                        'start' => new DateTimeEx($this->event->getCurrentDate()),
                        'isAdmin' => false
                    ];
                }
            }
            
             foreach ($eventInfo->getAdminSubjects() as $subject)
                    {
                        if($subject->getTeamPlayer()->getStatus() != 'unactive')
                        {
                             
                            yield (object)[
                            'subject' => $subject,
                            'event' => $this->event,
                            'start' => new DateTimeEx($this->event->getCurrentDate()),
                            'isAdmin' => true
                        ];
                        }
                        
                       
                    }
        }
    }
    
    protected function dataSource()
    {
        return $this->addData(iterator_to_array($this->getActualEventInfos()));
    }
}