<?php namespace Core\Notifications;

use Core\ServiceLayer;
use Core\Types\DateTimeEx;
use Core\Collections\HashSet as Set;
use Core\Db;
use Core\DbUtils;
use \__;

if (DEBUG)
{
    function test_updateHoursAndMins(DateTimeEx &$dateTime)
    {
        $dateTime = $dateTime->setHour(10)->setMinute(12);
    }
}

class EventInvitationStore extends NotificationStore
{
    use Db;
    
    private $db;
    
    private $forceEventInfo = false;
    
    function __construct() { $this->db = $this->getDb(); }
    
     public  function getForceEventInfo() {
return $this->forceEventInfo;
}

public  function setForceEventInfo( $forceEventInfo) {
$this->forceEventInfo = $forceEventInfo;
}
    
    public  function getEvents() 
    {
        if(null == $this->events)
        {
            $teamEventManager = ServiceLayer::getService('TeamEventManager');
            $this->events = $teamEventManager->getBeepEvents(7);
        }

        return $this->events;
    }

    public  function setEvents($events) {
        $this->events = $events;
    }

    protected function getActualEvents()
    {
        $teamManager = ServiceLayer::getService('TeamManager');
        $teamEventManager = ServiceLayer::getService('TeamEventManager');
        foreach ($this->getEvents() as $eventDateString => $dateEvents)
        {
            
            foreach ($dateEvents as $event)
            {
                if ($event->getPeriod() === "none") continue;

                $nextEventDateTime = DateTimeEx::now()
                    ->addDay($event->getNotifyBeepDays())
                    ->setSecond(0);

                $currentDateTime = DateTimeEx::of($event->getCurrentDate());
               

                if (DEBUG)
                {
                    test_updateHoursAndMins($nextEventDateTime);
                    test_updateHoursAndMins($currentDateTime);
                }

                if ((string)$nextEventDateTime === (string)$currentDateTime or $this->getForceEventInfo())
                {
                    
                     //send only players with non confirmed attendance
                    $attendance = $teamEventManager->getEventAttendance($event);
                    $event->setAttendance($attendance);

                    $confirmedPlayerIds = array();
                    foreach($attendance as $eventAttendance)
                    {
                        if($eventAttendance->getStatus() == '1')
                        {
                            $confirmedPlayerIds[]  = $eventAttendance->getTeamPlayerId();
                        }
                    }

                    foreach ($teamManager->getTeamPlayers($event->getTeamId()) as $teamPlayer)
                    {
                        if ((int)$teamPlayer->getPlayerId() !== 0 && !in_array($teamPlayer->getId() , $confirmedPlayerIds) && $teamPlayer->getStatus() != 'unactive')
                        {
                            yield (object)[ 
                                'event' => $event,
                                'teamPlayer' => $teamPlayer,
                                'start' => $currentDateTime
                            ];
                        }
                    }
                }
            }
        }
    }
    
    private function getUserIds($playerEventInfos)
    {
        return Set::of(__::map($playerEventInfos, function($playerEventInfo)
        {
            return $playerEventInfo->teamPlayer->getPlayerId();
        }))->toArray();
    }
    
    private function getUsers($userIds)
    {
        foreach ($this->db->co_users()
            ->select('id, email, default_lang, name, surname')
            ->where('id', $userIds) as $user)
        {
            yield $user['id'] => (object)iterator_to_array($user);
        }
    }
    
    private function addData($playerEventInfos)
    {
        $users = iterator_to_array($this->getUsers($this->getUserIds($playerEventInfos)));
        $players = __::map($playerEventInfos, function($i) { return $i->teamPlayer; });
        foreach ($playerEventInfos as $playerEventInfo)
        {
            $userId = $playerEventInfo->teamPlayer->getPlayerId();
            $playerEventInfo->user = $users[$userId];
            yield $playerEventInfo;
        }
    }
    
    protected function dataSource()
    {
        return $this->addData(iterator_to_array($this->getActualEvents()));
    }
}

class ImmediatelyEventInvitationStore extends EventInvitationStore
{
    private $event;
    
    function __construct($event)
    {
        parent::__construct();
        $this->event = $event;
    }
    
    protected function getActualEvents()
    {
        $nearestTermin = DateTimeEx::of($this->event->getNearestTermin(new \DateTime()))
            ->timeFrom(DateTimeEx::of($this->event->getStart()));
        foreach (ServiceLayer::getService('TeamManager')->getTeamPlayers($this->event->getTeamId()) as $teamPlayer)
        {
            if ((int)$teamPlayer->getPlayerId() !== 0)
            {
                yield (object)[ 
                    'event' => $this->event,
                    'teamPlayer' => $teamPlayer,
                    'start' => $nearestTermin
                ];
            }
        }
    }
}

class CustomImmediatelyEventInvitationStore extends EventInvitationStore
{
    private $event;
    private $recipients;
    
    function __construct($event,$recipients)
    {
        parent::__construct();
        $this->event = $event;
        $this->recipients = $recipients;
    }
    
    protected function getActualEvents()
    {
        $nearestTermin = DateTimeEx::of($this->event->getCurrentDate())->timeFrom(DateTimeEx::of($this->event->getStart()));
        foreach ($this->recipients as $teamPlayer)
        {
            if ((int)$teamPlayer->getPlayerId() !== 0)
            {
                yield (object)[ 
                    'event' => $this->event,
                    'teamPlayer' => $teamPlayer,
                    'start' => $nearestTermin
                ];
            }
        }
    }
}