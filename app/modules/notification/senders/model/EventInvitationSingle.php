<?php namespace Core\Notifications;

use Core\ServiceLayer;
use Core\Types\DateTimeEx;
use Core\Collections\HashSet as Set;
use Core\Db;
use Core\DbUtils;
use \__;

class EventInvitationSingleStore extends NotificationStore
{
    use Db;
    
    private $db;
    
    function __construct() { $this->db = $this->getDb(); }
    
    protected function getActualEvents()
    {
        $teamManager = ServiceLayer::getService('TeamManager');
        $teamEventManager = ServiceLayer::getService('TeamEventManager');
        foreach ($teamEventManager->getBeepEvents(7) as $eventDateString => $dateEvents)
        {
            foreach ($dateEvents as $event)
            {
                if ($event->getPeriod() !== "none") continue;

                $nextEventDateTime = DateTimeEx::now()
                    ->addDay($event->getNotifyBeepDays())
                    ->setSecond(0);
                $currentDateTime = DateTimeEx::of($event->getCurrentDate());
                if (DEBUG)
                {
                    test_updateHoursAndMins($nextEventDateTime);
                    test_updateHoursAndMins($currentDateTime);
                }
                if ((string)$nextEventDateTime === (string)$currentDateTime)
                {
                    foreach ($teamManager->getTeamPlayers($event->getTeamId()) as $teamPlayer)
                    {
                        if ((int)$teamPlayer->getPlayerId() !== 0)
                        {
                            yield (object)[ 
                                'event' => $event,
                                'teamPlayer' => $teamPlayer,
                                'start' => $currentDateTime
                            ];
                        }
                    }
                }
            }
        }
        $todayEvents = $teamEventManager->getDateNotifyEvents();
        $now = DateTimeEx::now();
        //dump($todayEvents);exit;
        foreach ($todayEvents as $event)
        {
            if (DEBUG)
            {
                $now = $now->setHour(6)->setMinute(0);
            }
            if ($event->getPeriod() !== "none" || !($now->getHour() == 6 && $now->getMinute() == 0)) continue;
            $terminFind = false;
            $i = 0;
            while ($terminFind == false and $i < 60)
            {
                $terminDate = new \DateTime(date('Y-m-d', strToTime("+{$i} day")));
                $nearestDate = $event->getNearestTermin($terminDate);
                if (!is_null(nearestDate))
                {
                    $terminFind = true;
                    $event->setCurrentDate($nearestDate);
                }
                $i++;
            }
            foreach ($teamManager->getTeamPlayers($event->getTeamId()) as $teamPlayer)
            {
                if ((int)$teamPlayer->getPlayerId() !== 0)
                {
                    yield (object)[ 
                        'event' => $event,
                        'teamPlayer' => $teamPlayer,
                        'start' =>  DateTimeEx::of($nearestDate)
                    ];
                }
            }
        }
    }
    
    private function getUserIds($playerEventInfos)
    {
        return Set::of(__::map($playerEventInfos, function($playerEventInfo)
        {
            return $playerEventInfo->teamPlayer->getPlayerId();
        }))->toArray();
    }
    
    private function getUsers($userIds)
    {
        foreach ($this->db->co_users()
            ->select('id, email, default_lang, name, surname')
            ->where('id', $userIds) as $user)
        {
            yield $user['id'] => (object)iterator_to_array($user);
        }
    }
    
    private function addData($playerEventInfos)
    {
        $users = iterator_to_array($this->getUsers($this->getUserIds($playerEventInfos)));
        $players = __::map($playerEventInfos, function($i) { return $i->teamPlayer; });
        foreach ($playerEventInfos as $playerEventInfo)
        {
            $userId = $playerEventInfo->teamPlayer->getPlayerId();
            $playerEventInfo->user = $users[$userId];
            yield $playerEventInfo;
        }
    }
    
    protected function dataSource()
    {
        return $this->addData(iterator_to_array($this->getActualEvents()));
    }
}