<?php namespace Core\Notifications;

use Core\ServiceLayer;
use Core\Types\DateTimeEx;
use Core\Db;
use Core\Events\EventInfo;
use Core\Collections\HashSet as Set;
use \__;

class MatchResultsStore extends NotificationStore
{
    use Db;
    
    private $db;
    private $event;
    
    function __construct($event)
    {
        $this->db = $this->getDb();
        $this->event = $event;
    }
    
    protected function getActualEventInfos()
    {
        $teamManager = ServiceLayer::getService('TeamManager');
        $teamEventManager = ServiceLayer::getService('TeamEventManager');
        $now = DateTimeEx::now();
        $type = $this->event->getEventType();
        $eventInfo = new EventInfo($this->event);
        foreach ($eventInfo->getTeamPlayers() as $player)
        {
            yield (object)[
                'player' => $player,
                'event' => $this->event,
                'start' => new DateTimeEx($this->event->getCurrentDate())
            ];
        }
    }
    
    private function getLangs($playerEventInfos)
    {
        $userIds = Set::of(__::map($playerEventInfos, function($info)
        {
            return $info->player->getId();
        }))->toArray();
        return $this->db->co_users('id', $userIds)
            ->select('id, default_lang')
            ->fetchPairs('id', 'default_lang');
    }
    
    protected function addData($playerEventInfos)
    {
        $langs = $this->getLangs($playerEventInfos);
        return iterator_to_array((function($playerEventInfos, $langs)
        {
            foreach ($playerEventInfos as $playerEventInfo)
            {
                $playerEventInfo->userId = $playerEventInfo->player->getPlayerId();
                $playerEventInfo->lang = !isset($langs[$playerEventInfo->userId]) || is_null($langs[$playerEventInfo->userId]) ?
                    LANG : $langs[$playerEventInfo->userId];
                yield $playerEventInfo;
            }
        })($playerEventInfos, $langs));
    }
    
    protected function dataSource()
    {
        return $this->addData(iterator_to_array($this->getActualEventInfos()));
    }
}