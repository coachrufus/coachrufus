<?php namespace Core\Notifications;

use Core\ServiceLayer;
use Core\Types\DateTimeEx;
use Core\Db;
use Core\Events\EventInfo;
use Core\Collections\HashSet as Set;
use \__;

class BuyStore extends NotificationStore
{
    private $invoice;

    function __construct($invoice)
    {
        $this->invoice = $invoice;
    }
    
    protected function dataSource()
    {
        return [$this->invoice];
    }
}