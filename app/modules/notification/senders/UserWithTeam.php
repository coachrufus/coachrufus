<?php namespace Core\Notifications;

class UserWithTeamStore extends UniqueItemsNotificationStore
{
    protected function dataSource()
    {
        return \Core\DbQuery::prepare('
            SELECT u.id, u.email, u.name, u.surname, u.default_lang FROM
            (
                SELECT co_user_id, max(date) AS date
                FROM log
                GROUP BY co_user_id
            ) d
            LEFT JOIN team_players tp ON tp.player_id = d.co_user_id
            JOIN co_users u ON u.id = co_user_id
            WHERE
                WAS_BEFORE_DAYS(d.date, :days) AND
                tp.player_id IS NOT NULL AND
                 u.receive_notifications = 1
            GROUP BY u.id')
                ->bindParam('days', 14)
                ->execute()
                ->fetchAll(\PDO::FETCH_ASSOC);
    }
}

class UserWithTeamNotificationSender extends NotificationSender
{
    use MailNotification;
    
    function send($userInfo)
    {
        $notification = $this->getNotification($userInfo->email, $userInfo->default_lang);
        $notification->message->assignObject($userInfo);
        $this->sendMail($userInfo->email, $notification->message);
    }
}