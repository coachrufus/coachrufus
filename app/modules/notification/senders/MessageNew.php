<?php namespace Core\Notifications;

class MessageNewStore extends NotificationStore
{
    protected function dataSource()
    {
        //return array('test' => 'blawwwwwwwwwwwww');
    }
}

class MessageNewNotificationSender extends NotificationSender
{
    use MailNotification;
    

    public function getNotificationMessage($data)
    {
        $notification = $this->getNotification($data['lang'],$data['lang']);
        $notification->message->lang = $data['lang'];
        $notification->message->sender_name = $data['sender_name'];
        $notification->message->mid = $data['mid'];
        return $notification->message;
    }
    

    function send($data)
    {
        $message = $this->getNotificationMessage($data);
        $this->sendMail($data['email'], $message);
    }
}