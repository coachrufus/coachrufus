<?php namespace Core\Notifications;

class MatchResultsNotificationSender extends NotificationSender
{
    use MailNotification;
    
    function getInterval() { return "none"; }
    
    private function addEventDetailLink(&$template, $data)
    {
        $permissions = $this->getPermissionManager();
        $template->eventDetailGuid = $permissions->newPermission("EVENT_RATE_REDIRECT", $data)->guid;
    }
    
    function send($playerEventInfo)
    {
        $player = $playerEventInfo->player;
        if (!empty($player->getEmail()))
        {
            $notification = $this->getNotification($player->getEmail(), $playerEventInfo->lang);
            $notification->message->assignObject($playerEventInfo);
            $notification->message->player = $playerEventInfo->player;
            $eventId = $playerEventInfo->event->getId();
            $lineupId = $playerEventInfo->event->getLineup()->getId();
            $startDateString = $playerEventInfo->start->toString('Y-m-d');
            if (!empty($playerEventInfo->userId))
            {
                $this->addEventDetailLink($notification->message, [
                    'userId' => $playerEventInfo->userId,
                    'eventId' => $eventId,
                    'start' => $startDateString,
                    'lineupId' =>$lineupId
                ]);
            }
            
            $this->sendMail($player->getEmail(), $notification->message);
            /*
            if($player->getTeamRole() != 'ADMIN')
            {
                $this->sendMail($player->getEmail(), $notification->message);
            }
            */
            
            
        }
    }
}