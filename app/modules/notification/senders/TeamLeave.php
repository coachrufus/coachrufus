<?php namespace Core\Notifications;

class TeamLeaveStore extends NotificationStore
{
    protected function dataSource()
    {
        //return array('test' => 'blawwwwwwwwwwwww');
    }
}

class TeamLeaveNotificationSender extends NotificationSender
{
    use MailNotification;
    
     public function getNotificationMessage($data)
    {
        $notification = $this->getNotification($data['email'],$data['lang']);
        $notification->message->lang = $data['lang'];
        $notification->message->player_name = $data['player_name'];
        $notification->message->team_name = $data['team_name'];
        $notification->message->scouting_link = $data['scouting_link'];
        return $notification->message;
    }
    

    function send($data)
    {
        $message = $this->getNotificationMessage($data);
        $this->sendMail($data['email'], $message);
    }
}