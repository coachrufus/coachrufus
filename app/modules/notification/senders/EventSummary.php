<?php namespace Core\Notifications;

class EventSummaryNotificationSender extends NotificationSender
{
    use MailNotification;
    
    function getInterval() { return "every minute"; }
    
    private function addEventDetailLink(&$template, $data)
    {
        $permissions = $this->getPermissionManager();
        $template->eventDetailGuid = $permissions->newPermission("EVENT_LINEUP_REDIRECT", $data)->guid;
    }
    
    public function getSummary(int $eventId, string $dateString, bool $isAdmin)
    {
        $request = \Core\ServiceLayer::getService('request');
        $type = $isAdmin ? "admin" : "player";
        return file_get_contents("{$request->getRoot()}/reports/{$type}-event-summary/{$eventId}/{$dateString}");
    }
    
    public function addSummary()
    {
        
    }
    
    function send($playerEventInfo)
    {
        $teamPlayer = $playerEventInfo->subject->getTeamPlayer();
        if (!empty($teamPlayer->getEmail()))
        {
            $notification = $this->getNotification($teamPlayer->getEmail(), $playerEventInfo->lang,
                $playerEventInfo->isAdmin ? 'admin' : '');
            $notification->message->assignObject($playerEventInfo);
            $notification->message->subject = $playerEventInfo->subject;
            $eventId = $playerEventInfo->event->getId();
            $startDateString = $playerEventInfo->start->toString('Y-m-d');
            $this->addEventDetailLink($notification->message, [
                'userId' => $playerEventInfo->userId,
                'eventId' => $eventId,
                'start' => $startDateString
            ]);
            $notification->message->summary = $this->getSummary($eventId, $startDateString, $playerEventInfo->isAdmin);
            
            
             //first check if has allowed this type of notification
            $userRepository =  \Core\ServiceLayer::getService('user_repository');
            $hasAllowedNotication = false;
            $user =  $userRepository->find($playerEventInfo->userId);

            $playerNotifySettings = json_decode($user->getNotifySettings(), true);
            if (array_key_exists('event_summary', $playerNotifySettings['email_notify']))
            {
                 $hasAllowedNotication = true;
            }   

            $hasAllowedTeamNotication = false;
            //check if has allowed notifications for this team
             if (
                     (array_key_exists('all', $playerNotifySettings['team_notify']) && $playerNotifySettings['team_notify']['all'] == '1') or 
                     (array_key_exists($playerEventInfo->event->getTeamId(), $playerNotifySettings['team_notify']) && $playerNotifySettings['team_notify'][$playerEventInfo->event->getTeamId()] == '1') 
            )
            {
                 $hasAllowedTeamNotication = true;
            }   
            
 

            if('' != trim( $notification->message->summary) && $hasAllowedTeamNotication && $hasAllowedNotication)
            {
                $this->sendMail($teamPlayer->getEmail(), $notification->message);
            }

           
            
        }
    }
}