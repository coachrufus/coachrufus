<?php namespace Core\Notifications;

class ShareLinkInvitationAdminStore extends NotificationStore
{
    protected function dataSource()
    {
        //return array('test' => 'blawwwwwwwwwwwww');
    }
}

class ShareLinkInvitationAdminNotificationSender extends NotificationSender
{
    use MailNotification;
    
     public function getNotificationMessage($data)
    {
        $notification = $this->getNotification($data['email'],$data['lang']);
        $notification->message->lang = $data['lang'];
        $notification->message->sender_name = $data['sender_name'];
        $notification->message->team_name = $data['team_name'];
        $notification->message->management_link = $data['management_link'];
        
        return $notification->message;
    }
    

    function send($data)
    {
        $message = $this->getNotificationMessage($data);
        $this->sendMail($data['email'], $message);
    }
}