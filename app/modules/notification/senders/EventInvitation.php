<?php namespace Core\Notifications;

class EventInvitationNotificationSender extends NotificationSender
{
    use MailNotification;
    
    function getInterval() { return "every minute"; }
    
    private $customText;
    
    function getCustomText()
    {
        return $this->customText;
    }

    function setCustomText($customText)
    {
        $this->customText = $customText;
    }

    private function addInOutLinks(&$template, $data)
    {
        $permissions = $this->getPermissionManager();
        foreach ([
            'EVENT_IN' => 'eventInGuid',
            'EVENT_OUT' => 'eventOutGuid'
        ] as $subject => $prop)
        {
            $template->{$prop} = $permissions->newPermission($subject, $data)->guid;
        }
    }
    
    private function iCal($eventId, $dateTimeString)
    {
        $request = \Core\ServiceLayer::getService('request');
        $dateTimeString = urlencode($dateTimeString);
        return file_get_contents("{$request->getRoot()}/event/ical/{$eventId}/subscription?datetime={$dateTimeString}");
    }
    
    function send($playerEventInfo)
    {
        $notification = $this->getNotification($playerEventInfo->user->email, $playerEventInfo->user->default_lang);
        $playerEventInfo->customText = ($this->getCustomText() == null) ? '' : $this->getCustomText();
        $notification->message->assignObject($playerEventInfo);
        $this->addInOutLinks($notification->message, [
            'eventId' => $playerEventInfo->event->getId(),
            'teamPlayerId' => $playerEventInfo->teamPlayer->getId(),
            'start' => $playerEventInfo->start->toString('Y-m-d'),
            'userId' => $playerEventInfo->user->id,
        ]);
        
        //first check if has allowed this type of notification
        $userRepository =  \Core\ServiceLayer::getService('user_repository');
        $hasAllowedNotication = false;
        $user =  $userRepository->find($playerEventInfo->user->id);
        
        
        
        
        $playerNotifySettings = json_decode($user->getNotifySettings(), true);
        if (array_key_exists('event_invitation', $playerNotifySettings['email_notify']))
        {
             $hasAllowedNotication = true;
        }   

        $hasAllowedTeamNotication = false;
        //check if has allowed notifications for this team
         if (
                 (array_key_exists('all', $playerNotifySettings['team_notify']) && $playerNotifySettings['team_notify']['all'] == '1') or 
                 (array_key_exists($playerEventInfo->event->getTeamId(), $playerNotifySettings['team_notify']) && $playerNotifySettings['team_notify'][$playerEventInfo->event->getTeamId()] == '1') 
        )
        {
             $hasAllowedTeamNotication = true;
        }   

        if ($hasAllowedNotication && $hasAllowedTeamNotication)
        {
            $this->sendMail($playerEventInfo->user->email, $notification->message,
            [
                'iCal' =>
                [
                    'data' => $this->iCal($playerEventInfo->event->getId(), (string)$playerEventInfo->start),
                    'mimeType' => 'text/calendar'
                ]
            ]);
        }
        
       
        
        
      
    }
}