<?php 
$translator = Core\ServiceLayer::getService('translator');
?>
<?php if($request->hasFlashMessage('is_active_add_success')): ?>
<div class="alert alert-success">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<?php echo $request->getFlashMessage('is_active_add_success') ?>
</div>
<?php endif; ?>

<form action="" class="form-horizontal form-bordered" method="post">
<?php echo $validator->showAllErrors() ?>

			<div class="form-group">
              <label class="col-sm-3 control-label"><?php echo $translator->translate($form->getFieldLabel("id")) ?><span class="asterisk">*</span></label>
              <div class="col-sm-6">
                <?php echo $form->renderInputTag("id",array('class' => 'form-control')) ?>
                <?php echo $validator->showError("id") ?>
              </div>
            </div>
			<div class="form-group">
              <label class="col-sm-3 control-label"><?php echo $translator->translate($form->getFieldLabel("demand_status_id")) ?></label>
              <div class="col-sm-6">
                <?php echo $form->renderInputTag("demand_status_id",array('class' => 'form-control')) ?>
                <?php echo $validator->showError("demand_status_id") ?>
              </div>
            </div>
			<div class="form-group">
              <label class="col-sm-3 control-label"><?php echo $translator->translate($form->getFieldLabel("notification_type")) ?><span class="asterisk">*</span></label>
              <div class="col-sm-6">
                <?php echo $form->renderInputTag("notification_type",array('class' => 'form-control')) ?>
                <?php echo $validator->showError("notification_type") ?>
              </div>
            </div>
			<div class="form-group">
              <label class="col-sm-3 control-label"><?php echo $translator->translate($form->getFieldLabel("subject")) ?><span class="asterisk">*</span></label>
              <div class="col-sm-6">
                <?php echo $form->renderInputTag("subject",array('class' => 'form-control')) ?>
                <?php echo $validator->showError("subject") ?>
              </div>
            </div>
			<div class="form-group">
              <label class="col-sm-3 control-label"><?php echo $translator->translate($form->getFieldLabel("message")) ?><span class="asterisk">*</span></label>
              <div class="col-sm-6">
                <?php echo $form->renderInputTag("message",array('class' => 'form-control')) ?>
                <?php echo $validator->showError("message") ?>
              </div>
            </div>
			<div class="form-group">
              <label class="col-sm-3 control-label"><?php echo $translator->translate($form->getFieldLabel("is_active")) ?><span class="asterisk">*</span></label>
              <div class="col-sm-6">
                <?php echo $form->renderInputTag("is_active",array('class' => 'form-control')) ?>
                <?php echo $validator->showError("is_active") ?>
              </div>
            </div>
 <div class="row">
	<div class="col-sm-6 col-sm-offset-3">
	  <button class="btn btn-primary">Submit</button>&nbsp;
	  <button class="btn btn-default">Cancel</button>
	</div>
 </div>
</form>
