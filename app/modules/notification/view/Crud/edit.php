<?php 
$translator = Core\ServiceLayer::getService('translator');
?>

<?php if($request->hasFlashMessage('user_edit_success')): ?>
<div class="alert alert-success">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<?php echo $request->getFlashMessage('user_edit_success') ?>
</div>
<?php endif; ?>

<form action="" class="form-horizontal form-bordered" method="post">
<?php echo $validator->showAllErrors() ?>
			
			<div class="form-group">
              <label class="col-sm-3 control-label"><?php echo $translator->translate('status_name') ?></label>
              <div class="col-sm-6">
                <?php echo $form->getEntity()->getStatusName() ?>
               
              </div>
            </div>
            
            <div class="form-group">
              <label class="col-sm-3 control-label"><?php echo $translator->translate('notification_type') ?></label>
              <div class="col-sm-6">
                <?php echo $form->getEntity()->getNotificationType() ?>
               
              </div>
            </div>

			<div class="form-group">
              <label class="col-sm-3 control-label"><?php echo $translator->translate($form->getFieldLabel("subject")) ?><span class="asterisk">*</span></label>
              <div class="col-sm-6">
                <?php echo $form->renderInputTag("subject",array('class' => 'form-control')) ?>
                <?php echo $validator->showError("subject") ?>
              </div>
            </div>
            
			<div class="form-group">
              <label class="col-sm-3 control-label"><?php echo $translator->translate($form->getFieldLabel("message")) ?><span class="asterisk">*</span></label>
              <div class="col-sm-6">
                <?php echo $form->renderTextareaTag("message",array('class' => 'form-control')) ?>
                <?php echo $validator->showError("message") ?>
              </div>
            </div>
			<div class="form-group">
              <label class="col-sm-3 control-label"><?php echo $translator->translate($form->getFieldLabel("is_active")) ?><span class="asterisk">*</span></label>
              <div class="col-sm-6">
                <?php echo $form->renderCheckboxTag("is_active",array('class' => 'form-control')) ?>
                <?php echo $validator->showError("is_active") ?>
              </div>
            </div>

 <div class="row">
	<div class="col-sm-6 col-sm-offset-3">
	  <button class="btn btn-primary"><?php echo $translator->translate('BTN_SAVE')?></button>&nbsp;
	  <a href="<?php echo $router->link('notification_list')?>" class="btn btn-default"><?php echo $translator->translate('BTN_BACK')?></button>
	</div>
 </div>

</form>

<?php  $layout->startSlot('footer_js') ?>

<script src="/js/ckeditor/ckeditor.js"></script>
<script src="/js/ckeditor/adapters/jquery.js"></script>
<script type="text/javascript">
jQuery('#record_message').ckeditor();
</script>

<?php  $layout->endSlot('footer_js') ?>