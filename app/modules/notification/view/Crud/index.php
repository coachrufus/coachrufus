<?php 
$router = Core\ServiceLayer::getService('router');
?>

<?php if($request->hasFlashMessage('notification_edit_success')): ?>
<div class="alert alert-success">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<?php echo $request->getFlashMessage('notification_edit_success') ?>
</div>
<?php endif; ?>

<div class="panel">
<div class="panel-body">
<table class="table" id="datatable">
<!-- SORTOVANIE -->
 <thead>
<tr class="sort-row">
    <?php foreach ($admin_table->getTableRows() as $row_index => $row_data): ?>
        <th>
             <?php $sort_criteria = $admin_table->getSortCriteria(); ?>
             <a class="sort-up <?php echo (array_key_exists($row_index, $sort_criteria) && $sort_criteria[$row_index] == $row_index.' asc' ) ? 'active' : '' ?>" href="<?php echo $_SERVER['REQUEST_URI'] ?>&amp;datagridsort=up&amp;sortcolumn=<?php echo $row_index ?>"></a>
             <span class="sort_label"><?php echo $admin_table->getColumnLabel($row_index)?></span>
             <a class="sort-down <?php echo (array_key_exists($row_index, $sort_criteria) && $sort_criteria[$row_index] == $row_index.' desc' ) ? 'active' : '' ?>" href="<?php echo $_SERVER['REQUEST_URI'] ?>&amp;datagridsort=down&amp;sortcolumn=<?php echo $row_index ?>"></a> 
        </th>
    <?php endforeach ?>
    <th></th>
</tr>
 </thead>

<!-- UDAJE -->
 <tbody>
<?php $i = 0; foreach ($admin_table->getRecords() as $data): ?>
    <tr>
        
	<td><?php echo $data["notification_type"]?></td>
	<td><?php echo $data["subject"]?></td>
	<td><?php echo $data["demand_status_name"]?></td>
	<td><?php echo $data["is_active"]?></td>
        
        <td class="table-action">
			<a href="<?php echo $router->link('notification_edit',array('id' => $data['ID'])) ?>"><?php echo $translator->translate('BTN_EDIT')?></a>
		</td>
    </tr>
<?php endforeach; ?>
</tbody>
</table>


</div>
</div>