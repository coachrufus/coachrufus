<?php if ($request->hasFlashMessage('send')): ?>
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <?php echo $request->getFlashMessage('send') ?>
    </div>
<?php endif; ?>

<table class="table table-striped">
    <tbody>
<?php foreach($links as $action => $link): ?>
<tr>
    <td>
        <a href="<?php echo $link ?>"><?php echo $link ?></a> 
        <a href="<?php echo $link ?>?lang=en">EN</a>
    </td>
    <td>
        <form action="" method="post">
            <input type="hidden" name="source_url" value="<?php echo $action ?>" />
            <input type="text" name="email" />
            <button type="submit">Odoslať</button>
        </form>
        
    </td>
<tr>
<?php endforeach; ?>
    </tbody>
</table>