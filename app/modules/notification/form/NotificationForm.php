<?php
namespace  PH\Notification\Form;
use \Core\Form as Form;
use  PH\Notification\Model\Notification as Notification;

class NotificationForm extends Form 
{
	protected $fields = array (
  'id' => 
  array (
    'type' => 'int',
    'max_length' => '11',
    'required' => false,
  ),
  'demand_status_id' => 
  array (
    'type' => 'int',
    'max_length' => '11',
    'required' => false,
  ),
  'notification_type' => 
  array (
    'type' => 'string',
    'max_length' => '128',
    'required' => false,
  ),
  'subject' => 
  array (
    'type' => 'string',
    'max_length' => '150',
    'required' => true,
  ),
  'message' => 
  array (
    'type' => 'string',
    'max_length' => '',
    'required' => true,
  ),
  'is_active' => 
  array (
    'type' => 'boolean',
    'max_length' => '',
    'required' => false,
  ),
);
				
	public function __construct() 
	{

	}
}