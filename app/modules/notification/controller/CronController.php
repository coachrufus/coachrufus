<?php

namespace Notification\Controller;

use Core\Controller as Controller;
use Core\Request as Request;
use Core\ServiceLayer;
use Core\DatagridTable as DatagridTable;
use Notification\Model\NotificationDatagrid as NotificationDatagrid;
use Notification\Model\Notification as Notification;
use Notification\Model\NotificationRepository as NotificationRepository;
use Notification\Form\NotificationForm as NotificationForm;
use Core\Validator as Validator;
use Core\RestController;
class CronController extends RestController
{
    
    public function sendQueueEmailsAction()
    {
        $queueRepo  = ServiceLayer::getService('QueueRepository');
        $records = $queueRepo->getWaitingRecords('email_resend');
        $notificationManager = ServiceLayer::getService('notification_manager');
        
        
        $mailer = ServiceLayer::getService('mailer');
        
    
        foreach($records as $record)
        {
            $messageData = json_decode($record['data'],true);
            
            $message = $mailer->createMessage();
            $message->setSubject($messageData['subject']);
            $message->setTo($messageData['to']);
            $message->setBody($messageData['body'], 'text/html');

            $fromName = DEFAULT_MAIL_SENDER['name'];
            $fromEmail = DEFAULT_MAIL_SENDER['email'];
            $message->setFrom(array($fromEmail => $fromName));
            
            $result = $mailer->rawSend($message);
            $queueRepo->finishRecord($record['id']);


            if($result == 1)
            {
                
            }
            else
            {
                 //number o attempts
                $valueData = explode('###',$record['value']);
                $attempt =$valueData[1]+1;
                
                 $recipient = $message->getTo();
                if(is_array($recipient))
                {
                    $recipient = implode(',',array_keys($recipient));
                }
                    
                if($attempt > 3)
                {
                    $message = $mailer->createMessage();
                    $message->setSubject('Nepodarilo sa odoslat email');
                    $message->setTo(array(DEV_EMAIL));
                    $message->setBody('nepodarilo sa odoslat email na adresu '.$recipient, 'text/html');
                    $mailer->rawSend($message);
                }
                else
                {
                    $queueRepo->addRecord(array(
                        'action_key' => 'email_resend_error',
                        'value' =>  $recipient.'###'.$attempt.'###error:'.$result,
                        'action_type' => 'email_resend',
                        'data' => json_encode(array('subject' => $message->getSubject(),'body' =>$message->getBody(),'to' => $message->getTo() ),JSON_UNESCAPED_UNICODE)
                        ));
                }
            }
        }
        
        return $this->asJson(['result' => 'success']);
    }
    
    
    public function emailMarketingEventAction()
    {

        $teamEventManager = ServiceLayer::getService('TeamEventManager');
        
        $day1Events = $teamEventManager->getEmailMarketingEvents(2);
        
        t_dump($day1Events);
        
        
    }   
    
    
    public function resendAccountActivationAction()
    {
        
        \MailLogger::log(["to" => 'resend-account-activation',"subject" => 'resend-account-activation']);
        $notifyManager = ServiceLayer::getService('notification_manager');
        $userRepo = ServiceLayer::getService('user_repository');
        $unactivatedUsers = $userRepo->findUnactivatedUser('-24 hours');
        
        foreach($unactivatedUsers as $unactivatedUser)
        {
            $data = array();
            $data['lang'] = 'sk';
            $data['name'] = $unactivatedUser->getFullName();
            $data['email'] =  $unactivatedUser->getEmail();
            $data['activationGuid'] = $unactivatedUser->getActivationGuid();
            
            $notifyManager->sendActivationEmail($data);
        }
        
        $unactivatedUsers5 = $userRepo->findUnactivatedUser('-5 days');
        foreach($unactivatedUsers5 as $unactivatedUser5)
        {
            $notifyManager->sendLastActivationEmail('sk', $unactivatedUser5);
        }
        
        //remove older accounts
         $unactivatedUsers6 = $userRepo->findUnactivatedUser('-6 days');
        foreach($unactivatedUsers6 as $unactivatedUser6)
        {
            $userRepo->delete($unactivatedUser6->getId());
        }
        
        return $this->asJson(['result' => 'success']);
        
    }
    
    
    public function sendNotificationsAction()
    {
        $teamEventManager = ServiceLayer::getService('TeamEventManager');
        $notifyManager = ServiceLayer::getService('notification_manager');
        $teamManager = ServiceLayer::getService('TeamManager');
        $beepEvents = $teamEventManager->getBeepEvents();

        foreach ($beepEvents as $eventDate => $dateEvents)
        {
            foreach ($dateEvents as $event)
            {
                $beepDate = date('Y-m-d', strtotime('+' . $event->getNotifyBeepDays() . ' day'));
                $team = $teamManager->findTeamById($event->getTeamId());
                if ($beepDate == $event->getCurrentDate()->format('Y-m-d'))
                {
                    $attendance = $teamEventManager->getEventAttendance($event);
                    foreach ($attendance as $attend)
                    {
                        $notifyManager->sendTeamEventReminder($team, $event, $attend->getPlayer()->getEmail());
                    }
                }
            }
        }


        $todayEvents = $teamEventManager->getDateNotifyEvents();
        foreach ($todayEvents as $event)
        {

            $terminDate = new \DateTime();
            $terminFind = false;
            $i = 0;
            //find nearest termin in 2 months
            while ($terminFind == false and $i < 60)
            {

                $terminDate = new \DateTime(date('Y-m-d', strtotime("+" . $i . " day")));
                $nearestDate = $event->getNearestTermin($terminDate);
                if (null != $nearestDate)
                {
                    $terminFind = true;
                    $event->setCurrentDate($nearestDate);
                }
                $i++;
            }

            $attendance = $teamEventManager->getEventAttendance($event);
            $team = $teamManager->findTeamById($event->getTeamId());
            foreach ($attendance as $attend)
            {
                $notifyManager->sendTeamEventReminder($team, $event, $attend->getPlayer()->getEmail());
            }
        }


        exit;
    }
}
