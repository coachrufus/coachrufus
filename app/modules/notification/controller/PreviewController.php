<?php

namespace Notification\Controller;

use Core\Notifications\ActivationAccountNotificationSender;
use Core\Notifications\ActivationAccountStore;
use Core\Notifications\AddRateNotificationSender;
use Core\Notifications\AddStatisticsNotificationSender;
use Core\Notifications\AdminWithoutPlayersNotificationSender;
use Core\Notifications\AdminWithoutPlayersStore;
use Core\Notifications\BaseRegisterNotificationSender;
use Core\Notifications\BaseRegisterStore;
use Core\Notifications\EventSummaryNotificationSender;
use Core\Notifications\EventSummaryStore;
use Core\Notifications\FloorballCouponNotificationSender;
use Core\Notifications\FloorballCouponStore;
use Core\Notifications\LastActivationAccountNotificationSender;
use Core\Notifications\LastActivationAccountStore;
use Core\Notifications\MatchResultsNotificationSender;
use Core\Notifications\NotificationStore;
use Core\Notifications\NotificationTemplate;
use Core\Notifications\TeamInvitationDeclineNotificationSender;
use Core\Notifications\TeamInvitationNotificationSender;
use Core\Notifications\TeamInvitationStore;
use Core\Notifications\TeamInvitationUnregistredNotificationSender;
use Core\Notifications\TeamInvitationUnregistredStore;
use Core\Notifications\UserWithoutTeamNotificationSender;
use Core\Notifications\UserWithoutTeamStore;
use Core\Notifications\UserWithTeamNotificationSender;
use Core\Notifications\UserWithTeamStore;
use Core\Notifications\VerifyEmailNotificationSender;
use Core\Notifications\VerifyEmailStore;
use Core\Notifications\TeamJoinRequestRefusedNotificationSender;
use Core\Notifications\TeamJoinRequestRefusedStore;
use Core\Notifications\TeamJoinRequestAcceptedNotificationSender;
use Core\Notifications\TeamJoinRequestAcceptedStore;
use Core\RestController;
use Core\ServiceLayer;
use Core\Templates\Regions;
use Core\Types\DateTimeEx;
use DateTime;
use Exception;
use stdClass;
use User\Model\User;
use Webteamer\Event\Model\Event;
use Webteamer\Team\Model\Team;
use Webteamer\Team\Model\TeamPlayer;
use const DEV_EMAIL;
use const PUBLIC_DIR;
use const SENDER_MAIL;
use const WEB_DOMAIN;

class PreviewController extends RestController {

    public function indexAction()
    {
        $links = array(
            'verifyEmailAction' => '/preview/verify-email', 
            'registrationEmailAction' => '/preview/registration-email', 
            'activationEmailAction' => '/preview/activation-email', 
            'lastActivationEmailAction' => '/preview/last-activation-email', 
            'userWithoutTeamEmailAction' => '/preview/user-without-team', 
            'userWithTeamEmailAction' => '/preview/user-with-team', 
            'adminWithoutPlayersEmailAction' => '/preview/admin-without-players', 
            'teamInvitationRegistredEmailAction' => '/preview/team-invitation-registred', 
            'TeamJoinRequestRefusedEmailAction' => '/preview/team-join-request-refused', 
            'TeamJoinRequestAcceptedEmailAction' => '/preview/team-join-request-accepted', 
            'teamInvitationDeclineEmailAction' => '/preview/team-invitation-decline', 
            'teamInvitationUnregistredEmailAction' => '/preview/team-invitation-unregistred',
            'eventInvitationSingleEmailAction' => '/preview/event-invitation-single', 
            'eventInvitationEmailAction' =>  '/preview/event-invitation', 
            'eventSummaryEmailAdminAction' =>  '/preview/event-summary-admin', 
            'eventSummaryEmailAction' => '/preview/event-summary', 
            'eventAddStatEmailAction' => '/preview/event-add-stat', 
            'matchResultEmailAction' => '/preview/match-result', 
            'floorballEmailAction' => '/preview/floorball-email', 
            'shareLinkInvitationAction' => '/preview/share-link-invitation', 
            'deleteTeamAction' => '/preview/delete-team', 
            'leaveTeamEmailAction' => '/preview/leave-team', 
            'paymentSuccessAction' => '/preview/payment-success', 
            'paymentYearNotifyAction' => '/preview/payment-year-notify', 
            'tournamentJoinAdminAction' => '/preview/tournament-join-admin', 
            'tournamentChangedStatusConfirmedAction' => '/preview/tournament-changed-status-confirmed', 
            'tournamentChangedStatusRejectedAction' => '/preview/tournament-changed-status-rejected', 
        );
        
       
        
        $request = ServiceLayer::getService('request');
        $router = ServiceLayer::getService('router');
        $notifyManager = ServiceLayer::getService('notification_manager');
        if('POST' == $request->getMethod())
        {
            $mailer = ServiceLayer::getService('mailer');
            $message = $mailer->createMessage();
            try
            {
                $response = call_user_func(array($this,$request->get('source_url')));
                $messageBody = $response->getContent();

                $messageBody = $notifyManager->embedMessageImages($messageBody, $message, PUBLIC_DIR);
                $subject = 'Rufus';
                $message->setSubject($subject);
                $message->setFrom(SENDER_MAIL);
                $message->setTo($request->get('email'));
                $message->setBody($messageBody, 'text/html');
                $mailer->sendMessage($message);
                $request->addFlashMessage('send', 'Send');
                $request->redirect($router->link('preview_index'));
                
            }
            catch(Exception $e)
            {
               var_dump($e);
            }
        }


        return $this->render('Notification\NotificationModule:Test:index.php',
                        array(
                                        'links' => $links
                        )); 
        
    }
    
    public function tournamentChangedStatusConfirmedAction()
    {
        $data = new \stdClass();
        $data->default_lang =  (null != $this->getRequest()->get('lang')) ? $this->getRequest()->get('lang') : 'sk';
        $data->tournament_name = 'Turnaj XY';
        $data->team_name = 'Team XY';
        $data->user_name = 'Fero hruska';
        $data->status = 'confirmed';
        $sender = new \Core\Notifications\TournamentChangedStatusConfirmedNotificationSender(new \Core\Notifications\TournamentChangedStatusConfirmedStore());
        $mailData = $sender->getMessageRegions($data);
        return $this->asHtml($mailData['body']);
    }
    
    public function tournamentChangedStatusRejectedAction()
    {
        $data = new \stdClass();
        $data->default_lang =  (null != $this->getRequest()->get('lang')) ? $this->getRequest()->get('lang') : 'sk';
        $data->tournament_name = 'Turnaj XY';
        $data->team_name = 'Team XY';
        $data->user_name = 'Fero hruska';
        $data->status = 'confirmed';
        $sender = new \Core\Notifications\TournamentChangedStatusRejectedNotificationSender(new \Core\Notifications\TournamentChangedStatusRejectedStore());
        $mailData = $sender->getMessageRegions($data);
        return $this->asHtml($mailData['body']);
    }
    

    public function tournamentJoinAdminAction()
    {
        $data = new \stdClass();
        $data->default_lang =  (null != $this->getRequest()->get('lang')) ? $this->getRequest()->get('lang') : 'sk';
        $data->tournament_name = 'Turnaj XY';
        $data->team_name = 'Team XY';
        $data->user_name = 'Admin turnaja';
        $sender = new \Core\Notifications\TournamentJoinAdminNotificationSender(new \Core\Notifications\TournamentJoinAdminStore());
        $mailData = $sender->getMessageRegions($data);
        return $this->asHtml($mailData['body']);
    }
    
    /**
     * verify_email_sk
     */
    public function verifyEmailAction()
    {

        $data = new \stdClass();
        $data->default_lang =  (null != $this->getRequest()->get('lang')) ? $this->getRequest()->get('lang') : 'sk';

        /*
        $data['email'] = DEV_EMAIL;
        $data['activationGuid'] = '00000';
        */
        $sender = new VerifyEmailNotificationSender(new VerifyEmailStore());
        $mailData = $sender->getMessageRegions($data);
        return $this->asHtml($mailData['body']);
    }

    /**
     * registration_email_sk
     */
    public function registrationEmailAction()
    {

        $data = new \stdClass();
        $data->default_lang =  (null != $this->getRequest()->get('lang')) ? $this->getRequest()->get('lang') : 'sk';

        $sender = new BaseRegisterNotificationSender(new BaseRegisterStore());
        $data = $sender->getMessageRegions($data);
        return $this->asHtml($data['body']);
    }

    /**
     * activation_email_sk
     */
    public function activationEmailAction()
    {
        $data = new \stdClass();
        $data->default_lang =  (null != $this->getRequest()->get('lang')) ? $this->getRequest()->get('lang') : 'sk';

        $sender = new ActivationAccountNotificationSender(new ActivationAccountStore());
        $data = $sender->getMessageRegions($data);
        return $this->asHtml($data['body']);
    }
    
    /**
     * floorball_challenge_activation_email
     */
    public function floorballEmailAction()
    {
        $data = new \stdClass();
        $data->default_lang =  (null != $this->getRequest()->get('lang')) ? $this->getRequest()->get('lang') : 'sk';
        $data->activationGuid = '000000';
        //$data->hash = 'getShareLinkHash';
        $sender = new FloorballCouponNotificationSender(new FloorballCouponStore());
        $data = $sender->getMessageRegions($data);
        return $this->asHtml($data['body']);
    }
    
    /**
     * shareLinkInvitationAction
     */
     public function shareLinkInvitationAction()
    {
        $data = new \stdClass();
        $data->default_lang =  (null != $this->getRequest()->get('lang')) ? $this->getRequest()->get('lang') : 'sk';
        $data->activationGuid = '000000';
        $data->sender_name = 'Janko hraško';
        $data->team_name = 'TESTERI';
        $data->management_link = $this->getRouter()->link('team_players_list',array('team_id' => 0000 ));
        //$data->hash = 'getShareLinkHash';
        $sender = new \Core\Notifications\ShareLinkInvitationAdminNotificationSender(new \Core\Notifications\ShareLinkInvitationAdminStore());
        $data = $sender->getMessageRegions($data);
        return $this->asHtml($data['body']);
    }
    
    /**
     * payment success
     */
    public function paymentSuccessAction()
    {
        $data = new \stdClass();
        $data->default_lang =  (null != $this->getRequest()->get('lang')) ? $this->getRequest()->get('lang') : 'sk';
        $data->activationGuid = '000000';
        $data->team_name = 'TESTERI';
        $data->scouting = '/scoutig';
        //$data->hash = 'getShareLinkHash';
        $sender = new \Core\Notifications\PaymentSuccessNotificationSender(new \Core\Notifications\PaymentSuccessStore());
        $data = $sender->getMessageRegions($data);
        
      

        return $this->asHtml($data['body']);
    }
    
    public function paymentYearNotifyAction()
    {
        $data = new \stdClass();
        $data->default_lang =  (null != $this->getRequest()->get('lang')) ? $this->getRequest()->get('lang') : 'sk';
        $data->activationGuid = '000000';

        $sender = new \Core\Notifications\PaymentYearNotifyNotificationSender(new \Core\Notifications\PaymentYearNotifyStore());
        $data = $sender->getMessageRegions($data);
        
       

        return $this->asHtml($data['body']);
    }
    
    
    
    /**
     * preview_delete_team
     */
     public function deleteTeamEmailAction()
    {
        $data = new \stdClass();
        $data->default_lang =  (null != $this->getRequest()->get('lang')) ? $this->getRequest()->get('lang') : 'sk';
        $data->activationGuid = '000000';
        $data->team_name = 'TESTERI';
        $data->scouting = '/scoutig';
        //$data->hash = 'getShareLinkHash';
        $sender = new \Core\Notifications\TeamDeleteNotificationSender(new \Core\Notifications\TeamDeleteStore());
        $data = $sender->getMessageRegions($data);
        return $this->asHtml($data['body']);
    }
    
    /**
     * preview_leave_team
     */
     public function leaveTeamEmailAction()
    {

         $data = new \stdClass();
        $data->default_lang =  (null != $this->getRequest()->get('lang')) ? $this->getRequest()->get('lang') : 'sk';
        $data->activationGuid = '000000';
        $data->player_name = 'Janko hgrasko';
        $data->team_name = 'Vrany';
        $data->scouting = '/scoutig';
        //$data->hash = 'getShareLinkHash';
        $sender = new \Core\Notifications\TeamLeaveNotificationSender(new \Core\Notifications\TeamLeaveStore());
        $data = $sender->getMessageRegions($data);
        return $this->asHtml($data['body']);
    }
    
    /**
     * last_activation_email_sk
     */
     public function lastActivationEmailAction()
    {
       $data = new \stdClass();
        $data->default_lang =  (null != $this->getRequest()->get('lang')) ? $this->getRequest()->get('lang') : 'sk';
        
        $sender = new LastActivationAccountNotificationSender(new LastActivationAccountStore());
        $data = $sender->getMessageRegions($data);
        return $this->asHtml($data['body']);
         
    }
    
    /**
     *  test_user_without_team
     */
     public function userWithoutTeamEmailAction()
    {
        $data = new \stdClass();
        $data->default_lang =  (null != $this->getRequest()->get('lang')) ? $this->getRequest()->get('lang') : 'sk';
        
        $sender = new UserWithoutTeamNotificationSender(new UserWithoutTeamStore());
        $data = $sender->getMessageRegions($data);
        return $this->asHtml($data['body']);
    }
    
    /**
     * test_user_with_team
     */
    public function userWithTeamEmailAction()
    {
        $data = new stdClass();
        $data->email = DEV_EMAIL;
        $data->default_lang = 'sk';
        $sender = new UserWithTeamNotificationSender(new UserWithTeamStore());
        $data = $sender->getMessageRegions($data);
        return $this->asHtml($data['body']);
       
    }
    
    /**
     * test_admin_without_players
     */
    public function adminWithoutPlayersEmailAction()
    {
         
        $data = new \stdClass();
        $data->default_lang =  (null != $this->getRequest()->get('lang')) ? $this->getRequest()->get('lang') : 'sk';
        
        $sender = new AdminWithoutPlayersNotificationSender(new AdminWithoutPlayersStore());
        $data = $sender->getMessageRegions($data);
        return $this->asHtml($data['body']);
    }
    
    
    /**
     * TeamInvitation_sk
     */
     public function teamInvitationRegistredEmailAction()
    {

        $user = new User();
        $user->setName('Test');
        
        $team = new Team();
        $team->setName('TEST TEAM');
        
        $teamPlayer = new TeamPlayer();
        $teamPlayer->setCreatedAt(new DateTime());
        $teamPlayer->setEmail(DEV_EMAIL);
        $teamPlayer->setPlayerId(1);
     
        $sender = new TeamInvitationNotificationSender(new TeamInvitationStore());
        
        $notifyData = new stdClass();
        $notifyData->email = $teamPlayer->getEmail();
        $notifyData->default_lang =   (null != $this->getRequest()->get('lang')) ? $this->getRequest()->get('lang') : 'sk';
        $notifyData->sender_name = $user->getFullName();
        $notifyData->team_name = $team->getName();
        $notifyData->invite_link = WEB_DOMAIN . $this->getRouter()->link('team_member_invite', array('hash' => $team->getShareLinkHash(),'tp' => $teamPlayer->getId(),'h' => $teamPlayer->getHash()));
        $message = str_replace('{INVITE_LINK}', WEB_DOMAIN . $this->getRouter()->link('team_member_invite', array('hash' => $team->getShareLinkHash())), $message);
        $notifyData->accept_link = WEB_DOMAIN . $this->getRouter()->link('team_member_invite',array('tp' => $teamPlayer->getId() ,'hash' => $team->getShareLinkHash(),'h' => $teamPlayer->getHash() ));
        $notifyData->decline_link = WEB_DOMAIN . $this->getRouter()->link('team_member_invite_decline', array('tp' => $teamPlayer->getId() ,'hash' => $team->getShareLinkHash(),'h' => $teamPlayer->getHash() ));
        $data = $sender->getMessageRegions($notifyData);
        return $this->asHtml($data['body']);
    }
    
    
    /**
     * TeamInvitationDecline_sk
     */
    public function teamInvitationDeclineEmailAction()
    {
        $sender = new TeamInvitationDeclineNotificationSender(new TeamInvitationStore());
        $notifyData = new stdClass();
        $notifyData->email = DEV_EMAIL;
        $notifyData->default_lang =  (null != $this->getRequest()->get('lang')) ? $this->getRequest()->get('lang') : 'sk';
        $notifyData->team_player_name = 'PLAYER NAME';
        $notifyData->team_name = 'TEAM NAME';
        $notifyData->sender_name = 'ADMIN NAME';
        
        $data = $sender->getMessageRegions($notifyData);
        return $this->asHtml($data['body']);
        
    }
    
    /**
     * notification which is send to the user afted team admine refuse his request
     */
    public function teamJoinRequestRefusedEmailAction()
    {
        $sender = new TeamJoinRequestRefusedNotificationSender(new TeamJoinRequestRefusedStore());
        $notifyData = new stdClass();
        $notifyData->email = DEV_EMAIL;
        $notifyData->default_lang =  (null != $this->getRequest()->get('lang')) ? $this->getRequest()->get('lang') : 'sk';
        $notifyData->team_player_name = 'PLAYER NAME';
        $notifyData->team_name = 'TEAM NAME';
        $notifyData->sender_name = 'ADMIN NAME';
        
        $data = $sender->getMessageRegions($notifyData);
        return $this->asHtml($data['body']);
        
    }
    
    /**
     * notification which is send to the user afted team admine confirm his request
     */
    public function teamJoinRequestAcceptedEmailAction()
    {
        $sender = new TeamJoinRequestAcceptedNotificationSender(new TeamJoinRequestAcceptedStore());
        $notifyData = new stdClass();
        $notifyData->email = DEV_EMAIL;
        $notifyData->default_lang =  (null != $this->getRequest()->get('lang')) ? $this->getRequest()->get('lang') : 'sk';
        $notifyData->team_player_name = 'PLAYER NAME';
        $notifyData->team_name = 'TEAM NAME';
        $notifyData->sender_name = 'ADMIN NAME';
        
        $data = $sender->getMessageRegions($notifyData);
        return $this->asHtml($data['body']);
        
    }
    
     /**
     * TeamInvitationUnregistred_sk
     */
     public function teamInvitationUnregistredEmailAction()
    {

        $user = new User();
        $user->setName('Test');
        
        $team = new Team();
        $team->setName('TEST TEAM');
        
        $teamPlayer = new TeamPlayer();
        $teamPlayer->setCreatedAt(new DateTime());
        $teamPlayer->setEmail(DEV_EMAIL);
        $teamPlayer->setPlayerId(0);
     
        $sender = new TeamInvitationUnregistredNotificationSender(new TeamInvitationUnregistredStore());
        
        $notifyData = new stdClass();
        $notifyData->email = $teamPlayer->getEmail();
        $notifyData->default_lang =    (null != $this->getRequest()->get('lang')) ? $this->getRequest()->get('lang') : 'sk';
        $notifyData->sender_name = $user->getFullName();
        $notifyData->team_name = $team->getName();
        $notifyData->invite_link = WEB_DOMAIN . $this->getRouter()->link('team_member_invite', array('hash' => $team->getShareLinkHash(),'tp' => $teamPlayer->getId(),'h' => $teamPlayer->getHash()));
        $message = str_replace('{INVITE_LINK}', WEB_DOMAIN . $this->getRouter()->link('team_member_invite', array('hash' => $team->getShareLinkHash())), $message);
        $notifyData->accept_link = WEB_DOMAIN . $this->getRouter()->link('team_member_invite',array('tp' => $teamPlayer->getId() ,'hash' => $team->getShareLinkHash(),'h' => $teamPlayer->getHash() ));
        $notifyData->decline_link = WEB_DOMAIN . $this->getRouter()->link('team_member_invite_decline', array('tp' => $teamPlayer->getId() ,'hash' => $team->getShareLinkHash(),'h' => $teamPlayer->getHash() ));
        $data = $sender->getMessageRegions($notifyData);
        return $this->asHtml($data['body']);
    }
    
    
    /**
     * EventInvitation_sk
     */
     public function eventInvitationEmailAction()
    {
       $teamManager = ServiceLayer::getService('TeamManager');
        

        $teamEventManager = ServiceLayer::getService('TeamEventManager');
        $event = $teamEventManager->findEventById(1046); 
        $event->setCurrentDate(new \DateTime('2018-08-14'));
        
        $attendance = $teamEventManager->getEventAttendance($event);
        $event->setAttendance($attendance);
        $eventsList = array('2018-08-14' => array('1046' => $event));
        
        $store = new \Core\Notifications\EventInvitationStore();
        $store->setEvents($eventsList);
        $store->setForceEventInfo(true);
        $sender = new \Core\Notifications\EventInvitationNotificationSender($store);
        
        $data = $store->getData();
        $dataArray = iterator_to_array($data);


        $sendData = $dataArray[0];
        $sendData->isAdmin = false;
       // $sendData->lineupSummary = $sender->getSummary(1046, '2018-08-14', $sendData->isAdmin);
         $sendData->default_lang  = (null != $this->getRequest()->get('lang')) ? $this->getRequest()->get('lang') : 'sk';
         
         
        $data = $sender->getMessageRegions($sendData);
        
      
        return   $this->asHtml($data['body']);
       
    }
    
      /**
     * EventInvitationSingle_sk
     */
     public function eventInvitationSingleEmailAction()
    {
        
        
        $event = new Event();
        $event->setName('Test event');
        $event->setTeamId(2);
        $event->setPlaygroundData('a:12:{s:2:"id";s:2:"12";s:4:"name";s:44:"Vysoká 2950/21, 811 06 Bratislava, Slovakia";s:10:"created_at";s:19:"2016-04-27 23:39:33";s:9:"author_id";s:1:"4";s:11:"locality_id";s:2:"15";s:4:"city";s:10:"Bratislava";s:6:"street";s:7:"Vysoká";s:13:"street_number";s:0:"";s:11:"description";N;s:9:"area_type";N;s:3:"lat";s:7:"48.1491";s:3:"lng";s:7:"17.1112";}');
        $lang = (null != $this->getRequest()->get('lang')) ? $this->getRequest()->get('lang') : 'sk';
        $event->setCurrentDate( new   DateTime());
        
        
        $notifyData = new stdClass();
        $notifyData->email = DEV_EMAIL;
        $notifyData->default_lang =    (null != $this->getRequest()->get('lang')) ? $this->getRequest()->get('lang') : 'sk';
        $notifyData->eventInGuid = '8bd92cb5-3153-4994-9666-f3a957d84b07';
        $notifyData->eventOutGuid = 'cad6277c-bcaf-430b-9116-c8179a8548b7';
        $notifyData->event = $event;
        $notifyData->start = new   DateTimeEx();

        $sender = new \Core\Notifications\EventInvitationSingleNotificationSender(new \Core\Notifications\EventInvitationSingleStore());
        $data = $sender->getMessageRegions($notifyData);
        return $this->asHtml($data['body']);
        
      
    }
    
    /**
     * EventSummary_admin_sk
     */
     public function eventSummaryEmailAdminAction()
    {
        $teamManager = ServiceLayer::getService('TeamManager');
        $eventId = 1046;
        $eventDate = '2017-07-13';

        $teamEventManager = ServiceLayer::getService('TeamEventManager');
        $event = $teamEventManager->findEventById($eventId); 
        $event->setCurrentDate(new \DateTime($eventDate));
         
        $attendance = $teamEventManager->getEventAttendance($event);
        $event->setAttendance($attendance);
        
        $eventsList = array($eventDate => array($eventId => $event));
        
        $store = new EventSummaryStore();
        $store->setEvents($eventsList);
        $store->setForceEventInfo(true);
        $sender = new EventSummaryNotificationSender($store);
        $data = $store->getData();

        $sendData = $data[0];
        $sendData->isAdmin = true;
        $sendData->default_lang  = (null != $this->getRequest()->get('lang')) ? $this->getRequest()->get('lang') : 'sk';
        $sendData->lineupSummary = $sender->getSummary($eventId, $eventDate, true);

        $data = $sender->getMessageRegions($sendData);
        return $this->asHtml($data['body']);
    }
    
    
    /**
     * EventSummary_sk
     */
     public function eventSummaryEmailAction()
    {
      
       $teamManager = ServiceLayer::getService('TeamManager');
        

        $teamEventManager = ServiceLayer::getService('TeamEventManager');
        $event = $teamEventManager->findEventById(1046); 
        $event->setCurrentDate(new \DateTime('2017-07-13'));
        
        $attendance = $teamEventManager->getEventAttendance($event);
        $event->setAttendance($attendance);
        $eventsList = array('2017-07-13' => array('1046' => $event));
        
        $store = new EventSummaryStore();
        $store->setEvents($eventsList);
        $store->setForceEventInfo(true);
        $sender = new EventSummaryNotificationSender($store);
        $data = $store->getData();

        $sendData = $data[0];
        $sendData->isAdmin = false;
        $sendData->lineupSummary = $sender->getSummary(1046, '2017-07-13', $sendData->isAdmin);
         $sendData->default_lang  = (null != $this->getRequest()->get('lang')) ? $this->getRequest()->get('lang') : 'sk';
        $data = $sender->getMessageRegions($sendData);
        return $this->asHtml($data['body']);
        
    }
    
    /**
     * MatchResults_sk
     */
     public function matchResultEmailAction()
    {
        $data = new \stdClass();
        $data->email = DEV_EMAIL;
        $data->default_lang = 'sk';
        $data->eventDetailGuid = '0000';
        
        $event = new Event();
        $event->setname('TEST');
        $data->event = $event;
        $sender = new MatchResultsNotificationSender(new TestStore());
        
        $player =  new TeamPlayer();
        $player->setEmail(DEV_EMAIL);
        $player->setTeamRole('ADMIN');
        $data->player = $player;
         $data->default_lang  = (null != $this->getRequest()->get('lang')) ? $this->getRequest()->get('lang') : 'sk';
        $data->start = new DateTimeEx();
       $data = $sender->getMessageRegions($data);
        return $this->asHtml($data['body']);
    }
    
    /**
     * AddRate_sk
     */
    public function eventAddRateEmailAction()
    {
        $data = new \stdClass();
        $data->email = DEV_EMAIL;
        $data->default_lang = 'sk';
        $data->eventDetailGuid = '0000';
        
        $event = new Event();
        $event->setname('TEST');
        $data->event = $event;
        $sender = new AddRateNotificationSender(new TestStore());
        
        $data->subject = new playerEventInfoSubjectMock();
        $data->start = new DateTimeEx();
       $data = $sender->getMessageRegions($data);
        return $this->asHtml($data['body']);
    }
   
     public function eventAddStatEmailAction()
    {
        $data = new \stdClass();
        $data->email = DEV_EMAIL;
        $data->default_lang = 'sk';
        $data->eventDetailGuid = '0000';
        
        $event = new Event();
        $event->setname('TEST');
        $data->event = $event;
        $sender = new AddStatisticsNotificationSender(new TestStore());
        
        $data->subject = new playerEventInfoSubjectMock();
        $data->start = new DateTimeEx();
         $data->default_lang  = (null != $this->getRequest()->get('lang')) ? $this->getRequest()->get('lang') : 'sk';
        $data = $sender->getMessageRegions($data);
        return $this->asHtml($data['body']);
    }
    
    
    
    

}

/**
 * MOCK CLASSES
 */
class playerEventInfoSubjectMock {
    /*
      public function __construct() {
      $this->isAdmin = false;
      }
     */

    public function getTeamPlayer()
    {
        $player = new TeamPlayer();
        $player->setEmail(DEV_EMAIL);
        return $player;
    }

}

class TestStore extends NotificationStore {

    protected function dataSource()
    {
        //return array('test' => 'blawwwwwwwwwwwww');
    }

}
