<?php namespace Notification\Controller;

use Core\RestController;
use Core\Request;
use Core\ServiceLayer;
use Core\Activity\ActivityTypes;

class NotificationsController extends RestController
{    
    private $request;
    private $permissionManager;
    
    function __construct()
    {
        $this->request = ServiceLayer::getService('request');
        $this->permissionManager = ServiceLayer::getService('PermissionManager');
    }
    
    function sendEveryDayAction()
    {
        \MailLogger::log([ "to" => 'every_day',"subject" => 'every_day']);
        ServiceLayer::getService('NotificationSendQueue')->send("every day");
        return $this->asJson(['result' => 'success']);
    }
    
    function sendEveryMinuteAction()
    {
         
         $teamManager = ServiceLayer::getService('TeamManager');
         $notificationManager = ServiceLayer::getService('SystemNotificationManager');
         
        //send push notify
        $hpWallManager = ServiceLayer::getService('HpWallManager');
        $eventManager = ServiceLayer::getService('TeamEventManager');
        $upcomingEvents = $eventManager->findUpcomingEvents(1);
        $time = strtotime('+4 hours');
        $currentTime = date('H-i',$time);

        foreach($upcomingEvents[date('Y-m-d')] as $event)
        {

            if($currentTime == $event->getCurrentDate()->format('H-i') )
            {
                $team = $teamManager->findTeamById($event->getTeamId());
                $teamPlayers = $teamManager->getTeamPlayers($team->getId());
                foreach($teamPlayers as $teamPlayer)
                {
                    if($teamPlayer->getPlayerId() != null)
                    {
                        $hpWallManager->generatePositionChangePost($teamPlayer,$team);
                        //$notificationManager->sendGeneratedPersonalPostPushNotify($teamPlayer);
                        
                        
                    }
                }
            }
        }
        
        \MailLogger::log([ "to" => 'every_minute',"subject" => 'every_minute']);
        ServiceLayer::getService('NotificationSendQueue')->send("every minute");
        
        return $this->asJson(['result' => 'success']);
    }
    
    function deactivateAction()
    {
        
        $permission = $this->permissionManager->getPermission($this->request->get('guid'));
        if ($permission !== false && $permission->subject === "DEACTIVATE_NOTIFICATIONS")
        {
            $blacklist = ServiceLayer::getService('NotificationBlacklist');
            $blacklist->add($permission->data->email);
            
            $this->request->addFlashMessage('user_edit_success', $this->getTranslator()->translate('user.disable_notify.success'));
            $this->request->redirect($this->getRouter()->link('profil').'#notify');
        }
        else
        {
            $this->request->redirect("/notifications/invalidlink");
        }
    }
    
    function eventDetailAction()
    {
        $permission = $this->permissionManager->getPermission($this->request->get('guid'),false);
        if ($permission !== false && $permission->subject === "EVENT_DETAIL_REDIRECT")
        {
            $data = $permission->data;
            $this->autoLogin($data->userId);
            $this->request->redirect("/event/{$data->eventId}/{$data->start}");
        }
        else
        {
            $this->request->redirect("/notifications/invalidlink");
        }
    }
    
    function eventRateAction()
    {
        $permission = $this->permissionManager->getPermission($this->request->get('guid'),false);
        if ($permission !== false && $permission->subject === "EVENT_RATE_REDIRECT")
        {
            $data = $permission->data;
            $this->autoLogin($data->userId);
            
            
            $link = $this->getRouter()->link('team_event_rating',array('id' =>$data->eventId,'current_date' => $data->start ));
            $this->request->redirect($link);
            //$this->request->redirect("/event/{$data->eventId}/{$data->start}");
        }
        else
        {
            $this->request->redirect("/notifications/invalidlink");
        }
    }
    
    
    function eventLineupAction()
    {
        $permission = $this->permissionManager->getPermission($this->request->get('guid'),false);
        if ($permission !== false && $permission->subject === "EVENT_LINEUP_REDIRECT")
        {
            $data = $permission->data;
            $this->autoLogin($data->userId);
            
            $link = $this->getRouter()->link('team_match_new_lineup',array('event_id' =>$data->eventId,'event_date' => $data->start ));
            $this->request->redirect($link);
            //$this->request->redirect("/event/{$data->eventId}/{$data->start}");
        }
        else
        {
            $this->request->redirect("/notifications/invalidlink");
        }
    }
    
    function eventStatisticsAction()
    {
        $permission = $this->permissionManager->getPermission($this->request->get('guid'),false);
        if ($permission !== false && $permission->subject === "EVENT_STATISTICS_REDIRECT")
        {
            $data = $permission->data;
            $this->autoLogin($data->userId);
            

            $link = $this->getRouter()->link('team_match_create_live_stat',array('event_id' =>$data->eventId,'eventdate' => $data->start));
            $this->request->redirect($link);
            //$this->request->redirect("/event/{$data->eventId}/{$data->start}");
        }
        else
        {
            $this->request->redirect("/notifications/invalidlink");
        }
    }
    
    
    
    
    private function autoLogin(int $userId)
    {
        $security = ServiceLayer::getService('security');
        $security->unauthenticateUser();
        $user = $security->getIdentityProvider()->findUserById($userId);
        $security->authenticateUserEntity($user);
    }
    
    private function changePlayerAttendance($eventId, $eventDate, $teamPlayerId, $type)
    {
        $eventManager = ServiceLayer::getService('TeamEventManager');
        $teamManager = ServiceLayer::getService('TeamManager');
        $event = ServiceLayer::getService('EventRepository')->find($eventId);
        $team = $teamManager->findTeamById($event->getTeamId());
        $teamPlayer = $teamManager->findTeamPlayerById($teamPlayerId);
        $eventManager->deleteEventAttendance($teamPlayer, $event, $eventDate);
        $eventManager->createEventAttendance($teamPlayer, $event, $eventDate, $type == "EVENT_IN" ? 1 : 3);
    }
    
    private function eventInOut($type)
    {
        $permission = $this->permissionManager->getPermission($this->request->get('guid'),false);
        if ($permission !== false && $permission->subject === $type)
        {
            $data = $permission->data;
            $this->autoLogin($data->userId);
            $this->changePlayerAttendance(
                $data->eventId,
                $data->start,
                $data->teamPlayerId,
                $type
            );
            $this->request->redirect("/event/{$data->eventId}/{$data->start}");
        }
        else $this->request->redirect("/notifications/invalidlink");
    }
    
    function eventInAction() { $this->eventInOut('EVENT_IN'); }
    function eventOutAction() { $this->eventInOut('EVENT_OUT'); }
    
    function deactivatedAction()
    {
        $translator = ServiceLayer::getService('translator');
        $translator->setLang($this->request->get('lang'));
        return $this->render();
    }
    
    function invalidlinkAction()
    {
        
         $layout = ServiceLayer::getService('layout');
	 $layout->setTemplate(GLOBAL_DIR.'/templates/ClearLayout.php');
            
        return $this->render('Notification\NotificationModule:Notifications:invalidlink.php',array());
        
    }
}
