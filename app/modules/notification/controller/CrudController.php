<?php
namespace Notification\Controller;
use Core\Controller as Controller;
use Core\Request as Request;
use Core\ServiceLayer;
use Core\DatagridTable as DatagridTable;

use Notification\Model\NotificationDatagrid as NotificationDatagrid;
use Notification\Model\Notification as Notification;
use Notification\Model\NotificationRepository as NotificationRepository;
use Notification\Form\NotificationForm as NotificationForm;
use Core\Validator as Validator;

class CrudController extends Controller {
	
	public function previewAction()
        {
            $request = ServiceLayer::getService('request');
            $repository = ServiceLayer::getService('notification_repository');
            $notify = $repository->findOneBy(array('notification_type'=>$request->get('type'),'lang' => $request->get('l')));
            
            $notifyManager = ServiceLayer::getService($service_name);
            
            $layout = ServiceLayer::getService('layout');
	    $layout->setTemplate(GLOBAL_DIR.'/templates/MailLayout.php');
            
            
		return $this->render('Notification\NotificationModule:Crud:preview.php',
				array(
						'notify' => $notify
				));
        }
    
        public function indexAction()
	{
		$repository = ServiceLayer::getService('notification_repository');
		$all = $repository->findAll();

		$layout = ServiceLayer::getService('layout');
		$layout->setTemplateParameters(array(
				'crumb' => array('nav.notification.list' => 'notification_list'))
		);
		
	
		$admin_table = new NotificationDatagrid();
		
		
		return $this->render('Notification\NotificationModule:Crud:index.php',
				array(
						'admin_table' => $admin_table
				));
	}
	
	public function addAction()
	{
		$request = ServiceLayer::getService('request');
		$repository = ServiceLayer::getService('notification_repository');
		$translator = ServiceLayer::getService('translator');
		$form = new NotificationForm();
		$form->setEntity(new Notification());
		
		$validator = new Validator();
		$validator->setRules($form->getFields());
		
		if('POST' == $request->getMethod())
		{
			$post_data = $request->get('record');
			$form->bindData($post_data);
			$validator->setData($post_data);
			$validator->validateData();
			
			if(!$validator->hasErrors())
			{
				$entity = $form->getEntity();
				$repository->save($entity);
				$request->addFlashMessage('notification_add_success', $translator->translate('notification.crud_add.success'));
				$request->redirect();
			}
		}

		$layout = ServiceLayer::getService('layout');
		$layout->setTemplateParameters(array(
				'crumb' => array('nav.notification.list' => 'notification_list','nav.notification.add' => null))
		);
		
		
		return $this->render('PH\Notification\NotificationModule:Crud:add.php',
				array(
						'form' => $form,
						'request' => $request,
						'validator' => $validator
				));
	}
	
	public function editAction()
	{
		$request = ServiceLayer::getService('request');
		$repository = ServiceLayer::getService('notification_repository');
		$translator = ServiceLayer::getService('translator');

		
		$notification = $repository->find($request->get('id'));

		$form = new NotificationForm();
		$form->setEntity($notification);
		
		$validator = new Validator();
		$validator->setRules($form->getFields());
	
		if('POST' == $request->getMethod())
		{
			$post_data = $request->get('record');
			$form->bindData($post_data);
			
			$validator->setData($post_data);
			$validator->validateData();
			
			if(!$validator->hasErrors())
			{
				$entity = $form->getEntity();
				$repository->save($entity);
				$request->addFlashMessage('notification_edit_success', $translator->translate('NOTE_DATA_UPDATED '));
				$request->redirect(ServiceLayer::getService('router')->link('notification_list'));
			}
		}
	
		$layout = ServiceLayer::getService('layout');
		
		$layout->setTemplateParameters(array(
				'crumb' => array('nav.notification.list' => 'notification_list','nav.notification.edit' => null))
		);
		
	
	
		return $this->render('PH\Notification\NotificationModule:Crud:edit.php',
				array(
						'form' => $form,
						'request' => $request,
						'validator' => $validator
				));
	}
}