<?php namespace Notification\Controller;

use Core\Templates\Regions;
use Core\Layout;
use Core\Notifications\NotificationTemplate;
use Core\RestController;
use Core\ServiceLayer;
use DateTime;
use Exception;
use Webteamer\Event\Model\Event;
use Webteamer\Locality\Model\Playground;
use Webteamer\Team\Model\Team;
use Core\Types\DateTimeEx;
use Core\Events\EventInfo;
use Core\Collections\Iter;

class TestController extends RestController
{
    
    public function emailMarketingAction()
    {
        //$emailMarketingManager = \Core\ServiceLayer::getService('EmailMarketingManager');
        //$emailMarketingManager->sendAfterScoreSetup($user,$team);
        
        $api = new \GetResponse\Api();
        $api->createTag();
        
        /*
        $tags = $api->getTags();
        var_dump($tags);
        
        $custom = $api->getCustomFields();
        var_dump($custom);
        */
        exit;
       
    }
    
    public function showBeepEventsAction()
    {
        $teamEventManager = ServiceLayer::getService('TeamEventManager');
        $teamEventManager->getBeepEvents();
        
        
    }
    

    public function EventCancelPlayerAttendanceNotificationAction()
    {
        $data = array();
        $data['lang'] = 'sk';
        $data['email'] =  DEV_EMAIL;

        $notificationManger = ServiceLayer::getService('notification_manager');
        $notificationManger->sendCancelPlayerAttendance($data);
    }
            
    
    
    
    /**
     * Get recipients for event
     */
    public function eventInvitationStoreAction()
    {
        $request = ServiceLayer::getService('request');
        $eventManager = ServiceLayer::getService('EventManager');
        $teamEventManager = ServiceLayer::getService('TeamEventManager');
        $event = $teamEventManager->findEventById($request->get('id'));
        $eventCurrentDate = $request->get('current_date');
        $event->setCurrentDate(new \DateTime($eventCurrentDate));
        
        //t_dump($event);
        
        $store = new \Core\Notifications\EventInvitationStore();
        //$test = iterator_to_array($store->yieldEvent($event));

        
        
        $sender = new \Core\Notifications\EventInvitationNotificationSender(new \Core\Notifications\EventInvitationStore());
$sender->sendAll();

/*
        $storeData = iterator_to_array($sender->getStoreData());

        t_dump($storeData);
        */
    }
    
    
    
    public function saveMessageToFile($notification,$name)
    {
        ob_start();
        echo $notification->getMessage();
        $mailStream = ob_get_contents();
        ob_end_clean();
        file_put_contents(APPLICATION_PATH . '/log/' . $name, $mailStream);
    }
    
    /**
    * Send TeamInvitationDecline notification to DEV_EMAIL 
    * @return type
    */
    public function eventInvitationEmailAction()
    {
        die('asdfa');
        $request = ServiceLayer::getService('request');
        $eventManager = ServiceLayer::getService('EventManager');
        $teamEventManager = ServiceLayer::getService('TeamEventManager');
        $event = $teamEventManager->findEventById(194);
        $eventCurrentDate = '2018-08-14';
        $event->setCurrentDate(new \DateTime($eventCurrentDate));
        $store = new \Core\Notifications\EventInvitationStore();
        $sender = new \Core\Notifications\EventInvitationNotificationSender(new \Core\Notifications\EventInvitationStore());
        $sender->sendAll();

        
        
        /*
        if (null == $lang) $lang = 'sk';
        $notification_manager = ServiceLayer::getService('notification_manager');
        $notificationType = "EventInvitation_{$lang}";
        $notification_repository = ServiceLayer::getService('notification_repository');
        $notification = $notification_repository->findOneBy([
            'notification_type' => $notificationType
        ]);
        $template = new \Core\Notifications\NotificationTemplate("senders/{$notificationType}");
        $event = new Event();
        $event->setName('Test event');
        $event->setTeamId(2);
        $event->setPlaygroundData('a:12:{s:2:"id";s:2:"12";s:4:"name";s:44:"Vysoká 2950/21, 811 06 Bratislava, Slovakia";s:10:"created_at";s:19:"2016-04-27 23:39:33";s:9:"author_id";s:1:"4";s:11:"locality_id";s:2:"15";s:4:"city";s:10:"Bratislava";s:6:"street";s:7:"Vysoká";s:13:"street_number";s:0:"";s:11:"description";N;s:9:"area_type";N;s:3:"lat";s:7:"48.1491";s:3:"lng";s:7:"17.1112";}');
        $template->userEmail = DEV_EMAIL;
        $template->lang = $lang;
        $template->activationGuid = '887c6285-a121-49dd-a3e0-f496f3903e22';
        $template->eventInGuid = '8bd92cb5-3153-4994-9666-f3a957d84b07';
        $template->eventOutGuid = 'cad6277c-bcaf-430b-9116-c8179a8548b7';
        $template->event = $event;
        $template->start = new DateTimeEx();
        $regions = Regions::parse($template);
        $notification->setSubject($regions['subject']);
        $notification->setMessage($regions['default']);
        $this->saveMessageToFile($notification, $notificationType . '.html');
        
        try
        {
            $notification->setRecipient(DEV_EMAIL);
            $send_result = $notification_manager->sendNotification($notification);
            return $this->asJson(['result' => 'success','send_result' => $send_result]);
        }
        catch (Exception $e)
        {
            dump($e);
        }
         * 
         */
    }
    
    public function eventSummaryEmailAction()
    {
        $teamManager = ServiceLayer::getService('TeamManager');
        
        $team = $teamManager->findTeamById(3);
        $teamEventManager = ServiceLayer::getService('TeamEventManager');
        $from = new \DateTime('2016-10-01');
        $to = new \DateTime('2016-10-31');
        $events = $teamEventManager->findEvents($team, array('from' => $from, 'to' => $to));
        $eventsList = $teamEventManager->buildTeamEventList($events, $from, $to);
        $store = new \Core\Notifications\EventSummaryStore();
        $store->setEvents($eventsList);
        $store->setForceEventInfo(true);
        $sender = new \Core\Notifications\EventSummaryNotificationSender($store);
        $data = $store->getData();
        $sendData = $data[0];
        $sendData->isAdmin = false;
        try
        {
             $sender->send($sendData);
             return $this->asJson(['result' => 'success','send_result' => $send_result]);
        }
        catch (Exception $e)
        {
            
        }

        
        /*
        $data = new \stdClass();
        $data->email = DEV_EMAIL;
        $data->default_lang = 'sk';
        $data->eventDetailGuid = '0000';
        
        $event = new Event();
        $event->setname('TEST');
        $event->setId(0);
        $data->event = $event;
        $data->isAdmin = false;
        $sender = new \Core\Notifications\EventSummaryNotificationSender(new TestStore());
        
      
        
        $data->subject = new playerEventInfoSubjectMock();
        $data->start = new \Core\Types\DateTimeEx();
        try
        {
             $sender->send($data);
             return $this->asJson(['result' => 'success','send_result' => $send_result]);
        }
        catch (Exception $e)
        {
            
        }
        */
       
    }
    
    public function eventSummaryEmailAdminAction()
    {
        $data = new \stdClass();
        $data->email = DEV_EMAIL;
        $data->default_lang = 'sk';
        $data->eventDetailGuid = '0000';
        
        $event = new Event();
        $event->setname('TEST');
        $event->setId(0);
        $data->event = $event;
        $data->isAdmin = true;
        $sender = new \Core\Notifications\EventSummaryNotificationSender(new TestStore());
        
      
        
        $data->subject = new playerEventInfoSubjectMock();
        $data->start = new \Core\Types\DateTimeEx();
        try
        {
             $sender->send($data);
             return $this->asJson(['result' => 'success','send_result' => $send_result]);
        }
        catch (Exception $e)
        {
            
        }
    }
    
     /**
     * Send TeamInvitationDecline notification to DEV_EMAIL 
     * @return type
     */
    public function TeamInvitationDeclineEmailAction()
    {
         if (null == $lang) $lang = 'sk';
          $notification_manager = ServiceLayer::getService('notification_manager');
        $notificationType = "TeamInvitationDecline_{$lang}";
        $notification_repository = ServiceLayer::getService('notification_repository');
        $notification = $notification_repository->findOneBy([
            'notification_type' => $notificationType
        ]);
        $template = new \Core\Notifications\NotificationTemplate("senders/{$notificationType}");
         $template->userEmail =DEV_EMAIL;
        $template->lang = $lang;
        $template->activationGuid = '0000';
         $template->sender_name = 'ADMIN NAME';
         $template->team_name = 'TEAM NAME';
        $notification->setMessage((string)$template);
        $this->saveMessageToFile($notification,$notificationType.'.html');
        try
        {
            $notification->setRecipient(DEV_EMAIL);
             $send_result = $notification_manager->sendNotification($notification);
             return $this->asJson(['result' => 'success','send_result' => $send_result]);
        }
        catch (Exception $e)
        {
            
        }
    }
    
    /**
     * Send TeamInvitation notification to DEV_EMAIL 
     * @return type
     */
    /*
    public function teamInvitationEmailAction()
    {
         if (null == $lang) $lang = 'sk';
          $notification_manager = ServiceLayer::getService('notification_manager');
        $notificationType = "TeamInvitation_{$lang}";
        $notification_repository = ServiceLayer::getService('notification_repository');
        $notification = $notification_repository->findOneBy([
            'notification_type' => $notificationType
        ]);
        $template = new \Core\Notifications\NotificationTemplate("senders/{$notificationType}");
         $template->userEmail =DEV_EMAIL;
        $template->lang = $lang;
        $template->activationGuid = '0000';
         $template->sender_name = 'ADMIN NAME';
         $template->team_name = 'TEAM NAME';
        $notification->setMessage((string)$template);
        $this->saveMessageToFile($notification,$notificationType.'.html');
        try
        {
            $notification->setRecipient(DEV_EMAIL);
             $send_result = $notification_manager->sendNotification($notification);
             return $this->asJson(['result' => 'success','send_result' => $send_result]);
        }
        catch (Exception $e)
        {
            
        }
    }
    */
     
    /**
     * Send AdminWithoutPlayers notification to DEV_EMAIL 
     * @return type
     */
    public function userWithoutTeamEmailAction()
    {
        $data = new \stdClass();
        $data->email = DEV_EMAIL;
        $data->default_lang = 'sk';
        
        $sender = new \Core\Notifications\UserWithoutTeamNotificationSender(new \Core\Notifications\UserWithoutTeamStore());

        try
        {
             $sender->send($data);
             return $this->asJson(['result' => 'success','send_result' => $send_result]);
        }
        catch (Exception $e)
        {
            
        }
    }
    
    /**
     * Send userWithTeam notification to DEV_EMAIL 
     * @return type
     */
    public function userWithTeamEmailAction()
    {
        $data = new \stdClass();
        $data->email = DEV_EMAIL;
        $data->default_lang = 'sk';
        
        $sender = new \Core\Notifications\UserWithTeamNotificationSender(new \Core\Notifications\UserWithTeamStore());

        try
        {
             $sender->send($data);
             return $this->asJson(['result' => 'success','send_result' => $send_result]);
        }
        catch (Exception $e)
        {
            
        }
    }
    
    /**
     * Send AdminWithoutPlayers notification to DEV_EMAIL 
     * @return type
     */
    public function adminWithoutPlayersEmailAction()
    {
         
         $data = new \stdClass();
        $data->email = DEV_EMAIL;
        $data->default_lang = 'sk';
        
        $sender = new \Core\Notifications\AdminWithoutPlayersNotificationSender(new \Core\Notifications\AdminWithoutPlayersStore());

        try
        {
             $sender->send($data);
             return $this->asJson(['result' => 'success','send_result' => $send_result]);
        }
        catch (Exception $e)
        {
            
        }
        
    
    }
    
    public function messageNewEmailAction()
    {
        $data = array();
        $data['lang'] = 'sk';
        $data['sender_name'] = 'JANO KOVAC';
         $data['email'] =  DEV_EMAIL;
        $data['mid'] = '0000';

        $notificationManger = ServiceLayer::getService('notification_manager');
        $notificationManger->sendMessageNewEmail($data);
    }        

    
     /**
     * Send test last activation email to DEV_EMAIL 
     * @return type
     */
    public function lastActivationEmailAction()
    {
        $data = array();
        $data['lang'] = 'sk';
        $data['name'] = 'JANO KOVAC';
        $data['email'] =  DEV_EMAIL;
        $data['activationGuid'] = '00000';
        
        $notificationManger = ServiceLayer::getService('notification_manager');
        $notificationManger->sendLastActivationEmail($data);
         
    }
     /**
     * Send test activation email to DEV_EMAIL 
     * route_name test_last_activation_email
     * @return type
     */
    public function activationEmailAction()
    {
        $data = array();
        $data['lang'] = 'sk';
        $data['name'] = 'JANO KOVAC';
        $data['email'] =  DEV_EMAIL;
        $data['activationGuid'] = '00000';
        
        $notificationManger = ServiceLayer::getService('notification_manager');
        $notificationManger->sendActivationEmail($data);
    }
    
     /**
     * Send test registration email to DEV_EMAIL 
     * @return type
     */
    public function registrationEmailAction()
    {
       
        $data = array();
        $data['lang'] = 'sk';
        $data['email'] =  DEV_EMAIL;
        $data['activationGuid'] = '00000';
        
        $notificationManger = ServiceLayer::getService('notification_manager');
        $notificationManger->sendBaseRegisterEmail($data);
    }
    

    /**
    * team invitation for unregistred player
    */
    public function teamInvitationUnregistredEmailAction()
    {

        $user = new \User\Model\User();
        $user->setName('Test');
        
        $team = new Team();
        $team->setName('TEST TEAM');
        
        $teamPlayer = new \Webteamer\Team\Model\TeamPlayer();
        $teamPlayer->setCreatedAt(new \DateTime());
        $teamPlayer->setEmail(DEV_EMAIL);
        $teamPlayer->setId(0);
     
        $notifyManger = ServiceLayer::getService('notification_manager');
        $notifyManger->sendMemberInvitation($user, $team,  $teamPlayer );
       
    }
    
      /**
    * team invitation for registred player
    */
    public function teamInvitationRegistredEmailAction()
    {

        $user = new \User\Model\User();
        $user->setName('Test');
        
        $team = new Team();
        $team->setName('TEST TEAM');
        
        $teamPlayer = new \Webteamer\Team\Model\TeamPlayer();
        $teamPlayer->setCreatedAt(new \DateTime());
        $teamPlayer->setEmail(DEV_EMAIL);
        $teamPlayer->setPlayerId(1);
     
        $notifyManger = ServiceLayer::getService('notification_manager');
        $notifyManger->sendMemberInvitation($user, $team,  $teamPlayer );
       
    }
    
    /**
     * Send test verify email to DEV_EMAIL 
     * route_name test_verify_email
     * @return type
     */
    public function verifyEmailAction()
    {
        
        $data = array();
        $data['lang'] = 'sk';
        $data['email'] =  DEV_EMAIL;
        $data['activationGuid'] = '00000';
        
        $notificationManger = ServiceLayer::getService('notification_manager');
        $notificationManger->sendVerifyEmail($data);
       
        
      
         
    }
    
    public function eventAddStatEmailAction()
    {
        $teamManager = ServiceLayer::getService('TeamManager');
        
        $team = $teamManager->findTeamById(3);
        $teamEventManager = ServiceLayer::getService('TeamEventManager');
        $from = new \DateTime('2016-10-01');
        $to = new \DateTime('2016-10-31');
        $events = $teamEventManager->findEvents($team, array('from' => $from, 'to' => $to));
        $eventsList = $teamEventManager->buildTeamEventList($events, $from, $to);
        $store = new \Core\Notifications\AddStatisticsStore();
        //$store->setEvents($eventsList);
        $store->setForceEventInfo(true);
        $sender = new \Core\Notifications\AddStatisticsNotificationSender($store);
        $data = $store->getData();
        //t_dump($data);
        
        exit;
        try
        {
             $sender->send($data[0]);
             return $this->asJson(['result' => 'success','send_result' => $send_result]);
        }
        catch (Exception $e)
        {
            
        }
    }
    
     public function eventAddRateEmailAction()
    {
        $data = new \stdClass();
        $data->email = DEV_EMAIL;
        $data->default_lang = 'sk';
        $data->eventDetailGuid = '0000';
        
        $event = new Event();
        $event->setname('TEST');
        $event->setId(1031);
        $event->setCurrentDate(new \DateTime('2016-09-27'));
        $data->event = $event;
        $sender = new \Core\Notifications\AddRateNotificationSender(new TestStore());
        
        $data->subject = new playerEventInfoSubjectMock();
        $data->start = new \Core\Types\DateTimeEx(new \DateTime('2016-09-27'));
        $data->userId = 3;
        try
        {
             $sender->send($data);
             return $this->asJson(['result' => 'success','send_result' => $send_result]);
        }
        catch (Exception $e)
        {
            
        }
    }
  
     public function matchResultEmailAction()
    {
        $data = new \stdClass();
        $data->email = DEV_EMAIL;
        $data->default_lang = 'sk';
        $data->eventDetailGuid = '0000';
        
        $lineup = new \Webteamer\Team\Model\TeamMatchLineup();
        $lineup->setId(0);
        $event = new Event();
        $event->setLineup($lineup);
        $event->setname('TEST');
        $event->setId(1031);
        $event->setCurrentDate(new \DateTime('2016-09-27'));
        $data->event = $event;
        $sender = new \Core\Notifications\MatchResultsNotificationSender(new TestStore());
        
        $player =  new \Webteamer\Team\Model\TeamPlayer();
        $player->setEmail(DEV_EMAIL);
        $player->setTeamRole('ADMIN');
        $data->player = $player;
        $data->userId = 3;
        
        $data->start = new \Core\Types\DateTimeEx();
        try
        {
             $sender->send($data);
             return $this->asJson(['result' => 'success','send_result' => $send_result]);
        }
        catch (Exception $e)
        {
            
        }
    }
    
   
        
  
}

/**
 * MOCK CLASSES
 */
class playerEventInfoSubjectMock
{
    /*
    public function __construct() {
        $this->isAdmin = false;
    }
    */
    public function getTeamPlayer()
    {
        $player =  new \Webteamer\Team\Model\TeamPlayer();
        $player->setEmail(DEV_EMAIL);
        return $player;
    }
    
    
}


class TestStore extends \Core\Notifications\NotificationStore
{
    protected function dataSource()
    {
        //return array('test' => 'blawwwwwwwwwwwww');
         //return $this->addData(iterator_to_array($this->getActualEventInfos()));
    }
}