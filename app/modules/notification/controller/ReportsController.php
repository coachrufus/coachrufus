<?php namespace Notification\Controller;

use Core\RestController;
use Core\Request;
use Core\ServiceLayer;
use Core\Activity\ActivityTypes;
use Core\Types\DateTimeEx;
use Core\Events\EventInfo;
use Core\RenderOptions;

class ReportsController extends RestController
{    
    private $request;
    private $eventInfo;
    
    function __construct()
    {
        $this->request = ServiceLayer::getService("request");
    }
    
    function playerEventSummaryAction()
    {
        $eventInfo = EventInfo::create(
            $this->request->get('id'),
            DateTimeEx::parse($this->request->get('date'), 'Y-m-d')
        );
        
        return $this->render([
            'event' => $eventInfo->getEvent(),
            'attendance' => $eventInfo->getAttendance(),
            'lineup' => $eventInfo->getLineup(),
            'subjects' => $eventInfo->getSubjects(false),
            'lineupSubjects' => $eventInfo->getLineupSubjects(),
            'teamPlayers' => $eventInfo->getTeamPlayers()
        ], new RenderOptions(['layout' => false]));
    }
    
    function adminEventSummaryAction()
    {
        return $this->playerEventSummaryAction();
    }
}