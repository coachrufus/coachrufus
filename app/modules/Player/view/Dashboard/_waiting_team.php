<div class="panel panel-default team-panel-waiting-wrap">
    <div class="panel-heading">
        <h3><i class="ico ico-team"></i> <?php echo $team->getName() ?></h3>
        <div class="pull-right panel-tools">
            <a href="#" class="panel-settings-trigger" data-target="team-panel-settings-<?php echo $team->getId() ?>"><i class="ico ico-action ico-menu-list"></i></a>
            <i class="ico ico-action ico-panel-close"></i>
        </div>
    </div>
    <div  class="panel-settings" id="team-panel-settings-<?php echo $team->getId() ?>">
        <div class="settings-row">
            <div class="settings-cell-2">

            </div>
            <div class="settings-cell-2 settings-cell-right">
                <a  href="<?php echo $router->link('team_event_list',array('team_id' => $team->getId())) ?>">
                    <?php echo $translator->translate('All events') ?> <i class="ico ico-calendar-empty ico-action"><?php echo date('d') ?></i>
                </a>
            </div>
        </div>

        <div class="settings-row">
            <div class="settings-cell-2">

            </div>
            <div class="settings-cell-2 settings-cell-right">
                <a  href="<?php echo $router->link('team_public_players_list',array('team_id' => $team->getId())) ?>">
                    <?php echo $translator->translate('All members') ?> <i class="ico ico-dress-b"></i>
                </a>
            </div>
        </div>

        <div class="settings-row">
            <div class="settings-cell-2">

            </div>
            <div class="settings-cell-2 settings-cell-right">
                <a  href="<?php echo $router->link('widget_list',array('team_id' => $team->getId())) ?>">
                    <?php echo $translator->translate('All graphs') ?> <i class="ico ico-graph"></i>
                </a>
            </div>
        </div> 
    </div>
    <div class="panel-body  panel-full-body team-panel ">

        <div class="panel-team-header">
             <div class="img_round_wrap team_img_round">
                <a  style="background-image: url('<?php echo $team->getMidPhoto() ?>')"  href="<?php echo $router->link('team_overview', array('id' => $team->getId())) ?>" ></a>
            </div>
            <div class="team-info fake-link" data-redirect="<?php echo $router->link('team_overview', array('id' => $team->getId())) ?>">
                <h3 class=""><?php echo $team->getName() ?></h3>
                <h4 class=""><?php echo $translator->translate($sportEnum[$team->getSportId()]) ?></h4>
                <small></small>
            </div>
            <a class="team-more-link" href="<?php echo $router->link('team_overview', array('id' => $team->getId())) ?>"><i class="ico ico-team-more"></i></a>
        </div>
        
        <div class="team-panel-waiting">
            <h3><?php echo $translator->translate('TEAM_PANEL_INVITATION_TITLE') ?></h3>
            <p><?php echo $translator->translate('TEAM_PANEL_INVITATION_TEXT') ?></p>
            <div class="row">
                <div class="col-xs-6">
                     <a href="<?php echo $router->link('team_member_invite',array('tp' => $teamPlayer->getId() ,'hash' => $team->getShareLinkHash(),'h' => $teamPlayer->getHash() )) ?>" class="btn btn-primary" type="submit"><?php echo $translator->translate('TEAM_PANEL_INVITATION_BTN_YES') ?></a>
                </div>
                <div class="col-xs-6">
                     <a href="<?php echo $router->link('team_member_invite_decline',array('tp' => $teamPlayer->getId() ,'hash' => $team->getShareLinkHash(),'h' => $teamPlayer->getHash() )) ?>" class="btn btn-danger" type="submit"><?php echo $translator->translate('TEAM_PANEL_INVITATION_BTN_NO') ?></a>
                </div>
            </div>
        </div>


    </div>
</div>

