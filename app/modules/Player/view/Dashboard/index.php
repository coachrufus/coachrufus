<?php if ($request->hasFlashMessage('dashboard_flash_message')): ?>
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <?php echo $request->getFlashMessage('dashboard_flash_message') ?>
    </div>
<?php endif; ?>

<?php if ($request->hasFlashMessage('member_invite_decline_message')): ?>
<p class="alert alert-danger">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <?php echo $request->getFlashMessage('member_invite_decline_message') ?>
</p>
<?php endif; ?> 

<?php if ($request->hasFlashMessage('member_team_join_request_cancel_message')): ?>
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <?php echo $request->getFlashMessage('member_team_join_request_cancel_message') ?>
    </div>
<?php endif; ?>

<section class="content">
    <div class="row widget-grid">
        
        
        <?php if($user->getMobileAppDevice() != 1 && $user->getLayoutSettingsValue('hideMobileBanner') != true): ?>
        <div class="col-md-4 col-sm-6  widget banner-widget">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3><?php echo $translator->translate('dashboard.banner.downloadApp') ?></h3>
                    <a class="pull-right dashboard-banner-close" href="#"><i class="ico ico-close-black"></i></a>
                </div>

                <div class="panel-body  panel-full-body team-panel banner-panel" data-redirect="/team/overview/1543">
                    <img src="/theme/img/mobile_app_banner_wide_<?php echo $user->getDefaultLang() ?>.jpg" alt="" />
                     
                     <a  onclick="window.open(this,'_blank');return false;" href="https://play.google.com/store/apps/details?id=com.altamira.rufus" class="mobile_app_promo mobile_app_promo_google">
                        <img  src="/theme/img/rufus_badge_sk_googleplay_<?php echo $user->getDefaultLang() ?>.svg" alt="" title="" />
                    </a>
                    <a  onclick="window.open(this,'_blank');return false;"href="https://itunes.apple.com/us/app/rufus-your-favourite-teammate/id1435067809" class="mobile_app_promo mobile_app_promo_ios">
                        <img  src="/theme/img/rufus_badge_sk_ios_<?php echo $user->getDefaultLang() ?>.svg" alt="" title="" />
                    </a>
                </div>
            </div>
        </div>
        <?php endif; ?>

        <div class="col-md-4 col-sm-6 widget">
              <?php $layout->includePart(MODUL_DIR . '/Event/view/widget/_small_calendar.php') ?>
        </div>
        
        <div class="col-md-4 col-sm-6 widget">
              <?php $layout->includePart(MODUL_DIR . '/Scouting/view/widget/_user_dashboard.php') ?>
        </div>
        
          <?php foreach ($waitingTeams as $waitingTeam): ?>
             <div class="col-md-4 col-sm-4 widget invitation-widget-panel">
             <?php $layout->includePart(MODUL_DIR . '/Player/view/Dashboard/_waiting_team.php',array('team' => $waitingTeam['team'],'teamPlayer' => $waitingTeam['player'],'sportEnum' => $sportEnum)) ?>
             </div>
          <?php endforeach; ?>

        <?php foreach ($teams as $team): ?>
        <div class="col-md-4 col-sm-6 team-widget-panel widget" data-team-id="<?php echo $team->getId() ?>"></div>
        <?php endforeach; ?>
        
         <?php foreach ($tournamentList as $tournament): ?>
            <?php $layout->includePart(MODUL_DIR . '/tournament/view/tournament/_tournament_panel.php',array(
                'tournament' => $tournament,
                'sportChoices' => $sportEnum
                    )) ?> 
        <?php endforeach; ?>
        
        
     </div> 
    
  
   
    
    <?php if(!empty($unconfirmedTeams)): ?>
        <h2><?php echo $translator->translate('Wating to approve') ?></h2>
        <div class="row widget-grid">
              <?php foreach ($unconfirmedTeams as $unconfirmedTeam): ?>
             <?php $layout->includePart(MODUL_DIR . '/Player/view/Dashboard/_unconfirmed_team.php',array('team' => $unconfirmedTeam,'user' => $user)) ?>
             <?php endforeach; ?>
        </div> 
    <?php endif; ?>
    
    
   
</section>

<?php $layout->addJavascript('js/TeamEventManager.js') ?>
<?php $layout->addJavascript('js/ScoutingManager.js') ?>
<?php $layout->addJavascript('js/TeamManager.js') ?>
<?php $layout->addJavascript('js/TeamWallManager.js') ?>

<?php $layout->addJavascript('/plugins/select2/select2.full.min.js') ?>

<?php $layout->addStylesheet('plugins/pickadate/themes/classic.css') ?>
<?php $layout->addStylesheet('plugins/pickadate/themes/classic.date.css') ?>
<?php $layout->addJavascript('plugins/pickadate/picker.js') ?>
<?php $layout->addJavascript('plugins/pickadate/picker.date.js') ?>

<?php $layout->addStylesheet('plugins/timepicker/bootstrap-timepicker.min.css') ?>
<?php $layout->addJavascript('plugins/timepicker/bootstrap-timepicker.js') ?>
<?php $layout->addJavascript('js/SmallCalendar.js') ?>


<?php $layout->addJavascript('plugins/flot/jquery.flot.min.js') ?>
<?php $layout->addJavascript('plugins/flot/jquery.flot.pie.min.js') ?>
<?php $layout->addJavascript('js/fb.js') ?>


<?php  $layout->startSlot('javascript') ?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXU1duK54f1gIsRy_xc7z4uQURovsZdE8&libraries=places&callback=initLocalityManager" async defer></script>
 <script type="text/javascript">
     
     $('.dashboard-banner-close').on('click',function(e){
        e.preventDefault();
         $(this).parents('.banner-widget').fadeOut();
         
          $.ajax({
            method: "GET",
            url: '<?php echo $router->link('user_update_layout_settings') ?>',
            data: {'a':'hide-mobile-banner'}
          }).done(function( response ) {
           
         });
         
         
     });
     
     
     var team_manager = new TeamManager({
        'dataUrl': '<?php echo $router->link('widget_team_panel') ?>'
    });

     $('.team-widget-panel').each(function(key, element){
         team_manager.loadWidget($(element));
     });

    event_manger = new TeamEventsManager();
    event_manger.init();
      

    small_calendar = new SmallCalendar({
        'dataUrl': '<?php echo $router->link('rest_event_get_month_grid') ?>',
        'localityManager': new LocalityManager()
    });
    small_calendar.initLoad();
    
      
    
      
      var scouting_manger = new ScoutingManager({
          'dataUrl': '<?php echo $router->link('rest_last_scouting') ?>'
      });
     
      
    
      
      $(".select2").select2();
       jQuery('#scouting_game_time').pickadate({'format': 'dd.mm.yyyy'});
       jQuery('#scouting_valid_to').pickadate({'format': 'dd.mm.yyyy'});

       jQuery("#scouting_game_time_time").timepicker({showInputs: false,showMeridian: false, minuteStep: 5});
       jQuery("#scouting_game_time_to_time").timepicker({defaultTime:'00:00',showInputs: false,showMeridian: false, minuteStep: 5});
       if(jQuery("#scouting_game_time_to_time").val() == '00:00')
        {
            jQuery("#scouting_game_time_to_time").val('');
        }
      

    function initLocalityManager()
    {

        locality_manger = new LocalityManager();
        locality_manger.google = google;
        locality_manger.createSearchInput('scouting-form-location1');  
        
        locality_manger2 = new LocalityManager();
        locality_manger2.google = google;
        locality_manger2.createSearchInput('scouting-form-location2');  
        
        locality_manger3 = new LocalityManager();
        locality_manger3.google = google;
        locality_manger3.createSearchInput('scouting-form-location3');  
        
        locality_manger4 = new LocalityManager();
        locality_manger4.google = google;
        locality_manger4.createSearchInput('scouting-form-location4');  
        
        scouting_manger.loadWidget(google);
        
        /*
        $('#event-place').on('load',function(){
            console.log('loaded');
        });
        */
       
       <?php if('pro' == $request->get('payment')): ?>
            $('#team-credit-modal').modal('show'); 
       <?php endif; ?>
       
        
    }
    
    /*
    var wall_manger = new TeamWallManager({
          'dataUrl': ''
      });
      //console.log(wall_manger);
    wall_manger.loadWidget();
    
    */

 </script>
<?php  $layout->endSlot('javascript') ?>