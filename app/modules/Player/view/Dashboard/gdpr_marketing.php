<div id="full-window">
    <div class="full-window-cnt">
    <div class="row">
        <div class="col-sm-12">
           
            <img src="/theme/img/icon/big-envelope.svg" />
           
            <div>
               
             <?php echo $translator->translate('gdpr.marketing.agreeText') ?>
            </div>

        </div>
        <form action="" method="post" class="button-form">
                <input type="hidden" name="f_act" value="gdpr_marketing_agreement" />
                 <button name="gdpr_marketing_agreement" value="2" class="btn btn-primary agree-clear" type="submit"><?php echo $translator->translate('gdpr.marketing.skip') ?></button>
                  <button name="gdpr_marketing_agreement" value="1" class="btn btn-primary agree-clear agree-primary" type="submit"><?php echo $translator->translate('gdpr.marketing.agree') ?></button>
                
            </form>
    </div>
    </div>
</div>
