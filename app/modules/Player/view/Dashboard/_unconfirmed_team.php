<div class="col-md-4 col-sm-6 " data-team-id="6">                
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3><i class="ico ico-team"></i> <?php echo $translator->translate('Wating to approve') ?>: <?php echo $team->getName() ?></h3>

        </div>

        <div class="panel-body  panel-full-body team-panel" data-redirect="/team/overview/6">

            <div class="panel-team-header">
                <div class="img_round_wrap team_img_round" style="background-image: url('<?php echo $team->getMidPhoto() ?>')">

                </div>
                <div class="team-info">
                    <h3 class=""><?php echo $team->getName() ?></h3>
                    <h4 class=""><?php echo $translator->translate($sportEnum[$team->getSportId()]) ?></h4>
                    <a class="btn btn-danger btn-sm" href="<?php echo $router->link('team_join_request_canceled',array('hash' => $team->getShareLinkHash() )) ?>"><?php echo $translator->translate('dashboard.unconfirmed.cancelInvitation') ?></a>
                </div>

            </div>






        </div>

    </div></div>