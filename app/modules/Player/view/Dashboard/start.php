
<?php if ($request->hasFlashMessage('member_team_join_request_cancel_message')): ?>
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <?php echo $request->getFlashMessage('member_team_join_request_cancel_message') ?>
    </div>
<?php endif; ?>

<section class="content-header">
    <h1>
        <?php echo $translator->translate('Dashboard') ?>
    </h1>
</section>

<section class="content">
<div class="row">
    
    
    
     <?php if($user->getMobileAppDevice() != 1 && $user->getLayoutSettingsValue('hideMobileBanner') != true): ?>
        <div class="col-md-4 col-sm-6  widget banner-widget  col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3><?php echo $translator->translate('dashboard.banner.downloadApp') ?></h3>
                    <a class="pull-right dashboard-banner-close" href="#"><i class="ico ico-close-black"></i></a>
                </div>

                <div class="panel-body  panel-full-body team-panel banner-panel" data-redirect="/team/overview/1543">
                    <img src="/theme/img/mobile_app_banner_wide_<?php echo $user->getDefaultLang() ?>.jpg" alt="" />
                     
                     <a  onclick="window.open(this,'_blank');return false;" href="https://play.google.com/store/apps/details?id=com.altamira.rufus" class="mobile_app_promo mobile_app_promo_google">
                        <img  src="/theme/img/rufus_badge_sk_googleplay_<?php echo $user->getDefaultLang() ?>.svg" alt="" title="" />
                    </a>
                    <a  onclick="window.open(this,'_blank');return false;" href="https://itunes.apple.com/us/app/rufus-your-favourite-teammate/id1435067809" class="mobile_app_promo mobile_app_promo_ios">
                        <img  src="/theme/img/rufus_badge_sk_ios_<?php echo $user->getDefaultLang() ?>.svg" alt="" title="" />
                    </a>
                </div>
            </div>
        </div>
        <?php endif; ?>
    
     <?php if(!empty($unconfirmedTeams)): ?>
         <?php foreach ($unconfirmedTeams as $unconfirmedTeam): ?>
         <?php $layout->includePart(MODUL_DIR . '/Player/view/Dashboard/_unconfirmed_team.php',array('team' => $unconfirmedTeam,'user' => $user)) ?>
         <?php endforeach; ?>
    <?php endif; ?>
    
    <?php if(!empty($waitingTeams)): ?>
        <?php foreach ($waitingTeams as $waitingTeam): ?>
             <div class="col-md-4 col-sm-4 widget invitation-widget-panel">
             <?php $layout->includePart(MODUL_DIR . '/Player/view/Dashboard/_waiting_team.php',array('team' => $waitingTeam['team'],'teamPlayer' => $waitingTeam['player'],'sportEnum' => $sportEnum)) ?>
             </div>
          <?php endforeach; ?>
 <?php endif; ?>
  
<?php if(\Core\ServiceLayer::getService('security')->getIdentity()->getUser()->getResource() == 'group'): ?>    
          <div class="col-md-4 col-md-offset-2">
                    <div class="box box-widget widget-user create-team-widget">
                        <div class="widget-user-header">
                            <h3 class="widget-user-username"><?php echo $translator->translate('Crate new team') ?></h3>
                        </div>
                        <div class="widget-user-image">
                            <a  class="ico ico-create-team" href="<?php echo $router->link('team_create') ?>"></a>
                        </div> 
                        <div class="box-footer">

                        </div>
                    </div>
                </div>
      <?php $layout->includePart(MODUL_DIR . '/Team/view/team/_group_team_create.php') ?>
<?php else: ?>
     <div class="col-md-4  <?php echo ($user->getMobileAppDevice() != 1 && $user->getLayoutSettingsValue('hideMobileBanner') != true) ? '' : 'col-md-offset-4' ?>">
                    <div class="box box-widget widget-user create-team-widget">
                        <div class="widget-user-header">
                            <h3 class="widget-user-username"><?php echo $translator->translate('Crate new team') ?></h3>
                        </div>
                        <div class="widget-user-image">
                            <a  class="ico ico-create-team" href="<?php echo $router->link('team_create') ?>"></a>
                        </div> 
                        <div class="box-footer">

                        </div>
                    </div>
                </div>
<?php endif; ?>
    
    
   

    
   
    </div>
    
   
    
     
  
</div>
</section>

<?php  $layout->startSlot('javascript') ?>
 <script type="text/javascript">
      $('.dashboard-banner-close').on('click',function(e){
        e.preventDefault();
         $(this).parents('.banner-widget').fadeOut();
         
          $.ajax({
            method: "GET",
            url: '<?php echo $router->link('user_update_layout_settings') ?>',
            data: {'a':'hide-mobile-banner'}
          }).done(function( response ) {
         });
     });
</script>
<?php  $layout->endSlot('javascript') ?>


