


<!-- Main content -->
<section class="content">

    <div class="row">
        <div class="col-md-3">

            <!-- Profile Image -->
            <div class="box box-primary">
                <div class="box-body box-profile">
                    <div class="img_round_wrap profil_img_round public_profil_img_round" style="background-image:url('<?php echo $player->getMidPhoto() ?>') ">

                    </div>
                    <h3 class="profile-username text-center"><?php echo $player->getFullName() ?></h3>
                    <strong><i class="fa fa-futbol-o margin-r-5"></i> <?php echo $translator->translate('Sports') ?></strong>
                    <p>
                        <?php foreach ($playerSports as $playerSport): ?>
                            <span class="label label-warning"><?php echo $playerSport->getSportName() ?></span>
                        <?php endforeach; ?>
                    </p>

                    <?php if($isFollower): ?>
                         <a href="<?php echo $router->link('player_public_unfollow',array('id' => $player->getId())) ?>" class="btn btn-warning btn-block"><?php echo $translator->translate('Unfollow') ?></a>
                    <?php else: ?>
                        <a href="<?php echo $router->link('player_public_follow',array('id' => $player->getId())) ?>" class="btn btn-success btn-block"><?php echo $translator->translate('Follow') ?></a>
                    <?php endif; ?>
                        
                        
                <a href="<?php echo $router->link('player_add_friend',array('id' => $player->getId())) ?>" class="btn btn-warning btn-block"><?php echo $translator->translate('Add friend') ?></a>
                  
                </div><!-- /.box-body -->
            </div><!-- /.box -->

            <!-- About Me Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"><?php echo $translator->translate('Teams') ?></h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <?php foreach ($teams as $team): ?>
                        <div class="box box-widget widget-user">
                            <div class="widget-user-header bg-aqua-active">
                                <h3 class="widget-user-username"><?php echo $team->getName() ?></h3>
                                <h5 class="widget-user-desc"><?php echo $translator->translate($sportEnum[$team->getSportId()]) ?></h5>
                            </div>
                            <div class="widget-user-image img_round_wrap team_img_round">
                                <a  style="background-image: url('<?php echo $team->getMidPhoto() ?>')"  href="<?php echo $router->link('team_public_page', array('id' => $team->getId())) ?>" >


                                </a>
                            </div>
                            <div class="box-footer">
                                <div class="row">
                                    <div class="col-sm-4 border-right">
                                        <div class="description-block">
                                            <h5 class="description-header"><?php echo $teamInfo[$team->getId()]['members_count'] ?></h5>
                                            <span class="description-text"><?php echo $translator->translate('Memebers') ?></span>
                                        </div><!-- /.description-block -->
                                    </div><!-- /.col -->
                                    <div class="col-sm-4 border-right">
                                        <div class="description-block">
                                            <h5 class="description-header"><?php echo $teamInfo[$team->getId()]['followers'] ?></h5>
                                            <span class="description-text"><?php echo $translator->translate('Followers') ?></span>
                                        </div><!-- /.description-block -->
                                    </div><!-- /.col -->
                                    <div class="col-sm-4">
                                        <div class="description-block">
                                            <h5 class="description-header"><?php echo $playerPermissiomEnum[$userTeamRoles[$team->getId()]['role']] ?></h5>
                                            <span class="description-text"></span>
                                        </div><!-- /.description-block -->
                                    </div><!-- /.col -->
                                </div><!-- /.row -->
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
        <div class="col-md-9">
            <div class="nav-tabs-custom">
                 <ul class="nav nav-tabs">
                  <li class="active"><a href="#posts" data-toggle="tab"><?php echo $translator->translate('Posts') ?></a></li>
                  <li><a href="#followers" data-toggle="tab"><?php echo $translator->translate('Followers') ?></a></li>
                </ul>

                <div class="tab-content">
                    <div class="active tab-pane" id="posts">
                        <?php if($isOwner): ?>
                        <div class="post row">
                            <form action="<?php echo $router->link('create_player_wall_post') ?>" method="post" class="player-wall-post-form ajax-form">
                                <div class="form-group col-sm-12" data-error-bind="body">
                                    <label><?php echo $translator->translate('Add new post') ?></label>
                                    <?php echo $form->renderTextareaTag('body', array('class' => 'form-control', 'style' => 'width:100%;')) ?>
                                </div>

                                <div class="form-group col-sm-12">
                                    <button type="submit" class="btn btn-primary"><?php echo $translator->translate('Create') ?></button>
                                </div>
                            </form>
                        </div>
                        <?php endif; ?>
                         <div id="wall_posts">
                        <?php foreach($posts as $post): ?>
                            <!-- Post -->
                           <?php $layout->includePart(MODUL_DIR.'/Player/view/Public/_wall_line.php',array('post' => $post, 'isOwner' => $isOwner )) ?>
                        <?php endforeach; ?>
                         </div>
                    </div><!-- /.tab-pane -->
                    <div class="tab-pane" id="followers">
                        <ul class="users-list followers-list  clearfix">
                            <?php foreach ($followers as $follower): ?>
                           <li>
                                <div class="img_round_wrap member_img_round_100" style="background-image: url('<?php echo $follower->getMidPhoto() ?>');">&nbsp;</div>
                                <a class="users-list-name" href="<?php echo $router->link('player_public_profile',array('player_id' => $follower->getId())) ?>"><?php echo $follower->getFullName() ?></a>
                          </li>
                          <?php endforeach; ?>
                      </ul><!-- /.users-list -->
                        
                    </div>


                </div><!-- /.tab-content -->
            </div><!-- /.nav-tabs-custom -->
        </div><!-- /.col -->
    </div><!-- /.row -->

</section><!-- /.content -->

<div class="modal fade" id="removePostModal" tabindex="-1" role="dialog" aria-labelledby="<?php echo $translator->translate('Remove post') ?>">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?php echo $translator->translate('Remove post') ?></h4>
            </div>
            <div class="modal-body">
                <?php echo $translator->translate('Are you sure?') ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $translator->translate('Cancel') ?></button>
                <button type="button" class="btn btn-danger modal_submit"><?php echo $translator->translate('Confirm') ?></button>
            </div>
        </div>
    </div>
</div>


<?php $layout->addStylesheet('css/public.css') ?>
<?php $layout->addJavascript('js/PlayerManager.js') ?>
<?php  $layout->startSlot('javascript') ?>
 <script type="text/javascript">

      player_manger = new PlayerManager();
      player_manger.bindTriggers();
      

        $('.delete-post-trigger').on('click',function(e){
            e.preventDefault();
            $('#removePostModal .modal_submit').attr('data-href',$(this).attr('href'));
            $('#removePostModal .modal_submit').attr('data-target',$(this).attr('data-target'));
            $('#removePostModal').modal('show');

       });

       $('#removePostModal .modal_submit').on('click',function(){
           //location.href=$(this).attr('data-href');
           var action = $(this).attr('data-href');
           var target_post = $(this).attr('data-target');
            $.ajax({
            method: "GET",
            url: action,
            dataType: 'json'
                }).done(function (response) {
                   
                    if (response.result == 'ERROR')
                    {
                        alert('error');
                    }
                    else
                    {
                        $('#'+target_post).fadeOut();
                        $('#removePostModal').modal('hide');
                    }
                });
           
       });
      
   
    

 </script>
<?php  $layout->endSlot('javascript') ?>

