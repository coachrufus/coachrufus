<div class="post" id="post-<?php echo $post->getId() ?>">
    <div class="user-block">
        <div class="img_round_wrap member_img_round" style="background-image: url('<?php echo $post->getAuthor()->getMidPhoto() ?>');">

        </div>
        <span class='username'>
            <a href="<?php echo $router->link('player_public_profile',array('player_id' => $post->getAuthorId())) ?>"><?php echo $post->getAuthor()->getFullName() ?></a>
            
            <?php if($isOwner): ?>
            <a data-target="post-<?php echo $post->getId() ?>" href='<?php echo $router->link('delete_player_wall_post',array('id' => $post->getId())) ?>' class='pull-right btn-box-tool delete-post-trigger'><i class='fa fa-times'></i></a>
            <?php endif; ?>
        </span>
        <span class='description'><?php echo $post->getFormatedCreatedAt() ?></span>
    </div><!-- /.user-block -->
    <?php echo $post->getBody() ?>



    <ul class="list-inline">


        <li class="pull-right"><a data-target="post_comments_<?php echo $post->getId() ?>" href="<?php echo $router->link('get_player_wall_post_comments', array('post' => $post->getId())) ?>" class="link-black text-sm show-comments-trigger"><i class="fa fa-comments-o margin-r-5"></i> <?php echo $translator->translate('Comments') ?> (<?php echo $post->getCommentsCount() ?>)</a></li>
    </ul>






    <div class="post_comments_wrap" id="post_comments_<?php echo $post->getId() ?>">
        <div class="input-group post_comment_wrap col-sm-12">
            <input  id="post-comment-body-<?php echo $post->getId() ?>" class="form-control input-sm" type="text" placeholder="<?php echo $translator->translate('Type comment') ?>" />
            <span class="input-group-btn">
                <button data-action="<?php echo $router->link('create_player_wall_post_comment') ?>" type="button" data-post-id="<?php echo $post->getId() ?>" class="btn btn-sm btn-success btn-flat post_comment_trigger"><?php echo $translator->translate('Send') ?></button>
            </span>
        </div>



    </div>


</div><!-- /.post -->