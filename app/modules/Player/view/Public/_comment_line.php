<div class="direct-chat-msg">
    <div class="direct-chat-info clearfix">
        <span class="direct-chat-name pull-left"><?php echo $comment->getAuthor()->getFullName() ?></span>
        <span class="direct-chat-timestamp pull-right"><?php echo $comment->getFormatedCreatedAt() ?></span>
    </div><!-- /.direct-chat-info -->
    <div class="img_round_wrap member_img_round" style="background-image: url('<?php echo $comment->getAuthor()->getMidPhoto() ?>');">

        </div>
    <div class="direct-chat-text">
        <?php echo $comment->getBody() ?>
    </div><!-- /.direct-chat-text -->
</div>