<form class="form-horizontal" action="#tab_privacy"  method="post">
     <input type="hidden" name="form_action" value="privacy-data" />
    <table class="table table-striped table-condensed">
        <thead>
            <tr>
                <th></th>
                <?php foreach ($privacyMatrix['groups'] as $grupName): ?>
                    <th><?php echo $translator->translate($grupName) ?></th>
                <?php endforeach; ?>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($privacyMatrix['sections'] as $sectionKey => $sectionsData): ?>
                <tr>
                    <td class="col-xs-2"><strong><?php echo $translator->translate($sectionsData['name']) ?></strong></td>
                    <?php foreach ($sectionsData['groups'] as $groupId => $groupData): ?>
                        <td  class="col-xs-2"><div class="icheck"><label> 
                                    <input value="1" type="checkbox" name="privacy[<?php echo $sectionKey ?>][<?php echo $groupId ?>]" <?php echo ($groupData['enabled'] == 1) ? 'checked="checked"' : '' ?> ></label></div></td>
                    <?php endforeach; ?>




                </tr>
            <?php endforeach; ?>




        </tbody>
    </table>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-danger"><?php echo $translator->translate('Submit') ?></button>
        </div>
    </div>
</form>

