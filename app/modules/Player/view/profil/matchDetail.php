


<section class="content">
    <div class="text-center">
        <h3><?php echo $event->getName() ?>, <?php echo $matchOverview->getEventDateFormated() ?></h3>
        <h4> <?php echo $playground->getName() ?></h4>
    </div>


    <div class="row">
        <div class="col-xs-6 text-center">
            <?php echo $matchOverview->getFirstLineName() ?>
            <h3><?php echo $matchOverview->getFirstLineGoals() ?></h3>
        </div>
        <div class="col-xs-6  text-center">
            <?php echo $matchOverview->getSecondLineName() ?>
            <h3><?php echo $matchOverview->getSecondLineGoals() ?></h3>
        </div>
    </div>


    <table class="table table-bordered table-striped">
        <tbody>
            <?php foreach ($timeline as $time => $timeItems): ?>
                <tr>
                    <td class="text-right"><?php echo $time ?></td>

                    <td class="text-center">
                        <?php foreach ($timeItems as $item): ?>
                            <?php if ($item->getLineupPosition() == 'first_line' && $item->getLineupPlayerId() > 0): ?>
                                <?php echo $hitEnum[$item->getHitType()] ?>: <?php echo $item->getPlayerName() ?><br />
                            <?php endif; ?>  
                        <?php endforeach; ?>
                    </td>

                    <td class="text-center">
                        <?php foreach ($timeItems as $item): ?>
                            <?php if ($item->getLineupPosition() == 'second_line' && $item->getLineupPlayerId() > 0): ?>
                                <?php echo $hitEnum[$item->getHitType()] ?>: <?php echo $item->getPlayerName() ?><br />
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </td>

                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>


    <table class="table table-condensed" style="background: white;">
        <thead>
            <tr>
                <th> <?php echo $translator->translate('Player') ?></th>
                <th><?php echo $translator->translate('Goals') ?></th>
                <th><?php echo $translator->translate('Assists') ?></th>
                <th><?php echo $translator->translate('Man of match') ?></th>
            </tr>

        <tbody>
            <?php foreach ($playersStat as $playerStat): ?>
                <tr>
                    <td><?php echo $playerStat['player']->getPlayerName() ?></td>
                    <td><?php echo $playerStat['stat']->getGoals() ?></td>
                    <td><?php echo $playerStat['stat']->getAssists() ?></td>
                    <td><?php if ($playerStat['stat']->getManOfMatch() == 1): ?><i class="fa fa-check"></i> <?php endif ?></td>

                </tr>
            <?php endforeach; ?>
        </tbody>
        </thead>
    </table>





</section>
