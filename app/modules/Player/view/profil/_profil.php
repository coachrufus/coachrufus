  <form action="#" class="form-horizontal form-bordered" method="post">
                <input type="hidden" name="form_action" value="base-data" />
                <div class="box box-primary">
                    <div class="box-body box-profile">
                        <div class="col-sm-12 text-center web_profile_photo">
                        <?php echo $form->renderInputHiddenTag('photo') ?>
                            <div id="avatar_photo"  class="img_round_wrap  profil_img_round" style="background-image: url('<?php echo $form->getEntity()->getMidPhoto() ?>');"></div>
                          
                        
                         <span class="btn btn-info fileinput-button btn-xs">
                               <i class="glyphicon glyphicon-plus"></i>
                               <span><?php echo $translator->translate('Change photo') ?></span>
                                
                           <input id="fileupload" type="file" name="avatar" data-url="<?php echo $router->link('profil_upload_photo') ?>" />
                         </span>
                        
                          <div class="upload-progress">
                            <span class="progress-num"></span>
                           <div class="progress progress-sm active">
                            <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                            </div>
                          </div>
                         </div>
                        </div>
                        <div class="col-sm-12 text-center mob_profile_photo">
                             <div id="avatar_photo"  class="img_round_wrap  profil_img_round" style="background-image: url('<?php echo $form->getEntity()->getMidPhoto() ?>');"></div>
                            <div class="alert alert-info"><?php echo $translator->translate($form->getFieldLabel('profile.photo.upload.alert')) ?></div>
                        </div>
 <div class="row">
                            <div class="col-sm-12">
                                <label class="control-label"><?php echo $translator->translate($form->getFieldLabel('email')) ?></label>
                                <?php echo $form->renderInputTag('email', array('class' => 'form-control', 'placeholder' => 'Email')) ?>
                                <?php echo $validator->showError("email", $translator->translate('validator.required')) ?>
                                <?php echo $validator->showError("email_unique", $translator->translate('validator.email_unique')) ?>
                            </div>
                            
                            <div class="col-sm-12">
                                <label class="control-label"><?php echo $translator->translate('New password') ?></label>
                                <input class="form-control" type="password" name="password" placeholder="<?php echo $translator->translate('Type new password') ?>" />
                                 <input class="form-control" type="password" name="password_confirm" placeholder="<?php echo $translator->translate('Retype new password') ?>" />
                                  <?php echo $validator->showError("confirm_password") ?>
                                <p class="help-block"><?php echo $translator->translate('Enter password only to change it') ?>.</p>
                            </div>
   
                          
                        
                       
                            <div class="col-sm-12">
                                <label class="control-label"><?php echo $translator->translate( $form->getFieldLabel('nickname')) ?></label>
                                <?php echo $form->renderInputTag('nickname', array('class' => 'form-control', 'placeholder' => $translator->translate( $form->getFieldLabel('nickname')))) ?>
                                <?php echo $validator->showError("nickname") ?>
                            </div>


                            <div class="col-sm-12">
                                <label class="control-label"><?php echo $translator->translate($form->getFieldLabel('name')) ?></label>
                                <?php echo $form->renderInputTag('name', array('class' => 'form-control', 'placeholder' => $translator->translate( $form->getFieldLabel('name')))) ?>
                                <?php echo $validator->showError("name", $translator->translate('validator.required')) ?>
                            </div>

                            <div class="col-sm-12">
                                <label class="control-label"><?php echo $translator->translate($form->getFieldLabel('surname')) ?></label>
                                <?php echo $form->renderInputTag('surname', array('class' => 'form-control')) ?>
                                <?php echo $validator->showError("surname", $translator->translate('validator.required')) ?>
                            </div>

                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label class="control-label"><?php echo $translator->translate('Birthdate') ?></label>
                                    </div>

                                    <div class="col-xs-4">
                                        <?php echo $form->renderSelectTag('birthyear', array('class' => 'form-control', 'placeholder' => $form->getFieldHelp('birthyear'))) ?>
                                    </div>
                                    <div class="col-xs-4">
                                        <?php echo $form->renderSelectTag('birthmonth', array('class' => 'form-control', 'placeholder' => $form->getFieldHelp('birthmonth'))) ?>
                                    </div>
                                    <div class="col-xs-4">
                                        <?php echo $form->renderSelectTag('birthday', array('class' => 'form-control', 'placeholder' => $form->getFieldHelp('birthday'))) ?>
                                    </div>
                                </div>
                            </div>

                     
                             <div class="col-sm-6">
                                <label class="control-label"><?php echo $translator->translate($form->getFieldLabel('weight') ) ?></label>
                                <?php echo $form->renderInputTag('weight', array('class' => 'form-control')) ?>
                                <?php echo $validator->showError("weight", $translator->translate('validator.required')) ?>
                            </div>

                            <div class="col-sm-6">
                                <label class="control-label"><?php echo $translator->translate($form->getFieldLabel('height')) ?></label>
                                <?php echo $form->renderInputTag('height', array('class' => 'form-control')) ?>
                                <?php echo $validator->showError("height", $translator->translate('validator.required')) ?>
                            </div>

                            <div class="col-sm-6">
                                <label class="control-label"><?php echo $translator->translate($form->getFieldLabel('phone') ) ?></label>
                                <?php echo $form->renderInputTag('phone', array('class' => 'form-control',)) ?>
                                <?php echo $validator->showError("phone", $translator->translate('validator.required')) ?>

                            </div>

                            <div class="col-sm-6">
                                <label class="control-label"><?php echo $translator->translate($form->getFieldLabel('default_lang') ) ?></label>
                                <?php echo $form->renderSelectTag('default_lang', array('class' => 'form-control')) ?>
                                <?php echo $validator->showError("default_lang", $translator->translate('validator.required')) ?>
                            </div>

                            <div class="col-sm-6">
                                <label class="control-label"><?php echo $translator->translate($form->getFieldLabel('gender') ) ?></label>
                                <?php echo $form->renderSelectTag('gender', array('class' => 'form-control',)) ?>
                                <?php echo $validator->showError("gender", $translator->translate('validator.required')) ?>

                            </div>
                            
                              <div class="col-sm-6 label-margin">

                            
      
                            </div>
                            

                            <div class="col-sm-12"><br /> 
                                <p><button type="submit" class="btn btn-primary btn-block "><?php echo $translator->translate('Save') ?></button></p>
                            </div>
                        </div>

                    </div>
                </div>
                
              
            </form>

