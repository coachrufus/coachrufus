



<?php if ($request->hasFlashMessage('user_edit_success')): ?>
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <?php echo $request->getFlashMessage('user_edit_success') ?>
    </div>
<?php endif; ?>


<section class="content-header">
    <h1>
        <?php echo $translator->translate('Profil') ?>
    </h1>
     <?php $layout->includePart(GLOBAL_DIR.'/templates/parts/_breadcrumb.php',array(
        'crumbs' => array(
           'Profil' => '',
            ))) ?>
</section>
<section class="content profil-page">
    <div class="row">
        <div class="col-md-4 col-lg-4 col-sm-12 profil-desk">
          <?php $layout->includePart(MODUL_DIR.'/Player/view/profil/_profil.php',array('form' => $form,'validator' => $validator )) ?>
        </div>


        <div class="col-md-8 col-lg-8 col-sm-12">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs profil-tabs">
                    
                    <li class="profil-tab"><a href="#profil" data-toggle="tab"><?php echo $translator->translate('Profil') ?></a></li>
                    
                    <li class="active"><a href="#locality" data-toggle="tab"><?php echo $translator->translate('Locality') ?></a></li>
    
                    <li><a href="#sports" data-toggle="tab"><?php echo $translator->translate('Sports') ?></a></li>
                    <li><a href="#notify" data-toggle="tab"><?php echo $translator->translate('Notifications') ?></a></li>
                    <!--
                    <li><a href="#subscriptions" data-toggle="tab"><?php echo $translator->translate('Subscriptions') ?></a></li>
                    <li><a href="#billing" data-toggle="tab"><?php echo $translator->translate('Billing History') ?></a></li>
                    -->
                </ul>
                <div class="tab-content">
                    <div class="tab-pane" id="profil">
                         <?php $layout->includePart(MODUL_DIR.'/Player/view/profil/_profil.php',array('form' => $form,'validator' => $validator )) ?>
                    </div>
                    <div class="tab-pane active" id="locality">
                         <?php $layout->includePart(MODUL_DIR.'/Player/view/profil/_locality.php',array('form' => $form,'validator' => $validator )) ?>
                    </div>
                   
                    <div class="tab-pane" id="sports">
                        <?php $layout->includePart(MODUL_DIR.'/Player/view/profil/_sports.php',array('playerSports' => $playerSports, 'form' => $form)) ?>
                    </div> 
                    <div class="tab-pane" id="notify">
                        <?php $layout->includePart(MODUL_DIR.'/Player/view/profil/_notify.php',array('form' => $form)) ?>
                    </div> 
                    <!--
                    <div class="tab-pane" id="subscriptions">
                        <?php echo $layout->renderControllerAction('Webteamer\Player\PlayerModule:Profil:subscriptionsList')->getContent() ?>
                    </div> 
                    <div class="tab-pane" id="billing">
                        <?php echo $layout->renderControllerAction('Webteamer\Player\PlayerModule:Profil:billingList')->getContent() ?>
                    </div> 
                    -->
                </div>
            </div>
        </div>

    </div>
</section>


<?php $layout->startSlot('javascript') ?>

<script src="/dev/plugins/fileupload/jquery.ui.widget.js"></script>
<script src="/dev/plugins/fileupload/jquery.iframe-transport.js"></script>
<script src="/dev/plugins/fileupload/jquery.fileupload.js"></script>
<script src="/dev/plugins/select2/select2.full.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXU1duK54f1gIsRy_xc7z4uQURovsZdE8&libraries=places&callback=initUserLocality" async defer></script>
<?php if(null == $form->getEntity()->getTimezone()): ?>
<?php $layout->addJavascript('js/jstz.js') ?>
 <script type="text/javascript">
    var tz = jstz.determine();
     $('#record_timezone').val(tz.name());
</script>
<?php endif; ?>

<script type="text/javascript">
    
<?php if ($request->hasFlashMessage('lang_changed')): ?>
   
    var messageOrigin;
    var messageSource;
    document.addEventListener('message', function (e) {
      messageOrigin = e.origin;
      messageSource = e.source;
    });
    
    var message = {
                'action': 'langChanged',
                'userLang': '<?php echo $request->getFlashMessage('lang_changed') ?>'
            };
            /*
      var message = {
            'userLang': '<?php echo $request->getFlashMessage('lang_changed') ?>'
        };
    */
       
       setTimeout(function(){ 
           window.postMessage(JSON.stringify(message) ,messageOrigin);
       }, 500);
       
       
    
     
     //console.log(message);
<?php endif ?>
    
   
function initUserLocality()
{
   
   player_locality_manager = new PlayerLocalityManager();
   player_locality_manager.google = google;
   
   <?php if (null == $form->getEntity()->getLocalityId()): ?>

/*
    navigator.geolocation.getCurrentPosition(foundLocation, noLocation);

    function foundLocation(position) 
    {
      var lat = position.coords.latitude;
      var lng = position.coords.longitude;
       player_locality_manager.initMap({lat: lat, lng: lng});
       player_locality_manager.createSearchInput();
      player_locality_manager.setNewLocation({lat: lat, lng: lng});


    };
    function noLocation() {
         player_locality_manager.createSearchInput();
    };
    */
   <?php else: ?>
        player_locality_manager.initMap({lat: <?php echo  $form->getEntity()->getLocality()->getLat();?>, lng: <?php  echo $form->getEntity()->getLocality()->getLng();?>});
         player_locality_manager.createSearchInput();
   <?php endif ?>
    
}
    
    
    
    
    $(".select2").select2();
    profilManager = new UserProfilManager();
    profilManager.init();

    $(function () {
        $('#fileupload').fileupload({
            dataType: 'json',
            done: function (e, data) {
                $.each(data.result.files, function (index, file) {
                    $('<p/>').text(file.name).appendTo(document.body);
                });
            },
            progress: function (e, data) {
                $('.upload-progress').show();
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('.upload-progress .progress-num').text((progress-1) + '%');
                $('.upload-progress .progress-bar').css('width',progress + '%');
                 $('.upload-error').remove();
            },
            done: function (e, data) {

                if(data.result.result == 'SUCCESS')
                {
                        console.log(data.result.file);
            $('#avatar_photo').css('background-image', 'url('+data.result.file+')');
                        $('#record_photo').val(data.result.file_name);
                        $('.upload-progress .progress-num').text('100%');
                        $('.upload-progress').hide();
                }

                if(data.result.result == 'ERROR')
                {
                    $('.upload-progress').hide();        
                    $('.upload-progress').before('<div class="alert alert-error upload-error">'+data.result.error_message +'</div>');
                }
           
            }
        });
    });
    
    <?php if ($request->hasFlashMessage('payment_fail')): ?>
        <?php echo $request->getFlashMessage('payment_fail'); ?>
        $('#subscription-fail-modal').modal('show');
    <?php endif; ?>
    
 
// Change hash for page-reload
/*
$('.nav-tabs a').on('shown', function (e) {
    window.location.hash = e.target.hash.replace("#", "#" + prefix);
});
*/
var url = document.location.toString();
if (url.match('#')) {
    $('.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
} 

// Change hash for page-reload
$('.nav-tabs a').on('shown.bs.tab', function (e) {
    /*
        e.preventDefault();
    window.location.hash = e.target.hash;
    
    $('html, body').animate({
        scrollTop: $(".nav-tabs-custom").offset().top-50
    }, 100);
    */
    
})

  if(window.location.hash != "") {
      $('a[href="' + window.location.hash + '"]').click()
  }

</script>

<?php $layout->endSlot('javascript') ?>