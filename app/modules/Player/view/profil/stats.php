
<?php Core\Layout::getInstance()->startSlot('stylesheet') ?>  
<link rel="stylesheet" href="/dev/plugins/datatables/dataTables.bootstrap.css">
<?php Core\Layout::getInstance()->endSlot('stylesheet') ?>


<section class="content-header">
    <h1>
        <?php echo $translator->translate('Stats') ?>
    </h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
              
                <div class="box-body">
 
                  
                    
                    <table class="table table-bordered" id="stat_table">
                        <thead>
                            <tr>
                                <th></th>
                                <th><?php echo $translator->translate('Date') ?></th>
                                <th><?php echo $translator->translate('Team') ?></th>
                                <th><?php echo $translator->translate('Lineup') ?></th>
                                <th><?php echo $translator->translate('Result') ?></th>
                                <th><?php echo $translator->translate('Goals') ?></th>
                                <th><?php echo $translator->translate('Assists') ?></th>
                               
                            </tr>
                        </thead>
                        <tbody>
                    <?php foreach ($matchOverview as $match): ?>
                   <tr class="<?php echo $resultClass[$match->getResultStatus()] ?>">
                       <td>
                           <?php if('draw' == $match->getResultStatus()): ?>
                                 <span class="label label-info"><?php echo $translator->translate('Draw') ?></span>
                                <?php endif; ?>
                                <?php if('win' == $match->getResultStatus()): ?>
                                                    <span class="label label-success"><?php echo $translator->translate('Win') ?></span>
                                <?php endif; ?>
                                <?php if('loose' == $match->getResultStatus()): ?>
                                 <span class="label label-danger"><?php echo $translator->translate('Loose') ?></span>
                                <?php endif; ?>
                                
                           
                       </td>
                       <td>
                           <a class="view_match_overview" href="<?php echo $router->link('player_match_detail',array('player' => $player->getId(), 'l' => $match->getLineupId() )) ?>"><?php echo  $match->getEventDate() ?></a>
                                
                                
                            </td>
                            <td><?php echo  $match->getTeamName() ?></td>
                            <td><?php echo  $match->getLineupName() ?></td>
                            <td><?php echo  $match->getResult() ?></td>
                            <td><?php echo $match->getGoals() ?></td>
                            <td><?php echo $match->getAssists() ?></td>
                        </tr>
                        
                       

                    <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
     
    </div>
</section>


 <?php $layout->includePart(MODUL_DIR . '/Player/view/profil/_match_overview_modal.php') ?>


<?php $layout->startSlot('javascript') ?>
<script src="/dev/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/dev/plugins/datatables/dataTables.bootstrap.min.js"></script>
 <script>
      $(function () {
        $("#stat_table").DataTable({
             "paging": false
        });
        
        $('.view_match_overview').on('click',function(e){
            e.preventDefault();
            $('#match-overview-frame').attr('src',$(this).attr('href'));
            $('#match-overview-modal').modal('show');
        });
       
      });
      
      
      
    </script>
  
<?php $layout->endSlot('javascript') ?>
