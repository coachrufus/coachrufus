
<a name="sports"></a>
<form class="form-horizontal" action="#sports"  method="post" id="sport-form">
   
     <input type="hidden" name="form_action" value="sports-data" />
    <div class="well well-sm">
         <strong><?php echo $translator->translate('profil.addNewSport') ?></strong>
    <div class="row">
        <div class="col-sm-6">
            <?php echo $form->renderSelectTag('sport_id', array('class' => 'form-control select2', 'id' => 'sport_selector', 'style' => 'width:100%; margin:10px 0;')) ?>
        </div>
        
         <div class="col-sm-4">
            <?php echo $form->renderSelectTag('sport_level', array('class' => 'form-control select2', 'id' => 'sport_level_template', 'style' => 'width:100%; margin:10px 0;')) ?>
        </div>
        <div class="col-sm-2">
            <button id="add_player_sport" class="btn btn-success btn-flat" type="button"><?php echo $translator->translate('Add it!') ?></button>
        </div>
    </div>
    </div>

        <h3><?php echo $translator->translate('My sports') ?></h3>
        <div id="player_sports">
    <?php foreach ($playerSports as $playerSport): ?>
    <div class="row sport-row"  id="sport_item_<?php echo $playerSport->getSportId() ?>">
         <div class="col-xs-10">
            <strong class="sport-name"><?php echo $translator->translate($playerSport->getSportName()) ?></strong>
             <?php echo $form->renderSelectTag('sport_level', array('class' => 'form-control', 'name' => 'sports[' . $playerSport->getSportId() . '][level]', 'value' => $playerSport->getLevel())) ?>
        </div>
        
         <div class="col-xs-2">
            <a class="player_sport_remove" href=""><i class="ico ico-trash"></i></a>
             <input type="hidden" name="sports[<?php echo $playerSport->getSportId() ?>][sport]" value="<?php echo $playerSport->getSportId() ?>">
        </div>
        
    </div>
    <?php endforeach; ?>
        </div>
    <button type="submit" class="btn btn-danger save-changes"><?php echo $translator->translate('Submit') ?></button>
   
</form>
