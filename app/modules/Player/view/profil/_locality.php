<form class="form-horizontal" action="#tab_locality"  method="post">
     <input type="hidden" name="form_action" value="locality-data" />
<div class="row">
    <div class="col-md-12 col-lg-6">
        <div class="well" >
        <input type="hidden" name="locality[locality_json_data]" value="" id="locality_json_data" />
        <input type="hidden" name="locality[locality_id]" value="<?php echo $form->getEntity()->getLocalityId() ?>" id="locality_id" />
        <input type="hidden" name="locality[name]" value="" id="locality_name" />
        <input type="hidden" name="locality[city]" value="" id="locality_city" />
        <input type="hidden" name="locality[street]" value="" id="locality_street" />
        <input type="hidden" name="locality[street_number]" value="" id="locality_street_number" />

        <?php if (null == $form->getEntity()->getLocalityId()): ?>
        <div class="text-center">
       
         <div class="alert alert-danger"><?php echo $translator->translate('You have no location selected') ?></div>
            <label><?php echo $translator->translate('find your location') ?> </label> 
            <input style="box-shadow: none; border:1px solid gray; font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif" id="pac-input" class="controls form-control" type="text" placeholder="Názov obce, ulica atd ">
            
            
            
            
            <label><?php echo $translator->translate('or accept') ?></label>
            <div id="map_wrap" style="height:200px;">
                <div id="map" style="left:0; height:200px" >
                    <?php echo $translator->translate('You have disabled access to browser geolocation and we cant find your current position') ?><br />
                    <a class="popup-link" href="http://waziggle.com/BrowserAllow.aspx"><?php echo $translator->translate('Read how to enable it') ?></a> <?php echo $translator->translate('or type city, street etc in search input') ?>
                   
                </div>
            </div>
           
        </div>
        <?php else: ?>
             <div id="map_wrap" style="height:200px;">
                <div id="map" style="left:0; height:200px;"></div>
            </div>
            <?php echo $form->renderInputTag('address', array('class' => 'form-control','id' => 'pac-input','style' => 'box-shadow: none; border:1px solid gray; font-family: "Source Sans Pro","Helvetica Neue",Helvetica,Arial,sans-serif', 'placeholder' => 'Názov obce, ulica atd')) ?>
           
            
        <?php endif; ?>
        </div>
    </div>
    
    <div class="col-md-12 col-lg-6">
        <div class="row">
            <div class="col-sm-12">
                <label class="control-label"><?php echo  $translator->translate($form->getFieldLabel('timezone')) ?></label>
                <?php echo $form->renderSelectTag('timezone', array('class' => 'form-control select2')) ?>
                <?php echo $validator->showError("timezone", $translator->translate('validator.required')) ?>
            </div>

            <div class="col-sm-6">
                <label class="control-label"><?php echo $translator->translate($form->getFieldLabel('unitsystem')) ?></label>
                <?php echo $form->renderSelectTag('unitsystem', array('class' => 'form-control')) ?>
                <?php echo $validator->showError("unitsystem", $translator->translate('validator.required')) ?>
            </div>

            <div class="col-sm-6">
                <label class="control-label"><?php echo $translator->translate($form->getFieldLabel('timeformat')) ?></label>
                <?php echo $form->renderSelectTag('timeformat', array('class' => 'form-control')) ?>
                <?php echo $validator->showError("timeformat", $translator->translate('validator.required')) ?>
            </div>
            <div class="col-sm-6">
                <label class="control-label"><?php echo $translator->translate($form->getFieldLabel('dateformat')) ?></label>
                <?php echo $form->renderSelectTag('dateformat', array('class' => 'form-control',)) ?>
                <?php echo $validator->showError("dateformat", $translator->translate('validator.required')) ?>

            </div>
            <div class="col-sm-6">
                <label class="control-label"><?php echo $translator->translate($form->getFieldLabel('week_start')) ?></label>
                <?php echo $form->renderSelectTag('week_start', array('class' => 'form-control')) ?>
                <?php echo $validator->showError("week_start", $translator->translate('validator.required')) ?>
            </div>
        </div>
    </div>
</div>

     
      <div class="form-group">
        <div class="col-sm-offset-7 col-sm-2">
            <button type="submit" class="btn btn-danger"><?php echo $translator->translate('Submit') ?></button>
        </div>
    </div>
</form>


