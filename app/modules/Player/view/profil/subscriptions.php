<?php Core\Layout::getInstance()->startSlot('stylesheet') ?>  
<link rel="stylesheet" href="/dev/plugins/datatables/dataTables.bootstrap.css">
<?php Core\Layout::getInstance()->endSlot('stylesheet') ?>


<?php if ($request->hasFlashMessage('payment_success')): ?>
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <?php echo $request->getFlashMessage('payment_success') ?>
    </div>
<?php endif; ?>

<?php if ($request->hasFlashMessage('subscription_cancel_success')): ?>
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <?php echo $request->getFlashMessage('subscription_cancel_success') ?>
    </div>
<?php endif; ?>

<h3><?php echo $translator->translate('Subscriptions') ?></h3>
<div class="row order_table_wrap">
    <div class="col-sm-12">

<div class="row order_table_header order-table-row" id="order_table">


   <div class="col-sm-3 order_table_cell"><?php echo $translator->translate('TEAM') ?></div>
   <div class="col-sm-4 order_table_cell"><?php echo $translator->translate('Subscription') ?></div>
    <div class="col-sm-2 order_table_cell"><?php echo $translator->translate('SUBSCRIPTION_VALID_TO') ?></div>
   <div class="col-sm-3 order_table_cell"><?php echo $translator->translate('SUBSCRIPTION_RENEW_DATE') ?></div>
</div>

<?php foreach($orders as $order): ?>
    
    <div class="subscription-change-row row order-table-row">
        <div class="subscription-change-desc col-sm-3 order_table_cell">
            <strong>PRO</strong> <br />
            <?php echo $order->getTeamName()  ?><br />
            <?php echo $order->getTeamId() ?>
        </div>
        <div class="subscription-change-btn-wrap col-sm-4 order_table_cell" >
           
            
            <?php if($order->isChangable()): ?>
            <div class="subscription-change-btn-row">
                <button data-variant="12" data-tid="<?php echo $order->getPaymentId() ?>" data-oid="<?php echo $order->getid()  ?>" class="subscription-change-trigger <?php echo ($order->getVariant() == 12) ? 'active' : '' ?>" type="button"></button>
                <?php echo $prices['year'] ?> &euro; <?php echo $translator->translate('SUBSCRIPTION_YEARLY') ?>/<?php echo $translator->translate('team') ?>
                <span class="subscription-save"><?php echo $translator->translate('Save 40%') ?></span>
            </div>  
                    
            <div class="subscription-change-btn-row">
                <button data-variant="1"  data-tid="<?php echo $order->getPaymentId() ?>" data-oid="<?php echo $order->getid()  ?>" class="subscription-change-trigger <?php echo ($order->getVariant() == 1) ? 'active' : '' ?>" type="button"></button>
                <?php echo $prices['month'] ?> &euro; <?php echo $translator->translate('SUBSCRIPTION_MONTHLY') ?>/<?php echo $translator->translate('team') ?>
             </div>  
            <?php else: ?>
            <div class="subscription-change-btn-row">
                <button  class="<?php echo ($order->getVariant() == 12) ? 'active' : 'subscription-disabled-trigger' ?>" type="button"></button>
                <?php echo $prices['year'] ?> &euro; <?php echo $translator->translate('SUBSCRIPTION_YEARLY') ?>/<?php echo $translator->translate('team') ?>
                <span class="subscription-save"><?php echo $translator->translate('Save 40%') ?></span>
            </div>  
                    
            <div class="subscription-change-btn-row">
                <button  class="<?php echo ($order->getVariant() == 1) ? 'active' : 'subscription-disabled-trigger' ?>" type="button"></button>
                <?php echo $prices['month'] ?> &euro; <?php echo $translator->translate('SUBSCRIPTION_MONTHLY') ?>/<?php echo $translator->translate('team') ?>
             </div>  
            <?php endif; ?>
            
        </div>
        <div class="col-sm-2 order_table_cell">
            <strong class="l_760"><?php echo $translator->translate('SUBSCRIPTION_VALID_TO') ?></strong>
            <?php echo $order->getFormatedValidTo()  ?>
        </div>
        <div class="col-sm-3 order_table_cell">
           
             <strong class="l_760"><?php echo $translator->translate('SUBSCRIPTION_RENEW_DATE') ?></strong>
          
            
            <?php if($order->getRenewPayment() == 1): ?>
                <?php echo $order->getFormatedRenewDate()  ?> 
            <a class="subscription-cancel-trigger" data-title="PRO<br /> <?php echo $order->getTeamName()  ?> <?php echo $order->getTeamId() ?>" data-valid-to="<?php echo $translator->translate('Subscription Valid to').' '.$order->getFormatedValidTo()  ?>" href="<?php echo $router->link('gopay_cancel_subscription',array('tid' => $order->getPaymentId() )) ?>"><?php echo $translator->translate('SUBSCRIPTION_CANCEL') ?></a>
                    
                  
                    
            <?php endif; ?>
                
            <?php if($order->getStatus() == 'canceled'): ?>
               <?php echo $translator->translate('Canceled') ?>
                    <?php if(date('Y-m-d') == $order->getValidTo()->format('Y-m-d') ): ?>
                         <a class="team_credit_modal_trigger" href="#"><?php echo $translator->translate('SUBSCRIPTION_REACTIVATE') ?></a>
                    <?php endif; ?>
            <?php endif; ?>
            
           
        </div>
       

    </div>
<?php endforeach; ?> 

    </div>
</div>

<?php $layout->includePart(MODUL_DIR . '/Pay/view/Pay/_subscription_change_modal.php',array('prices' => $prices)) ?>
<?php $layout->includePart(MODUL_DIR . '/Pay/view/Pay/_subscription_disabled_modal.php',array('prices' => $prices)) ?>
<?php $layout->includePart(MODUL_DIR . '/Pay/view/Pay/_subscription_cancel_modal.php') ?>
 <?php $layout->includePart(MODUL_DIR . '/Pay/view/Pay/_subscription_fail_modal.php',array('failPaymentData' => $failPaymentData)) ?>

