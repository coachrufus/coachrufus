<div class="modal fade" id="match-overview-modal" tabindex="-1" role="dialog" aria-labelledby="<?php echo $translator->translate('Match overview') ?>">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $translator->translate('Match overview') ?></h4>
      </div>
      <div class="modal-body">
         
         
          <iframe id="match-overview-frame" style="width:100%; height:500px; border:0;" src="" ></iframe>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $translator->translate('Close') ?></button>
      </div>
    </div>
  </div>
</div>
