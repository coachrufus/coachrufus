<?php Core\Layout::getInstance()->startSlot('stylesheet') ?>  
<link rel="stylesheet" href="/dev/plugins/datatables/dataTables.bootstrap.css">
<?php Core\Layout::getInstance()->endSlot('stylesheet') ?>


<?php if ($request->hasFlashMessage('payment_success')): ?>
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <?php echo $request->getFlashMessage('payment_success') ?>
    </div>
<?php endif; ?>

<h3><?php echo $translator->translate('Billing History') ?></h3>
<div class="row order_table_wrap">
    <div class="col-sm-12">
<div class="row order_table_header order-table-row">
    
    <div class="col-sm-2 order_table_cell"><?php echo $translator->translate('Date') ?></div>
    <div class="col-sm-3 col-lg-2 order_table_cell"><?php echo $translator->translate('Invoice number') ?></div>
    <div class="col-sm-2 order_table_cell"><?php echo $translator->translate('Transaction') ?></div>
    <div class="col-sm-3 col-lg-4 order_table_cell"><?php echo $translator->translate('Description (Team Name,ID,Product,Date )') ?></div>
    <div class="col-sm-2 order_table_cell"><?php echo $translator->translate('Amount') ?></div>
    
</div>

<?php foreach($invoices as $invoice): ?>
      <div class="row order-table-row">
        <div class="col-sm-2 order_table_cell"> <strong class="l_760"><?php echo $translator->translate('Date') ?></strong><?php echo $invoice->getInvoiceDateFormated()  ?></div>
        <div class="col-sm-3 col-lg-2 order_table_cell"> <strong class="l_760"><?php echo $translator->translate('Invoice number') ?></strong><?php echo $invoice->getInvoiceNo()  ?></div>
         <div class="col-sm-2 order_table_cell"> <strong class="l_760"><?php echo $translator->translate('Transaction') ?></strong><?php echo $invoice->getTransactionId()  ?></div>
        <div class="col-sm-3 col-lg-4 order_table_cell"> <strong class="l_760"><?php echo $translator->translate('Description (Team Name,ID,Product,Date )') ?></strong><?php echo  $invoice->getFormatedDescription() ?></div>
        <div class="col-sm-2 order_table_cell">
            <strong class="l_760"><?php echo $translator->translate('Amount') ?></strong>
            <?php echo $invoice->getAmount() ?> <?php echo  $invoice->getCurrency() ?>
            <a class="popup-link" href="<?php echo $router->link('invoice_pdf',array('order_no' => $invoice->getInvoiceNo() )) ?>"><?php echo $translator->translate('Print reciept') ?></a>
       </div>

    </div>
<?php endforeach; ?> 
    </div>
</div>


<?php $layout->startSlot('javascript') ?>
<script src="/dev/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/dev/plugins/datatables/dataTables.bootstrap.min.js"></script>
 <script>
      $(function () {
        $("#order_table").DataTable({
             "paging": false
        });
        
       
       
      });
      
      
      
    </script>
  
<?php $layout->endSlot('javascript') ?>