<form class="form-horizontal" action="#notify"  method="post">
    <input type="hidden" name="form_action" value="notify-data" />

    <div class="col-sm-12 label-margin">
        <div class="form-group">
            <?php echo $form->renderCheckboxTag('receive_notifications', array('class' => 'form-control')) ?>

            <label class="control-label"><?php echo $translator->translate($form->getFieldLabel('receive_notifications')) ?></label>
            <div><br /><?php echo $translator->translate('DISABLE_NOTIFY_ALERT') ?></div>
        </div>
    </div>                   

    <div class="form-group">
        <div class="col-sm-12">
            <button type="submit" class="btn btn-danger"><?php echo $translator->translate('Submit') ?></button>
        </div>
    </div>
</form>

