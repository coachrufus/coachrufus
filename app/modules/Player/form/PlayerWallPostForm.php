<?php
namespace  CR\Player\Form;
use \Core\Form as Form;
use  CR\Player\Model\PlayerWallPost as PlayerWallPost;

class PlayerWallPostForm extends Form 
{
	protected $fields = array (
  
  
 
  'body' => 
  array (
    'type' => 'string',
    'max_length' => '500',
    'required' => true,
  ),
);
				
	public function __construct() 
	{

	}
}