<?php

namespace Webteamer\Player\Form;

use Core\ServiceLayer;
use Core\Validator;

class ProfilFormValidator extends Validator {

    public function validateData()
    {
        parent::validateData();
        
        //validate unique email
        
        
        
    }
    
    public function validateUniqueEmail($repository,$email,$userId)
    {
        $existEmail = $repository->findOneBy(array('email' => $email));
        
        if(null != $existEmail && $existEmail->getId() != $userId )
            {
               $mesage = 'Zadaný email už existuje';
               $this->addErrorMessage('email_unique',$mesage);	
        }
    }

}
