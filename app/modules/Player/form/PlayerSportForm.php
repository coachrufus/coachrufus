<?php
namespace  Webteamer\Player\Form;
use \Core\Form as Form;
use  Webteamer\Player\Model\PlayerSport as PlayerSport;

class PlayerSportForm extends Form 
{
	protected $fields = array (
  'id' => 
  array (
    'type' => 'int',
    'max_length' => '11',
    'required' => true,
  ),
  'sport_id' => 
  array (
    'type' => 'int',
    'max_length' => '11',
    'required' => false,
  ),
  'player_id' => 
  array (
    'type' => 'int',
    'max_length' => '11',
    'required' => false,
  ),
  'level' => 
  array (
    'type' => 'string',
    'max_length' => '50',
    'required' => false,
  ),
  'often' => 
  array (
    'type' => 'string',
    'max_length' => '50',
    'required' => false,
  ),
  'availability' => 
  array (
    'type' => 'string',
    'max_length' => '50',
    'required' => false,
  ),
  'interest' => 
  array (
    'type' => 'string',
    'max_length' => '50',
    'required' => false,
  ),
  'note' => 
  array (
    'type' => 'string',
    'max_length' => '',
    'required' => false,
  ),
  'involved_type' => 
  array (
    'type' => 'string',
    'max_length' => '255',
    'required' => false,
  ),
);
				
	public function __construct() 
	{

	}
}