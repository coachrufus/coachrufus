<?php
namespace  Webteamer\Player\Form;
use \Core\Form as Form;
use Core\ServiceLayer;
use User\Model\User as User;

class ProfilForm extends \User\Form\RegisterForm {


	public function __construct()
	{
                
		$this->fields['receive_notifications']['choices'] =  array('2' => 'Nie', '1' => 'Áno');
                 $this->setField('gender', array(
                    'type' => 'choice',
                    'choices' => array(),
                    'required' => true,
                ));
                 $this->setField('sport_id', array(
                    'type' => 'choice',
                    'choices' => array(),
                    'required' => true,
                ));
                 $this->setField('sport_level', array(
                    'type' => 'choice',
                    'choices' => array(),
                    'required' => true,
                ));
                
              
                 $this->setField('level', array(
                    'type' => 'choice',
                    'choices' => array(),
                    'required' => true,
                ));
                 $this->setField('player_status', array(
                    'type' => 'choice',
                    'choices' => array(),
                    'required' => true,
                ));
                
                 $this->setField('default_lang', array(
                    'type' => 'choice',
                    'choices' =>  array(),
                    'required' => true,
                ));
        
                 $this->setField('locality', array(
                    'type' => 'string',
                    'required' => true,
                ));
                 
                 $this->setField('nickname', array(
                     'type' => 'string',
                    'max_length' => '128',
                    'required' => false              ));
                 
                 $this->setField('address', array(
                     'type' => 'string',
                    'max_length' => '256',
                    'required' => false              ));
                 
                 
                 $this->setField('surname', array(
                    'type' => 'string',
                    'max_length' => '128',
                    'required' => false     ));
                 
                
                 
                 
             
                
                 
                 $day_choices = range(1, 31);
                $day_choices = array_combine($day_choices, $day_choices);
                $this->setFieldChoices('birthday', $day_choices);

                $month_choices = range(1, 12);
                $month_choices = array_combine($month_choices, $month_choices);
                $this->setFieldChoices('birthmonth', $month_choices);

                $year_choices = range(date('Y')-99, date('Y')-5);
                $year_choices = array_reverse(array_combine($year_choices, $year_choices),true);
                $this->setFieldChoices('birthyear', $year_choices);

                $this->setFieldChoices('timezone', array('' => '')+$this->getTimezoneChoices());
        
		
	}
        
	
	
		
	
	
		
}