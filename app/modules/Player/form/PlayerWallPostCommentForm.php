<?php

namespace CR\Player\Form;

use \Core\Form as Form;
use CR\Player\Model\PlayerWallPostComment as PlayerWallPostComment;

class PlayerWallPostCommentForm extends Form {

    protected $fields = array(
        'body' =>
        array(
            'type' => 'string',
            'max_length' => '300',
            'required' => false,
        ),
    );

    public function __construct()
    {
        
    }

}
