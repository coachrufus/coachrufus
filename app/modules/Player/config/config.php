<?php
namespace Webteamer\Player;
require_once(MODUL_DIR.'/Player/PlayerModule.php');

use Core\DbStorage;
use Core\EntityMapper;
use Core\ServiceLayer as ServiceLayer;
use Webteamer\Player\Model\PlayerMatchStorage;
use Webteamer\Player\Model\PlayerScoreStorage;
use Webteamer\Player\Model\PlayerSportStorage;
use Webteamer\Player\Model\PlayerStorage;



$playerLocalityMapper =  new EntityMapper('\Webteamer\Player\Model\Player');
$playerLocalityMapper->addAssocEntity(array(
        'class' => '\Webteamer\Locality\Model\Locality' , 
        'method' => 'setLocality',
        'mapper' => '\Core\EntityMapper'));
$playerStorage = new PlayerStorage('co_users');
$playerStorage->addAssocTable('a_','locality');

ServiceLayer::addService('PlayerRepository',array(
'class' => "\Webteamer\Player\Model\PlayerRepository",
	'params' => array(
		$playerStorage,
		$playerLocalityMapper
	)
));


$playerSportMapper =  new EntityMapper('\Webteamer\Player\Model\PlayerSport');
$playerSportMapper->addAssocEntity(array(
        'class' => '\Webteamer\Sport\Model\Sport' , 
        'method' => 'setSport',
        'mapper' => '\Core\EntityMapper'));
$playerSportStorage = new PlayerSportStorage('player_sport');
$playerSportStorage->addAssocTable('a_','sport');

ServiceLayer::addService('PlayerSportRepository',array(
'class' => "\Webteamer\Player\Model\PlayerSportRepository",
	'params' => array(
		$playerSportStorage,
                $playerSportMapper,
	)
));

ServiceLayer::addService('PlayerScoreRepository',array(
'class' => "Webteamer\Player\Model\PlayerScoreRepository",
	'params' => array(
		new  PlayerScoreStorage('player_score'),
		new EntityMapper('Webteamer\Player\Model\PlayerScore'),
	)
));

ServiceLayer::addService('PlayerPrivacyRepository',array(
'class' => "Webteamer\Player\Model\PlayerPrivacyRepository",
	'params' => array(
		new  DbStorage('player_privacy'),
		new EntityMapper('Webteamer\Player\Model\PlayerPrivacy'),
	)
));

ServiceLayer::addService('PlayerTimelineEventRepository',array(
'class' => "Webteamer\Player\Model\PlayerTimelineEventRepository",
	'params' => array(
		new Model\PlayerTimelineEventStorage('player_timeline_event'),
		new \Core\EntityMapper('Webteamer\Player\Model\PlayerTimelineEvent')
	)
));

ServiceLayer::addService('PlayerStatManager',array(
	'class' => "Webteamer\Player\Model\PlayerStatManager",
	'params' => [
		ServiceLayer::getService('PlayerScoreRepository'),
		ServiceLayer::getService('PlayerTimelineEventRepository'),
		ServiceLayer::getService('ActivityLogger')
	]
));

ServiceLayer::addService('PlayerStatSetManager',array(
	'class' => "Webteamer\Player\Model\PlayerStatSetManager",
	'params' => [
		ServiceLayer::getService('PlayerTimelineEventRepository')
	]
));


ServiceLayer::addService('PlayerManager',array(
'class' => "Webteamer\Player\Model\PlayerManager",
    'params' => array(
        ServiceLayer::getService('PlayerRepository'),
        ServiceLayer::getService('PlayerSportRepository'),
        ServiceLayer::getService('PlayerPrivacyRepository'),
    )
));

ServiceLayer::addService('PlayerWallPostRepository',array(
'class' => "CR\Player\Model\PlayerWallPostRepository",
	'params' => array(
                new \CR\Player\Model\PlayerWallStorage('player_wall_post'),
		new \Core\EntityMapper('CR\Player\Model\PlayerWallPost')
	)
));

ServiceLayer::addService('PlayerWallPostCommentRepository',array(
'class' => "CR\Player\Model\PlayerWallPostCommentRepository",
	'params' => array(
                new \CR\Player\Model\PlayerWallPostCommentStorage('player_wall_post_comment'),
		new \Core\EntityMapper('CR\Player\Model\PlayerWallPostComment')
	)
));


ServiceLayer::addService('PlayerWallManager',array(
'class' => "CR\Player\Model\PlayerWallManager",
    'params' => array(
        ServiceLayer::getService('PlayerWallPostRepository'),
        ServiceLayer::getService('PlayerWallPostCommentRepository'),
       
    )
));


ServiceLayer::addService('PlayerFollowRepository',array(
'class' => "CR\Player\Model\PlayerFollowRepository",
	'params' => array(
		new  \CR\Player\Model\PlayerFollowStorage('player_follow')
	)
));

ServiceLayer::addService('PlayerFollowManager',array(
'class' => "CR\Player\Model\PlayerFollowManager",
    'params' => array(
		ServiceLayer::getService('PlayerFollowRepository'),
	)
));


ServiceLayer::addService('PlayerFriendRepository',array(
'class' => "CR\Player\Model\PlayerFriendRepository",
	'params' => array(
		new  \CR\Player\Model\PlayerFriendStorage('player_friends'),
		new \Core\EntityMapper('CR\Player\Model\PlayerFriend')
	)
));

ServiceLayer::addService('PlayerFriendManager',array(
'class' => "CR\Player\Model\PlayerFriendManager",
    'params' => array(
		ServiceLayer::getService('PlayerFriendRepository'),
	)
));




$acl = ServiceLayer::getService('acl');




//routing
$router = ServiceLayer::getService('router');


$acl->allowRole('ROLE_USER','player_dashboard');
$router->addRoute('player_dashboard','/player/dashboard/:userid',array(
		'controller' => 'Webteamer\Player\PlayerModule:Dashboard:index'
));

$acl->allowRole('ROLE_USER','player_start');
$router->addRoute('player_start','/player/start/:userid',array(
		'controller' => 'Webteamer\Player\PlayerModule:Dashboard:index'
));

$acl->allowRole('ROLE_USER','player_search');
$router->addRoute('player_search','/player/search',array(
		'controller' => 'Webteamer\Player\PlayerModule:Ajax:search'
));

$acl->allowRole('ROLE_USER','add_player_rating');
$router->addRoute('add_player_rating','/player/add-rating',array(
		'controller' => 'Webteamer\Player\PlayerModule:Ajax:addRating'
));

$acl->allowRole('ROLE_USER','player_interest');
$router->addRoute('player_interest','/player/interest',array(
		'controller' => 'Webteamer\Player\PlayerModule:Profil:interest'
));



$acl->allowRole('ROLE_USER','profil_upload_photo');
$router->addRoute('profil_upload_photo','/profil/upload-photo',array(
		'controller' => '\Webteamer\Player\PlayerModule:Profil:uploadPhoto'
));






$acl->allowRole(array('guest'),'player_stats');
$router->addRoute('player_stats','/profil/stats',array(
		'controller' => 'Webteamer\Player\PlayerModule:Profil:stats'
));

$acl->allowRole(array('guest'),'player_match_detail');
$router->addRoute('player_match_detail','/profil/match_detail',array(
		'controller' => 'Webteamer\Player\PlayerModule:Profil:matchDetail'
));



$acl->allowRole(array('ROLE_USER'),'player_public_profile');
$router->addRoute('player_public_profile','/profil/:player_id',array(
		'controller' => 'Webteamer\Player\PlayerModule:Public:index'
));

$acl->allowRole(array('ROLE_USER'),'player_public_follow');
$router->addRoute('player_public_follow','/player/follow/:id',array(
		'controller' => 'Webteamer\Player\PlayerModule:Public:follow'
));

$acl->allowRole(array('ROLE_USER'),'player_public_unfollow');
$router->addRoute('player_public_unfollow','/player/unfollow/:id',array(
		'controller' => 'Webteamer\Player\PlayerModule:Public:unfollow'
));

$acl->allowRole(array('ROLE_USER'),'create_player_wall_post');
$router->addRoute('create_player_wall_post','/player/create_wall_post',array(
		'controller' => 'Webteamer\Player\PlayerModule:Ajax:createWallPost'
));

$acl->allowRole(array('ROLE_USER'),'delete_player_wall_post');
$router->addRoute('delete_player_wall_post','/player/delete_wall_post',array(
		'controller' => 'Webteamer\Player\PlayerModule:Ajax:deleteWallPost'
));

$acl->allowRole(array('ROLE_USER'),'create_player_wall_post_comment');
$router->addRoute('create_player_wall_post_comment','/player/create-wall-post-comment',array(
		'controller' => 'Webteamer\Player\PlayerModule:Ajax:createWallPostComment'
));

$acl->allowRole(array('ROLE_USER'),'get_player_wall_post_comments');
$router->addRoute('get_player_wall_post_comments','/player/get-wall-post-comments',array(
		'controller' => 'Webteamer\Player\PlayerModule:Ajax:getWallPostComments'
));

$acl->allowRole(array('ROLE_USER'),'player_mentions');
$router->addRoute('player_mentions','/player/mentions',array(
		'controller' => 'Webteamer\Player\PlayerModule:Ajax:getPlayerMentions'
));


$acl->allowRole(array('ROLE_USER'),'player_add_friend');
$router->addRoute('player_add_friend','/player/add-friend',array(
		'controller' => 'Webteamer\Player\PlayerModule:Friends:addFriend'
));

$acl->allowRole(array('ROLE_USER'),'player_confirm_friend');
$router->addRoute('player_confirm_friend','/player/confirm-friend',array(
		'controller' => 'Webteamer\Player\PlayerModule:Friends:confirmFriend'
));

$acl->allowRole(array('ROLE_USER'),'player_remove_friend');
$router->addRoute('player_remove_friend','/player/remove-friend',array(
		'controller' => 'Webteamer\Player\PlayerModule:Friends:removeFriend'
));


$acl->allowRole(array('ROLE_USER'),'user_update_layout_settings');
$router->addRoute('user_update_layout_settings','/player/update-layout-settings',array(
		'controller' => 'Webteamer\Player\PlayerModule:Profil:updateLayoutSettings'
));


$acl->allowRole('guest','player_icon');
$router->addRoute('player_icon','/player/icon',array(
		'controller' => 'Webteamer\Player\PlayerModule:Profil:icon'
));







