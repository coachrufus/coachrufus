<?php

namespace Webteamer\Player\Controller;

use Core\Controller as Controller;
use Core\ServiceLayer;
use Core\Validator;
use CR\Player\Form\PlayerWallPostForm;
use CR\Player\Model\PlayerWallPost;
use DateTime;

class FriendsController extends Controller {

   
    
     public function addFriendAction()
     {
         $request = ServiceLayer::getService('request');
         $FriendManager = ServiceLayer::getService('PlayerFriendManager');
         $playerManager = ServiceLayer::getService('PlayerManager');
         $security = ServiceLayer::getService('security');
         $user = $security->getIdentity()->getUser();
         $player = $playerManager->findPlayerById($request->get('id'));

         $FriendManager->createFriend($player,$user);
         $request->redirect($this->getRouter()->link('player_public_profile',array('player_id' => $player->getId())));
     }
     
     public function confirmFriendAction()
     {
         $request = ServiceLayer::getService('request');
         $security = ServiceLayer::getService('security');
         $friendManager = ServiceLayer::getService('PlayerFriendManager');
         $user = $security->getIdentity()->getUser();
         $friend = $friendManager->getPlayerFriendById($user,$request->get('f'));
         
         if(null != $friend)
         {
             $friendManager->confirmFriend($friend);
         }
         
         $request->redirect();
     }
     
     public function removeFriendAction()
     {
         $request = ServiceLayer::getService('request');
         $security = ServiceLayer::getService('security');
         $friendManager = ServiceLayer::getService('PlayerFriendManager');
         $user = $security->getIdentity()->getUser();
         $friend = $friendManager->getPlayerFriendById($user,$request->get('f'));
         
         if(null != $friend)
         {
             $friendManager->removeFriend($friend);
         }
         //\Core\DbQuery::
         //$request->redirect();
     }
     
     public function unfollowAction()
     {
         $request = ServiceLayer::getService('request');
         $teamFollowManager = ServiceLayer::getService('PlayerFollowManager');
         $playerManager = ServiceLayer::getService('PlayerManager');
         $security = ServiceLayer::getService('security');
         $user = $security->getIdentity()->getUser();
         $player = $playerManager->findPlayerById($request->get('id'));
        
         
         $teamFollowManager->removeFollower($player,$user);
         $request->redirect($this->getRouter()->link('player_public_profile',array('player_id' => $player->getId())));
         
     }
     
     public function searchAction()
     {
         return $this->render('Webteamer\Player\PlayerModule:Public:search.php', array(
                   
        ));
     }
    
    
   
}
