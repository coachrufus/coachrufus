<?php

namespace Webteamer\Player\Controller;

use Core\Controller as Controller;
use Core\ServiceLayer;
use Core\Validator;
use CR\Player\Form\PlayerWallPostForm;
use CR\Player\Model\PlayerWallPost;
use DateTime;

class PublicController extends Controller {

    public function indexAction()
    {
        $request = ServiceLayer::getService('request');
        $security = ServiceLayer::getService('security');
        $teamManager = ServiceLayer::getService('TeamManager');
        $sportManager = ServiceLayer::getService('SportManager');
        $playerManager = ServiceLayer::getService('PlayerManager');
        $playerWallManager = ServiceLayer::getService('PlayerWallManager');
        $playerFollowManager = ServiceLayer::getService('PlayerFollowManager');
        $sportEnum = $sportManager->getSportFormChoices();
        $player = $playerManager->findPlayerById($request->get('player_id'));
        $playerSports = $playerManager->findPlayerSports($player);
       
        $teams = $teamManager->getUserTeams($player);
        $followingTeams = $teamManager->getUserFollowingTeams($player);
        $teamInfo = $teamManager->getTeamsInfo($teams+$followingTeams);
        $form = new PlayerWallPostForm();
        
        $posts = $playerWallManager->getWallPosts($player);
        $followers = $playerFollowManager->getPlayerFollowers($player);
        
        


        $user = $security->getIdentity()->getUser();
        $isFollower = $playerFollowManager->isFollower($user,$followers);
        $isOwner = false;
        if($user->getId() == $player->getId())
        {
            $isOwner = true;
        }
        $layout = ServiceLayer::getService('layout');
        $layout->setTemplate(GLOBAL_DIR.'/templates/PublicLayout.php');
        return $this->render('Webteamer\Player\PlayerModule:Public:index.php', array(
                    'player' => $player,
                    'user' => $user,
                    'teams' => $teams,
                    'followingTeams' => $followingTeams,
                    'teamInfo' => $teamInfo,
                    'sportEnum' => $sportEnum,
                    'playerSports' => $playerSports,
                    'form' => $form,
                    'posts' => $posts,
                    'isOwner' => $isOwner,
                    'followers' => $followers,
                    'isFollower' => $isFollower
        ));
    }
    
     public function followAction()
     {
         $request = ServiceLayer::getService('request');
         $teamFollowManager = ServiceLayer::getService('PlayerFollowManager');
         $playerManager = ServiceLayer::getService('PlayerManager');
         $security = ServiceLayer::getService('security');
         $user = $security->getIdentity()->getUser();
         $player = $playerManager->findPlayerById($request->get('id'));
        
         
         $teamFollowManager->createFollower($player,$user);
         $request->redirect($this->getRouter()->link('player_public_profile',array('player_id' => $player->getId())));
         
     }
     
     public function unfollowAction()
     {
         $request = ServiceLayer::getService('request');
         $teamFollowManager = ServiceLayer::getService('PlayerFollowManager');
         $playerManager = ServiceLayer::getService('PlayerManager');
         $security = ServiceLayer::getService('security');
         $user = $security->getIdentity()->getUser();
         $player = $playerManager->findPlayerById($request->get('id'));
        
         
         $teamFollowManager->removeFollower($player,$user);
         $request->redirect($this->getRouter()->link('player_public_profile',array('player_id' => $player->getId())));
         
     }
     
     public function searchAction()
     {
         return $this->render('Webteamer\Player\PlayerModule:Public:search.php', array(
                   
        ));
     }
    
    
   
}
