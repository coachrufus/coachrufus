<?php

namespace Webteamer\Player\Controller;

use Core\Controller as Controller;
use Core\Request as Request;
use Core\ServiceLayer;

class DashboardController extends Controller {

    public function indexAction()
    {
        $request = ServiceLayer::getService('request');
        $teamManager = ServiceLayer::getService('TeamManager');
        $teamEventManager = ServiceLayer::getService('TeamEventManager');
        $sportManager = ServiceLayer::getService('SportManager');
        $friendManager = ServiceLayer::getService('PlayerFriendManager');
        $notificationManager = ServiceLayer::getService('SystemNotificationManager');
        $sportEnum = $sportManager->getSportFormChoices();
        $security = ServiceLayer::getService('security');
        $userIdentity = $security->getIdentity();
        $user = $userIdentity->getUser();

        unset($_SESSION['active_team']['id']);
        $teams = $userIdentity->getUserTeams();
      

   
        
        
        \Core\ServiceLayer::getService('TeamCreditManager')->refreshTeamPackages();

        $unconfirmedTeams = $teamManager->getUserUnconfirmedTeams($user);
        t_dump($unconfirmedTeams);
        $waitingTeams = $teamManager->getUserWaitingTeams($user);
        //set up system land
        if(null !=  $user->getDefaultLang())
        {
            $_SESSION['app_lang'] = $user->getDefaultLang();
            ServiceLayer::getService('translator')->setLang($_SESSION['app_lang']);
        }
        
        //if user has flch cookie
        if($request->get('promo') == 'flch' && $user->getResource() != 'flch')
        {
            $userRepo = ServiceLayer::getService('user_repository');
            $user->setResource('flch');
            $userRepo->save($user);
            //refresh user
            $security->authenticateUserEntity($user);
            //$userIdentity->setUser($user);
            
        }
       
        

       
         
        $userTeamRoles = $security->getIdentity()->getTeamRoles();
        $playerStatusEnum = $teamManager->getPlayerStatusEnum();
        $playerPermissiomEnum = $teamManager->getPermissionEnum();
        $followingTeams = $teamManager->getUserFollowingTeams($user);
        
        $friends = $friendManager->getPlayerFriends($user);

        $teamInfo = $teamManager->getTeamsInfo($teams+$followingTeams);

        
        $scountingForm = new \CR\Scouting\Form\PlayerLookingTeamForm();
        $scountingForm->setFieldChoices('sport_id', $sportEnum);
        $scountingForm->setFieldChoices('level',   ServiceLayer::getService('PlayerManager')->getLevelEnum());

         //login throught social, persist cookie
        if(null != $request->get('sl'))
        {
             $security->remeberUser($user);
        }
        
         //if invitation exist
        if(null != $request->get('inv'))
        {
            $invitationTeam = $teamManager->findTeamByHash($request->get('inv'));
            $invitationTeamExist = false;
            //check if invitation team is in user teams
            foreach($teams as $team)
            {
                if($team->getId() == $invitationTeam->getId())
                {
                    $invitationTeamExist = true;
                    $player = $teamManager->findTeamPlayerByEmail($team,$user->getEmail());
                    $teamManager->updatePlayerFromArray($player->getId(),array('status' => 'confirmed')); 
                    
                    //send push
                    
                    $notificationManager->createPushNotify('create_player',array('team' => $team,'player' => $player));
                }
            }
            
            if($invitationTeamExist == false)
            {
                $finalStatus = 'unconfirmed';
                
                if(null != $request->get('tp') )
                {
                    $player = $teamManager->findTeamPlayerById($request->get('tp'));
                    if(null !=$player &&  $request->get('h')  == $player->getHash())
                    {
                        $player->setEmail($user->getEmail());
                        $teamManager->getRepository()->saveTeamPlayer($player);
                    }
                    else 
                    {
                        throw new AclException();
                    }
                }
                else
                {
                    $player = $teamManager->findTeamPlayerByEmail($invitationTeam,$user->getEmail());
                    //player not exist, create it, and set status as unconfirmed
                    if(null == $player)
                    {
                        $data['player_id'] = $user->getId();
                        $data['first_name'] = $user->getName();
                        $data['last_name'] = $user->getSurname();
                        $data['email'] = $user->getEmail();
                        $data['team_id'] = $invitationTeam->getId();
                        $data['team_role'] = 'PLAYER';
                        $data['level'] = 'beginner';
                        $player = $teamManager->saveTeamPlayer($data); 
                        
                        //send admin notify
                         //send admin mail
                        $teamAdmins = $teamManager->getTeamAdminList($invitationTeam);
                        $userRepo = ServiceLayer::getService('user_repository');
                        foreach($teamAdmins as $teamAdmin)
                        {
                            $teamAdminUser = $userRepo->find($teamAdmin->getPlayerId());
                            $notifyManager = ServiceLayer::getService('notification_manager');
                            if(null != $teamAdminUser)
                            {
                                $data['sender_name'] =  $user->getFullName();
                                $data['team_name'] = $invitationTeam->getName();
                                $data['email'] = $teamAdminUser->getEmail();
                                $data['lang'] =  (null != $teamAdminUser->getDefaultLang()) ? $teamAdminUser->getDefaultLang() : 'en';
                                $data['management_link'] = WEB_DOMAIN.$this->getRouter()->link('team_players_list',array('team_id' => $invitationTeam->getId(),'tp' => $player->getId() )).'#sp_'.$player->getId();
                                $notifyManager->sendShareLinkInvitationAdmin($data);
                                
                                //push notify
                                $pushData = array('player' => $player,'team' => $invitationTeam);
                                $notificationManager->sendPlayerInvitationPushNotify($teamAdminUser,$pushData);
                            }
                        }
            
                        $finalStatus = 'unconfirmed';
                    }
                }
                
                $teamManager->updatePlayerFromArray($player->getId(),array('status' => $finalStatus,'player_id' => $user->getId())); 
               
            }
            //reload teams
             $teams = $teamManager->getUserTeams($user);
             
             unset($_SESSION['invitation']);
             
             $this->getRequest()->redirect($this->getRouter()->link('player_dashboard',array('userid' => $user->getId())));
        }
        
        

        $listTo = new \DateTime();
        $listTo->setTimestamp(strtotime("+ 30 day"));
        
   
        $template = 'index.php';
        if(count($teams) == 0)
        {
            $template = 'start.php';
        }
        
        $tournamentRepository = ServiceLayer::getService('TournamentRepository');
        $tournamentList = $tournamentRepository->findUserTournaments($user);
        
         $privacyRepo = \Core\ServiceLayer::getService('PlayerPrivacyRepository');
         if ('POST' == $request->getMethod() && 'gdpr_marketing_agreement' == $request->get('f_act'))
        {
             $userPrivacy = new \Webteamer\Player\Model\PlayerPrivacy();
            $userPrivacy->setSection('gdpr_marketing_agreement');
            $userPrivacy->setUserGroup('coachrufus');
            $userPrivacy->setPlayerId($user->getId());
            $userPrivacy->setValue($request->get('gdpr_marketing_agreement'));
            $privacyRepo->save($userPrivacy);
            

            
            
             $queueRepo  = ServiceLayer::getService('QueueRepository');
            if(1 == $request->get('gdpr_marketing_agreement'))
            {
                 $queueRepo->addRecord(
                             array(
                                 'action_key' => 'user_id',
                                 'value' => $user->getId(),
                                 'action_type' => 'mailchimp_marketing_export'));
                
            }
            
            $request->redirect();
            
            
        }
        

        
        if ($security->hasMarketingAgreements($security->getIdentity()->getUser()) === 'unknown')
        {
            $layout = \Core\ServiceLayer::getService('layout');
            $layout->setTemplate(GLOBAL_DIR . '/templates/ClearLayout.php');
            $layout->setTemplateParameters(array('body-class' => 'full-overlay-window'));

            return $this->render('Webteamer\Player\PlayerModule:Dashboard:gdpr_marketing.php', array(
            ));
        }
        

        return $this->render('Webteamer\Player\PlayerModule:Dashboard:'.$template, array(
                    'teams' => $teams,
                    'followingTeams' => $followingTeams,
                    'teamInfo' => $teamInfo,
                    'sportEnum' => $sportEnum,
                    'userTeamRoles' => $userTeamRoles,
                    'playerStatusEnum' => $playerStatusEnum,
                    'playerPermissiomEnum' => $playerPermissiomEnum,
                    'userTeamRoles' => $userTeamRoles,
                    'scountingForm' => $scountingForm,
                    'friends' => $friends,
                    'unconfirmedTeams' => $unconfirmedTeams,
                    'waitingTeams' => $waitingTeams,
                    'tournamentList' => $tournamentList,
                    'user' => $user
        ));
    }
    
  

}
