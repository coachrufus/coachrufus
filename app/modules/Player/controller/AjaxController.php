<?php

namespace Webteamer\Player\Controller;

use Core\ControllerResponse;
use Core\RestController;
use Core\ServiceLayer;
use Core\Validator;
use CR\Player\Form\PlayerWallPostCommentForm;
use CR\Player\Form\PlayerWallPostForm;
use CR\Player\Model\PlayerWallPost;
use CR\Player\Model\PlayerWallPostComment;
use DateTime;

class AjaxController extends RestController {
    private function setupTemplate()
    {
        $layout = ServiceLayer::getService('layout');
        $layout->setTemplate(null);
    }
    
    public function searchAction()
    {
        $this->setupTemplate();
        
        
        $playerManager = ServiceLayer::getService('PlayerManager');
        //$players = $playerManager->fulltextFindPlayer($this->getRequest()->get('phrase'));
        $players = $playerManager->fulltextEmailFindPlayer($this->getRequest()->get('phrase'));
        
        
        $result = array();
        foreach($players as $player)
        {
            $result[] = $playerManager->convertEntityToArray($player);
        }
        
        $response = new ControllerResponse();
        $response->setStatus('success');
        $response->setContent(json_encode($result));
        $response->setType('json');
        return $response;
    }
    
    
     public function addRatingAction()
    {
        $this->setupTemplate();
        $playerManager = ServiceLayer::getService('PlayerManager');
        $statManager = ServiceLayer::getService('PlayerStatManager');
        $teamEventManager = ServiceLayer::getService('TeamEventManager');
        $playerId = $this->getRequest()->get('player_id');
        $teamPlayerId = $this->getRequest()->get('team_player_id');
        $security = ServiceLayer::getService('security');
        $user = $security->getIdentity()->getUser();
        
        $lineup = $teamEventManager->getLineupById($this->getRequest()->get('match_id'));
        $lineupRatings = $statManager->getLineupRatings($lineup);

        //check permission
        if($lineup->getEventId() != $this->getRequest()->get('event_id') or $lineup->getTeamId() != $this->getRequest()->get('team_id') )
        {
            throw new \AclException();        
        }

        $newVote = true;

        foreach($lineupRatings as $playerScore)
        {
            if($playerScore->getAuthorId() ==  $user->getId()  && ($playerScore->getTeamPlayerId() == $teamPlayerId or $playerScore->getPlayerId() == $playerId))
            {
                $newVote = false;
                //update rating
                $playerScore->setScore($this->getRequest()->get('rating'));
                $statManager->updatePlayerRating($user,$playerScore);
            }
        }

        $player = $playerManager->findPlayerById($playerId);
        if($playerId != $user->getId() && $newVote)
        {
            $ratingData['match_id'] = $this->getRequest()->get('match_id');
            $ratingData['event_id'] = $this->getRequest()->get('event_id');
            $ratingData['event_date'] = $this->getRequest()->get('event_date');
            $ratingData['team_id'] = $this->getRequest()->get('team_id');
            $ratingData['team_player_id'] = $teamPlayerId;
            $ratingData['score'] = $this->getRequest()->get('rating');

            $statManager->addPlayerRating($player,$user,$ratingData);
            
            if(0 != $playerId)
            {
                $notificationManager = ServiceLayer::getService('SystemNotificationManager');
                $notificationManager->sendPlayerRatingPushNotify($player,array('eventId' =>$ratingData['event_id'] , 'eventDate' => $ratingData['event_date']));
            }

            
            
        }
        ServiceLayer::getService('StepsManager')->finishRating($this->getRequest()->get('team_id'));
         
       
        
        //current player match rating
        $newLineupRatings = $statManager->getLineupRatings($lineup);
        $currentRating = 0;
        $currentRatingCount = 0;
        foreach($newLineupRatings as $lineupRating)
        {
            if($lineupRating->getTeamPlayerId() == $teamPlayerId or $lineupRating->getPlayerId() == $playerId)
            {
                $currentRating += $lineupRating->getScore();
                $currentRatingCount++;
            }
        }
        
        $currentAvgRating = str_replace(',','.',(round($currentRating/$currentRatingCount,1)));

        
        //$currentRating = $statManager->getPlayerRating($player);
        $result['rating'] =$currentAvgRating;
        $response = new ControllerResponse();
        $response->setStatus('success');
        $response->setContent(json_encode($result));
        $response->setType('json');
        return $response;
        
       
    }


    
     public function createWallPostAction()
    {
        $request = $this->getRequest();
        $userIdentity = ServiceLayer::getService('security')->getIdentity();
        $user = $userIdentity->getUser();
        $playerWallManager = ServiceLayer::getService('PlayerWallManager');
        
        
        $form = new PlayerWallPostForm();
        $post = new PlayerWallPost();
        $form->setEntity($post);
        $data =   $request->get($form->getName());
        $form->bindData($data);
        
        $validator = new Validator();
        $validator->setRules($form->getFields());
        $validator->setData($data);
        $validator->validateData();
        
        if(!$validator->hasErrors())
        {
            $post->setCreatedAt(new DateTime());
            $post->setStatus('visible');
            $post->setAuthorId($user->getId());
           
            $playerWallManager->savePostWall($post);    
            $returnData = $playerWallManager->convertEntityToArray($post);
            
            
          
            $isOwner = true;
            $post->setAuthor($user);
            ob_start();
             ServiceLayer::getService('layout')->includePart(MODUL_DIR.'/Player/view/Public/_wall_line.php',array('post' => $post,'isOwner' =>$isOwner ));
            $item = ob_get_contents();
            ob_end_clean();
            
            $result = array('result' => 'SUCCESS','scouting' =>  $returnData,'item' =>$item);
        }
        else
        {
            $result = array('result' => 'ERROR','errors' => $validator->getErrors());
        }
        
        return $this->asJson($result, $status = 'success');
    }
    
    public function deleteWallPostAction()
    {
        $request = $this->getRequest();
        $userIdentity = ServiceLayer::getService('security')->getIdentity();
        $user = $userIdentity->getUser();
        $playerWallManager = ServiceLayer::getService('PlayerWallManager');
        
        $post = $playerWallManager->getPostById($request->get('id'));
         
        if($user->getId() == $post->getAuthorId())
        {
             $playerWallManager->deletePostWall($post);
             $result = array('result' => 'SUCCESS');
             return $this->asJson($result, $status = 'success');
        }
        
        $result = array('result' => 'ERROR');
        return $this->asJson($result, $status = 'error');
         
    }

    public function createWallPostCommentAction()
    {
        $request = $this->getRequest();
        $userIdentity = ServiceLayer::getService('security')->getIdentity();
        $user = $userIdentity->getUser();
        $playerWallManager = ServiceLayer::getService('PlayerWallManager');
        
        
        $form = new PlayerWallPostCommentForm();
        $comment = new PlayerWallPostComment();
        $form->setEntity($comment);
        $data =   $request->get('comment');
        $form->bindData($data);
        
        $validator = new Validator();
        $validator->setRules($form->getFields());
        $validator->setData($data);
        $validator->validateData();
        
        if(!$validator->hasErrors())
        {
            $comment->setCreatedAt(new DateTime());
            $comment->setStatus('visible');
            $comment->setAuthorId($user->getId());
            $comment->setPostId($data['post_id']);
           
            $playerWallManager->savePostWallComment($comment);    
            $returnData = $playerWallManager->convertEntityToArray($comment);

            $comment->setAuthor($user);
            ob_start();
             ServiceLayer::getService('layout')->includePart(MODUL_DIR.'/Player/view/Public/_comment_line.php',array('comment' => $comment ));
            $item = ob_get_contents();
            ob_end_clean();
            
            $result = array('result' => 'SUCCESS','scouting' =>  $returnData,'item' =>$item);
        }
        else
        {
            $result = array('result' => 'ERROR','errors' => $validator->getErrors());
        }
        
        return $this->asJson($result, $status = 'success');

    }
    
    public function getWallPostCommentsAction()
    {
         $request = $this->getRequest();
         $playerWallManager = ServiceLayer::getService('PlayerWallManager');
         $post = $playerWallManager->getPostById($request->get('post'));
         $comments = $playerWallManager->getPostComments($post);
         
         $list = array();
         $lines = array();
         foreach ($comments as $comment)
         {
             $list[] = $playerWallManager->convertEntityToArray($comment);
             
            ob_start();
             ServiceLayer::getService('layout')->includePart(MODUL_DIR.'/Player/view/Public/_comment_line.php',array('comment' => $comment ));
            $item = ob_get_contents();
            ob_end_clean();
             
             $lines[] = $item;
         }
         $result = array('result' => 'SUCCESS','comments' =>  $list,'lines' => $lines);
         return $this->asJson($result, $status = 'success');
    }
    
    public function getPlayerMentionsAction()
    {
        $teamPlayerManager = ServiceLayer::getService('TeamPlayerManager');
        $friendManager = ServiceLayer::getService('PlayerFriendManager');
        $seoManager = ServiceLayer::getService('seo');
        $term = strtolower( $seoManager->removeDiacritic($this->getRequest()->get('q')));
        $userIdentity = ServiceLayer::getService('security')->getIdentity();
        $user = $userIdentity->getUser();
       
        $userList = array();
        $teamMates = $teamPlayerManager->findPlayerTeamMates($user,$term);

        
        foreach($teamMates as $teamMate)
        {
            if($teamMate->getPlayerId() > 0)
            {
                $userList[$teamMate->getPlayerId()] = $teamMate->getFullName();
            }
            
        }
        
        
        
        //friends
        $friends = $friendManager->findPlayerFriends($user,$term);

        foreach($friends as $friend)
        {
           $userList[$friend->getPlayer()->getId()] = $friend->getPlayer()->getFullName();
        }
        
        
        /*
        $mentions = array();
         $mentions[] = 'ttt';
         $mentions[] = 'tteeeet';
         * 
         */
        $result = array('result' => 'SUCCESS','mentions' =>  $userList);
         return $this->asJson($result, $status = 'success');
    }


}
