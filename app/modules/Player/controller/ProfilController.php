<?php

namespace Webteamer\Player\Controller;

use Core\Controller as Controller;
use Core\ControllerResponse;
use Core\ServiceLayer as ServiceLayer;
use Webteamer\Player\Form\PlayerSportForm;
use Webteamer\Player\Form\ProfilForm as ProfilForm;
use Webteamer\Player\Form\ProfilFormValidator as ProfilFormValidator;
use Webteamer\Player\Model\PlayerPrivacy;
use Webteamer\Player\Model\PlayerSport;

class ProfilController extends Controller {

    public function createProfilForm()
    {
        $sportManager = ServiceLayer::getService('SportManager');
        $playerManager = ServiceLayer::getService('PlayerManager');
        $translator = ServiceLayer::getService('translator');
        $form = new ProfilForm();
        
        $sportChoices = $sportManager->getSportFormChoices();
        $sportChoicesTrans = array();
        foreach($sportChoices as $key => $name)
        {
            $sportChoicesTrans[$key] = $translator->translate($name);
        }
        
        $form->setFieldChoices('sport_id', $sportChoicesTrans);
        $form->setFieldOption('sport_id', 'label', 'Sport');
        
        
        
        $form->setFieldChoices('sport_level', $playerManager->getLevelFormChoices());
        $form->setFieldChoices('level', $playerManager->getLevelFormChoices());
        $form->setFieldChoices('gender',  array('man' => $translator->translate('Muž'), 'woman' =>  $translator->translate('Žena')));
        $form->setFieldOption('gender','required', false);
        $form->setFieldChoices('player_status', $playerManager->getStatusFormChoices());

        
       

        $form->setFieldChoices('unitsystem', array(
            '' => $translator->translate('Unitsystem'),
            'metric' => $translator->translate('Metric (kilometer)'),
            'imperial' => $translator->translate('Imperial (miles)')
        ));
        
        $form->setFieldChoices('gender', array(
             '' => $translator->translate('Gender'),
            'male' => $translator->translate('Male'),
            'female' => $translator->translate('Female')
        ));
        
         
        $form->setFieldChoices('default_lang', array(
             '' => $translator->translate('Language'),
            'sk' => $translator->translate('Slovak'),
            'en' => $translator->translate('English'),
            'de' => $translator->translate('German'),
            'es' => $translator->translate('Spanish'),
            'pl' => $translator->translate('Polish'),
            'srb' => $translator->translate('Serbian'),
        ));
        
        $form->setFieldChoices('week_start', array(
            '' => $translator->translate('Week start'),
            'monday' => $translator->translate('Monday'),
            'sunday' => $translator->translate('Sunday')
        ));
        
         $form->setFieldChoices('birthday', array('' => $translator->translate('Day')) + $form->getFieldChoices('birthday'));
         $form->setFieldChoices('birthmonth', array('' => $translator->translate('Month')) + $form->getFieldChoices('birthmonth'));
         $form->setFieldChoices('birthyear', array('' => $translator->translate('Year')) + $form->getFieldChoices('birthyear'));
        
         
        
        
        return $form;
    }
    
    public function subscriptionsListAction()
    {
        $security = ServiceLayer::getService('security');
        $orderManager = ServiceLayer::getService('OrderManager');
        $paymentManager = ServiceLayer::getService('PaymentManager');
        $userOrders  = $orderManager->getUserSubscriptions($security->getIdentity()->getUser());
        \Core\ServiceLayer::getService('TeamCreditManager')->refreshTeamPackages();
        $paymentManager = ServiceLayer::getService('PaymentManager');
        $failPaymentData = array('teamId' => '','variant' => '','desc','prices' => $paymentManager->getProPrices());
        if(null != $this->getRequest()->get('subscription-fail'))
        {
            $gopay = new \GoPay\Api();
            $orderStatus = $gopay->getPaymentInfo($this->getRequest()->get('subscription-fail'));
            $order = $orderManager->getOrderByNumber($orderStatus->order_number);
            $teamManager = ServiceLayer::getService('TeamManager');
            $team = $teamManager->findTeamById($order->getTeamId());
            
            $failPaymentData['teamId'] = $order->getTeamId();
            $failPaymentData['variant'] =$order->getVariant();
            $failPaymentData['desc'] = 'PRO<br/>'.$team->getName().'<br />ID '.$team->getId();
            
           
        }
        
        
       
       
        $prices = $paymentManager->getProPrices();

        return $this->render('Webteamer\Player\PlayerModule:profil:subscriptions.php', array(
            'orders' => $userOrders,
            'prices' => $prices,
            'failPaymentData' => $failPaymentData,
        ));
    }
    
      public function billingListAction()
    {
        $security = ServiceLayer::getService('security');
        $invoiceManager = ServiceLayer::getService('InvoiceManager');
        $userInvoices  = $invoiceManager->getUserInvoices($security->getIdentity()->getUser());
        


        return $this->render('Webteamer\Player\PlayerModule:profil:billing.php', array(
            'invoices' => $userInvoices,
        ));
    }
    
    public function interestAction()
    {
        $request = ServiceLayer::getService('request');
        $security = ServiceLayer::getService('security');
        $sportManager = ServiceLayer::getService('SportManager');
        $sport = $sportManager->findSportById($request->get('sport_id'));
        $playerManager = ServiceLayer::getService('PlayerManager');
        $user = $security->getIdentity()->getUser();
        
        
        $playerSport = $playerManager->findPlayerSport($user,$sport);
        
        $form = new PlayerSportForm();
         
        
        return $this->render('Webteamer\Player\PlayerModule:profil:interest.php', array(

        ));
    }
    
    public function profilAction()
    {
        $request = ServiceLayer::getService('request');
        $repository = ServiceLayer::getService('PlayerRepository');
        $playerManager = ServiceLayer::getService('PlayerManager');
        $translator = ServiceLayer::getService('translator');
        $security = ServiceLayer::getService('security');
        $user = $security->getIdentity()->getUser();
        $localityManager = ServiceLayer::getService('LocalityManager');
        $sportManager = ServiceLayer::getService('SportManager');
        $blacklist = ServiceLayer::getService('NotificationBlacklist');
        $sports = $sportManager->getSportFormChoices();
        $entity = $repository->find($user->getId());
        $playerSports = $playerManager->findPlayerSports($entity);
        $playerPrivacy =  $playerManager->findPlayerPrivacy($entity);
        $privacyMatrix = $playerManager->getPrivacyMatrix($playerPrivacy);
       

        if($blacklist->isBlacklisted($entity->getEmail()))
        {
            $entity->setReceiveNotifications(false);
        }
        else
        {
            $entity->setReceiveNotifications(true);
        }
        $form = $this->createProfilForm();
        $form->setEntity($entity);
        $validator = new ProfilFormValidator($form->getFields());

        $langChanged = false;
        if('POST' == $request->getMethod() )
        {
            if('base-data' == $request->get('form_action'))
            {
                $post_data = $request->get('record');
                
                if($post_data['default_lang'] != $user->getDefaultLang())
                {
                    $request->addFlashMessage('lang_changed', $post_data['default_lang']);
                }
                
                $form->bindData($post_data);
                $validator->setData($post_data);
                $validator->validateData();
                $validator->validateUniqueEmail($repository,$post_data['email'],$user->getId());

                //check passwords
                if(null != $request->get('password'))
                {
                   $pass1 = $request->get('password');
                   $pass2 = $request->get('password_confirm');
                   
                   if($pass1 != $pass2)
                   {
                       $validator->addErrorMessage('confirm_password', $this->getTranslator()->translate('Password not confirm'));
                   }
                }

                if (!$validator->hasErrors())
                {
                      $entity = $form->getEntity();

                      
                      if(null != $request->get('password'))
                      {
                          $encodedPassword = $security->encodePassword($request->get('password'),$entity->getSalt());
                          $entity->setPassword($encodedPassword);
                      }
                     
                      
                      $repository->save($entity);
                      
                      //update name and surname in all teams
                      $repository->updatePlayerTeamData($entity,array('first_name' => $entity->getName(),'last_name' => $entity->getSurname() ));
                      
                      //set language according to language from form
                      if(in_array($entity->getDefaultLang(),array('sk','en')))
                      {
                           $_SESSION['app_lang'] = $entity->getDefaultLang();
                           $translator->setLang($_SESSION['app_lang']);
                      }
                     
                      
                       
                        $security->reloadUser($user);
                        $request->addFlashMessage('user_edit_success', $translator->translate('user.crud_edit.success'));
                        $request->redirect();
                }
                
               
            }
            
            if('privacy-data' == $request->get('form_action'))
            {
                $privacyData= $request->get('privacy');
                $entity = $form->getEntity();
                $this->savePlayerPrivacy($entity,$privacyData);

                $security->reloadUser($user);
                $request->addFlashMessage('user_edit_success', $translator->translate('user.crud_edit.success'));
                $request->redirect();
            }
            
            if('sports-data' == $request->get('form_action'))
            {
                $sportsData = $request->get('sports');
                $entity = $form->getEntity();
                $this->savePlayerSports($entity,$sportsData);

                $security->reloadUser($user);
                $request->addFlashMessage('user_edit_success', $translator->translate('user.crud_edit.success'));
                $request->redirect();
            }
            
            if('locality-data' == $request->get('form_action'))
            {
                $localityData = $request->get('locality');
                $post_data = $request->get('record');
                //$form->bindData($post_data);
                $entity = $form->getEntity();
                $this->saveLocalityInfo($entity,$post_data,$localityData);

                $security->reloadUser($user);
                $request->addFlashMessage('user_edit_success', $translator->translate('user.crud_edit.success'));
                $request->redirect();
               
            }
            
            if('notify-data' == $request->get('form_action'))
            {
                $post_data = $request->get('record');
                $blacklist = ServiceLayer::getService('NotificationBlacklist');

                if(null == $post_data['receive_notifications'])
                {
                     $blacklist->add($entity->getEmail());
                }
                else
                {
                    $blacklist->remove($entity->getEmail());
                }
                
                $security->reloadUser($user);
                $request->addFlashMessage('user_edit_success', $translator->translate('user.crud_edit.success'));
                $request->redirect();
            }
        }

        $router = ServiceLayer::getService('router');
        return $this->render('Webteamer\Player\PlayerModule:profil:profil.php', array(
                    'form' => $form,
                    'request' => $request,
                    'validator' => $validator,
                    'playerSports' => $playerSports,
                    'privacyMatrix' => $privacyMatrix,
                    'langChanged' => $langChanged,
                    'userLang' => $entity->getDefaultLang()
                    
        ));
    }
    
    
    private function saveLocalityInfo($entity,$post_data,$localityData)
    {
        $localityManager = ServiceLayer::getService('LocalityManager');
        $playerManager = ServiceLayer::getService('PlayerManager');
        if ($localityData['locality_id'] == null)
        {
            //check if exist locality as json
            if ($localityData['locality_json_data'] != null)
            {
                $locality = $localityManager->createObjectFromJsonData($localityData['locality_json_data']);

                $locality->setCity($localityData['city']);
                $locality->setStreet($localityData['street']);
                $locality->setAuthorId($entity->getId());
                $locality->setStreetNumber($localityData['street_number']);
                $locality_id = $localityManager->saveLocality($locality);
                $entity->setLocalityId($locality_id);
            }
        }
         $entity->setAddress($post_data['address']);
        $entity->setTimezone($post_data['timezone']);
        $entity->setUnitsystem($post_data['unitsystem']);
        $entity->setTimeformat($post_data['timeformat']);
        $entity->setDateformat($post_data['dateformat']);
        $entity->setWeekStart($post_data['week_start']);
        $playerManager->savePlayer($entity);
    }
    
    private function savePlayerSports($entity,$sportsData)
    {
        $playerManager = ServiceLayer::getService('PlayerManager');
        $playerSports = array();
        foreach($sportsData as $sportData )
        {
            $playerSport = new PlayerSport();
            $playerSport->setSportId($sportData['sport']);
            $playerSport->setLevel($sportData['level']);
            $playerSports[] = $playerSport;
        }
        $playerManager->savePlayerSports($entity,$playerSports);
    }
    
    private function savePlayerPrivacy($entity,$privacyData)
    {
        $playerManager = ServiceLayer::getService('PlayerManager');
        $playerPrivacy = array();
        
     
        
        foreach($privacyData as $sectionId => $groups )
        {
            foreach($groups as $groupId => $val)
            {
                $privacy = new PlayerPrivacy();
                $privacy->setSection($sectionId);
                $privacy->setUserGroup($groupId);
                $privacy->setValue($val);
                $playerPrivacy[] = $privacy;
            }
          
        }
        $playerManager->savePlayerPrivacy($entity,$playerPrivacy);
    }
    
    public function uploadPhotoAction()
    {
        $request = ServiceLayer::getService('request');
        
         //validate file size
        $fileSize = $request->getFileSize('avatar');
        $allowedSize = 3*1024*1024;
        
        if($allowedSize > $fileSize)
        {
            $baseName = md5(time()).'_'.$_FILES['avatar']['name'];
            $request->uploadFile('avatar',PUBLIC_DIR.'/img/users/',$baseName);

            $imageTransform = ServiceLayer::getService('imageTransform');
             $imageTransform->resizeImage(array(
                'sourceImg' => PUBLIC_DIR.'img/users/'.$baseName,
                'width' => 90, 
                'height' => 90,
                'targetDir' =>PUBLIC_DIR.'/img/users/thumb_90_90'));

            $imageTransform->resizeImage(array(
                'sourceImg' => PUBLIC_DIR.'img/users/'.$baseName,
                'width' => 320, 
                'height' => 320,
                'targetDir' =>PUBLIC_DIR.'/img/users/thumb_320_320'));

            $imageTransform->resizeImage(array(
                'sourceImg' => PUBLIC_DIR.'img/users/'.$baseName,
                'width' => 640, 
                'height' => 640,
                'targetDir' =>PUBLIC_DIR.'/img/users/thumb_640_640'));

            $imageTransform->resizeImage(array(
                'sourceImg' => PUBLIC_DIR.'img/users/'.$baseName,
                'width' => 960, 
                'height' => 960,
                'targetDir' =>PUBLIC_DIR.'/img/users/thumb_960_960'));



            $result['file'] = '/img/users/thumb_320_320/'.$baseName;
            $result['file_name'] = $baseName;
            $playerManager = ServiceLayer::getService('PlayerManager');
            $security = ServiceLayer::getService('security');
            $user = $security->getIdentity()->getUser();
            $user->setPhoto($baseName);
            $playerManager->savePlayer($user);
            $result['result'] = 'SUCCESS';
        }
        else
        {
             $result['result'] = 'ERROR';
             $result['error_message'] = ServiceLayer::getService('translator')->translate('Allowed max size is 3MB');
        }
        
        $layout = ServiceLayer::getService('layout');
        $layout->setTemplate(null);
        
        $response = new ControllerResponse();
        $response->setStatus('success');
        $response->setContent(json_encode($result));
        $response->setType('json');
         return $response;
    }
    

    
   
    
    public function statsAction()
    {
        $request = ServiceLayer::getService('request');
        $playerStatManager = ServiceLayer::getService('PlayerStatManager');
        $playerManager = ServiceLayer::getService('PlayerManager');
       
        $player =  $playerManager->findPlayerById($request->get('player_id'));

        
        $matchOverview = $playerStatManager->getPlayerAllMatchOverview($player);
        $resultClass = array('win' => 'success', 'loose' => 'danger', 'draw' => 'info');

        
        return $this->render('Webteamer\Player\PlayerModule:profil:stats.php', array(
                'matchOverview' => $matchOverview,
                'resultClass' => $resultClass,
                'player' => $player
        ));
    }

    
     
    public function matchDetailAction()
    {
        $request = ServiceLayer::getService('request');
        $playerStatManager = ServiceLayer::getService('PlayerStatManager');
        $playerManager = ServiceLayer::getService('PlayerManager');
        $teamEventManager = ServiceLayer::getService('TeamEventManager');
        $teamManager = ServiceLayer::getService('TeamManager');
        $lineup = $teamEventManager->getLineupById($request->get('l'));
        $team = $teamManager->findTeamById($lineup->getTeamId());
        
        ServiceLayer::getService('security')->getIdentity()->checkTeamAccess($team);
        
        $timeline = $playerStatManager->getGroupedEventTimelineByLineup($lineup);
        $hitEnum =  $playerStatManager->getHitTypeEnum();
        
        $event = $teamEventManager->findEventById($lineup->getEventId());
        $event->setCurrentDate($lineup->getEventDate());
        $playground = $event->getPlayground();
        
        $matchOverview = $playerStatManager->getMatchOverview($lineup);
        
        $matchPlayerStats =  $playerStatManager->getMatchPlayersStat($lineup);
        $lineupPlayers = $teamEventManager->getEventLineupPlayers($lineup);

        $playersStat = array();
        foreach($lineupPlayers as $index => $lineupPlayer)
        {
            $playersStat[$index]['player'] = $lineupPlayer;
            
            if(!array_key_exists($lineupPlayer->getId(), $matchPlayerStats))
            {
                 $playersStat[$index]['stat'] = new \Webteamer\Player\Model\PlayerStat();
            }
            else
            {
                 $playersStat[$index]['stat'] = $matchPlayerStats[$lineupPlayer->getId()];
            }
           
        }
     
        
        $layout = \Core\ServiceLayer::getService('layout');
        $layout->setTemplate(GLOBAL_DIR.'/templates/ClearLayout.php');
        $resultClass = array('win' => 'success', 'loose' => 'danger', 'draw' => 'info');
        return $this->render('Webteamer\Player\PlayerModule:profil:matchDetail.php', array(
                'timeline' => $timeline,
            'hitEnum' => $hitEnum,
            'matchOverview' => $matchOverview,
            'event' => $event,
            'playground' => $playground,
            'resultClass' => $resultClass,
            'playersStat' => $playersStat
        ));
    }
    
    public function iconAction()
    {
        $request = $this->getRequest();  
        $iconName = $request->get('file');
        
        if(!file_exists( PUBLIC_DIR.'img/users/square_90_90/'.$iconName))
        {
             $imageTransform = ServiceLayer::getService('imageTransform');
             
             $sourceImage = PUBLIC_DIR.'img/users/'.$iconName;

             if(!file_exists($sourceImage))
             {
                  $sourceImage = PUBLIC_DIR.'img/users/thumb_320_320/'.$iconName;
             }

             
             $imageTransform->crop(array(
                'sourceImg' => $sourceImage,
                'width' => 90, 
                'height' => 90,
                'targetDir' =>PUBLIC_DIR.'/img/users/square_90_90'));
        }

        $file_extension = strtolower(substr(strrchr($iconName,"."),1));

        switch( $file_extension ) {
            case "gif": $ctype="image/gif"; break;
            case "png": $ctype="image/png"; break;
            case "jpeg":
            case "jpg": $ctype="image/jpeg"; break;
            default:
        }

        header('Content-type: ' . $ctype);
        echo file_get_contents(PUBLIC_DIR.'img/users/square_90_90/'.$iconName);
    }
    
    
    public function updateLayoutSettingsAction()
    {
        $request = $this->getRequest();
        $security = ServiceLayer::getService('security');
        $user = $security->getIdentity()->getUser();
        $repository = ServiceLayer::getService('user_repository');
        $userData = $repository->find($user->getId());
        
        $action = $request->get('a');
        if($action == 'hide-mobile-banner')
        {
            $layoutSettings = $userData->getLayoutSettings();

            if(null == $layoutSettings)
            {
                $layoutSettings = array();
            }
            else
            {
                $layoutSettings = json_decode($layoutSettings,true);
            }

            $layoutSettings["hideMobileBanner"] = true;

            $userData->setLayoutSettings(json_encode($layoutSettings));
            $repository->save($userData);
            
            $security->reloadUser($user);

        }
    }
    
}
