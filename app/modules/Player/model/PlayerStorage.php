<?php
namespace Webteamer\Player\Model;

use Core\DbQuery;
use Core\DbStorage;
use PDO;




class PlayerStorage extends DbStorage
{
        public function fulltextSearch($phrase,$section)
        {
            /*
            $query = \Core\DbQuery::prepare('SELECT u.*  '
                        . ' FROM '.$this->getTableName().' u WHERE u.name like :id');
             $query->bindParam('id', "jano%");
           */
           /*
            $query = \Core\DbQuery::prepare('SELECTT u.*  '
                        . ' FROM '.$this->getTableName().' u WHERE u.name like :name or u.surname like :surname or u.nickname like :nickname or u.email like :email');*/
             $query = \Core\DbQuery::prepare('SELECT u.*  '
                        . ' FROM '.$this->getTableName().' u WHERE u.name like :name or u.surname like :surname or u.nickname like :nickname or u.email like :email');
           /*
           $query = \Core\DbQuery::prepare('SELECT u.*  '
                        . ' FROM '.$this->getTableName().' u WHERE u.name like :name');*/
           $query->bindParam('name', $phrase."%");
          
           $query->bindParam('surname', $phrase."%");
           $query->bindParam('nickname', $phrase."%");
           $query->bindParam('email', $phrase."%");
           
           
           $data = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
           return $data;
           
        }
        public function fulltextEmailSearch($phrase)
        {
           
             $query = \Core\DbQuery::prepare('SELECT u.*  '
                        . ' FROM '.$this->getTableName().' u WHERE  u.email like :email');
           $query->bindParam('email', "%".$phrase."%");
           $data = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
           return $data;
           
        }
    
        public function findBy($query_params = array(),$options = array())
	{


            $where_criteria_items  = array();
		foreach ($query_params as $column => $column_val)
		{
			if(is_string($column_val))
			{
				$where_criteria_items[] = ' u.'.$column.'=:'.$column.' ';
			}
			else
			{
				$where_criteria_items[] = ' u.'.$column.'=:'.$column.' ';
			}
			
		}
		$where_criteria = ' WHERE '. implode(' AND ',$where_criteria_items);
        
                $sort_criteria = '';
                if(array_key_exists('order',$options))
                {
                    $sort_criteria = ' order by u.'.$options['order'];
                }
                

		$query = \Core\DbQuery::prepare('SELECT u.* , '.$this->getAssocColumnsQuery().' '
                        . ' FROM '.$this->getTableName().' u LEFT JOIN locality a ON u.locality_id = a.id'. $where_criteria.$sort_criteria);
                
                
                
		foreach($query_params as $column => $value)
		{
			if("" != $value)
			{
				$query->bindParam(':'.$column, $value);
			}
		}
		$data = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);


		return $data;
	}
        
          public function findPlayersByArea($center, $distance)
        {
            
             
              $data = \Core\DbQuery::prepare('SELECT  u.*,  (6371 * ACOS(COS(RADIANS('.$center['lat'].')) * COS(RADIANS(l.lat)) * COS(RADIANS(l.lng) - RADIANS('.$center['lng'].')) + SIN(RADIANS('.$center['lat'].')) * SIN(RADIANS(l.lat)))) AS distance 
                    FROM locality l 
                    LEFT JOIN co_users u ON l.id = u.locality_id 
                    WHERE u.id is not null
                    HAVING distance < '.$distance)
		->execute()
		->fetchAll(\PDO::FETCH_ASSOC);
		return $data;
        }
        
        public function updatePlayerTeamData($player_id,$data)
        {
            $query =  \Core\DbQuery::prepare('UPDATE team_players set first_name=:first_name, last_name=:last_name where player_id=:player_id');
             $query->bindParam('first_name', $data['first_name']);
             $query->bindParam('last_name', $data['last_name']);
             $query->bindParam('player_id',$player_id);
              $query->execute();
        }
       
       
        
        
       
}

