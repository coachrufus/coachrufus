<?php
namespace CR\Player\Model;

use Core\DbQuery;
use Core\DbStorage;
use PDO;




class PlayerWallPostCommentStorage extends DbStorage
{
       public function getPostComments($postId)
       {
            $data = \Core\DbQuery::prepare('
                SELECT c.*, u.name, u.surname, u.photo
                FROM '.$this->getTableName().' c
                 LEFT JOIN co_users u ON c.author_id = u.id
                    
                WHERE c.post_id = :id
                ORDER  by c.id asc')
                 ->bindParam(':id', $postId)
		->execute()
		->fetchAll(\PDO::FETCH_ASSOC);
		return $data;
       }
       
      
              
        
        
       
}

