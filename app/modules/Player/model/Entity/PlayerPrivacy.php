<?php
	
namespace Webteamer\Player\Model;
use Core\EntityMapper as EntityMapper;
use Core\ServiceLayer as ServiceLayer;
		
class PlayerPrivacy {	

	
	
		
private $repository;
private $mapper_rules  = array(
	'id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => true
							
					),
'section' => array(
					'type' => 'string',
					'max_length' => '50',
					'required' => false
							
					),
'user_group' => array(
					'type' => 'string',
					'max_length' => '50',
					'required' => false
							
					),
'value' => array(
					'type' => 'string',
					'max_length' => '50',
					'required' => false
							
					),
'player_id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => false
							
					),
'data' => array(
					'type' => 'string',
					'max_length' => '500',
					'required' => false
							
					),

);
	
public function getDataMapperRules()
{
	return $this->mapper_rules;
}

	
protected $id;

protected $section;

protected $userGroup;

protected $value;

protected $playerId;
protected $data;

				
public function setId($val){$this->id = $val;}

public function getId(){return $this->id;}
	
public function setSection($val){$this->section = $val;}

public function getSection(){return $this->section;}
	
public  function getUserGroup() {
return $this->userGroup;
}

public  function setUserGroup($userGroup) {
$this->userGroup = $userGroup;
}


	
public function setValue($val){$this->value = $val;}

public function getValue(){return $this->value;}
public  function getPlayerId() {
return $this->playerId;
}

public  function setPlayerId($playerId) {
$this->playerId = $playerId;
}

function getData() {
return $this->data;
}

 function setData($data) {
$this->data = $data;
}





}

		