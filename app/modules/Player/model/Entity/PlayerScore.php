<?php
	
namespace Webteamer\Player\Model;
use Core\EntityMapper as EntityMapper;
use Core\ServiceLayer as ServiceLayer;
		
class PlayerScore {	

	
private $mapper_rules  = array(
	'id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => true
							
					),
'player_id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => false
							
					),
'author_id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => false
							
					),
'score' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => false
							
					),
'created_at' => array(
					'type' => 'datetime',
					'max_length' => '',
					'required' => false
							
					),
'match_id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => false
							
					),
'author_scoring' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => false
							
					),
'event_id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => false
							
					),
'event_date' => array(
					'type' => 'datetime',
					'max_length' => '11',
					'required' => false
							
					),
'team_id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => false
							
					),
'team_player_id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => false
							
					),

);
	
public function getDataMapperRules()
{
	return $this->mapper_rules;
}

	
protected $id;

protected $playerId;

protected $authorId;

protected $score;

protected $createdAt;

protected $matchId;

protected $authorScoring;

protected $eventId;
protected $eventDate;
protected $teamId;
protected $teamPlayerId;
protected $voters = array();

				
public function setId($val){$this->id = $val;}

public function getId(){return $this->id;}
	
public function setPlayerId($val){$this->playerId = $val;}

public function getPlayerId(){return $this->playerId;}
	
public function setAuthorId($val){$this->authorId = $val;}

public function getAuthorId(){return $this->authorId;}
	
public function setScore($val){$this->score = $val;}

public function getScore(){return $this->score;}
	
public function setCreatedAt($val){$this->createdAt = $val;}

public function getCreatedAt(){return $this->createdAt;}
	
public function setMatchId($val){$this->matchId = $val;}

public function getMatchId(){return $this->matchId;}
	
public function setAuthorScoring($val){$this->authorScoring = $val;}

public function getAuthorScoring(){return $this->authorScoring;}

public  function getEventId() {
return $this->eventId;
}

public  function getEventDate() {
return $this->eventDate;
}

public  function getTeamId() {
return $this->teamId;
}

public  function setEventId($eventId) {
$this->eventId = $eventId;
}

public  function setEventDate($eventDate) {
$this->eventDate = $eventDate;
}

public  function setTeamId($teamId) {
$this->teamId = $teamId;
}


public  function getVoters() {
return $this->voters;
}

public  function setVoters($voters) {
$this->voters = $voters;
}

public function addVoter($key,$val)
{
    $this->voters[$key] = $val;
}

public  function getTeamPlayerId() {
return $this->teamPlayerId;
}

public  function setTeamPlayerId($teamPlayerId) {
$this->teamPlayerId = $teamPlayerId;
}




}

		