<?php

namespace Webteamer\Player\Model;

/**
 * Player stats for individaul match
 */
class PlayerBasketballMatchStat extends PlayerMatchStat {

    protected $points1;
    protected $points2;
    protected $points3;

    function getPoints1()
    {
        return $this->points1;
    }

    function getPoints2()
    {
        return $this->points2;
    }

    function getPoints3()
    {
        return $this->points3;
    }

    function setPoints1($points1)
    {
        $this->points1 = $points1;
    }

    function setPoints2($points2)
    {
        $this->points2 = $points2;
    }

    function setPoints3($points3)
    {
        $this->points3 = $points3;
    }
    
    /*
    public function getResultStatus()
    {
        $firstLineWins = 0;
        $secondLineWins = 0;
        
        foreach($this->getSets() as $set)
        {
            if($set['first_line'] > $set['second_line'])
            {
                $firstLineWins++;
            }
            
            if($set['first_line'] < $set['second_line'])
            {
                $secondLineWins++;
            }
            
        }
        
         //wins, draws, looses
        if ($firstLineWins == $secondLineWins)
        {
            return 'draw';
        }
        elseif ($firstLineWins > $secondLineWins && $this->getLineupPosition() == 'first_line')
        {
            return 'win';
        }
        elseif ($firstLineWins < $secondLineWins && $this->getLineupPosition() == 'second_line')
        {
            return 'win';
        }
        else
        {
            return 'loose';
        }
    }
     * 
     */

}
