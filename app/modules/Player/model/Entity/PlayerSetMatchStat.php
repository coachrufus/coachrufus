<?php

namespace Webteamer\Player\Model;

/**
 * Player stats for individaul match
 */
class PlayerSetMatchStat extends PlayerMatchStat {

    protected $sets;
    
    function getSets()
    {
        return $this->sets;
    }

    function setSets($sets)
    {
        $this->sets = $sets;
    }

    public function addSet($index, $data)
    {
        $this->sets[$index] = $data;
    }
    
    public function getResultStatus()
    {
        $firstLineWins = 0;
        $secondLineWins = 0;
        
        foreach($this->getSets() as $set)
        {
            if($set['first_line'] > $set['second_line'])
            {
                $firstLineWins++;
            }
            
            if($set['first_line'] < $set['second_line'])
            {
                $secondLineWins++;
            }
            
        }
        
         //wins, draws, looses
        if ($firstLineWins == $secondLineWins)
        {
            return 'draw';
        }
        elseif ($firstLineWins > $secondLineWins && $this->getLineupPosition() == 'first_line')
        {
            return 'win';
        }
        elseif ($firstLineWins < $secondLineWins && $this->getLineupPosition() == 'second_line')
        {
            return 'win';
        }
        else
        {
            return 'loose';
        }
    }
    

    public function getWonSet()
    {
        $wins = 0;
        foreach($this->getSets() as $set)
        {
            if($set['first_line'] > $set['second_line']  && $this->getLineupPosition() == 'first_line')
            {
                $wins++;
            }
            
            if($set['first_line'] < $set['second_line'] && $this->getLineupPosition() == 'second_line')
            {
                $wins++;
            }
        }
        
        return $wins;
    }
    
    public function getDrawSet()
    {
        $draws = 0;
        foreach($this->getSets() as $set)
        {
            if($set['first_line'] == $set['second_line'] )
            {
                $draws++;
            }
        }
        
        return $draws;
    }
    
    public function getLooseSet()
    {
        $looses = 0;
        foreach($this->getSets() as $set)
        {
            if($set['first_line'] < $set['second_line']  && $this->getLineupPosition() == 'first_line')
            {
                $looses++;
            }
            
            if($set['first_line'] > $set['second_line'] && $this->getLineupPosition() == 'second_line')
            {
                $looses++;
            }
        }
        
        return $looses;
    }
   



}
