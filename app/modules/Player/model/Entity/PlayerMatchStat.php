<?php

namespace Webteamer\Player\Model;

/**
 * Player stats for individaul match
 */
class PlayerMatchStat {

    protected $assists;
    protected $goals;
    protected $hits;
    protected $manOfMatch;
    protected $player;
    protected $saves;
    protected $eventDate;
    protected $eventId;
    protected $lineupPosition;
    protected $lineupId;
    protected $result;
    protected $teamName;
    protected $firstLineGoals;
    protected $secondLineGoals;
    protected $firstLineName;
    protected $secondLineName;
    protected $eventName;

    public function getAssists()
    {
        return $this->assists;
    }

    public function getGoals()
    {
        return $this->goals;
    }

    public function getHits()
    {
        return $this->hits;
    }

    public function getManOfMatch()
    {
        return $this->manOfMatch;
    }

    public function getPlayer()
    {
        return $this->player;
    }

    public function getSaves()
    {
        return $this->saves;
    }

    public function getEventDate()
    {
        return $this->eventDate;
    }

    public function getLineupName()
    {
        return $this->lineupName;
    }

    public function getResult()
    {
        return $this->result;
    }

    public function setAssists($assists)
    {
        $this->assists = $assists;
    }

    public function setGoals($goals)
    {
        $this->goals = $goals;
    }

    public function setHits($hits)
    {
        $this->hits = $hits;
    }

    public function setManOfMatch($manOfMatch)
    {
        $this->manOfMatch = $manOfMatch;
    }

    public function setPlayer($player)
    {
        $this->player = $player;
    }

    public function setSaves($saves)
    {
        $this->saves = $saves;
    }

    public function setEventDate($eventDate)
    {
        $this->eventDate = $eventDate;
    }

    public function setLineupName($lineupName)
    {
        $this->lineupName = $lineupName;
    }

    public function setResult($result)
    {
        $this->result = $result;
    }

    public function getTeamName()
    {
        return $this->teamName;
    }

    public function setTeamName($teamName)
    {
        $this->teamName = $teamName;
    }

    public function getFirstLineGoals()
    {
        return $this->firstLineGoals;
    }

    public function getSecondLineGoals()
    {
        return $this->secondLineGoals;
    }

    public function setFirstLineGoals($firstLineGoals)
    {
        $this->firstLineGoals = $firstLineGoals;
    }

    public function setSecondLineGoals($secondLineGoals)
    {
        $this->secondLineGoals = $secondLineGoals;
    }

    public function getLineupPosition()
    {
        return $this->lineupPosition;
    }

    public function setLineupPosition($lineupPosition)
    {
        $this->lineupPosition = $lineupPosition;
    }

    public function getLineupId()
    {
        return $this->lineupId;
    }

    public function setLineupId($lineupId)
    {
        $this->lineupId = $lineupId;
    }

    public function getEventId()
    {
        return $this->eventId;
    }

    public function setEventId($eventId)
    {
        $this->eventId = $eventId;
    }

    public function getFirstLineName()
    {
        return $this->firstLineName;
    }

    public function getSecondLineName()
    {
        return $this->secondLineName;
    }

    public function setFirstLineName($firstLineName)
    {
        $this->firstLineName = $firstLineName;
    }

    public function setSecondLineName($secondLineName)
    {
        $this->secondLineName = $secondLineName;
    }

    function getEventName()
    {
        return $this->eventName;
    }

    function setEventName($eventName)
    {
        $this->eventName = $eventName;
    }

    public function getResultStatus()
    {

        //wins, draws, looses
        if ($this->getFirstLineGoals() == $this->getSecondLineGoals())
        {
            return 'draw';
        }
        elseif ($this->getFirstLineGoals() > $this->getSecondLineGoals() && $this->getLineupPosition() == 'first_line')
        {
            return 'win';
        }
        elseif ($this->getFirstLineGoals() < $this->getSecondLineGoals() && $this->getLineupPosition() == 'second_line')
        {
            return 'win';
        }
        else
        {
            return 'loose';
        }
    }

    public function getEventDateFormated($format='d.m. Y')
    {
        if(is_string($this->getEventDate()))
        {
            $eventDate = \DateTime::createFromFormat('Y-m-d H:i:s', $this->getEventDate());
            return $eventDate->format($format);
        }
        
        return $this->getEventDate()->format($format);
    }
    
    public function isManOfMatch()
    {
        if($this->getManOfMatch() == 1)
        {
            return true;
        }
        return false;
    }


}
