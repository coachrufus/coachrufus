<?php

namespace Webteamer\Player\Model;

use Core\EntityMapper as EntityMapper;
use Core\ServiceLayer as ServiceLayer;

class Player extends \User\Model\User {

    protected $repository;
    protected $dataMapper;

    public function __construct()
    {
        $this->dataMapper = new PlayerMapper();
        $this->mapper_rules = $this->dataMapper->getDataMapperRules();
    }

    public function getDataMapperRules()
    {
        return $this->mapper_rules;
    }

    public function getRepository()
    {
        if (null == $this->repository)
        {
            $this->repository = ServiceLayer::getService("PlayerRepository");
        }
        return $this->repository;
    }

    public function setRepository($repository)
    {
        $this->repository = $repository;
    }

    protected $id;
    protected $userId;
    protected $name;
    protected $surname;
    protected $email;
    protected $birthday;
    protected $birthmonth;
    protected $birthyear;
    protected $lang;
    protected $localityAliasId;
    protected $nickname;
    protected $locality;
    protected $localityId;
    protected $sports = array();
    protected $stats;

    public function setId($val)
    {
        $this->id = $val;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setUserId($val)
    {
        $this->userId = $val;
    }

    public function getUserId()
    {
        return $this->userId;
    }

    public function setName($val)
    {
        $this->name = $val;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setSurname($val)
    {
        $this->surname = $val;
    }

    public function getSurname()
    {
        return $this->surname;
    }

    public function setEmail($val)
    {
        $this->email = $val;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getBirthday()
    {
        return $this->birthday;
    }

    public function getBirthmonth()
    {
        return $this->birthmonth;
    }

    public function getBirthyear()
    {
        return $this->birthyear;
    }

    public function getLang()
    {
        return $this->lang;
    }

    public function getLocalityAliasId()
    {
        return $this->localityAliasId;
    }

    public function getLocalityId()
    {
        return $this->localityId;
    }

    public function getNickname()
    {
        return $this->nickname;
    }

  

    /**
     * 
     * @param newVal
     */
    public function setBirthday($newVal)
    {
        $this->birthday = $newVal;
    }

    /**
     * 
     * @param newVal
     */
    public function setBirthmonth($newVal)
    {
        $this->birthmonth = $newVal;
    }

    /**
     * 
     * @param newVal
     */
    public function setBirthyear($newVal)
    {
        $this->birthyear = $newVal;
    }

    /**
     * 
     * @param newVal
     */
    public function setLang($newVal)
    {
        $this->lang = $newVal;
    }

    /**
     * 
     * @param newVal
     */
    public function setLocalityAliasId($newVal)
    {
        $this->localityAliasId = $newVal;
    }

    /**
     * 
     * @param newVal
     */
    public function setNickname($newVal)
    {
        $this->nickname = $newVal;
    }

 

    public function getLocality()
    {
        return $this->locality;
    }

  

    /**
     * 
     * @param newVal
     */
    public function setLocality($newVal)
    {
        $this->locality = $newVal;
    }

 
    public function getFullName()
    {
        return $this->getName().' '.$this->getSurname();
    }
    
    


public function getLocalityName()
{
    return  $this->getLocality()->getName();
}

public  function getSports() {
return $this->sports;
}

public  function setSports($sports) {
$this->sports = $sports;
}


public function addSport($sport)
{
    $this->sports[] = $sport;
}

public  function getStats() {
return $this->stats;
}

public  function setStats($stats) {
$this->stats = $stats;
}




}
