<?php
	
namespace Webteamer\Player\Model;
use Core\EntityMapper as EntityMapper;
use Core\ServiceLayer as ServiceLayer;
		
class PlayerSport {	

	
	
		
private $repository;
private $mapper_rules  = array(
	'id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => true
							
					),
'sport_id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => false
							
					),
'player_id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => false
							
					),
'level' => array(
					'type' => 'string',
					'max_length' => '50',
					'required' => false
							
					),
'often' => array(
					'type' => 'string',
					'max_length' => '50',
					'required' => false
							
					),
'availability' => array(
					'type' => 'string',
					'max_length' => '50',
					'required' => false
							
					),
'interest' => array(
					'type' => 'string',
					'max_length' => '50',
					'required' => false
							
					),
'note' => array(
					'type' => 'string',
					'max_length' => '',
					'required' => false
							
					),
'involved_type' => array(
					'type' => 'string',
					'max_length' => '255',
					'required' => false
							
					),

);
	
public function getDataMapperRules()
{
	return $this->mapper_rules;
}

	
protected $id;

protected $sportId;

protected $playerId;

protected $level;

protected $often;

protected $availability;

protected $interest;

protected $note;

protected $involvedType;
protected $sport;

				
public function setId($val){$this->id = $val;}

public function getId(){return $this->id;}
	
public function setSportId($val){$this->sportId = $val;}

public function getSportId(){return $this->sportId;}
	
public function setPlayerId($val){$this->playerId = $val;}

public function getPlayerId(){return $this->playerId;}
	
public function setLevel($val){$this->level = $val;}

public function getLevel(){return $this->level;}
	
public function setOften($val){$this->often = $val;}

public function getOften(){return $this->often;}
	
public function setAvailability($val){$this->availability = $val;}

public function getAvailability(){return $this->availability;}
	
public function setInterest($val){$this->interest = $val;}

public function getInterest(){return $this->interest;}
	
public function setNote($val){$this->note = $val;}

public function getNote(){return $this->note;}
	
public function setInvolvedType($val){$this->involvedType = $val;}

public function getInvolvedType(){return $this->involvedType;}

public  function getSport() {
return $this->sport;
}

public  function setSport($sport) {
$this->sport = $sport;
}

public function getSportName()
{
    return $this->getSport()->getName();
}



}

		