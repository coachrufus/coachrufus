<?php

namespace Webteamer\Player\Model;

class SetMatch {

    protected $players;
    protected $sets;

    function getPlayers()
    {
        return $this->players;
    }

    function getSets()
    {
        return $this->sets;
    }

    function setPlayers($players)
    {
        $this->players = $players;
    }

    function setSets($sets)
    {
        $this->sets = $sets;
    }
    
    public function addPlayer($lineupPosition,$playerData)
    {
        $this->players[$lineupPosition] = $playerData;
    }
    
    public function addSet($setPosition,$setData)
    {
        $this->sets[$setPosition] = $setData;
    }
    
    public function getSet($index)
    {
        if(array_key_exists($index, $this->sets))
        {
            return $this->sets[$index];
        }
        
    }
    
    public function getPlayerSetPoints($lineupPlayerId,$setIndex)
    {
        $set = $this->getSet($setIndex);
        return $set['points'][$lineupPlayerId];
    }
    
    public function getSetDuration($setIndex)
    {
        $set = $this->getSet($setIndex);
        if(null == $set)
        {
            return '00:00';
        }
        
        return $set['duration'];
    }
    
    public function getTotalSetPoints($lineupPosition,$setIndex)
    {
        $set = $this->getSet($setIndex);
       
        
        return $set['total_points'][$lineupPosition];
    }

}
