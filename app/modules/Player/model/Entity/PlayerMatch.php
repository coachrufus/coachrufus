<?php

namespace Webteamer\Player\Model;

use Core\EntityMapper as EntityMapper;
use Core\ServiceLayer as ServiceLayer;

class PlayerMatch {

    private $repository;
    private $mapper_rules = array(
        'assists' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'event_id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'goals' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'man_of_match' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'match_date' => array(
            'type' => 'datetime',
            'max_length' => '',
            'required' => false
        ),
        'one_points' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'player_id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'saves' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'shots' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'sport_id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'match_team_id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'three_points' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'two_points' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => true
        ),
        'team_id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'match_id' => array(
            'type' => 'string',
            'max_length' => '255',
            'required' => false
        ),
    );

    public function getDataMapperRules()
    {
        return $this->mapper_rules;
    }

    protected $assists;
    protected $eventId;
    protected $goals;
    protected $manOfMatch;
    protected $matchDate;
    protected $onePoints;
    protected $playerId;
    protected $saves;
    protected $shots;
    protected $sportId;
    protected $matchTeamId;
    protected $threePoints;
    protected $twoPoints;
    protected $id;
    protected $teamId;
    protected $matchId;
    protected $player;
    protected $ratings;

    public function setAssists($val)
    {
        $this->assists = $val;
    }

    public function getAssists()
    {
        return $this->assists;
    }

    public function setEventId($val)
    {
        $this->eventId = $val;
    }

    public function getEventId()
    {
        return $this->eventId;
    }

    public function setGoals($val)
    {
        $this->goals = $val;
    }

    public function getGoals()
    {
        return $this->goals;
    }

    public function setManOfMatch($val)
    {
        $this->manOfMatch = $val;
    }

    public function getManOfMatch()
    {
        return $this->manOfMatch;
    }

    public function setMatchDate($val)
    {
        $this->matchDate = $val;
    }

    public function getMatchDate()
    {
        return $this->matchDate;
    }

    public function setOnePoints($val)
    {
        $this->onePoints = $val;
    }

    public function getOnePoints()
    {
        return $this->onePoints;
    }

    public function setPlayerId($val)
    {
        $this->playerId = $val;
    }

    public function getPlayerId()
    {
        return $this->playerId;
    }

    public function setSaves($val)
    {
        $this->saves = $val;
    }

    public function getSaves()
    {
        return $this->saves;
    }

    public function setShots($val)
    {
        $this->shots = $val;
    }

    public function getShots()
    {
        return $this->shots;
    }

    public function setSportId($val)
    {
        $this->sportId = $val;
    }

    public function getSportId()
    {
        return $this->sportId;
    }

    public function setMatchTeamId($val)
    {
        $this->matchTeamId = $val;
    }

    public function getMatchTeamId()
    {
        return $this->matchTeamId;
    }

    public function setThreePoints($val)
    {
        $this->threePoints = $val;
    }

    public function getThreePoints()
    {
        return $this->threePoints;
    }

    public function setTwoPoints($val)
    {
        $this->twoPoints = $val;
    }

    public function getTwoPoints()
    {
        return $this->twoPoints;
    }

    public function setId($val)
    {
        $this->id = $val;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setTeamId($val)
    {
        $this->teamId = $val;
    }

    public function getTeamId()
    {
        return $this->teamId;
    }

    public function getMatchId()
    {
        return $this->matchId;
    }

    public function setMatchId($matchId)
    {
        $this->matchId = $matchId;
    }

    public function getPlayerName()
    {
        return $this->getPlayer()->getName();
    }

   

    
    public  function getPlayer() {
return $this->player;
}

public  function setPlayer($player) {
$this->player = $player;
}

public  function getRatings() {
return $this->ratings;
}

public  function setRatings($ratings) {
$this->ratings = $ratings;
}




}
