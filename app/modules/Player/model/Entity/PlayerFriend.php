<?php

namespace CR\Player\Model;

use Core\EntityMapper as EntityMapper;
use Core\ServiceLayer as ServiceLayer;

class PlayerFriend {

    private $repository;
    private $mapper_rules = array(
        'id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => true
        ),
        'player_id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'friend_id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'created_at' => array(
            'type' => 'datetime',
            'max_length' => '',
            'required' => false
        ),
        'status' => array(
            'type' => 'string',
            'max_length' => '50',
            'required' => false
        ),
        'friend_created_at' => array(
            'type' => 'datetime',
            'max_length' => '',
            'required' => false
        ),
    );

    public function getDataMapperRules()
    {
        return $this->mapper_rules;
    }

    protected $id;
    protected $playerId;
    protected $friendId;
    protected $createdAt;
    protected $status;
    protected $friendCreatedAt;
    protected $player;

    public function setId($val)
    {
        $this->id = $val;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setPlayerId($val)
    {
        $this->playerId = $val;
    }

    public function getPlayerId()
    {
        return $this->playerId;
    }

    public function setFriendId($val)
    {
        $this->friendId = $val;
    }

    public function getFriendId()
    {
        return $this->friendId;
    }

    public function setCreatedAt($val)
    {
        $this->createdAt = $val;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function getFriendCreatedAt()
    {
        return $this->friendCreatedAt;
    }

    public function setFriendCreatedAt($friendCreatedAt)
    {
        $this->friendCreatedAt = $friendCreatedAt;
    }
    
    public  function getStatus() {
return $this->status;
}

public  function setStatus($status) {
$this->status = $status;
}



    public function getPlayer()
    {
        return $this->player;
    }

    public function setPlayer($player)
    {
        $this->player = $player;
    }

    public function getFullName()
    {
        return $this->getPlayer()->getFullname();
    }

}
