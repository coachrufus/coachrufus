<?php

namespace Webteamer\Player\Model;

class PlayerBasketballStat extends PlayerStat {

    protected $points1;
    protected $points2;
    protected $points3;

    function getPoints1()
    {
        return $this->points1;
    }

    function getPoints2()
    {
        return $this->points2;
    }

    function getPoints3()
    {
        return $this->points3;
    }

    function setPoints1($points1)
    {
        $this->points1 = $points1;
    }

    function setPoints2($points2)
    {
        $this->points2 = $points2;
    }

    function setPoints3($points3)
    {
        $this->points3 = $points3;
    }

    public function getAveragePoints1()
    {
        if (0 == $this->getGamePlayed())
        {
            return 0;
        }

        return round($this->getPoints1() / $this->getGamePlayed(), 2);
    }

    public function getAveragePoints2()
    {
        if (0 == $this->getGamePlayed())
        {
            return 0;
        }

        return round($this->getPoints2() / $this->getGamePlayed(), 2);
    }

    public function getAveragePoints3()
    {
        if (0 == $this->getGamePlayed())
        {
            return 0;
        }

        return round($this->getPoints3() / $this->getGamePlayed(), 2);
    }

    public function getCRSCriteria()
    {
        $default = array(
            'win_match' => 1,
            'draw_match' => 1,
            'points1' => 1,
            'points2' => 1,
            'points3' => 1
        );

        $crs = $default;

        if (null != $this->crsCriteria)
        {
            $crs = $this->crsCriteria;
        }
        return $crs;
    }

    public function getCRS()
    {

        $criteria = $this->getCRSCriteria();
        return
                ($this->getWins() * $criteria['win_match']) +
                ($this->getDraws() * $criteria['draw_match']) +
                ($this->getPoints1() * $criteria['points1']) +
                ($this->getPoints2() * $criteria['points2']) +
                ($this->getPoints3() * $criteria['points3']);
    }

    public function getAverageCRS()
    {
        $val = 0;
        if (0 < $this->getGamePlayed())
        {
            $val = round($this->getCRS() / $this->getGamePlayed(), 2);
        }

        return $val;
    }
    
    public function getPoints1MaxPercent()
    {
        if ($this->getMaxValue('points1') > 0)
        {
            return round($this->getPoints1() / $this->getMaxValue('points1'), 2) * 100;
        }
        return 0;
    }
    public function getPoints2MaxPercent()
    {
        if ($this->getMaxValue('points2') > 0)
        {
            return round($this->getPoints2() / $this->getMaxValue('points2'), 2) * 100;
        }
        return 0;
    }
    public function getPoints3MaxPercent()
    {
        if ($this->getMaxValue('points3') > 0)
        {
            return round($this->getPoints3() / $this->getMaxValue('points3'), 2) * 100;
        }
        return 0;
    }

}
