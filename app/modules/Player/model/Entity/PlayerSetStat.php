<?php
namespace Webteamer\Player\Model;

class PlayerSetStat extends PlayerStat
{

    
    public function getCRSCriteria()
    {
        $default = array(
            'win_match' => 1,
            'draw_match' => 1,
            'win_set' => 1,
            'draw_set' => 1,
            'cru' => 1
        );
        
        $crs = $default;

        if (null != $this->crsCriteria)
        {
            $crs = $this->crsCriteria;
        }
        return $crs;
    }
    
    public function getCRS()
    {

        $criteria = $this->getCRSCriteria();
        return
                ($this->getWins() * $criteria['win_match']) +
                ($this->getDraws() * $criteria['draw_match']) +
                ($this->getWonSet() * $criteria['win_set']) +
                ($this->getDrawSet() * $criteria['draw_set']) +
                ($this->getCRU() * $criteria['cru']);
    }
    
    public function getAverageCRS()
    {
        $val = 0;
        if (0 < $this->getGamePlayed())
        {
            $val = round($this->getCRS() / $this->getGamePlayed(), 2);
        }

        return $val;
    }
    




}