<?php

namespace CR\Player\Model;

use Core\EntityMapper as EntityMapper;
use Core\ServiceLayer as ServiceLayer;

class PlayerWallPost {

    private $repository;
    private $mapper_rules = array(
        'id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => true
        ),
        'created_at' => array(
            'type' => 'datetime',
            'max_length' => '',
            'required' => true
        ),
        'author_id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => true
        ),
        'status' => array(
            'type' => 'string',
            'max_length' => '50',
            'required' => true
        ),
        'body' => array(
            'type' => 'string',
            'max_length' => '',
            'required' => true
        ),
    );

    public function getDataMapperRules()
    {
        return $this->mapper_rules;
    }

    protected $id;
    protected $createdAt;
    protected $authorId;
    protected $status;
    protected $body;
    protected $author;
    protected $comments;
    protected $commentsCount;

    public function setId($val)
    {
        $this->id = $val;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setCreatedAt($val)
    {
        $this->createdAt = $val;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setAuthorId($val)
    {
        $this->authorId = $val;
    }

    public function getAuthorId()
    {
        return $this->authorId;
    }

    public function setStatus($val)
    {
        $this->status = $val;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setBody($val)
    {
        $this->body = $val;
    }

    public function getBody()
    {
        return $this->body;
    }

    function getAuthor()
    {
        return $this->author;
    }

    function setAuthor($author)
    {
        $this->author = $author;
    }

    public function getFormatedCreatedAt()
    {
        return $this->getCreatedAt()->format('d.m.Y H:i:s');
    }

    function getComments()
    {
        return $this->comments;
    }

    function setComments($comments)
    {
        $this->comments = $comments;
    }

    function getCommentsCount()
    {
        return $this->commentsCount;
    }

    function setCommentsCount($commentsCount)
    {
        $this->commentsCount = $commentsCount;
    }

}
