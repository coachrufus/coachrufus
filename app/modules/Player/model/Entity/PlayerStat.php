<?php

namespace Webteamer\Player\Model;

/**
 * Toal player stats
 */
class PlayerStat {

    protected $assists;
    protected $goals;
    protected $hits;
    protected $manOfMatch;
    protected $player;
    protected $saves;
    protected $gamePlayed;
    protected $wins;
    protected $looses;
    protected $draws;
    protected $plusPoints;
    protected $minusPoints;
    protected $playerLastGames;
    protected $crsCriteria;
    protected $maxValues;
    protected $wonSet;
    protected $looseSet;
    protected $drawSet;
    protected $cru;
    protected $goalkeeperGoals;
    protected $goalkeeperShotouts;
    protected $goalkeeperGamePlayes;

    public function getAssists()
    {
        return $this->assists;
    }

    public function getGoals()
    {
        return $this->goals;
    }

    public function getManOfMatch()
    {
        return $this->manOfMatch;
    }

    public function getPlayer()
    {
        return $this->player;
    }

    public function getSaves()
    {
        return $this->saves;
    }

    /**
     * 
     * @param newVal
     */
    public function setAssists($newVal)
    {
        $this->assists = $newVal;
    }

    /**
     * 
     * @param newVal
     */
    public function setGoals($newVal)
    {
        $this->goals = $newVal;
    }

    /**
     * 
     * @param newVal
     */
    public function setManOfMatch($newVal)
    {
        $this->manOfMatch = $newVal;
    }

    /**
     * 
     * @param newVal
     */
    public function setPlayer($newVal)
    {
        $this->player = $newVal;
    }

    /**
     * 
     * @param newVal
     */
    public function setSaves($newVal)
    {
        $this->saves = $newVal;
    }

    public function getGamePlayed()
    {
        return $this->gamePlayed;
    }

    /**
     * 
     * @param newVal
     */
    public function setGamePlayed($newVal)
    {
        $this->gamePlayed = $newVal;
    }

    public function getWins()
    {
        return $this->wins;
    }

    public function getLooses()
    {
        return $this->looses;
    }

    public function getDraws()
    {
        return $this->draws;
    }

    public function setWins($wins)
    {
        $this->wins = $wins;
    }

    public function setLooses($looses)
    {
        $this->looses = $looses;
    }

    public function setDraws($draws)
    {
        $this->draws = $draws;
    }

    public function getMinusPoints()
    {
        return $this->minusPoints;
    }

    public function setMinusPoints($minusPoints)
    {
        $this->minusPoints = $minusPoints;
    }

    public function getHits()
    {
        return $this->hits;
    }

    public function setHits($hits)
    {
        $this->hits = $hits;
    }

    public function getPlayerLastGames()
    {
        return $this->playerLastGames;
    }

    public function setPlayerLastGames($playerLastGames)
    {
        $this->playerLastGames = $playerLastGames;
    }

    function getMaxValues()
    {
        return $this->maxValues;
    }

    function setMaxValues($maxValues)
    {
        $this->maxValues = $maxValues;
    }

    public function getMaxValue($index)
    {
        return $this->maxValues[$index];
    }

    public function getPlayerName()
    {
        return $this->getPlayer()->getName();
    }
    
    
    public function getGoalkeeperGoals()
    {
        return $this->goalkeeperGoals;
    }

    public function getGoalkeeperShotouts()
    {
        return $this->goalkeeperShotouts;
    }

    public function setGoalkeeperGoals($goalkeeperGoals)
    {
        $this->goalkeeperGoals = $goalkeeperGoals;
    }

    public function setGoalkeeperShotouts($goalkeeperShotouts)
    {
        $this->goalkeeperShotouts = $goalkeeperShotouts;
    }

    
    public function getAverageGoals()
    {
        if (0 == $this->getGamePlayed())
        {
            return 0;
        }
        
        //$gp = $this->getGamePlayed() - $this->getGoalkeeperGamePlayes();
        $gp = $this->getGamePlayed();
        return round($this->getGoals() / $gp, 2);
    }

    public function getAverageAssists()
    {
        if (0 == $this->getGamePlayed())
        {
            return 0;
        }
        //$gp = $this->getGamePlayed() - $this->getGoalkeeperGamePlayes();
        $gp = $this->getGamePlayed();
        return round($this->getAssists() / $gp, 2);
    }

    public function getAverageGoalPoints()
    {
        $criteria = $this->getPointsCriteria();
        return $this->getAverageGoals() * $criteria['goal'];
    }

    public function getAverageAssistPoints()
    {
        $criteria = $this->getPointsCriteria();
        return $this->getAverageAssists() * $criteria['assists'];
    }

    public function getGA()
    {
        return $this->getGoals() + $this->getAssists();
    }

    public function getAverageGA()
    {
        if (0 == $this->getGamePlayed())
        {
            return 0;
        }
        //$gp = $this->getGamePlayed() - $this->getGoalkeeperGamePlayes();
        $gp = $this->getGamePlayed();
        return round($this->getGA() /  $gp, 2);
    }

    public function getAverageManOfMatch()
    {
        if (0 == $this->getGamePlayed())
        {
            return 0;
        }

        return round($this->getManOfMatch() / $this->getGamePlayed(), 2);
    }

    public function getWinsPrediction()
    {
        $val = 0;
        if ($this->getGamePlayed() > 0)
        {
            $val = round((2 * $this->getWins() + $this->getDraws() ) / (2 * $this->getGamePlayed()), 2) * 100;
        }
        return $val;
    }

    public function getPlusPoints()
    {
        return $this->plusPoints;
    }

    public function setPlusPoints($plusPoints)
    {
        $this->plusPoints = $plusPoints;
    }

    public function getPlusMinusDiff()
    {
        return $this->getPlusPoints() - $this->getMinusPoints();
    }

    public function getKbi()
    {
        $criteria = $this->getKBICriteria();
        return
                ($this->getGoals() * $criteria['goal']) +
                ($this->getAssists() * $criteria['assists']) +
                ($this->getWins() * $criteria['game_win']) +
                ($this->getDraws() * $criteria['game_draw']) +
                ($this->getManOfMatch() * $criteria['man_of_match'])+
                ($this->getGoalkeeperShotouts() * $criteria['shotout']);
    }

    public function getCRS()
    {
        return $this->getKbi();
    }

    public function getCRSGroup()
    {
        $criteria = $this->getCRSCriteria();
        $group = array();
        $group['goal'] = $this->getGoals() * $criteria['goal'];
        $group['assists'] = $this->getAssists() * $criteria['assists'];
        $group['game_win'] = $this->getWins() * $criteria['game_win'];
        $group['game_draw'] = $this->getDraws() * $criteria['game_draw'];
        $group['man_of_match'] = $this->getManOfMatch();
        $group['shotout'] = $this->getGoalkeeperShotouts();
        return $group;
    }

    public function setCRSCriteria($val)
    {
        if (!empty($val))
        {
            $this->crsCriteria = $val;
        }
    }

    public function getKBICriteria()
    {
        $defaultKbi = array(
            'goal' => 1,
            'assists' => 1,
            'game_win' => 1,
            'game_draw' => 1,
            'man_of_match' => 1,
            'shotout' => 1
        );
        $kbi = $defaultKbi;
        if (null != $this->crsCriteria)
        {
            $kbi = $this->crsCriteria;
        }
        
        $finalKbi = array();
        foreach($defaultKbi as $key => $val)
        {
            if(array_key_exists($key, $kbi))
            {
                 $finalKbi[$key] = $kbi[$key];
            }
            else
            {
                $finalKbi[$key] = $val;
            }
           
        }


        return $finalKbi;
    }

    public function getCRSCriteria()
    {
        return $this->getKBICriteria();
    }

    public function getPointsCriteria()
    {
        $points = array(
            'goal' => 1,
            'assists' => 1,
            'game_win' => 1,
            'game_draw' => 1,
            'man_of_match' => 1,
             'shotout' => 1
        );
        return $points;
    }

    public function getAverageKbi()
    {
        $val = 0;
        if (0 < $this->getGamePlayed())
        {
            $val = round($this->getKbi() / $this->getGamePlayed(), 2);
        }

        return $val;
    }

    public function getAverageCRS()
    {
        return $this->getAverageKbi();
    }

    public function getOveralEffectivyPlayer()
    {
        // celkova uspesnost/(celkova uspesnot + starring)
    }

    public function getCurrentGameEfficientPoints($currentGame, $index)
    {
        $pointsCriteria = $this->getPointsCriteria();
        if (false != $currentGame && null != $currentGame)
        {
            $efficientPoints = ($index * ($currentGame['goal'] * $pointsCriteria['goal'])) + ($index * ($currentGame['assist'] * $pointsCriteria['assists']));
        }
        else
        {
            $efficientPoints = ($index * $this->getAverageGoalPoints()) + ($index * $this->getAverageAssistPoints());
        }

        return $efficientPoints;
    }

    /**
     * last games efficiency, is count from last team matches, not from last player games!!!
     * @param type $playerPoints
     * @return type
     */
    public function getLastGamesEfficiency($playerPoints = null)
    {
        if (null == $playerPoints)
        {
            $playerPoints = $this->getPlayerLastGames();
        }


        $pointsCriteria = $this->getPointsCriteria();
        $efficientPoints = 0.45 * $this->getAverageGoalPoints() + 0.45 * $this->getAverageAssistPoints();
        $efficientPointsIndex = array(0 => 0.25, 1 => 0.15, 2 => 0.1, 3 => 0.05);
        
        foreach ($efficientPointsIndex as $index)
        {
            $currentGame = current($playerPoints);
            $efficientPoints += $this->getCurrentGameEfficientPoints($currentGame, $index);


            next($playerPoints);
        }
        
        /*
        if('NaN' == $playerPoints)
        {
           $playerPoints = 0; 
        }
     */
        return round($efficientPoints, 2);
    }
    
    public function getLastGamesGoalEfficiency($playerPoints  = null)
    {
        if (null == $playerPoints)
        {
            $playerPoints = $this->getPlayerLastGames();
        }
        $efficientPoints = 0.45 * $this->getAverageGoals();
        $efficientPointsIndex = array(0 => 0.25, 1 => 0.15, 2 => 0.1, 3 => 0.05);
        foreach ($efficientPointsIndex as $index)
        {
            $currentGame = current($playerPoints);
            $efficientPoints += $currentGame['goal']*$index;
            next($playerPoints);
        }
        return round($efficientPoints, 2);
    }
    
     public function getLastGamesAssistEfficiency($playerPoints  = null)
    {
        if (null == $playerPoints)
        {
            $playerPoints = $this->getPlayerLastGames();
        }
        $efficientPoints = 0.45 * $this->getAverageAssists();
        $efficientPointsIndex = array(0 => 0.25, 1 => 0.15, 2 => 0.1, 3 => 0.05);
        foreach ($efficientPointsIndex as $index)
        {
            $currentGame = current($playerPoints);
            $efficientPoints += $currentGame['assist']*$index;
            next($playerPoints);
        }
        return round($efficientPoints, 2);
    }
    
    public function getLastGamesCRSEfficiency($playerPoints  = null)
    {
        if (null == $playerPoints)
        {
            $playerPoints = $this->getPlayerLastGames();
        }
       // t_dump($playerPoints);
       $pointsCriteria = $this->getPointsCriteria();
        $efficientPoints = (0.45 * $this->getAverageGoalPoints()) + (0.45 * $this->getAverageAssistPoints());
        //t_dump('eff=0.45*'.$this->getAverageGoalPoints().'='.$efficientPoints);
        $efficientPointsIndex = array(0 => 0.25, 1 => 0.15, 2 => 0.1, 3 => 0.05);
        foreach ($efficientPointsIndex as $index)
        {
            $currentGame = current($playerPoints);
            $efficientPoints += ($currentGame['goal']*$index*$pointsCriteria['goal']) + ($currentGame['assist']*$index*$pointsCriteria['assists']) ;
            //t_dump('eff='.$index.'*'.$currentGame['goal'].'='.($currentGame['goal']*$index));
            next($playerPoints);
            //next($playerPoints);
        }

        return round($efficientPoints, 2);
        
    }

    public function getGamePlayedMaxPercent()
    {
        if ($this->getMaxValue('game_played') > 0)
        {
            return round($this->getGamePlayed() / $this->getMaxValue('game_played'), 2) * 100;
        }

        return 0;
    }

    public function getWinsMaxPercent()
    {
        if ($this->getMaxValue('win') > 0)
        {
            return round($this->getWins() / $this->getMaxValue('win'), 2) * 100;
        }

        return 0;
    }

    public function getDrawsMaxPercent()
    {
        if ($this->getMaxValue('draw') > 0)
        {
            return round($this->getDraws() / $this->getMaxValue('draw'), 2) * 100;
        }
        return 0;
    }

    public function getLooseMaxPercent()
    {
        if ($this->getMaxValue('looses') > 0)
        {
            return round($this->getLooses() / $this->getMaxValue('looses'), 2) * 100;
        }
        return 0;
    }

    public function getWinsPredictionAreaPercent()
    {
        return round(($this->getWinsPrediction() / 10)) * 10;
    }

    public function getGoalMaxPercent()
    {
        if ($this->getMaxValue('goal') > 0)
        {
            return round($this->getGoals() / $this->getMaxValue('goal'), 2) * 100;
        }
        return 0;
    }

    public function getAssistsMaxPercent()
    {
        if ($this->getMaxValue('assist') > 0)
        {
            return round($this->getAssists() / $this->getMaxValue('assist'), 2) * 100;
        }
        return 0;
    }

    public function getMoMMax()
    {
        return $this->getMaxValue('mom');
    }

    public function getAverageGoalsAreaPercent()
    {
        $percent = round(($this->getAverageGoals() * 50));

        if ($percent > 100)
        {
            $percent = 100;
        }
        $percent = round(($percent / 10)) * 10;
        return $percent;
    }

    public function getAverageAssistsAreaPercent()
    {
        $percent = round(($this->getAverageAssists() * 50));

        if ($percent > 100)
        {
            $percent = 100;
        }
        $percent = round(($percent / 10)) * 10;
        return $percent;
    }

    public function getGAMaxPercent()
    {
        if ($this->getMaxValue('ga') > 0)
        {
            return round(($this->getGoals() + $this->getAssists()) / ($this->getMaxValue('ga')), 2) * 100;
        }
        return 0;
    }

    public function getAverageGAAreaPercent()
    {
        $percent = round(($this->getAverageGA() * 50));

        if ($percent > 100)
        {
            $percent = 100;
        }
        $percent = round(($percent / 10)) * 10;
        return $percent;
    }

    public function getCRSMaxPercent()
    {
        if ($this->getMaxValue('crs') > 0)
        {
            return round($this->getCRS() / ($this->getMaxValue('crs')), 2) * 100;
        }
        return 0;
    }

    public function getAverageCRSAreaPercent()
    {
        $percent = round(($this->getAverageCRS() * 5));

        if ($percent > 100)
        {
            $percent = 100;
        }
        $percent = round(($percent / 10)) * 10;
        return $percent;
    }

    public function getMoMMaxPercent()
    {
        if ($this->getMaxValue('mom') > 0)
        {
            return round(($this->getManOfMatch()) / ($this->getMaxValue('mom')), 2) * 100;
        }
        return 0;
    }

    public function getAverageMoMAreaPercent()
    {
        $percent = round(($this->getAverageManOfMatch() * 100));

        if ($percent > 100)
        {
            $percent = 100;
        }
        $percent = round(($percent / 10)) * 10;
        return $percent;
    }

    public function getPlusMinusDiffOffset($index)
    {
        $maxPlus = $this->getMaxValue('pm-diff-max');
        $maxMin = abs($this->getMaxValue('pm-diff-min'));

        $total = $maxPlus + $maxMin;
        if($total > 0)
        {
            $unit = 100 / $total;
            $offsets['unit'] = $unit;
            $offsets['plusOffset'] = ceil($unit * $maxMin);
            $offsets['length'] = ceil($unit * abs($this->getPlusMinusDiff()));
            $offsets['minusMargin'] = $offsets['plusOffset'] - $offsets['length'];
            return $offsets[$index];
        }
    }

    function getWonSet()
    {
        return $this->wonSet;
    }

    function setWonSet($wonSet)
    {
        $this->wonSet = $wonSet;
    }
    
    function getLooseSet() {
return $this->looseSet;
}

 function getDrawSet() {
return $this->drawSet;
}

 function setLooseSet($looseSet) {
$this->looseSet = $looseSet;
}

 function setDrawSet($drawSet) {
$this->drawSet = $drawSet;
}


function getCRU() {
return $this->getPlusMinusDiff()*10;
}


public  function getGoalkeeperGamePlayes() {
return $this->goalkeeperGamePlayes;
}

public  function setGoalkeeperGamePlayes($goalkeeperGamePlayes) {
$this->goalkeeperGamePlayes = $goalkeeperGamePlayes;
}



public function getGoalAgainstAverage()
{
    if ($this->getGoalkeeperGoals() > 0)
    {
        return round($this->getGoalkeeperGoals()/$this->getGoalkeeperGamePlayes(),2);
    }
    return 0;
}

}
