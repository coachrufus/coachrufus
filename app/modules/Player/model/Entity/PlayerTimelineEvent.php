<?php

namespace Webteamer\Player\Model;

use Core\EntityMapper as EntityMapper;
use Core\ServiceLayer as ServiceLayer;

class PlayerTimelineEvent {

    private $repository;
    private $mapper_rules = array(
        'id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => true
        ),
        'user_id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'hit_type' => array(
            'type' => 'string',
            'max_length' => '50',
            'required' => false
        ),
        'hit_time' => array(
            'type' => 'string',
            'max_length' => '50',
            'required' => false
        ),
        'note' => array(
            'type' => 'string',
            'max_length' => '',
            'required' => false
        ),
        'lineup_id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'match_team_name' => array(
            'type' => 'string',
            'max_length' => '100',
            'required' => false
        ),
        'event_id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'team_position' => array(
            'type' => 'string',
            'max_length' => '50',
            'required' => false
        ),
        'event_date' => array(
            'type' => 'datetime',
            'max_length' => '',
            'required' => false
        ),
        'lineup_position' => array(
            'type' => 'string',
            'max_length' => '50',
            'required' => false
        ),
        'lineup_player_id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'team_id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'points' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'duration_time' => array(
            'type' => 'string',
            'max_length' => '50',
            'required' => false
        ),
        'set_position' => array(
            'type' => 'int',
            'max_length' => '50',
            'required' => false
        ),
        'hit_group' => array(
            'type' => 'string',
            'max_length' => '50',
            'required' => false
        ),
    );

    public function getDataMapperRules()
    {
        return $this->mapper_rules;
    }

    protected $id;
    protected $userId;
    protected $hitType;
    protected $hitTime;
    protected $note;
    protected $lineupId;
    protected $lineupPosition;
    protected $lineupPlayerId;
    protected $matchTeamName;
    protected $eventId;
    protected $teamPosition;
    protected $eventDate;
    protected $playerName;
    protected $points;
    protected $durationTime;
    protected $setPosition;
    protected $hitGroup;

    public function setId($val)
    {
        $this->id = $val;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setUserId($val)
    {
        $this->userId = $val;
    }

    public function getUserId()
    {
        return $this->userId;
    }

    public function setHitType($val)
    {
        $this->hitType = $val;
    }

    public function getHitType()
    {
        return $this->hitType;
    }

    public function setHitTime($val)
    {
        $this->hitTime = $val;
    }

    public function getHitTime()
    {
        return $this->hitTime;
    }

    public function setNote($val)
    {
        $this->note = $val;
    }

    public function getNote()
    {
        return $this->note;
    }

    public function setMatchTeamName($val)
    {
        $this->matchTeamName = $val;
    }

    public function getMatchTeamName()
    {
        return $this->matchTeamName;
    }

    public function setEventId($val)
    {
        $this->eventId = $val;
    }

    public function getEventId()
    {
        return $this->eventId;
    }

    public function setTeamPosition($val)
    {
        $this->teamPosition = $val;
    }

    public function getTeamPosition()
    {
        return $this->teamPosition;
    }

    public function setEventDate($val)
    {
        $this->eventDate = $val;
    }

    public function getEventDate()
    {
        return $this->eventDate;
    }

    public function getLineupId()
    {
        return $this->lineupId;
    }

    public function getLineupPosition()
    {
        return $this->lineupPosition;
    }

    public function setLineupId($lineupId)
    {
        $this->lineupId = $lineupId;
    }

    public function setLineupPosition($lineupPosition)
    {
        $this->lineupPosition = $lineupPosition;
    }

    public function getPlayerName()
    {
        return $this->playerName;
    }

    public function setPlayerName($playerName)
    {
        $this->playerName = $playerName;
    }

    public function getLineupPlayerId()
    {
        return $this->lineupPlayerId;
    }

    public function setLineupPlayerId($lineupPlayerId)
    {
        $this->lineupPlayerId = $lineupPlayerId;
    }

    public function getTeamId()
    {
        return $this->teamId;
    }

    public function setTeamId($teamId)
    {
        $this->teamId = $teamId;
    }

    public function getHitTypeAcr()
    {
        return strtoupper(substr($this->getHitType(), 0, 1));
    }

    function getPoints()
    {
        return $this->points;
    }

    function setPoints($points)
    {
        $this->points = $points;
    }

    function getDurationTime()
    {
        return $this->durationTime;
    }

    function setDurationTime($durationTime)
    {
        $this->durationTime = $durationTime;
    }

    function getSetPosition()
    {
        return $this->setPosition;
    }

    function setSetPosition($setPosition)
    {
        $this->setPosition = $setPosition;
    }

    function getHitGroup()
    {
        return $this->hitGroup;
    }

    function setHitGroup($hitGroup)
    {
        $this->hitGroup = $hitGroup;
    }

}
