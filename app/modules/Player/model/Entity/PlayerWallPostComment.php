<?php
	
namespace CR\Player\Model;
use Core\EntityMapper as EntityMapper;
use Core\ServiceLayer as ServiceLayer;
		
class PlayerWallPostComment {	

	
	
		
private $repository;
private $mapper_rules  = array(
	'id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => true
							
					),
'post_id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => false
							
					),
'author_id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => false
							
					),
'body' => array(
					'type' => 'string',
					'max_length' => '1000',
					'required' => false
							
					),
'status' => array(
					'type' => 'string',
					'max_length' => '50',
					'required' => false
							
					),
'created_at' => array(
					'type' => 'datetime',
					'max_length' => '',
					'required' => false
							
					),

);
	
public function getDataMapperRules()
{
	return $this->mapper_rules;
}


		
		
		
protected $id;

protected $postId;

protected $authorId;

protected $body;
protected $status;

protected $createdAt;

	protected $author;			
public function setId($val){$this->id = $val;}

public function getId(){return $this->id;}
	
public function setPostId($val){$this->postId = $val;}

public function getPostId(){return $this->postId;}
	
public function setAuthorId($val){$this->authorId = $val;}

public function getAuthorId(){return $this->authorId;}
	
public function setBody($val){$this->body = $val;}

public function getBody(){return $this->body;}
	
public function setCreatedAt($val){$this->createdAt = $val;}

public function getCreatedAt(){return $this->createdAt;}

function getAuthor() {
return $this->author;
}

 function setAuthor($author) {
$this->author = $author;
}

public function getFormatedCreatedAt()
{
    return $this->getCreatedAt()->format('d.m.Y H:i:s');
}

function getStatus() {
return $this->status;
}

 function setStatus($status) {
$this->status = $status;
}



}

		