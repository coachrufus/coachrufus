<?php

namespace CR\Player\Model;

use Core\Manager;
use DateTime;

/**
 * @author Marek Hubacek
 * @version 1.0
 * @created 18-12-2015 11:11:39
 */
class PlayerFollowManager extends Manager {

   public function createFollower($player,$follower)
   {
       $this->getRepository()->createFollower($player,$follower);
   }
   
   public function removeFollower($player,$follower)
   {
       $this->getRepository()->removeFollower($player,$follower);
   }
   
   public function getPlayerFollowers($player)
   {
       return $this->getRepository()->getPlayerFollowers($player);
   }
   

   /**
    * Check if user is follower
    * @param type $user
    * @param array $followers list of followers
    */
   public function isFollower($user, $followers)
   {
       foreach($followers as $follower)
       {
           if($follower->getId() == $user->getId())
           {
               return true;
           }
       }
       
       return false;
   }


}

?>