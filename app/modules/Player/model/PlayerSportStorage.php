<?php
namespace Webteamer\Player\Model;

use Core\DbQuery;
use Core\DbStorage;
use PDO;




class PlayerSportStorage extends DbStorage
{
      
    
        public function findBy($query_params = array(),$options = array())
	{


            $where_criteria_items  = array();
		foreach ($query_params as $column => $column_val)
		{
			if(is_string($column_val))
			{
				$where_criteria_items[] = ' u.'.$column.'=:'.$column.' ';
			}
			else
			{
				$where_criteria_items[] = ' u.'.$column.'=:'.$column.' ';
			}
			
		}
		$where_criteria = ' WHERE '. implode(' AND ',$where_criteria_items);
        
                $sort_criteria = '';
                if(array_key_exists('order',$options))
                {
                    $sort_criteria = ' order by u.'.$options['order'];
                }
                

		$query = \Core\DbQuery::prepare('SELECT u.* , '.$this->getAssocColumnsQuery().' '
                        . ' FROM '.$this->getTableName().' u LEFT JOIN sport a ON u.sport_id = a.id'. $where_criteria.$sort_criteria);
                
                
                
		foreach($query_params as $column => $value)
		{
			if("" != $value)
			{
				$query->bindParam(':'.$column, $value);
			}
		}
		$data = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);


		return $data;
	}
        
        
       
}

