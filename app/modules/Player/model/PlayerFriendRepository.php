<?php
namespace CR\Player\Model;
use Core\Repository as Repository;




class PlayerFriendRepository extends Repository
{
    public function createFriend($player,$friend)
   {
       $data['player_id'] = $player->getId();
       $data['friend_id'] = $friend->getId();
       $data['created_at'] = date('Y-m-d H:i:s');
       $data['status'] = 'waiting';
       $this->getStorage()->create($data);
   }
   
   public function removeFriend($player,$friend)
   {
      $this->deleteBy(array('friend_id' => $friend->getId(), 'player_id' => $player->getId() ));
   }
   
   public function getPlayerFriends($player)
   {
       $data = $this->getStorage()->getPlayerFriends($player->getId());

        foreach ($data as $friendData)
        {

            
            $player = new \Webteamer\Player\Model\Player();
            $friend = new PlayerFriend();
            $friend->setStatus($friendData['friend_status']);
            $friend->setId($friendData['f_id']);
            $this->updateEntityFromArray($player,$friendData);
            
            $friend->setPlayer($player);
           

            $list[] = $friend;
        }
        return $list;
   }
   
   public function findPlayerFriends($user,$term)
   {
       $data = $this->getStorage()->findPlayerFriendsByTerm($user->getId(),$term);
       $list = array();
       foreach ($data as $friendData)
        {
            $player = new \Webteamer\Player\Model\Player();
            $friend = new PlayerFriend();
            $friend->setStatus($friendData['friend_status']);
            $friend->setId($friendData['friend_id']);
            $this->updateEntityFromArray($player,$friendData);
            
            $friend->setPlayer($player);
           

            $list[] = $friend;
        }
        return $list;
   }
}

