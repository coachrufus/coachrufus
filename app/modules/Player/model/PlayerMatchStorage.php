<?php
namespace Webteamer\Player\Model;
use \Core\DbStorage;

class PlayerMatchStorage extends DbStorage 
{
   public function getIndividualPlayerStat($playerId)
   {
        $data = \Core\DbQuery::prepare('
                select count(*) as game_played, sum(goals) as goals, sum(assists) as assists, sum(saves) as saves, sum(man_of_match) as man_of_match from
                player_match m
                WHERE m.player_id = :id
                group by m.player_id')
		->bindParam('id', $playerId)
		->execute()
		->fetchOne(\PDO::FETCH_ASSOC);
		return $data;
   }
   
   public function getTeamPlayerStat($playerId)
   {
        $data = \Core\DbQuery::prepare('
                select  t.result, count(t.result) as count, sum(t.goals) as plus_goals, sum(t2.goals) as minus_goals FROM player_match_team t
LEFT JOIN player_match m ON t.id = m.match_team_id

LEFT JOIN player_match_team t2 ON t.match_id = t2.match_id and t2.id != t.id

WHERE player_id = :id
GROUP BY t.result')
		->bindParam('id', $playerId)
		->execute()
		->fetchAll(\PDO::FETCH_ASSOC);
		return $data;
   }
   
    public function findBy($query_params = array(),$options = array())
	{


            $where_criteria_items  = array();
		foreach ($query_params as $column => $column_val)
		{
			if(is_string($column_val))
			{
				$where_criteria_items[] = ' u.'.$column.'=:'.$column.' ';
			}
			else
			{
				$where_criteria_items[] = ' u.'.$column.'=:'.$column.' ';
			}
			
		}
		$where_criteria = ' WHERE '. implode(' AND ',$where_criteria_items);
        
                $sort_criteria = '';
                if(array_key_exists('order',$options))
                {
                    $sort_criteria = ' order by u.'.$options['order'];
                }
                

		$query = \Core\DbQuery::prepare('SELECT u.* , '.$this->getAssocColumnsQuery().' '
                        . ' FROM '.$this->getTableName().' u LEFT JOIN co_users a ON u.player_id = a.id'. $where_criteria.$sort_criteria);
                
                
                
		foreach($query_params as $column => $value)
		{
			if("" != $value)
			{
				$query->bindParam(':'.$column, $value);
			}
		}
		$data = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);


		return $data;
	}
}
