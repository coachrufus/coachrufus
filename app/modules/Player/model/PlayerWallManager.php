<?php

namespace CR\Player\Model;

use Core\ServiceLayer;

class PlayerWallManager extends \Core\Manager {

    protected $commentRepository;

    public function __construct($repository = null, $commentRepo)
    {
        parent::__construct($repository);
        
        $this->setCommentRepository($commentRepo);
    }

    function getCommentRepository()
    {
        return $this->commentRepository;
    }

    function setCommentRepository($commentRepository)
    {
        $this->commentRepository = $commentRepository;
    }

    public function savePostWall($post)
    {
        $this->getRepository()->save($post);
    }
    
    public function deletePostWall($post)
    {
        $this->getRepository()->delete($post->getId());
    }

    public function savePostWallComment($comment)
    {
        $this->getCommentRepository()->save($comment);
    }

    public function getPlayerWallPosts($player)
    {
        return $this->getRepository()->getPlayerWallPosts($player);
    }

    public function getPlayerFollowingPosts($player)
    {
        return $this->getRepository()->getPlayerFollowingPosts($player);
    }

    public function getWallPosts($player)
    {
        $playerWall = $this->getPlayerWallPosts($player);
        $followingWall = $this->getPlayerFollowingPosts($player);

        $list = array();
        foreach ($playerWall as $w)
        {
            $list[$w->getCreatedAt()->getTimestamp()] = $w;
        }
        foreach ($followingWall as $w)
        {
            $list[$w->getCreatedAt()->getTimestamp()] = $w;
        }

        krsort($list);
        return $list;
    }
    
    public function getPostById($id)
    {
        return $this->getRepository()->find($id);
    }
    
    public function getPostComments($post)
    {
        $comments = $this->getCommentRepository()->findPostComments($post);
        return $comments;
    }

}
