<?php
namespace Webteamer\Player\Model;

use Core\Repository as Repository;
use Core\Types\DateTimeEx;

class PlayerTimelineEventRepository extends Repository
{
    public function getEventTimeline($eventId,$eventDate)
    {
        $data = $this->getStorage()->getEventTimeline($eventId,$eventDate);
        return  $this->createObjectList($data);
    }
    
     public function getEventTimelineByLineup($lineup)
    {
        $data = $this->getStorage()->getEventTimelineByLineup($lineup->getId());
        return  $this->createObjectList($data);
    }

    
    public function getPlayerMatchCount($player,$team)
    {
        if(null != $player)
        {
            $data = $this->getStorage()->getPlayerMatchCount($player->getId(),$team->getId());
            return $data;
        }
    }
    
    public function getTeamPlayerMatchCount($teamPlayer)
    {
        if(null != $teamPlayer)
        {
            $data = $this->getStorage()->getPlayerMatchCount($teamPlayer->getId(),$teamPlayer->getTeamId());
            return $data;
        }
    }
    
  
    public function getPlayerPointsTotal($player,$team)
    {
         $data = array();
        if(null != $player)
        {
            $data = $this->getStorage()->getPlayerPoints($player->getId(),$team->getId());
            return $data;
        }
        return $data;
    }
    
    public function getTeamPlayerPointsTotal($teamPlayer)
    {
         $data = array();
        if(null != $teamPlayer)
        {
            $data = $this->getStorage()->getPlayerPoints($teamPlayer->getPlayerId(),$teamPlayer->getTeamId());
            return $data;
        }
        return $data;
    }
    
    /* pridat sezonu */
    
    public function getAllTeamPlayersPointsTotal($team, $seasonId = null, $dateFrom = null, $dateTo = null)
    {
        $data = $this->getStorage()->getTeamPlayersPoints($team->getId(), $seasonId, $dateFrom, $dateTo);
        $list = array();
        foreach($data as $d)
        {
            $list[$d['team_player_id']][$d['hit_type']] = $d['count'];
        }
        return $list;
    }
    
    public function getAllTeamPlayersPointsTotalByDate($team, $seasonId = null, $dateFrom = null, $dateTo = null)
    {
        $data = $this->getStorage()->getTeamPlayersPoints($team->getId(), $seasonId, $dateFrom, $dateTo);
        $list = [];
        foreach($data as $d)
        {
            $index= $d['team_player_id'];
            $eventDate = $d['event_date'];
            $hitType = $d['hit_type'];
            $list[$index][$eventDate][$hitType] = $d['count'];
        }
        return $list;
    }

    public function getTeamPlayerPointsByGames($teamPlayer,$criteria)
    {
        $data = array();
        if(array_key_exists('last_games', $criteria) && null != $teamPlayer)
        {
             $data = $this->getStorage()->getTeamPlayerPointsByGamesCount($teamPlayer->getPlayerId(),$teamPlayer->getTeamId(),$criteria['last_games']);
            return $data;
        }
         return $data;
    }
    
     public function getTeamPlayersPointsByGames($teamId,$criteria)
    {
        $points = array();
        if(array_key_exists('last_games', $criteria) && null != $teamId)
        {
            $data = $this->getStorage()->getTeamPlayersPointsByGamesCount($teamId,$criteria['last_games']);
            $lineups = array();
            foreach($data as $d)
            {
                 $lineups[$d['id']] = null;
            }
            //t_dump(array_keys($lineups));
            krsort($lineups);
            
            foreach($data as $d)
            {
                if(!array_key_exists($d['team_player_id'], $points))
                {
                    $points[$d['team_player_id']] = $lineups;
                }
                
                
                if('assist' == $d['hit_type'])
                {
                    $points[$d['team_player_id']][$d['id']]['assist'] = $d['points'];
                }
                if('goal' == $d['hit_type'])
                {
                    $points[$d['team_player_id']][$d['id']]['goal'] = $d['points'];
                }
            }

           
            return $points;
        }
         return $points;
    }
    
    public function getTeamPlayerMatchResults($teamPlayer)
    {
        $data = array();
        if(null != $teamPlayer)
        {
            $data = $this->getStorage()->getTeamPlayerMatchResults($teamPlayer->getPlayerId(),$teamPlayer->getTeamId());
        }
        return $data;
    }
    
    /* pridat sezonu */
    
    public function getAllTeamPlayersMatchResults($team, $seasonId = null, $dateFrom = null, $dateTo = null)
    {
        $data = $this->getStorage()->getAllTeamPlayerMatchResults($team->getId(), $seasonId, $dateFrom, $dateTo);
        $list = array();
        foreach($data as $d)
        {
            $list[$d['team_player_id']][] = $d;
        }
        return $list;
    }
    
    public function getAllTeamPlayersMatchResultsByDate($team, $seasonId = null, $dateFrom = null, $dateTo = null)
    {
        $data = $this->getStorage()->getAllTeamPlayerMatchResults($team->getId(), $seasonId, $dateFrom, $dateTo);
        $list = [];
        foreach($data as $d)
        {
            $team_player_id = $d['team_player_id'];
            $eventDate = $d['event_date'];
            $list[$team_player_id][$eventDate][] = $d;
        }
        return $list;
    }
    
    public function getPlayerAllMatchOverview($player)
    {
        $data = array();
        if(null != $player)
        {
            $data = $this->getStorage()->getPlayerAllMatchOverview($player->getId());
            
        }
        return $data;
    }
    public function getPlayerMatchOverview($player,$lineup)
    {
        $data = array();
        if(null != $player)
        {
            $data = $this->getStorage()->getPlayerMatchOverview($player->getId(),$lineup->getId());
            
        }
        return $data;
    }
    
    public function getMatchOverview($lineup)
    {
        $data = array();
        $data = $this->getStorage()->getMatchOverview($lineup->getId());
        return $data;
    }
    
    public function getMatchOverviewByEvent($event)
    {
        $eventId = $event->getId();
        $eventDate = $event->getCurrentDate()->format('Y-m-d 00:00:00');
        $data = array();
        $data = $this->getStorage()->getMatchOverviewByEvent($eventId,$eventDate);
        return $data;
    }
    
    
    /**
     * return overview of all matches for team player
     * @param type $teamPlayer
     * @return type
     */
     public function  getTeamPlayerAllMatchOverview($teamPlayer, DateTimeEx $dateFrom = null, DateTimeEx $dateTo = null)
    {
        $data = [];
        if(null != $teamPlayer)
        {
            $data = $this->getStorage()->getTeamPlayerAllMatchOverview($teamPlayer->getId(), $dateFrom, $dateTo);
        }
        return $data;
    }
    
    
    /**
     * Return set matchs overview
     * @param type $team
     * @param type $dateFrom
     * @param type $dateTo
     * @return type
     */
    public function  getSetMatchOverviewByDate($team, $dateFrom = null, $dateTo = null)
    {
        $data = $this->getStorage()->getSetMatchResults($team->getId(),$dateFrom,$dateTo);
        $lineupList = array();
        foreach($data as $d)
        {
           $lineupList[$d['lineup_id']]['set'][$d['set_position']][$d['lineup_position']] = $d;
           $lineupList[$d['lineup_id']]['players'][$d['lineup_position']][$d['team_player_id']] = $d;
        }

        $matchList = array();
        foreach($lineupList as $lineupInfo)
        {
            foreach($lineupInfo['players'] as $lineup => $lineupPlayers)
            {
                foreach($lineupPlayers as $lineupPlayer)
                {
                    $playerMatchStat = new PlayerSetMatchStat();
                    $playerMatchStat->setPlayer($lineupPlayer['player_name']);
                    $playerMatchStat->setGoals($lineupPlayer['points']);
                    $playerMatchStat->setEventDate($lineupPlayer['event_date']);
                    $playerMatchStat->setEventId($lineupPlayer['event_id']);
                    $playerMatchStat->setLineupPosition($lineupPlayer['lineup_position']);

                    foreach($lineupList[$lineupPlayer['lineup_id']]['set'] as $setIndex => $setData)
                    {
                        $playerMatchSetsData[$setIndex]['first_line'] =  $setData['first_line']['points'];
                        $playerMatchSetsData[$setIndex]['second_line'] =  $setData['second_line']['points'];
                    }
                    $playerMatchStat->setSets($playerMatchSetsData);
                    $matchList[$lineupPlayer['team_player_id']][$lineupPlayer['lineup_id']] = $playerMatchStat;
                }
            }
        }
        return $matchList;
    }
    
    /**
     * Return volleybal set matchs overview
     * @param type $team
     * @param type $dateFrom
     * @param type $dateTo
     * @return type
     */
    public function  getVolleyballSetMatchOverviewByDate($team, $dateFrom = null, $dateTo = null)
    {
        $data = $this->getStorage()->getVolleyballSetMatchResults($team->getId(),$dateFrom,$dateTo);
        $lineupList = array();
        $setPoints = array();
        foreach($data as $d)
        {
           
           $setPoints[$d['lineup_id']][$d['set_position']][$d['lineup_position']] = $d['points'];
           $lineupList[$d['lineup_id']]['players'][$d['lineup_position']][$d['team_player_id']] = $d;
           //$lineupList[$d['lineup_id']]['lineup_players'][$d['lineup_position']][$d['lineup_player_id']] = $d;
        }


        $matchList = array();
        foreach($lineupList as $lineupId =>  $lineupInfo)
        {
            foreach($lineupInfo['players'] as $lineup => $lineupPlayers)
            {
                foreach($lineupPlayers as $lineupPlayer)
                {
                    $playerMatchStat = new PlayerVolleyballMatchStat();
                    $playerMatchStat->setPlayer($lineupPlayer['player_name']);
                    $playerMatchStat->setGoals($lineupPlayer['points']);
                    $playerMatchStat->setEventDate($lineupPlayer['event_date']);
                    $playerMatchStat->setEventId($lineupPlayer['event_id']);
                    $playerMatchStat->setLineupPosition($lineupPlayer['lineup_position']);
                    $playerMatchStat->setSets($setPoints[$lineupId]);
                    $matchList[$lineupPlayer['team_player_id']][$lineupPlayer['lineup_id']] = $playerMatchStat;
                }
            }
        }
        return $matchList;
    }
    
    private function buildPlayerBasketballMatchStat($lineupPlayer,$points)
    {
        $playerMatchStat = new PlayerBasketballMatchStat();
        $playerMatchStat->setPlayer($lineupPlayer['player_name']);
        $playerMatchStat->setPoints1($lineupPlayer['points1']);
        $playerMatchStat->setPoints2($lineupPlayer['points2']);
        $playerMatchStat->setPoints3($lineupPlayer['points3']);
        $playerMatchStat->setManOfMatch($lineupPlayer['mom']);
        $playerMatchStat->setEventDate($lineupPlayer['event_date']);
        $playerMatchStat->setEventId($lineupPlayer['event_id']);
        $playerMatchStat->setLineupPosition($lineupPlayer['lineup_position']);
        $playerMatchStat->setFirstLineGoals($points[$lineupPlayer['lineup_id']]['first_line']);
        $playerMatchStat->setSecondLineGoals($points[$lineupPlayer['lineup_id']]['second_line']);
        
        return $playerMatchStat;
    }
    
     /**
     * Return volleybal set matchs overview
     * @param type $team
     * @param type $dateFrom
     * @param type $dateTo
     * @return type
     */
    public function  getBasketballMatchOverviewByDate($team, $dateFrom = null, $dateTo = null)
    {
        $data = $this->getStorage()->getBasketballMatchResults($team->getId(),$dateFrom,$dateTo);
        $lineupList = array();
        $points = array();
        foreach($data as $d)
        {
           
           $points[$d['lineup_id']][$d['lineup_position']] += ($d['points1']+($d['points2']*2)+($d['points3']*3));
           $lineupList[$d['lineup_id']]['players'][$d['lineup_position']][$d['team_player_id']] = $d;
        }

        $matchList = array();
        foreach($lineupList as $lineupId =>  $lineupInfo)
        {
            foreach($lineupInfo['players'] as $lineup => $lineupPlayers)
            {
                foreach($lineupPlayers as $lineupPlayer)
                {
                    $playerMatchStat = $this->buildPlayerBasketballMatchStat($lineupPlayer,$points);
                    $matchList[$lineupPlayer['team_player_id']][$lineupPlayer['lineup_id']] = $playerMatchStat;
                }
            }
        }
        return $matchList;
    }
    
    
    
    public function  getBasketballMatchOverviewByLineup($lineup)
    {
        $data = $this->getStorage()->getBasketballMatchOverviewByLineupId($lineup->getId());
        $points = array();
        foreach($data as $d)
        {
           $points[$d['lineup_id']][$d['lineup_position']] += ($d['points1']+($d['points2']*2)+($d['points3']*3));
        }

        
         $matchOverview = new \Webteamer\Team\Model\BasketballMatchOverview();
         foreach($data as $lineupPlayer)
        {
            $playerMatchStat = $this->buildPlayerBasketballMatchStat($lineupPlayer,$points);
            $matchOverview->addPlayerStat($lineupPlayer['team_player_id'], $playerMatchStat);
        }
        
        return $matchOverview;
    }
    
    
    public function getMatchResult($lineupId)
    {
        $data = $this->getStorage()->getMatchResult($lineupId);
        
        $result = array();
        foreach($data as $d)
        {
            $result[$d['lineup_position']] = $d['goals'];
        }
        
        return $result;
    }
    
   
}
