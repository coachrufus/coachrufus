<?php
namespace Webteamer\Player\Model;
use Core\Repository as Repository;


class PlayerSportRepository extends Repository
{
    public function clearPlayerSports($player)
    {
        $this->getStorage()->deleteByParams(array('player_id' => $player->getId())); 
    }   
    
    public function savePlayerSports($player,$sports)
    {
        foreach($sports as $sport)
        {
            $sport->setPlayerId($player->getId());
            $this->save($sport);
        }
    }
}
