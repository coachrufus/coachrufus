<?php
namespace CR\Player\Model;
use Core\DbStorage;
class PlayerFriendStorage extends DbStorage {
    
	public function getPlayerFriends($playerId)
        {
             $data = \Core\DbQuery::prepare('
               SELECT pf.status as friend_status, pf.id as f_id, p.*
                FROM player_friends pf
                LEFT JOIN co_users p ON pf.friend_id = p.id
                WHERE pf.player_id = :id')
                 ->bindParam(':id', $playerId)
		->execute()
		->fetchAll(\PDO::FETCH_ASSOC);
		return $data;
        }
        
        public function findPlayerFriendsByTerm($playerId,$term)
        {
             $data = \Core\DbQuery::prepare('
                SELECT pf.status as friend_status, pf.id as friend_id, p.*
                FROM player_friends pf
                LEFT JOIN co_users p ON pf.friend_id = p.id
                WHERE pf.player_id = :id
                AND (p.name like :term1 OR  p.surname like  :term2 OR  p.email like  :term3 OR  p.nickname like  :term4)')
                 ->bindParam(':id', $playerId)
                 ->bindParam(':term1', $term."%")
                 ->bindParam(':term2', $term."%")
                 ->bindParam(':term3', $term."%")
                 ->bindParam(':term4', $term."%")
		->execute()
		->fetchAll(\PDO::FETCH_ASSOC);
		return $data;
        }

}