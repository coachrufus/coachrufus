<?php
namespace CR\Player\Model;
use Core\Repository as Repository;




class PlayerWallPostRepository extends Repository
{
    public function getPlayerWallPosts($player)
    {
        $data = $this->getStorage()->getPlayerWallPosts($player->getId());
        
        $list = array();
        foreach ($data as $d)
        {
            $object = $this->factory->createEntityFromArray($d);
            
            $player = new \Webteamer\Player\Model\Player();
            $player->setName($d['name']);
            $player->setSurname($d['surname']);
            $player->setPhoto($d['photo']);
            $object->setAuthor($player);
            $object->setCommentsCount($d['comments_count']);
            
            $list[] = $object;
        }
        return $list;
    }
    public function getPlayerFollowingPosts($player)
    {
        $data = $this->getStorage()->getPlayerFollowingPosts($player->getId());
        
        $list = array();
        foreach ($data as $d)
        {
            $object = $this->factory->createEntityFromArray($d);
            
            $player = new \Webteamer\Player\Model\Player();
            $player->setName($d['name']);
            $player->setSurname($d['surname']);
            $player->setPhoto($d['photo']);
            $object->setAuthor($player);
             $object->setCommentsCount($d['comments_count']);
            $list[] = $object;
        }
        return $list;
    }
}
