<?php

namespace Webteamer\Player\Model;

use Core\Manager;
use DateTime;
use Core\Activity\ActivityTypes;
use Core\Types\DateTimeEx;

/**
 * @author Marek Hubacek
 * @version 1.0
 * @created 09-12-2015 14:51:04
 */
class PlayerStatSetManager extends Manager {

     public function getTeamCRSCriteria($team)
    {
        $teamManager =  \Core\ServiceLayer::getService('TeamManager');
        $crsSettings = $teamManager->getTeamSettings($team);
        $criteria = array();
        foreach($crsSettings as $crsSetting)
        {
            if($crsSetting->getSettingsGroup() == 'crs')
            {
                $criteria[$crsSetting->getName()] = $crsSetting->getValue();
            }
        }
        
        return $criteria;
    }
    
    public function getAllTeamPlayersStat($team,$statFrom,$statTo)
    {
         $allMatchs = $this->getRepository()->getSetMatchOverviewByDate($team, $statFrom, $statTo);
         
         $crsCriteria = $this->getTeamCRSCriteria($team);
         $playerStats= array();
         
         foreach($allMatchs as $teamPlayerId => $matchList)
         {
             foreach($matchList as $match)
             {
                 if(!array_key_exists($teamPlayerId, $playerStats))
                 {
                     $playerStats[$teamPlayerId] = new PlayerSetStat();
                     $playerStats[$teamPlayerId]->setCRSCriteria($crsCriteria);
                 }
                 $stat = $playerStats[$teamPlayerId];
                 $stat->setCRSCriteria($crsCriteria);

                 $stat->setGamePlayed($stat->getGamePlayed()+1);
                 $stat->setWonSet($stat->getWonSet()+$match->getWonSet());
                 $stat->setLooseSet($stat->getLooseSet()+$match->getLooseSet());
                 $stat->setDrawSet($stat->getDrawSet()+$match->getDrawSet());

                 if($match->getResultStatus() == 'win')
                 {
                      $stat->setWins($stat->getWins()+1);
                 }
                 if($match->getResultStatus() == 'draw')
                 {
                      $stat->setDraws($stat->getDraws()+1);
                 }
                 if($match->getResultStatus() == 'loose')
                 {
                      $stat->setLooses($stat->getLooses()+1);
                 }
                 
                 foreach($match->getSets() as $setData)
                 {
                     $stat->setPlusPoints($stat->getPlusPoints()+$setData[$match->getLineupPosition()]);
                     
                     $oppositeLine = ($match->getLineupPosition() == 'first_line') ? 'second_line' : 'first_line';
                     $stat->setMinusPoints($stat->getMinusPoints()+$setData[$oppositeLine]);
                     
                 }
                 $playerStats[$teamPlayerId]->setPlayer($match->getPlayer());
             }
         }
         
         return $playerStats;
    }
    
    public function getAllTeamPlayersVolleyballStat($team,$statFrom,$statTo)
    {
         $allMatchs = $this->getRepository()->getVolleyballSetMatchOverviewByDate($team, $statFrom, $statTo);
         $crsCriteria = $this->getTeamCRSCriteria($team);
         $playerStats= array();

         unset($allMatchs['']); //remove fake player
         foreach($allMatchs as $teamPlayerId => $matchList)
         {
             foreach($matchList as $match)
             {
                 if(!array_key_exists($teamPlayerId, $playerStats))
                 {
                     $playerStats[$teamPlayerId] = new PlayerSetStat();
                     $playerStats[$teamPlayerId]->setCRSCriteria($crsCriteria);
                 }
                 $stat = $playerStats[$teamPlayerId];
                 $stat->setCRSCriteria($crsCriteria);

                 $stat->setGoals($match->getGoals());
                 $stat->setGamePlayed($stat->getGamePlayed()+1);
                 $stat->setWonSet($stat->getWonSet()+$match->getWonSet());
                 $stat->setLooseSet($stat->getLooseSet()+$match->getLooseSet());
                 $stat->setDrawSet($stat->getDrawSet()+$match->getDrawSet());

                 if($match->getResultStatus() == 'win')
                 {
                      $stat->setWins($stat->getWins()+1);
                 }
                 if($match->getResultStatus() == 'draw')
                 {
                      $stat->setDraws($stat->getDraws()+1);
                 }
                 if($match->getResultStatus() == 'loose')
                 {
                      $stat->setLooses($stat->getLooses()+1);
                 }
                 
                 foreach($match->getSets() as $setData)
                 {
                     $stat->setPlusPoints($stat->getPlusPoints()+$setData[$match->getLineupPosition()]);
                     
                     $oppositeLine = ($match->getLineupPosition() == 'first_line') ? 'second_line' : 'first_line';
                     $stat->setMinusPoints($stat->getMinusPoints()+$setData[$oppositeLine]);
                     
                 }
                 $playerStats[$teamPlayerId]->setPlayer($match->getPlayer());
             }
         }
         
         return $playerStats;
    }
    
   
    
    
   


}

?>