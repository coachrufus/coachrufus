<?php

namespace Webteamer\Player\Model;

use Core\ServiceLayer;

class PlayerManager  extends \Core\Manager
{
    
    protected $repository;
    protected $playerSportRepository;
    protected $playerPrivacyRepository;

    public function __construct($repository = null,$playerSportRepository = null,$playerPrivacyRepository = null)
    {
        if(null != $repository)
        {
            $this->setRepository($repository);
        }
        if(null != $playerSportRepository)
        {
            $this->setPlayerSportRepository($playerSportRepository);
        }
        if(null != $playerPrivacyRepository)
        {
            $this->setPlayerPrivacyRepository($playerPrivacyRepository);
        }
    }
    
    public  function getPlayerPrivacyRepository() {
return $this->playerPrivacyRepository;
}

public  function setPlayerPrivacyRepository($playerPrivacyRepository) {
$this->playerPrivacyRepository = $playerPrivacyRepository;
}


    
    
   public  function getPlayerSportRepository() {
return $this->playerSportRepository;
}

public  function setPlayerSportRepository($playerSportRepository) {
$this->playerSportRepository = $playerSportRepository;
}



    public function getLevelEnum()
    {
        $translator = ServiceLayer::getService('translator');
        return array(
            'beginner' => $translator->translate('Začiatočník') ,
            'advanced' => $translator->translate('Pokročilý') ,
            'expert' => $translator->translate('Profi') , 
       );
    }
    
    public function getLevelFormChoices()
    {
       return $this->getLevelEnum();
    }

    
    public function getStatusEnum()
    {
        $translator = ServiceLayer::getService('translator');
        return array(
            'public' => $translator->translate('Verejný') ,
            'closed' => $translator->translate('Uzavretý') ,
       );
    }
    
     public function getStatusFormChoices()
    {
       return $this->getStatusEnum();
    }
    
     public function getLangFormChoices()
    {
       return array('sk' => 'SK', 'en' => 'EN', 'de' => 'DE' , 'fr' => 'FR');
    }
    
    public function fulltextFindPlayer($phrase,$section = 'all')
    {
        return $this->getRepository()->fulltextSearch($phrase,$section);
    }
    
  
    
    public function fulltextEmailFindPlayer($phrase)
    {
        return $this->getRepository()->fulltextEmailSearch($phrase);
    }
    
    public function findPlayerById($id)
    {
        return $this->getRepository()->findOneBy(array('id' => $id));
    }
    
    public function findPlayersByLocality($locality)
    {
        return $this->getRepository()->findBy(array('locality_id' => $locality->getId()));
    }
    
    public function findPlayersByArea($center, $distance = 5)
    {
        return $this->getRepository()->findPlayersByArea($center, $distance);
    }
    
    
    public function findPlayerSports($player)
    {
        $repo = $this->getPlayerSportRepository();
        if(null != $player)
        {
             $list = $repo->findBy(array('player_id' => $player->getId()));
            return $list;
        }
       
    }
    
    public function savePlayerSports($player,$sports)
    {
        $repo = $this->getPlayerSportRepository();
        $repo->clearPlayerSports($player);
        $repo->savePlayerSports($player,$sports);
    }
    
    public function findPlayerPrivacy($player)
    {
         $repo = $this->getPlayerPrivacyRepository();
         $list = $repo->findBy(array('player_id' => $player->getId()));
         return $list;
    }
    
     public function savePlayerPrivacy($player,$privacy)
    {
        $repo = $this->getPlayerPrivacyRepository();
        $repo->clearPlayerPrivacy($player);
        $repo->savePlayerPrivacy($player,$privacy);
    }
    
    public function getPrivacyMatrix($playerPrivacy = null)
    {

        
        $existMatrix = array();
        
        foreach($playerPrivacy as $priv)
        {
            $existMatrix[$priv->getSection()][$priv->getUserGroup()] = $priv->getValue();
        }
        
        $sections = array('profil'=>'Profil','statistic' => 'Statistic','messages' => 'Messages');
        $groups = array('all' => 'All','fof'  => 'Friends of friends','f' => 'Friends','n' => 'None');
        
        $matrix = array('groups' => $groups);
        
        foreach($sections as $sectionKey=> $sectionName)
        {
            foreach($groups as $groupId => $groupName)
            {
                $enabled = false;
                if(array_key_exists($sectionKey, $existMatrix) && array_key_exists($groupId, $existMatrix[$sectionKey]))
                {
                    $enabled = true;
                }
                
                
                $matrix['sections'][$sectionKey]['groups'][$groupId] = array('name' => $groupName,'enabled' => $enabled);
            }
            
            $matrix['sections'][$sectionKey]['name'] = $sectionName;
        }
        
        return $matrix;
    }
    
    public function savePlayer($player)
    {
        $this->getRepository()->save($player);
    }
    
     public function createAvatar($name)
    {
        $seoManager = ServiceLayer::getService('seo');
        $name = strtolower($name);
        $name = $seoManager->removeDiacritic($name);
         
         $nameParts = explode(' ',trim($name));

        if(count($nameParts) > 1)
        {
            $label = mb_substr($nameParts[0], 0, 1).substr($nameParts[1], 0, 1);
            
        }
        else
        {
            $label = mb_substr($nameParts[0], 0, 2);
        }
        
       
        $label = strtoupper($label);
        //remove diacritic
       // $seo = ServiceLayer::getService('seo');
       // removeDiacritic
        
       

        
        $im     = imagecreatefrompng(PUBLIC_DIR.'/img/users/letterAvatar.png');
        $bgColor = $red = imagecolorallocate($im, rand(1,124), rand(1,124), rand(1,124));
        $textColor = imagecolorallocate($im, 255, 255, 255);
        imagefill($im, 0,0, $bgColor);
        
        //$font = PUBLIC_DIR.'/vendor/AnonymousProBold.ttf'; // path to font
        $font = PUBLIC_DIR.'/vendor/montserrat-bold.ttf'; // path to font
        
        //$leftPos = (count($nameParts) > 1) ? 40 : 60;
        
        
        $textBox = imagettfbbox(54,0,$font,$label);
        $length =$textBox[2] - $textBox[0];
        
        $center = round((imagesx($im)-$length)/2);

        imagettftext($im,54, 0, $center, 130, $textColor, $font, $label );
         
        
        return $im;

             
    }
    
    
    public function saveTeamPlayerAvatar($teamPlayer,$im)
    {
        $tmpName = $teamPlayer->getId().'.png';
        imagepng($im,PUBLIC_DIR.'/img/team-players/'.$tmpName);
        return $tmpName;
    }
     
    public function savePlayerAvatar($player,$im)
    {
        $tmpName = $player->getId().'.png';
        imagepng($im,PUBLIC_DIR.'/img/users/'.$tmpName);
        imagepng($im,PUBLIC_DIR.'/img/users/thumb_320_320/'.$tmpName);
        imagepng($im,PUBLIC_DIR.'/img/users/thumb_90_90/'.$tmpName);
        return $tmpName;
    }
    
     public function saveTournamentTeamPlayerAvatar($teamPlayer,$im)
    {
        $tmpName = $teamPlayer->getId().'.png';
        imagepng($im,PUBLIC_DIR.'/img/tournament-team-players/'.$tmpName);
        return $tmpName;
    }
     
  
    
   
    
    
    

}
