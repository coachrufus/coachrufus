<?php

namespace Webteamer\Player\Model;

use Core\Manager;
use DateTime;
use Core\Activity\ActivityTypes;
use Core\Types\DateTimeEx;

/**
 * @author Marek Hubacek
 * @version 1.0
 * @created 09-12-2015 14:51:04
 */
class PlayerStatManager extends Manager {


    private $scoreRepository;
    private $timelineRepository;
    private $activityLogger;
    protected $teamLastGames;
    
    public function __construct($scoreRepository = null, $timelineRepository = null, $activityLogger = null)
    {
        if (null != $scoreRepository)
        {
            $this->setScoreRepository($scoreRepository);
        }
        if (null != $timelineRepository)
        {
            $this->setTimelineRepository($timelineRepository);
        }
        $this->activityLogger = $activityLogger;
    }
    
    public function getHitTypeEnum()
    {
        return array(
            'hit' => 'Hit',
            'assist' => 'Assist',
            'save' => 'Save',
            'goal' => 'Goal',
            'foul' => 'Foul',
            'mom' => 'Man of match',
            );
    }
    
    public function getScoreRepository()
    {
        return $this->scoreRepository;
    }

    public function setScoreRepository($scoreRepository)
    {
        $this->scoreRepository = $scoreRepository;
    }

    public function getMatchRepository()
    {
        return $this->matchRepository;
    }

    public function setMatchRepository($matchRepository)
    {
        $this->matchRepository = $matchRepository;
    }

    public function getTimelineRepository()
    {
        return $this->timelineRepository;
    }

    public function setTimelineRepository($timelineRepository)
    {
        $this->timelineRepository = $timelineRepository;
    }
    
    public function deleteTimelineEvent($event)
    {
        $this->getTimelineRepository()->delete($event->getId());
    }
    
    public function clearTimelineMomEvent($lineupId)
    {
        $this->timelineRepository->deleteBy(array('lineup_id' => $lineupId,'hit_type' => 'mom'));
    }
    
    public function clearTimeline($lineupId)
    {
        //delete all events
        $this->timelineRepository->deleteBy(array('lineup_id' => $lineupId));
    }

    public function addPlayerRating($player, $author, $ratingData)
    {
        if(null == $player)
        {
            $playerId = null;
        }
        else
        {
            $playerId = $player->getId();
        }
        $rating = new PlayerScore();
        $rating->setPlayerId($playerId);
        $rating->setScore($ratingData['score']);
        $rating->setAuthorId($author->getId());
        $rating->setCreatedAt(new DateTime());
        $rating->setMatchId($ratingData['match_id']);
        $rating->setEventDate(new DateTime($ratingData['event_date']));
        $rating->setEventId($ratingData['event_id']);
        $rating->setTeamId($ratingData['team_id']);
        $rating->setTeamPlayerId($ratingData['team_player_id']);
        $this->getScoreRepository()->save($rating);
        $this->activityLogger->log($author->getId(), ActivityTypes::RATE, json_encode($ratingData));
    }
    
     public function updatePlayerRating($author,$rating)
    {
        $this->getScoreRepository()->save($rating);
        $this->activityLogger->log($author->getId(), ActivityTypes::RATE, serialize($rating));
    }
    
    public function getPlayerRating($player)
    {
         $average = $this->getScoreRepository()->getPlayerAverageRating($player);
         return array('average' => round($average['avg'],1));
    }
    
    public function getPlayerTeamRating($player,$team)
    {
         $average = $this->getScoreRepository()->getPlayerTeamAverageRating($player,$team);
         return array('average' => round($average['avg'],1));
    }
    
    
    /**
     * return overview of all matches for team player
     * @param type $teamPlayer
     * @return \Webteamer\Player\Model\PlayerMatchStat
     */
    public function getTeamPlayerAllMatchOverview($teamPlayer, DateTimeEx $dateFrom = null, DateTimeEx $dateTo = null)
    {
        $playerMatchResults = $this->getTimelineRepository()->getTeamPlayerAllMatchOverview($teamPlayer, $dateFrom, $dateTo);
        $list = [];
        foreach($playerMatchResults as $match)
        {
            $result = ($match['first_line_goals'] != null) ? $match['first_line_goals'] : '0';
            $result .= ' : ';
            $result .= ($match['second_line_goals'] != null) ? $match['second_line_goals'] : '0';
            $matchStat = new PlayerMatchStat();
            $matchStat->setEventDate($match['event_date']);
            $matchStat->setEventId($match['event_id']);
            $matchStat->setAssists($match['assist']);
            $matchStat->setGoals($match['goals']);
            $matchStat->setLineupName($match[$match['lineup_position']]);
            $matchStat->setLineupId($match['lineup_id']);
            $matchStat->setLineupPosition($match['lineup_position']);
            $matchStat->setResult($result);
            $matchStat->setTeamName($match['name']);
            $matchStat->setFirstLineGoals($match['first_line_goals']);
            $matchStat->setSecondLineGoals($match['second_line_goals']);
            $matchStat->setFirstLineName($match['first_line']);
            $matchStat->setSecondLineName($match['second_line']);
            $list[] = $matchStat;
        }
        return $list;
    }
    
    
    /**
     * return overview of all matches for registred player
     * @param type $player
     * @return \Webteamer\Player\Model\PlayerMatchStat
     */
    public function getPlayerAllMatchOverview($player)
    {
        $playerMatchResults =  $this->getTimelineRepository()->getPlayerAllMatchOverview($player);
        $list = array();
        foreach($playerMatchResults as $match)
        {
            $matchStat = new PlayerMatchStat();
            $matchStat->setEventDate($match['event_date']);
            $matchStat->setEventId($match['event_id']);
            $matchStat->setAssists($match['assist']);
            $matchStat->setGoals($match['goals']);
            $matchStat->setLineupName($match[$match['lineup_position']]);
            $matchStat->setLineupId($match['lineup_id']);
            $matchStat->setLineupPosition($match['lineup_position']);
            $matchStat->setResult($match['first_line_goals'].':'.$match['second_line_goals']);
            $matchStat->setTeamName($match['name']);
            $matchStat->setFirstLineGoals($match['first_line_goals']);
            $matchStat->setSecondLineGoals($match['second_line_goals']);
            $list[] = $matchStat;
        }
            
        return $list;
    }
    
   /**
    * get match
    * @param type $lineup
    * @return \Webteamer\Player\Model\PlayerMatchStat
    */
    public function getMatchOverview($lineup)
    {
        $match =  $this->getTimelineRepository()->getMatchOverview($lineup);
        $matchStat = new PlayerMatchStat();
        
        $eventDate = DateTime::createFromFormat('Y-m-d H:i:s', $match['event_date']);
        $matchStat->setEventDate($eventDate);
        $matchStat->setEventId($match['event_id']);
       
        $matchStat->setLineupName($match[$match['lineup_position']]);
        $matchStat->setLineupId($match['lineup_id']);
        $matchStat->setLineupPosition($match['lineup_position']);
        
        
        $firstLineGoals = (null != $match['first_line_goals']) ? $match['first_line_goals'] : 0;
        $secondLineGoals = (null != $match['second_line_goals']) ? $match['second_line_goals'] : 0;
        
        $matchStat->setResult($firstLineGoals.':'.$secondLineGoals);
        $matchStat->setTeamName($match['name']);
        $matchStat->setFirstLineGoals($firstLineGoals);
        $matchStat->setSecondLineGoals($secondLineGoals);
        $matchStat->setFirstLineName($match['first_line']);
        $matchStat->setSecondLineName($match['second_line']);
            
        return $matchStat;
    }
    
    public function getMatchResult($lineup)
    {
         $result =  $this->getTimelineRepository()->getMatchResult($lineup->getId());
         return $result;
    }
    
    /**
     * get match result based on event date and event id
     * @param type $eventId
     * @param type $eventDate
     */
    public function getMatchOverviewByEvent($event)
    {
        $match =  $this->getTimelineRepository()->getMatchOverviewByEvent($event);
        if(false != $match)
        {
            $matchStat = new PlayerMatchStat();
        
            $matchStat->setEventDate($event->getCurrentDate());
            $matchStat->setEventId($event->getId());

            $matchStat->setLineupName($match[$match['lineup_position']]);
            $matchStat->setLineupId($match['lineup_id']);
            $matchStat->setLineupPosition($match['lineup_position']);

            $firstLineGoals = (null != $match['first_line_goals']) ? $match['first_line_goals'] : 0;
            $secondLineGoals = (null != $match['second_line_goals']) ? $match['second_line_goals'] : 0;

            $matchStat->setResult($firstLineGoals.':'.$secondLineGoals);
            $matchStat->setTeamName($match['name']);
            $matchStat->setFirstLineGoals($firstLineGoals);
            $matchStat->setSecondLineGoals($secondLineGoals);
            $matchStat->setFirstLineName($match['first_line']);
            $matchStat->setSecondLineName($match['second_line']);

            return $matchStat;
        }
        
    }
    
    public static function compare($a, $b) 
    {
        if($a->getGoals() == $b->getGoals()){ return 0 ; }
	return ($a->getGoals() < $b->getGoals()) ? 1 : -1;
        
        
       
    }
    
    /**
     * Return playerst stat for specific match
     * @param type $lineup
     */
    public function getMatchPlayersStat($lineup)
    {
        $hits = $this->getTimelineRepository()->getEventTimelineByLineup($lineup);   
        $teamEventManager = \Core\ServiceLayer::getService('TeamEventManager');
        $lineupPlayers = $teamEventManager->getEventLineupPlayers($lineup);
        $playerStats = array();
        
       

        foreach($hits as $timelineEvent)
        {
            $statIndex = $timelineEvent->getLineupPlayerId();
            
            
            if(0 != $statIndex) //NOBODY asistencie a goly
            {
                 if(!array_key_exists($statIndex, $playerStats))
                {
                    $playerStats[$statIndex] = new PlayerStat();

                     $playerStats[$statIndex]->setGoals(0);
                     $playerStats[$statIndex]->setAssists(0);
                }

                $stat = $playerStats[$statIndex];

                if($timelineEvent->getHitType() == 'goal')
                {
                     $stat->setGoals($stat->getGoals()+1);
                }

                if($timelineEvent->getHitType() == 'assist')
                {
                     $stat->setAssists($stat->getAssists()+1);
                }

                if($timelineEvent->getHitType() == 'mom')
                {
                      $stat->setManOfMatch(1);
                }
            }
        }
     
        uasort($playerStats, array('Webteamer\Player\Model\PlayerStatManager','compare')); 
        $orderedStats = $playerStats;
       
        //add other
        foreach($lineupPlayers as $lineupPlayer)
        {
            if(!array_key_exists($lineupPlayer->getId(), $playerStats))
            {
                $orderedStats[$lineupPlayer->getId()] = new PlayerStat();
                $orderedStats[$lineupPlayer->getId()]->setGoals(0);
                $orderedStats[$lineupPlayer->getId()]->setAssists(0);
            }
        }
        
       

        return $orderedStats;
    }
    
    
    public function getTeamCRSCriteria($team)
    {
        $teamManager =  \Core\ServiceLayer::getService('TeamManager');
        $crsSettings = $teamManager->getTeamSettings($team);
        $criteria = array();
        foreach($crsSettings as $crsSetting)
        {
            if($crsSetting->getSettingsGroup() == 'crs')
            {
                $criteria[$crsSetting->getName()] = $crsSetting->getValue();
            }
        }
        
        return $criteria;
    }
    
    /* PRIDAT SEZONU
    - getAllTeamPlayersMatchResults
    - getAllTeamPlayersPointsTotal
    */
    public function getAllTeamPlayersStat($team, $seasonId = null, $dateFrom = null, $dateTo = null)
    {
        $allMatchs = $this->getTimelineRepository()->getAllTeamPlayersMatchResults($team, $seasonId, $dateFrom, $dateTo);
        $allPoints = $this->getTimelineRepository()->getAllTeamPlayersPointsTotal($team, $seasonId, $dateFrom, $dateTo);
        $crsCriteria = $this->getTeamCRSCriteria($team);
        
        $maxValues = array(
            'game_played' => 0,
            'win' => 0,
            'draw' => 0,
            'loose' => 0,
            'plus' => 0,
            'minus' => 0,
            'goal' => 0,
            'assist' => 0,
            'mom' => 0,
            'looses' => 0,
            'ga' => 0,
            'crs' => 0,
            'avg-crs' => 0,
            'avg-goal' => 0,
            'avg-assist' => 0,
            'pm-diff-max' => 0,
            'pm-diff-min' => 0,
            );

        $list = array();
        foreach($allMatchs as $playerId => $playerMatchs)
        {
            $playerStat = new PlayerStat();
            $playerStat->setCRSCriteria($crsCriteria);
            $wins = 0;
            $looses = 0;
            $draws = 0;
            $plusPoints = 0;
            $minusPoints = 0;
            $matchCount = 0;
            $goalkeeperGoals = 0;
            $goalkeeperShotouts = 0;
            $goalkeeperGamePlayes = 0;
            //$goals = 0;
            //$assists = 0;
           
            foreach($playerMatchs as $match)
            {
                if(null == $match['first_line_goals'] )
                {
                    $match['first_line_goals']  = 0;
                }

                if(null == $match['second_line_goals'] )
                {
                    $match['second_line_goals']  = 0;
                }
                //wins, draws, looses
                if($match['first_line_goals'] == $match['second_line_goals'] )
                {
                    $draws++;
                }
                else 
                {
                    if($match['first_line_goals'] > $match['second_line_goals'] && $match['lineup_position'] == 'first_line')
                    {
                       $wins++;
                    }
                    elseif($match['first_line_goals'] < $match['second_line_goals'] && $match['lineup_position'] == 'second_line')
                    {
                        $wins++;
                    }
                    else 
                    {
                        $looses++;

                    }
                }

                //+- points
                if($match['lineup_position'] == 'first_line')
                {
                    $plusPoints += $match['first_line_goals'];
                    $minusPoints += $match['second_line_goals'];
                }
                elseif($match['lineup_position'] == 'second_line') 
                {
                     $plusPoints += $match['second_line_goals'];
                     $minusPoints += $match['first_line_goals'];
                }
                
                //goal keeper goals
                if('GOAL_KEEPER' == $match['player_position'])
                {
                    $goalkeeperGamePlayes++;
                    if($match['lineup_position'] == 'first_line')
                    {
                        $goalkeeperGoals += $match['second_line_goals'];
                        if($match['second_line_goals'] == 0)
                        {
                            $goalkeeperShotouts ++;
                        }
                    }
                    if($match['lineup_position'] == 'second_line')
                    {
                        $goalkeeperGoals += $match['first_line_goals'];
                        if($match['first_line_goals'] == 0)
                        {
                            $goalkeeperShotouts ++;
                        }
                    }
                    
                   
                }
                
                
                $matchCount++;
            }
            
            $playerStat->setWins($wins);
            $playerStat->setLooses($looses);
            $playerStat->setDraws($draws);
            $playerStat->setPlusPoints($plusPoints);
            $playerStat->setMinusPoints($minusPoints);
            $playerStat->setGoalkeeperGoals($goalkeeperGoals);
            $playerStat->setGoalkeeperShotouts($goalkeeperShotouts);
            $playerStat->setGoalkeeperGamePlayes($goalkeeperGamePlayes);
            
            if(array_key_exists($playerId, $allPoints))
            {
                if(array_key_exists('goal',$allPoints[$playerId]))
                {
                    $playerStat->setGoals($allPoints[$playerId]['goal']);
                }
                else 
                {
                    $playerStat->setGoals(0);
                }
                
                if(array_key_exists('assist',$allPoints[$playerId]))
                {
                    $playerStat->setAssists($allPoints[$playerId]['assist']);
                }
                else 
                {
                    $playerStat->setAssists(0);
                }
                
                if(array_key_exists('mom',$allPoints[$playerId]))
                {
                    $playerStat->setManOfMatch($allPoints[$playerId]['mom']);
                }
                else 
                {
                    $playerStat->setManOfMatch(0);
                }
                
            }
            else
            {
                $playerStat->setGoals(0);
                $playerStat->setAssists(0);
            }

            $playerStat->setGamePlayed($matchCount);
            
            /*STAT MAX*/
            if($maxValues['game_played'] < $matchCount)
            {
                $maxValues['game_played'] = $matchCount;
            }
            
            if($maxValues['win'] < $wins)
            {
                $maxValues['win'] = $wins;
            }
            
            if($maxValues['draw'] < $draws)
            {
                $maxValues['draw'] = $draws;
            }
            
            
            if($maxValues['looses'] < $looses)
            {
                $maxValues['looses'] = $looses;
            }
            
            if($maxValues['plus'] < $plusPoints)
            {
                $maxValues['plus'] = $plusPoints;
            }
            
            if($maxValues['minus'] < $minusPoints)
            {
                $maxValues['minus'] = $minusPoints;
            }
            
             if($maxValues['pm-diff-max'] < $playerStat->getPlusMinusDiff())
            {
                $maxValues['pm-diff-max'] = $playerStat->getPlusMinusDiff();
            }
            
            
             if($maxValues['pm-diff-min'] > $playerStat->getPlusMinusDiff())
            {
                $maxValues['pm-diff-min'] = $playerStat->getPlusMinusDiff();
            }
            
            if(array_key_exists($playerId, $allPoints))
            {
                 if( array_key_exists('goal',  $allPoints[$playerId]) && $maxValues['goal'] < $allPoints[$playerId]['goal'])
                {
                    $maxValues['goal'] = $allPoints[$playerId]['goal'];
                }

                if(array_key_exists('assist',  $allPoints[$playerId]) && $maxValues['assist'] < $allPoints[$playerId]['assist'])
                {
                    $maxValues['assist'] = $allPoints[$playerId]['assist'];
                }

                if( array_key_exists('mom',  $allPoints[$playerId]) && $maxValues['mom'] < $allPoints[$playerId]['mom'])
                {
                    $maxValues['mom'] = $allPoints[$playerId]['mom'];
                }

                if( array_key_exists('goal',  $allPoints[$playerId]) && array_key_exists('assist',  $allPoints[$playerId]) && $maxValues['ga'] < ($allPoints[$playerId]['goal']+$allPoints[$playerId]['assist']))
                {
                    $maxValues['ga'] = $allPoints[$playerId]['goal']+$allPoints[$playerId]['assist'];
                }
            }
            
           
            
            
            if($maxValues['crs'] < $playerStat->getCRS() )
            {
                $maxValues['crs'] = $playerStat->getCRS();
            }
            
            if($maxValues['avg-crs'] < $playerStat->getAverageCRS() )
            {
                $maxValues['avg-crs'] = $playerStat->getAverageCRS();
            }
            if($maxValues['avg-goal'] < $playerStat->getAverageGoals() )
            {
                $maxValues['avg-goal'] = $playerStat->getAverageGoals();
            }
            if($maxValues['avg-assist'] < $playerStat->getAverageAssists() )
            {
                $maxValues['avg-assist'] = $playerStat->getAverageAssists();
            }
            
            
            /*
             $maxValues = array(
            'game_played' => 0,
            'win' => 0,
            'draw' => 0,
            'loose' => 0,
            'plus' => 0,
            'minus' => 0,
            'goal' => 0,
            'assist' => 0,
            'mom' => 0,
            );
            */
            
            $list[$playerId] = $playerStat;
           
        }
        $list['maxValues'] = $maxValues;
        return $list;
    }
    
    
    public function getAllTeamPlayersStatByDate($team, $seasonId = null, $fromDate, $toDate)
    {
        $allMatchs = $this->getTimelineRepository()->getAllTeamPlayersMatchResultsByDate($team, $seasonId, $fromDate, $toDate);
        $allPoints = $this->getTimelineRepository()->getAllTeamPlayersPointsTotalByDate($team, $seasonId, $fromDate, $toDate);

        $crsCriteria = $this->getTeamCRSCriteria($team);
        $list = [];
        foreach($allMatchs as $teamPlayerId => $eventDates)
        {
            foreach ($eventDates as $eventDate => $playerMatchs)
            {
                $playerStat = new PlayerStat();
                $playerStat->setCRSCriteria($crsCriteria);
                $wins = 0;
                $looses = 0;
                $draws = 0;
                $plusPoints = 0;
                $minusPoints = 0;
                $matchCount = 0;
                foreach($playerMatchs as $match)
                {
                    if (null == $match['first_line_goals'])
                    {
                        $match['first_line_goals']  = 0;
                    }
                    if (null == $match['second_line_goals'])
                    {
                        $match['second_line_goals']  = 0;
                    }
                    if ($match['first_line_goals'] == $match['second_line_goals'])
                    {
                        $draws++;
                    }
                    else 
                    {
                        if ($match['first_line_goals'] > $match['second_line_goals'] && $match['lineup_position'] == 'first_line')
                        {
                            $wins++;
                        }
                        elseif ($match['first_line_goals'] < $match['second_line_goals'] && $match['lineup_position'] == 'second_line')
                        {
                            $wins++;
                        }
                        else 
                        {
                            $looses++;
                        }
                    }
                    if ($match['lineup_position'] == 'first_line')
                    {
                        $plusPoints += $match['first_line_goals'];
                        $minusPoints += $match['second_line_goals'];
                    }
                    elseif ($match['lineup_position'] == 'second_line') 
                    {
                        $plusPoints += $match['second_line_goals'];
                        $minusPoints += $match['first_line_goals'];
                    }
                    $matchCount++;
                }
                $playerStat->setWins($wins);
                $playerStat->setLooses($looses);
                $playerStat->setDraws($draws);
                $playerStat->setPlusPoints($plusPoints);
                $playerStat->setMinusPoints($minusPoints);

                if (isset($allPoints[$teamPlayerId]) && isset($allPoints[$teamPlayerId][$eventDate]))
                {
                    $points = $allPoints[$teamPlayerId][$eventDate];
                    $playerStat->setGoals(isset($points['goal']) ? (float)$points['goal'] : 0);
                    $playerStat->setAssists(isset($points['assist']) ? (float)$points['assist'] : 0);
                    $playerStat->setManOfMatch(isset($points['mom']) ? (float)$points['mom'] : 0);
                }
                else
                {
                    $playerStat->setGoals(0);
                    $playerStat->setAssists(0);
                }
                $playerStat->setGamePlayed($matchCount);
                $list[$teamPlayerId][$eventDate] = $playerStat;
            }
        }
        return $list;
    }

    /**
     * Get Team player stats. Get the stats only for player team scope
     * @param type Webteamer\Team\Model\TeamPlayer $teamPlayer
     * @param type $team
     * @return \Webteamer\Player\Model\PlayerStatgetAllTeamPlayersStat
     */
    public function getTeamPlayerStat($teamPlayer, $dateFrom = null, $dateTo = null)
    {
        $playerStat = new PlayerStat();
        //$matchCount = $this->getTimelineRepository()->getTeamPlayerMatchCount($teamPlayer);

        $playerTotalPoints = $this->getTimelineRepository()->getTeamPlayerPointsTotal($teamPlayer);
        $playerMatchResults =  $this->getTimelineRepository()->getTeamPlayerMatchResults($teamPlayer);

        $wins = 0;
        $looses = 0;
        $draws = 0;
        $plusPoints = 0;
        $minusPoints = 0;
        $matchCount = 0;
        foreach($playerMatchResults as $match)
        {

            if(null == $match['first_line_goals'] )
            {
                $match['first_line_goals']  = 0;
            }
            
            if(null == $match['second_line_goals'] )
            {
                $match['second_line_goals']  = 0;
            }
            //wins, draws, looses
            if($match['first_line_goals'] == $match['second_line_goals'] )
            {
                $draws++;
            }
            else 
            {
                if($match['first_line_goals'] > $match['second_line_goals'] && $match['lineup_position'] == 'first_line')
                {
                   $wins++;
                }
                elseif($match['first_line_goals'] < $match['second_line_goals'] && $match['lineup_position'] == 'second_line')
                {
                    $wins++;
                }
                else 
                {
                    $looses++;
                }
            }
            
            //+- points
            if($match['lineup_position'] == 'first_line')
            {
                $plusPoints += $match['first_line_goals'];
                $minusPoints += $match['second_line_goals'];
            }
            elseif($match['lineup_position'] == 'second_line') 
            {
                 $plusPoints += $match['second_line_goals'];
                 $minusPoints += $match['first_line_goals'];
            }
             $matchCount++;
        }
       
        $playerStat->setWins($wins);
        $playerStat->setLooses($looses);
        $playerStat->setDraws($draws);
        $playerStat->setPlusPoints($plusPoints);
        $playerStat->setMinusPoints($minusPoints);
        $playerStat->setGamePlayed($matchCount);
        foreach ($playerTotalPoints as $points)
        {
            switch ($points['hit_type']) {
                case 'goal':
                    $playerStat->setGoals($points['count']);
                    break;
                case 'assist':
                    $playerStat->setAssists($points['count']);
                    break;
                case 'hit':
                    $playerStat->setHits($points['count']);
                    break;
                case 'save':
                    $playerStat->setSaves($points['count']);
                    break;

                default:
                    break;
            }
        }

        //$lastGames = $this->getTeamLastGames($teamPlayer->getTeamId());

        
        //$playerStat->setGamePlayed($matchCount['count']);
        //
        //
        //$playerStat->setPlayerLastGames($this->getTeamPlayerLastGames($teamPlayer));
        $lastGames = $this->getTeamLastGames($teamPlayer->getTeamId());
        $playerStat->setPlayerLastGames($lastGames[$teamPlayer->getId()]);
        return $playerStat;

    }
    
    public function getTeamLastGames($teamId)
    {
        if(null == $this->teamLastGames)
        {
            $this->teamLastGames = $this->getTimelineRepository()->getTeamPlayersPointsByGames($teamId,array('last_games' => 4));
        }
        
        return   $this->teamLastGames;
    }

    /**
     * Get last games for player, depracted method
     * @param type $teamPlayer
     * @return type
     */
    public function getTeamPlayerLastGames($teamPlayer)
    {
        $playerPointsData = $this->getTimelineRepository()->getTeamPlayerPointsByGames($teamPlayer,array('last_games' => 4));
        $playerPointsTemplate = array('goal' => 0, 'assist' => 0, 'save' => 0, 'hit' => 0);
        $playerPoints = array();
        foreach($playerPointsData as $data)
        {
           
            $playerPoints[$data['lineup_id']][$data['hit_type']] = $data['count'];
        }
        
        
        
        foreach($playerPoints as $key =>  $points)
        {
            $playerPoints[$key] = array_merge($playerPointsTemplate,$points);
        }

        krsort($playerPoints);
        return $playerPoints;
    }
    
    
    public function saveTimelineEvent($event)
    {
        return $this->getTimelineRepository()->save($event);
    }
    
    public function getEventTimeline($event)
    {
        $events = $this->getTimelineRepository()->getEventTimeline($event->getId(),$event->getCurrentDate()->format('Y-m-d H:i:s'));
        $matchTimeline = new MatchTimeline($events);
        
        return $matchTimeline;
    }
    
     public function getGroupedEventTimeline($event)
    {
        $timeGroups = array();
        $events = $this->getTimelineRepository()->getEventTimeline($event->getId(),$event->getCurrentDate()->format('Y-m-d H:i:s'));
        
        foreach($events as $event)
        {
           $timeGroups[$event->getHitGroup()][] = $event;
        }
        
        return $timeGroups;
    }
    
     public function getGroupedEventTimelineByLineup($lineup)
    {
         $timeGroups = array();
         $events = $this->getTimelineRepository()->getEventTimelineByLineup($lineup);
         foreach($events as $event)
        {
           $timeGroups[$event->getHitTime()][] = $event;
        }
        
        return $timeGroups;
    }
  
    public function getEventTimelineByLineup($lineup)
    {
        $events = $this->getTimelineRepository()->getEventTimelineByLineup($lineup);
        return $events;
    }
    
    public function getTimelineHitById($id)
    {
        $events = $this->getTimelineRepository()->find($id);
        return $events;
    }
    
     public function getTimelineHitsByHitGroup($hg)
    {
        $events = $this->getTimelineRepository()->findBy(array('hit_group' => $hg));
        return $events;
    }
    
  
    
    public function getLineupRatings($lineup)
    {
        if(null != $lineup)
        {
             $ratings = $this->getScoreRepository()->findBy(array('match_id' => $lineup->getId()));
            return $ratings;
        }
       
    }
    
    public function getTeamKbiCriteria()
    {
         //$crsSettings = $manager->getTeamSettings($team);
    }
    
     public function clearEventTimeline($event)
    {
        $this->getTimelineRepository()->deleteBy(array('event_id' =>$event->getId(),'event_date' =>  $event->getCurrentDate()->format('Y-m-d H:i:s')));
      
    }
    
    public function clearPlayerEventHits($criteria)
    {
        $this->getTimelineRepository()->deleteBy(array('lineup_id' =>$criteria['lineup_id'],'lineup_player_id' => $criteria['lineup_player_id'],'hit_type' =>   $criteria['hit_type']));
      
    }
    
    public function getBasketballMatchOverview($existLineup)
    {
        $matchOverview = $this->getTimelineRepository()->getBasketballMatchOverviewByLineup($existLineup);
        return $matchOverview;
    }
    
    public function getAllTeamPlayersBasketballStat($team, $dateFrom, $dateTo)
    {
         $allMatchs = $this->getTimelineRepository()->getBasketballMatchOverviewByDate($team, $dateFrom, $dateTo);
         $crsCriteria = $this->getTeamCRSCriteria($team);
         $playerStats= array();

         $maxValues = array(
            'game_played' => 0,
            'win' => 0,
            'draw' => 0,
            'loose' => 0,
            'plus' => 0,
            'minus' => 0,
            'goal' => 0,
            'assist' => 0,
            'mom' => 0,
            'looses' => 0,
            'ga' => 0,
            'crs' => 0,
            'pm-diff-max' => 0,
            'pm-diff-min' => 0,
            'points1' => 0,
            'points2' => 0,
            'points3' => 0,
            );
         
         foreach($allMatchs as $teamPlayerId => $matchList)
         {
            $wins = 0;
            $looses = 0;
            $draws = 0;
            $plusPoints = 0;
            $minusPoints = 0;
             $matchCount = 0;
             foreach($matchList as $match)
             {
                
                 if(!array_key_exists($teamPlayerId, $playerStats))
                 {
                     $playerStats[$teamPlayerId] = new PlayerBasketballStat();
                     $playerStats[$teamPlayerId]->setCRSCriteria($crsCriteria);
                 }
                 $stat = $playerStats[$teamPlayerId];
                 $stat->setCRSCriteria($crsCriteria);
                 $stat->setGamePlayed($stat->getGamePlayed()+1);
                 
                 $stat->setPoints1($stat->getPoints1()+$match->getPoints1());
                 $stat->setPoints2($stat->getPoints2()+$match->getPoints2());
                 $stat->setPoints3($stat->getPoints3()+$match->getPoints3());
                 $stat->setManOfMatch($stat->getManOfMatch()+$match->getManOfMatch());
               
                 
                 
                //wins, draws, looses
                if($match->getFirstLineGoals() == $match->getSecondLineGoals())
                {
                    $draws++;
                }
                else 
                {
                    if($match->getFirstLineGoals() > $match->getSecondLineGoals() && $match->getLineupPosition()  == 'first_line')
                    {
                       $wins++;
                    }
                    elseif($match->getFirstLineGoals() < $match->getSecondLineGoals() && $match->getLineupPosition() == 'second_line')
                    {
                        $wins++;
                    }
                    else 
                    {
                        $looses++;
                    }
                }

                //+- points
                if($match->getLineupPosition() == 'first_line')
                {
                    $plusPoints += $match->getFirstLineGoals();
                    $minusPoints += $match->getSecondLineGoals();
                }
                elseif($match->getLineupPosition() == 'second_line') 
                {
                     $plusPoints += $match->getSecondLineGoals();
                     $minusPoints += $match->getFirstLineGoals();
                }
                
                $stat->setWins($wins);
                $stat->setLooses($looses);
                $stat->setDraws($draws);
                $stat->setPlusPoints($plusPoints);
                $stat->setMinusPoints($minusPoints);
                $playerStats[$teamPlayerId]->setPlayer($match->getPlayer());
                $matchCount++;
                
                
             }
             
            if($maxValues['points1'] < $stat->getPoints1())
            {
                $maxValues['points1'] = $stat->getPoints1();
            }
             
            if($maxValues['points2'] < $stat->getPoints2())
            {
                $maxValues['points2'] = $stat->getPoints2();
            }
             
            if($maxValues['points3'] < $stat->getPoints3())
            {
                $maxValues['points3'] = $stat->getPoints3();
            }
             
             
            if($maxValues['mom'] < $stat->getManOfMatch())
            {
                $maxValues['mom'] = $stat->getManOfMatch();
            }
             
             
            if($maxValues['game_played'] < $matchCount)
            {
                $maxValues['game_played'] = $matchCount;
            }


            if($maxValues['win'] < $wins)
            {
                $maxValues['win'] = $wins;
            }

            if($maxValues['draw'] < $draws)
            {
                $maxValues['draw'] = $draws;
            }


            if($maxValues['looses'] < $looses)
            {
                $maxValues['looses'] = $looses;
            }

            if($maxValues['plus'] < $plusPoints)
            {
                $maxValues['plus'] = $plusPoints;
            }

            if($maxValues['minus'] < $minusPoints)
            {
                $maxValues['minus'] = $minusPoints;
            }
             
            
             if($maxValues['pm-diff-max'] < $stat->getPlusMinusDiff())
            {
                $maxValues['pm-diff-max'] = $stat->getPlusMinusDiff();
            }
            
            
            if($maxValues['pm-diff-min'] > $stat->getPlusMinusDiff())
            {
                $maxValues['pm-diff-min'] = $stat->getPlusMinusDiff();
            }
            
           
           
         }
         foreach($playerStats as $playerStat)
         {
             $playerStat->setMaxValues($maxValues);
         }
         
         return $playerStats;


    }
    
    /**
     * Return  all team match results between dates
     */
    public function getTeamMatchResults($team,$resultStart,$resultEnd)
    {
         $matchResults = \Core\ServiceLayer::getService('TeamMatchLineupRepository')->findTeamMatchResults($team,$resultStart,$resultEnd);
         t_dump($matchResults);
         return $matchResults;
         
    }
    
    
    public function toArray($stats)
    {
        $teamPlayer = $stats->getPlayer();
       $data = array(
                        'name' => $teamPlayer->getFirstName(),
                        'surname' => $teamPlayer->getLastName(),
                        'game_played' => $stats->getGamePlayed(),
                        'wins' => $stats->getWins(),
                        'draws'=> $stats->getDraws(),
                        'looses'=> $stats->getLooses(),
                        'win_prediction'=> $stats->getWinsPrediction(),
                        's_plus'=> $stats->getPlusPoints(),
                        's_minus'=> $stats->getMinusPoints(),
                        'plus_minus_diff' => $stats->getPlusMinusDiff(),
                        'goals' => $stats->getGoals(),
                        'assists' => $stats->getAssists(),
                        'average_goals' => $stats->getAverageGoals(),
                        'average_assists' => $stats->getAverageAssists(),
                        'ga' => $stats->getGA(),
                        'average_ga' => $stats->getAverageGA(),
                        'crs' => $stats->getCRS(),
                        'average_crs' => $stats->getAverageCRS(),
                        'gaa' => $stats->getGoalAgainstAverage(),
                        'shotouts' => $stats->getGoalkeeperShotouts(),
                        'mom' => $stats->getManOfMatch(),
                        'average_mom' => $stats->getAverageManOfMatch(),
                    );
       
       return $data;
    
        /*
        $list = array();
        foreach ($objectList as $object)
        {
            if(!is_array($object))
            {
                 $data_mapper = $object->getDataMapperRules();
                $values = array();

                foreach ($data_mapper as $key => $props)
                {
                    $method_name_parts = explode('_', $key);
                    $method_name_parts = array_map('ucfirst', $method_name_parts);
                    $method_name = 'get' . implode($method_name_parts);


                    if (method_exists($object, $method_name))
                    {
                        if ('datetime' == $props['type'])
                        {
                            $date_object = call_user_func(array($object, $method_name));
                            if (null == $date_object)
                            {
                                $values[$key] = null;
                            }
                            else
                            {
                                $values[$key] = $date_object->format('Y-m-d H:i:s');
                            }
                        }
                        elseif ('boolean' == $props['type'])
                        {
                            $bool_data = call_user_func(array($object, $method_name));
                            if (null !== $bool_data)
                            {
                                $values[$key] = ($bool_data === true) ? 1 : 0;
                            }
                            else
                            {
                                $values[$key] = null;
                            }
                        }
                        else
                        {
                            $values[$key] = call_user_func(array($object, $method_name));
                        }
                    }  
                }


                $list[] = $values;
            }
            else
            {
                $list[] = $object;
            }
           
        }
        return $list;
         * 
         */
        
        
      
    }
    
    
    /**
     * Create ladder from historical data
     * @param type $playersStats
     * @param type $team
     * @return int
     */
    public function createLadderFromHistoryData($playersStats,$team)
    {
        $teamManager = \Core\ServiceLayer::getService('TeamManager');
        $teamMembers = $teamManager->getTeamPlayers($team->getId());
        $indexedTeamMembers = array();
        foreach ($teamMembers as $teamMember)
        {
            $indexedTeamMembers[$teamMember->getId()] = $teamMember;
        }

        $ladder = array();
        foreach ($playersStats as $index => $statData)
        {
            $playerStat = new \Webteamer\Player\Model\PlayerStat();
            $playerStat->setGoals($statData['goals']);
            $playerStat->setAssists($statData['assists']);
            $playerStat->setWins($statData['wins']);
            $playerStat->setLooses($statData['looses']);
            $playerStat->setDraws($statData['draws']);
            $playerStat->setManOfMatch($statData['mom']);

            $crs = $playerStat->getCRS() * 1000;
            if (array_key_exists($crs, $ladder))
            {
                $crs = ($playerStat->getCRS() * 1000) + 1;
            }
            $ladder[$crs] = array(
                'crs' => $playerStat->getCRS(),
                'name' => $indexedTeamMembers[$index]->getFullName(),
                'img' => $indexedTeamMembers[$index]->getMidPhoto(),
                'teamPlayerId' => $indexedTeamMembers[$index]->getId(),
            );
        }

        krsort($ladder);
        //reindex by team player id
        $teamPlayerIdLadder = array();
        $positionLadder = array();
        $position = 1;
        foreach ($ladder as $index => $data)
        {
            $data['position'] = $position;
            $teamPlayerIdLadder[$data['teamPlayerId']] = $data;
            $positionLadder[$position] = $data;
            $position++;
        }

        $finalLadder = array(
            'position' => $positionLadder,
            'players' => $teamPlayerIdLadder
        );

         return $finalLadder;
      
    }
    
    
    /**
     * Create ladder from current stats data
     * @param type $team
     * @return int
     */
     public function createCurrentLadder($team)
    {
      
        $teamManager = \Core\ServiceLayer::getService('TeamManager');
        $teamMembers = $teamManager->getTeamPlayers($team->getId());
        $indexedTeamMembers = array();
        foreach ($teamMembers as $teamMember)
        {
            $indexedTeamMembers[$teamMember->getId()] = $teamMember;
        }
        
          $seasonManager = \Core\ServiceLayer::getService('TeamSeasonManager');

        $actualSeason = $seasonManager->getTeamCurrentSeason($team);
        $seasonId = $actualSeason->getId();
        $currentSeason = $seasonManager->getSeasonById($seasonId);

        $statFrom = new DateTimeEx($currentSeason->getStartDate());
        $statTo = new DateTimeEx($currentSeason->getEndDate());
        $currentStats = $this->getAllTeamPlayersStat($team, null, $statFrom, $statTo);


        $ladder = array();
        foreach ($currentStats as $index => $playerStat)
        {
            if (is_a($playerStat, 'Webteamer\Player\Model\PlayerStat'))
            {
                $crs = $playerStat->getCRS() * 1000;
                if (array_key_exists($crs, $ladder))
                {
                    $crs = ($playerStat->getCRS() * 1000) + 1;
                }
                $ladder[$crs] = array(
                    'crs' => $playerStat->getCRS(),
                    'name' => $indexedTeamMembers[$index]->getFullName(),
                    'img' => $indexedTeamMembers[$index]->getMidPhoto(),
                    'teamPlayerId' => $indexedTeamMembers[$index]->getId(),
                );
            }
        }

        krsort($ladder);
        //reindex by team player id
        $teamPlayerIdLadder = array();
        $positionLadder = array();
        $position = 1;
        foreach ($ladder as $index => $data)
        {
            $data['position'] = $position;
            $teamPlayerIdLadder[$data['teamPlayerId']] = $data;
            $positionLadder[$position] = $data;
            $position++;
        }

        $finalLadder = array(
            'position' => $positionLadder,
            'players' => $teamPlayerIdLadder
        );

       return $finalLadder;
      
    }
    
           

}

?>