<?php
namespace CR\Player\Model;
use Core\DbStorage;
class PlayerFollowStorage extends DbStorage {
    
	public function getPlayerFollowers($playerId)
        {
             $data = \Core\DbQuery::prepare('
               SELECT p.*
                FROM player_follow pf
                LEFT JOIN co_users p ON pf.follower_id = p.id
                WHERE pf.player_id = :id')
                 ->bindParam(':id', $playerId)
		->execute()
		->fetchAll(\PDO::FETCH_ASSOC);
		return $data;
        }

}