<?php

namespace CR\Player\Model;

use Core\DbStorage;
use Core\EntityMapper;
use Core\Repository as Repository;
use Core\ServiceLayer;


class PlayerFollowRepository extends Repository {
    
  public function __construct($storage) 
  {
        $this->storage = $storage;
   }
   
   public function createFollower($player,$follower)
   {
       $data['player_id'] = $player->getId();
       $data['follower_id'] = $follower->getId();
       $data['created_at'] = date('Y-m-d H:i:s');
       $this->getStorage()->create($data);
   }
   
   public function removeFollower($player,$follower)
   {
      $this->deleteBy(array('follower_id' => $follower->getId(), 'player_id' => $player->getId() ));
   }
   
   public function getPlayerFollowers($player)
   {
       $data = $this->getStorage()->getPlayerFollowers($player->getId());
       $list = array();
        foreach ($data as $d)
        {
            $player = new \Webteamer\Player\Model\Player();
            $player->setId($d['id']);
            $player->setName($d['name']);
            $player->setSurname($d['surname']);
            $player->setPhoto($d['photo']);

            $list[] = $player;
        }
        return $list;
   }
}