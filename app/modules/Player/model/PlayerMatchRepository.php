<?php
namespace Webteamer\Player\Model;
use Core\Repository as Repository;




class PlayerMatchRepository extends Repository
{
    private $teamStorage;
    public function __construct($storage, $factory, $teamStorage)
    {
        parent::__construct($storage, $factory);
        $this->setTeamStorage($teamStorage);
    }
    
    public  function setTeamStorage($teamStorage) {
$this->teamStorage = $teamStorage;
}

    public function getTeamStorage()
    {
        return $this->teamStorage;
    }
    
    public function saveTeam($data)
    {
        $id = $this->getTeamStorage()->create($data);
        return $id;
    }
    
    
   public function getIndividualPlayerStat($player)
   {
       $stat= $this->getStorage()->getIndividualPlayerStat($player->getPlayerId());
       return $stat;
   }
   
   public function getTeamPlayerStat($player)
   {
       $stat= $this->getStorage()->getTeamPlayerStat($player->getPlayerId());
       return $stat;
   }
    
}
