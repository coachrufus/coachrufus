<?php
namespace Webteamer\Player\Model;
use Core\Repository as Repository;



class PlayerScoreRepository extends Repository
{
    public function getPlayerAverageRating($player)
    {
        return $this->getStorage()->getPlayerAverageRating($player->getId());
    }
    public function getPlayerTeamAverageRating($player,$team)
    {
        return $this->getStorage()->getPlayerTeamAverageRating($player->getId(),$team->getId());
    }
}
