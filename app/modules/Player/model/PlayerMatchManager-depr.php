<?php

namespace Webteamer\Player\Model;

use Core\ServiceLayer;

class PlayerMatchManager  extends \Core\Manager
{
   public function saveTeam($data)
   {
       $teamId = $this->getRepository()->saveTeam($data);
       return $teamId;
   }
   
   public function savePlayerMatch($match)
   {
       $this->repository->save($match);
   }
   
   public function getPlayerStat($player)
   {
        $stat = $this->getRepository()->getIndividualPlayerStat($player);
        $teamStats = $this->getRepository()->getTeamPlayerStat($player);
        $playerStat = new PlayerStat();
        
        $plusPoints = 0;
        $minusPoints = 0;
        foreach($teamStats as $teamStat)
        {
            if($teamStat['result'] == 'win')
            {
                $playerStat->setWins($teamStat['count']);
            }
            if($teamStat['result'] == 'loose')
            {
                $playerStat->setLooses($teamStat['count']);
            }
            if($teamStat['result'] == 'draw')
            {
                $playerStat->setDraws($teamStat['count']);
            }
            
            $plusPoints += $teamStat['plus_goals'];
            $minusPoints += $teamStat['minus_goals'];
        }
        $playerStat->setPlusPoints($plusPoints);
        $playerStat->setMinusPoints($minusPoints);
        $playerStat->setPlayer($player);
        $playerStat->setGamePlayed($stat['game_played']);
        $playerStat->setGoals($stat['goals']);
        $playerStat->setAssists($stat['assists']);
        $playerStat->setSaves($stat['saves']);
        $playerStat->setManOfMatch($stat['man_of_match']);
       
       return $playerStat;
   }
   
   public function getPlayerMatchByTeam($team)
   {
       return  $this->getRepository()->findBy(array('team_id'=> $team->getId() ));
   }
   
  
   
   public function userCanRatePlayerMatch($user,$playerMatch)
   {
       foreach($playerMatch->getRatings() as $rating)
       {
           if($user->getId() == $rating->getAuthorId())
           {
               return false;
           }
       }
       return true;
   }
   
   public function getPlayerRatingByAuthor($user,$playerMatch)
   {
       foreach($playerMatch->getRatings() as $rating)
       {
           if($user->getId() == $rating->getAuthorId())
           {
               return $rating->getScore();
           }
       }
       return false;
   }
   
   public function getPlayerRating($playerMatch)
   {
       $totalRating = 0;
       $count = 0;
       foreach($playerMatch->getRatings() as $rating)
       {
          $totalRating += $rating->getScore();
          $count++;
       }
       
       if($count> 0)
       {
           return round($totalRating/$count,2);
       }
       
       return false;
   }
   
    
}
