<?php

namespace CR\Player\Model;

use Core\Manager;
use DateTime;

/**
 * @author Marek Hubacek
 * @version 1.0
 * @created 18-12-2015 11:11:39
 */
class PlayerFriendManager extends Manager {

   
   public function createFriend($player,$friend)
   {
       $this->getRepository()->createFriend($player,$friend);
   }
   
   public function removeFriend($friend)
   {
       $this->getRepository()->delete($friend->getId());
   }
   
   public function getPlayerFriends($player)
   {
       return $this->getRepository()->getPlayerFriends($player);
   }
   
   public function getPlayerFriendById($player,$friendId)
   {
       $friend = $this->getRepository()->find($friendId);
       if(null != $friend)
       {
           if( $friend->getPlayerId() != $player->getId())
           {
               throw new \AclException();
           }
       }
       return $friend;
   }
   
   public function confirmFriend($friend)
   {
       $friend->setStatus('confirmed');
       $this->getRepository()->save($friend);       
   }
   
   public function findPlayerFriends($user,$term)
   {
        $friends = $this->getRepository()->findPlayerFriends($user,$term);  
        return $friends;
   }
   
 



}

?>