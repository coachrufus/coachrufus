<?php
namespace Webteamer\Player\Model;

use Core\DbQuery;
use Core\DbStorage;
use PDO;
use Core\Types\DateTimeEx;

class PlayerTimelineEventStorage extends DbStorage
{
       public function getEventTimeline($eventId,$eventDate)
       {
           $sql = 'SELECT e.*, p.player_name FROM player_timeline_event e 
                    LEFT JOIN team_match_lineup_player p ON e.lineup_player_id = p.id
                    where e.event_id = :event_id and e.event_date = :event_date order by id asc';
           
           $query = \Core\DbQuery::prepare($sql);
           $query->bindParam(':event_id', $eventId);
           $query->bindParam(':event_date', $eventDate);
            $data = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);

            
            return $data;
       }
       
       public function getEventTimelineByLineup($lineupId)
       {
           $sql = 'SELECT e.*, p.player_name FROM player_timeline_event e 
                    LEFT JOIN team_match_lineup_player p ON e.lineup_player_id = p.id
                    where e.lineup_id = :linup_id order by e.id asc';
           
           $query = \Core\DbQuery::prepare($sql);
           $query->bindParam(':linup_id', $lineupId);
           $data = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);

            
            return $data;
       }

       public function getPlayerMatchCount($playerId,$teamId)
       {
            $sql = 'SELECT count(*) as count FROM (select lp.id
                    from team_match_lineup_player lp 
                    LEFT JOIN player_timeline_event te ON lp.lineup_id = te.lineup_id
                    WHERE te.id is not null
                    AND lp.player_id = :id
                    AND te.team_id = :team_id
                    GROUP BY lp.lineup_id) player_match';
           $query = \Core\DbQuery::prepare($sql);
           $query->bindParam(':id', $playerId);
           $query->bindParam(':team_id', $teamId);
           $data = $query->execute()->fetchOne(\PDO::FETCH_ASSOC);
           return $data; 
       }
       
       public function getTeamPlayersPoints($teamId, int $seasonId = null, DateTimeEx $dateFrom = null, DateTimeEx $dateTo = null)
       {
           $sql = 'SELECT hit_type, count(hit_type) AS count, lp.team_player_id, player_timeline_event.event_date
                   FROM player_timeline_event   
                   [SEASON.JOIN]
                   LEFT JOIN team_match_lineup_player lp ON player_timeline_event.lineup_player_id = lp.id
                   LEFT JOIN team_match_lineup lip ON lp.lineup_id = lip.id
                   WHERE player_timeline_event.team_id = :team_id AND lip.status = "closed" 
                   [SEASON.AND_CONDITION] [DATE.AND_CONDITION]
                   GROUP BY hit_type, lp.team_player_id';
           $sql = $this->addSeason($sql, 'player_timeline_event', $seasonId);
           $sql = $this->addDateFilter($sql, 'player_timeline_event', $dateFrom, $dateTo);
           $query = \Core\DbQuery::prepare($sql);
           $query->bindParam(':team_id', $teamId);
           if (!is_null($seasonId)) $query->bindParam(':seasonId', $seasonId);
           if (!is_null($dateFrom) && !is_null($dateTo))
           {
               $query->bindParam(':dateFrom', $dateFrom->toString('Y-m-d'));
               $query->bindParam(':dateTo', $dateTo->toString('Y-m-d'));
           }
           return $query->execute()->fetchAll(\PDO::FETCH_ASSOC); 
       }   
       
       public function getPlayerPoints($playerId, $teamId)
       {
           $sql = '
            SELECT hit_type, count(hit_type) AS count
            FROM player_timeline_event 
            WHERE user_id = :id
            AND team_id = :team_id
            GROUP BY hit_type';
            $query = \Core\DbQuery::prepare($sql);
            $query->bindParam(':id', $playerId);
            $query->bindParam(':team_id', $teamId);
            $data = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
            return $data; 
       }
       
        public function getTeamPlayerPointsByGamesCount($playerId, $teamId, $gamesCount)
        {
            $sql = "
            SELECT te.lineup_id,hit_type,count(hit_type) AS count FROM (
                SELECT lineup_id, user_id
                FROM  player_timeline_event 
                WHERE user_id = :id
                AND team_id = :team_id
                GROUP BY lineup_id
                ORDER BY lineup_id desc
                LIMIT 0, {$gamesCount}
            ) lp LEFT JOIN player_timeline_event te ON lp.lineup_id = te.lineup_id and lp.user_id = te.user_id
            GROUP BY te.hit_type, te.lineup_id order by te.lineup_id";
            $query = \Core\DbQuery::prepare($sql);
            $query->bindParam(':id', $playerId);
            $query->bindParam(':team_id', $teamId);
            return $query->execute()->fetchAll(\PDO::FETCH_ASSOC); 
        }   
        
         
        public function getTeamPlayersPointsByGamesCount($teamId, $gamesCount)
        {
            $sql = "
           SELECT l.id, 
                e.hit_type, 
                Count(hit_type) AS points, 
                e.lineup_position, 
                e.user_id, 
                lp.player_id, 
                lp.player_name, 
                lp.team_player_id 
         FROM   (SELECT l.id 
                 FROM   team_match_lineup l 
                 WHERE  l.team_id = :team_id 
                        AND l.status = 'closed' 
                 ORDER  BY id DESC 
                 LIMIT  0, {$gamesCount}) l 
                LEFT JOIN player_timeline_event e 
                       ON l.id = e.lineup_id 
                LEFT JOIN team_match_lineup_player lp 
                       ON e.lineup_player_id = lp.id 
                where lp.team_player_id is not null
         GROUP  BY team_player_id, 
                   hit_type, 
                   l.id ";
            $query = \Core\DbQuery::prepare($sql);
            $query->bindParam(':team_id', $teamId);
            return $query->execute()->fetchAll(\PDO::FETCH_ASSOC); 
        }   
        
        
        

        

        private function addSeason($sql, $table, $seasonId)
        {
            foreach ([
                'JOIN' => "JOIN team_event te ON {$table}.event_id = te.id",
                'WHERE_CONDITION' => 'WHERE te.season = :seasonId',
                'AND_CONDITION' => 'AND te.season = :seasonId'
            ] as $key => $value)
            {
                $sql = str_replace("[SEASON.{$key}]",
                    is_null($seasonId) ? "" : $value, $sql);
            }
            return $sql;
        }
        
        private function addDateFilter($sql, $table, $dateFrom, $dateTo)
        {
            foreach ([
                'AND_CONDITION' => "AND DATE({$table}.event_date) >= :dateFrom AND DATE({$table}.event_date) <= :dateTo"
            ] as $key => $value)
            {
                $sql = str_replace("[DATE.{$key}]",
                    is_null($dateFrom) || is_null($dateTo)  ? "" : $value, $sql);
            }
            return $sql;
        }
        
        public function getAllTeamPlayerMatchResults($teamId, int $seasonId = null, DateTimeEx $dateFrom = null, DateTimeEx $dateTo = null)
        {
           $sql = '
            SELECT d.* FROM (
                SELECT 
                    tp.player_id,
                    tp.team_player_id,
                    tp.lineup_position,
                    tp.lineup_id,
                    tp.event_date,
                    tp.player_position,
                    first_line_goals,
                    second_line_goals
                FROM team_match_lineup_player tp 
                LEFT JOIN team_players t ON tp.team_player_id = t.id
                [SEASON.JOIN]
                LEFT JOIN (
                    SELECT lineup_id, count(hit_type) AS first_line_goals FROM player_timeline_event
                    WHERE hit_type="goal" and lineup_position = "first_line" and team_id = :first_line_team_id AND lineup_player_id > 0 
                    GROUP BY lineup_id
                ) f ON tp.lineup_id = f.lineup_id
                LEFT JOIN (
                    SELECT lineup_id, count(hit_type) AS second_line_goals
                    FROM player_timeline_event
                    WHERE hit_type="goal" and lineup_position = "second_line" and team_id = :second_line_team_id AND lineup_player_id > 0 
                    GROUP BY lineup_id
                ) s ON tp.lineup_id = s.lineup_id
                LEFT JOIN team_match_lineup lp ON tp.lineup_id = lp.id
                WHERE t.team_id = :team_id 
                AND lp.status = "closed"
                [SEASON.AND_CONDITION]
                [DATE.AND_CONDITION]
                GROUP BY tp.lineup_id, tp.event_date, tp.team_player_id ORDER BY tp.lineup_id
            ) d';
            $sql = $this->addSeason($sql, 'tp', $seasonId);
            $sql = $this->addDateFilter($sql, 'tp', $dateFrom, $dateTo);
            $query = \Core\DbQuery::prepare($sql);
            $query->bindParam(':team_id', $teamId);
            $query->bindParam(':first_line_team_id', $teamId);
            $query->bindParam(':second_line_team_id', $teamId);
            if (!is_null($seasonId)) $query->bindParam(':seasonId', $seasonId);
            if (!is_null($dateFrom) && !is_null($dateTo))
            {
                $query->bindParam(':dateFrom', $dateFrom->toString('Y-m-d'));
                $query->bindParam(':dateTo', $dateTo->toString('Y-m-d'));
            }
            return $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
        }
       
       public function getTeamPlayerMatchResults($playerId,$teamId)
       {
           
           $sql = ' select d.* from (SELECT tp.player_id, tp.lineup_position, tp.lineup_id, first_line_goals, second_line_goals from  team_match_lineup_player tp 

LEFT JOIN (select lineup_id, count(hit_type) as first_line_goals from player_timeline_event where hit_type="goal" and lineup_position = "first_line"  and team_id = :first_line_team_id
                    group by lineup_id ) f ON tp.lineup_id = f.lineup_id
                    
LEFT JOIN (select lineup_id, count(hit_type) as second_line_goals from player_timeline_event where hit_type="goal" and lineup_position = "second_line"   and team_id = :second_line_team_id
                    group by lineup_id ) s ON  tp.lineup_id = s.lineup_id                   
                    


group by tp.lineup_id,  tp.player_id  order by tp.lineup_id) d where d.player_id = :id';
           
           /*
            $sql = ' SELECT tp.lineup_position, tp.lineup_id, first_line_goals, second_line_goals from  team_match_lineup_player tp 

LEFT JOIN (select lineup_id, count(hit_type) as first_line_goals from player_timeline_event where hit_type="goal" and lineup_position = "first_line"  and team_id = :first_line_team_id
                    group by lineup_id ) f ON tp.lineup_id = f.lineup_id
                    
LEFT JOIN (select lineup_id, count(hit_type) as second_line_goals from player_timeline_event where hit_type="goal" and lineup_position = "second_line"   and team_id = :second_line_team_id
                    group by lineup_id ) s ON  tp.lineup_id = s.lineup_id                   
                    

WHERE tp.player_id = :id 
group by tp.lineup_id order by tp.lineup_id';
            * */
            
           
           $query = \Core\DbQuery::prepare($sql);
           $query->bindParam(':id', $playerId);
           $query->bindParam(':first_line_team_id', $teamId);
           $query->bindParam(':second_line_team_id', $teamId);
           $data = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);

           return $data; 
          
       }
       
       public function getPlayerAllMatchOverview($playerId)
       {
           
            $sql = 'SELECT tp.lineup_position, 
                tp.lineup_id, 
                first_line_goals, 
                second_line_goals, 
                t.name, 
                tp.event_date, 
                tp.event_id, 
                goal.goals, 
                assist.assist, 
                lp.first_line_name  AS "first_line", 
                lp.second_line_name AS "second_line" 
             FROM   team_match_lineup_player tp 
                    LEFT JOIN (SELECT lineup_id, 
                                      Count(hit_type) AS first_line_goals 
                               FROM   player_timeline_event 
                               WHERE  hit_type = "goal" 
                                      AND lineup_position = "first_line" 
                               GROUP  BY lineup_id) f 
                           ON tp.lineup_id = f.lineup_id 
                    LEFT JOIN (SELECT lineup_id, 
                                      Count(hit_type) AS second_line_goals 
                               FROM   player_timeline_event 
                               WHERE  hit_type = "goal" 
                                      AND lineup_position = "second_line" 
                               GROUP  BY lineup_id) s 
                           ON tp.lineup_id = s.lineup_id 
                    LEFT JOIN team_match_lineup lp 
                           ON tp.lineup_id = lp.id 
                    LEFT JOIN team t 
                           ON lp.team_id = t.id 
                    LEFT JOIN (SELECT lineup_id, 
                                      Count(hit_type) AS "goals" 
                               FROM   player_timeline_event 
                               WHERE  user_id = :id1 
                                      AND hit_type = "goal" 
                               GROUP  BY lineup_id) goal 
                           ON tp.lineup_id = goal.lineup_id 
                    LEFT JOIN (SELECT lineup_id, 
                                      Count(hit_type) AS "assist" 
                               FROM   player_timeline_event 
                               WHERE  user_id = :id2 
                                      AND hit_type = "assist" 
                               GROUP  BY lineup_id) assist 
                           ON tp.lineup_id = goal.lineup_id 
             WHERE  tp.player_id = :id3 
             GROUP  BY tp.lineup_id 
             ORDER  BY tp.lineup_id ';
           
           $query = \Core\DbQuery::prepare($sql);
           $query->bindParam(':id1', $playerId);
           $query->bindParam(':id2', $playerId);
           $query->bindParam(':id3', $playerId);
           $data = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
           return $data; 
       }
       
       public function getTeamPlayerAllMatchOverview(int $teamPlayerId, DateTimeEx $dateFrom = null, DateTimeEx $dateTo = null)
       {
            $sql = 'SELECT tp.lineup_position, 
                tp.lineup_id, 
                first_line_goals, 
                second_line_goals, 
                t.name, 
                tp.event_date, 
                tp.event_id, 
                goal.goals, 
                assist.assist, 
                lp.first_line_name  AS "first_line", 
                lp.second_line_name AS "second_line" 
             FROM team_match_lineup_player tp 
                    LEFT JOIN (SELECT lineup_id, 
                                      Count(hit_type) AS first_line_goals 
                               FROM   player_timeline_event 
                               WHERE  hit_type = "goal" 
                                      AND lineup_position = "first_line" 
                                      AND lineup_player_id > 0
                               GROUP  BY lineup_id) f 
                           ON tp.lineup_id = f.lineup_id 
                    LEFT JOIN (SELECT lineup_id, 
                                      Count(hit_type) AS second_line_goals 
                               FROM   player_timeline_event 
                               WHERE  hit_type = "goal" 
                                      AND lineup_position = "second_line" 
                                      AND lineup_player_id > 0
                               GROUP  BY lineup_id) s 
                           ON tp.lineup_id = s.lineup_id 
                    LEFT JOIN team_match_lineup lp 
                           ON tp.lineup_id = lp.id 
                    LEFT JOIN team t 
                           ON lp.team_id = t.id 
                    LEFT JOIN (SELECT player_timeline_event.lineup_id, 
                                      Count(hit_type) AS "goals" 
                               FROM   player_timeline_event 
                               LEFT JOIN team_match_lineup_player ON player_timeline_event.lineup_player_id = team_match_lineup_player.id
                               WHERE  team_match_lineup_player.team_player_id = :id1
                                      AND hit_type = "goal" 
                               GROUP  BY player_timeline_event.lineup_id ) goal 
                           ON tp.lineup_id = goal.lineup_id 
                    LEFT JOIN (SELECT player_timeline_event.lineup_id, 
                                      Count(hit_type) AS "assist" 
                               FROM   player_timeline_event 
                               LEFT JOIN team_match_lineup_player ON player_timeline_event.lineup_player_id = team_match_lineup_player.id
                               WHERE  team_match_lineup_player.team_player_id = :id2
                                      AND hit_type = "assist" 
                               GROUP  BY player_timeline_event.lineup_id) assist 
                           ON tp.lineup_id = assist.lineup_id 
             WHERE  tp.team_player_id = :id3 [DATE.AND_CONDITION]
             AND lp.status = "closed"
             GROUP  BY tp.lineup_id 
             ORDER  BY tp.lineup_id';
           $sql = $this->addDateFilter($sql, 'tp', $dateFrom, $dateTo);
           $query = \Core\DbQuery::prepare($sql);
           $query->bindParam(':id1', $teamPlayerId);
           $query->bindParam(':id2', $teamPlayerId);
           $query->bindParam(':id3', $teamPlayerId);
           if (!is_null($dateFrom) && !is_null($dateTo))
           {
               $query->bindParam(':dateFrom', $dateFrom->toString('Y-m-d'));
               $query->bindParam(':dateTo', $dateTo->toString('Y-m-d'));
           }
           $data = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
           return $data; 
       }
       
        public function getPlayerMatchOverview($playerId,$lineupId)
       {
           
            $sql = ' SELECT tp.lineup_position, tp.lineup_id, first_line_goals, second_line_goals , t.name, tp.event_date, tp.event_id, goal.goals, assist.assist, lp.first_line_name as "first_line", 
 lp.second_line_name   as "second_line"  
                from  team_match_lineup_player tp 

LEFT JOIN (select lineup_id, count(hit_type) as first_line_goals from player_timeline_event where hit_type="goal" and lineup_position = "first_line" 
                    group by lineup_id ) f ON tp.lineup_id = f.lineup_id
                    
LEFT JOIN (select lineup_id, count(hit_type) as second_line_goals from player_timeline_event where hit_type="goal" and lineup_position = "second_line" 
                    group by lineup_id ) s ON  tp.lineup_id = s.lineup_id         
						  
LEFT JOIN team_match_lineup lp On tp.lineup_id = lp.id      
LEFT JOIN team t On lp.team_id = t.id   

LEFT JOIN (select lineup_id,count(hit_type) as "goals"
                    from  player_timeline_event 
                    WHERE user_id = :id1
                    and hit_type = "goal"
                    GROUP BY lineup_id) goal ON tp.lineup_id = goal.lineup_id        
						  
LEFT JOIN (select lineup_id,count(hit_type) as "assist"
from  player_timeline_event 
WHERE user_id = :id2
and hit_type = "assist"
GROUP BY lineup_id) assist ON tp.lineup_id = goal.lineup_id    

WHERE tp.player_id = :id3 
AND tp.lineup_id = :lineup_id
group by tp.lineup_id order by tp.lineup_id';
           
           $query = \Core\DbQuery::prepare($sql);
           $query->bindParam(':id1', $playerId);
           $query->bindParam(':id2', $playerId);
           $query->bindParam(':id3', $playerId);
           $query->bindParam(':lineup_id', $lineupId);
           $data = $query->execute()->fetchOne(\PDO::FETCH_ASSOC);
           return $data; 
          
       }
       
       
       /**
        * get match overview
        * @param type $lineupId
        * @return type
        */
        public function getMatchOverview($lineupId)
       {
           
            $sql = 'SELECT tp.lineup_position, tp.lineup_id, first_line_goals, second_line_goals , t.name, tp.event_date, tp.event_id,  lp.first_line_name as "first_line", 
 lp.second_line_name   as "second_line"  
                from  team_match_lineup_player tp 

LEFT JOIN (select lineup_id, count(hit_type) as first_line_goals from player_timeline_event where hit_type="goal" and lineup_position = "first_line"   AND lineup_player_id > 0
                    group by lineup_id ) f ON tp.lineup_id = f.lineup_id
                    
LEFT JOIN (select lineup_id, count(hit_type) as second_line_goals from player_timeline_event where hit_type="goal" and lineup_position = "second_line"   AND lineup_player_id > 0
                    group by lineup_id ) s ON  tp.lineup_id = s.lineup_id         
						  
LEFT JOIN team_match_lineup lp On tp.lineup_id = lp.id      
LEFT JOIN team t On lp.team_id = t.id   



WHERE tp.lineup_id = :lineup_id
group by tp.lineup_id order by tp.lineup_id';
           
           $query = \Core\DbQuery::prepare($sql);
         
           $query->bindParam(':lineup_id', $lineupId);
           $data = $query->execute()->fetchOne(\PDO::FETCH_ASSOC);
           return $data; 
          
       }
       
       public function getMatchOverviewByEvent($eventId,$eventDate)
       {
           
            $sql = 'SELECT tp.lineup_position, tp.lineup_id, first_line_goals, second_line_goals , t.name, tp.event_date, tp.event_id,  lp.first_line_name as "first_line", 
 lp.second_line_name   as "second_line"  
                from  team_match_lineup_player tp 

LEFT JOIN (select lineup_id, count(hit_type) as first_line_goals from player_timeline_event where hit_type="goal" and lineup_position = "first_line"   AND lineup_player_id > 0
                    group by lineup_id ) f ON tp.lineup_id = f.lineup_id
                    
LEFT JOIN (select lineup_id, count(hit_type) as second_line_goals from player_timeline_event where hit_type="goal" and lineup_position = "second_line"   AND lineup_player_id > 0
                    group by lineup_id ) s ON  tp.lineup_id = s.lineup_id         
						  
LEFT JOIN team_match_lineup lp On tp.lineup_id = lp.id      
LEFT JOIN team t On lp.team_id = t.id   



WHERE tp.event_id = :event_id and tp.event_date = :event_date and lp.status = "closed"
group by tp.lineup_id order by tp.lineup_id';
           
           $query = \Core\DbQuery::prepare($sql);
         
           $query->bindParam(':event_id', $eventId);
           $query->bindParam(':event_date', $eventDate);
           $data = $query->execute()->fetchOne(\PDO::FETCH_ASSOC);
           return $data; 
          
       }
       
       
       public function getSetMatchResults($teamId,$dateFrom,$dateTo)
       {
           $sql ='SELECT e.user_id,
                    e.points,
                    e.event_date,
                    e.event_id,
                    lp.player_name,
                    e.set_position,
                    e.lineup_id,
                    e.lineup_player_id,
                    lp.lineup_position,
                    lp.team_player_id
             FROM player_timeline_event e
             LEFT JOIN team_match_lineup_player lp ON e.lineup_player_id = lp.id
             WHERE e.team_id = :team_id
              [DATE.AND_CONDITION]
               AND hit_type = "set"';
            $sql = $this->addDateFilter($sql, 'e', $dateFrom, $dateTo);


            $query = \Core\DbQuery::prepare($sql);
           $query->bindParam(':team_id', $teamId);
           
            if (!is_null($dateFrom) && !is_null($dateTo))
           {
               $query->bindParam(':dateFrom', $dateFrom->toString('Y-m-d'));
               $query->bindParam(':dateTo', $dateTo->toString('Y-m-d'));
           }

           $data = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
           return $data; 
       }
       
      
       public function getVolleyballSetMatchResults($teamId,$dateFrom,$dateTo)
       {
           $sql ='SELECT e.user_id,
                    e.points,
                    e.event_date,
                    e.event_id,
                    lp.player_name,
                    e.set_position,
                    e.lineup_id,
                    e.lineup_player_id,
                    e.lineup_position,
                    lp.team_player_id
             FROM player_timeline_event e
             LEFT JOIN team_match_lineup_player lp ON e.lineup_player_id = lp.id
             WHERE e.team_id = :team_id
              [DATE.AND_CONDITION]
               AND hit_type = "volleybal_set"';
            $sql = $this->addDateFilter($sql, 'e', $dateFrom, $dateTo);


            $query = \Core\DbQuery::prepare($sql);
           $query->bindParam(':team_id', $teamId);
           
            if (!is_null($dateFrom) && !is_null($dateTo))
           {
               $query->bindParam(':dateFrom', $dateFrom->toString('Y-m-d'));
               $query->bindParam(':dateTo', $dateTo->toString('Y-m-d'));
           }

           $data = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
           return $data; 
       }
       
       private function getBasketballMatchBaseQuery()
       {
            $sql ='SELECT e.user_id,
                    e.points as "points1",
                    e2.points as "points2",
                    e3.points as "points3",
                    CASE
                        WHEN e4.hit_type = "mom" THEN 1
                        ELSE NULL
                    END AS mom,
                    e.event_date,
                    e.event_id,
                    lp.player_name,
                    e.set_position,
                    e.lineup_id,
                    e.lineup_player_id,
                    e.lineup_position,
                    lp.team_player_id
                  FROM player_timeline_event e
             LEFT JOIN player_timeline_event e2 ON e2.lineup_player_id = e.lineup_player_id AND e2.hit_type = "points2"
             LEFT JOIN player_timeline_event e3 ON e3.lineup_player_id = e.lineup_player_id AND e3.hit_type = "points3"
             LEFT JOIN player_timeline_event e4 ON e4.lineup_player_id = e.lineup_player_id AND e4.hit_type = "mom"
             LEFT JOIN team_match_lineup_player lp ON e.lineup_player_id = lp.id';
            
            return $sql;
       }
       
       public function getBasketballMatchResults($teamId,$dateFrom,$dateTo)
       {
           $sql = $this->getBasketballMatchBaseQuery().' WHERE e.team_id = :team_id [DATE.AND_CONDITION] AND (e.hit_type = "points1")';
           $sql = $this->addDateFilter($sql, 'e', $dateFrom, $dateTo);


           $query = \Core\DbQuery::prepare($sql);
           $query->bindParam(':team_id', $teamId);
           
            if (!is_null($dateFrom) && !is_null($dateTo))
           {
               $query->bindParam(':dateFrom', $dateFrom->toString('Y-m-d'));
               $query->bindParam(':dateTo', $dateTo->toString('Y-m-d'));
           }

           $data = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
           return $data; 
       }
       
       
       public function getBasketballMatchOverviewByLineupId($lineupId)
       {
           $sql = $this->getBasketballMatchBaseQuery().' WHERE e.lineup_id = :lineup_id AND (e.hit_type = "points1")';
          


           $query = \Core\DbQuery::prepare($sql);
           $query->bindParam(':lineup_id', $lineupId);
           $data = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
           return $data; 
       }
       
       
       public function getMatchResult($lineupId)
       {
           $sql ='select lineup_position, count(hit_type) as goals from player_timeline_event 
                where lineup_id = :lineupId
                and  hit_type="goal"
                group by lineup_position';
         

           $query = \Core\DbQuery::prepare($sql);
           $query->bindParam(':lineupId', $lineupId);
           
         
           $data = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
           return $data; 
       }
       
      
      
        
       
}

