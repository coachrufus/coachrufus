<?php
namespace Webteamer\Player\Model;
use Core\Repository as Repository;




class PlayerRepository extends Repository
{
   
    
    
    public function fulltextSearch($phrase,$section='all')
    {
        $list = $this->getStorage()->fulltextSearch($phrase,$section);
        
        return $this->createObjectList($list);
    }
    public function fulltextEmailSearch($phrase)
    {
        $list = $this->getStorage()->fulltextEmailSearch($phrase);
        
        return $this->createObjectList($list);
    }
    
     public function findPlayersByArea($center, $distance)
    {
        $list =  $this->getStorage()->findPlayersByArea($center, $distance);
        return $this->createObjectList($list);
    }
    /**
     * Update first name, surname in player teams 
     */
    public function updatePlayerTeamData($player,$data)
    {
         $this->getStorage()->updatePlayerTeamData($player->getId(),$data);
    }
    
   
    
    
}
