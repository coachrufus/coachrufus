<?php
namespace Webteamer\Player\Model;

use Core\DbQuery;
use Core\DbStorage;
use PDO;




class PlayerScoreStorage extends DbStorage
{
       public function getPlayerAverageRating($playerId)
       {
            $data = \Core\DbQuery::prepare('
               select avg(score) as avg from player_score where player_id = :id')
		->bindParam('id', $playerId)
		->execute()
		->fetchOne(\PDO::FETCH_ASSOC);
		return $data;
       }
       
       public function getPlayerTemAverageRating($playerId,$teamId)
       {
            $data = \Core\DbQuery::prepare('
               select avg(score) as avg from player_score where player_id = :id and team_id = :team_id')
		->bindParam('id', $playerId)
		->bindParam('team_id', $teamId)
		->execute()
		->fetchOne(\PDO::FETCH_ASSOC);
		return $data;
       }
}

