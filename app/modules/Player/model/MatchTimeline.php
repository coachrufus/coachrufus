<?php
namespace Webteamer\Player\Model;

class MatchTimeline extends \Core\Collections\Queue 
{
   public function isClosed()
   {
       
       foreach($this->getItems() as $item)
       {
           if($item->getHitType() == 'mom')
           {
               return true;
           }
       }
       
       return false;
   }
   
   public function isEmpty()
   {
       if(0  == count($this->getItems()))
       {
           return true;
       }
       return false;
   }
}
