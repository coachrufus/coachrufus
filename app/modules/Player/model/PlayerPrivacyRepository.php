<?php
namespace Webteamer\Player\Model;
use Core\Repository as Repository;



class PlayerPrivacyRepository extends Repository
{
     public function clearPlayerPrivacy($player)
    {
        $this->getStorage()->deleteByParams(array('player_id' => $player->getId())); 
    }   
    
    public function savePlayerPrivacy($player,$privacy)
    {
       
        foreach($privacy as $priv)
        {
            $priv->setPlayerId($player->getId());
            $this->save($priv);
        }
    }
}
