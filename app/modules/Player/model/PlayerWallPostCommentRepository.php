<?php
namespace CR\Player\Model;
use Core\Repository as Repository;




class PlayerWallPostCommentRepository extends Repository
{
    public function findPostComments($post)
    {
        $data = $this->getStorage()->getPostComments($post->getId());
        $list = array();
        foreach ($data as $d)
        {
            $object = $this->factory->createEntityFromArray($d);
            
            $player = new \Webteamer\Player\Model\Player();
            $player->setName($d['name']);
            $player->setSurname($d['surname']);
            $player->setPhoto($d['photo']);
            $object->setAuthor($player);
            
            $list[] = $object;
        }
        return $list;
    }
}
