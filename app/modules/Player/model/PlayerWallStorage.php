<?php
namespace CR\Player\Model;

use Core\DbQuery;
use Core\DbStorage;
use PDO;




class PlayerWallStorage extends DbStorage
{
       public function getPlayerWallPosts($playerId,$limit = 20)
       {
            $data = \Core\DbQuery::prepare('
                SELECT p.*, u.name, u.surname, u.photo, count(c.id) as comments_count
                FROM '.$this->getTableName().' p
                LEFT JOIN co_users u ON p.author_id = u.id
                LEFT JOIN  player_wall_post_comment c on p.id = c.post_id 
                    
                WHERE p.author_id = :id
                group by p.id
                ORDER  by p.id desc
                LIMIT 0,'.$limit)
                 ->bindParam(':id', $playerId)
		->execute()
		->fetchAll(\PDO::FETCH_ASSOC);
		return $data;
       }
       
       public function getPlayerFollowingPosts($playerId,$limit = 2)
       {
           
            $data = \Core\DbQuery::prepare('
               SELECT   p.*, 
                        u.name, 
                        u.surname, 
                        u.photo,
                        count(c.id) as comments_count
                 FROM  player_follow pf 
                        LEFT JOIN player_wall_post p 
                               ON pf.player_id = p.author_id 
                        LEFT JOIN co_users u 
                               ON p.author_id = u.id 
                         LEFT JOIN  player_wall_post_comment c on p.id = c.post_id 
                 WHERE  pf.follower_id = :id
                 AND p.id is not null
                 group by p.id LIMIT 0,'.$limit)
                 ->bindParam(':id', $playerId)
		->execute()
		->fetchAll(\PDO::FETCH_ASSOC);
		return $data;
       }
               
              
        
        
       
}

