<?php
namespace  Webteamer\Locality;
require_once(MODUL_DIR.'/Locality/LocalityModule.php');

use Core\EntityMapper;

use Core\DbStorage;
use Core\ServiceLayer as ServiceLayer;
use Webteamer\Locality\Model\CountryStorage;
use Webteamer\Locality\Model\LocalityStorage;
use Webteamer\Locality\Model\PlaygroundStorage;
use Webteamer\Locality\Model\ScheduleEventStorage;


ServiceLayer::addService('CountryRepository',array(
'class' => "Webteamer\Locality\Model\CountryRepository",
	'params' => array(
		new CountryStorage('country'),
		new EntityMapper('Webteamer\Locality\Model\Country')
	)
));

ServiceLayer::addService('LocalityRepository',array(
'class' => "Webteamer\Locality\Model\LocalityRepository",
	'params' => array(
		new LocalityStorage('locality'),
		new EntityMapper('Webteamer\Locality\Model\Locality')
	)
));

ServiceLayer::addService('PlaygroundAliasRepository',array(
'class' => "Webteamer\Locality\Model\PlaygroundAliasRepository",
	'params' => array(
		new DbStorage('playground_alias'),
		new EntityMapper('Webteamer\Locality\Model\PlaygroundAlias')
	)
));

$playgroundMapper =  new EntityMapper('Webteamer\Locality\Model\Playground');
$playgroundMapper->addAssocEntity(array(
    'class' => 'Webteamer\Locality\Model\Locality' , 
    'method' => 'setLocality',
    'mapper' => '\Core\EntityMapper'));

$playgroundStorage = new PlaygroundStorage('playground');
$playgroundStorage->addAssocTable('a_','locality');

ServiceLayer::addService('PlaygroundRepository',array(
'class' => "Webteamer\Locality\Model\PlaygroundRepository",
	'params' => array(
		$playgroundStorage,
		$playgroundMapper
	)
));



$eventMapper =  new EntityMapper('Webteamer\Locality\Model\ScheduleEvent');
$eventMapper->addAssocEntity(array('class' => 'Webteamer\Team\Model\Team' , 'method' => 'addTeam','mapper' => '\Core\EntityMapper'));
ServiceLayer::addService('ScheduleEventRepository',array(
'class' => "Webteamer\Locality\Model\ScheduleEventRepository",
	'params' => array(
		new ScheduleEventStorage('schedule_event'),
                $eventMapper)
	)
);

ServiceLayer::addService('LocalityManager',array(
'class' => "Webteamer\Locality\Model\LocalityManager",
    'params' => array(
		ServiceLayer::getService('LocalityRepository')
	)
));

ServiceLayer::addService('PlaygroundManager',array(
'class' => "Webteamer\Locality\Model\PlaygroundManager",
    'params' => array(
		ServiceLayer::getService('PlaygroundRepository'),
                ServiceLayer::getService('LocalityManager'),
	)
));

ServiceLayer::addService('ScheduleEventManager',array(
'class' => "Webteamer\Locality\Model\ScheduleEventManager",
    'params' => array(
		ServiceLayer::getService('ScheduleEventRepository'),
	)
));



$acl = ServiceLayer::getService('acl');


//routing
$router = ServiceLayer::getService('router');

$acl->allowRole('ROLE_USER','locality_list');
$router->addRoute('locality_list','/ajax/locality',array(
		'controller' => 'Webteamer\Locality\LocalityModule:Ajax:locality'
));


$acl->allowRole('ROLE_USER','playground_create');
$router->addRoute('playground_create','/playground/create',array(
		'controller' => 'Webteamer\Locality\LocalityModule:Web:create'
));

$acl->allowRole('ROLE_USER','playground_list');
$router->addRoute('playground_list','/ajax/locality/playgrounds',array(
		'controller' => 'Webteamer\Locality\LocalityModule:Ajax:playgrounds'
));

$acl->allowRole('ROLE_USER','playground_area_list');
$router->addRoute('playground_area_list','/ajax/locality/area',array(
		'controller' => 'Webteamer\Locality\LocalityModule:Ajax:area'
));


$acl->allowRole('ROLE_USER','playgrounds_overview_all');
$router->addRoute('playgrounds_overview_all','/playgrounds/overview-all',array(
		'controller' => 'Webteamer\Locality\LocalityModule:Owner:playgroundsOverviewAll'
));



$acl->allowRole('ROLE_USER','locality_modul_info');
$router->addRoute('locality_modul_info','/locality/info',array(
		'controller' => 'Webteamer\Locality\LocalityModule:Default:info'
));


$acl->allowRole('ROLE_USER','locality_create');
$router->addRoute('locality_create','/locality/create',array(
		'controller' => 'Webteamer\Locality\LocalityModule:Web:create'
));

$acl->allowRole('ROLE_USER','locality_overview_all');
$router->addRoute('locality_overview_all','/locality/overview-all',array(
		'controller' => 'Webteamer\Locality\LocalityModule:Owner:overviewAll'
));

$acl->allowRole('ROLE_USER','playground_calendar');
$router->addRoute('playground_calendar','/playground/calendar',array(
		'controller' => 'Webteamer\Locality\LocalityModule:Owner:calendar'
));

$acl->allowRole('ROLE_USER','playground_calendar_add_event');
$router->addRoute('playground_calendar_add_event','/playground/calendar/add-event',array(
		'controller' => 'Webteamer\Locality\LocalityModule:Owner:calendarAddEvent'
));


$acl->allowRole('ROLE_USER','playground_calendar_remove_event');
$router->addRoute('playground_calendar_remove_event','/playground/calendar/remove-event',array(
		'controller' => 'Webteamer\Locality\LocalityModule:Owner:calendarRemoveEvent'
));


$acl->allowRole('guest','locality_overview');
$router->addRoute('locality_overview','/locality/overview',array(
		'controller' => 'Webteamer\Locality\LocalityModule:Public:index'
));

$acl->allowRole('ROLE_USER','playground_ajax_global');
$router->addRoute('playground_ajax_global','/playground/ajax/global',array(
		'controller' => 'Webteamer\Locality\LocalityModule:Ajax:globalPlaygrounds'
));

$acl->allowRole('ROLE_USER','city_ajax_search');
$router->addRoute('city_ajax_search','/city/ajax/search',array(
		'controller' => 'Webteamer\Locality\LocalityModule:Ajax:globalCitySearch'
));


$acl->allowRole('ROLE_USER','playground_import');
$router->addRoute('playground_import','/playground/import',array(
		'controller' => 'Webteamer\Locality\LocalityModule:Default:import'
));






