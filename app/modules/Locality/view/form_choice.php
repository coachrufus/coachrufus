
            <ul id="locality_choice">
                
            </ul>
            <input id="pac-input" class="controls" type="text" placeholder="Search Box">
            <div id="map"></div>
            <script>
// This example adds a search box to a map, using the Google Place Autocomplete
// feature. People can enter geographical searches. The search box will return a
// pick list containing a mix of places and predicted search terms.

function initAutocomplete() {
  var map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: -33.8688, lng: 151.2195},
    zoom: 13,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });

  // Create the search box and link it to the UI element.
  var input = document.getElementById('pac-input');
  var searchBox = new google.maps.places.SearchBox(input);
  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

  // Bias the SearchBox results towards current map's viewport.
  map.addListener('bounds_changed', function() {
    searchBox.setBounds(map.getBounds());
  });
  
  
  var markers = [];
  // [START region_getplaces]
  // Listen for the event fired when the user selects a prediction and retrieve
  // more details for that place.
  searchBox.addListener('places_changed', function() {
    var places = searchBox.getPlaces();

    if (places.length == 0) {
      return;
    }

    // Clear out the old markers.
    markers.forEach(function(marker) {
      marker.setMap(null);
    });
    markers = [];

    // For each place, get the icon, name and location.
    var bounds = new google.maps.LatLngBounds();
    places.forEach(function(place) {
      var icon = {
        url: place.icon,
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(25, 25)
      };

      // Create a marker for each place.
      markers.push(new google.maps.Marker({
        map: map,
        icon: icon,
        title: place.name,
        position: place.geometry.location
      }));

      if (place.geometry.viewport) {
        // Only geocodes have viewport.
        bounds.union(place.geometry.viewport);
      } else {
        bounds.extend(place.geometry.location);
      }
      
      console.log(place.geometry.location.lat());
       var new_locality_alias = '<li><input type="hidden" name="<?php echo $form->getName() ?>[locality_alias]['+place.place_id+'][name]" value="'+place.name+','+place.formatted_address+'" /><input type="hidden" name="<?php echo $form->getName() ?>[locality_alias]['+place.place_id+'][lat]" value="'+place.geometry.location.lat()+'" /><input type="hidden" name="<?php echo $form->getName() ?>[locality_alias]['+place.place_id+'][lng]" value="'+place.geometry.location.lng()+'" />'+place.name+','+place.formatted_address+'</li>';
       $('#locality_choice').append(new_locality_alias);
       
        //val( place.name+','+place.formatted_address);
      
    });
    
    
    google.maps.event.addListener(map, 'click', function(event) {
        placeMarker(event.latLng);
        //console.log(event.latLng.latitude);
        //$('#record_lat').val(event.latLng.lat());
        //$('#record_lng').val(event.latLng.lng());
     });

     function placeMarker(location) 
     {
          markers.forEach(function(marker) {
            marker.setMap(null);
        });
        
        var marker = new google.maps.Marker({
             position: location, 
             map: map
         });
         
          markers.push(marker);
     }
    
    
    
    
    map.fitBounds(bounds);
  });
  // [END region_getplaces]
}


    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXU1duK54f1gIsRy_xc7z4uQURovsZdE8&libraries=places&callback=initAutocomplete"
         async defer></script>
