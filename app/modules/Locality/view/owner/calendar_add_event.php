


    <form action="" class="form-horizontal form-bordered" method="post" id="add_playground_event_form">

        <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo $translator->translate('Name') ?></label>
            <div class="col-sm-6">
                <?php echo $form->renderInputTag("name", array('class' => 'form-control')) ?>
                 <?php echo $form->renderInputHiddenTag("termin", array('class' => 'form-control')) ?>

                <?php echo $form->renderInputHiddenTag("playground_id", array('class' => 'form-control')) ?>
            </div>
        </div>
        
        


        <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo $translator->translate('team') ?></label>
            <div class="col-sm-6">
               <?php echo $form->renderSelectTag("teams", array('class' => 'form-control')) ?>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo $translator->translate('sport_id') ?></label>
            <div class="col-sm-6">
               <?php echo $form->renderSelectTag("sport_id", array('class' => 'form-control')) ?>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo $translator->translate('note') ?></label>
            <div class="col-sm-6">
               <?php echo $form->renderTextareaTag("note", array('class' => 'form-control')) ?>
            </div>
        </div>
        
        <button id="playground_event_send_trigger" type="submit" class="btn btn-primary">Odoslať</button>

    </form>





