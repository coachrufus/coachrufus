<?php Core\Layout::getInstance()->startSlot('crumb') ?>
    <li><?php echo $translator->translate('Lokality') ?></li>
<?php Core\Layout::getInstance()->endSlot('crumb') ?>
    
    
<div class="row">
    
  
    <div class="col-md-4">
        <div class="well">
        <h2><?php echo $translator->translate('My playgrounds') ?></h2>
        
        
        <table class="table">
            <thead>
                <tr>
                    <td><?php echo $translator->translate('Playground name') ?></td>
                    
                </tr>
            </thead>
            <tbody>
                <?php foreach($playgrounds as $playground): ?>
                <tr>
                    <td><a  href="<?php echo $router->link('playground_calendar',array('id' => $playground->getId())) ?>" ><?php echo $playground->getName() ?></a></td>
                    
                </tr>
                <?php endforeach; ?>
            </tbody>
            
        </table>
        
        
        <a class="btn btn-primary" href="<?php echo  $router->link('playground_create') ?>"><?php echo $translator->translate('Create playground') ?></a>
        </div>
    </div>
    <div class="col-md-8">
        <div class="well">
        <h2>Najbližšie udalosti</h2>
                    <table class="table">
                        <thead>
                            <tr >
                                <th><?php echo $translator->translate('Termin') ?></th>
                                <th><?php echo $translator->translate('Team') ?></th>
                                <th><?php echo $translator->translate('Attendance') ?></th>
                                <th><?php echo $translator->translate('Type') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($events as $event): ?>
                                <tr>
                                    <td><?php echo $event->getStart()->format('d.m.Y H:i:s') ?></td>
                                    
                                    <td><a href="<?php echo $router->link('team_overview',array('id' => $event->getTeamId())) ?>"><?php echo $event->getTeamName() ?></a></td>
                                    <td><?php echo $event->getLocalityCapacity() ?> / <?php  echo $event->getParticipantCount() ?></td>
                                    <td><?php echo $event->getEventType() ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                        
                    </table>
    </div>
    </div>
</div>



