<?php Core\Layout::getInstance()->startSlot('crumb') ?>
    <li><?php echo $translator->translate('Lokality') ?></li>
<?php Core\Layout::getInstance()->endSlot('crumb') ?>
    
    
<div class="row">
    
  
    <div class="col-md-4">
        <h2><?php echo $translator->translate('My playgrounds') ?></h2>
        
        
        <table class="table">
            <thead>
                <tr>
                    <td><?php echo $translator->translate('Locality name') ?></td>
                    
                </tr>
            </thead>
            <tbody>
                <?php foreach($locations as $locality): ?>
                <tr>
                    <td><a  href="<?php echo $router->link('locality_calendar',array('id' => $locality->getId())) ?>" ><?php echo $locality->getName() ?></a></td>
                    
                </tr>
                <?php endforeach; ?>
            </tbody>
            
        </table>
        
        
        <a class="btn btn-primary" href="<?php echo  $router->link('locality_create') ?>">Create locality</a>
    </div>
    <div class="col-md-8">
        <h2>Najbližšie udalosti</h2>
                    <table class="table">
                        <thead>
                            <tr >
                                <th>datum, cas, tím</th>
                                <th>dochadzka = miesta/pocet prihlasenych</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($events as $event): ?>
                                <tr>
                                    <td><?php echo $event->getStart()->format('d.m.Y H:i:s') ?>, <a href="<?php echo $router->link('team_overview',array('id' => $event->getTeamId())) ?>"><?php echo $event->getTeamName() ?></a></td>
                                    <td><?php echo $event->getLocalityCapacity() ?> / <?php  echo $event->getParticipantCount() ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                        
                    </table>
    </div>
</div>



