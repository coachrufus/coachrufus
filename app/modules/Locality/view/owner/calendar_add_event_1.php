<?php Core\Layout::getInstance()->startSlot('crumb') ?>
<li><a href="<?php echo $router->link('locality_overview_all') ?>"><?php echo $translator->translate('Lokality') ?></a></li>
<li class="active"><?php echo $playground->getName() ?></li>
<?php Core\Layout::getInstance()->endSlot('crumb') ?>

<div class="row">


    <div class="col-md-12">
        <h2><?php echo $playground->getName() ?></h2>
    </div>


    <form action="" class="form-horizontal form-bordered" method="post">

        <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo $translator->translate($form->getFieldLabel("name")) ?></label>
            <div class="col-sm-6">
                <?php echo $form->renderInputTag("name", array('class' => 'form-control')) ?>
                <?php echo $validator->showError("name") ?>
            </div>
        </div>
        
         <?php echo $form->renderInputTag("termin", array('class' => 'form-control')) ?>
         <?php echo $form->renderInputTag("playground_id", array('class' => 'form-control')) ?>


        <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo $translator->translate($form->getFieldLabel("teams")) ?></label>
            <div class="col-sm-6">
                <?php echo $form->renderSelectTag("teams", array('class' => 'form-control')) ?>
                <?php echo $validator->showError("teams") ?>
            </div>
        </div>
        
        <button type="submit" class="btn btn-primary">Odoslať</button>

    </form>

</div>



