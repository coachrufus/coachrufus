<?php Core\Layout::getInstance()->startSlot('crumb') ?>
<li><a href="<?php echo $router->link('playgrounds_overview_all') ?>"><?php echo $translator->translate('Playground') ?></a></li>
<li class="active"><?php echo $playground->getName() ?></li>
<?php Core\Layout::getInstance()->endSlot('crumb') ?>

<div class="row">


    <div class="col-md-12">
        <h2><?php echo $playground->getName() ?></h2>
    </div>
    <table class="table">
        <thead>
            <tr>
                <th></th>
                <?php foreach ($days as $day): ?>
                    <th><?php echo $day->format('d.m.Y') ?></th>
                <?php endforeach; ?>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($schedule->getHours() as $hour): ?>
                <tr>
                    <td><?php echo $hour ?></td>
                    <?php foreach ($days as $day): ?>
                    <?php  $termin_events = $schedule->getEvents($day, $hour) ?>
                        <td>
                            <?php foreach ($termin_events as $schedule_event): ?>
                            
                            <a class="playground_calendar_remove_event" href="<?php echo $router->link('playground_calendar_remove_event',array('id' => $schedule_event->getId())) ?>"><span class="glyphicon glyphicon-trash pull-right" aria-hidden="true"></span></a>
                            
                            
                            <strong><?php echo $schedule_event->getName() ?></strong><br /> <?php echo implode(',',$schedule_event->getTeamNames()) ?>
                            

                           <span class="label label-default"><?php echo $schedule_event->getNote() ?></span>
                            

                            
                           
                            
                            <?php endforeach; ?>

                            <?php if (empty($termin_events)): ?>
                                <a class="playground_calendar_add_event" data-termin="<?php echo $day->format('Y-m-d').' '. $hour ?>" data-playground="<?php echo  $playground->getId()  ?>" href="<?php echo $router->link('playground_calendar_add_event', array('termin' => $day->format('Y-m-d') . ' ' . $hour, 'p' => $playground->getId())) ?>"><?php echo $translator->translate('Pridať') ?></a>
                            <?php endif; ?>



                        </td>
                    <?php endforeach; ?>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    
    <div id="calendar_dialog" title="Basic dialog">
        <?php $layout->includePart(MODUL_DIR.'/Locality/view/owner/calendar_add_event.php',array('form' => $form)) ?>
    </div>

</div>



