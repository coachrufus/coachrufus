<h2>
    <?php echo $locality->getName() ?>
</h2>

<div class="row">
<div class="col-md-4">
          <div class="panel panel-default">
            <div class="panel-heading"><strong><?php echo $translator->translate('Events') ?></strong></div>
            <div class="panel-body">
        <table class="table">
                        <thead>
                            <tr >
                                <th>datum, cas, tím</th>
                                <th>dochadzka = miesta/pocet prihlasenych</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($events as $event): ?>
                                <tr>
                                    <td><?php echo $event->getStart()->format('d.m.Y H:i:s') ?>, <a href="<?php echo $router->link('team_overview',array('id' => $event->getTeamId())) ?>"><?php echo $event->getTeamName() ?></a></td>
                                    <td><?php echo $event->getLocalityCapacity() ?> / <?php  echo $event->getParticipantCount() ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                        
                    </table>
            </div>
          </div>
    </div>
    
     <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading"><strong><?php echo $translator->translate('Teams') ?></strong></div>
            <div class="panel-body">
                <table class="table">
                    <thead>
                        <tr>
                            <td><?php echo $translator->translate('Team name') ?></td>

                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($teams as $team): ?>
                            <tr>
                                <td><a  href="<?php echo $router->link('team_overview', array('id' => $team->getId())) ?>" ><?php echo $team->getName() ?></a></td>

                            </tr>
                        <?php endforeach; ?>
                    </tbody>

                </table>


                
            </div>
        </div>
    </div>
    
     <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading"><strong><?php echo $translator->translate('Players') ?></strong></div>
            <div class="panel-body">
                <table class="table">
                    <thead>
                        <tr>
                            <td><?php echo $translator->translate('Player name') ?></td>

                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($players as $player): ?>
                            <tr>
                                <td><a href="<?php echo $router->link('player_public_profile',array('player_id' => $player->getId())) ?>"><?php echo $player->getFullName() ?></a></td>

                            </tr>
                        <?php endforeach; ?>
                    </tbody>

                </table>


               
            </div>
        </div>
    </div>
    
    
    
 </div>