<div class="row">
    <div class="col-sm-12">
        <h1><?php echo $localityName ?></h1>
        <div class="alert alert-info">
            <input id="pac-input" class="form-control" type="text" placeholder="<?php echo $translator->translate('City name, street, etc.') ?>">
        </div>
    </div>

    <div class="col-md-4">

        <div class="panel panel-default  hp-events">
            <div class="panel-heading">
                <i class="fa fa-calendar"></i> 
                <strong><?php echo $translator->translate('Events') ?></strong>
            </div>


            <div class="panel-body">

                <?php
                $i = 0;
                foreach ($eventsList as $dayEvents):
                    ?>
                    <?php foreach ($dayEvents as $event): ?>

                        <div class="row<?php echo (fmod($i++, 2) == 0) ? ' dark' : ' white' ?>">
                            <div class="col-xs-4 event-date">
                                <span class="month-accr"><?php echo substr($translator->translate($event->getCurrentDate()->format('F')), 0, 4) ?> </span>
                                <span class="day"><?php echo $translator->getRegionalDate('%d', $event->getCurrentDate()) ?></span>
                                <span class="dayname"><?php echo $translator->translate($event->getCurrentDate()->format('l')) ?></span>
                                <span class="time"><?php echo $event->getStart()->format('H:i'); ?> </span>
                            </div>
                            <div class="col-xs-8 event-info">
                                <strong><a href="<?php echo $router->link('team_public_page', array('id' => $event->getTeamId())) ?>"><?php echo $event->getTeamName() ?></a></strong>
                                <h3><?php echo $event->getName() ?></h3>
                                <?php echo $event->getPlaygroundName() ?>

                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php endforeach; ?>



            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title"><?php echo $translator->translate('Locality teams') ?></h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <?php foreach ($teams as $team): ?>
                    <div class="box box-widget widget-user">
                        <div class="widget-user-header bg-aqua-active">
                            <h3 class="widget-user-username"><?php echo $team->getName() ?></h3>
                            <h5 class="widget-user-desc"><?php echo $translator->translate($sportEnum[$team->getSportId()]) ?></h5>
                        </div>
                        <div class="widget-user-image img_round_wrap team_img_round">
                            <a  style="background-image: url('<?php echo $team->getMidPhoto() ?>')"  href="<?php echo $router->link('team_public_page', array('id' => $team->getId())) ?>" >


                            </a>
                        </div>
                        <div class="box-footer">
                            <div class="row">
                                <div class="col-sm-4 border-right">
                                    <div class="description-block">
                                        <h5 class="description-header"><?php echo $teamInfo[$team->getId()]['members_count'] ?></h5>
                                        <span class="description-text"><?php echo $translator->translate('Memebers') ?></span>
                                    </div><!-- /.description-block -->
                                </div><!-- /.col -->
                                <div class="col-sm-4 border-right">
                                    <div class="description-block">
                                        <h5 class="description-header"><?php echo $teamInfo[$team->getId()]['followers'] ?></h5>
                                        <span class="description-text"><?php echo $translator->translate('Followers') ?></span>
                                    </div><!-- /.description-block -->
                                </div><!-- /.col -->
                                <div class="col-sm-4">
                                    <div class="description-block">
                                        <h5 class="description-header"><?php echo $playerPermissiomEnum[$userTeamRoles[$team->getId()]['role']] ?></h5>
                                        <span class="description-text"></span>
                                    </div><!-- /.description-block -->
                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div>
                    </div>
                <?php endforeach; ?>

            </div><!-- /.box-body -->

        </div><!--/.box -->
    </div>

    <div class="col-md-4">
        <div class="box box-warning">
            <div class="box-header with-border">
                <h3 class="box-title"><?php echo $translator->translate('Locality players') ?></h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <ul class="users-list clearfix">
                    <?php foreach ($players as $player): ?>

                        <li>
                            <div class="img_round_wrap member_img_round_100" style="background-image: url('<?php echo $player->getMidPhoto() ?>');">&nbsp;</div>
                            <a class="users-list-name" href="<?php echo $router->link('player_public_profile', array('player_id' => $player->getId())) ?>"><?php echo $player->getFullName() ?> </a>



                        </li>
                    <?php endforeach; ?>



                </ul><!-- /.users-list -->

            </div><!-- /.box-body -->

        </div><!--/.box -->

        <div class="box box-warning">
            <div class="box-header with-border">
                <h3 class="box-title"><?php echo $translator->translate('Locality scounting') ?></h3>
            </div><!-- /.box-header -->
            <div class="box-body">

                <div>
                    <?php foreach ($scoutingItems as $item): ?>
                        <strong><?php echo $item['scouting']->getCreatedAt()->format('d.m.Y') ?></strong>
                        <p class='text-muted'>
                            <?php echo $item['content'] ?>
                        </p>
                        
                       <?php if(\Core\ServiceLayer::getService('security')->getIdentity()->getUser() == null): ?>
                             <a  href="<?php echo $router->link('login') ?>"><?php echo $translator->translate('Kontaktovať') ?></a>
                        <?php else: ?>
                            <a data-scouting-id="<?php echo $item['scouting']->getId() ?>" class="contact-scouting-modal-trigger" href=""><?php echo $translator->translate('Kontaktovať') ?></a>
                        <?php endif; ?>
                        
                        
                       
                        <hr />
                    <?php endforeach; ?>
                </div>

            </div><!-- /.box-body -->

        </div><!--/.box -->








    </div>

</div>

<?php $layout->includePart(MODUL_DIR . '/Scouting/view/default/_contact_modal.php') ?>


<?php $layout->addJavascript('js/public/LocalityManager.js') ?>

<?php $layout->startSlot('javascript') ?>
<script type="text/javascript">
    function initLocalityManager()
    {
        locality_manger = new LocalityManager();
        locality_manger.searchCallback = function (latLng) {
            location.href = '/locality/overview?lat=' + latLng.lat() + '&lng=' + latLng.lng() + '&name=' + $('#pac-input').val();
        };

        locality_manger.google = google;
        locality_manger.createSearchInput();
    }
    
    
     $('.contact-scouting-modal-trigger').on('click',function(e){
        e.preventDefault();
       
       $('#contact_scouting_form_id').val($(this).attr('data-scouting-id'));
        $('#contact-scouting-modal').modal('show');
       
   });
    
    
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXU1duK54f1gIsRy_xc7z4uQURovsZdE8&libraries=places&callback=initLocalityManager" async defer></script>
<?php $layout->endSlot('javascript') ?>
