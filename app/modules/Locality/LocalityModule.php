<?php
namespace  Webteamer\Locality;
use Core\Module as Module;
class LocalityModule extends Module
{
    public function __construct() 
    {
        $this->setBaseDir(MODUL_DIR.'/Locality');
        $this->setName('Locality');
    }
}

