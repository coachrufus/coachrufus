<?php

namespace Webteamer\Locality\Controller;

use Core\Controller;
use Core\ControllerResponse;
use Core\ServiceLayer;
use Core\Validator;
use DateTime;
use Webteamer\Locality\Form\CreatePlaygroundEventForm;
use Webteamer\Locality\Model\PlaygroundSchedule;
use Webteamer\Locality\Model\ScheduleEvent;

/**
 * @author Marek Hubáček
 * @version 1.0
 * @created 21-10-2015 14:37:26
 */
class OwnerController extends Controller {

    public function playgroundsOverviewAllAction()
    {

        $manager = ServiceLayer::getService('PlaygroundManager');
        $eventManager = ServiceLayer::getService('EventManager');
        $security = ServiceLayer::getService('security');
        $user = $security->getIdentity()->getUser();
        $playgrounds = $manager->getUserPlaygrounds($user);
        $events = $eventManager->getPlaygroundsEvents($playgrounds);

        return $this->render('Webteamer\Locality\LocalityModule:owner:playgrounds_overview_all.php', array(
                    'playgrounds' => $playgrounds,
                    'events' => $events
        ));
    }

    public function overviewAllAction()
    {

        $localityManager = ServiceLayer::getService('LocalityManager');
        $security = ServiceLayer::getService('security');
        $user = $security->getIdentity()->getUser();
        $locations = $localityManager->getUserLocations($user);
        $events = $localityManager->getNearestEvent($locations);

        return $this->render('Webteamer\Locality\LocalityModule:owner:overview_all.php', array(
                    'locations' => $locations,
                    'events' => $events
        ));
    }

    public function calendarAction()
    {
        $request = ServiceLayer::getService('request');
        $manager = ServiceLayer::getService('PlaygroundManager');
        $teamManager = ServiceLayer::getService('TeamManager');
        $sportManager = ServiceLayer::getService('SportManager');
        $playground = $manager->findPlaygroundById($request->get('id'));



        $schedule = new PlaygroundSchedule();
        $schedule->setPlayground($playground);
        $today = new DateTime();
        $days = $schedule->getWeekDaysByDate($today);


        $scheduleEvent = new ScheduleEvent();
        $scheduleEvent->setPlaygroundId($playground->getId());
        $playgroundTeams = $teamManager->getTeamsByPlayground($playground);
        $teamChoices = array();
        foreach ($playgroundTeams as $playgroundTeam)
        {
            $teamChoices[$playgroundTeam->getId()] = $playgroundTeam->getName();
        }

        
        
        $form = new CreatePlaygroundEventForm();
        
        $form->setTeamChoices($teamChoices);
         $form->buildFields();
        $form->setFieldChoices('sport_id', $sportManager->getSportFormChoices());
       
        $form->setEntity($scheduleEvent);






        return $this->render('Webteamer\Locality\LocalityModule:owner:calendar.php', array(
                    'playground' => $playground,
                    'days' => $days,
                    'schedule' => $schedule,
                    'form' => $form
        ));
    }

    public function calendarAddEventAction()
    {
         $layout = ServiceLayer::getService('layout');
        $layout->setTemplate(null);
        
        $request = ServiceLayer::getService('request');
        $router = ServiceLayer::getService('router');
 $sportManager = ServiceLayer::getService('SportManager');
        $manager = ServiceLayer::getService('PlaygroundManager');
        $teamManager = ServiceLayer::getService('TeamManager');
        $repo = ServiceLayer::getService('ScheduleEventRepository');
        $playground = $manager->findPlaygroundById($request->get('p'));

        /*
          $playgroundTeams = $teamManager->getTeamsByPlayground($playground);
          $team_choices = array();
          foreach ($playgroundTeams as $playgroundTeam)
          {
          $team_choices[$playgroundTeam->getId()] = $playgroundTeam->getName();
          }
         */

        $scheduleEvent = new ScheduleEvent();
        //$scheduleEvent->setPlaygroundId($playgroundTeam->getId());
        //$scheduleEvent->setTermin(new \DateTime($request->get('termin').':00'));

        $form = new CreatePlaygroundEventForm();
        
         $form->buildFields();
        $form->setFieldChoices('sport_id', $sportManager->getSportFormChoices());
        $form->setEntity($scheduleEvent);


        $validator = new Validator();
        $validator->setRules($form->getFields());

        $post_data = $request->get($form->getName());
        $form->bindData($post_data);
        $validator->setData($post_data);
        $validator->validateData();
        $response = new ControllerResponse();
        if (!$validator->hasErrors())
        {
            $entity = $form->getEntity();

            $id = $repo->save($entity);
            $response->setStatus('success');

            $event = $repo->find($id);

            $returnData = $repo->convertToArray($event);
            
            $returnData['teams'] = $event->getTeamNames();
            

            $response->setContent(json_encode($returnData));
        }
        else
        {
            $response->setStatus('error');
        }


        $response->setType('json');
        return $response;
    }
    
     public function calendarRemoveEventAction()
    {
        $layout = ServiceLayer::getService('layout');
        $layout->setTemplate(null);
        
        $manager = ServiceLayer::getService('ScheduleEventManager');
        $manager->removeEventById($this->getRequest()->get('id'));
        
        $this->getRequest()->redirect();
    }

}

?>