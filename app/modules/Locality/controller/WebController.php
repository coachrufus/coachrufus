<?php

namespace Webteamer\Locality\Controller;

use Core\Controller;
use Core\ServiceLayer;
use Core\Validator;
use DateTime;
use Webteamer\Locality\Form\PlaygroundForm;
use Webteamer\Locality\Model\Playground;

/**
 * @author Marek Hubáček
 * @version 1.0
 * @created 21-10-2015 14:37:26
 */
class WebController extends Controller {

    private $handler;

    private function getCreatePlaygroundForm()
    {
        $form = new PlaygroundForm();
        $entity = new Playground();
        $form->setEntity($entity);
        $manager = ServiceLayer::getService('PlaygroundManager');
        $sportManager = ServiceLayer::getService('SportManager');

        $form->setFieldChoices('sport', $sportManager->getSportFormChoices());
        $form->setFieldChoices('status', $manager->getStatusEnum());


        return $form;
    }

    public function createAction()
    {
        $request = ServiceLayer::getService('request');
        $translator = ServiceLayer::getService('translator');
        $security = ServiceLayer::getService('security');
        $manager = ServiceLayer::getService('PlaygroundManager');

        $form = $this->getCreatePlaygroundForm();
        $playgroundForm = new PlaygroundForm();
        $playgroundValidator = new Validator();


        $validator = new Validator();
        $validator->setRules($form->getFields());


        if ('POST' == $request->getMethod())
        {
            $post_data = $request->get($form->getName());
            $form->bindData($post_data);
            $validator->setData($post_data);
            $validator->validateData();

            if (!$validator->hasErrors())
            {
                $entity = $form->getEntity();
                $entity->setCreatedAt(new DateTime());
                $entity->setAuthorId($security->getIdentity()->getUser()->getId());
                $manager->savePlaygroundAlias($entity);
                $request->addFlashMessage('locality_create_create_success', $translator->translate('CREATE_ENTITY_SUCCESS'));
                $request->redirect($this->getRouter()->link('playgrounds_overview_all'));
            }
        }


        return $this->render('Webteamer\Locality\LocalityModule:Crud:create.php', array(
                    'form' => $form,
                    'playgroundForm' => $playgroundForm,
                    'request' => $request,
                    'validator' => $validator,
                    'playgroundValidator' => $playgroundValidator,
        ));
    }

    public function deleteAction()
    {
        
    }

    public function getHandler()
    {
        return $this->handler;
    }

    /**
     * 
     * @param newVal
     */
    public function setHandler($newVal)
    {
        $this->handler = $newVal;
    }

    public function updateAction()
    {
        
    }

    public function exploreLocalityAction()
    {
        $request = ServiceLayer::getService('request');
        $localityManager = ServiceLayer::getService('LocalityManager');
        $teamManager = ServiceLayer::getService('TeamManager');
        $playerManager = ServiceLayer::getService('PlayerManager');
        $manager = ServiceLayer::getService('EventManager');

        $locality = $localityManager->getLocalityById($request->get('locality_id'));
        $events = $manager->getLocalityEvents($locality);
        $teams = $teamManager->getLocalityTeams($locality);
        $players = $playerManager->findPlayersByLocality($locality);

        return $this->render('Webteamer\Locality\LocalityModule:Web:explore.php', array(
                    'locality' => $locality,
                    'events' => $events,
                    'teams' => $teams,
                    'players' => $players
        ));
    }

}

?>