<?php

namespace Webteamer\Locality\Controller;

use Core\Controller;
use Core\ControllerResponse;
use Core\ServiceLayer;

/**
 * @author Marek Hubáček
 * @version 1.0
 * @created 21-10-2015 14:37:26
 */
class AjaxController extends Controller {

    public function globalCitySearchAction()
    {
        $layout = ServiceLayer::getService('layout');
        $layout->setTemplate(null);
        
        
        $request = ServiceLayer::getService('request');
        $LocalityManager = ServiceLayer::getService('LocalityManager');

        $cityName = $request->get('query');
        $citys = $LocalityManager->findCityByName($cityName);
        $items = array();
        foreach($citys as $city)
        {
            $items[] = array('value'=> $city['fullname'],'data' => $city['id'] ) ;
        }
        
       
        $result = array('suggestions' => $items);
        
        $response = new ControllerResponse();
        $response->setStatus('success');
        $response->setContent(json_encode($result));
        $response->setType('json');
        return $response;
    }
    
    public function localityAction()
    {
        $layout = ServiceLayer::getService('layout');
        $layout->setTemplate(null);


        $request = ServiceLayer::getService('request');
        $LocalityManager = ServiceLayer::getService('LocalityManager');

        $localityId = $request->get('locality_id');
        $locality = $LocalityManager->findLocalityByGoogleId($localityId);

        $localityData = array();
        if(null != $locality)
        {
            $localityData = $LocalityManager->convertEntityToArray($locality);
        }
        
        $response = new ControllerResponse();
        $response->setStatus('success');
        $response->setContent(json_encode($localityData));
        $response->setType('json');
        return $response;
    }
    
    public function playgroundsAction()
    {
        $layout = ServiceLayer::getService('layout');
        $layout->setTemplate(null);


        $request = ServiceLayer::getService('request');
        $playgroundManager = ServiceLayer::getService('PlaygroundManager');
        $LocalityManager = ServiceLayer::getService('LocalityManager');
        $locality = $LocalityManager->findLocalityByGoogleId( $request->get('locality_id'));


        $playgrounds = array();
        if(null != $locality)
        {
            $localityPlaygrounds = $playgroundManager->findPlaygroundsByLocality($locality);
            foreach($localityPlaygrounds as $playground)
            {
                $playgrounds[] = $playgroundManager->convertEntityToArray($playground);
            }

           
        }
        
        $response = new ControllerResponse();
        $response->setStatus('success');
        $response->setContent(json_encode($playgrounds));
        $response->setType('json');
        return $response;
    }
    
    public function areaAction()
    {
        $layout = ServiceLayer::getService('layout');
        $layout->setTemplate(null);
        
        
        $request = ServiceLayer::getService('request');
        $playgroundManager = ServiceLayer::getService('PlaygroundManager');
       // $LocalityManager = ServiceLayer::getService('LocalityManager');
       // $locality = $LocalityManager->findLocalityByGoogleId( $request->get('locality_id'));
        $center = array('lat' =>$request->get('lat') , 'lng' => $request->get('lng') );

        
        
        $distance = ($request->get('distance') == null) ? 1 : $request->get('distance');
         $playgrounds = array();
            $areaPlaygrounds = $playgroundManager->findPlaygroundsByArea($center,$distance);
            foreach($areaPlaygrounds as $playground)
            {
                $playgrounds[] = $playgroundManager->convertEntityToArray($playground);
            }
            
     
        $response = new ControllerResponse();
        $response->setStatus('success');
        $response->setContent(json_encode($playgrounds));
        $response->setType('json');
        return $response;
    }
    
    public function globalPlaygroundsAction()
    {
        $layout = ServiceLayer::getService('layout');
        $layout->setTemplate(null);
        $playgroundManager = ServiceLayer::getService('PlaygroundManager');
        $globalPlaygrounds = $playgroundManager->getGlobalPlaygrounds();
        foreach ($globalPlaygrounds as $playground)
        {
            $playgroundsChoices[$playground->getId()] = $playground->getName();
        }
        $result['items'] = $playgroundsChoices;
        
        $response = new ControllerResponse();
        $response->setStatus('success');
        $response->setContent(json_encode($result));
        $response->setType('json');
        return $response;
        
    }
    
   

}

?>