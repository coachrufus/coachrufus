<?php


namespace Webteamer\Locality\Controller;

use Webteamer\Locality\Handler;
/**
 * @author Marek Hubáček
 * @version 1.0
 * @created 21-10-2015 21:58:33
 */
class RestController
{

	private $handler;

	function __construct()
	{
	}

	function __destruct()
	{
	}



	public function createAction()
	{
	}

	public function deleteAction()
	{
	}

	public function getHandler()
	{
		return $this->handler;
	}

	public function listAction()
	{
	}

	/**
	 * 
	 * @param newVal
	 */
	public function setHandler($newVal)
	{
		$this->handler = $newVal;
	}

	public function updateAction()
	{
	}

}
?>