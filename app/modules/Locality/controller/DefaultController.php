<?php
namespace  Webteamer\Locality\Controller;

use Core\Controller as Controller;
use Core\ServiceLayer;
use Webteamer\Locality\Model\Playground;


class DefaultController extends Controller {
	
	public function infoAction()
	{
		
		
		return $this->render(' Webteamer\Locality\Locality::info.php',
				array(
						'foo' => 'foo'
				));
	}
        
        public function importAction()
        {
            $data = file(APPLICATION_PATH . '/export_sportovisk.csv');
            $manager = ServiceLayer::getService('PlaygroundManager');
            $localityManager = ServiceLayer::getService('LocalityManager');
            foreach ($data as $row)
            {
                $rowData = explode(';', trim($row));

                $name = $rowData[0];
                $address = explode(',',$rowData[1]); 
                $street = $address[0];
                $city = $address[1];
                $zip = $address[2];
                $country = $address[3];
                $sports = $rowData[2];
                
                
                if (null != $rowData[1])
                {
                    
                    $locality = new \Webteamer\Locality\Model\Locality();
                    $locality->setName($name);
                    $locality->setStreet($street);
                    $locality->setCity($city);
                    $locality_id = $localityManager->saveLocality($locality);
                    
                    
                    
                    
                    $playground = new Playground();
                    $playground->setName($name);
                    $playground->setStreet($street);
                    $playground->setCity($city);
                    $playground->setZip($zip);
                    $playground->setCountry($country);
                    $playground->setAuthorId(0);
                    $playground->setDescription($sports);
                    $playground->setLocalityId($locality_id);
                    $playgroundId = $manager->savePlayground($playground);
                    
                    $alias = new \Webteamer\Locality\Model\PlaygroundAlias();
                    $alias->setPlaygroundId($playgroundId);
                    $alias->setName($name);
                    $manager->getRepository()->saveAlias($alias);
                }
            }
            
            exit;
        }
	
	
	
	
}