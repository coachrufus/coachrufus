<?php

namespace Webteamer\Locality\Controller;

use Core\Controller as Controller;
use Core\Request as Request;
use Core\ServiceLayer;

class PublicController extends Controller {

    public function indexAction()
    {
        $layout = ServiceLayer::getService('layout');
        $layout->setTemplate(GLOBAL_DIR . '/templates/PublicLayout.php');
        $request = $this->getRequest();
        $playgrounds = array();
        $localityName = $this->getTranslator()->translate('Find locality'); 

        
        if (null != $request->get('lat') && null != $request->get('lng'))
        {
            $playgroundManager = ServiceLayer::getService('PlaygroundManager');
            $playerManager = ServiceLayer::getService('PlayerManager');
            $teamManager = ServiceLayer::getService('TeamManager');
            $teamEventManager = ServiceLayer::getService('TeamEventManager');
            $scoutingManager = ServiceLayer::getService('ScoutingManager');
            $center = array('lat' => $request->get('lat'), 'lng' => $request->get('lng'));
            $distance = ($request->get('distance') == null) ? 2 : $request->get('distance');
            $playgrounds = $playgroundManager->findPlaygroundsByArea($center, $distance);
            $teams = $teamManager->getTeamsByArea($center, $distance);
            $teamInfo = $teamManager->getTeamsInfo($teams);
            $players = $playerManager->findPlayersByArea($center, $distance);
            
            $listTo = new \DateTime();
            $listTo->setTimestamp(strtotime("+ 14 day"));
            $events = $teamEventManager->findEvents($teams, array('from' => new \DateTime(), 'to' => $listTo));
            $eventsList = $teamEventManager->buildTeamEventList($events, new \DateTime(date('Y-m-d')), $listTo);
            
            $scoutingList = $scoutingManager->findScoutingByArea($center, $distance);
            $scoutingItems = array();
            foreach($scoutingList as $scouting)
            {
                $scoutingItem = $scoutingManager->getScoutingLine($scouting);
                $scoutingItems[$scouting->getId() ] = array('content' => $scoutingItem,'scouting' => $scouting);
            }
                    
            $localityName = $request->get('name');
            //$sports
            
            
        }

        return $this->render('Webteamer\Locality\LocalityModule:public:index.php', array(
                    'playgrounds' => $playgrounds,
                    'teams' => $teams,
                    'teamInfo' => $teamInfo,
                    'players' => $players,
                    'eventsList' => $eventsList,
                    'scoutingItems' => $scoutingItems,
                    'localityName' => $localityName
        ));
    }

}
