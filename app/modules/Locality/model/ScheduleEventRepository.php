<?php

namespace Webteamer\Locality\Model;

use Core\DbStorage;
use Core\EntityMapper;
use Core\Repository as Repository;
use Core\ServiceLayer;


class ScheduleEventRepository extends Repository {

    public function getTeamEventStorage()
    {
         return new DbStorage('schedule_event_team');
    }
    
    
    public function getTeamMapper()
    {
        return new EntityMapper('Webteamer\Team\Model\Team');
    }
    
    public function save($object)
    {
        $teams = $object->getTeams();
        $first_team = ServiceLayer::getService('TeamRepository')->find($teams[0]);
        //$object->setSportId($first_team->getSportId());
        
        
        $event_id = parent::save($object);

        foreach ($teams as $team_id)
        {
            $data = array();
            $data['team_id'] = $team_id;
            $data['event_id'] = $event_id;
            $this->getTeamEventStorage()->create($data);
        }
        
        return $event_id;
    }
    
    public function findEventTeams($event_id)
    {
       $data = $this->getStorage()->findEventTeams($event_id);
       
        $entityMapper = $this->getTeamMapper();
        
        $list = array();
        foreach($data as $d)
        {
                $object = $entityMapper->createEntityFromArray($d);
                $list[] = $object;
        }

        return $list;
    }
    
    /*
     public function findBy($criteria, $sort_criteria = array()) {
        $data = $this->storage->findBy($criteria, $sort_criteria);
        
        $list = array();
        foreach ($data as $d)
        {
            $dataset = explode('delimiter',$d);
            
            var_dump($dataset);
            
            
            $object = $this->factory->createEntityFromArray($d);
            $list[] = $object;
        }
        return $list;
    }
    
    /*
    public function findBy($criteria, $sort_criteria = array()) {
        $data = $this->storage->findBy($criteria, $sort_criteria);
        $list = array();
        foreach ($data as $d)
        {
            $object = $this->factory->createEntityFromArray($d);
            $list[] = $object;
        }
        return $list;
    }
    */
}
