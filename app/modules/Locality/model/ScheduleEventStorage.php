<?php
namespace Webteamer\Locality\Model;
use Core\DbStorage;
class ScheduleEventStorage extends DbStorage {
    
	public function findEventTeams($event_id)
        {
            $data = \Core\DbQuery::prepare('
               SELECT t.id, t.name
                FROM schedule_event_team et
                LEFT JOIN team t ON et.team_id = t.id
                WHERE et.event_id = :id')
		->bindParam('id', $event_id)
		->execute()
		->fetchAll(\PDO::FETCH_ASSOC);
		return $data;
        }
        
        public function find($index)
	{
		$data = \Core\DbQuery::prepare('SELECT e.* ,"a_" as assoc,
t.id as a_id, t.name as a_name, t.sport_id as a_sport_id, t.created_at as a_created_at, t.author_id as a_author_id, t.age_from as a_age_from, t.age_to as a_age_to, 
t.status as a_status, t.description as a_description, t.phone as a_phone, t.email as a_email   FROM '.$this->getTableName().' e 
LEFT JOIN schedule_event_team et ON e.id = et.event_id
LEFT JOIN team t ON et.team_id  = t.id WHERE e.id =:id ')
		->bindParam('id', $index)
		->execute()
		->fetchOne(\PDO::FETCH_ASSOC);
		return $data;
	}
        
       
	public function findBy($query_params = array(),$options = array())
	{

		$where_criteria_items  = array();
		foreach ($query_params as $column => $column_val)
		{
			if(is_string($column_val))
			{
				$where_criteria_items[] = ' '.$column.'="'.$column_val.'" ';
			}
			else
			{
				$where_criteria_items[] = ' '.$column.'='.$column_val.' ';
			}
			
		}
		$where_criteria = ' WHERE '. implode(' AND ',$where_criteria_items);
        
                $sort_criteria = '';
                if(array_key_exists('order',$options))
                {
                    $sort_criteria = ' order by '.$options['order'];
                }
		
		//$query = \Core\DbQuery::prepare('SELECT p.*, t.*  FROM '.$this->getTableName().' '. $where_criteria.$sort_criteria);
		$query = \Core\DbQuery::prepare('SELECT e.* ,"a_" as assoc,
t.id as a_id, t.name as a_name, t.sport_id as a_sport_id, t.created_at as a_created_at, t.author_id as a_author_id, t.age_from as a_age_from, t.age_to as a_age_to, 
t.status as a_status, t.description as a_description, t.phone as a_phone, t.email as a_email   FROM '.$this->getTableName().' e 
LEFT JOIN schedule_event_team et ON e.id = et.event_id
LEFT JOIN team t ON et.team_id  = t.id '. $where_criteria.$sort_criteria);
                
                
                
		foreach($query_params as $column => $value)
		{
			if("" != $value)
			{
				$query->bindParam(':'.$column, $value);
			}
		}
		$data = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
		return $data;
	}
	
	
	
}