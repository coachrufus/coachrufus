<?php
namespace Webteamer\Locality\Model;
use Core\Repository as Repository;



class PlaygroundRepository extends Repository
{
    
    
    public function getAliasStorage()
    {
        return new \Core\DbStorage('playground_alias');
    }
    
    public function getAliasEntityMapper()
    {
        return new \Core\EntityMapper('Webteamer\Locality\Model\PlaygroundAlias');
    }
    
    public function createAliasObjectFromArray($data)
    {
         $object = $this->getAliasEntityMapper()->createEntityFromArray($data);
          return $object;
    }
    
     public function saveAlias($object)
    {
        $data = $this->convertToStorageData($object);
        $storage = $this->getAliasStorage();
        if ($object->getId() == null)
        {
            return $storage->create($data);
        }
        else
        {
            return $storage->update($object->getId(), $data);
        }
    }
    
    public function findTeamPlaygrounds($teamId)
    {
        $data = $this->getStorage()->findTeamPlaygrounds($teamId);
        return $this->createObjectList($data);
    }
    
    public function findPlaygroundsByTeam($teamId)
    {
        $data = $this->getStorage()->findPlaygroundsByTeam($teamId);
        return $this->createObjectList($data);
    }
    
    public function findPlaygroundsAliasByTeam($teamId)
    {
        $data = $this->getStorage()->findPlaygroundsAliasByTeam($teamId);
        return $this->createObjectList($data);
    }
    
    public function findPlaygroundsAliasByPlayground($playgroundId)
    {
        $data = $this->getStorage()->findPlaygroundsAliasByPlayground($playgroundId);
        return $this->createAliasObjectFromArray($data);
    }
    
    public function findPlaygroundAliasById($id)
    {
        $data = $this->getAliasStorage()->find($id);
        return $this->createAliasObjectFromArray($data);
    }
    
      public function findPlaygroundsByArea($center, $distance)
    {
        $data = $this->getStorage()->findPlaygroundsByArea($center, $distance);

        $list = array();
        foreach ($data as $d)
        {
            $object = $this->factory->createEntityFromArray($d);
            $object->setLat($d['lat']);
            $object->setLng($d['lng']);
            $list[] = $object;
        }
        return $list;
    }
    
    public function findGlobalPlaygrounds()
    {
        $data = $this->getStorage()->findGlobalPlaygrounds();
        t_dump($data);
        
       return $this->createObjectList($data);
    }
    
}


