<?php

namespace Webteamer\Locality\Model;

use Core\EntityMapper;
use Core\Repository as Repository;


class LocalityRepository extends Repository {

  
    public function getTeamMapper()
    {
        return new EntityMapper('Webteamer\Team\Model\Team');
    }
    
    public function findCityByName($name)
    {
         $data = $this->getStorage()->findCityByName($name);
         return $data;
    }
    
     public function findAllCitys()
    {
         $data = $this->getStorage()->findAllCitys();
         return $data;
    }
    /*
    public function findTeamLocality($team_id)
    {
        $data = $this->getStorage()->findTeamLocality($team_id);
        return $this->createObjectList($data);
    }
    
    public function findLocalityTeams($locality_id)
    {
        $data = $this->getStorage()->findLocalityTeams($locality_id);
        
        
        $entityMapper = $this->getTeamMapper();
        
        $list = array();
        foreach($data as $d)
        {
                $object = $entityMapper->createEntityFromArray($d);
                $list[] = $object;
        }

        return $list;
    }
     * 
     */
}
