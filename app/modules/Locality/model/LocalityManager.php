<?php

namespace Webteamer\Locality\Model;

use Core\ServiceLayer;

class LocalityManager  extends \Core\Manager{

    

    public function getUserLocalityFormChoices($user)
    {
        $repo = ServiceLayer::getService('LocalityRepository');

        $db_choices = $repo->findBy(array('author_id' => $user->getId()));
        $choices = array();

        foreach ($db_choices as $db_choice)
        {
            $choices[$db_choice->getId()] = $db_choice->getName();
        }

        return $choices;
    }

    public function getUserLocations($user)
    {
        $repo = ServiceLayer::getService('LocalityRepository');
        $list = $repo->findBy(array('author_id' => $user->getId()));
        return $list;
    }

    public function getNearestEvent($locations)
    {
        $eventRepo = ServiceLayer::getService('EventRepository');

        $locality_ids = array();
        foreach ($locations as $locality)
        {
            $locality_ids[] = $locality->getId();
        }

        $events = $eventRepo->findNearestLocalityEvents($locality_ids);
        return $events;
    }

    public function getLocalityTeams($locality)
    {
        $repo = ServiceLayer::getService('LocalityRepository');
        return $repo->findLocalityTeams($locality->getId());
    }

    /**
     * 
     * @param google_id
     */
    public function findLocalityByGoogleId($google_id)
    {
        $locality = $this->getRepository()->findOneBy(array('google_id' => $google_id));
        return $locality;
    }
    
    /**
     * 
     * @param locality_id
     */
    public function getLocalityById($id)
    {
        $locality = $this->getRepository()->findOneBy(array('id' => $id));
        return $locality;
    }
    
    
    
    public function createObjectFromJsonData($jsonString)
    {
        $data = json_decode($jsonString);
        $locality = new Locality();
        $locality->setName($data->formatted_address);
       
        
        //$locality->setLat($data->geometry->bounds->O->O);
        //$locality->setLng($data->geometry->bounds->j->j);
        $locality->setGoogleId($data->place_id);
        $locality->setJsonData($jsonString);
        
        return $locality;
    }
    
    public function saveLocality($locality)
    {
        $locality->setCreatedAt(new \DateTime());
        return $this->getRepository()->save($locality);
    }
    
    public function getLocalityByIp($ip = null)
    {
        if($ip == null)
        {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        //$ip = '193.87.81.34';
        //http://freegeoip.net/json/170.66.1.233
      
        
        //$details = json_decode(file_get_contents("http://freegeoip.net/json/".$ip));
        $details = geoip_record_by_name($ip);
     
        if(false != $details)
        {
           
            $result  = new \stdClass();
            $result->ip = $ip;
            $result->country_code  = $details['country_code'];
            $result->country_name  = $details['country_name'];
            $result->region_code  =  $details['region'];
            $result->region_name   = $details['region'];
            $result->city    = iconv("ISO-8859-1","UTF-8",$details['city']);
            $result->zip_code    = $details['postal_code'];
            $result->time_zone     = geoip_time_zone_by_country_and_region($details['country_code']);
            $result->latitude     = $details['latitude'];
            $result->longitude      = $details['longitude'];
            $result->metro_code       = '';
            //$details = json_decode(file_get_contents("http://ipinfo.io/".$ip."/json"));
            return $result;
        }
        return null;
    }
    
    
     public function findCityByName($name)
    {
        $list = $this->getRepository()->findCityByName($name);
        return $list;
    }

}
