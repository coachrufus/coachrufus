<?php
namespace Webteamer\Locality\Model;
use Core\DbStorage;
class LocalityStorage extends DbStorage {
    
        public function findCityByName($name)
        {

            $data = \Core\DbQuery::prepare('
               SELECT * FROM village WHERE fullname like :name')
		->bindParam('name', $name."%")
		->execute()
		->fetchAll(\PDO::FETCH_ASSOC);
		return $data;    
}
        public function findAllCitys()
        {
            $data = \Core\DbQuery::prepare(' SELECT * FROM village')
		->execute()
		->fetchAll(\PDO::FETCH_ASSOC);
		return $data;    
}
    
    /*
	public function findTeamLocality($team_id)
        {
            $data = \Core\DbQuery::prepare('
               SELECT l.id, l.name
                FROM team_locality_alias tla
                LEFT JOIN locality_alias la ON tla.alias_id = la.id
                LEFT JOIN locality l ON la.locality_id = l.id
                WHERE tla.team_id = :id')
		->bindParam('id', $team_id)
		->execute()
		->fetchAll(\PDO::FETCH_ASSOC);
		return $data;
        }
	
	public function findLocalityTeams($locality_id)
        {
            $data = \Core\DbQuery::prepare('
               SELECT t.id, t.name
                FROM team_locality_alias tla
                LEFT JOIN team t ON tla.team_id = t.id
                
                LEFT JOIN locality_alias la ON tla.alias_id = la.id
                
                WHERE la.locality_id = :id')
		->bindParam('id', $locality_id)
		->execute()
		->fetchAll(\PDO::FETCH_ASSOC);
		return $data;
        }
	
	*/
}