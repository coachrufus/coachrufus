<?php

namespace Webteamer\Locality\Model;
use Core\DbStorage;

/**
 * @author hubacek
 * @version 1.0
 * @created 12-11-2015 20:42:36
 */
class PlaygroundStorage extends DbStorage
{
        
        public function findBy($query_params = array(),$options = array())
	{

		$where_criteria_items  = array();
		foreach ($query_params as $column => $column_val)
		{
			if(is_string($column_val))
			{
				$where_criteria_items[] = ' p.'.$column.'=:'.$column.' ';
			}
			else
			{
				$where_criteria_items[] = ' p.'.$column.'=:'.$column.' ';
			}
			
		}
		$where_criteria = ' WHERE '. implode(' AND ',$where_criteria_items);
        
                $sort_criteria = '';
                if(array_key_exists('order',$options))
                {
                    $sort_criteria = ' order by '.$options['order'];
                }
		
		$query = \Core\DbQuery::prepare('SELECT p.* , '.$this->getAssocColumnsQuery().'
                FROM  playground_alias pa 
                LEFT JOIN playground p ON pa.playground_id = p.id
                LEFT JOIN locality a ON p.locality_id = a.id '. $where_criteria.$sort_criteria);
		foreach($query_params as $column => $value)
		{
			if("" != $value)
			{
				$query->bindParam(':'.$column, $value);
			}
		}
		$data = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
		return $data;
            }
        
    
        public function findTeamPlaygrounds($teamId)
        {
            $data = \Core\DbQuery::prepare('
              SELECT p.* , '.$this->getAssocColumnsQuery().'
                FROM  playground_alias pa
                LEFT JOIN playground p ON pa.playground_id = p.id
                LEFT JOIN locality a ON p.locality_id = a.id
                WHERE pa.team_id = :id')
		->bindParam('id', $teamId)
		->execute()
		->fetchAll(\PDO::FETCH_ASSOC);
		return $data;
        }
        
        public function findPlaygroundsByTeam($teamId)
        {
            $data = \Core\DbQuery::prepare('
              SELECT p.* , '.$this->getAssocColumnsQuery().'
                FROM team_playground_alias tpa
                LEFT JOIN playground_alias pa ON tpa.alias_id = pa.id
                LEFT JOIN playground p ON pa.playground_id = p.id
                LEFT JOIN locality a ON p.locality_id = a.id
                WHERE tpa.team_id = :id')
		->bindParam('id', $teamId)
		->execute()
		->fetchAll(\PDO::FETCH_ASSOC);
		return $data;
        }
        
        public function findPlaygroundsAliasByTeam($teamId)
        {
            $data = \Core\DbQuery::prepare('
              SELECT p.* ,pa.id as alias_id, pa.name as alias_name, '.$this->getAssocColumnsQuery().'
                FROM team_playground_alias tpa
                LEFT JOIN playground_alias pa ON tpa.alias_id = pa.id
                LEFT JOIN playground p ON pa.playground_id = p.id
                LEFT JOIN locality a ON p.locality_id = a.id
                WHERE tpa.team_id = :id')
		->bindParam('id', $teamId)
		->execute()
		->fetchAll(\PDO::FETCH_ASSOC);
		return $data;
        }
        
        public function findPlaygroundsAliasByPlayground($playgroundId)
        {
            $data = \Core\DbQuery::prepare('
              SELECT * from playground_alias  WHERE playground_id = :id')
		->bindParam('id', $playgroundId)
		->execute()
                ->fetchOne(\PDO::FETCH_ASSOC);
		return $data;
        }
        
        public function findPlaygroundsByArea($center, $distance)
        {
            $data = \Core\DbQuery::prepare('SELECT p.*, l.lat, l.lng, (6371 * ACOS(COS(RADIANS('.$center['lat'].')) * COS(RADIANS(l.lat)) * COS(RADIANS(l.lng) - RADIANS('.$center['lng'].')) + SIN(RADIANS('.$center['lat'].')) * SIN(RADIANS(l.lat)))) AS distance FROM locality l
LEFT JOIN playground p ON l.id = p.locality_id
HAVING distance < '.$distance)
		->execute()
		->fetchAll(\PDO::FETCH_ASSOC);
		return $data;
        }
        
        public function findGlobalPlaygrounds()
        {
           
            
            $data = \Core\DbQuery::prepare('SELECT p.* , '.$this->getAssocColumnsQuery().'
                FROM playground p 
                LEFT JOIN locality a ON p.locality_id = a.id ')
		->execute()
                ->fetchAll(\PDO::FETCH_ASSOC);
		return $data;
        }


}
?>