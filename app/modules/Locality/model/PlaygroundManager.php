<?php

namespace Webteamer\Locality\model;

use Core\Manager;
use Webteamer\Locality\Model\Locality;
use Webteamer\Locality\Model\PlaygroundAlias;

/**
 * @author hubacek
 * @version 1.0
 * @created 12-11-2015 12:33:05
 */
class PlaygroundManager extends Manager {

    private $localityManager;
    
    private $statusEnum = array('public' => 'public', 'closed' => 'closed', 'private' => 'private');
    private $areaTypeEnum = array('indoor' => 'Indoor', 'outdoor' => 'Outdoor');
   
  

    function getStatusEnum()
    {
        return $this->statusEnum;
    }

    function setStatusEnum($statusEnum)
    {
        $this->statusEnum = $statusEnum;
    }
    
    function getAreaTypeEnum()
    {
        return $this->areaTypeEnum;
    }

    function setAreaTypeEnum($areaTypeEnum)
    {
        $this->areaTypeEnum = $areaTypeEnum;
    }

    public function __construct($repository = null, $localityManager)
    {
        if (null != $repository)
        {
            $this->setRepository($repository);
        }
        $this->setLocalityManager($localityManager);
    }

    public function createObjectFromArray($data)
    {
        $object =  $this->getRepository()->createObjectFromArray($data);
        if(array_key_exists('lng', $data))
        {
            $object->setLng($data['lng']);
        }
        if(array_key_exists('lat', $data))
        {
            $object->setLat($data['lat']);
        }
        return $object;
    }
    
     public function convertEntityToArray($entity)
    {
        $array =  $this->getRepository()->convertToArray($entity);
        $array['lat'] = $entity->getLat();
        $array['lng'] = $entity->getLng();
        return $array;
    }
    
    public function getUserPlaygrounds($user)
    {
        return $this->getRepository()->findBy(array('author_id' => $user->getId()));
    }
    
    public function getGlobalPlaygrounds()
    {
         return $this->getRepository()->findBy(array('author_id' => '0'));
    }
    
    
    /*
     public function getNearestEvent($playgrounds)
    {
        return 
    }
    */

    public function saveNewPlayground($entity)
    {
        $result = array();
        $result['RESULT'] = 'FAIL';
        
        

        if (is_array($entity))
        {
            $entity = $this->createObjectFromArray($entity);
        }
        $entity->setCreatedAt(new \DateTime());

        $savedId = $this->getRepository()->save($entity);
        $entity->setId($savedId);
        $result['RESULT'] = 'SUCCESS';
        $result['entity'] = $entity;

        return $result;
    }
    
    public function savePlayground($entity)
    {
        return $this->getRepository()->save($entity);
    }

    /**
     * Save playground locality and alias
     * @param type $entity
     * @return PlaygroundAlias
     */
    public function savePlaygroundAlias($entity)
    {
        $result = array();
        $result['RESULT'] = 'FAIL';
        if(array_key_exists('team_id', $entity))
        {
            $teamId = $entity['team_id'];
        }
        else
        {
            $teamId = null;
        }

        
        //if locality not exist, create it
        if(!array_key_exists('loocality_id', $entity))
        {
            //check if is selected  exist playground
            if(array_key_exists('exist_playground', $entity) && null != $entity['exist_playground'])
            {
                $playgroundId = $entity['exist_playground'];
                $playgroundName = $entity['name'];
                
                //create new alias
                $alias = new PlaygroundAlias();
                $alias->setPlaygroundId($playgroundId);
                $alias->setTeamId($teamId);
                $alias->setName($playgroundName);
                $savedId = $this->getRepository()->saveAlias($alias);
                $alias->setId($savedId);

                $result['RESULT'] = 'SUCCESS';
                $result['entity'] = $alias;
                
            }
            elseif(array_key_exists('locality_json_data', $entity))  //check if exist locality as json
            {
                $locality =  $this->getLocalityManager()->createObjectFromJsonData($entity['locality_json_data']);


                if(array_key_exists('city', $entity))
                {
                    $locality->setCity($entity['city']);
                }
                
                
                if(array_key_exists('street', $entity))
                {
                    $locality->setStreet($entity['street']);
                }
                
                
                if(array_key_exists('street_number', $entity))
                {
                    $locality->setStreetNumber($entity['street_number']);
                }

                $locality->setAuthorId($entity['author_id']);
                $locality_id = $this->getLocalityManager()->saveLocality($locality);
                $entity['locality_id'] = $locality_id;
                
                 //first save playground
                $result = $this->saveNewPlayground($entity);

                //save alias
                $alias = new PlaygroundAlias();
                $alias->setPlaygroundId($result['entity']->getId());
                $alias->setName($result['entity']->getName());
                $alias->setTeamId($teamId);
                $savedId = $this->getRepository()->saveAlias($alias);
                $alias->setId($savedId);

                $result['RESULT'] = 'SUCCESS';
                $result['entity'] = $alias;

               
            }
            elseif(is_a($entity, '\Webteamer\Locality\Model\PlaygroundAlias'))
            {
                 $this->getRepository()->saveAlias($entity);
            }
        }

       
        return $result;
       
    }

    /**
     * 
     * @param locality
     */
    public function findPlaygroundsByLocality(Locality $locality)
    {
        $list = $this->getRepository()->findBy(array('locality_id' => $locality->getId()));
        return $list;
    }
    
    /**
     * Find playground nearby locality according to distance
     * @param array $center lattitude and longtitude array('lat' => 48.12,'lng' =>18.54);
     * @param float $distance distance in kilometersgit a
     * @return type
     */
    public function findPlaygroundsByArea($center, $distance)
    {
        $list = $this->getRepository()->findPlaygroundsByArea($center,$distance);
        return $list;
    }
    
    public function getTeamPlaygrounds($team)
    {
         $list = $this->getRepository()->findTeamPlaygrounds($team->getId());
        return $list;
    }
    
    
    public function findPlaygroundsByTeam($team)
    {
        $list = $this->getRepository()->findPlaygroundsByTeam($team->getId());
        return $list;
    }
    
    public function findPlaygroundsAliasByTeam($team)
    {
        $list = $this->getRepository()->findPlaygroundsAliasByTeam($team->getId());
        return $list;
    }
    
    public function findPlaygroundsAliasByPlayground($playground)
    {
        $list = $this->getRepository()->findPlaygroundsAliasByPlayground($playground->getId());
        return $list;
    }
    
     public function findPlaygroundAliasById($id)
    {
        $list = $this->getRepository()->findPlaygroundAliasById($id);
        return $list;
    }
    
    public function findPlaygroundById($id)
    {
        $entity = $this->getRepository()->find($id);
        return $entity;
    }
    
    

    public function getLocalityManager()
    {
        return $this->localityManager;
    }

    /**
     * 
     * @param newVal
     */
    public function setLocalityManager($newVal)
    {
        $this->localityManager = $newVal;
    }
    
    public function deletePlayground($playground)
    {
        $this->getRepository()->delete($playground->getId());
    }
   

}

?>