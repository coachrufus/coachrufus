<?php
	
namespace Webteamer\Locality\Model;
use Core\EntityMapper as EntityMapper;
use Core\ServiceLayer as ServiceLayer;
		
class PlaygroundAlias {	

	
	
		
private $repository;
private $mapper_rules  = array(
	'id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => true
							
					),
'name' => array(
					'type' => 'string',
					'max_length' => '255',
					'required' => false
							
					),
'playground_id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => false
							
					),
'team_id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => false
							
					),

);
	
public function getDataMapperRules()
{
	return $this->mapper_rules;
}

	
		
protected $id;

protected $name;

protected $playgroundId;
protected $teamId;

				
public function setId($val){$this->id = $val;}

public function getId(){return $this->id;}
	
public function setName($val){$this->name = $val;}

public function getName(){return $this->name;}
	
public function setPlaygroundId($val){$this->playgroundId = $val;}

public function getPlaygroundId(){return $this->playgroundId;}

function getTeamId() {
return $this->teamId;
}

 function setTeamId($teamId) {
$this->teamId = $teamId;
}



}

		