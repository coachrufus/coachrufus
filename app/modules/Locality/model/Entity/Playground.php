<?php

namespace Webteamer\Locality\Model;

use Core\EntityMapper as EntityMapper;
use Core\ServiceLayer as ServiceLayer;

/**
 * Konkrétne športovisko, napr
 * Volejbalová hala na Pasienkoch
 * @author hubacek
 * @version 1.0
 * @updated 12-11-2015 12:43:42
 */
class Playground {

    private $mapper_rules = array(
        'id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => true
        ),
        'name' => array(
            'type' => 'string',
            'max_length' => '255',
            'required' => false
        ),
        'created_at' => array(
            'type' => 'datetime',
            'max_length' => '',
            'required' => false
        ),
        'author_id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'locality_id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'city' => array(
            'type' => 'string',
            'required' => false
        ),
        'zip' => array(
            'type' => 'string',
            'required' => false
        ),
        'country' => array(
            'type' => 'string',
            'required' => false
        ),
        'street' => array(
            'type' => 'string',
            'required' => false
        ),
        'street_number' => array(
            'type' => 'string',
            'required' => false
        ),
        'description' => array(
            'type' => 'string',
            'required' => false
        ),
        'area_type' => array(
            'type' => 'string',
            'required' => false
        ),
        'locality' => array(
            'type' => 'non-persist',
            'required' => false
        ),
        'alias_id' => array(
            'type' => 'non-persist',
            'required' => false
        ),
        'alias_name' => array(
            'type' => 'non-persist',
            'required' => false
        ),
    );

    public function getDataMapperRules()
    {
        return $this->mapper_rules;
    }

    protected $id;
    protected $name;
    protected $createdAt;
    protected $authorId;
    protected $localityId;
    protected $localityName;
    protected $locality;
    protected $lat;
    protected $lng;
    protected $street;
    protected $street_number;
    protected $city;
    protected $zip;
    protected $country;
    protected $description;
    protected $areaType;
    protected $aliasId;
    protected $aliasName;

    public function setId($val)
    {
        $this->id = $val;
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * 
     * @param newVal
     */
    public function setName($val)
    {
        $this->name = $val;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setCreatedAt($val)
    {
        $this->createdAt = $val;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setAuthorId($val)
    {
        $this->authorId = $val;
    }

    public function getAuthorId()
    {
        return $this->authorId;
    }

    public function setLocalityId($val)
    {
        $this->localityId = $val;
    }

    public function getLocalityId()
    {
        return $this->localityId;
    }

    public function getLocality()
    {
        return $this->locality;
    }

    /**
     * 
     * @param newVal
     */
    public function setLocality($newVal)
    {
        $this->locality = $newVal;
    }

    public function getLat()
    {
        if (null == $this->lat && null != $this->getLocality())
        {
            $this->lat = $this->getLocality()->getLat();
        }

        return $this->lat;
    }

    public function getLng()
    {
        if (null == $this->lng)
        {
            $this->lng = $this->getLocality()->getLng();
        }

        return $this->lng;
    }

    public function setLat($lat)
    {
        $this->lat = $lat;
    }

    public function setLng($lng)
    {
        $this->lng = $lng;
    }

    public function getStreet()
    {
        return $this->street;
    }

    public function getStreetNumber()
    {
        return $this->street_number;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setStreet($street)
    {
        $this->street = $street;
    }

    public function setStreetNumber($street_number)
    {
        $this->street_number = $street_number;
    }

    public function setCity($city)
    {
        $this->city = $city;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }
    
    function getAreaType()
    {
        return $this->areaType;
    }

    function setAreaType($areaType)
    {
        $this->areaType = $areaType;
    }

    public function getAliasId()
    {
        return $this->aliasId;
    }

    public function getAliasName()
    {
        return $this->aliasName;
    }

    public function setAliasId($aliasId)
    {
        $this->aliasId = $aliasId;
    }

    public function setAliasName($aliasName)
    {
        $this->aliasName = $aliasName;
    }
    
    public  function getZip() {
return $this->zip;
}

public  function getCountry() {
return $this->country;
}

public  function setZip($zip) {
$this->zip = $zip;
}

public  function setCountry($country) {
$this->country = $country;
}



    public function getLocalityName()
    {
        if (null == $this->localityName)
        {
            $this->localityName = $this->getLocality()->getName();
        }
        return $this->localityName;
    }

    public function setLocalityName($localityName)
    {
        $this->localityName = $localityName;
    }
    
    public function getFullAddress()
    {
        return $this->getCity().', '.$this->getStreet().' '.$this->getStreetNumber();
    }

    
    
}
