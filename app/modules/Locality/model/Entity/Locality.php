<?php
	
namespace Webteamer\Locality\Model;
use Core\EntityMapper as EntityMapper;
use Core\ServiceLayer as ServiceLayer;
		
class Locality {	

	
	
		
private $repository;
private $mapper_rules  = array(
	'id' => array(
					'type' => 'int',
					'max_length' => '11',
					'required' => true
							
					),
'name' => array(
					'type' => 'string',
					'max_length' => '255',
					'required' => true
							
					),
'lat' => array(
					'type' => '',
					'max_length' => '',
					'required' => true
							
					),
'lng' => array(
					'type' => '',
					'max_length' => '',
					'required' => true
							
					),
     'created_at' => array(
					'type' => 'datetime',
					'required' => false
							
					),  
  'author_id' => array(
					'type' => 'int',
					'required' => false
							
					),  
  'google_id' => array(
					'type' => 'int',
					'required' => false
							
					),  
    'json_data' => array(
					'type' => 'string',
					'required' => false
							
					),  
    'city' => array(
					'type' => 'string',
					'required' => false
							
					),  
    'street' => array(
					'type' => 'string',
					'required' => false
							
					),  
    'street_number' => array(
					'type' => 'string',
					'required' => false
							
					),  
    'description' => array(
					'type' => 'string',
					'required' => false
							
					),  

);

/*
public function setMapRule()
{
    
}
	*/
public function getDataMapperRules()
{
	return $this->mapper_rules;
}


		
		
protected $id;

protected $name;

protected $lat;

protected $lng;
protected $createdAt;
protected $authorId;
protected $googleId;
protected $jsonData;
protected $street;
protected $street_number;
protected $city;
protected $description;

				
public function setId($val){$this->id = $val;}

public function getId(){return $this->id;}
	
public function setName($val){$this->name = $val;}

public function getName(){return $this->name;}
	
public function setLat($val){$this->lat = $val;}

public function getLat(){
    
    if(null == $this->lat)
    {
        $data = json_decode($this->getJsonData());

        if(isset($data->geometry) && isset($data->geometry->location) && isset($data->geometry->location->lat))
        {
            return $data->geometry->location->lat;
        }
        
        if(isset($data->geometry) && isset($data->geometry->bounds) && isset($data->geometry->bounds->O))
        {
            return $data->geometry->bounds->O->O;
        }
    }
    else 
        {
        return $this->lat;
        }
}
	
public function setLng($val){$this->lng = $val;}

public function getLng(){
    
    if(null == $this->lng)
    {
        $data = json_decode($this->getJsonData());

        if(isset($data->geometry) && isset($data->geometry->location) && isset($data->geometry->location->lng))
        {
            return $data->geometry->location->lng;
        }
        
        if(isset($data->geometry) && isset($data->geometry->bounds) && isset($data->geometry->bounds->j))
        {
            return $data->geometry->bounds->j->j;
        }
    }
    else 
        {
        return $this->lng;
        }
    

    
}

function getCreatedAt() {
return $this->createdAt;
}

 function getAuthorId() {
return $this->authorId;
}

 function setCreatedAt($createdAt) {
$this->createdAt = $createdAt;
}

 function setAuthorId($authorId) {
$this->authorId = $authorId;
}

function getGoogleId() {
return $this->googleId;
}

 function setGoogleId($googleId) {
$this->googleId = $googleId;
}

public  function getJsonData() {
return $this->jsonData;
}

public  function setJsonData($jsonData) {
$this->jsonData = $jsonData;
}

public  function getStreet() {
return $this->street;
}

public  function getStreetNumber() {
return $this->street_number;
}

public  function getCity() {
return $this->city;
}

public  function getDescription() {
return $this->description;
}

public  function setStreet($street) {
$this->street = $street;
}

public  function setStreetNumber($street_number) {
$this->street_number = $street_number;
}

public  function setCity($city) {
$this->city = $city;
}

public  function setDescription($description) {
$this->description = $description;
}


public function getRegion()
{
     $data = json_decode($this->getJsonData());
        if(isset($data->address_components))
        {
            foreach($data->address_components as $address_components)
            {
                if($address_components->types[0] == 'administrative_area_level_1')
                {
                    return $address_components->long_name;
                }

            }
        }
}
public function getDistrict()
{
     $data = json_decode($this->getJsonData());
        if(isset($data->address_components))
        {
            foreach($data->address_components as $address_components)
            {
                if($address_components->types[0] == 'administrative_area_level_2')
                {
                    return $address_components->long_name;
                }

            }
        }
}
public function getCity2()
{
     $data = json_decode($this->getJsonData());
        if(isset($data->address_components))
        {
            foreach($data->address_components as $address_components)
            {
                if($address_components->types[2] == 'sublocality_level_1')
                {
                    return $address_components->long_name;
                }

            }
        }
}
public function getFullAddress()
{
     $data = json_decode($this->getJsonData());
        if(isset($data->formatted_address))
        {
           return $data->formatted_address;
        }
}



}

		