<?php
	
namespace Webteamer\Locality\Model;
use Core\EntityMapper as EntityMapper;
use Core\ServiceLayer as ServiceLayer;
		
class Country {	

	
	
		

private $mapper_rules  = array(
	'id' => array(
					'type' => 'int',
					'max_length' => '10',
					'required' => true
							
					),
'country_code' => array(
					'type' => 'string',
					'max_length' => '2',
					'required' => true
							
					),
'sk_name' => array(
					'type' => 'string',
					'max_length' => '75',
					'required' => true
							
					),
'en_name' => array(
					'type' => 'string',
					'max_length' => '75',
					'required' => true
							
					),
'vat' => array(
					'type' => '',
					'max_length' => '',
					'required' => true
							
					),

);
	
public function getDataMapperRules()
{
	return $this->mapper_rules;
}


		
		
		
protected $id;

protected $countryCode;

protected $skName;

protected $enName;

protected $vat;

				
public function setId($val){$this->id = $val;}

public function getId(){return $this->id;}
	
public function setCountryCode($val){$this->countryCode = $val;}

public function getCountryCode(){return $this->countryCode;}
	
public function setSkName($val){$this->skName = $val;}

public function getSkName(){return $this->skName;}
	
public function setEnName($val){$this->enName = $val;}

public function getEnName(){return $this->enName;}
	
public function setVat($val){$this->vat = $val;}

public function getVat(){return $this->vat;}

}

		