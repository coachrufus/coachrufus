<?php

namespace Webteamer\Locality\Model;

use Core\EntityMapper as EntityMapper;
use Core\ServiceLayer as ServiceLayer;

class ScheduleEvent {

    private $repository;
    private $mapper_rules = array(
        'id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => true
        ),
        'name' => array(
            'type' => 'string',
            'max_length' => '50',
            'required' => false
        ),
        'termin' => array(
            'type' => 'datetime',
            'max_length' => '',
            'required' => false
        ),
        'type' => array(
            'type' => 'string',
            'max_length' => '50',
            'required' => false
        ),
        'playground_id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'sport_id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'note' => array(
            'type' => 'string',
            'max_length' => '2000',
            'required' => false
        ),
        'teams' => array(
            'type' => 'non-persist',
            'required' => false
        ),
    );

    public function getDataMapperRules()
    {
        return $this->mapper_rules;
    }

    protected $id;
    protected $name;
    protected $termin;
    protected $type;
    protected $playgroundId;
    protected $sportId;
    protected $note;
    protected $teams;

    public function setId($val)
    {
        $this->id = $val;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setName($val)
    {
        $this->name = $val;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setTermin($val)
    {
        $this->termin = $val;
    }

    public function getTermin()
    {
        return $this->termin;
    }

    public function setType($val)
    {
        $this->type = $val;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getPlaygroundId()
    {
        return $this->playgroundId;
    }

    public function setPlaygroundId($playgroundId)
    {
        $this->playgroundId = $playgroundId;
    }

    public function setSportId($val)
    {
        $this->sportId = $val;
    }

    public function getSportId()
    {
        return $this->sportId;
    }

    public function getTeams()
    {

        return $this->teams;
    }
    
    public function addTeam($val)
    {
         $this->teams[] = $val;
    }

    public function setTeams($teams)
    {
        $this->teams = $teams;
    }

    public function getTeamNames()
    {
        $names = array();
        foreach ($this->getTeams() as $team)
        {
            $names[] = $team->getName();
        }

        return $names;
    }
    
    public  function getNote() {
return $this->note;
}

public  function setNote($note) {
$this->note = $note;
}



}
