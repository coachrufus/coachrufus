<?php
namespace Webteamer\Locality\Model;

use Core\Manager;


class ScheduleEventManager extends Manager {
    public function removeEventById($id)
    {
        $this->getRepository()->delete($id);
    }
}