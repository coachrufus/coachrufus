<?php
namespace Webteamer\Locality\Model;
/**
 * @author hubacek
 * @version 1.0
 * @updated 05-11-2015 12:41:21
 */
class PlaygroundSchedule
{

	private $end;
	private $playground;
	private $start;



	public function getEnd()
	{
		return $this->end;
	}


	public function getStart()
	{
		return $this->start;
	}

	/**
	 * 
	 * @param newVal
	 */
	public function setEnd($newVal)
	{
		$this->end = $newVal;
	}

	
        public  function getPlayground() {
return $this->playground;
}

public  function setPlayground($playground) {
$this->playground = $playground;
}


        

	/**
	 * 
	 * @param newVal
	 */
	public function setStart($newVal)
	{
		$this->start = $newVal;
	}
        
        
       
        public function getDays()
        {
            $start = $this->getStart();
            $end = $this->getEnd();
            $interval = $start->diff($end);
            $days =array();
            for($i = 0; $i <= $interval->format('%d'); $i++)
            {
           
                $days[] = new \DateTime(date('Y-m-d H:i:s', $start->getTimestamp()+(60*60*24*$i)));
            }
            
           return $days;
        }
        
        public function getWeekDaysByDate($date)
        {
            
            $date->setISODate($date->format('Y'), $date->format("W"));
            $this->setStart(new \DateTime( $date->format('Y-m-d H:i:s')));
            $this->setEnd(new \DateTime(date('Y-m-d H:i:s', $this->getStart()->getTimestamp()+(60*60*24*6))));
            return $this->getDays();
        }
        
        public function getHours()
        {
            $hours = array('08:00','09:00','10:00');
            $choices = array_combine($hours, $hours);
            return $choices;
        }
        
        public function getEvents($day,$hour)
        {
            $repo = \Core\ServiceLayer::getService('ScheduleEventRepository');
            
            //$events = $repo->findEvents(array('playground_id' => $this->getPLayground()->getId(),'termin' => $day->format('Y-m-d').' '.$hour));
            
            $events = $repo->findBy(array('playground_id' => $this->getPLayground()->getId(),'termin' => $day->format('Y-m-d').' '.$hour));

            return $events;
        }
       

}

