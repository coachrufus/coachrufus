<?php

namespace Webteamer\Locality\Form;

use \Core\Form as Form;

/**
 * @author Marek Hubáček
 * @version 1.0
 * @created 21-10-2015 14:54:15
 */
class PlaygroundForm extends Form {

    private $sportChoices;
    private $statusChoices;

    public function __construct()
    {
       

        $this->setField('name', array(
            'type' => 'string',
            'max_length' => '255',
            'required' => true,
        ));

        $this->setField('lat', array(
            'type' => 'string',
            'max_length' => '255',
            'required' => false,
        ));

        $this->setField('lng', array(
            'type' => 'string',
            'max_length' => '255',
            'required' => false,
        ));

        $this->setField('sport', array(
            'type' => 'choice',
            'choices' => $this->getSportChoices(),
            'required' => false,
        ));


        $this->setField('status', array(
            'type' => 'choice',
            'choices' => $this->getStatusChoices(),
            'required' => false,
        ));

        $this->setField('street', array(
            'type' => 'text',
            'required' => false,
        ));

        $this->setField('city', array(
            'type' => 'text',
            'required' => false,
        ));
        $this->setField('street_number', array(
            'type' => 'text',
            'required' => false,
        ));
        $this->setField('description', array(
            'type' => 'text',
            'required' => false,
        ));
        $this->setField('area_type', array(
            'type' => 'choice',
            'required' => false,
        ));
        
        $this->setName('playgrounds');
    }

    public function getSportChoices()
    {
        return $this->sportChoices;
    }

    public function getStatusChoices()
    {
        return $this->statusChoices;
    }

    public function setSportChoices($sportChoices)
    {
        $this->sportChoices = $sportChoices;
    }

    public function setStatusChoices($statusChoices)
    {
        $this->statusChoices = $statusChoices;
    }

}

?>