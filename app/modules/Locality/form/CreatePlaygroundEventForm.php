<?php

namespace Webteamer\Locality\Form;

use \Core\Form as Form;

/**
 * @author Marek Hubáček
 * @version 1.0
 * @created 21-10-2015 14:54:15
 */
class CreatePlaygroundEventForm extends Form {

    private $teamChoices;

    public function __construct()
    {
       
    }

    public function getTeamChoices()
    {
        return $this->teamChoices;
    }

    public function setTeamChoices($teamChoices)
    {
        $this->teamChoices = $teamChoices;
    }
    
    public function buildFields()
    {
         $this->setField('teams', array(
            'type' => 'choice',
            'choices' => $this->getTeamChoices(),
             'multiple' => true,
            'required' => false,
        ));
         $this->setField('sport_id', array(
            'type' => 'choice',
            'choices' => array(),
             'multiple' => false,
            'required' => false,
        ));
         $this->setField('name', array(
            'type' => 'string',
            'required' => false,
        ));
         $this->setField('note', array(
            'type' => 'string',
            'required' => false,
        ));
         $this->setField('termin', array(
            'type' => 'datetime',
            'format' => 'd.m.Y H:i:s',
            'required' => false,
        ));
         $this->setField('playground_id', array(
            'type' => 'int',
            'required' => false,
        ));
    }
    
    

}

?>