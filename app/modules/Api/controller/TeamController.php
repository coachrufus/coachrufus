<?php

namespace Coachrufus\Api\Controller;

use Core\GUID;
use Core\RestApiController;
use Core\ServiceLayer;
use User\Handler\StepRegisterHandler;

class TeamController extends RestApiController {

    
      public function getWebviewContent($html,$options=array())
    {
        ServiceLayer::getService('layout')->setTemplate( GLOBAL_DIR.'/templates/WebviewLayout.php');
        $content = $this->renderTemplate(GLOBAL_DIR.'/templates/WebviewLayout.php',array(
             'module_content' => $html,
            'body_class' => $options['body_class']
         ))->getContent();
        
        return $content;  
    }
    
    
    public function userTeamsAction()
    {
        if ($this->getRequest()->getMethod() == 'GET')
        {
             if(false == $this->checkApiKey())
            {
                return $this->unvalidApiKeyResult();
            }
            $user = $this->createUserFromRequest();
           
            $teamManager = ServiceLayer::getService('TeamManager');
            $teams = $teamManager->getUserTeams($user);
            $list = array();
            foreach($teams as $team)
            {
                $teamPlayer = $teamManager->findTeamPlayerByPlayerId($team,$user->getId());
                
                $list[$team->getId()] = array('name' => $team->getName(),'id' => $team->getId(), 'team_player_id'=> $teamPlayer->getId()) ;  
            }

            return $this->asJson(array('status' => 'SUCCESS', 'data' => $list));
        }
        else
        {
            return $this->asJson(array('status' => 'ERROR', 'error' => 'GET METHOD IS REQUIRED, NOT ' . $this->getRequest()->getMethod()));
        }
    }
    
    
    public function createAction()
    {
        if ($this->getRequest()->getMethod() == 'GET')
        {
            $html = $this->render('Coachrufus\Api\ApiModule:Team:create.php', array())->getContent();

            $response = array();
            $response['html'] = $html;
            $response['backlink'] = $this->getRouter()->link('api_content_home');
            $response['filters'] = '';

            return $this->asJson(array('status' => 'SUCCESS', 'data' => $response));
        }
        else
        {
            return $this->asJson(array('status' => 'ERROR', 'error_message' => 'GET METHOD IS REQUIRED, NOT ' . $this->getRequest()->getMethod()));
        }
    }
    
    public function playerCreateAction()
    {
        if ($this->getRequest()->getMethod() == 'GET')
        {
            $data = $this->getRequest()->getGet();
            if(false == $this->checkApiKey())
            {
                return $this->unvalidApiKeyResult();
            }
            $user = $this->createUserFromRequest();
            $apikey = $data['apikey'];
            $userId = $data['user_id'];

            $security = ServiceLayer::getService('security');
         
            if(array_key_exists('a', $data) && 'r' == $data['a'])
            {
                $link =   WEB_DOMAIN. $this->getRouter()->generateApiWidgetUrl('api_content_webview',array(
                 'view' => '/team/manage-players/'.$data['team'],
                 'apikey' => CR_API_KEY,
                 'user_id' => $user->getId()
                 ));
                
                 $this->getRequest()->redirect($link);
            }
            
            $teamRoles = $security->getIdentity()->getTeamRoles();
            $teamManager = \Core\ServiceLayer::getService('TeamManager');
            $teams = $teamManager->getUserTeams($user);
            $availableTeams = array();
            foreach($teams as $team)
            {
                if($teamRoles[$team->getId()]['role'] == 'ADMIN')
                {
                    $availableTeams[] = $team;
                }
            }
            
            if(count($availableTeams) == 1)
            {
                $this->getRequest()->redirect($this->getRouter()->link('team_players_list',array('team_id' => $availableTeams[0]->getId())));
            }
            
            
               $html = $this->render('Coachrufus\Api\ApiModule:Team:playerCreate.php', array(
                        'user' => $user,
                        'apikey' => $apikey,
                   'teams' => $availableTeams
                    ))->getContent();
            
            return $this->asHtml($this->getWebviewContent($html));
        }
        else
        {
            return $this->asJson(array('status' => 'ERROR', 'error_message' => 'GET METHOD IS REQUIRED, NOT ' . $this->getRequest()->getMethod()));
        }
    }
    
    
    public function paymentAction()
    {
        if ($this->getRequest()->getMethod() == 'GET')
        {
            $html = $this->render('Coachrufus\Api\ApiModule:Team:payment.php', array())->getContent();

            $response = array();
            $response['html'] = $html;
            $response['backlink'] = $this->getRouter()->link('api_content_home');
            $response['filters'] = '';

            return $this->asJson(array('status' => 'SUCCESS', 'data' => $response));
        }
        else
        {
            return $this->asJson(array('status' => 'ERROR', 'error_message' => 'GET METHOD IS REQUIRED, NOT ' . $this->getRequest()->getMethod()));
        }
    }
    
    public function seasonAction()
    {
        if ($this->getRequest()->getMethod() == 'GET')
        {
            $data = $this->getRequest()->getGet();
            $repository = ServiceLayer::getService('TeamSeasonRepository');
           
           
            $list = $repository->findBy(array('team_id' => $data['team_id']),array('order' => 'start_date desc'));
           
            foreach($list as $l)
            {
                $response[] = array('id' => $l->getId(),'name' => $l->getName());
            }
          

            return $this->asJson(array('status' => 'SUCCESS', 'data' => $response));
        }
        else
        {
            return $this->asJson(array('status' => 'ERROR', 'error_message' => 'GET METHOD IS REQUIRED, NOT ' . $this->getRequest()->getMethod()));
        }
    }
    
    public function missingTeamAction()
    {
         if ($this->getRequest()->getMethod() == 'GET')
        {
            $data = $this->getRequest()->getGet();
            if(false == $this->checkApiKey())
            {
                return $this->unvalidApiKeyResult();
            }
            $user = $this->createUserFromRequest();
            
            $userId = $data['user_id'];
          

            $html = $this->render('Coachrufus\Api\ApiModule:Team:missing.php', array('userId' => $userId))->getContent();

            $response = array();
            $response['html'] = $html;
            $response['backlink'] = $this->getRouter()->link('api_content_home');
            $response['filters'] = '';

              return $this->asHtml( $this->getWebviewContent($html,array('body_class' => 'alert_body')));
        }
        else
        {
            return $this->asJson(array('status' => 'ERROR', 'error_message' => 'GET METHOD IS REQUIRED, NOT ' . $this->getRequest()->getMethod()));
        }
    }
    
    public function changePlayerStatusAction()
    {
        if ($this->getRequest()->getMethod() == 'PUT')
        {
            //$putData = array();
            //parse_str(file_get_contents("php://input"),$putData);
            
            $putData = json_decode(file_get_contents("php://input"), true);

            $teamPlayerId = $this->getRequest()->get('tp');
            $status = $putData['status'];


            
            if(false == $this->checkApiKey($putData['apikey']))
            {
                return $this->unvalidApiKeyResult();
            }
           
            $teamManager = ServiceLayer::getService('TeamManager');
            $teamManager->updatePlayerFromArray($teamPlayerId, array('status' => $status));
            
            
        }
         else
        {
            return $this->asJson(array('status' => 'ERROR', 'error' => 'PUT METHOD IS REQUIRED, NOT '.$this->getRequest()->getMethod()));
        }
    }
    
    

}
