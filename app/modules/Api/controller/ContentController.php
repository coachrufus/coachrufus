<?php

namespace Coachrufus\Api\Controller;

use Core\RestApiController;
use Core\ServiceLayer;

class ContentController extends RestApiController {

    public function getWebviewContent($html,$options=array())
    {
        ServiceLayer::getService('layout')->setTemplate( GLOBAL_DIR.'/templates/WebviewLayout.php');
        $content = $this->renderTemplate(GLOBAL_DIR.'/templates/WebviewLayout.php',array(
             'module_content' => $html,
             'body_class' => $options['body_class']
         ))->getContent();
        
        return $content;  
    }
    
    public function getWebviewContent1($html,$options=array())
    {
        ServiceLayer::getService('layout')->setTemplate( GLOBAL_DIR.'/templates/WebviewContentLayout.php');
        $content = $this->renderTemplate(GLOBAL_DIR.'/templates/WebviewContentLayout.php',array(
             'module_content' => $html,
             'body_class' => $options['body_class']
         ))->getContent();
        
        return $content;  
    }
    
     public function getWebviewWidgetContent($html)
    {
        ServiceLayer::getService('layout')->setTemplate( GLOBAL_DIR.'/templates/WebviewWidgetLayout.php');
        $content = $this->renderTemplate(GLOBAL_DIR.'/templates/WebviewWidgetLayout.php',array(
             'module_content' => $html
         ))->getContent();
        
        return $content;  
    }
    
    //public function setupL
    
    public function webviewAction()
    {
       //first check user permission
        $request = $this->getRequest();
        $security = ServiceLayer::getService('security');
        //$getData = $request->getGet();
        //$data = json_decode( base64_decode(substr($getData['id'],12)),true);
        
        $data = $request->getDataFromEncodedApiUrl();
        $addData = $request->getGet();
        unset($addData['id']);
        
        //echo http_build_query($addData);
        

  
  
        if(false == $this->checkApiKey($data['apikey']))
        {
            return $this->unvalidApiKeyResult();
        }

        $_SESSION['api']['layout'] = 'base';
        $authHash = $data['auth_key'];
        $userId = $data['user_id'];
        $redirectUrl = $data['view'];

        $user = $security->getIdentityProvider()->findUserByPersistHash($authHash);
        if(null == $user)
        {
            $user =  $security->getIdentityProvider()->findUserById($userId);
        }
            
        if(null != $user)
        {
            $security->authenticateUserEntity($user);    
            $_SESSION['app_lang'] = $user->getDefaultLang();
            $this->getTranslator()->setLang($user->getDefaultLang());

            $hasParams =  strpos($redirectUrl, '?');
            
            $hashTags = explode('#',$redirectUrl);
            if(count($hashTags > 1))
            {
                $hashTag = '#'.$hashTags[1];
            }
            
            if($hasParams === false)
            {
                 $request->Redirect($hashTags[0].'?lt=mal&'.http_build_query($addData).$hashTag);
            }
            else 
            {
                 $request->Redirect($hashTags[0].'&lt=mal&'.http_build_query($addData).$hashTag);
            }
            
           
        }
        else
        {
            $result['status'] = 'ERROR';
            $result['error_message'] = 'UNVALID USER';
            return $this->asJson($result);
        }
        
        
    }
    
    
    public function homeAction()
    {
        if ($this->getRequest()->getMethod() == 'GET')
        {
            $data = $this->getRequest()->getGet();
            $userId = $data['user_id'];
            
            
            if(false == $this->checkApiKey())
            {
                return $this->unvalidApiKeyResult();
            }

            $user = $this->createUserFromRequest();
            $teamManager = ServiceLayer::getService('TeamManager');
            $teams = $teamManager->getUserTeams($user);
            
            
           
            $_SESSION['api']['layout'] = 'base';
            
             $html = $this->render('Coachrufus\Api\ApiModule:Content:home.php', 
                    array(
                        'userId' => $userId ,
                        'a' => $data['a'],
                        'postEdit' =>  $data['post-edit'] ,
                        'pm' => $data['pm'],
                        'tab' => (null == $data['tab'] ? 'team' : $data['tab']  ),
                        'user' =>$user
                    ))
                    ->getContent();
                 return $this->asHtml( $this->getWebviewWidgetContent($html));
                 
                 /*
            if(count($teams) == 0)
            {
                $html = $this->render('Coachrufus\Api\ApiModule:Team:missing.php',
                        array(
                            'userId' => $userId
                        ))
                        ->getContent();
                 return $this->asHtml( $this->getWebviewContent($html,array('body_class' => 'alert_body')));
            }
            else
            {
                 $html = $this->render('Coachrufus\Api\ApiModule:Content:home.php', 
                    array(
                        'userId' => $userId ,
                        'a' => $data['a'],
                        'postEdit' =>  $data['post-edit'] ,
                        'pm' => $data['pm'],
                        'tab' => (null == $data['tab'] ? 'team' : $data['tab']  ),
                        'user' =>$user
                    ))
                    ->getContent();
                 return $this->asHtml( $this->getWebviewWidgetContent($html));
            }
            
           */
           
            

        }
        else
        {
            return $this->asJson(array('status' => 'ERROR', 'error_message' => 'GET METHOD IS REQUIRED, NOT ' . $this->getRequest()->getMethod()));
        }
    }
    
    public function mainMenuAction()
    {
        if ($this->getRequest()->getMethod() == 'GET')
        {
            if(false == $this->checkApiKey())
            {
                return $this->unvalidApiKeyResult();
            }
            $user = $this->createUserFromRequest();

            
            $teamManager = ServiceLayer::getService('TeamManager');
            $teams = $teamManager->getUserTeams($user);
            $teamsCount = count($teams);
            
            $repository = ServiceLayer::getService('TournamentRepository');
            $tournaments = $repository->findUserTournaments($user);
            $tournamentsCount = count($tournaments);
            
            $messageManager = ServiceLayer::getService('MessageManager');
            $messagesInfo = $messageManager->getRecipientMessagesInfo($user);

             $html = $this->render('Coachrufus\Api\ApiModule:Content:mainMenu.php', array(
                'user' => $user,
                'teamsCount' => $teamsCount,
                'tournamentsCount' => $tournamentsCount,
                 'unreadMessages' => $messagesInfo['unread'],
            ))->getContent();
             return $this->asHtml( $this->getWebviewContent($html));
            
        }
        else
        {
            return $this->asJson(array('status' => 'ERROR', 'error_message' => 'GET METHOD IS REQUIRED, NOT ' . $this->getRequest()->getMethod()));
        }
    }
    
    
    public function floatMenuAction()
    {
        $translator = ServiceLayer::getService('translator');
        
        
        if ($this->getRequest()->getMethod() == 'GET')
        {

            $response = array();
            
            $userId = $this->getRequest()->get('user_id');
            
            if(false == $this->checkApiKey())
            {
                return $this->unvalidApiKeyResult();
            }
            $user = $this->createUserFromRequest();
           
            $response['home'][] = array(
                'name' => $translator->translate('mobileApp.floatMenu.home'),
                'icon' => 'home',
                'target' => 'home',
            );
            
             $response['home'][] = array(
                'name' => $translator->translate('mobileApp.floatMenu.addFeedPost'),
                'icon' => 'chat',
                'url' =>  WEB_DOMAIN. '/api/content/home?apikey=app@40101499408386491&user_id='.$userId.'&a=createWindowPopup',
                 'window' => 'webview'
            );
             
            
             $response['home'][] = array(
                'name' => $translator->translate('mobileApp.floatMenu.createTeam'),
                'icon' => 'team',
                'url' =>  WEB_DOMAIN. $this->getRouter()->generateApiWidgetUrl('api_content_webview',array(
                 'view' => '/team/create',
                 'apikey' => CR_API_KEY,
                 'user_id' => $userId
                 )),
            );
             
             $playerCreateLink =  $this->getRouter()->link('api_content_team_player_create',array('apikey' => CR_API_KEY,'user_id' => $userId));
             $response['home'][] = array(
                'name' => $translator->translate('mobileApp.floatMenu.createPlayer'),
                'icon' => 'player',
                'url' =>  WEB_DOMAIN. $this->getRouter()->generateApiWidgetUrl('api_content_webview',array(
                 'view' => $playerCreateLink,
                 'apikey' => CR_API_KEY,
                 'user_id' => $userId
                 )),
            );
             
            $createEventLink =  $this->getRouter()->link('api_content_add_event_team_choice',array('apikey' => CR_API_KEY,'user_id' => $userId));
            $response['home'][] = array(
                'name' => $translator->translate('mobileApp.floatMenu.createEvent'),
                'icon' => 'events',
                'url' =>  WEB_DOMAIN. $this->getRouter()->generateApiWidgetUrl('api_content_webview',array(
                 'view' => $createEventLink,
                 'apikey' => CR_API_KEY,
                 'user_id' => $userId
                 )),
            );
            
            $response['home'][] = array(
                'name' => $translator->translate('mobileApp.floatMenu.addScore'),
                'icon' => 'score',
                'target' => 'score',
            );
             
             
             
             
             
            $response['home'][] = array(
                'name' => $translator->translate('mobileApp.floatMenu.scouting'),
                'icon' => 'search',
                'url' =>  WEB_DOMAIN. $this->getRouter()->generateApiWidgetUrl('api_content_webview',array(
                    'view' => '/scouting',
                    'apikey' => CR_API_KEY,
                    'user_id' => $userId
                    )),
            );
            $response['home'][] = array(
                'name' => $translator->translate('mobileApp.floatMenu.tournament'),
                'icon' => 'trophy',
                 'url' =>  WEB_DOMAIN. $this->getRouter()->generateApiWidgetUrl('api_content_webview',array(
                 'view' => '/tournament',
                 'apikey' => CR_API_KEY,
                 'user_id' => $userId
                 )),
            );
            $response['events'] = $response['home'];
            //$response['attendance'] = $response['home'];
            //$response['payments'] = $response['home'];
            $response['score']= $response['home'];
            //$response['results'] = $response['home'];
            $response['stats'] = $response['home'];
            //$response['stat_team'] =$response['home'];
           
            


            return $this->asJson(array('status' => 'SUCCESS', 'data' => $response));
        }
        else
        {
            return $this->asJson(array('status' => 'ERROR', 'error_message' => 'GET METHOD IS REQUIRED, NOT ' . $this->getRequest()->getMethod()));
        }
    }
    
    
    public function tournamentAction()
    {
        if ($this->getRequest()->getMethod() == 'GET')
        {
            $html = $this->render('Coachrufus\Api\ApiModule:Content:tournament.php', array())->getContent();

            $response = array();
            $response['html'] = $html;
            $response['backlink'] = $this->getRouter()->link('api_content_home');
            $response['filters'] = '';

            return $this->asJson(array('status' => 'SUCCESS', 'data' => $response));
        }
        else
        {
            return $this->asJson(array('status' => 'ERROR', 'error_message' => 'GET METHOD IS REQUIRED, NOT ' . $this->getRequest()->getMethod()));
        }
    }
    
     public function chatAction()
    {
        if ($this->getRequest()->getMethod() == 'GET')
        {
            $html = $this->render('Coachrufus\Api\ApiModule:Content:chat.php', array())->getContent();

            $response = array();
            $response['html'] = $html;
            $response['backlink'] = $this->getRouter()->link('api_content_home');
            return $this->asHtml( $this->getWebviewContent($html));

        }
        else
        {
            return $this->asJson(array('status' => 'ERROR', 'error_message' => 'GET METHOD IS REQUIRED, NOT ' . $this->getRequest()->getMethod()));
        }
    }
    
    public function notifySetupAction()
    {
        if ($this->getRequest()->getMethod() == 'GET' or 'AJAX_GET' == $this->getRequest()->getMethod())
        {
           
            if(false == $this->checkApiKey())
            {
                return $this->unvalidApiKeyResult();
            }
            $user = $this->createUserFromRequest();
            
            $teamManager = ServiceLayer::getService('TeamManager');
           
          
            $settings = json_decode($user->getNotifySettings(), true);

            $emailNotifyCodes = array('event_invitation', 'event_summary');
            
            $teams = $teamManager->getUserTeams($user);
            
             $html = $this->render('Coachrufus\Api\ApiModule:Content:notifySetup.php', array(
                        'user_id' => $user->getId(),
                        'settings' => $settings,
                        'emailNotifyCodes' => $emailNotifyCodes,
                        'teams' => $teams
                    ))->getContent();
            
            if(null != $this->getRequest()->get('w'))
            {
                return $this->asHtml($this->getWebviewContent1($html));
            }
            else
            {
                return $this->asHtml($this->getWebviewContent($html));
            }


           
            
        }
        else
        {
            return $this->asJson(array('status' => 'ERROR', 'error_message' => 'GET METHOD IS REQUIRED, NOT ' . $this->getRequest()->getMethod()));
        }
    }

    public function notifyAction()
    {
        if ($this->getRequest()->getMethod() == 'GET' or 'AJAX_GET' == $this->getRequest()->getMethod())
        {
            $router = $this->getRouter();

            if(false == $this->checkApiKey())
            {
                return $this->unvalidApiKeyResult();
            }
            $user = $this->createUserFromRequest();

            $manager = ServiceLayer::getService('SystemNotificationManager');
            $request = $this->getRequest();

            if (null != $request->get('ba') && 'allread' == $request->get('ba'))
            {
                     $manager->userBulkAction($user,'allread');
            }

            if (null != $request->get('mid'))
            {

                $message = $manager->getMessageById($request->get('mid'));
                $message->setStatus('fullread');
                $manager->saveMessage($message);


                $link = WEB_DOMAIN . $router->generateApiWidgetUrl('api_content_webview', array(
                            'apikey' => CR_API_KEY,
                            'user_id' => $user->getId(),
                            'view' => $message->getLink(),
                                )
                        );
                $request->redirect($link);
            }
            
            $start = 0;
            $length = 5;
            if(null != $request->get('start'))
            {
                $start = $request->get('start');
            }
            $listCritera = array('length' =>$length,'from' => $start);
            $list = $manager->getNoticationList($user, $listCritera);
            $stringUtil = new \Core\Types\StringUtils();

            
            if('only_items' == $request->get('mode'))
            {
                $html = '';
                foreach($list->getItems() as $notify)
                {
                    $html .= $this->render('Coachrufus\Api\ApiModule:Content:_notify_row.php', array('notify' => $notify,'stringUtil' => $stringUtil,'user' => $user))->getContent();
                }

                $response = array();
                $response['html'] = $html;
                if($html == '')
                {
                    $response['nextStart'] = 'end';
                }
                else
                {
                    $response['nextStart'] = $length+$start;
                }
                

                return $this->asJson($response);
            }
            else
            {
                $html = $this->render('Coachrufus\Api\ApiModule:Content:notify.php', array(
                        'list' => $list,
                        'stringUtil' => $stringUtil,
                        'user' => $user,
                        'nextStart' => $length
                    ))->getContent();

                $response = array();
                $response['html'] = $html;
                $response['backlink'] = $this->getRouter()->link('api_content_home');
            
                return $this->asHtml($this->getWebviewContent($html));
            }
            
        }
        elseif($this->getRequest()->getMethod() == 'POST')
        {
            var_dump($_FILES);
            /*
            $pushMessage = new \CR\Message\Model\PushMessage();
            $pushManager = \Core\ServiceLayer::getService('PushManager');
            $data = $this->getRequest()->getPost();
            
            $pushMessage->setRecipientId(1566);

              $pushMessage->setSubject(array(
                        'sk' => 'Nová správa',
                        'en' => 'New Message'
                    ));

               $pushMessage->setBody(array(
                      'sk' =>' test ',
                      'en' => 'test',
                      ));
               
               $pushMessage->setData($data);
               


            $response = $pushManager->sendMessage($pushMessage,'notifications');
            var_dump($response);
             * 
             */

        }
        else
        {
            return $this->asJson(array('status' => 'ERROR', 'error_message' => 'GET METHOD IS REQUIRED, NOT ' . $this->getRequest()->getMethod()));
        }
    }
    
 
    

}
