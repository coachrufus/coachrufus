<?php

namespace Coachrufus\Api\Controller;

use Core\ServiceLayer;
use Core\Types\DateTimeEx;
use DateTime;
use const APPLICATION_PATH;
use function t_dump;

class HpWallTestController extends TestController {

    public function likeUpdateAction()
    {
        $this->setup();
        $url = '/api/hpwall/post-like';
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['pid'] = 28;
        $data['t'] = 'l';

        $response = $this->call($url, 'GET', $data);
        $list['LIKE'] = array('response' => $response, 'url' => urldecode($this->buildCallUrl($url, 'GET', $data)), 'params' => $data);


        $uData = $data;
        $uData['t'] = 'u';
        $response = $this->call($url, 'GET', $uData);
        $list['UNLIKE'] = array('response' => $response, 'url' => urldecode($this->buildCallUrl($url, 'GET', $uData)), 'params' => $uData);


        return $this->render('Coachrufus\Api\ApiModule:test:result.php', array(
                    'url' => urldecode($this->buildCallUrl($url, 'GET', $data)),
                    'list' => $list
        ));
    }

    public function postAction()
    {
        $this->setup();
        $url = '/api/hpwall/post';

        $list = array();

        //success GET
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';

        //GET
        $getData = $data;
        $getData['author_id'] = '3';
        $response = $this->call($url, 'GET', $getData);
        $list['GET'] = array('response' => $response, 'url' => urldecode($this->buildCallUrl($url, 'GET', $getData)), 'params' => $getData);

        //POST
        $postData = $data;
        $postData['author_id'] = '3';
        $postData['body'] = 'random text';
        $postData['status'] = 'proposal';
        $postData['team_id'] = '3';
        $postResponse = $this->call($url, 'POST', $postData);
        $list['POST'] = array('response' => $postResponse, 'params' => $postData);

        //PUT
        $putData = $data;
        $putData['id'] = $postResponse['recordId'];
        $putData['body'] = 'random text update';
        $putData['status'] = 'proposal';
        $putData['team_id'] = '3';

        $putResponse = $this->call($url, 'PUT', $putData);
        $list['PUT'] = array('response' => $putResponse, 'params' => $putData);

        //DELETE
        //$deleteData['id'] = $postResponse['recordId'];
        $deleteResponse = $this->call($url . '/' . $postResponse['recordId'], 'DELETE');
        $list['DELETE'] = array('response' => $deleteResponse, 'url' => urldecode($this->buildCallUrl($url . '/' . $postResponse['recordId'], 'DELETE')));

        return $this->render('Coachrufus\Api\ApiModule:test:result.php', array(
                    'url' => urldecode($this->buildCallUrl($url, 'GET', $data)),
                    'list' => $list
        ));
    }

    private function generateLineupImg()
    {
        $hpWallManager = ServiceLayer::getService('HpWallManager');
        $img = $hpWallManager->generateLineupImg(1731);
        return $img;
    }

    private function generatePersonalPerformanceUpImg()
    {
        $hpWallManager = ServiceLayer::getService('HpWallManager');
        $teamEventManager = ServiceLayer::getService('TeamEventManager');
        $teamStatHistoryRepo = ServiceLayer::getService('TeamStatHistoryRepository');
        $playerStatManager = ServiceLayer::getService('PlayerStatManager');
        $teamManager = ServiceLayer::getService('TeamManager');
        $teamPlayer = $teamManager->findTeamPlayerById(743);
        $team = $teamManager->findTeamById(3);
        $seasonManager = ServiceLayer::getService('TeamSeasonManager');
        $actualSeason = $seasonManager->getTeamCurrentSeason($team);
        $seasonId = $actualSeason->getId();
        $currentSeason = $seasonManager->getSeasonById($seasonId);

        $statFrom = new DateTimeEx($currentSeason->getStartDate());
        $statTo = new DateTimeEx($currentSeason->getEndDate());
        $teamPlayersStats = $playerStatManager->getTeamPlayerStat($teamPlayer, $statFrom, $statTo);
        //t_dump($teamPlayersStats);

        $lineupPlayers = $teamEventManager->getEventLineupPlayers($existLineup);

        $teamMatchLineupRepository = ServiceLayer::getService('TeamMatchLineupRepository');
        $previewMatch = $teamMatchLineupRepository->findPlayerNearestPreviewMatch(new DateTime(), $teamPlayer->getId(), 2);


        //$matchStats = $playerStatManager->getMatchPlayersStat($previewMatch);

        t_dump($previewMatch);
        exit;



        //t_dump($teamPlayer);
        $img = $hpWallManager->generatePersonalPerformanceImg($teamPlayer, $teamPlayersStats, 'down');
        return $img;
    }

    public function testGeneratePersonalPerformancePost()
    {
        $hpWallManager = ServiceLayer::getService('HpWallManager');
        $eventManager = ServiceLayer::getService('TeamEventManager');
        $teamManager = ServiceLayer::getService('TeamManager');
        $team = $teamManager->findTeamById(1000);
        $teamPlayer = $teamManager->findTeamPlayerById(689);


        $event = $eventManager->findEventById(1225);
        $event->setCurrentDate(new \DateTime('2018-10-15 20:00:00'));


        $image = $hpWallManager->generatePersonalPerformancePost($teamPlayer, $team, $event);

        return $image;
    }

    private function testGenerateTeamLadderImgPost()
    {
        $hpWallManager = ServiceLayer::getService('HpWallManager');
        $teamManager = ServiceLayer::getService('TeamManager');
        $eventManager = ServiceLayer::getService('TeamEventManager');
        $team = $teamManager->findTeamById(1000);
        $teamPlayer = $teamManager->findTeamPlayerById(689);

        $event = $eventManager->findEventById(1225);
        $event->setCurrentDate(new \DateTime('2018-10-01 20:00:00'));

        $image = $hpWallManager->generateTeamLadderImgPost($team, $teamPlayer, $event);
        return $image;
    }

    private function testGenerateTeamLadderImg()
    {
        $hpWallManager = ServiceLayer::getService('HpWallManager');
        $teamManager = ServiceLayer::getService('TeamManager');
        $team = $teamManager->findTeamById(1572);
        $teamPlayer = $teamManager->findTeamPlayerById(47572);
        $image = $hpWallManager->generateTeamLadderImg($team, $teamPlayer);
        file_put_contents(PUBLIC_DIR . '/img/hpWallPosts/test.png', $image);
        $img = '/img/hpWallPosts/test.png';


        return $img;
    }

    public function testGenerateTeamAfterMatchImgPost()
    {
        $eventManager = ServiceLayer::getService('TeamEventManager');
        $existLineup = $eventManager->getLineupById(1800);
        $hpWallManager = ServiceLayer::getService('HpWallManager');
        $hpWallManager->generateTeamAfterMatchImgPost($existLineup);
    }

    public function testGenerateDistanceChangeImg()
    {
        $hpWallManager = ServiceLayer::getService('HpWallManager');
        $teamManager = ServiceLayer::getService('TeamManager');
        $team = $teamManager->findTeamById(1000);
        $targetPlayer = $teamManager->findTeamPlayerById(689);
        $distancePlayer = $teamManager->findTeamPlayerById(579);
        $type = 'lowerPlayerDistanceBigger';
        $image = $hpWallManager->generateDistanceChangeImg($targetPlayer, 55, $distancePlayer, 66, $type);


        file_put_contents(PUBLIC_DIR . '/img/hpWallPosts/test.png', $image);
        $img = '/img/hpWallPosts/test.png';


        return $img;
    }

    public function testGenerateDistanceChangePost()
    {
        $teamManager = ServiceLayer::getService('TeamManager');
        $eventManager = ServiceLayer::getService('TeamEventManager');

        $hpWallManager = ServiceLayer::getService('HpWallManager');
        $teamPlayerId = 576;
        $teamPlayer = $teamManager->findTeamPlayerById($teamPlayerId);
        $team = $teamManager->findTeamById(1000);

        $event = $eventManager->findEventById(1225);
        $event->setCurrentDate(new \DateTime('2019-03-18 19:15:00'));

        $hpWallManager->generateDistanceChangePost($teamPlayer, $team, $event);
    }
    
    public function testGeneratePositionChangeImg()
    {
        $hpWallManager = ServiceLayer::getService('HpWallManager');
        $teamManager = ServiceLayer::getService('TeamManager');
        $team = $teamManager->findTeamById(1000);
        $targetPlayer = $teamManager->findTeamPlayerById(585);
        $distancePlayer = $teamManager->findTeamPlayerById(621);
        $type = 'down';
        $image = $hpWallManager->generatePositionChangeImg($targetPlayer, 55, $distancePlayer, 66, $type);

        file_put_contents(PUBLIC_DIR . '/img/hpWallPosts/test.png', $image);
        $img = '/img/hpWallPosts/test.png';

        return $img;
    }
    
    public function testGeneratePositionChangePost()
    {
        $teamManager = ServiceLayer::getService('TeamManager');
        $eventManager = ServiceLayer::getService('TeamEventManager');
        $hpWallManager = ServiceLayer::getService('HpWallManager');
        
        /*
        $notificationManager = ServiceLayer::getService('SystemNotificationManager');
        $upcomingEvents = $eventManager->findUpcomingEvents(1);
        $time = strtotime('+4 hours');
        $currentTime = date('H-i',$time);
        foreach($upcomingEvents[date('Y-m-d')] as $event)
        {
            if($currentTime == $event->getCurrentDate()->format('H-i') or true )
            {

                $team = $teamManager->findTeamById($event->getTeamId());
                $teamPlayers = $teamManager->getTeamPlayers($team->getId());
                foreach($teamPlayers as $teamPlayer)
                {
                    if($teamPlayer->getPlayerId() != null)
                    {
                        $hpWallManager->generatePositionChangePost($teamPlayer, $team);
                        $notificationManager->sendGeneratedPersonalPostPushNotify($teamPlayer);
                    }
                }
            }
        }
        
        exit;
         * 
         */
        //today events

        
        $teamPlayerId = 621;
        $teamPlayer = $teamManager->findTeamPlayerById($teamPlayerId);
        $team = $teamManager->findTeamById(1000);

        //$event = $eventManager->findEventById(194);
        //$event->setCurrentDate(new \DateTime('2018-11-20 19:15:00'));

        $hpWallManager->generatePositionChangePost($teamPlayer,null);
    }

    public function postGenerateAction()
    {

        //$img = $this->testGenerateDistanceChangePost();
        //$img = $this->testGenerateTeamLadderImg();
        $img = $this->testGenerateDistanceChangePost();

        return $this->render('Coachrufus\Api\ApiModule:test:post_generate.php', array(
                    'img' => $img
        ));
    }

}
