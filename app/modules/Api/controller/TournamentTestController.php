<?php

namespace Coachrufus\Api\Controller;

use Core\ServiceLayer;
use Core\Types\DateTimeEx;
use DateTime;
use const APPLICATION_PATH;
use function t_dump;

class TournamentTestController extends TestController {

    public function detailAction()
    {
        $this->setup();
        $url = '/api/tournament/detail';
        
        $list = array();

        //success
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['tid'] = '1434';
        $response = $this->getResponse($url,'GET',$data);
        $list['TEAMS'] = array('response' => $response,'params' => $data,'url' => urldecode($this->buildCallUrl($url,'GET',$data)));
        
    
        //bad method
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['tid'] = '1434';
        $response4 = $this->call($url,'POST',$data);
        $list['BAD_METHOD'] = array('response' => $response4,'params' => $data);

        return $this->render('Coachrufus\Api\ApiModule:test:result.php', array(
                    'url' => $this->apiUrl.$url,
                    'list' => $list
        ));
    }
    
     public function currentTimelineEventAction()
    {
        $this->setup();
        $url = '/api/tournament/live/currentTimelineEvent';
        
        $list = array();

        //success
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['tid'] = '1442';
        $response = $this->getResponse($url,'GET',$data);
        $list['TEAMS'] = array('response' => $response,'params' => $data,'url' => urldecode($this->buildCallUrl($url,'GET',$data)));
        
    
        //bad method
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['tid'] = '1442';
        $response4 = $this->call($url,'POST',$data);
        $list['BAD_METHOD'] = array('response' => $response4,'params' => $data);

        return $this->render('Coachrufus\Api\ApiModule:test:result.php', array(
                    'url' => $this->apiUrl.$url,
                    'list' => $list
        ));
    }
    
    
    public function statsAction()
    {
        $this->setup();
        $url = '/api/tournament/stats';
        
        $list = array();

        //success
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['tid'] = '1442';
        $response = $this->getResponse($url,'GET',$data);
        $list['TEAMS'] = array('response' => $response,'params' => $data,'url' => urldecode($this->buildCallUrl($url,'GET',$data)));
        
    
        //bad method
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['tid'] = '1442';
        $response4 = $this->call($url,'POST',$data);
        $list['BAD_METHOD'] = array('response' => $response4,'params' => $data);

        return $this->render('Coachrufus\Api\ApiModule:test:result.php', array(
                    'url' => $this->apiUrl.$url,
                    'list' => $list
        ));
    }
    
     public function playersStatsAction()
    {
        $this->setup();
        $url = '/api/tournament/players-stats';
        
        $list = array();

        //success
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['tid'] = '1442';
        $response = $this->getResponse($url,'GET',$data);
        $list['TEAMS'] = array('response' => $response,'params' => $data,'url' => urldecode($this->buildCallUrl($url,'GET',$data)));
        
    
        //bad method
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['tid'] = '1442';
        $response4 = $this->call($url,'POST',$data);
        $list['BAD_METHOD'] = array('response' => $response4,'params' => $data);

        return $this->render('Coachrufus\Api\ApiModule:test:result.php', array(
                    'url' => $this->apiUrl.$url,
                    'list' => $list
        ));
    }
    
    public function subscribeUserEventAction()
    {
        $this->setup();
        $url = '/api/tournament/events-subscribe';
        
        $list = array();

        //success
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['tid'] = '1442';
        $data['lineup_id'] = '10';
        $response = $this->getResponse($url,'POST',$data);
        $list['TEAMS'] = array('response' => $response,'params' => $data,'url' => urldecode($this->buildCallUrl($url,'GET',$data)));
        
    
        //bad method
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['tid'] = '1442';
         $data['lineup_id'] = '10';
        $response4 = $this->call($url,'GET',$data);
        $list['BAD_METHOD'] = array('response' => $response4,'params' => $data);

        return $this->render('Coachrufus\Api\ApiModule:test:result.php', array(
                    'url' => $this->apiUrl.$url,
                    'list' => $list
        ));
    }
    
    public function teamsAction()
    {
        $this->setup();
        $url = '/api/tournament/teams';
        
        $list = array();

        //success
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['tid'] = '1434';
        $response = $this->getResponse($url,'GET',$data);
        $list['TEAMS'] = array('response' => $response,'params' => $data,'url' => urldecode($this->buildCallUrl($url,'GET',$data)));
        
    
        //bad method
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['tid'] = '1434';
        $response4 = $this->call($url,'POST',$data);
        $list['BAD_METHOD'] = array('response' => $response4,'params' => $data);

        return $this->render('Coachrufus\Api\ApiModule:test:result.php', array(
                    'url' => $this->apiUrl.$url,
                    'list' => $list
        ));
    }
    
    public function teamsPlayersAction()
    {
        $this->setup();
        $url = '/api/tournament/teams/players';
        
        $list = array();

        //success
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['tid'] = '3';
        $response = $this->getResponse($url,'GET',$data);
        $list['TEAMS'] = array('response' => $response,'params' => $data,'url' => urldecode($this->buildCallUrl($url,'GET',$data)));
        
    
        //bad method
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['tid'] = '3';
        $response4 = $this->call($url,'POST',$data);
        $list['BAD_METHOD'] = array('response' => $response4,'params' => $data);

        return $this->render('Coachrufus\Api\ApiModule:test:result.php', array(
                    'url' => $this->apiUrl.$url,
                    'list' => $list
        ));
    }
    
    public function eventsAction()
    {
        $this->setup();
        $url = '/api/tournament/events';
        
        $list = array();

        //success
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['tid'] = '1434';
        $response = $this->getResponse($url,'GET',$data);
        $list['TEAMS'] = array('response' => $response,'params' => $data,'url' => urldecode($this->buildCallUrl($url,'GET',$data)));
        
    
        //bad method
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['tid'] = '1434';
        $response4 = $this->call($url,'POST',$data);
        $list['BAD_METHOD'] = array('response' => $response4,'params' => $data);

        return $this->render('Coachrufus\Api\ApiModule:test:result.php', array(
                    'url' => $this->apiUrl.$url,
                    'list' => $list
        ));
    }
    
    public function eventsDetailAction()
    {
        $this->setup();
        $url = '/api/tournament/events/1929';
        
        $list = array();

        //success
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $response = $this->getResponse($url,'GET',$data);
        $list['TEAMS'] = array('response' => $response,'params' => $data,'url' => urldecode($this->buildCallUrl($url,'GET',$data)));
        
    
        //bad method
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['tid'] = '1434';
        $response4 = $this->call($url,'POST',$data);
        $list['BAD_METHOD'] = array('response' => $response4,'params' => $data);

        return $this->render('Coachrufus\Api\ApiModule:test:result.php', array(
                    'url' => $this->apiUrl.$url,
                    'list' => $list
        ));
    }
    
    public function liveEventsAction()
    {
        $this->setup();
        $url = '/api/tournament/live/events';
        
        $list = array();

        //success
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['tid'] = '1434';
        $response = $this->getResponse($url,'GET',$data);
        $list['TEAMS'] = array('response' => $response,'params' => $data,'url' => urldecode($this->buildCallUrl($url,'GET',$data)));
        
    
        //bad method
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['tid'] = '1434';
        $response4 = $this->call($url,'POST',$data);
        $list['BAD_METHOD'] = array('response' => $response4,'params' => $data);

        return $this->render('Coachrufus\Api\ApiModule:test:result.php', array(
                    'url' => $this->apiUrl.$url,
                    'list' => $list
        ));
    }

   
}
