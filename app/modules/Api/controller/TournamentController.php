<?php

namespace Coachrufus\Api\Controller;

use Core\GUID;
use Core\RestApiController;
use Core\ServiceLayer;
use User\Handler\StepRegisterHandler;

class TournamentController extends RestApiController {

    public function subscribeUserEventAction()
    {
        if ($this->getRequest()->getMethod() == 'POST')
        {
            $postData = $this->getRequest()->getPost();
            $userId = $postData['user_id'];
            $lineupId = $postData['lineup_id'];
            $action = $postData['action'];
            
             if(false == $this->checkApiKey($postData['apikey']))
            {
                return $this->unvalidApiKeyResult();
            }
            $manager = ServiceLayer::getService('TournamentManager');
            
           
            if($action == 'subscribe')
            {
                 $manager->subscribeUserEvent($userId,$lineupId);
            }
            
            if($action == 'unsubscribe')
            {
                 $manager->unsubscribeUserEvent($userId,$lineupId);
            }
            
            
            
            return $this->asJson(array('status' => 'SUCCESS', 'data' => ''));
        }
        else
        {
            return $this->asJson(array('status' => 'ERROR', 'error' => 'GET METHOD IS REQUIRED, NOT ' . $this->getRequest()->getMethod()));
        }
    }
    
    public function statsAction()
    {
        if ($this->getRequest()->getMethod() == 'GET')
        {
            if (false == $this->checkApiKey())
            {
                return $this->unvalidApiKeyResult();
            }
            $manager = ServiceLayer::getService('TournamentManager');
            $ladder = $manager->getTournamentLadder($this->getRequest()->get('tid'));
            return $this->asJson(array('status' => 'SUCCESS', 'data' => $ladder));
        }
        else
        {
            return $this->asJson(array('status' => 'ERROR', 'error' => 'GET METHOD IS REQUIRED, NOT ' . $this->getRequest()->getMethod()));
        }
    }
    
    public function detailAction()
    {
        if ($this->getRequest()->getMethod() == 'GET')
        {
            if (false == $this->checkApiKey())
            {
                return $this->unvalidApiKeyResult();
            }
            $userId =  $this->getRequest()->get('user_id');

             
            $repository = ServiceLayer::getService('TournamentRepository');
            $tournament = $repository->find($this->getRequest()->get('tid'));
            $sportManager = \Core\ServiceLayer::getService('SportManager');
            $sportChoices = $sportManager->getSportFormChoices();

            $data = $repository->convertToArray($tournament);
            $data['share_link'] =  $this->getRouter()->link('tournament_share_link', array('hash' => $tournament->getShareLinkHash(),'lt' => 'mal'));
            $data['sport_name'] =  $tournament->getSportName();
            
            $addressParts = array();
            $addressParts[] = $tournament->getAddressCity();
            $addressParts[] = $tournament->getAddressStreet();
            $addressParts[] = $tournament->getAddressStreetNum();
            $addressParts[] = $tournament->getAddressZip();
            
            $data['address'] =  implode(array_filter($addressParts));
            $data['p'] = $repository->getTournamentUserPermission($tournament,$userId);
            
            
            $data['sport_name'] = $this->getTranslator()->translate($sportChoices[$tournament->getSportId()]) ;
            $data['gender_name'] = $this->getTranslator()->translate('tournament.public.detail.gender.'.$data['gender']) ;
            

            return $this->asJson(array('status' => 'SUCCESS', 'data' => $data));
        }
        else
        {
            return $this->asJson(array('status' => 'ERROR', 'error' => 'GET METHOD IS REQUIRED, NOT ' . $this->getRequest()->getMethod()));
        }
    }
    
   
    
    public function teamsAction()
    {
        if ($this->getRequest()->getMethod() == 'GET')
        {
            if (false == $this->checkApiKey())
            {
                return $this->unvalidApiKeyResult();
            }
            $repository = ServiceLayer::getService('TournamentRepository');

            $tournament = $repository->find($this->getRequest()->get('tid'));
            $teams = $repository->findTournamentTeams($tournament->getId(), array('status' => 'confirmed'));
            $teamIcons = array();
            foreach($teams as $team)
            {
                $teamIcons[$team->getId()]  = $team->getMidPhoto(); 
            }
            $list = $repository->createArrayList($teams);
            foreach($list as $key => $l)
            {
                $list[$key]['icon'] = $teamIcons[$l['id']];
            }

            //getMidPhoto()


            return $this->asJson(array('status' => 'SUCCESS', 'data' => $list));
        }
        else
        {
            return $this->asJson(array('status' => 'ERROR', 'error' => 'GET METHOD IS REQUIRED, NOT ' . $this->getRequest()->getMethod()));
        }
    }

    public function teamsPlayersAction()
    {
        if ($this->getRequest()->getMethod() == 'GET')
        {
            if (false == $this->checkApiKey())
            {
                return $this->unvalidApiKeyResult();
            }
            $teamManager = ServiceLayer::getService('TeamManager');
            $repository = ServiceLayer::getService('TeamRepository');
            $teamPlayers = $teamManager->getActiveTeamPlayers($this->getRequest()->get('tid'));

            $list = $repository->createArrayList($teamPlayers);
            
            $icons = array();
            foreach($teamPlayers as $teamPlayer)
            {
                $icons[$teamPlayer->getId()]  = $teamPlayer->getMidPhoto(); 
            }

            foreach($list as $key => $l)
            {
                $list[$key]['icon'] = $icons[$l['id']];
            }
            
            



            return $this->asJson(array('status' => 'SUCCESS', 'data' => $list));
        }
        else
        {
            return $this->asJson(array('status' => 'ERROR', 'error' => 'GET METHOD IS REQUIRED, NOT ' . $this->getRequest()->getMethod()));
        }
    }

    public function eventsAction()
    {
        if ($this->getRequest()->getMethod() == 'GET')
        {
            if (false == $this->checkApiKey())
            {
                return $this->unvalidApiKeyResult();
            }
            $tournamentRepository = ServiceLayer::getService('TournamentRepository');
            $repository = ServiceLayer::getService('EventRepository');
            $teamEventManager = ServiceLayer::getService('TeamEventManager');
            $statManager = ServiceLayer::getService('PlayerStatManager');

            $userId =  $this->getRequest()->get('user_id');
            
            $events = $repository->findBy(array('tournament_id' => $this->getRequest()->get('tid')));
            $list = $repository->createArrayList($events);
            
            $subscribedEventUsers = array();
            
            //lineups
            
            $eventsLineups = array();
            foreach($events as $event)
            {
                 $event->setCurrentDate($event->getStart());
                 $lineup = $teamEventManager->getEventLineup($event);
                 
                 if(null != $lineup)
                 {
                      $lineupData = $repository->convertToArray($lineup);
                 
                 $lineupData['match_result'] = $this->getMatchResuls($lineup);
                 
                 $lineupData['match_time'] = $this->getMatchTime($lineup);
                 
                 $eventsLineups[$event->getId()] = $lineupData;
                 
                 $subscribedEventUsers[$event->getId()] = $tournamentRepository->getSubscribedEventUsers($lineup->getId(),$userId);
                 }
                 
                 
                
  
            }
            
            //var_dump($subscribedEventUsers);exit;
            
             //subscribed notify users
            

           //teams
            $teams = $tournamentRepository->findTournamentTeams($this->getRequest()->get('tid'), array('status' => 'confirmed'));
            $teamIcons = array();
            foreach($teams as $team)
            {
                $teamIcons[$team->getId()]  = $team->getMidPhoto(); 
            }
            $teamList = $repository->createArrayList($teams);
            foreach($teamList as $key => $l)
            {
                $teamList[$l['id']] = $l;
                $teamList[$l['id']]['icon'] = $teamIcons[$l['id']];
            }
            
            foreach ($list as $key => $event)
            {
                $list[$key]['lineup'] = $eventsLineups[$event['id']];
                $list[$key]['team'] = $teamList[$event['team_id']];
                $list[$key]['opponent_team'] = $teamList[$event['opponent_team_id']];     
                
                if(array_key_exists($event['id'], $subscribedEventUsers) && array_key_exists('user_id', $subscribedEventUsers[$event['id']]) && $subscribedEventUsers[$event['id']]['user_id'] == $userId)
                {
                    $list[$key]['user_subscribed'] = 'yes';
                }
                else
                {
                    $list[$key]['user_subscribed'] = 'no';
                }
            }

            return $this->asJson(array('status' => 'SUCCESS', 'data' => $list));
        }
        else
        {
            return $this->asJson(array('status' => 'ERROR', 'error' => 'GET METHOD IS REQUIRED, NOT ' . $this->getRequest()->getMethod()));
        }
    }

    public function eventsDetailAction()
    {
        if ($this->getRequest()->getMethod() == 'GET')
        {
            if (false == $this->checkApiKey())
            {
                return $this->unvalidApiKeyResult();
            }
            $teamEventManager = ServiceLayer::getService('TeamEventManager');
            
            
            $event = $teamEventManager->findEventById($this->getRequest()->get('event_id'));
            $event->setCurrentDate($event->getStart());
            
            
            $teamManager = ServiceLayer::getService('TeamManager');
            $teamPlayers = $teamManager->getActiveTeamPlayers($event->getTeamId());
            $icons = array();
            foreach($teamPlayers as $teamPlayer)
            {
                $icons[$teamPlayer->getId()]  = $teamPlayer->getMidPhoto(); 
            }
            $opponnentTeamPlayers = $teamManager->getActiveTeamPlayers($event->getOpponentTeamId());
            foreach($opponnentTeamPlayers as $teamPlayer)
            {
                $icons[$teamPlayer->getId()]  = $teamPlayer->getMidPhoto(); 
            }
            
            $lineup = $teamEventManager->getEventLineup($event);
            $lineupPlayers = $teamEventManager->getEventLineupPlayers($lineup);
            $list = array();
            foreach($lineupPlayers as $lineupPlayer)
            {
                $list[$lineupPlayer->getlineupPosition()][] = array(
                    'name' =>  $lineupPlayer->getPlayerName(),
                    'id' => $lineupPlayer->getId(),
                    'icon' => $icons[$lineupPlayer->getTeamPlayerId()]
                );
            }
           


            return $this->asJson(array('status' => 'SUCCESS', 'data' => $list));
        }
        else
        {
            return $this->asJson(array('status' => 'ERROR', 'error' => 'GET METHOD IS REQUIRED, NOT ' . $this->getRequest()->getMethod()));
        }
    }
    
     public function liveEventsAction()
    {
        if ($this->getRequest()->getMethod() == 'GET')
        {
            if (false == $this->checkApiKey())
            {
                return $this->unvalidApiKeyResult();
            }
            $tournamentRepository = ServiceLayer::getService('TournamentRepository');
            $repository = ServiceLayer::getService('EventRepository');
            $teamEventManager = ServiceLayer::getService('TeamEventManager');
           
            $events = $repository->findBy(array('tournament_id' => $this->getRequest()->get('tid')));
            //$list = $repository->createArrayList($events);
            
            //lineups
            $eventsLineups = array();
            foreach($events as $event)
            {
                $event->setCurrentDate($event->getStart());
                $lineup = $teamEventManager->getEventLineup($event);
                
                
                
                if($lineup->getStatus() == 'paused' or $lineup->getStatus() == 'running')
                {
                    $lineupData = $repository->convertToArray($lineup);
                    $lineupData['match_result'] = $this->getMatchResuls($lineup);
                    $lineupData['match_time'] = $this->getMatchTime($lineup);
                    $eventsLineups[$event->getId()] = $lineupData;
                    $list[] = $repository->convertToArray($event);
                }
            }

           //teams
            $teams = $tournamentRepository->findTournamentTeams($this->getRequest()->get('tid'), array('status' => 'confirmed'));
            $teamIcons = array();
            foreach($teams as $team)
            {
                $teamIcons[$team->getId()]  = $team->getMidPhoto(); 
            }
            $teamList = $repository->createArrayList($teams);
            foreach($teamList as $key => $l)
            {
                $teamList[$l['id']] = $l;
                $teamList[$l['id']]['icon'] = $teamIcons[$l['id']];
            }
            
            foreach ($list as $key => $event)
            {
                $list[$key]['lineup'] = $eventsLineups[$event['id']];
                $list[$key]['team'] = $teamList[$event['team_id']];
                $list[$key]['opponent_team'] = $teamList[$event['opponent_team_id']];
            }



            return $this->asJson(array('status' => 'SUCCESS', 'data' => $list));
        }
        else
        {
            return $this->asJson(array('status' => 'ERROR', 'error' => 'GET METHOD IS REQUIRED, NOT ' . $this->getRequest()->getMethod()));
        }
    }
    
    private function getMatchTime($lineup)
    {
        if($lineup->getStatus() == 'paused')
        {
            $matchTime = $lineup->getSecondsPast();

        }
        if($lineup->getStatus() == 'running')
        {
            $currentTime = new \DateTime();
            $matchTime = $currentTime->getTimestamp()-$lineup->getStartedAt()->getTimestamp();
        }
        $result =  array();
        $result['minutes'] = floor($matchTime/60);
        $result['seconds'] = $matchTime-(60*$result['minutes']);
        return $result;
    }
    
    private function getMatchResuls($lineup)
    {
        
        $statManager = ServiceLayer::getService('PlayerStatManager');
        $matchResultData = $statManager->getMatchResult($lineup);
        $matchResult = array('first_line' => 0, 'second_line' => 0);
        if (null != $matchResultData)
        {
            $matchResult['first_line'] = ($matchResultData['first_line'] == null) ? 0 :$matchResultData['first_line'];
            $matchResult['second_line'] = ($matchResultData['second_line'] == null) ? 0 : $matchResultData['second_line'];
            $matchResult['winner'] = 'none';

            if ($matchResult['first_line'] > $matchResult['second_line'])
            {
                $matchResult['winner'] = 'first_line';
            }

            if ($matchResult['first_line'] < $matchResult['second_line'])
            {
                $matchResult['winner'] = 'second_line';
            }
        }
        
        return $matchResult;

    }
    
      public function playersStatsAction()
    {
        if ($this->getRequest()->getMethod() == 'GET')
        {
            if (false == $this->checkApiKey())
            {
                return $this->unvalidApiKeyResult();
            }
            $manager = ServiceLayer::getService('TournamentManager');
            $ladder = $manager->getTournamentPlayersLadder($this->getRequest()->get('tid'));
            return $this->asJson(array('status' => 'SUCCESS', 'data' => $ladder));
        }
        else
        {
            return $this->asJson(array('status' => 'ERROR', 'error' => 'GET METHOD IS REQUIRED, NOT ' . $this->getRequest()->getMethod()));
        }
    }
    
    public function currentTimelineEventAction()
    {
         if ($this->getRequest()->getMethod() == 'GET' or 'AJAX_GET' == $this->getRequest()->getMethod())
        {
           
            $manager = ServiceLayer::getService('TournamentManager');
            $ladder = $manager->getTournamentHits($this->getRequest()->get('tid'));
            return $this->asJson(array('status' => 'SUCCESS', 'data' => $ladder));
        }
        else
        {
            return $this->asJson(array('status' => 'ERROR', 'error' => 'GET METHOD IS REQUIRED, NOT ' . $this->getRequest()->getMethod()));
        }
    }

}
