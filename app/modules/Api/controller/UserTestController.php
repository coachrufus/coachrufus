<?php

namespace Coachrufus\Api\Controller;

use Core\ServiceLayer;
use Core\Types\DateTimeEx;
use DateTime;



class UserTestController extends TestController {

    public function privacyAction()
    {
        $this->setup();
        $url = '/api/user/privacy/';
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['marketing_agreement'] = 1;

        $response = $this->call($url, 'POST', $data);
        $list['UPDATE'] = array('response' => $response, 'url' => urldecode($this->buildCallUrl($url, 'POST', $data)), 'params' => $data);

        return $this->render('Coachrufus\Api\ApiModule:test:result.php', array(
                    'url' => urldecode($this->buildCallUrl($url, 'POST', $data)),
                    'list' => $list
        ));
    }

   

}
