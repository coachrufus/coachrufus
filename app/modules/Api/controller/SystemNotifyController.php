<?php

namespace Coachrufus\Api\Controller;

use Core\GUID;
use Core\RestApiController;
use Core\ServiceLayer;
use User\Handler\StepRegisterHandler;

class SystemNotifyController extends RestApiController {

    public function unreadListAction()
    {
        if ($this->getRequest()->getMethod() == 'GET')
        {
            $data = $this->getRequest()->getGet();
             if(false == $this->checkApiKey())
            {
                return $this->unvalidApiKeyResult();
            }
            $user = $this->createUserFromRequest();

            
            if(null == $user)
            {
                  return $this->asJson(array('status' => 'ERROR', 'error_message' => 'BAD USER ID'));
            }

            $notifyManager = ServiceLayer::getService('SystemNotificationManager');
            $notifyInfo = $notifyManager->getNotificationsInfo($user);
            $notifications = $notifyManager->getUnreadSystemNotifications($user, 4);

            $list = array();
            foreach($notifications as $notify)
            {
                $list[$notify->getId()]['subject'] = $notify->getSubject(); 
                $list[$notify->getId()]['body'] = $notify->getBody(); 
                $list[$notify->getId()]['created_at'] = $notify->getCreatedAt(); 
            }

            return $this->asJson(array('status' => 'SUCCESS', 'data' => array('summary' => $notifyInfo ,'messages' => $list)));
        }
        else
        {
            return $this->asJson(array('status' => 'ERROR', 'error_message' => 'GET METHOD IS REQUIRED, NOT ' . $this->getRequest()->getMethod()));
        }
    }
    
    public function changeStatusAction()
    {
        if ($this->getRequest()->getMethod() == 'PUT')
        {
            $putData = array();
            parse_str(file_get_contents("php://input"),$putData);
            $messagesId = json_decode($putData['messages_id']);
            
             if(false == $this->checkApiKey($putData['apikey']))
            {
                return $this->unvalidApiKeyResult();
            }


            $notifyManager = ServiceLayer::getService('SystemNotificationManager');
            foreach($messagesId as $messageId)
            {
                $message = $notifyManager->getMessageById($messageId);
                
                if('read' == $putData['status'])
                {
                     $notifyManager->readMessage($message);
                }
                
                if('unread' == $putData['status'])
                {
                     $notifyManager->unreadMessage($message);
                }
                
               
            }
           

            return $this->asJson(array('status' => 'SUCCESS'));
        }
        else
        {
            return $this->asJson(array('status' => 'ERROR', 'error_message' => 'PUT METHOD IS REQUIRED, NOT ' . $this->getRequest()->getMethod()));
        }
    }
    
    public function setupAction()
    {
        if ($this->getRequest()->getMethod() == 'GET' or $this->getRequest()->getMethod() == 'AJAX_GET')
        {
            $data = $this->getRequest()->getGet();
            if(false == $this->checkApiKey())
            {
                return $this->unvalidApiKeyResult();
            }
            $user = $this->createUserFromRequest();

            $userRepository = ServiceLayer::getService('user_repository');

            if(null == $user)
            {
                  return $this->asJson(array('status' => 'ERROR', 'error_message' => 'BAD USER ID'));
            }
            $settings['push_notify'] = $data['push_notify'];
            $settings['email_notify'] = $data['email_notify'];
            $settings['team_notify'] = $data['team_notify'];

            $user->setNotifySettings(json_encode($settings));
            $userRepository->save($user);
            
            $security = ServiceLayer::getService('security');
            $security->reloadUser($user);
            
            exit;
            return $this->asJson(array('status' => 'SUCCESS'));
        }
        else
        {
            return $this->asJson(array('status' => 'ERROR', 'error_message' => 'GET METHOD IS REQUIRED, NOT ' . $this->getRequest()->getMethod()));
        }
    }

}
