<?php
namespace Coachrufus\Api\Controller;

use Core\RestApiController;
use Core\ServiceLayer;


class ScoutingController extends RestApiController {
	
	
     public function indexAction()
    {
        if ($this->getRequest()->getMethod() == 'GET')
        {
            $html = $this->render('Coachrufus\Api\ApiModule:Scouting:index.php', array())->getContent();

            $response = array();
            $response['html'] = $html;
            $response['backlink'] = $this->getRouter()->link('api_content_home');
            $response['filters'] = '';

            return $this->asJson(array('status' => 'SUCCESS', 'data' => $response));
        }
        else
        {
            return $this->asJson(array('status' => 'ERROR', 'error_message' => 'GET METHOD IS REQUIRED, NOT ' . $this->getRequest()->getMethod()));
        }
    }
    
    public function scoutingWidgetAction()
        {
            header("Access-Control-Allow-Origin: *");
            ob_start();
            include(MODUL_DIR.'/Api/view/Scouting/widget.js');
            $js = ob_get_contents();
            ob_end_clean();
            
            return $this->asJavascript($js);
        }
    
    
        public function sliderWidgetAction()
	{
            header("Access-Control-Allow-Origin: *");
            /*
            $request = ServiceLayer::getService('request');
            if(null != $request->get('lang'))
            {
                $_SESSION['app_lang'] = $request->get('lang');
                $translator = ServiceLayer::getService('translator');
                $translator->setLang($request->get('lang'));
            }
            */
            
            
            $manager = ServiceLayer::getService('ScoutingManager');
            $manager->setItemPerPage(20);
            $list = $manager->getLastScouting(array());
            
            $items = $list->getItems();
            $sections = array_chunk($items, 4);
            
             $html = $this->render('Coachrufus\Api\ApiModule:Scouting:sliderWidget.php', array(
                       'sections' => $sections,
                    ))->getContent();
             
             $response  = array();
             $response['html'] = $html;
             
             $css = $this->render('Coachrufus\Api\ApiModule:Scouting:sliderWidget.css.php', array(
                        'imgPath' => WEB_DOMAIN
                    ))->getContent();
             
             
             $response['css'] = $css;
             
            return $this->asJson($response);
             
            
            
           
	}
}