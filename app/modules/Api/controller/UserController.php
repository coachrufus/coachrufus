<?php
namespace Coachrufus\Api\Controller;

use Core\GUID;
use Core\RestApiController;
use Core\ServiceLayer;
use User\Handler\StepRegisterHandler;
class UserController extends RestApiController
{   
    public function privacyAction()
    {
        
        if ($this->getRequest()->getMethod() == 'POST')
        {
            if (false == $this->checkApiKey())
            {
                return $this->unvalidApiKeyResult();
            }
            $privacyRepo = \Core\ServiceLayer::getService('PlayerPrivacyRepository');
            $user = $this->createUserFromRequest();

            $request = $this->getRequest();
            $userPrivacy = new \Webteamer\Player\Model\PlayerPrivacy();
            $userPrivacy->setSection('gdpr_marketing_agreement');
            $userPrivacy->setUserGroup('coachrufus');
            $userPrivacy->setPlayerId($user->getId());
            $userPrivacy->setValue($request->get('marketing_agreement'));
            $privacyRepo->save($userPrivacy);


            $queueRepo = ServiceLayer::getService('QueueRepository');
            if (1 == $request->get('gdpr_marketing_agreement'))
            {
                $queueRepo->addRecord(
                        array(
                            'action_key' => 'user_id',
                            'value' => $user->getId(),
                            'action_type' => 'mailchimp_marketing_export'));
            }



            return $this->asJson(array('status' => 'SUCCESS'));
        }
        else
        {
            return $this->asJson(array('status' => 'ERROR', 'error' => 'GET METHOD IS REQUIRED, NOT ' . $this->getRequest()->getMethod()));
        }
    }

    public function registerAction()
    {
        if ($this->getRequest()->getMethod() == 'POST')
        {
            
            $postData = $this->getRequest()->getPost();
            if(null != $postData['password'])
            {
                $postData['password'] = $postData['pass'];
            }
            else
            {
                $postData['password'] = substr(sha1(time().rand(1,100).$postData['email']),0,8) ;
            }
            
            $postData['vop_agreement']  = '1';
            
            $registerHandler = new StepRegisterHandler();
            $registerHandler->setPostData($postData);
            $registerHandler->sanitizeData();
			
			  
            if($postData['facebook_id'] != null or $postData['google_id'] != null)
            {
              
                    $security = ServiceLayer::getService('security');
                    $user_repository = \Core\ServiceLayer::getService('user_repository');
                    $exist_user = $user_repository->findOneBy([
                        'email' => $postData['email']
                    ]);
                    
                    if(null !=$exist_user ) //exist user, login and update fb id
                    {
                        $security->authenticateUserEntity($exist_user);      
                        $userIdentity  = $security->getIdentity();
                        $user = $userIdentity->getUser();
                        
                        $user->setFacebookId($postData['facebook_id'] );
                        $user->setGoogleId($postData['google_id'] );
                        $user_repository->save($user);
                         return $this->asJson(array('status' => 'SUCCESS', 'data' => array('USER_ID' =>  $user->getId())));
                    }
                    else 
                    {
                          $validateResult = true;
                    }
        
                
            }
            else
            {
                 $validateResult = $registerHandler->validateStart();
            }
            
            
			
            //$validateResult = $registerHandler->validateStart();
            
            //var_dump($validateResult);
           if ($validateResult)
           {
                
                $activationGuid = GUID::create();
                $postData['activation_guid'] = $activationGuid;
                $postData['status'] = 1;
                $registerHandler->persistStartData($postData);
                
                $security = ServiceLayer::getService('security');
                $auth_result = $security->authenticateUser($postData['email'], $postData['password']);
                
                $queueRepo  = ServiceLayer::getService('QueueRepository');
                $queueRepo->addRecord(array('action_key' => 'user_id','value' => $auth_result['ID'],'action_type' => 'mailchimp_export'));

                //add user to email marketing
                /*
                $user = ServiceLayer::getService('user_repository')->findOneBy(array('id' => $auth_result['ID']));
                $emailMarketingManager = \Core\ServiceLayer::getService('EmailMarketingManager');
                $emailMarketingManager->createUser($user);
                $emailMarketingManager->addUserTag($user,'user_noteam');
                */
               return $this->asJson(array('status' => 'SUCCESS', 'data' => array('USER_ID' =>  $auth_result['ID'])));
            }
            else
            {
                 $errors = $registerHandler->getValidator('start')->getErrors();

                 foreach($errors as $key => $error)
                 {
                     $errors[$key] = $this->getTranslator()->translate($error,$postData['lang']);
                 }

                
                return $this->asJson(array('status' => 'ERROR','postData' => $postData, 'errors' => $errors));
            }
          
            
           
            /*
            $teamPlayerRepository = ServiceLayer::getService('TeamPlayerRepository');
            
            $this->checkRequestPermissions($data,$type = 'PLAYERS_CREATE');
            
            
            foreach ($data['players'] as $playerData)
            {
                $player = new \CR\Tournament\Model\TeamPlayer();
                $teamPlayerRepository->updateEntityFromArray($player, $playerData);
                $player->setTeamId($data['team_id']);
                $teamPlayerRepository->save($player);
            }
             * 
             */
        }
        else
        {
            return $this->asJson(array('status' => 'ERROR', 'error' => 'POST METHOD IS REQUIRED, NOT '.$this->getRequest()->getMethod()));
        }

    }
    
    
    
    public function loginAction()
    {
        $result = array();
        $result['params'] = $this->getRequestParams();
        if(false == $this->checkApiKey())
        {
            return $this->unvalidApiKeyResult();
        }
        
        $security = ServiceLayer::getService('security');
        $username = $this->getRequest()->get('username');
        $plain_password = $this->getRequest()->get('pass');
        $auth_result = $security->authenticateUser($username, $plain_password);
        
        if($auth_result['RESULT'] == 1 && $auth_result['MESSAGE'] == 'NOTE_LOGIN_SUCCESS')
        {
          
           $userIdentity  = $security->getIdentity();
           $user = $userIdentity->getUser();
        
           $user_data =  $this->buildUserData($user);
           $result['user_data'] =$user_data;
           $result['status'] = 'success';
           
        }
       
        
        if($auth_result['RESULT'] == 0)
        {
            $result['error_message'] = 'UNVALID LOGIN DATA';
            $result['status'] = 'error';
            
        }
       
        return $this->asJson($result);
    }
    
    public function  loginFbAction()
    {
        $result = array();
        $result['params'] = $this->getRequestParams();
        if(false == $this->checkApiKey())
        {
            return $this->unvalidApiKeyResult();
        }
        
        $security = ServiceLayer::getService('security');
        $identityProvider = $security->getIdentityProvider();
        $username = $this->getRequest()->get('username');
        $fbID = $this->getRequest()->get('fbid');
        
        $user = $identityProvider->findUserByUsername($username);
        
		
       if(null == $user)
		{
			$result['error_message'] = 'NOT EXIST USER';
            $result['status'] = 'error';
		}
        elseif($user->getFacebookId() == $fbID or $user->getFacebookId() == null)
        {
            if($user->getFacebookId() == null)
			{
				$user->setFacebookId($fbID);
				$repository = ServiceLayer::getService('user_repository');
				$repository->save($user);
			}
			
			
			$security->authenticateUserEntity($user);      
            $userIdentity  = $security->getIdentity();
            $user = $userIdentity->getUser();


           $user_data =  $this->buildUserData($user);
           $result['user_data'] =$user_data;
            $result['status'] = 'success';
        }
        else
        {
             $result['error_message'] = 'UNVALID LOGIN DATA';
             $result['status'] = 'error';
        }
        return $this->asJson($result);
    }
    
    public function  loginGoogleAction()
    {
        $result = array();
        $result['params'] = $this->getRequestParams();
        if(false == $this->checkApiKey())
        {
            return $this->unvalidApiKeyResult();
        }
        
        $security = ServiceLayer::getService('security');
        $identityProvider = $security->getIdentityProvider();
        $username = $this->getRequest()->get('username');
        $gID = $this->getRequest()->get('gid');
        
        $user = $identityProvider->findUserByUsername($username);
		
		//var_dump($user->getGoogleId());
		
		
        
		 if(null == $user)
		{
			$result['error_message'] = 'NOT EXIST USER';
            $result['status'] = 'error';
		}
		elseif(substr($user->getGoogleId(),0,20) == substr($gID,0,20) or $user->getGoogleId() == null)
        {
            $security->authenticateUserEntity($user);      
            $userIdentity  = $security->getIdentity();
            $user = $userIdentity->getUser();
			
			if($user->getGoogleId() == null)
			{
				$user->getGoogleId($gID);
				$repository = ServiceLayer::getService('user_repository');
				$repository->save($user);
			}

            $user_data =  $this->buildUserData($user);
            $result['user_data'] =$user_data;
            $result['status'] = 'success';
			
			//update gid
			if(strlen($gID) > 20)
			{
				
				$repository = ServiceLayer::getService('user_repository');
                $user->setGoogleId($gID);
                $repository->save($user);
                 
			}
			
        }
        else
        {
             $result['error_message'] = 'UNVALID LOGIN DATA';
             $result['status'] = 'error';
        }
        return $this->asJson($result);
    }
    
    public function authInfoAction()
    {
         $security = ServiceLayer::getService('security');
         $userIdentity  = $security->getIdentity();
         $user = $userIdentity->getUser();
         if(null != $user)
         {
           $result['auth_status'] = 'login';
           $result['user_data'] = array();
           $result['user_data']['id'] = $user->getId();
           $result['user_data']['authkey'] = $user->getPersistHash();
           $result['user_data']['facebook_id'] = $user->getFacebookId();
           $result['user_data']['google_id'] = $user->getGoogleId();
           $result['user_data']['username'] = $user->getEmail();
           $result['user_data']['name'] = $user->getName();
           $result['user_data']['surname'] = $user->getSurname();
           $result['user_data']['icon'] =  WEB_DOMAIN.'/player/icon?file='.$user->getPhoto();
           $result['user_data']['teamRoles'] = $userIdentity->getTeamRoles();
         }
         else
         {
              $result['auth_status'] = 'logout';
         }
          return $this->asJson($result);
    }
    
     public function userInfoAction()
    {
        $result = array();
        $teamManager = ServiceLayer::getService('TeamManager');
        $sportManager = ServiceLayer::getService('SportManager');
        $translator = ServiceLayer::getService('translator');
        $sportEnum = $sportManager->getSportFormChoices();
        $result['params'] = $this->getRequestParams();
        $uid = $result['params']['uid'];
        $security = ServiceLayer::getService('security');
        $user = $security->getIdentityProvider()->findUserById($uid);

        $marketingAgreement = $security->hasMarketingAgreements($user);



        if (null != $user)
        {
            $result['user_data'] = array();

            $result['user_data']['id'] = $user->getId();
            $result['user_data']['privacy']['marketingAgreement'] = $marketingAgreement;
            $result['user_data']['username'] = $user->getEmail();
            $result['user_data']['name'] = $user->getName();
            $result['user_data']['surname'] = $user->getSurname();
            $result['user_data']['icon'] = WEB_DOMAIN . '/player/icon?file=' . $user->getPhoto();
             $result['user_data']['lang'] = $user->getDefaultLang();
            //$result['user_data']['teams'] = array();
            $userTeams = $security->getIdentityProvider()->getUserTeams($user);
            $teamRoles = $security->getIdentity()->getTeamRoles();
            foreach ($userTeams as $userTeam)
            {
                $result['user_data']['teams'][] = array('id' => $userTeam->getId(), 'name' => $userTeam->getName(),'role' => $teamRoles[$userTeam->getId()]['role']);
            }

            $waitingTeams = $teamManager->getUserWaitingTeams($user);
            $result['user_data']['teamInvitations'] = array();
            foreach ($waitingTeams as $waitingTeam)
            {
                $teamPlayer = $waitingTeam['player'];
                $team = $waitingTeam['team'];
                $result['user_data']['teamInvitations'][] = array(
                    'teamName' => $team->getName(),
                    'sport' => $translator->translate($sportEnum[$team->getSportId()]),
                    'icon' => WEB_DOMAIN . $team->getMidPhoto(),
                    'acceptLink' => $this->getRouter()->link('team_member_invite', array('tp' => $teamPlayer->getId(), 'hash' => $team->getShareLinkHash(), 'h' => $teamPlayer->getHash())),
                    'refuseLink' => $this->getRouter()->link('team_member_invite_decline', array('tp' => $teamPlayer->getId(), 'hash' => $team->getShareLinkHash(), 'h' => $teamPlayer->getHash()))
                );
            }
            
            //info about unconfirmed players
            $result['user_data']['unconfirmedPlayers'] = array();
            foreach($userTeams as $userTeam)
            {
                if($teamRoles[$userTeam->getId()]['role'] == 'ADMIN')
                {
                    $teamPlayers =  $teamManager->getTeamPlayers($userTeam->getId());
                    foreach($teamPlayers as $teamPlayer)
                    {
                        if($teamPlayer->getStatus() == 'unconfirmed')
                        {
                            $result['user_data']['unconfirmedPlayers'][] = array(
                                'name' => $teamPlayer->getFullName(),
                                'teamName' => $userTeam->getName(),
                                 'icon  ' => WEB_DOMAIN . $teamPlayer->getMidPhoto(),
                                'acceptLink' => $this->getRouter()->link('api_team_change_player_status', array('apikey' => CR_API_KEY,'tp' => $teamPlayer->getId(), 'h' => $teamPlayer->getHash())),
                                'refuseLink' => $this->getRouter()->link('api_team_change_player_status', array('apikey' => CR_API_KEY,'tp' => $teamPlayer->getId(), 'h' => $teamPlayer->getHash())),
                            );
                        }
                    }
                   
                }
            }

            

            //write info about app
            $user->setMobileAppDevice('1');
            $userRepo = ServiceLayer::getService("user_repository");
            $userRepo->save($user);
        }
        else
        {
            $result['auth_status'] = 'non exist uswer';
        }
        return $this->asJson($result);
    }

    private function buildUserData($user)
    {
        $security = ServiceLayer::getService('security');
        $userIdentity  = $security->getIdentity();
        $teamRoles = $userIdentity->getTeamRoles();

        if($user->getPersistHash() == null)
        {
            $hash = sha1($user->getSalt().$user->getEmail()) ;
            $security->getIdentityProvider()->saveUserPersistHash($user,$hash);
            $user->setPersistHash($hash);
        }
        
        $user_data = array();
        $user_data['id'] = $user->getId();
        $user_data['username'] = $user->getEmail();
        $user_data['name'] = $user->getName();
        $user_data['surname'] = $user->getSurname();
        $user_data['teamRoles'] =$teamRoles;
        $user_data['icon'] =  WEB_DOMAIN.'/player/icon?file='.$user->getPhoto();
        //$user_data['facebook_id'] = $user->getFacebookId();
        //$user_data['google_id'] = $user->getGoogleId();
        $user_data['authKey'] = $user->getPersistHash();
        $user_data['lang'] = $user->getDefaultLang();
        return $user_data;
    }
    
    public function forgottenPasswordAction()
    {
        $result = array();
        $result['params'] = $this->getRequestParams();
        $uid = $result['params']['uid'];
        //var_dump($uid);
         $security = ServiceLayer::getService('security');
		  $identityProvider = $security->getIdentityProvider();
        //$user = $security->getIdentityProvider()->findUserById($uid);
		$username = $result['params']['username'];
        $user = $identityProvider->findUserByUsername($username);
         if(null == $user)
            {
                $result['error_message'] = 'NOT EXIST USER';
                $result['status'] = 'error';
                
            }
            else
            {
                $repository = ServiceLayer::getService('user_repository');
                $password = $security->generatePassword();
                $encoded_password = $security->encodePassword($password,$user->getSalt());
                $user->setPassword($encoded_password);
                $repository->save($user);
                 
                 
                $sender = new \Core\Notifications\PasswordLostNotificationSender(new \Core\Notifications\PasswordLostStore());
                $notifyData = array();
                $notifyData['lang'] = $user->getDefaultLang();
                $notifyData['email'] =$user->getEmail();
                $notifyData['password'] = $password;
                $sender->send($notifyData);
                

                $result['status'] = 'success';
            }
        
        return $this->asJson($result);
    }
	
}