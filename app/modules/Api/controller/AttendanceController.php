<?php

namespace Coachrufus\Api\Controller;

use Core\RestApiController;
use Core\ServiceLayer;
use Core\Types\DateTimeEx;

class AttendanceController extends RestApiController {

      public function getWebviewContent($html,$options=array())
    {
        ServiceLayer::getService('layout')->setTemplate( GLOBAL_DIR.'/templates/WebviewLayout.php');
        $content = $this->renderTemplate(GLOBAL_DIR.'/templates/WebviewLayout.php',array(
             'module_content' => $html,
            'body_class' => $options['body_class']
         ))->getContent();
        
        return $content;  
    }
    
       
   public function getWebviewContent1($html,$options=array())
    {
        ServiceLayer::getService('layout')->setTemplate( GLOBAL_DIR.'/templates/WebviewContentLayout.php');
        $content = $this->renderTemplate(GLOBAL_DIR.'/templates/WebviewContentLayout.php',array(
             'module_content' => $html,
            'body_class' => $options['body_class']
         ))->getContent();
        
        return $content;  
    }
    
    public function indexAction()
    {
        if ($this->getRequest()->getMethod() == 'GET')
        {
          
            $data = $this->getRequest()->getGet();
            if(false == $this->checkApiKey())
            {
                return $this->unvalidApiKeyResult();
            }
            $user = $this->createUserFromRequest();

            
            $userId = $data['user_id'];
            $apikey = $data['apikey'];
             $selectedTeamId = null;
            $body_class = 'alert_body';
            if(array_key_exists('team_id', $data))
            {
                $selectedTeamId = $data['team_id'];
                $body_class = '';
            }

            $teamManager = \Core\ServiceLayer::getService('TeamManager');
            $teams = $teamManager->getUserTeams($user);
             if(empty($teams))
            {
                 $html = $this->render('Coachrufus\Api\ApiModule:Team:missing.php',
                        array(
                            'userId' => $user->getId()
                        ))
                        ->getContent();
                 return $this->asHtml( $this->getWebviewContent($html,array('body_class' => 'alert_body')));
            }
            
            
            
             if(null != $selectedTeamId)
             {
                 $controller = new \Webteamer\Team\Controller\TeamController();
                 $attendanceData = $controller->getPlayersAttendanceActionData();
                 
                   $apiViewData = array(
                        'userId' => $userId,
                        'user' => $user,
                        'apikey' => $apikey,
                        'teamId' => $selectedTeamId
                    );
                    $viewData = array_merge($apiViewData,$attendanceData);
                    $html = $this->render('Coachrufus\Api\ApiModule:Attendance:index.php',$viewData )->getContent();
                    return $this->asHtml( $this->getWebviewContent1($html,array('body_class' => 'iframe-webview-content-layout')));
                 
             }
             else
             {
                   $html = $this->render('Coachrufus\Api\ApiModule:Attendance:index.php', array(
                    'userId' => $userId,
                    'user' => $user,
                    'apikey' => $apikey,
                    'teamId' => $selectedTeamId
                ))->getContent();
                   return $this->asHtml( $this->getWebviewContent($html,array('body_class' => $body_class)));
            
             }
                    
          
            
            

         
        }
        else
        {
            return $this->asJson(array('status' => 'ERROR', 'error_message' => 'GET METHOD IS REQUIRED, NOT ' . $this->getRequest()->getMethod()));
        }
    }
    
    public function eventAttendanceAction()
    {
  
        
        if ($this->getRequest()->getMethod() == 'GET')
        {
            $data = $this->getRequest()->getGet();
            if(false == $this->checkApiKey())
            {
                return $this->unvalidApiKeyResult();
            }
            $user = $this->createUserFromRequest();

            $seasonManager = ServiceLayer::getService('TeamSeasonManager');
            $teamEventManager = ServiceLayer::getService('TeamEventManager');
            $teamManager = ServiceLayer::getService('TeamManager');
            $eventAttendanceRepository = ServiceLayer::getService('EventAttendanceRepository');
            $playerStatManager = ServiceLayer::getService('PlayerStatManager');
            
             
            $event = $teamEventManager->findEventById($data['event_id']);
            $eventCurrentDate = $data['event_current_date'];
            $event->setCurrentDate(new \DateTime($eventCurrentDate));
            $attendance = $teamEventManager->getEventAttendance($event);
            
            $teamMembers = $teamManager->getTeamPlayers($event->getTeamId());
            $team = $teamManager->findTeamById($event->getTeamId());
            $eventSeason = $seasonManager->getSeasonById($event->getSeason());
            $statFrom = new DateTimeEx($eventSeason->getStartDate());
            $statTo = new DateTimeEx($eventSeason->getEndDate());
            $teamPlayersStats = $playerStatManager->getAllTeamPlayersStat($team, null, $statFrom, $statTo);
            $lastGameEffeciencyDiff = 1;
            
            
            
            $indexedTeamMembers = array();
            foreach($teamMembers as $teamMember)
            {
                $playerStats = $teamPlayersStats[$teamMember->getId()];
               if(null != $playerStats)
               {
                   $teamMember->setStats($playerStats);
               }
               else {
                   $teamMember->setStats(new \Webteamer\Player\Model\PlayerStat());
               }
                $indexedTeamMembers[$teamMember->getId()] = $teamMember;
            }

            $response = array();
            foreach($attendance as $attendant)
            {
                if ($attendant->getTeamPlayerId() == null)
                {
                    $lastGameEffeciency = 0;
                }
                else
                {
                    $lastGameEffeciency = $indexedTeamMembers[$attendant->getTeamPlayerId()]->getStats()->getLastGamesEfficiency();
                }
                $efficiencyIndex = round($lastGameEffeciency * 10000) + $lastGameEffeciencyDiff++;
                
                
                $teamAttendant = $eventAttendanceRepository->convertToArray($indexedTeamMembers[$attendant->getTeamPlayerId()]);
                $teamAttendant['efficiency'] = $lastGameEffeciency;
                 $teamAttendant['avatar'] = $indexedTeamMembers[$attendant->getTeamPlayerId()]->getMidPhoto();
                 $teamAttendant['player_number'] = $indexedTeamMembers[$attendant->getTeamPlayerId()]->getPlayerNumber();
                 $teamAttendant['player_position'] = $indexedTeamMembers[$attendant->getTeamPlayerId()]->getTeamRole();
                $response[] = $teamAttendant;
            }
           
           

             return $this->asJson($response);
        }
        else
        {
            return $this->asJson(array('status' => 'ERROR', 'error_message' => 'GET METHOD IS REQUIRED, NOT ' . $this->getRequest()->getMethod()));
        }
    }
    
    
    public function paymentsAction()
    {
        if ($this->getRequest()->getMethod() == 'GET')
        {
    
            $data = $this->getRequest()->getGet();
            if(false == $this->checkApiKey())
            {
                return $this->unvalidApiKeyResult();
            }
            $user = $this->createUserFromRequest();

            
            $userId = $data['user_id'];
            $filters = json_decode($data['filters'],true);

            $html = $this->render('Coachrufus\Api\ApiModule:Attendance:payments.php', array('userId' => $userId,'filters' => $filters ))->getContent();

            $response = array();
            $response['html'] = $html;
            $response['backlink'] = $this->getRouter()->link('api_content_home');
            $response['filters'] = '';

              return $this->asHtml( $this->getWebviewContent($html,array('body_class' => 'alert_body')));
        }
        else
        {
            return $this->asJson(array('status' => 'ERROR', 'error_message' => 'GET METHOD IS REQUIRED, NOT ' . $this->getRequest()->getMethod()));
        }
    }

}
