<?php

namespace Coachrufus\Api\Controller;

use Core\RestApiController;
use Core\ServiceLayer;
use Core\Types\DateTimeEx;
use CR\HpWall\Model\HpWallPost;

class HpWallController extends RestApiController {

    public function likeUpdateAction()
    {
        if ($this->getRequest()->getMethod() == 'GET')
        {
            $data = $this->getRequest()->getGet();
            $repo = ServiceLayer::getService('HpWallPostRepository');
            $likeRepo = ServiceLayer::getService('HpWallLikeRepository');
            if(false == $this->checkApiKey())
            {
                return $this->unvalidApiKeyResult();
            }
            
            $postId = $data['pid'];
            $type = $data['t'];
            $authorId = $data['user_id'];
            $likeId = $data['lid'];
            
            
            
            if('l' == $type)
            {
                $post = $repo->find($postId);
                $likes = $post->getLikeCount();
                $post->setLikeCount($likes+1);
                $repo->save($post);
                
                
                $postLike = new \CR\HpWall\Model\HpWallLike();
                $postLike->setAuthorId($authorId);
                $postLike->setCreatedAt(new \Datetime());
                $postLike->setRecordId($postId);
                $postLike->setType('postLike');
                $likeId = $likeRepo->save($postLike);
                
            }
            
             if('u' == $type)
            {
                $post = $repo->find($postId);
                $likes = $post->getLikeCount();
                if($likes >= 1)
                {
                    $post->setLikeCount($likes-1);
                    $repo->save($post);
                }
                
                $postLike = $likeRepo->delete($likeId);
                
            }
            
            return $this->asJson(array('status' => 'success','likes' => $post->getLikeCount(),'likeId' => $likeId ));
           
          
        }
        else
        {
            return $this->asJson(array('status' => 'ERROR', 'error_message' => 'GET METHOD IS REQUIRED, NOT ' . $this->getRequest()->getMethod()));
        }
    }
    
    /*
    public function postPublishAction()
    {
        if ($this->getRequest()->getMethod() == 'GET')
        {
            $data = $this->getRequest()->getGet();
            if(false == $this->checkApiKey())
            {
                return $this->unvalidApiKeyResult();
            }
            
            $postId = $data['post_id'];
            $authorId = $data['uid'];
            
            if(null == $postId)
            {
                $post = new HpWallPost();
                $post->setAuthorId($authorId);
                $post->setBody('test');
                $post->setStatus('published');
                
                //$repo = new \Cr\HpWall\Model\HpWallPostRepository();
                
                $repo = ServiceLayer::getService('HpWallPostRepository');
                $repo->save($post);
            }
            
            return $this->asJson(array('status' => 'success'));
          
          
        }
        else
        {
            return $this->asJson(array('status' => 'ERROR', 'error_message' => 'GET METHOD IS REQUIRED, NOT ' . $this->getRequest()->getMethod()));
        }
    }
    */
    public function postAction()
    {
        $repo = ServiceLayer::getService('HpWallPostRepository'); 
        $notificationManager = ServiceLayer::getService('SystemNotificationManager');
         $teamManager = ServiceLayer::getService('TeamManager');
        
        if ($this->getRequest()->getMethod() == 'GET')
        {
             if(false == $this->checkApiKey())
            {
                return $this->unvalidApiKeyResult();
            }
           
            
            $data = $this->getRequest()->getGet();
            
             if(array_key_exists('author_id', $data))
             {
                  $criteria['hp.author_id'] = $data['author_id'];
             }

            if(array_key_exists('status', $data))
            {
                 $criteria['hp.status'] =  $data['status'];
            }
            
             if(array_key_exists('id', $data))
            {
                 $criteria['hp.id'] =  $data['id'];
            }

            $options = array('order' => ' hp.published_at DESC, hp.created_at DESC ');
            if(array_key_exists('teams', $data))
            {
                $options['custom_query'] = ' AND hpv.team_id in ('.$data['teams'].')';
            }
            
            $options['limit'] = array('from' => 0,'length' => 20);
            
            $objectList = $repo->findBy($criteria,$options);
            $list = $repo->createArrayList($objectList);
            
            return $this->asJson(array('status' => 'success','method'=>'GET','criteria' => $criteria,'result' => $list));
        }
        //create post
        if ($this->getRequest()->getMethod() == 'POST')
        {
            $postData = $this->getRequest()->getPost();
            
            if(false == $this->checkApiKey($postData['apikey']))
            {
                return $this->unvalidApiKeyResult();
            }
         
            
            
            $post = new HpWallPost();
            $post->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
            $post->setAuthorId($postData['author_id']);
            $post->setVisibility($postData['team_id']);
            $post->setBody($postData['body']);
            $post->setStatus($postData['status']); 
            $post->setType($postData['type']); 
            if($postData['status'] == 'published')
            {
                $post->setPublishedAt(new \DateTime);
            }

            $postId = $repo->saveNew($post);
            if($postData['status'] == 'published')
            {
                //var_dump($post->getVisibility());
                
                
                foreach($post->getVisibility() as $postVisibilityTeamId)
                {
                   $team = $teamManager->findTeamById($postVisibilityTeamId);
                   $notificationManager->createPushNotify('hp_wall_post',array('team' => $team,'senderId' => $postData['author_id'],'postId' => $postId));
                }


            }
            
            //set visibility
            return $this->asJson(array('status' => 'success','method'=>'POST','recordId' => $postId));
        }
        
        if ($this->getRequest()->getMethod() == 'PUT')
        {
            $putData = json_decode(file_get_contents("php://input"), true);
            $post = $repo->find($putData['id']);
            
            
              if(false == $this->checkApiKey($putData['apikey']))
            {
                return $this->unvalidApiKeyResult();
            }
         
            
            
            if($putData['status'] == 'published')
            {
                $putData['published_at'] = date('Y-m-d H:i:s');
            }
            
            unset($putData['id']);
            $repo->updateEntityFromArray($post,$putData);
            //$repo->save($post);
            
            //get post visibility
            $postVisibility = $repo->getPostVisibility($post);
            //send notify all 
             if($putData['status'] == 'published')
            {
                foreach($postVisibility as $postVisibilityItem)
                {
                   $team = $teamManager->findTeamById($postVisibilityItem['team_id']);
                   $notificationManager->createPushNotify('hp_wall_post',array('team' => $team,'senderId' => $postData['author_id'],'postId' => $postId));
                }
            }

            return $this->asJson(array('status' => 'success','method'=>'PUT'));
        }
        
        if ($this->getRequest()->getMethod() == 'DELETE')
        {
            
            $deleteId = $this->getRequest()->get('id');
            $repo->delete($deleteId);
            return $this->asJson(array('status' => 'success','method'=>'DELETE','deleteId' => $deleteId));
        }
    }
    
     public function postCommentAction()
    {
        $repo = ServiceLayer::getService('HpWallPostCommentRepository'); 
        $postRepo = ServiceLayer::getService('HpWallPostRepository'); 
        $teamManager = ServiceLayer::getService('TeamManager');
        $notificationManager = ServiceLayer::getService('SystemNotificationManager');
        if ($this->getRequest()->getMethod() == 'GET')
        {
             $data = $this->getRequest()->getGet();
             
              if(false == $this->checkApiKey())
            {
                return $this->unvalidApiKeyResult();
            }
            $user = $this->createUserFromRequest();
            
             if(array_key_exists('id', $data))
             {
                  $criteria['hp.post_id'] = $data['id'];
             }

          
            $options = array('order' => ' hp.created_at ASC ');
           
            
            $objectList = $repo->findBy($criteria,$options);
            $list = $repo->createArrayList($objectList);
            
            return $this->asJson(array('status' => 'success','method'=>'GET','criteria' => $criteria,'result' => $list));
        }
        //create post
        if ($this->getRequest()->getMethod() == 'POST')
        {
            $postData = $this->getRequest()->getPost();
            
            if(false == $this->checkApiKey($postData['apikey']))
            {
                return $this->unvalidApiKeyResult();
            }

            $post = new \CR\HpWall\Model\HpWallPostComment();
            $post->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
            $post->setPostId($postData['post_id']);
            $post->setAuthorId($postData['author_id']);
            $post->setBody($postData['body']);
            $post->setStatus($postData['status']); 
            $postId = $repo->save($post);
            $post->setId($postId);
            
            $security = ServiceLayer::getService('security');
            $author =  $security->getIdentityProvider()->findUserById($postData['author_id']);

            
            //send notify
            $wallPost = $postRepo->find($postData['post_id']);
            //$notificationManager->sendPostCommentPushNotify($wallPost,$author);
            
            
            //set visibility
            return $this->asJson(array('status' => 'success','method'=>'POST','recordId' => $postId));
        }
        
        if ($this->getRequest()->getMethod() == 'PUT')
        {
            /*
            $putData = json_decode(file_get_contents("php://input"), true);
            $post = $repo->find($putData['id']);
            
            if($putData['status'] == 'published')
            {
                $putData['published_at'] = date('Y-m-d H:i:s');
            }
            
            unset($putData['id']);
            $repo->updateEntityFromArray($post,$putData);
            $repo->save($post);

            return $this->asJson(array('status' => 'success','method'=>'PUT'));
             * 
             */
        }
        
        if ($this->getRequest()->getMethod() == 'DELETE')
        {
            /*
            $deleteId = $this->getRequest()->get('id');
            $repo->delete($deleteId);
            return $this->asJson(array('status' => 'success','method'=>'DELETE','deleteId' => $deleteId));
             * 
             */
        }
    }
    
    
    
    public function uploadAction()
    {
        $request = ServiceLayer::getService('request');
        $hpWallManager = ServiceLayer::getService('HpWallManager');
         //validate file size
        $fileSize = $request->getFileSize('file');
        $allowedSize = 3*1024*1024;
        $uploadDir  = $hpWallManager->getPostDir($request->get('pid'));
      
        if($allowedSize > $fileSize)
        {
            $seo = ServiceLayer::getService('seo');
            
            $baseName = md5(time()).'_'.$seo->createSlug($_FILES['file']['name']);
            $request->uploadFile('file',$uploadDir,$baseName);

            $imageTransform = ServiceLayer::getService('imageTransform');
           

            $imageTransform->resizeImage(array(
                'sourceImg' => $uploadDir.'/'.$baseName,
                'width' => 320, 
                'height' => 320,
                'targetDir' =>$uploadDir.'/thumb_320_320'));


            $result['file'] = '/img/hpWallPosts/'.$postDir.'/thumb_320_320/'.$baseName;
            $result['file_name'] = $baseName;
            
         
            $result['result'] = 'SUCCESS';
        }
        else
        {
             $result['result'] = 'ERROR';
             $result['error_message'] = ServiceLayer::getService('translator')->translate('Allowed max size is 3MB');
        }
        
        
        return $this->asJson($result);
        
     
    }
    

   
}
