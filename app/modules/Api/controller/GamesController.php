<?php

namespace Coachrufus\Api\Controller;

use Core\RestApiController;
use Core\ServiceLayer;
use Core\Types\DateTimeEx;

class GamesController extends RestApiController {

    public function getWebviewContent($html)
    {
        ServiceLayer::getService('layout')->setTemplate( GLOBAL_DIR.'/templates/WebviewLayout.php');
        $content = $this->renderTemplate(GLOBAL_DIR.'/templates/WebviewLayout.php',array(
             'module_content' => $html
         ))->getContent();
        
        return $content;  
    }
    
    public function getWebviewWidgetContent($html)
    {
        ServiceLayer::getService('layout')->setTemplate( GLOBAL_DIR.'/templates/WebviewWidgetLayout.php');
        $content = $this->renderTemplate(GLOBAL_DIR.'/templates/WebviewWidgetLayout.php',array(
             'module_content' => $html
         ))->getContent();
        
        return $content;  
    }
    
    
    public function scoreListAction()
    {
        if ($this->getRequest()->getMethod() == 'GET' or 'AJAX_GET' == $this->getRequest()->getMethod())
        {
            $data = $this->getRequest()->getGet();
             $userId = $data['user_id'];
            if(false == $this->checkApiKey())
            {
                return $this->unvalidApiKeyResult();
            }
            $user = $this->createUserFromRequest();

            $teamManager = \Core\ServiceLayer::getService('TeamManager');
            $teams = $teamManager->getUserTeams($user);
             if(empty($teams))
            {
                 $html = $this->render('Coachrufus\Api\ApiModule:Team:missing.php',
                        array(
                            'userId' => $user->getId()
                        ))
                        ->getContent();
                 return $this->asHtml( $this->getWebviewContent($html,array('body_class' => 'alert_body')));
            }
            
            

            $eventApiController = new EventController();
            $eventsData = $eventApiController->currentEvents($data);

            if (array_key_exists('mode', $data) && 'only_items' == $data['mode'])
            {
                $templateData = array(
                    'apikey' => $eventsData['apikey'],
                    'userId' => $eventsData['userId'],
                    'events' => $eventsData['calendarEventsList'],
                    'teams' => $eventsData['teams'],
                    'teamsPlayers' => $eventsData['teamsPlayers'],
                    'userTeamPlayerIds' => $eventsData['userTeamPlayerIds'],
                );

                $html = $this->render('Coachrufus\Api\ApiModule:Games:_score_list_item.php', $templateData)->getContent();
                $response = array();
                $response['html'] = $html;
                $response['nextPeriod'] = $eventsData['nextPeriod'];
                $response['prevPeriod'] = $eventsData['prevPeriod'];

                return $this->asJson($response);
            }
            else
            {
                $html = $this->render('Coachrufus\Api\ApiModule:Games:score_list.php', $eventsData)->getContent();
                return $this->asHtml($this->getWebviewContent($html));
            }
        }
        else
        {
            return $this->asJson(array('status' => 'ERROR', 'error_message' => 'GET METHOD IS REQUIRED, NOT ' . $this->getRequest()->getMethod()));
        }
    }
    
    //public function load

    public function resultsAction()
    {
        if ($this->getRequest()->getMethod() == 'GET' or 'AJAX_GET' == $this->getRequest()->getMethod())
        {
            $data = $this->getRequest()->getGet();
            if(false == $this->checkApiKey())
            {
                return $this->unvalidApiKeyResult();
            }
            $user = $this->createUserFromRequest();

            $apikey = $data['apikey'];
            $userId = $data['user_id'];

            $selectedTeamId =  ( array_key_exists('team_id', $data)) ? $data['team_id'] : null;
            
            
            if(!array_key_exists('start', $data))
            {
                $startDate = new \DateTime();
                $startDate->modify('-180 day');
                $endDate = new \DateTime();
            }
            else
            {
                $endDate = new \DateTime($data['end']);
                $startDate = new \DateTime($data['start']);
            }
            /*
            $endDate = new \DateTime($endPeriod);
            $startDays = 60;
            $startDate = new \DateTime($endPeriod);
            $startDate->modify('-'.$startDays.' day');
            /*
            $nextStartPeriod = new \DateTime($endPeriod);
            $nextStartPeriod->modify('-61 day');
            */

            $apiEventManager =  ServiceLayer::getService('ApiEventManager');
            $teamPlayersMap = $apiEventManager->getUserTeamsTeamPlayersMap($user);
            $teams = $teamPlayersMap['teams'];
            $teamsPlayers = $teamPlayersMap['teamsPlayers'];
            $userTeamPlayerIds = $teamPlayersMap['userTeamPlayerIds'];
            $teamEventManager = ServiceLayer::getService('TeamEventManager');
            

            if(empty($teams))
            {
                 $html = $this->render('Coachrufus\Api\ApiModule:Team:missing.php',
                        array(
                            'userId' => $user->getId()
                        ))
                        ->getContent();
                 return $this->asHtml( $this->getWebviewContent($html,array('body_class' => 'alert_body')));
            }
            
            
            
            
            $selectedTeams = $teams;

            if(null != $selectedTeamId )
            {
                $selectedTeams = array($selectedTeamId => $teams[$selectedTeamId]);
            }
             
            $eventsResult = $apiEventManager->getUserLastResultsEvents($user,array('startDate' => $startDate,'endDate' => $endDate,'selectedTeams' => $selectedTeams));
            
            
           // t_dump($eventsResult);
            
          
                
            
            //find ale match 
 
            /*
            $endPeriodDatetime = new \DateTime($endPeriod);
            $startPeriod = new \DateTime($endPeriod);
            $startPeriod->modify('-60 day');
            
            $nextStartPeriod = new \DateTime($endPeriod);
            $nextStartPeriod->modify('-61 day');
             * 
             */
            /*
            $calendarEvents = $teamEventManager->findEvents($selectedTeams, array('from' => $startPeriod, 'to' => $endPeriodDatetime));
            $calendarEventsList = $teamEventManager->buildTeamEventList($calendarEvents, $startPeriod,  $endPeriodDatetime);
            $apiEventManager->addEventListRowInfo($calendarEventsList,$teamsPlayers,$selectedTeams);
            
            //check
            $finishedEvents = array();
            foreach($calendarEventsList as $dayEvents){
               foreach($dayEvents as $event)
               {
                   if((!empty($event->getLineup())  &&  $event->getLineup()->getStatus() == 'closed'))
                   {
                       $finishedEvents[] = $event;
                   }
               }
            }
            
            if(empty($finishedEvents))
            {
                $endPeriodDatetime = new \DateTime($endPeriod);
                $startPeriod = new \DateTime($endPeriod);
                $startPeriod->modify('-'.$startDays.' day');

                $nextStartPeriod = new \DateTime($endPeriod);
                $nextStartPeriod->modify('-'.($startDays+1).' day');
                $calendarEvents = $teamEventManager->findEvents($selectedTeams, array('from' => $startPeriod, 'to' => $endPeriodDatetime));
                $calendarEventsList = $teamEventManager->buildTeamEventList($calendarEvents, $startPeriod,  $endPeriodDatetime);
                $apiEventManager->addEventListRowInfo($calendarEventsList,$teamsPlayers,$selectedTeams);
                $finishedEvents = array();
                foreach($calendarEventsList as $dayEvents){
                   foreach($dayEvents as $event)
                   {
                       if((!empty($event->getLineup())  &&  $event->getLineup()->getStatus() == 'closed'))
                       {
                           $finishedEvents[] = $event;
                       }
                   }
                }
            }
             * */
            //////////////
            
            $events = array_reverse($eventsResult['events'],true);
            
            if(array_key_exists('mode',$data) &&  'only_items' == $data['mode'])
           {
                $templateData = array(
                   'userId' => $userId,
                    'user' =>$user,
                    'selectedTeamId' => $selectedTeamId,
                    'events' => $events,
                    'teamsPlayers' => $teamsPlayers,
                    'userTeamPlayerIds' => $userTeamPlayerIds,
                    'apikey' => $apikey,
                    'teams' => $teams,
                    'startDate' => $eventsResult['nexStartDate']->format('Y-m-d'),
                    'endDate' => $eventsResult['nextEndDate']->format('Y-m-d')
                   
                );
                
                $html = $this->render('Coachrufus\Api\ApiModule:Games:_result_item.php', $templateData)->getContent();
                
                $response = array();
                $response['html'] = $html;
                $response['startDate'] = $eventsResult['nexStartDate']->format('Y-m-d');
                $response['endDate'] = $eventsResult['nextEndDate']->format('Y-m-d');
                return $this->asJson($response);
           }
            else
            {
                $html = $this->render('Coachrufus\Api\ApiModule:Games:results.php', array(
                    'userId' => $userId,
                    'user' =>$user,
                    'selectedTeamId' => $selectedTeamId,
                    'events' => $events,
                    'teamsPlayers' => $teamsPlayers,
                    'userTeamPlayerIds' => $userTeamPlayerIds,
                    'apikey' => $apikey,
                    'teams' => $teams,
                    'startDate' => $eventsResult['nexStartDate']->format('Y-m-d'),
                    'endDate' => $eventsResult['nextEndDate']->format('Y-m-d')
                ))->getContent();
                 return $this->asHtml( $this->getWebviewContent($html));
            }
          
        }
        else
        {
            return $this->asJson(array('status' => 'ERROR', 'error_message' => 'GET METHOD IS REQUIRED, NOT ' . $this->getRequest()->getMethod()));
        }
    }

   
    public function statsDataAction()
    {
        if ($this->getRequest()->getMethod() == 'GET')
        {
            if(false == $this->checkApiKey())
            {
                return $this->unvalidApiKeyResult();
            }
           

            $data = $this->getRequest()->getGet();
            $teamManager = ServiceLayer::getService('TeamManager');
            $seasonManager = ServiceLayer::getService('TeamSeasonManager');
            $playerStatManager = ServiceLayer::getService('PlayerStatManager');
            $team = $teamManager->findTeamById($data['team_id']);
            $list = $teamManager->getTeamPlayers($data['team_id']);

            if(null != $data['season_id'])
            {
                $currentSeason = $seasonManager->getSeasonById($data['season_id']);
            }
            else 
            {
                $currentSeason = $seasonManager->getTeamCurrentSeason($team);
            }

            $statFrom = new DateTimeEx($currentSeason->getStartDate());
            $statTo = new DateTimeEx($currentSeason->getEndDate());
            $teamPlayersStats = $playerStatManager->getAllTeamPlayersStat($team, null, $statFrom, $statTo);
        


            $maxValues = $teamPlayersStats['maxValues'];
            foreach ($list as $teamPlayer)
            {
                if (array_key_exists($teamPlayer->getId(), $teamPlayersStats))
                {
                    $teamPlayersStats[$teamPlayer->getId()]->setMaxValues($maxValues);
                    $teamPlayer->setStats($teamPlayersStats[$teamPlayer->getId()]);
                }
            }
            
           $response = array();
            foreach ($list as $teamPlayer)
            {
                $midPhoto = $teamPlayer->getMidPhoto();

                if (null == $midPhoto)
                {
                    $midPhoto = '/img/team/users.svg';
                }

                if (null != $teamPlayer->getStats())
                {
                    $stats = $teamPlayer->getStats();

                    $response['players'][] = array(
                        'avatar' => $midPhoto,
                        'name' => $teamPlayer->getFirstName(),
                        'surname' => $teamPlayer->getLastName(),
                        'game_played' => $stats->getGamePlayed(),
                        'wins' => $stats->getWins(),
                        'draws' => $stats->getDraws(),
                        'looses' => $stats->getLooses(),
                        'win_prediction' => $stats->getWinsPrediction(),
                        's_plus' => $stats->getPlusPoints(),
                        's_minus' => $stats->getMinusPoints(),
                        'plus_minus_diff' => $stats->getPlusMinusDiff(),
                        'goals' => $stats->getGoals(),
                        'assists' => $stats->getAssists(),
                        'average_goals' => $stats->getAverageGoals(),
                        'average_assists' => $stats->getAverageAssists(),
                        'ga' => $stats->getGA(),
                        'average_ga' => $stats->getAverageGA(),
                        'crs' => $stats->getCRS(),
                        'average_crs' => $stats->getAverageCRS(),
                        'gaa' => $stats->getGoalAgainstAverage(),
                        'shotouts' => $stats->getGoalkeeperShotouts(),
                        'mom' => $stats->getManOfMatch(),
                        'average_mom' => $stats->getAverageManOfMatch(),
                    );
                }
                else
                {

                    $response['players'][] = array(
                        'avatar' => $midPhoto,
                        'name' => $teamPlayer->getFirstName(),
                        'surname' => $teamPlayer->getLastName(),
                        'game_played' => '-',
                        'wins' => '-',
                        'draws' => '-',
                        'looses' => '-',
                        'win_prediction' => '-',
                        's_plus' => '-',
                        's_minus' => '-',
                        'plus_minus_diff' => '-',
                        'goals' => '-',
                        'assists' => '-',
                        'average_goals' => '-',
                        'average_assists' => '-',
                        'ga' => '-',
                        'average_ga' => '-',
                        'crs' => '-',
                        'average_crs' => '-',
                        'gaa' => '-',
                        'shotouts' => '-',
                        'mom' => '-',
                        'average_mom' => '-',
                    );
                }
            }
            return $this->asJson($response);
        }
        else
        {
            return $this->asJson(array('status' => 'ERROR', 'error_message' => 'GET METHOD IS REQUIRED, NOT ' . $this->getRequest()->getMethod()));
        }
    }
    
    public function createLineupAction()
    {
        if ($this->getRequest()->getMethod() == 'POST')
        {
            
           
            
            $postData = $this->getRequest()->getPost();
            $userId = $postData['user_id'];
            
             if(false == $this->checkApiKey($postData['apikey']))
            {
                return $this->unvalidApiKeyResult();
            }

            $teamMatchManager = ServiceLayer::getService('TeamMatchManager');
            $eventManager = ServiceLayer::getService('TeamEventManager');
            $teamManager = ServiceLayer::getService('TeamManager');
            $event = $eventManager->findEvent($postData['event_id']);
            $event->setCurrentDate(new \DateTime($postData['event_current_date']));
            $team = $teamManager->findTeamById($event->getTeamId());
            
            $security = ServiceLayer::getService('security');
            $identityProvider = $security->getIdentityProvider();
            $user = $identityProvider->findUserById($userId);
            $security->authenticateUserEntity($user);      
           

            $lineupId = $teamMatchManager->createLineup($event,$team,$postData['type']);
            
            return $this->asJson(array('lineupId' => $lineupId));
        }
         else
        {
            return $this->asJson(array('status' => 'ERROR', 'error' => 'POST METHOD IS REQUIRED, NOT '.$this->getRequest()->getMethod()));
        }
    }
    
    public function lineupAction()
    {

        if ($this->getRequest()->getMethod() == 'GET')
        {
            $data = $this->getRequest()->getGet();
            if(false == $this->checkApiKey())
            {
                return $this->unvalidApiKeyResult();
            }
            $user = $this->createUserFromRequest();
            $router =  ServiceLayer::getService('router');
           

            $teamEventManager = ServiceLayer::getService('TeamEventManager');
            $teamManager = ServiceLayer::getService('TeamManager');
            $eventAttendanceRepository = ServiceLayer::getService('EventAttendanceRepository');
            $teamPlayerRepository = ServiceLayer::getService('TeamPlayerRepository');
            $teamMatchLineupRepository = ServiceLayer::getService('TeamMatchLineupRepository');
            $event = $teamEventManager->findEventById($data['event_id']);
            $team = $teamManager->findTeamById($event->getTeamId());
            $opponentTeam = $teamManager->findTeamById($event->getOpponentTeamId());
            
            $teamImage = $team->getMidPhoto();
            $teamOpponentImage = (null != $opponentTeam) ? $opponentTeam->getMidPhoto() : '';
           
            $eventCurrentDate = $data['event_current_date'];
            $event->setCurrentDate(new \DateTime($eventCurrentDate));
            
            if(null != $data['lid'])
            {
                 $existLineup = $teamEventManager->getLineupById($data['lid']);
            }
            else
            {
                $existLineup = $teamEventManager->getEventLineup($event);
            }
           

            $response = array(
                'id' => null,
                'status' => null,
                'started_at' => null,
                'seconds_past' => 0,
                'firstLine' => array(),
                'secondLine' => array(),
                'firstLineName' => '',
                'secondLineName' => '',
                'teamImage' => $teamImage,
                'teamOpponentImage' => $teamOpponentImage
              );
            
           if (!empty($existLineup))
           {
                $seasonManager = ServiceLayer::getService('TeamSeasonManager');
                 $playerStatManager = ServiceLayer::getService('PlayerStatManager');
                  $team = $teamManager->findTeamById($event->getTeamId());
               $eventSeason = $seasonManager->getSeasonById($event->getSeason());
                $statFrom = new DateTimeEx($eventSeason->getStartDate());
                $statTo = new DateTimeEx($eventSeason->getEndDate());
                $teamPlayersStats = $playerStatManager->getAllTeamPlayersStat($team, null, $statFrom, $statTo);
               $lastGameEffeciencyDiff = 1;
               
               $members = $teamManager->getTeamPlayers($event->getTeamId()); 
               foreach ($members as $member)
                {
                    //$player =  $playerManager-> findPlayerById($member->getPlayerId());
                    $playerStats = $teamPlayersStats[$member->getId()];
                    if(null != $playerStats)
                    {
                        $member->setStats($playerStats);
                    }
                    else {
                        $member->setStats(new \Webteamer\Player\Model\PlayerStat());
                    }
                    //$member->getStats()->getLastGamesEfficiency();
                    $players[$member->getId()] = $member;
                }
                
                
                if($event->getOpponentTeamId() != null)
                {
                     $members = $teamManager->getTeamPlayers($event->getOpponentTeamId()); 
                    foreach ($members as $member)
                     {
                         //$player =  $playerManager-> findPlayerById($member->getPlayerId());
                         $playerStats = $teamPlayersStats[$member->getId()];
                         if(null != $playerStats)
                         {
                             $member->setStats($playerStats);
                         }
                         else {
                             $member->setStats(new \Webteamer\Player\Model\PlayerStat());
                         }
                         //$member->getStats()->getLastGamesEfficiency();
                         $players[$member->getId()] = $member;
                     }
                }
 
               $firstLine = array();
               $secondLine = array();
               
               $lineupPlayers = $teamEventManager->getEventLineupPlayers($existLineup);
                foreach ($lineupPlayers as $lineupPlayer)
                {
                    $lineupPlayerData = $teamMatchLineupRepository->convertToArray($lineupPlayer);
                    
                    if ($lineupPlayer->getTeamPlayerId() == null)
                    {
                        $lastGameEffeciency = 0;
                    }
                    else
                    {
                        $lastGameEffeciency = $players[$lineupPlayer->getTeamPlayerId()]->getStats()->getLastGamesEfficiency();
                    }
                    $efficiencyIndex = round($lastGameEffeciency * 10000) + $lastGameEffeciencyDiff++;

                    if ($lineupPlayer->getLineupPosition() == 'first_line')
                    {
                        $teamPlayer = null;
                        if (null != $lineupPlayer->getTeamPlayerId())
                        {
                            $lineupPlayerData['avatar'] = $players[$lineupPlayer->getTeamPlayerId()]->getMidPhoto();
                            $teamPlayer = $teamPlayerRepository->convertToArray($players[$lineupPlayer->getTeamPlayerId()]);
                        }

                        $firstLine[$efficiencyIndex] = array('player' => $lineupPlayerData, 'teamPlayer' => $teamPlayer, 'efficiency' => $lastGameEffeciency);
                    }
                    if ($lineupPlayer->getLineupPosition() == 'second_line')
                    {
                        $teamPlayer = null;
                        if (null != $lineupPlayer->getTeamPlayerId())
                        {
                            $lineupPlayerData['avatar'] = $players[$lineupPlayer->getTeamPlayerId()]->getMidPhoto();
                            $teamPlayer = $teamPlayerRepository->convertToArray($players[$lineupPlayer->getTeamPlayerId()]);
                        }
                        $secondLine[$efficiencyIndex] = array('player' => $lineupPlayerData, 'teamPlayer' => $teamPlayer, 'efficiency' => $lastGameEffeciency);
                    }

                   
                }
                krsort($firstLine);
                krsort($secondLine);
                
                $sportManager = ServiceLayer::getService('SportManager');
                $sportAddStatRoute = $sportManager->getTeamSportAddStatRoute($team);
                $fastScoreUrl = '';
                $scoreUrl = '';
                if($sportAddStatRoute == 'team_match_create_live_stat')
                {
                    $fastScoreUrl = $router->generateApiWidgetUrl('api_content_games_simplescore_widget',
                            array(
                                'apikey' => $this->getRequest()->get('apikey'),
                                'user_id' => $this->getRequest()->get('user_id'),
                                'event_id' => $event->getId(),
                                'lineup_id' => $existLineup->getId(),
                                'event_date' => $event->getCurrentDate()->format('Y-m-d')
                        ));
                    $scoreUrl = $router->generateApiWidgetUrl('api_content_games_score_widget',
                            array(
                                'apikey' => $this->getRequest()->get('apikey'),
                                'user_id' => $this->getRequest()->get('user_id'),
                                'event_id' => $event->getId(),
                                'lineup_id' => $existLineup->getId(),
                                'event_date' => $event->getCurrentDate()->format('Y-m-d')
                        ));
                }
                $response = array(
                    'id' => $existLineup->getId(),
                    'status' => $existLineup->getStatus(),
                    'started_at' => $existLineup->getStartedAt(),
                    'seconds_past' => ($existLineup->getSecondsPast() == null) ? 0 : $existLineup->getSecondsPast(),
                    'firstLine' => $firstLine,
                    'secondLine' => $secondLine,
                    'firstLineName' => $existLineup->getFirstLineName(),
                    'secondLineName' => $existLineup->getSecondLineName(),
                    'fastScoreUrl' => $fastScoreUrl,
                    'scoreUrl' => $scoreUrl,
                    'lineupUrl' => $router->generateApiWidgetUrl('api_content_games_lineup_widget',
                            array(
                                'apikey' => $this->getRequest()->get('apikey'),
                                'user_id' => $this->getRequest()->get('user_id'),
                                'event_id' => $event->getId(),
                                'lid' => $existLineup->getId(),
                                'event_date' => $event->getCurrentDate()->format('Y-m-d')
                        )),
                     'teamImage' => $teamImage,
                     'teamOpponentImage' => $teamOpponentImage
                );
                
                
               
            }

            return $this->asJson($response);
        }
        else
        {
            return $this->asJson(array('status' => 'ERROR', 'error_message' => 'GET METHOD IS REQUIRED, NOT ' . $this->getRequest()->getMethod()));
        }
    }
    
    public function startMatchAction()
    {

        if ($this->getRequest()->getMethod() == 'PUT')
        {
            
            $putData = array();
            parse_str(file_get_contents("php://input"),$putData);

            $userId = $putData['user_id'];
            $lineupId = $putData['lid'];
            
            if(false == $this->checkApiKey($putData['apikey']))
            {
                return $this->unvalidApiKeyResult();
            }
           

            $eventManager = ServiceLayer::getService('TeamEventManager');
            
            $security = ServiceLayer::getService('security');
            $identityProvider = $security->getIdentityProvider();
            $user = $identityProvider->findUserById($userId);
            $security->authenticateUserEntity($user);      

            $lineup = $eventManager->getLineupById($lineupId);
            $lineup->setStartedAt(new \DateTime());
            $lineup->setStatus('running');
            $eventManager->getLineupRepository()->save($lineup);
        }
         else
        {
            return $this->asJson(array('status' => 'ERROR', 'error' => 'PUT METHOD IS REQUIRED, NOT '.$this->getRequest()->getMethod()));
        }
    }
    
     public function pauseMatchAction()
    {

        if ($this->getRequest()->getMethod() == 'PUT')
        {
            
            $putData = array();
            parse_str(file_get_contents("php://input"),$putData);

            $userId = $putData['user_id'];
            $lineupId = $putData['lid'];
            $seconds = $putData['seconds'];
            
             if(false == $this->checkApiKey($putData['apikey']))
            {
                return $this->unvalidApiKeyResult();
            }

            $eventManager = ServiceLayer::getService('TeamEventManager');
            
            $security = ServiceLayer::getService('security');
            $identityProvider = $security->getIdentityProvider();
            $user = $identityProvider->findUserById($userId);
            $security->authenticateUserEntity($user);      

            $lineup = $eventManager->getLineupById($lineupId);
            $lineup->setStartedAt(new \DateTime());
            $lineup->setStatus('paused');
            $lineup->setSecondsPast($seconds);
            $eventManager->getLineupRepository()->save($lineup);
        }
         else
        {
            return $this->asJson(array('status' => 'ERROR', 'error' => 'PUT METHOD IS REQUIRED, NOT '.$this->getRequest()->getMethod()));
        }
    }
    
    public function timelineAction()
    {
        if ($this->getRequest()->getMethod() == 'GET')
        {
            $data = $this->getRequest()->getGet();
            if(false == $this->checkApiKey())
            {
                return $this->unvalidApiKeyResult();
            }
           // $user = $this->createUserFromRequest();
            $teamManager = ServiceLayer::getService('TeamManager');
             $teamEventManager = ServiceLayer::getService('TeamEventManager');
             $event = $teamEventManager->findEventById($data['event_id']);
            $eventCurrentDate = $data['event_current_date'];
            $event->setCurrentDate(new \DateTime($eventCurrentDate));
            
            $statManager = ServiceLayer::getService('PlayerStatManager');
            $eventTimeline = $statManager->getGroupedEventTimeline($event);
            
            $existLineup = $teamEventManager->getLineupById($data['lid']);
            $lineupPlayers = $teamEventManager->getEventLineupPlayers($existLineup);
            
            $members = $teamManager->getTeamPlayers($existLineup->getTeamId());
            $players = array();
            foreach ($members as $member)
            {
                $players[$member->getId()] = $member;
            }
            
            $opponentMembers = $teamManager->getTeamPlayers($existLineup->getOpponentTeamId());
            foreach ($opponentMembers as $member)
            {
                $players[$member->getId()] = $member;
            }
            
            
            $indexedLineup = array();
            foreach($lineupPlayers as $lineupPlayer)
            {
                if (null != $lineupPlayer->getTeamPlayerId())
                {
                    $lineupPlayer->setTeamPlayer($players[$lineupPlayer->getTeamPlayerId()]);
                }
                $indexedLineup[$lineupPlayer->getId()] = $lineupPlayer;
            }
            
            
            
           

            
            $response = array();
            foreach($eventTimeline as $group_key => $group)
            {
                foreach($group as $groupPlayer)
                {
                    $response[$group_key][$groupPlayer->getHitType()]['time'] = $groupPlayer->getHitTime();
                    $response[$group_key][$groupPlayer->getHitType()]['id'] = $groupPlayer->getId();
                    $response[$group_key][$groupPlayer->getHitType()]['linup_player_id'] = $groupPlayer->getLineupPlayerId();
                    if(array_key_exists($groupPlayer->getLineupPlayerId(), $indexedLineup))
                    {
                         $response[$group_key][$groupPlayer->getHitType()]['player'] = $indexedLineup[$groupPlayer->getLineupPlayerId()]->getPlayerName();
                         $response[$group_key][$groupPlayer->getHitType()]['player_avatar'] = $indexedLineup[$groupPlayer->getLineupPlayerId()]->getTeamPlayer()->getMidPhoto();
                         $response[$group_key][$groupPlayer->getHitType()]['player_number'] = $indexedLineup[$groupPlayer->getLineupPlayerId()]->getTeamPlayer()->getPlayerNumber();
                    }
                    else
                    {
                         $response[$group_key][$groupPlayer->getHitType()]['player'] = '?';
                    }
                    
                     $response[$group_key]['lineup_position'] =$groupPlayer->getLineupPosition();

                     if($groupPlayer->getLineupPosition() == 'first_line')
                     {
                        $response[$group_key]['lineup_name'] =$existLineup->getFirstLineName();
                     }

                     if($groupPlayer->getLineupPosition() == 'second_line')
                     {
                        $response[$group_key]['lineup_name'] =$existLineup->getSecondLineName();
                     }
                    
                     
                     if('assist' == $groupPlayer->getHitType() or 'goal' == $groupPlayer->getHitType())
                     {
                          $response[$group_key]['type'] = 'hit';
                     }
                    else 
                     {
                        $response[$group_key]['type'] = $groupPlayer->getHitType();
                     }
                    //$response[$group_key][$groupPlayer->getHitType()]['player'] = $groupPlayer->getLineupPlayerId();
                }
            }
            krsort($response);
            
            $matchResult = array('first_line' => 0, 'second_line' => 0,'first_line_name' =>$existLineup->getFirstLineName(),'second_line_name' =>$existLineup->getSecondLineName());
            $matchOverview = $statManager->getMatchOverview($existLineup);
            
            if(null !=  $matchOverview)
            {
                $matchResult['first_line'] = ($matchOverview->getFirstLineGoals() == null) ? 0 : $matchOverview->getFirstLineGoals() ;
                $matchResult['second_line'] = ($matchOverview->getSecondLineGoals() == null) ? 0 : $matchOverview->getSecondLineGoals() ;
                $matchResult['winner'] = 'none';

                if($matchResult['first_line']  > $matchResult['second_line'])
                {
                     $matchResult['winner'] = 'first_line' ;
                }

                if($matchResult['first_line']  < $matchResult['second_line'])
                {
                     $matchResult['winner'] = 'second_line' ;
                }
            }
            $fullResponse['match_status'] =  $existLineup->getStatus();
            $fullResponse['match_seconds_past'] =  $existLineup->getSecondsPast();
            $fullResponse['match_started_at'] =  $existLineup->getStartedAt();
            $fullResponse['timeline_items'] = $response;
            $fullResponse['match_result'] = $matchResult;

            return $this->asJson($fullResponse);
          
        }
        else
        {
            return $this->asJson(array('status' => 'ERROR', 'error_message' => 'GET METHOD IS REQUIRED, NOT ' . $this->getRequest()->getMethod()));
        }
    }
    
    
    public function lineupWidgetAction()
    {
        if ($this->getRequest()->getMethod() == 'GET')
        {
            $request = $this->getRequest();
            $data = $request->getDataFromEncodedApiUrl();

            $userId = $data['user_id'];
            $apikey = $data['apikey'];
            $eventId = $data['event_id'];
            $eventDate = $data['event_date'];
            $token = $data['token'];
            $provider =CR_API_PROVIDER;
            
            if(false == $this->checkApiKey($apikey))
            {
                return $this->unvalidApiKeyResult();
            }
            $user = $this->createUserFromRequest($userId);

            $selectedTeamId =  ( array_key_exists('team_id', $data)) ? $data['team_id'] : null;
            $html = $this->render('Coachrufus\Api\ApiModule:Games:lineup.php', array(
                'userId' => $userId,
                'eventId' => $eventId,
                'eventDate' => $eventDate,
                'token' => $token,
                'provider' => $provider,
                'apikey' => $apikey,
                'user' => $user,
                'teamId' => $selectedTeamId
            ))->getContent();

            $response = array();
            $response['html'] = $html;
            $response['backlink'] = $this->getRouter()->link('api_content_home');
            $response['filters'] = '';

            return $this->asHtml( $this->getWebviewContent($html));
        }
        else
        {
            return $this->asJson(array('status' => 'ERROR', 'error_message' => 'GET METHOD IS REQUIRED, NOT ' . $this->getRequest()->getMethod()));
        }
    }

    
     public function scoreWidgetAction()
    {
        if ($this->getRequest()->getMethod() == 'GET')
        {
             $request = $this->getRequest();
            $data = $request->getDataFromEncodedApiUrl();
            
             
            $userId = $data['user_id'];
            $apikey = $data['apikey'];
            $eventId = $data['event_id'];
            $eventDate = $data['event_date'];
            $lineupId = $data['lineup_id'];
            $token = $data['token'];
            $provider =CR_API_PROVIDER;
            
              if(false == $this->checkApiKey($apikey))
            {
                return $this->unvalidApiKeyResult();
            }
            $user = $this->createUserFromRequest($userId);
            
          
            
            $selectedTeamId =  ( array_key_exists('team_id', $data)) ? $data['team_id'] : null;
            
           

            $html = $this->render('Coachrufus\Api\ApiModule:Games:score.php', array(
                'userId' => $userId,
                'eventId' => $eventId,
                'eventDate' => $eventDate,
                'lineupId' => $lineupId,
                'token' => $token,
                'provider' => $provider,
                'apikey' => $apikey,
                'user' => $user,
                'teamId' => $selectedTeamId
            ))->getContent();

            $response = array();
            $response['html'] = $html;
            $response['backlink'] = $this->getRouter()->link('api_content_home');
            $response['filters'] = '';

            return $this->asHtml( $this->getWebviewWidgetContent($html));
        }
        else
        {
            return $this->asJson(array('status' => 'ERROR', 'error_message' => 'GET METHOD IS REQUIRED, NOT ' . $this->getRequest()->getMethod()));
        }
    }
    
    public function simpleScoreWidgetAction()
    {
        if ($this->getRequest()->getMethod() == 'GET')
        {
            $request = $this->getRequest();
            $data = $request->getDataFromEncodedApiUrl();

            
        

            $userId = $data['user_id'];
            $apikey = $data['apikey'];
            $eventId = $data['event_id'];
            $eventDate = $data['event_date'];
            $lineupId = $data['lineup_id'];
            $token = $data['token'];
            $provider =CR_API_PROVIDER;
            
              if(false == $this->checkApiKey($apikey))
            {
                return $this->unvalidApiKeyResult();
            }
            $user = $this->createUserFromRequest($userId);
            
            $selectedTeamId =  ( array_key_exists('team_id', $data)) ? $data['team_id'] : null;
            
         

            $html = $this->render('Coachrufus\Api\ApiModule:Games:simple_score.php', array(
                'userId' => $userId,
                'eventId' => $eventId,
                'eventDate' => $eventDate,
                'lineupId' => $lineupId,
                'token' => $token,
                'provider' => $provider,
                'apikey' => $apikey,
                'user' => $user,
                'teamId' => $selectedTeamId
            ))->getContent();

            $response = array();
            $response['html'] = $html;
            $response['backlink'] = $this->getRouter()->link('api_content_home');
            $response['filters'] = '';

            return $this->asHtml( $this->getWebviewWidgetContent($html));
        }
        else
        {
            return $this->asJson(array('status' => 'ERROR', 'error_message' => 'GET METHOD IS REQUIRED, NOT ' . $this->getRequest()->getMethod()));
        }
    }
}
