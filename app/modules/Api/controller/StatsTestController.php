<?php

namespace Coachrufus\Api\Controller;

use Core\ServiceLayer;

class StatsTestController extends TestController {

    public function personalAction()
    {
        $this->setup();
        $url = '/api/content/stats/personal';
        
        $list = array();

        //success
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
         $data['filters'] = json_encode(array('team_id' => array(1000)));
        $response = $this->getResponse($url,'GET',$data);
        $list['CORRECT_FILTERED'] = array('response' => $response,'params' => $data,'url' => urldecode($this->buildCallUrl($url,'GET',$data)));
        
        //success 2
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['filters'] = json_encode(array('team_id' => array('all')));
        $response2 = $this->call($url,'GET',$data);
        $list['CORRECT_ALL'] = array('response' => $response2,'params' => $data);

        //bad method
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['filters'] = json_encode(array('team_id' => array(1000)));
        $response4 = $this->call($url,'POST',$data);
        $list['BAD_METHOD'] = array('response' => $response4,'params' => $data);

        return $this->render('Coachrufus\Api\ApiModule:test:result.php', array(
                    'url' => $this->apiUrl.$url,
                    'list' => $list
        ));
    }
    
    public function tablesAction()
    {
        $this->setup();
        $url = '/api/content/stats/tables';
        
        $list = array();

        //success
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
         $data['filters'] = json_encode(array('team_id' => array(1000)));
        $response = $this->getResponse($url,'GET',$data);
        $list['CORRECT_FILTERED'] = array('response' => $response,'params' => $data,'url' => urldecode($this->buildCallUrl($url,'GET',$data)));
        
    

        return $this->render('Coachrufus\Api\ApiModule:test:result.php', array(
                    'url' => $this->apiUrl.$url,
                    'list' => $list
        ));
    }
    
    public function graphsAction()
    {
        $this->setup();
        $url = '/api/content/stats/graphs';
        
        $list = array();

        //success
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
         $data['filters'] = json_encode(array('team_id' => array(1000)));
        $response = $this->getResponse($url,'GET',$data);
        $list['CORRECT_FILTERED'] = array('response' => $response,'params' => $data,'url' => urldecode($this->buildCallUrl($url,'GET',$data)));
        
    

        return $this->render('Coachrufus\Api\ApiModule:test:result.php', array(
                    'url' => $this->apiUrl.$url,
                    'list' => $list
        ));
    }

}

