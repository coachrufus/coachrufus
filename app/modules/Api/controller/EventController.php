<?php

namespace Coachrufus\Api\Controller;

use Core\RestApiController;
use Core\ServiceLayer;
use Webteamer\Event\Model\EventCalendar;

class EventController extends RestApiController {

    public function getWebviewContent($html)
    {
        ServiceLayer::getService('layout')->setTemplate( GLOBAL_DIR.'/templates/WebviewLayout.php');
        $content = $this->renderTemplate(GLOBAL_DIR.'/templates/WebviewLayout.php',array(
             'module_content' => $html
         ))->getContent();
        
        return $content;  
    }
    
    public function getWebviewContent1($html,$options=array())
    {
        ServiceLayer::getService('layout')->setTemplate( GLOBAL_DIR.'/templates/WebviewContentLayout.php');
        $content = $this->renderTemplate(GLOBAL_DIR.'/templates/WebviewContentLayout.php',array(
             'module_content' => $html,
            'body_class' => $options['body_class']
         ))->getContent();
        
        return $content;  
    }
    
    public function getArrayParamValue($data,$index)
    {
        if(array_key_exists($index, $data))
        {
            return $data[$index];
        }
        else
        {
            return null;
        }
    }
    
    public function addEventTeamChoiceAction()
    {
        if ($this->getRequest()->getMethod() == 'GET')
        {
            
            
            $data = $this->getRequest()->getGet();
             if(false == $this->checkApiKey())
            {
                return $this->unvalidApiKeyResult();
            }
            $user = $this->createUserFromRequest();

            $apikey = $data['apikey'];
            $userId = $data['user_id'];

            $security = ServiceLayer::getService('security');

            if(array_key_exists('a', $data) && 'r' == $data['a'])
            {
                $link =   WEB_DOMAIN. $this->getRouter()->generateApiWidgetUrl('api_content_webview',array(
                 'view' => '/team/create-event/'.$data['team'],
                 'apikey' => CR_API_KEY,
                 'user_id' => $user->getId()
                 ));
                
                 $this->getRequest()->redirect($link);
            }
            $teamRoles = $security->getIdentity()->getTeamRoles();
            $teamManager = \Core\ServiceLayer::getService('TeamManager');
            $teams = $teamManager->getUserTeams($user);
            $availableTeams = array();
            foreach($teams as $team)
            {
                if($teamRoles[$team->getId()]['role'] == 'ADMIN')
                {
                    $availableTeams[] = $team;
                }
            }
            
            if(count($availableTeams) == 1)
            {
                $this->getRequest()->redirect($this->getRouter()->link('create_team_event',array('team_id' => $availableTeams[0]->getId())));
            }
            
            

            $html = $this->render('Coachrufus\Api\ApiModule:Event:addEventTeamChoice.php', array(
                        'user' => $user,
                        'apikey' => $apikey,
                        'teams' => $availableTeams
                    ))->getContent();


            //return $html;
            return $this->asHtml($this->getWebviewContent($html, array('body_class' => $body_class)));
        }
        else
        {
            return $this->asJson(array('status' => 'ERROR', 'error_message' => 'GET METHOD IS REQUIRED, NOT ' . $this->getRequest()->getMethod()));
        }
    }

    public function currentEvents($data)
    {
            $apikey = $data['apikey'];
            $userId = $data['user_id'];

            
            
            $selectedTeamId = null;
            if (array_key_exists('team_id', $data))
            {
                $selectedTeamId = $data['team_id'];
            }
            elseif(array_key_exists('api', $_SESSION) && array_key_exists('selected_team', $_SESSION['api']))
            {
                 $selectedTeamId = $_SESSION['api']['selected_team'];
            }


            if(!array_key_exists('period', $data))
            {
                $currentPeriod = date('Y-m-d');
            }
            else
            {
                $currentPeriod = $data['period'];
            }
            $currentPeriodDatetime =  new \DateTime($currentPeriod);
            
            $currentPeriodEnd = new \DateTime($currentPeriod);
            $currentPeriodEnd->modify('+59 day');
            
            $nextPeriod = new \DateTime($currentPeriod);
            $nextPeriod->modify('+60 day');
            
            $prevPeriod = new \DateTime($currentPeriod);
            $prevPeriod->modify('-60 day');

            
            $security = ServiceLayer::getService('security');
            $identityProvider = $security->getIdentityProvider();
            $user = $identityProvider->findUserById($userId);
            $security->authenticateUserEntity($user);      
            
            $apiEventManager =  ServiceLayer::getService('ApiEventManager');
            $teamPlayersMap = $apiEventManager->getUserTeamsTeamPlayersMap($user);
            $teams = $teamPlayersMap['teams'];
           
            
            $teamsPlayers = $teamPlayersMap['teamsPlayers'];
            $userTeamPlayerIds = $teamPlayersMap['userTeamPlayerIds'];
            $teamEventManager = ServiceLayer::getService('TeamEventManager');
            
            $selectedTeams = $teams;
            if(null != $selectedTeamId )
            {
                $selectedTeams = array($selectedTeamId => $teams[$selectedTeamId]);
                
                $_SESSION['api']['selected_team'] = $selectedTeamId;
            }
           
             
            $calendarEvents = $teamEventManager->findEvents($selectedTeams, array('from' => $currentPeriodDatetime, 'to' => $currentPeriodEnd));
            $calendarEventsList = $teamEventManager->buildTeamEventList($calendarEvents, $currentPeriodDatetime,  $currentPeriodEnd);
            $apiEventManager->addEventListRowInfo($calendarEventsList,$teamsPlayers,$selectedTeams);
           
         
            return array(
                    'userId' => $userId,
                    'user' =>$user,
                    'selectedTeamId' => $selectedTeamId,
                    'calendarEventsList' => $calendarEventsList,
                    'teamsPlayers' => $teamsPlayers,
                    'userTeamPlayerIds' => $userTeamPlayerIds,
                    'apikey' => $apikey,
                    'teams' => $teams,
                    'nextPeriod' => $nextPeriod->format('Y-m-d'),
                    'prevPeriod' => $prevPeriod->format('Y-m-d')
                );
          
           
    }
    
    
    public function currentEventsAction()
    {
        if ($this->getRequest()->getMethod() == 'GET' or 'AJAX_GET' == $this->getRequest()->getMethod())
        {
            $data = $this->getRequest()->getGet();
            $userId = $data['user_id'];
            if(false == $this->checkApiKey())
            {
                return $this->unvalidApiKeyResult();
            }

            $user = $this->createUserFromRequest();
            $teamManager = \Core\ServiceLayer::getService('TeamManager');
            $teams = $teamManager->getUserTeams($user);
            
             
            if(empty($teams))
            {
                 $html = $this->render('Coachrufus\Api\ApiModule:Team:missing.php',
                        array(
                            'userId' => $user->getId()
                        ))
                        ->getContent();
                 return $this->asHtml( $this->getWebviewContent($html,array('body_class' => 'alert_body')));
            }
            
            
            
            
            $eventsData =  $this->currentEvents($data);
            
            
            if(array_key_exists('mode',$data) &&  'only_items' == $data['mode'])
           {
                $templateData = array(
                    'apikey' => $eventsData['apikey'],
                    'userId' => $eventsData['userId'],
                 'events' => $eventsData['calendarEventsList'],
                  'teams' => $eventsData['teams'],
                 'teamsPlayers' =>  $eventsData['teamsPlayers'],
                 'userTeamPlayerIds' => $eventsData['userTeamPlayerIds'],
                );
                
                $html = $this->render('Coachrufus\Api\ApiModule:Event:_current_item.php', $templateData)->getContent();
                $response = array();
                $response['html'] = $html;
                $response['nextPeriod'] = $eventsData['nextPeriod'];
                $response['prevPeriod'] = $eventsData['prevPeriod'];
                
                return $this->asJson($response);
           }
            elseif(array_key_exists('mode',$data) &&  'raw' == $data['mode'])
            {
                return $this->asJson($eventsData);
            }
            else
            {
                $html = $this->render('Coachrufus\Api\ApiModule:Event:current.php', $eventsData)->getContent();
                return $this->asHtml( $this->getWebviewContent($html));
            }
        }
        else
        {
            return $this->asJson(array('status' => 'ERROR', 'error_message' => 'GET METHOD IS REQUIRED, NOT ' . $this->getRequest()->getMethod()));
        }
    }
    
   
    
    
    public function calendarMonthAction()
    {
         if ($this->getRequest()->getMethod() == 'GET' or 'AJAX_GET' == $this->getRequest()->getMethod())
        {
             $data = $this->getRequest()->getGet();
            if(false == $this->checkApiKey())
            {
                return $this->unvalidApiKeyResult();
            }
            $user = $this->createUserFromRequest();

            $apikey = $data['apikey'];
            $userId = $data['user_id'];
            $month = $data['month'];
            $dir = $data['dir'];
            
            if($dir == 'prev')
            {
                $nextMonth = \DateTime::createFromFormat('Y-m-d',$month.'-1');
                $nextMonth->modify('-1 month');
            }
            else {
                $nextMonth = \DateTime::createFromFormat('Y-m-d',$month.'-1');
                $nextMonth->modify('+1 month');
            }
            
            
            $calendarMonth = $this->getMonthCalendar($userId,$month);

           
             $response = array();
                $response['html'] = $calendarMonth->getContent();
                $response['periodInfo'] = $nextMonth->format('Y-m');
                
                return $this->asJson($response);
             //return $this->asHtml( $calendarMonth->getContent());
        }
        else
        {
            return $this->asJson(array('status' => 'ERROR', 'error_message' => 'GET METHOD IS REQUIRED, NOT ' . $this->getRequest()->getMethod()));
        }
    }
    
    public function calendarAction()
    {
        if ($this->getRequest()->getMethod() == 'GET')
        {
             $data = $this->getRequest()->getGet();
            if(false == $this->checkApiKey())
            {
                return $this->unvalidApiKeyResult();
            }
            $user = $this->createUserFromRequest();


            $apikey = $data['apikey'];
            $userId = $data['user_id'];
            $selectedTeamId = $data['team_id'];
            
            $nextMonth = \DateTime::createFromFormat('Y-m-d', date('Y').'-'.date('m').'-1');
            $nextMonth->modify('+1 month');

            
            
            $calendarMonth = $this->getMonthCalendar($userId);
            $calendarMonth2 = $this->getMonthCalendar($userId,$nextMonth->format('Y-m'));
            
             $html = $this->render('Coachrufus\Api\ApiModule:Event:calendar.php', array(
                'calendarMonth' => $calendarMonth->getContent(),
                'calendarMonth2' => $calendarMonth2->getContent(),
                'nextPeriod' => $nextMonth->modify('+1 month')->format('Y-m'),
                'prevPeriod' => $nextMonth->modify('-3 month')->format('Y-m'),
                 'apikey' => $apikey,
                 'userId' => $userId,
                 'selectedTeamId' => $selectedTeamId
              ))->getContent();
             
             return $this->asHtml( $this->getWebviewContent($html));
        }
        else
        {
            return $this->asJson(array('status' => 'ERROR', 'error_message' => 'GET METHOD IS REQUIRED, NOT ' . $this->getRequest()->getMethod()));
        }
    }
    
    
    
    
    public function getMonthCalendar($userId,$month = null)
    {
        $security = ServiceLayer::getService('security');
        $identityProvider = $security->getIdentityProvider();
        $user = $identityProvider->findUserById($userId);

        $apiEventManager =  ServiceLayer::getService('ApiEventManager');
        $teamPlayersMap = $apiEventManager->getUserTeamsTeamPlayersMap($user);
        $teams = $teamPlayersMap['teams'];
        $teamsPlayers = $teamPlayersMap['teamsPlayers'];
        $userTeamPlayerIds = $teamPlayersMap['userTeamPlayerIds'];
        $teamEventManager = ServiceLayer::getService('TeamEventManager');

        if($month == null)
        {
            $month =  date('Y').'-'.date('m');
        }


        $startDate = new \DateTime($month . '-01');
        $monthInfo = $teamEventManager->getPagerMonthInfo($startDate);



        $calendarEvents = $teamEventManager->findEvents($teams, array('from' => $monthInfo['from'], 'to' => $monthInfo['to']));

        $calendar = new EventCalendar();
        $calendar->setDateFrom($monthInfo['from']);
        $calendar->setDateTo($monthInfo['to']);
        $calendar->setCurrentMonth($monthInfo['current_month']);
        $calendar->buildMonthGrid($month);

       $calendarEventsList = $teamEventManager->buildTeamEventList($calendarEvents, $monthInfo['from'], $monthInfo['to']);
       $apiEventManager->addEventListRowInfo($calendarEventsList,$teamsPlayers);
       $calendar->setEvents($calendarEventsList);


       $userAttednanceMatrix = $teamEventManager->getPlayerEventsAttendanceMatrix($user, $calendarEventsList);

       $html = $this->render('Coachrufus\Api\ApiModule:Event:calendar_month.php', array(
                'attendanceList' => $userAttednanceMatrix,
                'calendar' => $calendar,
                'teams' => $teams,
                'teamsPlayers' => $teamsPlayers,
                'userTeamPlayerIds' => $userTeamPlayerIds,
            ))->getContent();
        return $this->asHtml( $html);
       
    }
    
    
    
    
    public function createAction()
    {
         if ($this->getRequest()->getMethod() == 'GET')
        {
            $html = $this->render('Coachrufus\Api\ApiModule:Event:create.php', array())->getContent();

            $response = array();
            $response['html'] = $html;
            $response['backlink'] = $this->getRouter()->link('api_content_home');
            $response['filters'] = '';

            return $this->asJson(array('status' => 'SUCCESS', 'data' => $response));
        }
        else
        {
            return $this->asJson(array('status' => 'ERROR', 'error_message' => 'GET METHOD IS REQUIRED, NOT ' . $this->getRequest()->getMethod()));
        }
    }
    public function scoreCreateAction()
    {
         if ($this->getRequest()->getMethod() == 'GET')
        {
            $html = $this->render('Coachrufus\Api\ApiModule:Event:scoreCreate.php', array())->getContent();

            $response = array();
            $response['html'] = $html;
            $response['backlink'] = $this->getRouter()->link('api_content_home');
            $response['filters'] = '';

            return $this->asJson(array('status' => 'SUCCESS', 'data' => $response));
        }
        else
        {
            return $this->asJson(array('status' => 'ERROR', 'error_message' => 'GET METHOD IS REQUIRED, NOT ' . $this->getRequest()->getMethod()));
        }
    }
    
    public function eventContentDetailAction()
    {
        $request = $this->getRequest();

        if ($request->getMethod() == 'GET')
        {
            $data = $request->getGet();
            if(false == $this->checkApiKey())
            {
                return $this->unvalidApiKeyResult();
            }
            //$user = $this->createUserFromRequest();

            $teamEventManager = ServiceLayer::getService('TeamEventManager');
            $teamManager = ServiceLayer::getService('TeamManager');
            $statManager = ServiceLayer::getService('PlayerStatManager');
            $event = $teamEventManager->findEventById($request->get('id'));
            $eventCurrentDate = $request->get('current_date');
            $event->setCurrentDate(new \DateTime($eventCurrentDate));
            $existLineup = $teamEventManager->getEventLineup($event);

            $attendance = $teamEventManager->getEventAttendance($event);
            $event->setAttendance($attendance);

            if (null != $existLineup)
            {
                $matchOverview = $statManager->getMatchOverview($existLineup);
                $event->setMatchResult($matchOverview->getResult());
                $event->setMatchOverview($matchOverview);

                if ($existLineup->getStatus() == 'closed')
                {
                    $event->setClosed(true);
                }
            }

            $lineupPlayers = $teamEventManager->getEventLineupPlayers($existLineup);
            $teamsPlayers = $teamManager->getTeamPlayers($event->getTeamId());
            $membersCollection = array();
            foreach ($teamsPlayers as $member)
            {
                $membersCollection[$member->getId()] = $member;
            }

            foreach ($lineupPlayers as $lineupPlayer)
            {
                if ($lineupPlayer->getTeamPlayerId() > 0)
                {
                    $lineupPlayer->setTeamPlayer($membersCollection[$lineupPlayer->getTeamPlayerId()]);
                }
            }
            

            $existLineup->setLineupPlayers($lineupPlayers);
            $event->setLineup($existLineup);

            $firstLine = array();
            $secondLine = array();
            $players = array();
            foreach ($lineupPlayers as $lineupPlayer)
            {
                if (true or array_key_exists($lineupPlayer->getTeamPlayerId(), $membersCollection))
                {
                    $lineupPlayer->setTeamPlayer($membersCollection[$lineupPlayer->getTeamPlayerId()]);
                    $players[$lineupPlayer->getId()] = $lineupPlayer;
                }

                if ($lineupPlayer->getLineupPosition() == 'first_line')
                {
                    $firstLine[] = $lineupPlayer;
                }
                if ($lineupPlayer->getLineupPosition() == 'second_line')
                {
                    $secondLine[] = $lineupPlayer;
                }
            }

            $matchStats = $statManager->getMatchPlayersStat($existLineup);
            
            $html = $this->render('Coachrufus\Api\ApiModule:Event:detail.php', array(
                        'matchStats' => $matchStats,
                        'players' => $players,
                        'event' => $event
                    ))->getContent();


            return $this->asHtml($this->getWebviewContent($html));
        }
        else
        {
            return $this->asJson(array('status' => 'ERROR', 'error_message' => 'GET METHOD IS REQUIRED, NOT ' . $this->getRequest()->getMethod()));
        }
    }

}
