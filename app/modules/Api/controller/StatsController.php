<?php

namespace Coachrufus\Api\Controller;

use Core\RestApiController;
use Core\ServiceLayer;

class StatsController extends RestApiController {
   public function getWebviewContent($html,$options=array())
    {
        ServiceLayer::getService('layout')->setTemplate( GLOBAL_DIR.'/templates/WebviewLayout.php');
        $content = $this->renderTemplate(GLOBAL_DIR.'/templates/WebviewLayout.php',array(
             'module_content' => $html,
            'body_class' => $options['body_class']
         ))->getContent();
        
        return $content;  
    }
    
   public function getWebviewContent1($html,$options=array())
    {
        ServiceLayer::getService('layout')->setTemplate( GLOBAL_DIR.'/templates/WebviewContentLayout.php');
        $content = $this->renderTemplate(GLOBAL_DIR.'/templates/WebviewContentLayout.php',array(
             'module_content' => $html,
            'body_class' => $options['body_class']
         ))->getContent();
        
        return $content;  
    }
    
  
     
    
    public function personalAction()
    {
        if ($this->getRequest()->getMethod() == 'GET')
        {
            $data = $this->getRequest()->getGet();
            if(false == $this->checkApiKey())
            {
                return $this->unvalidApiKeyResult();
            }
            $user = $this->createUserFromRequest();
            
            $userId = $data['user_id'];
            $apikey = $data['apikey'];
            $selectedTeamId =  ( array_key_exists('team_id', $data)) ? $data['team_id'] : null;
            $teamPlayerId = null;
          

            $teamManager = \Core\ServiceLayer::getService('TeamManager');
            $teams = $teamManager->getUserTeams($user);
            if(empty($teams))
            {
                 $html = $this->render('Coachrufus\Api\ApiModule:Team:missing.php',
                        array(
                            'userId' => $user->getId()
                        ))
                        ->getContent();
                 return $this->asHtml( $this->getWebviewContent($html,array('body_class' => 'alert_body')));
            }

           if(null != $selectedTeamId)
           {
               $teamPlayers = $teamManager->getTeamPlayers($selectedTeamId);

               foreach($teamPlayers as $teamPlayer)
               {
                   if($teamPlayer->getPlayerId() == $userId)
                   {
                       $teamPlayerId = $teamPlayer->getId();
                   }
               }
               
                $controller = new \Webteamer\Team\Controller\TeamPlayerController();
                
                $statsData = $controller->getTeamPlayerStatsActionData($teamPlayerId);
                 $apiViewData = array(
                    'userId' => $userId,
                    'apikey' => $apikey,
                    'user' => $user,
                    'teamId' => $selectedTeamId,
                    'teamPlayerId' => $teamPlayerId
                );
                    
                $viewData = array_merge($apiViewData,$statsData);

                $html = $this->render('Coachrufus\Api\ApiModule:Stats:personal.php',$viewData )->getContent();

                return $this->asHtml( $this->getWebviewContent1($html,array('body_class' => 'iframe-webview-content-layout')));
               
               
           }
           else 
           {
                $html = $this->render('Coachrufus\Api\ApiModule:Stats:personal.php', array(
                    'userId' => $userId,
                    'apikey' => $apikey,
                    'user' => $user,
                    'teamId' => $selectedTeamId,
                    'teamPlayerId' => $teamPlayerId
                ))->getContent();
                return $this->asHtml( $this->getWebviewContent($html,array('body_class' => 'alert_body')));
           }


           
        }
        else
        {
            return $this->asJson(array('status' => 'ERROR', 'error_message' => 'GET METHOD IS REQUIRED, NOT ' . $this->getRequest()->getMethod()));
        }
    }
    
    public function tablesAction()
    {
        if ($this->getRequest()->getMethod() == 'GET')
        {
            $data = $this->getRequest()->getGet();
            if(false == $this->checkApiKey())
            {
                return $this->unvalidApiKeyResult();
            }
            $user = $this->createUserFromRequest();
            
            $userId = $data['user_id'];
            $apikey = $data['apikey'];
            $selectedTeamId = null;
            $body_class = 'alert_body';
            if(array_key_exists('team_id', $data))
            {
                $selectedTeamId = $data['team_id'];
                $body_class = '';
            }

            $teamManager = \Core\ServiceLayer::getService('TeamManager');
            $teams = $teamManager->getUserTeams($user);
            if(empty($teams))
            {
                 $html = $this->render('Coachrufus\Api\ApiModule:Team:missing.php',
                        array(
                            'userId' => $user->getId()
                        ))
                        ->getContent();
                 return $this->asHtml( $this->getWebviewContent($html,array('body_class' => 'alert_body')));
            }
            

            $html = $this->render('Coachrufus\Api\ApiModule:Stats:tables.php', array(
                'userId' => $userId,
                'apikey' => $apikey,
                'user' => $user,
                'teamId' => $selectedTeamId
            ))->getContent();

            $response = array();
            $response['html'] = $html;
            $response['backlink'] = $this->getRouter()->link('api_content_home');
            $response['filters'] = '';
            
            
            return $this->asHtml( $this->getWebviewContent($html,array('body_class' => $body_class)));

            //return $this->asHtml( $this->getWebviewContent($html));
        }
        else
        {
            return $this->asJson(array('status' => 'ERROR', 'error_message' => 'GET METHOD IS REQUIRED, NOT ' . $this->getRequest()->getMethod()));
        }
    }
    
    public function graphsAction()
    {
        if ($this->getRequest()->getMethod() == 'GET')
        {
            $data = $this->getRequest()->getGet();
             if(false == $this->checkApiKey())
            {
                return $this->unvalidApiKeyResult();
            }
            $user = $this->createUserFromRequest();
            
            $userId = $data['user_id'];
            $apikey = $data['apikey'];
            $selectedTeamId =  ( array_key_exists('team_id', $data)) ? $data['team_id'] : null;

            $teamManager = \Core\ServiceLayer::getService('TeamManager');
            $teams = $teamManager->getUserTeams($user);
            if(empty($teams))
            {
                 $html = $this->render('Coachrufus\Api\ApiModule:Team:missing.php',
                        array(
                            'userId' => $user->getId()
                        ))
                        ->getContent();
                 return $this->asHtml( $this->getWebviewContent($html,array('body_class' => 'alert_body')));
            }
            else
            {
                
                if(null != $selectedTeamId)
                {
                    
                    $controller = new \CR\Widget\Controller\AnalyticsController();
                    $data = $controller->getListActionData();
                     $apiViewData = array(
                        'userId' => $userId,
                        'apikey' => $apikey,
                        'user' => $user,
                        'teamId' => $selectedTeamId,
                        'errorAlertContent' => false
                    );

                    if($data['status'] == 'NO_STAT_ROUTE')
                    {
                        
                        $html = $this->render('Webteamer\Team\TeamModule:teamMatch:season_stat_coming_soon.php', array(
                            'team' => $data['team'],
                        ))->getContent();
                        
                        $apiViewData['errorAlertContent'] = $html;
                    }
                    if($data['status'] == 'NO_ACTUAL_SEASON')
                    {
                        
                        $html = $this->render('Webteamer\Team\TeamModule:teamMatch:season_stat_unavailable.php', array(
                            'team' =>  $data['team'],
                        ))->getContent();
                        
                         $apiViewData['errorAlertContent'] = $html;
                    }
                    

                    $viewData = array_merge($apiViewData,$data);
                    
                    $html = $this->render('Coachrufus\Api\ApiModule:Stats:graphs.php',$viewData )->getContent();

                    return $this->asHtml( $this->getWebviewContent1($html,array('body_class' => 'iframe-webview-content-layout')));
                    
                }
                else
                {
                     $html = $this->render('Coachrufus\Api\ApiModule:Stats:graphs.php', array(
                    'userId' => $userId,
                    'apikey' => $apikey,
                    'user' => $user,
                    'teamId' => $selectedTeamId
                ))->getContent();
                
                return $this->asHtml( $this->getWebviewContent($html,array('body_class'=>'alert_body')));
                }
                
               
                
                
                
                /*
               
                 * 
                 */
            }

            
        }
        else
        {
            return $this->asJson(array('status' => 'ERROR', 'error_message' => 'GET METHOD IS REQUIRED, NOT ' . $this->getRequest()->getMethod()));
        }
    }
    
    
 
    

}
