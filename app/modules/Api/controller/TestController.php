<?php

namespace Coachrufus\Api\Controller;

use Core\ServiceLayer;

class TestController extends \Core\Controller {

    protected $apiUrl = WEB_DOMAIN;
    
    protected function setup()
    {
        ServiceLayer::getService('layout')->setTemplate(null);
    }
    
    public function listAction()
    {
          $this->setup();
        return $this->render('Coachrufus\Api\ApiModule:test:list.php', array(
                   
        ));
    }

    public function loginAction()
    {
        $this->setup();
        $url = '/api/user/login';
        
        $list = array();
        
        $data['apikey'] = 'app@40101499408386491';
        $data['username'] = 'api-test@coachrufus.com';
        $data['pass'] = 'aa';
        $response = $this->call($url,'GET',$data);
        
         //success
        $list['CORRECT'] = array('response' => $response,'params' => $data);
        
        //bad login data
        $data['pass'] = 'cc';
        $response2 = $this->call($url,'GET',$data);
        $list['UNVALID _LOGIN_DATA'] = array('response' => $response2,'params' => $data);
        
        //empty login data
        $data['pass'] = '';
        $response3 = $this->call($url,'GET',$data);
        $list['EMPTY_LOGIN_DATA'] = array('response' => $response3,'params' => $data);

        return $this->render('Coachrufus\Api\ApiModule:test:result.php', array(
                    'url' => $this->apiUrl.$url,
                    'list' => $list
        ));
    }

    public function loginFbAction()
    {
        $this->setup();
        $url = '/api/user/login-fb';
        $list = array();
        $data['apikey'] = 'app@40101499408386491';
        $data['username'] = 'api-test@coachrufus.com';
        $data['fbid'] = '11112222';
        $response = $this->call($url,'GET',$data);
        
        //success
        $list['CORRECT'] = array('response' => $response,'params' => $data);
        
        
        return $this->render('Coachrufus\Api\ApiModule:test:result.php', array(
                    'url' => $url,
                    'list' => $list
        ));
    }

    public function loginGoogleAction()
    {
        $this->setup();
        $url = '/api/user/login-google';
        $data['apikey'] = 'app@40101499408386491';
        $data['username'] = 'api-test@coachrufus.com';
        $data['gid'] = '11112222';
        $response = $this->call($url,'GET',$data);
        $list['CORRECT'] = array('response' => $response,'params' => $data);
        return $this->render('Coachrufus\Api\ApiModule:test:result.php', array(
                    'url' => $url,
                     'list' => $list
        ));
    }

    public function registerAction()
    {
        $this->setup();
        $url = '/api/user/register';
        $data['apikey'] = 'app@40101499408386491';
        $data['email'] = 'api-test-'.md5(time().rand()).'@coachrufus.com';
        $data['facebook_id'] = '111758029719193';
        $data['lang'] = 'sk';
        $data['name'] = 'Api';
        $data['surname'] = 'Tester';
        $data['pass'] = 'aa';
       
        $response = $this->call($url,'POST',$data);
        
        $list = array();

        //success
        $list['CORRECT'] = array('response' => $response,'params' => $data);
        
        //missing email
        $data['email'] = '';
        $response2 = $this->call($url,'POST',$data);
        $list['MISSING_EMAIL'] = array('response' => $response2,'params' => $data);
        
        //missing pass
        $data['email'] = 'api-test-'.md5(time().rand()).'@coachrufus.com';
        unset($data['pass']);
        $response3 = $this->call($url,'POST',$data);
        $list['MISSING_PASS'] = array('response' => $response3,'params' => $data);

        
        return $this->render('Coachrufus\Api\ApiModule:test:result.php', array(
                    'url' => $this->apiUrl.$url,
                    'list' => $list
        ));
    }
    
     public function registerFbAction()
    {
        $this->setup();
        $url = '/api/user/register';
        $data['apikey'] = 'app@40101499408386491';
        $data['email'] = 'fb-api-test@coachrufus.com';
        $data['facebook_id'] = '1122';
        $data['password'] = 'aa';
        $data['lang'] = 'sk';
        $response = $this->call($url,'POST',$data);

        
        return $this->render('Coachrufus\Api\ApiModule:test:result.php', array(
                    'url' => $url,
                    'response' => $response
        ));
    }
     public function registerGoogleAction()
    {
        $this->setup();
        $url = '/api/user/register';
        $data['apikey'] = 'app@40101499408386491';
        $data['email'] = 'google-api-test@coachrufus.com';
        $data['google_id'] = '3344';
        $data['password'] = 'aa';
        $data['lang'] = 'sk';
        $response = $this->call($url,'POST',$data);

        
        return $this->render('Coachrufus\Api\ApiModule:test:result.php', array(
                    'url' => $url,
                    'response' => $response
        ));
    }
    
    
    
    public function userListAction()
    {
        $this->setup();
        $url = '/api/team/user-list';
        $list = array();
        
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $response = $this->call($url,'GET',$data);
        $list['CORRECT'] = array('response' => $response,'params' => $data);
         
        return $this->render('Coachrufus\Api\ApiModule:test:result.php', array(
                    'url' => $this->apiUrl.$url,
                    'list' => $list
        ));
    }
    
    public function systemNotifyUnreadListAction()
    {
        $this->setup();
        $url = '/api/system-notify/unread';
        
        $list = array();

        //success
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $response = $this->call($url,'GET',$data);
        $list['CORRECT'] = array('response' => $response);
        
        //bad user id
        $data['user_id'] = '0';
        $response2 = $this->call($url,'GET',$data);
        $list['BAD_USER_ID'] = array('response' => $response2);
        
        //bad api key
        $data['apikey'] = '-';
        $data['user_id'] = '3';
        $response3 = $this->call($url,'GET',$data);
        $list['BAD_API_KEY'] = array('response' => $response3);
        
        //bad method
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $response4 = $this->call($url,'POST',$data);
        $list['BAD_METHOD'] = array('response' => $response4);

        return $this->render('Coachrufus\Api\ApiModule:test:result.php', array(
                    'url' => $this->apiUrl.$url,
                    'list' => $list
        ));
    }
    
    public function systemNotifyChangeStatusAction()
    {
        $this->setup();
        $url = '/api/system-notify/change-status';
        
        $list = array();

        //success single
        $data['apikey'] = 'app@40101499408386491';
        $data['messages_id'] =  json_encode(array('1109','1108'));
        $data['status'] =  'unread';
        $response = $this->call($url,'PUT',$data);
        $list['CORRECT_SINGLE'] = array('response' => $response);


        return $this->render('Coachrufus\Api\ApiModule:test:result.php', array(
                    'url' => $this->apiUrl.$url,
                    'list' => $list
        ));
    }
    
    public function contentHomeAction()
    {
        $this->setup();
        $url = '/api/content/home';
        
        
        $list = array();

        //success
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $response = $this->getResponse($url,'GET',$data);
        $list['CORRECT'] = array('response' => $response);

        //bad method
        $data['apikey'] = 'app@40101499408386491';
        $response4 = $this->getResponse($url,'POST',$data);
        $list['BAD_METHOD'] = array('response' => $response4);

        return $this->render('Coachrufus\Api\ApiModule:test:result.php', array(
                    'url' => $this->buildCallUrl($url,'GET',$data),
                    'list' => $list
        ));
    }
    
    public function currentEventsAction()
    {
        $this->setup();
        $url = '/api/content/events/current';
        
        $list = array();

        //success
        $data = array();
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        //$data['filters'] = json_encode(array('team_id' => array(1544)));
        $response = $this->getResponse($url,'GET',$data);
        $list['FULL LIST'] = array('response' => $response,'params' => $data,'url' => urldecode($this->buildCallUrl($url,'GET',$data)));
        
      
         //success
        $data = array();
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['mode'] = 'only_items';
        //$data['filters'] = json_encode(array('team_id' => array(1544)));
        $response = $this->getResponse($url,'GET',$data);
        $list['ONLY ITEMS LIST'] = array('response' => $response,'params' => $data,'url' => urldecode($this->buildCallUrl($url,'GET',$data)));
        
        
        //success 2
        $data = array();
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['filters'] = json_encode(array('team_id' => array('all')));
        $response2 = $this->call($url,'GET',$data);
        $list['CORRECT_ALL'] = array('response' => $response2,'params' => $data,'url' => urldecode($this->buildCallUrl($url,'GET',$data)));

        //bad method
        $data = array();
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['filters'] = json_encode(array('team_id' => array(1544)));
        $response4 = $this->call($url,'POST',$data);
        $list['BAD_METHOD'] = array('response' => $response4,'params' => $data,'url' => urldecode($this->buildCallUrl($url,'GET',$data)));

        return $this->render('Coachrufus\Api\ApiModule:test:result.php', array(
                    'url' => urldecode($this->buildCallUrl($url,'GET',$data)),
                    'list' => $list
        ));
    }
    
     public function eventsCalendarAction()
    {
        $this->setup();
        $url = '/api/content/events/calendar';
        
        $list = array();

        //success
        $data = array();
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $response = $this->getResponse($url,'GET',$data);
        $list['FULL LIST'] = array('response' => $response,'params' => $data,'url' => urldecode($this->buildCallUrl($url,'GET',$data)));
        
      
    

        return $this->render('Coachrufus\Api\ApiModule:test:result.php', array(
                    'url' => urldecode($this->buildCallUrl($url,'GET',$data)),
                    'list' => $list
        ));
    }
    
    

    public function attendanceAction()
    {
        $this->setup();
        $url = '/api/content/attendance';
        
        $list = array();

        //success
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
         $data['filters'] = json_encode(array('team_id' => array(3)));
        $response = $this->getResponse($url,'GET',$data);
        $list['CORRECT_FILTERED'] = array('response' => $response,'params' => $data,'url' => urldecode($this->buildCallUrl($url,'GET',$data)));
        
        //success 2
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['filters'] = json_encode(array('team_id' => array('all')));
        $response2 = $this->call($url,'GET',$data);
        $list['CORRECT_ALL'] = array('response' => $response2,'params' => $data);

        //bad method
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['filters'] = json_encode(array('team_id' => array(3)));
        $response4 = $this->call($url,'POST',$data);
        $list['BAD_METHOD'] = array('response' => $response4,'params' => $data);

        return $this->render('Coachrufus\Api\ApiModule:test:result.php', array(
                    'url' => urldecode($this->buildCallUrl($url,'GET',$data)),
                    'list' => $list
        ));
    }
    
     public function eventAttendanceAction()
    {

         $this->setup();
        $url = '/api/content/event-attendance';
        
        $list = array();

        //success
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['event_id'] = '1046';
        $data['event_current_date'] = '2018-05-08';
        
        
        $response = $this->getResponse($url,'GET',$data);
        
        $list['CORRECT_FILTERED'] = array('response' => $response,'params' => $data,'url' => urldecode($this->buildCallUrl($url,'GET',$data)));
        
       

        return $this->render('Coachrufus\Api\ApiModule:test:result.php', array(
                    'url' => urldecode($this->buildCallUrl($url,'GET',$data)),
                    'list' => $list
        ));
    }
    
     public function eventDetailAction()
    {

         $this->setup();
        $url = '/api/event/detail';
        
        $list = array();

        //success
        $data['apikey'] = 'app@40101499408386491';
        $data['uid'] = '3';
        $data['id'] = '1046';
        $data['date'] = '2018-05-08';
        
        
        $response = $this->getResponse($url,'GET',$data);
        
        $list['CORRECT_FILTERED'] = array('response' => $response,'params' => $data,'url' => urldecode($this->buildCallUrl($url,'GET',$data)));
        
       

        return $this->render('Coachrufus\Api\ApiModule:test:result.php', array(
                    'url' => urldecode($this->buildCallUrl($url,'GET',$data)),
                    'list' => $list
        ));
    }
    
     public function eventContentDetailAction()
    {

         $this->setup();
        $url = '/api/content/event/detail';
        
        $list = array();

        //success
        $data['apikey'] = 'app@40101499408386491';
        $data['uid'] = '3';
        $data['id'] = '194';
        $data['date'] = '2018-08-07';
        
        
        $response = $this->getResponse($url,'GET',$data);
        
        $list['CORRECT_FILTERED'] = array('response' => $response,'params' => $data,'url' => urldecode($this->buildCallUrl($url,'GET',$data)));
        
       

        return $this->render('Coachrufus\Api\ApiModule:test:result.php', array(
                    'url' => urldecode($this->buildCallUrl($url,'GET',$data)),
                    'list' => $list
        ));
    }

    
     public function gameLineupAction()
    {

         $this->setup();
        $url = '/api/content/game/lineup';
        
        $list = array();

        //success
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['event_id'] = '1046';
        $data['event_current_date'] = '2018-05-08';
        
        
        $response = $this->getResponse($url,'GET',$data);
        
        $list['CORRECT_FILTERED'] = array('response' => $response,'params' => $data,'url' => urldecode($this->buildCallUrl($url,'GET',$data)));
        
       

        return $this->render('Coachrufus\Api\ApiModule:test:result.php', array(
                    'url' => urldecode($this->buildCallUrl($url,'GET',$data)),
                    'list' => $list
        ));
    }
    
     public function gameLineupCreateAction()
    {

         $this->setup();
        $url = '/api/content/game/create-lineup';
        
        $list = array();

        //success
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['event_id'] = '1046';
        $data['event_current_date'] = '2018-05-08';
        $data['type'] = 'manual';
        
        
        $response = $this->getResponse($url,'POST',$data);
        
        $list['CORRECT_FILTERED'] = array('response' => $response,'params' => $data,'url' => urldecode($this->buildCallUrl($url,'POST',$data)));
        
       

        return $this->render('Coachrufus\Api\ApiModule:test:result.php', array(
                    'url' => urldecode($this->buildCallUrl($url,'GET',$data)),
                    'list' => $list
        ));
    }
    
    public function attendancePaymentsAction()
    {
        $this->setup();
        $url = '/api/content/payments';
        
        $list = array();

        //success
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
       //$data['filters'] = json_encode(array('team_id' => array(1544)));
        $response = $this->getResponse($url,'GET',$data);
        $list['CORRECT_FILTERED'] = array('response' => $response,'params' => $data,'url' => urldecode($this->buildCallUrl($url,'GET',$data)));
        
        //success 2
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        //$data['filters'] = json_encode(array('team_id' => array('all')));
        $response2 = $this->call($url,'GET',$data);
        $list['CORRECT_ALL'] = array('response' => $response2,'params' => $data);

        //bad method
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        //$data['filters'] = json_encode(array('team_id' => array(1544)));
        $response4 = $this->call($url,'POST',$data);
        $list['BAD_METHOD'] = array('response' => $response4,'params' => $data);

        return $this->render('Coachrufus\Api\ApiModule:test:result.php', array(
                    'url' => $this->apiUrl.$url,
                    'list' => $list
        ));
    }
    
    public function gamesAction()
    {
        $this->setup();
        $url = '/api/content/games/score';
        
        $list = array();

        //success
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
         $data['filters'] = json_encode(array('team_id' => array(1544)));
        $response = $this->getResponse($url,'GET',$data);
        $list['CORRECT_FILTERED'] = array('response' => $response,'params' => $data,'url' => urldecode($this->buildCallUrl($url,'GET',$data)));
        
        //success 2
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['filters'] = json_encode(array('team_id' => array('all')));
        $response2 = $this->call($url,'GET',$data);
        $list['CORRECT_ALL'] = array('response' => $response2,'params' => $data);

        //bad method
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['filters'] = json_encode(array('team_id' => array(1544)));
        $response4 = $this->call($url,'POST',$data);
        $list['BAD_METHOD'] = array('response' => $response4,'params' => $data);

        return $this->render('Coachrufus\Api\ApiModule:test:result.php', array(
                    'url' => $this->apiUrl.$url,
                    'list' => $list
        ));
    }
    
     public function timelineAction()
    {
        $this->setup();
        $url = '/api/content/game/timeline';
        
        $list = array();
        //success
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['event_id'] = '1046';
        $data['event_current_date'] = '2018-05-08';
        $data['lid'] = '1635';
        
        
        $response = $this->getResponse($url,'GET',$data);
        
        $list['CORRECT_FILTERED'] = array('response' => $response,'params' => $data,'url' => urldecode($this->buildCallUrl($url,'GET',$data)));
        
       

        return $this->render('Coachrufus\Api\ApiModule:test:result.php', array(
                    'url' => urldecode($this->buildCallUrl($url,'GET',$data)),
                    'list' => $list
        ));
    }
    
     public function simpleScoreAction()
    {
        $this->setup();
        $url = '/api/content/game/simple-score';
        
        $list = array();
        //success
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['event_id'] = '1046';
        $data['event_current_date'] = '2018-05-08';
        $data['lid'] = '1635';
        
        
        $response = $this->getResponse($url,'GET',$data);
        
        $list['CORRECT_FILTERED'] = array('response' => $response,'params' => $data,'url' => urldecode($this->buildCallUrl($url,'GET',$data)));
        
       

        return $this->render('Coachrufus\Api\ApiModule:test:result.php', array(
                    'url' => urldecode($this->buildCallUrl($url,'GET',$data)),
                    'list' => $list
        ));
    }
    
    
    
     public function userInfoAction()
    {
        $this->setup();
        $url = '/api/user/info';
        
        $list = array();
        //success
        $data['apikey'] = 'app@40101499408386491';
        $data['uid'] = '3';
      
        
        $response = $this->getResponse($url,'GET',$data);
        
        $list['CORRECT_FILTERED'] = array('response' => $response,'params' => $data,'url' => urldecode($this->buildCallUrl($url,'GET',$data)));
        
       

        return $this->render('Coachrufus\Api\ApiModule:test:result.php', array(
                    'url' => urldecode($this->buildCallUrl($url,'GET',$data)),
                    'list' => $list
        ));
    }
    
    
    
    public function resultsAction()
    {
        $this->setup();
        $url = '/api/content/games/results';
        
        $list = array();

        //success
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
         $data['filters'] = json_encode(array('team_id' => array(1544)));
        $response = $this->getResponse($url,'GET',$data);
        $list['CORRECT_FILTERED'] = array('response' => $response,'params' => $data,'url' => urldecode($this->buildCallUrl($url,'GET',$data)));
        
        //success 2
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['filters'] = json_encode(array('team_id' => array('all')));
        $response2 = $this->call($url,'GET',$data);
        $list['CORRECT_ALL'] = array('response' => $response2,'params' => $data);

        //bad method
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['filters'] = json_encode(array('team_id' => array(1544)));
        $response4 = $this->call($url,'POST',$data);
        $list['BAD_METHOD'] = array('response' => $response4,'params' => $data);

        return $this->render('Coachrufus\Api\ApiModule:test:result.php', array(
                    'url' => $this->apiUrl.$url,
                    'list' => $list
        ));
    }

    public function gameLineupWidgetAction()
    {
        $this->setup();
        $url = '/api/content/games/lineup/widget';
        
        $list = array();

        //success
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '4';
        $data['event_id'] = '1758';
        $data['event_date'] = '2018-11-25';
        $data['token']  = sha1($data['user_id'].$data['event_id']).'#'.sha1(time());
       // $requestData['id'] = base64_encode(json_encode($data));
        $requestData = $this->getRouter()->generateApiWidgetUrlData($data);

        $response = $this->getResponse($url,'GET',$requestData);
        $list['CORRECT'] = array('response' => $response,'params' => $data,'url' => urldecode($this->buildCallUrl($url,'GET',$requestData)));

        return $this->render('Coachrufus\Api\ApiModule:test:result.php', array(
                    'url' => $this->apiUrl.$url,
                    'list' => $list
        ));
        
    
    }
    
     public function scoreWidgetAction()
    {
        $this->setup();
        $url = '/api/content/games/score/widget';  
        $router = $this->getRouter();
        $list = array();
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['lineup_id'] = '1963';
        $data['event_id'] = '1929';
        $data['event_date'] = '2019-05-21';
        $data['token']  = sha1($data['user_id'].$data['event_id']).'#'.sha1(time());
         $requestData['id'] = base64_encode(json_encode($data));
        //success
         
        $url = $router->generateApiWidgetUrl('api_content_games_score_widget',$data); 
        $response = $this->getResponse($url,'GET',$requestData);
        $list['CORRECT'] = array('response' => $response,'params' => $data,'url' => $url);
        return $this->render('Coachrufus\Api\ApiModule:test:result.php', array(
                    'url' => $this->apiUrl.$url,
                    'list' => $list
        ));
    }
    
     public function simpleScoreWidgetAction()
    {
        $this->setup();
        $url = '/api/content/games/simple-score/widget';
        
        $list = array();
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '4';
        $data['lineup_id'] = '1863';
        $data['event_id'] = '1758';
        $data['event_date'] = '2018-11-25';
        $data['token']  = sha1($data['user_id'].$data['event_id']).'#'.sha1(time());

        //success
         $requestData = $this->getRouter()->generateApiWidgetUrlData($data);
        $response = $this->getResponse($url,'GET',$requestData);
        $list['CORRECT'] = array('response' => $response,'params' => $data,'url' => urldecode($this->buildCallUrl($url,'GET',$requestData)));
        
     

        return $this->render('Coachrufus\Api\ApiModule:test:result.php', array(
                    'url' => $this->apiUrl.$url,
                    'list' => $list
        ));
    }
    
    public function notifySetupAction()
    {
        $this->setup();
        $url = '/api/content/notify-setup';
        
        $list = array();

        //success
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['filters'] = json_encode(array('team_id' => array(1544)));
        $response = $this->getResponse($url,'GET',$data);
        $list['CORRECT_FILTERED'] = array('response' => $response,'params' => $data);
        
  

        return $this->render('Coachrufus\Api\ApiModule:test:result.php', array(
                    'url' => urldecode($this->buildCallUrl($url,'GET',$data)),
                    'list' => $list
        ));
    }
    
    
    public function contentMainMenuAction()
    {
        $this->setup();
        $url = '/api/content/main-menu';
        
        $list = array();

        //success
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['filters'] = json_encode(array('team_id' => array(1544)));
        $response = $this->getResponse($url,'GET',$data);
        $list['CORRECT_FILTERED'] = array('response' => $response,'params' => $data);
        
        //success 2
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['filters'] = json_encode(array('team_id' => array('all')));
        $response2 = $this->call($url,'GET',$data);
        $list['CORRECT_ALL'] = array('response' => $response2,'params' => $data);

        //bad method
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['filters'] = json_encode(array('team_id' => array(1544)));
        $response4 = $this->call($url,'POST',$data);
        $list['BAD_METHOD'] = array('response' => $response4,'params' => $data);

        return $this->render('Coachrufus\Api\ApiModule:test:result.php', array(
                    'url' => urldecode($this->buildCallUrl($url,'GET',$data)),
                    'list' => $list
        ));
    }
    
    public function contentFloatMenuAction()
    {
        $this->setup();
        $url = '/api/content/float-menu';
        
        $list = array();

        //success
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $response = $this->call($url,'GET',$data);
        $list['CORRECT'] = array('response' => $response,'params' => $data);
        
      


        return $this->render('Coachrufus\Api\ApiModule:test:result.php', array(
                    'url' => urldecode($this->buildCallUrl($url,'GET',$data)),
                    'list' => $list
        ));
    }
    public function hpWallPublishAction()
    {
        $this->setup();
        $url = '/api/hpwall/post/publish';
        
        $list = array();

        //success
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $response = $this->call($url,'GET',$data);
        $list['CORRECT'] = array('response' => $response,'params' => $data);
        
      


        return $this->render('Coachrufus\Api\ApiModule:test:result.php', array(
                    'url' => urldecode($this->buildCallUrl($url,'GET',$data)),
                    'list' => $list
        ));
    }
    
    public function contentChatAction()
    {
        $this->setup();
        $url = '/api/content/chat';
        
        $list = array();

        //success
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $response = $this->getResponse($url,'GET',$data);
        $list['CORRECT_FILTERED'] = array('response' => $response,'params' => $data);
        
      

        //bad method
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $response4 = $this->call($url,'POST',$data);
        $list['BAD_METHOD'] = array('response' => $response4,'params' => $data);

        return $this->render('Coachrufus\Api\ApiModule:test:result.php', array(
                    'url' => $this->apiUrl.$url,
                    'list' => $list
        ));
    }
    
    
    
    public function contentTeamCreateAction()
    {
        $this->setup();
        $url = '/api/content/team/create';
        
        $list = array();

        //success
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['filters'] = json_encode(array('team_id' => array(1544)));
        $response = $this->call($url,'GET',$data);
        $list['CORRECT_FILTERED'] = array('response' => $response,'params' => $data);
        
        //success 2
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['filters'] = json_encode(array('team_id' => array('all')));
        $response2 = $this->call($url,'GET',$data);
        $list['CORRECT_ALL'] = array('response' => $response2,'params' => $data);

        //bad method
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['filters'] = json_encode(array('team_id' => array(1544)));
        $response4 = $this->call($url,'POST',$data);
        $list['BAD_METHOD'] = array('response' => $response4,'params' => $data);

        return $this->render('Coachrufus\Api\ApiModule:test:result.php', array(
                    'url' => $this->apiUrl.$url,
                    'list' => $list
        ));
    }
    public function contentTeamPlayerCreateAction()
    {
        $this->setup();
        $url = '/api/content/team/player/create';
        
        $list = array();

        //success
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['filters'] = json_encode(array('team_id' => array(1544)));
        $response = $this->call($url,'GET',$data);

         $list['CORRECT_FILTERED'] = array('response' => $response,'params' => $data,'url' => urldecode($this->buildCallUrl($url,'GET',$data)));
         
        //success 2
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['filters'] = json_encode(array('team_id' => array('all')));
        $response2 = $this->call($url,'GET',$data);
        $list['CORRECT_ALL'] = array('response' => $response2,'params' => $data);

        //bad method
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['filters'] = json_encode(array('team_id' => array(1544)));
        $response4 = $this->call($url,'POST',$data);
        $list['BAD_METHOD'] = array('response' => $response4,'params' => $data);

        return $this->render('Coachrufus\Api\ApiModule:test:result.php', array(
                    'url' => urldecode($this->buildCallUrl($url,'GET',$data)),
                    'list' => $list
        ));
    }
    public function contentEventCreateAction()
    {
        $this->setup();
        $url = '/api/content/event/create';
        
        $list = array();

        //success
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['filters'] = json_encode(array('team_id' => array(1544)));
        $response = $this->call($url,'GET',$data);
        $list['CORRECT_FILTERED'] = array('response' => $response,'params' => $data);
        
        //success 2
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['filters'] = json_encode(array('team_id' => array('all')));
        $response2 = $this->call($url,'GET',$data);
        $list['CORRECT_ALL'] = array('response' => $response2,'params' => $data);

        //bad method
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['filters'] = json_encode(array('team_id' => array(1544)));
        $response4 = $this->call($url,'POST',$data);
        $list['BAD_METHOD'] = array('response' => $response4,'params' => $data);

        return $this->render('Coachrufus\Api\ApiModule:test:result.php', array(
                    'url' => $this->apiUrl.$url,
                    'list' => $list
        ));
    }
    public function contentEventScoreCreateAction()
    {
        $this->setup();
        $url = '/api/content/event/score/create';
        
        $list = array();

        //success
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['filters'] = json_encode(array('team_id' => array(1544)));
        $response = $this->call($url,'GET',$data);
        $list['CORRECT_FILTERED'] = array('response' => $response,'params' => $data);
        
        //success 2
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['filters'] = json_encode(array('team_id' => array('all')));
        $response2 = $this->call($url,'GET',$data);
        $list['CORRECT_ALL'] = array('response' => $response2,'params' => $data);

        //bad method
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['filters'] = json_encode(array('team_id' => array(1544)));
        $response4 = $this->call($url,'POST',$data);
        $list['BAD_METHOD'] = array('response' => $response4,'params' => $data);

        return $this->render('Coachrufus\Api\ApiModule:test:result.php', array(
                    'url' => $this->apiUrl.$url,
                    'list' => $list
        ));
    }
    public function contentTeamPaymentAction()
    {
        $this->setup();
        $url = '/api/content/team/payment';
        
        $list = array();

        //success
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['filters'] = json_encode(array('team_id' => array(1544)));
        $response = $this->call($url,'GET',$data);
        $list['CORRECT_FILTERED'] = array('response' => $response,'params' => $data);
        
        //success 2
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['filters'] = json_encode(array('team_id' => array('all')));
        $response2 = $this->call($url,'GET',$data);
        $list['CORRECT_ALL'] = array('response' => $response2,'params' => $data);

        //bad method
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['filters'] = json_encode(array('team_id' => array(1544)));
        $response4 = $this->call($url,'POST',$data);
        $list['BAD_METHOD'] = array('response' => $response4,'params' => $data);

        return $this->render('Coachrufus\Api\ApiModule:test:result.php', array(
                    'url' => $this->apiUrl.$url,
                    'list' => $list
        ));
    }
    public function addEventTeamChoiceAction()
    {
        $this->setup();
        $url = '/api/event/team-choice';
        
        $list = array();

        //success
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $response = $this->call($url,'GET',$data);
        $list['CORRECT_FILTERED'] = array('response' => $response,'params' => $data,'url' => urldecode($this->buildCallUrl($url,'GET',$data)));
     
        //bad method
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $response4 = $this->call($url,'POST',$data);
        $list['BAD_METHOD'] = array('response' => $response4,'params' => $data);

        return $this->render('Coachrufus\Api\ApiModule:test:result.php', array(
                    'url' => urldecode($this->buildCallUrl($url,'GET',$data)),
                    'list' => $list
        ));
        
       
    
        
        
    }
    public function scoutingAction()
    {
        $this->setup();
        $url = '/api/content/scouting';
        
        $list = array();

        //success
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['filters'] = json_encode(array('team_id' => array(1544)));
        $response = $this->call($url,'GET',$data);
        $list['CORRECT_FILTERED'] = array('response' => $response,'params' => $data);
        
        //success 2
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['filters'] = json_encode(array('team_id' => array('all')));
        $response2 = $this->call($url,'GET',$data);
        $list['CORRECT_ALL'] = array('response' => $response2,'params' => $data);

        //bad method
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['filters'] = json_encode(array('team_id' => array(1544)));
        $response4 = $this->call($url,'POST',$data);
        $list['BAD_METHOD'] = array('response' => $response4,'params' => $data);

        return $this->render('Coachrufus\Api\ApiModule:test:result.php', array(
                    'url' => $this->apiUrl.$url,
                    'list' => $list
        ));
    }
    public function tournamentAction()
    {
        $this->setup();
        $url = '/api/content/tournament';
        
        $list = array();

        //success
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['filters'] = json_encode(array('team_id' => array(1544)));
        $response = $this->call($url,'GET',$data);
        $list['CORRECT_FILTERED'] = array('response' => $response,'params' => $data);
        
        //success 2
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['filters'] = json_encode(array('team_id' => array('all')));
        $response2 = $this->call($url,'GET',$data);
        $list['CORRECT_ALL'] = array('response' => $response2,'params' => $data);

        //bad method
        $data['apikey'] = 'app@40101499408386491';
        $data['user_id'] = '3';
        $data['filters'] = json_encode(array('team_id' => array(1544)));
        $response4 = $this->call($url,'POST',$data);
        $list['BAD_METHOD'] = array('response' => $response4,'params' => $data);

        return $this->render('Coachrufus\Api\ApiModule:test:result.php', array(
                    'url' => $this->apiUrl.$url,
                    'list' => $list
        ));
    }
    public function seasonListAction()
    {
        $this->setup();
        $url = '/api/team/season';
        
        $list = array();

        //success
        $data['apikey'] = 'app@40101499408386491';
        $data['team_id'] = '3';
       
        $response = $this->call($url,'GET',$data);
        $list['CORRECT'] = array('response' => $response,'params' => $data);
        
    

        return $this->render('Coachrufus\Api\ApiModule:test:result.php', array(
                    'url' => $this->apiUrl.$url,
                    'list' => $list
        ));
    }
    
    
     public function forgottenPasswordAction()
    {

         $this->setup();
        $url = '/api/user/forgotten-password';
        
        $list = array();

        //success
        $data['apikey'] = 'app@40101499408386491';
        $data['uid'] = '3';
        
        
        $response = $this->getResponse($url,'GET',$data);
        
        $list['CORRECT_FILTERED'] = array('response' => $response,'params' => $data,'url' => urldecode($this->buildCallUrl($url,'GET',$data)));
        
       

        return $this->render('Coachrufus\Api\ApiModule:test:result.php', array(
                    'url' => urldecode($this->buildCallUrl($url,'GET',$data)),
                    'list' => $list
        ));
    }
    
    protected function buildCallUrl($apiMethod = null, $httpMethod = 'GET', $params = array())
    {
        $urlParams = '';
        $restParams = '';
       
        if (!empty($params) && 'GET' == $httpMethod)
        {
            $urlParams = '?' . http_build_query($params);
        }
        return $this->apiUrl . $apiMethod . $urlParams;

    }
    
    protected function getResponse($apiMethod = null, $httpMethod = 'GET', $params = array())
    {
        $ch = curl_init();

        $header = array( 
            'Accept: application/json',
        );
        $urlParams = '';
        $restParams = '';
        if ($httpMethod == 'POST')
        {
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
            //$header[] = 'Content-Type: application/x-www-form-urlencoded';
        }
        elseif ('PUT' == $httpMethod)
        {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode    ($params));
            
            curl_setopt($ch, CURLOPT_HTTPHEADER , array(
            "cache-control: no-cache",
            "content-type: application/json",
          ));
            
        }
        elseif ('DELETE' == $httpMethod)
        {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
           curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
        }
        else
        {
            //$header[] = 'Content-Type: application/json';
            /*
            if (!empty($params))
            {
                $urlParams = '?' . http_build_query($params);
            }
             * 
             */
        }
        $restUrl = $this->buildCallUrl($apiMethod, $httpMethod, $params);


        //$restUrl = $this->apiUrl . $apiMethod . $urlParams;


        curl_setopt($ch, CURLOPT_URL, $restUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        curl_close($ch);

        if (RUN_ENV == 'dev')
        {

            t_dump('API URL:' . $restUrl);
            t_dump('API PARAMS:');
            t_dump($params);
            t_dump('API RAW RESPONSE: ' . $response);
            t_dump(json_decode($response, true));
        }
        
        return $response;
    }

    protected function call($apiMethod = null, $httpMethod = 'GET', $params = array())
    {
        $response = $this->getResponse($apiMethod,$httpMethod,$params);
        return json_decode($response, true);
    }

}
