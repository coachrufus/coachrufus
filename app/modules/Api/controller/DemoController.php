<?php

namespace Coachrufus\Api\Controller;

use Core\ServiceLayer;

class DemoController extends \Core\Controller {

    private $apiUrl = WEB_DOMAIN;
    
    private function setup()
    {
        ServiceLayer::getService('layout')->setTemplate(null);
    }

    public function indexAction()
    {
        //$this->setup();
        ServiceLayer::getService('layout')->setTemplate(GLOBAL_DIR.'/templates/ClearLayout.php');
        //$layout->setTemplate(GLOBAL_DIR.'/templates/ClearLayout.php');

        return $this->render('Coachrufus\Api\ApiModule:demo:index.php', array(
                   
        ));
    }

   
}
