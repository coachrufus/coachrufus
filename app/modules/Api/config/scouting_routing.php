<?php
$acl->allowRole(array('guest'),'api_scouting_widget');
$router->addRoute('api_scouting_widget','/api/scoutingWidget',array(
		'controller' => 'Coachrufus\Api\ApiModule:Scouting:scoutingWidget'
));

$acl->allowRole(array('guest'),'api_scouting_slider_data');
$router->addRoute('api_scouting_slider_data','/api/scouting/widget/slider-data',array(
		'controller' => 'Coachrufus\Api\ApiModule:Scouting:sliderWidget'
));

$acl->allowRole(array('guest'),'api_content_scouting');
$router->addRoute('api_content_scouting','/api/content/scouting',array(
		'controller' => 'Coachrufus\Api\ApiModule:Scouting:index'
));

