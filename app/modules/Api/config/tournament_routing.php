<?php

$acl->allowRole(array('guest'),'api_content_tournament');
$router->addRoute('api_content_tournament','/api/content/tournament',array(
		'controller' => 'Coachrufus\Api\ApiModule:Content:tournament'
));



/**
 * Get  tournament detail
 */
$acl->allowRole('guest','api_tournament_detail');
$router->addRoute('api_tournament_detail','/api/tournament/detail',array(
		'controller' => '\Coachrufus\Api\ApiModule:Tournament:detail'
));
//TEST
$acl->allowRole('guest','api_tournament_detail_test');
$router->addRoute('api_tournament_detail_test','/api/test/tournament/detail',array(
		'controller' => '\Coachrufus\Api\ApiModule:TournamentTest:detail'
));

/**
 * Get  tournament stats
 */
$acl->allowRole('guest','api_tournament_stats');
$router->addRoute('api_tournament_stats','/api/tournament/stats',array(
		'controller' => '\Coachrufus\Api\ApiModule:Tournament:stats'
));
//TEST
$acl->allowRole('guest','api_tournament_stats_test');
$router->addRoute('api_tournament_stats_test','/api/test/tournament/stats',array(
		'controller' => '\Coachrufus\Api\ApiModule:TournamentTest:stats'
));

/**
 * Get  tournament players stats
 */
$acl->allowRole('guest','api_tournament_players_stats');
$router->addRoute('api_tournament_players_stats','/api/tournament/players-stats',array(
		'controller' => '\Coachrufus\Api\ApiModule:Tournament:playersStats'
));
//TEST
$acl->allowRole('guest','api_tournament_players_stats_test');
$router->addRoute('api_tournament_players_stats_test','/api/test/tournament/players-stats',array(
		'controller' => '\Coachrufus\Api\ApiModule:TournamentTest:playersStats'
));



/**
 * Get list of tournament teams
 */
$acl->allowRole('guest','api_tournament_teams');
$router->addRoute('api_tournament_teams','/api/tournament/teams',array(
		'controller' => '\Coachrufus\Api\ApiModule:Tournament:teams'
));
//TEST
$acl->allowRole('guest','api_tournament_teams_test');
$router->addRoute('api_tournament_teams_test','/api/test/tournament/teams',array(
		'controller' => '\Coachrufus\Api\ApiModule:TournamentTest:teams'
));

/**
 * Get list of tournament players
 */
$acl->allowRole('guest','api_tournament_teams_players');
$router->addRoute('api_tournament_teams_players','/api/tournament/teams/players',array(
		'controller' => '\Coachrufus\Api\ApiModule:Tournament:teamsPlayers'
));
//TEST
$acl->allowRole('guest','api_tournament_teams_players_test');
$router->addRoute('api_tournament_teams_players_test','/api/test/tournament/teams/players',array(
		'controller' => '\Coachrufus\Api\ApiModule:TournamentTest:teamsPlayers'
));


/**
 * Get list of tournament events
 */
$acl->allowRole('guest','api_tournament_events');
$router->addRoute('api_tournament_events','/api/tournament/events',array(
		'controller' => '\Coachrufus\Api\ApiModule:Tournament:events'
));
//TEST
$acl->allowRole('guest','api_tournament_events_test');
$router->addRoute('api_tournament_events_test','/api/test/tournament/events',array(
		'controller' => '\Coachrufus\Api\ApiModule:TournamentTest:events'
));


/**
 * subscribe user to game
 */
$acl->allowRole('guest','api_tournament_events_subscribe_user');
$router->addRoute('api_tournament_events_subscribe_user','/api/tournament/events-subscribe',array(
		'controller' => '\Coachrufus\Api\ApiModule:Tournament:subscribeUserEvent'
));
//TEST
$acl->allowRole('guest','api_tournament_events_subscribe_user_test');
$router->addRoute('api_tournament_events_subscribe_user_test','/api/test/tournament/events-subscribe',array(
		'controller' => '\Coachrufus\Api\ApiModule:TournamentTest:subscribeUserEvent'
));

/**
 * Get  tournament event detail
 */
$acl->allowRole('guest','api_tournament_events_detail');
$router->addRoute('api_tournament_events_detail','/api/tournament/events/:event_id',array(
		'controller' => '\Coachrufus\Api\ApiModule:Tournament:eventsDetail'
));
//TEST
$acl->allowRole('guest','api_tournament_events_detail_test');
$router->addRoute('api_tournament_events_detail_test','/api/test/tournament/events/detail',array(
		'controller' => '\Coachrufus\Api\ApiModule:TournamentTest:eventsDetail'
));


/**
 * Get list of running tournament events
 */
$acl->allowRole('guest','api_tournament_live_events');
$router->addRoute('api_tournament_live_events','/api/tournament/live/events',array(
		'controller' => '\Coachrufus\Api\ApiModule:Tournament:liveEvents'
));
//TEST
$acl->allowRole('guest','api_tournament_live_events_test');
$router->addRoute('api_tournament_events_test','/api/test/tournament/live/events',array(
		'controller' => '\Coachrufus\Api\ApiModule:TournamentTest:liveEvents'
));

/**
 * 
 * Get current timelineEvent
 */
$acl->allowRole('guest','api_tournament_current_timeline_event');
$router->addRoute('api_tournament_current_timeline_event','/api/tournament/live/currentTimelineEvent',array(
		'controller' => '\Coachrufus\Api\ApiModule:Tournament:currentTimelineEvent'
));
//TEST
$acl->allowRole('guest','api_tournament_current_timeline_event_test');
$router->addRoute('api_tournament_current_timeline_event_test','/api/test/tournament/live/currentTimelineEvent',array(
		'controller' => '\Coachrufus\Api\ApiModule:TournamentTest:currentTimelineEvent'
));