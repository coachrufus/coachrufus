<?php

$acl->allowRole('guest','rest_system_notify_unread');
$router->addRoute('rest_system_notify_unread','/api/system-notify/unread',array(
		'controller' => '\Coachrufus\Api\ApiModule:SystemNotify:unreadList'
));

$acl->allowRole('guest','rest_system_notify_change_status');
$router->addRoute('rest_system_notify_change_status','/api/system-notify/change-status',array(
		'controller' => '\Coachrufus\Api\ApiModule:SystemNotify:changeStatus'
));


$acl->allowRole(array('guest'),'api_system_notify_setup');
$router->addRoute('api_system_notify_setup','/api/system-notify/setup',array(
		'controller' => 'Coachrufus\Api\ApiModule:SystemNotify:setup'
));