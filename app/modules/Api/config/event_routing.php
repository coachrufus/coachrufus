<?php

$acl->allowRole(array('guest'),'api_content_event_create');
$router->addRoute('api_content_event_create','/api/content/event/create',array(
		'controller' => 'Coachrufus\Api\ApiModule:Event:create'
));

$acl->allowRole(array('guest'),'api_content_event_score_create');
$router->addRoute('api_content_event_score_create','/api/content/event/score/create',array(
		'controller' => 'Coachrufus\Api\ApiModule:Event:scoreCreate'
));

/*
 * @package \Coachrufus\Api\Test
 * Current events test
 */
$acl->allowRole('guest','api_content_event_current_test');
$router->addRoute('api_content_event_current_test','/api/test/content/events/current',array(
		'controller' => '\Coachrufus\Api\ApiModule:Test:currentEvents'
));

/*
 * @package \Coachrufus\Api
 * Current events
 */

$acl->allowRole(array('guest'),'api_content_event_current');
$router->addRoute('api_content_event_current','/api/content/events/current',array(
		'controller' => 'Coachrufus\Api\ApiModule:Event:currentEvents'
));

/*
 * @package \Coachrufus\Api\Test
 * Events calendar
 */
$acl->allowRole(array('guest'),'api_content_event_calendar_test');
$router->addRoute('api_content_event_calendar_test','/api/test/content/events/calendar',array(
		'controller' => 'Coachrufus\Api\ApiModule:Test:eventsCalendar'
));

/*
 * @package \Coachrufus\Api
 * Events calendar
 */
$acl->allowRole(array('guest'),'api_content_event_calendar');
$router->addRoute('api_content_event_calendar','/api/content/events/calendar',array(
		'controller' => 'Coachrufus\Api\ApiModule:Event:calendar'
));


/*
 * @package \Coachrufus\Api
 * Events calendar month
 */
$acl->allowRole(array('guest'),'api_content_event_calendar_month');
$router->addRoute('api_content_event_calendar_month','/api/content/events/calendar-month',array(
		'controller' => 'Coachrufus\Api\ApiModule:Event:calendarMonth'
));


/*
<<<<<<< HEAD
=======
 * @package \Coachrufus\Api\Test
 * Events attendance
 */
$acl->allowRole('guest','api_content_attendance_test');
$router->addRoute('api_content_attendance_test','/api/test/content/attendance',array(
		'controller' => '\Coachrufus\Api\ApiModule:Test:attendance'
));

/*
>>>>>>> api
 * @package \Coachrufus\Api
 * Events attendance
 */
$acl->allowRole(array('guest'),'api_content_attendance');
$router->addRoute('api_content_attendance','/api/content/attendance',array(
		'controller' => 'Coachrufus\Api\ApiModule:Attendance:index'
));


/*
 * @package \Coachrufus\Api\Test
 * Event attendance
 */
$acl->allowRole('guest','api_content_event_attendance_test');
$router->addRoute('api_content_event_attendance_test','/api/test/content/event-attendance',array(
		'controller' => '\Coachrufus\Api\ApiModule:Test:eventAttendance'
));


/*
 * @package \Coachrufus\Api
 * Events attendance
 */
$acl->allowRole(array('guest'),'api_content_event_attendance');
$router->addRoute('api_content_event_attendance','/api/content/event-attendance',array(
		'controller' => 'Coachrufus\Api\ApiModule:Attendance:eventAttendance'
));






/*
 * @package \Coachrufus\Api\Test
 * Events attendance payments
 */
$acl->allowRole(array('guest'),'api_content_attendance_payments');
$router->addRoute('api_content_attendance_payments','/api/content/payments',array(
		'controller' => 'Coachrufus\Api\ApiModule:Attendance:payments'
));


/*
 * @package \Coachrufus\Api
 * Events attendance payments
 */
$acl->allowRole('guest','api_content_attendance_payments_test');
$router->addRoute('api_content_attendance_payments_test','/api/test/content/payments',array(
		'controller' => '\Coachrufus\Api\ApiModule:Test:attendancePayments'
));


/*
 * @package \Coachrufus\Api\Test
 * 
 */
$acl->allowRole(array('guest'),'api_event_detail_test');
$router->addRoute('api_event_detail_test','/api/test/event/detail',array(
		'controller' => 'Coachrufus\Api\ApiModule:Test:eventDetail'
));


/*
 * @package \Coachrufus\Api\Test
 * 
 */
$acl->allowRole(array('guest'),'api_content_event_detail_test');
$router->addRoute('api_content_event_detail_test','/api/test/content/event/detail',array(
		'controller' => 'Coachrufus\Api\ApiModule:Test:eventContentDetail'
));

/*
 * @package \Coachrufus\Api\Test
 * 
 */
$acl->allowRole(array('guest'),'api_content_event_detail');
$router->addRoute('api_content_event_detail','/api/content/event/detail',array(
		'controller' => 'Coachrufus\Api\ApiModule:Event:eventContentDetail'
));



/*
 * @package \Coachrufus\Api
 * Add event choice test 
 */
$acl->allowRole('guest','api_content_add_event_team_choice_test');
$router->addRoute('api_content_add_event_team_choice_test','/api/test/event/team-choice',array(
		'controller' => '\Coachrufus\Api\ApiModule:Test:addEventTeamChoice'
));


/*
 * @package \Coachrufus\Api\Test
 * add event choice
 */
$acl->allowRole(array('guest'),'api_content_add_event_team_choice');
$router->addRoute('api_content_add_event_team_choice','/api/event/team-choice',array(
		'controller' => 'Coachrufus\Api\ApiModule:Event:addEventTeamChoice'
));
