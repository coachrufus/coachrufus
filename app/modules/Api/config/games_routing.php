<?php

/*
 * @package \Coachrufus\Api\Test
 * Games score. 
 */
$acl->allowRole('guest','api_content_games_test');
$router->addRoute('api_content_games_test','/api/test/content/games/score',array(
		'controller' => '\Coachrufus\Api\ApiModule:Test:games'
));

/*
 * @package \Coachrufus\Api
 * Games score. Only events newest then today
 */
$acl->allowRole(array('guest'),'api_content_games_score');
$router->addRoute('api_content_games_score','/api/content/games/score',array(
		'controller' => 'Coachrufus\Api\ApiModule:Games:scoreList'
));


/*
 * @package \Coachrufus\Api\Test
 * Games result. Only older events
 */
$acl->allowRole('guest','api_content_games_results_test');
$router->addRoute('api_content_games_results_test','/api/test/content/games/results',array(
		'controller' => '\Coachrufus\Api\ApiModule:Test:results'
));

/*
 * @package \Coachrufus\Api
 * Games result
 */
$acl->allowRole(array('guest'),'api_content_games_results');
$router->addRoute('api_content_games_results','/api/content/games/results',array(
		'controller' => 'Coachrufus\Api\ApiModule:Games:results'
));




/*
 * @package \Coachrufus\Api
 * Games stats
 */
$acl->allowRole(array('guest'),'api_content_games_stats_data');
$router->addRoute('api_content_games_stats_data','/api/content/games/stats-data',array(
		'controller' => 'Coachrufus\Api\ApiModule:Games:statsData'
));



/*
 * @package \Coachrufus\Api\Test
 * create Lineup widget  test
 */
$acl->allowRole('guest','api_content_games_lineup_widget_test');
$router->addRoute('api_content_games_lineup_widget_test','/api/test/content/games/lineup/widget',array(
		'controller' => '\Coachrufus\Api\ApiModule:Test:gameLineupWidget'
));



/*
 * @package \Coachrufus\Api\Test
 * create Lineup widget 
 */
$acl->allowRole('guest','api_content_games_lineup_widget');
$router->addRoute('api_content_games_lineup_widget','/api/content/games/lineup/widget',array(
		'controller' => '\Coachrufus\Api\ApiModule:Games:lineupWidget'
));


/*
 * @package \Coachrufus\Api\Test
 * create Score widget  test
 */
$acl->allowRole('guest','api_content_games_score_widget_test');
$router->addRoute('api_content_games_score_widget_test','/api/test/content/games/score/widget',array(
		'controller' => '\Coachrufus\Api\ApiModule:Test:scoreWidget'
));



/*
 * @package \Coachrufus\Api\Test
 * create Lineup widget 
 */
$acl->allowRole('guest','api_content_games_score_widget');
$router->addRoute('api_content_games_score_widget','/api/content/games/score/widget',array(
		'controller' => '\Coachrufus\Api\ApiModule:Games:scoreWidget'
));



/*
 * @package \Coachrufus\Api\Test
 * create simple Score widget  test
 */
$acl->allowRole('guest','api_content_games_simplescore_widget_test');
$router->addRoute('api_content_games_simplescore_widget_test','/api/test/content/games/simple-score/widget',array(
		'controller' => '\Coachrufus\Api\ApiModule:Test:simpleScoreWidget'
));



/*
 * @package \Coachrufus\Api\Test
 * create Lineup widget 
 */
$acl->allowRole('guest','api_content_games_simplescore_widget');
$router->addRoute('api_content_games_simplescore_widget','/api/content/games/simple-score/widget',array(
		'controller' => '\Coachrufus\Api\ApiModule:Games:simpleScoreWidget'
));



/*
 * @package \Coachrufus\Api\Test
 * Event lineup data
 */
$acl->allowRole('guest','api_content_games_linuep_test');
$router->addRoute('api_content_games_linuep_test','/api/test/content/game/lineup',array(
		'controller' => '\Coachrufus\Api\ApiModule:Test:gameLineup'
));


/*
 * @package \Coachrufus\Api
 * Events attendance
 */
$acl->allowRole(array('guest'),'api_content_games_linuep');
$router->addRoute('api_content_games_linuep','/api/content/game/lineup',array(
		'controller' => 'Coachrufus\Api\ApiModule:Games:lineup'
));




/*
 * @package \Coachrufus\Api\Test
 * Event lineup create
 */
$acl->allowRole('guest','api_content_games_linuep_create_test');
$router->addRoute('api_content_games_linuep_create_test','/api/test/content/game/create-lineup',array(
		'controller' => '\Coachrufus\Api\ApiModule:Test:gameLineupCreate'
));

/*
 * @package \Coachrufus\Api
 * Event lineup create
 */
$acl->allowRole(array('guest'),'api_content_games_linuep_create');
$router->addRoute('api_content_games_linuep_create','/api/content/game/create-lineup',array(
		'controller' => 'Coachrufus\Api\ApiModule:Games:createLineup'
));


/*
 * @package \Coachrufus\Api
 * Event lineup create
 */
$acl->allowRole(array('guest'),'api_content_games_start_match');
$router->addRoute('api_content_games_start_match','/api/content/game/start-match',array(
		'controller' => 'Coachrufus\Api\ApiModule:Games:startMatch'
));

/*
 * @package \Coachrufus\Api
 * Event lineup create
 */
$acl->allowRole(array('guest'),'api_content_games_pause_match');
$router->addRoute('api_content_games_pause_match','/api/content/game/pause-match',array(
		'controller' => 'Coachrufus\Api\ApiModule:Games:pauseMatch'
));



/*
 * @package \Coachrufus\Api\Test
 * Get game timeline
 */
$acl->allowRole(array('guest'),'api_content_games_timeline_test');
$router->addRoute('api_content_games_timeline_test','/api/test/content/game/timeline',array(
		'controller' => 'Coachrufus\Api\ApiModule:Test:timeline'
));

/*
 * @package \Coachrufus\Api
 * Get game timeline
 */
$acl->allowRole(array('guest'),'api_content_games_timeline');
$router->addRoute('api_content_games_timeline','/api/content/game/timeline',array(
		'controller' => 'Coachrufus\Api\ApiModule:Games:timeline'
));


/*
 * @package \Coachrufus\Api\Test
 * simple score
 */
$acl->allowRole(array('guest'),'api_content_games_simple_score_test');
$router->addRoute('api_content_games_simple_score_test','/api/test/content/game/simple-score',array(
		'controller' => 'Coachrufus\Api\ApiModule:Test:simpleScore'
));

/*
 * @package \Coachrufus\Api
 * Get game timeline
 */
$acl->allowRole(array('guest'),'api_content_games_simple_score');
$router->addRoute('api_content_games_simple_score','/api/content/game/simple-score',array(
		'controller' => 'Coachrufus\Api\ApiModule:Games:simpleScore'
));