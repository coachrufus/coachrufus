<?php



$acl->allowRole('guest','rest_user_register');
$router->addRoute('rest_user_register','/api/user/register',array(
		'controller' => '\Coachrufus\Api\ApiModule:User:register'
));

$acl->allowRole('guest','rest_user_register_fb');
$router->addRoute('rest_user_register_fb','/api/user/register-fb',array(
		'controller' => '\Coachrufus\Api\ApiModule:User:registerFb'
));

$acl->allowRole('guest','rest_user_register_google');
$router->addRoute('rest_user_register_google','/api/user/register-google',array(
		'controller' => '\Coachrufus\Api\ApiModule:User:registerGoogle'
));

$acl->allowRole('guest','rest_user_login');
$router->addRoute('rest_user_login','/api/user/login',array(
		'controller' => '\Coachrufus\Api\ApiModule:User:login'
));

$acl->allowRole('guest','rest_user_login_fb');
$router->addRoute('rest_user_login_fb','/api/user/login-fb',array(
		'controller' => '\Coachrufus\Api\ApiModule:User:loginFb'
));

$acl->allowRole('guest','rest_user_login_google');
$router->addRoute('rest_user_login_google','/api/user/login-google',array(
		'controller' => '\Coachrufus\Api\ApiModule:User:loginGoogle'
));

$acl->allowRole('guest','rest_user_auth_info');
$router->addRoute('rest_user_auth_info','/api/user/auth-info',array(
		'controller' => '\Coachrufus\Api\ApiModule:User:authInfo'
));

$acl->allowRole('guest','rest_user_info_test');
$router->addRoute('rest_user_info_test','/api/test/user/info',array(
		'controller' => '\Coachrufus\Api\ApiModule:Test:userInfo'
));

$acl->allowRole('guest','rest_user_info');
$router->addRoute('rest_user_info','/api/user/info',array(
		'controller' => '\Coachrufus\Api\ApiModule:User:userInfo'
));


$acl->allowRole('guest','api_user_forgotten_pass_test');
$router->addRoute('api_user_forgotten_pass_test','/api/test/user/forgotten',array(
		'controller' => '\Coachrufus\Api\ApiModule:Test:forgottenPassword'
));

$acl->allowRole('guest','api_user_forgotten_pass');
$router->addRoute('api_user_forgotten_pass','/api/user/forgotten-password',array(
		'controller' => '\Coachrufus\Api\ApiModule:User:forgottenPassword'
));

/*MARKETING AGREE*/
$acl->allowRole('guest','api_user_privacy_test');
$router->addRoute('api_user_privacy_test','/api/test/user/privacy',array(
		'controller' => '\Coachrufus\Api\ApiModule:UserTest:privacy'
));

$acl->allowRole('guest','api_user_privacy');
$router->addRoute('api_user_privacy','/api/user/privacy',array(
		'controller' => '\Coachrufus\Api\ApiModule:User:privacy'
));
