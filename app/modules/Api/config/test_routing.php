<?php

$acl->allowRole('guest','rest_list_test');
$router->addRoute('rest_list_test','/api/test/list',array(
		'controller' => '\Coachrufus\Api\ApiModule:Test:list'
));

/*USER*/
$acl->allowRole('guest','rest_user_login_test');
$router->addRoute('rest_user_login_test','/api/test/user/login',array(
		'controller' => '\Coachrufus\Api\ApiModule:Test:login'
));

$acl->allowRole('guest','rest_user_login_fb_test');
$router->addRoute('rest_user_login_fb_test','/api/test/user/login-fb',array(
		'controller' => '\Coachrufus\Api\ApiModule:Test:loginFb'
));

$acl->allowRole('guest','rest_user_login_google_test');
$router->addRoute('rest_user_login_google_test','/api/test/user/login-google',array(
		'controller' => '\Coachrufus\Api\ApiModule:Test:loginGoogle'
));

$acl->allowRole('guest','rest_user_register_test');
$router->addRoute('rest_user_register_test','/api/test/user/register',array(
		'controller' => '\Coachrufus\Api\ApiModule:Test:register'
));

$acl->allowRole('guest','rest_user_register_fb_test');
$router->addRoute('rest_user_register_fb_test','/api/test/user/register-fb',array(
		'controller' => '\Coachrufus\Api\ApiModule:Test:registerFB'
));

$acl->allowRole('guest','rest_user_register_google_test');
$router->addRoute('rest_user_register_google_test','/api/test/user/register-google',array(
		'controller' => '\Coachrufus\Api\ApiModule:Test:registerGoogle'
));


/*TEAM*/
$acl->allowRole('guest','rest_team_user_list_test');
$router->addRoute('rest_team_user_list_test','/api/test/team/user-list',array(
		'controller' => '\Coachrufus\Api\ApiModule:Test:userList'
));

$acl->allowRole('guest','api_team_season_test');
$router->addRoute('api_team_season_test','/api/test/team/season-list',array(
		'controller' => '\Coachrufus\Api\ApiModule:Test:seasonList'
));

/*SYSTEM NOTIFY*/
$acl->allowRole('guest','rest_system_notify_unread_test');
$router->addRoute('rest_system_notify_unread_test','/api/test/system-notify/unread',array(
		'controller' => '\Coachrufus\Api\ApiModule:Test:systemNotifyUnreadList'
));

$acl->allowRole('guest','rest_system_notify_change_status_test');
$router->addRoute('rest_system_notify_change_status_test','/api/test/system-notify/change-status',array(
		'controller' => '\Coachrufus\Api\ApiModule:Test:systemNotifyChangeStatus'
));

include_once 'test_content_routing.php';




