<?php
$acl->allowRole(array('guest'),'api_content_webview');
$router->addRoute('api_content_webview','/api/content/webview',array(
		'controller' => 'Coachrufus\Api\ApiModule:Content:webview'
));


/*
 * @package \Coachrufus\Api\Test
 * home  test
 */
$acl->allowRole('guest','api_content_home_test');
$router->addRoute('api_content_home_test','/api/test/content/home',array(
		'controller' => '\Coachrufus\Api\ApiModule:Test:contentHome'
));

/*
 * @package \Coachrufus\Api
 * home 
 */
$acl->allowRole(array('guest'),'api_content_home');
$router->addRoute('api_content_home','/api/content/home',array(
		'controller' => 'Coachrufus\Api\ApiModule:Content:home'
));

/*
 * @package \Coachrufus\Api\Test
 * main menu test
 */
$acl->allowRole('guest','api_content_main_menu_test');
$router->addRoute('api_content_main_menu_test','/api/test/content/main-menu',array(
		'controller' => '\Coachrufus\Api\ApiModule:Test:contentMainMenu'
));

/*
 * @package \Coachrufus\Api
 * main menu
 */
$acl->allowRole(array('guest'),'api_content_main_menu');
$router->addRoute('api_content_main_menu','/api/content/main-menu',array(
		'controller' => 'Coachrufus\Api\ApiModule:Content:mainMenu'
));


$acl->allowRole(array('guest'),'api_content_float_menu');
$router->addRoute('api_content_float_menu','/api/content/float-menu',array(
		'controller' => 'Coachrufus\Api\ApiModule:Content:floatMenu'
));


$acl->allowRole(array('guest'),'api_content_chat');
$router->addRoute('api_content_chat','/api/content/chat',array(
		'controller' => 'Coachrufus\Api\ApiModule:Content:chat'
));


$acl->allowRole(array('guest'),'api_content_notify');
$router->addRoute('api_content_notify','/api/content/notify',array(
		'controller' => 'Coachrufus\Api\ApiModule:Content:notify'
));


/*
 * @package \Coachrufus\Api\Test
 * notify setup  test
 */
$acl->allowRole('guest','api_content_notify_setup_test');
$router->addRoute('api_content_notify_setup_test','/api/test/content/notify-setup',array(
		'controller' => '\Coachrufus\Api\ApiModule:Test:notifySetup'
));

/*
 * @package \Coachrufus\Api
 * notify setup 
 */
$acl->allowRole(array('guest'),'api_content_notify_setup');
$router->addRoute('api_content_notify_setup','/api/content/notify-setup',array(
		'controller' => 'Coachrufus\Api\ApiModule:Content:notifySetup'
));
