<?php


/*rest*/
$acl->allowRole('guest','rest_team_user_list');
$router->addRoute('rest_team_user_list','/api/team/user-list',array(
		'controller' => '\Coachrufus\Api\ApiModule:Team:userTeams'
));


$acl->allowRole(array('guest'),'api_team_season');
$router->addRoute('api_team_season','/api/team/season',array(
		'controller' => 'Coachrufus\Api\ApiModule:Team:season'
));




/*content*/
$acl->allowRole(array('guest'),'api_content_team_missing');
$router->addRoute('api_content_team_missing','/api/content/team/missing',array(
		'controller' => 'Coachrufus\Api\ApiModule:Team:missingTeam'
));


$acl->allowRole(array('guest'),'api_content_team_create');
$router->addRoute('api_content_team_create','/api/content/team/create',array(
		'controller' => 'Coachrufus\Api\ApiModule:Team:create'
));

/*
 * @package \Coachrufus\Api\Test
 * create player test
 */
$acl->allowRole('guest','api_content_team_player_create_test');
$router->addRoute('api_content_team_player_create_test','/api/test/content/team/player/create',array(
		'controller' => '\Coachrufus\Api\ApiModule:Test:contentTeamPlayerCreate'
));

/*
 * @package \Coachrufus\Api\Team
 * create player
 */

$acl->allowRole(array('guest'),'api_content_team_player_create');
$router->addRoute('api_content_team_player_create','/api/content/team/player/create',array(
		'controller' => 'Coachrufus\Api\ApiModule:Team:playerCreate'
));

$acl->allowRole(array('guest'),'api_content_team_payment');
$router->addRoute('api_content_team_payment','/api/content/team/payment',array(
		'controller' => 'Coachrufus\Api\ApiModule:Team:payment'
));



$acl->allowRole(array('guest'),'api_team_change_player_status');
$router->addRoute('api_team_change_player_status','/api/team/player/changeStatus',array(
		'controller' => 'Coachrufus\Api\ApiModule:Team:changePlayerStatus'
));




