<?php


/*
 * @package \Coachrufus\Api\Test
 * publish post
 */
$acl->allowRole('guest','api_hpwall_post_publish_test');
$router->addRoute('api_hpwall_post_publish_test','/api/test/hpwall/post/publish',array(
		'controller' => '\Coachrufus\Api\ApiModule:Test:hpWallPublish'
));


/*
 * @package \Coachrufus\Api
 * Games result
 */

$acl->allowRole(array('guest'),'api_hpwall_post_publish');
$router->addRoute('api_hpwall_post_publish','/api/hpwall/post/publish',array(
		'controller' => 'Coachrufus\Api\ApiModule:HpWall:postPublish'
));

/*
 * @package \Coachrufus\Api\Test
 * post rest test
 */
$acl->allowRole(array('guest'),'api_hpwall_post_create');
$router->addRoute('api_hpwall_post_create','/api/hpwall/post',array(
		'controller' => 'Coachrufus\Api\ApiModule:HpWall:post'
));

$acl->allowRole(array('guest'),'api_hpwall_post');
$router->addRoute('api_hpwall_post','/api/hpwall/post/:id',array(
		'controller' => 'Coachrufus\Api\ApiModule:HpWall:post'
));


$acl->allowRole(array('guest'),'api_hpwall_post_upload');
$router->addRoute('api_hpwall_post_upload','/api/hpwall/upload',array(
		'controller' => 'Coachrufus\Api\ApiModule:HpWall:upload'
));



/*
 * @package \Coachrufus\Api
 * post rest
 */

$acl->allowRole(array('guest'),'api_hpwall_post_test');
$router->addRoute('api_hpwall_post_test','/api/test/hpwall/post',array(
		'controller' => 'Coachrufus\Api\ApiModule:HpWallTest:post'
));


/*
 * @package \Coachrufus\Api\Test
 * hp wall post like test
 */
$acl->allowRole(array('guest'),'api_hpwall_post_like_test');
$router->addRoute('api_hpwall_post_like_test','/api/test/hpwall/post-like',array(
		'controller' => 'Coachrufus\Api\ApiModule:HpWallTest:likeUpdate'
));

/*
 * @package \Coachrufus\Api
 * hp wall post like 
 */
$acl->allowRole(array('guest'),'api_hpwall_post_like');
$router->addRoute('api_hpwall_post_like','/api/hpwall/post-like',array(
		'controller' => 'Coachrufus\Api\ApiModule:HpWall:likeUpdate'
));


$acl->allowRole(array('guest'),'api_hpwall_post_comment_create');
$router->addRoute('api_hpwall_post_comment_create','/api/hpwall/postComment',array(
		'controller' => 'Coachrufus\Api\ApiModule:HpWall:postComment'
));


$acl->allowRole(array('guest'),'api_hpwall_post_comment');
$router->addRoute('api_hpwall_post_comment','/api/hpwall/postComment/:id',array(
		'controller' => 'Coachrufus\Api\ApiModule:HpWall:postComment'
));



/*
 * @package \Coachrufus\Api\Test
 * hp wall post generate
 */
$acl->allowRole(array('guest'),'api_hpwall_post_generate_test');
$router->addRoute('api_hpwall_post_generate_test','/api/test/hpwall/post-generate',array(
		'controller' => 'Coachrufus\Api\ApiModule:HpWallTest:postGenerate'
));

