<?php
namespace Coachrufus\Api;
require_once(MODUL_DIR.'/Api/ApiModule.php');


use Core\ServiceLayer as ServiceLayer;


$acl = ServiceLayer::getService('acl');

ServiceLayer::addService('ApiEventManager',array(
'class' => "Coachrufus\Api\Manager\EventManager",
));

//routing
$router = ServiceLayer::getService('router');




$acl->allowRole(array('guest'),'api_demo');
$router->addRoute('api_demo','/api/demo',array(
		'controller' => 'Coachrufus\Api\ApiModule:Demo:index'
));

include_once 'system_notify_routing.php';
include_once 'user_routing.php';
include_once 'team_routing.php';
include_once 'scouting_routing.php';
include_once 'event_routing.php';
include_once 'games_routing.php';
include_once 'tournament_routing.php';
include_once 'content_routing.php';
include_once 'hp_wall_routing.php';
include_once 'stats_routing.php';
include_once 'test_routing.php';








