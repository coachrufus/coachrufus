<?php
/*
 * @package \Coachrufus\Api\Test
 * Personal stats
 */
$acl->allowRole('guest','api_content_stats_personal_test');
$router->addRoute('api_content_stats_personal_test','/api/test/content/stats/personal',array(
		'controller' => '\Coachrufus\Api\ApiModule:StatsTest:personal'
));

/*
 * @package \Coachrufus\Api
 * Personal stats
 */
$acl->allowRole(array('guest'),'api_content_stats_personal');
$router->addRoute('api_content_stats_personal','/api/content/stats/personal',array(
		'controller' => 'Coachrufus\Api\ApiModule:Stats:personal'
));

/*
 * @package \Coachrufus\Api\Test
 * Stats tables
 */
$acl->allowRole('guest','api_content_stats_tables_test');
$router->addRoute('api_content_stats_tables_test','/api/test/content/stats/tables',array(
		'controller' => '\Coachrufus\Api\ApiModule:StatsTest:tables'
));

/*
 * @package \Coachrufus\Api
 * Stats tables
 */
$acl->allowRole(array('guest'),'api_content_stats_tables');
$router->addRoute('api_content_stats_tables','/api/content/stats/tables',array(
		'controller' => 'Coachrufus\Api\ApiModule:Stats:tables'
));

/*
 * @package \Coachrufus\Api\Test
 * Stats graphs
 */
$acl->allowRole('guest','api_content_stats_graphs_test');
$router->addRoute('api_content_stats_graphs_test','/api/test/content/stats/graphs',array(
		'controller' => '\Coachrufus\Api\ApiModule:StatsTest:graphs'
));

/*
 * @package \Coachrufus\Api
 * Stats graphs
 */
$acl->allowRole(array('guest'),'api_content_stats_graphs');
$router->addRoute('api_content_stats_graphs','/api/content/stats/graphs',array(
		'controller' => 'Coachrufus\Api\ApiModule:Stats:graphs'
));



