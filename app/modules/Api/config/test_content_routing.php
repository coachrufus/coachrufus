<?php




$acl->allowRole(array('guest'),'api_content_float_menu_test');
$router->addRoute('api_content_float_menu_test','/api/test/content/float-menu',array(
		'controller' => 'Coachrufus\Api\ApiModule:Test:contentFloatMenu'
));


$acl->allowRole('guest','api_content_chat_test');
$router->addRoute('api_content_chat_test','/api/test/content/chat',array(
		'controller' => '\Coachrufus\Api\ApiModule:Test:contentChat'
));

$acl->allowRole('guest','api_content_team_create_test');
$router->addRoute('api_content_team_create_test','/api/test/content/team/create',array(
		'controller' => '\Coachrufus\Api\ApiModule:Test:contentTeamCreate'
));



$acl->allowRole('guest','api_content_team_payment_test');
$router->addRoute('api_content_team_payment_test','/api/test/content/team/payment',array(
		'controller' => '\Coachrufus\Api\ApiModule:Test:contentTeamPayment'
));

$acl->allowRole('guest','api_content_event_create_test');
$router->addRoute('api_content_event_create_test','/api/test/content/event/create',array(
		'controller' => '\Coachrufus\Api\ApiModule:Test:contentEventCreate'
));

$acl->allowRole('guest','api_content_event_score_create_test');
$router->addRoute('api_content_event_score_create_test','/api/test/content/event/score/create',array(
		'controller' => '\Coachrufus\Api\ApiModule:Test:contentEventScoreCreate'
));

$acl->allowRole('guest','api_content_scouting_test');
$router->addRoute('api_content_scouting_test','/api/test/content/scouting',array(
		'controller' => '\Coachrufus\Api\ApiModule:Test:scouting'
));

$acl->allowRole('guest','api_content_tournament_test');
$router->addRoute('api_content_tournament_test','/api/test/content/tournament',array(
		'controller' => '\Coachrufus\Api\ApiModule:Test:tournament'
));
