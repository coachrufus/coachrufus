<?php
namespace Coachrufus\Api\Manager;

use Core\ServiceLayer;


class EventManager 
{
    /**
     * Add info about event necessary to display event row in event list
     */
    public function addEventListRowInfo($calendarEventsList,$teamsPlayers,$selectedTeams = null)
    {
        $teamEventManager = ServiceLayer::getService('TeamEventManager');
        $statManager = ServiceLayer::getService('PlayerStatManager');
        $sportManager = ServiceLayer::getService('SportManager');
        
        $statRoutes = array();
        foreach($selectedTeams as $selectedTeam)
        {
            $statRoutes[$selectedTeam->getId()] = $sportManager->getTeamSportAddStatRoute($selectedTeam);
        }
        
        
        foreach ($calendarEventsList as $dayEvents)
        {
            foreach ($dayEvents as $event)
            {
                $attendance = $teamEventManager->getEventAttendance($event);
                $lineup = $teamEventManager->getEventLineup($event);
                if (null != $lineup)
                {
                    $lineupPlayers = $teamEventManager->getEventLineupPlayers($lineup);

                    foreach ($lineupPlayers as $lineupPlayer)
                    {
                        if ($lineupPlayer->getTeamPlayerId() > 0)
                        {
                            $lineupPlayer->setTeamPlayer($teamsPlayers[$event->getTeamId()][$lineupPlayer->getTeamPlayerId()]);
                        }
                    }
                    $lineup->setLineupPlayers($lineupPlayers);

                    //result
                    if ($lineup->getStatus() == 'closed')
                    {
                        $matchOverview = $statManager->getMatchOverview($lineup);
                        $event->setMatchResult($matchOverview->getResult());
                        $event->setMatchOverview($matchOverview);
                    }
                }
                $event->setLineup($lineup);
                $event->setAttendance($attendance);
                $event->setStatRoute($statRoutes[$event->getTeamId()]) ;
                
                
                foreach ($attendance as $att)
                {
                    if (array_key_exists($att->getTeamPlayerId(), $teamsPlayers[$event->getTeamId()]))
                    {
                        $teamsPlayers[$event->getTeamId()][$att->getTeamPlayerId()]->addEventAttendance($event->getUid(), $att);
                    }
                }
                $playgroundData = $event->getPlaygroundDataAsArray();
                $playground = null;
                if (false != $playgroundData)
                {
                    $playground['name'] = $playgroundData['name'];
                    $playground['lat'] = $playgroundData['lat'];
                    $playground['lng'] = $playgroundData['lng'];
                }
            }
        }
        
        return $calendarEventsList;
    }
    
    /**
     * return all team players in all user teams
     */
    public function getUserTeamsTeamPlayersMap($user)
    {
        $teamManager = ServiceLayer::getService('TeamManager');
        $teams = $teamManager->getUserTeams($user);


        $userTeamPlayerIds = array();
        $teamsPlayers  = array();
        foreach($teams as $team)
        {
            $teamMembers = $teamManager->getTeamPlayers($team->getId());
            $indexedTeamMembers  = array();
            foreach($teamMembers as $teamMember)
            {
                $indexedTeamMembers[$teamMember->getId()] = $teamMember;

                if($teamMember->getPlayerId() == $user->getId())
                {
                    $userTeamPlayerIds[$team->getId()] = (int)$teamMember->getId();
                }
            }
            $teamsPlayers[$team->getId()] = $indexedTeamMembers;
        }
        
        
        
        return array(
            'teamsPlayers' => $teamsPlayers,
            'userTeamPlayerIds' => $userTeamPlayerIds,
            'teams' => $teams
        );  
    }
    
    public function getFinishedLineups($selectedTeams,$startDate,$endDate)
    {
        $matchRepo  = ServiceLayer::getService('TeamMatchLineupRepository');
        $finishedLinups = array();
        foreach($selectedTeams as $selectedTeam)
        {
            $lineups = $matchRepo->findTeamMatchs($selectedTeam,$startDate,$endDate);
            foreach($lineups as $lineup)
            {
                 if($lineup->getStatus() == 'closed')
                 {
                     $finishedLinups[] = $lineup;
                 }
                
            }
        }
        
        
        return $finishedLinups;
    }
    
    /*
     * Retrun last exist events according specific criteria.
     */
    public function getUserLastResultsEvents($user,$criteria = array('startDate' => null,'endDate' => null,'selectedTeams' => array()))
    {
        $teamEventManager  = ServiceLayer::getService('TeamEventManager');
        $matchRepo  = ServiceLayer::getService('TeamMatchLineupRepository');
        
        
        if($criteria['endDate'] == null)
        {
            $endDate = new \DateTime();
        }
        else
        {
            $endDate = $criteria['endDate'];
        }
        $startDate = $criteria['startDate'];
        $selectedTeams = $criteria['selectedTeams'];
        
        /*
        t_dump($startDate);
        t_dump($endDate);
        t_dump($selectedTeams);
         * 
         */
        
        //check if has played match in this time interval
        $finishedLinups = $this->getFinishedLineups($selectedTeams,$startDate,$endDate);
    
        
        
        //fuck recursion
        /*
        if(count($finishedLinups) < 10)
        {
            $startDate->modify('-60 day');
            $finishedLineups = $this->getFinishedLineups($selectedTeams,$startDate,$endDate);
            
            if(count($finishedLinups) < 10)
            {
                $startDate->modify('-180 day');
                $finishedLineups = $this->getFinishedLineups($selectedTeams,$startDate,$endDate);

                if(count($finishedLinups) < 10)
                {
                    $startDate->modify('-360 day');
                    $finishedLineups = $this->getFinishedLineups($selectedTeams,$startDate,$endDate);
                }
            }
        }   
        */

        if (!empty($finishedLinups))
        {
            $calendarEventsList = array();    
            foreach($finishedLinups as $finishedLinup)
            {
                $event = $teamEventManager->findEvent($finishedLinup->getEventId());
                
                if($event != null)
                {
                    $currentDate = new \DateTime($finishedLinup->getEventDate()->format('Y-m-d').' '.$event->getStart()->format('H:i:s'));
                    $event->setCurrentDate($currentDate);
                    $calendarEventsList[$event->getCurrentDate()->format('Y-m-d')][$event->getId()] = $event;
                }
               
            }

            $apiEventManager =  ServiceLayer::getService('ApiEventManager');
            $selectedTeams = $criteria['selectedTeams'];
            $teamPlayersMap = $this->getUserTeamsTeamPlayersMap($user);
            $teamsPlayers = $teamPlayersMap['teamsPlayers'];
            //$calendarEvents = $teamEventManager->findEvents($selectedTeams, array('from' => $startDate, 'to' => $endDate));
            //$calendarEventsList = $teamEventManager->buildTeamEventList($calendarEvents, $startDate, $endDate);
            
            $apiEventManager->addEventListRowInfo($calendarEventsList,$teamsPlayers,$selectedTeams);
            $finishedEvents = array();
            foreach ($calendarEventsList as $dayEvents)
            {
                foreach ($dayEvents as $event)
                {
                    if ((!empty($event->getLineup()) && $event->getLineup()->getStatus() == 'closed'))
                    {
                        $finishedEvents[] = $event;
                    }
                }
            }
            
            //t_dump($finishedEvents);
            
            $nextEndDate = $finishedEvents[0]->getCurrentDate();
            $nextEndDate->modify('-1 day');
            $nexStartDate = clone $nextEndDate;
            $nexStartDate->modify('-180 day');
            return array('events' => $finishedEvents,'nexStartDate' => $nexStartDate,'nextEndDate' => $nextEndDate);

        }
        else
        {
            //$limit = 360;
           
            $endDate = clone $startDate;
            $startDate->modify('-180 day');
            
            $currentDate =new \Datetime();
            $interval = $currentDate->diff($endDate);
         
            
            if($interval->days > 720)
            {
                 return array('events' => array(),'nexStartDate' => $startDate,'nextEndDate' => $endDate);
            }
            
           
            return $this->getUserLastResultsEvents($user,$criteria = array('startDate' => $startDate,'endDate' => $endDate,'selectedTeams' => $selectedTeams));
        }


        /*
        $startDays = $criteria['startDays'];
        $selectedTeams = $criteria['selectedTeams'];
        $teamPlayersMap = $this->getUserTeamsTeamPlayersMap($user);
        $teamsPlayers = $teamPlayersMap['teamsPlayers'];
        
        $endPeriodDatetime = new \DateTime($endPeriod);
        $startPeriod = new \DateTime($endPeriod);
        $startPeriod->modify('-' . $startDays . ' day');

        $nextStartPeriod = new \DateTime($endPeriod);
        $nextStartPeriod->modify('-' . ($startDays + 1) . ' day');
        $calendarEvents = $teamEventManager->findEvents($selectedTeams, array('from' => $startPeriod, 'to' => $endPeriodDatetime));
        $calendarEventsList = $teamEventManager->buildTeamEventList($calendarEvents, $startPeriod, $endPeriodDatetime);
        
        //find event lineup
        
        
        //$this->addEventListRowInfo($calendarEventsList, $teamsPlayers, $selectedTeams);
        $finishedEvents = array();
        foreach ($calendarEventsList as $dayEvents)
        {
            foreach ($dayEvents as $event)
            {
                if ((!empty($event->getLineup()) && $event->getLineup()->getStatus() == 'closed'))
                {
                    $finishedEvents[] = $event;
                }
            }
        }
        
        
        $limit = 360;
        
        if(empty($finishedEvents) && $startDays < $limit)
        {
            $nextStart  = $startDays+60;
            $nextEnd = $startPeriod;
            return $this->getUserLastResultsEvents($user,$criteria = array('startDays' => $nextStart,'endPeriod' => $nextEnd->format('Y-m-d'),'selectedTeams' => $selectedTeams ));
        }
        else 
        {
            return array('events' => $finishedEvents,'startPeriod' => $startPeriod);
        }
         * 
         */
    }

}
