<div class="container">
 <div class="row top-panel">
        <div class="col-xs-8 btn-back">
            <a class="past_months_trigger" href="#"> <i class="ico ico-arr-lft-line"></i> <span><?php echo $translator->translate('calendar.past.moreBtn') ?></span></a>
        </div>
        
        <div class="col-xs-4 text-right top-toolbar">

            <div class="btn-group tool-item" role="group">  
                <a class="dropdown-toggle export-event" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="/team/events-calendar?team_id=3"><i class="ico ico-export"></i></a>
                <ul class="dropdown-menu">
                    <li> <a class="popup-link" href="/event/ical/3/event/subscription">Stiahnúť .ics súbor</a></li>
                    <li> <a class="popup-link" href="webcal://app.coachrufus.lamp/event/ical/3/event/subscription">Outlook</a></li>
                    <li> <a class="popup-link" href="webcal://app.coachrufus.lamp/event/ical/3/event/subscription">iCalc</a></li>
                    <li> <a class="popup-link" href="https://www.google.com/calendar/render?cid=webcal://app.coachrufus.lamp/event/ical/3/event/subscription">Google kalendár</a></li>
                </ul>
            </div>
 
            <a class="tool-item tool-item-link" href="<?php echo $router->link('api_content_event_current',array('apikey' => $apikey,'user_id' => $userId,'team_id' => $selectedTeamId)) ?>"><i class="ico ico-event-list"></i></a> 

        </div>
    </div>
</div>
<div id="small-calendar-wrap" class="container infinity-scroll-container" data-next-period="<?php echo $nextPeriod ?>" data-prev-period="<?php echo $prevPeriod ?>">
     

    <?php echo $calendarMonth ?>
    <?php echo $calendarMonth2 ?>
    
     <div class="row">
        <div class="col-xs-12 alert alert-info text-center loader">Loading</div>
   </div>
</div>
<?php $layout->addJavascript('js/TeamEventManager.js') ?>   
<?php  $layout->startSlot('javascript') ?>
<script type="text/javascript">
   $(document).ready(function() {
       
        
        var win = $(window);
        var scrollContainer = $('.infinity-scroll-container');
        var loader = $('.infinity-scroll-container .loader');
        var next_period = scrollContainer.attr('data-next-period');
        var prev_period = scrollContainer.attr('data-prev-period');
        var loading = false;
        
        //past events
        $('.past_months_trigger').on('click',function(e){
             e.preventDefault();
            loader.css('top','0');
            loader.css('bottom','auto');
            loader.show();
            loading = true;
            $.ajax({
            url: '<?php echo $router->link('api_content_event_calendar_month',array(
                'apikey' =>$apikey,
                'user_id' => $userId,
                    )) ?>&month='+prev_period+'&dir=prev',
                    dataType: 'json',
                    success: function(response) {
                        scrollContainer.prepend(response.html);
                        scrollContainer.attr('data-prev-period',response.periodInfo);
                        prev_period = response.periodInfo;
                        loader.hide();
                        loading = false;

                        //$(".past_months_trigger").detach().prependTo('.infinity-scroll-container');
                    }  
            });   
        });
        
        //load future months
        win.scroll(function() {
            if ($(document).height() - win.height() == win.scrollTop() && loading==false) {
                loader.css('top','auto');
                loader.css('bottom','-30px');

                loader.show();
                loading = true;

                
                $.ajax({
                url: '<?php echo $router->link('api_content_event_calendar_month',array(
                    'apikey' =>$apikey,
                    'user_id' => $userId,
                        )) ?>&month='+next_period+'&dir=next',
                        dataType: 'json',
                        success: function(response) {
                            scrollContainer.append(response.html);

                            scrollContainer.attr('data-next-period',response.periodInfo);
                            next_period = response.periodInfo;
                            loader.hide();
                            loading = false;
                        }  
                }); 
                
            }
        });
        

        $(document).on('click','.day_events_show_trigger',function(){
            $('.day_events').hide();
            $('.event-detail-row').hide();
            $('td.active').removeClass('active');
            
            $(this).addClass('active');
            var target_day = $(this).attr('data-day-target');
            var target_week = $(this).attr('data-week-target');
            $('.day_events_'+target_day).show();
            $('.event-detail-row_'+target_week).show();
            var offset = $(this).offset();
            $('.small-calendar .day_events .arr').css('left',(offset.left+20)+'px'); 
       });
       
       
        event_manger = new TeamEventsManager();
        $(document).on('click','.attendance_trigger',function(e){
            e.preventDefault();
            
            
            var elem = $(this);
            var type = $(this).attr('data-rel');
            var elemContainer = elem.parents('.row');
            var elemPar = $(this).parent();
            elem.addClass('loading');
            elem.html('<i class="fa fa-refresh fa-spin fa-fw"></i>');
            
            event_manger.changePlayerAttendance($(this),function(){
               
                
                
                elem.html('');
                elem.removeClass('loading');
                //elemPar.append(elem);
                
                 if(type == 'in')
                {
                    elemContainer.find('.attendance_trigger').addClass('attend-accept');
                    elemContainer.find('.attendance_trigger').removeClass('attend-deny');
                }
                
                if(type == 'out')
                {
                    elemContainer.find('.attendance_trigger').addClass('attend-deny');
                    elemContainer.find('.attendance_trigger').removeClass('attend-accept');
                }
                
                
            });

            
        });
       
   });
</script>
<?php  $layout->endSlot('javascript') ?>
 