<?php if( \Core\ServiceLayer::getService('security')->getIdentity()->hasTeamPermission('manageEvents',$teams[$event->getTeamId()])): ?>
<div class="row-sidebar">
 <a href="#" class="row-sidebar-trigger" data-target="row-sidebar-cnt-<?php echo $event->getUid() ?>"><i class="ico ico-more-b"  aria-hidden="true"></i></a>
</div>




<div class="row-sidebar-cnt" id="row-sidebar-cnt-<?php echo $event->getUid() ?>" >

    
    <a class="tool-item mpopup" data-action="popup-full" data-popup-onclose-action="refresh" href="<?php echo WEB_DOMAIN.$router->link('edit_team_event', array('lt'=>'mal','event_id' => $event->getId(),'event_date' => $event->getCurrentDate()->format('Y-m-d'))) ?>"><i class="ico ico-edit"></i> <?php echo $translator->translate('widget.event.sidebar.edit') ?></a>  
    
    <a class="tool-item mpopup" data-action="popup-full" data-popup-onclose-action="refresh" href="<?php echo WEB_DOMAIN.$router->link('team_event_detail', array('lt'=>'mal','id' => $event->getId(),'current_date' => $event->getCurrentDate()->format('Y-m-d'))) ?>"><i class="ico ico-edit"></i> <?php echo $translator->translate('widget.event.sidebar.attendance') ?></a>  
    
    <a class="tool-item mpopup"  data-action="popup-full" data-popup-onclose-action="refresh" href="<?php echo WEB_DOMAIN.$router->generateApiWidgetUrl('api_content_games_lineup_widget',array('apikey' => $apikey,'user_id' => $userId,'event_id' => $event->getId(),'event_date' => $event->getCurrentDate()->format('Y-m-d'))) ?>"><i class="ico ico-edit"></i> <?php echo $translator->translate('widget.event.sidebar.lineup') ?></a>  
    
    <?php if($event->getLineup() != null && $event->getStatRoute() == 'team_match_create_live_stat'): ?>
        <a class="tool-item mpopup" data-action="popup-full" data-popup-onclose-action="refresh" href="<?php echo WEB_DOMAIN.$router->generateApiWidgetUrl('api_content_games_score_widget',array('apikey' => $apikey,'user_id' => $userId,'event_id' => $event->getId(),'lineup_id' => $event->getLineup()->getId() ,'event_date' => $event->getCurrentDate()->format('Y-m-d'))) ?>"><i class="ico ico-edit"></i> <?php echo $translator->translate('widget.event.sidebar.addScore') ?></a>  

    <?php endif; ?>
    
 

    <?php if($event->getLineup() == null or $event->getLineup()->getStatus() != 'closed'): ?>
        <a class="tool-item delete-event-trigger" data-event-type="<?php echo $event->getPeriod() ?>" href="<?php echo $router->link('delete_team_event', array('current_date' => $event->getCurrentDate()->format('Y-m-d'),'event_id' => $event->getId())) ?>"><span><i class="ico ico-trash"></i> <?php echo $translator->translate('Delete event') ?> </span></a>
    <?php endif; ?>
        
    
            
        
        


</div>

<?php endif; ?>