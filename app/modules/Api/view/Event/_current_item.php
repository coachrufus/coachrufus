   <?php foreach($events as $dayEvents): ?>
    <?php foreach($dayEvents as $event): ?>  

    <?php  
    $timeLimit = DateTime::createFromFormat('Y-m-d H:i:s', date('Y-m-d').' 00:00:00');
    ?>

    <div class="row event-item future-item <?php echo ($event->getCurrentDate()->getTimestamp() < $timeLimit->getTimestamp()) ? 'past-item' : 'future-item' ?>">
        <div class="col-sm-12">
            <?php $layout->includePart(MODUL_DIR . '/Api/view/Event/_event_sidebar.php',array(
             'apikey' => $apikey,
                  'userId' => $userId,
                'event' => $event,
             'teams' => $teams,
             'teamsPlayers' => $teamsPlayers,
             'userTeamPlayerIds' => $userTeamPlayerIds)) ?>
            
            

           
                <?php if($event->getCurrentDate()->format('Y-m-d') == date('Y-m-d')): ?>
                <span class="date today-date">
                    <?php echo $translator->translate('api.event.today') ?> <?php echo $event->getCurrentDate()->format('H:i')  ?>
                 </span>
                <?php else: ?>
                <span class="date">
                <?php echo substr($translator->translate($event->getCurrentDate()->format('D')),0,2) ?>. <?php echo $event->getCurrentDate()->format('d')  ?>. <?php echo $translator->translate($event->getCurrentDate()->format('F') ) ?> <?php echo $event->getCurrentDate()->format('H:i')  ?>
                 </span>
                <?php endif; ?>
                
                 
            <div class="mpopup1" data-action="popup-full" data-href="<?php echo WEB_DOMAIN.$router->link('team_event_detail',array('id' => $event->getId(),'current_date' => $event->getCurrentDate()->format('Y-m-d') )) ?>">

                <h2><?php echo $event->getName() ?></h2>
                <span class="team-name">
                    <?php echo $event->getTeamName() ?>
                    <?php if(null != $event->getPlayground()): ?>
                       (<?php echo $event->getPlayground()->getName() ?>)
                   <?php endif; ?>

                </span>

                <ul class="attendace_list">
                    <?php foreach($event->getAttendance() as $attendance): ?>
                    <?php if($attendance->getStatus() == '1'): ?>
                        <li class="face" style="background-image: url(<?php echo $attendance->getPlayer()->getMidPhoto() ?>)"></li>
                   <?php endif; ?>

                    <?php endforeach; ?>

                    <li class="sum"><?php echo  $event->getAttendanceInSum() ?><span>/<?php echo  $event->getCapacity() ?></span></li>
                </ul>

            </div>
            
            <?php if( $event->getAttendanceInSum() >=  $event->getCapacity() or $event->getCurrentDate()->getTimestamp()-(1*60*60) < time() ): ?>
                 <?php if($event->getLineup() == null && null != $event->getStatRoute()): ?>
                    <a class="cta-btn mpopup" data-action="popup-full" data-popup-onclose-action="refresh" href="<?php echo  WEB_DOMAIN.$router->generateApiWidgetUrl('api_content_games_lineup_widget',array('apikey' => $apikey,'user_id' => $userId,'event_id' => $event->getId(),'event_date' => $event->getCurrentDate()->format('Y-m-d'))) ?>"><?php echo $translator->translate('event.past.createLineup') ?></a><br />
                 <?php endif; ?>
            <?php endif; ?>

        
        <div class="row">
            <?php $teamPlayer = $teamsPlayers[$event->getTeamId()][$userTeamPlayerIds[$event->getTeamId()]] ;?>
            <?php if(null != $teamPlayer): ?>
            
            <?php if(null == $teamPlayer->getEventAttendanceStatus($event->getUid())): ?>
                <div class="col-xs-6 text-right">
                    <a class="attend-btn attend-btn-yes  attend-trigger-yes attendance_trigger" href="#"
                        data-url="<?php echo $router->link('change_player_attendance') ?>" 
                        data-rel="in" 
                        data-event="<?php echo $event->getId() ?>" 
                        data-eventdate="<?php echo  $event->getCurrentDate()->format('Y-m-d')  ?>" 
                        data-team-id="<?php echo $event->getTeamId() ?>"
                        data-toggle="tooltip" data-placement="top" title="<?php echo $translator->translate('In') ?>"
                       ></a>
                </div>
                <div class="col-xs-6 text-left">
                     <a class="attend-btn attend-btn-no attend-trigger-no attendance_trigger" href="#"
                        data-url="<?php echo $router->link('change_player_attendance') ?>" 
                        data-rel="out" 
                        data-event="<?php echo $event->getId() ?>" 
                        data-eventdate="<?php echo  $event->getCurrentDate()->format('Y-m-d')  ?>" 
                        data-team-id="<?php echo $event->getTeamId() ?>"
                        data-toggle="tooltip" data-placement="top" title="<?php echo $translator->translate('In') ?>"
                        ></a>
                </div>
            <?php elseif('1' == $teamPlayer->getEventAttendanceStatus($event->getUid())): ?>
                 <div class="col-xs-6 text-right">
                    <a class="attend-btn attend-accept attend-btn-yes  attend-trigger-yes attendance_trigger" href="#"
                       data-url="<?php echo $router->link('change_player_attendance') ?>" 
                        data-rel="in" 
                        data-event="<?php echo $event->getId() ?>" 
                        data-eventdate="<?php echo  $event->getCurrentDate()->format('Y-m-d')  ?>" 
                        data-team-id="<?php echo $event->getTeamId() ?>"
                        data-toggle="tooltip" data-placement="top" title="<?php echo $translator->translate('In') ?>"
                       ></a>
                </div>
                <div class="col-xs-6 text-left">
                     <a class="attend-btn attend-accept attend-btn-no attend-trigger-no attendance_trigger" href="#"
                        data-url="<?php echo $router->link('change_player_attendance') ?>" 
                        data-rel="out" 
                        data-event="<?php echo $event->getId() ?>" 
                        data-eventdate="<?php echo  $event->getCurrentDate()->format('Y-m-d')  ?>" 
                        data-team-id="<?php echo $event->getTeamId() ?>"
                        data-toggle="tooltip" data-placement="top" title="<?php echo $translator->translate('In') ?>"
                        ></a>
                </div>
            <?php elseif('3' == $teamPlayer->getEventAttendanceStatus($event->getUid())): ?>
                 <div class="col-xs-6 text-right">
                    <a class="attend-btn attend-deny attend-btn-yes  attend-trigger-yes attendance_trigger" href="#"
                       data-url="<?php echo $router->link('change_player_attendance') ?>" 
                        data-rel="in" 
                        data-event="<?php echo $event->getId() ?>" 
                        data-eventdate="<?php echo  $event->getCurrentDate()->format('Y-m-d')  ?>" 
                        data-team-id="<?php echo $event->getTeamId() ?>"
                        data-toggle="tooltip" data-placement="top" title="<?php echo $translator->translate('In') ?>"
                       
                       ></a>
                </div>
                <div class="col-xs-6 text-left">
                    <a class="attend-btn attend-deny attend-btn-no attend-trigger-no attendance_trigger" href="#"
                       data-url="<?php echo $router->link('change_player_attendance') ?>" 
                        data-rel="out" 
                        data-event="<?php echo $event->getId() ?>" 
                        data-eventdate="<?php echo  $event->getCurrentDate()->format('Y-m-d')  ?>" 
                        data-team-id="<?php echo $event->getTeamId() ?>"
                        data-toggle="tooltip" data-placement="top" title="<?php echo $translator->translate('In') ?>"
                       ></a>
                </div>
            <?php endif; ?>
             <?php endif; ?>
        </div>
        </div>
    </div>


 <?php endforeach; ?>
<?php endforeach; ?>
