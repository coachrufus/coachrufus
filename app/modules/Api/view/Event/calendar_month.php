
<h2 class="month-title"><?php echo $translator->translate($calendar->getCurrentMonthName()) ?> <?php echo  $calendar->getDateFrom()->format('Y') ?></h2>

<table class="small-calendar">
    <thead>
        <tr class="days">       
            <th><?php echo mb_substr($translator->translate('Monday'),0,1) ?></th>
            <th><?php echo mb_substr($translator->translate('Tuesday'),0,1) ?></th>
            <th><?php echo mb_substr($translator->translate('Wednesday'),0,1) ?></th>
            <th><?php echo mb_substr($translator->translate('Thursday'),0,1) ?></th>
            <th><?php echo mb_substr($translator->translate('Friday'),0,1) ?></th>
            <th><?php echo mb_substr($translator->translate('Saturday'),0,1) ?></th>
            <th><?php echo mb_substr($translator->translate('Sunday'),0,1)?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($calendar->getCalendarMonthOverview() as $week_key => $week): ?>
            <tr>
                <?php foreach ($week as $day): ?>
                    <td data-day-target="<?php echo $day['day'] ?>_<?php echo $day['month'] ?>" data-week-target="<?php echo $week_key ?>" class="day_events_show_trigger <?php echo $day['class']  ?>">
                        <?php if($calendar->getCurrentMonth() == $day['month']): ?>
                        <div class="cell-wrap">
                        <strong><?php echo $day['day'] ?></strong>
                        </div>
                        <div class="day_events_mark">
                            <?php if(array_key_exists('events', $day)): ?>
                            <?php $atendanceRows = array('unknown'=>array(),'in' => array(),'out' => array()); ?>
                            <?php foreach ($day['events'] as $event): ?>
                            
                                <?php 
                                $attendance_status = 'unknown';
                                 if (
                                            array_key_exists($event->getId(), $attendanceList) &&
                                            array_key_exists($event->getCurrentDate()->format('Y-m-d'), $attendanceList[$event->getId()]) &&
                                            $attendanceList[$event->getId()][$event->getCurrentDate()->format('Y-m-d')] == 1)
                                    {
                                        $attendance_status = 'in';
                                    }

                                    if (
                                            array_key_exists($event->getId(), $attendanceList) &&
                                            array_key_exists($event->getCurrentDate()->format('Y-m-d'), $attendanceList[$event->getId()]) &&
                                            $attendanceList[$event->getId()][$event->getCurrentDate()->format('Y-m-d')] == 3)
                                    {
                                        $attendance_status = 'out';
                                    }
                                $atendanceRows[$attendance_status][] = ' <i class="fa fa-circle attendance_type_'.$attendance_status.'" aria-hidden="true"></i>';
                                ?>
  
                            <?php endforeach; ?>
                            <?php 
                              $final = array_merge($atendanceRows['in'],$atendanceRows['unknown'],$atendanceRows['out']) ;
                                     //$atendanceRows['in']+$atendanceRows['unknown']+$atendanceRows['out'];
                                $final1 = array_slice($final,0,4);
                                $final2 = array_slice($final,4,4);
                                echo implode($final1,'').'<br />';
                                echo implode($final2,'');
                            
                            ?>
                            <?php endif; ?>
                        </div>
                        <?php endif; ?>
                       
                        
                    </td>
                <?php endforeach; ?>
            </tr>
            
            <!-- week events -->
            <tr class="event-detail-row event-detail-row_<?php echo $week_key ?>">
                <td colspan="7">
                     <?php foreach ($week as $day): ?>
                    <div class="day_events day_events_<?php echo $day['day'] ?>_<?php echo $day['month'] ?>">
                        <?php if(array_key_exists('events', $day)): ?>
                            <?php foreach ($day['events'] as $event): ?>
                                <div class="row calendar-event-row">
                                    <div class="col-xs-9">
                                         <h3><?php echo $event->getName() ?></h3>
                                         <div class="row">
                                            <div class="col-xs-3 date">
                                                <?php echo $event->getCurrentDate()->format('H:i')  ?>
                                            </div>
                                             <div class="col-xs-9 team-name">
                                                <?php echo $event->getTeamName() ?>
                                            </div>
                                         </div>
                                         <div class="row">
                                             <div class="col-xs-3 date">
                                               
                                            </div>
                                             <div class="col-xs-9 playground">
                                                <?php if(null != $event->getPlayground()): ?>
                                                    (<?php echo $event->getPlayground()->getName() ?>)
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                         <div class="row">
                                              <ul class="attendace_list">
                                                   <li class="sum"><?php echo  $event->getAttendanceInSum() ?><span>/<?php echo  $event->getCapacity() ?></span></li>
                                                <?php foreach($event->getAttendance() as $attendance): ?>
                                                <?php if($attendance->getStatus() == '1'): ?>
                                                    <li class="face" style="background-image: url(<?php echo $attendance->getPlayer()->getMidPhoto() ?>)"></li>
                                               <?php endif; ?>

                                                <?php endforeach; ?>
                                               
                                            </ul>
                                         </div>
                                    </div>
                                    <div class="col-xs-3 attend-col">
                                        <?php $teamPlayer = $teamsPlayers[$event->getTeamId()][$userTeamPlayerIds[$event->getTeamId()]] ;?>
                                            
                                         <a class="attend-btn <?php echo ('1' == $teamPlayer->getEventAttendanceStatus($event->getUid())) ? 'attend-accept' : ''?> attend-btn-yes  attend-trigger-yes attendance_trigger" href="#"
                                                    data-url="<?php echo $router->link('change_player_attendance') ?>" 
                                                    data-rel="in" 
                                                    data-event="<?php echo $event->getId() ?>" 
                                                    data-eventdate="<?php echo  $event->getCurrentDate()->format('Y-m-d')  ?>" 
                                                    data-team-id="<?php echo $event->getTeamId() ?>"
                                                    data-toggle="tooltip" data-placement="top" title="<?php echo $translator->translate('In') ?>"
                                                   ></a>
                                           
                                                 <a class="attend-btn  <?php echo ('3' == $teamPlayer->getEventAttendanceStatus($event->getUid())) ? 'attend-deny' : ''?> attend-btn-no attend-trigger-no attendance_trigger" href="#"
                                                    data-url="<?php echo $router->link('change_player_attendance') ?>" 
                                                    data-rel="out" 
                                                    data-event="<?php echo $event->getId() ?>" 
                                                    data-eventdate="<?php echo  $event->getCurrentDate()->format('Y-m-d')  ?>" 
                                                    data-team-id="<?php echo $event->getTeamId() ?>"
                                                    data-toggle="tooltip" data-placement="top" title="<?php echo $translator->translate('In') ?>"
                                                    ></a>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                             <?php else: ?>
                                <div class="row calendar-event-row">
                                    <h2><?php echo $translator->translate('calendar.noEvents') ?></h2>
                                </div>
                             <?php endif; ?>
                        <span class="arr"></span>
                        </div>
                    <?php endforeach; ?>
                </td>
            </tr>
            
        <?php endforeach; ?>
    </tbody>
</table>