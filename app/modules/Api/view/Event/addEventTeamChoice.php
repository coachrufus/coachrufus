 <div class="wrapper"> 
     
     
<div class="mobile-app-nav">
    <div class="navBtn"></div>
    <?php echo $translator->translate('api.event.teamChoice') ?>
 </div>
     
     
     
<div id="system-notify-cnt" class="team-choice-list">
    <div  id="team_notify_panel">
            <?php foreach ($teams as $team): ?>
                <div class="system-notify-row team-choice-row"> 
                    <div class="sn-avatar-wrap">
                        <i class="sn-avatar" style="background-image: url('<?php echo $team->getMidPhoto() ?>');"></i>
                    </div>
                    <div class="sn-cnt">
                            <a   href="<?php echo $router->link('create_team_event',array('team_id' => $team->getId())) ?>" >
                            <strong><?php echo $team->getName() ?></strong>
                         </a>
                    </div>
                </div>
            <?php endforeach; ?>
          <div class="row">
   </div>
    </div>
</div>

 </div>

<?php  $layout->startSlot('javascript') ?>
<script type="text/javascript">
   $(document).ready(function() {
	
       
        
         var messageOrigin;
        var messageSource;
        document.addEventListener('message', function (e) {
          messageOrigin = e.origin;
          messageSource = e.source;

        });
         $('.navBtn').on('click',function(e){
            e.preventDefault();
            var message = {
                'action': 'close',
            };
            
             window.postMessage(JSON.stringify(message) ,messageOrigin);
        });
});
</script>
<?php  $layout->endSlot('javascript') ?>
    
   


















