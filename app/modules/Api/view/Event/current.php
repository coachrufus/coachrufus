<div class="current-events-holder container">
    <div class="row top-panel">
        <div class="col-xs-3 btn-back">
           
        </div>
        <div class="col-xs-6 text-center">
             <?php $layout->includePart(MODUL_DIR . '/Api/view/Content/_teamNativeSelect.php',array(
         'user' => $user,
         'selectedTeamId' => $selectedTeamId,
         'link' => $router->link('api_content_event_current',array('apikey' => $apikey,'user_id' => $userId))
             
             )) ?>
        </div>
        <div class="col-xs-3 text-right top-toolbar">

            <div class="btn-group tool-item" role="group">  
                <a class="dropdown-toggle export-event" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="/team/events-calendar?team_id=<?php echo $selectedTeamId ?>"><i class="ico ico-export"></i></a>
                <ul class="dropdown-menu">
                    <li> <a class="popup-link" href="/event/ical/<?php echo $selectedTeamId ?>/event/subscription">Stiahnúť .ics súbor</a></li>
                    <li> <a class="popup-link" href="webcal://<?php echo WEB_DOMAIN_NAME ?>/event/ical/<?php echo $selectedTeamId ?>/event/subscription">Outlook</a></li>
                    <li> <a class="popup-link" href="webcal://<?php echo WEB_DOMAIN_NAME ?>/event/ical/<?php echo $selectedTeamId ?>/event/subscription">iCalc</a></li>
                    <li> <a class="popup-link" href="https://www.google.com/calendar/render?cid=webcal://<?php echo WEB_DOMAIN_NAME ?>/event/ical/<?php echo $selectedTeamId ?>/event/subscription">Google kalendár</a></li>
                </ul>
            </div>
 
            <a class="tool-item tool-item-link"  href="<?php echo WEB_DOMAIN.$router->generateApiWidgetUrl('api_content_webview',array('apikey' => CR_API_KEY,'view' => $router->link('api_content_event_calendar',array('user_id' => $userId)).'&apikey='.CR_API_KEY,'user_id' => $userId)) ?>"><i class="ico ico-calendar"></i></a> 

            
         
            
        </div>
    </div>

    <div class="infinity-scroll-container event-list" data-next-period="<?php echo $nextPeriod ?>" data-prev-period="<?php echo $prevPeriod ?>">
        <a class="past_events_trigger" href=""><?php echo $translator->translate('event.past.moreBtn') ?></a>
         <?php $layout->includePart(MODUL_DIR . '/Api/view/Event/_current_item.php',array(
              'apikey' => $apikey,
                'userId' => $userId,
             'events' => $calendarEventsList,
              'teams' => $teams,
             'teamsPlayers' => $teamsPlayers,
             'userTeamPlayerIds' => $userTeamPlayerIds)) ?>
   <div class="row">
        <div class="col-xs-12 alert alert-info text-center loader">Loading</div>
   </div>
    </div> 
   
</div>


<?php $layout->includePart(MODUL_DIR . '/Team/view/events/_confirm_delete_modal.php') ?>

<?php $layout->addJavascript('js/TeamEventManager.js') ?>
<?php $layout->addJavascript('js/WebviewManager.js') ?>
<?php  $layout->startSlot('javascript') ?>
<script type="text/javascript">
   $(document).ready(function() {
       
    $('#team_select_native').on('change',function(){
        location.href='<?php echo  WEB_DOMAIN.$router->link('api_content_event_current', array('apikey' => $apikey, 'user_id' => $userId)).'&team_id=' ?>'+$(this).val();
    });
    
      
         
	
	
        event_manger = new TeamEventsManager();
        webviewManager = new WebviewManager();
        webviewManager.handleSidebar();
        webviewManager.handlePostMessages();
        
      
        if($(".past-item").last().length > 0)
        {
             $('html, body').animate({
                scrollTop: $(".past-item").last().offset().top+140
            }, 100);
        }
        
         $('.delete-event-trigger').on('click',function(e){
            e.preventDefault();
            var event_type = $(this).attr('data-event-type');

            var link = $(this).attr('href');
            var type = $(this).attr('data-event-type');

            var link_all = link+'&t=all';
            var link_only = link+'&t=only';
            var link_next = link+'&t=next';


            event_manger.delete_calendar_modal.find('#delete_event_link_all').attr('href',link_all);
            event_manger.delete_calendar_modal.find('#delete_event_link_all_repeat').attr('href',link_all);
            event_manger.delete_calendar_modal.find('#delete_event_link_only').attr('href',link_only);
            event_manger.delete_calendar_modal.find('#delete_event_link_next').attr('href',link_next);



            if('none' == event_type)
            {
                //manager.delete_single_modal.modal('show'); 
                event_manger.delete_calendar_modal.find('#single-delete-event-wrap').show();
                event_manger.delete_calendar_modal.find('#multi-delete-event-wrap').hide();
            }
            else
            {
                event_manger.delete_calendar_modal.find('#single-delete-event-wrap').hide();
                event_manger.delete_calendar_modal.find('#multi-delete-event-wrap').show();
                //manager.delete_calendar_modal.modal('show'); 
            }

            event_manger.delete_calendar_modal.modal('show'); 

        });

        
        
      
      
        $(document).on('click','.attendance_trigger',function(e){
            e.preventDefault();

            var elem = $(this);
            var type = $(this).attr('data-rel');
            var elemContainer = elem.parents('.row');
            var elemPar = $(this).parent();
            
            elemPar.html('<i class="fa fa-refresh fa-spin fa-fw"></i>');
            
            event_manger.changePlayerAttendance($(this),function(){
                
                
                elemPar.html('');
                elemPar.append(elem);
                
                if(type == 'in')
                {
                    elemContainer.find('.attendance_trigger').addClass('attend-accept');
                    elemContainer.find('.attendance_trigger').removeClass('attend-deny');
                }
                
                if(type == 'out')
                {
                    elemContainer.find('.attendance_trigger').addClass('attend-deny');
                    elemContainer.find('.attendance_trigger').removeClass('attend-accept');
                }
            });
        });
        
        
        var win = $(window);
        var scrollContainer = $('.infinity-scroll-container');
        var loader = $('.infinity-scroll-container .loader');
        var next_period = scrollContainer.attr('data-next-period');
        var prev_period = scrollContainer.attr('data-prev-period');
        var loading = false;
        //load past events
        $('.past_events_trigger').on('click',function(e){
             e.preventDefault();
             loader.css('top','0');
                loader.css('bottom','auto');
                loading = true;
                $.ajax({
                url: '<?php echo $router->link('api_content_event_current',array(
                    'apikey' =>$apikey,
                    'user_id' => $userId,
                    'team_id' => $selectedTeamId,
                    'mode' => 'only_items'
                        )) ?>&period='+prev_period,
                        dataType: 'json',
                        success: function(response) {
                            scrollContainer.prepend(response.html);
                            scrollContainer.attr('data-prev-period',response.prevPeriod);
                            prev_period = response.prevPeriod;
                            loader.hide();
                            loading = false;
                            
                            $(".past_events_trigger").detach().prependTo('.infinity-scroll-container');
                        }  
                });   
        });
        
     //load future events
	win.scroll(function() {
            if ($(document).height() - win.height() == win.scrollTop() && loading==false) {
                loader.css('top','auto');
                loader.css('bottom','-30px');
            
                loader.show();
                loading = true;
                
                $.ajax({
                url: '<?php echo $router->link('api_content_event_current',array(
                    'apikey' =>$apikey,
                    'user_id' => $userId,
                     'team_id' => $selectedTeamId,
                    'mode' => 'only_items'
                        )) ?>&period='+next_period,
                        dataType: 'json',
                        success: function(response) {
                            scrollContainer.append(response.html);
                                
                            scrollContainer.attr('data-newxt-period',response.nextPeriod);
                            next_period = response.nextPeriod;
                            loader.hide();
                            loading = false;
                        }  
                });   
            }
	});
        
     
         
        
    
    
    
      
});
</script>
<?php  $layout->endSlot('javascript') ?>
