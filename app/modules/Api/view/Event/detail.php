 <div class="mobile-app-nav">
        <div class="navBtn"></div>
        <?php echo $translator->translate('api.content.event.detail') ?>
    </div>
<div class="content-event-detail-wrap">
<div class="row event-item past-item">
        <div class="col-sm-12">
           <span class="date">
        <?php echo substr($translator->translate($event->getCurrentDate()->format('D')),0,2) ?>. <?php echo $event->getCurrentDate()->format('d')  ?>. <?php echo $translator->translate($event->getCurrentDate()->format('F') ) ?> <?php echo $event->getStart()->format('H:i')  ?>
         </span>



            <h2><?php echo $event->getName() ?></h2>
             <span class="team-name">
            <?php echo $event->getTeamName() ?>
            <?php if(null != $event->getPlayground()): ?>
               (<?php echo $event->getPlayground()->getName() ?>)
           <?php endif; ?>              
        </span>

                    <?php if($event->getLineup() != null): ?>
         <div class="row vs_row">
            <div class="col-xs-5 text-right">
                <h4><?php echo $event->getLineup()->getFirstLineName() ?></h4>
                  <ul class="attendace_list">
                    <?php foreach($event->getLineup()->getLineupPlayers('first_line') as $player): ?>
                        <?php if($player->getTeamPlayer() != null): ?>
                            <li class="face" title="<?php echo $player->getPlayerName() ?>" style="background-image: url(<?php echo $player->getTeamPlayer()->getMidPhoto() ?>)"></li>
                        <?php else: ?>
                            <li class="face" title="<?php echo $player->getPlayerName() ?>" style="background-image: url('/img/users/default.svg')"></li>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </ul>
            </div>
             <div class="col-xs-2 vs-acr">
                <?php if($event->getLineup() == null or $event->getLineup()->getStatus() != 'closed'): ?>
                    <strong>vs</strong>
               <?php else: ?>
                    <?php echo $event->getMatchResult() ?>
               <?php endif; ?>
             </div>
            
            
            <div class="col-xs-5 text-left">
                 <h4><?php echo $event->getLineup()->getSecondLineName() ?></h4>
                 <ul class="attendace_list">
                    <?php foreach($event->getLineup()->getLineupPlayers('second_line') as $player): ?>
                        <?php if($player->getTeamPlayer() != null): ?>
                            <li class="face" title="<?php echo $player->getPlayerName() ?>" style="background-image: url(<?php echo $player->getTeamPlayer()->getMidPhoto() ?>)"></li>
                        <?php else: ?>
                            <li class="face" title="<?php echo $player->getPlayerName() ?>" style="background-image: url('/img/users/default.svg')"></li>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
        <?php endif; ?>
        </div>
</div>
<div class="row">
    <div class="col-sm-12">
         <table class="table table-striped table-rating table-match-stat">

                            <thead>
                                <tr>
                                    <th class="player"></th>
                                    <th class="goal">G</th>
                                    <th  class="assist">A</th>
                                    <th class="mom">MoM (<?php echo $translator->translate('Most valuable player') ?>)</th>
                                </tr>
                            </thead>   

                            <tbody>

                                <?php foreach ($matchStats as $lineupPlayerId => $playerStat): ?>
                                    <?php $lineupPlayer = $players[$lineupPlayerId] ?>
                                    <?php $teamMember = $lineupPlayer->getTeamPlayer() ?>
                                    <tr>
                                        <td>
                                            <div class="user-block">
                                                <?php if (null != $teamMember): ?>
                                                    <div class="round-50" style="background-image:url(<?php echo $teamMember->getMidPhoto() ?>)"></div>
                                                    <span class="username"><?php echo $lineupPlayer->getPlayerName() ?></span>                          
                                                <?php else: ?>
                                                    <span class="username"><?php echo $lineupPlayer->getPlayerName() ?></span>
                                                <?php endif; ?>
                                            </div><!-- /.user-block -->
                                        </td>
                                        <td><?php echo $playerStat->getGoals() ?></td>
                                        <td><?php echo $playerStat->getAssists() ?></td>
                                        <td><?php echo (1 == $playerStat->getManOfMatch()) ? 'M' : '' ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>

    </div>
</div>
</div>
<?php  $layout->startSlot('javascript') ?>
<script type="text/javascript">
   $(document).ready(function() {
	
       
        
         var messageOrigin;
        var messageSource;
        document.addEventListener('message', function (e) {
          messageOrigin = e.origin;
          messageSource = e.source;

        });
         $('.navBtn').on('click',function(e){
            e.preventDefault();
            var message = {
                'action': 'close',
            };
            
             window.postMessage(JSON.stringify(message) ,messageOrigin);
        });
});
</script>
<?php  $layout->endSlot('javascript') ?>
    