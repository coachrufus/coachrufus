<div id="system-notify-tool" class="row">
   
     <div class="col-xs-3">
          <a href="<?php echo WEB_DOMAIN.$router->generateApiWidgetUrl('api_content_webview',
                 array(
                    'apikey' => CR_API_KEY,
                    'view' => $router->link('api_content_notify_setup').'?apikey='.CR_API_KEY.'&user_id='.$user->getId(),
                    'user_id' => $user->getId()
                 )) ?>"><?php echo $translator->translate('api.systemNotify.settings') ?></a>
     </div>
     <div class="col-xs-9 text-right">
    <a class="notify_all_read" href="<?php echo WEB_DOMAIN.$router->generateApiWidgetUrl('api_content_webview',
                 array(
                    'apikey' => CR_API_KEY,
                    'view' => $router->link('api_content_notify').'?apikey='.CR_API_KEY.'&user_id='.$user->getId().'&ba=allread',
                    'user_id' => $user->getId()
                 )) ?>"><?php echo $translator->translate('api.systemNotify.allRead') ?></a>
    </div>
</div>

<div id="system-notify-cnt ">
    <div  id="team_notify_panel" class="infinity-scroll-container"  data-next-start="<?php echo $nextStart ?>">
            <?php foreach ($list->getItems() as $notify): ?>
               <?php $layout->includePart(MODUL_DIR . '/Api/view/Content/_notify_row.php',array('notify' => $notify,'stringUtil' => $stringUtil,'user' => $user)) ?>
            <?php endforeach; ?>
          <div class="row">
        <div class="col-xs-12 alert alert-info text-center loader">Loading</div>
   </div>
    </div>
</div>
 


<?php $layout->addJavascript('js/SystemNotifyManager.js') ?>
<?php $layout->startSlot('javascript') ?>

<script type="text/javascript">
    
     var messageOrigin;
    var messageSource;
    document.addEventListener('message', function (e) {
      messageOrigin = e.origin;
      messageSource = e.source;

    });
    
    
     $('.mpopup').on('click',function(e){
        e.preventDefault();
        var message = {
            'href':$(this).attr('href'),
            'action': $(this).attr('data-action'),
            'screen': $(this).attr('data-screen'),
            'onclose-action': $(this).attr('data-popup-onclose-action')
        };

         window.postMessage(JSON.stringify(message) ,messageOrigin);
    });
    
    var win = $(window);
    var scrollContainer = $('.infinity-scroll-container');
    var loader = $('.infinity-scroll-container .loader');
    var next_start = scrollContainer.attr('data-next-start');
    var loading = false;
     win.scroll(function() {
               
        if ($(document).height() - win.height() == win.scrollTop() && loading==false && 'end' != next_start) {
                loader.css('top','auto');
                loader.css('bottom','-30px');
                
                

                loader.show();
                loading = true;

                $.ajax({
                url: '<?php echo $router->link('api_content_notify',array(
                    'apikey' => CR_API_KEY ,
                    'user_id' => $user->getId(),
                    'mode' => 'only_items'
                        )) ?>&start='+next_start,
                        dataType: 'json',
                        success: function(response) {
                            scrollContainer.append(response.html);

                            scrollContainer.attr('data-next_start',response.nextStart);
                            next_start = response.nextStart;
                            loader.hide();
                            loading = false;

                        }  
                });   
            }
        
    });
        
    
    
    
  

</script>
<?php $layout->endSlot('javascript') ?>