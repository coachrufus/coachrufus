<h1>Chat</h1>


<a class="mpopup"
   data-action="redirect" 
   data-screen="home" >
    TEST1 Po kliknut ma uzivatela presmerovat na obrazovku home
</a>
<br />

<a class="mpopup"
   href="https://www.coachrufus.com/sk/"
   data-action="popup"  >
    TEST2 Po kliknuti zobrazi full screen webview s url https://www.coachrufus.com/sk/ a po kliknuti na back  okno zavrie
</a>
<br />
<a class="mpopup"
   href="https://www.coachrufus.com/sk/"
   data-action="popup"
   data-popup-onclose-action="refresh"
   >
    TEST3 Po kliknuti zobrazi full screen webview s url https://www.coachrufus.com/sk/ a po kliknuti na back  okno zavrie a refreshne webview
</a>
<br />
<a class="mpopup"
   href="https://www.coachrufus.com/sk/"
   data-action="popup" 
   data-popup-onclose-action="redirect"
   data-screen="home" >
    TEST4 Po kliknuti zobrazi full screen webview s url https://www.coachrufus.com/sk/ a po kliknuti na back  ma uzivatela presmerovat na obrazovku home
</a>

<br />


<a class="mpopup"
   href="https://www.coachrufus.com/sk/"
   data-action="popup-full" 
   data-popup-onclose-action="redirect"
   data-screen="home" >
    TEST5 Po kliknuti zobrazi full (ale fakt full) screen webview s url https://www.coachrufus.com/sk/ a po kliknuti na back  ma uzivatela presmerovat na obrazovku home
</a>
<br />

<a class="mpopup"
   data-action="redirect" 
   href="https://www.coachrufus.com/sk/"
   data-screen="home" >
    TEST6 Po kliknut ma uzivatela presmerovat na obrazovku home a nacitat webview s url https://www.coachrufus.com/sk/
</a>
<br />
<a class="mpopup"
   href="#"
   data-action="close" 
   data-screen="home"
   >
    TEST7 zavrie aktualne okno
</a>
<br />

<a class="mpopup"
   href="#"
   data-action="langChanged"
   >
    TEST8 jazyk uzivatela 
</a>
<br />
<script>
    
    
    var messageOrigin;
    var messageSource;
    document.addEventListener('message', function (e) {
      messageOrigin = e.origin;
      messageSource = e.source;
      //console.log( e.source);
      //messageSource.postMessage('webviewInit success', messageOrigin);
      //alert('frame alert'+e.data);
    });
    
    
    $('.mpopup').on('click',function(e){
        e.preventDefault();
        
        if($(this).attr('data-action') == 'langChanged')
        {
            var message = {
                'action': 'langChanged',
                'userLang': 'en'
            };
        }
        else
        {
           var message = {
                'href':$(this).attr('href'),
                'action': $(this).attr('data-action'),
                'screen': $(this).attr('data-screen'),
                'onclose-action': $(this).attr('data-popup-onclose-action')
            };
         
        }
        
        console.log(message);
        

        
         window.postMessage(JSON.stringify(message) ,messageOrigin);
        
    });
    
    
    

   
 
</script>
