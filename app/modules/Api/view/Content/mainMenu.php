<ul class="menu-list">
    <li>
        <a class="mpopup" data-action="popup-full" href="<?php echo WEB_DOMAIN.$router->generateApiWidgetUrl('api_content_webview',array('apikey' => CR_API_KEY,'view' => $router->link('profil'),'user_id' => $user->getId())) ?>">
            <span class="icon"><img src="<?php echo $user->getMidPhoto() ?>" class="user-image" alt="User Image"></span>
            <span class="cnt"><?php echo $user->getName() ?> 
                <?php echo $user->getSurname() ?>
            </span>
        </a>
    </li>
    
   
    
    
    <li class="section-last"><a class="mpopup" data-action="popup-full" href="<?php echo WEB_DOMAIN.$router->generateApiWidgetUrl('api_content_webview',array('apikey' => CR_API_KEY,'view' => $router->link('profil').'#sports','user_id' => $user->getId())) ?>"><span class="icon"></span><span class="cnt"><?php echo $translator->translate('api.mainMenu.sport') ?></span></a></li>
   
    <li class="heading"><?php echo $translator->translate('api.mainMenu.teamsHeading') ?></li>
    <li><a class="mpopup" data-action="popup-full" href="<?php echo WEB_DOMAIN.$router->generateApiWidgetUrl('api_content_webview',array('apikey' => CR_API_KEY,'view' => $router->link('scouting'),'user_id' => $user->getId())) ?>"><span class="icon"></span><span class="cnt"><?php echo $translator->translate('api.mainMenu.scouting') ?></span></a></li>
    <li><a class="mpopup" data-action="popup-full" href="<?php echo WEB_DOMAIN.$router->generateApiWidgetUrl('api_content_webview',array('apikey' => CR_API_KEY,'view' => $router->link('teams_overview'),'user_id' => $user->getId())) ?>"><span class="icon"></span><span class="cnt"><?php echo $translator->translate('api.mainMenu.teams') ?><span class="stat"><?php echo $teamsCount ?></span></span></a></li>
    <li><a class="mpopup" data-action="popup-full" href="<?php echo WEB_DOMAIN.$router->generateApiWidgetUrl('api_content_webview',array('apikey' => CR_API_KEY,'view' => $router->link('tournament'),'user_id' => $user->getId())) ?>"><span class="icon"></span><span class="cnt"><?php echo $translator->translate('api.mainMenu.tournaments') ?><span class="stat"><?php echo $tournamentsCount ?></span></span></a></li>
      <li class="section-last"><a class="mpopup" data-action="popup-full" href="<?php echo WEB_DOMAIN.$router->generateApiWidgetUrl('api_content_webview',array('apikey' => CR_API_KEY,'view' => $router->link('message_user_list'),'user_id' => $user->getId())) ?>"><span class="icon"></span><span class="cnt"><?php echo $translator->translate('api.mainMenu.messages') ?><span class="stat"><?php echo ($unreadMessages > 0) ? $unreadMessages : '' ?></span></span></a></li>
    
    
    
    
    <li class="heading"><?php echo $translator->translate('api.mainMenu.settings') ?></li>
    <li class="section-last"><a class="mpopup" data-action="popup-full" href="<?php echo WEB_DOMAIN.$router->generateApiWidgetUrl('api_content_webview',
                 array(
                    'apikey' => CR_API_KEY,
                    'view' => $router->link('api_content_notify_setup').'?apikey='.CR_API_KEY.'&w=full&user_id='.$user->getId(),
                    'user_id' => $user->getId()
                 )) ?>"><span class="icon ico-gear"></span><span class="cnt"><?php echo $translator->translate('api.mainMenu.notifySettings') ?></span></a></li>
  
  
</ul>

<?php 
$platformColor = '#ffffff';

if($_SESSION['api']['platform']  == 'iphone')
{
    $platformColor = 'green';
    $reviewLink = 'https://itunes.apple.com/us/app/rufus-your-favourite-teammate/id1435067809?mt=8&action=write-review';
}

if($_SESSION['api']['platform']  == 'android')
{
    $platformColor = 'red';
    $reviewLink = 'https://play.google.com/store/apps/details?id=com.altamira.rufus&reviewId=0';
}
?>
 <div class="platform_check" style="display:block;background:<?php echo $platformColor ?>">&nbsp;</div>
 
 <a class="btn btn-review mpopup" data-action="popup-full" href="<?php echo  $reviewLink ?>"><?php echo $translator->translate('mainmenu.writeAppReview') ?></a>
 
 



<?php  $layout->startSlot('javascript') ?>
<script type="text/javascript">
   $(document).ready(function() {
	
       
        
         var messageOrigin;
        var messageSource;
        document.addEventListener('message', function (e) {
          messageOrigin = e.origin;
          messageSource = e.source;

        });
         $('.mpopup').on('click',function(e){
            e.preventDefault();
            var message = {
                'href':$(this).attr('href'),
                'action': $(this).attr('data-action'),
                'screen': $(this).attr('data-screen'),
                'onclose-action': $(this).attr('data-popup-onclose-action')
            };
            
             window.postMessage(JSON.stringify(message) ,messageOrigin);
        });
});
</script>
<?php  $layout->endSlot('javascript') ?>
