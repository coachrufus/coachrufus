<?php
$teamManager = \Core\ServiceLayer::getService('TeamManager');
$teams = $teamManager->getUserTeams($user);
?>


<div class="row team-choice-panel text-center">

    <div class="custom_native_select">
        <select id="team_select_native" name="team" style="max-width: 160px;">
            <option value=""><?php echo $translator->translate('api.teamSelectChoice') ?></option>
<?php foreach ($teams as $team): ?>
                <option <?php echo $selectedTeamId == $team->getId() ? 'selected="selected"' : '' ?> value="<?php echo $team->getId() ?>"><?php echo $team->getName() ?> 
                    ( 
                        <?php if ($team->getGender() == 'woman'): ?>
                            <?php echo $translator->translate('api.teamSelectFemale') ?>
                        <?php elseif ($team->getGender() == 'man'): ?>
                             <?php echo $translator->translate('api.teamSelectMale') ?>
                        <?php elseif ($team->getGender() == 'mixed'): ?>
                        <?php echo $translator->translate('api.teamSelectMixed') ?>
                            <i class="fa fa-female" /> <i class="fa fa-male" />
                        <?php endif ?>
                )</option>
            <?php endforeach; ?>
        </select>
    </div>

</div>