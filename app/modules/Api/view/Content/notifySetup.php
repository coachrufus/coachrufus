<form action="" id="notify_form_settings">
<ul class="list-group">
    <li class="list-group-item heading"><h4><?php echo $translator->translate('api.settings.push.heading') ?></h4></li>
    <li class="list-group-item">
        <h4><?php echo $translator->translate('api.settings.push.hp_wall_post.title') ?></h4>
        <div class="checkbox">
            <label>
                <input <?php echo ( array_key_exists('hp_wall_post', $settings['push_notify']) && $settings['push_notify']['hp_wall_post'] == 1  ) ? 'checked="checked"' : '' ?>  type="checkbox" class="no-icheck" value="1" name="push_notify[hp_wall_post]"><?php echo $translator->translate('api.setting.push.hp_wall_post.desc') ?>
            </label>
        </div>
    </li>
    <li class="list-group-item">
        <h4><?php echo $translator->translate('api.settings.push.event_capacity.title') ?></h4>
        <div class="checkbox">
            <label>
                <input <?php echo ( array_key_exists('event_capacity', $settings['push_notify']) && $settings['push_notify']['event_capacity'] == 1  ) ? 'checked="checked"' : '' ?>  type="checkbox" class="no-icheck" value="1" name="push_notify[event_capacity]"><?php echo $translator->translate('api.setting.push.event_capacity.desc') ?>
            </label>
        </div>
    </li>
     <li class="list-group-item">
        <h4><?php echo $translator->translate('api.settings.push.event_attendance.title') ?></h4>
        <div class="checkbox">
            <label>
                <input <?php echo ( array_key_exists('event_attendance', $settings['push_notify']) && $settings['push_notify']['event_attendance'] == 1  ) ? 'checked="checked"' : '' ?>  type="checkbox" class="no-icheck" value="1" name="push_notify[event_attendance]"><?php echo $translator->translate('api.setting.push.event_attendance.desc') ?>
            </label>
        </div>
    </li>
    <li class="list-group-item">
        <h4><?php echo $translator->translate('api.settings.push.create_player.title') ?></h4>
        <div class="checkbox">
            <label>
                <input <?php echo ( array_key_exists('create_player', $settings['push_notify']) && $settings['push_notify']['create_player'] == 1  ) ? 'checked="checked"' : '' ?>  type="checkbox" class="no-icheck" value="1" name="push_notify[create_player]"><?php echo $translator->translate('api.setting.push.create_player.desc') ?>
            </label>
        </div>
    </li>
    <li class="list-group-item">
        <h4><?php echo $translator->translate('api.settings.push.delete_event.title') ?></h4>
        <div class="checkbox">
            <label>
                <input <?php echo ( array_key_exists('delete_event', $settings['push_notify']) && $settings['push_notify']['delete_event'] == 1  ) ? 'checked="checked"' : '' ?>  type="checkbox" class="no-icheck" value="1" name="push_notify[delete_event]"><?php echo $translator->translate('api.setting.push.delete_event.desc') ?>
            </label>
        </div>
    </li>
    <li class="list-group-item">
        <h4><?php echo $translator->translate('api.settings.push.create_lineup.title') ?></h4>
        <div class="checkbox">
            <label>
                <input <?php echo ( array_key_exists('create_lineup', $settings['push_notify']) && $settings['push_notify']['create_lineup'] == 1  ) ? 'checked="checked"' : '' ?>  type="checkbox" class="no-icheck" value="1" name="push_notify[create_lineup]"><?php echo $translator->translate('api.setting.push.create_lineup.desc') ?>
            </label>
        </div>
    </li>
  
    <li class="list-group-item">
        <h4><?php echo $translator->translate('api.settings.push.close_match.title') ?></h4>
        <div class="checkbox">
            <label>
                <input <?php echo ( array_key_exists('close_match', $settings['push_notify']) && $settings['push_notify']['close_match'] == 1  ) ? 'checked="checked"' : '' ?>  type="checkbox" class="no-icheck" value="1" name="push_notify[close_match]"><?php echo $translator->translate('api.setting.push.close_match.desc') ?>
            </label>
        </div>
    </li>
    
 <li class="list-group-item"><strong><?php echo $translator->translate('api.settings.push.heading.admin') ?></strong></li>
    <li class="list-group-item">
        <h4><?php echo $translator->translate('api.settings.push.missing_players.title') ?></h4>
        <div class="checkbox">
            <label>
                <input <?php echo ( array_key_exists('missing_players', $settings['push_notify']) && $settings['push_notify']['missing_players'] == 1  ) ? 'checked="checked"' : '' ?>   type="checkbox" class="no-icheck" value="1" name="push_notify[missing_players]"><?php echo $translator->translate('api.setting.push.missing_players.desc') ?>
            </label>
        </div>
    </li>
    <li class="list-group-item">
        <h4><?php echo $translator->translate('api.settings.push.player_leave_team.title') ?></h4>
        <div class="checkbox">
            <label>
                <input <?php echo ( array_key_exists('player_leave_team', $settings['push_notify']) && $settings['push_notify']['player_leave_team'] == 1  ) ? 'checked="checked"' : '' ?>   type="checkbox" class="no-icheck" value="1" name="push_notify[player_leave_team]"><?php echo $translator->translate('api.setting.push.player_leave_team.desc') ?>
            </label>
        </div>
    </li>

    
    
   
    
    


  <li class="list-group-item heading"><h4><?php echo $translator->translate('api.settings.email_notify.heading') ?></h4></li>
<?php foreach($emailNotifyCodes as $emailNotify): ?>
 <li class="list-group-item">
    <h4><?php echo $translator->translate('api.settings.email_notify.'.$emailNotify.'.title') ?></h4>
    <div class="checkbox">
        <label>
            <input <?php echo ( array_key_exists($emailNotify, $settings['email_notify']) && $settings['email_notify'][$emailNotify] == 1  ) ? 'checked="checked"' : '' ?>  type="checkbox" class="no-icheck" value="1" name="email_notify[<?php echo $emailNotify ?>]"><?php echo $translator->translate('api.setting.email_notify.'.$emailNotify.'.desc') ?>
        </label>
    </div>
</li>
<?php endforeach; ?>

  <li class="list-group-item heading"><h4><?php echo $translator->translate('api.settings.tams.heading') ?></h4></li>
        <?php foreach($teams as $team): ?>
         <li class="list-group-item">
            <div class="checkbox">
                <label>
                    <input <?php echo ( array_key_exists($team->getId(), $settings['team_notify']) && $settings['team_notify'][$team->getId()] == 1  ) ? 'checked="checked"' : '' ?>  type="checkbox" class="no-icheck" value="1" name="team_notify[<?php echo $team->getId() ?>]"><?php echo $team->getName() ?>
                </label>
            </div>
        </li>
        <?php endforeach; ?>
    </ul>
</form>



<?php $layout->startSlot('javascript') ?>
<script type="text/javascript">
    
    
    $('#notify_form_settings input').on('change',function(){
        var form_data = $('#notify_form_settings').serialize();
          $.ajax({
                url: '<?php echo $router->generateApiWidgetUrl('api_content_webview',array('apikey' => CR_API_KEY,'view' => $router->link('api_system_notify_setup',array('apikey' => CR_API_KEY,'user_id' => $user_id)),'user_id' => $user_id))?>&'+form_data,
                        dataType: 'json',
                        success: function(response) {
                          

                        }  
                });   
    });
    
    
  

</script>
<?php $layout->endSlot('javascript') ?>