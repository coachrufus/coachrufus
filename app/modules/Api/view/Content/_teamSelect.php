<?php
$teamManager = \Core\ServiceLayer::getService('TeamManager');
$teams = $teamManager->getUserTeams($user);


?>


<div class="row team-choice-panel text-center">
    <div class="btn-group">
        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <?php echo (null == $selectedTeamId) ? $translator->translate('api.teamSelect') : $teams[$selectedTeamId]->getName() ?> <span class="caret"></span>
            
        </button>
        <ul class="dropdown-menu">
            <li><a href="<?php echo $link ?>"><?php echo $translator->translate('api.teamSelect') ?></a></li>
            <?php foreach($teams as $team): ?>
            <li><a href="<?php echo  WEB_DOMAIN.$link ?>&team_id=<?php echo $team->getId() ?>"><?php echo $team->getName() ?></a></li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>