<?php $layout->addStylesheet('js/Api/homewall/styles/index.css') ?>
<?php $layout->addJavascript('js/Api/homewall/bundle.js') ?>
<?php if($a == 'edit'): ?>
<div id="homewall" data-uid="<?php echo $userId ?>" data-tab="<?php echo $tab ?>" data-action="editPost" data-post-id="<?php echo $postEdit ?>" data-lang="<?php echo $user->getDefaultLang() ?>"></div>
<?php elseif($a == 'create'): ?>
<div id="homewall" data-uid="<?php echo $userId ?>" data-tab="<?php echo $tab ?>" data-action="createPost" data-lang="<?php echo $user->getDefaultLang() ?>"></div>
<?php elseif($a == 'createWindowPopup'): ?>
<div id="homewall" data-uid="<?php echo $userId ?>" data-tab="<?php echo $tab ?>" data-action="createPostPopup" data-lang="<?php echo $user->getDefaultLang() ?>"></div>
<?php elseif($a == 'closePopups'): ?>
<div id="homewall" data-uid="<?php echo $userId ?>" data-tab="<?php echo $tab ?>" data-action="closePopups" data-lang="<?php echo $user->getDefaultLang() ?>"></div>
<?php elseif($a == 'comments'): ?>
<div id="homewall" data-uid="<?php echo $userId ?>" data-tab="<?php echo $tab ?>" data-action="addComment" data-post-id="<?php echo $postEdit ?>" data-lang="<?php echo $user->getDefaultLang() ?>"></div>
<?php else: ?>
<div id="homewall" data-uid="<?php echo $userId ?>" data-tab="<?php echo $tab ?>" data-lang="<?php echo $user->getDefaultLang() ?>" data-lang="<?php echo $user->getDefaultLang() ?>"></div>
<?php endif; ?>

<?php if($pm == 'close'): ?>

<?php  $layout->startSlot('javascript') ?>
<script type="text/javascript">
    var messageOrigin;
    var messageSource;
    document.addEventListener('message', function (e) {
      messageOrigin = e.origin;
      messageSource = e.source;

    });
    
     var message = {
        'action': 'redirect',
        'screen': 'home'
    };

     window.postMessage(JSON.stringify(message) ,messageOrigin);
    
</script>
<?php  $layout->endSlot('javascript') ?>
<?php endif; ?>
