<a name="<?php echo $notify->getId() ?>"></a>

<?php 
$linkData = $notify->getMobileLinkData();
?>

<div class="system-notify-row <?php echo $notify->getStatus() ?> "> 
    <div class="sn-avatar-wrap">
        <?php if( $notify->getSenderId() != null ): ?>
            <i class="sn-avatar" style="background-image: url('<?php echo $notify->getUserPhoto() ?>');"></i>
        <?php else: ?>
            <i class="sn-avatar" style="background-image: url('<?php echo $notify->getTeamPhoto() ?>');"></i>
        <?php endif; ?>
        
        
       
    </div>
    <div class="sn-cnt">
        
          
        
        <?php if($linkData['window'] == 'fullpopup'): ?>
             <a class="mpopup" data-action="popup-full" data-screen="<?php echo $linkData['screen']  ?>" href="<?php echo WEB_DOMAIN.$router->generateApiWidgetUrl('api_content_webview',
                 array(
                    'apikey' => CR_API_KEY,
                    'view' => $router->link('api_content_notify').'?apikey='.CR_API_KEY.'&user_id='.$user->getId().'&mid='.$notify->getId(),
                    'user_id' => $user->getId()
                 )) ?>">
        
        <?php elseif($linkData['window'] == 'webview'): ?>
            <a class="mpopup" data-action="redirect" data-screen="<?php echo $linkData['screen'] ?>" href="<?php echo  $linkData['url']  ?>" >
        <?php endif; ?>
        
        
       
            <strong><?php echo $notify->getSubject() ?></strong>
            <span><?php echo $notify->getBody() ?></span>
         </a>
         <span class="created-date"><?php echo $stringUtil->formatTime($notify->getCreatedAt()->getTimestamp(),'d.m. H:i') ?></span>
    </div>
    
    
</div>