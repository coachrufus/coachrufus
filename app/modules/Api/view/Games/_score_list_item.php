   <?php foreach($events as $dayEvents): ?>
    <?php foreach($dayEvents as $event): ?>  

    <?php  
    $timeLimit = DateTime::createFromFormat('Y-m-d H:i:s', date('Y-m-d').' 00:00:00');
    ?>



<?php if($event->getAttendanceInSum() >= 1 && (empty($event->getLineup())  or  $event->getLineup()->getStatus() != 'closed')): //only items with more than 2 players and not closed match ?>

   <div class="row event-item <?php echo ($event->getCurrentDate()->getTimestamp() < $timeLimit->getTimestamp()) ? 'past-item' : 'future-item' ?>">
        <div class="col-sm-12">
            <?php $layout->includePart(MODUL_DIR . '/Api/view/Event/_event_sidebar.php',array(
                    'apikey' => $apikey,
                    'userId' => $userId,
                    'event' => $event,
                    'teams' => $teams,
                    'teamsPlayers' => $teamsPlayers,
                    'userTeamPlayerIds' => $userTeamPlayerIds)) ?>
                <?php if($event->getCurrentDate()->format('Y-m-d') == date('Y-m-d')): ?>
                <span class="date today-date">
                    <?php echo $translator->translate('api.event.today') ?> <?php echo $event->getCurrentDate()->format('H:i')  ?>
                 </span>
                <?php else: ?>
                <span class="date">
                <?php echo substr($translator->translate($event->getCurrentDate()->format('D')),0,2) ?>. <?php echo $event->getCurrentDate()->format('d')  ?>. <?php echo $translator->translate($event->getCurrentDate()->format('F') ) ?> <?php echo $event->getCurrentDate()->format('H:i')  ?>
                 </span>
                <?php endif; ?>
                
            <div class="mpopup1" data-action="popup-full" data-href="<?php echo WEB_DOMAIN.$router->link('team_event_detail',array('id' => $event->getId(),'current_date' => $event->getCurrentDate()->format('Y-m-d') )) ?>">
        <h2><?php echo $event->getName() ?></h2>
        <span class="team-name">
            <?php echo $event->getTeamName() ?>
            <?php if(null != $event->getPlayground()): ?>
               (<?php echo $event->getPlayground()->getName() ?>)
           <?php endif; ?>
                            
        </span>
        
        <ul class="attendace_list">
            <?php foreach($event->getAttendance() as $attendance): ?>
            <?php if($attendance->getStatus() == '1'): ?>
                <li class="face" style="background-image: url(<?php echo $attendance->getPlayer()->getMidPhoto() ?>)"></li>
           <?php endif; ?>
            
            <?php endforeach; ?>
            
            
          
            <li class="sum"><?php echo  $event->getAttendanceInSum() ?><span>/<?php echo  $event->getCapacity() ?></span></li>
        </ul>
            </div>
       
        
            <?php if($event->getCurrentDate()->getTimestamp()-(6*60*60) < time() && $event->getStatRoute() == 'team_match_create_live_stat'): ?>
                 <?php if($event->getLineup() != null && $event->getLineup()->getStatus() != 'closed'): ?>
                    <a class="cta-btn mpopup" data-action="popup-full" data-popup-onclose-action="refresh" href="<?php echo WEB_DOMAIN.$router->generateApiWidgetUrl('api_content_games_score_widget',array('apikey' => $apikey,'user_id' => $userId,'event_id' => $event->getId(),'lineup_id' => $event->getLineup()->getId() ,'event_date' => $event->getCurrentDate()->format('Y-m-d'))) ?>"><?php echo $translator->translate('event.past.addLiveScore') ?></a>
                 <?php endif; ?>

                 <?php if($event->getLineup() == null && $event->getStatRoute() == 'team_match_create_live_stat'): ?>
                    <a class="cta-btn cta-btn-missing-lineup mpopup" data-action="popup-full" data-popup-onclose-action="refresh" href="<?php echo  WEB_DOMAIN.$router->generateApiWidgetUrl('api_content_games_lineup_widget',array('apikey' => $apikey,'user_id' => $userId,'event_id' => $event->getId(),'event_date' => $event->getCurrentDate()->format('Y-m-d'))) ?>"><?php echo $translator->translate('event.past.missingLineup') ?></a>
                    <span class="cta-btn cta-btn-disabled"><?php echo $translator->translate('event.past.addLiveScore') ?></span>
                <?php endif; ?>
            <?php else: ?>
                 <?php if($event->getLineup() == null && null != $event->getStatRoute()): ?>
                    <a class="cta-btn mpopup" data-action="popup-full" data-popup-onclose-action="refresh" href="<?php echo  WEB_DOMAIN.$router->generateApiWidgetUrl('api_content_games_lineup_widget',array('apikey' => $apikey,'user_id' => $userId,'event_id' => $event->getId(),'event_date' => $event->getCurrentDate()->format('Y-m-d'))) ?>"><?php echo $translator->translate('event.past.createLineup') ?></a>
                 <?php else: ?>
                      <a class="cta-btn mpopup" data-action="popup-full" data-popup-onclose-action="refresh" href="<?php echo WEB_DOMAIN.$router->generateApiWidgetUrl('api_content_games_lineup_widget',array('apikey' => $apikey,'user_id' => $userId,'event_id' => $event->getId(),'event_date' => $event->getCurrentDate()->format('Y-m-d'))) ?>"><?php echo $translator->translate('event.past.editLineup') ?></a>
                <?php endif; ?>

            <?php endif; ?> 
            

        </div>
    </div>

<?php endif; ?>

 <?php endforeach; ?>
<?php endforeach; ?>
