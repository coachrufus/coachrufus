<div class="current-events-holder container">
    <div class="row top-panel">
        <div class="col-xs-3 btn-back">
           
        </div>
        <div class="col-xs-6 text-center">
             <?php $layout->includePart(MODUL_DIR . '/Api/view/Content/_teamNativeSelect.php',array(
                'user' => $user,
                'selectedTeamId' => $selectedTeamId,
                'link' => $router->link('api_content_games_score',array('apikey' => $apikey,'user_id' => $userId))
             )) ?>
            
        </div>
        <div class="col-xs-3 text-right top-toolbar">

        </div>
    </div>

    <div class="infinity-scroll-container event-list" data-next-period="<?php echo $nextPeriod ?>" data-prev-period="<?php echo $prevPeriod ?>">
        <a class="past_events_trigger" href=""><?php echo $translator->translate('event.past.moreBtn') ?></a>
 
         <?php $layout->includePart(MODUL_DIR . '/Api/view/Games/_score_list_item.php',array(
              'apikey' => $apikey,
                'userId' => $userId,
             'events' => $calendarEventsList,
              'teams' => $teams,
             'teamsPlayers' => $teamsPlayers,
             'userTeamPlayerIds' => $userTeamPlayerIds)) ?>
   <div class="row">
        <div class="col-xs-12 alert alert-info text-center loader">Loading</div>
   </div>
    </div> 
   
</div>


<?php $layout->addJavascript('js/TeamEventManager.js') ?>
<?php $layout->addJavascript('js/WebviewManager.js') ?>
<?php  $layout->startSlot('javascript') ?>
<script type="text/javascript">
   $(document).ready(function() {
       
         $('#team_select_native').on('change',function(){
            location.href='<?php echo  WEB_DOMAIN.$router->link('api_content_games_score', array('apikey' => $apikey, 'user_id' => $userId)).'&team_id=' ?>'+$(this).val();
        });
	
        event_manger = new TeamEventsManager();
        webviewManager = new WebviewManager();
        webviewManager.handleSidebar();
        webviewManager.handlePostMessages();
        
      
        if($(".past-item").last().length > 0)
        {
             $('html, body').animate({
            scrollTop: $(".past-item").last().offset().top+140
        }, 100);
        }
        
      
      
        $(document).on('click','.attendance_trigger',function(e){
            e.preventDefault();

            var elem = $(this);
            var type = $(this).attr('data-rel');
            var elemContainer = elem.parents('.row');
            var elemPar = $(this).parent();
            
            elemPar.html('<i class="fa fa-refresh fa-spin fa-fw"></i>');
            
            event_manger.changePlayerAttendance($(this),function(){
                
                
                elemPar.html('');
                elemPar.append(elem);
                
                if(type == 'in')
                {
                    elemContainer.find('.attendance_trigger').addClass('attend-accept');
                    elemContainer.find('.attendance_trigger').removeClass('attend-deny');
                }
                
                if(type == 'out')
                {
                    elemContainer.find('.attendance_trigger').addClass('attend-deny');
                    elemContainer.find('.attendance_trigger').removeClass('attend-accept');
                }
            });
        });
        
        
        var win = $(window);
        var scrollContainer = $('.infinity-scroll-container');
        var loader = $('.infinity-scroll-container .loader');
        var next_period = scrollContainer.attr('data-next-period');
        var prev_period = scrollContainer.attr('data-prev-period');
        var loading = false;
        //load past events
        $('.past_events_trigger').on('click',function(e){
             e.preventDefault();
             loader.css('top','0');
                loader.css('bottom','auto');
                loading = true;
                $.ajax({
                url: '<?php echo $router->link('api_content_games_score',array(
                    'apikey' =>$apikey,
                    'user_id' => $userId,
                    'team_id' => $selectedTeamId,
                    'mode' => 'only_items'
                        )) ?>&period='+prev_period,
                        dataType: 'json',
                        success: function(response) {
                            scrollContainer.prepend(response.html);
                            scrollContainer.attr('data-prev-period',response.prevPeriod);
                            prev_period = response.prevPeriod;
                            loader.hide();
                            loading = false;
                            
                            $(".past_events_trigger").detach().prependTo('.infinity-scroll-container');
                        }  
                });   
        });
        
     //load future events
	win.scroll(function() {
            if ($(document).height() - win.height() == win.scrollTop() && loading==false) {
                loader.css('top','auto');
                loader.css('bottom','-30px');
            
                loader.show();
                loading = true;
                
                $.ajax({
                url: '<?php echo $router->link('api_content_games_score',array(
                    'apikey' =>$apikey,
                    'user_id' => $userId,
                     'team_id' => $selectedTeamId,
                    'mode' => 'only_items'
                        )) ?>&period='+next_period,
                        dataType: 'json',
                        success: function(response) {
                            scrollContainer.append(response.html);
                                
                            scrollContainer.attr('data-newxt-period',response.nextPeriod);
                            next_period = response.nextPeriod;
                            loader.hide();
                            loading = false;
                        }  
                });   
            }
	});
        

      
});
</script>
<?php  $layout->endSlot('javascript') ?>
