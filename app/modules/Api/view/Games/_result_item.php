   <?php foreach($events as $event): ?>  

    <?php  
    $timeLimit = DateTime::createFromFormat('Y-m-d H:i:s', date('Y-m-d').' 00:00:00');
    ?>



<?php if( (!empty($event->getLineup())  &&  $event->getLineup()->getStatus() == 'closed')): ?>

<div class="row event-item past-item">
        <div class="col-sm-12">
            
             <?php $layout->includePart(MODUL_DIR . '/Api/view/Event/_event_sidebar.php',array(
             'apikey' => $apikey,
                 'userId' => $userId,
                 'event' => $event,
             'teams' => $teams,
             'teamsPlayers' => $teamsPlayers,
             'userTeamPlayerIds' => $userTeamPlayerIds)) ?>

        <span class="date">
        <?php echo substr($translator->translate($event->getCurrentDate()->format('D')),0,2) ?>. <?php echo $event->getCurrentDate()->format('d')  ?>. <?php echo $translator->translate($event->getCurrentDate()->format('F') ) ?> <?php echo $event->getCurrentDate()->format('H:i')  ?>
         </span>
            
            
           
          
               
                
        <div class="mpopup1" data-action="popup-full" data-href="<?php echo WEB_DOMAIN.$router->generateApiWidgetUrl('api_content_webview',
                    array(
                        'apikey' => CR_API_KEY,
                        'view' => $router->link('api_content_event_detail',
                                array(
                                    'apikey' => $apikey,
                                    'user_id' => $userId,
                                    'id' => $event->getId(),
                                    'current_date' => $event->getCurrentDate()->format('Y-m-d')
                                )),
                        'user_id' => $userId)
                    ) ?>">
        <h2><?php echo $event->getName() ?></h2>
        <span class="team-name">
            <?php echo $event->getTeamName() ?>
            <?php if(null != $event->getPlayground()): ?>
               (<?php echo $event->getPlayground()->getName() ?>)
           <?php endif; ?>              
        </span>
        
        <?php if($event->getLineup() != null): ?>
         <div class="row vs_row">
            <div class="col-xs-5 text-right">
                <h4><?php echo $event->getLineup()->getFirstLineName() ?></h4>
                  <ul class="attendace_list">
                    <?php foreach($event->getLineup()->getLineupPlayers('first_line') as $player): ?>
                        <?php if($player->getTeamPlayer() != null): ?>
                            <li class="face" title="<?php echo $player->getPlayerName() ?>" style="background-image: url(<?php echo $player->getTeamPlayer()->getMidPhoto() ?>)"></li>
                        <?php else: ?>
                            <li class="face" title="<?php echo $player->getPlayerName() ?>" style="background-image: url('/img/users/default.svg')"></li>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </ul>
            </div>
             <div class="col-xs-2 vs-acr">
                <?php if($event->getLineup() == null or $event->getLineup()->getStatus() != 'closed'): ?>
                    <strong>vs</strong>
               <?php else: ?>
                    <?php echo $event->getMatchResult() ?>
               <?php endif; ?>
             </div>
            
            
            <div class="col-xs-5 text-left">
                 <h4><?php echo $event->getLineup()->getSecondLineName() ?></h4>
                 <ul class="attendace_list">
                    <?php foreach($event->getLineup()->getLineupPlayers('second_line') as $player): ?>
                        <?php if($player->getTeamPlayer() != null): ?>
                            <li class="face" title="<?php echo $player->getPlayerName() ?>" style="background-image: url(<?php echo $player->getTeamPlayer()->getMidPhoto() ?>)"></li>
                        <?php else: ?>
                            <li class="face" title="<?php echo $player->getPlayerName() ?>" style="background-image: url('/img/users/default.svg')"></li>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
        

        
        
        
            <?php if($event->getLineup() != null && $event->getLineup()->getStatus() != 'closed'): ?>
             <a class="cta-btn" href="<?php echo $router->generateApiWidgetUrl('api_content_games_score_widget',array('apikey' => $apikey,'user_id' => $userId,'event_id' => $event->getId(),'lineup_id' => $event->getLineup()->getId() ,'event_date' => $event->getCurrentDate()->format('Y-m-d'))) ?>"><?php echo $translator->translate('event.past.addScore') ?></a>
             <?php endif; ?>
        <?php else: ?>
            <a class="cta-btn" href="<?php echo $router->generateApiWidgetUrl('api_content_games_lineup_widget',array('apikey' => $apikey,'user_id' => $userId,'event_id' => $event->getId(),'event_date' => $event->getCurrentDate()->format('Y-m-d'))) ?>"><?php echo $translator->translate('event.past.createLineup') ?></a>
        
           
        
        <?php endif; ?>
       
        
        
      
             </div>
        </div>
    </div>
    
<?php endif; ?>
    
 <?php endforeach; ?>
