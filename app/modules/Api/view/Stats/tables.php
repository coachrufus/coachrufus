
    
<?php if(null != $teamId): ?>
<div class="container">
 <div class="row top-panel">
        <div class="col-xs-3 btn-back">
        </div>
        <div class="col-xs-6 text-center">
            <?php
            $layout->includePart(MODUL_DIR . '/Api/view/Content/_teamNativeSelect.php', array(
                'user' => $user,
                'selectedTeamId' => $teamId,
                'link' => $router->link('api_content_stats_tables', array('apikey' => $apikey, 'user_id' => $userId))
            ))
            ?>
        </div>
        <div class="col-xs-3 text-right top-toolbar">
        </div>
    </div>
    
    <div class="row stats-wrap">
        <div class="col-sm-12" style="padding:0;">

                <?php $layout->addStylesheet('js/Api/stats/styles/index.css') ?>
                <?php $layout->addJavascript('js/Api/stats/bundle.js') ?>
                <div id="stat_widget" data-t="<?php echo $teamId ?>" data-s="" data-h="" data-api-provider="<?php echo WEB_DOMAIN ?>"></div>
        </div>
    </div>
    
    </div>
<?php else: ?>
    
    
<div class="rufus-alert rufus-alert-missing-team rufus-alert-stats-team">
    <img class="rufus-alert-buble" src="/img/error/back.png">
    <div class="rufus-alert-text">
        <h1><?php echo $translator->translate('api.alert.stats.choiceTeam.title') ?></h1>
        <strong><?php echo $translator->translate('api.alert.stats.choiceTeam.text') ?></strong>
        
        
          <?php
            $layout->includePart(MODUL_DIR . '/Api/view/Content/_teamNativeSelect.php', array(
                'user' => $user,
                'selectedTeamId' => $teamId,
                'link' => $router->link('api_content_stats_tables', array('apikey' => $apikey, 'user_id' => $userId))
            ))
            ?>
        
        
        
       
       
    </div>
    <img class="rufus-alert-body" src="/img/rufus/rufus_ukazuje@2x.png" >
</div>
    
<?php endif; ?>
    
   




<?php  $layout->startSlot('javascript') ?>
<script type="text/javascript">
$(document).ready(function() {
    $('#team_select_native').on('change',function(){
        location.href='<?php echo  WEB_DOMAIN.$router->link('api_content_stats_tables', array('apikey' => $apikey, 'user_id' => $userId)).'&team_id=' ?>'+$(this).val();
    });
});
</script>
<?php  $layout->endSlot('javascript') ?>
   



