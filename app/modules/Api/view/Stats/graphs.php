<?php if(null != $teamId): ?>

<div class="container"  style="background: white;">
 <div class="row top-panel">
        <div class="col-xs-3 btn-back">
        </div>
        <div class="col-xs-6 text-center">
                        <br />

            <?php
            $layout->includePart(MODUL_DIR . '/Api/view/Content/_teamNativeSelect.php', array(
                'user' => $user,
                'selectedTeamId' => $teamId,
                'link' => $router->link('api_content_stats_graphs', array('apikey' => $apikey, 'user_id' => $userId))
            ))
            ?>
        </div>
        <div class="col-xs-3 text-right top-toolbar">
        </div>
    </div>
    
    <?php if($errorAlertContent != false): ?>
        <br /> <?php echo $errorAlertContent  ?>
    
    <?php else: ?>

    
    
    <div class="row stats-wrap">
        <div class="col-md-12" style="margin-top: 20px">
        <div class="container-fluid">
          
           
                <form class="navbar-form navbar-center" role="search">
                    <div class="form-group">
                        <div class="dropdown" id="seasonDropdownMenu">
                            <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                <span class="value"><?php echo is_null($actualSeason) ? $translator->translate('Season') : $actualSeason['fullName']; ?></span>&nbsp;&nbsp;<span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                <?php $isFirstSeason = true; foreach ($seasons as $season): ?>
                                    <li><a href="#" class="change-sesason-trigger" data-value="<?php echo $season['id'] ?>"><?php echo $season['fullName']; ?></a></li>
                                    <?php if ($isFirstSeason): ?>
                                        <?php if (count($seasons) > 1): ?>
                                            <li role="separator" class="divider"></li>
                                        <?php endif; ?>
                                        <?php $isFirstSeason = false; ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                </form>
                <span id="seasonLoading" class="loading"></span>
            </div>
        </div>
        
        <input type="hidden" id="team-id" value="<?php echo $teamId ?>">
        <section class="content" id="charts-container"></section>
    </div>
    <?php endif; ?>
    </div>
<?php else: ?>
    
    
<div class="rufus-alert rufus-alert-missing-team rufus-alert-stats-team">
    <img class="rufus-alert-buble" src="/img/error/back.png">
    <div class="rufus-alert-text">
        <h1><?php echo $translator->translate('api.alert.stats.choiceTeam.title') ?></h1>
        <strong><?php echo $translator->translate('api.alert.stats.choiceTeam.text') ?></strong>
        
        
          <?php
            $layout->includePart(MODUL_DIR . '/Api/view/Content/_teamNativeSelect.php', array(
                'user' => $user,
                'selectedTeamId' => $teamId,
                'link' => $router->link('api_content_stats_graphs', array('apikey' => $apikey, 'user_id' => $userId))
            ))
            ?>
    </div>
    <img class="rufus-alert-body" src="/img/rufus/rufus_ukazuje@2x.png" >
</div>
    
<?php endif; ?>
    
   




<?php  $layout->startSlot('javascript') ?>
<script type="text/javascript">
$(document).ready(function() {
    $('#team_select_native').on('change',function(){
        location.href='<?php echo  WEB_DOMAIN.$router->link('api_content_stats_graphs', array('apikey' => $apikey, 'user_id' => $userId)).'&team_id=' ?>'+$(this).val();
    });
});
 $('.webiew_frame').height($(window).height());
</script>

<?php if(null != $teamId): ?>
    <script>
        window.UISettings = <?php echo json_encode($UISettings) ?>;
        window.lang = '<?php echo $lang ?>';
        window.actualSeason = <?php echo json_encode($actualSeason) ?>;
        window.teamId = <?php echo $team->getId() ?>;
        window.teamPlayers = <?php echo json_encode($teamPlayers) ?>;
        window.teamMates = <?php echo json_encode($teamMates) ?>;
        window.personalForm = <?php echo json_encode($personalForm) ?>;
        window.seasons = <?php echo json_encode($seasons) ?>;

        window.seasonStats = <?php echo json_encode($seasonStats) ?>;
        window.individualStats = <?php echo json_encode($individualStats) ?>;
        window.packageProducts = <?php echo $packageProducts; ?>;


        $('.change-sesason-trigger').on('click',function(){
            location.reload();
        });
    </script> 
    <script src="/js/Lib/jquery.flot.js"></script> 
    <script src="/js/Lib/jquery.flot.pie.js"></script> 
    <script src="/js/Lib/jquery.flot.stack.js"></script> 
    <script src="/js/Lib/jquery.flot.tooltip.min.js"></script>
    <script src="/js/Lib/JUMFlot.min.js"></script>


    <script src="/js/plugins/daterangepicker/moment.min.js"></script> 
    <script src="/js/plugins/daterangepicker/daterangepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="/js/plugins/daterangepicker/daterangepicker-bs3.css"></link>

<?php endif; ?>


<?php  $layout->endSlot('javascript') ?>
   



