
    
<?php if(null != $teamPlayerId): ?>
<div class="container" style="background: white;">
 <div class="row top-panel">
        <div class="col-xs-3 btn-back">
        </div>
        <div class="col-xs-6 text-center"><br />
            <?php
            $layout->includePart(MODUL_DIR . '/Api/view/Content/_teamNativeSelect.php', array(
                'user' => $user,
                'selectedTeamId' => $teamId,
                'link' => $router->link('api_content_stats_personal', array('apikey' => $apikey, 'user_id' => $userId))
            ))
            ?>
        </div>
        <div class="col-xs-3 text-right top-toolbar">
        </div>
    </div>
           
                   <table class="table table-bordered resp_stat_table" id="stat_table">
                        <thead>
                            <tr>
                                <th style="min-width:100px"></th>
                                <th  style="min-width:20px"></th>
                                <th>G</th>
                                <th>A</th>
                               
                            </tr>
                        </thead>
                        <tbody>
                    <?php foreach ($matchOverview as $match): ?>
                   <tr class="<?php echo $resultClass[$match->getResultStatus()] ?>">
                       <td data-order="<?php echo $match->getEventDateFormated('Y-m-d') ?>">
                           <?php if('draw' == $match->getResultStatus()): ?>
                                 <span class="label label-info"><?php echo $translator->translate('Draw') ?></span>
                                <?php endif; ?>
                                <?php if('win' == $match->getResultStatus()): ?>
                                                    <span class="label label-success"><?php echo $translator->translate('Win') ?></span>
                                <?php endif; ?>
                                <?php if('loose' == $match->getResultStatus()): ?>
                                 <span class="label label-danger"><?php echo $translator->translate('Loose') ?></span>
                                <?php endif; ?>
                                 
                                   <a class="mpopup" data-action="popup-full" href="<?php echo WEB_DOMAIN.$router->generateApiWidgetUrl('api_content_webview',
                                        array(
                                            'apikey' => CR_API_KEY,
                                            'view' => $router->link('api_content_event_detail',
                                                    array(
                                                        'apikey' => $apikey,
                                                        'user_id' => $userId,
                                                        'id' => $match->getEventId(),
                                                        'current_date' => $match->getEventDateFormated('Y-m-d')
                                                    )),
                                            'user_id' => $userId)
                                        ) ?>">
                                       <?php echo   $match->getEventDateFormated() ?><br />
                                

                                   <?php echo  $match->getFirstLineName() ?>:<?php echo  $match->getSecondLineName() ?>
                                   </a>
                       </td>

                           
                            <td><?php echo  $match->getResult() ?></td>
                            <td><?php echo $match->getGoals() ?></td>
                            <td><?php echo $match->getAssists() ?></td>
                        </tr>
                        
                       

                    <?php endforeach; ?>
                        </tbody>
                    </table>
                       
    
</div>
  
      

<?php else: ?>
<div class="rufus-alert rufus-alert-missing-team rufus-alert-stats-team">
    <img class="rufus-alert-buble" src="/img/error/back.png">
    <div class="rufus-alert-text">
        <h1><?php echo $translator->translate('api.alert.stats.choiceTeam.title') ?></h1>
        <strong><?php echo $translator->translate('api.alert.stats.choiceTeam.text') ?></strong>
        
        
          <?php
            $layout->includePart(MODUL_DIR . '/Api/view/Content/_teamNativeSelect.php', array(
                'user' => $user,
                'selectedTeamId' => $teamId,
                'link' => $router->link('api_content_stats_personal', array('apikey' => $apikey, 'user_id' => $userId))
            ))
            ?>
        
        
        
       
       
    </div>
    <img class="rufus-alert-body" src="/img/rufus/rufus_ukazuje@2x.png" >
</div>
    
<?php endif; ?>
    
   



 <?php $layout->includePart(MODUL_DIR . '/Player/view/profil/_match_overview_modal.php') ?>

<?php $layout->addStylesheet('plugins/datatables/dataTables.bootstrap.css') ?>
<?php $layout->addJavascript('plugins/datatables/jquery.dataTables.min.js') ?>
<?php $layout->addJavascript('plugins/datatables/dataTables.bootstrap.min.js') ?>
<?php  $layout->startSlot('javascript') ?>

<script type="text/javascript">
    
      var messageOrigin;
        var messageSource;
        document.addEventListener('message', function (e) {
          messageOrigin = e.origin;
          messageSource = e.source;

        });
         $('.mpopup').on('click',function(e){
            e.preventDefault();
            var message = {
                'href':$(this).attr('href'),
                'action': $(this).attr('data-action'),
                'screen': $(this).attr('data-screen'),
                'onclose-action': $(this).attr('data-popup-onclose-action')
            };
            
             window.postMessage(JSON.stringify(message) ,messageOrigin);
        });
    
    
    
$(document).ready(function() {
    $('#team_select_native').on('change',function(){
        location.href='<?php echo  WEB_DOMAIN.$router->link('api_content_stats_personal', array('apikey' => $apikey, 'user_id' => $userId)).'&team_id=' ?>'+$(this).val();
    });
});
  $("#stat_table").DataTable({
             "paging": false,
             "order": [[ 0, "desc" ]]
        });

 
   $('.view_match_overview').on('click',function(e){
            e.preventDefault();
            $('#match-overview-frame').attr('src',$(this).attr('href'));
            $('#match-overview-modal').modal('show');
        });
</script>
<?php  $layout->endSlot('javascript') ?>
   







