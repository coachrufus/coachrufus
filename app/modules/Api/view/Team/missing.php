
<div class="rufus-alert rufus-alert-missing-team">
    <img class="rufus-alert-buble" src="/img/error/back.png">
    <div class="rufus-alert-text">
        <h1><?php echo $translator->translate('api.alert.missingTeam.title') ?></h1>
        <strong><?php echo $translator->translate('api.alert.missingTeam.text') ?></strong>
        <a class="cta-btn-missing-team mpopup" data-action="popup-full" data-popup-onclose-action="refresh" href="<?php echo WEB_DOMAIN.$router->generateApiWidgetUrl('api_content_webview',array('apikey' => CR_API_KEY,'view' => $router->link('team_create'),'user_id' => $userId)) ?>"><?php echo $translator->translate('api.alert.missingTeam.cta') ?></a>
       
    </div>
    <img class="rufus-alert-body" src="/img/rufus/rufus_ukazuje@2x.png" >
</div>



<?php $layout->startSlot('javascript') ?>

<script type="text/javascript">
    
     var messageOrigin;
    var messageSource;
    document.addEventListener('message', function (e) {
      messageOrigin = e.origin;
      messageSource = e.source;

    });
    
    
     $('.mpopup').on('click',function(e){
        e.preventDefault();
        var message = {
            'href':$(this).attr('href'),
            'action': $(this).attr('data-action'),
            'screen': $(this).attr('data-screen'),
            'onclose-action': $(this).attr('data-popup-onclose-action')
        };

         window.postMessage(JSON.stringify(message) ,messageOrigin);
    });
   
</script>
<?php $layout->endSlot('javascript') ?>
