#cr-slider-widget-wrap {
    width:100%;
    box-sizing: border-box;
    overflow:hidden;
}

#cr-slider-widget-wrap *{
    box-sizing: border-box;
}

#cr-slider-widget-wrap p {
    margin:0;
}

#cr-slider-widget-wrap .sr-footer {
    margin:5px 0;
    overflow:hidden;
}


#cr-slider-widget-wrap .cr-slider-section {
    display:none;
}
#cr-slider-widget-wrap .cr-slider-section:nth-child(1) {
    display:block;
     overflow:hidden;
}

#cr-slider-widget-wrap .scouting-item {
        float: left;
    width:25%;
    margin:0;
    text-align:center;
    position:relative;
}

#cr-slider-widget-wrap .sr-footer a{
display:inline-block;
width:50%;
color:#10B4BF;
text-decoration:none;
float:left;
text-align:center;
}





#cr-slider-widget-wrap .ico {
  display: inline-block;
  text-indent: -9999px;
  font-size: 0;
  line-height: 0;
  position: relative;
  top: 0;
  vertical-align: top;
  width:18px;
  height:18px;
  background-size: cover;
  background-position:center center;
  background-repeat: no-repeat;
}

#cr-slider-widget-wrap .ico-reply{background-image:url('<?php echo $imgPath ?>/theme/img/icon/reply.svg'); width:20px; height:20px;}
#cr-slider-widget-wrap .ico-share {background-image:url('<?php echo $imgPath ?>/theme/img/icon/share.svg');}


.bx-wrapper .bx-viewport {

box-shadow:none
}

@media only screen and (max-width: 768px) {
    #cr-slider-widget-wrap .scouting-item {
        width:100%;
        padding:10px 0;
    }
}

