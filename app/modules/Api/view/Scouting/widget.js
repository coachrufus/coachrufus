(function() {

var jQuery;

if (window.jQuery === undefined) {
    var script_tag = document.createElement('script');
    script_tag.setAttribute("type","text/javascript");
    script_tag.setAttribute("src",
        "https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js");
    if (script_tag.readyState) {
      script_tag.onreadystatechange = function () { // For old versions of IE
          if (this.readyState == 'complete' || this.readyState == 'loaded') {
              
              
              
              scriptLoadHandler();
          }
      };
    } else { // Other browsers
      script_tag.onload = scriptLoadHandler;
    }
    // Try to find the head, otherwise default to the documentElement
    (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(script_tag);
} else {
    // The jQuery version on the window is the one we want to use
    jQuery = window.jQuery;
    scriptLoadHandler();
    //main();
}



function scriptLoadHandler() {
    // Restore $ and window.jQuery to their previous values and store the
    // new jQuery in our local jQuery variable
    jQuery = window.jQuery;
    //jQuery = window.jQuery.noConflict(true);

    var bx_slider_css =     document.createElement('link');
    bx_slider_css.setAttribute("rel","stylesheet");    
    bx_slider_css.setAttribute("href","https://app.coachrufus.com/dev/plugins/bxslider/jquery.bxslider.css");    
     
     
    var bx_slider_js =     document.createElement('script');
    bx_slider_js.setAttribute("type","text/javascript");
    bx_slider_js.setAttribute("src","https://app.coachrufus.com/dev/plugins/bxslider/jquery.bxslider.min.js");
   
   (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(bx_slider_css);
   (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(bx_slider_js);
    
     if (bx_slider_js.readyState) {
      bx_slider_js.onreadystatechange = function () { // For old versions of IE
          if (this.readyState == 'complete' || this.readyState == 'loaded') {
              
              
              
              main();
          }
      };
    } else { // Other browsers
      
        bx_slider_js.onload = main;
    }
    
    //main(); 
}



function main() { 
    jQuery(document).ready(function($) { 
        
       
        var widget = new ScoutingSliderWidget();
        widget.loadData();
        
        
    });
}



var ScoutingSliderWidget = function ()
{
    this.widgetContainer = jQuery('#cr_slider_widget');
    this.dataUrl = 'https://app.coachrufus.com/api/scouting/widget/slider-data';
    this.timer;
    this.currentSlideIndex = 1;
};


ScoutingSliderWidget.prototype.loadData= function ()
{
    manager = this;
    url = '';
    jQuery.ajax({
        method: "GET",
        url: manager.dataUrl
    }).done(function (response) {
        manager.widgetContainer.html(response.html);
        css = '<style>'+response.css+'</style>';
        jQuery('head').append(css);
        
      
         jQuery('#cr-slider-widget-wrap').bxSlider({
            minSlides:1 ,
            maxSlides: 5,
            slideWidth: 320,
            slideMargin: 0,
            auto: true,
            pager: false
          });
        manager.addShareScouting();
        manager.bindReply();
        this.timer = setInterval(function(){ manager.slideItems() }, 10000);
        
    });
};



ScoutingSliderWidget.prototype.slideItems= function ()
{
    jQuery('#cr-slider-widget-wrap .cr-slider-section:nth-child('+this.currentSlideIndex+')').hide();
     this.currentSlideIndex++;
     
     if( this.currentSlideIndex > 4)
     {
         this.currentSlideIndex  = 1;
     }
     jQuery('#cr-slider-widget-wrap .cr-slider-section:nth-child('+this.currentSlideIndex+')').fadeIn(1000);
};

ScoutingSliderWidget.prototype.bindReply = function ()
{
   jQuery('.scouting-reply').on('click',function(e){
       e.preventDefault();
       window.open(jQuery(this).attr('href'),'_blank');
   });
};


ScoutingSliderWidget.prototype.addShareScouting = function(){
   
     jQuery('.share-scouting').on('click',function(e){
        e.preventDefault();
       window.open(jQuery(this).attr('href'),'_blank');
     });
    
   
    /*
    window.fbAsyncInit = function () {
        FB.init({
            appId: '944943362242789',
            xfbml: true,
            version: 'v2.4'
        });
    };

    (function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {
        return;
    }
    js = d.createElement(s);
    js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    
    jQuery('.share-scouting').on('click',function(e){
        e.preventDefault();
        var link = jQuery(this).attr('href');
         FB.ui({
             method: 'share',
             display: 'popup',
             href: link,
             hashtag: '#coachrufus'
           }, function(response){});
     });
     */
};






})(); 
