<div id="cr-slider-widget-wrap">
    <?php foreach ($sections as $section): ?>

            <?php foreach ($section as $item): ?>
                <div class="scouting-item">
                    <strong><?php echo $item->getPerex() ?></strong>
                    <p><?php echo $item->getContent() ?></p>
                    
                        <div class="sr-footer">
               
                <a class="scouting-reply" href="<?php echo WEB_DOMAIN ?><?php echo $router->link('scouting_detail',array('id' => $item->getId())) ?>"><i class="ico ico-reply"></i> <?php echo $translator->translate('Reply') ?></a>
            
                <a title="<?php echo $translator->translate('Share on facebook') ?>" class="share-scouting" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo WEB_DOMAIN ?><?php echo $router->link('scouting_detail',array('id' => $item->getId())) ?>"><i class="ico ico-share"></i> <?php echo $translator->translate('Share') ?></a>
            </div>
                    
                    
                </div>
            <?php endforeach; ?>

    <?php endforeach; ?>
</div>

<script type="text/javascript">



</script>