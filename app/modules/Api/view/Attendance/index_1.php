
<div class="container">
    <div class="row top-panel">
        <div class="col-xs-3 btn-back">
        </div>
        <div class="col-xs-6 text-center">
            <?php
            $layout->includePart(MODUL_DIR . '/Api/view/Content/_teamSelect.php', array(
                'user' => $user,
                'selectedTeamId' => $teamId,
                'link' => $router->link('api_content_attendance', array('apikey' => $apikey, 'user_id' => $userId))
            ))
            ?>
        </div>
        <div class="col-xs-3 text-right top-toolbar">
        </div>
    </div>
    
    <div class="row stats-wrap" style="height: 100%;">
        <div class="col-sm-12" style="height: 100%;">
            <?php if(null != $teamId): ?>
               <iframe style="width:100%;height:100%;border:0; min-height: 640px;" src="<?php echo WEB_DOMAIN ?>/team/players-attendance/<?php echo $teamId ?>?layout=api"></iframe>
            <?php else: ?>
                <div class="alert alert-info text-center"><?php echo $translator->translate('api.games.stats.choiceTeamAlert') ?></div>
            <?php endif; ?>
        </div>
    </div>
</div>










