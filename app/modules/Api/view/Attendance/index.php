<?php if(null != $teamId): ?>
<div class="container">
    <div class="row top-panel">
        <div class="col-xs-3 btn-back">
        </div>
        <div class="col-xs-6 text-center">
            <br />
            <?php
            $layout->includePart(MODUL_DIR . '/Api/view/Content/_teamNativeSelect.php', array(
                'user' => $user,
                'selectedTeamId' => $teamId,
                'link' => $router->link('api_content_attendance', array('apikey' => $apikey, 'user_id' => $userId))
            ))
            ?>
        </div>
        <div class="col-xs-3 text-right top-toolbar">
        </div>
    </div>
    
    <div class="row stats-wrap attendance-frame" style="height: 100%;">
        <div class="col-sm-12">
            <?php if(null != $teamId): ?>
                 <form action="<?php echo $router->link('api_content_attendance') ?>" class="form-inline">
                     <input type="hidden" name="apikey" value="<?php echo $apikey ?>" />
                     <input type="hidden" name="user_id" value="<?php echo $userId ?>" />
                     <input type="hidden" name="team_id" value="<?php echo $teamId ?>" />
                    <?php echo $filterForm->renderSelectTag('event_type', array('class' => 'form-control')) ?>
                    <?php echo $translator->translate('From') ?>:
                    <?php echo $filterForm->renderInputTag('from', array('class' => 'form-control')) ?>
                    <?php echo $translator->translate('To') ?>: <?php echo $filterForm->renderInputTag('to', array('class' => 'form-control')) ?>
                    <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-refresh"></i> <?php echo $translator->translate('Filter') ?></button>
                </form>
        
                 <div id="resp_attendance">
<?php foreach ($eventsList as $dayIndex => $dayEvents): ?>
    <?php foreach ($dayEvents as $event): ?>
                        <div class="resp_attendance_item">
                            <h3><?php echo $event->getFormatedCurrentDate() ?><br /> <?php echo $event->getPlaygroundName() ?></h3>
                            <div class="members-attendance" id="members-attendance-<?php echo $event->getUid() ?>">
                                <span><?php echo $translator->translate('Attendance') ?>:</span> <span class="members-attendance-in"><?php echo $event->getAttendanceInSum() ?></span> /<?php echo $event->getCapacity() ?>
                                <div class="progress progress-xs">
                                    <div class="progress-bar progress-bar-success progress-bar-green" role="progressbar" aria-valuenow="<?php echo $event->getAttendanceInSum() ?>" aria-valuemin="0" aria-valuemax="<?php echo $event->getCapacity() ?>" style="width:  <?php echo round($event->getAttendanceInSum() / $event->getCapacity() * 100) ?>%">

                                    </div>
                                </div>
                            </div>

                            <table>

        <?php foreach ($attendanceMatrix['eventMatrix'][$dayIndex . '-' . $event->getId()]['players'] as $cell): ?>
                                    <tr>
                                        <td><?php echo $cell['player'] ?></td>
                                        <td>
            <?php if ($cell['editable'] == true or \Core\ServiceLayer::getService('security')->getIdentity()->hasTeamPermission('manageEvents', $team)): ?>



                                                <a href="#"
                                                   data-url="<?php echo $router->link('change_player_attendance') ?>" 
                                                   data-rel="in" 
                                                   data-event="<?php echo $event->getId() ?>" 
                                                   data-eventdate="<?php echo $event->getCurrentDate()->format('Y-m-d') ?>" 
                                                   data-player="<?php echo $cell['player_id '] ?>" 
                                                   data-team-role="<?php echo $cell['team_role'] ?>"
                                                   data-team-player="<?php echo $cell['team_player_id'] ?>" 
                                                   class="in attendance_trigger attendance_trigger_btn <?php echo ($cell['status'] == 1 ) ? 'accept' : '' ?>">
                                                    <i class="ico ico-check"></i>
                                                </a>

                                                <a href="#"  data-url="<?php echo $router->link('change_player_attendance') ?>" 
                                                   data-rel="out" 
                                                   data-event="<?php echo $event->getId() ?>" 
                                                   data-eventdate="<?php echo $event->getCurrentDate()->format('Y-m-d') ?>" 
                                                   data-player="<?php echo $cell['player_id '] ?>" 
                                                   data-team-role="<?php echo $cell['team_role'] ?>"
                                                   data-team-player="<?php echo $cell['team_player_id'] ?>" 
                                                   class="out attendance_trigger attendance_trigger_btn    <?php echo ($cell['status'] == 3 ) ? 'deny' : '' ?>">
                                                    <i class="ico ico-close"></i>
                                                </a>

                                            <?php else: ?>
                                                <?php if ($cell['status'] == 3): ?>
                                                    <span class="attendance_trigger_btn deny"><i class="ico ico-close"></i></span>
                                                <?php elseif ($cell['status'] == 1): ?>
                                                    <span class="attendance_trigger_btn accept "> <i class="ico ico-check"></i></span>
                                                    <?php else: ?>
                                                    <span class="attendance_trigger_btn"><i class="ico ico-question"></i></span>
                <?php endif; ?>
            <?php endif; ?>

                                        </td>
                                    </tr>


                        <?php endforeach; ?>
                            </table>
                        </div>
    <?php endforeach; ?>
<?php endforeach; ?>

            </div>
        
        
        
        
             
            <?php else: ?>
                <div class="alert alert-info text-center"><?php echo $translator->translate('api.games.stats.choiceTeamAlert') ?></div>
            <?php endif; ?>

    </div>
</div>
</div>
<?php else: ?>
    
    
<div class="rufus-alert rufus-alert-missing-team rufus-alert-stats-team">
    <img class="rufus-alert-buble" src="/img/error/back.png">
    <div class="rufus-alert-text">
        <h1><?php echo $translator->translate('api.alert.attendance.choiceTeam.title') ?></h1>
        <strong><?php echo $translator->translate('api.alert.attendance.choiceTeam.text') ?></strong>
        
        
          <?php
            $layout->includePart(MODUL_DIR . '/Api/view/Content/_teamNativeSelect.php', array(
                'user' => $user,
                'selectedTeamId' => $teamId,
                'link' => $router->link('api_content_attendance', array('apikey' => $apikey, 'user_id' => $userId))
            ))
            ?>
        
       
       
    </div>
    <img class="rufus-alert-body" src="/img/rufus/rufus_ukazuje@2x.png" >
</div>
    
<?php endif; ?>

<?php $layout->addJavascript('js/TeamEventManager.js') ?>
<?php $layout->addStylesheet('plugins/pickadate/themes/classic.css') ?>
<?php $layout->addStylesheet('plugins/pickadate/themes/classic.date.css') ?>
<?php $layout->addJavascript('plugins/pickadate/picker.js') ?>
<?php $layout->addJavascript('plugins/pickadate/picker.date.js') ?>
<?php $layout->addJavascript('plugins/pickadate/translations/' . LANG . '.js') ?>

<?php  $layout->startSlot('javascript') ?>
<script type="text/javascript">
$(document).ready(function() {
    $('#team_select_native').on('change',function(){
        location.href='<?php echo  WEB_DOMAIN.$router->link('api_content_attendance', array('apikey' => $apikey, 'user_id' => $userId)).'&team_id=' ?>'+$(this).val();
    });
});

 $('#filter_from').pickadate({'format': 'dd.mm.yyyy', 'formatSubmit': 'dd.mm.yyyy'});
    $('#filter_to').pickadate({'format': 'dd.mm.yyyy', 'formatSubmit': 'dd.mm.yyyy'});

    event_manger = new TeamEventsManager();
    event_manger.init();


</script>
<?php  $layout->endSlot('javascript') ?>
    
   


















