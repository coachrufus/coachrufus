
<div class="rufus-alert rufus-alert-in-progress">
    <img class="rufus-alert-buble" src="/img/error/back.png">
    <div class="rufus-alert-text">
        <h1><?php echo $translator->translate('api.alert.payments.title') ?></h1>
        <strong><?php echo $translator->translate('api.alert.payments.strong') ?></strong>
        <p><?php echo $translator->translate('api.alert.payments.text') ?></p>
    </div>
    <img class="rufus-alert-body" src="/img/rufus/rufus-in-progress@2x.png" >
</div>
