<ul>
    <li><a href="/api/test/user/login">USER LOGIN</a></li>
    <li><a href="/api/test/user/login-fb">USER LOGIN FB</a></li>
    <li><a href="/api/test/user/login-google">USER LOGIN G</a></li>
    <li><a href="/api/test/user/register">USER REGISTER</a></li>
    <li><a href="/api/test/user/register-fb">USER REGISTER FB </a></li>
    <li><a href="/api/test/user/register-google">USER REGISTER G </a></li>
    <li><a href="/api/test/team/user-list">TEAM USER LIST</a></li>
    <li><a href="/api/test/team/season-list">TEAM SEASON LIST</a></li>
    <li><a href="/api/test/system-notify/unread">SYSTEM NOTIFY UNREAD</a></li>
    <li><a href="/api/test/system-notify/change-status">SYSTEM NOTIFY CHNAGE STATUS</a></li>
</ul>
CONTENT
<ul>
    <li><a href="/api/test/content/float-menu">FLOAT MENU</a></li>
    <li><a href="/api/test/content/home">HOME</a></li>
    <li><a href="/api/test/content/main-menu">MAIN MENU</a></li>
    <li><a href="/api/test/content/notify-setup">NOTIFY SETUP</a></li>
    <li><a href="/api/test/content/events/current">EVENTS</a></li>
    <li><a href="/api/test/content/events/calendar">CALENDAR</a></li>
    <li><a href="/api/test/content/attendance">ATTENDANCE</a></li>
    <li><a href="/api/test/content/event-attendance">EVENT ATTENDANCE</a></li>
    <li><a href="/api/test/event/detail">EVENT DETAIL</a></li>
    <li><a href="/api/test/content/event/detail">EVENT DETAIL</a></li>
    <li><a href="/api/test/event/team-choice">EVENT CHOICE</a></li>
   
</ul>
GAMES
<ul>
    <li><a href="/api/test/content/games/score">SCORE</a></li>
    <li><a href="/api/test/content/games/results">RESULTS</a></li>
    <li><a href="/api/content/games/stats-data">STATS DATA</a></li>
    <li><a href="/api/test/content/games/lineup/widget">LINEUP WIDGET</a></li>
    <li><a href="/api/test/content/games/score/widget">SCORE WIDGET</a></li>
    <li><a href="/api/test/content/games/simple-score/widget">SIMPLE SCORE WIDGET</a></li>
    <li><a href="/api/test/content/game/lineup">LINEUP DATA</a></li>
    <li><a href="/api/test/content/game/create-lineup">CREATE LINEUP</a></li>
    <li><a href="/api/test/content/game/timeline">TIMELINE</a></li>
    <li><a href="/api/test/content/game/simple-score">SIMPLE SCORE</a></li>
</ul>
STATS
<ul>
    <li><a href="/api/test/content/stats/personal">PERSONAL</a></li>
    <li><a href="/api/test/content/stats/tables">TABLES</a></li>
    <li><a href="/api/test/content/stats/graphs">GRAPHS</a></li>
   
</ul>