<?php 
$teamManager = \Core\ServiceLayer::getService('TeamManager');
$activeTeam = Core\ServiceLayer::getService('ActiveTeamManager')->getActiveTeam();
$team = $teamManager->findTeamById($activeTeam['id']);
$isPromoTeam = $teamManager->isPromoTeam($team,'GROUP');


?>
<div class="modal fade progress-modal" id="skip_modal" tabindex="-1" role="dialog" aria-labelledby="<?php echo $translator->translate('S2S_SKIP_TITLE') ?>">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="ico ico-close-black"></i></span></button>
                <h4 class="modal-title" id="myModalLabel"><?php echo $translator->translate('S2S_SKIP_TITLE') ?></span></h4>
            </div>

            <div class="modal-body">

                <p>
                <?php echo $translator->translate('S2S_SKIP_TEXT') ?>
                </p>
                <a class="btn btn-primary s2s_skip_cancel" href="#"><?php echo $translator->translate('S2S_SKIP_BTN') ?></a>


                
                <?php if($isPromoTeam): ?>
                  <div class="row">
                    <div class="skip-step-block">   
                        <div class="col-xs-12">
                            <?php echo $translator->translate('S2S_SKIP_BUY_TEXT') ?> <a class="skip_and_free_trigger" href="<?php echo $router->link('skip_step_by_step',array('team_id' => $activeTeam['id'],'tt'=>'group')) ?>">PRO</a>
                        </div>
                        
                        <div class="skip-step-block-divide col-xs-12"></div>
                        
                        <div class="col-xs-12">
                            <?php echo $translator->translate('S2S_SKIP_GET_FREE') ?> <a class="skip_and_free_trigger" href="<?php echo $router->link('skip_step_by_step',array('team_id' => $activeTeam['id'],'tt'=>'group')) ?>">Free</a>
                        </div>
                    </div>
                </div>
                <?php else: ?>
                <div class="row">
                    <div class="skip-step-block">   
                        
                        <div class="col-xs-12">
                            <a class="skip_and_free_trigger" href="<?php echo $router->link('skip_step_by_step',array('team_id' => $activeTeam['id'])) ?>"> <?php echo $translator->translate('S2S_SKIP_GET_FREE') ?></a>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
                



            </div>

            <div class="modal-footer">

            </div>
        </div>
    </div>
</div>
