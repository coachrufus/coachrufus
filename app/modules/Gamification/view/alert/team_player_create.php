<div clas="splash_divider"></div>
<p>Zadaj ich mená a e-maily, na ktoré im príde pozvánka do tímu. Neskôr budú môcť dostávať dôležité <strong>tímové notifikácie.</strong>
    Členovia tímu môžu mať <strong>rôzne role</strong> (napr. pri prihlasovaní na zápas má hráč prednosť pred náhradníkom).
</p>