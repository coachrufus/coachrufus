<div clas="splash_divider"></div>
<p>Enter their names and emails so they can receive the invitation to the team.  Later on, they will receive important <strong>team email notifications.</strong>
    The team members can have <strong>different roles</strong> (e.g. when signing up for the match, the player is more important than the substitute).
</p>