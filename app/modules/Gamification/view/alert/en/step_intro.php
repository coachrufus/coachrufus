
<div clas="splash_divider"></div>


<img src="/theme/img/icon/s2s/Steps_icon.svg" />
<p>In order to get familiar with the application, I have prepared a Wizzard for you that will show you in 9 steps how it works.</p>
<div class="pro-sbs-intro">
    <i class="pro-icon">PRO</i>
    <?php echo $freeTime ?> <?php echo $translator->translate('INTRO_DAY_FOR_FREE') ?>
</div>
<p>After their completion I will run PRO version for FREE_DAYS days free of charge so you have time to test it thoroughly.</p>
<img src="/theme/img/icon/s2s/Team_add_icon.svg" />
<p>If you have already set your team in the application and you don´t want to run the Wizzard, you can skip it in any step.</p>

<a class="btn btn-primary s2s_intro_continue" href="#"><?php echo $translator->translate('S2S_CONTINUE') ?></a>