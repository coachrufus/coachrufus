<div class="modal fade progress-modal ready" id="event_attendance_confirm" tabindex="-1" role="dialog" aria-labelledby="">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
           
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="ico ico-close-black"></i></span></button>
                <h4 class="modal-title" id="myModalLabel"><span>Confirmed</span></h4>
            </div>

            <div class="modal-body">
Perfect, now you can continue! 

                
            </div>

           <div class="modal-footer">
                 <div class="row">
                     <div class="col-sm-12 text-center">
                           <a class="btn btn-primary s2s_attendance_confirm_link"  href="#"><?php echo $translator->translate('S2S_CONTINUE_TO_ROSTER') ?></a>
                     </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var achievement_manager = new AchievementManager();
    achievement_manager.showAlert('<?php echo $code ?>');
</script>