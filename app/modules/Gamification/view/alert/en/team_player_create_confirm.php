<div clas="splash_divider"></div>
<p>Awesome! You added players to your team, now you can create new event.</p>


<?php $layout->startSlot('sbs_modal_actions') ?>

 <div class="row">
    
    <div class="col-xs-4 text-right">
        <a data-dismiss="modal" class="s2s_text_link" href="#"><?php echo $translator->translate('SBS_MODAL_ADD_PLAYERS') ?></a>
    </div>
     
     <div class="col-xs-4 ">
            <a class="btn btn-primary" href="<?php echo $router->link('create_team_event',array('team_id' => $teamId)) ?>" ><?php echo $translator->translate('S2S_CONTINUE') ?></a>
    </div>

    <div class="col-xs-4 text-left">
        <a class="s2s_skip_trigger" data-skip-id="<?php echo $code ?>" href=""><?php echo $translator->translate('S2S_SKIP') ?></a>
    </div>
</div>


 <?php $layout->endSlot('sbs_modal_actions') ?>