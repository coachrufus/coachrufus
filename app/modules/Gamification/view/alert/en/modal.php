<div class="modal fade progress-modal" id="<?php echo $code ?>" tabindex="-1" role="dialog" aria-labelledby="<?php echo $title?>">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="ico ico-close-black"></i></span></button>
                <h4 class="modal-title" id="myModalLabel"><span><?php echo $title ?></span></h4>
            </div>

            <div class="modal-body">

              <?php echo $text ?>
                
                
                 <?php if (null != $teamId && $isFloorballChallengeTeam == false): ?>
                <?php if($layout->slotExist('sbs_modal_actions')): ?>
                    <?php echo $layout->insertSlot('sbs_modal_actions') ?>
                <?php else: ?>
                      
                     <div class="row">
                        <div class="col-xs-6 col-sm-offset-4 col-sm-4">
                            <?php if ('#' == $s2sContinueLink): ?>
                                <a class="btn btn-primary" href="#" data-dismiss="modal" ><?php echo $translator->translate('S2S_CONTINUE') ?></a>
                            <?php else: ?>
                                <a class="btn btn-primary s2s_modal_continue" href="<?php echo $s2sContinueLink ?>"><?php echo $translator->translate('S2S_CONTINUE') ?></a>
                            <?php endif; ?>
                        </div>

                        <div class="col-xs-6 col-sm-4 text-left">
                            <a class="s2s_skip_trigger" data-skip-id="<?php echo $code ?>" href=""><?php echo $translator->translate('S2S_SKIP') ?></a>
                        </div>
                    </div>
                    <?php endif; ?>
                <?php endif; ?>
                
                

             
            </div>

            <div class="modal-footer">
                
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var achievement_manager = new AchievementManager();
    achievement_manager.showAlert('<?php echo $code ?>');
</script>