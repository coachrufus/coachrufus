<?php

namespace CR\Gamifications\Controller;

use Core\Controller as Controller;
use Core\ControllerResponse;
use Core\ServiceLayer;
use Core\GUID;

class ProgressController extends Controller {

    public function progressBarAction()
    {
       
         $response = new ControllerResponse();
        $response->setStatus('success');
        $response->setContent('');
        $response->setType('text/html');
        return $response;
        
        $identity = ServiceLayer::getService('security')->getIdentity();
        $user = $identity->getUser();
        $progressChainManager = ServiceLayer::getService('ProgressChainManager');
        $chain = $progressChainManager->getProgressStepsChain(ServiceLayer::getService('security')->getIdentity());
        $allowedPlaces = $progressChainManager->getAllowedPlaces();
        $currentRoute = $this->getRouter()->getCurrentRouteName();
       
        //$finishedProgressStepFinal = $stepsManager->isStepFinished('step3_final');
        //$AchievementAlertManager = ServiceLayer::getService('AchievementAlertManager');
       //$finishedProgressStepFinal = $AchievementAlertManager->userHasStepsFinished('step3_final',$user);
        $team =  \Core\ServiceLayer::getService('ActiveTeamManager')->getActiveTeam();
        //check if has pro or finished step 3
        
        $stepsManager = \Core\ServiceLayer::getService('StepsManager');
        if($stepsManager->showStepByStep() == false)
        {
            $response = new ControllerResponse();
            $response->setStatus('success');
            $response->setContent('');
            $response->setType('text/html');
            return $response;
        }
          //only for admin

        /*
        $actualTeamId = $identity->getUserCurrentTeamId();
        $userTeamRoles = $identity->getTeamRoles();
        $teamRole = $userTeamRoles[$actualTeamId];
       
        if($teamRole['role'] != 'ADMIN' && $actualTeamId != null)
        {
            $response = new ControllerResponse();
            $response->setStatus('success');
            $response->setContent('');
            $response->setType('text/html');
            return $response;
        }
        */

        //$activeTeam =  \Core\ServiceLayer::getService('ActiveTeamManager')->getActiveTeam();

        //$userIdentity = \Core\ServiceLayer::getService('security')->getIdentity();
        //$userTeamRoles = $userIdentity->getTeamRoles();

        //$teamRole = $userTeamRoles[$activeTeam['id']];
       
    
        
        if(in_array($currentRoute, $allowedPlaces) )
        {
            //$userPackageRepository =  \Core\ServiceLayer::getService('UserPackageRepository');
            //$existUserPro = $userPackageRepository->userHadProPackage($user->getId());
 return $this->render('CR\Gamifications\GamificationModule:progress:progress_bar.php', array(
                        'chain' => $chain,
                        'team' => $team
                ));
 
           
        
        
      
        }
        
        $response = new ControllerResponse();
        $response->setStatus('success');
        $response->setContent('');
        $response->setType('text/html');
        return $response;
    }
    
    public function skipProgressAction()
    {
        $teamManager = ServiceLayer::getService('TeamManager');
        $identity = ServiceLayer::getService('security')->getIdentity();
        $user = $identity->getUser();
        
        $teamId = $this->getRequest()->get('team_id');
        $team = $teamManager->findTeamById($teamId);
        
        $type= $this->getRequest()->get('tt');
        if($type == 'group')
        {
            //create 90 days pro
            $end = new \DateTime();
            $start= new \DateTime();
            $start->modify('-1 second');
            $end->modify('+3 month');
            $pm = ServiceLayer::getService('PackageManager');
            $proPackage = $pm->getPackageWithProducts(5);
            ServiceLayer::getService('UserPackageManager')->addUserPackage([
                'user_id' => $user->getId(),
                'package_id' => 5,
                'team_id' => $team->getId(),
                'team_name' => $team->getName(),
                'date' => $start,
                'start_date' => $start,
                'end_date' => $end,
                'name' => 'PRO',
                'note' => 'TRIAL',
                'level' => 5,
                'products' => json_encode($proPackage['products']),
                'player_limit' => 100,
                'guid' => GUID::create(),
                 'variant' => 0,
                 'renew_payment' => 0
            ]);
        }
        
        
       $teamAdmins = $teamManager->getTeamAdminList($team);
       $teamAdminIds = array();
       foreach($teamAdmins as $teamAdmin)
       {
           $teamAdminIds[] = $teamAdmin->getPlayerId();
       }
       
       if(!in_array($user->getId(), $teamAdminIds))
       {
           throw new \AclException();
       }
        
        \Core\DbQuery::query("INSERT INTO team_achievement_alert_log (name, achievement_code, created_at, created_by, team_id, alert_status, place_code) VALUES ('A je to tu!', 'step3_final', '".date('Y-m-d H:i:s')."', ".$team->getAuthorId().",  ".$team->getId().", 'finished', 'event_detail_rating')");
        
        \Core\ServiceLayer::getService('TeamCreditManager')->refreshTeamPackages();
        

        if($type == 'group')
        {
            $this->getRequest()->redirect($this->getRouter()->link('team_overview',array('id' => $team->getId())));
        }
        
        
        $this->getRequest()->redirect();
    }

}
