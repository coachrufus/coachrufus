<?php
namespace CR\Gamifications;
require_once(MODUL_DIR.'/Gamification/GamificationModule.php');


use Core\ServiceLayer as ServiceLayer;


$acl = ServiceLayer::getService('acl');

ServiceLayer::addService('AlertLogRepository',array(
'class' => "CR\Gamification\Model\AlertLogRepository",
	'params' => array(
		new \Core\DbStorage('team_achievement_alert_log'),
		new \Core\EntityMapper('CR\Gamification\Model\AlertLog')
	)
));


ServiceLayer::addService('ProgressChainManager',array(
    'class' => 'CR\Gamification\Model\ProgressChainManager',
     'params' => array(
        ServiceLayer::getService('AlertLogRepository'),
    )
));

ServiceLayer::addService('AchievementAlertManager',array(
    'class' => 'CR\Gamification\Model\AchievementAlertManager',
    'params' => array(
        ServiceLayer::getService('router'),
        ServiceLayer::getService('AlertLogRepository'),
    )
));

ServiceLayer::addService('StepsManager',array(
    'class' => 'CR\Gamification\Model\StepsManager',
    'params' => array(
        ServiceLayer::getService('AchievementAlertManager'),
        ServiceLayer::getService('ProgressChainManager'),
    )
));



$acl->allowRole('ROLE_USER', 'skip_step_by_step');
$router->addRoute('skip_step_by_step', '/step-by-step/skip', [
    'controller' => 'CR\Gamifications\GamificationModule:Progress:skipProgress'
]);


