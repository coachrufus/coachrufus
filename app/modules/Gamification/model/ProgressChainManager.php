<?php

namespace CR\Gamification\Model;

class ProgressChainManager {

    private $steps;
    private $stepRepository;

    function __construct($stepRepository)
    {
        $this->steps = $this->buildStepsChain();
        $this->setStepRepository($stepRepository);
    }

    public function getSteps()
    {
        return $this->steps;
    }

    public function setSteps($steps)
    {
        $this->steps = $steps;
    }

    public function getStepRepository()
    {
        return $this->stepRepository;
    }

    public function setStepRepository($stepRepository)
    {
        $this->stepRepository = $stepRepository;
    }

    public function getNextStepKey($lastStepIndex, $steps)
    {
        $nextStepKey = key($steps);
        if (null != $lastStepIndex)
        {
            $keys = array_keys($steps);
            $currentKey = array_search($lastStepIndex, $keys);
            $nextStepKey = $keys[$currentKey + 1];
        }

        return $nextStepKey;
    }

    public function getProgressStepsChain($userIdentity)
    {
        $steps = $this->getSteps();

        $lastStepIndex = 1;

        //first check if all finished
        if ($this->userStepsFinished($userIdentity->getUser()->getId()))
        {
            foreach ($steps as $step)
            {
                $step->setStatus('finished');
            }
        }
        else
        {
            $finishedSteps = $this->getExistAlert($userIdentity->getUserCurrentTeamId());
            foreach ($finishedSteps as $finishedStep)
            {
                if (array_key_exists($finishedStep->getAchievementCode(), $steps))
                {
                   
                        $step = $steps[$finishedStep->getAchievementCode()];
                        $steps[$step->getCode()]->setStatus($finishedStep->getAlertStatus());
                        //$lastStepIndex = $finishedStep->getAchievementCode();
                        $lastStepIndex = $finishedStep->getAchievementCode();

                   
                }
            }
            $nextKey = $this->getNextStepKey($lastStepIndex, $steps);
            if (null != $nextKey)
            {
                $steps[$nextKey]->setStatus('next');
            }
        }


        return $steps;
    }

    public function buildStepsChain($stepsDefinitions = null)
    {
        if (null == $stepsDefinitions)
        {
            $stepsDefinitions = array(
                'team_create' => array(
                    'label' => 1,
                    'status' => 'waiting',
                    'text' => 'Vyplň pár dôležitých údajov, aby vám appka fungovala čo najlepšie.',
                    'code' => 'team_create',
                    'target_link' => 'team_create'),
                'team_player_create' => array(
                    'label' => 2,
                    'status' => 'locked',
                    'text' => 'Zadaj ich mená a e-maily, na ktoré im príde pozvánka do tímu. Neskôr budú môcť dostávať dôležité tímové notifikácie.
Členovia tímu môžu mať rôzne role (napr. pri prihlasovaní na zápas má hráč prednosť pred náhradníkom).',
                    'code' => 'team_player_create',
                    'target_link' => 'team_players_list'),
                'event_create' => array(
                    'label' => 3,
                    'status' => 'locked',
                    'text' => 'Udalosťou môže byť napr. každotýždňový zápas ale aj jednorázovka.
Pre poriadok udalosti zaraďujeme do sezón. Prvá sa vám vytvorí automaticky ako časovo neohraničená, neskôr ju môžeš upraviť, ak si budete chcieť porovnávať štatistiky z rôznych sezón. ',
                    'code' => 'event_create',
                    'target_link' => 'create_team_event'),
                'event_attendance_create' => array(
                    'label' => 4,
                    'status' => 'locked',
                    'text' => 'Potvrď účasť aspoň dvoch hráčov na udalosti',
                    'code' => 'event_attendance_create',
                    'target_link' => 'team_event_detail'),
                'event_roster_manual_create' => array(
                    'label' => 5,
                    'status' => 'locked',
                    'text' => 'Rozdeľ hráčov do tímov už pred zápasom- hráčom o tom príde notifikácia a predzápasové „podpichovačky“ môžu začať.  Súpisku si môžeš vytlačiť a počas zápasu do nej zapisovať góly a asistencie.
Neskôr ťa budú čakať ďalšie možnosti vytvorenia súpisky napr. na základe aktuálnej formy hráčov alebo náhodne',
                    'code' => 'event_roster_manual_create',
                    'target_link' => 'team_match_new_lineup'),
                'event_score_manual_create' => array(
                    'label' => 6,
                    'status' => 'locked',
                    'text' => 'Počas zápasu si zaznamenajte góly a asistencie. Po ich nahratí bude možné sledovať zaujímavé štatistiky a vytvárať súpisku automaticky na základe aktuálnej formy hráčov.
Pri ďalšom zápase už budeš mať možnosť zapisovať skóre v reálnom čase priamo do mobilu.',
                    'code' => 'event_score_manual_create',
                    'target_link' => 'team_match_create_live_stat'),
                'event_rating_create' => array(
                    'label' => 7,
                    'status' => 'locked',
                    'text' => 'Prideľ im hviezdy na základe ich výkonu v zápase, bez ohľadu na to, či skórovali. Aj účinná obrana mohla rozhodnúť zápas!
Po hodnotení nájdeš v hlavnej stránke tímu nový graf Porovnanie výkonov',
                    'code' => 'event_rating_create',
                    'target_link' => 'team_event_rating'),
                'step1_final' => array(
                    'label' => 8,
                    'status' => 'locked',
                    'text' => 'Zadaj ešte výsledky z 2 najbližších zápasov a po každom z nich sa tvojmu tímu odkryjú nové funkcie, z ktorých vám padne sánka.',
                    'code' => 'step1_final',
                    'target_link' => 'team_event_list'),
                'step2_final' => array(
                    'label' => 9,
                    'status' => 'locked',
                    'text' => 'Skvelé! Čaká na teba posledná úloha - odohrať a zaznamenať ešte jednu udalosť.',
                    'code' => 'step2_final',
                    'target_link' => 'team_event_list'),
                'step2_final2' => array(
                    'label' => 9,
                    'status' => 'locked',
                    'text' => 'Skvelé! Čaká na teba posledná úloha - odohrať a zaznamenať ešte jednu udalosť.',
                    'code' => 'step2_final2',
                    'target_link' => 'team_event_list'),
                'step3_final' => array(
                    'label' => 10,
                    'status' => 'locked',
                    'text' => 'Získavate plný prístup do PRO verzie na 30 dní',
                    'code' => 'step3_final',
                    'target_link' => 'team_event_list'),
            );
        }

        $steps = array();
        foreach ($stepsDefinitions as $key => $stepDefinition)
        {
            $step = new ProgressStep();
            $step->setStatus($stepDefinition['status']);
            $step->setCode($stepDefinition['code']);
            $step->setLabel($stepDefinition['label']);
            $step->setTooltip($stepDefinition['text']);
            $step->setTargetLink($stepDefinition['target_link']);
            $steps[$key] = $step;
        }
        return $steps;
    }

    /**
     * 
     * @param type $teamId
     * @return array
     */
    public function getExistAlert($teamId)
    {
        $existAlert = $this->getStepRepository()->findBy(array('team_id' => $teamId));
        if (null == $existAlert)
        {
            return array();
        }
        return $existAlert;
    }

    public function allStepsFinished($teamId)
    {
        $existAlert = $this->getStepRepository()->findOneBy(array('team_id' => $teamId, 'name' => 'step3_final', 'alert_status' => 'finished'));
        if (null != $existAlert)
        {
            return true;
        }
        return false;
    }
    
    public function userStepsFinished($userId)
    {
        $existAlert = $this->getStepRepository()->findOneBy(array('created_by' => $userId, 'name' => 'step3_final', 'alert_status' => 'finished'));
        if (null != $existAlert)
        {
            return true;
        }
        return false;
    }

    public function getAllowedPlaces()
    {
        return array('team_create', 'team_players_list', 'create_team_event', 'create_team_event_progress', 'team_match_new_lineup', 'team_match_edit_lineup', 'team_match_create_live_stat', 'team_event_detail', 'team_event_rating', 'team_event_list','team_overview','team_settings','team_match_new_lineup');
    }

    public function getCurrentStep()
    {
        $chain = $this->getProgressStepsChain(\Core\ServiceLayer::getService('security')->getIdentity());
        foreach ($chain as $step)
        {
            if ($step->getStatus() == 'waiting' or $step->getStatus() == 'next')
            {
                return $step;
            }
        }
    }

}
