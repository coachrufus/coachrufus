<?php

namespace CR\Gamification\Model;

use Core\EntityMapper as EntityMapper;
use Core\ServiceLayer as ServiceLayer;

class AlertLog {

    private $repository;
    private $mapper_rules = array(
        'id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => true
        ),
        'name' => array(
            'type' => 'string',
            'max_length' => '50',
            'required' => false
        ),
        'achievement_code' => array(
            'type' => 'string',
            'max_length' => '50',
            'required' => false
        ),
        'created_at' => array(
            'type' => 'datetime',
            'max_length' => '',
            'required' => false
        ),
        'created_by' => array(
            'type' => 'int',
            'max_length' => '',
            'required' => false
        ),
        'team_id' => array(
            'type' => 'int',
            'max_length' => '',
            'required' => false
        ),
        'alert_status' => array(
            'type' => 'string',
            'max_length' => '50',
            'required' => false
        ),
        'place_code' => array(
            'type' => 'string',
            'max_length' => '50',
            'required' => false
        ),
    );

    public function getDataMapperRules()
    {
        return $this->mapper_rules;
    }

    protected $id;
    protected $name;
    protected $achievementCode;
    protected $createdAt;
    protected $createdBy;
    protected $teamId;
    protected $alertStatus;
    protected $placeCode;

    public function setId($val)
    {
        $this->id = $val;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setName($val)
    {
        $this->name = $val;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setCreatedAt($val)
    {
        $this->createdAt = $val;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setCreatedBy($val)
    {
        $this->createdBy = $val;
    }

    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    public function getTeamId()
    {
        return $this->teamId;
    }

    public function setTeamId($teamId)
    {
        $this->teamId = $teamId;
    }
    
    public  function getAlertStatus() {
return $this->alertStatus;
}

public  function setAlertStatus($alertStatus) {
$this->alertStatus = $alertStatus;
}

public  function getAchievementCode() {
return $this->achievementCode;
}

public  function getPlaceCode() {
return $this->placeCode;
}

public  function setAchievementCode($achievementCode) {
$this->achievementCode = $achievementCode;
}

public  function setPlaceCode($placeCode) {
$this->placeCode = $placeCode;
}





}
