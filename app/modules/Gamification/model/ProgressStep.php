<?php

namespace CR\Gamification\Model;

class ProgressStep {

    private $name;
    private $tooltip;
    private $status;
    private $label;
    private $targetLink;
    private $code;

    function __construct()
    {
        
    }
    
    public  function getCode() {
return $this->code;
}

public  function setCode($code) {
$this->code = $code;
}



    public function getName()
    {
        return $this->name;
    }

    public function getTooltip()
    {
        return $this->tooltip;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function setTooltip($tooltip)
    {
        $this->tooltip = $tooltip;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function setLabel($label)
    {
        $this->label = $label;
    }

    public function getTargetLink()
    {
        return $this->targetLink;
    }

    public function setTargetLink($targetLink)
    {
        $this->targetLink = $targetLink;
    }
    
    public function getDescription()
    {
        //$text = $this->getCode().'<br /> TEXT';
        /*
        $layout = \Core\ServiceLayer::getService('layout');
        ob_start();
        $layout->includePart(MODUL_DIR . '/Gamification/view/progress/'.$this->getCode().'_desc.php');
        $text = ob_get_contents();
        ob_end_clean();
        */
        return $this->getTooltip();
    }
    


}
