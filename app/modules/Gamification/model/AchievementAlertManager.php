<?php

namespace CR\Gamification\Model;

class AchievementAlertManager {

    private $router;
    private $logManager;
    private $places;
    private $achievements;

    function __construct($router, $logManager)
    {
        $this->setRouter($router);
        $this->setLogManager($logManager);

        $places = $this->buildAlertPlaces();


        $this->setPlaces($places);
    }

    public function getLogManager()
    {
        return $this->logManager;
    }

    public function setLogManager($logManager)
    {
        $this->logManager = $logManager;
    }

    public function getRouter()
    {
        return $this->router;
    }

    public function setRouter($router)
    {
        $this->router = $router;
    }

    public function getPlaces()
    {
        return $this->places;
    }

    public function setPlaces($val)
    {
        $this->places = $val;
    }

    /**
     * 
     * @return array collection of \CR\Gamification\Model\Achievement
     */
    public function getAchievements()
    {
        $achievements = array();
        $achievementsDefinitions = array(
            'step_intro' => array('name' => 'step_intro'),
            'team_create' => array('name' => 'CREATE TEAM'),
            'team_player_create' => array('name' => 'CREATE TEAM PLAYER'),
            'team_player_create_confirm' => array('name' => 'team_player_create_confirm'),
            'event_create' => array('name' => 'CREATE EVENT'),
            'event_repeat_create' => array('name' => 'CREATE REPEAT EVENT'),
            'event_attendance_create' => array('name' => 'CREATE EVENT ATTENDANCE'),
            'event_attendance_confirm' => array('name' => 'event_attendance_confirm'),
            'event_roster_manual_create' => array('name' => 'event_roster_manual_create'),
            'event_score_manual_create' => array('name' => 'event_score_manual_create'),
            'event_rating_create' => array('name' => 'event_rating_create'),
            'step1_final' => array('name' => 'step1_final'),
            'event_roster_random_create' => array('name' => 'event_roster_random_create'),
            'event_score_live_create' => array('name' => 'event_score_live_create'),
            'step2_final' => array('name' => 'step2_final'),
            'step2_final2' => array('name' => 'step2_final2'),
            'event_roster_performance_create' => array('name' => 'event_roster_form_create'),
            'step3_final' => array('name' => 'step3_final'),
        );

        foreach ($achievementsDefinitions as $code => $achievementsDefinition)
        {
            $achievement = new Achievement();
            $achievement->setCode($code);
            $achievement->setName($achievementsDefinition['name']);
            $achievements[$code] = $achievement;
        }
        return $achievements;
    }

    /**
     * 
     * @param type $achievementCode
     * &return CR\Gamification\Model\Achievement
     */
    public function getAchievementByCode($achievementCode)
    {
        $achievements = $this->getAchievements();
        return $achievements[$achievementCode];
    }

    /**
     * 
     * @param array places defintion in format  array( key => array('name' => 'alert name', 'routes' => array() ));
     * @return \CR\Gamification\Model\AchievementAlertPlace
     */
    public function buildAlertPlaces($placesDefinitions = null)
    {

        if (null == $placesDefinitions)
        {
            $placesDefinitions = array(
                'team_create' => array(
                    'step_intro' => array(
                        'name' => 'Sprievodca',
                        'routes' => array('team_create'),
                    ),
                    'team_create' => array(
                        'name' => 'Vytvor si svoj prvý tím!',
                        'routes' => array('team_create'),
                    ),
                ),
                'team_players_list' => array(
                    'team_player_create' => array(
                        'name' => 'Pozvi členov do svojho tímu!',
                        'routes' => array('team_players_list'),
                    ),
                    'team_player_create_confirm' => array(
                        'name' => 'team_player_create_confirm_modal',
                        'routes' => array('team_players_list'),
                    )
                ),
                'event_create' => array(
                    'event_repeat_create' => array(
                        'name' => 'Vytvor svoju prvú udalosť!',
                        'routes' => array('create_team_event'),
                    ),
                ),
                'event_detail' => array(
                    'event_create' => array(
                        'name' => 'Vytvor svoju prvú udalosť!',
                        'routes' => array('create_team_event_progress'),
                    ),
                ),
                'event_detail_attendance' => array(
                    'event_attendance_create' => array(
                        'name' => 'Účasť hráčov',
                        'routes' => array('create_team_event_progress'),
                    ),
                    'event_attendance_confirm' => array(
                        'name' => 'Potvrdenie súpisky',
                        'routes' => array('create_team_event_progress'),
                    ),
                ),
                'event_detail_roster' => array(
                    'event_roster_manual_create' => array(
                        'name' => 'Vytvor zápasovú súpisku',
                        'routes' => array('team_match_new_lineup'),
                    ),
                    'event_roster_random_create' => array(
                        'name' => 'Náhodne generovaná supiska',
                        'routes' => array('team_match_new_lineup'),
                    ),
                    'event_roster_performance_create' => array(
                        'name' => 'Zápasová súpiska podľa formy',
                        'routes' => array('team_match_new_lineup'),
                    ),
                ),
                'event_detail_score' => array(
                    'event_score_manual_create' => array(
                        'name' => 'Zapisovanie gólov a asistencií',
                        'routes' => array('team_match_create_live_stat'),
                    ),
                    'event_score_live_create' => array(
                        'name' => 'Live Score',
                        'routes' => array('team_match_create_live_stat'),
                    ),
                ),
                'event_detail_rating' => array(
                    'event_rating_create' => array(
                        'name' => 'Ohodnoť svojich spoluhráčov.',
                        'routes' => array('team_event_rating'),
                    ),
                    'step1_final' => array(
                        'name' => 'Už len 2 kroky k získaniu PRO verzie na FREE_DAYS dní zadarmo!',
                        'routes' => array('team_event_rating'),
                    ),
                    'step2_final' => array(
                        'name' => 'Skvelé! Čaká na teba posledná úloha - odohrať a zaznamenať ešte jeden zápas.',
                        'routes' => array('team_event_rating'),
                    ),
                    'step3_final' => array(
                        'name' => 'A je to tu!',
                        'routes' => array('team_event_rating'),
                    ),
                ),
                'event_list' => array(
                    'step1_final' => array(
                        'name' => 'Už len 2 kroky k získaniu PRO verzie na FREE_DAYS dní zadarmo!',
                        'routes' => array('team_event_list'),
                    ),
                    'step2_final2' => array(
                        'name' => 'K získaniu plného prístupu do PRO verzie musíte odohrať a zapísať skóre ešte 1 zápasu',
                        'routes' => array('team_event_list'),
                    ),
                ),
            );
        }

        $places = array();
        foreach ($placesDefinitions as $placeCode => $achievementDefinition)
        {
            foreach ($achievementDefinition as $achievementCode => $achievementPlaceDefintion)
            {
                $alertPlace = new AchievementAlertPlace();
                $alertPlace->setRoutes($achievementPlaceDefintion['routes']);
                $alertPlace->setCode($placeCode);

                $alert = new AchievementAlert();
                $alert->setCode($achievementCode);
                $alert->setName($achievementPlaceDefintion['name']);
                $alert->setText($achievementPlaceDefintion['name']);
                $alertPlace->setAlert($alert);

                $achievement = $this->getAchievementByCode($achievementCode);
                $alertPlace->setAchievement($achievement);

                $places[$placeCode][$achievementCode] = $alertPlace;
            }
        }

        return $places;
    }

    /**
     * Return exit alert log from storage
     * @param type $achievementAlertPlace
     * @param type $teamId
     * @return CR\Gamification\Model\AlertLog
     */
    public function getTeamPlaceAlertLog($achievementAlertPlace, $teamId)
    {
        $achievementCode = $achievementAlertPlace->getAchievement()->getCode();
        $achievementPlace = $achievementAlertPlace->getCode();
        $existAlert = $this->getLogManager()->findOneBy(array('team_id' => $teamId, 'achievement_code' => $achievementCode, 'place_code' => $achievementPlace));
        return $existAlert;
    }
    
    public function userHasStepsFinished($stepCode,$user)
    {
         $step = $this->getLogManager()->findOneBy(array('created_by' => $user->getId(), 'achievement_code' => $stepCode));
         if(null != $step)
         {
             return $step->getAlertStatus();
         }
    }

    /**
     * 
     * @param \CR\Gamification\Model\CR\Gamification\Model\Achievement $achievement
     * @return boolean
     */
    public function checkAchievementCriteria(\CR\Gamification\Model\Achievement $achievement, $teamId,$userIdentity)
    {
         if(null == $userIdentity )
        {
            $userIdentity = \Core\ServiceLayer::getService('security')->getIdentity();
        }


        //accessible only for admins
        /*
        $userTeamRoles = $userIdentity->getTeamRoles();
        $teamRole = $userTeamRoles[$teamId];
       
        if($teamRole['role'] != 'ADMIN' && $teamId != null)
        {
            return false;
        }

        $step3FinishAlert = $this->getLogManager()->findOneBy(array('created_by' => $userIdentity->getUser()->getId(), 'achievement_code' => 'step3_final','alert_status' => 'finished'));

        if(null != $step3FinishAlert)
        {
            return false;
        }
         * 
         */
        $stepsManager = \Core\ServiceLayer::getService('StepsManager');


        if($stepsManager->showStepByStep() == false)
        {
            return false;
        }
        //check if ste by step finished for this team
        /*
        $actualTeamId = $userIdentity->getUserCurrentTeamId();
        if(null != $actualTeamId)
        {
            $step3FinishAlert = $this->getLogManager()->findOneBy(array('team_id' => $actualTeamId, 'achievement_code' => 'step3_final','alert_status' => 'finished'));
            if(null != $step3FinishAlert)
            {
                return false;
            }
        }
        */
        
        /*
        $userPackageRepository =  \Core\ServiceLayer::getService('UserPackageRepository');
        $existUserPro = $userPackageRepository->userHadProPackage($userIdentity->getUser()->getId());
        
        if(!empty($existUserPro))
        {
             return false;
        }
        */
        
        if ('step_intro' == $achievement->getCode())
        {
            return true;
        }
        
        if ('team_create' == $achievement->getCode())
        {
            return true;
        }

        if ('team_player_create' == $achievement->getCode())
        {
            return true;
        }
        
        if ('team_player_create_confirm' == $achievement->getCode())
        {
            $alertLog = $this->getLogManager()->findOneBy(array('team_id' => $teamId, 'achievement_code' => 'team_player_create'));
            if (null != $alertLog && $alertLog->getAlertStatus() == 'finished')
            {
                return true;
            }
        }

        if ('event_create' == $achievement->getCode())
        {
            return true;
        }

        if ('event_repeat_create' == $achievement->getCode())
        {
            $alertLog = $this->getLogManager()->findOneBy(array('team_id' => $teamId, 'achievement_code' => 'step1_final'));
            if (null != $alertLog && $alertLog->getAlertStatus() == 'finished')
            {
                return true;
            }
        } 

        if ('event_attendance_create' == $achievement->getCode())
        {
            return true;
        }
        if ('event_attendance_confirm' == $achievement->getCode())
        {
            return true;
        }

        if ('event_roster_manual_create' == $achievement->getCode())
        {
            return true;
        }

        if ('event_score_manual_create' == $achievement->getCode())
        {
            return true;
        }

        if ('event_rating_create' == $achievement->getCode())
        {
            return true;
        }

        if ('step1_final' == $achievement->getCode())
        {
            $alertLog = $this->getLogManager()->findOneBy(array('team_id' => $teamId, 'achievement_code' => 'event_rating_create'));
            if (null != $alertLog && $alertLog->getAlertStatus() == 'finished')
            {
                return true;
            }
        }

        if ('event_roster_random_create' == $achievement->getCode())
        {
            $alertLog = $this->getLogManager()->findOneBy(array('team_id' => $teamId, 'achievement_code' => 'event_score_manual_create'));
            if (null != $alertLog && $alertLog->getAlertStatus() == 'finished')
            {
                return true;
            }
        }

        if ('event_score_live_create' == $achievement->getCode())
        {
            $alertLog = $this->getLogManager()->findOneBy(array('team_id' => $teamId, 'achievement_code' => 'event_roster_random_create'));
            if (null != $alertLog && $alertLog->getAlertStatus() == 'finished')
            {
                return true;
            }
        }

        if ('step2_final' == $achievement->getCode())
        {
            $alertLog = $this->getLogManager()->findOneBy(array('team_id' => $teamId, 'achievement_code' => 'event_score_live_create'));
            if (null != $alertLog && $alertLog->getAlertStatus() == 'finished')
            {
                return true;
            }
        }

        if ('step2_final2' == $achievement->getCode())
        {
            $alertLog = $this->getLogManager()->findOneBy(array('team_id' => $teamId, 'achievement_code' => 'event_score_live_create'));
            if (null != $alertLog && $alertLog->getAlertStatus() == 'finished')
            {
                return true;
            }
        }
        if ('event_roster_performance_create' == $achievement->getCode())
        {
            $alertLog = $this->getLogManager()->findOneBy(array('team_id' => $teamId, 'achievement_code' => 'event_score_live_create'));
            if (null != $alertLog && $alertLog->getAlertStatus() == 'finished')
            {
                return true;
            }
        }

        if ('step3_final' == $achievement->getCode())
        {
            $alertLog = $this->getLogManager()->findOneBy(array('team_id' => $teamId, 'achievement_code' => 'event_roster_performance_create'));
            if (null != $alertLog && $alertLog->getAlertStatus() == 'finished')
            {
                return true;
            }
        }


        return false;
    }

    /**
     * Return status for alert, if not exist, return 'wwaiting'
     * @param string $achievementCode
     * @param int $teamId
     * @return string
     */
    public function getTeamAlertStatus($achievementCode, $teamId)
    {
        $existAlert = $this->getTeamAlertLog($achievementCode, $teamId);
        $alertLog = $this->getTeamAlertLog($achievementCode, $teamId);
        $place = $this->getPlaceByCode($achievementCode);


        if (null != $existAlert)
        {
            return $existAlert->getAlertStatus();
        }
        else
        {
            return 'waiting';
        }
    }

    /**
     * 
     * @param type $achievementAlertPlace
     */
    public function showAlertForPlace($achievementAlertPlace)
    {
        $achievementAlertPlace->showAlert();
    }

    /**
     * 
     * @param type $achievementCode
     * @return array array of CR\Gamification\Model\AchievementAlertPlace
     */
    public function getPlacesByPlaceCode($achievementCode)
    {
        $places = $this->getPlaces();
        if (array_key_exists($achievementCode, $places))
        {
            return $places[$achievementCode];
        }
    }

    /**
     * Get AchievementAlertPlace for specific place and achievement
     * @param type $placeCode
     * @param type $achievementCode
     * @return CR\Gamification\Model\AchievementAlertPlace
     */
    public function getPlaceAchievementAlert($placeCode, $achievementCode)
    {
        $places = $this->getPlacesByPlaceCode($placeCode);
        $achievementAlertPlace = $places[$achievementCode];
        return $achievementAlertPlace;
    }
    
    

    public function showPlaceAchievementAlert($placeCode, $achievementCode, $userIdentity,$force = false)
    {
        //first check if is finished
        return;
        
        $achievementAlertPlace = $this->getPlaceAchievementAlert($placeCode, $achievementCode);
        $achievement = $achievementAlertPlace->getAchievement();
        $teamId = $userIdentity->getUserCurrentTeamId();
        $alertLog = $this->getTeamPlaceAlertLog($achievementAlertPlace, $teamId);
        $achievementCriteria = $this->checkAchievementCriteria($achievement,$teamId,$userIdentity);

        if($force == false)
        {
             if($achievementCriteria == true)
            {
                if (null == $alertLog)
                {
                    if (true == $achievementCriteria)
                    {
                        $this->showAlertForPlace($achievementAlertPlace);
                    }
                }
                else
                {

                    $alertLogStatus = $alertLog->getAlertStatus();
                    if ($alertLogStatus != 'finished')
                    {
                        $this->showAlertForPlace($achievementAlertPlace);
                    }
                }
            }
        }
        else
        {
            $this->showAlertForPlace($achievementAlertPlace);
        }
       


       
    }
    
   

    /**
     * Show achievement alert if not exist or is not finished, and finished it after that
     * @param type $placeCode
     * @param type $achievementCode
     * @param type $userIdentity
     */
    public function showAndFinishPlaceAchievementAlert($placeCode, $achievementCode, $userIdentity)
    {
        return;
        $places = $this->getPlacesByPlaceCode($placeCode);
        $achievementAlertPlace = $places[$achievementCode];
        $achievement = $achievementAlertPlace->getAchievement();
        $teamId = $userIdentity->getUserCurrentTeamId();
        $alertLog = $this->getTeamPlaceAlertLog($achievementAlertPlace, $teamId);
        
        $achievementCriteria = $this->checkAchievementCriteria($achievement,$teamId);

        if($achievementCriteria == true)
        {
                if (null == $alertLog)
               {
                   $this->showAlertForPlace($achievementAlertPlace);
                       $this->createFinishedPlaceAlertLog($achievementAlertPlace, $userIdentity);
               }
               else
               {
                   $alertLogStatus = $alertLog->getAlertStatus();
                   if ($alertLogStatus != 'finished')
                   {
                       $this->showAlertForPlace($achievementAlertPlace);
                       $this->finishAlertLog($alertLog);
                   }
               }
        }

       
    }

    private function buildAlertLog($achievementAlertPlace, $userIdentity)
    {
        $alert = $achievementAlertPlace->getAlert();
        $achievement = $achievementAlertPlace->getAchievement();
        $log = new AlertLog();
        $log->setName($alert->getName());
        $log->setAchievementCode($achievement->getCode());
        $log->setPlaceCode($achievementAlertPlace->getCode());
        $log->setCreatedAt(new \DateTime());
        $log->setCreatedBy($userIdentity->getUser()->getId());
        $log->setTeamId($userIdentity->getUserCurrentTeamId());
        return $log;
    }

    /**
     * Create alert log and set status as finished
     * @param \CR\Gamification\Model\AchievementAlertPlace $achievementAlertPlace
     * @param User\Model\UserIdentity $userIdentity
     */
    public function createFinishedPlaceAlertLog(\CR\Gamification\Model\AchievementAlertPlace $achievementAlertPlace, $userIdentity)
    {
        $logManager = $this->getLogManager();
        $log = $this->buildAlertLog($achievementAlertPlace, $userIdentity);
        $log->setAlertStatus('finished');
        $logManager->save($log);
    }

    /**
     * Create alert log and set as waiting
     * @param \CR\Gamification\Model\AchievementAlertPlace $achievementAlertPlace
     * @param User\Model\UserIdentity $userIdentity 
     */
    public function createWaitingAchievementAlertLog($placeCode, $achievementCode, $userIdentity)
    {
        $places = $this->getPlacesByPlaceCode($placeCode);
        $achievementAlertPlace = $places[$achievementCode];

        $logManager = $this->getLogManager();

        //check if exist
        $existLog = $this->getTeamPlaceAlertLog($achievementAlertPlace, $userIdentity->getUserCurrentTeamId());

        if (null == $existLog)
        {
            $log = $this->buildAlertLog($achievementAlertPlace, $userIdentity);
            $log->setAlertStatus('waiting');
            $logManager->save($log);
        }
    }

    /**
     * Set status of exist alert log as finished
     * @param CR\Gamification\Model\AlertLog $alertLog
     */
    public function finishAlertLog($alertLog)
    {
        if (null != $alertLog)
        {
            $logManager = $this->getLogManager();
            $alertLog->setAlertStatus('finished');
            $logManager->save($alertLog);
        }
    }
    
    public function showLastUnfinishedAchievementAlert($teamId)
    {
        //check if pro is paid
        
        
        
        $progresChain = array('team_create','team_player_create','event_create','event_attendance_create','event_attendance_confirm' ,'event_roster_manual_create','event_score_manual_create','event_rating_create','step1_final', 'event_repeat_create' ,'event_roster_random_create','event_score_live_create','step2_final','step2_final2','event_roster_performance_create'  );
        
        $existAlerts = $this->getLogManager()->findBy(array('team_id' => $teamId,'alert_status' => 'waiting'));
        
        $unfinished = array();
        foreach($existAlerts as $existAlert)
        {
            if(in_array($existAlert->getAchievementCode() , $progresChain))
            {
                $key = array_search($existAlert->getAchievementCode(),$progresChain);
                $unfinished[$key] = $existAlert;
            }
        }
        
        $nearest = current($unfinished);
        
        if(null != $nearest)
        {
            $this->showPlaceAchievementAlert($nearest->getPlaceCode(),$nearest->getAchievementCode(),\Core\ServiceLayer::getService('security')->getIdentity());
        }
    }
    
    public function saveAlertLog($alertLog)
    {
        $this->getLogManager()->save($alertLog);
    }
    
   

}
