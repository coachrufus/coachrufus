<?php

namespace CR\Gamification\Model;

class AchievementAlert {

    private $name;
    private $code;
    private $text;
    private $status;

    function __construct()
    {
        
    }

    public function getName()
    {
        return $this->name;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function getText()
    {
        return $this->text;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function setCode($code)
    {
        $this->code = $code;
    }

    public function setText($text)
    {
        $this->text = $text;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
        $this->status = $status;
    }
    
    public function showSplashScreen()
    {
        $layout = \Core\ServiceLayer::getService('layout');
        $translator = \Core\ServiceLayer::getService('translator');
        $request = \Core\ServiceLayer::getService('request');
        
        $layout->addJavascript('js/AchievementManager.js');
        
        $userIdentity =  \Core\ServiceLayer::getService('security')->getIdentity();
        $actualTeamId = $userIdentity->getUserCurrentTeamId();
        
        
        $teamManager = \Core\ServiceLayer::getService('TeamManager');
        $team = $teamManager->findTeamById($actualTeamId);
        if(null !=$team)
        {
             $isFloorballChallengeTeam = $teamManager->isFloorballChallengeTeam($team);
             $isPromoTeam = $teamManager->isPromoTeam($team,'GROUP');
        }
        else
        {
            $isFloorballChallengeTeam = false;
        }


        if($isFloorballChallengeTeam)
        {
            $this->setName(str_replace('na 30 dní zadarmo', 'do 30.júna 2017', $this->getName()));
        }
        
        $templateLangDir = (LANG == 'sk') ? '/' : '/en/';
        $alertTemplate = $this->getCode().'.php';
        
        $freeTime = 30;
        if($isPromoTeam or $request->get('tt') == 'promo-group' )
        {
            $freeTime = 90;
        }
        
        ob_start();
        $layout->includePart(MODUL_DIR . '/Gamification/view/alert'.$templateLangDir.$alertTemplate,array(
            'isFloorballChallengeTeam' => $isFloorballChallengeTeam,
            'freeTime' => $freeTime,
            'teamId' => $actualTeamId,
                ));
        $text = ob_get_contents();
        ob_end_clean();
        
        $s2sContinueLink = '#';
        
        if($this->getCode() == 'event_roster_manual_create')
        {
            $s2sContinueLink = '#create_lineup';
        }
        
        $title = str_replace('FREE_DAYS', $freeTime, $translator->translate($this->getName()));
        $text = str_replace('FREE_DAYS', $freeTime, $text);
        
        
        $layout->includePart(MODUL_DIR . '/Gamification/view/alert'.$templateLangDir.'modal.php',array(
            'title' => $title,
            'text' => $text,
            'code' => $this->getCode(),
            'teamId' => $actualTeamId,
            'isFloorballChallengeTeam' => $isFloorballChallengeTeam,
            's2sContinueLink' => $s2sContinueLink
            ));
    }

}
