<?php

namespace CR\Gamification\Model;

class AchievementAlertPlace {

    private $alert;
    private $achievement;
    private $routes;
    private $code;

    public function __construct()
    {
        
    }
    
   public  function getCode() {
return $this->code;
}

public  function setCode($code) {
$this->code = $code;
}





    public function getAlert()
    {
        return $this->alert;
    }

    public function setAlert($alert)
    {
        $this->alert = $alert;
    }

    public function getRoutes()
    {
        return $this->routes;
    }

    public function setRoutes($route)
    {
        $this->routes = $route;
    }

    public function getAchievement()
    {
        return $this->achievement;
    }

    public function setAchievement($achievement)
    {
        $this->achievement = $achievement;
    }

    public function showAlert()
    {
        $this->getAlert()->showSplashScreen();
    }

}
