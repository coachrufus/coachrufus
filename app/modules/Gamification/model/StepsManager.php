<?php

namespace CR\Gamification\Model;

use Core\GUID;
use Core\ServiceLayer;

class StepsManager {

    private $achievementManager;
    private $progressManager;
    private $chain;

    public function __construct($achievementManager, $progressManager)
    {
        $this->setAchievementManager($achievementManager);
        $this->setProgressManager($progressManager);
    }

    public function getAchievementManager()
    {
        return $this->achievementManager;
    }

    public function setAchievementManager($achievementManager)
    {
        $this->achievementManager = $achievementManager;
    }

    public function getProgressManager()
    {
        return $this->progressManager;
    }

    public function setProgressManager($progressManager)
    {
        $this->progressManager = $progressManager;
    }

    public function getUserIdentity()
    {
        $security = ServiceLayer::getService('security');
        $identity = $security->getIdentity();
        return $identity;
    }

    public function getChain()
    {
        if(null == $this->chain)
        {
            $progressChainManager = $this->getProgressManager();
            $this->chain = $progressChainManager->getProgressStepsChain($this->getUserIdentity());
        }
        return $this->chain;
    }

    public function setChain($chain)
    {
        $this->chain = $chain;
    }

    public function finishCreateTeam()
    {
        $achievementAlertManager = $this->getAchievementManager();
        $achievementAlertPlace = $achievementAlertManager->getPlaceAchievementAlert('team_create', 'team_create');
        $achievementAlertManager->createFinishedPlaceAlertLog($achievementAlertPlace, $this->getUserIdentity());
        $achievementAlertManager->createWaitingAchievementAlertLog('team_players_list', 'team_player_create', $this->getUserIdentity());
    }
    
    public function finishCreateTeamPlayer($team)
    {
        $achievementAlertManager = ServiceLayer::getService('AchievementAlertManager');
        $achievementAlertPlace = $achievementAlertManager->getPlaceAchievementAlert('team_players_list','team_player_create');
        $alertLog = $achievementAlertManager->getTeamPlaceAlertLog($achievementAlertPlace,$team->getId());
        $achievementAlertManager->finishAlertLog($alertLog);
        $achievementAlertManager->createWaitingAchievementAlertLog('event_detail','event_create',$this->getUserIdentity());
        $achievementAlertManager->createWaitingAchievementAlertLog('team_players_list', 'team_player_create_confirm', $this->getUserIdentity());
    }
    
    public function finishCreateEvent($team)
    {
         $achievementAlertManager = ServiceLayer::getService('AchievementAlertManager');
        $achievementAlertPlace = $achievementAlertManager->getPlaceAchievementAlert('event_detail','event_create');
        $alertLog = $achievementAlertManager->getTeamPlaceAlertLog($achievementAlertPlace,$team->getId());
        $achievementAlertManager->finishAlertLog($alertLog);
        $achievementAlertManager->createWaitingAchievementAlertLog('event_detail_attendance','event_attendance_create',$this->getUserIdentity());
    }
    
    public function finishCreateAttendance($team)
    {
        $achievementAlertManager = ServiceLayer::getService('AchievementAlertManager');
        $achievementAlertPlace = $achievementAlertManager->getPlaceAchievementAlert('event_detail_attendance','event_attendance_create');
        $alertLog = $achievementAlertManager->getTeamPlaceAlertLog($achievementAlertPlace,$team->getId());
        $achievementAlertManager->finishAlertLog($alertLog);
        $achievementAlertManager->createWaitingAchievementAlertLog('event_detail_roster','event_roster_manual_create',$this->getUserIdentity());
    }
    
    public function finishCreateManualLineup($team)
    {
        $achievementAlertManager = ServiceLayer::getService('AchievementAlertManager');
        $achievementAlertPlace = $achievementAlertManager->getPlaceAchievementAlert('event_detail_roster','event_roster_manual_create');
        $alertLog = $achievementAlertManager->getTeamPlaceAlertLog($achievementAlertPlace,$team->getId());
        $achievementAlertManager->finishAlertLog($alertLog);
        $achievementAlertManager->createWaitingAchievementAlertLog('event_detail_score','event_score_manual_create',$this->getUserIdentity());
    }
    
     public function finishCreateManualScore($teamId)
    {
        $achievementAlertManager = ServiceLayer::getService('AchievementAlertManager');
        $achievementAlertPlace = $achievementAlertManager->getPlaceAchievementAlert('event_detail_score','event_score_manual_create');
        $alertLog = $achievementAlertManager->getTeamPlaceAlertLog($achievementAlertPlace,$teamId);
        $achievementAlertManager->finishAlertLog($alertLog);
        $achievementAlertManager->createWaitingAchievementAlertLog('event_detail_rating','event_rating_create',$this->getUserIdentity());
    }
    
    public function finishRating($teamId)
    {
        $achievementAlertManager = ServiceLayer::getService('AchievementAlertManager');
        $achievementAlertPlace = $achievementAlertManager->getPlaceAchievementAlert('event_detail_rating','event_rating_create');
        $alertLog = $achievementAlertManager->getTeamPlaceAlertLog($achievementAlertPlace,$teamId);
        $achievementAlertManager->finishAlertLog($alertLog);
        $achievementAlertManager->createWaitingAchievementAlertLog('event_list','step1_final',ServiceLayer::getService('security')->getIdentity());
        $achievementAlertManager->createWaitingAchievementAlertLog('event_create','event_repeat_create',$this->getUserIdentity());
        
        
    }
    
     
    public function finishRepeatEvent($team)
    {
        $achievementAlertManager = ServiceLayer::getService('AchievementAlertManager');
        $achievementAlertPlace = $achievementAlertManager->getPlaceAchievementAlert('event_create','event_repeat_create');
        $alertLog = $achievementAlertManager->getTeamPlaceAlertLog($achievementAlertPlace,$team->getId());
        
        $achievementAlertPlace2 = $achievementAlertManager->getPlaceAchievementAlert('event_list','step1_final');
        $alertLog2 = $achievementAlertManager->getTeamPlaceAlertLog($achievementAlertPlace2,$team->getId());
        
        
        if(null != $alertLog) 
        {
            $achievementAlertManager->finishAlertLog($alertLog);
            $achievementAlertManager->finishAlertLog($alertLog2);
            $achievementAlertManager->createWaitingAchievementAlertLog('event_detail_roster','event_roster_random_create',$this->getUserIdentity());
        }
    }
    
    
    
    public function finishCreateRandomLineup($team)
    {
        $achievementAlertManager = ServiceLayer::getService('AchievementAlertManager');
        $achievementAlertPlace = $achievementAlertManager->getPlaceAchievementAlert('event_detail_roster','event_roster_random_create');
        $alertLog = $achievementAlertManager->getTeamPlaceAlertLog($achievementAlertPlace,$team->getId());
        $achievementAlertManager->finishAlertLog($alertLog);
        if(null != $alertLog) 
        {
             $achievementAlertManager->createWaitingAchievementAlertLog('event_detail_score','event_score_live_create',$this->getUserIdentity());
        }
    }
    
   public function finishCreateLiveScore($teamId)
    {
        $achievementAlertManager = ServiceLayer::getService('AchievementAlertManager');
        $achievementAlertPlace = $achievementAlertManager->getPlaceAchievementAlert('event_detail_score','event_score_live_create');
        $alertLog = $achievementAlertManager->getTeamPlaceAlertLog($achievementAlertPlace,$teamId);
        $achievementAlertManager->finishAlertLog($alertLog);

        if(null != $alertLog)
        {
             $achievementAlertManager->createWaitingAchievementAlertLog('event_detail_rating','step2_final',$this->getUserIdentity());
             $achievementAlertManager->createWaitingAchievementAlertLog('event_list','step2_final2',$this->getUserIdentity());
             $achievementAlertManager->createWaitingAchievementAlertLog('event_detail_roster','event_roster_performance_create',$this->getUserIdentity());
        }
    }
    
    
    public function finishCycle2($teamId)
    {
        $achievementAlertManager = ServiceLayer::getService('AchievementAlertManager');
        $achievementAlertPlace = $achievementAlertManager->getPlaceAchievementAlert('event_detail_rating','step2_final');
        $alertLog = $achievementAlertManager->getTeamPlaceAlertLog($achievementAlertPlace,$teamId);
        
        
        if(null !=  $alertLog)
        {
            $achievementAlertManager->finishAlertLog($alertLog);
            
            /*
            $achievementAlertPlace2 = $achievementAlertManager->getPlaceAchievementAlert('event_list','step2_final2');
            $alertLog2 = $achievementAlertManager->getTeamPlaceAlertLog($achievementAlertPlace2,$teamId);
            $achievementAlertManager->finishAlertLog($alertLog2);
             * 
             */
        }
    }

    public function finishCreatePerformanceLineup($team)
    {
        $achievementAlertManager = ServiceLayer::getService('AchievementAlertManager');
        $achievementAlertPlace = $achievementAlertManager->getPlaceAchievementAlert('event_detail_roster','event_roster_performance_create');
        $alertLog = $achievementAlertManager->getTeamPlaceAlertLog($achievementAlertPlace,$team->getId());
       
        if(null != $alertLog) 
        {
            $achievementAlertManager->finishAlertLog($alertLog);
            $achievementAlertPlace2 = $achievementAlertManager->getPlaceAchievementAlert('event_list','step2_final2');
            $alertLog2 = $achievementAlertManager->getTeamPlaceAlertLog($achievementAlertPlace2,$team->getId());
            $achievementAlertManager->finishAlertLog($alertLog2);
            $achievementAlertManager->createWaitingAchievementAlertLog('event_detail_rating','step3_final',$this->getUserIdentity());
        }
    }
    
    
    
    
    public function finishCycle3($team)
    {
        $achievementAlertManager = ServiceLayer::getService('AchievementAlertManager');
        $achievementAlertPlace = $achievementAlertManager->getPlaceAchievementAlert('event_detail_rating','step3_final');
        $alertLog = $achievementAlertManager->getTeamPlaceAlertLog($achievementAlertPlace,$team->getId());
        
        
        
        $teamManager = \Core\ServiceLayer::getService('TeamManager');
        $isFloorballChallengeTeam = $teamManager->isFloorballChallengeTeam($team);
        
        $isPromoTeam = $teamManager->isPromoTeam($team,'GROUP');


        if(null != $alertLog && $alertLog->getAlertStatus() == 'waiting')
        {

            if($isFloorballChallengeTeam)
            {
                $end = new \DateTime('2017-06-30 00:00:00');
            }
            elseif($isPromoTeam)
            {
                $end = new \DateTime();
                $end->modify('+3 month');
            }
            else 
            {
                $end = new \DateTime();
                $end->modify('+30 day');
            }

            $pm = ServiceLayer::getService('PackageManager');
            $proPackage = $pm->getPackageWithProducts(5);
            ServiceLayer::getService('UserPackageManager')->addUserPackage([
                'user_id' => $this->getUserIdentity()->getUser()->getId(),
                'package_id' => 5,
                'team_id' => $team->getId(),
                'team_name' => $team->getName(),
                'date' => new \DateTime(),
                'start_date' => new \DateTime(),
                'end_date' => $end,
                'name' => 'PRO',
                'note' => 'TRIAL',
                'level' => 5,
                'products' => json_encode($proPackage['products']),
                'player_limit' => 100,
                'guid' => GUID::create(),
                 'variant' => 0,
                 'renew_payment' => 0
            ]);
            
            
             $achievementAlertManager->finishAlertLog($alertLog);
             
             
             $request = ServiceLayer::getService('request');
             $request->addFlashMessage('final_step3', 'success');
            
        }
        
                   
    }
    
    
    /*
    public function finishAll($team)
    {
        $achievementAlertManager = ServiceLayer::getService('AchievementAlertManager');
        $achievementAlertPlace = $achievementAlertManager->getPlaceAchievementAlert('event_detail_rating','step3_final');
        $alertLog = $achievementAlertManager->getTeamPlaceAlertLog($achievementAlertPlace,$team->getId());
        
        if(null != $alertLog)
        {
             $achievementAlertManager->finishAlertLog($alertLog);
        }
    }
     * 
     */
    
    /**
     * Check if is step finished
     * @param string $key 
     * @return bool
     * 
     */
    public function isStepFinished($key)
    {
        if($this->showStepByStep() == false)
        {
            return true;
        }
        
        
        $chain =  $this->getChain();
        foreach ($chain as $stepKey => $step)
        {
            if ($stepKey == $key && $step->getStatus() == 'finished')
            {
               return true;
            }
        }
        return false;
    }

    public function getMenuStepClass($key)
    {
        $menuItems = array(
            'team_settings' => 'unactive',
            'team_playground_list' => 'unactive',
            'team_event_list' => 'unactive',
            'team_season_list' => 'unactive',
            'team_public_players_list' => 'unactive',
            'team_players_attendance' => 'unactive',
            'team_player_stat' => 'unactive',
            'widget_list' => 'unactive',
            'attendance' => 'unactive',
        );
        
        if($this->showStepByStep() == false)
        {
            return '';
        }
       
        
        $chain =  $this->getChain();
        foreach ($chain as $stepKey => $step)
        {
            if ($stepKey == 'team_create' && ($step->getStatus() == 'finished'))
            {
                $menuItems['team_settings'] = '';
                $menuItems['team_playground_list'] = '';
               
            }
            if ($stepKey == 'team_player_create' && ($step->getStatus() == 'finished'))
            {
                 $menuItems['team_public_players_list'] = '';
            } 
            if ($stepKey == 'event_create' && ($step->getStatus() == 'finished'))
            {
                 $menuItems['team_event_list'] = '';
                 $menuItems['team_season_list'] = '';
            } 
             if ($stepKey == 'event_attendance_create' && ($step->getStatus() == 'finished'))
            {
                 $menuItems['team_players_attendance'] = '';
            } 
            if ($stepKey == 'step1_final' && ($step->getStatus() == 'finished'))
            {
                 $menuItems['team_player_stat'] = '';
                 $menuItems['widget_list'] = '';
            } 
            if ($stepKey == 'event_rating_create' && ($step->getStatus() == 'finished'))
            {
                 $menuItems['attendance'] = '';
            } 
        }

        return $menuItems[$key];
    }
    
    /**
     * Check if show step by step
     * @param type $user
     */
    public function showStepByStep()
    {
        /*
        if($_SESSION['user_had_pro'] == true)
        {
            return false;
        }
        */
        return false;
        
        $userIdentity = ServiceLayer::getService('security')->getIdentity();
        $actualTeamId = $userIdentity->getUserCurrentTeamId();
        $userTeamRoles = $userIdentity->getTeamRoles();
        //check if this team has finished step by step
        if(null != $actualTeamId)
        {
            $achievementAlertManger = \Core\ServiceLayer::getService('AchievementAlertManager');
            $step3FinishAlert = $achievementAlertManger->getLogManager()->findOneBy(array('team_id' => $actualTeamId, 'achievement_code' => 'step3_final','alert_status' => 'finished'));
        
            //check if this team has finished step by step
            if(null != $step3FinishAlert)
            {
                return false;
            }
            
            //only for admin
            $teamRole = $userTeamRoles[$actualTeamId];
            if($teamRole['role'] != 'ADMIN' && $actualTeamId != null)
            {
                return false;
            }
        }

        return true;
    }
    
    
    /*
     * Refresh criteria for step by step display
     */
    public function refreshStepByStepViewCriteria($user)
    {
        $userPackageRepository =  \Core\ServiceLayer::getService('UserPackageRepository');
        $existUserPro = $userPackageRepository->userHadProPackage($user->getId());
        
        if(!empty($existUserPro))
        {
             $_SESSION['user_had_pro'] = true;
        }
        else
        {
            $_SESSION['user_had_pro'] = false;
        }
    }
    
    public function handleRedirectToTeamLastStep($team)
    {
        return;
        if($this->showStepByStep() && false == $this->isStepFinished('step1_final') && ServiceLayer::getService('security')->getIdentity()->hasTeamPermission('manageSettings',$team))
        {
            $progressChainManager = ServiceLayer::getService('ProgressChainManager');
            $teamEventManager = ServiceLayer::getService('TeamEventManager');
            $currentStep = $progressChainManager->getCurrentStep();
            
            $lastEvent = $teamEventManager->getTeamLastEvent($team, new \DateTime());
            $router = ServiceLayer::getService('router');
            $request = ServiceLayer::getService('request');
            

          



            if($currentStep->getTargetLink() == 'team_event_detail' or $currentStep->getTargetLink() == 'team_event_rating')
            {
               
                if(null == $lastEvent)
                {
                    $link = $router->link('create_team_event',array('team_id' => $team->getId() ));
                    $request->redirect($link);
                }
                $link = $router->link($currentStep->getTargetLink(),array('id' => $lastEvent->getId(),'current_date' => $lastEvent->getCurrentDate()->format('Y-m-d')));
                $request->redirect($link);
            }
            
            if($currentStep->getTargetLink() == 'create_team_event')
            {
               
                $link = $router->link($currentStep->getTargetLink(),array('team_id' => $team->getId()));
                $request->redirect($link);
            }
            
            if($currentStep->getTargetLink() == 'team_players_list')
            {
               
                $link = $router->link($currentStep->getTargetLink(),array('team_id' => $team->getId()));
                $request->redirect($link);
            }
            
            if($currentStep->getTargetLink() == 'team_match_new_lineup')
            {
                if(null == $lastEvent)
                {
                    $link = $router->link('create_team_event',array('team_id' => $team->getId() ));
                    $request->redirect($link);
                }
                
                $link = $router->link($currentStep->getTargetLink(),array('event_id' => $lastEvent->getId(),'event_date' => $lastEvent->getCurrentDate()->format('Y-m-d')));
                $request->redirect($link);
            }
            
            if($currentStep->getTargetLink() == 'team_match_create_live_stat')
            {
                if(null == $lastEvent)
                {
                    $link = $router->link('create_team_event',array('team_id' => $team->getId() ));
                    $request->redirect($link);
                }
                
                $link = $router->link($currentStep->getTargetLink(),array('event_id' => $lastEvent->getId(),'eventdate' => $lastEvent->getCurrentDate()->format('Y-m-d')));
               $request->redirect($link);
            }
        }
    }
    
    
    public function finishStepByStep($authorId,$teamId)
    {
         \Core\DbQuery::query("INSERT INTO team_achievement_alert_log (name, achievement_code, created_at, created_by, team_id, alert_status, place_code) VALUES ('A je to tu!', 'step3_final', '".date('Y-m-d H:i:s')."', ".$authorId.",  ".$teamId.", 'finished', 'event_detail_rating')");
    }
  
    
    
    /*
    public function buildStepLink($routeName,$team)
    {
       
    }
    */
}
