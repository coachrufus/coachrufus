<?php
namespace User\Controller;

use Core\RestApiController;
use Core\ServiceLayer;
class RestController extends RestApiController
{   
   
    
    public function loginAction()
    {
        $result = array();
        $result['params'] = $this->getRequestParams();
        if(false == $this->checkApiKey())
        {
            return $this->unvalidApiKeyResult();
        }
        
        $security = ServiceLayer::getService('security');
        $username = $this->getRequest()->get('username');
        $plain_password = $this->getRequest()->get('pass');
        $auth_result = $security->authenticateUser($username, $plain_password);
        
        if($auth_result['RESULT'] == 1 && $auth_result['MESSAGE'] == 'NOTE_LOGIN_SUCCESS')
        {
           $userIdentity  = $security->getIdentity();
           $user = $userIdentity->getUser();
           $teamRoles = $userIdentity->getTeamRoles();
           
         
           $result['user_data'] = array();
           $result['user_data']['id'] = $user->getId();
           $result['user_data']['username'] = $user->getEmail();
           $result['user_data']['name'] = $user->getName();
           $result['user_data']['surname'] = $user->getSurname();
           $result['user_data']['icon'] =  WEB_DOMAIN.'/player/icon?file='.$user->getPhoto();
           $result['user_data']['teamroles'] = $teamRoles;
           $result['status'] = 'success';
           
        }
       
        
        if($auth_result['RESULT'] == 0)
        {
            $result['error_message'] = 'UNVALID LOGIN DATA';
            $result['status'] = 'error';
            
        }
       
        return $this->asJson($result);
    }
    
    public function  loginFbAction()
    {
        $result = array();
        $result['params'] = $this->getRequestParams();
        if(false == $this->checkApiKey())
        {
            return $this->unvalidApiKeyResult();
        }
        
        $security = ServiceLayer::getService('security');
        $identityProvider = $security->getIdentityProvider();
        $username = $this->getRequest()->get('username');
        $fbID = $this->getRequest()->get('fbid');
        
        $user = $identityProvider->findUserByUsername($username);
        

        
        if($user->getFacebookId() == $fbID)
        {
            $security->authenticateUserEntity($user);      
            $userIdentity  = $security->getIdentity();
            $user = $userIdentity->getUser();
            $teamRoles = $userIdentity->getTeamRoles();


            $result['user_data'] = array();
            $result['user_data']['id'] = $user->getId();
            $result['user_data']['username'] = $user->getEmail();
            $result['user_data']['name'] = $user->getName();
            $result['user_data']['surname'] = $user->getSurname();
            $result['user_data']['icon'] =  WEB_DOMAIN.'/player/icon?file='.$user->getPhoto();
            $result['user_data']['teamroles'] = $teamRoles;
            $result['status'] = 'success';
        }
        else
        {
             $result['error_message'] = 'UNVALID LOGIN DATA';
             $result['status'] = 'error';
        }
        return $this->asJson($result);
    }
    
    public function  loginGoogleAction()
    {
        $result = array();
        $result['params'] = $this->getRequestParams();
        if(false == $this->checkApiKey())
        {
            return $this->unvalidApiKeyResult();
        }
        
        $security = ServiceLayer::getService('security');
        $identityProvider = $security->getIdentityProvider();
        $username = $this->getRequest()->get('username');
        $gID = $this->getRequest()->get('gid');
        
        $user = $identityProvider->findUserByUsername($username);
        

        
        if($user->getGoogleId() == $gID)
        {
            $security->authenticateUserEntity($user);      
            $userIdentity  = $security->getIdentity();
            $user = $userIdentity->getUser();
            $teamRoles = $userIdentity->getTeamRoles();


            $result['user_data'] = array();
            $result['user_data']['id'] = $user->getId();
            $result['user_data']['username'] = $user->getEmail();
            $result['user_data']['name'] = $user->getName();
            $result['user_data']['surname'] = $user->getSurname();
            $result['user_data']['icon'] =  WEB_DOMAIN.'/player/icon?file='.$user->getPhoto();
            $result['user_data']['teamroles'] = $teamRoles;
            $result['status'] = 'success';
        }
        else
        {
             $result['error_message'] = 'UNVALID LOGIN DATA';
             $result['status'] = 'error';
        }
        return $this->asJson($result);
    }
    
    public function authInfoAction()
    {
         $security = ServiceLayer::getService('security');
         $userIdentity  = $security->getIdentity();
         $user = $userIdentity->getUser();
         if(null != $user)
         {
           $result['auth_status'] = 'login';
           $result['user_data'] = array();
           $result['user_data']['id'] = $user->getId();
           $result['user_data']['facebook_id'] = $user->getFacebookId();
           $result['user_data']['google_id'] = $user->getGoogleId();
           $result['user_data']['username'] = $user->getEmail();
           $result['user_data']['name'] = $user->getName();
           $result['user_data']['surname'] = $user->getSurname();
           $result['user_data']['icon'] =  WEB_DOMAIN.'/player/icon?file='.$user->getPhoto();
           $result['user_data']['teamRoles'] = $userIdentity->getTeamRoles();
         }
         else
         {
              $result['auth_status'] = 'logout';
         }
         
          return $this->asJson($result);
         
    }
	
}