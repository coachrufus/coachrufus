<?php
namespace User\Controller;
use Core\Controller as Controller;
use Core\Request as Request;
use Core\ServiceLayer;
use Core\DatagridTable as DatagridTable;
use Core\Validator as Validator;

use User\Model\User as User;
use User\Form\UserForm as UserForm;
use User\Form\UserProfilForm as UserProfilForm;
use User\Model\UserRepository as UserRepository;
use Core\DbQuery;
use User\Model\UserIdentityProvider;

class SecurityController extends Controller
{   
    public function redirectLoginAction()
    {
        
    }
    
    
    public function loginAction()
    {
        $request = ServiceLayer::getService('request');
        if(null != $request->get('lang'))
        {
            $_SESSION['app_lang'] = $request->get('lang');
            $translator = ServiceLayer::getService('translator');
            $translator->setLang($request->get('lang'));
        }
        //reset
         unset($_SESSION['user_had_pro']);
        
        if(array_key_exists('invitation', $_SESSION) && $request->get('inv') == null)
        {
           $request->redirect($this->getRouter()->link('login',array('lang' => $request->get('lang'),'inv' => $_SESSION['invitation']['hash'])));
        }
        
        if(null != $request->get('skliga2019'))
        {
            $_SESSION['skliga2019'] = $request->get('skliga2019');
        }

        

        $security = ServiceLayer::getService('security');
        $router = ServiceLayer::getService('router');
        $validator = new Validator();
        
        //check remember me
        
        if(isset($_COOKIE['persist_hash']))
        {
           
            
            $auth_result = $security->authenticatePersistUser($_COOKIE['persist_hash']);
              if($auth_result['RESULT'] == 1 && $auth_result['MESSAGE'] == 'NOTE_LOGIN_SUCCESS')
              {
                  //set lang by user language
                  $user = $security->getIdentity()->getUser();
                  
                  if('flch' == $this->getRequest()->get('promo'))
                  {
                      $user->setResource('flch');
                      $userRepo = ServiceLayer::getService('user_repository');
                      $userRepo->save($user);
                      $security->reloadUser($user);
                  }

                  if(null != $user->getDefaultLang())
                  {
                    $_SESSION['app_lang'] =$user->getDefaultLang();
                    $translator = ServiceLayer::getService('translator');
                    $translator->setLang($_SESSION['app_lang']);
                  }
                  
                  
                  if(null !=$request->get('rurl'))
                  {
                      $request->Redirect($request->get('rurl'));
                  }
                  else
                  {
                      $request->Redirect($router->link('player_dashboard',array('userid' =>  $auth_result['ID'] )));
                  }
                  
                  
              }
        }
        
        
        if('POST' == $request->getMethod())
        {
            $username = $request->get('username');
            $plain_password = $request->get('password');
            $auth_result = $security->authenticateUser($username, $plain_password);

            if($auth_result['RESULT'] == 1 && $auth_result['MESSAGE'] == 'NOTE_LOGIN_SUCCESS')
            {
                if(1 == $request->get('remember'))
                {
                    $user = $security->getIdentity()->getUser();
                    $security->remeberUser($user);
                }
                
                if('flch' == $this->getRequest()->get('promo'))
                {
                    $user = $security->getIdentity()->getUser();
                    $user->setResource('flch');
                    $userRepo = ServiceLayer::getService('user_repository');
                    $userRepo->save($user);
                    $security->reloadUser($user);
                }
                
                if('group' == $this->getRequest()->get('promo'))
                {
                     $user = $security->getIdentity()->getUser();
                    $user->setResource('group');
                    $userRepo = ServiceLayer::getService('user_repository');
                    $userRepo->save($user);
                    $security->reloadUser($user);
                }
                
                
                
                if(null != $request->get('inv'))
                {
                    $request->Redirect($router->link('player_dashboard', array('userid' =>  $auth_result['ID'],'inv' => $request->get('inv') )));
                }
                elseif(null != $request->get('rurl'))
                {
                    $request->Redirect($request->get('rurl'));
                }
                else 
                {
                    $request->Redirect($router->link('player_dashboard',array('userid' =>  $auth_result['ID'] )));
                }
            }
            if($auth_result['RESULT'] == 1 && $auth_result['MESSAGE'] == 'NOTE_PASSWORD_EXPIRED')
            {
                $_SESSION['expired_user_id'] = $auth_result['ID'];
                $request->Redirect($router->link('password_expired'));
            }
            if($auth_result['RESULT'] == 0)
            {
                $validator->addErrorMessage('bad_login',$translator->translate('Wrong password or username'));
            }
        }
        $layout = \Core\ServiceLayer::getService('layout');
        $layout->setTemplate(GLOBAL_DIR.'/templates/LoginLayout.php');
        return $this->render('User\UserModule:Security:login.php', ['validator' => $validator, 'promo' => $request->get('promo') ]);
    }
    
    public function logoutAction()
    {
            $request = ServiceLayer::getService('request');
            $router = ServiceLayer::getService('router');
            $security = ServiceLayer::getService('security');
            $security->unauthenticateUser();
            ServiceLayer::getService('ActualPackageManager')->removeFromSession();
            $request->Redirect($router->link('login'));
    }
    
    public function passwordLostAction()
    {
        $layout = ServiceLayer::getService('layout');
        $request = ServiceLayer::getService('request');
        $validator = new Validator();
        $translator = ServiceLayer::getService('translator');
        if('POST' == $request->getMethod())
        {

            $security = ServiceLayer::getService('security');
            $username = $request->get('username');

            $user = $security->getIdentityProvider()->findUserByUsername($username);
            
            $repository = ServiceLayer::getService('user_repository');
            
            if(null == $user)
            {
                if($username == null)
                {
                    $validator->addErrorMessage('not_exist','MSG_EMAIL_MISSING');
                }
                else
                {
                    $validator->addErrorMessage('not_exist','PASSWORD_LOST_EMAIL_MISSING');
                }
                
            }
            else
            {
                $security = ServiceLayer::getService('security');
                $password = $security->generatePassword();
                $encoded_password = $security->encodePassword($password,$user->getSalt());
                $user->setPassword($encoded_password);
                $sender = new \Core\Notifications\PasswordLostNotificationSender(new \Core\Notifications\PasswordLostStore());
                $notifyData = array();
                $notifyData['lang'] = $user->getDefaultLang();
                $notifyData['email'] =$user->getEmail();
                $notifyData['password'] = $password;
                $sender->send($notifyData);

/*
                $notification_manager = ServiceLayer::getService('notification_manager');
                $notification_repository = ServiceLayer::getService('notification_repository');
                $notification = $notification_repository->findOneBy(array('notification_type'=>'lost_password'));
                $notification->setRecipient($user->getEmail());
                
                $notification_message = $notification->getMessage();
                $notification_message = str_replace('{{PASSWORD}}',$password , $notification_message);
                $notification->setMessage($notification_message);
                
                $send_result = $notification_manager->sendNotification($notification);
 * 
 */
                $send_result['RESULT'] = 1;
                
                if($send_result['RESULT'] == 1)
                {
                                        $repository->save($user);
                    $request->addFlashMessage('password_sent', $translator->translate('New password has been sent to the email address'));
                    $request->redirect();
                }
                else
                {
                    $validator->addErrorMessage('email_not_send',$send_result['MESSAGE']);
                }
                
                
                
                
                
            }

        }
        
        $layout = \Core\ServiceLayer::getService('layout');
                $layout->setTemplate(GLOBAL_DIR.'/templates/LoginLayout.php');
        
        
        return $this->render('\User\UserModule:Security:password_lost.php',
                array(
                    'validator' => $validator,
                    'request' => $request
                ));
    }
    
    public function registerAction()
    {
        
        $layout = \Core\ServiceLayer::getService('layout');
        $layout->setTemplate(GLOBAL_DIR.'/templates/ClearLayout.php');
        
        return $this->render('PH\User\UserModule:Security:register.php',
                array(
                        
                ));
    }
    
    public function passwordChangeAction()
    {
        $request = ServiceLayer::getService('request');
        $translator = ServiceLayer::getService('translator');
        $user = ServiceLayer::getService('security')->getIdentity()->getUser();
        $repository = ServiceLayer::getService('user_repository');

        $validator = new Validator();
        
        if('POST' == $request->getMethod())
        {
            
            $data = $request->get('record');
            $new_pass = $data['new_pass'];
            $old_pass = $data['old_pass'];
            $new_pass_confirm = $data['new_pass_confirm'];
            
            $new_pass_encoded =  ServiceLayer::getService('security')->encodePassword($new_pass,$user->getSalt());
            $old_pass_encoded =  ServiceLayer::getService('security')->encodePassword($old_pass,$user->getSalt());
            

            if($user->getPassword() !=  $old_pass_encoded)
            {
                $validator->addErrorMessage('old_pass', 'MSG_PASSWORD_NOT_VALID');
            }
            
            if($new_pass != $new_pass_confirm)
            {
                $validator->addErrorMessage('new_pass_confirm', 'MSG_PASSWORD_NOT_SAME');
            }
            
            if($user->getPasswordBefore1() ==  $new_pass_encoded or $user->getPasswordBefore2() ==  $new_pass_encoded )
            {
                $validator->addErrorMessage('new_pass', 'MSG_PASSWORD_USED_BEFORE ');
            }
            if(!$validator->hasErrors())
            {
                
                $user->setPasswordBefore2($user->getPasswordBefore1());
                $user->setPasswordBefore1($user->getPassword());
                $user->setPassword($new_pass_encoded);
                $user->setPasswordChanged(new \DateTime());
                $repository->save($user);
                
                
                $request->addFlashMessage('user_edit_success', $translator->translate('Vaše heslo bolo zmenené'));
                $request->redirect();
                
            }
        }
        
        $layout = ServiceLayer::getService('layout');
        
        $layout->setTemplateParameters(array(
                'crumb' => array('nav.users.change_password' => 'password_change'))
        );
        
        return $this->render('User\UserModule:Security:password_change.php',
                array(

                    'request' => $request,
                    'validator' => $validator
                ));
    }
        
        
    
    
    public function profilAction()
    {
        $request = ServiceLayer::getService('request');
        $repository = ServiceLayer::getService('PlayerRepository');
        $translator = ServiceLayer::getService('translator');
        $security = ServiceLayer::getService('security');
        $user = $security->getIdentity()->getUser();
                $localityManager =  ServiceLayer::getService('LocalityManager');
                $sportManager = ServiceLayer::getService('SportManager');
                $sports = $sportManager->getSportFormChoices();
        
        $entity = $repository->find($user->getId());
    
                
        $form = new UserProfilForm();
        $form->setEntity($entity);
        $validator = new Validator();
        $validator->setRules($form->getFields());
                
                
        
        if('POST' == $request->getMethod())
        {
            $post_data = $request->get('record');
            $form->bindData($post_data);
            $validator->setData($post_data);
            $validator->validateData();
            
            if(!$validator->hasErrors())
            {
                $entity = $form->getEntity();
                                
                                $localityData = $request->get('locality');
                                
                                
                                 if($localityData['locality_id'] == null)
                                {
                                     //check if exist locality as json
                                    if($localityData['locality_json_data'] != null)
                                    {
                                        $locality =  $localityManager->createObjectFromJsonData($localityData['locality_json_data']);

                                        $locality->setCity($localityData['city']);
                                        $locality->setStreet($localityData['street']);
                                        $locality->setAuthorId($entity->getId());
                                        $locality->setStreetNumber($localityData['street_number']);
                                        $locality_id = $localityManager->saveLocality($locality);
                                        $entity->setLocalityId($locality_id);
                                    }
                                }

                                
                $repository->save($entity);
                $security->reloadUser($user);
                $request->addFlashMessage('user_edit_success', $translator->translate('user.crud_edit.success'));
                $request->redirect();
            }
        }
    
        
        
        return $this->render('User\UserModule:Security:profil.php',
                array(
                        'form' => $form,
                        'request' => $request,
                        'validator' => $validator,
                                                 'sports' => $sports
                ));
    }
    
    
    public function passwordExpiredAction()
    {
        $request = ServiceLayer::getService('request');
        $translator = ServiceLayer::getService('translator');
        $user = ServiceLayer::getService('security')->getIdentity()->getUser();
        $user  = ServiceLayer::getService('user_repository')->find($_SESSION['expired_user_id']);
        
        if(null == $user)
        {
            $user  = ServiceLayer::getService('partner_repository')->find($_SESSION['expired_user_id']);
        }

        if($user->getRole() == 'ROLE_PARTNER')
        {
            $repository = ServiceLayer::getService('partner_repository');
            $logger = ServiceLayer::getService('partner_logger');
            $log_data['partner_ID'] = $user->getId();
            $log_data['activity'] = 'password-expired';
            //$log_data['table_object_ID'] = $user->getId();
        }
        else
        {
            $repository = ServiceLayer::getService('user_repository');
            $logger = ServiceLayer::getService('user_logger');
            $log_data['user_ID'] = $user->getId();
            $log_data['activity'] = 'password-expired';
            $log_data['source_table'] = 'ph_users';
            $log_data['table_object_ID'] = $user->getId();
        }
        
        
        $validator = new Validator();
        
        if('POST' == $request->getMethod())
        {
            
            $data = $request->get('record');
            $new_pass = $data['new_pass'];
            $new_pass_confirm = $data['new_pass_confirm'];
            $new_pass_encoded =  ServiceLayer::getService('security')->encodePassword($new_pass,$user->getSalt());
        
            if(!$validator->validatePassword($new_pass))
            {
                $validator->addErrorMessage('pass_strength', 'MSG_PASSWORD_STRENGTH');
            }
            
            if($new_pass != $new_pass_confirm)
            {
                $validator->addErrorMessage('new_pass_confirm', 'MSG_PASSWORD_NOT_SAME');
            }
            
            if($user->getPasswordBefore1() ==  $new_pass_encoded or $user->getPasswordBefore2() ==  $new_pass_encoded )
            {
                $validator->addErrorMessage('new_pass', 'MSG_PASSWORD_USED_BEFORE ');
            }

            if(!$validator->hasErrors())
            {
                
                $user->setPasswordBefore2($user->getPasswordBefore1());
                $user->setPasswordBefore1($user->getPassword());
                $user->setPassword($new_pass_encoded);
                $user->setPasswordChanged(new \DateTime());
                $repository->save($user);
                $logger->addRecord($log_data);
                
                $request->addFlashMessage('password_sent', $translator->translate('EXPIRED_PASSWORD_UPDATED'));
                $request->redirect();
                
            }
        }
        
        $layout = ServiceLayer::getService('layout');
        $layout->setTemplate(GLOBAL_DIR.'/templates/ClearLayout.php');
        
        return $this->render('PH\User\UserModule:Security:password_expired.php',
                array(
        
                        'request' => $request,
                        'validator' => $validator
                ));
    }
    
    public function gdprAction()
    {
        $request = ServiceLayer::getService('request');
        $router = ServiceLayer::getService('router');
        $security = ServiceLayer::getService('security');
        $user = $security->getIdentity()->getUser();
        $privacyRepo = \Core\ServiceLayer::getService('PlayerPrivacyRepository');
        
        
        if ('POST' == $request->getMethod())
        {
            if (null != $request->get('vop'))
            {
                $userPrivacy = new \Webteamer\Player\Model\PlayerPrivacy();
                $userPrivacy->setSection('vop_agreement');
                $userPrivacy->setUserGroup('coachrufus');
                $userPrivacy->setPlayerId($user->getId());
                $userPrivacy->setValue('1');
                $privacyRepo->save($userPrivacy);
                $request->redirect( $router->link('gdpr_settings',array('step' => 'm','rurl' =>  $request->get('rurl'))));
                
            }
            if (null != $request->get('marketing'))
            {
                $userPrivacy = new \Webteamer\Player\Model\PlayerPrivacy();
                $userPrivacy->setSection('marketing_agreement');
                $userPrivacy->setUserGroup('coachrufus');
                $userPrivacy->setPlayerId($user->getId());
                if($request->get('marketing') == 1)
                {
                    $userPrivacy->setValue('1');
                }
                
                 if($request->get('marketing') == 2)
                {
                    $userPrivacy->setValue('2');
                }
                

                $privacyRepo->save($userPrivacy);
                $request->redirect($request->get('rurl'));
            }
        }
        
        $layout = \Core\ServiceLayer::getService('layout');
        $layout->setTemplateParameters(array('body-class' => 'full-overlay-window'));

        
        //$layout->setTemplate(GLOBAL_DIR.'/templates/BaseLayout.php');


        if($request->get('step') == 'm')
        {
            return $this->render('User\UserModule:Security:gdpr_marketing.php', array('rurl' => $request->get('rurl')));
        }
        else
        {
            return $this->render('User\UserModule:Security:gdpr_vop.php', array('rurl' => $request->get('rurl')));
        }

        
    }

}
