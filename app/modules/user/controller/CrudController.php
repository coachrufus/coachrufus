<?php
namespace PH\User\Controller;
use Core\Controller as Controller;
use Core\Request as Request;
use Core\ServiceLayer;
use Core\DatagridTable as DatagridTable;

use PH\User\Model\User as User;
use PH\User\Model\UserRepository as UserRepository;
use PH\User\Model\UserDatagrid as UserDatagrid;
use PH\User\Form\UserForm as UserForm;
use PH\User\Form\UserValidator as Validator;

class CrudController extends Controller {
	
	public function indexAction()
	{
		$repository = ServiceLayer::getService('user_repository');
		//$all = $repository->findAll();

		$layout = ServiceLayer::getService('layout');
		$layout->setTemplateParameters(array(
				'crumb' => array('nav.users.list' => 'user_list'))
		);
		
	

		//TODO - prerobit a vytesnit repository logiku z datagrid table  do repository
		$admin_table = new UserDatagrid();
		$admin_table->setRepository($repository);
		
		
		return $this->render('PH\User\UserModule:Crud:index.php',
				array(
						'admin_table' => $admin_table,
						'request' => ServiceLayer::getService('request')
				));
	}
	
	public function addAction()
	{
		$request = ServiceLayer::getService('request');
		$repository = ServiceLayer::getService('user_repository');
		$translator = ServiceLayer::getService('translator');
		$form = new UserForm();
		$form->setEntity(new User());
		$validator = new Validator();
		$validator->setRules($form->getFields());
		

		if('POST' == $request->getMethod())
		{
			$post_data = $request->get('record');
			$form->bindData($post_data);
			$validator->setData($post_data);
			$validator->validateData();
			if(!$validator->hasErrors())
			{
				$entity = $form->getEntity();
				$entity->setDateCreated(new \DateTime());
				$entity->setSalt(md5(time()));
				$security = ServiceLayer::getService('security');
				$password = $security->generatePassword();
				$encoded_password = $security->encodePassword($password,$entity->getSalt());
				$entity->setPassword($encoded_password);
				$saved_id = $repository->save($entity);
			
				//log
				$user = ServiceLayer::getService('security')->getIdentity()->getUser();
				$logger = ServiceLayer::getService('user_logger');
				$logger->addRecord(array(
						'user_ID' => $user->getId(),
						'activity' => 'add',
						'source_table' => 'ph_users',
						'table_object_ID' => $saved_id
				));
				
				//send email
				$notification_manager = ServiceLayer::getService('notification_manager');
				$notification_repository = ServiceLayer::getService('notification_repository');
				$notification = $notification_repository->findOneBy(array('notification_type'=>'new_user_1'));
				$notification->setRecipient($entity->getEmail());
				
				$notification_message = $notification->getMessage();
				$notification_message = str_replace('{{USERNAME}}',$entity->getEmail() , $notification_message);
				$notification_message = str_replace('{{PASSWORD}}',$password , $notification_message);
				$notification->setMessage($notification_message);
				$send_result = $notification_manager->sendNotification($notification);
			
			
				$request->addFlashMessage('user_add_success', $translator->translate('NOTE_CREATING_OBJECT_SUCCESS'));
				$request->redirect(ServiceLayer::getService('router')->link('user_list'));
			
			}
		}

		$layout = ServiceLayer::getService('layout');
		$layout->setTemplateParameters(array(
				'crumb' => array('nav.users.list' => 'user_list', 'nav.users.add' => null ))
		);
		
	
		
		return $this->render('PH\User\UserModule:Crud:add.php',
				array(
						'form' => $form,
						'request' => $request,
						'validator' => $validator,
				));
	}
	
	public function editAction()
	{
		$request = ServiceLayer::getService('request');
		$repository = ServiceLayer::getService('user_repository');
		$translator = ServiceLayer::getService('translator');
		
		$user = $repository->find($request->get('id'));
		$user->loadRegionIds();
		
		$form = new UserForm();
		$form->setEntity($user);
		$validator = new Validator();
		$validator->setValidEmail($user->getEmail());
		$validator->setRules($form->getFields());

		if('POST' == $request->getMethod())
		{
			$post_data = $request->get('record');
			$form->bindData($post_data);
			$validator->setData($post_data);
			$validator->validateData();
			if(!$validator->hasErrors())
			{
				$entity = $form->getEntity();
				
				$repository->save($entity);
				$user = ServiceLayer::getService('security')->getIdentity()->getUser();
				$logger = ServiceLayer::getService('user_logger'); 
				$logger->addRecord(array(
						'user_ID' => $user->getId(),
						'activity' => 'edit',
						'source_table' => 'ph_users',
						'table_object_ID' => $entity->getId()
				));
				
				
				$request->addFlashMessage('user_edit_success', $translator->translate('NOTE_DATA_UPDATED'));
				$request->redirect();
			}
		}
	
		$layout = ServiceLayer::getService('layout');
		$layout->setTemplateParameters(array(
				'crumb' => array('nav.users.list' => 'user_list', 'nav.users.edit' => null ))
		);
		

		return $this->render('PH\User\UserModule:Crud:edit.php',
				array(
						'form' => $form,
						'request' => $request,
						'validator' => $validator,
				));
	}
	
	public function detailAction()
	{
		$request = ServiceLayer::getService('request');
		$repository = ServiceLayer::getService('user_repository');
		$demand_repository = ServiceLayer::getService('demand_repository');
		$user = $repository->find($request->get('id'));
		$demands = $demand_repository->getUserDemands($user);
		$logs = $repository->getUserActivity($user);
		
		$layout = ServiceLayer::getService('layout');
		$layout->setTemplateParameters(array(
				'crumb' => array('nav.users.list' => 'user_list', 'nav.users.detail' => null ))
		);
		



		
		return $this->render('PH\User\UserModule:Crud:detail.php',
				array(
						'user' => $user,
						'demands' => $demands,
						'logs' => $logs
				));
	}
}