<?php
namespace User\Controller;

use Core\Controller as Controller;
use Core\ServiceLayer;


class CronController extends Controller {
	
    public function mailchimpExportAction()
    {
        $this->exportUsers('mailchimp_export');
         $this->exportUsers('mailchimp_marketing_export');
    }
    
    private function exportUsers($type = 'mailchimp_export')
    {
         ServiceLayer::getService('layout')->setTemplate(null);
       $queueRepo  = ServiceLayer::getService('QueueRepository');
       $export = $queueRepo->getWaitingRecords($type);
       
     
       foreach($export as $qData)
       {

            $userRepo = ServiceLayer::getService('user_repository');
            $user = $userRepo->find($qData['value']);
            
            if(null != $user)
            {
                $mailchimpListId = '967dfc9f8f';
                if($user->getResource() == 'flch')
                {
                     $mailchimpListId = '4ecbb36ada';
                }
                
                if($type == 'mailchimp_marketing_export')
                {
                    $mailchimpListId = '8a65c3b61f';
                    //APPRDATE
                }
                
                if(null !=  $qData['data'])
                {
                    $qInfo = json_decode($qData['data'],true);
                    if(array_key_exists('listId', $qInfo))
                    {
                        $mailchimpListId = $qInfo['listId'];
                    }
                }

                
                $MailChimp = ServiceLayer::getService('MailchimpManager');
                $logger = ServiceLayer::getService('system_logger');
                $response = $MailChimp->post('lists/'.$mailchimpListId.'/members',array(
                    'email_address'=> $user->getEmail(),
                    'language' => $user->getDefaultLang(), 
                    'status' => 'subscribed', 
                    'merge_fields' => array('APPRDATE'=>date('m/d/Y')),
                    ));
                $log_data = array();
                if($response['status'] == 400)
                {
                    $log_data['activity'] = 'mailchimp_export_error';
                    $log_data['detail'] = 'Error export email '.$user->getEmail().',DETAIL:'.$response['detail '];
                }
                else
                {
                    $log_data['activity'] = 'mailchimp_export_succes';
                    $log_data['detail'] = 'export email '.$user->getEmail().',ID:'.$response['id'].',status'.$response['status'];
                }
                $logger->addSystemRecord($log_data);
            }
            $queueRepo->finishRecord($qData['id']);

       }
    }
}