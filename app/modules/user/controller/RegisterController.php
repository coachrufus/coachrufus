<?php

namespace User\Controller;

use Core\Controller as Controller;
use Core\ServiceLayer;
use User\Form\RegisterForm as RegisterForm;
use User\Handler\RegisterHandler as RegisterHandler;
use User\Handler\StepRegisterHandler;
use Core\GUID;

class RegisterController extends Controller {

    public function preExecute()
    {
        $layout = \Core\ServiceLayer::getService('layout');
        $layout->setTemplate(GLOBAL_DIR.'/templates/LoginLayout.php');
    }
    
    public function simpleAction()
    {
        $request = ServiceLayer::getService('request');
        $register_form = new RegisterForm();
        $translator = ServiceLayer::getService('translator');
        $translator->setLang($request->get('lang'));
        $router = ServiceLayer::getService('router');

        $register_handler = new RegisterHandler();

        if ($request->getMethod() == 'POST')
        {
            $post_data = $request->get('record');
            $register_form->bindData($post_data);

            $register_handler->setPostData($post_data);
            $register_handler->sanitizeData();
            $validate_result = $register_handler->validateData();
            if ($validate_result)
            {
                $register_handler->persistData($post_data);
                $register_handler->sendRegisterEmail($request->get('lang'));

                $request->addFlashMessage('register_success', $translator->translate('Vaša registrácia bola úspešna. Môžete sa prihlásiť údajmi ktoré sta zadali pri registrácii'));
                $request->redirect($router->link('login', array('lang' => $request->get('lang'))));
            }
        }
        return $this->render('User\UserModule:Register:simple.php', array(
                    'form' => $register_form,
                    'validator' => $register_handler->getValidator('base')
        ));
    }
    
    private function buildRegisterForm()
    {
        $translator = ServiceLayer::getService('translator');
        $sportManager = ServiceLayer::getService('SportManager');
        $playerManager = ServiceLayer::getService('PlayerManager');
         
        $register_form = new RegisterForm();
        
        
        $sportChoices =  array('' => $translator->translate('Sport'));
        foreach($sportManager->getSportFormChoices() as $sportChoiceId => $sportChoiceName)
        {
            $sportChoices[$sportChoiceId] = $translator->translate($sportChoiceName);
        }
        
        $register_form->setFieldChoices('sport_id',$sportChoices);
        $register_form->setFieldChoices('sport_level', array('' => $translator->translate('Level'))+$playerManager->getLevelFormChoices());

        $register_form->setFieldChoices('birthday', array('' => $translator->translate('Day')) + $register_form->getFieldChoices('birthday'));
        $register_form->setFieldChoices('birthmonth', array('' => $translator->translate('Month')) + $register_form->getFieldChoices('birthmonth'));
        $register_form->setFieldChoices('birthyear', array('' => $translator->translate('Year')) + $register_form->getFieldChoices('birthyear'));
        $register_form->setFieldChoices('timezone', array('' => $translator->translate('Timezone')) + $register_form->getFieldChoices('timezone'));
        $register_form->setFieldChoices('dateformat', array('' => $translator->translate('Date format')) + $register_form->getFieldChoices('dateformat'));
        $register_form->setFieldChoices('timeformat', array('' => $translator->translate('Time format')) + $register_form->getFieldChoices('timeformat'));
        
        $countryRepo = ServiceLayer::getService('CountryRepository');
        $country = $countryRepo->findAll();
        $countryChoices = array();
        foreach($country as $c)
        {
            if($translator->getLang() == 'sk')
            {
                $countryChoices[$c->getCountryCode()] = $c->getSkName();
            }
            else
            {
                $countryChoices[$c->getCountryCode()] = $c->getEnName();
            }
            
        }
        $register_form->setFieldChoices('country', array('' => $translator->translate('Select country')) +$countryChoices);

        $register_form->setFieldChoices('unitsystem', array(
            '' => $translator->translate('Unitsystem'),
            'metric' => $translator->translate('Metric (kilometer)'),
            'imperial' => $translator->translate('Imperial (miles)')
        ));
        
        $register_form->setFieldChoices('gender', array(
            '' => $translator->translate('Gender'),
            'male' => $translator->translate('Male'),
            'female' => $translator->translate('Female')
        ));
        
         
        $register_form->setFieldChoices('default_lang', array(
            '' => $translator->translate('Language'),
            'sk' => $translator->translate('Slovak'),
            'en' => $translator->translate('English')
        ));
        
        $register_form->setFieldChoices('week_start', array(
            '' => $translator->translate('Week start'),
            'monday' => $translator->translate('Monday'),
            'sunday' => $translator->translate('Sunday')
        ));
        
        if($translator->getLang() == 'sk')
        {
            $register_form->setFieldValue('default_lang', 'sk');
        }
        else
        {
            $register_form->setFieldValue('default_lang', 'en');
        }
            
            
        
        /*
        $ip = $_SERVER['REMOTE_ADDR'];
        $details = json_decode(file_get_contents("http://ipinfo.io/".$ip."/json"));
        
        if(null != $details)
        {
            $register_form->setFieldValue('country', $details->country);
            if($translator->getLang() == 'sk')
            {
                $register_form->setFieldValue('default_lang', 'sk');
            }
            else
            {
                $register_form->setFieldValue('default_lang', 'en');
            }

            $location = $details->loc;
            $locationInfo = explode(',',$location);
           
            $countryInfo = json_decode(file_get_contents("http://api.geonames.org/timezoneJSON?lat=".$locationInfo[0]."&lng=".$locationInfo[1]."&username=coachrufus"));

            
            if(null != $countryInfo)
            {
                 $register_form->setFieldValue('timezone', $countryInfo->timezoneId);
            }
            
        }
        
       */
        

       
        
        return $register_form;
    }
    
    public function flchStartAction()
    {
        //set cookie
        //setcookie('promo', 'flch',time()+(60*60*24*90),'/');
        $_GET['promo']  = 'flch';
        $response = $this->startAction();
        return $response;
    }
    
     public function promoStartAction()
    {
        //set cookie
        //setcookie('promo', 'flch',time()+(60*60*24*90),'/');
        $_GET['promo']  = 'group';
        $response = $this->startAction();
        return $response;
    }


    public function startAction()
    {
        $request = ServiceLayer::getService('request');
        $_SESSION['app_lang'] = $request->get('lang');
        $translator = ServiceLayer::getService('translator');
        $translator->setLang($request->get('lang'));
        
        $register_form = $this->buildRegisterForm();
        
        if(null != $request->get('e'))
        {
            $register_form->setFieldValue('email', $request->get('e'));
        }
        
        //add condition for people who had come from flch promo page
        /*
        if(isset($_COOKIE['promo']) && $_COOKIE['promo'] == 'flch')
        {
             $_GET['promo']  = 'flch';
             setcookie('promo', 'flch',time()-3600,'/');
        }
        */
        
        $router = ServiceLayer::getService('router');
        //if invitation
        if(array_key_exists('invitation', $_SESSION) && $request->get('inv') == null)
        {
           $request->redirect($router->link('register_start',array(
                'lang' => $request->get('lang'),
                'inv' => $_SESSION['invitation']['hash'],
                'tp' => $_SESSION['invitation']['tp'],
                'h' =>$_SESSION['invitation']['h']   )));
        }
        

        $register_handler = new StepRegisterHandler();
        
        if ($request->getMethod() == 'POST')
        {
            $post_data = $request->get('record');
            $register_form->bindData($post_data);

            $register_handler->setPostData($post_data);
            $register_handler->sanitizeData();
            $validate_result = $register_handler->validateStart();
           if ($validate_result)
            {
                $activationGuid = GUID::create();
                $post_data['activation_guid'] = $activationGuid;
                $post_data['status'] = 1;
                $register_handler->persistStartData($post_data);
                $security = ServiceLayer::getService('security');
                $auth_result = $security->authenticateUser($post_data['email'], $post_data['password']);
                
                $queueRepo  = ServiceLayer::getService('QueueRepository');
                $queueRepo->addRecord(array('action_key' => 'user_id','value' => $auth_result['ID'],'action_type' => 'mailchimp_export'));
                
                /*
                if(array_key_exists('marketing_agreement', $post_data) && '1' == $post_data['marketing_agreement'])
                {
                    //APPRDATE
                     $queueRepo->addRecord(
                             array(
                                 'action_key' => 'user_id',
                                 'value' => $auth_result['ID'],
                                 'action_type' => 'mailchimp_marketing_export'));
                }
                */
                
                
                //add user to email marketing
                $user = ServiceLayer::getService('user_repository')->findOneBy(array('id' => $auth_result['ID']));
                $emailMarketingManager = \Core\ServiceLayer::getService('EmailMarketingManager');
                $emailMarketingManager->createUser($user);
                $emailMarketingManager->addUserTag($user,'user_noteam');
                 
                if(null !=  $post_data['inv_hash'])
                {
                    $request->Redirect($router->link('player_start', array('userid' =>  $auth_result['ID'],'inv' =>  $post_data['inv_hash'],'tp' =>  $post_data['tp'],'h' => $post_data['h'])));
                }
                else 
                {
                    $request->Redirect($router->link('player_start',array('userid' =>  $auth_result['ID'] )));
                }
                
                
                
                //login and forward to app
                //$register_handler->sendRegisterEmail($request->get('lang'), $activationGuid);
                //$request->redirect($router->link('register_start_finish', array('lang' => $request->get('lang'))));
            }
        }
        
        $conditionsLink = 'http://www.coachrufus.com/terms-and-conditions/';
        if('en' == $_SESSION['app_lang'])
        {
             $conditionsLink = 'http://www.coachrufus.com/en/terms-and-conditions/';
        }

        return $this->render('User\UserModule:Register:start.php', array(
                    'form' => $register_form,
                    'validator' => $register_handler->getValidator('start'),
                    'invHash' => $request->get('inv'),
                    'tp' => $request->get('tp'),
                    'h' => $request->get('h'),
                    'promo' => $request->get('promo'),
                    'conditionsLink' => $conditionsLink
        ));
    }
    
    public function startFinishAction()
    {
         return $this->render('User\UserModule:Register:start-finish.php', array(

        ));
    }
    
    public function step1Action()
    {
        $request = ServiceLayer::getService('request');
        $translator = ServiceLayer::getService('translator');
        $register_form = $this->buildRegisterForm();
       
        $translator->setLang($request->get('lang'));
        $router = ServiceLayer::getService('router');

        $register_handler = new StepRegisterHandler();
        $data = $register_handler->getRegisterdData();
        
        $this->checkRegisterProcess($data);

        if(array_key_exists('step1',$data))
        {
             $register_form->bindData($data['step1']);
        }

        if ($request->getMethod() == 'POST')
        {
            $post_data = $request->get('record');
            $register_form->bindData($post_data);

            $register_handler->setPostData($post_data);
            $register_handler->sanitizeData();
            $validate_result = $register_handler->validateStep1();
            if ($validate_result)
            {
                
                $register_handler->persistStep1Data($post_data);
                $request->redirect($router->link('register_step2', array('lang' => $request->get('lang'))));
            }
        }

        return $this->render('User\UserModule:Register:step1.php', array(
                    'form' => $register_form,
                    'validator' => $register_handler->getValidator('step1')
        ));
    }
    
    public function step2Action()
    {
        $request = ServiceLayer::getService('request');
       
        $translator = ServiceLayer::getService('translator');
        $register_form = $this->buildRegisterForm();
       
        $translator->setLang($request->get('lang'));
        $router = ServiceLayer::getService('router');

        $register_handler = new StepRegisterHandler();
        $data = $register_handler->getRegisterdData();
         $this->checkRegisterProcess($data);
        
        
        //$ip = '189.27.42.54';
        $ip = $_SERVER['REMOTE_ADDR'];
        $details = json_decode(file_get_contents("http://ipinfo.io/".$ip."/json"));
        t_dump($details);
        if(null != $details)
        {
            $location = $details->loc;
            $locationInfo = explode(',',$location);
            $countryInfo = json_decode(file_get_contents("http://api.geonames.org/timezoneJSON?lat=".$locationInfo[0]."&lng=".$locationInfo[1]."&username=coachrufus"));

            if(null != $countryInfo)
            {
                 $register_form->setFieldValue('timezone', $countryInfo->timezoneId);
            }
            
        }
        
        
        
        
        
        if(array_key_exists('step2',$data))
        {
             $register_form->bindData($data['step2']);
        }
       

        
        if ($request->getMethod() == 'POST')
        {
            $post_data = $request->get('record');
            $register_form->bindData($post_data);

            $register_handler->setPostData($post_data);
            $register_handler->sanitizeData();
            $validate_result = $register_handler->validateStep2();
            if ($validate_result)
            {
                
                $register_handler->persistStep2Data($post_data);
                $request->redirect($router->link('register_step3', array('lang' => $request->get('lang'))));
            }
        }

        return $this->render('User\UserModule:Register:step2.php', array(
                    'form' => $register_form,
                    'validator' => $register_handler->getValidator('step2')
        ));
    }
    
    public function activationAction()
    {
        $request = ServiceLayer::getService('request');
        $translator = ServiceLayer::getService('translator');
        $router = ServiceLayer::getService('router');
        $security = ServiceLayer::getService('security');
        $userRepository = ServiceLayer::getService('user_repository');
        $user = $userRepository->findOneBy(array('activation_guid' => $request->get('code')));
        
        
        
            
        if(null != $user)
        {
            $register_handler = new StepRegisterHandler();
            $user->setStatus(1);
            $userRepository->save($user);
           
            
            
            //if team player
            $startData = array();
            if(null !=  $request->get('inv'))
            {
                $startData['inv_hash'] = $request->get('inv');
            }
            if(null !=  $request->get('tp'))
            {
                $startData['tp'] = $request->get('tp');
            }
             if(null !=  $request->get('h'))
            {
                $startData['h'] = $request->get('h');
            }
            
            if(!empty($startData))
            {
                $register_handler->saveToSession('start', $startData);
            }
            
            $register_handler->persistActivationData(array('activation_guid' => $request->get('code')));
            $security->authenticateUserEntity($user);
            
            $queueRepo  = ServiceLayer::getService('QueueRepository');
            $queueRepo->addRecord(array('action_key' => 'user_id','value' => $user->getId(),'action_type' => 'mailchimp_export' ));
            
            

            $request->addFlashMessage('REGISTER_ACTIVATION_SUCCESS',  $translator->translate('REGISTER_ACTIVATION_SUCCESS'));
            $request->redirect($router->link('register_step1', array('lang' => $request->get('lang'))));
        }
        else
        {
            return $this->render('User\UserModule:Register:activate-fail.php', array(
                   
            ));
        }
    }
    
     public function activationEmailAction()
    {
        $request = ServiceLayer::getService('request');
        $translator = ServiceLayer::getService('translator');
        $blacklist = ServiceLayer::getService('NotificationBlacklist');
        $translator->setLang($request->get('lang'));

        $userRepository = ServiceLayer::getService('user_repository');
        $user = $userRepository->findOneBy(array('activation_guid' => $request->get('code')));
        if(null != $user)
        {
             $blacklist->remove($user->getEmail());
             
              $queueRepo  = ServiceLayer::getService('QueueRepository');
            $queueRepo->addRecord(array('action_key' => 'user_id','value' => $user->getId(),'action_type' => 'mailchimp_export' ));
            
            $request->addFlashMessage('REGISTER_ACTIVATION_EMAIL_SUCCESS',  $translator->translate('REGISTER_ACTIVATION_EMAIL_SUCCESS'));
             return $this->render('User\UserModule:Register:activate-email-success.php', array(
                   
            ));
        }
        else
        {
            $request->addFlashMessage('REGISTER_ACTIVATION_EMAIL_FAIL',  $translator->translate('REGISTER_ACTIVATION_EMAIL_FAIL'));
            return $this->render('User\UserModule:Register:activate-email-fail.php', array(
                   
            ));
        }
    }
    
    
    public function step3Action()
    {
        $request = ServiceLayer::getService('request');
        $security = ServiceLayer::getService('security');
        $translator = ServiceLayer::getService('translator');
        $register_form = $this->buildRegisterForm();
       
        $translator->setLang($request->get('lang'));
        $router = ServiceLayer::getService('router');

        $register_handler = new StepRegisterHandler();
        $register_data = $register_handler->getRegisterdData();
         $this->checkRegisterProcess($register_data);
        if(array_key_exists('step3',$register_data))
        {
             $register_form->bindData($register_data['step3']);
        }

        if ($request->getMethod() == 'POST')
        {
            $post_data = $request->get('record');
            $register_form->bindData($post_data);
            $register_handler->setPostData($post_data);
            $register_handler->sanitizeData();
            $validate_result = $register_handler->validateStep3();
            if ($validate_result)
            {
                $register_handler->persistStep3Data($post_data);
                //$activationGuid = GUID::create();
                $register_handler->persistPostActivationData();
                
                $security->reloadUser($security->getIdentity()->getUser());
                
                 //check if invitation
                if(null != $register_data['start']['inv_hash'])
                {
                    
                    $redirectData =  array('inv' => $register_data['start']['inv_hash']);
                    if(null != $register_data['start']['tp'])
                    {
                        $redirectData['tp'] = $register_data['start']['tp'];
                    }
                    if(null != $register_data['start']['h'])
                    {
                        $redirectData['h'] = $register_data['start']['h'];
                    }
                    
                    $redirectData['userid'] = $security->getIdentity()->getUser()->getId();
                    
                    $redirectUrl = $router->link('player_start', $redirectData);
                }
                else
                {
                    $redirectUrl = $router->link('player_start',array('userid' => $security->getIdentity()->getUser()->getId()));
                }
               
                //activation_guid
                //$register_data
                
               // $security->authenticateUser($register_data['start']['email'],$register_data['start']['password']);
                //$register_handler->sendRegisterEmail($request->get('lang'), $activationGuid);
                $request->redirect($redirectUrl);
            }
        }

        return $this->render('User\UserModule:Register:step3.php', array(
                    'form' => $register_form,
                    'validator' => $register_handler->getValidator('step3')
        ));
    }
    
   
    
    public function checkRegisterProcess($data)
    {
         $request = ServiceLayer::getService('request');
         $router = ServiceLayer::getService('router');
        if(!array_key_exists('start', $data))
        {
            $request->redirect($router->link('register_start',array('lang' => $_SESSION['app_lang'])));
        }
    }

}
