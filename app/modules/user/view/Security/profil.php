<?php if ($request->hasFlashMessage('user_edit_success')): ?>
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <?php echo $request->getFlashMessage('user_edit_success') ?>
    </div>
<?php endif; ?>

<?php Core\Layout::getInstance()->startSlot('crumb') ?>
<li><?php echo $translator->translate('Profil') ?></li>
<?php Core\Layout::getInstance()->endSlot('crumb') ?>


<form action="" class="form-horizontal form-bordered" method="post">
    <?php echo $validator->showAllErrors() ?>

    <div class="form-group">
        <label class="col-sm-3 control-label"><?php echo $form->getFieldLabel('nickname') ?></label>
        <div class="col-sm-6">
            <?php echo $form->renderInputTag('nickname', array('class' => 'form-control', 'placeholder' => 'Nickname')) ?>
            <?php echo $validator->showError("nickname", $translator->translate('validator.required')) ?>
        </div>
    </div>




    <div class="form-group">
        <label class="col-sm-3 control-label"><?php echo $form->getFieldLabel('locality') ?></label>
        <div class="col-sm-6">

            
            <input type="hidden" name="locality[locality_json_data]" value="" id="locality_json_data" />
            <input type="hidden" name="locality[locality_id]" value="<?php echo $form->getEntity()->getLocalityId() ?>" id="locality_id" />
            <input type="hidden" name="locality[name]" value="" id="locality_name" />
            <input type="hidden" name="locality[city]" value="" id="locality_city" />
            <input type="hidden" name="locality[street]" value="" id="locality_street" />
            <input type="hidden" name="locality[street_number]" value="" id="locality_street_number" />
            
           <?php if(null ==  $form->getEntity()->getLocalityId()): ?>
            
             <input id="pac-input" class="controls" type="text" placeholder="Názov obce, ulica atd ">
            <div id="map_wrap">
                <div id="map"></div>
            </div>
        <?php else: ?>
             <?php echo $form->getEntity()->getLocalityName() ?>
        <?php endif; ?>
            
        </div>
    </div>

    



    <div class="form-group">
        <label class="col-sm-3 control-label"><?php echo $form->getFieldLabel('name') ?></label>
        <div class="col-sm-6">
            <?php echo $form->renderInputTag('name', array('class' => 'form-control', 'placeholder' => 'Help Text')) ?>
            <?php echo $validator->showError("name", $translator->translate('validator.required')) ?>
        </div>
    </div>


    <div class="form-group">
        <label class="col-sm-3 control-label">Surname</label>
        <div class="col-sm-6">
            <?php echo $form->renderInputTag('surname', array('class' => 'form-control')) ?>
            <?php echo $validator->showError("surname", $translator->translate('validator.required')) ?>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label"><?php echo $form->getFieldLabel('email') ?></label>
        <div class="col-sm-6">
            <?php echo $form->renderInputTag('email', array('class' => 'form-control', 'placeholder' => 'Email')) ?>
            <?php echo $validator->showError("email", $translator->translate('validator.required')) ?>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label"><?php echo $form->getFieldLabel('phone') ?></label>
        <div class="col-sm-6">
            <?php echo $form->renderInputTag('phone', array('class' => 'form-control')) ?>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label"><?php echo $translator->translate('Birthdate') ?></label>
        <div class="col-sm-6">
            <div class="row">
                <div class="col-sm-4">
                    <?php echo $form->getFieldLabel('birthyear') ?>
                    <?php echo $form->renderInputTag('birthyear', array('class' => 'form-control', 'placeholder' => $form->getFieldHelp('birthyear'))) ?>
                </div>
                <div class="col-sm-4">
                    <?php echo $form->getFieldLabel('birthmonth') ?>
                    <?php echo $form->renderInputTag('birthmonth', array('class' => 'form-control', 'placeholder' => $form->getFieldHelp('birthmonth'))) ?>
                </div>
                <div class="col-sm-4">
                    <?php echo $form->getFieldLabel('birthday') ?>
                    <?php echo $form->renderInputTag('birthday', array('class' => 'form-control', 'placeholder' => $form->getFieldHelp('birthday'))) ?>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label"><?php echo $form->getFieldLabel('level') ?></label>
        <div class="col-sm-6">
            <?php echo $form->renderSelectTag('level', array('class' => 'form-control')) ?>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label"><?php echo $form->getFieldLabel('player_status') ?></label>
        <div class="col-sm-6">
            <?php echo $form->renderSelectTag('player_status', array('class' => 'form-control')) ?>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label"><?php echo $form->getFieldLabel('gender') ?></label>
        <div class="col-sm-6">
            <?php echo $form->renderSelectTag('gender', array('class' => 'form-control')) ?>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label"><?php echo $form->getFieldLabel('default_lang') ?></label>
        <div class="col-sm-6">
            <?php echo $form->renderSelectTag('default_lang', array('class' => 'form-control')) ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label"><?php echo $form->getFieldLabel('receive_notifications') ?></label>
        <div class="col-sm-6">
            <?php echo $form->renderCheckboxTag('receive_notifications', array('class' => 'form-control')) ?>
        </div>
    </div>





    <div class="row">
        <div class="col-sm-6 col-sm-offset-3">
            <button class="btn btn-primary"><?php echo $translator->translate('BTN_SAVE') ?></button>

        </div>
    </div>

</form>

<?php  $layout->startSlot('javascript') ?>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXU1duK54f1gIsRy_xc7z4uQURovsZdE8&libraries=places&callback=UserLocality" async defer></script>
<script type="text/javascript">
    
    
   
</script>
<?php  $layout->endSlot('javascript') ?>