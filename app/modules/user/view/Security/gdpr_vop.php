<div id="full-window">
    <div class="full-window-cnt">
    <div class="row">
        <div class="col-sm-12">
            <?php echo $translator->translate('gdpr.vop.text') ?>
            <form action="<?php echo $router->link('gdpr_settings') ?>" method="post">
                  <input type="hidden" name="rurl" value="<?php echo $rurl ?>" />
                <input type="hidden" name="vop" value="1" />
                <button class="btn btn-primary" type="submit"><?php echo $translator->translate('gdpr.vop.agree') ?></button>
            </form>

        </div>
    </div>
    </div>
</div>