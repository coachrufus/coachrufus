<div id="full-window">
    <div class="full-window-cnt">
    <div class="row">
        <div class="col-sm-12">
            <?php echo $translator->translate('gdpr.marketing.text') ?>
            <form action="<?php echo $router->link('gdpr_settings') ?>" method="post">
                <input type="hidden" name="marketing" value="1" />
                 <input type="hidden" name="rurl" value="<?php echo $rurl ?>" />
                <button class="btn btn-primary" type="submit"><?php echo $translator->translate('gdpr.marketing.agree') ?></button>
                <button name="marketing" value="2" class="btn btn-primary" type="submit"><?php echo $translator->translate('gdpr.marketing.skip') ?></button>
                
            </form>

        </div>
    </div>
    </div>
</div>