  
  <?php if($request->hasFlashMessage('password_sent')): ?>
<div class="alert alert-success">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<?php echo $request->getFlashMessage('password_sent') ?>
	
	<script type="text/javascript">
		setTimeout("location.href='<?php echo $router->link('login') ?>'", 2000);

	</script>
</div>
<?php else: ?>
  
  				<form method="post" action="" class="col-md-5">
  				Vaše heslo expirovalo, zadajte nové heslo
                  
                    <?php if($validator->hasError('password')): ?>
                    	<div class="alert alert-danger">
			                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			               <?php echo $translator->translate($validator->getErrorMessage('not_exist')) ?>
			            </div>
                    <?php endif;?>
                  
                 <input name="record[new_pass]" type="text" class="form-control uname" placeholder="<?php echo $translator->translate('form.new_password') ?>" />
                 <input name="record[new_pass_confirm]" type="text" class="form-control uname" placeholder="<?php echo $translator->translate('form.new_password_confirm') ?>" />
                 <?php echo $validator->showError("new_pass") ?>
                 <?php echo $validator->showError("new_pass_confirm") ?>
                 <?php echo $validator->showError("pass_strength") ?>
                  
	
                    <button class="btn btn-success btn-block"><?php echo $translator->translate('BTN_SAVE') ?></button>
                 
                </form>
                
<?php endif; ?>