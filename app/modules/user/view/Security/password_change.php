<?php if($request->hasFlashMessage('user_edit_success')): ?>
<div class="alert alert-success">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<?php echo $request->getFlashMessage('user_edit_success') ?>
</div>
<?php endif; ?>

<form action="" class="form-horizontal form-bordered" method="post">
<?php echo $validator->showAllErrors() ?>

	<div class="form-group">
		<label class="col-sm-3 control-label"><?php echo $translator->translate('Súčasné heslo')?></label>
		<div class="col-sm-5">
			<input type="password" name="record[old_pass]" class="form-control" />
			<?php echo $validator->showError("old_pass") ?>
		</div>
	</div>
	

	<div class="form-group">
		<label class="col-sm-3 control-label"><?php echo $translator->translate('Nové heslo')?></label>
		<div class="col-sm-5">
			<input type="password" name="record[new_pass]" class="form-control" />
			<?php echo $validator->showError("new_pass") ?>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-3 control-label"><?php echo $translator->translate('Zopakujte nové heslo')?></label>
		<div class="col-sm-5">
			<input type="password" name="record[new_pass_confirm]" class="form-control" />
			<?php echo $validator->showError("new_pass_confirm") ?>
		</div>
	</div>
	
	
	

	<div class="row">
		<div class="col-sm-6 col-sm-offset-3">
			<button class="btn btn-primary"><?php echo $translator->translate('Uložiť')?></button>
			
		</div>
	</div>

</form>