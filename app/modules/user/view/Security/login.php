<?php if ($request->hasFlashMessage('register_success')): ?>
    <div class="row" style="margin-top:20px;">
        <div class="col-md-6 col-md-offset-3">
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $request->getFlashMessage('register_success') ?>
            </div>
        </div>
    </div>
<?php endif; ?>


<div class="login-box">

     <?php $layout->includePart(MODUL_DIR.'/user/view/_logo.php') ?>
    <div class="login-box-body">
        
         <div class="row register-lang">
            <div class="col-xs-4"></div>
            <div class="col-xs-2">
                <a class="<?php echo ($_SESSION['app_lang'] == 'en') ? 'active' : '' ?>" href="<?php echo $router->link('login',array('lang' => 'en')) ?>">EN</a>
            </div>
            <div class="col-xs-2">
                <a class="<?php echo ($_SESSION['app_lang'] == 'sk') ? 'active' : '' ?>" href="<?php echo $router->link('login',array('lang' => 'sk')) ?>">SK</a>
            </div>
        </div>
        
        
        <p class="login-box-msg"><?php echo $translator->translate('Sign in') ?></p>
        
          <div class="social-auth-links text-center">
            
            <?php echo Core\WebApp::getInstance()->renderController('Social\SocialModule:Facebook:login') ?>
            <?php echo Core\WebApp::getInstance()->renderController('Social\SocialModule:Google:login') ?>
              <p class="or-divide">- OR -</p>
        </div><!-- /.social-auth-links -->
        

        <?php if ($validator->hasError('bad_login')): ?>
            <div class="alert alert-danger">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $translator->translate($validator->getErrorMessage('bad_login')) ?>
            </div>
        <?php endif; ?>
        <form action="#" method="post">
            <input type="hidden" name="inv" value="<?php echo $request->get('inv') ?>" />
            <input type="hidden" name="promo" value="<?php echo $request->get('promo') ?>" />
           
            
            
            <div class="form-group has-feedback">
                  <div class="input-group">
                    <div class="input-group-addon">
                         <i class="ico ico-user"></i>
                    </div>
                   <input type="email"  name="username" class="form-control" placeholder="<?php echo $translator->translate('E-mail') ?>">
                </div>
            </div>
            <div class="form-group has-feedback">
                 <div class="input-group">
                    <div class="input-group-addon">
                        <i class="ico ico-lock"></i>
                    </div>
                   <input type="password" name="password" class="form-control" placeholder="<?php echo $translator->translate('Password') ?>">
                </div>
                
                
                 
            </div>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <div class="checkbox icheck">
                        <label>
                            <input name="remember" value="1" type="checkbox"> <span class="remember-text"><?php echo $translator->translate('Remember Me') ?></span>
                        </label>
                    </div>
                </div><!-- /.col -->
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-primary btn-block btn-flat bg_orange"><?php echo $translator->translate('Sign In ') ?>    </button>
                </div><!-- /.col -->
            </div>
        </form>

      
        <div class="row login-footer">
            <div class="col-xs-6"><a href="<?php echo $router->link('register_start', array('lang' => 'sk','promo' => $promo)) ?>"><?php echo $translator->translate('New account') ?></a></div>
            <div class="col-xs-6 text-right"><a href="<?php echo $router->link('password_lost') ?>"><?php echo $translator->translate('I forgot my password') ?></a></div>
        </div>


        

    </div><!-- /.login-box-body -->
</div><!-- /.login-box -->









