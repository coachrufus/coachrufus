<div class="login-box">
    <?php $layout->includePart(MODUL_DIR.'/user/view/_logo.php') ?>
    <div class="login-box-body">
        <p class="login-box-msg"><?php echo $translator->translate('Password lost') ?></p>





        <div class="form-box text-center" id="login-box">
            
            <?php if ($request->hasFlashMessage('password_sent')): ?>
                <div class="alert alert-success">
                    <?php echo $request->getFlashMessage('password_sent') ?>

                </div>
                <a class="btn btn-primary" href="<?php echo $router->link('login') ?>"><?php echo $translator->translate('Sign in') ?></a>
            <?php else: ?>

                <form action="" method="post">
                    <div class="body">



                        <?php if ($validator->hasError('not_exist')): ?>
                            <div class="alert alert-danger">
                                <i class="fa fa-ban"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <?php echo $translator->translate($validator->getErrorMessage('not_exist')) ?>
                            </div>
                        <?php endif; ?>
                        <?php if ($validator->hasError('email_not_send')): ?>
                            <div class="alert alert-danger">
                                <i class="fa fa-ban"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <?php echo $translator->translate($validator->getErrorMessage('email_not_send')) ?>
                            </div>
                        <?php endif; ?>



                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">
                                     <i class="ico ico-user"></i>
                                </div>
                               <input type="email"  name="username" class="form-control" placeholder="<?php echo $translator->translate('E-mail') ?>">
                            </div>
                            
                           
                        </div>


                    </div>
                    <div class="footer">                                                               
                       <button type="submit" class="btn btn-primary btn-block btn-flat bg_orange"><?php echo $translator->translate('Send') ?></button>  
                    </div>


                </form>
                
                <div class="row login-footer">
                    <div class="col-xs-6 text-left"><a href="<?php echo $router->link('register_start', array('lang' => 'sk','promo' => $promo)) ?>"><?php echo $translator->translate('New account') ?></a></div>
                    <div class="col-xs-6 text-right">
                    <a  href="<?php echo $router->link('login', array('lang' => 'sk','inv' => $invHash,'promo' => $promo)) ?>"><?php echo $translator->translate('Login') ?></a>
                    </div>
                </div>

            <?php endif; ?>

        </div> 

    </div>
</div>






