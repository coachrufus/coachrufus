<?php if($request->hasFlashMessage('user_add_success')): ?>
<div class="alert alert-success">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<?php echo $request->getFlashMessage('user_add_success') ?>
</div>
<?php endif; ?>

<form action="" class="form-horizontal form-bordered" method="post">
<div class="row">
<div class="col-sm-6">
<?php echo $validator->showAllErrors() ?>

	<div class="form-group">
		<label class="col-sm-3 control-label"><?php echo $translator->translate($form->getFieldLabel("user_type_id")) ?></label>
		<div class="col-sm-6">

			<?php echo $form->renderSelectTag('user_type_id',
					array('class' => 'form-control' , 
					)) ?>
		</div>
	</div>
	
		<div class="form-group">
              <label class="col-sm-3 control-label"><?php echo $translator->translate($form->getFieldLabel("photo")) ?></label>
              <div class="col-sm-6 input-group">
              	<span class="input-group-addon"><i style="font-size:24px;" class="fa fa-picture-o"></i></span>
                <?php echo $form->renderInputTag("photo",array('class' => 'form-control file_input')) ?>
                <?php echo $validator->showError("photo") ?>
            </div> 
    </div>
	
	<div class="form-group">
		<label class="col-sm-3 control-label"><?php echo $translator->translate($form->getFieldLabel("parent_id")) ?></label>
		<div class="col-sm-6">

			<?php echo $form->renderSelectTag('parent_id',
					array('class' => 'form-control' , 
					)) ?>
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-sm-3 control-label"><?php echo $translator->translate($form->getFieldLabel("name")) ?></label>
		<div class="col-sm-6">
			<?php echo $form->renderInputTag('name',array('class' => 'form-control' , 'placeholder' => 'Help Text')) ?>
			<?php echo $validator->showError("name") ?>
		</div>
	</div>
	
	
	<div class="form-group">
		<label class="col-sm-3 control-label"><?php echo $translator->translate($form->getFieldLabel("surname")) ?></label>
		<div class="col-sm-6">
			<?php echo $form->renderInputTag('surname',array('class' => 'form-control')) ?>
			<?php echo $validator->showError("surname") ?>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-3 control-label"><?php echo $translator->translate($form->getFieldLabel("email")) ?></label>
		<div class="col-sm-6">
			<?php echo $form->renderInputTag('email',array('class' => 'form-control' , 'placeholder' => 'Email')) ?>
			<?php echo $validator->showError("email") ?>
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-sm-3 control-label"><?php echo $translator->translate($form->getFieldLabel("phone")) ?></label>
		<div class="col-sm-6">
			<?php echo $form->renderInputTag('phone',array('class' => 'form-control'  )) ?>
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-sm-3 control-label"><?php echo $translator->translate($form->getFieldLabel("position")) ?></label>
		<div class="col-sm-6">
			<?php echo $form->renderInputTag('position',array('class' => 'form-control'  )) ?>
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-sm-3 control-label"><?php echo $translator->translate($form->getFieldLabel("provision_fixed")) ?></label>
		<div class="col-sm-6">
			<?php echo $form->renderInputTag('provision_fixed',array('class' => 'form-control' , 'placeholder' => '')) ?>
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-sm-3 control-label"><?php echo $translator->translate($form->getFieldLabel("provision_percentage")) ?></label>
		<div class="col-sm-6">
			<?php echo $form->renderInputTag('provision_percentage',array('class' => 'form-control')) ?>
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-sm-3 control-label"><?php echo $translator->translate($form->getFieldLabel("receive_notifications")) ?></label>
		<div class="col-sm-6">
			<?php echo $form->renderSelectTag('receive_notifications',array('class' => 'form-control')) ?>
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-sm-3 control-label"><?php echo $translator->translate($form->getFieldLabel("status")) ?></label>
		<div class="col-sm-6">
			<?php echo $form->renderSelectTag('status',array('class' => 'form-control')) ?>
		</div>
	</div>

</div><!-- left col -->

<div class="col-sm-6">
	<h4><?php echo $translator->translate('Lokalita')?></h4>
	<div class="form-group">
	
		<?php foreach($form->getRegionChoices() as $district_name => $regions): ?>
		<div class="col-sm-6 col-md-3" style="min-height: 280px;"  >
				<h5><?php echo $district_name?></h5>

				<?php foreach($regions as $region): ?>
					<input <?php echo (in_array($region->getId(), $form->getEntity()->getRegionIds())) ? 'checked="checked"' : '' ?> type="checkbox" name="record[region_ids][]" value="<?php echo $region->getId() ?>" /> <?php echo $region->getName()?><br />
				<?php endforeach ?>
				
		</div>
		<?php endforeach ?>
	
		
	</div>
	

</div><!-- region col -->


</div><!-- row -->	

	<div class="row">
		<div class="col-sm-6 col-sm-offset-3">
			<button class="btn btn-primary"><?php echo  $translator->translate('BTN_SAVE')?></button>
			&nbsp;
			<a class="btn btn-default" href="<?php echo $router->link('user_list')?>"><?php echo  $translator->translate('BTN_BACK')?></a>
		</div>
	</div>

</form>