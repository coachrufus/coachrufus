<div class="row">
        
        <div class="col-sm-9">
          
         <div class="panel">
	<div class="panel-body">
	
	<div class="panel-heading">
              <div class="panel-btns">
               <a class="" href="<?php echo $router->link('user_edit',array('id' =>  $user->getId())) ?>"><i class="fa fa-edit"></i> <?php echo $translator->translate('BTN_EDIT')?></a>
              </div><!-- panel-btns -->
              <h3 class="panel-title"><?php echo $user->getFullName() ?>(<?php echo $user->getRole()?>) </h3>
            </div>

		<ul class="detail_list">
			
			<li><strong><?php echo $translator->translate('User type') ?>:</strong>
			<?php echo $user->getUserTypeName() ?></li>
			
			<li><strong><?php echo $translator->translate('Email') ?>:</strong>
			<?php echo $user->getEmail() ?></li>
			<li><strong><?php echo $translator->translate('Phone') ?>:</strong>
			<?php echo $user->getPhone() ?></li>
			<li><strong><?php echo $translator->translate('Position') ?>:</strong>
			<?php echo $user->getPosition() ?></li>
		</ul>
		<ul class="detail_list">
			<li><strong><?php echo $translator->translate('Provision fixed') ?>:</strong>
			<?php echo $user->getProvisionFixed() ?></li>
			<li><strong><?php echo $translator->translate('Provision percentage') ?>:</strong>
			<?php echo $user->getProvisionPercentage() ?></li>
			<li><strong><?php echo $translator->translate('Receive notifications') ?>:</strong>
			<?php echo $user->getReceiveNotifications() ?></li>
			<li><strong><?php echo $translator->translate('Status') ?>:</strong>
			<?php echo $user->getStatus() ?></li>
		</ul>

	</div>
	</div>
          
          <!-- Nav tabs -->
        <ul class="nav nav-tabs nav-justified nav-profile">
          <li><a href="#activities" data-toggle="tab"><strong>Aktivity</strong></a></li>
          <li class="active"><a href="#demands" data-toggle="tab"><strong>Dopyty</strong></a></li>
        </ul>
        
        <!-- Tab panes -->
        <div class="tab-content">
          <div class="tab-pane" id="activities">
            <div class="activity-list">
				<?php if(count($logs) > 0): ?>
	              <table class="table datable">
	              	<thead>
	              		<th>Dátum</th>
	              		<th>IP</th>
	              		<th>Aktivita</th>
	              		<th>tabulka</th>
	              		<th>objekt_id</th>
	              		<th>browser</th>
	              	</thead>
	              	
	              	<tbody>
	              		<?php foreach($logs as $log): ?>
						<tr>
							<td><?php echo $log['date'] ?></td>
							<td><?php echo $log['ip'] ?></td>
							<td><?php echo $log['activity'] ?></td>
							<td><?php echo $log['source_table'] ?></td>
							<td><?php echo $log['table_object_ID'] ?></td>
							<td><?php echo $log['browser'] ?></td>
						</tr>
						<?php endforeach; ?>
	              	</tbody>
	              	
	              </table>
				<?php else: ?>
				<div class="alert alert-warning"><?php echo $translator->translate('NOTE_NO_ACTIVITY_ASSIGNED') ;?></div>
				<?php endif; ?>
            </div><!-- activity-list -->
          </div>
          
          <div class="tab-pane active" id="demands">
            <div class="demands-list">
			<?php if(count($demands) > 0): ?>
              <table class="table datable">
              	<thead>
              		<th>ID dopytu</th>
              		<th>zadanie do systemu</th>
              		<th>stav</th>
              		<th>klient</th>
              		<th>dátum a čas poslednej zmeny stavu,</th>
              		<th></th>
              	</thead>
              	
              	<tbody>
              		<?php foreach($demands as $demand): ?>
					<tr>
						<td><?php echo $demand->getId() ?></td>
						<td><?php echo $demand->getDateCreated()->format('d.m.Y') ?></td>
						<td><?php echo $demand->getStatusName() ?></td>
						<td><?php echo $demand->getClientFullName() ?></td>
						<td><?php echo $demand->getStateLastUpdate() ?></td>
						<td><a href="<?php echo $router->link('demand_detail',array('id' => $demand->getId())) ?>"><?php echo $translator->translate('BTN_DETAIL') ;?></a></td>
					</tr>
					<?php endforeach; ?>
              	</tbody>
              	
              </table>
			<?php else: ?>
			<div class="alert alert-warning"><?php echo $translator->translate('NOTE_NO_DEMANDS_ASSIGNED') ;?></div>
			<?php endif; ?>
            </div><!--demands-list -->
          </div>
        </div><!-- tab-content -->
          
        </div><!-- col-sm-9 -->
</div><!-- row -->
