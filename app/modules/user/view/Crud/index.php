<?php 
$router = Core\ServiceLayer::getService('router');
?>

<?php if($request->hasFlashMessage('user_add_success')): ?>
<div class="alert alert-success">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<?php echo $request->getFlashMessage('user_add_success') ?>
</div>
<?php endif; ?>

<div class="panel">
<div class="panel-body">

<?php if(count($admin_table->getRecords()) > 0): ?>

<table class="table" id="datatable">
<!-- SORTOVANIE -->
 <thead>
<tr class="sort-row">
    <?php foreach ($admin_table->getTableRows() as $row_index => $row_data): ?>
        <th>
             <?php $sort_criteria = $admin_table->getSortCriteria(); ?>
             <a class="sort-up <?php echo (array_key_exists($row_index, $sort_criteria) && $sort_criteria[$row_index] == $row_index.' asc' ) ? 'active' : '' ?>" href="<?php echo $_SERVER['REQUEST_URI'] ?>&amp;datagridsort=up&amp;sortcolumn=<?php echo $row_index ?>"></a>
             <span class="sort_label"><?php echo $admin_table->getColumnLabel($row_index)?></span>
             <a class="sort-down <?php echo (array_key_exists($row_index, $sort_criteria) && $sort_criteria[$row_index] == $row_index.' desc' ) ? 'active' : '' ?>" href="<?php echo $_SERVER['REQUEST_URI'] ?>&amp;datagridsort=down&amp;sortcolumn=<?php echo $row_index ?>"></a> 
        </th>
    <?php endforeach ?>
    <th></th>
</tr>
 </thead>

<!-- UDAJE -->
 <tbody>
<?php $i = 0; foreach ($admin_table->getRecords() as $user_data): ?>
    <tr>
        <td><?php echo $user_data['surname']?></td>
        <td><?php echo $user_data['name']  ?></td>
        <td><?php echo $user_data['user_type_name']  ?></td>
        <td><?php echo $user_data['email']  ?></td>
        <td><?php echo $user_data['position']  ?></td>
        <td><?php echo $admin_table->renderField('status') ?></td>
        <td class="table-action">
			<a href="<?php echo $router->link('user_detail',array('id' => $user_data['ID'])) ?>"><?php echo $translator->translate('BTN_DETAIL')?></a>
			<a href="<?php echo $router->link('user_edit',array('id' => $user_data['ID'])) ?>"><?php echo $translator->translate('BTN_EDIT')?></a>
		</td>
    </tr>
<?php endforeach; ?>
</tbody>
</table>

<?php else: ?>
<div class="alert alert-warning"><?php echo $translator->translate('NOTE_NO_DATA') ;?></div>
<?php endif; ?>

</div>
</div>

<a class="btn btn-primary" href="<?php echo $router->link('user_add') ?>"><?php echo $translator->translate('BTN_USER_ADD')?></a>

