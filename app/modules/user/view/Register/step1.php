<div class="login-box">
    <?php $layout->includePart(MODUL_DIR.'/user/view/_logo.php') ?>
    <div class="login-box-body">
        
        <?php if($request->hasFlashMessage('REGISTER_ACTIVATION_SUCCESS')): ?>
        <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $request->getFlashMessage('REGISTER_ACTIVATION_SUCCESS') ?>
        </div>
        <?php endif; ?>
        
        
        
        
        <div class="row progres-step-wrap">
            <div class="col-xs-4 register-active-step">
                 <?php echo $translator->translate('Basic info') ?>
            </div>
            <div class="col-xs-4 ">
                <?php echo $translator->translate('Locality') ?>
            </div>
            <div class="col-xs-4 ">
                <?php echo $translator->translate('Final') ?>
            </div>
            
            <div class="col-sm-12">
              <div class="progress">
                <div class="progress-bar progress-bar-green" role="progressbar" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100" style="width: 33%">
                  <span class="sr-only"></span>
                </div>
              </div>
            </div>
        </div>
             
        
        
        


        <form action="#" method="post">
            <div class="form-group">
                <?php echo $validator->showError('name', $translator->translate('Required Field!')) ?>
                <?php echo $form->renderInputTag('name', array('class' => 'form-control', 'placeholder' => $translator->translate('My name'))) ?>
            </div>
            <div class="form-group">
                <?php echo $validator->showError('surname', $translator->translate('Required Field!')) ?>
                <?php echo $form->renderInputTag('surname', array('class' => 'form-control', 'placeholder' => $translator->translate('Surname'))) ?> 
            </div>
            
            <div class="form-group">
                <?php echo $validator->showError('nickname', $translator->translate('Required Field!')) ?>
                <?php echo $form->renderInputTag('nickname', array('class' => 'form-control', 'placeholder' => $translator->translate('Nickname'))) ?> 
            </div>
            
            <div class="form-group">
                 <p class="login-box-msg"><?php echo $translator->translate('Birthdate') ?></p>
                  <?php echo $validator->showError('birthdate', $translator->translate('Required Field!')) ?>
                <div class="row">
                   
                    <div class="col-md-4 col-xs-12">
                         <?php echo $form->renderSelectTag('birthday', array('class' => 'form-control', 'placeholder' => $translator->translate('Day'))) ?> 
                    </div>
                    <div class="col-md-4 col-xs-12">
                         <?php echo $form->renderSelectTag('birthmonth', array('class' => 'form-control', 'placeholder' => $translator->translate('Month'))) ?> 
                    </div>
                    <div class="col-md-4 col-xs-12">
                         <?php echo $form->renderSelectTag('birthyear', array('class' => 'form-control', 'placeholder' => $translator->translate('Year'))) ?> 
                    </div>
                </div>
               
            </div>
            
            
            
            <div class="row">
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-primary btn-block btn-flat bg_orange"><?php echo $translator->translate('Continue') ?>    </button>
                </div><!-- /.col -->
            </div>
        </form>

     


    </div><!-- /.login-box-body -->
</div><!-- /.login-box -->


