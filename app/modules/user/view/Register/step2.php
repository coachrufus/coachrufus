<div class="login-box">
      <?php $layout->includePart(MODUL_DIR.'/user/view/_logo.php') ?>
    <div class="login-box-body">
         <div class="row  progres-step-wrap">
            <div class="col-xs-4">
                <a href="<?php echo $router->link('register_step1',array('lang' => LANG)) ?>"><?php echo $translator->translate('Basic info') ?></a>
            </div>
            <div class="col-xs-4 register-active-step">
                <?php echo $translator->translate('Locality') ?>
            </div>
            <div class="col-xs-4 ">
                <?php echo $translator->translate('Final') ?>
            </div>
            
            <div class="col-sm-12">
              <div class="progress">
                <div class="progress-bar progress-bar-green" role="progressbar" aria-valuenow="66" aria-valuemin="0" aria-valuemax="100" style="width: 66%">
                  <span class="sr-only"></span>
                </div>
              </div>
            </div>
        </div>


        <form action="#" method="post">
            <div class="row">
                
            <div class="form-group col-sm-12">
                <?php echo $validator->showError('timezone', $translator->translate('Required Field!')) ?>
                <?php echo $form->renderSelectTag('timezone', array('class' => 'form-control select2')) ?>
            </div>
            <div class="form-group  col-sm-6">
                <?php echo $validator->showError('unitsystem', $translator->translate('Required Field!')) ?>
                <?php echo $form->renderSelectTag('unitsystem', array('class' => 'form-control', )) ?> 
            </div>
            
            <div class="form-group col-sm-6">
                <?php echo $validator->showError('dateformat', $translator->translate('Required Field!')) ?>
                <?php echo $form->renderSelectTag('dateformat', array('class' => 'form-control')) ?> 
            </div>
            <div class="form-group col-sm-6">
                <?php echo $validator->showError('timeformat', $translator->translate('Required Field!')) ?>
                <?php echo $form->renderSelectTag('timeformat', array('class' => 'form-control')) ?> 
            </div>
            
            <div class="form-group col-sm-6">
                <?php echo $validator->showError('week_start', $translator->translate('Required Field!')) ?>
                <?php echo $form->renderSelectTag('week_start', array('class' => 'form-control')) ?> 
            </div>
            
           </div>
            
            <div class="row">
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-primary btn-block btn-flat bg_orange"><?php echo $translator->translate('Continue') ?>    </button>
                </div><!-- /.col -->
            </div>
        </form>

     


    </div><!-- /.login-box-body -->
</div><!-- /.login-box -->

<?php $layout->startSlot('javascript') ?>
<?php $layout->addStylesheet('plugins/select2/select2.min.css')?> 
<?php $layout->addJavascript('plugins/select2/select2.full.min.js')?> 


<script src="/dev/js/jstz.js"></script>
<script>
    
  $('#record_timezone').select2();
   
var tz = jstz.determine();
var current_zone_val = $('#record_timezone').val();
if(current_zone_val == '')
{
    $('#record_timezone').val(tz.name());
   
}

</script>
<?php $layout->endSlot('javascript') ?>


