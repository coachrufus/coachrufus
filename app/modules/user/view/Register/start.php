<div class="login-box">
    <?php $layout->includePart(MODUL_DIR.'/user/view/_logo.php') ?>
    <div class="login-box-body">
        <div class="row register-lang">
            <div class="col-xs-4"></div>
            <div class="col-xs-2">
                <a class="<?php echo ($_SESSION['app_lang'] == 'en') ? 'active' : '' ?>" href="/en/register/start">EN</a>
            </div>
            <div class="col-xs-2">
                <a class="<?php echo ($_SESSION['app_lang'] == 'sk') ? 'active' : '' ?>" href="/sk/register/start">SK</a>
            </div>
        </div>
        
        
        <p class="login-box-msg"><?php echo $translator->translate('Register') ?></p>

     
          <div class="social-auth-links text-center">
              
              
            <a href="#" class="btn btn-clear btn-clear-white btn-social" data-toggle="modal" data-target="#register_agree_popup_fb"><i class="ico ico-fb"></i>  <?php echo $translator->translate('Register using Facebook') ?></a>

             <a href="#" class="btn btn-clear btn-clear-white btn-social" data-toggle="modal" data-target="#register_agree_popup_g"><i class="ico ico-google"></i>  <?php echo $translator->translate('Register using Google+') ?></a>

          
               
              
                  <?php $layout->includePart(MODUL_DIR.'/user/view/Register/agree_popup_fb.php') ?>
              <?php $layout->includePart(MODUL_DIR.'/user/view/Register/agree_popup_g.php') ?>
              
           
       
              <p class="or-divide">- <?php echo $translator->translate('OR') ?> -</p>
        </div><!-- /.social-auth-links -->
        
        
        <form action="#" method="post">
            <input type="hidden" name="record[inv_hash]" value="<?php echo $invHash ?>" />
            <input type="hidden" name="record[tp]" value="<?php echo $tp ?>" />
            <input type="hidden" name="record[h]" value="<?php echo $h ?>" />
            <input type="hidden" name="record[promo]" value="<?php echo $promo ?>" />
            
             <div class="form-group has-feedback">
                    <?php echo $validator->showError('name',$translator->translate('Required Field!')) ?>
                  <div class="input-group input-group-full">
                    
                       <?php echo $form->renderInputTag('name',array('class' => 'form-control', 'placeholder' => $translator->translate('register.name') ))?>
                    </div>
                 </div>
            
           
             <div class="form-group has-feedback">
                    <?php echo $validator->showError('surname',$translator->translate('Required Field!')) ?>
                  <div class="input-group input-group-full">
                       <?php echo $form->renderInputTag('surname',array('class' => 'form-control', 'placeholder' => $translator->translate('register.surname') ))?>
                    </div>
                 </div>
            
           
            
            <div class="form-group">
                 <?php echo $validator->showError('exist_user',$translator->translate('User Exist')) ?>
                <?php echo $validator->showError('email',$translator->translate('Required Field!')) ?>
                
                
                 <div class="input-group">
                    <div class="input-group-addon">
                         <i class="ico ico-user"></i>
                    </div>
                   <?php echo $form->renderInputTag('email',array('class' => 'form-control', 'placeholder' => $translator->translate('E-mail') ))?>
                </div>

            </div>
            <div class="form-group has-feedback">
               
                <?php echo $validator->showError('password',$translator->translate('Required Field!')) ?>
                <?php echo $validator->showError('password_confirm',$translator->translate('Password not confirm')) ?>
                
                
                 <div class="input-group">
                    <div class="input-group-addon">
                         <i class="ico ico-lock"></i>
                    </div>
                   <?php echo $form->renderPasswordTag('password',array('class' => 'form-control', 'placeholder' => $translator->translate('Password')))?>
                </div>
            </div>
            <div class="form-group has-feedback">
                 <?php echo $validator->showError('password_retype',$translator->translate('Required Field!')) ?>
                
                 <div class="input-group">
                    <div class="input-group-addon">
                         <i class="ico ico-lock"></i>
                    </div>
                    <?php echo $form->renderPasswordTag('password_retype',array('class' => 'form-control', 'placeholder' => $translator->translate('Retype password')))?>
                </div>
            </div>
            
            
           
           
             <div class="form-group">
                <?php echo $form->renderInputHiddenTag('default_lang', array('class' => 'form-control')) ?>
            </div>
            
            
            
            <div class="row">
                <div class="col-xs-12">
                    <div class="tcblock">
                        
                     
                         <?php echo $validator->showError('vop_agreement',$translator->translate('register.vop.agreement')) ?>
                      <br />
                        <?php echo $form->renderCheckboxTag('vop_agreement', array('class' => 'form-control')) ?>
                        
                        <?php echo $translator->translate('register.vop.checkbox') ?><br />
                         <br />
                    </div>
                    
                  
                    
                    <button type="submit" class="btn btn-primary btn-block btn-flat bg_orange"><?php echo $translator->translate('Register') ?>    </button>
                </div><!-- /.col -->
            </div>
        </form>

      
       

        <div class="row login-footer">
            <div class="col-sm-12 text-center exist-account">
                 <a  href="<?php echo $router->link('login', array('lang' => 'sk','inv' => $invHash,'promo' => $promo)) ?>" class="btn btn-sm text-center"><?php echo $translator->translate('I already have a membership') ?></a>
            </div>
        </div>
       

    </div><!-- /.login-box-body -->
</div><!-- /.login-box -->


