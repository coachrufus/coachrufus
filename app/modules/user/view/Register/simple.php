<?php if($request->hasFlashMessage('register_success')): ?>
<div class="alert alert-success">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<?php echo $request->getFlashMessage('register_success') ?>
</div>
<?php endif; ?>


<form action="" method="post" class="register-form form-horizontal form-bordered">
    <h2><?php echo $translator->Translate('Zaregistrujte sa') ?></h2>
    
    
    
     <div class="form-group">
        <label class="col-sm-3 control-label"><?php echo $translator->Translate('E-mail') ?></label>
        <div class="col-sm-6">
            
            <?php echo $form->renderInputTag('email',array('class' => 'form-control'))?>
            <?php echo $validator->showError('exist_user') ?>
            <?php echo $validator->showError('email') ?>
        </div>
    </div>
    
    
     <div class="form-group">
        <label class="col-sm-3 control-label"><?php echo $translator->Translate('Heslo') ?></label>
        <div class="col-sm-6">
       
            <?php echo $form->renderPasswordTag('password',array('class' => 'form-control'))?>
               <?php echo $validator->showError('password') ?>
        </div>
    </div>
    
    
     <div class="form-group">
        <label class="col-sm-3 control-label"><?php echo $translator->Translate('Zopakujte heslo') ?></label>
        <div class="col-sm-6">

	<?php echo $form->renderPasswordTag('password_retype',array('class' => 'form-control'))?>
            <?php echo $validator->showError('password_retype') ?>
        </div>
    </div>
    
    
    
	
	
	<button class="btn btn-primary" type="submit"><?php echo $translator->Translate('Odoslať') ?></button>
</form>
