<div class="login-box">
     <?php $layout->includePart(MODUL_DIR.'/user/view/_logo.php') ?>
    <div class="login-box-body">
        <div class="row progres-step-wrap">
            <div class="col-xs-4">
                <a href="<?php echo $router->link('register_step1', array('lang' => LANG)) ?>"><?php echo $translator->translate('Basic info') ?></a>
            </div>
            <div class="col-xs-4 ">
                <a href="<?php echo $router->link('register_step2', array('lang' => LANG)) ?>"><?php echo $translator->translate('Locality') ?></a>
            </div>
            <div class="col-xs-4  register-active-step">
                <?php echo $translator->translate('Final') ?>
            </div>

            <div class="col-sm-12">
                <div class="progress">
                    <div class="progress-bar progress-bar-green" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                        <span class="sr-only"></span>
                    </div>
                </div>
            </div>
        </div>


        <form action="#" method="post">
            <div class="row">
                <div class="form-group col-sm-6">
                    <?php echo $validator->showError('default_lang', $translator->translate('Required Field!')) ?>
                    <?php echo $form->renderSelectTag('default_lang', array('class' => 'form-control')) ?> 
                </div>
                <div class="form-group  col-sm-6">
                    <?php echo $validator->showError('gender', $translator->translate('Required Field!')) ?>
                    <?php echo $form->renderSelectTag('gender', array('class' => 'form-control')) ?> 
                </div>

                <div class="col-sm-12">
                <?php echo $translator->translate('My sports') ?>
               
                
                
                <div class="sport-template-row sport_row row">
                    <div class="form-group  col-sm-5">
                       <?php echo $validator->showError('sport_id', $translator->translate('Required Field!')) ?>
                       <?php echo $form->renderSelectTag('sport_id', array('class' => 'form-control','name' => 'record[sports][0][sport]')) ?> 
                   </div>
                    <div class="form-group  col-sm-5">
                       <?php echo $validator->showError('sport_level', $translator->translate('Required Field!')) ?>
                       <?php echo $form->renderSelectTag('sport_level', array('class' => 'form-control','name' => 'record[sports][0][level]')) ?> 
                   </div>
                   <div class="col-sm-2 btn-col">
                       <span class="add-sport-register"><i class="ico ico-plus-w"></i></span>
                   </div>
                </div>
                     </div>
            </div>









            <div class="row">

                <div class="col-xs-12">
                    <button type="submit" class="btn btn-primary btn-block bg_orange"><?php echo $translator->translate('Continue') ?>    </button>
                </div><!-- /.col -->
            </div>
        </form>




    </div><!-- /.login-box-body -->
</div><!-- /.login-box -->


<?php $layout->addJavascript('js/register.js') ?>