
<div class="modal fade" id="register_agree_popup_g" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $translator->translate('Register using Google+') ?></h4>
      </div>
      <div class="modal-body">
          <div>
          <?php echo $translator->translate('register.agree.text') ?>
          </div>
       <?php echo Core\WebApp::getInstance()->renderController('Social\SocialModule:Google:register') ?>
          
      </div>
      
    </div>
  </div>
</div>