<div class="login-box">
    <div class="login-logo">
        <img src="/img/main/logo-white.svg" onerror="this.onerror=null; this.src='/img/logo-white.png'" title="Je čas na hru" alt="logo Coach Rufus">
    </div><!-- /.login-logo -->
    <div class="login-box-body text-center">
       

      <h2><?php echo $translator->translate('Activate email success') ?></h2>
       
       
 <div class="alert alert-success"><?php echo $translator->translate('REGISTER_ACTIVATION_EMAIL_SUCCESS') ?></div>
 
 <a class="btn btn-primary btn-block btn-flat bg_orange" href="<?php echo $router->link('login') ?>"><?php echo $translator->translate('Login') ?></a>

       

    </div><!-- /.login-box-body -->
</div><!-- /.login-box -->




