<?php
namespace User\Model;
use Core\Repository as Repository;
use Core\ServiceLayer;

class UserRepository extends Repository
{
    public function save($user)
    {
        $saved_id = parent::save($user);
        if(null == $user->getId())
        {
            $user->setId($saved_id);
        }
    }
    
    public function findUnactivatedUser($delay)
    {
        $limitDate = new \DateTime();
        $limitDate->setTimestamp(strtotime($delay));
        $data = $this->getStorage()->findUnactivatedUser($limitDate->format('Y-m-d H:i'));
         $list = array();
        foreach ($data as $d)
        {
            $object = $this->factory->createEntityFromArray($d);

            $list[] = $object;
        }

        return $list;
    }
    
    public function getMailchimpExportUsers()
    {
        $data = $this->getStorage()->findMailchimpExportUsers();
        $list = array();
        foreach ($data as $d)
        {
            $object = $this->factory->createEntityFromArray($d);

            $list[] = $object;
        }

        return $list;
    }
}
