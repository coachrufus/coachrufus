<?php
namespace User\Model;
use Core\DbStorage;

class UserStorage extends DbStorage
{
    public function findUnactivatedUser($date)
    {
       t_dump('SELECT * from co_users where DATE_FORMAT(date_created,"%Y-%m-%d %H:%i") = "'.$date.'" and status = 0');
        $data = \Core\DbQuery::prepare('SELECT * from co_users where DATE_FORMAT(date_created,"%Y-%m-%d %H:%i") = "'.$date.'" and status = 0')
					->execute()
					->fetchAll(\PDO::FETCH_ASSOC);
		return $data;
    }
    
    /**
     * find user for mailchimp export
     * @param type $limit limit in minutes
     * @return type
     */
    public function findMailchimpExportUsers($interval = '15')
    {
      
        $data = \Core\DbQuery::prepare('select * from co_users where date_created > NOW() - INTERVAL '.$interval.' MINUTE and status = 1')
					->execute()
					->fetchAll(\PDO::FETCH_ASSOC);
		return $data;
    }
    
}