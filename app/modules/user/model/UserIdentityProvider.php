<?php
namespace User\Model;
use Core\ServiceLayer;
use Core\IdentityProvider as IdentityProvider;

class UserIdentityProvider  implements IdentityProvider
{
	protected $teamRoles;
        protected $teams;
    
        /**
	 * Nájde užívateľa podľa jeho prihlasovacieho mena
	 * @see \Core\IdentityProvider::findUserByUsername()
	 * @param $username užívateľské meno (email)
	 * @return Object|false
	 */
	public function findUserByUsername($username)
	{
		if(empty($username))
		{
			return null;
		}
		$user_repository = \Core\ServiceLayer::getService('user_repository');
		$user = $user_repository->findBy(array('email' => $username,'status' => '1'));
			
		if(empty($user))
		{
			return null;
		}
                
                //$teamRoles = $this->getTeamRoles($user[0]);
                //$user[0]->setTeamRoles($teamRoles);
                
		
		return $user[0];
	}
	
	/**
	 * Nájde užívateľa podľa jeho  id
	 * @param $id id
	 * @return Object|false
	 */
	public function findUserById($id)
	{
		if(empty($id))
		{
			return null;
		}
	
		$user_repository = \Core\ServiceLayer::getService('user_repository');
		$user = $user_repository->findBy(array('id' => $id,'status' => '1'));
	
		if(empty($user))
		{
			$partner_repository = \Core\ServiceLayer::getService('partner_repository');
			$user = $partner_repository->findBy(array('id' => $id));
		}
			
		if(empty($user))
		{
			return null;
		}
	
		return $user[0];
	}
	
	
	/**
	 * Zakoduje heslo
	 * @see \Core\IdentityProvider::encodePassword()
	 * ©return string
	 */
	public function encodePassword($password,$salt)
	{
		$password = sha1($password).sha1($salt);
		return  $password;
	}
	
	
	public function getDefaultIdentity()
	{
		return new \User\Model\UserIdentity() ;
	}
        
        
        public  function setTeamRoles($teamRoles) {
            $this->teamRoles = $teamRoles;
        }
        
        public function getUserTeamsInfo($user)
        {
            $teamManager = ServiceLayer::getService('TeamManager');
            
            $userTeamRoles = $this->getTeamRoles($user);
            $teams = $this->getUserTeams($user);
            $teamList = array();

            foreach($teams as $team)
            {
                $teamPlayers = $teamManager->getTeamPlayers($team->getId());
                $teamList[$team->getId()]['team'] = $team;
                $teamList[$team->getId()]['role'] = $userTeamRoles[$team->getId()]['role'];
                $teamList[$team->getId()]['status'] = $userTeamRoles[$team->getId()]['status'];
                $teamList[$team->getId()]['teamPlayers'] = $teamPlayers;
            }

             return $teamList;
        }

        public function getUserTeams($user)
        {
            if(null == $this->teams)
            {
                $teamManager = ServiceLayer::getService('TeamManager');
                $teams = $teamManager->getUserTeams($user);
                return $this->teams = $teams; 
            }
            return $this->teams;
        }
        
	
        public function getTeamRoles($user)
        {
            if(null == $this->teamRoles)
            {
                $teamRepository = \Core\ServiceLayer::getService('TeamRepository');
                $playerTeams = $teamRepository->getTeamPlayerStorage()->findBy(array('player_id' => $user->getId()));
                $teamRoles = array();

                foreach ($playerTeams as $playerTeam)
                {
                    $teamRoles[$playerTeam['team_id']]['role'] = $playerTeam['team_role'];
                    $teamRoles[$playerTeam['team_id']]['status'] = $playerTeam['status'];
                }

                $ownerTeams =  $teamRepository->findBy(array('author_id' => $user->getId()));
                foreach($ownerTeams as $ownerTeam)
                {
                    $teamRoles[$ownerTeam->getId()]['role'] = 'ADMIN';
                    $teamRoles[$ownerTeam->getId()]['status'] = 'confirmed';
                }
                
                $this->teamRoles = $teamRoles;
            }
            return  $this->teamRoles;
        }
        
        public function saveUserPersistHash($user,$hash)
        {
            $user_repository = \Core\ServiceLayer::getService('user_repository');
            $encoded = $this->encodePassword($hash, $user->getSalt());
            $user->setPersistHash($encoded);
            $user_repository->save($user);
        }
        
        public function findUserByPersistHash($hash)
        {
           if(empty($hash))
            {
                    return null;
            }
            $user_repository = \Core\ServiceLayer::getService('user_repository');
            $user = $user_repository->findBy(array('persist_hash' => $hash,'status' => '1'));

            if(empty($user))
            {
                return null;
            }

            return $user[0];
        }
        
        public function updateLastLogin($user)
        {
            $user_repository = \Core\ServiceLayer::getService('user_repository');
            $user->setLastLogin(new \DateTime());
            $user_repository->save($user);
        }

        public function getPrivacySettings($user)
        {
             $privacyRepo = \Core\ServiceLayer::getService('PlayerPrivacyRepository');
             $privacySettings = $privacyRepo->findBy(array('player_id' => $user->getId()));
             return $privacySettings;
        }
        
	
}