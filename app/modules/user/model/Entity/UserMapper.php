<?php

namespace User\Model;

class UserMapper {

    protected $mapper_rules = array(
        
        'id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'date_created' => array(
            'type' => 'datetime',
            'required' => false
        ),
        'name' => array(
            'type' => 'string',
            'max_length' => '128',
            'required' => false
        ),
        'photo' => array(
            'type' => 'string',
            'max_length' => '255',
            'required' => false
        ),
        'surname' => array(
            'type' => 'string',
            'max_length' => '128',
            'required' => false
        ),
        'email' => array(
            'type' => 'string',
            'max_length' => '128',
            'required' => true
        ),
        'password' => array(
            'type' => 'string',
            'max_length' => '256',
            'required' => false
        ),
        'salt' => array(
            'type' => 'string',
            'max_length' => '256',
            'required' => false
        ),
        'phone' => array(
            'type' => 'string',
            'max_length' => '128',
            'required' => false
        ),
       
        'receive_notifications' => array(
            'type' => 'boolean',
            'max_length' => '',
            'required' => false
        ),
        'status' => array(
            'type' => 'string',
            'max_length' => '10',
            'required' => false
        ),
        'gender' => array(
            'type' => 'string',
            'max_length' => '20',
            'required' => false
        ),
       
        'uid' => array(
            'type' => 'string',
            'max_length' => '50',
            'required' => false
        ),
        'nickname' => array(
            'type' => 'string',
            'max_length' => '50',
            'required' => false
        ),
       
        
        'sport_id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'level' => array(
            'type' => 'string',
            'max_length' => '50',
            'required' => false
        ),
        'birthyear' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'birthmonth' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'birthday' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'default_lang' => array(
            'type' => 'string',
            'max_length' => '50',
            'required' => false
        ),
        'locality_id' => array(
            'type' => 'int',
            'max_length' => '50',
            'required' => false
        ),
        'facebook_id' => array(
            'type' => 'string',
            'max_length' => '50',
            'required' => false
        ),
        'google_id' => array(
            'type' => 'string',
            'max_length' => '50',
            'required' => false
        ),
        'user_type' => array(
            'type' => 'string',
            'max_length' => '50',
            'required' => false
        ),
         'timezone' => array(
            'type' => 'string',
            'max_length' => '128',
            'required' => false
        ),

        'unitsystem' => array(
            'type' => 'string',
            'max_length' => '128',
            'required' => false
        ),

        'dateformat' => array(
            'type' => 'choice',
             'choices' => array('dd.mm.yy' => 'dd.mm.yy','mm/dd/yyyy' => 'mm/dd/yyyy'),
            'max_length' => '128',
            'required' => false
        ),
        'timeformat' => array(
            'type' => 'string',
            'max_length' => '128',
            'required' => false
        ),
        'language' => array(
            'type' => 'string',
            'max_length' => '128',
            'required' => false
        ),
        'gender' => array(
            'type' => 'string',
            'max_length' => '128',
            'required' => false
        ),
        
        'week_start' => array(
            'type' => 'string',
            'max_length' => '128',
            'required' => false
        ),
        
        'weight' => array(
            'type' => 'float',
            'max_length' => '10',
            'required' => false
        ),
        
        'height' => array(
            'type' => 'float',
            'max_length' => '10',
            'required' => false
        ),
        
        'activation_guid' => array(
            'type' => 'string',
            'max_length' => '36',
            'required' => true
        ),
         'persist_hash' => array(
            'type' => 'string',
            'max_length' => '256',
            'required' => true
        ),
         'address' => array(
            'type' => 'string',
            'max_length' => '256',
            'required' => false
        ),
         'country' => array(
            'type' => 'string',
            'max_length' => '256',
            'required' => false
        ),
         'last_login' => array(
            'type' => 'datetime',
            'required' => false
        ),
         'resource' => array(
            'type' => 'varchar',
            'required' => false
        ),
         'notify_settings' => array(
            'type' => 'string',
            'required' => false
        ),
         'mobile_app_device' => array(
            'type' => 'string',
            'required' => false
        ),
         'layout_settings' => array(
            'type' => 'string',
            'required' => false
        ),
    );

    public function getDataMapperRules()
    {
        return $this->mapper_rules;
    }

}
