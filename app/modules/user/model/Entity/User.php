<?php

namespace User\Model;

 use\Core\ServiceLayer as ServiceLayer;
use User\Model\User;
use User\Model\UserMapper;

class User {

    private $repository;
    protected $dataMapper;

    public function __construct()
    {
        $this->dataMapper = new UserMapper();
        $this->mapper_rules = $this->dataMapper->getDataMapperRules();
    }

    public function getDataMapperRules()
    {
        return $this->mapper_rules;
    }

    public function getRepository()
    {
        if (null == $this->repository)
        {
            $this->repository = ServiceLayer::getService("user_repository");
        }
        return $this->repository;
    }

    public function setRepository($repository)
    {
        $this->repository = $repository;
    }

    public function getChoiceName()
    {
        return $this->getName() . ' ' . $this->getSurname() . ' (' . $this->getEmail() . ')';
    }

    public function getFullName()
    {
        return $this->getName() . ' ' . $this->getSurname();
    }

    protected $id;
    protected $userTypeId;
    protected $name;
    protected $surname;
    protected $nickname;
    protected $email;
    protected $password;
    protected $salt;
    protected $passwordBefore1;
    protected $passwordBefore2;
    protected $passwordChanged;
    protected $phone;
    protected $dateCreated;
    protected $birthday;
    protected $birthmonth;
    protected $birthyear;
    protected $receiveNotifications;
    protected $status;
    protected $role;
    protected $photo;
    protected $gender;
    protected $facebookId;
    protected $googleId;
    protected $userType;
    protected $localityId;
    protected $timezone;
    protected $unitsystem;
    protected $dateformat;
    protected $timeformat;
    protected $defaultLang;
    protected $weekStart;
    protected $weight;
    protected $height;
    protected $facebookData;
    protected $activationGuid;
    protected $persistHash;
    protected $address;
    protected $lastLogin;
    protected $country;
    protected $resource;
    protected $notifySettings;
    protected $mobileAppDevice;
    protected $layoutSettings;

    public function setId($val)
    {
        $this->id = $val;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setUserTypeId($val)
    {
        $this->userTypeId = $val;
    }

    public function getUserTypeId()
    {
        return $this->userTypeId;
    }

    public function setName($val)
    {
        $this->name = $val;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setSurname($val)
    {
        $this->surname = $val;
    }

    public function getSurname()
    {
        return $this->surname;
    }

    public function setEmail($val)
    {
        $this->email = $val;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setPassword($val)
    {
        $this->password = $val;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPasswordBefore1($val)
    {
        $this->passwordBefore1 = $val;
    }

    public function getPasswordBefore1()
    {
        return $this->passwordBefore1;
    }

    public function setPasswordBefore2($val)
    {
        $this->passwordBefore2 = $val;
    }

    public function getPasswordBefore2()
    {
        return $this->passwordBefore2;
    }

    public function setPasswordChanged($val)
    {
        $this->passwordChanged = $val;
    }

    public function getPasswordChanged()
    {
        return $this->passwordChanged;
    }

    public function setPhone($val)
    {
        $this->phone = $val;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function setDateCreated($val)
    {
        $this->dateCreated = $val;
    }

    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    public function setReceiveNotifications($val)
    {
        $this->receiveNotifications = $val;
    }

    public function getReceiveNotifications()
    {
        return $this->receiveNotifications;
    }

    public function setStatus($val)
    {
        $this->status = $val;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function getSalt()
    {
        return $this->salt;
    }

    public function setSalt($salt)
    {
        $this->salt = $salt;
    }

    public function getRole()
    {

        $this->setRole('ROLE_USER');
        return $this->role;
    }

    public function setRole($role)
    {
        $this->role = $role;
    }

    function getPersistHash()
    {
        return $this->persistHash;
    }

    function setPersistHash($persistHash)
    {
        $this->persistHash = $persistHash;
    }

    /**
     * photo
     * @return unkown
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    public function getMidPhoto()
    {

        if (null == $this->photo)
        {
            $midPhoto = '/img/users/default.svg';
        }
        else
        {
            $midPhoto = '/img/users/thumb_320_320/' . $this->getPhoto();
        }


        return $midPhoto;
    }

    /**
     * photo
     * @param unkown $photo
     * @return User
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;
        return $this;
    }

    function getFacebookId()
    {
        return $this->facebookId;
    }

    function getGoogleId()
    {
        return $this->googleId;
    }

    function setFacebookId($facebookId)
    {
        $this->facebookId = $facebookId;
    }

    function setGoogleId($googleId)
    {
        $this->googleId = $googleId;
    }

    function getUserType()
    {
        return $this->userType;
    }

    function setUserType($userType)
    {
        $this->userType = $userType;
    }

    public function getLocalityId()
    {
        return $this->localityId;
    }

    public function setLocalityId($localityId)
    {
        $this->localityId = $localityId;
    }

    public function getDataMapper()
    {
        return $this->dataMapper;
    }

    public function getTimezone()
    {
        return $this->timezone;
    }

    public function getUnitsystem()
    {
        return $this->unitsystem;
    }

    public function getDateformat()
    {
        return $this->dateformat;
    }

    public function getTimeformat()
    {
        return $this->timeformat;
    }

    public function getWeekStart()
    {
        return $this->weekStart;
    }

    public function getWeight()
    {
        return $this->weight;
    }

    public function getHeight()
    {
        return $this->height;
    }

    public function setDataMapper($dataMapper)
    {
        $this->dataMapper = $dataMapper;
    }

    public function setTimezone($timezone)
    {
        $this->timezone = $timezone;
    }

    public function setUnitsystem($unitsystem)
    {
        $this->unitsystem = $unitsystem;
    }

    public function setDateformat($dateformat)
    {
        $this->dateformat = $dateformat;
    }

    public function setTimeformat($timeformat)
    {
        $this->timeformat = $timeformat;
    }

    public function setWeekStart($weekStart)
    {
        $this->weekStart = $weekStart;
    }

    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    public function setHeight($height)
    {
        $this->height = $height;
    }

    public function getDefaultLang()
    {
        return $this->defaultLang;
    }

    public function setDefaultLang($defaultLang)
    {
        $this->defaultLang = $defaultLang;
    }

    public function getNickname()
    {
        return $this->nickname;
    }

    public function getBirthday()
    {
        return $this->birthday;
    }

    public function getBirthmonth()
    {
        return $this->birthmonth;
    }

    public function getBirthyear()
    {
        return $this->birthyear;
    }

    public function setNickname($nickname)
    {
        $this->nickname = $nickname;
    }

    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;
    }

    public function setBirthmonth($birthmonth)
    {
        $this->birthmonth = $birthmonth;
    }

    public function setBirthyear($birthyear)
    {
        $this->birthyear = $birthyear;
    }

    public function getGender()
    {
        return $this->gender;
    }

    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    public function getFacebookData()
    {
        return $this->facebookData;
    }

    public function setFacebookData($facebookData)
    {
        $this->facebookData = $facebookData;
    }

    public function getActivationGuid()
    {
        return $this->activationGuid;
    }

    public function setActivationGuid($activationGuid)
    {
        $this->activationGuid = $activationGuid;
    }

    function getAddress()
    {
        return $this->address;
    }

    function setAddress($address)
    {
        $this->address = $address;
    }

    public function getLastLogin()
    {
        return $this->lastLogin;
    }

    public function setLastLogin($lastLogin)
    {
        $this->lastLogin = $lastLogin;
    }

    public function getCountry()
    {
        return $this->country;
    }

    public function setCountry($country)
    {
        $this->country = $country;
    }

    public function getResource()
    {
        return $this->resource;
    }

    public function setResource($resource)
    {
        $this->resource = $resource;
    }

    public function getNotifySettings()
    {
        return $this->notifySettings;
    }

    public function setNotifySettings($notifySettings)
    {
        $this->notifySettings = $notifySettings;
    }

    public function getMobileAppDevice()
    {
        return $this->mobileAppDevice;
    }

    public function getLayoutSettings()
    {
        return $this->layoutSettings;
    }

    public function setMobileAppDevice($mobileAppDevice)
    {
        $this->mobileAppDevice = $mobileAppDevice;
    }

    public function setLayoutSettings($layoutSettings)
    {
        $this->layoutSettings = $layoutSettings;
    }
    
    public function getLayoutSettingsValue($key)
    {
       $layoutSettings = $this->getLayoutSettings();
        if(null == $layoutSettings)
        {
            $layoutSettings = array();
        }
        else
        {
            $layoutSettings = json_decode($layoutSettings,true);
        }
 
        if(array_key_exists($key, $layoutSettings))
        {
            return $layoutSettings[$key];
        }
        else
        {
            return null;
        }
    }

}
