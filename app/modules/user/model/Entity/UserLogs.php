<?php
namespace PH\User\Model;
class UserLogs
{
	protected $id;
	protected $userId;
	protected $ip;
	protected $browser;
	protected $date;
	protected $activity;
	protected $table;
	protected $tableObjectId;

	public function setId($val)
	{
		$this->id = $val;
	}
	public function getId()
	{
		return $this->id;
	}
	public function setUserId($val)
	{
		$this->userId = $val;
	}
	public function getUserId()
	{
		return $this->userId;
	}
	public function setIp($val)
	{
		$this->ip = $val;
	}
	public function getIp()
	{
		return $this->ip;
	}
	public function setBrowser($val)
	{
		$this->browser = $val;
	}
	public function getBrowser()
	{
		return $this->browser;
	}
	public function setDate($val)
	{
		$this->date = $val;
	}
	public function getDate()
	{
		return $this->date;
	}
	public function setActivity($val)
	{
		$this->activity = $val;
	}
	public function getActivity()
	{
		return $this->activity;
	}
	public function setTable($val)
	{
		$this->table = $val;
	}
	public function getTable()
	{
		return $this->table;
	}
	public function setTableObjectId($val)
	{
		$this->tableObjectId = $val;
	}
	public function getTableObjectId()
	{
		return $this->tableObjectId;
	}

}

