<?php

namespace User\Model;
use AclException;

class UserIdentity extends \Core\UserIdentity {

    private $teamRoles;
    private $teamPermissions;

    public function getTeamRoles()
    {
        //return $this->teamRoles;
        $user = $this->getUser();
        $roles = \Core\ServiceLayer::getService('security')->getIdentityProvider()->getTeamRoles($user);
        return $roles;
    }
    
     public function getUserTeams($user = null)
    {
        if(null == $user)
        {
            $user = $this->getUser();
        }
        $teams = \Core\ServiceLayer::getService('security')->getIdentityProvider()->getUserTeams($user);
        return $teams;
    }
     
    public function getUserTeamsInfo($user = null)
    {
        if(null == $user)
        {
            $user = $this->getUser();
        }
        $info = \Core\ServiceLayer::getService('security')->getIdentityProvider()->getUserTeamsInfo($user);
        return $info;
    }
    
    public function getUserCurrentTeamId()
    {
        if(null == $_SESSION['active_team'])
        {
            $teams = $this->getUserTeams();
            if(null != current($teams))
            {
                $_SESSION['active_team']['id'] = current($teams)->getId();
            }
            else
            {
                return null;
            }
            
        }
        return $_SESSION['active_team']['id'];
    }
    
    

    /**
     * 
     * @param newVal
     */
    public function setTeamRoles($newVal)
    {
        $this->teamRoles = $newVal;
    }

    /**
     * Check team permissions
     * @param type $team
     * @return boolean
     */
    public function getTeamPermissions($team)
    {
        $permissionTable = array();
        foreach($this->getTeamRoles() as $teamId => $teamRole)
        {
            if($teamRole['role'] == 'ADMIN')
            {
                 $permissionTable[$teamId]['manageEvents'] = true;
                 $permissionTable[$teamId]['managePlayers'] = true;
                 $permissionTable[$teamId]['manageSettings'] = true;
                 $permissionTable[$teamId]['activePlayer'] = true;
                 $permissionTable[$teamId]['managePlaygrounds'] = true;
                 $permissionTable[$teamId]['manageScouting'] = true;
                 $permissionTable[$teamId]['view'] = true;
            }
            else
            {
                 $permissionTable[$teamId]['manageEvents'] = false;
                 $permissionTable[$teamId]['managePlayers'] = false;
                 $permissionTable[$teamId]['manageSettings'] = false;
                 $permissionTable[$teamId]['managePlaygrounds'] = false;
                 $permissionTable[$teamId]['manageScouting'] = false;
                 $permissionTable[$teamId]['view'] = true;
            }
            
            
            if($teamRole['status'] == 'confirmed')
            {
                $permissionTable[$teamId]['activePlayer'] = true;
            }
            else
            {
                 $permissionTable[$teamId]['activePlayer'] = false;
            }
            
        }
        if(null != $team)
        {
            return $permissionTable[$team->getId()];
        }
        
    }
    
    public function hasTeamPermission($name,$team)
    {
        $permission = $this->getTeamPermissions($team);
        if($permission[$name] == true)
        {
            return true;
        }
        
        if(array_key_exists('super_admin_mode', $_SESSION) && 'yes' == $_SESSION['super_admin_mode'])
        {
            return true;
        }
        
        return false;
    }
    
    
    /**
     * Check if uset can view team
     * @param type $team
     * @throws AclException
     */
    public function checkTeamAccess($team)
    {
        $request = \Core\ServiceLayer::getService('request');
        $translator = \Core\ServiceLayer::getService('translator');
        $router = \Core\ServiceLayer::getService('router');
           
        
        //check players number
        $teamCreditManager = \Core\ServiceLayer::getService('TeamCreditManager');
        
        $teamsInfo =  $this->getUserTeamsInfo($this->getUser());
        $teamPlayersCount =  count($teamsInfo[$team->getId()]['teamPlayers']);
        
        $adminCount = 0;
        foreach($teamsInfo[$team->getId()]['teamPlayers'] as $teamPlayer)
        {
            if($teamPlayer->getTeamRole() == 'ADMIN')
            {
                $adminCount++;
            }
        }
        
        //t_dump($teamsInfo);exit;
        
        
        $teamPlayersLimit = $teamCreditManager->getTeamPlayersLimit($team);

        if($teamPlayersCount > $teamPlayersLimit)
        {
             $request->addFlashMessage('too_many_players_free',$translator->translate('TOO_MANY_PLAYERS_MODAL_TEXT'));
             $request->redirect($router->link('team_players_list',array('team_id' => $team->getId())));
        }
        
        $adminsLimit = $teamCreditManager->getAdminsLimit($team);
        if($adminCount > $adminsLimit)
        {
             $request->addFlashMessage('too_many_admins_free',$translator->translate('TOO_MANY_ADMINS_MODAL_TEXT'));
             $request->redirect($router->link('team_players_list',array('team_id' => $team->getId())));
        }
        
        
        if(!$this->hasTeamPermission('view',$team))
        {
            throw  new AclException();
        }
    }

   

}
