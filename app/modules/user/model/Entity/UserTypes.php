<?php
namespace User\Model;
class UserTypes
{

	protected $id;
	protected $name;

	public function setId($val)
	{
		$this->id = $val;
	}
	public function getId()
	{
		return $this->id;
	}
	public function setName($val)
	{
		$this->name = $val;
	}
	public function getName()
	{
		return $this->name;
	}

}

