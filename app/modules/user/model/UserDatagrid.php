<?php 
namespace User\Model;
use Core\DatagridTable;

class UserDatagrid extends DatagridTable
{
    protected $repository;
    private $status_enum = ['0' => 'unactive', '1' => 'Active', '2' => 'Locked'];

    public function __construct()
    {
        $this->setTableName('ph_users');
        $this->setTableRows([
            'Priezvisko' => 'surname',
            'Meno' => 'name',
            'Rola' => 'user_type_name',
            'Email' => 'email',
            'Pracovná pozícia' =>
            'position',
            'status' => 'status'
        ]);
        $this->query = 'SELECT  a.*,
        t.name as user_type_name
        FROM '.$this->getTableName().' a
        LEFT JOIN ph_user_types t ON a.user_type_ID = t.id';
    }

    public function getRepository()
    {
        return $this->repository;
    }

    public function setRepository($repository)
    {
        $this->repository = $repository;
    }

    public function getStatus($record)
    {
        return $this->status_enum[$record['status']];
    }
}