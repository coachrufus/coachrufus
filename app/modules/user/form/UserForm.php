<?php
namespace PH\User\Form;
use \Core\Form as Form;
use Core\ServiceLayer;
use PH\User\Model\User as User;

class UserForm extends Form {
	protected $fields = array (
			'id' => array (
					'type' => 'int',
					'max_length' => '11',
					'required' => false 
			),
			'parent_id' => array (
					'type' => 'choice',
					'max_length' => '11',
					'required' => false 
			),
			
			'user_type_id' => array (
					'type' => 'int',
					'max_length' => '11',
					'required' => false 
			),
			'name' => array (
					'type' => 'string',
					'max_length' => '128',
					'required' => true 
			),
			'surname' => array (
					'type' => 'string',
					'max_length' => '128',
					'required' => true 
			),
			'email' => array (
					'type' => 'string',
					'max_length' => '128',
					'required' => true 
			),
			'password' => array (
					'type' => 'string',
					'max_length' => '32',
					'required' => false 
			),
			'salt' => array (
					'type' => 'string',
					'max_length' => '32',
					'required' => false 
			),
			'password_before1' => array (
					'type' => 'string',
					'max_length' => '32',
					'required' => false 
			),
			'password_before2' => array (
					'type' => 'string',
					'max_length' => '32',
					'required' => false 
			),
			'password_changed' => array (
					'type' => 'datetime',
					'max_length' => '',
					'required' => false 
			),
			'phone' => array (
					'type' => 'string',
					'max_length' => '128',
					'required' => false 
			),
			'position' => array (
					'type' => 'string',
					'max_length' => '128',
					'required' => false 
			),
			'provision_fixed' => array (
					'type' => '',
					'max_length' => '',
					'required' => false 
			),
			'provision_percentage' => array (
					'type' => '',
					'max_length' => '',
					'required' => false 
			),
			'date_created' => array (
					'type' => 'datetime',
					'max_length' => '',
					'required' => false 
			),
			'receive_notifications' => array (
					'type' => 'boolean',
					'max_length' => '',
					'required' => true 
			),
			'status' => array (
					'type' => 'string',
					'max_length' => '',
					'required' => true 
			),
			'photo' => array (
					'type' => 'string',
					'max_length' => '',
					'required' => false 
			),
			'region_ids' => array (
					'type' => 'choice',
					'max_length' => '11',
					'required' => false
			),
	);

	public function __construct()
	{
		//$translator = ServiceLayer::getService('translator');
		$this->fields['user_type_id']['choices'] =  $this->getUserTypeChoices();
		$this->fields['parent_id']['choices'] =  $this->getParentChoices();
		$this->fields['region_ids']['choices'] =  $this->getRegionChoices();
		//$this->fields['parent_id']['value'] =  $this->getEntity()->getParentId();
		//$this->fields['user_type_id']['message'] =  ;
		
		$this->fields['receive_notifications']['choices'] =  array('0' => 'Nie', '1' => 'Áno');
		$this->fields['status']['choices'] =  array('0' => 'unactive','1'=>'Active','2' => 'Locked');
		
	}
	
	public function getUserTypeChoices()
	{
		$repository = ServiceLayer::getService('user_repository');
		$choices = $repository->getUserTypeFormChoices();
		return $choices;
	}
		
	public function getParentChoices()
	{
		$repository = ServiceLayer::getService('user_repository');
		$choices = $repository->getMangersChoices();
		$choices = array('none' => 'žiadny')+$choices;
		return $choices;
	}
	
	public function getRegionChoices()
	{
		$d_repository = ServiceLayer::getService('district_repository');
		$districts = $d_repository->findAll();
		$repository = ServiceLayer::getService('region_repository');
		$choices = array();
		foreach($districts as $district)
		{
			if(0 != $district->getId())
			{
				$choices[$district->getName()] = $repository->findby(array('district_id' => $district->getId()));
			}
			
		}
		
		
		return $choices;
	}
		
}