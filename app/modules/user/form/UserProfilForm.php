<?php
namespace User\Form;
use \Core\Form as Form;
use Core\ServiceLayer;
use User\Model\User as User;

class UserProfilForm extends Form {
	protected $fields = array (
			'id' => array (
					'type' => 'int',
					'max_length' => '11',
					'required' => false 
			),
			'parent_id' => array (
					'type' => 'choice',
					'max_length' => '11',
					'required' => false 
			),
			
			'user_type_id' => array (
					'type' => 'int',
					'max_length' => '11',
					'required' => false 
			),
			'nickname' => array (
					'type' => 'string',
					'max_length' => '128',
					'required' => true 
			),
			'name' => array (
					'type' => 'string',
					'max_length' => '128',
					'required' => true 
			),
			'surname' => array (
					'type' => 'string',
					'max_length' => '128',
					'required' => true 
			),
			'email' => array (
					'type' => 'string',
					'max_length' => '128',
					'required' => true 
			),
			'password' => array (
					'type' => 'string',
					'max_length' => '32',
					'required' => false 
			),
			'salt' => array (
					'type' => 'string',
					'max_length' => '32',
					'required' => false 
			),
			'password_before1' => array (
					'type' => 'string',
					'max_length' => '32',
					'required' => false 
			),
			'password_before2' => array (
					'type' => 'string',
					'max_length' => '32',
					'required' => false 
			),
			'password_changed' => array (
					'type' => 'datetime',
					'max_length' => '',
					'required' => false 
			),
			'phone' => array (
					'type' => 'string',
					'max_length' => '128',
					'required' => false 
			),
			'position' => array (
					'type' => 'string',
					'max_length' => '128',
					'required' => false 
			),
			'provision_fixed' => array (
					'type' => '',
					'max_length' => '',
					'required' => false 
			),
			'provision_percentage' => array (
					'type' => '',
					'max_length' => '',
					'required' => false 
			),
			'date_created' => array (
					'type' => 'datetime',
					'max_length' => '',
					'required' => false 
			),
			'receive_notifications' => array (
					'type' => 'boolean',
					'max_length' => '',
					'required' => false 
			),
			'status' => array (
					'type' => 'string',
					'max_length' => '',
					'required' => false 
			),
			'photo' => array (
					'type' => 'string',
					'max_length' => '',
					'required' => false 
			),
			'region_ids' => array (
					'type' => 'choice',
					'max_length' => '11',
					'required' => false
			),
			'birthyear' => array (
					'type' => 'string',
					'max_length' => '11',
					'required' => false,
                                        'help' => 'birthyear'
			),
			'birthmonth' => array (
					'type' => 'string',
					'max_length' => '11',
					'required' => false,
                            'help' => 'birthmonth'
			),
			'birthday' => array (
					'type' => 'string',
					'max_length' => '11',
					'required' => false,
                            'help' => 'birthday'
			),
			'level' => array (
					'type' => 'choice',
					'max_length' => '11',
					'required' => false,
  
			),
			'locality' => array (
					'type' => 'string',
					'max_length' => '11',
					'required' => false,
  
			),
	);

	public function __construct()
	{
                $sportManager = ServiceLayer::getService('SportManager');
                $playerManager = ServiceLayer::getService('PlayerManager');
                $translator = ServiceLayer::getService('translator');
		
		$this->fields['receive_notifications']['choices'] =  array('2' => 'Nie', '1' => 'Áno');
                
                
                $this->setField('sport_id', array(
                    'type' => 'choice',
                    'choices' =>  $sportManager->getSportFormChoices(),
                    'required' => true,
                ));
                 $this->setField('level', array(
                    'type' => 'choice',
                    'choices' =>  $playerManager->getLevelFormChoices(),
                    'required' => true,
                ));
                 $this->setField('player_status', array(
                    'type' => 'choice',
                    'choices' =>  $playerManager->getStatusFormChoices(),
                    'required' => true,
                ));
                 $this->setField('gender', array(
                    'type' => 'choice',
                    'choices' =>  array('man' => $translator->translate('Muž'), 'woman' =>  $translator->translate('Žena')),
                    'required' => true,
                ));
                 $this->setField('default_lang', array(
                    'type' => 'choice',
                    'choices' =>  $playerManager->getLangFormChoices(),
                    'required' => true,
                ));
        
		
	}
	
	public function getUserTypeChoices()
	{
		$repository = ServiceLayer::getService('user_repository');
		$choices = $repository->getUserTypeFormChoices();
		return $choices;
	}
		
	
	
		
}