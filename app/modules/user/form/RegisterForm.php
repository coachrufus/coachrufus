<?php

namespace User\Form;

use \Core\Form as Form;
use Core\ServiceLayer;
use User\Model\User as User;

class RegisterForm extends Form {

    private $translator;

    public function getTranslator()
    {
        return $this->translator;
    }

    public function setTranslator($translator)
    {
        $this->translator = $translator;
    }

    protected $fields = array(
        'id' => array(
            'type' => 'int',
            'max_length' => '11',
            'required' => false
        ),
        'name' => array(
            'type' => 'string',
            'max_length' => '128',
            'required' => true,
            'label' => 'My name'
        ),
        'surname' => array(
            'type' => 'string',
            'max_length' => '128',
            'required' => false
        ),
        'nickname' => array(
            'type' => 'string',
            'max_length' => '128',
            'required' => true
        ),
        'email' => array(
            'type' => 'string',
            'max_length' => '128',
            'required' => true
        ),
        'password' => array(
            'type' => 'string',
            'max_length' => '128',
            'required' => false
        ),
        'password_retype' => array(
            'type' => 'string',
            'max_length' => '128',
            'required' => false
        ),
        'birthday' => array(
            'type' => 'choice',
            'choices' => array(),
            'max_length' => '32',
            'required' => false
        ),
        'birthmonth' => array(
            'type' => 'choice',
            'choices' => array(),
            'max_length' => '32',
            'required' => false
        ),
        'birthyear' => array(
            'type' => 'choice',
            'choices' => array(),
            'max_length' => '32',
            'required' => false
        ),
        
          
        'timezone' => array(
            'type' => 'choice',
             'choices' => array(),
            'max_length' => '128',
            'required' => false
        ),
        
       
        
        'unitsystem' => array(
            'type' => 'choice',
             'choices' => array(),
            'max_length' => '128',
            'required' => false
        ),
        
        
        'dateformat' => array(
            'type' => 'choice',
             'choices' => array('dd.mm.yy' => 'dd.mm.yy','mm/dd/yyyy' => 'mm/dd/yyyy'),
            'max_length' => '128',
            'required' => false
        ),
        'timeformat' => array(
            'type' => 'choice',
             'choices' => array('24H' => '24H','AM/PM' => 'AM/PM'),
            'max_length' => '128',
            'required' => false
        ),
        'default_lang' => array(
            'type' => 'choice',
             'choices' => array(),
            'max_length' => '128',
            'required' => false
        ),
        'gender' => array(
            'type' => 'choice',
             'choices' => array(),
            'max_length' => '128',
            'required' => false
        ),
        
        'week_start' => array(
            'type' => 'choice',
            'choices' => array(),
            'max_length' => '128',
            'required' => false
        ),
        
        'weight' => array(
            'type' => 'int',
            'max_length' => '10',
            'required' => false
        ),
        
        'height' => array(
            'type' => 'int',
            'max_length' => '10',
            'required' => false
        ),
        
        
        'phone' => array(
            'type' => 'string',
            'max_length' => '128',
            'required' => false
        ),
        'receive_notifications' => array(
            'type' => 'boolean',
            'max_length' => '',
            'required' => true
        ),
        'status' => array(
            'type' => 'string',
            'max_length' => '',
            'required' => true
        ),
        'photo' => array(
            'type' => 'string',
            'max_length' => '256',
            'required' => false
        ),
        'country' => array(
            'type' => 'string',
            'max_length' => '256',
            'required' => false
        ),
        'sport_id' => array(
            'type' => 'choice',
            'required' => false
        ),
        'sport_level' => array(
            'type' => 'choice',
            'required' => false
        ),
        'vop_agreement' => array(
            'type' => 'choice',
            'required' => true
        ),
        'marketing_agreement' => array(
            'type' => 'choice',
            'required' => false
        ),
    );

    public function __construct()
    {
        $day_choices = range(1, 31);
        $day_choices = array_combine($day_choices, $day_choices);
        $this->setFieldChoices('birthday', $day_choices);

        $month_choices = range(1, 12);
        $month_choices = array_combine($month_choices, $month_choices);
        $this->setFieldChoices('birthmonth', $month_choices);
        
        $year_choices = range(date('Y')-99, date('Y')-5);
        $year_choices = array_reverse(array_combine($year_choices, $year_choices),true);
        $this->setFieldChoices('birthyear', $year_choices);
        
        $this->setFieldChoices('timezone', $this->getTimezoneChoices());
        
        
        
    }
    
    public function getTimezoneChoices()
    {
        $timezone_choices = array();
        $timezones = \DateTimeZone::listIdentifiers(\DateTimeZone::ALL);
        foreach($timezones as $timezone)
        {
            $tz = new \DateTimeZone($timezone);
            $offset = $tz->getOffset(new \DateTime());
            $offset_prefix = $offset < 0 ? '-' : '+';
            $utc_suffix = $offset_prefix.gmdate('H:i',$offset);
            
            $timezoneParts = explode('/',$timezone);
            

            if(array_key_exists(1, $timezoneParts))
            {
                 $timezone_choices[$timezone] =  $timezoneParts[1].'/'.$timezoneParts[0].' (GMT'.$utc_suffix.')';
            }
           

        }
        
        return $timezone_choices;
        
    }

}
