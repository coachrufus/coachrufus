<?php
namespace PH\User\Form;
use \Core\Validator as CoreValidator;
use Core\ServiceLayer;
use PH\User\Model\User as User;

class UserValidator extends CoreValidator 
{
	private $validEmail;
	
	public function validateData()
	{
		parent::validateData();
		
		$translator = ServiceLayer::getService('translator');
		$data = $this->getData();
		
		//unique email
		$exist_user_email = ServiceLayer::getService('user_repository')->findOneBy(array('email' => $data['email'] ));
		$exist_partner_email =  ServiceLayer::getService('partner_repository')->findOneBy(array('email' => $data['email'] ));

		if( (null != $exist_user_email && $exist_user_email->getEmail() != $this->getValidEmail() )
		  	or 
		 	(null != $exist_partner_email  && $exist_partner_email->getEmail() != $this->getValidEmail() )
		 )
		{
			$this->addErrorMessage('email',$mesage = $translator->translate('validator.non_unique_user_email'));
		}
		
		
		
	}
	

	public function getValidEmail()
	{
	    return $this->validEmail;
	}

	public function setValidEmail($validEmail)
	{
	    $this->validEmail = $validEmail;
	}
}