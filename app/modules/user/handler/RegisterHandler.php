<?php
namespace User\Handler;
use \Core\ServiceLayer as ServiceLayer;
use Core\FormHandler;
use \Core\Validator as Validator;
use Core\Notifications\NotificationTemplate;

class RegisterHandler extends FormHandler
{
    public function __construct()
    {
        $validator = new Validator();
        $this->setValidator('base', $validator);
    }
    
    public function validateData()
    {
        //overi ci uz existuje uzivatel s tymto menom
        $data = $this->getSanitizedData();
        $validator = $this->getValidator('base');
        $validator->setData($data);
        $validator->setRules([
            'email' => [
                'required' => true,
                'type' => 'string'
            ],
            'password' => [
                'required' => true,
                'type' => 'string'
            ],
            'password_retype' => [
                'required' => true,
                'type' => 'string'
            ]
        ]);
        $validator->validateData();
        
        
        $user_repository = \Core\ServiceLayer::getService('user_repository');
        $exist_user = $user_repository->findBy(array(
            'email' => $data['email']
        ));
        
        if (null != $exist_user) {
            $validator->addErrorMessage('exist_user', 'Užívateľ s týmto emailom už je zaregistrovaný');
        }
        
        
        if (false == $validator->validateEmail($data['email']) && '' != $data['email']) {
            $validator->addErrorMessage('email', 'Neplatný formát emailu');
        }
        
        if ($data['password'] != $data['password_retype'] && '' != $data['password'] && '' != $data['password_retype']) {
            $validator->addErrorMessage('password_confirm', 'Password not confirm');
        }
        
        
       
        
        if ($validator->hasErrors()) {
            return false;
        } else {
            return true;
        }
        return true;
    }
    
    public function persistData($data = null, $activationGuid = null)
    {
        if (null == $data)
        {
            $data = $this->getSanitizedData();
        }
        $user = new \User\Model\User();
        $security = \Core\ServiceLayer::getService('security');
        $repo = $user->getRepository();
        $user->setSalt(md5(time()));
        $repo->updateEntityFromArray($user, $data);
        $encoded_password = $security->encodePassword($data['password'], $user->getSalt());
        $user->setPassword($encoded_password);
        $user->setDateCreated(new \DateTime());
        if(null == $data['status'])
        {
            $user->setStatus(0);
        }
        $user->setActivationGuid($activationGuid);
        $repo->save($user);
    }
    
    public function registerToMailchimp($user)
    {
         $MailChimp = ServiceLayer::getService('MailchimpManager');
         $MailChimp->post('lists/967dfc9f8f/members',array(
                'email_address'=> $user->getEmail(),
                'language' => $user->getDefaultLang(), 
                'status' => 'subscribed', 
                ));
    }
    
    public function sendRegisterEmail($lang, $activationGuid = null)
    {
        if (null == $lang) $lang = 'sk';
        $data = $this->getSanitizedData();
        $notificationType = "registration_email_{$lang}";
        $notification_manager = ServiceLayer::getService('notification_manager');
        $notification_repository = ServiceLayer::getService('notification_repository');
        $notification = $notification_repository->findOneBy([
            'notification_type' => $notificationType
        ]);
        $template = new NotificationTemplate($notificationType);
        $template->userEmail = $data['email'];
        $template->userPlainPassword = $data['password'];
        $template->lang = $lang;
        $template->activationGuid = $activationGuid;
        $notification->setMessage((string)$template);
        try
        {
            $notification->setRecipient($data['email']);
            //$notification->setBcc('marek.hubacek@gmail.com');
            $send_result = $notification_manager->sendNotification($notification);
        }
        catch (Exception $e)
        {
            
        }
    }
   
    
     public function sendVerifyEmail($user)
    {
        
        $data = array();
        $data['lang'] = $user->getDefaultLang();
        $data['email'] =  $user->getEmail();
        $data['activationGuid'] =  $user->getActivationGuid();
        $notificationManger = ServiceLayer::getService('notification_manager');
        $notificationManger->sendVerifyEmail($data);
       
    }
}