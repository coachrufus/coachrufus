<?php
namespace User\Handler;

use \Core\ServiceLayer as ServiceLayer;
use Core\FormHandler;
use \Core\Validator as Validator;
use Core\Notifications\NotificationTemplate;
use Core\Activity\ActivityTypes;

class StepRegisterHandler extends FormHandler
{
    private $activityLogger;
    
    public function __construct()
    {
        $this->setValidator('start', new Validator());
        $this->setValidator('step1', new Validator());
        $this->setValidator('step2', new Validator());
        $this->setValidator('step3', new Validator());
        $this->activityLogger = ServiceLayer::getService('ActivityLogger');
    }
    
    public function saveToSession($index, $data)
    {
        $_SESSION['multistep_register_data'][$index] = $data;
    }
    
    
    public function getRegisterdData()
    {
        return $_SESSION['multistep_register_data'];
    }

    public function validateStart()
    {
        //overi ci uz existuje uzivatel s tymto menom
        $data = $this->getSanitizedData();
        $validator = $this->getValidator('start');
        $validator->setData($data);
        $rules = [
            'email' => [
                'required' => true,
                'type' => 'string'
            ],
            'password' => [
                'required' => true,
                'type' => 'string'
            ],
            'password_retype' => [
                'required' => true,
                'type' => 'string'
            ],
            'name' => [
                'required' => true,
                'type' => 'string'
            ],
        ];
        $validator->setRules($rules);
        $validator->validateData();
        
        
        $user_repository = \Core\ServiceLayer::getService('user_repository');
        $exist_user = $user_repository->findBy([
            'email' => $data['email']
        ]);
        
        if (null != $exist_user)
        {
            $validator->addErrorMessage('exist_user', 'Užívateľ s týmto emailom už je zaregistrovaný');
        }

        if (false == $validator->validateEmail($data['email']) && '' != $data['email'])
        {
            $validator->addErrorMessage('email', 'Neplatný formát emailu');
        }

        if ($data['password'] != $data['password_retype'] && '' != $data['password'] && '' != $data['password_retype']) {
            $validator->addErrorMessage('password_confirm', 'Password not confirm');
        }
        
        if(!array_key_exists('vop_agreement', $data))
        {
             $validator->addErrorMessage('vop_agreement', 'register.vop.agreement');
        }
        
        
        if ($validator->hasErrors()) 
        {
            return false;
        }
        else 
        {
            return true;
        }
    }
    
    public function validateStep1()
    {
        $data = $this->getSanitizedData();
        $validator = $this->getValidator('step1');
        $validator->setData($data);
        $rules = [
            'birthdate_day' => [
                'required' => true,
                'type' => 'int'
            ],
            'birthdate_month' => [
                'required' => true,
                'type' => 'int'
            ],
            'birthdate_year' => [
                'required' => true,
                'type' => 'int'
            ]
        ];
        $validator->setRules($rules);
        $validator->validateData();
        
        if ($validator->hasErrors())
        {
            $validator->addErrorMessage('birthdate', 'Invalid');
            return false;
        }
        else
        {
            return true;
        }
    }
    
    public function validateStep2()
    {
        $data = $this->getSanitizedData();
        $validator = $this->getValidator('step2');
        $validator->setData($data);
        
        $rules = [
            'birthdate_day' => [
                'required' => true,
                'type' => 'int'
            ],
            'birthdate_month' => [
                'required' => true,
                'type' => 'int'
            ],
            'birthdate_year' => [
                'required' => true,
                'type' => 'int'
            ]
        ];
        $validator->setRules($rules);
        $validator->validateData();
        
        if ($validator->hasErrors())
        {
            $validator->addErrorMessage('birthdate', 'Invalid');
            return false;
        }
        else
        {
            return true;
        }
    }
    
    public function validateStep3() { return true; }
    
    public function persistStartData($data = null)
    {
        if (null == $data) 
        {
            $data = $this->getSanitizedData();
        }
        $this->saveToSession('start', $data);
        
        if(null == $data['default_lang'])
        {
            $data['default_lang'] = 'sk';
        }
        

        $user_data = $this->getRegisterdData();
        $user = new \User\Model\User();
        $security = \Core\ServiceLayer::getService('security');
        $repo = $user->getRepository();
        $user->setSalt(md5(time()));
        $repo->updateEntityFromArray($user, $user_data['start']);
        $encoded_password = $security->encodePassword($user_data['start']['password'], $user->getSalt());
        $user->setPassword($encoded_password);
        $status = (null == $data['status']) ? 0 : $data['status'];
        $user->setStatus($status);
        $user->setCountry($data['country']);
        $user->setDefaultLang($data['default_lang']);
        $user->setTimezone($data['timezone']);
        $user->setResource($data['promo']);
        
        $user->setActivationGuid($data['activation_guid']);
        $user->setDateCreated(new \DateTime());
        $user->setReceiveNotifications(true);
        $repo->save($user);
        
        $privacyRepo = \Core\ServiceLayer::getService('PlayerPrivacyRepository');
         //save info about vop
        $userPrivacy = new \Webteamer\Player\Model\PlayerPrivacy();
        $userPrivacy->setSection('vop_agreement');
        $userPrivacy->setUserGroup('coachrufus');
        $userPrivacy->setPlayerId($user->getId());
        $userPrivacy->setValue('1');
        $privacyRepo->save($userPrivacy);
        
      
        
        //team admin
        if(array_key_exists('team_admin_agreement', $data) && '1' == $data['team_admin_agreement'])
        {
            $teamAdminPrivacy = new \Webteamer\Player\Model\PlayerPrivacy();
            $teamAdminPrivacy->setSection('team_admin_agreement');
            $teamAdminPrivacy->setUserGroup('coachrufus');
            $teamAdminPrivacy->setPlayerId($user->getId());
            $teamAdminPrivacy->setValue('1');
            $privacyRepo->save($teamAdminPrivacy);
        }
      
       
        
    }
    
    public function persistActivationData($data = null)
    {   
        $this->saveToSession('activation', $data);
    }
    
    public function persistStep1Data($data = null)
    {
        if (null == $data) $data = $this->getSanitizedData();        
        $this->saveToSession('step1', $data);
    }
    
    public function persistStep2Data($data = null)
    {
        if (null == $data) $data = $this->getSanitizedData();
        $this->saveToSession('step2', $data);
    }
    public function persistStep3Data($data = null)
    {
        if (null == $data) $data = $this->getSanitizedData();
        $this->saveToSession('step3', $data);
         $security = \Core\ServiceLayer::getService('security');
        
        //save sports
        $playerManager = ServiceLayer::getService('PlayerManager');
        $playerSports = array();
        foreach($data['sports'] as $sportData )
        {
            if(null != $sportData['sport'])
            {
                $playerSport = new \Webteamer\Player\Model\PlayerSport();
                $playerSport->setSportId($sportData['sport']);
                $playerSport->setLevel($sportData['level']);
                $playerSports[] = $playerSport;
            }
            
        }

        $user = $security->getIdentity()->getUser();
        $playerManager->savePlayerSports($user,$playerSports);
        
    }
    
    public function persistData($data = null, $activationGuid = null)
    {
        if (null == $data) $data = $this->getRegisterdData();
        $user_data = $data['start'] + $data['step1'] + $data['step2'] + $data['step3'];
        $user = new \User\Model\User();
        $security = \Core\ServiceLayer::getService('security');
        $repo = $user->getRepository();
        $user->setSalt(md5(time()));
        $repo->updateEntityFromArray($user, $user_data);
        $encoded_password = $security->encodePassword($user_data['password'], $user->getSalt());
        $user->setDateCreated(new \DateTime());
        $user->setPassword($encoded_password);
        $user->setStatus(0);
        $user->setActivationGuid($activationGuid);
        $repo->save($user);
        if (!is_null($activationGuid)) $this->activityLogger->log($user->getId(), ActivityTypes::REGISTRATION);
    }
    
    public function persistPostActivationData($data = null)
    {
        if (null == $data)
        {
            $data = $this->getRegisterdData();
        }
        $playerManager = \Core\ServiceLayer::getService('PlayerManager');
        $userData = array_merge($data['step1'],$data['step2'],$data['step3']);
        $security = \Core\ServiceLayer::getService('security');
        
        $user = $security->getIdentity()->getUser();
        if(null != $user)
        {
            $repo = $user->getRepository();
            
            $avatar = $playerManager->createAvatar($userData['name'].' '.$userData['surname']);
            $avatarName = $playerManager->savePlayerAvatar($user,$avatar);
            $userData['photo'] = $avatarName;
            
            $repo->updateEntityFromArray($user, $userData);
            $repo->save($user);
        }
       
    }
    
    public function sendRegisterEmail($lang, $activationGuid = null)
    {
        
        $registerData = $this->getRegisterdData();
        
        
        if(null != $registerData['start']['inv_hash'])
        {
            $activationGuid = $activationGuid.'?inv='.$registerData['start']['inv_hash'];
            
            if(null != $registerData['start']['tp'])
            {
                $activationGuid = $activationGuid.'&tp='.$registerData['start']['tp'].'&h='.$registerData['start']['h'];
            }
        }
        
        $data = array();
        $data['lang'] = $lang;
        $data['email'] =  $registerData['start']['email'];
        $data['activationGuid'] = $activationGuid;
        
        $notificationManger = ServiceLayer::getService('notification_manager');
        $notificationManger->sendBaseRegisterEmail($data);

    }
    
    
}