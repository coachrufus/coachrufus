<?php
namespace User;
require_once(MODUL_DIR.'/user/UserModule.php');
require_once(MODUL_DIR.'/user/handler/CrudHandler.php');

use Core\ServiceLayer as ServiceLayer;


//services
/**
 * Ulozisko dat viazucich sa k uzivatelovi
 */
ServiceLayer::addService('user_repository',array(
'class' => "\User\Model\UserRepository",
	'params' => array(
		new \User\Model\UserStorage('co_users'),
		new \Core\EntityMapper('\User\Model\User')
	)
));



//security
$acl = ServiceLayer::getService('acl');
$acl->allowRole('guest','register');
$acl->allowRole('guest','login');
$acl->allowRole('guest','logout');
$acl->allowRole('guest','password_expired');
$acl->allowRole('guest','password_lost');
$acl->allowRole('ROLE_ADMIN','user_list');
$acl->allowRole('ROLE_ADMIN','user_detail');
$acl->allowRole('ROLE_ADMIN','user_edit');
$acl->allowRole('ROLE_ADMIN','user_add');
$acl->allowRole(array('ROLE_USER'),'password_change');
$acl->allowRole(array('ROLE_USER'),'profil');


$acl->allowRole(array('ROLE_ADMIN','ROLE_MANAGER'),'user_list_manager');
$acl->allowRole(array('ROLE_ADMIN','ROLE_MANAGER'),'user_detail_manager');
$acl->allowRole(array('ROLE_ADMIN','ROLE_MANAGER'),'user_edit_manager');
$acl->allowRole(array('ROLE_ADMIN','ROLE_MANAGER'),'user_add_manager');

//routing
$router = ServiceLayer::getService('router');

include_once('cron_routing.php');

/*
$router->addRoute('register','/:lang/register',array(
		'controller' => '\User\UserModule:Register:simple'
));
*/

$acl->allowRole('guest','register_start');
$router->addRoute('register_start','/:lang/register/start',array(
		'controller' => '\User\UserModule:Register:start'
));

$acl->allowRole('guest','register_start_flch');
$router->addRoute('register_start_flch','/:lang/register-flch/start',array(
		'controller' => '\User\UserModule:Register:flchStart'
));

$acl->allowRole('guest','register_start_promo');
$router->addRoute('register_start_promo','/:lang/particka-hokeja/start',array(
		'controller' => '\User\UserModule:Register:promoStart'
));

$acl->allowRole('guest','register_start_finish');
$router->addRoute('register_start_finish','/:lang/register/start-finish',array(
		'controller' => '\User\UserModule:Register:startFinish'
));

$acl->allowRole('guest','register_step1');
$router->addRoute('register_step1','/:lang/register/step1',array(
		'controller' => '\User\UserModule:Register:step1'
));
$acl->allowRole('guest','register_step2');
$router->addRoute('register_step2','/:lang/register/step2',array(
		'controller' => '\User\UserModule:Register:step2'
));
$acl->allowRole('guest','register_step3');
$router->addRoute('register_step3','/:lang/register/step3',array(
		'controller' => '\User\UserModule:Register:step3'
));
$acl->allowRole('guest','register_activation');
$router->addRoute('register_activation','/:lang/register/activation/:code', [
    'controller' => '\User\UserModule:Register:activation'
]);

$acl->allowRole('guest','register_email_activation');
$router->addRoute('register_email_activation','/:lang/register/activation-email/:code', [
    'controller' => '\User\UserModule:Register:activationEmail'
]);

$router->addRoute('login','/:lang/login',array(
		'controller' => '\User\UserModule:Security:login',
                'default' => array('lang' =>  $_SESSION['app_lang'] )
));

$router->addRoute('logout','/logout',array(
		'controller' => '\User\UserModule:Security:logout'
));

$router->addRoute('password_expired','/password-expired',array(
		'controller' => '\User\UserModule:Security:passwordExpired'
));

$router->addRoute('password_lost','/password-lost',array(
		'controller' => '\User\UserModule:Security:passwordLost'
));

$router->addRoute('password_change','/password-change',array(
		'controller' => '\User\UserModule:Security:passwordChange'
));

$router->addRoute('profil','/profil',array(
		'controller' => '\Webteamer\Player\PlayerModule:Profil:profil'
));

$acl->allowRole(array('ROLE_USER'),'gdpr_settings');
$router->addRoute('gdpr_settings','/gdpr',array(
		'controller' => '\User\UserModule:Security:gdpr'
));






$router->addRoute('user_list','/user/list',array(		
	'controller' => '\User\UserModule:Crud:index'
));

$router->addRoute('user_add','/user/add',array(		
	'controller' => '\User\UserModule:Crud:add'
));

$router->addRoute('user_edit','/user/edit',array(		
	'controller' => '\User\UserModule:Crud:edit'
));

$router->addRoute('user_detail','/user/detail',array(		
	'controller' => '\User\UserModule:Crud:detail'
));

$router->addRoute('user_delete','/user/delete',array(		
	'controller' => '\User\UserModule:Crud:edit'
));

//manager role routes
$router->addRoute('user_list_manager','/user/list-manager',array(
		'controller' => '\User\UserModule:Manager:list'
));

$router->addRoute('user_detail_manager','/user/manager/detail',array(
		'controller' => '\User\UserModule:Manager:detail'
));

$router->addRoute('user_edit_manager','/user/manager/edit',array(
		'controller' => '\User\UserModule:Manager:edit'
));

$router->addRoute('user_add_manager','/user/manager/add',array(
		'controller' => '\User\UserModule:Manager:add'
));




