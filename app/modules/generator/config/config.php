<?php
namespace Generator;
require_once(MODUL_DIR.'/generator/GeneratorModule.php');


use Core\ServiceLayer as ServiceLayer;


//services


$acl = ServiceLayer::getService('acl');


//routing
$router = ServiceLayer::getService('router');

$acl->allowRole('guest','generate_modul');
$router->addRoute('generate_modul','/generator/generate/module',array(
		'controller' => 'Generator\GeneratorModule:Generator:generateModule'
));

$acl->allowRole('guest','generate_entity');
$router->addRoute('generate_entity','/generator/generate/entity',array(
		'controller' => 'Generator\GeneratorModule:Generator:generateEntity'
));

$acl->allowRole('guest','generate_entity_mapper');
$router->addRoute('generate_entity_mapper','/generator/generate/entity-mapper',array(
		'controller' => 'Generator\GeneratorModule:Generator:generateEntityMapper'
));

$acl->allowRole('guest','generate_form');
$router->addRoute('generate_form','/generator/generate/form',array(
		'controller' => 'Generator\GeneratorModule:Generator:generateForm'
));


$acl->allowRole('guest','generate_view');
$router->addRoute('generate_view','/generator/generate/view',array(
		'controller' => 'Generator\GeneratorModule:Generator:generateView'
));








