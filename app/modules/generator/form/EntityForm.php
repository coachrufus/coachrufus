<?php
namespace Generator\Form;
use \Core\Form as Form;
use Core\ServiceLayer;


class EntityForm extends Form {
	protected $fields = array (

			'namespace' => array (
					'type' => 'choice',
					'max_length' => '128',
					'required' => true 
			),
                        'name' => array (
					'type' => 'string',
					'max_length' => '128',
					'required' => true 
			),
			'table_name' => array (
					'type' => 'string',
					'max_length' => '128',
					'required' => true 
			),
			
	);

	public function __construct()
	{
		$web_app = \Core\WebApp::getInstance();
                $namespaces = $web_app->getNamespaces();
                $choices = array();
                foreach($namespaces as $space => $dir)
                {
                    $choices[$space.'{DEL}'.$dir] = $space;
                }
                
                
                $this->fields['namespace']['choices'] =  $choices;
		
	}
	
	
		
}