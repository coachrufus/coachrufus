<?php
namespace Generator\Form;
use \Core\Form as Form;
use Core\ServiceLayer;


class ModulForm extends Form {
	protected $fields = array (

			'name' => array (
					'type' => 'string',
					'max_length' => '128',
					'required' => true 
			),
			'namespace' => array (
					'type' => 'string',
					'max_length' => '128',
					'required' => true 
			),
			
	);

	public function __construct()
	{
		
		
	}
	
	
		
}