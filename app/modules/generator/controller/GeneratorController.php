<?php
namespace Generator\Controller;

use Core\Controller as Controller;
use Core\ServiceLayer;
use Core\Validator as Validator;
use Generator\Model\Generator as Generator;


class GeneratorController extends Controller {
	
	public function generateModuleAction()
	{
		
            $form = new \Generator\Form\ModulForm();
            $validator = new Validator();
            
            $request = ServiceLayer::getService('request');
            $generator = new Generator();

            
           
            if($request->getMethod() == 'POST')
		{
			$post_data = $request->get('record');
			$name = $post_data['name'];
			$namespace = $post_data['namespace'];
                        
                        $generator->setNamespace($namespace);
                        $generator->setName($name);
                        
                        
                        $generator->generateStructure();
                        $generator->generateConfig();
                        
			
			$request->addFlashMessage('generator_success', $translator->translate('Generator success'));
			$request->redirect();

		}
                
                
                
		return $this->render('Generator\GeneratorModule:Generator:module.php',
				array(
						'form' => $form,
                                    'validator' => $validator
				));
	}
        
        public function generateEntityAction()
	{
		
            $form = new \Generator\Form\EntityForm();
            $validator = new Validator();
            
            $request = ServiceLayer::getService('request');
            $generator = new Generator();
            

           
            if($request->getMethod() == 'POST')
		{
			$post_data = $request->get('record');
                        
                        $namespace_info  = explode('{DEL}',$post_data['namespace']);
                        
                        $pathinfo = pathinfo($namespace_info[1]);
                       
                        
                        
                        $generator->setNamespace($namespace_info[0]);
                        $generator->setModulDir($namespace_info[1]);
                        //$generator->setName($pathinfo['basename']);
                       
                        
			$name = $post_data['name'];
			$table_name = $post_data['table_name'];
                        
                        $generator->setEntity($name);
                        $generator->setTable($table_name);
                        $generator->generateRepository($name);
                        $generator->generateEntity($table_name ,$name);
                     
			
			$request->addFlashMessage('generator_success', $translator->translate('Generator success'));
			//$request->redirect();

		}
                
                
                
		return $this->render('Generator\GeneratorModule:Generator:entity.php',
				array(
						'form' => $form,
                                    'validator' => $validator
				));
	}
        
         public function generateEntityMapperAction()
	{
		
            $form = new \Generator\Form\EntityForm();
            $validator = new Validator();
            
            $request = ServiceLayer::getService('request');
            $generator = new Generator();
            

           
            if($request->getMethod() == 'POST')
		{
			$post_data = $request->get('record');
                        
                        $namespace_info  = explode('{DEL}',$post_data['namespace']);
                        
                        $pathinfo = pathinfo($namespace_info[1]);
                       
                        
                        
                        $generator->setNamespace($namespace_info[0]);
                        $generator->setModulDir($namespace_info[1]);
                        //$generator->setName($pathinfo['basename']);
                       
                        
			$name = $post_data['name'];
			$table_name = $post_data['table_name'];
                        
                        $generator->setEntity($name);
                        $generator->setTable($table_name);
                        $generator->generateRepository($name);
                        $generator->generateEntityMapper($table_name ,$name);
                     
			
			$request->addFlashMessage('generator_success', $translator->translate('Generator success'));
			//$request->redirect();

		}
                
                
                
		return $this->render('Generator\GeneratorModule:Generator:entity.php',
				array(
						'form' => $form,
                                    'validator' => $validator
				));
	}
        
        public function generateFormAction()
        {
            $form = new \Generator\Form\FormForm();
            $validator = new Validator();
            
            $request = ServiceLayer::getService('request');
            $generator = new Generator();
            
             if($request->getMethod() == 'POST')
             {
                 $post_data = $request->get('record');
                        
                        $namespace_info  = explode('{DEL}',$post_data['namespace']);
                        
                        $pathinfo = pathinfo($namespace_info[1]);
                       
                        
                        
                        $generator->setNamespace($namespace_info[0]);
                        $generator->setModulDir($namespace_info[1]);
                        
                 
                 $generator->generateForm( $post_data['name'] ,$post_data['entity_name']);
             }
            
            return $this->render('Generator\GeneratorModule:Generator:form.php',
				array(
				    'form' => $form,
                                    'validator' => $validator
				));
        }
        
        public function generateViewAction()
        {
            $form = new \Generator\Form\ViewForm();
            $validator = new Validator();
            
            $request = ServiceLayer::getService('request');
            $generator = new Generator();
            
             if($request->getMethod() == 'POST')
             {
                 $post_data = $request->get('record');
                        
                        $namespace_info  = explode('{DEL}',$post_data['namespace']);
                        
                        $pathinfo = pathinfo($namespace_info[1]);
                       
                        
                        
                        $generator->setNamespace($namespace_info[0]);
                        $generator->setModulDir($namespace_info[1]);
                        
                 
                 $generator->generateViews( $post_data['name'] ,$post_data['folder']);
             }
            
            return $this->render('Generator\GeneratorModule:Generator:view.php',
				array(
				    'form' => $form,
                                    'validator' => $validator
				));
        }
        

}