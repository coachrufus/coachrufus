<?php
namespace Generator\Model;

class Generator {

    private $namespace;
    private $entity;
    private $table;
    private $modul_dir;
    private $modul;
    private $modul_name;
    private $name;

    public function __construct()
    {
        /*
        $this->namespace = 'Generator'; //{{NAMESPACE}}
        $this->class = 'Generator'; //{{ENTITY}}
        $this->table = 'generator'; //{{TABLE}}
        $this->name = 'generator'; //{{NAME}}
        $this->modul_dir = APP_DIR . '/modules/generator';
        $this->modul = 'generator'; //{{MODUL}}
        $this->modul_name = 'GeneratorModule';  //{{MODULE_NAME}}
         * 
         */
    }

    public function getNamespace()
    {
        return $this->namespace;
    }

    public function getTable()
    {
        return $this->table;
    }

    public function getModulDir()
    {
        return $this->modul_dir;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setNamespace($namespace)
    {
        $this->namespace = $namespace;
    }

    public function setTable($table)
    {
        $this->table = $table;
    }

    public function setModulDir($modul_dir)
    {
        $this->modul_dir = $modul_dir;
    }

    public function setName($name)
    {
        $this->name = $name;
    }
    
   public function getEntity() {
return $this->entity;
}

 public function setEntity($entity) {
$this->entity = $entity;
}



    public function replaceMarks($cnt)
    {
        
        $pathInfo = pathinfo ($this->getModulDir() ); 
        
        $cnt = str_replace('{{NAMESPACE}}', $this->namespace, $cnt);
        $cnt = str_replace('{{ENTITY}}', $this->getEntity(), $cnt);
        $cnt = str_replace('{{NAME}}',  $this->getName(), $cnt);
        $cnt = str_replace('{{MODUL_DIR_NAME}}', $pathInfo['basename'], $cnt);
        $cnt = str_replace('{{TABLE}}', $this->getTable(), $cnt);
        $cnt = str_replace('{{MODULE_NAME}}', $this->getName(), $cnt);
        $cnt = str_replace('{{SYSTEM_NAME}}', strtolower($this->getName()), $cnt);
        return $cnt;
    }

    public function replaceFile($file)
    {
        $cnt = file_get_contents($file);
        $cnt = $this->replaceMarks($cnt);
        file_put_contents($file, $cnt);
    }

    public function generateRepository()
    {
        $cnt = file_get_contents(GENERATOR_DIR . '/model/Repository.php');
        $cnt = $this->replaceMarks($cnt);
        file_put_contents($this->getModulDir() . '/model/' . $this->getEntity() . 'Repository.php', $cnt);
        
        
        
        
        
    }

    public function generateController($name)
    {
        $controller_cnt = file_get_contents(GENERATOR_DIR . '/controller/DefaultController.php');
        $controller_cnt = $this->replaceMarks($controller_cnt);
        $controller_cnt = str_replace('{{CONTROLLER_NAME}}',$name, $controller_cnt);
        
        file_put_contents($this->modul_dir . '/controller/' . $name . 'Controller.php', $controller_cnt);
    }

    public function generateEntity($table_name,$class_name)
    {
        $storage = new \Core\DbStorage($table_name);
        
        if(!file_exists($this->getModulDir() . '/model'))
        {
            mkdir($this->getModulDir() . '/model');
        }
        
        if(!file_exists($this->getModulDir() . '/model/Entity'))
        {
            mkdir($this->getModulDir() . '/model/Entity');
        }
        
        $this->setEntity($class_name);
        $this->setTable($table_name);
        
        $storage->generateEntity($class_name, $this->getModulDir() . '/model/Entity');
        
        $cnt = file_get_contents($this->getModulDir() . '/model/Entity/' . $class_name . '.php');
        $cnt = $this->replaceMarks($cnt);
        file_put_contents($this->getModulDir() . '/model/Entity/' . $class_name . '.php', $cnt);
        //replaceFile($base_dir . '/model/Entity/' . $class . '.php');
    }
    
     public function generateEntityMapper($table_name,$class_name)
    {
        $storage = new \Core\DbStorage($table_name);
        
        if(!file_exists($this->getModulDir() . '/model'))
        {
            mkdir($this->getModulDir() . '/model');
        }
        
        if(!file_exists($this->getModulDir() . '/model/Entity'))
        {
            mkdir($this->getModulDir() . '/model/Entity');
        }
        
        $this->setEntity($class_name);
        $this->setTable($table_name);
        
        $storage->generateEntityMapper($class_name, $this->getModulDir() . '/model/Entity');
        
        $cnt = file_get_contents($this->getModulDir() . '/model/Entity/' . $class_name . 'Mapper.php');
        $cnt = $this->replaceMarks($cnt);
        file_put_contents($this->getModulDir() . '/model/Entity/' . $class_name . 'Mapper.php', $cnt);
        //replaceFile($base_dir . '/model/Entity/' . $class . '.php');
    }

    public function generateForm($form_name,$class_name)
    {
        require($this->getModulDir() . '/model/Entity/' . $class_name . '.php');
        $entity_name = $this->getNamespace() . '\\Model\\' . $class_name;
        $entity = new $entity_name();
        $form = new \Core\Form();
        $form->generateForm($this->getModulDir() . '/form/', $form_name, $entity);
        $cnt = file_get_contents($this->getModulDir() . '/form/' . $form_name . 'Form.php');
        $cnt = $this->replaceMarks($cnt);
        file_put_contents($this->getModulDir() . '/form/' . $form_name . 'Form.php', $cnt);
    }
    
    public function generateCreateView()
    {
        
    }

    public function generateViews($form_name,$view_dir_name)
    {
        require($this->getModulDir() . '/form/' . $form_name. '.php');
        $form_name = $this->getNamespace() . '\\Form\\' . $form_name ;
        $generated_form = new $form_name();
        $fields_string = '';
        $list_string = '';
        foreach ($generated_form->getFields() as $name => $field)
        {
            $required_asterix = ($field['required'] == true) ? '<span class="asterisk">*</span>' : '';
            $fields_string .= '
                                <div class="form-group">
                      <label class="col-sm-3 control-label"><?php echo $translator->translate($form->getFieldLabel("' . $name . '")) ?>' . $required_asterix . '</label>
                      <div class="col-sm-6">
                        <?php echo $form->renderInputTag("' . $name . '",array(\'class\' => \'form-control\')) ?>
                        <?php echo $validator->showError("' . $name . '") ?>
                      </div>
                    </div>';


            $list_string .= '
                <td><?php echo $data["' . $name . '"]?></td>';
        }

        $view_dir = $this->getModulDir(). '/view/' . $view_dir_name;
        if (!file_exists($view_dir))
        {
            mkdir($view_dir);
        }


        $cnt = file_get_contents(GENERATOR_DIR . '/view/Crud/index.php');
        $list_cnt = str_replace('{{LIST_ROWS}}', $list_string, $cnt);
        $list_cnt = $this->replaceMarks($list_cnt);
        file_put_contents($view_dir . '/index.php', $list_cnt);


        $cnt = file_get_contents(GENERATOR_DIR . '/view/Crud/create.php');
        $add_cnt = str_replace('{{FORM_FIELDS}}', $fields_string, $cnt);
        $add_cnt = $this->replaceMarks($add_cnt);
        file_put_contents($view_dir . '/add.php', $add_cnt);

        $cnt = file_get_contents(GENERATOR_DIR . '/view/Crud/edit.php');
        $edit_cnt = str_replace('{{FORM_FIELDS}}', $fields_string, $cnt);
        file_put_contents($view_dir . '/edit.php', $edit_cnt);
    }

    public function generateConfig()
    {
        $cnt = file_get_contents(GENERATOR_DIR . '/config/config.php');
        $cnt = $this->replaceMarks($cnt);
        //file_put_contents($this->modul_dir . '/config/' . $this->class . '_config.php', $cnt);
        //if(null == $this->getModulDir())
       
        file_put_contents($this->getModulDir() . '/config/config.php', $cnt);
    }
    
    public function generateModulClass()
    {
        $cnt = file_get_contents(GENERATOR_DIR . '/Module.php');
        $cnt = $this->replaceMarks($cnt);
        file_put_contents($this->getModulDir() . '/'.$this->getName().'Module.php', $cnt);
    }
    

    
    public function generateStructure()
    {
        $name = $this->getName();
        $modulDir =  APP_DIR . '/modules/'.$name;
        $this->setModulDir($modulDir);

        //vytvori adresar
        if(!file_exists($modulDir))
        {
           //vytvori adrsarovu strukturu
            mkdir($modulDir);
            mkdir($modulDir.'/config');
            mkdir($modulDir.'/controller');
            mkdir($modulDir.'/view');
           
            
            //vytvori modul file
            $this->generateModulClass();
            $this->generateConfig();
            $this->generateController('Default');
        }
    }


}
