<?php if($request->hasFlashMessage('genrate_success')): ?>
<div class="alert alert-success">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<?php echo $request->getFlashMessage('genrate_success') ?>
</div>
<?php endif; ?>


<form action="" method="post" class="register-form">
    <h2><?php echo $translator->Translate('Views') ?></h2>
	
	
        
        
	<label><?php echo $translator->Translate('Module') ?></label>
	<?php echo $validator->showError('namespace') ?>
	<?php echo $form->renderSelectTag('namespace')?>
        
        <label><?php echo $translator->Translate('Folder') ?></label>
	<?php echo $validator->showError('folder') ?>
	<?php echo $form->renderInputTag('folder')?>
        
	<label><?php echo $translator->Translate('Názov') ?></label>
	<?php echo $validator->showError('name') ?>
	<?php echo $form->renderInputTag('name')?>
	
	<button type="submit"><?php echo $translator->Translate('Odoslať') ?></button>
</form>
