<?php
namespace {{NAMESPACE}};
use Core\Module as Module;
class {{MODULE_NAME}}Module extends Module
{
    public function __construct() 
    {
        $this->setBaseDir(MODUL_DIR.'/{{NAME}}');
        $this->setName('{{NAME}}');
    }
}

