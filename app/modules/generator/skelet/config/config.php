<?php
namespace {{NAMESPACE}};
require_once(MODUL_DIR.'/{{MODUL_DIR_NAME}}/{{MODULE_NAME}}Module.php');


use Core\ServiceLayer as ServiceLayer;


$acl = ServiceLayer::getService('acl');


//routing
$router = ServiceLayer::getService('router');


$acl->allowRole('guest','{{SYSTEM_NAME}}_modul_info');

$router->addRoute('{{SYSTEM_NAME}}_modul_info','/{{SYSTEM_NAME}}/info',array(
		'controller' => '{{NAMESPACE}}\{{MODULE_NAME}}Module:Default:info'
));






