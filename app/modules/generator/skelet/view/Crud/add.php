<?php 
$translator = Core\ServiceLayer::getService('translator');
?>
<?php if($request->hasFlashMessage('{{NAME}}_add_success')): ?>
<div class="alert alert-success">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<?php echo $request->getFlashMessage('{{NAME}}_add_success') ?>
</div>
<?php endif; ?>

<form action="" class="form-horizontal form-bordered" method="post">
<?php echo $validator->showAllErrors() ?>
{{FORM_FIELDS}}
 <div class="row">
	<div class="col-sm-6 col-sm-offset-3">
	  <button class="btn btn-primary">Submit</button>&nbsp;
	  <button class="btn btn-default">Cancel</button>
	</div>
 </div>
</form>
