<?php 
$router = Core\ServiceLayer::getService('router');
?>

<table class="table" id="datatable">
<!-- SORTOVANIE -->
 <thead>
<tr class="sort-row">
    <?php foreach ($admin_table->getTableRows() as $row_index => $row_data): ?>
        <th>
             <?php $sort_criteria = $admin_table->getSortCriteria(); ?>
             <a class="sort-up <?php echo (array_key_exists($row_index, $sort_criteria) && $sort_criteria[$row_index] == $row_index.' asc' ) ? 'active' : '' ?>" href="<?php echo $_SERVER['REQUEST_URI'] ?>&amp;datagridsort=up&amp;sortcolumn=<?php echo $row_index ?>"></a>
             <span class="sort_label"><?php echo $admin_table->getColumnLabel($row_index)?></span>
             <a class="sort-down <?php echo (array_key_exists($row_index, $sort_criteria) && $sort_criteria[$row_index] == $row_index.' desc' ) ? 'active' : '' ?>" href="<?php echo $_SERVER['REQUEST_URI'] ?>&amp;datagridsort=down&amp;sortcolumn=<?php echo $row_index ?>"></a> 
        </th>
    <?php endforeach ?>
    <th></th>
</tr>
 </thead>

<!-- UDAJE -->
 <tbody>
<?php $i = 0; foreach ($admin_table->getRecords() as $data): ?>
    <tr>
        
        
        {{LIST_ROWS}}
        
        <td class="table-action">
			<a href="<?php echo $router->link('{{NAME}}_edit',array('id' => $data['id'])) ?>"><i class="fa fa-pencil"></i></a>
			<a href="<?php echo $router->link('{{NAME}}_delete',array('id' => $data['id'])) ?>" class="delete-row"><i class="fa fa-trash-o"></i></a>
		</td>
    </tr>
<?php endforeach; ?>
</tbody>
</table>

<a class="btn btn-primary" href="<?php echo $router->link('{{NAME}}_add')?>"><?php echo $translator->translate('create') ?></a>