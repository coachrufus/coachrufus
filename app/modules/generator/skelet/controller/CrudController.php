<?php
namespace {{NAMESPACE}}\Controller;
use Core\Controller as Controller;
use Core\Request as Request;
use Core\ServiceLayer;
use Core\DatagridTable as DatagridTable;

use {{NAMESPACE}}\Model\{{ENTITY}} as {{ENTITY}};
use {{NAMESPACE}}\Model\{{ENTITY}}Repository as {{ENTITY}}Repository;
use {{NAMESPACE}}\Form\{{ENTITY}}Form as {{ENTITY}}Form;
use Core\Validator as Validator;

class {{ENTITY}}Controller extends Controller {
	
	public function indexAction()
	{
		$repository = ServiceLayer::getService('{{NAME}}_repository');
		$all = $repository->findAll();

		$layout = ServiceLayer::getService('layout');
		$layout->setTemplateParameters(array(
				'title' => 'nav.{{NAME}}.title',
				'subtitle' => 'nav.{{NAME}}.list',
				'crumb' => array('nav.{{NAME}}.title' => '{{NAME}}_list', 'nav.{{NAME}}.list' => '{{NAME}}_list'))
				);
		
		$admin_table = new DatagridTable();
		$admin_table->setTableName('{{TABLE}}');
		
		return $this->render('{{NAMESPACE}}\{{MODULE_NAME}}:{{ENTITY}}:index.php',
				array(
						'admin_table' => $admin_table
				));
	}
	
	public function addAction()
	{
		$request = ServiceLayer::getService('request');
		$repository = ServiceLayer::getService('{{NAME}}_repository');
		$translator = ServiceLayer::getService('translator');
		$form = new {{ENTITY}}Form();
		$form->setEntity(new {{ENTITY}}());
		
		$validator = new Validator();
		$validator->setRules($form->getFields());
		
		if('POST' == $request->getMethod())
		{
			$post_data = $request->get('record');
			$form->bindData($post_data);
			$validator->setData($post_data);
			$validator->validateData();
			
			if(!$validator->hasErrors())
			{
				$entity = $form->getEntity();
				$repository->save($entity);
				$request->addFlashMessage('{{NAME}}_add_success', $translator->translate('{{NAME}}.crud_add.success'));
				$request->redirect();
			}
		}

		$layout = ServiceLayer::getService('layout');
		$layout->setTemplateParameters(array(
				'title' => 'nav.{{NAME}}.title',
				'subtitle' => 'nav.{{NAME}}.add_{{NAME}}',
				'crumb' => array('nav.{{NAME}}.title' => '{{NAME}}_list', 'nav.{{NAME}}.add_{{NAME}}' => '{{NAME}}_add'))
		);
		
		return $this->render('{{NAMESPACE}}\{{MODULE_NAME}}:{{ENTITY}}:add.php',
				array(
						'form' => $form,
						'request' => $request,
						'validator' => $validator
				));
	}
	
	public function editAction()
	{
		$request = ServiceLayer::getService('request');
		$repository = ServiceLayer::getService('{{NAME}}_repository');
		$translator = ServiceLayer::getService('translator');

		
		${{NAME}} = $repository->find($request->get('id'));
		$form = new {{ENTITY}}Form();
		$form->setEntity(${{NAME}});
		
		$validator = new Validator();
		$validator->setRules($form->getFields());
	
		if('POST' == $request->getMethod())
		{
			$post_data = $request->get('record');
			$form->bindData($post_data);
			
			$validator->setData($post_data);
			$validator->validateData();
			
			if(!$validator->hasErrors())
			{
				$entity = $form->getEntity();
				$repository->save($entity);
				$request->addFlashMessage('{{NAME}}_edit_success', $translator->translate('{{NAME}}.crud_edit.success'));
				$request->redirect();
			}
		}
	
		$layout = ServiceLayer::getService('layout');
		
		$layout->setTemplateParameters(array(
				'title' => 'nav.{{NAME}}.title',
				'subtitle' => 'nav.{{NAME}}.edit_{{NAME}}',
				'crumb' => array('nav.{{NAME}}.title' => '{{NAME}}_list', 'nav.{{NAME}}.edit_{{NAME}}' => '{{NAME}}_edit'))
		);
	
		return $this->render('{{NAMESPACE}}\{{MODULE_NAME}}:{{ENTITY}}:edit.php',
				array(
						'form' => $form,
						'request' => $request,
						'validator' => $validator
				));
	}
}