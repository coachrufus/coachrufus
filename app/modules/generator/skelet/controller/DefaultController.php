<?php
namespace {{NAMESPACE}}\Controller;
use Core\Controller as Controller;
use Core\Request as Request;
use Core\ServiceLayer;


class {{CONTROLLER_NAME}}Controller extends Controller {
	
	public function infoAction()
	{
		
		
		return $this->render('{{NAMESPACE}}\{{MODULE_NAME}}:{{ENTITY}}:info.php',
				array(
						'foo' => 'foo'
				));
	}
	
	
	
	
}