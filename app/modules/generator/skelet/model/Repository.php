<?php
namespace {{NAMESPACE}}\Model;
use Core\Repository as Repository;


ServiceLayer::addService('{{ENTITY}}Repository',array(
'class' => "{{NAMESPACE}}\Model\{{ENTITY}}Repository",
	'params' => array(
		new \Core\DbStorage('{{TABLE}}'),
		new \Core\EntityMapper('{{NAMESPACE}}\Model\{{ENTITY}}')
	)
));

class {{ENTITY}}Repository extends Repository
{

}
