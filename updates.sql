ALTER TABLE `co_users` ADD COLUMN `activation_guid` CHAR(36) AFTER `height`;

CREATE TABLE `log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `co_user_id` int(10) unsigned NOT NULL,
  `type` int(4) unsigned NOT NULL,
  `note` text COLLATE utf8_slovak_ci,
  `date` datetime NOT NULL,
  `ip` varchar(45) COLLATE utf8_slovak_ci NOT NULL,
  `host` varchar(255) COLLATE utf8_slovak_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovak_ci;

ALTER TABLE `co_notifications` MODIFY COLUMN `message` TEXT CHARACTER SET utf8 COLLATE utf8_slovak_ci DEFAULT NULL;

ALTER TABLE `playground` ADD COLUMN `area_type` VARCHAR(255) NULL DEFAULT NULL COMMENT 'Indoor/outdoor' AFTER `description`;

CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `subject` varchar(45) COLLATE utf8_slovak_ci DEFAULT NULL,
  `guid` char(36) COLLATE utf8_slovak_ci NOT NULL,
  `date` datetime NOT NULL,
  `data` text COLLATE utf8_slovak_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovak_ci;

CREATE TABLE `notification_blacklist` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovak_ci;

DELIMITER $$

DROP FUNCTION IF EXISTS `date_minus_days` $$
CREATE DEFINER=`root`@`localhost` FUNCTION `date_minus_days`(`date` DATETIME, days INT(10)) RETURNS datetime
BEGIN
  RETURN DATE_SUB(DATE(`date`), INTERVAL days DAY);
END $$

DELIMITER ;

DELIMITER $$

DROP FUNCTION IF EXISTS `was_before_days` $$
CREATE DEFINER=`root`@`localhost` FUNCTION `was_before_days`(`date` DATETIME, `days` INT) RETURNS int(11)
BEGIN
  IF DATE(`date`) = date_minus_days(now(), `days`) THEN RETURN 1;
  ELSE RETURN 0;
  END IF;
END $$

DELIMITER ;

ALTER TABLE `notification_blacklist` ADD `notification_type` VARCHAR(255) NULL ;

ALTER TABLE `co_users` ADD COLUMN `persist_hash` VARCHAR(255) NULL DEFAULT NULL AFTER `activation_guid`;

ALTER TABLE `co_users` ADD COLUMN `address` VARCHAR(256) NOT NULL AFTER `email`;


CREATE TABLE `ui` (
  `id` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INTEGER UNSIGNED NOT NULL,
  `settings` TEXT NOT NULL,
  PRIMARY KEY (`id`)
)
ENGINE = InnoDB
CHARACTER SET utf8 COLLATE utf8_slovak_ci;

ALTER TABLE `team_event` DROP INDEX `IXFK_event_Playground`;

ALTER TABLE `team_event` ADD `playground_data` TEXT NULL ;

CREATE TABLE  `chat` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `from_user_id` int(10) unsigned NOT NULL,
  `to_user_id` int(10) unsigned NOT NULL,
  `body` text COLLATE utf8_slovak_ci NOT NULL,
  `datetime` datetime NOT NULL,
  `is_readed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `team_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=281 DEFAULT CHARSET=utf8 COLLATE=utf8_slovak_ci;

CREATE TABLE `message` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`created_at` DATETIME NULL DEFAULT NULL,
	`sender_name` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_slovak_ci',
	`sender_email` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_slovak_ci',
	`recipient_name` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_slovak_ci',
	`recipient_email` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_slovak_ci',
	`subject` VARCHAR(100) NULL DEFAULT NULL COLLATE 'utf8_slovak_ci',
	`Sloupec 6` VARCHAR(100) NULL DEFAULT NULL COLLATE 'utf8_slovak_ci',
	`body` TEXT NULL COLLATE 'utf8_slovak_ci',
	`sender_id` INT(11) NULL DEFAULT NULL,
	`recipient_id` INT(11) NULL DEFAULT NULL,
	`status` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_slovak_ci',
	`type` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_slovak_ci',
	`team_id` INT(11) NULL DEFAULT NULL,
	`recipient_team_player_id` INT(11) NULL DEFAULT NULL,
	`sender_team_player_id` INT(11) NULL DEFAULT NULL,
	`reply_group` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_slovak_ci',
	`reply_id` INT(11) NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	INDEX `message_co_users_sender_id` (`sender_id`),
	INDEX `message_co_users_recipient_id` (`recipient_id`),
	CONSTRAINT `message_co_users_recipient_id` FOREIGN KEY (`recipient_id`) REFERENCES `co_users` (`id`),
	CONSTRAINT `message_co_users_sender_id` FOREIGN KEY (`sender_id`) REFERENCES `co_users` (`id`)
)
COLLATE='utf8_slovak_ci'
ENGINE=InnoDB
AUTO_INCREMENT=62;

CREATE TABLE `scouting` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`created_at` DATETIME NULL DEFAULT NULL,
	`type` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_slovak_ci',
	`status` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_slovak_ci',
	`team_id` INT(11) NULL DEFAULT NULL,
	`team_name` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_slovak_ci',
	`player_id` INT(11) NULL DEFAULT NULL,
	`player_name` VARCHAR(250) NULL DEFAULT NULL COLLATE 'utf8_slovak_ci',
	`playground_id` INT(11) NULL DEFAULT NULL,
	`playground_name` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_slovak_ci',
	`sport_id` INT(11) NULL DEFAULT NULL,
	`level` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_slovak_ci',
	`location_data` TEXT NULL COLLATE 'utf8_slovak_ci',
	`author_id` INT(11) NULL DEFAULT NULL,
	`player_count` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_slovak_ci',
	`location_name` VARCHAR(200) NULL DEFAULT NULL COLLATE 'utf8_slovak_ci',
	`valid_to` DATETIME NULL DEFAULT NULL,
	`game_time` DATETIME NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_slovak_ci'
ENGINE=InnoDB
AUTO_INCREMENT=29;


ALTER TABLE `ui` ADD COLUMN `team_id` INT(10) UNSIGNED NOT NULL AFTER `settings`;

CREATE TABLE `team_follow` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`user_id` INT(11) NULL DEFAULT NULL,
	`team_id` INT(11) NULL DEFAULT NULL,
	`created_at` DATETIME NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_slovak_ci'
ENGINE=InnoDB
AUTO_INCREMENT=2;

CREATE TABLE `player_follow` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`player_id` INT(11) NULL DEFAULT NULL,
	`follower_id` INT(11) NULL DEFAULT NULL,
	`created_at` DATETIME NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_slovak_ci'
ENGINE=InnoDB
AUTO_INCREMENT=3;


CREATE TABLE `player_wall_post` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`created_at` DATETIME NOT NULL,
	`author_id` INT(11) NOT NULL,
	`status` VARCHAR(50) NOT NULL COLLATE 'utf8_slovak_ci',
	`body` TEXT NOT NULL COLLATE 'utf8_slovak_ci',
	PRIMARY KEY (`id`)
)
COLLATE='utf8_slovak_ci'
ENGINE=InnoDB
AUTO_INCREMENT=8;


CREATE TABLE `player_wall_post_comment` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`post_id` INT(11) NULL DEFAULT NULL,
	`author_id` INT(11) NULL DEFAULT NULL,
	`body` TINYTEXT NULL COLLATE 'utf8_slovak_ci',
	`created_at` DATETIME NULL DEFAULT NULL,
	`status` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_slovak_ci',
	PRIMARY KEY (`id`),
	INDEX `FK_player_wall_post_comment_co_users` (`author_id`),
	CONSTRAINT `FK_player_wall_post_comment_co_users` FOREIGN KEY (`author_id`) REFERENCES `co_users` (`id`)
)
COLLATE='utf8_slovak_ci'
ENGINE=InnoDB
AUTO_INCREMENT=11;

ALTER TABLE `scouting`
	ADD COLUMN `lat` FLOAT NULL DEFAULT NULL AFTER `game_time`,
	ADD COLUMN `lng` FLOAT NULL DEFAULT NULL AFTER `lat`;


ALTER TABLE `team_players`
	CHANGE COLUMN `player_number` `player_number` INT NULL DEFAULT NULL AFTER `email`;


ALTER TABLE `sport`
	ADD COLUMN `add_stat_route` VARCHAR(255) NULL AFTER `name`,
        ADD COLUMN `stat_route` VARCHAR(255) NULL AFTER `add_stat_route` ;

ALTER TABLE `player_timeline_event`
	ADD COLUMN `points` INT(11) NULL DEFAULT NULL AFTER `team_id`,
	ADD COLUMN `duration_time` DATETIME NULL DEFAULT NULL AFTER `points`,
	ADD COLUMN `set_position` INT(11) NULL DEFAULT NULL AFTER `duration_time`
;

-----------------PLATBY--------------------------------------------------------

CREATE TABLE `packages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(75) COLLATE utf8_slovak_ci NOT NULL,
  `note` text COLLATE utf8_slovak_ci NOT NULL,
  `price_month` decimal(10,2) NOT NULL,
  `price_year` decimal(10,2) NOT NULL,
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `creation_date` datetime NOT NULL,
  `last_update` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovak_ci;

CREATE TABLE `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(75) COLLATE utf8_slovak_ci NOT NULL,
  `name` varchar(75) COLLATE utf8_slovak_ci NOT NULL,
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovak_ci;

CREATE TABLE `package_products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `package_id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `FK_package_products_1` (`package_id`),
  KEY `FK_package_products_2` (`product_id`),
  CONSTRAINT `FK_package_products_1` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_package_products_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovak_ci;

CREATE TABLE `user_packages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `package_id` int(10) unsigned NOT NULL,
  `team_id` int(11) NOT NULL,
  `team_name` varchar(75) COLLATE utf8_slovak_ci NOT NULL,
  `date` datetime NOT NULL,
  `start_date` datetime NOT NULL,
  `name` varchar(75) COLLATE utf8_slovak_ci NOT NULL,
  `note` text COLLATE utf8_slovak_ci NOT NULL,
  `price_month` decimal(10,2) DEFAULT NULL,
  `price_year` decimal(10,2) DEFAULT NULL,
  `products` text COLLATE utf8_slovak_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_user_packages_1` (`package_id`),
  KEY `FK_user_packages_2` (`user_id`),
  KEY `FK_user_packages_3` (`team_id`),
  CONSTRAINT `FK_user_packages_1` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_user_packages_2` FOREIGN KEY (`user_id`) REFERENCES `co_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_user_packages_3` FOREIGN KEY (`team_id`) REFERENCES `team` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovak_ci;

ALTER TABLE `player_timeline_event`
	ADD COLUMN `hit_group` VARCHAR(50) NULL AFTER `set_position`;

ALTER TABLE `translations`
	ADD COLUMN `sk_tooltip` VARCHAR(2000) NULL AFTER `en_value`,
	ADD COLUMN `en_tooltip` VARCHAR(2000) NULL AFTER `sk_tooltip`;



CREATE TABLE `team_wall_post` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`created_at` DATETIME NOT NULL,
	`author_id` INT(11) NOT NULL,
	`status` VARCHAR(50) NOT NULL COLLATE 'utf8_slovak_ci',
	`body` TEXT NOT NULL COLLATE 'utf8_slovak_ci',
	`team_id` INT(11) NOT NULL,
	`add_content` TEXT NULL COLLATE 'utf8_slovak_ci',
	`images` TEXT NULL COLLATE 'utf8_slovak_ci',
	PRIMARY KEY (`id`)
)
COLLATE='utf8_slovak_ci'
ENGINE=InnoDB
ROW_FORMAT=COMPACT
AUTO_INCREMENT=48;

CREATE TABLE `team_wall_post_comment` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`post_id` INT(11) NULL DEFAULT NULL,
	`author_id` INT(11) NULL DEFAULT NULL,
	`body` TINYTEXT NULL COLLATE 'utf8_slovak_ci',
	`created_at` DATETIME NULL DEFAULT NULL,
	`status` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_slovak_ci',
	PRIMARY KEY (`id`)
)
COLLATE='utf8_slovak_ci'
ENGINE=InnoDB
ROW_FORMAT=COMPACT
AUTO_INCREMENT=11;

CREATE TABLE `team_wall_like` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`post_id` INT(11) NULL DEFAULT NULL,
	`user_id` INT(11) NULL DEFAULT NULL,
	`created_at` DATETIME NULL DEFAULT NULL,
	`type` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_slovak_ci',
	PRIMARY KEY (`id`),
	INDEX `user_id` (`user_id`)
)
COLLATE='utf8_slovak_ci'
ENGINE=InnoDB
AUTO_INCREMENT=34;


CREATE TABLE `player_friends` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`player_id` INT(11) NULL DEFAULT NULL,
	`friend_id` INT(11) NULL DEFAULT NULL,
	`created_at` DATETIME NULL DEFAULT NULL,
	`status` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_slovak_ci',
	`friend_created_at` DATETIME NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_slovak_ci'
ENGINE=InnoDB
ROW_FORMAT=COMPACT
AUTO_INCREMENT=3;

ALTER TABLE `message` ADD `body_data` TEXT NULL , ADD `rest_body` TEXT NULL ;

ALTER TABLE `team_event`
	ADD COLUMN `end_time` DATETIME NULL AFTER `playground_data`;

ALTER TABLE `team`
	ADD COLUMN `locality_id` INT NULL DEFAULT NULL AFTER `full_address`;

ALTER TABLE `playground` ADD `zip` VARCHAR(255) NULL , ADD `country` VARCHAR(255) NULL , ADD `import_sport` VARCHAR(255) NULL ;

ALTER TABLE `scouting`
	ADD COLUMN `game_time_to` DATETIME NULL DEFAULT NULL AFTER `game_time`;

ALTER TABLE `co_users`
	ADD COLUMN `last_login` DATETIME NULL AFTER `address`;

ALTER TABLE `co_users`
	ADD COLUMN `country` VARCHAR(50) NULL DEFAULT NULL AFTER `timezone`;

<<<<<<< HEAD
CREATE TABLE `team_achievement_alert_log` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_slovak_ci',
	`achievement_code` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_slovak_ci',
	`created_at` DATETIME NULL DEFAULT NULL,
	`created_by` INT(11) NULL DEFAULT NULL,
	`team_id` INT(11) NULL DEFAULT NULL,
	`alert_status` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_slovak_ci',
	`place_code` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_slovak_ci',
=======

CREATE TABLE `system_log` (
	`id` INT(10) NOT NULL AUTO_INCREMENT,
	`date` DATETIME NULL DEFAULT NULL,
	`activity` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_slovak_ci',
	`detail` TEXT NULL COLLATE 'utf8_slovak_ci',
	`scope` TEXT NULL COLLATE 'utf8_slovak_ci',
	`ip` TEXT NULL COLLATE 'utf8_slovak_ci',
	`browser` TEXT NULL COLLATE 'utf8_slovak_ci',
>>>>>>> master
	PRIMARY KEY (`id`)
)
COLLATE='utf8_slovak_ci'
ENGINE=InnoDB
<<<<<<< HEAD
AUTO_INCREMENT=1;

ALTER TABLE `player_score` ADD `team_player_id` INT NULL ;

CREATE TABLE IF NOT EXISTS `team_achievement_alert_log` (
`id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_slovak_ci DEFAULT NULL,
  `achievement_code` varchar(50) COLLATE utf8_slovak_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `team_id` int(11) DEFAULT NULL,
  `alert_status` varchar(50) COLLATE utf8_slovak_ci DEFAULT NULL,
  `place_code` varchar(50) COLLATE utf8_slovak_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=utf8 COLLATE=utf8_slovak_ci;

--
-- Klíče pro exportované tabulky
--

--
-- Klíče pro tabulku `team_achievement_alert_log`
--
ALTER TABLE `team_achievement_alert_log`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `team_achievement_alert_log`
--
ALTER TABLE `team_achievement_alert_log`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=84;

ALTER TABLE `team_wall_post`
	ADD COLUMN `parent_id` INT NULL AFTER `images`;
	ALTER TABLE `team_wall_post`
	ADD COLUMN `lvl` INT(11) NULL DEFAULT NULL AFTER `parent_id`;
	ALTER TABLE `team_wall_post`
	CHANGE COLUMN `lvl` `lvl` INT(11) NULL DEFAULT '0' AFTER `parent_id`;

ALTER TABLE `sport`
	ADD COLUMN `priority` INT NULL AFTER `stat_route`;
update sport set add_stat_route = 'team_match_create_live_stat' where id in (1 ,2 ,3 ,4 ,16 ,41 ,42 ,43 ,53 ,68);
update sport set priority = 1 where id in (1 ,2 ,3 ,4 ,16 ,41 ,42 ,43 ,53 ,68);  
UPDATE `coachrufus`.`sport` SET `priority`=1 WHERE  `id`=77;
update sport set priority = 3 where id in (7 ,11 ,14 ,18 ,20 ,21 ,22 ,23 ,24 ,27 ,28 ,29 ,31 ,32 ,33 ,34 ,35 ,36 ,37 ,38 ,39 ,40 ,44 ,45 ,46 ,47 ,48 ,49 ,51 ,52 ,54 ,55 ,56 ,57 ,58 ,59 ,63 ,64 ,66 ,69 ,70 ,71 ,73 ,74 ,75 ,76 ,78 ,80 ,81 ,82 ,83 ,84 ,85 ,86 ,87 ,88 ,89 ,90 ,91 ,92 ,93 ,94 ,95 ,96 ,97 ,98 ,99 ,100 ,101 ,105 ,107 ,108 ,111 ,113 ,114 ,117 ,118 ,119)

ALTER TABLE `order_data`
	ADD COLUMN `payment_id` VARCHAR(100) NULL AFTER `data`,
	ADD COLUMN `payment_data` TEXT NULL AFTER `payment_id`;


ALTER TABLE `order_data`
ADD COLUMN `team_id` INT NULL AFTER `payment_id`,	
	ADD COLUMN `user_id` INT NULL AFTER `team_id`,
	ADD COLUMN `renew_payment` INT NULL AFTER `user_id`,
	ADD COLUMN `created_at` DATETIME NULL AFTER `renew_payment`;


ALTER TABLE `invoices`
	ADD COLUMN `ip` VARCHAR(50) NULL AFTER `end_date`,
	ADD COLUMN `ip_country` VARCHAR(100) NULL AFTER `ip`,
	ADD COLUMN `pay_country` VARCHAR(100) NULL AFTER `ip_country`;
AUTO_INCREMENT=81;

CREATE TABLE `queue` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`action_key` VARCHAR(100) NULL DEFAULT NULL COLLATE 'utf8_slovak_ci',
	`value` TEXT NULL COLLATE 'utf8_slovak_ci',
	`action_type` VARCHAR(100) NULL DEFAULT NULL COLLATE 'utf8_slovak_ci',
	`status` VARCHAR(100) NULL DEFAULT NULL COLLATE 'utf8_slovak_ci',
	PRIMARY KEY (`id`)
)
COLLATE='utf8_slovak_ci'
ENGINE=InnoDB
AUTO_INCREMENT=5;


CREATE TABLE `fllorball_challenge_teams` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`team_id` INT(11) NULL DEFAULT NULL,
	`coupon_code` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_slovak_ci',
	`activation_date` DATETIME NULL DEFAULT NULL,
	`ip` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_slovak_ci',
	PRIMARY KEY (`id`)
)
COLLATE='utf8_slovak_ci'
ENGINE=InnoDB
AUTO_INCREMENT=3;


CREATE TABLE `user_packages` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`user_id` INT(11) NOT NULL,
	`package_id` INT(10) UNSIGNED NOT NULL,
	`team_id` INT(11) NOT NULL,
	`team_name` VARCHAR(75) NOT NULL COLLATE 'utf8_slovak_ci',
	`date` DATETIME NOT NULL,
	`start_date` DATETIME NOT NULL,
	`end_date` DATETIME NOT NULL,
	`name` VARCHAR(75) NOT NULL COLLATE 'utf8_slovak_ci',
	`note` TEXT NOT NULL COLLATE 'utf8_slovak_ci',
	`level` INT(10) UNSIGNED NOT NULL,
	`price_month` DECIMAL(10,2) NULL DEFAULT NULL,
	`price_year` DECIMAL(10,2) NULL DEFAULT NULL,
	`products` TEXT NOT NULL COLLATE 'utf8_slovak_ci',
	`player_limit` INT(10) UNSIGNED NULL DEFAULT NULL,
	`is_canceled` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
	`guid` CHAR(36) NOT NULL COLLATE 'utf8_slovak_ci',
	`order_id` INT(11) NOT NULL,
	`variant` INT(11) NOT NULL,
	`renew_payment` INT(11) NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	INDEX `FK_user_packages_1` (`package_id`),
	INDEX `FK_user_packages_2` (`user_id`),
	INDEX `FK_user_packages_3` (`team_id`)
)
COLLATE='utf8_slovak_ci'
ENGINE=InnoDB
AUTO_INCREMENT=470;

ALTER TABLE `team_event_attendance` ADD COLUMN `fee` float;

CREATE TABLE `team_event_costs` (
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `event_id` int(11) DEFAULT NULL,
	  `event_date` datetime DEFAULT NULL,
	  `costs` float DEFAULT NULL,
	  `created_at` datetime DEFAULT NULL,
	  PRIMARY KEY (`id`),
	  KEY `IXFK_costs_event` (`event_id`),
	  CONSTRAINT `FK_costs_event` FOREIGN KEY (`event_id`) REFERENCES `team_event` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8
